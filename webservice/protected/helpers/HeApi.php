<?php
/**
 * HeApi
 * Author: mgadegaard
 * Date: 13/05/2015
 * Time: 16:47
 * © ABA English 2015
 */

class HeApi {

    /**
     * Function that provides Connection safe table name string for an auxiliary DB table needed from another model.
     *
     * @param string $tableName
     * @return string The full table name on the form db.table
     */
    public static function auxTableName($tableName) {
        return Yii::app()->params['dbWebservice'].'.'. $tableName;
    }

    /**
     * public static function that selects translations locale based on locale and unit id
     *
     * @param integer $idUnit Id of unit
     * @param string $locale requested locale
     * @return string $selectedLocale 2 letter representation of locale selected for unit
     */
    public static function localeSelector($idUnit, $locale) {
        switch($locale) {
            case 'de':
                $selectedLocale = $idUnit <= HeUnits::getEndUnitByLevel(3) ? $locale : 'en';
                break;
            case 'fr':
                $selectedLocale = $idUnit <= HeUnits::getEndUnitByLevel(4) ? $locale : 'en';
                break;
            case 'ru':
                $selectedLocale = $idUnit <= HeUnits::getEndUnitByLevel(3) ? $locale : 'en';
                break;
            case 'zh':
                $selectedLocale = $idUnit <= HeUnits::getEndUnitByLevel(3) ? $locale : 'en';
                break;
            case 'tr':
                $selectedLocale = $idUnit <= HeUnits::getEndUnitByLevel(3) ? $locale : 'en';
                break;
            default:
                $selectedLocale = $locale;
                break;
        }
        return $selectedLocale;
    }

    /**
     * public static function to set table name based on locale
     *
     * @param string $tableName
     * @param string $locale
     * @return string string
     */
    public static function tableNameSetter($tableName, $locale) {
        switch($locale) {
            case 'de':
                $tableName .= '_de';
                break;
            case 'en':
                $tableName .= '_en';
                break;
            case 'fr':
                $tableName .= '_fr';
                break;
            case 'it':
                $tableName .= '_it';
                break;
            case 'pt':
                $tableName .= '_pt';
                break;
            case 'ru':
                $tableName .= '_ru';
                break;
            case 'zh':
                $tableName .= '_zh';
                break;
            case 'tr':
                $tableName .= '_tr';
                break;
            default:
                break;
        }
        return $tableName;
    }
}