<?php

class XTokenFilter extends CFilter
{
    /**
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     *
     * @param CFilterChain $filterChain
     *
     * @return bool
     * @throws CHttpException
     */
    protected function preFilter($filterChain)
    {
        $xtoken = Yii::app()->params['X_TOKEN'];

        $serverXtoken = HeUtils::getRequestParamValue('X_TOKEN');

        if(trim($xtoken) == trim($serverXtoken)) {
            return true;
        }

        throw new CHttpException(401, 'Unknown token');
    }

}