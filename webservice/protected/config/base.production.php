<?php

return array(
  'components' => array(
    'cache' => array(
      'class' => 'system.caching.CMemCache',
    ),
    'config' => array(
      'class' => 'common.protected.extensions.econfig.EConfig',
      'cacheID' => 'cache'
    ),
  ),
  'params' => array(
    'X_TOKEN' => "650EB608-138D-4329-856A-554133DABAD6"
  ),
);
