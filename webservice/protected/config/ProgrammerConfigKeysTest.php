<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 15/02/13
 * Time: 9:31
 * Just to overwrite som config keys in order to be able to test specific scenarios, like HTTPS in development, SMTP connections, sending emails , etc
 *  without afffecting other developers.
 */


/*++++++++++++++++++++++++++++++Only for PROGRAMMERS should BE FILLED. NOT IN DEVELOPMENT,
 NOR PREPRODUCTION NOR PRODUCTION. ONLY IN LOCAL ENVIRONMENTS.
++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
$aMyOwnConfigKeys = array(
    'ENABLE_LOG_WEBSERVICES' => 1,
    'ENABLE_AUTH_BASIC_RESTSERVICES' => 1,
    'ENABLE_SSL' => 1,
);