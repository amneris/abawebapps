<?php

return array(
  'components' => array(
    'cache' => array(
      'class' => 'system.caching.CMemCache',
    ),
    'config' => array(
      'class' => 'common.protected.extensions.econfig.EConfig',
      'cacheID' => 'cache'
    ),
  ),
  'params' => array(
    'X_TOKEN' => "D360D423-B178-49E7-A2CE-A55107242324"
  ),
);
