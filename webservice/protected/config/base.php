<?php
// This is the main Web application configuration. Any writable CWebApplication properties can be configured here.

return array(
    // basePath is path to webservice/protected/*, which is 'application'
    'basePath'   => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name'       => 'ABA English - Webservice',
    'theme'      => 'ABAenglish',
    'preload'    => array('log'),
    // Auto-loading model and component classes
    'import'     => array(
        /* All models and rules */
        'application.models.geo.*',
        'application.models.user.*',
        'application.models.course.*',
        'application.models.glossary.*',
        'application.models.logs.*',
        'application.models.forms.*',
        'application.models.product.*',
        /* Customized filters */
        'application.filters.*',
    ),
    /*'modules'=>array(
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'abaenglish',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'=>array('127.0.0.1','::1'),
        ),
    ),*/
    // application components
    'components' => array(
        'user'         => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        'urlManager'   => array(
            'urlFormat' => 'path',
            'rules'     => array(
                //<-- REST API Patterns -->
              array('root/Preflight', 'pattern' => 'api/*', 'verb' => 'OPTIONS'),
              array('root/MyIP', 'pattern' => 'api/users/me/ip', 'verb' => 'GET'),
              array(
                    '<controller>/index',
                    'pattern' => 'api/(<locale:(es|en|pt|it|fr|de|ru|zh|tr)>/)?<controller:\w+>',
                    'verb'    => 'GET'
                ),
                array(
                    '<controller>/view',
                    'pattern' => 'api/(<locale:(es|en|pt|it|fr|de|ru|zh|tr)>/)?<controller:\w+>/<viewCriteria:\d+>',
                    'verb'    => 'GET'
                ),
                array(
                    '<controller>/index',
                    'pattern' => 'api/(<locale:(es|en|pt|it|fr|de|ru|zh|tr)>/)?<controller:\w+>/<indexCriteria1:\d+>' .
                        '(/<indexCriteria2:\d+>)?',
                    'verb'    => 'GET'
                ),
                array(
                    '<controller>/view',
                    'pattern' => 'api/(<locale:(es|en|pt|it|fr|de|ru|zh|tr)>/)?<controller:\w+>/<action:\w+>' .
                        '(/<viewCriteria1:\d+>)?(/<viewCriteria2:\w+>)?(/<viewCriteria3:\d+>)?(/<viewCriteria4:\d+>)?',
                    'verb'    => 'GET'
                ),
                array(
                    '<controller>/<action>',
                    'pattern' => 'api/(<locale:(es|en|pt|it|fr|de|ru|zh|tr)>/)?<controller:\w+>/<action:\w+>',
                    'verb'    => 'POST'
                ),
                array('<model>/update', 'pattern' => 'api/<model:\w+>/<id:\d+>', 'verb' => 'PUT'),
                array('<model>/delete', 'pattern' => 'api/<model:\w+>/<id:\d+>', 'verb' => 'DELETE'),
                array('<model>/create', 'pattern' => 'api/<model:\w+>', 'verb' => 'POST'),
            ),
        ),
        'errorHandler' => array(
            'errorAction' => 'abaEnglishApi/error',
        ),
    ),
    // application-level parameters that can be accessed using Yii::app()->params['paramName']
    'params'     => array(
        // this is used in contact page
        'adminEmail'      => 'itbugs@abaenglish.com',
        'languages'       => array(
            'es' => 'Español',
            'en' => 'English',
            'pt' => 'Portugues',
            'it' => 'Italiano',
            'fr' => 'Français',
            'de' => 'German',
            'ru' => 'Ruso',
            'zh' => 'Chino'
        ),
        'levels'          => array(
            1 => "Beginners",
            2 => "Lower intermediate",
            3 => "Intermediate",
            4 => "Upper intermediate",
            5 => "Advanced",
            6 => "Business"
        ),
        'userType'        => array(
            DELETED => "Deleted",
            FREE    => "Free",
            PREMIUM => "Premium",
            TEACHER => "Teacher"
        ),
        'DbTransactional' => false,
        'dbVentasFlash'   => 'db_ventasflash',
        'dbCampusLogs'    => 'aba_b2c_logs',
        'dbCampusSummary' => 'aba_b2c_summary',
        'dbExtranet'      => 'aba_b2c_extranet',
        'dbWebservice'    => 'aba_course',

     		'logFacility' => (defined('LOG_LOCAL7') ? LOG_LOCAL7 : 8),
        'logGroup' => "webservice",
        'logName' => "[ABAWEBAPPS]",
        'X_TOKEN' => "37D7B5B6-58F2-4EEF-A3FB-3771856E60F0"
   ),
);
