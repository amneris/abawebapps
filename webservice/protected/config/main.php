<?php

defined('ABAWEBAPPS_APPNAME') or define('ABAWEBAPPS_APPNAME', 'webservice');

if( isset($_SERVER['ABAWEBAPPS_ENV']) ) {
    $envHost = $_SERVER['ABAWEBAPPS_ENV'];
    $fileConfig = dirname( __FILE__ ).'/base.'.$envHost.'.php';
    if ( is_file($fileConfig) ) {
        return CMap::mergeArray(
            (require ROOT_DIR . '/common/protected/config/main.php'),
            (require dirname(__FILE__) . '/base.php'),
            (require $fileConfig)
        );
    }
}

return CMap::mergeArray(
    (require ROOT_DIR . '/common/protected/config/main.php'),
    (require dirname(__FILE__) . '/base.php')
);