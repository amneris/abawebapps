
CREATE TABLE `aba_b2c_logs`.`log_course_api` (
  `id` int(10) unsigned NOT NULL auto_increment COMMENT 'Auto id, PK.',
  `dateRequest` datetime default NULL COMMENT 'Date and time of request',
  `ipFromRequest` varchar(17) default NULL COMMENT 'IP from the web service call.',
  `appNameFromRequest` varchar(45) default NULL COMMENT 'IP or name of the app. e.g.: Intranet, web',
  `urlRequest` varchar(80) default NULL COMMENT 'Full URL requested of our Campus WS.',
  `paramsRequest` text COMMENT 'QUERY STRING PARAMS',
  `nameService` varchar(30) default NULL COMMENT 'Requested endpoint',
  `dateResponse` datetime default NULL COMMENT 'Time of response',
  `paramsResponse` varchar(300) default NULL COMMENT 'Array of fields and values returned',
  `successResponse` tinyint(1) default NULL COMMENT '1 successful or 0 error',
  `errorCode` smallint(5) default NULL COMMENT 'Error code, if successResponse is 0',
  PRIMARY KEY  (`id`),
  KEY `byDateRequest` (`dateRequest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='To log requests for all calls to Course API.';