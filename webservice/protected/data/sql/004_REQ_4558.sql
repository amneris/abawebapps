/*
Requirement 4558 (Bug en el API). Some phrases has several translations and when concatenated we need to be able to
control the layout. To facilitate that, this script is deleting all leading and trailing spaces from phrase translation
tables.
*/
update phrase_translations set content =  trim(content);
update phrase_translations_it set content =  trim(content);
update phrase_translations_pt set content =  trim(content);