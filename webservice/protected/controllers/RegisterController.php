<?php

/**
 * Created by PhpStorm.
 * User: mikkel
 * Date: 19/08/15
 * Time: 10:56
 */
class RegisterController extends ApiController
{
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'register', 'registerfacebook'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function beforeAction($action)
    {
        $this->disableCWebLogRoute();
        $requestMethod = strtoupper($_SERVER['REQUEST_METHOD']);
        $data = array();
        if ($requestMethod == 'POST') {
            $data = HeMixed::getPostArgs();
        }
        $this->reqDataDecoded = $data;
        $this->reqMethod = strtoupper($requestMethod);
        return true;
    }

    /**
     * Function for api/<locale>/register/register WS endpoint, registers a new user
     *
     * @return bool|void
     */
    public function actionRegister()
    {
        Yii::app()->language = 'en';
        // Light security measure to avoid DOS attacks.
        HeMixed::securePublicUrlRequest("register/register");
        $signature = '';
        try {
            /*  Validation of parameters received */
            $validArray = array_intersect_key(
                array(
                    'signature'        => 1,
                    'deviceId'         => 2,
                    'sysOper'          => 3,
                    'deviceName'       => 4,
                    'email'            => 5,
                    'password'         => 6,
                    'langEnv'          => 7,
                    'name'             => 8,
                    'surnames'         => 9,
                    'ipMobile'         => 10,
                    'idPartner'        => 11,
                    'idSourceList'     => 12,
                    'deviceTypeSource' => 13,
                    'appVersion'       => 14
                ),
                $this->reqDataDecoded
            );
            if (!is_array($this->reqDataDecoded) || sizeof($validArray) < 10 || sizeof($validArray) > 14) {
                $this->sendResponse(400, array(
                    'status'  => 'Error',
                    'message' => 'Bad params received. Try again with the proper params.
                    Bear in mind that parameters are case sensitive.'
                ));

                return false;
            }
            $signature = $this->reqDataDecoded["signature"];
            $deviceId = $this->reqDataDecoded["deviceId"];
            $sysOper = $this->reqDataDecoded["sysOper"];
            $deviceName = $this->reqDataDecoded["deviceName"];
            $email = $this->reqDataDecoded["email"];
            $password = $this->reqDataDecoded["password"];
            $langEnv = $this->reqDataDecoded["langEnv"];
            $name = $this->reqDataDecoded["name"];
            $surnames = $this->reqDataDecoded["surnames"];
            $ipMobile = $this->reqDataDecoded["ipMobile"];
            $idPartner = null;
            $idSourceList = null;
            $deviceTypeSource = null;
            $appVersion = null;
            if (array_key_exists("idPartner", $this->reqDataDecoded)) {
                $idPartner = $this->reqDataDecoded["idPartner"];
            }
            if (array_key_exists("idSourceList", $this->reqDataDecoded)) {
                if ($this->reqDataDecoded["idSourceList"]!=""){
                    $idSourceList =  $this->reqDataDecoded["idSourceList"];
                }
            }

            if (array_key_exists("deviceTypeSource", $this->reqDataDecoded)) {
                $deviceTypeSource = $this->reqDataDecoded["deviceTypeSource"];
            }
            if (array_key_exists("appVersion", $this->reqDataDecoded)) {
                $appVersion = $this->reqDataDecoded["appVersion"];
            }

            /* deviceId, sysOper, deviceName, email, password langEnv name surnames ipMobile: */
            if (!$this->isValidSignature($signature, array(
                $deviceId,
                $sysOper,
                $deviceName,
                $email,
                $password,
                $langEnv,
                $name,
                $surnames,
                $ipMobile,
                $idPartner,
                $idSourceList,
                $deviceTypeSource,
                $appVersion
            ))) {
                $this->sendResponse(405);
                return false;
            }
            if (ip2long($ipMobile) === false) {
                $this->sendResponse(400, array("status" => 'Error', 'message' => 'IP not valid.'));
                return false;
            }
            $idCountry = Yii::app()->config->get('ABACountryId');
            $country = new CmAbaCountry();
            if ($country->getCountryCodeByIP($ipMobile)) {
                $idCountry = $country->getId();
            }
            if (empty($idPartner)) {
                $idPartner = PARTNER_ID_MOBILE;
            }
            if (empty($deviceTypeSource)) {
                $deviceTypeSource = DEVICE_SOURCE_MOBILE;
            }
            $registerUserRules = new CmAbaRegisterUserRules();
            $user = $registerUserRules->registerUser(
                $email,
                $password,
                $langEnv,
                $idCountry,
                $name,
                $surnames,
                1,
                $idPartner,
                $idSourceList,
                $deviceTypeSource
            );
            if ($user) {
                $token = $registerUserRules->registerNewDevice($deviceId, $sysOper, $deviceName, $user, $appVersion);
                if (!$token) {
                    $this->sendResponse(500, array(
                        'status'  => 'Error',
                        'message' => 'Your user was registered, ' .
                            'but your device could not be registered.'
                    ));
                    return false;
                }
                $teacher = new CmAbaUserTeacher($user->teacherId);
                $teacherMobileImage = '';
                $headerDevice = HeUtils::getServerValue('DEVICE');
                if ($headerDevice) {
                    $deviceRule = new DeviceRules();
                    $teacherMobileImage = $deviceRule->getMobileImageByDevice($headerDevice, $teacher);
                }

                // Return profiles
                $stProfiles = CmUserRules::getOrCreateUserExperimentProfilesWithType(
                  $user,
                  CmAbaExperimentsTypes::EXPERIMENT_TYPE_MOBILE_STYLE_CSS,
                  true
                );

                $this->sendResponse(
                    200,
                    array(
                        'userId'         => $user->getId(),
                        'userType'       => $user->userType,
                        'userLevel'      => $user->currentLevel,
                        'token'          => $token,
                        'countryNameIso' => $country->iso,
                        'userLang'       => $user->langEnv,
                        'teacherId'      => $user->teacherId,
                        'teacherName'    => $teacher->name,
                        'teacherImage'   => $teacherMobileImage == '' ? $teacher->mobileImage2x : $teacherMobileImage,
                        'expirationDate' => $user->expirationDate,
                        'idPartner'      => $user->idPartnerSource,
                        'idSource'       => is_null($user->idSourceList) ? "0" : $user->idSourceList,
                        'profiles'       => $stProfiles,
                        //#ABAWEBAPPS-681
                        'cooladata' => array(
                           array('userScope' => $this->getCooladataEvent('USERSCOPE', array('abaUser' => $user)))
                        )
                    ),
                    $this->_format
                );

                return true;
            } else {
                if ($registerUserRules->getErrorMessage() ==  Yii::t('mainApp', 'emailExist')) {
                    $this->sendResponse(444, array(
                      'status'  => 'Error',
                      'message' => $registerUserRules->getErrorMessage()
                    ));
                } else {
                    $this->sendResponse(500, array(
                      'status'  => 'Error',
                      'message' => 'Your user could not be registered successfully.'
                    ));
                }

                return false;
            }
        } catch (Exception $ex) {
            error_log($signature . " error=> " . $ex->getMessage());

            return $this->sendResponse(
                500,
                array(
                    'status'  => 'Exception',
                    'message' => "Internal error at register. Check with Aba IT Team and see logs Apache for " .
                        $signature
                )
            );
        }
    }

    /**
     * Function for api/<locale>/register/registerfacebook WS endpoint, registers a new user by facebook ID
     *
     * @return bool|void
     */
    public function actionRegisterfacebook()
    {
        Yii::app()->language = 'en';
        // Light security measure to avoid DOS attacks.
        HeMixed::securePublicUrlRequest("register/registerfacebook");
        if (Yii::app()->config->get("FB_APP_LOGIN_ENABLED") == 0) {
            return $this->sendResponse(500, array(
                'status' => 'Exception',
                'message' => "Internal error non identified at actionRegisterFacebook. Check with Aba IT Team"
            ));
        }
        $signature = '';
        $isnewuser = 0;
        $stProfiles = array();
        try {
            $validArray = array_intersect_key(
                array(
                    'signature' => 1,
                    'email' => 2,
                    'name' => 3,
                    'surnames' => 4,
                    'fbId' => 5,
                    'gender' => 6,
                    'langEnv' => 7,
                    'deviceId' => 8,
                    'sysOper' => 9,
                    'deviceName' => 10,
                    'ipMobile' => 11,
                    'idPartner' => 12,
                    'idSourceList' => 13,
                    'deviceTypeSource' => 14,
                    'appVersion' => 15
                ),
                $this->reqDataDecoded
            );
            if (!is_array($this->reqDataDecoded) || sizeof($validArray) < 11 || sizeof($validArray) > 15) {
                $this->sendResponse(400, array(
                    'status' => 'Error',
                    'message' => 'Bad params received. Try again with the proper params.
                    Bear in mind that parameters are case sensitive.'
                ));

                return false;
            }
            $ipMobile = $this->reqDataDecoded["ipMobile"];
            $idCountry = HeMixed::getCountryId($ipMobile);
            $signature = $this->reqDataDecoded["signature"];
            $email = $this->reqDataDecoded["email"];
            $name = $this->reqDataDecoded["name"];
            $surnames = $this->reqDataDecoded["surnames"];
            $fbId = $this->reqDataDecoded["fbId"];
            $gender = $this->reqDataDecoded["gender"];
            $langEnv = $this->reqDataDecoded["langEnv"];
            $deviceId = $this->reqDataDecoded["deviceId"];
            $sysOper = $this->reqDataDecoded["sysOper"];
            $deviceName = $this->reqDataDecoded["deviceName"];
            $idPartner = null;
            $idSourceList = null;
            $deviceTypeSource = null;
            $appVersion = null;
            if (array_key_exists("idPartner", $this->reqDataDecoded)) {
                $idPartner = $this->reqDataDecoded["idPartner"];
            }
            if (array_key_exists("idSourceList", $this->reqDataDecoded)) {
                $idSourceList = $this->reqDataDecoded["idSourceList"];
            }
            if (array_key_exists("deviceTypeSource", $this->reqDataDecoded)) {
                $deviceTypeSource = $this->reqDataDecoded["deviceTypeSource"];
            }
            if (array_key_exists("appVersion", $this->reqDataDecoded)) {
                $appVersion = $this->reqDataDecoded["appVersion"];
            }
            if (empty($fbId) || trim($fbId) == '' || $fbId == 0 || !is_numeric($fbId)) {
                $this->sendResponse(400, array("status" => 'Error', 'message' => 'Facebook ID not valid.'));
                return false;
            }

            if (!$this->isValidSignature($signature, array(
                $email,
                $name,
                $surnames,
                $fbId,
                $gender,
                $langEnv,
                $deviceId,
                $sysOper,
                $deviceName,
                $ipMobile,
                $idPartner,
                $idSourceList,
                $deviceTypeSource,
                $appVersion
            ))) {
                $this->sendResponse(405, array("status" => 'Error', 'message' => 'Invalid Signature'));
                return false;
            }
            if (ip2long($ipMobile) === false) {
                $this->sendResponse(400, array("status" => 'Error', 'message' => 'IP not valid.'));
                return false;
            }
            if (empty($idPartner)) {
                $idPartner = PARTNER_ID_MOBILE;
            }
            if (empty($deviceTypeSource)) {
                $deviceTypeSource = DEVICE_SOURCE_MOBILE;
            }
            $registerUserRules = new CmAbaRegisterUserRules();
            $this->user = $registerUserRules->checkIfFbUserExists($fbId);
            if ($this->user) {
                $this->device = new CmAbaUserDevice();
                if (!$this->device->getDeviceByIdUserId($deviceId, $this->user->id)) {
                    $token = $registerUserRules->registerNewDevice(
                        $deviceId,
                        $sysOper,
                        $deviceName,
                        $this->user,
                        $appVersion
                    );
                    if (!$token) {
                        $this->sendResponse(
                            500,
                            array(
                                'status' => 'Error',
                                'message' => 'Your user was created, but your device could not be registered.'
                            )
                        );

                        return false;
                    }
                } else {
                    $token = $this->device->token;
                }
            } else {
                $currentLevel = 1;
                $abaExistingUser = new CmAbaUser();
                $bUserExists = $abaExistingUser->populateUserByEmail($email);
                $this->user = $registerUserRules->registerUserFacebook(
                    $email,
                    $name,
                    $surnames,
                    $fbId,
                    $gender,
                    $langEnv,
                    $idCountry,
                    $idPartner,
                    $idSourceList,
                    $deviceTypeSource,
                    $currentLevel
                );
                if ($this->user) {
                    if (!$bUserExists) {
                        $isnewuser = 1;

                        // Return profiles
                        $stProfiles = CmUserRules::getOrCreateUserExperimentProfilesWithType(
                          $this->user,
                          CmAbaExperimentsTypes::EXPERIMENT_TYPE_MOBILE_STYLE_CSS,
                          true
                        );
                    }
                    $registerUserRules->registerGeoLocation($this->user, $ipMobile);
                    $token = $registerUserRules->registerNewDevice(
                        $deviceId,
                        $sysOper,
                        $deviceName,
                        $this->user,
                        $appVersion
                    );
                    if (!$token) {
                        $this->sendResponse(
                            500,
                            array(
                                'status' => 'Error',
                                'message' => 'Your user was registered, but your device could not be registered.'
                            )
                        );
                        return false;
                    }
                } else {
                    $this->sendResponse(
                        500,
                        array('status' => 'Error', 'message' => 'Your user could not be registered successfully.')
                    );
                    return false;
                }
            }
            if ($this->user === null) {
                // Error: Unauthorized
                $this->sendResponse(401, array('status' => 'Error', 'message' => Yii::t('mainApp', 'user_unknown')));
                return false;
            }
            $moCountry = new CmAbaCountry($this->user->countryId);
            $isoCodeCountry = $moCountry->iso;
            $teacher = new CmAbaUserTeacher($this->user->teacherId);
            $teacherMobileImage = '';
            $headerDevice = $headerDevice = HeUtils::getServerValue('DEVICE');
            if ($headerDevice) {
                $deviceRule = new DeviceRules();
                $teacherMobileImage = $deviceRule->getMobileImageByDevice($headerDevice, $teacher);
            }
            $productsPricesRules = new CmAbaProductPriceRules();
            $idPeriodPay = $productsPricesRules->getUserProductPeriodPay($this->user);

            // Return profiles
            $stProfiles = CmUserRules::getOrCreateUserExperimentProfilesWithType(
              $this->user,
              CmAbaExperimentsTypes::EXPERIMENT_TYPE_MOBILE_STYLE_CSS,
              false
            );

            $this->sendResponse(
                200,
                array(
                    'status' => 'OK',
                    'token' => $token,
                    'userId' => $this->user->getId(),
                    'userType' => $this->user->userType,
                    'userLevel' => $this->user->currentLevel,
                    'name' => $this->user->name,
                    'surnames' => $this->user->surnames,
                    'countryNameIso' => $isoCodeCountry,
                    'userLang' => $this->user->langEnv,
                    'teacherId' => $this->user->teacherId,
                    'teacherName' => $teacher->name,
                    'teacherImage' => $teacherMobileImage == '' ? $teacher->mobileImage2x : $teacherMobileImage,
                    'expirationDate' => $this->user->expirationDate,
                    'idPeriod' => $idPeriodPay,
                    'isnewuser' => $isnewuser,
                    'idPartner' => $this->user->idPartnerSource,
                    'idSource' => is_null($this->user->idSourceList) ? "0" : $this->user->idSourceList,
                    'profiles' => $stProfiles,
                    //#ABAWEBAPPS-681
                    'cooladata' => array(
                       array('userScope' => $this->getCooladataEvent('USERSCOPE', array('abaUser' => $this->user)))
                    )
                ),
                $this->_format
            );
            return true;
        } catch (Exception $ex) {
            error_log($signature . " error=> " . $ex->getMessage());
            return $this->sendResponse(
                500,
                array(
                    'status' => 'Exception',
                    'message' => "Internal error non identified at actionRegister. " .
                        "Check with Aba IT Team and see logs Apache for " . $signature
                )
            );
        }
    }

    public function actionView()
    {
        return true;
    }
}
