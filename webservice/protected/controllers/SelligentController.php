<?php
/**
 * SelligentController
 * © ABA English 2015
 */

class SelligentController extends ApiController {

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('view', 'tier'),
                'users'=>array('*'),
            )
        );
    }

    public function actionView()
    {
        $idUser = isset($_GET['viewCriteria']) ? $_GET['viewCriteria'] : null;

        $endpointSelligent = Yii::app()->config->get("URL_PROMO_DYNAMIC").$idUser;

        try {
            $urlPromocodeSelligent = file_get_contents($endpointSelligent);

            $posPromo = strpos($urlPromocodeSelligent, 'promocode=');
            if($posPromo)
            {
                $posPromo +=  10;
                $posFinalBody = strpos($urlPromocodeSelligent, '</body>');

                $final = $posFinalBody - $posPromo;

                $promocode = substr($urlPromocodeSelligent, $posPromo, $final);
            }else{
                $promocode ='free_generico';
            }
        }
        catch (Exception $problem) {

            HeLogger::sendLog(
              HeLogger::PREFIX_APP_LOGIC_L . ', looking for a promocode.',
              HeLogger::IT_BUGS,
              HeLogger::CRITICAL,
              "La llamada al endpoint de Selligent : " . $endpointSelligent . " con el usuario: " . $idUser ." No devuelve ningun promocode extraible. Resultado: " . $urlPromocodeSelligent
            );

            $promocode ='free_generico';
        }


        $moPromo = new ProductPromo();

        $modelPromo = $moPromo->getProdPromoSelligentById($promocode);


         //USUARIO

        $user = CmAbaUser::model('CmAbaUser')->findByPk($idUser);

        $country = CmAbaCountry::model('CmAbaCountry')->findByPk($user->countryId);
        $typeCurrency = $country->ABAIdCurrency;

        $enchangeRealtoUSD = 1;

        switch ($country->id) {
            case 199: //Spain
            case 73: //France
            case 138: //Mexico
            case 226: //USA
                break;
            case 30: //Brazil
                $currency = CmCurrencies::model('CmCurrencies')->findByPk($country->ABAIdCurrency);
                $enchangeRealtoUSD = $currency->outUsd;
                $typeCurrency = "USD";
                break;

            default:
                if ($country->ABAIdCurrency == 'EUR') { //Resto de europa EURO
                    $user->countryId = 29;
                } else { //Resto del mundo DOLLAR
                    $user->countryId = 39;
                }
                break;
        }

        $productPrices1 = new CmAbaProductPrice;
        $productPrices2 = new CmAbaProductPrice;
        $productPrices3 = new CmAbaProductPrice;

        $productPrices1->getProductByIdCountryandPeriod($user->countryId,30);
        $productPrices2->getProductByIdCountryandPeriod($user->countryId,180);
        $productPrices3->getProductByIdCountryandPeriod($user->countryId,360);

        //LOGICA DE DESCUENTO

        $dto30 = 1;
        $dto180 = 1;
        $dto360 = 1;

        if ($modelPromo!=false)
        {
            $modelPromo->idPeriodPay = $modelPromo->idPeriodPay ."";

            switch($modelPromo->idPeriodPay) {
                case "":
                    if($modelPromo->type == "PERCENTAGE") {
                        $dto30 = 1;
                        $dto180 = 1 - ($modelPromo->value / 100);
                        $dto360 = 1 - ($modelPromo->value / 100);
                    }
                    break;
                case "30":
                    if($modelPromo->type == "PERCENTAGE") {
                        $dto30 = 1 - ($modelPromo->value / 100);
                    } else {
                        $productPrices1->priceOfficialCry = $modelPromo->value;
                    }
                    break;
                case "180":
                    if($modelPromo->type == "PERCENTAGE") {
                        $dto180 = 1 - ($modelPromo->value / 100);
                    } else {
                        $productPrices2->priceOfficialCry = $modelPromo->value;
                    }
                    break;
                case "360":
                    if($modelPromo->type == "PERCENTAGE") {
                        $dto360 = 1 - ($modelPromo->value / 100);
                    } else {
                        $productPrices3->priceOfficialCry = $modelPromo->value;
                    }
                    break;
                default:
                    break;
            }
        }

        //APLICAMOS CURRENCY
        $productPrices1->priceOfficialCry *=  $enchangeRealtoUSD;
        $productPrices2->priceOfficialCry *=  $enchangeRealtoUSD;
        $productPrices3->priceOfficialCry *=  $enchangeRealtoUSD;

        //Miramos a que tier correspondería el precio original sin dto.
        $tierFromPriceOriginal30 = new CmPaymentsAppStorePricingMatrix;
        $tierFromPriceOriginal180 = new CmPaymentsAppStorePricingMatrix;
        $tierFromPriceOriginal360 = new CmPaymentsAppStorePricingMatrix;

        $tierFromPriceOriginal30->getTierbyCurrency($typeCurrency,$productPrices1->priceOfficialCry);
        $tierFromPriceOriginal180->getTierbyCurrency($typeCurrency,$productPrices2->priceOfficialCry);
        $tierFromPriceOriginal360->getTierbyCurrency($typeCurrency,$productPrices3->priceOfficialCry);

        //Miramos a que tier correspondería el precio original CON DTO.
        $productPrices1->priceOfficialCry *= $dto30;
        $productPrices2->priceOfficialCry *= $dto180;
        $productPrices3->priceOfficialCry *= $dto360;

        $tier30 = new CmPaymentsAppStorePricingMatrix;
        $tier180 = new CmPaymentsAppStorePricingMatrix;
        $tier360 = new CmPaymentsAppStorePricingMatrix;

        $tier30->getTierbyCurrency($typeCurrency,$productPrices1->priceOfficialCry);
        $tier180->getTierbyCurrency($typeCurrency,$productPrices2->priceOfficialCry);
        $tier360->getTierbyCurrency($typeCurrency,$productPrices3->priceOfficialCry);

        //Obtain identifier from price without dto.
        $tierABAFromPriceOriginal30 = new CmPaymentsAppABAPricingMatrix;
        $tierABAFromPriceOriginal180 = new CmPaymentsAppABAPricingMatrix;
        $tierABAFromPriceOriginal360 = new CmPaymentsAppABAPricingMatrix;

        $tierABAFromPriceOriginal30->getTierABA($tierFromPriceOriginal30->Tier, 1);
        $tierABAFromPriceOriginal180->getTierABA($tierFromPriceOriginal180->Tier, 6);
        $tierABAFromPriceOriginal360->getTierABA($tierFromPriceOriginal360->Tier, 12);

        //Obtain identifier from price with dto.
        $tierABA30 = new CmPaymentsAppABAPricingMatrix;
        $tierABA180 = new CmPaymentsAppABAPricingMatrix;
        $tierABA360 = new CmPaymentsAppABAPricingMatrix;

        $tierABA30->getTierABA($tier30->Tier, 1);
        $tierABA30->checkTier($tierABA30, $user, $promocode, $tier30, 1);

        $tierABA180->getTierABA($tier180->Tier, 6);
        $tierABA180->checkTier($tierABA180, $user, $promocode, $tier180, 6);

        $tierABA360->getTierABA($tier360->Tier, 12);
        $tierABA360->checkTier($tierABA360, $user, $promocode, $tier360, 12);

        //Users that need special response for apple testing.
        $specialUsersPrices = array (
          5721038=>array('1M_TIER25_FRANCE_FULL_PRICE','6M_TIER63_FRANCE_FULL_PRICE','12M_TIER71_FRANCE_FULL_PRICE'),
          5721058=>array('1M_TIER26_MEXICO_FULL_PRICE','6M_TIER64_MEXICO_FULL_PRICE','12M_TIER75_MEXICO_FULL_PRICE'),
          5721062=>array('1M_TIER20_SPAIN_FULL_PRICE','6M_TIER58_SPAIN_FULL_PRICE','12M_TIER52_SPAIN_ONE_YEAR_PROMO'),
          5721065=>array('1M_TIER10_USA_FULL_PRICE','6M_TIER40_USA_FULL_PRICE','12M_TIER48_USA_5EURO_PROMO'),
          5721072=>array('1M_TIER30_WORLD_DOLLAR_FULL_PRICE','6M_TIER66_WORLD_DOLLAR_FULL_PRICE','12M_TIER67_MEXICO_1YEAR_PROMO'),
          5721076=>array('1M_TIER25_FRANCE_FULL_PRICE','6M_TIER35_BRAZIL_5PERCENT_PROMO','12M_TIER47_BRAZIL_5PERCENT_PROMO'),
          5721081=>array('1M_TIER25_FRANCE_FULL_PRICE','6M_TIER56_MEXICO_5PERCENT_PROMO','12M_TIER65_MEXICO_5PERCENT_PROMO'),
          5721088=>array('1M_TIER25_FRANCE_FULL_PRICE','6M_TIER37_USA_5PERCENT_PROMO','12M_TIER51_USA_5PERCENT_PROMO'),
          5721093=>array('1M_TIER25_FRANCE_FULL_PRICE','6M_TIER51_BRAZIL_FIRST_PROMO','12M_TIER55_BRAZIL_FIRST_PROMO'),
          5721101=>array('1M_TIER25_FRANCE_FULL_PRICE','6M_TIER55_FRANCE_FIRST_PROMO','12M_TIER61_FRANCE_FIRST_PROMO'),
          5721107=>array('1M_TIER25_FRANCE_FULL_PRICE','6M_TIER60_MEXICO_FIRST_PROMO','12M_TIER70_MEXICO_FIRST_PROMO'),
          5721113=>array('1M_TIER25_FRANCE_FULL_PRICE','6M_TIER52_WORLD_EUR_FIRST_PROMO','12M_TIER59_WORLD_EUR_FIRST_PROMO'),
          5721121=>array('1M_TIER25_FRANCE_FULL_PRICE','6M_TIER45_SPAIN_TUTH_PROMO','12M_TIER54_SPAIN_TUTH_PROMO'),
          5721127=>array('1M_TIER25_FRANCE_FULL_PRICE','6M_TIER50_WORLD_DOLLAR_TUTH_PROMO','12M_TIER56_WORLD_DOLLAR_TUTH_PROMO'),
          5721151=>array('1M_TIER25_FRANCE_FULL_PRICE','6M_TIER43_WORLD_EUR_TUTH_PROMO','12M_TIER53_WORLD_EUR_TUTH_PROMO'),
          5721155=>array('1M_TIER25_FRANCE_FULL_PRICE','6M_TIER63_FRANCE_FULL_PRICE','12M_TIER68_MEXICO_TUTH_PROMO'),
          5721162=>array('1M_TIER25_FRANCE_FULL_PRICE','6M_TIER40_USA_FULL_PRICE','12M_TIER50_BRAZIL_PPM_PROMO'),
          5721167=>array('1M_TIER25_FRANCE_FULL_PRICE','6M_TIER34_USA_FIRST_PROMO','12M_TIER60_BRASIL_FULL_PRICE'),
          5721171=>array('1M_TIER25_FRANCE_FULL_PRICE','6M_TIER39_BRAZIL_TUTH_PROMO','12M_TIER46_BRAZIL_ONE_YEAR_PROMO'),
          5721173=>array('1M_TIER25_FRANCE_FULL_PRICE','6M_TIER32_USA_5PERCENT_PROMO','12M_TIER66_MEXICO_1YEAR_SPECIAL_PROMO'),
          5721179=>array('1M_TIER25_FRANCE_FULL_PRICE','6M_TIER53_SPAIN_FIRST_PROMO','12M_TIER57_FRANCE_5PERCENT_PROMO'));


        if (array_key_exists($idUser,$specialUsersPrices)){
            $responseSelligent =
              array(
                "text"=>"Special prices for the app!",
                "tiers" =>array(
                  array("originalidentifier" => $specialUsersPrices[$idUser][0],"discountidentifier" => $specialUsersPrices[$idUser][0], "days" => "30"),
                  array("originalidentifier" => $specialUsersPrices[$idUser][1],"discountidentifier" => $specialUsersPrices[$idUser][1], "days" => "180"),
                  array("originalidentifier" => $specialUsersPrices[$idUser][2],"discountidentifier" => $specialUsersPrices[$idUser][2], "days" => "360")
                )
              );

        } else {

            $responseSelligent =
              array(
                "text"=>"Special prices for the app!",
                "tiers" =>array(
                  array("originalidentifier" => $tierABAFromPriceOriginal30->Name,"discountidentifier" => $tierABA30->Name, "days" => "30"),
                  array("originalidentifier" => $tierABAFromPriceOriginal180->Name,"discountidentifier" => $tierABA180->Name, "days" => "180"),
                  array("originalidentifier" => $tierABAFromPriceOriginal360->Name,"discountidentifier" => $tierABA360->Name, "days" => "360")
                 )
              );

        }

        $this->sendResponse(200, $responseSelligent);
    }


    public function actionTier() {
        $jsonArray = json_decode(file_get_contents('php://input'));

        $responseSelligent = array();

        foreach($jsonArray as $field => $value) {

            switch ($field) {

                case "srcHrefSellg":

                    break;

                case "promocode":
                    $responseSelligent[] = array("TIER" => 1);
                    break;
            }

        }
        $this->sendResponse(200, $responseSelligent);
        Yii::app()->end();

    }

} 