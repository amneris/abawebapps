<?php

class RootController extends ApiController
{
    public function filters()
    {
        return array(
          array('application.filters.CORSFilter'),
        );
    }

    public function beforeAction($action)
    {
        $this->disableCWebLogRoute();
        $this->logStart('ENABLE_LOG_COURSE_API');
        return true;
    }

    /**
     * A preflight request is basically an OPTIONS request to ask for permission to use cross-domain features.
     */
    public function actionPreflight()
    {
        $this->sendResponse(200, []);
    }

    /**
     *  Just to obtain the IP from the user.
     *
     * URL: http://<HOSTNAME>/api/users/me/ip
     */
    public function actionMyIP()
    {
        $ipClient = Yii::app()->request->getUserHostAddress();
        $this->sendResponse(200, array('ip' => $ipClient), $this->_format);
    }

}
