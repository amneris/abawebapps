<?php
/**
 * VideosController
 * Author: mgadegaard
 * Date: 28/10/2014
 * Time: 13:28
 * © ABA English 2014
 */

class VideosController extends ApiController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2';

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }



    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index', 'view'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update'),
                'users'=>array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @return bool|void
     */
    public function actionView(){
        $video = new Videos();
        $id = $_GET['viewCriteria'];
        $row = $video->findByPrimaryKey($id);
        // Did we find the requested model? If not produce error response to requester.
        if(!$row) {
            $this->sendResponse(404, 'No Video found with id: '. $id);
        } else {
            $this->sendResponse(200, $row);
        }
    }

    /**
     * Manages all models.
     */
    public function actionAdmin(){
        $model=new Videos('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Videos']))
            $model->attributes=$_GET['Videos'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * Performs the AJAX validation.
     * @param Course $model the model to be validated
     */
    protected function performAjaxValidation($model){
        if(isset($_POST['ajax']) && $_POST['ajax']==='videos-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex(){
        $responseData = array();
        $locale = isset($_GET['locale']) ? $_GET['locale'] : 'en';
        $video = new Videos();
        if (!isset($_GET['indexCriteria1']) || $_GET['indexCriteria1'] == 0) {
            $this->sendResponse(400, 'Missing parameters in request, should be in the form /api/<locale>/videos/<idSection>/0/');
        } else {
            $idSection = $_GET['indexCriteria1'];
            $rows = $video->findVideosBySection($idSection, $locale);
        }
        if(empty($rows)) {
            // No db results inform requester.
            $this->sendResponse(404, 'No Videos with Section id ' . $idSection . ' found in Database');
        } else {
            // Prepare response
            foreach($rows as $row)
                $responseData[] = $row;
            $this->sendResponse(200, $rows);
        }
    }
} 