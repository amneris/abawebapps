<?php

/**
 * SelligentController
 * © ABA English 2015
 */
class GetProductsController extends ApiController
{
    //Exchange in 2015-12-10
    const exMXN2EUR = 0.0570463419127224;
    const exUSA2EUR = 0.94593956;
    const exBRL2EUR = 0.245341769397258;

    const exBRL2USA = 0.25936305;

    public function filters()
    {
        return array(
          'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
          array(
            'allow',
            'actions' => array('view'),
            'users' => array('*'),
          )
        );
    }

    public function actionView()
    {
        $action = isset($_GET['action']) ? $_GET['action'] : null;
        $idUser = isset($_GET['viewCriteria1']) ? $_GET['viewCriteria1'] : null;

        switch ($action) {
            case 'ios':
                $this->ios($idUser);
                break;
            case 'android':
                $this->android($idUser);
                break;
            default:
                break;
        }

        return;
    }

    private function android($idUser)
    {
        $modelPromo = $this->getPromocode($idUser);


        $user = CmAbaUser::model('CmAbaUser')->findByPk($idUser);

        $country = CmAbaCountry::model('CmAbaCountry')->findByPk($user->countryId);
        $typeCurrency = $country->ABAIdCurrency;

        $exchange = 1;

        switch ($country->id) {
            case 199: //Spain
            case 73: //France
                break;
            case 138: //Mexico
                //$currency = CmCurrencies::model('CmCurrencies')->findByPk($country->ABAIdCurrency);
                //$exchange = $currency->outEur;
                $exchange = $this::exMXN2EUR;
                break;
            case 226: //USA
                //$currency = CmCurrencies::model('CmCurrencies')->findByPk($country->ABAIdCurrency);
                //$exchange = $currency->outEur;
                $exchange = $this::exUSA2EUR;
                break;
                //EUR=DOLLAR
                break;
            case 30: //Brazil
                //$currency = CmCurrencies::model('CmCurrencies')->findByPk($country->ABAIdCurrency);
                //$exchange = $currency->outEur;
                $exchange = $this::exBRL2EUR;
                break;

            default:
                if ($country->ABAIdCurrency == 'EUR') { //Resto de europa EURO
                    $user->countryId = 29;
                } else { //Resto del mundo DOLLAR
                    $exchange = $this::exUSA2EUR;
                    $user->countryId = 39;
                }
                break;
        }

        $productPrices1 = new CmAbaProductPrice;
        $productPrices2 = new CmAbaProductPrice;
        $productPrices3 = new CmAbaProductPrice;

        $productPrices1->getProductByIdCountryandPeriod($user->countryId, 30);
        $productPrices2->getProductByIdCountryandPeriod($user->countryId, 180);
        $productPrices3->getProductByIdCountryandPeriod($user->countryId, 360);

        //LOGICA DE DESCUENTO

        $dto30 = 1;
        $dto180 = 1;
        $dto360 = 1;

        $final30 = 0;
        $final180 = 0;
        $final360 = 0;

        if ($modelPromo != false) {
            $modelPromo->idPeriodPay = $modelPromo->idPeriodPay . "";

            switch ($modelPromo->idPeriodPay) {
                case "":
                    if ($modelPromo->type == "PERCENTAGE") {
                        $dto30 = 1;  //the discount never apply to 30 days
                        $dto180 = 1 - ($modelPromo->value / 100);
                        $dto360 = 1 - ($modelPromo->value / 100);
                    }
                    break;
                case "30":
                    if ($modelPromo->type == "PERCENTAGE") {
                        $dto30 = 1 - ($modelPromo->value / 100);
                    } else {
                        $final30 = $modelPromo->value;
                    }
                    break;
                case "180":
                    if ($modelPromo->type == "PERCENTAGE") {
                        $dto180 = 1 - ($modelPromo->value / 100);
                    } else {
                        $final180 = $modelPromo->value;
                    }
                    break;
                case "360":
                    if ($modelPromo->type == "PERCENTAGE") {
                        $dto360 = 1 - ($modelPromo->value / 100);
                    } else {
                        $final360 = $modelPromo->value;
                    }
                    break;
                default:
                    break;
            }
        }

        //APLICAMOS CURRENCY
        $productPrices1->priceOfficialCry *= $exchange;
        $productPrices2->priceOfficialCry *= $exchange;
        $productPrices3->priceOfficialCry *= $exchange;

        //Miramos a que tier correspondería el precio original sin dto.

        $priceOriginal30 = new CmPaymentsAppABAPricingMatrixAndroid();
        $priceOriginal180 = new CmPaymentsAppABAPricingMatrixAndroid();
        $priceOriginal360 = new CmPaymentsAppABAPricingMatrixAndroid();

        $priceOriginal30->getPrice(1, $productPrices1->priceOfficialCry);
        $priceOriginal180->getPrice(6, $productPrices2->priceOfficialCry);
        $priceOriginal360->getPrice(12, $productPrices3->priceOfficialCry);

        //Miramos a que tier correspondería el precio original CON DTO.
        if ($final30 > 0) {
            $productPrices1->priceOfficialCry = $final30;
        } elseif ($final180 > 0) {
            $productPrices2->priceOfficialCry = $final180;
        } elseif ($final360 > 0) {
            $productPrices3->priceOfficialCry = $final360;
        } else {
            $productPrices1->priceOfficialCry *= $dto30;
            $productPrices2->priceOfficialCry *= $dto180;
            $productPrices3->priceOfficialCry *= $dto360;
        }

        $priceWithDto30 = new CmPaymentsAppABAPricingMatrixAndroid;
        $priceWithDto180 = new CmPaymentsAppABAPricingMatrixAndroid;
        $priceWithDto360 = new CmPaymentsAppABAPricingMatrixAndroid;

        $priceWithDto30->getPrice(1, $productPrices1->priceOfficialCry);
        $priceWithDto180->getPrice(6, $productPrices2->priceOfficialCry);
        $priceWithDto360->getPrice(12, $productPrices3->priceOfficialCry);

        $is2x1Default = "0";
        $is2x1 = (!isset($modelPromo->idDescription)) ? $is2x1Default : $modelPromo->idDescription;

        $pricesJson30 = array(
          "originalidentifier" => $priceOriginal30->Name,
          "discountidentifier" => $priceWithDto30->Name,
          "days" => "30",
          "promocode" => "",
          "is2x1" => $is2x1Default
        );
        $pricesJson180 = array(
          "originalidentifier" => $priceOriginal180->Name,
          "discountidentifier" => $priceWithDto180->Name,
          "days" => "180",
          "promocode" => "",
          "is2x1" => $is2x1Default
        );
        $pricesJson360 = array(
          "originalidentifier" => $priceOriginal360->Name,
          "discountidentifier" => $priceWithDto360->Name,
          "days" => "360",
          "promocode" => "",
          "is2x1" => $is2x1Default
        );

        if (($final30 + $final180 + $final360) > 0) { //We have a finalprice

            if ($final30 > 0) {
                $pricesJson30['promocode'] = $modelPromo->idPromoCode;
                $pricesJson30['is2x1'] = $is2x1;
            } elseif ($final180 > 0) {
                $pricesJson180['promocode'] = $modelPromo->idPromoCode;
                $pricesJson180['is2x1'] = $is2x1;
            } else {   //($final360>0)
                $pricesJson360['promocode'] = $modelPromo->idPromoCode;
                $pricesJson360['is2x1'] = $is2x1;
            }

            $responseSelligent =
              array(
                "text" => "Special prices for the app!",
                "tiers" => array($pricesJson30, $pricesJson180, $pricesJson360)
              );

        } else {

            if ($dto30 <> 1) {
                $pricesJson30['promocode'] = $modelPromo->idPromoCode;
                $pricesJson30['is2x1'] = $is2x1;
            }
            if ($dto180 <> 1) {
                $pricesJson180['promocode'] = $modelPromo->idPromoCode;
                $pricesJson180['is2x1'] = $is2x1;
            }
            if ($dto360 <> 1) {
                $pricesJson360['promocode'] = $modelPromo->idPromoCode;
                $pricesJson360['is2x1'] = $is2x1;
            }

            $responseSelligent =
              array(
                "text" => "Special prices for the app!",
                "tiers" => array($pricesJson30, $pricesJson180, $pricesJson360)
              );

        }

        $this->sendResponse(200, $responseSelligent);

    }

    private function ios($idUser)
    {

        $modelPromo = $this->getPromocode($idUser);

        //USUARIO

        $user = CmAbaUser::model('CmAbaUser')->findByPk($idUser);

        $country = CmAbaCountry::model('CmAbaCountry')->findByPk($user->countryId);
        $typeCurrency = $country->ABAIdCurrency;

        $enchangeRealtoUSD = 1;

        switch ($country->id) {
            case 199: //Spain
            case 73: //France
            case 138: //Mexico
            case 226: //USA
                break;
            case 30: //Brazil
                //$currency = CmCurrencies::model('CmCurrencies')->findByPk($country->ABAIdCurrency);
                //$enchangeRealtoUSD = $currency->outUsd;
                $enchangeRealtoUSD = $this::exBRL2USA;
                $typeCurrency = "USD";
                break;

            default:
                if ($country->ABAIdCurrency == 'EUR') { //Resto de europa EURO
                    $user->countryId = 29;
                } else { //Resto del mundo DOLLAR
                    $user->countryId = 39;
                }
                break;
        }

        $productPrices1 = new CmAbaProductPrice;
        $productPrices2 = new CmAbaProductPrice;
        $productPrices3 = new CmAbaProductPrice;

        $productPrices1->getProductByIdCountryandPeriod($user->countryId, 30);
        $productPrices2->getProductByIdCountryandPeriod($user->countryId, 180);
        $productPrices3->getProductByIdCountryandPeriod($user->countryId, 360);

        //LOGICA DE DESCUENTO

        $dto30 = 1;
        $dto180 = 1;
        $dto360 = 1;
        $final30 = 0;
        $final180 = 0;
        $final360 = 0;

        if ($modelPromo != false) {
            $modelPromo->idPeriodPay = $modelPromo->idPeriodPay . "";

            switch ($modelPromo->idPeriodPay) {
                case "":
                    if ($modelPromo->type == "PERCENTAGE") {
                        $dto30 = 1;
                        $dto180 = 1 - ($modelPromo->value / 100);
                        $dto360 = 1 - ($modelPromo->value / 100);
                    }
                    break;
                case "30":
                    if ($modelPromo->type == "PERCENTAGE") {
                        $dto30 = 1 - ($modelPromo->value / 100);
                    } else {
                        $final30 = $modelPromo->value;
                    }
                    break;
                case "180":
                    if ($modelPromo->type == "PERCENTAGE") {
                        $dto180 = 1 - ($modelPromo->value / 100);
                    } else {
                        $final180 = $modelPromo->value;
                    }
                    break;
                case "360":
                    if ($modelPromo->type == "PERCENTAGE") {
                        $dto360 = 1 - ($modelPromo->value / 100);
                    } else {
                        $final360 = $modelPromo->value;
                    }
                    break;
                default:
                    break;
            }
        }

        //APLICAMOS CURRENCY
        $productPrices1->priceOfficialCry *= $enchangeRealtoUSD;
        $productPrices2->priceOfficialCry *= $enchangeRealtoUSD;
        $productPrices3->priceOfficialCry *= $enchangeRealtoUSD;

        //Miramos a que tier correspondería el precio original sin dto.
        $tierFromPriceOriginal30 = new CmPaymentsAppStorePricingMatrix;
        $tierFromPriceOriginal180 = new CmPaymentsAppStorePricingMatrix;
        $tierFromPriceOriginal360 = new CmPaymentsAppStorePricingMatrix;

        $tierFromPriceOriginal30->getTierbyCurrency($typeCurrency, $productPrices1->priceOfficialCry);
        $tierFromPriceOriginal180->getTierbyCurrency($typeCurrency, $productPrices2->priceOfficialCry);
        $tierFromPriceOriginal360->getTierbyCurrency($typeCurrency, $productPrices3->priceOfficialCry);

        //Miramos a que tier correspondería el precio original CON DTO.
        if ($final30 > 0) {
            $productPrices1->priceOfficialCry = $final30;
        } elseif ($final180 > 0) {
            $productPrices2->priceOfficialCry = $final180;
        } elseif ($final360 > 0) {
            $productPrices3->priceOfficialCry = $final360;
        } else {
            $productPrices1->priceOfficialCry *= $dto30;
            $productPrices2->priceOfficialCry *= $dto180;
            $productPrices3->priceOfficialCry *= $dto360;
        }


        $tier30 = new CmPaymentsAppStorePricingMatrix;
        $tier180 = new CmPaymentsAppStorePricingMatrix;
        $tier360 = new CmPaymentsAppStorePricingMatrix;

        $tier30->getTierbyCurrency($typeCurrency, $productPrices1->priceOfficialCry);
        $tier180->getTierbyCurrency($typeCurrency, $productPrices2->priceOfficialCry);
        $tier360->getTierbyCurrency($typeCurrency, $productPrices3->priceOfficialCry);

        //Obtain identifier from price without dto.
        $tierABAFromPriceOriginal30 = new CmPaymentsAppABAPricingMatrix;
        $tierABAFromPriceOriginal180 = new CmPaymentsAppABAPricingMatrix;
        $tierABAFromPriceOriginal360 = new CmPaymentsAppABAPricingMatrix;

        $tierABAFromPriceOriginal30->getTierABA($tierFromPriceOriginal30->Tier, 1);
        $tierABAFromPriceOriginal180->getTierABA($tierFromPriceOriginal180->Tier, 6);
        $tierABAFromPriceOriginal360->getTierABA($tierFromPriceOriginal360->Tier, 12);

        //Obtain identifier from price with dto.
        $tierABA30 = new CmPaymentsAppABAPricingMatrix;
        $tierABA180 = new CmPaymentsAppABAPricingMatrix;
        $tierABA360 = new CmPaymentsAppABAPricingMatrix;

        $tierABA30->getTierABA($tier30->Tier, 1);
        $tierABA180->getTierABA($tier180->Tier, 6);
        $tierABA360->getTierABA($tier360->Tier, 12);

        $is2x1Default = "0";
        $is2x1 = (!isset($modelPromo->idDescription)) ? $is2x1Default : $modelPromo->idDescription;

        $pricesJson30 = array(
          "originalidentifier" => $tierABAFromPriceOriginal30->Name,
          "discountidentifier" => $tierABA30->Name,
          "days" => "30",
          "promocode" => "",
          "is2x1" => $is2x1Default
        );
        $pricesJson180 = array(
          "originalidentifier" => $tierABAFromPriceOriginal180->Name,
          "discountidentifier" => $tierABA180->Name,
          "days" => "180",
          "promocode" => "",
          "is2x1" => $is2x1Default
        );
        $pricesJson360 = array(
          "originalidentifier" => $tierABAFromPriceOriginal360->Name,
          "discountidentifier" => $tierABA360->Name,
          "days" => "360",
          "promocode" => "",
          "is2x1" => $is2x1Default
        );

        $specialUsersPrices = array(
          5721162 => array('1M_TIER10_USA_FULL_PRICE', '6M_TIER34_USA_FIRST_PROMO', '12M_TIER50_2_BRAZIL_PPM_PROMO')
        );

        if (array_key_exists($idUser, $specialUsersPrices)) {
            $responseSelligent =
              array(
                "text" => "Special prices for the app!",
                "tiers" => array(
                  array(
                    "originalidentifier" => $specialUsersPrices[$idUser][0],
                    "discountidentifier" => $specialUsersPrices[$idUser][0],
                    "days" => "30",
                    "promocode" => "",
                    "is2x1" => $is2x1Default
                  ),
                  array(
                    "originalidentifier" => $specialUsersPrices[$idUser][1],
                    "discountidentifier" => $specialUsersPrices[$idUser][1],
                    "days" => "180",
                    "promocode" => "",
                    "is2x1" => $is2x1Default
                  ),
                  array(
                    "originalidentifier" => $specialUsersPrices[$idUser][2],
                    "discountidentifier" => $specialUsersPrices[$idUser][2],
                    "days" => "360",
                    "promocode" => "",
                    "is2x1" => $is2x1Default
                  )
                )
              );

        } else {


            if (($final30 + $final180 + $final360) > 0) { //We have a finalprice

                if ($final30 > 0) {
                    $pricesJson30['promocode'] = $modelPromo->idPromoCode;
                    $pricesJson30['is2x1'] = $is2x1;
                } elseif ($final180 > 0) {
                    $pricesJson180['promocode'] = $modelPromo->idPromoCode;
                    $pricesJson180['is2x1'] = $is2x1;
                } else {   //($final360>0)
                    $pricesJson360['promocode'] = $modelPromo->idPromoCode;
                    $pricesJson360['is2x1'] = $is2x1;
                }

                $responseSelligent =
                  array(
                    "text" => "Special prices for the app!",
                    "tiers" => array($pricesJson30, $pricesJson180, $pricesJson360)
                  );

            } else {

                if ($dto30 <> 1) {
                    $pricesJson30['promocode'] = $modelPromo->idPromoCode;
                    $pricesJson30['is2x1'] = $is2x1;
                }
                if ($dto180 <> 1) {
                    $pricesJson180['promocode'] = $modelPromo->idPromoCode;
                    $pricesJson180['is2x1'] = $is2x1;
                }
                if ($dto360 <> 1) {
                    $pricesJson360['promocode'] = $modelPromo->idPromoCode;
                    $pricesJson360['is2x1'] = $is2x1;
                }

                $responseSelligent =
                  array(
                    "text" => "Special prices for the app!",
                    "tiers" => array($pricesJson30, $pricesJson180, $pricesJson360)
                  );
            }
        }
        $this->sendResponse(200, $responseSelligent);

    }

    public function getPromocode($idUser)
    {
        //SELLIGENT

        $endpointSelligent = Yii::app()->config->get("URL_PROMO_DYNAMIC") . $idUser;

        try {
            $urlPromocodeSelligent = file_get_contents($endpointSelligent);

            $posPromo = strpos($urlPromocodeSelligent, 'promocode=');
            if ($posPromo) {
                $posPromo += 10;
                $posFinalBody = strpos($urlPromocodeSelligent, '</body>');

                $final = $posFinalBody - $posPromo;

                $promocode = substr($urlPromocodeSelligent, $posPromo, $final);
            } else {
                $promocode = 'free_generico';
            }
        } catch (Exception $problem) {

            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_LOGIC_L . ', looking for a promocode.',
              HeLogger::IT_BUGS,
              HeLogger::CRITICAL,
              "La llamada al endpoint de Selligent : " . $endpointSelligent . " con el usuario: " . $idUser . " No devuelve ningun promocode extraible. Resultado: " . $urlPromocodeSelligent
            );

            $promocode = 'free_generico';
        }
        //       $promocode="1FR55";  //Code for test 'FINALPRICE

        $moPromo = new ProductPromo();

        $modelPromo = $moPromo->getProdPromoSelligentById($promocode);

        return $modelPromo;


    }

} 