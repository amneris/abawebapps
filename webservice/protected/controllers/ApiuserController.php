<?php

/**
 * ApiuserController
 * @author: mgadegaard
 * Date: 06/07/2015
 * Time: 12:49
 * © ABA English 2015
 */
class ApiuserController extends ApiController
{
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function beforeAction($action)
    {
        $this->disableCWebLogRoute();
//        $this->logStart('ENABLE_LOG_COURSE_API');
        $requestMethod = strtolower($_SERVER['REQUEST_METHOD']);
        $data = array();
        switch (strtoupper($requestMethod)) {
            case 'GET':
                $data = $this->actionParams;
                break;
            case 'POST':
                $data = HeMixed::getPostArgs();
                break;
        }
        $this->reqDataDecoded = $data;
        $this->reqMethod = strtoupper($requestMethod);
        return true;
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'freetopremium', 'recoverpassword', 'login', 'updateuserpassword',
                    'getcurrencybyuserid', 'getcountrybyuserid', 'existuserinzuora', 'insertuserzuora',
                    'cancelsubscriptionszuora', 'renewsubscriptionszuora', 'refundpaymentszuora', 'updateinvoicezuora',
                    'renewpaymentszuora', 'getpaymentsbysubscriptionid', 'getuser', 'getpaymentsbyaccountid', 'getcurrency',
                    'usertopremium', 'insertuserdata', 'updaterenewalscancels'
                ),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Handles execution of petitioned command
     *
     * @return bool
     */
    public function actionView()
    {
        if (isset($_GET['action'])) {
            switch (strtoupper($_GET['action'])) {
                case 'LOGIN':
                    $this->actionLogin();
                    return true;
                    break;
                case 'UPDATEATTRIBUTION':
                    $this->updateAttribution();
                    return true;
                    break;
                case 'UPDATEUSERLEVEL':
                    $this->updateUserlevel();
                    return true;
                    break;
                case 'LISTUSERPROFILES':
                    $this->listUserprofiles();
                    return true;
                    break;
                case 'GETCURRENCYBYUSERID':
                    $this->getCurrencyByUserID();
                    return true;
                    break;
                case 'GETCOUNTRYBYUSERID':
                    $this->getCountryByUserID();
                    return true;
                    break;
                case 'EXISTUSERINZUORA':
                    $this->existUserInZuora();
                    return true;
                    break;
                case 'GETPAYMENTSBYSUBSCRIPTIONID':
                    $this->actionGetPaymentsBySubscriptionId();
                    return true;
                    break;
                case 'GETPAYMENTSBYACCOUNTID':
                    $this->actionGetPaymentsByAccountId();
                    return true;
                    break;
                case 'GETUSER':
                    $this->actionGetUser();
                    return true;
                    break;
                case 'GETCURRENCY':
                    $this->getCurrency();
                    return true;
                    break;
                default:
                    break;
            }

        }
        $this->sendResponse(400, 'Missing or invalid action parameter', $this->_format);

        return false;
    }

    /**
     * Checks login of user, and also will generate the Token for temporary use in the mobile device. Also used to
     * refresh sensitive info of the mobile user. The call to this web service is accepted with or without parameters
     * by POST. If no params are received, then a TOKEN by HTTP HEADER will be expected.
     * Parameters expected by POST are:
     * string $signature
     * string $deviceId
     * string $sysOper
     * string $deviceName
     * string $email
     * string $password
     *
     * @return bool
     */
    public function actionLogin()
    {
        try {
            $loginDevice = new CmAbaUserDevice();

            $loginUser = new CmAbaUser();
            //1: Check to see if login is attempted by token
            $token = HeUtils::getSuperGlobal('SERVER', 'ABA_API_AUTH_TOKEN');
            if ($token) {
                $this->user = $this->authenticateUserByToken($token);
                if (isset($this->device)) {
                    $token = $this->device->token;
                }

            }
            //2: Check if login is attempted by REST pattern
            if (!$this->user) {
                $keyExternalLogin = HeUtils::getSuperGlobal('GET', 'viewCriteria2');
                if ($keyExternalLogin) {
                    $this->user = $this->authenticateUserByToken($keyExternalLogin);
                }
            }
            //3: Check if login is attempted by user/pass in header
            if (!$this->user) {
                $userEmail = HeUtils::getSuperGlobal('SERVER', 'PHP_AUTH_USER');
                $password = HeUtils::getSuperGlobal('SERVER', 'PHP_AUTH_PW');
                if ($userEmail && $password) {
                    if ($loginUser->populateUserByEmail($userEmail, $password, true, false)) {
                        $this->user = $loginUser;
                        if ($loginDevice->getDeviceByUserId($loginUser->id)) {
                            $this->device = $loginDevice;
                            $token = $this->device->token;
                        } else {
                            $token = 'n/a';
                        }
                    } else {
                        $this->sendResponse(
                            401,
                            array('status' => 'Error', 'message' => 'Wrong/Unknown email or password')
                        );
                    }
                }
            }
            //4: Check received GET Request parameters to see if that is the login method
            if (!$this->user) {
                $validArray = array_intersect_key(array(
                    'signature' => 1,
                    'deviceId' => 2,
                    'sysOper' => 3,
                    'deviceName' => 4,
                    'email' => 5,
                    'password' => 6
                ), $this->reqDataDecoded);
                if (is_array($this->reqDataDecoded) && (sizeof($validArray) >= 6 && sizeof($validArray) <= 7)) {
                    $signature = $this->reqDataDecoded["signature"];
                    $deviceId = $this->reqDataDecoded["deviceId"];
                    $sysOper = $this->reqDataDecoded["sysOper"];
                    $deviceName = $this->reqDataDecoded["deviceName"];
                    $email = $this->reqDataDecoded["email"];
                    $password = $this->reqDataDecoded["password"];
                    $appVersion = null;
                    if (array_key_exists("appVersion", $this->reqDataDecoded)) {
                        $appVersion = $this->reqDataDecoded["appVersion"];
                    }
                    if (!$this->isValidSignature(
                        $signature,
                        array($deviceId, $sysOper, $deviceName, $email, $password, $appVersion)
                    )
                    ) {
                        $this->sendResponse(
                            405,
                            array('status' => 'Error', 'message' => 'Signature does not match parameters')
                        );
                    } else {
                        if ($loginUser->populateUserByEmail($email, $password, true, false)) {
                            $this->user = $loginUser;
                            if (!$loginDevice->getDeviceByIdUserId($deviceId, $loginUser->id)) {
                                $userRules = new CmAbaRegisterUserRules();
                                $token = $userRules->registerNewDevice(
                                    $deviceId,
                                    $sysOper,
                                    $deviceName,
                                    $loginUser,
                                    $appVersion
                                );
                                if (!$token) {
                                    $this->sendResponse(500, array(
                                        'status' => 'Error',
                                        'message' => 'Your user was created, but your device could not be registered.'
                                    ));
                                }
                            } else {
                                $token = $loginDevice->token;
                            }
                        } else {
                            $this->sendResponse(
                                401,
                                array('status' => 'Error', 'message' => 'Wrong/Unknown email or password')
                            );
                        }
                    }
                } else {
                    $this->sendResponse(
                        400,
                        array('status' => 'Error', 'message' => 'Erroneous parameters in request')
                    );
                }
            }

            if ($this->user) {
                //5: Gather data for response
                //
                // Return profiles
                $userProfiles = CmUserRules::getOrCreateUserExperimentProfilesWithType(
                    $this->user,
                    CmAbaExperimentsTypes::EXPERIMENT_TYPE_MOBILE_STYLE_CSS,
                    false
                );
                $country = new CmAbaCountry($this->user->countryId);
                $isoCodeCountry = $country->iso;
                $teacher = new CmAbaUserTeacher($this->user->teacherId);
                $teacherMobileImage = '';
                $headerDevice = HeUtils::getSuperGlobal('SERVER', 'DEVICE');
                if ($headerDevice && $headerDevice !== '') {
                    $deviceRule = new DeviceRules();
                    $teacherMobileImage = $deviceRule->getMobileImageByDevice($headerDevice, $teacher);
                }
                $productPriceRules = new CmAbaProductPriceRules();
                $idPeriodPay = $productPriceRules->getUserProductPeriodPay($this->user);
                //6: Return response to requester
                $this->sendResponse(200, array(
                    'userId' => $this->user->id,
                    'userType' => $this->user->userType,
                    'userLevel' => $this->user->currentLevel,
                    'token' => $token ? $token : 'n/a',
                    'name' => $this->user->name,
                    // TODO: HACK to fix Bug#4979, remove when iOS app can handle null values
                    'surnames' => is_null($this->user->surnames) ? '' : $this->user->surnames,
                    'email' => $this->user->email,
                    'countryNameIso' => $isoCodeCountry,
                    'userLang' => $this->user->langEnv,
                    'teacherId' => $this->user->teacherId,
                    'teacherName' => $teacher->name,
                    'teacherImage' => $teacherMobileImage == '' ? $teacher->mobileImage2x : $teacherMobileImage,
                    'expirationDate' => $this->user->expirationDate,
                    'idPeriod' => $idPeriodPay,
                    'idPartner' => $this->user->idPartnerSource,
                    'idSource' => is_null($this->user->idSourceList) ? "0" : $this->user->idSourceList,
                    'gender' => HeUser::getGenderDescription($this->user->gender),
                    'birthDate' => HeDate::europeanToSQL($this->user->birthDate, false, "-"),
                    'phone' => (trim($this->user->telephone) != '' ? $this->user->telephone : ''),
                    'externalKey' => $this->user->keyExternalLogin,
                    'profiles' => $userProfiles,
                    //#ABAWEBAPPS-681
                    'cooladata' => array(
                        array('userScope' => $this->getCooladataEvent('USERSCOPE', array('abaUser' => $this->user)))
                    )
                ), $this->_format);

                return true;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            error_log("Login error=> " . $ex->getMessage());
            $this->sendResponse(500, array(
                'status' => 'Exception',
                'message' => "Internal non identified error. Check with Aba IT Team"
            ));
            return false;
        }
    }

    /**
     * Function updates idPartnerSource if current is of either iOS phone, tablet or Android
     * @return bool
     */
    public function updateAttribution()
    {
        HeMixed::securePublicUrlRequest("api/apiuser/updateattribution");
        $idUser = isset($_GET['user_id']) ? $_GET['user_id'] : null;
        $idPartner = isset($_GET['idPartner']) ? $_GET['idPartner'] : null;
        $idSource = isset($_GET['idSource']) ? $_GET['idSource'] : null;
        $signature = isset($_GET['signature']) ? $_GET['signature'] : null;
        if (is_null($idUser) || intval($idPartner) == 0 || is_null($idPartner) || is_null($signature)) {
            $this->sendResponse(
                400,
                array(
                    'status' => 'Error',
                    'message' => "Param missing or null. Request URI must be on the form:
                    api/apiuser/updateattribution/?idUser=VALUE&idPartner=VALUE&signature=VALUE
                    (With optional &idSource=VALUE)"
                ),
                $this->_format
            );
            return false;
        }
        if ($signature !== Yii::app()->config->get("ADJUST_ATTRIBUTION_SIGNATURE")) {
            $this->sendResponse(405, array('status' => 'Error', 'message' => "Incorrect signature"), $this->_format);
            return false;
        }
        $user = CmAbaUser::model('CmAbaUser')->findByPk($idUser);
        $userRules = new CmUserRules();
        if ($userRules->updateAttribution($user, $idPartner, $idSource)) {
            $commSelligent = new CmSelligentConnector();
            $commSelligent->sendOperationToSelligent(
                CmSelligentConnector::SYNCHRO_USER,
                $user,
                array(),
                "Api User Update attribution"
            );
            $this->sendResponse(200, array('status' => 'OK', "Message" => "Attribution updated"), $this->_format);
            return true;
        } else {
            $this->sendResponse(
                400,
                array('status' => 'Error', "Message" => "Attribution could not be updated"),
                $this->_format
            );
            return false;
        }
    }

    /**
     * Updates user Level in Aba DB and sends update to Selligent
     * URI: api/apiuser/updateuserlevel/<USER_ID>/<NEW_LEVEL>
     *
     * @return bool
     */
    protected function updateUserlevel()
    {
        $userId = HeUtils::getSuperGlobal(HeUtils::GET, 'viewCriteria1');
        $newLevel = HeUtils::getSuperGlobal(HeUtils::GET, 'viewCriteria2');
        if (!$newLevel || !$userId) {
            $this->sendResponse(
                400,
                array('status' => 'Error', 'message' => "Parameter(s) userId/newLevel missing."),
                $this->_format
            );
            return false;
        }
        $token = $this->getToken();
        if ($token) {
            if ($this->validateUser($token['token'], $token['tokenType'], $userId)) {
                $user = new CmAbaUser();
                $user->populateUserById($userId);
                $currentLevel = $user->currentLevel;
                if (intval($currentLevel) !== intval($newLevel)) {
                    if (!$user->updateUserCourseLevel($newLevel)) {
                        $this->sendResponse(
                            500,
                            array('status' => 'Error', 'message' => "Level could not be updated"),
                            $this->_format
                        );
                        return false;
                    }
                } else {
                    $this->sendResponse(
                        400,
                        array('status' => 'Error', 'message' => "Level is the same"),
                        $this->_format
                    );
                    return false;
                }
                $selligentConnector = new CmSelligentConnector();
                $selligentConnector->sendOperationToSelligent(
                    CmSelligentConnector::PERFIL,
                    $user,
                    array(),
                    "actionUpdate"
                );
                $this->sendResponse(200, array("status" => 'OK', "userLevel" => $user->currentLevel), $this->_format);
                return true;
            } else {
                $this->sendResponse(400, 'User id and token does not match');
                return false;
            }
        }
    }

    /**
     * POsT METHOD: Updates user password, send to Selligent, etc.
     *
     * @return bool
     */
    public function actionUpdateuserpassword()
    {
        $token = $this->getToken();
        if ($token) {
            $newPassword = isset($this->reqDataDecoded['password']) ? $this->reqDataDecoded['password'] : '';
            if ($newPassword == '') {
                $newPassword = HeMixed::getGetArgs("password");
                if (empty($newPassword)) {
                    $this->sendResponse(
                        400,
                        array('status' => 'Error', 'message' => "Param password missing or invalid."),
                        $this->_format
                    );
                    return false;
                }
            }
            $user = new CmAbaUser();
            $userDevice = new CmAbaUserDevice();
            if (!$this->device = $userDevice->getDeviceByToken($token['token'])) {
                $this->sendResponse(
                    402,
                    array('status' => 'Error', 'message' => "Token does not exist."),
                    $this->_format
                );
                return false;
            }
            $this->user = $user->populateUserById($userDevice->getUserIdByToken($token['token']));
            $userRules = new CmUserRules();
            if (!$userRules->changePassword($this->user, $newPassword, 'update user password through API', true, false)) {
                $this->sendResponse(
                    500,
                    array('status' => 'Error', 'message' => "Error while updating the password."),
                    $this->_format
                );
                return false;
            }
            $this->device->refresh();
            $this->sendResponse(200, array('status' => 'OK', "token" => $this->device->token), $this->_format);
            return true;
        } else {
            $this->sendResponse(401, 'Missing token');
            return false;
        }
    }

    /** By POST, expect email and language. Changes password and sends email
     * @return bool
     */
    public function actionRecoverpassword()
    {
        Yii::app()->language = 'en';
        // Light security measure to avoid DOS attacks.
        HeMixed::securePublicUrlRequest("apiuser/recoverpassword");
        $signature = '';
        try {
            /*  Validation of parameters received */
            $validArray = array_intersect_key(
                array('signature' => 1, 'email' => 2, 'langEnv' => 3),
                $this->reqDataDecoded
            );
            if (!is_array($this->reqDataDecoded) || sizeof($validArray) !== 3) {
                $this->sendResponse(400, array(
                    'status' => 'Error',
                    'message' => 'Bad params received. Try again with the proper params.
                                                                Bear in mind that parameters are case sensitive.'
                ));

                return false;
            }
            $signature = $this->reqDataDecoded["signature"];
            $email = $this->reqDataDecoded["email"];
            $langEnv = $this->reqDataDecoded["langEnv"];
            if (!$this->isValidSignature($signature, array($email, $langEnv))) {
                $this->sendResponse(405, 'Bad signature');

                return false;
            }
            Yii::app()->language = $langEnv;
            $userRules = new CmUserRules();
            if ($userRules->sendRecoverPasswordEmail($email)) {
                $this->sendResponse(
                    200,
                    array('status' => "success"),
                    $this->_format
                );
                return true;
            } else {
                $this->sendResponse(500, array(
                    'status' => 'Error',
                    'message' => $userRules->getErrMessage()
                ));
                return false;
            }
        } catch (Exception $ex) {
            error_log($signature . " error=> " . $ex->getMessage());
            return $this->sendResponse(
                500,
                array(
                    'status' => 'Exception',
                    'message' => "Internal error non identified at actionRecoverPassword.
                                  Check with Aba IT Team and see logs Apache for " . $signature
                )
            );
        }
    }

    /**
     * POsT METHOD: for calls to apiuser/freetopremium
     *
     * @return bool
     */
    public function actionFreetopremium()
    {

        if (array_key_exists('platform', $this->reqDataDecoded)) {
            switch ($this->reqDataDecoded['platform']) {
                case 'android':
                    $this->androidFreetopremium();
                    break;
                case 'ios':
                    $this->iosFreetopremium();
                    break;
                default:   //Right now this is for to be compatible with the ios app version.
                    $this->iosFreetopremium();
                    break;
            }

        } else {
            $this->iosFreetopremium();
            /*            $this->sendResponse(
                          400,
                          array('status' => 'Error', 'message' => "Param platform missing or invalid."),
                          $this->_format
                        );*/
        }
    }

    /**
     * @return bool
     */
    function androidFreetopremium()
    {

        if (!array_key_exists('countryId', $this->reqDataDecoded) ||
            !array_key_exists('currency', $this->reqDataDecoded) ||
            !array_key_exists('periodId', $this->reqDataDecoded) ||
            !array_key_exists('userId', $this->reqDataDecoded) ||
            !array_key_exists('price', $this->reqDataDecoded) ||
            !array_key_exists('purchaseSignature', $this->reqDataDecoded) ||
            !array_key_exists('purchaseReceipt', $this->reqDataDecoded)
        ) {
            $this->sendResponse(
                400,
                array('status' => 'Error', 'message' => "Some param missing or invalid."),
                $this->_format
            );
            return false;
        }

        $paymentSupplier = new CmPaySupplierPlayStore($this->reqDataDecoded['purchaseReceipt'], $this->reqDataDecoded['purchaseSignature']);

        if ($paymentSupplier->CheckSignature()) {

            $tienda = new HePlayStore();
            $purchase = $tienda->Subscription($paymentSupplier->getReceiptProductId(), $paymentSupplier->getReceiptPurchaseToken());

            //Verify expiryTime from the purchase
            If ($purchase) {
                $date = date("Y-m-d H:i:s", ($tienda->getExpiryTimeMillis() / 1000));
                if ($date < date("Y-m-d H:i:s")) {
                    $purchase = false;
                }
            }

            if ($purchase) {

                $countryId = $this->reqDataDecoded['countryId'];
                $currency = $this->reqDataDecoded['currency'];
                $periodId = $this->reqDataDecoded['periodId'];
                $userId = $this->reqDataDecoded['userId'];
                $price = $this->reqDataDecoded['price'];
                $purchaseReceipt = $this->reqDataDecoded['purchaseReceipt'];
                $token = $this->getToken();

                if (array_key_exists('promocode', $this->reqDataDecoded)) {
                    $promocode = $this->reqDataDecoded['promocode'];
                } else {
                    $promocode = "";
                }

                if ($token) {
                    if (is_numeric($userId) == true) {
                        if ($this->validateUser($token['token'], $token['tokenType'], $userId)) {

                            if (is_numeric($periodId) == false) {
                                $this->sendResponse(
                                    400,
                                    array('status' => 'Error', 'message' => "Param periodId missing or invalid."),
                                    $this->_format
                                );
                                return false;
                            }

                            if (trim($currency) == '') {
                                $this->sendResponse(
                                    400,
                                    array('status' => 'Error', 'message' => "Param currency missing or invalid."),
                                    $this->_format
                                );
                                return false;
                            }
                            if (is_numeric($price) == false) {
                                $this->sendResponse(
                                    400,
                                    array('status' => 'Error', 'message' => "Param price missing or invalid."),
                                    $this->_format
                                );
                                return false;
                            }
                            if (trim($purchaseReceipt) == '') {
                                $this->sendResponse(
                                    400,
                                    array('status' => 'Error', 'message' => "Param purchasereceipt missing or invalid."),
                                    $this->_format
                                );
                                return false;
                            }
                            if (trim($countryId) == '') {
                                $this->sendResponse(
                                    400,
                                    array('status' => 'Error', 'message' => "Param countryId missing or invalid."),
                                    $this->_format
                                );
                                return false;
                            }

                            $user = new CmAbaUser();
                            $this->user = $user->populateUserById($userId);

                            $device = new CmAbaUserDevice();
                            $this->device = $device->getDeviceByUserId($userId);

                            $userRules = new CmUserRules();

                            $result = $userRules->free2premiumAndroid(
                                $paymentSupplier,
                                $this->user,
                                $periodId,
                                $currency,
                                $price,
                                $countryId,
                                $userId,
                                $purchaseReceipt,
                                $tienda->getStartTimeMillis(),
                                $tienda->getExpiryTimeMillis(),
                                $promocode,
                                'Action Rest Web Service updateFreetopremium');

                            if (!$result) {
                                self::sendEmailInternal(
                                    "FREETOPREMIUM",
                                    $this->user->id,
                                    "Error",
                                    "Error while updating user to premium. " . trim($userRules->getErrMessage())
                                );
                                if ($userRules->getErrCode() == '410') {
                                    $this->sendResponse(
                                        410,
                                        array('status' => 'Error', 'message' => $userRules->getErrMessage()),
                                        $this->_format
                                    );
                                } else {
                                    $this->sendResponse(
                                        500,
                                        array('status' => 'Error', 'message' => "Error while updating user to premium."),
                                        $this->_format
                                    );
                                }
                                return false;
                            }

                            $this->device->refresh();
                            $productsPrices = new CmAbaProductPriceRules();
                            $idPeriodPay = $productsPrices->getUserProductPeriodPay($this->user);

                            $oUser = new CmAbaUser();
                            $oUser->populateUserById($this->user->getId());

                            $this->sendResponse(
                                200,
                                array(
                                    'status' => 'OK',
                                    'token' => $this->device->token,
                                    'userType' => $oUser->userType,
                                    'userLevel' => $oUser->currentLevel,
                                    'expirationDate' => $oUser->expirationDate,
                                    'idPeriod' => $idPeriodPay
                                ),
                                $this->_format
                            );
                            return true;


                        } else {
                            $this->sendResponse(401,
                                'Could not authenticate request! User id and token does not match');
                            return false;
                        }
                    } else {
                        $this->sendResponse(401, 'Something is wrong with user');
                        return false;
                    }
                } else {
                    $this->sendResponse(401, 'Missing token');
                    return false;
                }
            }
            {
                $this->sendResponse(401, 'Error getting PlayStore purchase');
                return false;
            }

        } else {

            // We have a hacker pissing me off

            $this->sendResponse(401, 'Something is wrong with purchase');
            return false;
        }
    }

    public function iosFreetopremium()
    {
        $periodId = $this->reqDataDecoded['periodId'];
        $currency = $this->reqDataDecoded['currency'];
        $price = $this->reqDataDecoded['price'];
        $countryId = $this->reqDataDecoded['countryId'];
        $userId = $this->reqDataDecoded['userId'];
        $purchaseReceipt = $this->reqDataDecoded['purchaseReceipt'];
        $token = $this->getToken();
        $idSession = (isset($this->reqDataDecoded['idSession']) ? $this->reqDataDecoded['idSession'] : null);
        if (array_key_exists('promocode', $this->reqDataDecoded)) {
            $promocode = $this->reqDataDecoded['promocode'];
        } else {
            $promocode = "";
        }

        if ($token) {
            if (trim($userId) == '') {
                $userId = HeMixed::getGetArgs("userid");
                if (empty($userId)) {
                    $this->sendEmailInternal("FREETOPREMIUM", $this->moUser->id, "Warning", "Empty param userid.");
                }
            }

            if ($this->validateUser($token['token'], $token['tokenType'], $userId)) {
                if (trim($periodId) == '') {
                    $periodId = HeMixed::getGetArgs("periodid");
                    if (empty($periodId)) {
                        $this->sendEmailInternal(
                            "FREETOPREMIUM",
                            $this->moUser->id,
                            "Error",
                            "Param periodId missing or invalid."
                        );
                        $this->sendResponse(
                            400,
                            array('status' => 'Error', 'message' => "Param periodId missing or invalid."),
                            $this->_format
                        );
                        return false;
                    }
                }
                if (trim($currency) == '') {
                    $currency = HeMixed::getGetArgs("currency");
                    if (empty($currency)) {
                        $this->sendEmailInternal(
                            "FREETOPREMIUM",
                            $this->moUser->id,
                            "Error",
                            "Param currency missing or invalid."
                        );
                        $this->sendResponse(
                            400,
                            array('status' => 'Error', 'message' => "Param currency missing or invalid."),
                            $this->_format
                        );
                        return false;
                    }
                }
                if (trim($price) == '') {
                    $price = HeMixed::getGetArgs("price");
                    if (empty($price)) {
                        $this->sendEmailInternal(
                            "FREETOPREMIUM",
                            $this->moUser->id,
                            "Error",
                            "Param price missing or invalid."
                        );
                        $this->sendResponse(
                            400,
                            array('status' => 'Error', 'message' => "Param price missing or invalid."),
                            $this->_format
                        );
                        return false;
                    }
                }
                if (trim($purchaseReceipt) == '') {
                    $purchaseReceipt = HeMixed::getGetArgs("purchasereceipt");
                    if (empty($purchaseReceipt)) {
                        $this->sendEmailInternal(
                            "FREETOPREMIUM",
                            $this->moUser->id,
                            "Error",
                            "Param purchasereceipt missing or invalid."
                        );
                        $this->sendResponse(
                            400,
                            array('status' => 'Error', 'message' => "Param purchasereceipt missing or invalid."),
                            $this->_format
                        );
                        return false;
                    }
                }
                if (trim($countryId) == '') {
                    $countryId = HeMixed::getGetArgs("countryid");
                    if (empty($countryId)) {
                        $this->sendEmailInternal(
                            "FREETOPREMIUM",
                            $this->user->id,
                            "Warning",
                            "Empty param countryid."
                        );
                    }
                }
                $user = new CmAbaUser();
                $this->user = $user->populateUserById($userId);
                $device = new CmAbaUserDevice();
                $this->device = $device->getDeviceByUserId($userId);
                /** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
                $userRules = new CmUserRules();
                if (!$userRules->free2premium(
                    $this->user,
                    $periodId,
                    $currency,
                    $price,
                    $countryId,
                    $userId,
                    $purchaseReceipt,
                    $idSession,
                    $promocode,
                    'Action Rest Web Service updateFreetopremium'
                )
                ) {
                    self::sendEmailInternal(
                        "FREETOPREMIUM",
                        $this->user->id,
                        "Error",
                        "Error while updating user to premium. " . trim($userRules->getErrMessage())
                    );
                    if ($userRules->getErrCode() == '410') {
                        $this->sendResponse(
                            410,
                            array('status' => 'Error', 'message' => $userRules->getErrMessage()),
                            $this->_format
                        );
                    } else {
                        $this->sendResponse(
                            500,
                            array('status' => 'Error', 'message' => "Error while updating user to premium."),
                            $this->_format
                        );
                    }
                    return false;
                }
                $this->device->refresh();
                $productsPrices = new CmAbaProductPriceRules();
                $idPeriodPay = $productsPrices->getUserProductPeriodPay($this->user);

                $oUser = new CmAbaUser();
                $oUser->populateUserById($this->user->getId());

                $this->sendResponse(
                    200,
                    array(
                        'status' => 'OK',
                        'token' => $this->device->token,
                        'userType' => $oUser->userType,
                        'userLevel' => $oUser->currentLevel,
                        'expirationDate' => $oUser->expirationDate,
                        'idPeriod' => $idPeriodPay),
                    $this->_format
                );
                return true;
            } else {
                $this->sendResponse(401, 'Could not authenticate request! User id and token does not match');
                return false;
            }
        } else {
            $this->sendResponse(401, 'Missing token');
            return false;
        }
    }

    public function getCurrencyByUserID()
    {

        $idUser = isset($_GET['viewCriteria1']) ? $_GET['viewCriteria1'] : null;

        if (is_null($idUser)) {
            $this->sendResponse(404, "Send my userID", 'application/json;charset=UTF-8');
        } else {
            $moUser = new CmAbaUser();
            if (!$moUser->getUserByApiUserId($idUser)) {
                $this->sendResponse(404, "User not found!", 'application/json;charset=UTF-8');
            } else {
                $this->sendResponse(200, $moUser->getCurrencyByUserId($idUser), 'application/json;charset=UTF-8');
            }
        }

    }

    public function getCurrency()
    {

        $iso = isset($_GET['viewCriteria2']) ? strtoupper($_GET['viewCriteria2']) : null;

        if (is_null($iso)) {
            $this->sendResponse(200, array('httpCode' => 400, "message" => "Parameter ISO not found", "currencyISO" => null), 'application/json;charset=UTF-8');
        } else {
            $objCurrency = new CmCurrencies();
            if (!$objCurrency->getCurrencyById($iso)) {
                $this->sendResponse(200, array('httpCode' => 404, "message" => "Currency not found", "currencyISO" => null), 'application/json;charset=UTF-8');
            } else {
                $this->sendResponse(200, array('httpCode' => 200, "message" => "", "currencyISO" => $objCurrency->getCurrencyById($iso)), 'application/json;charset=UTF-8');
            }
        }

    }

    public function getCountryByUserID()
    {

        $idUser = isset($_GET['viewCriteria1']) ? $_GET['viewCriteria1'] : null;

        if (is_null($idUser)) {
            $this->sendResponse(404, "Send my userID", 'application/json;charset=UTF-8');
        } else {
            $moUser = new CmAbaUser();
            if (!$moUser->getUserByApiUserId($idUser)) {
                $this->sendResponse(200, array('iso3' => "ABA", "idCountry" => 301), 'application/json;charset=UTF-8');
            } else {
                $this->sendResponse(200, $moUser->getCountryByUserId($idUser), 'application/json;charset=UTF-8');
            }
        }

    }

    /**
     * GET METHOD: for calls to apiuser/existuserinzuora
     *
     * @return bool
     */
    public function existUserInZuora()
    {

        $idUser = isset($_GET['viewCriteria1']) ? $_GET['viewCriteria1'] : null;

        if (is_null($idUser)) {
            $this->sendResponse(404, "Send my userID", 'application/json;charset=UTF-8');
        } else {
            $userZuora = new CmAbaUserZuora();

            $result = $userZuora->existUserinZuora($idUser);
            if ($result) {

                //#ZOR-291
                $names = HeUser::getDefaultNames(array("name" => $result['name'], "surnames" => $result['surnames']));

                $user = new UserZuora();
                $user->setId($result['id']);
                $user->setName((isset($names["name"]) ? $names["name"] : $result['name']));
                $user->setSurname((isset($names["surnames"]) ? $names["surnames"] : $result['surnames']));
                $user->setCountry($result['country']);
                $user->setCurrency($result['currency']);
                $user->setEmail($result['email']);

                $user->setUserType(CmAbaUserZuora::getUserType($result['userType']));

                if (!is_null($result['zuoraAccountId'])) {
                    $zuora = new Zuora();
                    $zuora->setZuoraAccountId($result['zuoraAccountId']);
                    $zuora->setZuoraAccountNumber($result['zuoraAccountNumber']);
                    $zuora->setDateAdd($result['dateAdd']);
                    $zuora->setStatus($result['status']);

                    $zuora->setZuoraSubscriptionId($result['zuoraSubscriptionId']);
                    $zuora->setZuoraSubscriptionNumber($result['zuoraSubscriptionNumber']);

                    $this->sendResponse(200, array('zuora' => $zuora, 'user' => $user),
                        'application/json;charset=UTF-8');
                } else {
                    $this->sendResponse(200, array('user' => $user), 'application/json;charset=UTF-8');
                }

            } else {
                $this->sendResponse(404, 'User not exist in Zuora', 'application/json;charset=UTF-8');
            }
        }
    }

    /**
     * POST METHOD: for calls to apiuser/insertuserzuora
     *
     * @return bool
     */
    public function actionInsertUserZuora()
    {

        $data = CJSON::decode(Yii::app()->request->rawBody, true);

        $success = false;
        $successPremium = false;
        $successSubscription = false;
        $paymentId = "";
        $partnerId = "";
        $warnings = "";

        if ((array_key_exists('userId', $data))) {

            $userId = $data['userId'];
            $zuoraAccountId = $data['zuoraAccountId'];
            $zuoraAccountNumber = (isset($data['zuoraAccountNumber']) ? $data['zuoraAccountNumber'] : "");
            $zuoraSubscriptionId = (isset($data['subscriptionId']) ? $data['subscriptionId'] : "");
            $zuoraSubscriptionNumber = (isset($data['subscriptionNumber']) ? $data['subscriptionNumber'] : "");

            $invoiceId = (isset($data['invoiceId']) ? $data['invoiceId'] : "");
            $invoiceNumber = (isset($data['invoiceNumber']) ? $data['invoiceNumber'] : "");

            $subscriptionsInfo = (isset($data['subscriptionsList']) ? $data['subscriptionsList'] : "");

            $userZuora = new CmAbaUserZuora();

            $userZuoraId = 0;

            try {
                if ($userZuora->insertUser($userId, $zuoraAccountId, $zuoraAccountNumber, $zuoraSubscriptionId,
                    $zuoraSubscriptionNumber)
                ) {
                    $success = true;
                    $userZuoraId = $userZuora->id;
                }
            } catch (Exception $e) {
            }

            try {
                $periodId = (isset($data['periodId']) ? $data['periodId'] : 1);
                $currency = (isset($data['currency']) ? $data['currency'] : "USD");
                $price = (isset($data['price']) ? $data['price'] : 0);
                $idCountry = (isset($data['idCountry']) ? $data['idCountry'] : "ABA");
                $idDiscount = (isset($data['idDiscount']) ? $data['idDiscount'] : "");
                $idRatePlanCharge = (isset($data['idRatePlanCharge']) ? $data['idRatePlanCharge'] : "");
                $idSession = (isset($data['idSession']) ? $data['idSession'] : "");
                $finalPrice = (isset($data['finalPrice']) ? $data['finalPrice'] : 0);

                $userRules = new CmUserRules();

                $free2premiumZresult = $userRules->free2premiumZ(
                    $userId,
                    $periodId,
                    $currency,
                    $price,
                    $idCountry,
                    $idSession,
                    $idDiscount,
                    $invoiceNumber,
                    $finalPrice,
                    $zuoraSubscriptionNumber,
                    $subscriptionsInfo,
                    true
                );

                if (isset($free2premiumZresult["success"]) AND $free2premiumZresult["success"]) {

                    $successPremium = true;

                    if (isset($free2premiumZresult["paymentId"])) {
                        $paymentId = $free2premiumZresult["paymentId"];
                    }

                    if (isset($free2premiumZresult["partnerId"])) {
                        $partnerId = $free2premiumZresult["partnerId"];
                    }

                    try {
                        $cmAbaUserZuoraInvoices = new CmAbaUserZuoraInvoices();

                        if (!$cmAbaUserZuoraInvoices->getSubscriptionByNumber($userId, $invoiceNumber)) {

                            $successSubscription = $cmAbaUserZuoraInvoices->insertUserZuoraInvoice(
                                $userZuoraId,
                                $paymentId,
                                $userId,
                                $invoiceId,
                                $invoiceNumber
                            );

                        }

                    } catch (Exception $e) {
                    }
                }

                if (isset($free2premiumZresult["warnings"])) {
                    $warnings = $free2premiumZresult["warnings"];
                }

            } catch (Exception $e) {
                $warnings = $e->getMessage();
            }

            $this->sendResponse(200, array(
                'success' => $success,
                'successFreeToPremium' => $successPremium,
                'successSubscription' => $successSubscription,
                'paymentId' => $paymentId,
                'partnerId' => $partnerId,
                'warnings' => $warnings
            ), 'application/json;charset=UTF-8');

        } else {
            $this->sendResponse(404, "User not found!", 'application/json;charset=UTF-8');
        }
    }

    /**
     * #ZOR-267
     *
     * POST METHOD: for calls to apiuser/cancelsubscriptionszuora
     *
     * @return bool
     */
    public function actionCancelSubscriptionsZuora()
    {

        $data = CJSON::decode(Yii::app()->request->rawBody, true);
        $response = array();

        if (is_array($data)) {

            foreach ($data as $key => $cancelledSubscription) {

                if (!isset($cancelledSubscription["originalSubscriptionId"]) || !isset($cancelledSubscription['accountId'])
                    || !isset($cancelledSubscription['cancelledDate'])
                ) {
                    $response[] = array("originalSubscriptionId" => "", "success" => false, "warnings" => 'Incorrect Params');
                    continue;
                }
                $userRules = new CmUserRules();

                $responseCancel = $userRules->cancelSubscriptionsZ($cancelledSubscription);

                $response[] = array("originalSubscriptionId" => $cancelledSubscription["originalSubscriptionId"], "success" => $responseCancel['success'], "warnings" => $responseCancel["warnings"]);

            }

            $this->sendResponse(200, $response, 'application/json;charset=UTF-8');

        } else {
            $this->sendResponse(404, "Malformed subscriptions!", 'application/json;charset=UTF-8');
        }
    }

    /**
     * #ZOR-267
     *
     * POST METHOD: for calls to apiuser/renewsubscriptionszuora
     *
     * @return bool
     */
    public function actionRenewSubscriptionsZuora()
    {

        $data = CJSON::decode(Yii::app()->request->rawBody, true);
        $response = array();

        if (is_array($data)) {

            foreach ($data as $key => $renewedSubscription) {

                if (!isset($renewedSubscription["originalSubscriptionId"]) || !isset($renewedSubscription["userExpirationDate"])) {
                    $response[] = array("originalSubscriptionId" => "", "success" => false, "warnings" => 'Incorrect Params');
                    continue;
                }

                $userRules = new CmUserRules();

                $responseRenew = $userRules->renewSubscriptionsZ($renewedSubscription);

                $response[] = array("originalSubscriptionId" => $renewedSubscription["originalSubscriptionId"], "success" => $responseRenew['success'], "warnings" => $responseRenew["warnings"]);

            }

            $this->sendResponse(200, $response, 'application/json;charset=UTF-8');

        } else {
            $this->sendResponse(404, "Malformed subscriptions!", 'application/json;charset=UTF-8');
        }
    }


    /**
     * #ZOR-558
     *
     * POST METHOD: for calls to apiuser/renewpaymentszuora
     *
     * @return bool
     */
    public function actionRenewPaymentsZuora()
    {

        $data = CJSON::decode(Yii::app()->request->rawBody, true);
        $response = array();

        if (is_array($data)) {

            foreach ($data as $key => $renewedPayment) {
                if (!isset($renewedPayment["originalSubscriptionId"]) || !isset($renewedPayment["paymentDate"]) ||
                    !isset($renewedPayment["nextRenewalDate"]) || !isset($renewedPayment["invoiceId"]) ||
                    !isset($renewedPayment["invoiceNumber"])
                ) {
                    $response[] = array("originalSubscriptionId" => "", "success" => false, "warnings" => 'Incorrect Params');
                    continue;
                }
                $userRules = new CmUserRules();

                $responseRenew = $userRules->renewPaymentsZ($renewedPayment);

                $response[] = array("originalSubscriptionId" => $renewedPayment["originalSubscriptionId"], "success" => $responseRenew['success'], "warnings" => $responseRenew["warnings"]);

            }

            $this->sendResponse(200, $response, 'application/json;charset=UTF-8');

        } else {
            $this->sendResponse(404, "Malformed subscriptions!", 'application/json;charset=UTF-8');
        }
    }

    /**
     * #ZOR-303
     *
     * POST METHOD: for calls to apiuser/refundpaymentszuora
     *
     * @return bool
     */
    public function actionRefundPaymentsZuora()
    {

        $data = CJSON::decode(Yii::app()->request->rawBody, true);
        $response = array();

        if (is_array($data)) {

            foreach ($data as $key => $refundedPayments) {

                if (!isset($refundedPayments["id"]) || !isset($refundedPayments["accountId"]) ||
                    !isset($refundedPayments["amount"]) || !isset($refundedPayments["refundDate"])
                ) {
                    $response[] = array("id" => "", "success" => false, "warnings" => 'Incorrect Params');
                    continue;
                }
                $userRules = new CmUserRules();

                $responseRefund = $userRules->refundPaymentsZ($refundedPayments);

                $response[] = array("originalSubscriptionId" => $refundedPayments["id"], "success" => $responseRefund['success'], "warnings" => $responseRefund['warnings']);

            }

            $this->sendResponse(200, $response, 'application/json;charset=UTF-8');

        } else {
            $this->sendResponse(404, "Malformed subscriptions!", 'application/json;charset=UTF-8');
        }
    }

    /**
     * #ZOR-563
     *
     * GET METHOD: for calls to apiuser/getpaymentsbysubscriptionid
     *
     * @return array(CmAbaPayment)
     */
    public function actionGetPaymentsBySubscriptionId()
    {

        $subscriptionId = isset($_GET['subscriptionId']) ? $_GET['subscriptionId'] : '';

        if (empty($subscriptionId)) {
            $this->sendResponse(404, "Send my subscriptionId", 'application/json;charset=UTF-8');
        } else {
            $cmAbaPayment = new CmAbaPayment();
            $response = $cmAbaPayment->getPaymentsbySubscriptionId($subscriptionId);

            $this->sendResponse(200,
                $response, 'application/json;charset=UTF-8');
        }

    }

    /**
     * #ZOR-563
     *
     * GET METHOD: for calls to apiuser/getpaymentsbyaccountid
     *
     * @return array(CmAbaPayment)
     */
    public function actionGetPaymentsByAccountId()
    {

        $subscriptionId = isset($_GET['accountId']) ? $_GET['accountId'] : '';

        if (empty($subscriptionId)) {
            $this->sendResponse(404, "Send my accountId", 'application/json;charset=UTF-8');
        } else {
            $cmAbaPayment = new CmAbaPayment();
            $response = $cmAbaPayment->getPaymentsbyAccountId($subscriptionId);

            $this->sendResponse(200,
                $response, 'application/json;charset=UTF-8');
        }

    }

    /**
     * #ZOR-563
     *
     * GET METHOD: for calls to apiuser/getuser
     *
     * @return CmAbaUser
     */
    public function actionGetUser()
    {

        $userId = isset($_GET['userId']) ? $_GET['userId'] : '';

        $moUser = new CmAbaUser();
        if (!$moUser->getUserByApiUserId($userId)) {
            $this->sendResponse(404, "User not found!", 'application/json;charset=UTF-8');
        } else {
            $this->sendResponse(200,
                array(
                    'id' => $moUser->id,
                    'name' => $moUser->name,
                    'surnames' => $moUser->surnames,
                    'email' => $moUser->email,
                    'langEnv' => $moUser->langEnv,
                    'userType' => $moUser->userType,
                    'expirationDate' => $moUser->expirationDate,
                ), 'application/json;charset=UTF-8');
        }
    }

    /**
     * Returns list of availables profiles
     *
     * @return bool/mixed returns bool for success/fail and json response to request
     */
    protected function listUserprofiles()
    {
        $experimentTypeIdentifier = (isset($this->reqDataDecoded['type']) ? $this->reqDataDecoded['type'] : null);
        if (trim($experimentTypeIdentifier) == "") {
            $experimentTypeIdentifier = HeMixed::getGetArgs("type");
        }

        $oCmAbaExperimentsTypes = new CmAbaExperimentsTypes();
        $oCmAbaExperimentsTypes->getExperimentTypeByIdentifier($experimentTypeIdentifier);

        $user = new CmAbaUser();
        $this->user = $user->populateUserById($this->getUserIdByToken(HeUtils::getServerValue('ABA_API_AUTH_TOKEN')));
        $stAllProfiles = CmUserRules::getOrCreateUserExperimentProfilesWithType(
            $this->user,
            (is_numeric($oCmAbaExperimentsTypes->experimentTypeId) ? $oCmAbaExperimentsTypes->experimentTypeId : null)
        );

        $this->sendResponse(200, array(
            "profiles" => $stAllProfiles
        ), $this->_format);

        return true;
    }

    /**
     * @param $token
     * @return bool|int $userId
     */
    private function getUserIdByToken($token)
    {
        $loginDevice = new CmAbaUserDevice();
        $userId = $loginDevice->getUserIdByToken($token);
        if (!$userId) {
            $loginUser = new CmAbaUser();
            $userId = $loginUser->getUserIdByKeyExternalLogin($token);
            if ($userId) {
                $newLoginDevice = $loginDevice->getDeviceByUserId($userId);
                if ($newLoginDevice) {
                    $this->device = $loginDevice;
                }
            }
        }
        return $userId;
    }

    /**
     * @param string $token
     *
     * @return bool|\CmAbaUser
     */
    private function authenticateUserByToken($token)
    {
        $loginUser = new CmAbaUser();
        $authenticated = true;
        // We search the device based on the Token, and from that we obtain the user as well.
        $userId = $this->getUserIdByToken($token);
        if (!$userId) {
            $authenticated = false;
            $this->sendResponse(400, array('status' => 'Error', 'message' => 'Unknown token.'));
        }
        if (!$loginUser->populateUserById($userId)) {
            $authenticated = false;
            $this->sendResponse(401, array('status' => 'Error', 'message' => 'User unknown or deleted'));
        }
        if ($authenticated) {
            return $loginUser;
        } else {
            return false;
        }
    }

    private function getToken()
    {
        $deviceToken = HeUtils::getSuperGlobal(HeUtils::SERVER, 'ABA_API_AUTH_TOKEN');
        $keyExternalLogin = HeUtils::getSuperGlobal(HeUtils::GET, 'viewCriteria3');
        if ($deviceToken) {
            $result['token'] = $deviceToken;
            $result['tokenType'] = HeUtils::DEVICE_TOKEN;
        } elseif ($keyExternalLogin) {
            $result['token'] = $keyExternalLogin;
            $result['tokenType'] = HeUtils::KEY_EXTERNAL_LOGIN_TOKEN;
        } else {
            $result = false;
        }
        return $result;
    }

    /**
     * @param $title
     * @param $userId
     * @param $status
     *
     * @param $msg
     */
    private function sendEmailInternal($title, $userId, $status, $msg)
    {
        HeLogger::sendLog($title . ". User " . $userId . ". " . $status . ". ", HeLogger::PAYMENTS_MOBILE, HeLogger::CRITICAL, $msg);
    }

    /**
     * POST METHOD: for calls to apiuser/updateinvoicezuora
     *
     * @return bool
     */
    public function actionUpdateInvoiceZuora()
    {

        $data = CJSON::decode(Yii::app()->request->rawBody, true);

        $success = false;
        $paymentId = "";
        $partnerId = "";
        $warnings = "";

        if ((array_key_exists('invoiceId', $data))) {

            $invoiceId = trim($data['invoiceId']);
            $invoiceNumber = trim($data['invoiceNumber']);
            $totalAmount = $data['totalAmount'];

            try {

                $cmAbaUserZuoraInvoices = new CmAbaUserZuoraInvoices();

                if ($cmAbaUserZuoraInvoices->getSubscriptionByInvoiceId($invoiceId)) {

                    if (trim($cmAbaUserZuoraInvoices->invoiceNumber) == '' AND $totalAmount > 0) {

                        $cmAbaUserZuoraInvoices->invoiceNumber = $invoiceNumber;

                        if ($cmAbaUserZuoraInvoices->updateInvoiceNumber()) {

                            $userZuora = new CmAbaUserZuora();

                            $userZuora->id = $cmAbaUserZuoraInvoices->idUserZuora;
                            $userZuora->userId = $cmAbaUserZuoraInvoices->userId;

                            if ($userZuora->getUserByUserZuoraId()) {

                                $cmAbaPayment = new CmAbaPayment();

                                $cmAbaPayment->paySuppExtProfId = $userZuora->zuoraSubscriptionNumber;
                                $cmAbaPayment->userId = $userZuora->userId;

                                if ($cmAbaPayment->getFirstSuccessPaymentZuoraByPaySuppExtProfId()) {

                                    $paymentId = $cmAbaPayment->id;
                                    $partnerId = $cmAbaPayment->idPartner;

                                    $cmAbaPayment->paySuppExtUniId = $invoiceNumber;
                                    $cmAbaPayment->amountPrice = $totalAmount;
                                    $cmAbaPayment->amountDiscount = 0;

                                    if ($cmAbaPayment->amountOriginal != $cmAbaPayment->amountPrice) {
                                        $cmAbaPayment->amountDiscount = $cmAbaPayment->amountOriginal - $cmAbaPayment->amountPrice;
                                        if ($cmAbaPayment->amountDiscount < 0) {
                                            $cmAbaPayment->amountDiscount = 0;
                                        }
                                    }

                                    $cmAbaPayment->setAmountWithoutTax($cmAbaPayment->dateEndTransaction);

                                    if ($cmAbaPayment->updateInvoiceZuoraPayment()) {

                                        $success = true;

                                        $cmAbaUserZuoraInvoices = new CmAbaUserInvoices();

                                        if ($cmAbaUserZuoraInvoices->getByPaymentId($cmAbaPayment->id)) {

                                            $cmAbaUserZuoraInvoices->amountPrice = $cmAbaPayment->amountPrice;
                                            $cmAbaUserZuoraInvoices->taxRateValue = $cmAbaPayment->taxRateValue;
                                            $cmAbaUserZuoraInvoices->amountTax = $cmAbaPayment->amountTax;
                                            $cmAbaUserZuoraInvoices->amountPriceWithoutTax = $cmAbaPayment->amountPriceWithoutTax;

                                            if (!$cmAbaUserZuoraInvoices->updateZuoraInvoice()) {
                                                $warnings .= "; Invoice could not be updated. PymentId=" . $cmAbaPayment->id . " ";
                                            }
                                        } else {
                                            $warnings .= "; Invoice could not be retrieved. PymentId=" . $cmAbaPayment->id . " ";
                                        }
                                    } else {
                                        $warnings .= "; Could not update payment. PymentId=" . $cmAbaPayment->id . " ";
                                    }

                                } else {
                                    $warnings .= "; Payment does not exist for userId=" . $cmAbaPayment->userId . " ";
                                }

                            }

                        } else {
                            $warnings .= "; Could not update invoice for userId=" . $cmAbaUserZuoraInvoices->userId . " ";
                        }
                    } else {
                        $warnings .= "; Invoice number is not empty OR Amount Total is 0 for userId=" . $cmAbaUserZuoraInvoices->userId . " ";
                    }
                } else {
                    $warnings .= "; There is no user linked to invoice identifier=" . $invoiceId . " ";
                }

            } catch (Exception $e) {
                $warnings .= "; ERROR=" . $e->getMessage() . " ";
            }

            $this->sendResponse(200, array(
                'success' => $success,
                'paymentId' => $paymentId,
                'partnerId' => $partnerId,
                'warnings' => $warnings
            ), 'application/json;charset=UTF-8');

        } else {
            $this->sendResponse(404, "User not found!", 'application/json;charset=UTF-8');
        }
    }

    /**
     * POST METHOD: for calls to /api/apiuser/UpdateRenewalsCancels
     *
     */
    public function actionUpdateRenewalsCancels()
    {

        $data = CJSON::decode(Yii::app()->request->rawBody, true);

        if ((array_key_exists('userId', $data))) {
            if ((array_key_exists('expirationDate', $data))) {


                $userId = $data['userId'];
                $expirationDate = $data['expirationDate'];

                $moUser = new CmAbaUser();

                if (!$moUser->getUserByApiUserId($userId)) {
                    $this->sendResponse(200, array('httpCode' => 404, 'message' => "User does not exist!"),
                        'application/json;charset=UTF-8');
                } else {
                    
                    $userType = $moUser->userType;

                    if ((array_key_exists('userType', $data)) && $data['userType'] != null) {
                        $userType = $data['userType'];
                    }

                    if (!$moUser->updateRenewalsCancels($moUser, $expirationDate, $userType)) {
                        $this->sendResponse(200,
                            array('httpCode' => 404, 'message' => "Error while updating expiration date and userTypegit s !"),
                            'application/json;charset=UTF-8');
                    }else{
                        $this->sendResponse(200, array('httpCode' => 200, "message" => ""), 'application/json;charset=UTF-8');
                    }

                }

            } else {
                $this->sendResponse(200, array('httpCode' => 404, 'message' => "expirationDate not found!"),
                    'application/json;charset=UTF-8');
            }
        } else {
            $this->sendResponse(200, array('httpCode' => 404, 'message' => "userId not found!"),
                'application/json;charset=UTF-8');
        }
    }


    /**
     * POST METHOD: for calls to /api/apiuser/usertopremium
     *
     */
    public function actionUserToPremium()
    {

        $data = CJSON::decode(Yii::app()->request->rawBody, true);

        if ((array_key_exists('userId', $data))) {

            $warnings = '';

            $userId = $data['userId'];
            $expirationDate = (isset($data['expirationDate']) ? $data['expirationDate'] : HeDate::todaySQL());
            $provider = (isset($data['provider']) ? $data['provider'] : "");

            $moUser = new CmAbaUser();

            if (!$moUser->getUserByApiUserId($userId)) {
                $this->sendResponse(200, array('httpCode' => 404, 'message' => "User does not exist!"),
                    'application/json;charset=UTF-8');
            }

            if ($moUser->userType == PREMIUM) {
                $warnings = "User already exists and is premium!";
            }

            switch ($provider) {
                case 'ANDROID':
                    if (!$moUser->updateUserToPremium($expirationDate, $moUser)) {
                        $this->sendResponse(200,
                            array('httpCode' => 404, 'message' => "Error while updating user to premium!"),
                            'application/json;charset=UTF-8');
                    }
                    break;

                default:
                    if (!$moUser->updateUserToPremium($expirationDate, $moUser)) {
                        $this->sendResponse(200,
                            array('httpCode' => 404, 'message' => "Error while updating user to premium!"),
                            'application/json;charset=UTF-8');
                    }
                    break;
            }

            $this->sendResponse(200, array('httpCode' => 200, 'message' => $warnings),
                'application/json;charset=UTF-8');
        }

        $this->sendResponse(200, array('httpCode' => 404, 'message' => "User not found!"),
            'application/json;charset=UTF-8');
    }


    /**
     * POST METHOD: for calls to apiuser/insertuserdata
     *
     * @return bool
     */
    public function actionInsertUserData()
    {

        $data = CJSON::decode(Yii::app()->request->rawBody, true);

        $success = false;
        $successPayment = false;
        $successSubscription = false;
        $paymentId = "";
        $partnerId = "";
        $warnings = "";

        if ((array_key_exists('userId', $data))) {

            $userId = $data['userId'];
            $zuoraAccountId = $data['zuoraAccountId'];
            $zuoraAccountNumber = (isset($data['zuoraAccountNumber']) ? $data['zuoraAccountNumber'] : "");
            $zuoraSubscriptionId = (isset($data['subscriptionId']) ? $data['subscriptionId'] : "");
            $zuoraSubscriptionNumber = (isset($data['subscriptionNumber']) ? $data['subscriptionNumber'] : "");

            $invoiceId = (isset($data['invoiceId']) ? $data['invoiceId'] : "");
            $invoiceNumber = (isset($data['invoiceNumber']) ? $data['invoiceNumber'] : "");

            $subscriptionsInfo = (isset($data['subscriptionsList']) ? $data['subscriptionsList'] : "");

            $userZuora = new CmAbaUserZuora();

            $userZuoraId = 0;

            try {
                if ($userZuora->insertUser($userId, $zuoraAccountId, $zuoraAccountNumber, $zuoraSubscriptionId,
                    $zuoraSubscriptionNumber)
                ) {
                    $success = true;
                    $userZuoraId = $userZuora->id;
                }
            } catch (Exception $e) {
            }

            try {
                $periodId = (isset($data['periodId']) ? $data['periodId'] : 1);
                $currency = (isset($data['currency']) ? $data['currency'] : "USD");
                $price = (isset($data['price']) ? $data['price'] : 0);
                $idCountry = (isset($data['idCountry']) ? $data['idCountry'] : "ABA");
                $idDiscount = (isset($data['idDiscount']) ? $data['idDiscount'] : "");
                $idSession = (isset($data['idSession']) ? $data['idSession'] : "");
                $finalPrice = (isset($data['finalPrice']) ? $data['finalPrice'] : 0);

                $userRules = new CmUserRules();

                $free2premiumZresult = $userRules->free2premiumZ(
                    $userId,
                    $periodId,
                    $currency,
                    $price,
                    $idCountry,
                    $idSession,
                    $idDiscount,
                    $invoiceNumber,
                    $finalPrice,
                    $zuoraSubscriptionNumber,
                    $subscriptionsInfo,
                    false
                );

                if (isset($free2premiumZresult["success"]) AND $free2premiumZresult["success"]) {

                    $successPayment = true;

                    if (isset($free2premiumZresult["paymentId"])) {
                        $paymentId = $free2premiumZresult["paymentId"];
                    }

                    if (isset($free2premiumZresult["partnerId"])) {
                        $partnerId = $free2premiumZresult["partnerId"];
                    }

                    try {
                        $cmAbaUserZuoraInvoices = new CmAbaUserZuoraInvoices();

                        if (!$cmAbaUserZuoraInvoices->getSubscriptionByNumber($userId, $invoiceNumber)) {

                            $successSubscription = $cmAbaUserZuoraInvoices->insertUserZuoraInvoice(
                                $userZuoraId,
                                $paymentId,
                                $userId,
                                $invoiceId,
                                $invoiceNumber
                            );

                        }

                    } catch (Exception $e) {
                    }
                }

                if (isset($free2premiumZresult["warnings"])) {
                    $warnings = $free2premiumZresult["warnings"];
                }

            } catch (Exception $e) {
                $warnings = $e->getMessage();
            }

            $this->sendResponse(200, array(
                'httpCode' => ($success ? 200 : 404),
                'httpCodePayment' => ($successPayment ? 200 : 404),
                'httpCodeSubscription' => ($successSubscription ? 200 : 404),
                'paymentId' => $paymentId,
                'partnerId' => $partnerId,
                'message' => $warnings
            ), 'application/json;charset=UTF-8');

        } else {
            $this->sendResponse(200, array('httpCode' => 404, 'message' => "User not found!"),
                'application/json;charset=UTF-8');
        }

    }

}
