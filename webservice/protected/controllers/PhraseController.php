<?php

class PhraseController extends ApiController {
    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'login'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     *
     * Returns JSON representation of Phrase DB row with given id
     *
     * API call: api/phrase/<PHRASE_ID>
     */
    public function actionView() {
        // parent::authenticate();
        $id = $_GET['viewCriteria'];
        $model = $this->loadModel($id);
        // Did we find the requested model? If not, return an error message
        if(is_null($model)) {
            $this->sendResponse(404, 'No Phrase found with id ' . $_GET['id']);
        } else {
            $row = $model->attributes;
            $this->sendResponse(200, $row);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Phrase;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if(isset($_POST['Phrase'])) {
            $model->attributes = $_POST['Phrase'];
            if($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate() {
        $id = $_GET['id'];
        $model = $this->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if(isset($_POST['Phrase'])) {
            $model->attributes = $_POST['Phrase'];
            if($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @throws CDbException
     * @throws CHttpException
     * @internal param int $id the ID of the model to be deleted
     */
    public function actionDelete() {
        /*		if(Yii::app()->request->isPostRequest)
                {
                    // we only allow deletion via POST request
                    $this->loadModel($id)->delete();

                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
                }
                else
                    throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        */
    }

    /**
     * Lists relevant columns of Phrases and Phrase_Translations.
     * Can be called in 3 different ways allowing the return of different result sets by specifying index criteria:
     * @param indexCriteria1 = idSection
     * @param indexCriteria2 = page
     * 1: All phrases
     * 2: All phrases by Unit ID
     * 3: All phrases by Unit ID and Section Type ID
     *
     * * API call: api/(<LOCALE>)?/phrase/<ID_UNIT>/<ID_SECTION_TYPE>/
     */
    public function actionIndex() {
        // If listCriteria1(idSection) not set or set = 0 get all phrases (Limited to a 1000 rows to avoid timeout)
        $locale = isset($_GET['locale']) && $_GET['locale'] != '' ? $_GET['locale'] : 'en';
        if(!isset($_GET['indexCriteria1']) || $_GET['indexCriteria1'] == 0) {
            $criteria = new CDbCriteria;
            $criteria->limit = 1000;
            $models = Phrase::model()->findAll($criteria);
            // Else find phrases belonging to section (and page)?
        } else {
            $idUnit = $_GET['indexCriteria1'];
            $idSectionType = $_GET['indexCriteria2'];
            $section = new Section();
            $idSection = $section->findSectionIDByUnitSectionType($idUnit, $idSectionType);
            if($idSectionType == 1 || $idSectionType == 5) {
                $models = Videos::model()->findVideosBySection($idSection, $locale);
            } else {
                $models = Phrase::model()->findAllBySectionPage($idUnit, $idSectionType, $idSection, $locale, 0);
            }
        }
        // Did we get some results?
        if(empty($models)) {
            // No db results inform requester.
            $this->sendResponse(404, sprintf('No rows where found in the database. Section id: %s is not valid', is_null($idSection) ? 'No section found' : $idSection));
        } else {
            // Prepare response
            $rows = array();
            foreach($models as $model)
                $rows[] = $model;
            // Send the response
            $this->sendResponse(200, $rows);
        }
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Phrase('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Phrase']))
            $model->attributes = $_GET['Phrase'];
        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param $id
     * @throws CHttpException
     * @return Phrase / CActiveRecord
     */
    public function loadModel($id) {
        $model = Phrase::model()->findByPk($id);
        if($model === null)
            throw new CHttpException(404, 'The requested resource does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if(isset($_POST['ajax']) && $_POST['ajax'] === 'phrase-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
