<?php

class UnitController extends ApiController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters(){
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules(){
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view'),
                'users'=>array('*'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('create','update','admin','delete'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }



    /**
     * Returns JSON representation of Unit row with given id
     *
     * API call: api/(<LOCALE>)?/unit/<UNIT_ID>/
     * @return bool|void
     */
    public function actionView(){
        $size = $this->getImageDimensions();
        $id = $_GET['viewCriteria'];
        $locale = isset($_GET['locale']) && $_GET['locale'] !='' ? $_GET['locale'] : 'en';
        $unit = new Unit();
            if ($locale == 'en') {
                $row = $unit->findByPrimaryKey($id);
            } else {
                $row = $unit->findByPkTranslation($id ,$locale);
            }
        $images = new Images();
        $unitImages[] = $images->getUnitImages($id, $size);
        $unitImages = $unitImages[0];
        foreach($unitImages as $key => $unitImage){
            $row[] = $unitImages[$key];
        }
        if($row){
            $this->sendResponse(200, $row);
        } else {
            $this->sendResponse(404, 'No row with id: ' . $id . ' in DB');
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate(){
        /*		$model=new Unit;

                // Uncomment the following line if AJAX validation is needed
                // $this->performAjaxValidation($model);

                if(isset($_POST['Unit']))
                {
                    $model->attributes=$_POST['Unit'];
                    if($model->save())
                        $this->redirect(array('view','id'=>$model->id));
                }

                $this->render('create',array(
                    'model'=>$model,
                ));
        */	}

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate(){
        /*		$model=$this->loadModel($id);

                // Uncomment the following line if AJAX validation is needed
                // $this->performAjaxValidation($model);

                if(isset($_POST['Unit']))
                {
                    $model->attributes=$_POST['Unit'];
                    if($model->save())
                        $this->redirect(array('view','id'=>$model->id));
                }

                $this->render('update',array(
                    'model'=>$model,
                ));
        */	}

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     * @throws CHttpException
     */
    public function actionDelete(){
        /*		if(Yii::app()->request->isPostRequest)
                {
                    // we only allow deletion via POST request
                    $this->loadModel($id)->delete();

                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
                }
                else
                    throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        */	}

    /**
     * Lists units.
     * Can be called in 3 different ways allowing for return of:
     * 1: All units (/api/unit/<empty or 0>/<empty or 0>)
     * 2: All units for a given course (/api/unit/<idCourse>/<empty or 0>)
     * 3: All units for a given level in a given course (/api/unit/<idCourse>/<idLevel>)
     */
    public function actionIndex(){
        $indexCriteria1 = isset($_GET['indexCriteria1']) ? $_GET['indexCriteria1'] : null;
        $indexCriteria2 = isset($_GET['indexCriteria2']) ? $_GET['indexCriteria2'] : null;
        $locale = isset($_GET['locale']) && $_GET['locale'] !='' ? $_GET['locale'] : 'en';
        $size = $this->getImageDimensions();
        $unit = new Unit();
        if (is_null($indexCriteria1)) {
            if($locale == 'en') {
                $rows = $unit->findAll();
            } else {
                $rows = $unit->findAllByLocale($locale);
            }
            // Else find all units for Course/(level)?
        } else {
            // If Level parameter is not set or set as 0 get all units for the course
            if (is_null($indexCriteria2) || intval($indexCriteria2) === 0) {
                $rows = $unit->findAllByCourse($indexCriteria1, $locale);
                // If level parameter is set > 0 get units for requested level only
            } else {
                $rows = $unit->findAllByCourseLevel($indexCriteria1, $indexCriteria2, $locale);
            }
        }
        // Did we get some results?
        if(empty($rows) || $rows === []) {
            // No db results, inform requester.
            $this->sendResponse(404, 'No rows where found in the database. Either course id: '. $indexCriteria1 .' or level id: '. $indexCriteria2.' or both are invalid or there are no translations for locale: '.$locale);
        } else {
            // Prepare response
            $responseArray = array();
            $images = new Images();
            foreach ($rows as $row) {
                unset($tempArray);
                $tempArray[] = $row;
                unset($unitImages);
                $unitImages[] = $images->getUnitImages($row['idUnit'], $size);
                $unitImages = $unitImages[0];
                foreach($unitImages as $key => $unitImage){
                    $tempArray[] = $unitImages[$key];
                }
                $responseArray[] = $tempArray;
            }
            // Send the response
            $this->sendResponse(200, $responseArray);
        }
    }

    /**
     * @return string
     */
    private function getImageDimensions(){
        $headerDevice = HeUtils::getServerValue('DEVICE');
        if ($headerDevice) {
            $device = new Devices();
            $size = $device->getDeviceSize($headerDevice);
        } else {
            $size = '2x';
        }
        return $size;
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new Unit('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Unit']))
            $model->attributes=$_GET['Unit'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model){
        if(isset($_POST['ajax']) && $_POST['ajax']==='unit-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
