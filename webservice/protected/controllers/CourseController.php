<?php

class CourseController extends ApiController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    public function filters() {
        return array('accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(array('allow',  // allow all users to perform 'index' and 'view' actions
            'actions' => array('index', 'view'), 'users' => array('*'),), array('allow', // allow authenticated user to perform 'create' and 'update' actions
            'actions' => array('create', 'update'), 'users' => array('admin'),), array('allow', // allow admin user to perform 'admin' and 'delete' actions
            'actions' => array('admin', 'delete'), 'users' => array('admin'),), array('deny',  // deny all users
            'users' => array('*'),),);
    }

    /**
     * Displays a particular model.
     * @return bool|void
     */
    public function actionView() {
        $id = $_GET['viewCriteria'];
        $model = Course::model()->findByPk($id);
        // Did we find the requested model? If not produce error response to requester.
        if(is_null($model)) {
            $this->sendResponse(404, 'No Course found with id: ' . $id);
        } else {
            $row = $model->attributes;
            $this->sendResponse(200, $row);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Course;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if(isset($_POST['Course'])) {
            $model->attributes = $_POST['Course'];
            if($model->save()) $this->redirect(array('view', 'id' => $model->id));
        }
        $this->render('create', array('model' => $model,));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @internal param integer $id the ID of the model to be updated
     */
    public function actionUpdate() {

        /*		$model= Course::model()->findByPk($id);

                // Uncomment the following line if AJAX validation is needed
                // $this->performAjaxValidation($model);

                if(isset($_POST['Course']))
                {
                    $model->attributes=$_POST['Course'];
                    if($model->save())
                        $this->redirect(array('view','id'=>$model->id));
                }

                $this->render('update',array(
                    'model'=>$model,
                ));
        */
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @internal param int $id the ID of the model to be deleted
     */
    public function actionDelete() {
        /*		if(Yii::app()->request->isPostRequest)
                {
                    // we only allow deletion via POST request
                    Course::model()->findByPk($id)->delete();

                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
                }
                else
                    throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        */
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $models = Course::model()->findAll();
        if(empty($models)) {
            // No db results inform requester.
            $this->sendResponse(404, 'No Courses found in Database');
        } else {
            // Prepare response
            $rows = array();
            foreach($models as $model) $rows[] = $model;
            // Send the response
            $this->sendResponse(200, $rows);
        }
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Course('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Course'])) $model->attributes = $_GET['Course'];

        $this->render('admin', array('model' => $model,));
    }

    /**
     * Performs the AJAX validation.
     * @param Course $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if(isset($_POST['ajax']) && $_POST['ajax'] === 'course-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
