<?php

/**
 *
 * Class AuthorizationController
 *
 * Api without aba user, ApiController & WsRestComController
 *
 */

class AuthorizationController extends CController
{

    static protected $version = "1.0";

    static protected $aHistoryVersion = array(
      "1.0" => "New API controller"
    );

    protected static $aResponseCodes = array(
      200 => 'OK',
      400 => 'Bad Request',
      401 => 'Unauthorized',
      402 => 'Unknown token',
      403 => 'Forbidden',
      404 => 'Not Found',
      500 => 'Internal Server Error',
      501 => 'Not Implemented',
      555 => 'Unidentified error',
    );

    const RESPONSE_CODE_OK = 200;
    const RESPONSE_CODE_BAD_REQUEST = 400;
    const RESPONSE_CODE_UNAUTHORIZED = 401;
    const RESPONSE_CODE_UNKNOWN_TOKEN = 402;
    const RESPONSE_CODE_FORBIDDEN = 403;
    const RESPONSE_CODE_NOT_FOUND = 404;
    const RESPONSE_CODE_INTERNAL_SERVER_ERROR = 500;
    const RESPONSE_CODE_NOT_IMPLEMENTED = 501;
    const RESPONSE_CODE_UNIDENTIFIED_ERROR = 555;

    const REQUEST_PARAM_X_TOKEN = 'X_TOKEN';
    const REQUEST_PARAM_X_EMAIL = 'EMAIL';

    const DEFAULT_CONTENT_TYPE = 'application/json;charset=UTF-8';

    protected $_format = 'application/json;charset=UTF-8';
    protected $_reqDataDecoded;
    protected $_reqMethod;

    /**
     * @param string $id
     * @param null $module
     */
    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);

        Yii::app()->params["isTransRunning"] = false;
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
          array(
            'allow',  // allow all users to perform 'index' and 'view' actions
            'actions' => array(
              'index', 'authorize', 'view'
            ),
            'users' => array('*'),
          ),
          array(
            'deny', // deny all users
            'users' => array('*'),
          ),
        );
    }

    /**
     * @return array
     */
    public function filters()
    {
        return array(
          'accessControl', // perform access control for CRUD operations
          array('application.filters.XTokenFilter'),
        );
    }

    /**
     * @param CAction $action
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterAction($action)
    {
    }

    /**
     * @param CAction $action
     *
     * @return bool
     */
    public function beforeAction($action)
    {
        $this->disableCWebLogRoute();

        $data = array();
        $requestMethod = strtolower($_SERVER['REQUEST_METHOD']);

        switch (strtoupper($requestMethod)) {

            case 'GET':
                $data = $this->actionParams;
                break;

            case 'POST':
                $data = HeMixed::getPostArgs();
                break;
        }

        $this->_reqDataDecoded = $data;
        $this->_reqMethod = strtoupper($requestMethod);

        return true;
    }

    /**
     * BUG #5725
     *
     * Cuando YII_DEBUG está habilitado y se utiliza showInFireBug en el CWebLogRoute aparece código HTML al final de
     * cada JSON de cada llamada REST.
     *
     */
    protected function disableCWebLogRoute()
    {
        if (YII_DEBUG) {
            foreach (Yii::app()->log->routes as $route) {
                if ($route instanceof CWebLogRoute) {
                    $route->enabled = false; // disable any weblogroutes
                }
            }
        }
    }

    /**
     * @param int $status
     * @param string $body
     * @param string $contentType
     */
    protected function sendResponse(
      $status = self::RESPONSE_CODE_OK,
      $body = '',
      $contentType = self::DEFAULT_CONTENT_TYPE
    ) {
        $statusHeader = 'HTTP/1.1 ' . $status . ' ' . (!is_array($body) ? $body : '');
        header($statusHeader);
        header('Content-Type: ' . $contentType);

        if (is_array($body)) {
            $body = CJSON::encode($body);
        } else {
            $body = CJSON::encode(array('httpCode' => $status, 'message' => $body));
        }
        echo $body;

        Yii::app()->end();
    }

    /**
     * Handles execution of petitioned command
     *
     * @return bool
     */
    public function actionView()
    {
        if (isset($_GET['action'])) {

            switch (strtoupper($_GET['action'])) {

                case 'AUTHORIZE':
                    $this->actionAuthorize();
                    return true;
                    break;

                default:
                    break;
            }
        }

        $this->sendResponse(self::RESPONSE_CODE_BAD_REQUEST,
          self::$aResponseCodes[self::RESPONSE_CODE_UNIDENTIFIED_ERROR],
          $this->_format
        );

        return false;
    }

    /**
     * Parameters expected in HEADERS:
     * X_TOKEN
     * EMAIL
     *
     * @return bool
     */
    public function actionAuthorize()
    {

        try {

            $email = HeUtils::getRequestParamValue(self::REQUEST_PARAM_X_EMAIL);

            $oCmAbaUser = new CmAbaUser();

            if ($oCmAbaUser->getUserByEmail($email)) {

                $this->sendResponse(self::RESPONSE_CODE_OK, array(
                  'userId' => $oCmAbaUser->id,
                  'userType' => $oCmAbaUser->userType,
                  'email' => $oCmAbaUser->email,
                ), self::DEFAULT_CONTENT_TYPE);

            }

        } catch (Exception $ex) {

            error_log("Login error=> " . $ex->getMessage());

            $this->sendResponse(
              self::RESPONSE_CODE_INTERNAL_SERVER_ERROR,
              self::$aResponseCodes[self::RESPONSE_CODE_INTERNAL_SERVER_ERROR],
              self::DEFAULT_CONTENT_TYPE
            );
        }

        $this->sendResponse(
          self::RESPONSE_CODE_NOT_FOUND,
          self::$aResponseCodes[self::RESPONSE_CODE_NOT_FOUND],
          self::DEFAULT_CONTENT_TYPE
        );

        return true;
    }

}
