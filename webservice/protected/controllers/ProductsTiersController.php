<?php

/**
 * Created by PhpStorm.
 * User: oleksandr
 * Date: 29/07/15
 * Time: 17:08
 */
class ProductsTiersController extends ApiController
{
    public function beforeAction($action)
    {
        $this->logStart('ENABLE_LOG_COURSE_API');
        $this->authenticate();
        $requestMethod = strtolower($_SERVER['REQUEST_METHOD']);
        $data = array();
        switch (strtoupper($requestMethod)) {
            case 'GET':
                $data = $this->actionParams;
                break;
            case 'POST':
                $data = HeMixed::getPostArgs();
                break;
        }
        $this->reqDataDecoded = $data;
        $this->reqMethod = strtoupper($requestMethod);
        return true;
    }

    public function actionView()
    {
        if (isset($_GET['action'])) {
            switch (strtoupper($_GET['action'])) {
                case 'PRODUCTSTIERS':
                    $this->getProductsTiers();
                    return true;
                    break;
                default:
                    break;
            }

        }

        $this->sendResponse(400, 'Missing or invalid action parameter', $this->_format);

        return false;
    }

    protected function getProductsTiers()
    {
        $productPricesRules = new CmAbaProductPriceRules();
        $productsTiers = $productPricesRules->getProductsTiersByUserCountry($this->user);

        if (count($productsTiers) == 0) {

            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_UNKNOWN_H . ': PRODUCTTIERS',
              HeLogger::IT_BUGS,
              HeLogger::CRITICAL,
              'Tiers list could not be generated.'
            );

            $this->sendResponse(500, array('status' => 'Error', 'message' => "Tiers list could not be generated"), $this->_format);
            return false;
        }

        $this->sendResponse(200, array("text" => Yii::t('mainApp', 'key_appStore_specialPrices', null, null, (isset($this->reqDataDecoded['lang']) ? $this->reqDataDecoded['lang'] : $this->user->langEnv)), "tiers" => $productsTiers), $this->_format);
        return true;
    }
}