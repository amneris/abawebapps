<?php

class GlossaryController extends ApiController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    public function filters()
    {
        return array(
          'accessControl', // perform access control for CRUD operations
          array('application.filters.CORSFilter'),
        );
    }

    public function beforeAction($action)
    {
        $this->disableCWebLogRoute();
        $this->logStart('ENABLE_LOG_COURSE_API');
        return true;
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(array('allow',
          'actions' => array('search'), 'users' => array('*'))
        );
    }

    /**
     * @param null $from
     * @param null $translateTo
     * @param null $word
     */
    public function actionSearch($from = null, $translateTo = null, $word = null)
    {
        if (empty($from) || empty($translateTo) || empty($word)) {
            $this->sendResponse(404, 'Parameters missing');
        }
        $models = Glossary::model()->search($from, $translateTo, $word);
        if (empty($models)) {
            // No db results inform requester.
            $this->sendResponse(404, 'No words found in Database');
        } else {
            // Send the response
            $this->sendResponse(200, $models);
        }
    }
}
