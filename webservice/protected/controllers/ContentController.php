<?php

/**
 * ContentController
 * Author: mgadegaard
 * Date: 03/11/2014
 * Time: 13:04
 * © ABA English 2014
 */
class ContentController extends ApiController
{

    public function filters()
    {
        return array(
          'accessControl', // perform access control for CRUD operations
           array('application.filters.CORSFilter'),
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     *
     */
    public function actionView()
    {
        $idUnit = isset($_GET['viewCriteria']) ? $_GET['viewCriteria'] : null;
        $locale = isset($_GET['locale']) ? $_GET['locale'] : '';
        $action = isset($_GET['action']) ? $_GET['action'] : null;
        if (strtoupper($action) == 'HELP') {
            $help = new Help();
            $helpJSON = $help->getLocalizedHelpContent($locale);
            $this->sendResponse(200, $helpJSON);
        }
        if (!is_null($idUnit)) {
            $section = new Section();
            $videos = new Videos();
            $evaluation = new Evaluation();
            // AbaFilm
            $idSection = $section->getSectionIdByUnitIdSectionTypeId(
                $idUnit,
                HeUnits::getSectionTypeIdByNewSectionName($this::ABA_FILM_SECTION)
            );
            $responseData[] = ['AbaFilm' => $videos->findVideosBySection($idSection, $locale)];
            // Speak
            $responseSpeakRows = $this->getSectionPhrases(
                $idUnit,
                HeUnits::getSectionTypeIdByNewSectionName($this::SPEAK_SECTION),
                $locale
            );
            $responseData[] = ['Speak' => $responseSpeakRows];
            // Write
            $responseWriteRows = $this->getSectionPhrases(
                $idUnit,
                HeUnits::getSectionTypeIdByNewSectionName($this::WRITE_SECTION),
                $locale
            );
            $responseData[] = ['Write' => $responseWriteRows];
            // Interpret
            $responseInterpretRows = $this->getSectionPhrases(
                $idUnit,
                HeUnits::getSectionTypeIdByNewSectionName($this::INTERPRET_SECTION),
                $locale
            );
            $responseData[] = ['Interpret' => $responseInterpretRows];
            // Video Class
            $idSection = $section->getSectionIdByUnitIdSectionTypeId(
                $idUnit,
                HeUnits::getSectionTypeIdByNewSectionName($this::VIDEO_CLASS_SECTION)
            );
            $responseData[] = ['Video Classes' => $videos->findVideosBySection($idSection, $locale)];
            // Exercises
            $responseRows = $this->getSectionPhrases(
                $idUnit,
                HeUnits::getSectionTypeIdByNewSectionName($this::EXERCISES_SECTION),
                $locale
            );
            $responseData[] = ['Exercises' => $responseRows];
            // Vocabulary
            $responseVocabularyRows = $this->getSectionPhrases(
                $idUnit,
                HeUnits::getSectionTypeIdByNewSectionName($this::VOCABULARY_SECTION),
                $locale
            );
            $responseData[] = ['Vocabulary' => $responseVocabularyRows];
            // Assessment
            $responseData[] = ['Assesment' => $evaluation->findAllByUnit($idUnit)];
        }
        // Did we get some results?
        if (empty($responseData)) {
            $this->sendResponse(
                400,
                'Missing Unit ID. Calls to content needs unit id and possibly a section type id as well'
            );
        } else {
            // Prepare response (rearrange array to reset array keys, which is necessary for CJSON::encode to treat the
            // array as we want it to.)
            $rows = array();
            if (count($responseData) > 1) {
                foreach ($responseData as $row) {
                    $rows[] = $row;
                }
                // Send the response
                $this->sendResponse(200, $rows);
            } else {
                $this->sendResponse(200, $responseData[0]);
            }
        }
    }

    /**
     * Lists relevant fields from Phrases, Videos and Evaluation tables (and respective Translations where applicable.)
     * Can be called in 2 different ways allowing the return of different result sets by specifying index criteria:
     * @param indexCriteria1 = idUnit
     * @param indexCriteria2 = idSectionType
     * 1: All phrases/Videos by Unit ID
     * 2: All phrases/videos by Unit ID and Section Type ID
     *
     * * API call: api/(<LOCALE>)?/content/<ID_UNIT>/<ID_SECTION_TYPE>/
     */
    public function actionIndex()
    {
        $responseData = array();
        $locale = isset($_GET['locale']) && $_GET['locale'] != '' ? $_GET['locale'] : 'en';
        $idUnit = isset($_GET['indexCriteria1']) ? $_GET['indexCriteria1'] : 0;
        $idSectionType = isset($_GET['indexCriteria2']) ? $_GET['indexCriteria2'] : 0;
        if ($idUnit > 0 && $idSectionType > 0) {
            $section = new Section();
            $idSection = $section->getSectionIdByUnitIdSectionTypeId($idUnit, $idSectionType);
            if ($idSectionType == 1 || $idSectionType == 5) {
                $videos = new Videos();
                $responseData = $videos->findVideosBySection($idSection, $locale);
            } elseif ($idSectionType == 8) {
                $evaluation = new Evaluation();
                $responseData = $evaluation->findAllByUnit($idUnit);
            } else {
                $phrase = new Phrase();
                $responseData = $phrase->findAllBySectionPage($idUnit, $idSectionType, $idSection, $locale, 0);
            }
        }
        // Did we get some results?
        if (empty($responseData)) {
            $this->sendResponse(
                400,
                'No content found for given parameters. Calls to content needs unit id (1-144) and possibly a section
                 type id (1-8) as well'
            );
        } else {
            // Prepare response (rearrange array to reset array keys, which is necessary for CJSON::encode to treat the
            // array as we want it to.)
            $rows = array();
            if (count($responseData) > 1) {
                foreach ($responseData as $row) {
                    $rows[] = $row;
                }
                // Send the response
                $this->sendResponse(200, $rows);
            } else {
                $this->sendResponse(200, $responseData[0]);
            }
        }
    }

    /**
     * @param $idUnit
     * @param $sectionType
     * @param $locale
     * @return array|bool
     * @throws CDbException
     */
    private function getSectionPhrases($idUnit, $sectionType, $locale)
    {
        $section = new Section();
        $phrase = new Phrase();
        $idSection = $section->getSectionIdByUnitIdSectionTypeId($idUnit, $sectionType);
        $rows = $phrase->findAllBySectionPage($idUnit, $sectionType, $idSection, $locale, 0);
        if ($rows) {
            $response = array();
            foreach ($rows as $row) {
                $response[] = $row;
            }
            return $response;
        }
        return false;
    }
}
