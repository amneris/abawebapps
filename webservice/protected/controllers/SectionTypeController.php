<?php

class SectionTypeController extends ApiController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}



	/**
	 * Displays a particular model.
     * @return bool|void
	 */
	public function actionView(){
        $id = $_GET['viewCriteria'];
        if(is_null($_GET['locale'])) {
            $model = SectionType::model()->findByPk($id);
        } else {
            $locale = $_GET['locale'];
            $model = SectionType::model()->findByPkLocalized($id, $locale);
        }
        if(is_null($model)){
            $this->sendResponse(404, 'Database has no Section Type with id: '.$id);
        }
        $this->sendResponse(200, $model[0]);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate(){
/*		$model=new SectionType;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SectionType']))
		{
			$model->attributes=$_POST['SectionType'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
*/	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate(){
/*		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SectionType']))
		{
			$model->attributes=$_POST['SectionType'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
*/	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete(){
/*		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
*/	}

	/**
	 * Lists all models.
	 */
	public function actionIndex(){
        $locale = $_GET['locale'] == '' ? 'en' : $_GET['locale'];
        $models = $locale == 'en' ? SectionType::model()->findAll() : SectionType::model()->findAllLocalized($locale);
        if(empty($models)){
            $this->sendResponse(404, 'No Section Types in the Database');
        }
        $rows = array();
        foreach($models as $model){
            $rows[] = $model;
        }
        $this->sendResponse(200, $rows);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new SectionType('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SectionType']))
			$model->attributes=$_GET['SectionType'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=SectionType::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='section-type-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
