<?php

class SectionController extends ApiController{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters(){
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules(){
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}



    /**
     * Returns JSON representation of Section row with given id
     * API call: api/(<LOCALE>)?/section/<ID_SECTION>/
     * @return bool|void
     */
    public function actionView(){
        $id = $_GET['viewCriteria'];
        $locale = isset($_GET['locale']) ? $_GET['locale'] : '';
        $model = $this->loadModelWithTranslation($id, $locale);
        $this->sendResponse(200, $model);
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Section;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Section']))
		{
			$model->attributes=$_POST['Section'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @internal param int $id the ID of the model to be updated
     */
	public function actionUpdate()
	{/*
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Section']))
		{
			$model->attributes=$_POST['Section'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));*/
	}

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @internal param int $id the ID of the model to be deleted
     */
	public function actionDelete()
	{
		/*if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');*/
	}

    /**
     * Lists Sections.
     * Can be called in 3 different ways allowing different result sets by specifying index criteria:
     * @param indexCriteria1 = idUnit
     * @param indexCriteria2 = idSectionType
     * 1: All sections
     * 2: All sections by Unit ID
     * 3: Section by Unit and Section Type ID
     */
    public function actionIndex(){
        $indexCriteria1 = isset($_GET['indexCriteria1']) ? $_GET['indexCriteria1'] : null;
        $indexCriteria2 = isset($_GET['indexCriteria2']) ? $_GET['indexCriteria2'] : null;
        $locale = isset($_GET['locale']) ? $_GET['locale'] : '';
        // If indexCriteria1 is not set or set = 0 get all sections
        if (is_null($indexCriteria1) || $indexCriteria1===0){
            $models = Section::model()->findAllByLocale($locale);
            // Else find all units for Course/(level)?
        } else {
            // If indexCriteria2 is not set or set as 0 return all sections for unit with idUnit = indexCriteria1
			// else returns just the section identified by section type id indexCriteria2
			$models = Section::model()->findAllByAttributesTranslated($indexCriteria1, $indexCriteria2, $locale);
        }
        // Did we get some results?
        if(empty($models)) {
            // No db results inform requester.
            $this->sendResponse(404, 'No Sections where found in the Database.');
        } else {
            // Prepare response
            $rows = array();
            foreach($models as $model)
                $rows[] = $model;
            // Send the response
            $this->sendResponse(200, $rows);
        }
    }


	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Section('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Section']))
			$model->attributes=$_GET['Section'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param $id
     * @throws CHttpException
     * @internal param the $integer ID of the model to be loaded
     * @return Section
     */
	public function loadModel($id)
	{
		$model=Section::model()->findByPk($id);
		if($model===null)
            $this->sendResponse(404, 'No Section found with id: '.$id);
		return $model[0];
	}

    /**
     * @param $id
     * @param $locale
     * @return array
     */
    public function loadModelWithTranslation($id, $locale){
        $model = Section::model()->findByPkTranslation($id, $locale);
        if($model===null || $model === [])
            $this->sendResponse(404, 'No Section or Translation found with id: '. $id .' For locale: ' . $locale);
        return $model[0];
    }

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='section-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
