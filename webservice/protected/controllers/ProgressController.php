<?php
/**
 * ProgressController
 * Author: mgadegaard
 * Date: 03/11/2014
 * Time: 13:29
 * © ABA English 2014
 */

class ProgressController extends ApiController {

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index', 'view', 'register'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update'),
                'users'=>array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }



    /**
     * function handles requests to uri's matching /progress/action/<viewCriteria1>/<viewCriteria2>/<viewCriteria>/<viewCriteria4>
     * Where any value after <viewcriteria1> is optional. Below the different actions are described:
     *
     * Progressall: Returns sum of percentages from all started sections of all units for the user
     * Levelprogress: Returns sum of percentages from all started sections of all units belonging to specified level
     * Unitsectionprogress: Returns percentages of every started section belonging to specified unit
     * Levelsectionprogress: Returns percentages of every started section belonging to units in specified level
     * Coursesectionprogress: Returns percentages of every started section belonging to specified course
     * Sectionlistcompletedelements: Returns actions performed within specified section
     * Unitlistcompletedelements: Returns actions performed within specified unit
     * Levellistcompletedelements: Returns actions performed within specified level
     * Courselistcompletedelements: Returns actions performed within specified course
     *
     * @throws CDbException
     */
    public function actionView(){
        $followUp = new AbaFollowup();
        $followupHTML = new AbaFollowUpHtml4();
        $body = '';
        $rearrange = true;
        $malformed = false;
        if(isset($_GET['action'])) {
            $idUser = isset($_GET['viewCriteria1']) ? $_GET['viewCriteria1'] : null;
            if($idUser == null){
                $body = 'Missing parameters. URI needs to be: /progress/<ACTION>/<ID_USER>/...';
                $malformed = true;
            } else {
                switch (strtoupper($_GET['action'])) {
                    case 'PROGRESSALL':
                        $body = $followUp->getUserTotalProgress($idUser);
                        break;
                    case 'LEVELPROGRESS':
                        $idLevel = isset($_GET['viewCriteria2']) ? $_GET['viewCriteria2'] : null;
                        if($idLevel == null){
                            $malformed = true;
                            $body = 'Missing parameters URI needs to be: /progress/progressall/<ACTION>/<ID_USER>/<ID_LEVEL>';
                        } else {
                            $body = $followUp->getUserTotalProgress($idUser, $idLevel);
                        }
                        break;
                    case 'UNITSECTIONPROGRESS':
                        $idUnit = isset($_GET['viewCriteria2']) ? $_GET['viewCriteria2'] : null;
                        if($idUnit == null){
                            $malformed = true;
                            $body = 'Missing parameters URI needs to be: /progress/unitsectionprogress/<ACTION>/<ID_USER>/<ID_UNIT>';
                        } else {
                            $followUp = $followUp->getFollowupByIds($idUser, $idUnit);
                            if($followUp) {
                                $body = $followUp->readableFollowUp($followUp->attributes);
                            }
                            $rearrange = false;
                        }
                        break;
                    case 'LEVELSECTIONPROGRESS':
                        $idLevel = isset($_GET['viewCriteria2']) ? $_GET['viewCriteria2'] : null;
                        if($idLevel == null){
                            $malformed = true;
                            $body = 'Missing parameters URI needs to be: /progress/levelsectionprogress/<ACTION>/<ID_USER>/<ID_LEVEL>';
                        } else {
                            $body = $followUp->getLevelSectionsProgress($idUser, $idLevel);
                        }
                        break;
                    case 'COURSESECTIONPROGRESS':
                        $body = $followUp->getCourseSectionsProgress($idUser);
                        break;
                    case 'SECTIONLISTCOMPLETEDELEMENTS':
                        $idUnit = isset($_GET['viewCriteria2']) ? $_GET['viewCriteria2'] : null;
                        $idSectionType = isset($_GET['viewCriteria3']) ? $_GET['viewCriteria3'] : null;
                        $unixTimestamp = isset($_GET['viewCriteria4']) ? $_GET['viewCriteria4'] : null;
                        if($idUnit == null || $idSectionType == null){
                            $body = 'Missing parameters URI needs to be: /progress/sectionlistcompletedelements/<ACTION>/<ID_USER>/<ID_UNIT>/<ID_SECTION_TYPE>';
                            $malformed = true;
                        } else {
                            if($unixTimestamp == null){
                                $body = $followupHTML->getSectionListCompletedElements($idUser, $idUnit, $idSectionType);
                            } else {
                                $body = $followupHTML->getSectionListCompletedElements($idUser, $idUnit, $idSectionType, $unixTimestamp);
                            }
                        }
                        break;
                    case 'UNITLISTCOMPLETEDELEMENTS':
                        $idUnit = isset($_GET['viewCriteria2']) ? $_GET['viewCriteria2'] : null;
                        $unixTimestamp = isset($_GET['viewCriteria3']) ? $_GET['viewCriteria3'] : null;
                         if($idUnit == null){
                             $malformed = true;
                             $body = 'Missing parameters URI needs to be: /progress/unitlistcompletedelements/<ACTION>/<ID_USER>/<ID_UNIT>';
                         } else {
                             if($unixTimestamp == null){
                             $body = $followupHTML->getUnitListCompletedElements($idUser, $idUnit);
                         } else {
                                 $body = $followupHTML->getUnitListCompletedElements($idUser, $idUnit, $unixTimestamp);
                    }
                         }
                        break;
                    case 'LEVELLISTCOMPLETEDELEMENTS':
                        $idLevel = isset($_GET['viewCriteria2']) ? $_GET['viewCriteria2'] : null;
                        $unixTimestamp = isset($_GET['viewCriteria3']) ? $_GET['viewCriteria3'] : null;
                        if($idLevel == null){
                            $malformed = true;
                            $body = 'Missing parameters URI needs to be: /progress/levellistcompletedelements/<ACTION>/<ID_USER>/<ID_LEVEL>';
                        } else {
                            if($unixTimestamp == null){
                                $body = $followupHTML->getLevelListCompletedElements($idUser, $idLevel);
                            } else {
                                $body = $followupHTML->getLevelListCompletedElements($idUser, $idLevel, $unixTimestamp);
                            }
                        }
                        break;
                    case 'COURSELISTCOMPLETEDELEMENTS':
                        $idCourse = isset($_GET['viewCriteria2']) ? $_GET['viewCriteria2'] : null;
                        $unixTimestamp = isset($_GET['viewCriteria3']) ? $_GET['viewCriteria3'] : null;
                        if($unixTimestamp == null){
                            $body = $followupHTML->getCourseListCompletedElements($idUser, $idCourse);
                        } else {
                            $body = $followupHTML->getCourseListCompletedElements($idUser, $idCourse, $unixTimestamp);
                        }
                        break;
                    default:
                        break;
                }
            }
            $bodyRearranged = array();
            if(count($body) > 1){
                if($rearrange){
                    // Prepare response (rearrange array to reset array keys, which is necessary for CJSON::encode to treat the array as we want)
                    foreach ($body as $element) {
                        $bodyRearranged[] = $element;
                    }
                    $this->sendResponse(200, $bodyRearranged);
                } else {
                    $this->sendResponse(200, $body);
                }
            } elseif($malformed) {
                $this->sendResponse(400, $body);
            } else {
                $this->sendResponse(200, $body);
            }
        }
        $this->sendResponse(400, 'Missing or invalid action parameter', $this->_format);
    }

    /**
     *
     * parameters: $id_learner, $unit, $section, $page, $id_elem, $action, $bien/$mal, $wtext, $time
     * parameters:
     **/
    public function actionRegister() {
        $locale = isset($_POST['locale']) ? $_POST['locale'] : $_GET['locale'];
        If($locale == '') {
            $body = 'Missing locale parameters URI';
            $this->sendResponse(400, $body);
            return false;
        }
        $jsonArray = json_decode(file_get_contents('php://input'));
        $accumulatedResponse = array();
        $noResponseEvent = false;
        $count = count($jsonArray);

        $throttle = Yii::app()->config->get("PROGRESS_REGISTER_THROTTLE");
        $shouldDeleteProgressFromClients = Yii::app()->config->get("PROGRESS_REGISTER_DELETE_EXCESS_PROGRESS_FROM_CLIENTS");

        if ($count > $throttle) { // ABAWEBAPPS-612 & ABAWEBAPPS-623
            Yii::log("ProgressController::actionRegister() - user: " . Yii::app()->user->id . " , count: " . $count);
            $rearrangedArray = array();

            if ($shouldDeleteProgressFromClients) {
                Yii::log("ProgressController::actionRegister() - user: " . Yii::app()->user->id . " - TRYING TO REMOVE");
                Yii::log(json_encode($jsonArray), CLogger::LEVEL_INFO, 'events');
                $clsAbaFollowUp = new AbaFollowup();
                $rearrangedArray[] = $clsAbaFollowUp->updatePercent($jsonArray[0]->idUser, "1", null, 0);
            }

            $this->sendResponse(200, $rearrangedArray);
            return true;
        }

        foreach($jsonArray as $rowProgress) {
            if($rowProgress->action == 'NEWSESSION') {
                $noResponseEvent = true;
            } elseif($rowProgress->action == 'ENTER') {
                $followUp = new AbaFollowup();
                $followUp->getFollowupByIds($rowProgress->idUser, $rowProgress->idUnit);
                $noResponseEvent = true;
            } else {
                $clsSection = new Section();
                $clsSectionItems = new SectionItems();
                $idSection = $clsSection->getSectionIdByUnitIdSectionTypeId($rowProgress->idUnit, $rowProgress->idSectionType);
                $selectedLocale = HeApi::localeSelector($rowProgress->idUnit, $locale);

                //ABC Testing
                $sectionBinInDecimal=0;
                switch ($rowProgress->idSectionType){
                    case 2:
                        $sectionBinInDecimal=64;
                        break;
                    case 7:
                        $sectionBinInDecimal=2;
                        break;
                }
                //Check if the user is in a test.
                $numTestUser=0;
                If ($sectionBinInDecimal>0){
                    $modelCourseTestUser = CmAbaCourseTestUser::model()->find('userId=' . $rowProgress->idUser);
                    if($modelCourseTestUser) {
                        $numTestUser = $modelCourseTestUser->courseId;
                        $courseSection=CmAbaCourseTest::model()->retrieveCourse($numTestUser,$selectedLocale);
                        //Check the language and section to see if we need to apply ABC Testing
                        if(($courseSection & $sectionBinInDecimal) == 0) {
                            $numTestUser = 0;
                        }
                    } else {
                        $numTestUser = 0;
                    }
                }

                if($rowProgress->idSectionType==7 && $numTestUser==3){
                    $exercises = $clsSectionItems->findExercises($idSection, 'es',$numTestUser);
                } else {
                    $exercises = $clsSectionItems->findExercises($idSection, $selectedLocale,$numTestUser);
                }

                switch($rowProgress->idSectionType) {
                    case 1: // ABA Film, "SPEAKING", 1, "EJERCICIOS ORALES"
                    {
                        $clsAbaFollowUp = new AbaFollowup();
                        $clsAbaFollowUp = $clsAbaFollowUp->getFollowupByIds($rowProgress->idUser, $rowProgress->idUnit);
                        IF(!$clsAbaFollowUp) {
                            $clsAbaFollowUp = new AbaFollowup();
                            $clsAbaFollowUp->insertRecord($rowProgress->idUser, $rowProgress->idUnit);
                        }
                        $jsonResponse = $clsAbaFollowUp->updatePercent($rowProgress->idUser, $rowProgress->idUnit, 'sit_por', 100);

                        break;
                    }
                    case 5: //Video Class, "GRAMMAR", 5, "GRAMMAR" (Section is comprised of two parts. Video class and grammar. The latter is currently not supported.)
                    {
                        $clsAbaFollowUp = new AbaFollowup();
                        $clsAbaFollowUp = $clsAbaFollowUp->getFollowupByIds($rowProgress->idUser, $rowProgress->idUnit);
                        if(!$clsAbaFollowUp) {
                            $clsAbaFollowUp = new AbaFollowup();
                            $clsAbaFollowUp->insertRecord($rowProgress->idUser, $rowProgress->idUnit);
                        }
                        $jsonResponse = $clsAbaFollowUp->updatePercent($rowProgress->idUser, $rowProgress->idUnit, 'gra_vid', 100);

                        break;
                    }
                    case 8: // Evaluation, "MINITEST", 8, "TEST DE NIVEL"
                    {
                        $clsAbaFollowUp = new AbaFollowup();
                        $clsAbaFollowUp = $clsAbaFollowUp->getFollowupByIds($rowProgress->idUser, $rowProgress->idUnit);
                        if($clsAbaFollowUp) {
                            $clsAbaFollowUp->updateField('exercises', $rowProgress->exercises);
                        } else {
                            $clsAbaFollowUp = new AbaFollowup();
                            if($clsAbaFollowUp->insertRecord($rowProgress->idUser, $rowProgress->idUnit)) {
                                $clsAbaFollowUp->updateField('exercises', $rowProgress->exercises);
                            }
                        }
                        $jsonResponse = $clsAbaFollowUp->updatePercent($rowProgress->idUser, $rowProgress->idUnit, 'eva_por', $rowProgress->puntuation);

                        break;
                    }
                    default : {
                        $clsAbaFollowUp = new AbaFollowup();
                        if($rowProgress->action == 'HELP') {
                            if($rowProgress->idSectionType == 3 || $rowProgress->idSectionType == 6) {
                                $jsonResponse = $clsAbaFollowUp->updateCount($rowProgress->idUser, $rowProgress->idUnit, HeUnits::getOldSectionTitle($rowProgress->idSectionType), 'ans');
                                $clsAbaFollowUp->getFollowupByIds($rowProgress->idUser, $rowProgress->idUnit);
                            }
                        } else {
                            $clsAbaFollowUpHtml4 = new AbaFollowUpHtml4();
                            $valor_por = $clsAbaFollowUpHtml4->registerProgress($rowProgress, $exercises);
                            If($rowProgress->correct == 0) {
                                if($rowProgress->text != '') {
                                    $jsonResponse = $clsAbaFollowUp->updateCount($rowProgress->idUser, $rowProgress->idUnit, HeUnits::getOldSectionTitle($rowProgress->idSectionType), 'err');
                                    $clsAbaFollowUp->getFollowupByIds($rowProgress->idUser, $rowProgress->idUnit);
                                }
                            } else {
                                $jsonResponse = $clsAbaFollowUp->updatePercent($rowProgress->idUser, $rowProgress->idUnit, strtolower(substr_replace(HeUnits::getOldSectionTitle($rowProgress->idSectionType), '', 3) . '_por'), $valor_por);
                            }
                        }
                        break;
                    }
                }
                if (isset($jsonResponse) && $jsonResponse) {
                    $accumulatedResponse[$rowProgress->idUnit] = $jsonResponse;
                } else {
                    Yii::log("no jsonResponse, skipping", CLogger::LEVEL_INFO);
                }
            }
        }
        if(!empty($accumulatedResponse)) {
            foreach($accumulatedResponse as $jsonElement) {
                $rearrangedArray[] = $jsonElement;
            }
            $this->sendResponse(200, $rearrangedArray);
        } else {
            if($noResponseEvent){
                $this->logEnd(null, true);
            }
            return true;
        }
    }

    public function actionIndex(){
        echo 'Action Index in the Progress Controller is not implemented';
    }

} 