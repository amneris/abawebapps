<?php
/**
 * EvaluationController
 * Author: mgadegaard
 * Date: 27/10/2014
 * Time: 17:37
 * © ABA English 2014
 */

class EvaluationController extends ApiController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2';

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }



    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index', 'view'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update'),
                'users'=>array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @return bool|void
     */
    public function actionView(){
        $id = $_GET['viewCriteria'];
        $model = Evaluation::model()->findByPk($id);
        // Did we find the requested model? If not produce error response to requester.
        if(is_null($model)) {
            $this->sendResponse(404, 'No Evaluation found with id: '. $id);
        } else {
            $row = $model->attributes;
            $this->sendResponse(200, $row);
        }
    }

    /**
     * Manages all models.
     */
    public function actionAdmin(){
        $model=new Evaluation('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Evaluation']))
            $model->attributes=$_GET['Evaluation'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * Performs the AJAX validation.
     * @param Course $model the model to be validated
     */
    protected function performAjaxValidation($model){
        if(isset($_POST['ajax']) && $_POST['ajax']==='evaluation-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex(){
        if (!isset($_GET['indexCriteria1']) || $_GET['indexCriteria1'] == 0) {
            $models = Evaluation::model()->findAll();
        } else {
            $idUnit = $_GET['indexCriteria1'];
            $models = Evaluation::model()->findAllByUnit($idUnit);
        }
        if(empty($models)) {
            // No db results inform requester.
            $this->sendResponse(404, 'No Evaluations found in Database');
        } else {
            // Prepare response
            $rows = array();
            foreach($models as $model)
                $rows[] = $model;
            // Send the response
            $this->sendResponse(200, $rows);
        }
    }
}