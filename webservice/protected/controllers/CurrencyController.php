<?php
/**
 * CurrencyController
 * Author: mgadegaard
 * Date: 09/03/2015
 * Time: 17:41
 * © ABA English 2015
 */

class CurrencyController extends ApiController {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('view'),
                'users'=>array('*'),
            ),
        );
    }

    /**
     *
     */
    public function actionView(){
        $action = isset($_GET['action']) ? $_GET['action'] : null;
        $amount = isset($_GET['viewCriteria1']) ? $_GET['viewCriteria1'] : null;
        $otherCurrency = isset($_GET['viewCriteria2']) ? $_GET['viewCriteria2'] : null;
        $rate = 0.0;
        $exchange = 0.0;
        if(is_null($action) || is_null($amount) || is_null($otherCurrency)){
            $this->sendResponse(400, 'Malformed request, should be on the form: action/amount/otherCurrency, please check documentation and try again');
        } elseif(strtolower($action) == 'fromeur'){
            $currency = new CmCurrencies(strtoupper($otherCurrency));
            $rate = $currency->unitEur;
            $exchange = $amount * $rate;
        } elseif(strtolower($action) == 'toeur'){
            $currency = new CmCurrencies(strtoupper($otherCurrency));
            $rate = $currency->outEur;
            $exchange = $amount * $rate;
        }
        $this->sendResponse(200, array('Rate'=>$rate, 'Value'=>$exchange));
    }
}