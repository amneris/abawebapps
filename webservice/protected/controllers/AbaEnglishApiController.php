<?php

/**
 * AbaEnglishApiController
 *
 * @author mgadegaard
 * Date: 13/10/2014
 * Time: 17:28
 * © ABA English 2014
 */
class AbaEnglishApiController extends ApiController
{
    public function beforeAction($action){
        $this->disableCWebLogRoute();
        $this->logStart('ENABLE_LOG_COURSE_API');
        return true;
    }

    /**
     *  Returns the current version number and history of the API.
     *
     * URL: http://<HOSTNAME>/api/abaEnglishApi/version
     */
    protected function viewVersion()
    {
        $this->sendResponse(
            200,
            array('versionNumber' => self::$version, "history" => self::$aHistoryVersion),
            $this->_format
        );
    }

    /**
     *  Returns the current version number and history of the API.
     *
     * URL: http://<HOSTNAME>/api/abaEnglishApi/requiredVersion
     */
    protected function viewRequiredVersion()
    {
        $this->sendResponse(
            200,
            array(
                "android" => trim((string)Yii::app()->config->get("APP_REQ_VERSION_ANDROID")),
                "ios" => trim((string)Yii::app()->config->get("APP_REQ_VERSION_IOS"))
            ),
            $this->_format
        );
    }

    /**
     *  Just to obtain the IP from the user.
     *
     * URL: http://<HOSTNAME>/api/abaEnglishApi/iprecognition
     */
    protected function viewIpRecognition()
    {
        $ipClient = Yii::app()->request->getUserHostAddress();
        $this->sendResponse(200, array('publicIp' => $ipClient), $this->_format);
    }

    /**
     * Returns an array with available API 'Commands'
     *
     * URL: http://<HOSTNAME>/api/abaEnglishApi/actions or http://<HOSTNAME>/api/abaEnglishApi/
     */
    protected function viewActions()
    {
        $this->sendResponse(
            200,
            array('This resource can be called with one of the following commands at the end of the URI: ',
                self::$infoActions),
            $this->_format
        );
    }

    /**
     * Handles execution of petitioned command
     *
     * @return bool
     */
    public function actionView()
    {
        if (HeUtils::getSuperGlobal('GET', 'action')) {
            switch (strtoupper(HeUtils::getSuperGlobal('GET', 'action'))) {
                case 'VERSION':
                    $this->viewVersion();
                    return true;
                    break;
                case 'REQUIREDVERSION':
                    $this->viewRequiredVersion();
                    return true;
                    break;
                case 'IPRECOGNITION':
                    $this->viewIpRecognition();
                    return true;
                    break;
                case 'ACTIONS':
                    $this->viewActions();
                    return true;
                    break;
                default:
                    break;
            }
        }
        $this->sendResponse(400, 'Missing or invalid action parameter', $this->_format);
        return false;
    }

    /**
     * Returns an array with available API 'Commands'
     *
     * @return bool
     */
    public function actionIndex()
    {
        $this->viewActions();
        return true;
    }

    public function actionError()
    {
    }
}
