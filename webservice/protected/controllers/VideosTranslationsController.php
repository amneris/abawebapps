<?php
/**
 * VideosTranslationsController
 * Author: mgadegaard
 * Date: 28/10/2014
 * Time: 13:41
 * © ABA English 2014
 */

class VideosTranslationsController extends ApiController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2';

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }



    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index', 'view'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update'),
                'users'=>array('admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @return bool|void
     */
    public function actionView(){
        $id = $_GET['viewCriteria'];
        $videoTranslation = new VideosTranslations();
        $responseData = $videoTranslation->findByPrimaryKey($id);
        // Did we find the requested model? If not produce error response to requester.
        if($responseData) {
            $this->sendResponse(200, $responseData);
        } else {
            $this->sendResponse(404, 'No Video Translations (Subtitle links) found with id: '. $id);
        }
    }

    /**
     * Manages all models.
     */
    public function actionAdmin(){
        $model=new VideosTranslations('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['VideosTranslations']))
            $model->attributes=$_GET['VideosTranslations'];
        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * Performs the AJAX validation.
     * @param Course $model the model to be validated
     */
    protected function performAjaxValidation($model){
        if(isset($_POST['ajax']) && $_POST['ajax']==='videostranslations-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex(){
        $videoTranslations = new VideosTranslations();
        if (!isset($_GET['indexCriteria1']) || $_GET['indexCriteria1'] == 0) {
            //$responseData = VideosTranslations::model()->findAll();
            $responseData = $videoTranslations->findAll();
        } else {
            $idSection = $_GET['indexCriteria1'];
            $locale = &$_GET['locale'];
            $responseData = $videoTranslations->findVideoTranslationsBySectionLocale($idSection, $locale);
        }
        if(empty($responseData)) {
            // No db results inform requester.
            $this->sendResponse(404, 'No Videos Translations found in Database');
        } else {
            // Prepare response
            $rows = array();
            foreach($responseData as $model)
                $rows[] = $model;
            // Send the response
            $this->sendResponse(200, $rows);
        }
    }
} 