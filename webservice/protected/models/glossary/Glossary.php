<?php
/**
 * This is the model class for tables "glo_<from>2<to>".
 *
 * The followings are the available columns in table 'glo_<from>2<to>':
 * @property integer $id
 * @property string $word
 * @property string $class
 * @property string $translation
 * @property string $audio
 */
class Glossary extends ApiCourseActiveRecord
{

    private $from = null;
    private $translateTo = null;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Glossary the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return Yii::app()->params['dbWebservice'].'.glo_'.$this->from.'2'.$this->translateTo;
    }

    /**
     * @return string the associated database table main fields
     */
    public function mainFields()
    {
        return 'id, word, class, translation, audio';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            /*array('name', 'required'),
            array('name', 'length', 'max'=>255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name', 'safe', 'on'=>'search'),*/
        );
    }

    /**
     * @param string $from
     * @param string $translateTo
     * @param string $word
     * @return array
     */
    public function search($from = 'english', $translateTo = 'spanish', $word = '')
    {
        $this->from = $from;
        $this->translateTo = $translateTo;

        if ($this->from === 'english') {
            $sourceField = 'word';
        } else {
            $sourceField = 'translation';
        }

        $responseData = array();
        $sql = " SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . "
            WHERE $sourceField LIKE '$word%';";

        $dataReader = $this->querySQL($sql, null, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        foreach ($rows as $row) {
            $responseData[] = ['id' => $row['id'],
            'word' => $row['word'],
            'class' => $row['class'],
            'translation' => $row['translation'],
            'audio' => $row['audio']];
        }

        return $responseData;
    }
}
