<?php
/**
 * AbaApiUserDevice
 * Author: mgadegaard
 * Date: 04/11/2014
 * Time: 16:36
 * © ABA English 2014
 */

class AbaApiUserDevice extends AbaActiveRecord {

    /** Compulsory for Yii framework
     * @return string
     */
    public function tableName()
{
    return 'user_devices';
}

    /** Compulsory for Yii framework
     * @return string
     */
    public function mainFields()
{
    return " ud.`id`, ud.`userId`, ud.`deviceName`, ud.`sysOper`, ud.`token` ";
}

    /**
     * @param $deviceId
     * @param $userId
     *
     * @return $this|bool
     */
    public function getDeviceByIdUserId($deviceId, $userId)
{
    $sql = " SELECT  " . $this->mainFields() . " FROM " . $this->tableName() .
        " ud WHERE ud.id=:DEVICEID AND  ud.`userId`=:USERID ";
    $paramsToSQL = array(":USERID" => $userId, ":DEVICEID" => $deviceId);

    $dataReader = $this->querySQL($sql, $paramsToSQL);
    if (($row = $dataReader->read()) !== false) {
        $this->fillDataColsToProperties($row);
        return $this;
    }

    $this->id = $deviceId;
    $this->userId = $userId;

    return false;
}

    /**
     * @param $userId
     *
     * @return $this|bool
     */
    public function getDeviceByUserId($userId)
{
    $sql = " SELECT  " . $this->mainFields() . " FROM " . $this->tableName() .
        " ud WHERE ud.`userId`=:USERID ORDER BY ud.dateLastModified DESC LIMIT 1";
    $paramsToSQL = array(":USERID" => $userId);
    $dataReader = $this->querySQL($sql, $paramsToSQL);
    if (($row = $dataReader->read()) !== false) {
        $this->fillDataColsToProperties($row);
        return $this;
    }
    return false;
}

    /** Get device based on the TOKEN of the Authentication
     * of mobile web services.
     * @param $token
     *
     * @return $this|bool
     */
    public function getDeviceByToken( $token )
{
    $sql = " SELECT  " . $this->mainFields() . " FROM " . $this->tableName() . " ud WHERE ud.token=:TOKEN ";
    $paramsToSQL = array(":TOKEN" => $token);

    $dataReader = $this->querySQL($sql, $paramsToSQL);
    if (($row = $dataReader->read()) !== false) {
        $this->fillDataColsToProperties($row);
        return $this;
    }

    return false;
}

    /** Inserts a new record into database. Returns true if successful.
     *  False whether it already exists or it was not inserted.
     *
     * @param $deviceId
     * @param $userId
     * @param $deviceName
     * @param $sysOper
     * @param $token
     *
     * @return bool
     */
    public function insertNewDevice($deviceId, $userId, $deviceName, $sysOper, $token)
{
    $sql = "INSERT INTO " . $this->tableName() .
        " ( id, userId, deviceName, sysOper, token )
            VALUES ( :ID, :USERID, :DEVICENAME, :SYSOPER, :TOKEN ) ";

    $params = array(":ID"      => $deviceId, ":USERID" => $userId, ":DEVICENAME" => $deviceName,
        ":SYSOPER" => $sysOper, ":TOKEN" => $token);
    $returnId = $this->executeSQL($sql, $params);
    if ($returnId !=false ) {
        $this->id = $deviceId;
        $this->userId = $userId ;
        $this->deviceName = $deviceName ;
        $this->sysOper = $sysOper;
        $this->token = $token ;
        return true;
    }


    return false;
}

    /** Updates 3 columns of an existing device. Never updates user nor device Id.
     * @return bool
     */
    public function updateDevice()
{
    $query = "UPDATE " . $this->tableName() . " SET deviceName=:DEVICENAME, sysOper=:SYSOPER, token=:TOKEN
                    WHERE userId=" . $this->userId . " AND id='" . $this->id . "' ";
    $params = array(":DEVICENAME" => $this->deviceName, ":SYSOPER" => $this->sysOper, ":TOKEN" => $this->token);

    if ($this->updateSQL($query, $params) > 0) {
        return true;
    } else {
        return false;
    }
}

    /** Updates the token column only.
     * @param string $token
     *
     * @return bool
     */
    public function updateToken($token)
{
    $query = "UPDATE " . $this->tableName() . " SET  token=:TOKEN
                    WHERE userId=" . $this->userId . " AND id='" . $this->id . "' ";
    $params = array( ":TOKEN" => $token );

    if ($this->updateSQL($query, $params) > 0) {
        $this->token = $token;
        return true;
    } else {
        return false;
    }
}

}