<!--?php
/**
 * AbaApiUser
 * Author: mgadegaard
 * Date: 04/11/2014
 * Time: 16:00
 * © ABA English 2014
 */

class AbaApiUser extends AbaActiveRecord {
    public $id; //primary key
    /**
     * @var AbaLogUserActivity
     */
    public $abaUserLogUserActivity;
    protected $errorMsg;
    protected $_md;

    // **********************--------------------------------------------------------------*****************************
    // **********************------------------------START OF YII NECESSARY FUNCTIONS------*****************************
    // **********************--------------------------------------------------------------*****************************
    /**
     *  Main constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user';
    }

    /**
     * Before the save method is executed.
     * @return array
     */
    public function rules()
    {
        return array(
            array('id',           'required'),
            array('name',         'required'),
            array('surnames',  	  'required'),
            array('email',  	  'email','allowEmpty'=>false),
            array('birthDate',    'date', 'format'=>'dd-mm-yyyy'),
            array('countryId',    'type', 'type'=>'integer', 'allowEmpty'=>true),
            array('city',         'type', 'type'=>'string',  'allowEmpty'=>true),
            array('telephone',    'type', 'type'=>'string',  'allowEmpty'=>true),
            array('currentLevel',    'type', 'type'=>'integer',  'allowEmpty'=>true),
//            array('langEnv',      "isValidLangEnv", 'on'=>'registerUser'),
        );
    }

    /** Customized validation in Yii format. Language should be one out of 6 available.
     * @param $attribute
     * @param $params
     * @noinspection PhpUnusedParameterInspection
     */
    public function isValidLangEnv( $attribute,  $params)
    {
        if( !array_key_exists($this->langEnv, Yii::app()->params['languages'])  )
        {
            $this->addError("langEnv", Yii::t('mainApp', 'langEnv_invalid_key'));
        }

    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    // **********************--------------------------------------------------------------*****************************
    // **********************-----------------END OF YII NECESSARY FUNCTIONS---------------*****************************
    // **********************--------------------------------------------------------------*****************************


    // **********************--------------------------------------------------------------*****************************
    // **********************--------------START OF ABA USER UTILITY FUNCTIONS-------------*****************************
    // **********************--------------------------------------------------------------*****************************

    /** All fields available in this model
     * @return string
     */
    public function mainFields()
    {
        return "u.`id`, u.`name`, u.`surnames`, u.`email`, u.`password` as `password`, u.`complete`, ".
        " u.`city`, u.`countryId`, u.`countryIdCustom`, ".
        "  DATE_FORMAT(u.birthDate,'%d-%m-%Y') as 'birthDate',".
        " u.`telephone`, u.`langEnv`, u.`langCourse`, u.`userType`, u.`currentLevel`, ".
        " DATE_FORMAT(u.entryDate,'%d-%m-%Y') as 'entryDate', ".
        " DATE_FORMAT(u.expirationDate,'%d-%m-%Y') as 'expirationDate', ".
        " u.`teacherId`, u.`idPartnerFirstPay`, u.`idPartnerSource`, u.`idPartnerCurrent`, ".
        " u.`savedUserObjective`, u.`firstLoginDone`, u.`keyExternalLogin`, u.`gender`, ".
        " u.`cancelReason`, u.`registerUserSource`, u.`hasWorkedCourse`, u.`dateLastSentSelligent`, ".
        " u.`followUpAtFirstPay`, u.`watchedVideosAtFirstPay`, u.`idSourceList`, u.`deviceTypeSource`, u.`dateUpdateEmma` ";
    }

    // **********************--------------------------------------------------------------*****************************
    // **********************---------------END OF ABA USER UTILITY FUNCTIONS--------------*****************************
    // **********************--------------------------------------------------------------*****************************

    // **********************--------------------------------------------------------------*****************************
    // **********************------------------ABA User customized functions---------------*****************************
    // **********************--------------------------------------------------------------*****************************
    /**
     * Initializes the object AbaUser based on email. Can be used to authenticate an user.
     * @param string $email
     * @param string $password
     * @param bool   $usePassword
     * @param bool $pwdEncrypted
     *
     * @return bool
     */
    public function getUserByEmail($email, $password="", $usePassword=false, $pwdEncrypted=false)
    {
        if(is_array($email)){
            $email = $email['email'];
        }

        $verifyPassword = ($usePassword == true) ? " AND (u.`password`='" . HeAbaSHA::_md5($password) .
            "' OR u.`password`=CONCAT('(',:PASSWORD,')') )" :
            "";
        $verifyPassword = ($usePassword && $pwdEncrypted) ? " AND (u.`password`='" . $password .
            "' OR u.`password`=CONCAT('(',:PASSWORD,')') )" :
            $verifyPassword;

        $sql= "SELECT ".$this->mainFields()." FROM ".$this->tableName()." u WHERE u.`email` = :EMAIL  $verifyPassword";
        $paramsToSQL = array(":EMAIL"=>$email);
        if($verifyPassword!==''){    $paramsToSQL[":PASSWORD"]=$password;   }
        $dataReader=$this->querySQL($sql,$paramsToSQL);
        if(($row = $dataReader->read())!==false)
        {
            $this->fillDataColsToProperties($row);
            $this->abaUserLogUserActivity = new AbaLogUserActivity($this);
            return true;
        }
        return false;
    }

} 