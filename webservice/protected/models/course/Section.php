<?php

/**
 * This is the model class for table "section".
 *
 * The followings are the available columns in table 'section':
 * @property integer     $id
 * @property integer     $idUnit
 * @property integer     $idSectionType
 * @property integer     $exercises
 *
 * The followings are the available model relations:
 * @property Phrase[]    $phrases
 * @property SectionType $sectionType
 * @property Unit        $unit
 */
class Section extends ApiCourseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return Section the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return Yii::app()->params['dbWebservice'] . '.section';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idUnit, idSectionType, exercises', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, idUnit, idSectionType, exercises', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'phrases'     => array(self::HAS_MANY, 'Phrase', 'idSection'),
            'sectionType' => array(self::BELONGS_TO, 'SectionType', 'idSectionType'),
            'unit'        => array(self::BELONGS_TO, 'Unit', 'idUnit'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'            => 'ID',
            'idUnit'        => 'Unit',
            'idSectionType' => 'Section Type',
            'exercises'     => 'Exercises',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->compare('idUnit', $this->idUnit);
        $criteria->compare('idSectionType', $this->idSectionType);
        $criteria->compare('exercises', $this->exercises);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Initializes the model with input parameters
     *
     * @param integer $idUnit
     * @param integer $idSectionType
     */
    public function initDefaults($idUnit, $idSectionType)
    {
        $this->idUnit = $idUnit;
        $this->idSectionType = $idSectionType;
        $this->id = 0;
    }

    /**
     * Function that will get a Section based on IDs of unit and Section Type.
     *
     * @param integer $idUnit
     * @param integer $idSectionType
     *
     * @return $this|bool|string
     *
     * @throws CDbException
     */
    public function getSectionIdByUnitIdSectionTypeId($idUnit = null, $idSectionType = null)
    {
        if (is_null($idUnit) || is_null($idSectionType)) {
            throw new CDbException(" To be able to load a Section a Unit Id and Section Type Id is a must. ");
        }
        $sql = "SELECT
                  s.id
                FROM
                  " . $this->tableName() . " s
                WHERE
                    s.idUnit=:idUnit_param
                AND
                    s.idSectionType=:idSectionType_param;";
        $params = array(":idUnit_param" => $idUnit, ":idSectionType_param" => $idSectionType);
        $dataReader = $this->querySQL($sql, $params, $this->selectDbConnection());
        if (($row = $dataReader->read()) !== false) {
            return $row['id'];
        }

        return false;
    }


    /**
     * Finds a Section based on PK and it's translation if locale is supplied (different than <en>) as input parameter.
     *
     * @param integer $idSectionType PK of Section in DB
     * @param string  $locale Supplied if translations is wanted
     *
     * @return array DB query result
     */
    public function findByPkTranslation($idSectionType, $locale)
    {
        if ($locale == '' || $locale == 'en') {
            $sql = 'SELECT s.id, st.name AS title
                FROM ' . $this->tableName() . ' s
                JOIN ' . $this->auxTableName('section_type') . ' st ON s.idSectionType = st.id
                WHERE s.id =:id_param';
            $params = array('id_param' => $idSectionType);
        } else {
            $sql = 'SELECT s.id, stt.content AS title
                FROM ' . $this->tableName() . ' s
                JOIN ' . $this->auxTableName('section_type') . ' st ON s.idSectionType = st.id
                LEFT JOIN ' . $this->auxTableName('section_type_translations') . ' stt ON stt.idSectionType = st.id
                WHERE s.id =:id_param
                AND stt.locale =:locale_param';
            $params = array('id_param' => $idSectionType, 'locale_param' => $locale);
        }
        $dataReader = $this->querySQL($sql, $params, $this->selectDbConnection());
        $responseData = $dataReader->readAll();
        return $responseData;
    }

    /**
     * Function finds all sections by given locale.
     *
     * @param string $locale
     *
     * @return array $responseData
     */
    public function findAllByLocale($locale = 'en')
    {
        $params = array();
        $responseData = array();
        if ($locale == 'en') {
            $sql = 'SELECT s.id, s.idUnit, s.idSectionType, st.name AS title
                    FROM ' . $this->tableName() . ' s
                    JOIN ' . $this->auxTableName('unit') . ' u ON s.idUnit = u.id
                    JOIN ' . $this->auxTableName('section_type') . ' st ON s.idSectionType = st.id';
            $dataReader = $this->querySQL($sql, $params);
            $rows = $dataReader->readAll();
        } else {
            $sql = 'SELECT s.id, s.idUnit, s.idSectionType, stt.content AS title
                    FROM ' . $this->tableName() . ' s
                    JOIN ' . $this->auxTableName('unit') . ' u ON s.idUnit = u.id
                    LEFT JOIN ' . $this->auxTableName('section_type') . ' st ON s.idSectionType = st.id
                    LEFT JOIN ' . $this->auxTableName('section_type_translations') . ' stt ON stt.idSectionType = st.id
                    WHERE stt.locale =:locale_param';
            $params = array(':locale_param' => $locale);
            $dataReader = $this->querySQL($sql, $params, $this->selectDbConnection());
            $rows = $dataReader->readAll();
        }
        foreach ($rows as $row) {
            $responseData[] = ['Section ID'      => $row['id'],
                               'Unit ID'         => $row['idUnit'],
                               'Section Type ID' => $row['idSectionType'],
                               'Title'           => $row['title']
            ];
        }
        return $responseData;
    }

    /**
     * Function finds all sections by given unit id, section type id and locale.
     *
     * @param integer $idUnit
     * @param integer $idSectionType
     * @param string  $locale
     *
     * @return array|bool $responseData
     */
    public function findAllByAttributesTranslated($idUnit = null, $idSectionType = null, $locale = 'en')
    {
        $responseData = array();
        if ($locale == 'en') {
            if ($idSectionType == 0) {
                $sql = 'SELECT s.id, st.name AS title, s.idUnit, s.idSectionType
                        FROM ' . $this->tableName() . ' s
                        JOIN ' . $this->auxTableName('unit') . ' u ON s.idUnit = u.id
                        LEFT JOIN ' . $this->auxTableName('section_type') . ' st ON s.idSectionType = st.id
                        WHERE s.idUnit =:idUnit_param';
                $params = array(':idUnit_param' => $idUnit);
            } else {
                $sql = 'SELECT s.id, st.name AS title, s.idUnit, s.idSectionType
                        FROM ' . $this->tableName() . ' s
                        JOIN ' . $this->auxTableName('unit') . ' u ON s.idUnit = u.id
                        LEFT JOIN ' . $this->auxTableName('section_type') . ' st ON s.idSectionType = st.id
                        WHERE s.idUnit =:idUnit_param
                        AND s.idSectionType =:idSectionType_param';
                $params = array(':idUnit_param'        => $idUnit,
                                ':idSectionType_param' => $idSectionType,
                                ':locale_param'        => $locale
                );
            }
        } else {
            if ($idSectionType == 0) {
                $sql = 'SELECT s.id, stt.content AS title, s.idUnit, s.idSectionType
                        FROM ' . $this->tableName() . ' s
                        JOIN ' . $this->auxTableName('unit') . ' u ON s.idUnit = u.id
                        LEFT JOIN ' . $this->auxTableName('section_type') . ' st ON s.idSectionType = st.id
                        LEFT JOIN ' . $this->auxTableName('section_type_translations') . ' stt
                            ON stt.idSectionType = st.id
                        WHERE s.idUnit =:idUnit_param
                        AND stt.locale =:locale_param';
                $params = array(':idUnit_param' => $idUnit, ':locale_param' => $locale);
            } else {
                $sql = 'SELECT s.id, stt.content AS title, s.idUnit, s.idSectionType
                        FROM ' . $this->tableName() . ' s
                        JOIN ' . $this->auxTableName('unit') . ' u ON s.idUnit = u.id
                        LEFT JOIN ' . $this->auxTableName('section_type') . ' st ON s.idSectionType = st.id
                        LEFT JOIN ' . $this->auxTableName('section_type_translations') . ' stt
                            ON stt.idSectionType = st.id
                        WHERE s.idUnit =:idUnit_param
                        AND stt.locale =:locale_param
                        AND s.idSectionType =:idSectionType_param';
                $params = array(':idUnit_param'        => $idUnit,
                                ':idSectionType_param' => $idSectionType,
                                ':locale_param'        => $locale
                );
            }
        }
        $dataReader = $this->querySQL($sql, $params, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        foreach ($rows as $row) {
            $responseData[] = ['Section ID'      => $row['id'],
                               'Unit ID'         => $row['idUnit'],
                               'Section Type ID' => $row['idSectionType'],
                               'Title'           => $row['title']
            ];
        }
        return $responseData;
    }

    /**
     * Auxiliary function that finds and returns section id based on unit and section type ids.
     *
     * @param integer $idUnit
     * @param integer $idSectionType
     *
     * @return bool|string $sectionID
     */
    public function findSectionIDByUnitSectionType($idUnit, $idSectionType)
    {
        $sql = 'SELECT *
                FROM ' . $this->tableName() . ' s
                WHERE s.idUnit=:idUnit_param
                AND s.idSectionType=:idSectionType_param';
        $params = array(':idUnit_param' => $idUnit, ':idSectionType_param' => $idSectionType);
        $dataReader = $this->querySQL($sql, $params, $this->selectDbConnection());
        $row = $dataReader->readAll();
        if (count($row) == 1) {
            $sectionID = $row[0]['id'];
            return $sectionID;
        } else {
            return false;
        }
    }
}
