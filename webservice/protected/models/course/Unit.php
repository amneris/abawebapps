<?php

/**
 * This is the model class for table "unit".
 *
 * The followings are the available columns in table 'unit':
 * @property integer            $id
 * @property string             $title
 * @property string             $description
 * @property integer            $idLevel
 * @property integer            $position
 *
 * The followings are the available model relations:
 * @property Section[]          $sections
 * @property Level              $level
 * @property UnitTranslations[] $unitTranslations
 */
class Unit extends ApiCourseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return Unit the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return Yii::app()->params['dbWebservice'] . '.unit';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, description, position', 'required'),
            array('idLevel, position', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, description, idLevel, position', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'sections'         => array(self::HAS_MANY, 'Section', 'idUnit'),
            'level'            => array(self::BELONGS_TO, 'Level', 'idLevel'),
            'unitTranslations' => array(self::HAS_MANY, 'UnitTranslations', 'idUnit'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'          => 'ID',
            'title'       => 'Title',
            'description' => 'Description',
            'idLevel'     => 'Level',
            'position'    => 'Position',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('idLevel', $this->idLevel);
        $criteria->compare('position', $this->position);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Finds a Unit based on PK.
     *
     * @param integer $idUnit
     *
     * @return array | bool $responseArray
     */
    public function findByPrimaryKey($idUnit)
    {
        $sql = "SELECT u.id, u.title, u.description, u.idLevel, u.position
                FROM " . $this->tableName() . " u
                WHERE u.id =:id_param";
        $params = array('id_param' => $idUnit);
        $dataReader = $this->querySQL($sql, $params, $this->selectDbConnection());
        $row = $dataReader->read();
        if ($row) {
            $responseArray[] = ['idUnit' => $row['id'], 'Title' => $row['title'], 'Description' => $row['description']];
            return $responseArray;
        } else {
            return false;
        }
    }

    /**
     * Finds a Unit based on PK and it's translation if locale is supplied as input parameter.
     *
     * @param $idUnit
     * @param $locale
     *
     * @return array | bool
     */
    public function findByPkTranslation($idUnit, $locale)
    {
        $sql = 'SELECT u.id, u.title, t.content AS description, u.idLevel, u.position
                FROM ' . $this->tableName() . ' u
                LEFT JOIN ' . $this->auxTableName('unit_translations') . ' t ON u.id = t.idUnit
                WHERE u.id =:id_param
                AND t.locale =:locale_param';
        $params = array('id_param' => $idUnit, 'locale_param' => $locale);
        $dataReader = $this->querySQL($sql, $params, $this->selectDbConnection());
        $row = $dataReader->read();
        if ($row) {
            $responseArray[] = ['idUnit' => $row['id'], 'Title' => $row['title'], 'Description' => $row['description']];
            return $responseArray;
        } else {
            return false;
        }
    }

    /**
     * Selects and returns all Units from DB.
     *
     * @return array|bool
     */
    public function findAll($condition='',$params=array())
    {
        $responseArray = array();
        $sql = 'SELECT u.id, u.title, u.description
                    FROM ' . $this->tableName() . ' u';
        $dataReader = $this->querySQL($sql, null, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        if ($rows) {
            foreach ($rows as $row) {
                $responseArray[] = ['idUnit'      => $row['id'],
                                    'Title'       => $row['title'],
                                    'Description' => $row['description']
                ];
            }
            return $responseArray;
        }
        return false;
    }

    /**
     * Function finds all Units by given locale.
     *
     * @param string $locale
     *
     * @return array|bool
     */
    public function findAllByLocale($locale)
    {
        $sql = 'SELECT u.id, u.title, trans2.trans AS description, u.idLevel, u.position
                    FROM ' . $this->tableName() . ' u
                    JOIN ' . $this->auxTableName('level') . ' l ON u.idLevel = l.id
                    JOIN (
                        SELECT t.idUnit AS trans_id, t.content AS trans
                        FROM ' . $this->auxTableName('unit_translations') . ' t
                        JOIN ' . $this->tableName() . ' u
                            ON t.idUnit = u.id
                        WHERE t.field = "description"
                        AND t.locale =:locale_param) AS trans2 ON u.id = trans2.trans_id';
        $params = array(':locale_param' => $locale);
        $dataReader = $this->querySQL($sql, $params, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        if ($rows) {
            foreach ($rows as $row) {
                $responseArray[] = ['idUnit'      => $row['id'],
                                    'Title'       => $row['title'],
                                    'Description' => $row['description']
                ];
            }
            return $responseArray;
        }
        return false;
    }

    /**
     * Retrieves an array of all Units for a given Course returned by the SQL statement.
     *
     * @param integer $idCourse
     * @param string  $locale
     *
     * @return array|bool Unit rows found.
     */
    public function findAllByCourse($idCourse, $locale = '')
    {
        if ($locale == '' || $locale == 'en') {
            $sql = 'SELECT u.*
                        FROM ' . $this->tableName() . ' u
                        JOIN ' . $this->auxTableName('level') . ' l ON u.idLevel = l.id
                        WHERE l.idCourse =:course_param';
            $params = array(':course_param' => $idCourse);
        } else {
            $sql = 'SELECT u.id, u.title, trans2.trans AS description, u.idLevel, u.position
                    FROM ' . $this->tableName() . ' u
                    JOIN ' . $this->auxTableName('level') . ' l ON u.idLevel = l.id
                    JOIN (
                        SELECT t.idUnit AS trans_id, t.content AS trans
                        FROM ' . $this->auxTableName('unit_translations') . ' t
                        JOIN ' . $this->tableName() . ' u
                            ON t.idUnit = u.id
                        WHERE t.field = "description"
                        AND t.locale =:locale_param) AS trans2 ON u.id = trans2.trans_id
                    WHERE l.idCourse =:course_param';
            $params = array(':course_param' => $idCourse, ':locale_param' => $locale);
        }
        $dataReader = $this->querySQL($sql, $params, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        if ($rows) {
            foreach ($rows as $row) {
                $responseArray[] = ['idUnit'      => $row['id'],
                                    'Title'       => $row['title'],
                                    'Description' => $row['description']
                ];
            }
            return $responseArray;
        }
        return false;
    }

    /**
     * Retrieves an array of all unit rows returned by the SQL statement
     *
     * @param int    $idCourse
     * @param        $idLevel
     * @param string $locale
     *
     * @return array of Unit rows found, Empty array is returned if SQL has no result set.
     */
    public function findAllByCourseLevel($idCourse, $idLevel, $locale = '')
    {
        $responseArray = array();
        if ($locale == 'en' || $locale == '') {
            $sql = 'SELECT u.*
                        FROM ' . $this->tableName() . ' u
                        JOIN ' . $this->auxTableName('level') . ' l ON u.idLevel = l.id
                        WHERE l.idCourse =:course_param
                        AND u.idLevel =:level_param';
            $params = array(':course_param' => $idCourse, 'level_param' => $idLevel);
        } else {
            $sql = 'SELECT u.id, u.title, trans2.trans AS description, u.idLevel, u.position
                    FROM ' . $this->tableName() . ' u
                    JOIN ' . $this->auxTableName('level') . ' l ON u.idLevel = l.id
                    JOIN (
                        SELECT t.idUnit AS trans_id, t.content AS trans
                        FROM ' . $this->auxTableName('unit_translations') . ' t
                        JOIN ' . $this->tableName() . ' u
                            ON t.idUnit = u.id
                        WHERE t.field = "description"
                        AND t.locale =:locale_param) AS trans2 ON u.id = trans2.trans_id
                    WHERE l.idCourse =:course_param
                    AND u.idLevel =:level_param';
            $params = array(':course_param' => $idCourse, 'level_param' => $idLevel, ':locale_param' => $locale);
        }
        $dataReader = $this->querySQL($sql, $params, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        if ($rows) {
            foreach ($rows as $row) {
                $responseArray[] = ['idUnit'      => $row['id'],
                                    'Title'       => $row['title'],
                                    'Description' => $row['description']
                ];
            }
            return $responseArray;
        }
        return false;
    }
}
