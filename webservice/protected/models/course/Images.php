<?php

/**
 * This is the model class for table "images".
 *
 * The followings are the available columns in table 'images':
 * @property integer $id
 * @property integer $unit
 * @property integer $type
 * @property integer $mode
 * @property string  $size
 * @property string  $imageProperty
 * @property string  $url
 */
class Images extends ApiCourseActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return Yii::app()->params['dbWebservice'] . '.images';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('unit, type, mode', 'numerical', 'integerOnly' => true),
            array('size', 'length', 'max' => 2),
            array('imageProperty', 'length', 'max' => 25),
            array('url', 'length', 'max' => 512),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, unit, type, mode, size, imageProperty, url', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'            => 'ID',
            'unit'          => 'Unit',
            'type'          => '1-UnitImage, 2-AbafilmImage, 3-VideoclassImage, 4-Role',
            'mode'          => '1-Active,2-Inactive',
            'size'          => 'X2, X3.',
            'imageProperty' => 'Rol name.',
            'url'           => 'Url',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('unit', $this->unit);
        $criteria->compare('type', $this->type);
        $criteria->compare('mode', $this->mode);
        $criteria->compare('size', $this->size, true);
        $criteria->compare('imageProperty', $this->imageProperty, true);
        $criteria->compare('url', $this->url, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * @return CDbConnection the database connection used for this class
     */
    public function getDbConnection()
    {
        return Yii::app()->dbWebservice;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return Images the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @param $idUnit
     * @param $size
     *
     * @return array|bool
     */
    public function getUnitImages($idUnit, $size = '2x')
    {
        if ($idUnit > 0 && $idUnit < 145) {
            $orderedRoleRows = array();
            $roleSmallSql = "SELECT imageProperty, url
                FROM " . $this->tableName() . "
				WHERE unit = :id_param
                AND type = 1
                AND MODE = 3
                AND size =:size_param";
            $params = array('id_param' => $idUnit, 'size_param' => $size);
            $dataReader = $this->querySQL($roleSmallSql, $params, $this->selectDbConnection());
            $roleSmallRows = $dataReader->readAll();
            foreach ($roleSmallRows as $roleSmallRow) {
                $orderedRoleRows[] = ['RoleName'          => $roleSmallRow['imageProperty'],
                                      'RoleSmallImageUrl' => $roleSmallRow['url']
                ];
            }
            $roleBigSql = "SELECT imageProperty, url
                FROM " . $this->tableName() . "
                WHERE unit = :id_param
                AND type = 1
                AND MODE = 4
                AND size =:size_param";
            $params = array('id_param' => $idUnit, 'size_param' => $size);
            $dataReader = $this->querySQL($roleBigSql, $params, $this->selectDbConnection());
            $roleBigRows = $dataReader->readAll();
            foreach ($roleBigRows as $roleBigRow) {
                $orderedRoleRows[] = ['RoleName'        => $roleBigRow['imageProperty'],
                                      'RoleBigImageUrl' => $roleBigRow['url']
                ];
            }

            $sql = "SELECT type, imageProperty, url, mode
                FROM " . $this->tableName() . "
                WHERE unit = :id_param
                AND type != 1
                AND size =:size_param";
            $params = array('id_param' => $idUnit, 'size_param' => $size);
            $dataReader = $this->querySQL($sql, $params, $this->selectDbConnection());
            $rows = $dataReader->readAll();
            $totalRows[] = ['Roles' => $orderedRoleRows];
            if ($rows) {
                foreach ($rows as $row) {
                    switch ($row['type']) {
                        case 2:
                            if ($row['mode'] == 1) {
                                $totalRows[] = ['AbaFilmImage' => $row['url']];
                            } else {
                                $totalRows[] = ['AbaFilmImageInactive' => $row['url']];
                            }
                            break;
                        case 3:
                            $totalRows[] = ['VideoClassImage' => $row['url']];
                            break;
                        case 4:
                            if ($row['mode'] == 1) {
                                $totalRows[] = ['UnitImage' => $row['url']];
                            } else {
                                $totalRows[] = ['UnitImageInactive' => $row['url']];
                            }
                            break;
                    }
                }
                return $totalRows;
            } else {
                return false;
            }
        }
        return false;
    }
}
