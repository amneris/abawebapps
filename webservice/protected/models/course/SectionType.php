<?php

/**
 * This is the model class for table "section_type".
 *
 * The followings are the available columns in table 'section_type':
 * @property integer                   $id
 * @property string                    $name
 * @property integer                   $position
 *
 * The followings are the available model relations:
 * @property Section[]                 $sections
 * @property SectionTypeTranslations[] $sectionTypeTranslations
 */
class SectionType extends ApiCourseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return SectionType the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return Yii::app()->params['dbWebservice'] . '.section_type';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, position', 'required'),
            array('position', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, position', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'sections'                => array(self::HAS_MANY, 'Section', 'idSectionType'),
            'sectionTypeTranslations' => array(self::HAS_MANY, 'SectionTypeTranslations', 'idSectionType'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'       => 'ID',
            'name'     => 'Name',
            'position' => 'Position',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('position', $this->position);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Finds a Section Type based on PK and it's translation if locale is supplied (different than <en>) as input
     * parameter.
     *
     * @param integer $id PK of Section Type
     * @param string  $locale
     *
     * @return array|bool $responseData
     */
    public function findByPkLocalized($id, $locale)
    {
        if ($locale == '' || $locale == 'en') {
            $sql = 'SELECT st.id, st.name, st.position
                FROM ' . $this->tableName() . ' st
                WHERE st.id =:id_param';
            $params = array('id_param' => $id);
        } else {
            $sql = 'SELECT st.id, stt.content AS name, st.position
                FROM ' . $this->tableName() . ' st
                LEFT JOIN ' . $this->auxTableName('section_type_translations') . ' stt ON stt.idSectionType = st.id
                WHERE st.id =:id_param
                AND stt.locale =:locale_param';
            $params = array('id_param' => $id, 'locale_param' => $locale);
        }
        $dataReader = $this->querySQL($sql, $params, $this->selectDbConnection());
        $responseData = $dataReader->readAll();
        return $responseData;
    }

    /**
     * Function finds all Section Types by given locale.
     *
     * @param $locale
     *
     * @return array|bool|string
     */
    public function findAllLocalized($locale)
    {
        $sql = 'SELECT st.id, stt.content AS name, st.position
                FROM ' . $this->tableName() . ' st
                LEFT JOIN ' . $this->auxTableName('section_type_translations') . ' stt ON stt.idSectionType = st.id
                WHERE stt.locale =:locale_param';
        $params = array('locale_param' => $locale);
        $dataReader = $this->querySQL($sql, $params, $this->selectDbConnection());
        $responseData = $dataReader->readAll();
        return $responseData;
    }
}
