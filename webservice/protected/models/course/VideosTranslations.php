<?php
/**
 * This is the model class for table "videos_translations"
 * Author: mgadegaard
 * Date: 28/10/2014
 * Time: 12:35
 * © ABA English 2014
 *
 * The followings are the available columns in the table:
 * @property integer $id
 * @property integer $idVideos
 * @property string $locale
 * @property string $url
 *
 * The following are the available model relations:
 * @property Section $section
 */

class VideosTranslations extends AbaActiveRecord {
    /**
     * Returns the static model of the specified ActiveRecord(ApiCourseActiveRecord) class.
     * @param string $className active record class name.
     * @return VideosTranslations the static model class
     */
    public static function model($className=__CLASS__){
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName(){
        return Yii::app()->params['dbWebservice'].'.videos_translations';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules(){
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idSectionVideos, locale', 'required'),
            array('idSectionVideos', 'numerical', 'integerOnly'=>true),
            array('locale', 'length', 'max'=>2),
            array('url', 'length', 'max'=>512),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, idSectionVideos, locale, url', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations(){
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'videos' => array(self::BELONGS_TO, 'Videos', 'id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels(){
        return array(
            'id' => 'ID',
            'idSectionVideos' => 'Videos',
            'locale' => 'Locale',
            'url' => 'URL',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search(){
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria=new CDbCriteria;
        $criteria->compare('id',$this->id);
        $criteria->compare('idSectionVideos',$this->idSection);
        $criteria->compare('locale',$this->locale);
        $criteria->compare('url',$this->url);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns row from videos_translated identified by id (PK)
     *
     * @param integer $id primary key in table
     *
     * @return array|bool $responseData Either return an array with relevant data or false if bo row exists
     */
    public function findByPrimaryKey($id){
        $responseData = array();
        $sql = "SELECT *
                FROM ". $this->tableName() ." vt
                WHERE vt.id = $id;";
        $dataReader = $this->querySQL($sql, null, $this->selectDbConnection());
        $row = $dataReader->read();
        if($row) {
            $responseData[] = ['idVideo'=>$row['idSectionVideos'], 'Locale'=>$row['locale'], 'URL'=>$row['url']];
            return $responseData;
        }
        return false;
    }

    /**
     * Finds and returns all Video Translations
     *
     * @return array|bool $responseData
     */
    public function findAll(){
        $responseData = array();
        $sql = "SELECT *
                FROM ". $this->tableName() ." vt";
        $dataReader = $this->querySQL($sql, null, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        if($rows) {
            foreach($rows as $row) {
                $responseData[] = ['idVideo'=>$row['idSectionVideos'], 'Locale'=>$row['locale'], 'URL'=>$row['url']];
            }
            return $responseData;
        }
        return false;
    }

    /**
     * Function used to find subtitles for ABAFilms and Video classes
     *
     * @param integer $idSectionVideos Video ID
     * @param string $locale ISO 3166-1 alpha-2 format country code to identify subtitle language
     *
     * @return array $responseData Array of subtitles for given Video ID
     */
    public function findVideoTranslationsBySectionLocale($idSectionVideos, $locale){
        $responseData = array();
        $sql = 'SELECT *
                FROM '. $this->tableName() .' vt
                WHERE vt.idSectionVideos =:id_videos_param
                AND (vt.locale = "en" OR vt.locale=:locale_param)';
        $params = array('id_videos_param'=>$idSectionVideos, 'locale_param'=>$locale);
        $dataReader = $this->querySQL($sql, $params, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        foreach ($rows as $row) {
            $responseData[] = ["Locale"=>$row["locale"], "SubtitleURL"=>$row["url"]];
        }
        return $responseData;
    }
}
