<?php

/**
 * This is the model class for table "section_items".
 *
 * The followings are the available columns in table 'section_items':
 * @property integer $id
 * @property integer $idSection
 * @property string  $locale
 * @property string  $idTest
 * @property integer $exercises
 */
class SectionItems extends ApiCourseActiveRecord
{
    /**
     * Returns the static model of the specified ActiveRecord(ApiCourseActiveRecord) class.
     *
     * @param string $className active record class name.
     *
     * @return Videos the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return Yii::app()->params['dbWebservice'] . '.section_items';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idSection, exercises', 'numerical', 'integerOnly' => true),
            array('locale', 'length', 'max' => 2),
            array('idTest', 'length', 'max' => 11),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, idSection, locale, idTest, exercises', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'        => 'ID',
            'idSection' => 'Id Section',
            'locale'    => 'Language',
            'idTest'    => 'Id Test',
            'exercises' => 'Number of exercises new system',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('idSection', $this->idSection);
        $criteria->compare('locale', $this->locale, true);
        $criteria->compare('idTest', $this->idTest, true);
        $criteria->compare('exercises', $this->exercises);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * @return CDbConnection the database connection used for this class
     */
    public function getDbConnection()
    {
        return Yii::app()->dbWebservice;
    }

    /**
     * Function finds number of exercises of a section under the specified locale.
     *
     * @param integer $idSection
     * @param string  $locale
     * @param int     $idTest
     *
     * @return bool|string $response/$cachedResponse
     */
    public function findExercises($idSection, $locale, $idTest = 0)
    {

        $sql = 'SELECT exercises
                FROM ' . $this->tableName() . ' s
                WHERE s.idSection=:idSection_param
                AND s.locale=:locale_param
                AND s.idTest=:idTest_param LIMIT 1';
        $params = array(':idSection_param' => $idSection, ':locale_param' => $locale, ':idTest_param' => $idTest);
        $dataReader = $this->querySQL($sql, $params);
        $row = $dataReader->read();
        if ($row) {
            $response = $row['exercises'];
            return $response;
        }
    }
}

