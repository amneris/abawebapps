<?php

/**
 * This is the model class for table "help".
 *
 * The followings are the available columns in table 'help':
 * @property integer $id
 * @property integer $idHelp
 * @property string  $locale
 * @property string  $title
 * @property string  $content
 */
class Help extends ApiCourseActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return Yii::app()->params['dbWebservice'] . '.help';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, content', 'required'),
            array('idHelp', 'numerical', 'integerOnly' => true),
            array('locale', 'length', 'max' => 10),
            array('title', 'length', 'max' => 500),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, idHelp, locale, title, content', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'      => 'Id/PK',
            'idHelp'  => 'Help item identifier',
            'locale'  => 'Locale of help item',
            'title'   => 'Title of help item',
            'content' => 'Help item content',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // Please modify the following code to remove attributes that should not be searched.
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->compare('idHelp', $this->idHelp);
        $criteria->compare('locale', $this->locale, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('content', $this->content, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * @return CDbConnection the database connection used for this class
     */
    public function getDbConnection()
    {
        return Yii::app()->dbWebservice;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return Help the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Function returns all help items for input locale.
     *
     * @param $locale
     *
     * @return array|bool|string
     */
    public function getLocalizedHelpContent($locale)
    {
        $responseData = array('helps' => array());
        $sql = 'SELECT
                    h.idHelp, h.title, h.content
                FROM
                    ' . $this->tableName() . ' h
                WHERE
                    h.locale =:locale_param';
        $param = ['locale_param' => $locale];
        $dataReader = $this->querySQL($sql, $param, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        foreach ($rows as $row) {
            switch ($row['title']) {
                case 'email':
                    $responseData['helps'][] = ["email" => $row["content"]];
                    break;
                default:
                    $responseData['helps'][] = [
                        "id"          => $row["idHelp"],
                        "title"       => $row["title"],
                        "description" => $row["content"]
                    ];
                    break;
            }
        }

        return $responseData;
    }
}
