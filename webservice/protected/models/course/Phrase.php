<?php
/**
 * This is the model class for table "phrase".
 *
 * The followings are the available columns in table 'phrase':
 * @property integer $id
 * @property integer $idSection
 * @property integer $parent
 * @property string $text
 * @property string $audio
 * @property string $role
 * @property string $phraseType
 * @property string $wordType
 * @property string $blank
 * @property integer $position
 * @property integer $page
 * @property integer $line
 *
 * The following are the available model relations:
 * @property Phrase $parent0
 * @property Phrase[] $phrases
 * @property Section $section
 * @property PhraseTranslations[] $phraseTranslations
 */
class Phrase extends ApiCourseActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Phrase the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return Yii::app()->params['dbWebservice'].'.phrase';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('text, audio, phraseType, position, page', 'required'),
            array('idSection, parent, position, page, line', 'numerical', 'integerOnly'=>true),
            array('text, audio, role, phraseType, blank', 'length', 'max'=>255),
            array('wordType', 'length', 'max'=>45),
            array('id, idSection, parent, text, audio, role, phraseType, wordType, blank, position, page, line',
                'safe',
                'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'parent0' => array(self::BELONGS_TO, 'Phrase', 'parent'),
            'phrases' => array(self::HAS_MANY, 'Phrase', 'parent'),
            'section' => array(self::BELONGS_TO, 'Section', 'idSection'),
            'phraseTranslations' => array(self::HAS_MANY, 'PhraseTranslations', 'idPhrase'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'idSection' => 'Section',
            'parent' => 'Parent',
            'text' => 'Text',
            'audio' => 'Audio',
            'role' => 'Role',
            'phraseType' => 'Phrase Type',
            'wordType' => 'Word Type',
            'blank' => 'Blank',
            'position' => 'Position',
            'page' => 'Page',
            'line' => 'Line',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->compare('idSection', $this->idSection);
        $criteria->compare('parent', $this->parent);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('audio', $this->audio, true);
        $criteria->compare('role', $this->role, true);
        $criteria->compare('phraseType', $this->phraseType, true);
        $criteria->compare('wordType', $this->wordType, true);
        $criteria->compare('blank', $this->blank, true);
        $criteria->compare('position', $this->position);
        $criteria->compare('page', $this->page);
        $criteria->compare('line', $this->line);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Function responsible for finding all phrases belonging to the given input parameters. It also formats an response
     * in the form of an array of JSON objects. DB config (MASTER_SLAVE_SELECTOR) will control whether data
     * is read from Master or Slave database.
     *
     * @param integer $idUnit
     * @param integer $idSectionType
     * @param integer $idSection
     * @param string $locale
     * @param integer $idPage (Currently not used in live code)
     *
     * @return array containing formatted JSON objects
     */
    public function findAllBySectionPage($idUnit, $idSectionType, $idSection, $locale, $idPage = 0)
    {
        $responseData = array();
        $parentKey = 0;
        $index = 0;
        $selectedLocale = HeApi::localeSelector($idUnit, $locale);
        switch ($idSectionType) {
            // ABA Film (Return URL to Video)
            case 1:
                // ABA FILMS aren't considered phrases. To be found through VideosController/Videos or ContentController
                break;
            // Speak / Habla
            case 2:
                $rows = $this->findAllSpeakPhrasesByIdSectionLocale($idSection, $selectedLocale);
                foreach ($rows as $key => $row) {
                    $parent = $row['parent'];
                    $id = $row['id'];
                    $phraseType = $row['phraseType'];
                    $line = $row['line'];
                    $translation = is_null($row["content"]) ? null : $row["content"];
                    if ($id != $parent) {
                        if ($phraseType == 'sample') {
                            if ($line != 0) {
                                if ($id != $line) {
                                    $responseData[$parentKey][$phraseType][$index][] = array('text'=>$row['text'],
                                        'audio'=>$row['audio'], 'translation'=>$row['content'], 'page'=>$row['page']);
                                } else {
                                    $responseData[$parentKey][$phraseType][][] = array('text'=>$row['text'],
                                        'audio'=>$row['audio'], 'translation'=>$row['content'], 'page'=>$row['page']);
                                    $index = count($responseData[$parentKey][$phraseType]) - 1;
                                }
                            } else {
                                $responseData[$parentKey][$phraseType][] = array('text'=>$row['text'],
                                    'audio'=>$row['audio'], 'translation'=>$row['content'], 'page'=>$row['page']);
                            }
                        } else { // phraseType = 'dialog'
                            if (is_null($translation)) {
                                $responseData[$parentKey][$phraseType][] = (array('text'=>$row['text'],
                                    'audio'=>$row['audio'], 'page'=>$row['page']));
                            } else {
                                $responseData[$parentKey][$phraseType][] = (array('text'=>$row['text'],
                                    'audio'=>$row['audio'], 'page'=>$row['page']));
                                $responseData[$parentKey]['translation'] = $row['content'];
                            }
                        }
                    } else {
                        $role =  $row['role'];
                        if (is_null($translation)) {
                            $responseData[$key] = array('Role'=>$role, $phraseType=>array(array('text'=>$row['text'],
                                'audio'=>$row['audio'], 'page'=>$row['page'])));
                        } else {
                            $responseData[$key] = array('Role'=>$role, $phraseType=>array(array('text'=>$row['text'],
                                'audio'=>$row['audio'], 'page'=>$row['page'])), 'translation'=>[$row['content']]);
                        }
                        $parentKey = $key;
                    }
                }
                return $responseData;
                break;
            // Write
            case 3:
                $rows = $this->findAllPhrasesByIdSection($idSection, $idPage);
                foreach ($rows as $key => $row) {
                    $idRow = $row["id"];
                    $parentRowId = $row["parent"];
                    if ($idRow == $parentRowId) {
                        $parentKey = $key;
                        $responseData[$key] = array('Role'=>$row['role'], $row['phraseType']=>array(array(
                            'text'=>$row['text'], 'audio'=>$row['audio'], 'page'=>$row['page'])));
                    } else {
                        $responseData[$parentKey][$row['phraseType']][] = (array('text'=>$row['text'],
                            'audio'=>$row['audio'], 'page'=>$row['page']));
                    }
                }
                return $responseData;
                break;
            // Interpret
            case 4:
                $rows = $this->findAllPhrasesByIdSection($idSection, $idPage);
                foreach ($rows as $key => $row) {
                    $responseData[$key] = array('Role'=>$row['role'], $row['phraseType']=>array(array(
                        'text'=>$row['text'], 'audio'=>$row['audio'], 'page'=>$row['page'])));
                }
                return $responseData;
                break;
            // Video Class
            case 5:
                break;
            // Exercises
            case 6:
                $rows = $this->findAllExercisePhrasesByIdSectionLocale($idSection, $selectedLocale);
                $index2 = 0;
                foreach ($rows as $key => $row) {
                    $phraseType = $row['phraseType'];
                    $translation = $row['translation'];
                    if ($phraseType == 'title') {
                        $responseData[$key] = array('title' => array(array('translation' => $row['translation'])));
                        $parentKey = $key;
                    } elseif ($phraseType == 'exercise') {
                        if ($row['audio'] == '') {
                            $parentOutput =array(array('text' => $row['text']));
                            $output = array('text' => $row['text']);
                        } else {
                            if ($row['posAudio'] > 0) {
                                $parentOutput = array(array('text' => $row['text'], 'audio' => $row['audio'],
                                    'posAudio' => $row['posAudio'], 'blank' => $row['blank'], 'page'=>$row['page']));
                                $output = array('text' => $row['text'], 'audio' => $row['audio'],
                                    'posAudio' => $row['posAudio'], 'blank' => $row['blank'], 'page'=>$row['page']);
                            } else {
                                $parentOutput = array(array('text' => $row['text'], 'audio' => $row['audio'],
                                    'blank' => $row['blank'], 'page'=>$row['page']));
                                $output = array('text' => $row['text'], 'audio' => $row['audio'],
                                    'blank' => $row['blank'], 'page'=>$row['page']);
                            }
                        }
                        if ($row['line'] == $row['id']) {
                            if (is_null($translation)) {
                                $responseData[$parentKey][$phraseType][][] = $parentOutput;
                            } else {
                                $responseData[$parentKey][$phraseType][][] = $parentOutput;
                                $index3 = count($responseData[$parentKey][$phraseType]) - 1;
                                $responseData[$parentKey][$phraseType][$index3][] = array(
                                    array('translation' => $row['translation']));
                            }
                            $index = count($responseData[$parentKey][$phraseType]) - 1;
                            $index2 = count($responseData[$parentKey][$phraseType][$index]) - 1;
                        } else {
                            if (is_null($translation)) {
                                $responseData[$parentKey][$phraseType][$index][$index2][] = $output;
                            } else {
                                $responseData[$parentKey][$phraseType][$index][$index2][] = $output;
                                $responseData[$parentKey][$phraseType][$index][] = array(
                                    'translation' => $row['translation']);
                            }
                            $index = count($responseData[$parentKey][$phraseType]) - 1;
                            $index2 = count($responseData[$parentKey][$phraseType][$index]) - 1;
                        }
                    }
                }
                return $responseData;
                break;
            // Vocabulary
            case 7:
                $rows = $this->findAllPhrasesTranslationsByIdSectionLocale($idSection, $selectedLocale);
                foreach ($rows as $key => $row) {
                    $responseData[] = array('text' => $row['text'], 'audio' => $row['audio'],
                        'wordType' => $row['wordType'], 'translation' => $row['translation'], 'page' => $row['page']);
                }
                return $responseData;
                break;
            // Evaluation
            case 8:
                break;
            default:
                return $responseData;
        }
        return $responseData;
    }

    /**
     * Function that finds all SPEAK phrases with related translations for given Section ID.
     *
     * @param int $idSection
     * @param string $locale
     * @return array DB rows
     */
    public function findAllSpeakPhrasesByIdSectionLocale($idSection, $locale)
    {
        $testClass = $this->findTestClass(64, $locale);
        $tableName = HeApi::tableNameSetter($this->tableName(), $locale);
        $auxTableName = HeApi::tableNameSetter(HeApi::auxTableName('phrase_translations'), $locale);
        if ($testClass == 0) {
            $innerJoin = $andTest = '';
        } else {
            $innerJoin = 'INNER JOIN aba_course.course_test_phrase t ON p.id=t.idPhrase AND p.idSection= t.idSection AND t.locale=:locale_param';
            $andTest = 'and t.idTest= ' . $testClass;
        }
        $sql = 'SELECT p.id, p.parent, p.text, p.audio, p.role, p.phraseType, p.position, p.page, p.line, pt.content
                    FROM ' . $tableName . ' p
                    LEFT JOIN
                      (SELECT pt.idPhrase AS idPhrase, group_concat(pt.content SEPARATOR \' \') AS content
                        FROM ' . $tableName . ' p, ' . $auxTableName . ' pt
                        WHERE p.idSection =:idSection_param
                        AND p.id = pt.idPhrase
                        AND pt.locale =:locale_param
                        GROUP BY p.id
                        ORDER BY p.page, p.position) AS pt
                    ON p.id = pt.idPhrase
                    ' . $innerJoin . '
                    WHERE p.idSection =:idSection_param ' . $andTest . '
                    ORDER BY p.page, p.position';
        $params = array(':idSection_param' => $idSection, ':locale_param' => $locale);
        $dataReader = $this->querySQL($sql, $params, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        return $rows;
    }

    /**
     * Function that finds all exercises phrases with related translations for given Section ID.
     * If page parameter is suppplied only phrases belonging to that page will be returned.
     *
     * @param int $idSection
     * @param string $locale
     * @param int $page
     * @return array DB rows
     */
    public function findAllExercisePhrasesByIdSectionLocale($idSection, $locale, $page = 0)
    {
        $tableName = HeApi::tableNameSetter($this->tableName(), $locale);
        $auxTableName = HeApi::tableNameSetter(HeApi::auxTableName('phrase_translations'), $locale);
        $andPage = $page > 0 ? ' AND p.page = ' . $page : '';
        $sql = 'SELECT
                p.id, p.text, p.posAudio, p.audio, p.blank, p.phraseType, p.page, p.line, pt.translation, p.position
                FROM '. $tableName .' p
                LEFT JOIN (SELECT pt.idPhrase AS idPhrase, pt.content as translation
                           FROM '. $tableName .' p, '. $auxTableName .' pt
                           WHERE p.idSection =:idSection_param
                           AND p.id = pt.idPhrase
                           AND pt.locale =:locale_param) AS pt
                ON p.id = pt.idPhrase
                WHERE p.idSection=:idSection_param
                ' . $andPage . '
                ORDER BY p.page, p.position';
        $params = array(':idSection_param' => $idSection, ':locale_param' => $locale);
        $dataReader = $this->querySQL($sql, $params, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        return $rows;
    }

    /**
     * Function that finds all phrases for given Section ID. If page parameter is suppplied only phrases belonging to
     * that page will be returned.
     *
     * @param int $idSection
     * @param int $page
     * @return array DB rows
     */
    public function findAllPhrasesByIdSection($idSection, $page = 0)
    {
        $andPage = $page > 0 ? "AND p.page = " . $page : "";
        $sql = "SELECT
                  p.id, p.role, p.text, p.audio, p.page, p.phraseType, p.parent
                FROM
                    " .$this->tableName() ." p
                WHERE
                    p.idSection =:idSection_param ". $andPage ."
                ORDER BY p.id";
        $params = array(':idSection_param' => $idSection);
        $dataReader = $this->querySQL($sql, $params, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        return $rows;
    }

    /**
     * Function to find all phrases and related translations based on Section Id and locale. A page parameter can be
     * included if all "pages" are not wanted.
     *
     * @param integer $idSection
     * @param string $locale
     * @param int $page (optional)
     * @return array DB rows
     */
    public function findAllPhrasesTranslationsByIdSectionLocale($idSection, $locale, $page = 0)
    {
        $testClass = $this->findTestClass(2, $locale);
        $andPage = $page > 0 ? " AND p.page = " . $page : "";
        //We include $testClass == 1 because 1=100% then is useless filter
        if($testClass == 0 || $testClass == 1) {
            $innerJoin = $andTest = '';
        } else {
            // I have hardcoded "tp.locale='es'" because all language vocaburary is joined togheder and with just 'es' filter is enough for all
            $innerJoin = " INNER JOIN aba_course.course_test_phrase tp ON p.id=tp.idPhrase AND p.idSection= tp.idSection AND tp.locale='es' ";
            $andTest = " and tp.idTest= " . $testClass;
        }
        $sql = "SELECT p.id, p.line, p.phraseType, p.text, p.audio, p.wordType, t.content AS translation, p.page
                FROM " . $this->tableName() . " p
                LEFT JOIN " . $this->auxTableName('phrase_translations') .
                          " t ON p.id = t.idPhrase AND t.locale =:locale_param " . $innerJoin . "
                WHERE p.idSection =:idSection_param " . $andTest . $andPage . "
                ORDER BY p.id";
        $params = array(':idSection_param' => $idSection, ':locale_param' => $locale);
        $dataReader = $this->querySQL($sql, $params, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        return $rows;
    }

    /**
     * Function used to identify if user belongs to a A/B tested segment and if so what class of Test
     * (what content subset)
     *
     * @param int $sectionBinInDecimal This value is a decimal representation of a binary number that represents
     *                                 what sections the test class is affecting. Each digit represents a section so
     *                                 00000000 means none and 0100000 means the Speak(second from the left on campus
     *                                 web) section is affected etc.
     * @param string $locale The localization
     * @return int|mixed|null
     */
    private function findTestClass($sectionBinInDecimal, $locale)
    {
        $model = CmAbaCourseTestUser::model()->find('userId=' . Yii::app()->user->id);
        $CmAbaCourseTest = new CmAbaCourseTest();
        if($model) {
            $numTest = $model->courseId;
            $Section = $CmAbaCourseTest->retrieveCourse($numTest,$locale);
            if(($Section & $sectionBinInDecimal) == 0 ) {
                $numTest = 0;
            }
        } else {
            $numTest = 0;
        }
        return $numTest;
    }
}
