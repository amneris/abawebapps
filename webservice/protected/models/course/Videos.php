<?php
/**
 * This is the model class for table "videos"
 * Author: mgadegaard
 * Date: 28/10/2014
 * Time: 12:12
 * © ABA English 2014
 *
 * The followings are the available columns in the table:
 * @property integer $id
 * @property integer $provider
 * @property integer $idSection
 * @property string $urlHD
 * @property string $urlSD
 * @property string $mobileSD
 * @property string $preview
 *
 * The following are the available model relations:
 * @property Section $section
 */


class Videos extends ApiCourseActiveRecord {

    /**
     * Returns the static model of the specified ActiveRecord(ApiCourseActiveRecord) class.
     * @param string $className active record class name.
     * @return Videos the static model class
     */
    public static function model($className=__CLASS__){
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName(){
        return Yii::app()->params['dbWebservice'].'.videos';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules(){
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('provider, idSection, urlHD, urlSD, mobileSD, preview', 'required'),
            array('provider, idSection', 'numerical', 'integerOnly'=>true),
            array('urlHD, urlSD, mobileSD, preview', 'length', 'max'=>512),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, idSection, urlHD, urlSD, mobileSD, preview', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations(){
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'section' => array(self::BELONGS_TO, 'Section', 'idSection'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels(){
        return array(
            'id' => 'ID',
            'provider' => 'Provider',
            'idSection' => 'Section',
            'urlHD' => 'HD URL',
            'urlSD' => 'SD URL',
            'mobileSD' => 'Mobile SD',
            'preview' => 'Preview',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search(){
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria=new CDbCriteria;
        $criteria->compare('id',$this->id);
        $criteria->compare('provider',$this->provider);
        $criteria->compare('idSection',$this->idSection);
        $criteria->compare('urlHD',$this->urlHD);
        $criteria->compare('urlSD',$this->urlSD);
        $criteria->compare('mobileSD',$this->mobileSD);
        $criteria->compare('preview',$this->preview);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Finds a Video based on PK.
     * Checks memcache and returns value if such exists.
     *
     * @param $id
     * @return array|bool|string
     */
    public function findByPrimaryKey($id){
        $api_cache_id = 'videos_cache_' . $id;
        $cachedCourseData = $this->getCachedData($api_cache_id);
        if($cachedCourseData) {
            return $cachedCourseData;
        }
        $responseData = array();
        $sql = "SELECT *
                FROM ". $this->tableName() ." v
                WHERE v.id = $id;";
        $dataReader = $this->querySQL($sql, null, $this->selectDbConnection());
        $row = $dataReader->read();
        if($row) {
            $responseData[] = ["idSection"=>$row['idSection'], "HD Video URL"=>$row["urlSD"]/*$row["urlHD"]*/, /*"SD Video URL"=>$row["urlSD"]*/ "SD Video URL"=>$row["mobileSD"]];
            $this->setCacheData($api_cache_id, $responseData);
            return $responseData;
        }
        return false;
    }

    /**
     * Finds Videos belonging to Section identified by input parameter $idSection and returns it with appropriate
     * subtitles.
     * Checks memcache and returns value if such exists.
     *
     * @param integer $idSection
     * @param string $locale
     * @return array|bool $responseData
     */
    public function findVideosBySection($idSection, $locale){

        If ($locale == 'zh') {
            $provider = 1;
        } else {
            $provider = 0;
        }

        $responseData = array();
        $sql = 'SELECT v.urlHD, v.urlSD, v.mobileSD, v.preview, vt.url as subt_en, trans.subt as subt_locale
                FROM '. $this->tableName() .' v
                JOIN '. $this->auxTableName('videos_translations') .' vt
                ON v.idSection = vt.idSectionVideos
                JOIN (SELECT vt.idSectionVideos AS idVideos, vt.url AS subt
                      FROM '. $this->tableName() .' v
                      JOIN '. $this->auxTableName('videos_translations') .' vt
                      ON v.idSection = vt.idSectionVideos
                      WHERE v.idSection =:id_section_param
        AND v.provider=0
        AND vt.locale =:locale_param) AS trans ON v.idSection = trans.idVideos
                WHERE v.idSection =:id_section_param
        AND vt.locale=:locale_en_param
        AND v.provider=:provider_param';

        $params = array('id_section_param'=> $idSection, ':locale_en_param' => 'en', ':locale_param'=> $locale, ':provider_param'=> $provider);
        $dataReader = $this->querySQL($sql, $params, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        foreach ($rows as $row) {
            $responseData[] = ["HD Video URL"=>$row["urlHD"], "SD Video URL"=>$row["urlSD"], "Mobile SD Video URL"=>$row["mobileSD"], "English Subtitles"=>$row['subt_en'], "Translated Subtitles"=>$row['subt_locale']];
        }
        return $responseData;
    }
}