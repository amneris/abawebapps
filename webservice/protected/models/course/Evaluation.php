<?php

/**
 * This is the model class for table "evaluation".
 * Author: mgadegaard
 * Date: 27/10/2014
 * Time: 16:07
 * © ABA English 2014
 *
 * * The followings are the available columns in table 'level':
 * @property integer $id
 * @property integer $idSection
 * @property integer $order
 * @property string  $question
 * @property string  $optiona
 * @property string  $optionb
 * @property string  $optionc
 * @property string  $answer
 *
 * The followings are the available model relations:
 * @property Section $section
 */
class Evaluation extends ApiCourseActiveRecord
{

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return Evaluation the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return Yii::app()->params['dbWebservice'] . '.evaluation';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idSection, order', 'required'),
            array('idSection, mt_level', 'numerical', 'integerOnly' => true),
            array('question, optiona, optionb, optionc', 'length', 'max' => 145),
            array('answer', 'length', 'max' => 1),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, idSection, order, question, optiona, optionb, optionc, answer', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'section' => array(self::BELONGS_TO, 'Section', 'idSection'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'        => 'ID',
            'idSection' => 'Section',
            'order'     => 'Order',
            'question'  => 'Question',
            'optiona'   => 'Option A',
            'optionb'   => 'Option B',
            'optionc'   => 'Option C',
            'answer'    => 'Answer',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('idSection', $this->idSection);
        $criteria->compare('order', $this->order);
        $criteria->compare('question', $this->question);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Looks up and returns all evaluation questions and answers for a Unit
     *
     * @param Integer $idUnit Identifier of unit for which we want an evaluation
     *
     * @return array $responseDate consisting of 10 sets of questions and answers
     */
    public function findAllByUnit($idUnit)
    {
        $responseData = array();
        $sql = 'SELECT e.*
                FROM ' . $this->tableName() . ' e
                LEFT JOIN ' . $this->auxTableName('section') . ' s ON s.id = e.idSection
                WHERE s.idUnit =:id_unit_param';
        $params = array('id_unit_param' => $idUnit);
        $dataReader = $this->querySQL($sql, $params, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        foreach ($rows as $row) {
            $responseData[] = [
                "Order"    => $row["order"],
                "Question" => $row["question"],
                "OptionA"  => $row["optiona"],
                "OptionB"  => $row["optionb"],
                "OptionC"  => $row["optionc"],
                "Answer"   => $row["answer"]
            ];
        }

        return $responseData;
    }
}
