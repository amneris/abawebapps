<?php

/**
 * This is the model class for table "phrase_translations".
 *
 * The followings are the available columns in table 'phrase_translations':
 * @property integer $id
 * @property integer $idPhrase
 * @property string  $locale
 * @property string  $field
 * @property string  $content
 *
 * The followings are the available model relations:
 * @property Phrase  $object
 */
class PhraseTranslations extends AbaActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return PhraseTranslations the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return Yii::app()->params['dbWebservice'] . '.phrase_translations';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('locale, field', 'required'),
            array('idPhrase', 'numerical', 'integerOnly' => true),
            array('locale', 'length', 'max' => 8),
            array('field', 'length', 'max' => 32),
            array('content', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, idPhrase, locale, field, content', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'object' => array(self::BELONGS_TO, 'Phrase', 'idPhrase'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'       => 'ID',
            'idPhrase' => 'Phrase',
            'locale'   => 'Locale',
            'field'    => 'Field',
            'content'  => 'Content',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('idPhrase', $this->idPhrase);
        $criteria->compare('locale', $this->locale, true);
        $criteria->compare('field', $this->field, true);
        $criteria->compare('content', $this->content, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
