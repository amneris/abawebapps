<!--?php
/**
 * COLLECTIONS related with [countries]
 * SELECT c.`ID_country`, c.`name` FROM country c
 */
class AbaCountries extends AbaActiveRecord
{
    /**
     * @param string $name
     *
     * @return array of AbaCountry
     */
    public function getAllCountries($name="")
    {
        /**
         * @var array of AbaCountry
         */
        $where = "";
        $abaCountries = array();
        $countriesNotAvailable = Yii::app()->config->get("COUNTRIES_NOT_AVAILABLE");
        if (!empty($countriesNotAvailable)){
            $where = " WHERE id NOT IN (".$countriesNotAvailable.") ";
        }else{
            $where = " WHERE id NOT IN (8888) ";
        }

		if($name!=="")
        {
            $where .= " AND name LIKE '".$name."%'";
        }


		$sql= " SELECT id, name FROM country ".$where;
		$dataRows = $this->queryAllSQL( $sql );
        if(!$dataRows)
        {
            return false;
        }
        foreach($dataRows as $row)
        {
            $abaCountry = new AbaCountry();
            $abaCountry->setId( $row['id'] );
            $abaCountry->setName( $row['name'] );
			$abaCountries[]=$abaCountry;
		}

		return $abaCountries;
	}

    /**
     * @param string $name
     *
     * @return array
     */
    public function getListAllCountries($name="")
    {
        $list = array();
        $abaCountries = $this->getAllCountries($name);
        foreach($abaCountries as $abaCountry)
        {
            $list[$abaCountry->getId()] = $abaCountry->getName();
        }
        return $list;
    }
}
?>