<!--?php
/**
 * @property integer id
 * @property string name_m
 * @property string name
 * @property string iso
 * @property string iso3
 * @property string days
 * @property string numcode
 * @property string ABAIdCurrency
 * @property string ABALanguage
 * @property string decimalPoint
 */
class AbaCountry  extends AbaActiveRecord
{
    public $id;
    /* @var GeoIP_Location $geoLocation */
    public $geoLocation;

    /**
     * @param integer $id Id of the country, can be NULL
     */
    public function  __construct($id=NULL)
    {
        parent::__construct();
        if(!is_null($id)){
            return $this->getCountryById($id);
        }
        return $this;
    }


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'country';
    }


    public function mainFields()
    {
        return " c.`id`, c.`days`, c.`ID_month`, c.`iso`, c.`name_m`, c.`name`, c.`iso3`, c.`numcode`,
                c.`officialIdCurrency`, c.`ABAIdCurrency`, c.`ABALanguage`, c.`decimalPoint` ";
    }

    /**
     * @param $id
     *
     * @return AbaCountry|bool
     */
    public function getCountryById($id)
    {
        $sql = " SELECT ".$this->mainFields()." FROM ".$this->tableName()." c WHERE c.id=:IDCOUNTRY ";
        $paramsToSQL = array(":IDCOUNTRY"=>$id);

        $dataReader=$this->querySQL($sql,$paramsToSQL);
        if(($row = $dataReader->read())!==false)
        {
            $this->fillDataColsToProperties( $row );
            return $this;
        }

        return false;
    }

    /**
     * @param $code
     *
     * @return bool
     */
    public function getCountryByIsoCode($code)
    {
        $sql = " SELECT c.`id`, c.`iso`, c.`name` FROM ".$this->tableName()." c WHERE c.iso=:ISOCODE ";
        $paramsToSQL = array(":ISOCODE"=>$code);

        $dataReader=$this->querySQL($sql,$paramsToSQL);
        if(($row = $dataReader->read())!==false)
        {
            $this->fillDataColsToProperties( $row );
            return true;
        }

        return false;
    }

    /**
     * @param null $ip
     *
     * @return AbaCountry|bool
     */
    public function getCountryCodeByIP($ip=NULL)
    {
        if(is_null($ip)){
            /* @var CGeoIP Yii::app()->geoip */
            $this->geoLocation = Yii::app()->geoip->lookupLocation();
        }
        else{
            /* @var CGeoIP Yii::app()->geoip */
            $this->geoLocation = Yii::app()->geoip->lookupLocation($ip);
        }


        if(is_null($this->geoLocation))
        {
            $this->geoLocation = new GeoIP_Location();
            $this->geoLocation->longitude = 0.000;
            $this->geoLocation->latitude = 0.000;
            $this->geoLocation->countryCode = 0.000;
            $this->geoLocation->areaCode = "";
            $this->geoLocation->city = "";
            $code= Yii::app()->config->get("DefaultIPCountryCode");
        }
        else
        {
            $code=$this->geoLocation->countryCode;
        }

        if( $this->getCountryByIsoCode($code) )
        {
            return $this;
        }

        return false;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
}
?>