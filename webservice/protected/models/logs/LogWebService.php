<?php

class LogWebService extends CmLogWebService {

    public function tableName() {
       return Yii::app()->params['dbCampusLogs'].'.log_course_api';
    }

    public function getDbConnection() {
        return Yii::app()->dbLogs;
    }

}