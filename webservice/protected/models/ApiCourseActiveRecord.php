<?php

/**
 * Base Active Record Class for Models in the Webservice project
 */

class ApiCourseActiveRecord extends AbaActiveRecord
{
    /**
     * Function that provides Connection safe table name string for an auxiliary DB table needed from another model.
     *
     * @param string $auxTableName
     * @return string The full table name on the form db.table
     */
    public function auxTableName($auxTableName)
    {
        return Yii::app()->params['dbWebservice'].'.'. $auxTableName;
    }

    /**
     * @return CDbConnection the database connection used for this class
     */
    public function getDbConnection() {
        return Yii::app()->dbWebservice;
    }

}
