<?php
/**
 * ApiController is the customized base controller class for the Course API.
 * All Course API controller classes should extend from this base class.
 */
class ApiController extends WsRestComController
{
    const APPLICATION_ID = 'ABA';
    const ABA_FILM_SECTION = 'ABAFILM';
    const SPEAK_SECTION = 'SPEAK';
    const WRITE_SECTION = 'WRITE';
    const INTERPRET_SECTION = 'INTERPRET';
    const VIDEO_CLASS_SECTION = 'VIDEOCLASS';
    const EXERCISES_SECTION = 'EXERCISES';
    const VOCABULARY_SECTION = 'VOCABULARY';
    const ASSESSMENT_SECTION = 'ASSESSMENT';
    static protected $version = "2.0";
    static protected $aHistoryVersion = array(
        "2.0"=>"Login and update attribution through API.",
        "1.9"=>"Added Currency Web service, that returns exchange amount based on given currency and amount.",
        "1.8"=>"Added Analytics layer to progress controller.",
        "1.7"=>"Added API endpoint that returns all content (Phrases & Videos) of a specified unit.",
        "1.6"=>"Refactor of image serving and addition of help endpoint",
        "1.5"=>"Clarification on Progress Write JSON Format",
        "1.4"=>"Correction of wrong URI’s and other minor edits",
        "1.3"=>"Last part of Progress added",
        "1.2"=>"First part of Progress added",
        "1.1"=>"Added page attribute to some JSON objects",
        "1.0"=> "First release of API for COURSE ABA ENGLISH"
        );

    protected static $aStatusCodes = [
        200 => 'Ok',
        400 => 'Malformed Request',
        401 => 'Unauthorized',
        402 => 'Unknown token',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Bad signature',
        500 => 'Internal Server Error'
    ];

    protected static $infoActions = array('actions', 'version', 'iprecognition');

    /* @var array $reqDataDecoded*/
   // protected $reqDataDecoded;
    protected $nameService;
    protected $aResponse;
    /* @var CmAbaUser $user */
    protected $user;
    /* @var CmAbaUserDevice $device */
    protected $device;

    protected function isValidSignature($signature, $params)
    {
        if ($signature === $this->getSignature($params)) {
            return true;
        } else {
            return false;
        }
    }

    public function retErrorWs($code = null, $additionalErrorMsg = "")
    {
        $aResponse = array( $code => self::getStatusCodeMessage($code) ." - ".$additionalErrorMsg );
        $this->aResponse = $aResponse;
        $this->isSuccess = 0;
        return $aResponse[$code];
    }

    /**
     * @param \CAction $action
     *
     * @return bool
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeAction($action)
    {
        $this->disableCWebLogRoute();
        $authenticated = $this->authenticate();
        $this->logStart('ENABLE_LOG_COURSE_API');
        return $authenticated;
    }

    /**
     * @param \CAction $action
     *
     * @return bool
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterAction($action)
    {
        $this->isSuccess = true;
    }

    protected function sendResponse($status = 200, $body = '', $contentType = 'application/json')
    {
        // set the header fields (Status and Content-type)
        $statusHeader = 'HTTP/1.1 ' . $status . ' ' . $this->getStatusCodeMessage($status);
        header($statusHeader);
        header('Content-Type: ' . $contentType);

        if (is_array($body)) {
            $body = CJSON::encode($body);
        } else {
            $body = CJSON::encode(array('httpCode'=>$status, 'message'=>$body));
        }
        if ($status == 200) {
            $this->logEnd($body, true);
        } else {
            $this->logEnd($body, false);
        }
        echo $body;
        Yii::app()->end();
    }

    private function getStatusCodeMessage($status)
    {
        return (isset(self::$aStatusCodes[$status])) ? self::$aStatusCodes[$status] : 'Unknown status Code:' . $status;
    }

    protected function authenticate()
    {
        Yii::app()->language = 'en';
        if (intval(Yii::app()->config->get("ENABLE_AUTH_BASIC_RESTSERVICES")) == 1) {
            // Check if we have the USERNAME and PASSWORD HTTP headers set?
            $username = HeUtils::getServerValue('ABA_API_AUTH_USER');
            $password = HeUtils::getServerValue('ABA_API_AUTH_PW');
            if (!$username and !$password) {
                $this->sendResponse(401, array("status"=>'Unauthorized', 'message'=>'Login Required'));
            }
            // Find the user
            $user = new CmAbaUser();
            $user->populateUserByEmail($username, $password, true, false);
            if ($user === null) {
                // Error: Unauthorized
                $this->sendResponse(401, array( 'status'=>'Error', 'message'=>'user_unknown'));
                return false;
            } elseif (!$user->validatePassword($password)) {
                // Error: Unauthorized
                $this->sendResponse(401, array( 'status'=>'Error','message'=>'Incorrect password'));
                return false;
            }
            $this->user = $user;
            return true;
        } else {
            $token = HeUtils::getServerValue('ABA_API_AUTH_TOKEN');
            if (!$token) {
                $this->sendResponse(401, array( 'status'=>'Error', 'message'=> 'Token missing'));
                return false;
            }
            $device = new CmAbaUserDevice();
            if (!$device->getDeviceByToken($token)) {
                $this->sendResponse(400, array( 'status'=>'Error', 'message'=>'Token or device not found.'));
                return false;
            }
            $user = new CmAbaUser();
            if (!$user->populateUserById($device->userId)) {
                $this->sendResponse(401, array( 'status'=>'Error', 'message'=>'user_unknown'));
                return false;
            }
            Yii::app()->user->id = $user->getId();
            $this->user = $user;
            $this->device = $device;
            return true;
        }
    }

    /**
     * Function used to check if token/keyExternalLogin matches supplied userId
     *
     * @param string $token
     * @param string $tokenType
     * @param int $userId
     *
     * @return bool
     */
    protected function validateUser($token, $tokenType, $userId)
    {
        $valid = false;
        switch ($tokenType) {
            case 'deviceToken':
                $device = new CmAbaUserDevice();
                if ($device->getUserIdByToken($token) == $userId) {
                    $valid = true;
                }
                break;
            case 'keyExternalLogin':
                $user = new CmAbaUser();
                if ($user->getUserIdByKeyExternalLogin($token) == $userId) {
                    $valid = true;
                }
                break;
            default:
                $valid = false;
                break;
        }
        return $valid;
    }

    /**
     * Checks authentication for the requests. If none, just implement with return true;
     *
     * @return bool
     */
    protected function checkIfLogined()
    {
        // TODO: Refactor - - - -  DELETE HERE AND EVERYWHERE ELSE
    }

    /**
     * @param $sEventName
     * @param array $stParams
     *
     * @return array
     */
    protected function getCooladataEvent($sEventName, $stParams = array())
    {

        $stData = array();

        switch ($sEventName) {
            case "USERSCOPE":
                $stData = array(
                  "user_id" => "",
                  "user_expiration_date" => "",
                  "user_user_type" => "",
                  "user_lang_id" => "",
                  "user_birthday" => "",
                  "user_level" => "",
                  "user_country_id" => "",
                  "user_partner_first_pay_id" => "",
                  "user_partner_source_id" => "",
                  "user_partner_current_id" => "",
                  "user_source_id" => "",
                  "user_device_type" => "",
                  "user_entry_date" => "",
                  "user_product" => "",
                  "user_conversion_date" => "",
                );

                if (isset($stParams["abaUser"]->id)) {

                    $oLoginUser = new CmAbaUser();
                    $oLoginUser->getUserByApiUserId($stParams["abaUser"]->id);

                    if($oLoginUser) {

                        $stData["user_id"] = $stParams["abaUser"]->id;
//                        $stData["user_expiration_date"] = HeDate::europeanToSQL($stParams["abaUser"]->expirationDate, false, "-") . " 00:00:00";
                        $stData["user_expiration_date"] = $oLoginUser->expirationDate;
                        $stData["user_user_type"] = $stParams["abaUser"]->userType;
                        $stData["user_lang_id"] = $stParams["abaUser"]->langEnv;
//                        $stData["user_birthday"] = HeDate::europeanToSQL($stParams["abaUser"]->birthDate, false, "-") . " 00:00:00";
                        $stData["user_birthday"] = $oLoginUser->birthDate;
                        $stData["user_level"] = $stParams["abaUser"]->currentLevel;
                        $stData["user_country_id"] = $stParams["abaUser"]->countryId;
                        $stData["user_partner_first_pay_id"] = $stParams["abaUser"]->idPartnerFirstPay;
                        $stData["user_partner_source_id"] = $stParams["abaUser"]->idPartnerSource;
                        $stData["user_partner_current_id"] = $stParams["abaUser"]->idPartnerCurrent;
                        $stData["user_source_id"] = is_null($stParams["abaUser"]->idSourceList) ? "" : $stParams["abaUser"]->idSourceList;
                        $stData["user_device_type"] = $stParams["abaUser"]->deviceTypeSource;
//                        $stData["user_entry_date"] = HeDate::europeanToSQL($stParams["abaUser"]->entryDate, false, "-") . " 00:00:00";
                        $stData["user_entry_date"] = $oLoginUser->entryDate;
                        $stData["user_product"] = "";
                        $stData["user_conversion_date"] = "";
                    }
                }
                break;
        }

        return $stData;
    }

}
