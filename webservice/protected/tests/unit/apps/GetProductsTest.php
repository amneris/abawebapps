<?php

require_once(ROOT_DIR . '/webservice/protected/controllers/GetProductsController.php');

class GetProductsTest extends CmAbaCDbTestCase
{
    private $user;
    private $controller;
    private $productPeriods;
    private $typeCurrency;

    /*
     * TODO: cosas a mejorar en GetProductsController
     * separar logica de descuentos para poder testear
     * añadir info en la db para que los periods disponibles para las apps no sean hardcoded
     */
    public function setup()
    {
        parent::setUp();

        $this->user = new CmAbaUser();
        $this->user->id = 0;
        $this->user->countryId = 199; // Spain

        $this->controller = new GetProductsController('GetProducts');

        $productPeriods = new CmProdPeriodicity();
        $productPeriods = $productPeriods->findAll();

        $availablePeriods = array(30, 180, 360);
        foreach ($productPeriods as $period) {
            // array for easy way to search by id
            if (in_array($period->id, $availablePeriods)) {
                $this->productPeriods[$period->id] = $period;
            }
        }

        $country = CmAbaCountry::model('CmAbaCountry')->findByPk($this->user->countryId);
        $this->typeCurrency = $country->ABAIdCurrency;
    }

    public function teardown()
    {
        parent::tearDown();
    }

    public function testGetPromoCode()
    {
        $productPromo = $this->controller->getPromocode($this->user->id);
        $this->assertInstanceOf('ProductPromo', $productPromo);
    }

    public function testGetProductByIdCountryAndPeriod()
    {
    $productPrices = array();

        foreach ($this->productPeriods as $period) {
            $productPrice = new CmAbaProductPrice();
            $productPrice = $productPrice->getProductByIdCountryandPeriod($this->user->countryId, $period->days);

            if ($productPrice) {
                array_push($productPrices, $productPrice);
            }
        }

        $this->assertNotEmpty($productPrices);

        return $productPrices;
    }

    /* **** iOS **************************************************************************************************** */

    /**
     * @depends testGetProductByIdCountryAndPeriod
     */
    public function testGetTierByCurrency(array $productPrices)
    {
        $tiersFromPriceOriginal = array();

        foreach ($productPrices as $product) {
            $tierFromPriceOriginal = new CmPaymentsAppStorePricingMatrix();
            $tierFromPriceOriginal = $tierFromPriceOriginal->getTierbyCurrency($this->typeCurrency, $product->priceOfficialCry);

            if ($tierFromPriceOriginal) {
                array_push($tiersFromPriceOriginal, $tierFromPriceOriginal);
            }
        }

        $this->assertEquals(count($productPrices), count($tiersFromPriceOriginal));

        return $tiersFromPriceOriginal;
    }

    /**
     * @depends testGetProductByIdCountryAndPeriod
     * @depends testGetTierByCurrency
     */
    public function testGetTierABA(array $productPrices, array $tiersFromPriceOriginal)
    {
        $tiersABAFromPriceOriginal = array();

        foreach ($productPrices as $index => $product) {
            $months = $this->productPeriods[$product->idPeriodPay]->months;
            $tier = $tiersFromPriceOriginal[$index]->Tier;

            $tierABAFromPriceOriginal = new CmPaymentsAppABAPricingMatrix();
            $tierABAFromPriceOriginal = $tierABAFromPriceOriginal->getTierABA($tier, $months);

            if ($tierABAFromPriceOriginal) {
                array_push($tiersABAFromPriceOriginal, $tierABAFromPriceOriginal);
            }
        }

        $this->assertEquals(count($productPrices), count($tiersABAFromPriceOriginal));
    }

    /* **** Android ************************************************************************************************ */

    /**
     * @depends testGetProductByIdCountryAndPeriod
     */
    public function testGetPrice(array $productPrices)
    {
        $pricesOriginal = array();

        foreach ($productPrices as $product) {
            $months = $this->productPeriods[$product->idPeriodPay]->months;

            $priceOriginal = new CmPaymentsAppABAPricingMatrixAndroid();
            $priceOriginal = $priceOriginal->getPrice($months, $product->priceOfficialCry);

            if ($priceOriginal) {
                array_push($pricesOriginal, $priceOriginal);
            }
        }

        $this->assertEquals(count($productPrices), count($pricesOriginal));
    }
}
