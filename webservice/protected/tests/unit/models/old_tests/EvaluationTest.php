<?php
/**
 * EvaluationTest
 * Author: mgadegaard
 * Date: 10/02/2015
 * Time: 16:08
 * © ABA English 2015
 */

class EvaluationTest extends CmAbaCDbTestCase {

    protected $testEvaluation;

    public function setUp() {
        parent::setUp();
        $this->testEvaluation = new Evaluation();
    }

    public function tearDown() {
        parent::tearDown();
        unset($this->testEvaluation);
    }

    public function testTableName() {
        $testTableName = $this->testCourse->tableName();
        $this->assertEquals('aba_course.evaluation', $testTableName);
    }

    public function testFindAllByUnit() {
        $testResponse = $this->assertEquals($this->testEvaluation->findallByUnit(0));
        $this->assertEqualsl('No content found for given parameters. Calls to content needs unit id (1-144) and possibly a section type id (1-8) as well', $testResponse['message']);
        for($i = 1; $i <= 144; $i++) {
            $testResponse = $this->testEvaluation->findallByUnit($i);
            $this->assertequals(10, count($testResponse));
            $this->assertArrayHasKey("Order", $testResponse);
            $this->assertArrayHasKey("Question", $testResponse);
            $this->assertArrayHasKey("OptionA", $testResponse);
            $this->assertArrayHasKey("OptionB", $testResponse);
            $this->assertArrayHasKey("OptionC", $testResponse);
            $this->assertArrayHasKey("Answer", $testResponse);
        }
        $testResponse = $this->testEvaluation->findallByUnit(145);
        $this->assertEqualsl('No content found for given parameters. Calls to content needs unit id (1-144) and possibly a section type id (1-8) as well', $testResponse['message']);
    }
}