<?php
/**
 * ImagesTest
 * Author: mgadegaard
 * Date: 11/02/2015
 * Time: 12:11
 * © ABA English 2015
 */

class ImagesTest extends CmAbaCDbTestCase {

    protected $testImages;

    public function setup() {
        parent::setUp();
        $this->testImages = new Images();
    }

    public function teardown(){
        parent::tearDown();
        unset($this->testImages);
    }

    public function testTableName() {

        $testTableName = $this->testImages->tableName();

        $this->assertEquals('aba_course.images', $testTableName);
    }

    public function testGetUnitImages() {
        $this->assertFalse($this->testImages->getUnitImages(0));
        $this->assertFalse($this->testImages->getUnitImages(145));
        $testResponseData = $this->testImages->getUnitImages(60);
        $this->assertGreaterThan(0, count($testResponseData));
    }
}
