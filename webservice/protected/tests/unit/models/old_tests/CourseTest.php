<?php
/**
 * CourseTest
 * Author: mgadegaard
 * Date: 09/02/2015
 * Time: 13:43
 * © ABA English 2015
 */

class CourseTest extends CmAbaCDbTestCase {

    protected $testcourse;

    public function setup() {
        parent::setUp();
        $this->testCourse = new Course();
    }

    public function teardown(){
        parent::tearDown();
        unset($this->testcourse);
    }

    public function testTableName() {

        $testTableName = $this->testCourse->tableName();

        $this->assertEquals('aba_course.course', $testTableName);
    }

    public function testFindAll() {
        $testResponseData = $this->testcourse->findAll();
        $this->assertGreaterThan(0, count($testResponseData));
    }
}