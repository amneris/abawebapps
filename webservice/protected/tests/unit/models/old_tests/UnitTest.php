<?php
/**
 * UnitTest
 * Author: mgadegaard
 * Date: 11/02/2015
 * Time: 16:38
 * © ABA English 2015
 */

class UnitTest extends CmAbaCDbTestCase {

    protected $testUnit;

    public function setup() {
        parent::setUp();
        $this->testUnit = new Unit();
    }

    public function teardown(){
        parent::tearDown();
        unset($this->testUnit);
    }

    public function testTableName() {

        $testTableName = $this->testUnit->tableName();

        $this->assertEquals('aba_course.images', $testTableName);
    }

    public function testFindByPrimaryKey() {
        for($i = 1; $i <= 144; $i++){
            $testResponse = $this->testUnit->findByPrimaryKey($i);
            $this->assertArrayHasKey("idUnit", $testResponse);
            $this->assertNotNull($testResponse['idUnit']);
            $this->assertArrayHasKey("Title", $testResponse);
            $this->assertNotNull($testResponse['Title']);
            $this->assertArrayHasKey("Description", $testResponse);
            $this->assertNotNull($testResponse['Description']);
        }
        $this->assertFalse($this->testUnit->findByPrimaryKey(0));
        $this->assertFalse($this->testUnit->findByPrimaryKey(145));
    }

    public function testFindByPkTranslation() {
        for($i = 1; $i <= 144; $i++){
            $testResponse = $this->testUnit->findByPkTranslation($i);
            $this->assertArrayHasKey("idUnit", $testResponse);
            $this->assertNotNull($testResponse['idUnit']);
            $this->assertArrayHasKey("Title", $testResponse);
            $this->assertNotNull($testResponse['Title']);
            $this->assertArrayHasKey("Description", $testResponse);
            $this->assertNotNull($testResponse['Description']);
        }
        $this->assertFalse($this->testUnit->findByPkTranslation(0));
        $this->assertFalse($this->testUnit->findByPkTranslation(145));
    }

    public function testFindAll() {
        $testResponse = $this->testUnit->findAll();
        $this->assertEqual(144, count($testResponse));
        for($i = 1; $i <= 144; $i++){
            $this->assertArrayHasKey("idUnit", $testResponse[$i]);
            $this->assertNotNull($testResponse[$i]['idUnit']);
            $this->assertArrayHasKey("Title", $testResponse[$i]);
            $this->assertNotNull($testResponse[$i]['Title']);
            $this->assertArrayHasKey("Description", $testResponse[$i]);
            $this->assertNotNull($testResponse[$i]['Description']);
        }
    }

    public function testFindAllByLocale() {
        //EN
        $testResponseEN = $this->testUnit->findAllByLocale('en');
        $this->assertEqual(144, count($testResponseEN));
        for($i = 1; $i <= 144; $i++){
            $this->assertArrayHasKey("idUnit", $testResponseEN[$i]);
            $this->assertNotNull($testResponseEN[$i]['idUnit']);
            $this->assertArrayHasKey("Title", $testResponseEN[$i]);
            $this->assertNotNull($testResponseEN[$i]['Title']);
            $this->assertArrayHasKey("Description", $testResponseEN[$i]);
            $this->assertNotNull($testResponseEN[$i]['Description']);
        }

        //ES
        $testResponseES = $this->testUnit->findAllByLocale('es');
        $this->assertEqual(144, count($testResponseES));
        for($i = 1; $i <= 144; $i++){
            $this->assertArrayHasKey("idUnit", $testResponseES[$i]);
            $this->assertNotNull($testResponseES[$i]['idUnit']);
            $this->assertArrayHasKey("Title", $testResponseES[$i]);
            $this->assertNotNull($testResponseES[$i]['Title']);
            $this->assertArrayHasKey("Description", $testResponseES[$i]);
            $this->assertNotNull($testResponseES[$i]['Description']);
        }

        //IT
        $testResponseIT = $this->testUnit->findAllByLocale('it');
        $this->assertEqual(144, count($testResponseIT));
        for($i = 1; $i <= 144; $i++){
            $this->assertArrayHasKey("idUnit", $testResponseIT[$i]);
            $this->assertNotNull($testResponseIT[$i]['idUnit']);
            $this->assertArrayHasKey("Title", $testResponseIT[$i]);
            $this->assertNotNull($testResponseIT[$i]['Title']);
            $this->assertArrayHasKey("Description", $testResponseIT[$i]);
            $this->assertNotNull($testResponseIT[$i]['Description']);
        }

        //PT
        $testResponsePT = $this->testUnit->findAllByLocale('pt');
        $this->assertEqual(144, count($testResponsePT));
        for($i = 1; $i <= 144; $i++){
            $this->assertArrayHasKey("idUnit", $testResponsePT[$i]);
            $this->assertNotNull($testResponsePT[$i]['idUnit']);
            $this->assertArrayHasKey("Title", $testResponsePT[$i]);
            $this->assertNotNull($testResponsePT[$i]['Title']);
            $this->assertArrayHasKey("Description", $testResponsePT[$i]);
            $this->assertNotNull($testResponsePT[$i]['Description']);
        }
    }

    public function testFindAllByCourse() {
        //EN
        $testResponseEN = $this->testUnit->findAllByCourse(1, 'en');
        $this->assertEqual(144, count($testResponseEN));
        for($i = 1; $i <= 144; $i++){
            $this->assertArrayHasKey("idUnit", $testResponseEN[$i]);
            $this->assertNotNull($testResponseEN[$i]['idUnit']);
            $this->assertArrayHasKey("Title", $testResponseEN[$i]);
            $this->assertNotNull($testResponseEN[$i]['Title']);
            $this->assertArrayHasKey("Description", $testResponseEN[$i]);
            $this->assertNotNull($testResponseEN[$i]['Description']);
        }

        //ES
        $testResponseES = $this->testUnit->findAllByCourse(1, 'es');
        $this->assertEqual(144, count($testResponseES));
        for($i = 1; $i <= 144; $i++){
            $this->assertArrayHasKey("idUnit", $testResponseES[$i]);
            $this->assertNotNull($testResponseES[$i]['idUnit']);
            $this->assertArrayHasKey("Title", $testResponseES[$i]);
            $this->assertNotNull($testResponseES[$i]['Title']);
            $this->assertArrayHasKey("Description", $testResponseES[$i]);
            $this->assertNotNull($testResponseES[$i]['Description']);
        }

        //IT
        $testResponseIT = $this->testUnit->findAllByCourse(1, 'it');
        $this->assertEqual(144, count($testResponseIT));
        for($i = 1; $i <= 144; $i++){
            $this->assertArrayHasKey("idUnit", $testResponseIT[$i]);
            $this->assertNotNull($testResponseIT[$i]['idUnit']);
            $this->assertArrayHasKey("Title", $testResponseIT[$i]);
            $this->assertNotNull($testResponseIT[$i]['Title']);
            $this->assertArrayHasKey("Description", $testResponseIT[$i]);
            $this->assertNotNull($testResponseIT[$i]['Description']);
        }

        //PT
        $testResponsePT = $this->testUnit->findAllByCourse(1, 'pt');
        $this->assertEqual(144, count($testResponsePT));
        for($i = 1; $i <= 144; $i++){
            $this->assertArrayHasKey("idUnit", $testResponsePT[$i]);
            $this->assertNotNull($testResponsePT[$i]['idUnit']);
            $this->assertArrayHasKey("Title", $testResponsePT[$i]);
            $this->assertNotNull($testResponsePT[$i]['Title']);
            $this->assertArrayHasKey("Description", $testResponsePT[$i]);
            $this->assertNotNull($testResponsePT[$i]['Description']);
        }
    }

    public function testFindAllByCourseLevel() {
        //EN
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 1, 'en')));
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 2, 'en')));
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 3, 'en')));
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 4, 'en')));
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 5, 'en')));
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 6, 'en')));
        $testResponseEN = $this->testUnit->findAllByCourseLevel(1, 1, 'en');
        $this->assertEqual(24, count($testResponseEN));
        for($i = 1; $i <= 24; $i++){
            $this->assertArrayHasKey("idUnit", $testResponseEN[$i]);
            $this->assertNotNull($testResponseEN[$i]['idUnit']);
            $this->assertArrayHasKey("Title", $testResponseEN[$i]);
            $this->assertNotNull($testResponseEN[$i]['Title']);
            $this->assertArrayHasKey("Description", $testResponseEN[$i]);
            $this->assertNotNull($testResponseEN[$i]['Description']);
        }

        //ES
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 1, 'es')));
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 2, 'es')));
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 3, 'es')));
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 4, 'es')));
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 5, 'es')));
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 6, 'es')));
        $testResponseES = $this->testUnit->findAllByCourseLevel(1, 1, 'es');
        $this->assertEqual(24, count($testResponseES));
        for($i = 1; $i <= 24; $i++){
            $this->assertArrayHasKey("idUnit", $testResponseES[$i]);
            $this->assertNotNull($testResponseES[$i]['idUnit']);
            $this->assertArrayHasKey("Title", $testResponseES[$i]);
            $this->assertNotNull($testResponseES[$i]['Title']);
            $this->assertArrayHasKey("Description", $testResponseES[$i]);
            $this->assertNotNull($testResponseES[$i]['Description']);
        }

        //IT
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 1, 'it')));
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 2, 'it')));
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 3, 'it')));
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 4, 'it')));
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 5, 'it')));
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 6, 'it')));
        $testResponseIT = $this->testUnit->findAllByCourseLevel(1, 1, 'it');
        $this->assertEqual(24, count($testResponseIT));
        for($i = 1; $i <= 24; $i++){
            $this->assertArrayHasKey("idUnit", $testResponseIT[$i]);
            $this->assertNotNull($testResponseIT[$i]['idUnit']);
            $this->assertArrayHasKey("Title", $testResponseIT[$i]);
            $this->assertNotNull($testResponseIT[$i]['Title']);
            $this->assertArrayHasKey("Description", $testResponseIT[$i]);
            $this->assertNotNull($testResponseIT[$i]['Description']);
        }

        //PT
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 1, 'pt')));
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 2, 'pt')));
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 3, 'pt')));
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 4, 'pt')));
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 5, 'pt')));
        $this->assertEquals(24, count($this->testUnit->findAllByCourseLevel(1, 6, 'pt')));
        $testResponsePT = $this->testUnit->findAllByCourseLevel(1, 1, 'pt');
        $this->assertEqual(24, count($testResponsePT));
        for($i = 1; $i <= 24; $i++){
            $this->assertArrayHasKey("idUnit", $testResponsePT[$i]);
            $this->assertNotNull($testResponsePT[$i]['idUnit']);
            $this->assertArrayHasKey("Title", $testResponsePT[$i]);
            $this->assertNotNull($testResponsePT[$i]['Title']);
            $this->assertArrayHasKey("Description", $testResponsePT[$i]);
            $this->assertNotNull($testResponsePT[$i]['Description']);
        }
    }
}