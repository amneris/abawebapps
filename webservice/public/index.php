<?php

/**
 * Support for custom headers in PHP with FastCGI Process Manager (aka: PHP-FPM)
 */
require dirname(__FILE__) . '/../../common/php_fpm_workaround.php';

/**
 * A Yii application can run in either debug or production mode, as determined by the value of the constant YII_DEBUG.
 * By default, this constant value is defined as false, meaning production mode. To run in debug mode, define this
 * constant as true before including the yii.php file. Running the application in debug mode is less efficient because
 * it keeps many internal logs. On the other hand, debug mode is also more helpful during the development stage because
 * it provides richer debugging information when an error occurs.
 */
if (getenv('YII_DEBUG')) {
// DEVELOPMENT, INTEGRATION and STAGE: ****
    define('YII_DEBUG', true);
//    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3); // @ascandroli: VERY VERY ANNOYING EVEN FOR DEBUGGING
} else {
// LIVE ENVIRONMENT ****
    defined('YII_DEBUG') or define('YII_DEBUG', false);
}

// It includes all common files and folders shared between different projects:
require dirname(__FILE__) . '/../../common/bootstrap.php';

// These keys overwrite the ones in the table config in the database.
// It is useful for customize a local environment ignoring the development
// database and without annoying ur sensitive buddies.
if (file_exists(dirname(__FILE__) . '/protected/config/ProgrammerConfigKeys.php')) {
    include_once(dirname(__FILE__) . '/protected/config/ProgrammerConfigKeys.php');
}

$config = ROOT_DIR . '/webservice/protected/config/main.php';
Yii::createWebApplication($config)->run();
