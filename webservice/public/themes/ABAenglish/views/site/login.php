<?php
/* @var LoginForm $model */
/* @var integer $timeout
/* @var string $forceUrl */
/* @var string $msgConfirmation */
/* @var string $createAccountUrl */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="<?php echo Yii::app()->language; ?>" />
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/main.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/fonts.css'); ?>
    <?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/publicEvents.js'); ?>


    <title>
        <?php echo CHtml::encode($this->pageTitle); ?>
    </title>
</head>

<body id="login-page">

<?php
$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array(
    'Login',
);
?>
<!--
<h1>Login</h1>

<p>Please fill out the following form with your login y password:</p>
-->
<div class="containerLogin">
    <div class="containerForm">
        <div class="title">
            <?php echo Yii::t('mainApp', 'Iniciar sesión') ?>
        </div>
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'login-form',
            'action'=> $this->createURL('/site/login'),
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        )); ?>
        <?php if($msgConfirmation!="") { ?>
            <div class="row red">
                <?php echo Yii::t('mainApp', $msgConfirmation); ?>
            </div>
        <?php } ?>
        <div class="clear"></div>
        <div class="containerInputs">
            <div class="row label">
                <?php echo $form->labelEx($model, 'email'); ?>
                <?php echo $form->error($model, 'email'); ?>
            </div>
            <div class="clear"></div>
            <div class="row input">
                <?php echo $form->textField($model, 'email'); ?>
            </div>
            <div class="clear"></div>
            <div class="row label">
                <?php echo $form->labelEx($model, 'password'); ?>
                <?php echo $form->error($model, 'password'); ?>
            </div>
            <div class="clear"></div>
            <div class="row input">
                <?php echo $form->passwordField($model, 'password'); ?>
            </div>
            <div class="clear"></div>
            <div class="row rememberMe">
                <?php echo $form->checkBox($model, 'rememberMe'); ?>
                <?php echo $form->label($model, 'rememberMe'); ?>
                <?php echo $form->error($model, 'rememberMe'); ?>
            </div>
        </div>
        <div class="clear"></div>
        <div class="row buttons">
            <?php echo CHtml::submitButton('Login', array('class' => 'LoginButton', 'id'=>'btnStartSession', 'value' => Yii::t('mainApp', 'Iniciar sesión'))); ?>
        </div>
        <div class="clear"></div>
        <?php if(isset($forceUrl)){ ?>
            <input name="forceUrl"  value="<?php echo $forceUrl ?>" type="hidden">
        <?php } ?>
        <?php $this->endWidget(); ?>
        <div class="forgottenPassword">
            <a id="aLinkForgotPassword" href="<?php echo Yii::app()->createUrl("site/recoverpassword") ?>">
                <?php echo Yii::t('mainApp', '¿Has olvidado tu contraseña?') ?>
            </a>
        </div>
        <?php if (intval(Yii::app()->user->getState( "idPartnerCurNavigation"))!= PARTNER_JUNTAEXTREMADURA ){
//                    if ( intval(HeMixed::getGetArgs("idpartner")) != PARTNER_JUNTAEXTREMADURA ){
            ?>
            <div class="createAccount">
                <a id="aLinkBackToWeb" href="<?php echo $createAccountUrl ?>">
                    <?php echo Yii::t('mainApp', '¿Aún no estás registrado?') ?>
                </a>
            </div>
        <?php } ?>
        <div class="clear"></div>
    </div><!-- form -->
</div>
</body>

</html>

