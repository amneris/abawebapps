<?php
/**
 * ${NAME}
 * Author: mgadegaard
 * Date: 12/05/2015
 * Time: 13:34
 * © ABA English 2015
 */

$this->sendResponse(400, 'Malformed request, Unknown URI!');