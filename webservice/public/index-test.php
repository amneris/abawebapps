<?php

// It includes all common files and folders shared between different projects:
require dirname(__FILE__) . '/../../common/bootstrap.php';


// These keys overwrite the ones in the table config in the database. It is useful for customize a local environment ignoring the development
// database and without annoying ur sensitive buddies.
if (file_exists(dirname(__FILE__).'/config/ProgrammerConfigKeysTest.php'))
{
    include_once(dirname(__FILE__).'/config/ProgrammerConfigKeysTest.php');
}

/**
 * This is the bootstrap file for test application.
 * This file should be removed when the application is deployed for production.
 */

// Change the following paths if necessary
$yii=dirname(__FILE__).'/../../yii/framework/yii.php';
$config=dirname(__FILE__).'/../config/test.php';

// remove the following line when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);

require_once($yii);
Yii::createWebApplication($config)->run();
