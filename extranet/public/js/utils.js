function ABALaunchAjax( url, parameters, successF, errorF, doneF ) {
    if ( typeof successF == 'undefined' || successF == '' ) successF = function(data) {};
    if ( typeof errorF == 'undefined' || errorF == '' ) errorF = function(jqXHR, textStatus, error) { console.log(jqXHR + ' ' + textStatus + ' ' + error); ABAStopLoading(); };
    if ( typeof doneF == 'undefined' || doneF == '' ) doneF = function() {};

    ABAShowLoading();
    $.ajax({
        data:  parameters,
        url: url,
        context: document.body,
        type: 'POST',
        dataType:"json",
        success: function(data)
        {
            successF( data );
        },
        error: function(jqXHR, textStatus, error) {
            errorF( jqXHR, textStatus, error );
        },
    }).done(function() {
        doneF();
    });
}

function ABAChangePage( pageURL, parameters, loading, target ) {
    if ( typeof loading == 'undefined' ) loading = true;
    if ( typeof target == 'undefined' ) target = '_self';

    $form = $('<form method="post" target="'+target+'"></form>');
    $form.attr('action', pageURL);

    $.each(parameters, function( index, value ) {
        $form.append('<input type="hidden" name="'+index+'" />');
    });
    $('body').append($form);

    $.each(parameters, function( index, value ) {
        $( "input[name='"+index+"']" ).val( value );
    });

    if (loading) ABAShowLoading();
    $form.submit();

//  $.ajax({ url:pageURL, data:parameters, context:document.body, success:function(data){
//    var newDoc = document.open("text/html", "replace");
//    newDoc.write(data);
//    newDoc.close();          
//  }});

}

//function ABAShowDialog( content, close ) {
////  var width = $('body').outerWidth(true);
////  var height = $('body').outerHeight(true);
//  var height1 = $(document).height();
//  var width1 = $(document).width();
//  var height = $(window).height();
//  var width = $(window).width();
//  
//  $('#divFullCover').hide();
//  $('#divFullCover').remove();
//  
//  if ( !( $('#iframeFullCover') > 0 ) ) {
//    var ifr=$('<iframe/>', {
//      id:'iframeFullCover',
//      src:'about:blank',
//      style:'display:none; position:absolote; top:0; left:0; width:'+width1+'px; height:'+height1+'px; z-index: 9997;',
//    });
//    $('body').append(ifr);        
//  }
//  
//  if ( !( $('#divFullCover') > 0 ) ) {
//    var div = $('<div id="divFullCover" style="display:none; position:absolute; top:0; left:0; width:'+width1+'px; height:'+height1+'px; background: rgba(0, 0, 0, 0.6); z-index: 9998; overflow: hide;"></div>' );
//    $('body').append(div);        
//  }
//  
//  if ( !( $('#divInnerFullCover') > 0 ) ) {
//    var subWidth = parseInt(width/2, 10);
//    var subHeight = parseInt(height/2, 10);
//    
//    var div = $('<div id="divInnerFullCover" style="position:absolute; top:'+((height/2)-(subHeight/2))+'px; left:'+((width/2)-(subWidth/2))+'px; width:'+subWidth+'px; height:'+subHeight+'px; z-index: 9999; background-color: #EBEBEB; border: 2px solid #333333; text-align: center; overflow:hidden;"> \
//                  <div id="divCloseFullCover" class="divCloseFullCover" style="position:absolute; top:10px; right:10px; width:35px; height:35px;cursor:pointer;"><img src="/images/close-round.png" widht="35" height="35"></div> \
//                  <div style="width:100%; height:100%; overflow:auto;"> \
//                    <table width="100%" height="100%"><tr><td id="tdInnerFullCover" style="padding:20px; width:100%; height:100%;"></td></tr></table> \
//                  </div> \
//                </div>' );
//    $('#divFullCover').append(div);        
//  }
//
//  $('.divCloseFullCover').click( function () {
//    $('#divFullCover').hide();
//    $('#divFullCover').remove();
//  });
//  if ( close !== false ) {
//  } else {
////    $('#divCloseFullCover').remove();
//    $('.divCloseFullCover').hide();
//  }
//  
//  $('#tdInnerFullCover').html( content );
//  $('#divFullCover').show();
//}
//
//$(window).resize(function() {
//  var width0 = $(window).width();
//  var height0 = $(window).height();
//  var width1 = $(document).width();
//  var height1 = $(document).height();
//
//  if ( width0 > width1 ) var width = width0; else var width = width1;
//  if ( height0 > height1 ) var height = height0; else var height = height1;
//  var subWidth = parseInt(width/2, 10);
//  var subHeight = parseInt(height/2, 10);
//
//  $('#divFullCover').css( 'width', width );
//  $('#divFullCover').css( 'height', height );
//  $('#iframeFullCover').css( 'width', width );
//  $('#iframeFullCover').css( 'height', height );
//  
//  $('#divInnerFullCover').css('top', ((height/2)-(subHeight/2)));
//  $('#divInnerFullCover').css('left', ((width/2)-(subWidth/2)));
//});
//
//
//function ABACloseDialog() {
//  $('#divFullCover').hide();
//  $('#divFullCover').remove();
//}

function ABACustomDialog( titulo, msg, ok, close, width, height ) {

    if ( ok ) {
        $('#hiddenMsgOk #hiddenMsgOk_titulo').html(titulo);
        $('#hiddenMsgOk #hiddenMsgOk_mensaje').html(msg);
        var content = $('#hiddenMsgOk').html();
    } else {
        $('#hiddenMsgError #hiddenMsgError_titulo').html(titulo);
        $('#hiddenMsgError #hiddenMsgError_mensaje').html(msg);
        var content = $('#hiddenMsgError').html();
    }

    ABAShowDialog( content, close, width, height );
}

function ABAShowDialog( content, close, width, height ) {
//console.log( width+'x'+height );  
    if( typeof width == 'undefined' ) width = '660px';
    if( typeof height == 'undefined' ) height = '50%';
//console.log( width+'x'+height );  
    ABAStopLoading();
    $.fancybox( {

        fitToView	: true,
        width		: width,
        height		: height,
        autoSize	: false,
        closeClick	: false,
        closeBtn    : close,
        modal       : !close,
        openEffect	: 'none',
        closeEffect	: 'none',
        scrolling   : 'auto',
        content: content,
    });
}

function ABACloseDialog() {
    $.fancybox.close();
}


function ABAValidateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

var ABAShowLoading_Timeout = null;
function ABAShowLoading() {
//console.log( 'Loading' );  
    if ( ABAShowLoading_Timeout ) return;
    ABAShowLoading_Timeout = setTimeout( function( ) {
        if( !ABAShowLoading_Timeout ) return;
        ABAShowLoading_Timeout = null;
        ABAShowDialog( $('#divHiddenPopup').html(), false, '30%', '30%' );
    }, 1000);
}

function ABAStopLoading() {
    if ( ABAShowLoading_Timeout ) { clearTimeout(ABAShowLoading_Timeout); ABAShowLoading_Timeout = null; }
    ABACloseDialog();
}

var nextBtnActive = true;
function ABAStartLevelTest( parameters, nextQuestion ) {
    if (typeof nextQuestion != 'undefined') var url = campusDomain+'/leveltest/next';
    else var url = campusDomain+'/leveltest';
    var offsetTimeout = 0;
    nextBtnActive = false;

//  var parameters = {};
//  parameters
    var successF = function(data) {
        if (!data.ok) {alert(data.errorMsg); return;}

        if ( data.okAnswer ) {
            $(".ico-success.ok").css("display", 'inline-block');
            setTimeout( function() {
                ABAShowDialog( data.html, true );
                nextBtnActive = true;
            }, offsetTimeout );
        } else {
            $(".ico-ko.ko").css("display", 'inline-block');
            setTimeout( function() {
                ABAShowDialog( data.html, true );
                nextBtnActive = true;
            }, offsetTimeout );
        }
    };
    var errorF = function() { console.log('Level Test error'); };
    var doneF = function() { };

    ABALaunchAjax(url, parameters, successF, errorF, doneF);
}

// ** Listados. ** //
function ABARefreshList( module, newPag, preFilters) {
    if ( typeof module == 'undefined' ) module = 'student';
    if ( typeof newPag == 'undefined' ) newPag = 1;
    if ( typeof preFilters != 'undefined' ) newPag = 1; else preFilters = {};
    ABAShowLoading();

    var filters = [];
    $.each( preFilters, function( index, value ) {
        filters.push( { key: 'filter_'+index, value: value } );
    });
    $('.tableListFilter').each( function() {
        var module_tmp = $(this).data('module');
        if ( module == module_tmp ) {
            filters.push( { key: this.id, value: $(this).val() } );
        }
    });

    var url = "/"+module+"/"+module+"/refresh";
    var parametros = {
        OrderBy : window['OrderBy_'+module],
        checkedList: window['checkedList_'+module],
        pag: newPag,
        filters: filters,
    };

    var successF = function(data) {
        if ( data.ok ) $('#divTableList_'+module).html( data.html );
        ABAStopLoading();
    }
    ABALaunchAjax(url, parametros, successF);
}

function ABAActivarFiltros() {
    $('.tableListFilter').keydown( function (e) {
        var module = $( this ).data( "module" );
        if ( e.keyCode == 13 ) ABARefreshList( module );
    });

    $('select.tableListFilter').change( function (e) {
        var module = $( this ).data( "module" );
        ABARefreshList( module );
    });

}

function ABAActivarOrderBy() {
    $('.orderedColumn').click( function() {
        var module = $(this).data('module');
        ABAChangeOrderBy( this, module );
    });
}

function ABAActivarPaginador() {
    $('#paginationList a').click( function(e) {
        e.preventDefault();

        var module = $( this ).data( "module" );
        var pag = $(this).attr('id').replace( 'aListPage_', '' );
        ABARefreshList( module, pag );

        return false;
    });
}

function ABAActivarQueryTooltips() {
    $(".imgQuery").tooltip({
        placement : 'top'
    });
}

function ABAChangeOrderBy( obj, module ) {

    var id = obj.id.replace( 'ordered_', '' );

    if ( window['OrderBy_'+module] == id ) {
        window['OrderBy_'+module] = id+'_desc';
//    $.each( $(obj).children(), function() {
//      if ( $(this).hasClass('tdTitleOrder') ) {
//        $(this).children().attr('src', '/images/icon_orderby_desc.png')
//      }
//    });
    } else {
        window['OrderBy_'+module] = id;
//    $.each( $(obj).children(), function() {
//      if ( $(this).hasClass('tdTitleOrder') ) {
//        $(this).children().attr('src', '/images/icon_orderby.png')
//      }
//    });
    }

    ABARefreshList( module );
}

function ABAActivarChecks() {
    $('.tableList input:checkbox').click( function() {
        var module = $( this ).data( "module" );
        if ( typeof module == 'undefined' ) return true;

        var idElement = $(this).attr('id').replace('check_', '');
        var idElement_tmp = idElement.split('_');
        if ( idElement_tmp[0] == 'all' && ( window['checkedList_'+module] == 'all' || window['checkedList_'+module] == ',all' ) ) window['checkedList_'+module] = '';
        else if ( idElement_tmp[0] == 'all' ) window['checkedList_'+module] = 'all';
        else window['checkedList_'+module] += ','+ idElement_tmp[0];

        if ( idElement == 'all_'+module )
        {
            var checked = $('#check_all_'+module).prop('checked');

            $('.tableList input:checkbox').each( function()
            {
                var module_tmp = $(this).data('module');
                if ( module == module_tmp )
                {
                    $(this).prop('checked', checked);
                    var item2 = $(this).closest("tr");
                    if(checked)
                    {
                        if($(this).attr('id')!='check_all_'+module)
                        {
                            item2.addClass("highlight");
                        }
                    }
                    else
                    {
                        item2.removeClass("highlight");
                    }

                }
            });

        } else {
            $('#check_all_'+module).prop('checked', false);
        }

    });
}

function ABAHideMsgList( module ) {
    $('msgList_'+module).hide();
    $('msgList_'+module).html("");
//        $('#alertMessage2').hide();
//        $('#alertMessage2 #textMessageStudentList').html("");
}


function ABAShowMsgList( module, msg, ok, position ) {
    if ( typeof position == 'undefined' ) position = 2;
//  console.log( 'Module: '+module );
//  console.log( 'Msg: '+msg );

    if ( ok ) {
        $('#msgList_'+position+'_'+module).show();
        if ( position == 1 ) {
            $('#msgList_' + position + '_' + module).html("<img src=\"/images/tick.png\"><span>" + msg + "</span>");
            $('#msgList_' + position + '_' + module).removeClass('spanMsgAddListError');
        } else {
            $('#msgList_' + position + '_' + module).html("<img src=\"/images/tick2.png\"><span>" + msg + "</span>");
        }
        $('#msgList_'+position+'_'+module).fadeIn('slow');
        setTimeout( function() {
         $('#msgList_'+position+'_'+module).fadeOut('slow');
         }, 10000);

    } else {
        $('#msgList_'+position+'_'+module).show();
        if ( position == 1 ) {
            $('#msgList_' + position + '_' + module).html("<img class=\"msgAddList\" src=\"/images/circlealert.png\"><span style=\"color:#962428\">" + msg + "</span>");
            $('#msgList_' + position + '_' + module).addClass('spanMsgAddListError');
        } else {
            $('#msgList_' + position + '_' + module).html("<img class=\"msgAddList\" src=\"/images/redalert2.png\"><span style=\"color:#962428\">" + msg + "</span>");
        }
        $('#msgList_'+position+'_'+module).fadeIn('slow');
        setTimeout( function() {
         $('#msgList_'+position+'_'+module).fadeOut('slow');
         }, 3000)

    }
}

function ABAShowOkMsgList( module, msg, position ) {
    if ( typeof position == 'undefined' ) position = 2;
    ABAShowMsgList( module, msg, true, position );
}

function ABAShowErrorMsgList( module, msg, position ) {
    if ( typeof position == 'undefined' ) position = 2;
    ABAShowMsgList( module, msg, false, position );
}

function ABAActivarUndoList( module ) {
    $('#undoDeleteList_'+module).click( function( e ) {
        e.preventDefault();

        if ( !window['undoSetTimeout_'+module] ) return;
        clearTimeout(window['undoSetTimeout_'+module]); window['undoSetTimeout_'+module] = null;

        var url = '/'+module+'/'+module+'/undodelete';
        var parametros = {
            checkedList: window['checkedList_'+module],
            undoCode: $( this ).data( "undocode" )
        };

        var successF = function(data) {
            ABARefreshList( module );
            return false;
        }

        var doneF = function(data) {
            ABAHideMsgList();
        }

        ABALaunchAjax(url, parametros, successF, '', doneF);
    });

    window['undoSetTimeout_'+module] = setTimeout( function() {
        ABAClearCheckList( module );
        ABARefreshList( module );
    }, 3000);
}

function ABAClearCheckList( module ) {
    window['checkedList_'+module] = '0';
}

function ABAActivarQueryElement() {
//  $('.divQueryListElement').click( function() {
//    var module = $( this ).data( "module" );
//    var id = $( this ).data( "id" );
////    var id = this.id.replace( 'divLinkQueryUser_', '');
//    ABAconfig_QueryItem( id, module);
//  });

    $('.tdListLink').each( function() {
        var parent = $(this).parent();
        var module = $(parent).data("module");
        var id = $(parent).data("rowid");

        $(this).click( function() {
            ABAconfig_QueryItem(id, module);
        })
    });

}

function ABAActivarDeleteElement() {

    $('.iconDeleteList').click( function() {
        var module = $( this ).data( "module" );
        if ( typeof module == 'undefined' ) return true;

        var filters = [];
        $('.tableListFilter').each( function() {
            var module_tmp = $(this).data('module');
            if ( module == module_tmp ) {
                filters.push( { key: this.id, value: $(this).val() } );
            }
        });

        var url = '/'+module+'/'+module+'/delete';
        var parametros = {
            checkedList: window['checkedList_'+module],
            filters: filters,
        };

        var successF = function(data) {
            if ( data.ok ) {
                ABAShowOkMsgList( module, data.okMsg );
                ABAActivarUndoList( module );
            } else {
                ABAShowErrorMsgList( module, data.errorMsg );
            }

            ABAStopLoading();
        }

        ABAShowLoading();
        ABALaunchAjax( url, parametros, successF );

    });

}




//function showHiddenGlobalHelp() {
//    $('#divGlobalHelp').animate({
//        width: 40
//    }, 'slow', function () {
//        $('#divGlobalHelp span').css( 'visibility', 'visible');
//    });
//}
//
//function hideHiddenGlobalHelp() {
//    $('#divGlobalHelp span').css( 'visibility', 'hidden');
//    $('#divGlobalHelp').animate({
//        width: 20
//    }, 'slow', function () {
//    });
//}

function fixedDivGloblaHelp() {

    var h = $( window ).height();
    var h1 = parseInt( $( "#divGlobalHelpSpan" ).width() + 60 );
    $('#divGlobalHelp').css('line-height', h1 + 'px' );
    var h2 = (h/2) - (h1/2);

    $("#divGlobalHelp").offset({ top: h2, left: 0 });
}

function openContact() {

    var url = "/extranet/help";
    var parametros = {
    };

    var successF = function(data) {
        if ( data.ok ) {
            ABAShowDialog( data.html, true, '600px','410px');
        } else {
            ABAShowDialog( data.html, true, '600px', '410px' );
        }
    };
    var errorF = function() { console.log('error open dialog'); ABAStopLoading(); };
    var doneF = function() { };

    ABALaunchAjax(url, parametros, successF, errorF, doneF);

//    $.ajax({
//      data:  parametros,
//      url: "/extranet/help",
//      context: document.body,
//      type: 'POST',
//      dataType:"json",
//      success: function(data) {
//        if ( data.ok ) {
//          ABAShowDialog( data.html, true );
//        } else {
//          ABAShowDialog( data.html, true );
//        }
//      },
//      error: function(jqXHR, textStatus, error) {
////          alert('error');
//      },        
//    }).done(function() {
//      // 
//    });

}

var ABARedrawBody_active = false;
function ABARedrawBody() {
    var height_document = $(document).height();
//      var height_window = $(document).height();
    var height_body = $(document.body).height();
//      var height_div = $("#divMainBody").height();

//console.log( 'Height: Body:'+height_body+' | Document: '+height_document + ' | Document-125:'+ (height_document-125) );
    $("#divMainBody").height( (height_document-125-3) );
    return;

    var height_div = 0;
    if ( height_body == height_document ) height_div = height_document;
    else if( !ABARedrawBody_active ) {
        height_div = height_document + 48;
        ABARedrawBody_active = true;
    } else {
        height_div = height_document;
    }

    height_div = height_document - 129;

//    height_div = height_document;
//    $("#divMainBody").height(height_div);
}

/*Custom func to validate email field.*/
function validateEmail(id)
{
    var email = $('#'+id).val();

    if(email =='')
    {
        $('#'+id+'Error').show();
    }
    else
    {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!re.test(email))
        {
            $('#'+id+'Error').show();
        }
        else
        {
            $('#'+id+'Error').hide();
        }
    }
}

function validateField(id)
{
    var field = $('#'+id).val();

    if(field =='')
    {
        $('#'+id+'Error').show();
    }
    else
    {
        $('#'+id+'Error').hide();
    }
}

//Luis, function to set and read cookies with js
function createCookie(name, value, days)
{
    var expires;
    if (days)
    {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function getCookie(c_name)
{
    if (document.cookie.length > 0)
    {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1)
        {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1)
            {
                c_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}