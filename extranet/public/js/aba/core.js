var abaCore = new function() {

  this.launchAjax = function ( url, parameters, successF, errorF, doneF ) {
    if ( typeof successF == 'undefined' || successF == '' ) successF = function(data) {};
    if ( typeof errorF == 'undefined' || errorF == '' ) errorF = function(jqXHR, textStatus, error) { console.log(textStatus + ': ' + error); abaCore.stopLoading(); };
    if ( typeof doneF == 'undefined' || doneF == '' ) doneF = function() {};

    this.showLoading();
    $.ajax({
      data:  parameters,
      url: url,
      context: document.body,
      type: 'POST',
      dataType:"json",
      success: function(data) {
        successF( data );
      },
      error: function(jqXHR, textStatus, error) {
        errorF( jqXHR, textStatus, error );
      },        
    }).done(function() {
      doneF();
    });
  }

  this.showLoading_Timeout = null;
  this.showLoading = function () {
  //console.log( 'Loading' );  
    if ( this.showLoading_Timeout ) return;
    this.showLoading_Timeout = setTimeout( function( ) {
      if( !this.showLoading_Timeout ) return;
      this.showLoading_Timeout = null;
      this.showDialog( $('#divHiddenPopup').html(), false, '30%', '30%' );
    }, 1000);
  }

  this.closeDialog = function() {
    $.fancybox.close();
  }  

  this.showDialog = function ( content, close, width, height ) {
  //console.log( width+'x'+height );  
    if( typeof content == 'undefined' ) content = '';
    if( typeof close == 'undefined' ) close = true;
    if( typeof width == 'undefined' ) width = '70%';
    if( typeof height == 'undefined' ) height = '50%';
  //console.log( width+'x'+height );  
    this.stopLoading();
    $.fancybox( {
      fitToView   : true,
      width       : width,
      height      : height,
      autoSize    : false,
      closeClick	: false,
      closeBtn    : close,
      modal       : !close,
      openEffect	: 'none',
      closeEffect	: 'none',
      scrolling   : 'auto',
      content     : content,
    });  
  }
  
  this.stopLoading = function() {
    if ( this.showLoading_Timeout ) { clearTimeout(this.showLoading_Timeout); this.showLoading_Timeout = null; }
    this.closeDialog();
  }

//  this.number2String = function ( number, decimalPoint, millarPoint ) {
//    if ( typeof decimalPoint == 'undefined' ) decimalPoint = ',';
//    if ( typeof millarPoint == 'undefined' ) decimalPoint = '.';
//    
//    var numberInt = parseInt(number, 10);
//    var numberDec = parseFloat( (number - numberInt) , 10 ).toFixed(2);
//    numberDec += '';
////    numberDec = numberDec.substr();
////    .substring(1, 4); 
//  }

  this.swfFlashCheck = function(){
      if ( typeof swfobject != 'undefined' )
        return ( swfobject.getFlashPlayerVersion().major != 0 );
      else {
        try {
          hasFlash = Boolean(new ActiveXObject('ShockwaveFlash.ShockwaveFlash'));
        } catch(exception) {
          hasFlash = ('undefined' != typeof navigator.mimeTypes['application/x-shockwave-flash']);
        }

        return hasFlash;
      }
  };

}
