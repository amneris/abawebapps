<?php

return array(

  'components' => array(

    'cache' => array(
      'class' => 'system.caching.CMemCache',
    ),
    'config' => array(
      'class' => 'common.protected.extensions.econfig.EConfig',
      'cacheID' => 'cache'
    ),
  ),

);
