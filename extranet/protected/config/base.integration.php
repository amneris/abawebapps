<?php

return array(
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
  'params' => array(
    'campusURLDomain' => 'https://campus.int.aba.land',
    'extranetURLDomain' => 'https://corporate.int.aba.land',
    'intranetURLDomain' => 'https://intranet.int.aba.land',
    'languages' => array('es' => 2, 'it' => 3, 'fr' => 4, 'en' => 1, 'cn' => 7),
  ),
);
