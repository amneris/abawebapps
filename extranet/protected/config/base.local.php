<?php

return array(

  'components' => array(
    'cache' => array(
      'class' => 'system.caching.CMemCache',
    ),
    'config' => array(
      'class' => 'common.protected.extensions.econfig.EConfig',
      'cacheID' => 'cache'
    ),
  ),

  'modules' => array(
    'gii' => array(
      'class' => 'system.gii.GiiModule',
      'password' => 'gii',
      'ipFilters' => array('127.0.0.1', '192.168.56.1', '::1'),
    ),
  ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
  'params' => array(
    'campusURLDomain' => 'https://campus.aba.local',
    'extranetURLDomain' => 'https://extranet.aba.local',
    'intranetURLDomain' => 'https://intranet.aba.local',
    'languages' => array('es' => 2, 'it' => 3, 'fr' => 4, 'en' => 1, 'cn' => 7),
  ),
);
