<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',

    'name' => ' Aba English Extranet ',
    'sourceLanguage' => 'en',
    'language' => 'es',

    // preloading 'log' component
    'preload' => array('log'),

    // autoloading model and component classes
    'import' => array(
        'application.extensions.phpexcel.*',
        'application.extensions.phpmailer.*',
        'application.models.*',
        'application.models.selfgenerated.*',
        'application.models.logs.*',
        'application.modules.enterprise.models.*',
        'application.modules.enterprise.models.selfgenerated.*',
        'application.modules.group.models.*',
        'application.modules.group.models.selfgenerated.*',
        'application.modules.period.models.*',
        'application.modules.period.models.selfgenerated.*',
        'application.modules.user.models.*',
        'application.modules.user.models.selfgenerated.*',
        'application.modules.price.models.*',
        'application.modules.price.models.selfgenerated.*',
        'application.modules.student.models.*',
        'application.modules.student.models.selfgenerated.*',
        'application.modules.student.*',
        'application.components.*',
    ),

    'modules' => array(
        // uncomment the following to enable the Gii tool

        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'gii',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),

        'leveltest',
//        => array(
//          'levels' => array(
//            'Begginner' => array(
//              'levelNumber' => 1,
//              'textQuestions' => 3,
//              'audioQuestions' => 1,
//            ),
//            'Low intermediate' => array(
//              'levelNumber' => 2,
//              'textQuestions' => 3,
//              'audioQuestions' => 1,
//            ),
//            'Intermediate' => array(
//              'levelNumber' => 3,
//              'textQuestions' => 3,
//              'audioQuestions' => 1,
//            ),
//            'Upper intermediate' => array(
//              'levelNumber' => 4,
//              'textQuestions' => 3,
//              'audioQuestions' => 1,
//            ),
//            'Advanced' => array(
//              'levelNumber' => 5,
//              'textQuestions' => 3,
//              'audioQuestions' => 1,
//            ),
//            'Business' => array(
//              'levelNumber' => 6,
//              'textQuestions' => 3,
//              'audioQuestions' => 1,
//            ),
//          ),
//        ),
        'user',
        'student',
        'enterprise',
        'group',
        'period',
        'price',
        'followup',
        'purchase',

    ),

    // application components
    'components' => array(
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'loginUrl' => array('/user/login'),
            'returnUrl' => array('/'),
            'registrationUrl' => array('/user/login'),
            'recoveryUrl' => array('/user/recovery'),
            'class' => 'WebUser',
//                      'hash' => 'md5',
        ),

        // uncomment the following to enable URLs in path-format

        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'caseSensitive' => true,
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),

        'session' => array(
            'sessionName' => 'EXTRANETABA',
//            'class' => 'CDbHttpSession',
            'class' => 'CHttpSession',
//            'connectionID' => 'dbExtranet',
//            'autoCreateSessionTable' => false,
            'cookieMode' => 'only',
        ),

        'errorHandler' => array(
            // use 'extranet/error' action to display errors
            'errorAction' => 'extranet/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                // uncomment the following to show log messages on web pages

                array(
                    'class' => 'CWebLogRoute',
                ),

            ),
        ),
        'bootstrap' => array(
            'class' => 'ext.bootstrap.components.Bootstrap',
            'responsiveCss' => true
        )
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
//		'adminEmail'=>'webmaster@example.com',
        'campusURLDomain' => 'https://campus.abaenglish.com',
        'extranetURLDomain' => 'https://corporate.abaenglish.com',

        'EMAIL_SMTP_HOST' => $_SERVER['CORP_EMAIL_SMTP_HOST'],
        'EMAIL_SMTP_USERNAME' => $_SERVER['CORP_EMAIL_SMTP_USERNAME'],
        'EMAIL_SMTP_PASSWORD' => $_SERVER['CORP_EMAIL_SMTP_PASSWORD'],
        'EMAIL_SMTP_SECURE'   => $_SERVER['CORP_EMAIL_SMTP_SECURE'],

        'languages' => array('es' => 2, 'it' => 3, 'fr' => 4),
        'dbCampusLogs' => 'aba_b2c_logs',
        'ENABLE_LOG_WEBSERVICES' => true,
        'defaultB2bChannel' => 250001,
        'agentByCountry' => array('es' => 4, 'it' => 17, 'fr' => 27, 'default' => 17,),
    ),
);
