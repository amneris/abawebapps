<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',

    'name' => ' Aba English Extranet ',
    'defaultController' => 'enterprise/enterprise',
    'sourceLanguage' => 'none',
    'language' => 'es',

    // preloading 'log' component
    'preload' => array('log'),

    // autoloading model and component classes
    'import' => array(
        'application.extensions.phpexcel.*',
        'application.extensions.phpmailer.*',
        'application.models.*',
        'application.models.selfgenerated.*',
        'application.models.logs.*',
        'application.modules.purchase.models.*',
        'application.modules.purchase.models.selfgenerated.*',
        'application.modules.enterprise.*',
        'application.modules.enterprise.models.*',
        'application.modules.enterprise.models.selfgenerated.*',
        'application.modules.group.*',
        'application.modules.group.models.*',
        'application.modules.group.models.selfgenerated.*',
        'application.modules.period.*',
        'application.modules.period.models.*',
        'application.modules.period.models.selfgenerated.*',
        'application.modules.user.models.*',
        'application.modules.user.*',
        'application.modules.user.models.selfgenerated.*',
        'application.modules.price.models.*',
        'application.modules.price.models.selfgenerated.*',
        'application.modules.student.models.*',
        'application.modules.student.models.selfgenerated.*',
        'application.modules.student.*',
        'application.components.*',
    ),

    'modules' => array(
        // uncomment the following to enable the Gii tool

//        'gii' => array(
//            'class' => 'system.gii.GiiModule',
//            'password' => 'gii',
//            // If removed, Gii defaults to localhost only. Edit carefully to taste.
//            'ipFilters' => array('127.0.0.1', '::1'),
//        ),
//
        'leveltest' => array(
            'callback' => 'StudentModule::actualizeLevelTest',
        ),
        'user',
        'student',
        'enterprise',
        'group',
        'period',
        'price',
        'followup',
        'purchase',

    ),

    // application components
    'components' => array(
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'autoRenewCookie' => true,
            'loginUrl' => array('/user/login'),
            'returnUrl' => array('/'),
            'registrationUrl' => array('/user/login'),
            'recoveryUrl' => array('/user/recovery'),
            'class' => 'WebUser',
//                      'hash' => 'md5',
        ),

        // uncomment the following to enable URLs in path-format

        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'caseSensitive' => true,
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),

        'session' => array(
            'sessionName' => 'EXTRANETABA',
            'class' => 'CDbHttpSession',
//            'class' => 'CHttpSession',
            'connectionID' => 'dbExtranet',
//            'autoCreateSessionTable' => false,
            'cookieMode' => 'only',
//            'autoStart'=>true,
        ),

        'errorHandler' => array(
            // use 'extranet/error' action to display errors
            'errorAction' => 'extranet/error',
        ),
        'bootstrap' => array(
            'class' => 'ext.bootstrap.components.Bootstrap',
            'responsiveCss' => true
        )
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        'campusURLDomain' => 'https://campus.abaenglish.com',
        'extranetURLDomain' => 'https://corporate.abaenglish.com',
        'intranetURLDomain' => 'https://intranet.abaenglish.com',
        'aba_b2c' => 'aba_b2c',   // ** Base de datos de b2c
        'minValueIdParner' => 200000,
        'maxValueIdParner' => 250000,
        'defaultB2bChannel' => 250001,

        'languages' => array('es' => 2, 'it' => 3, 'fr' => 4, 'en' => 1, 'zh' => 7),
        'dbCampusLogs' => 'aba_b2c_logs',
        'ENABLE_LOG_WEBSERVICES' => true,
        'agentByCountry' => array('es' => 4, 'it' => 17, 'br' => 594, 'pt' => 594, 'ar' => 321 , 'default' => 17,),
        'SupportMail' => array(array('mail' => 'corporate@abaenglish.com')),
        'magicKey' => $_SERVER['CORP_MAGIC_KEY'],

        'URL_CAMPUS_WSSELLIGENT' => 'https://campus.abaenglish.com/wsoapabaselligent/senddata/',

        'logFacility' => (defined('LOG_LOCAL7') ? LOG_LOCAL7 : 8),
        'logGroup' => "extranet",
        'logName' => "[ABAWEBAPPS]",
    ),
);
