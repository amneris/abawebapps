<?php
require dirname(__FILE__) . '/../../../common/bootstrap.php';

defined('ABAWEBAPPS_APPNAME') or define('ABAWEBAPPS_APPNAME', 'extranet');

if (isset($_SERVER['ABAWEBAPPS_ENV'])) {
    $envHost = $_SERVER['ABAWEBAPPS_ENV'];
    $fileConfig = dirname(__FILE__) . '/console.base.' . $envHost . '.php';
    if (is_file($fileConfig)) {
        return CMap::mergeArray(
            (require ROOT_DIR . '/common/protected/config/main.php'),
            (require dirname(__FILE__) . '/console.base.php'),
            (require $fileConfig)
        );
    }
}

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG', true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

return CMap::mergeArray(
    (require ROOT_DIR . '/common/protected/config/main.php'),
    (require dirname(__FILE__) . '/console.base.php')
);