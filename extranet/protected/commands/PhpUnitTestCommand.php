<?php

class PhpUnitTestCommand extends CConsoleCommand {

  protected function startSelenium() {
    $seleniumServerPath = Yii::app()->params['SeleniumServerPath'];
    if ( !is_file($seleniumServerPath) ) {
      die("No he encontrado el servidor Selenium. \n");
    }

    $command = "ps -eF";
    exec( $command, $output );
    foreach( $output as $linea ) {
      if ( str_replace( basename($seleniumServerPath), '', $linea) != $linea
        && str_replace( 'java', '', $linea) != $linea
        && str_replace( '-jar', '', $linea) != $linea
      ) return;   // ** Ya esta en ejecucion, no hace falta lanzarlo de nuevo.
    }

    echo "Starting Selenium server ...\n";
    $command = "nohup java -jar {$seleniumServerPath} -Dwebdriver.chrome.driver=/var/extra/utils/Selenium/chromedriver >> /tmp/selenium.log 2>&1 &";
    echo $command."\n";
    pclose(popen($command, 'r'));
    sleep(5);
  }
  
  protected function stopSelenium() {
    $command = "ps -eF";
    exec( $command, $output );
    foreach( $output as $linea ) {
      if ( str_replace( 'selenium-server-standalone-2.45.0.jar', '', $linea) != $linea
        && str_replace( 'java', '', $linea) != $linea
        && str_replace( '-jar', '', $linea) != $linea
      ) {
        while( str_replace( '  ', ' ', $linea ) != $linea ) $linea = str_replace( '  ', ' ', $linea );

        $arTmp = explode( " ", $linea );
        $command = "kill {$arTmp[1]}";
        exec( $command );

        echo "Selenium server stopped.\n";
      }
    }

  }

  public function actionPhpUnitTest() {
    // export ABAWEBAPPS_ENV=carlesdev; /usr/bin/php /var/www/abaenglish/abawebapps/extranet/protected/yiic phpunittest phpunittest

    $iniPath = dirname(__FILE__)."/../tests/";
    $arTest = array(
      array(
        'name' => 'Test ABA Manager login',
        'command' => "phpunit --filter testLoginABAManager -c {$iniPath} {$iniPath}functional/SiteTest.php",
        'status' => 'pending',
        'doTest' => false,
      ),
      array(
        'name' => 'Test ABA Agent login',
        'command' => "phpunit --filter testLoginAgent -c {$iniPath} {$iniPath}functional/SiteTest.php",
        'status' => 'pending',
        'doTest' => false,
      ),
      array(
        'name' => 'Test Partner login',
        'command' => "phpunit --filter testLoginPartner -c {$iniPath} {$iniPath}functional/SiteTest.php",
        'status' => 'pending',
        'doTest' => false,
      ),
      array(
        'name' => 'Test Representante login',
        'command' => "phpunit --filter testLoginRepresentante -c {$iniPath} {$iniPath}functional/SiteTest.php",
        'status' => 'pending',
        'doTest' => false,
      ),
      array(
        'name' => 'Test Previos sin IVA',
        'command' => "phpunit --filter testPreciosSinIVA -c {$iniPath} {$iniPath}functional/SiteTest.php",
        'status' => 'pending',
        'doTest' => true,
      ),
      array(
        'name' => 'Dar de baja usuarios B2B del Campus',
        'command' => "phpunit --filter testDarDeBajaUsuarioB2BEnCampus -c {$iniPath} {$iniPath}functional/SiteTest.php",
        'status' => 'pending',
        'doTest' => false,
      ),
    );

    $this->startSelenium();

    foreach ( $arTest as $k=>$test ) {
      if (!$test['doTest']) continue;

      $output = array();
      $command = $test['command'];
      echo "$command\n";
      exec( $command, $output );

      $testOk = false;
      foreach( $output as $linea ) {
        if ( substr($linea, 0, 2) == 'OK' ) $testOk = true;
      }

      $arTest[$k]['status'] = $testOk;

      if ( !$testOk ) var_export( $output );
    }


    foreach ( $arTest as $k=>$test ) {
      if ( $test['status'] === true ) $status = 'OK';
      else if( !$test['doTest'] ) $status = 'Not testing';
      else $status = 'KO';

      $resultado = $test['name']."\t$status\n";
      echo $resultado;
      file_put_contents('/tmp/resultadoTest.log', $resultado, FILE_APPEND);

    }

    $this->stopSelenium();


//    var_export($output);

//    if ( $testOk ) echo "test OK\n"; else echo "Test KO \n";
  }
  
}