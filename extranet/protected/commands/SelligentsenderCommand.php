<?php

class SelligentsenderCommand extends CConsoleCommand {
  
  public function actionSend() {
    echo date("Y-m-d H:i:s").": Action Send\n";
    
    $criteria = new CDbCriteria();
    $criteria->addCondition('t.isdeleted = 0');
    $criteria->addCondition('t.status = 0');
    $criteria->params = array();
    $criteria->limit = 100;
    $mlSelligentQueue = SelligentQueue::model()->findAll($criteria);
    if ( $mlSelligentQueue ) echo "Queue is not empty.\nWorking ...\n"; else echo "Empty queue.\n";
    if ( !$mlSelligentQueue ) $mlSelligentQueue = array();

    foreach( $mlSelligentQueue as $mSelligentElement ) {
      $allOk = HeAbaSelligent::send($mSelligentElement->action, unserialize($mSelligentElement->data), $mSelligentElement->callback);

//      $mSelligentElement = SelligentQueue::model()->findByPk( $mSelligentElement->id );
//      if ( !$mSelligentElement ) continue;  // ** TODO: Report error.

      if ( $allOk ) {
        $mSelligentElement->status = 1;
        $mSelligentElement->result = 1;
        $mSelligentElement->result_txt = '';
        $subjectMail = '(Sistema) Verificacio enviament selligent';
      } else {
        $mSelligentElement->status = 2;
        $mSelligentElement->result = 2;
        $mSelligentElement->result_txt = HeAbaSelligent::getLastErrorMsg();
        $subjectMail = '(Sistema) Error enviament selligent';
      }
      $mSelligentElement->sent = date("Y-m-d H:i:s");
      $mSelligentElement->save();

        // ** Mentres no tingui acces als logs ... per a no estar demanant-li a cada moment al Dani
      $toLog = array(
        'msg' => var_export( $mSelligentElement, true ),
        'date' => date("Y-m-d H:i:s"),
      );

    }

    echo "\n";
    echo date("Y-m-d H:i:s").": End Action Send\n";
  }

    // ** Envia un corrreo a los representantes con los ultimos test de nivel realizados.
  public function actionKpiLastLevelTestDone() {
    $conn = Yii::app()->dbExtranet;

    $sql = "select e.id as idEnterprise, s.name as studentName, s.surname as studentSurname, ltd.completed as dateCompleted, u.id as idUser, s.id as idStudent, ltd.completed as completed, u.name as rname, u.surname as rsurname, slt.created as created
            from LevelTest_Data ltd, student_level_test slt, student s, enterprise e, user u
            where slt.idLevelTest = ltd.id
              and slt.informe = 0
--              and (ltd.completed != '2000-01-01 00:00:00' or (ltd.completed = '2000-01-01 00:00:00' and ltd.jsonData = '' and ltd.status = 1 ) )
              and slt.idStudent = s.id
              and s.idEnterprise = e.id
              and e.idMainContact = u.id
            group by s.id
            order by ltd.completed desc";
    $command = $conn->createCommand( $sql );
    $rows = $command->queryAll();
    $arEnterprise = self::agruparPorEmpresa($rows);

    $fp = fopen( dirname(__FILE__).'/../../public/media/selligent.dailyleveltest.csv', 'w');
    if ( $fp ) {
      $header = false;
      foreach( $arEnterprise as $arTipos ) {

        foreach ($arTipos as $tipo => $rows) {
          foreach ($rows as $row) {

            $fields = array(
              'idEnterprise' => $row['idEnterprise'],
              'idUser' => $row['idUser'],
              'nombreRepresentante' => $row['rname'] . ' ' . $row['rsurname'],
              'idStudent' => $row['idStudent'],
              'nombreStudent' => $row['studentName'] . ' ' . $row['studentSurname'],
              'fecha' => $row['dateCompleted'],
              'tipo' => $tipo,
              'total' => count($rows),
              'dia' => date("Y-m-d H:i:s")
            );

            if (!$header) {
              $header = true;
              fputcsv($fp, array_keys($fields));
            }
            fputcsv($fp, $fields);
          }
        }
      }

      fclose($fp);
    }

  }

  static public function agruparPorEmpresa( $rows ) {
    $arFinal = array();
    foreach( $rows as $row ) {
      $tipo = ( $row['completed'] == '2000-01-01 00:00:00' ) ? 2 : 1;

      $row['dateCompleted'] = ( $tipo == 1 ) ? $row['dateCompleted'] : $row['created'];

      $arFinal[ $row['idEnterprise'] ][$tipo][] = $row;
    }

    return $arFinal;
  }

}