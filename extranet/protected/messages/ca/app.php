<?php
return array(
	'Student' => 'Alumne', 
	'Update account' => 'Actualitzar conta', 
	'Manage purchase' => 'Administrar compres', 
	'Agent' => 'Agent', 
	'German' => 'Alemany', 
	'Students' => 'Alumnes', 
	'ADD' => 'AFEGIR', 
	'Analytics' => 'Analítica', 
	'Annual' => 'Anual', 
	'Search' => 'Buscar', 
	'Loading' => 'Cargando', 
	'Close' => 'Tancar', 
	'Sign out' => 'Cerrar sesión', 
	'Settings' => 'Configuración', 
	'General settings' => 'Configuración general', 
	'Convocation' => 'Convocatoria', 
	'CONVOCATION' => 'CONVOCATORIA', 
	'Convocations' => 'Convocatorias', 
	'Email' => 'Email', 
	'CREATED' => 'CREAT', 
	'Created' => 'Creat', 
	'Undo' => 'Deshacer', 
	'Deleted' => 'Eliminado', 
	'EMAIL' => 'EMAIL', 
	'Company' => 'Empresa', 
	'Spanish' => 'Español', 
	'Entry date' => 'Fecha de alta', 
	'French' => 'Francés', 
	'Groups' => 'Grupos', 
	'Save' => 'Guardar', 
	'ID' => 'ID', 
	'English' => 'Inglés', 
	'Italian' => 'Italiano', 
	'license/s' => 'licencia/s', 
	'Month' => 'Mes', 
	'Name' => 'Nombre', 
	'NAME' => 'NOMBRE', 
	'Ok' => 'Ok', 
	'Please, wait some seconds.' => 'Por favor espere unos segundos.', 
	'Please select a crop region then press CROP.' => 'Por favor seleccione una región a recortar y luego haga clic en RECORTAR.', 
	'Portuguese' => 'Portugués', 
	'Prices' => 'Precios', 
	'Teacher' => 'Profesor', 
	'Trial' => 'Prueba', 
	'Crop' => 'Recortar', 
	'Reservation made successfully.' => 'Reserva efectuada con éxito.', 
	'Responsible' => 'Responsable', 
	'ROLE' => 'ROL', 
	'There was a problem cropping the image.' => 'Se encontró un problema recortando la imagen.', 
	'Is deleted' => 'Se ha eliminado', 
	'Semiannual' => 'Semestral', 
	'If you need it, you can crop de image to adjust the logo and keep a good appearance.' => 'Si lo necesita, puede recortar la imagen y ajustarla para que aparezca mejor.', 
	'Request licences' => 'Solicitar licencias', 
	'License Type' => 'Tipo de licencia', 
	'Quarterly' => 'Trimestral', 
	'Last updated' => 'Última actualización', 
	'User' => 'Usuario', 
	'Users' => 'Usuarios', 
	'Back' => 'Volver', 
	'PERIOD' => 'CONVOCATORIA', 
	'Month' => 'Mes', 
	'Quarterly' => 'Trimestral', 
	'Semiannual' => 'Semestral', 
	'Annual' => 'Anual', 
	'Two years' => 'Dos años', 
	'Select a new logo' => 'Seleccione un nuevo logo', 
	'Select a logo' => 'Seleccione un logo', 
	'SURNAMES' => 'APELLIDOS', 
	'Group' => 'Grupo', 
	'Groups' => 'Grupos', 
	'Periods' => 'Convocatorias', 
	'Period' => 'Convocatoria', 
	'Confirmation' => 'Confirmación', 
	'Find by email' => 'Buscar por email', 
	'Sorry, PNG or JPG are the only allowed formats at the moment.' => 'Disculpe, por ahora sólo se permiten imágenes en formato PNG o JPG.', 
	'logo' => 'logo', 
	'Contact with us' => 'Contacte con nosotros', 
	'Contact us filling this form and we will contact you as soon as posible.' => 'Contacte con nosotros a través del siguiente formulario, le responderemos lo antes posible.', 
	'Subject' => 'Asunto', 
	'Message' => 'Mensaje', 
	'Send' => 'Enviar', 
	'The email is not valid or blank.' => 'El correo no es válido o está vacío.', 
	'Required field.' => 'Campo requerido.', 
	'HELP' => 'AYUDA', 
	'Name must not be empty.' => 'El nombre no debe estar vacío.', 
	'Surname must not be empty.' => 'El apellido no debe estar vacío.', 
	'Email must not be empty.' => 'El correo electrónico no debe estar vacío.', 
	'Incorrect email.' => 'Correo electrónico incorrecto.', 
	'Group must not be empty.' => 'El grupo no debe estar vacío.', 
	'Agent' => 'Agente', 
	'User or password is incorrect.' => 'Usuario o contraseña incorrectos.', 
	'Forbidden, access denied' => 'Acceso denegado', 
	'The requested page does not exist.' => 'La página requerida no existe.', 
	'Petición de licencias ABA Corporate.' => 'Petición de licencias ABA Corporate.', 
	'Accedding when done.' => 'Accediendo ya terminado.', 
	'Accedding when pendding.' => 'Accediendo pendiente.', 
	'Accedding when new.' => 'Accediendo nuevo.', 
	'You must accept conditions.' => 'Debe aceptar las condiciones.', 
	'You must select some subject.' => 'Introduzca un asunto.', 
	'Message mustnt be empty' => 'El mensaje no puede estar vacío', 
	'Acepto las <a href="#" target="_blank">condiciones de uso</a> y la <a href="#" target="_blank">política de privacidad</a>' => 'Acepto las <a href="#" target="_blank">condiciones de uso</a> y la <a href="#" target="_blank">política de privacidad</a>', 
	'Select a subject' => 'Seleccione un asunto', 
	'Email sends successfully.' => 'Su consulta se ha enviado correctamente. En breve nos pondremos en contacto con usted.', 
	'Name field required.' => 'Campo \'nombre\' requerido.', 
	'Surnames field required.' => 'Campo \'apellidos\' requerido.', 
	'Undefined error.' => 'Error indefinido.', 
	'Something wrong inserting student.' => 'Se ha encontrado un error al crear el alumno.', 
	'Something wrong inserting group {$group}.' => 'Ha ocurrido un error al tratar de crear el grupo \'{$group}\'.', 
	'Something wrong inserting relation between student and group.' => 'Ha ocurrido un error al tratar de realizar la relación entre alumno y grupo.', 
	'Something wrong sending mail.' => 'Ha ocurrido un error en el envío del email.', 
	'Activar' => 'Activar', 
	'Escoge una convocatoria' => 'Escoja una convocatoria', 
	'License' => 'Licencia', 
	'No se ha encontrado al alumno' => 'No se ha encontrado el alumno', 
	'Por favor, verifica que has escrito correctamente lo que buscas.' => 'Por favor, verifique que ha escrito correctamente lo que busca.', 
	'COUNTRY' => 'PAÍS', 
	'Education' => 'Educación', 
	'Public institution' => 'Institución pública', 
	'ENTERPRISE TYPE' => 'TIPO DE EMPRESA', 
	'Language is not valid.' => 'El idioma no es válido.', 
	'Export to CSV' => 'Exportar a CSV', 
	'Show all' => 'Mostrar todo', 
	'¡ Gracias {nombreAgente} !<br>Has habilitado {numeroLicencias} {licencias} del tipo {tipoLicencia} de la empresa {nombreEmpresa} con éxito.' => '¡Gracias {nombreAgente} !<br>Ha habilitado con éxito {numeroLicencias} {licencias} del tipo {tipoLicencia} de la empresa {nombreEmpresa}.', 
	'Licencias validadas' => 'Licencias validadas', 
	'Descuento' => 'Descuento', 
	'Reservations' => 'Reserves', 
	'¿ Está seguro de querer borrar esta/s compra/s ?' => '¿Está seguro de querer borrar esta/s compra/s ?', 
	'lbl: Ok' => 'Ok', 
	'lbl: Error' => 'Error', 
	'Browser detection message' => 'Hemos detectado que la versión del navegador que estás utilizando no está actualizada.<br> Para una mejor navegación, te recomendamos que actualices la versión. Haz clic <a href="http://windows.microsoft.com/es-es/internet-explorer/download-ie">aquí</a> para actualizar :)', 
	'lbl: Sin definir' => 'Sense definir', 
	'Configuración' => 'Configuración', 
	'lbl: Resend welcome email' => 'Reenviar mail benvinguda', 
	'Password must not be empty.' => 'La contrasenya no pot ser buida.', 
	'lbl: Purchases validated successfully!' => 'Llicències validades amb éxit.', 
	'Chinese' => 'Xinès', 
	'lbl: Borrar' => 'Esborrar', 
);
