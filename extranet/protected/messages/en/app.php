<?php
return array(
	'Student' => 'Student', 
	'Update account' => 'Update account', 
	'Manage purchase' => 'Manage purchases', 
	'Agent' => 'Agent', 
	'German' => 'German', 
	'Students' => 'Students', 
	'ADD' => 'ADD', 
	'Analytics' => 'Analytics', 
	'Annual' => 'Yearly', 
	'Search' => 'Search', 
	'Loading' => 'Loading', 
	'Close' => 'Close', 
	'Sign out' => 'Log out', 
	'Settings' => 'Settings', 
	'General settings' => 'General settings', 
	'Convocation' => 'Period', 
	'CONVOCATION' => 'PERIOD', 
	'Convocations' => 'Periods', 
	'Email' => 'Email address', 
	'CREATED' => 'CREATED', 
	'Created' => 'Created', 
	'Undo' => 'Undo', 
	'Deleted' => 'Deleted', 
	'EMAIL' => 'EMAIL ADDRESS', 
	'Company' => 'Company', 
	'Spanish' => 'Spanish', 
	'Entry date' => 'Entry date', 
	'French' => 'French', 
	'Groups' => 'Groups', 
	'Save' => 'Save', 
	'ID' => 'ID', 
	'English' => 'English', 
	'Italian' => 'Italian', 
	'license/s' => 'license(s)', 
	'Month' => 'Month', 
	'Name' => 'Name', 
	'NAME' => 'NAME', 
	'Ok' => 'OK', 
	'Please, wait some seconds.' => 'Please wait a few seconds.', 
	'Please select a crop region then press CROP.' => 'Please select a region to be cropped and then press CROP.', 
	'Portuguese' => 'Portuguese', 
	'Prices' => 'Prices', 
	'Teacher' => 'Teacher', 
	'Trial' => 'Trial', 
	'Crop' => 'Crop', 
	'Reservation made successfully.' => 'Reservation made successfully.', 
	'Responsible' => 'Person in charge', 
	'ROLE' => 'ROLE', 
	'There was a problem cropping the image.' => 'There was a problem cropping the image.', 
	'Is deleted' => 'It has been deleted', 
	'Semiannual' => 'Six-monthly', 
	'If you need it, you can crop de image to adjust the logo and keep a good appearance.' => 'If you need, you can crop the image and adjust it so that it looks better.', 
	'Request licences' => 'Request licences', 
	'License Type' => 'License type', 
	'Quarterly' => 'Quarterly', 
	'Last updated' => 'Last updated', 
	'User' => 'User', 
	'Users' => 'Users', 
	'Back' => 'Go back', 
	'PERIOD' => 'PERIOD', 
	'Month' => 'Month', 
	'Quarterly' => 'Quarterly', 
	'Semiannual' => 'Six-Monthly', 
	'Annual' => 'Yearly', 
	'Two years' => 'Two-year', 
	'Select a new logo' => 'Select a new logo', 
	'Select a logo' => 'Select a logo', 
	'SURNAMES' => 'SURNAME(S)', 
	'Group' => 'Group', 
	'Groups' => 'Groups', 
	'Periods' => 'Periods', 
	'Period' => 'Period', 
	'Confirmation' => 'Confirmation', 
	'Find by email' => 'Search by email address', 
	'Sorry, PNG or JPG are the only allowed formats at the moment.' => 'Sorry, for the moment only PNG or JPG images are allowed.', 
	'logo' => 'logo', 
	'Contact with us' => 'Contact us', 
	'Contact us filling this form and we will contact you as soon as posible.' => 'Contact us by filling in this form and we will reply as soon as posible.', 
	'Subject' => 'Subject', 
	'Message' => 'Message', 
	'Send' => 'Send', 
	'The email is not valid or blank.' => 'The email address is invalid or blank.', 
	'Required field.' => 'Required field.', 
	'HELP' => 'HELP', 
	'Name must not be empty.' => 'The name field cannot be empty.', 
	'Surname must not be empty.' => 'The surname field cannot be empty.', 
	'Email must not be empty.' => 'The email address field cannot be empty.', 
	'Incorrect email.' => 'Incorrect email address.', 
	'Group must not be empty.' => 'The group field cannot be empty.', 
	'Agent' => 'Agent', 
	'User or password is incorrect.' => 'Username or password incorrect.', 
	'Forbidden, access denied' => 'Access denied', 
	'The requested page does not exist.' => 'The page you requested does not exist.', 
	'Petición de licencias ABA Corporate.' => 'Request for ABA Corporate licenses.', 
	'Accedding when done.' => 'Login now complete.', 
	'Accedding when pendding.' => 'Accedding when pendding.', 
	'Accedding when new.' => 'Accedding when new.', 
	'You must accept conditions.' => 'You must accept the terms and conditions.', 
	'You must select some subject.' => 'Enter a subject.', 
	'Message mustnt be empty' => 'The message cannot be empty', 
	'Acepto las <a href="#" target="_blank">condiciones de uso</a> y la <a href="#" target="_blank">política de privacidad</a>' => 'I accept the <a href="#" target="_blank">terms of use</a> and the <a href="#" target="_blank">privacy policy</a>', 
	'Select a subject' => 'Select a subject', 
	'Email sends successfully.' => 'Your question has been sent correctly. We will contact you soon.', 
	'Name field required.' => '\'Name\' field required.', 
	'Surnames field required.' => '\'Surname(s)\' field required.', 
	'Undefined error.' => 'Undefined error.', 
	'Something wrong inserting student.' => 'An error occurred while creating the student.', 
	'Something wrong inserting group {$group}.' => 'An error occurred while trying to create the group \'{$group}\'.', 
	'Something wrong inserting relation between student and group.' => 'An error occurred  while trying to group the student.', 
	'Something wrong sending mail.' => 'An error occurred while sending the email.', 
	'Activar' => 'Activate', 
	'Escoge una convocatoria' => 'Choose a period', 
	'License' => 'License', 
	'No se ha encontrado al alumno' => 'The student has not been found', 
	'Por favor, verifica que has escrito correctamente lo que buscas.' => 'Please check that you have written what you are looking for correctly.', 
	'COUNTRY' => 'COUNTRY', 
	'Education' => 'Education', 
	'Public institution' => 'Public institution', 
	'ENTERPRISE TYPE' => 'TYPE OF COMPANY', 
	'Language is not valid.' => 'The language is not valid.', 
	'Export to CSV' => 'Export to a CSV file', 
	'Show all' => 'Show all', 
	'¡ Gracias {nombreAgente} !<br>Has habilitado {numeroLicencias} {licencias} del tipo {tipoLicencia} de la empresa {nombreEmpresa} con éxito.' => 'Thanks {nombreAgente} !<br>You have successfully enabled {numeroLicencias} {licencias} of the {tipoLicencia} kind for the company {nombreEmpresa}.', 
	'Licencias validadas' => 'Validated licenses', 
	'Descuento' => 'Discount', 
	'Reservations' => 'Reservations', 
	'¿ Está seguro de querer borrar esta/s compra/s ?' => 'Are you sure you want to delete this/these purchase(s)?', 
	'lbl: Ok' => 'OK', 
	'lbl: Error' => 'Error', 
	'Browser detection message' => 'We have seen that the version of the browser you are using has not been updated.  In order to browse more easily, we recommend that you update your version. Click http://windows.microsoft.com/en-gb/internet-explorer/download-ie to update it :)', 
	'lbl: Sin definir' => 'Unknown', 
	'Configuración' => 'Settings', 
	'lbl: Resend welcome email' => 'Resend welcome email', 
	'Password must not be empty.' => 'The password field cannot be empty.', 
	'lbl: Purchases validated successfully!' => 'Licenses successfully validated.', 
	'Chinese' => 'Chinese', 
	'lbl: Borrar' => 'Delete', 
);
