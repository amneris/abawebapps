<?php

class SiteTest extends WebTestCase
{

	protected function setUp()
	{
		parent::setUp();
//		$this->setBrowser('*firefox');
//		$this->setBrowser('*chrome');
    $this->setBrowser('googlechrome');
    $this->setBrowserUrl(TEST_BASE_URL);
	}

		// ** Test Login
	protected function doLogin( $user, $pass ) {
//		$this->captureEntirePageScreenshot( '/tmp/screen.png' );

		$this->assertTextPresent('LOGIN');
		$this->type('id=inputUser',$user);
		$this->type('id=inputPass',$pass);
		$this->clickAndWait('id=btnLogin');
	}

	protected function doLoginABAManager() {
		$this->doLogin( 'carles74.test@gmail.com', 'm8f2Ksg8' );
	}

	protected function doLoginAgente() {
		$this->doLogin( 'carles74.test1@gmail.com', 'm8f2Ksg8' );
	}

	protected function doLoginPartner() {
		$this->doLogin( 'carles74.test3@gmail.com', 'm8f2Ksg8' );
	}

	protected function doLoginRepresentante() {
		$this->doLogin( 'carles74.test2@gmail.com', 'm8f2Ksg8' );
	}

	public function testLoginABAManager()
	{
		$this->open( Yii::app()->params['extranetURLDomain']);
		$this->windowMaximize( );
    $this->setSpeed('1');

//		$this->pause( 5000 );

		// ** Login
		$this->doLoginABAManager();

		// ** Veo el listado de empresas.
		$this->assertTextPresent('Organizaciones');

		// ** Logout
		$this->click('id=divUserManu');
		$this->clickAndWait('link=Cerrar sesión');
//		waitForPageToLoad
		$this->assertTextPresent('LOGIN');

//		"Usuario o contraseña incorrectos."
	}

	public function testLoginAgent()
	{
		$this->open( Yii::app()->params['extranetURLDomain']);
		$this->windowMaximize( );

//		$this->pause( 5000 );

		// ** Login
		$this->doLoginAgente();

		// ** Veo el listado de empresas.
		$this->assertTextPresent('Organizaciones');

		// ** Logout
		$this->click('id=divUserManu');
		$this->clickAndWait('link=Cerrar sesión');
//		waitForPageToLoad
		$this->assertTextPresent('LOGIN');

//		"Usuario o contraseña incorrectos."
	}

	public function testLoginPartner()
	{
		$this->open( Yii::app()->params['extranetURLDomain']);
		$this->windowMaximize( );

//		$this->pause( 5000 );

		// ** Login
		$this->doLoginPartner();

		// ** Veo el listado de empresas.
		$this->assertTextPresent('Organizaciones');

		// ** Logout
		$this->click('id=divUserManu');
		$this->clickAndWait('link=Cerrar sesión');
//		waitForPageToLoad
		$this->assertTextPresent('LOGIN');

//		"Usuario o contraseña incorrectos."
	}

	public function testLoginRepresentante()
	{
		$this->open( Yii::app()->params['extranetURLDomain']);
		$this->windowMaximize( );

//		$this->pause( 5000 );

		// ** Login
		$this->doLoginRepresentante();

		// ** Veo la gestion de usuarios
		$this->assertTextPresent('Organizaciones');

		// ** Logout
		$this->click('id=divUserManu');
		$this->clickAndWait('link=Cerrar sesión');
//		waitForPageToLoad
		$this->assertTextPresent('LOGIN');

//		"Usuario o contraseña incorrectos."
	}

  public function testComprarLicencias() {

  }

  public function testPreciosSinIVA() {
    $this->open( Yii::app()->params['extranetURLDomain']);
    $this->windowMaximize( );

    $this->doLoginRepresentante();

      // ** Vamos a la seccion de precios.
    $this->clickAndWait('id=idLiMenuPrices');

      // ** Buscamos el IVA.
    $this->assertTextPresent('Precio con IVA.');

      // ** Cerramos la sesion.
    $this->click('id=divUserManu');
    $this->clickAndWait('link=Cerrar sesión');
  }

  public function testDarDeBajaUsuarioB2BEnCampus() {
    $this->open( Yii::app()->params['intranetURLDomain']);
    $this->windowMaximize( );

      // ** Do login.
//    $this->assertTitleEquals('Back Office AbaEnglish - Login');
    $this->assertTextPresent('Home » Login');
    $this->type('id=LoginForm_username','pilar.cortes@abaenglish.com');
    $this->type('id=LoginForm_password','abaenglish');
    $this->clickAndWait('id=btnLogin');

      // ** Change section
    $this->mouseOver('idBtn_B2B');
    $this->mouseOver('idBtn_B2B_GeneralOptions');
    $this->clickAndWait('id=idBtn_B2B_GeneralOptions_Students');
    $this->assertTextPresent('Home » Extranet » User');

      // ** Test search by mail
    $this->type('name=commonNamespace_models_intranet_ExtranetUserListForm[email]', "carles\n");
    $this->pause( 5000 );
    $this->assertTextPresent('carles74');

  }

	/*
    public function testContact()
    {
      $this->open('?r=site/contact');
      $this->assertTextPresent('Contact Us');
      $this->assertElementPresent('name=ContactForm[name]');

      $this->type('name=ContactForm[name]','tester');
      $this->type('name=ContactForm[email]','tester@example.com');
      $this->type('name=ContactForm[subject]','test subject');
      $this->click("//input[@value='Submit']");
      $this->waitForTextPresent('Body cannot be blank.');
    }

    public function testLoginLogout()
    {
      $this->open('');
      // ensure the user is logged out
      if($this->isTextPresent('Logout'))
        $this->clickAndWait('link=Logout (demo)');

      // test login process, including validation
      $this->clickAndWait('link=Login');
      $this->assertElementPresent('name=LoginForm[username]');
      $this->type('name=LoginForm[username]','demo');
      $this->click("//input[@value='Login']");
      $this->waitForTextPresent('Password cannot be blank.');
      $this->type('name=LoginForm[password]','demo');
      $this->clickAndWait("//input[@value='Login']");
      $this->assertTextNotPresent('Password cannot be blank.');
      $this->assertTextPresent('Logout');

      // test logout process
      $this->assertTextNotPresent('Login');
      $this->clickAndWait('link=Logout (demo)');
      $this->assertTextPresent('Login');
    }
  */
}
