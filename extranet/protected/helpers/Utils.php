<?php
/**
 */
class Utils
{
  
  const _ZeroExpirationDate = '2000-01-01 00:00:00';
  
  /**
   * 
   */
  static public function resetSessionData() {
    Yii::app()->user->setState('homeUrl', '/');
    Yii::app()->user->setState('returnUrl', '/');
    Yii::app()->user->setState('loginUrl', '/user/login');
    Yii::app()->user->setState('logoutUrl', '/user/login/logout');
    Yii::app()->user->setState('recoveryUrl', '/user/login/recovery');
    Yii::app()->user->setState('idCountry', 300);
  }

  /**
   * 
   */
  static public function setSessionVar( $name, $val ) {
    Yii::app()->user->setState($name, $val);
    return $val;
  }
  
  /**
   * 
   */
  static public function getSessionVar( $name, $val = false ) {
    return Yii::app()->user->getState($name, $val);
  }
  
  /**
   * 
   */
  static public function getWsSignature( $params, $securityWord = 'Aba English Extranet Key' ) {
    $joinParams = implode("", $params);
    $joinParams = $securityWord . $joinParams;
//    $joinParams = HeMixed::getWord4Ws().$joinParams;
//file_put_contents('/tmp/extra.log', "Signature0: $joinParams\n", FILE_APPEND);    
    $signature = md5( $joinParams );

    return $signature;
  }
  
  /**
   * 
   */
  static public function soapRequest( $url, $serviceName, $arParams = array() ) {
      ini_set (  'soap.wsdl_cache_enable'  ,  0  );
      ini_set (  'soap.wsdl_cache_ttl'  ,  0  );
      
      $client = new SoapClient( $url , array('soap_version' => SOAP_1_1) );
      $signatureWsAbaEnglish = Utils::getWsSignature( $arParams );
      array_unshift( $arParams, $signatureWsAbaEnglish);
      $answer = call_user_func_array(array($client, $serviceName), $arParams);
      
      return $answer;
  }
 
  static public function change2Enterprise( $idEnterprise ) {
    $isAgentOrManager = ( Utils::getSessionVar('isAgent', false ) || Utils::getSessionVar('isAbaManager', false ) || Yii::app()->user->ispartner );
    if ( $isAgentOrManager ) $idRole = Role::_AGENT; else $idRole = Role::_RESPONSIBLE;

    $criteria = new CDbCriteria();
    $criteria->addCondition('t.id = :idEnterprise');
    $criteria->addCondition('t.isdeleted = :deleted');
    $criteria->params = array(':idEnterprise' => $idEnterprise, ':deleted' => 0 );
    $mEnterprise = Enterprise::model()->find($criteria);
    if (!$mEnterprise) return false;

    $criteria = new CDbCriteria();
    $criteria->addCondition('t.id = :idCountry');
    $criteria->params = array(':idCountry' => $mEnterprise->idCountry);
    $mCountry = Country::model()->find($criteria);
    if (!$mCountry) return false;

    $criteria = new CDbCriteria();
    $criteria->addCondition('t.id = :idCurrency');
    $criteria->params = array(':idCurrency' => $mCountry->ABAIdCurrency);
    $mCurrency = Currencies::model()->find($criteria);
    if (!$mCurrency) return false;

    $criteria = new CDbCriteria();
    $criteria->addCondition('t.id = :idLanguage');
    $criteria->params = array(':idLanguage' => $mEnterprise->idLanguage);
    $mLanguage = Language::model()->find($criteria);
    if (!$mLanguage) return false;

    Utils::setSessionVar('idEnterprise', $mEnterprise->id);
    Utils::setSessionVar('nameEnterprise', $mEnterprise->name);
    Utils::setSessionVar('idPartner', $mEnterprise->idPartner);
    Utils::setSessionVar('enterpriseName', $mEnterprise->name);
    Utils::setSessionVar('enterpriseLogo', (!empty($mEnterprise->urlLogo)) ? $mEnterprise->urlLogo : '' );
    Utils::setSessionVar('idCountry', $mEnterprise->idCountry);
    Utils::setSessionVar('nameCountry', $mCountry->name);
    Utils::setSessionVar('idRole', $idRole);
    Utils::setSessionVar('idCurrency', $mCurrency->id);
    Utils::setSessionVar('decimalPoint', $mCurrency->decimalPoint);
    Utils::setSessionVar('currencySymbol', $mCurrency->symbol);
    Utils::setSessionVar('idLanguageEnterprise', $mLanguage->id);
    Utils::setSessionVar('isoLanguageEnterprise', $mLanguage->iso);

    Yii::app()->user->setState('role', $idRole);
    Yii::app()->user->setState('language', Utils::getSessionVar('idLanguage'));

    return $idEnterprise;
  }

  /**
   * 
   */
  static public function adminCheckedList( $checkedList = '' ) {
    if ( empty($checkedList) ) $checkedList = Yii::app()->request->getParam('checkedList', '0');
    $ar = explode(',', $checkedList );

    $arTmp = array();
    foreach( $ar as $k=>$v ) {
      if ( empty($v) ) continue;
      if ( $v == 'all' ) {
        if ( isset($arTmp[$v]) ) {
          if ( count($arTmp) > 1 ) {
            $arTmp = array( $v => $v );
          } else {
            unset($arTmp[$v]);
          }
        } else $arTmp = array( $v => $v );

        continue;
      }

      if ( !isset($arTmp[$v]) ) $arTmp[$v] = $v;
      else unset($arTmp[$v]);
    }

    return implode( ',', $arTmp );
  }
  
  static public function newExpirationDate( $action, $actualExpirationDate, $relativeTime ) {
    if ( $actualExpirationDate == Utils::_ZeroExpirationDate ) $actualTime = strtotime( date("Y-m-d").' 00:00:00' );
    else $actualTime = strtotime($actualExpirationDate);
    
    $month = (int)($relativeTime / 30);
    $days = (int)($relativeTime % 30);
    if ( $action == 'add' ) {
      $newDate = strtotime("+{$month} month", $actualTime);
      $newDate = mktime( 0,0,0, date("m", $actualTime) + $month, date("d", $actualTime) + $days, date("Y", $actualTime) );
      $expirationDate = date("Y-m-d H:i:s", $newDate);
    } else if ( $action == 'del' ) {
      $newDate = strtotime("-{$month} month", $actualTime);
      $newDate = mktime( 0,0,0, date("m", $actualTime) - $month, date("d", $actualTime) - $days, date("Y", $actualTime) );
      $expirationDate = date("Y-m-d H:i:s", $newDate);
    } else $expirationDate = $actualExpirationDate;
    
    return $expirationDate;
  }
  
  static public function log( $module, $controller, $action ) {
    $idUser = Utils::getSessionVar('id', 0);
    $idEnterprise = Utils::getSessionVar('idEnterprise', 0);
    
    $mLog = new ExtraLogger;
    $mLog->action = $action;
    $mLog->controller = $controller;
    $mLog->idUser = $idUser;
    $mLog->idEnterprise = $idEnterprise;
    $mLog->idModule = $module;
    
    $arDatos = array( 'post' => $_POST, 'get' => $_GET );
    if ( isset($_SESSION) ) $arDatos['session'] = $_SESSION;
    
    $mLog->data = serialize( $arDatos );
    $mLog->save();
  }

  static public function getActualFilters( $filters = array() ) {

    if ( empty($filters) ) $filters = Yii::app()->request->getParam('filters', array());
    if ( !empty($filters) && !is_array($filters) && is_string($filters) ) {
      $tmpfilters = json_decode($filters);
      $filters = array();
      foreach( $tmpfilters as $key => $value ) {
        if ( $value instanceof stdClass ) $filter = array( 'key' => $value->key, 'value' => $value->value );
        else $filter = array( 'key' => 'filter_'.$key, 'value' => $value );
        $filters[] = $filter;
      }
    }
    $arFilters = array();
    foreach( $filters as $k=>$filter ) {
      if ( !is_array($filter)) $filter = array( 'key' => $filter->key, 'value' => $filter->value );

      if ( empty($filter['value']) ) continue;
      if ( substr($filter['key'], 0, strlen('filter_') ) != 'filter_' ) continue;

      $key = substr($filter['key'], strlen('filter_') );
      $arFilters[$key] = $filter['value'];
    }

    return $arFilters;
  }

  static public function toCharset( $str, $from = 'UTF-8', $to = 'UTF-8' ) {
    $str2 = $str;
    if ( $from == 'auto' ) return mb_convert_encoding ( $str , 'auto' );
    else if ( $from == 'UTF-8' ) { $str2 = utf8_decode($str2); $from = 'ISO-8859-1 '; }
    else if ( $from == 'MACINTOSH' || $from == 'ISO-8859-1' ) $str2 = $str2;
    else if ( $from == 'GB2312' && $to = 'UTF-8' ) return $str2;
    else if ( $from == 'GB18030' && $to = 'UTF-8' ) $str2 = $str2;
    else { $str2 = utf8_decode($str2); $from = 'ISO-8859-1 '; }

    try {
      $str2 = @iconv($from, $to, $str2);
    } catch(Exception $e) {
      $str2 = utf8_decode($str);
      $str2 = utf8_encode($str2);
    }

    return $str2;
  }

    static public function getLevelString($idLevel)
    {
        $levels = array(1=>'Beginner', 2=>'Lower intermediate', 3=>'Intermediate', 4=>'Upper intermediate', 5=>'Advanced', 6=>'Business');
        return $levels[$idLevel];
    }

  public static function delTree($dir) {
    if ( empty($dir) ) return;
    $files = array_diff(scandir($dir), array('.','..'));
    foreach ($files as $file) {
      (is_dir("$dir/$file")) ? self::delTree("$dir/$file") : unlink("$dir/$file");
    }
    return rmdir($dir);
  }

}

