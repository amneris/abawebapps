<?php

class SourceTranslationsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout='extranet';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view','admin','create','update'),
				'users'=>array('@'),
			),
			array('allow',
				'actions' => array('export'),
				'expression' => array('SourceTranslationsController','allowOnlyCarles')
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new SourceTranslations;
        $listOfSeccions = SourceTranslations::model()->getSeccionNames();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SourceTranslations']))
		{
			$model->attributes=$_POST['SourceTranslations'];

            $model = $this->trimStings($model);

			if($model->save())
				$this->redirect(array('view','id'=>$model->idsourceTranslations));
		}

		$this->render('create',array(
			'model'=>$model,  'listOfSeccion'=>$listOfSeccions,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
        $listOfSeccions = SourceTranslations::model()->getSeccionNames();

		if(isset($_POST['SourceTranslations']))
		{
			$model->attributes=$_POST['SourceTranslations'];

            $model = $this->trimStings($model);

			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model, 'listOfSeccion'=>$listOfSeccions,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SourceTranslations');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new SourceTranslations('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SourceTranslations']))
			$model->attributes=$_GET['SourceTranslations'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SourceTranslations the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=SourceTranslations::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SourceTranslations $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='source-translations-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    private function trimStings($xModel)
    {
        $xModel->label = trim($xModel->label);
        $xModel->es = trim($xModel->es);
        $xModel->en = trim($xModel->en);
        $xModel->fr = trim($xModel->fr);
        $xModel->it = trim($xModel->it);
        $xModel->pt = trim($xModel->pt);
        $xModel->ca = trim($xModel->ca);

        return $xModel;
    }

	protected function crearFichero( $path, $sectionName, $idioma ) {

		if ( $sectionName == 'app' ) {
			$fileName = $path.'/messages/'.$idioma.'/app.php';
		} else {
			$fileName = $path.'/modules/'.$sectionName.'/messages/'.$idioma.'/'.$sectionName.'.php';
		}

		$filePath = dirname( $fileName );
		$ar = explode("/", $filePath);
		$tmpPath = '';
		foreach( $ar as $dirName ) {
			if ( empty($dirName) ) continue;
			$tmpPath = $tmpPath.'/'.$dirName;

			if ( !is_dir($tmpPath) ) mkdir( $tmpPath );
		}

		$cabecera = "<?php\nreturn array(\n";
		file_put_contents( $fileName, $cabecera );

		$limit = 100;
		$criteria = new CDbCriteria();
		$criteria->addCondition( 't.seccion = :seccion' );
		$criteria->params = array_merge( $criteria->params, array( ':seccion' => $sectionName ) );
		$criteria->limit = $limit;
		$criteria->order = 't.idsourceTranslations';

		$arSecciones = array();
		$page = 0;
		while(true) {
			$tratados = 0;
			$criteria->offset = ($limit * $page++);
			$mlST = SourceTranslations::model()->findAll($criteria);
			foreach($mlST as $mTranslation) {
				$tratados++;
				$label = $mTranslation->label;
				$translation = $mTranslation->$idioma;

				file_put_contents( $fileName, "\t'".str_replace("'", "\'", $label)."' => '".str_replace("'", "\'", $translation)."', \n", FILE_APPEND );
			}

			if ( empty($tratados)) break;
		}

		file_put_contents( $fileName, ");\n", FILE_APPEND );

		return $fileName;
	}

	public function actionExport() {

		$arSecciones = SourceTranslations::getSeccionNames();

		$tmpfname = tempnam('../protected/tmp/', 'stc');
		unlink($tmpfname); mkdir($tmpfname);

		$arFiles = array();
		foreach( $arSecciones as $sectionName ) {
			$sectionName = strtolower($sectionName);

			foreach( Yii::app()->params['languages'] as $idioma => $v ) {
				$idioma = strtolower($idioma);
				$filename = $this->crearFichero( $tmpfname.'/zip', $sectionName, $idioma );
				$arFiles[$filename] = $filename;
			}

		}

		$zip = new ZipArchive();
		$zipFilename = $tmpfname."/translations_".date("Ymd").".zip";
		if ($zip->open($zipFilename, ZipArchive::CREATE)!==TRUE) {
			exit("cannot open <$zipFilename>\n");
		}

		foreach($arFiles as $file ) {
			$fileName = substr( $file, strlen($tmpfname.'/zip') );
			$zip->addFile($file,$fileName);
		}
		$zip->close();


		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.basename($zipFilename).'"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($zipFilename));
		readfile($zipFilename);

		Utils::delTree($tmpfname);
		die;
	}

	static public function allowOnlyCarles() {
		if(Yii::app()->user->id == User::_PKCARLES) return true; else return false;
	}

}
