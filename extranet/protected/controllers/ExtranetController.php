<?php

class ExtranetController extends Controller
{
    public function init()
    {
        parent::init();
        $this->layout = 'extranet';


//    Utils::validateSession();
    }

    public function filters()
    {
        return array(
          'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index','crop', 'addNewLang'),
                'roles' => array(Role::_AGENT, Role::_ABAMANAGER, Role::_PARTNER),
            ),
            array('allow',
//            'actions' => array('error', 'sendmails'),
                'users' => array('*'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {

    }

    public function actionAlumno() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('alumno/index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if (Yii::app()->errorHandler->error['code'] == 403) {
//          $this->redirect('/user/login/logout');
        } else if (Yii::app()->errorHandler->error['code'] == 404) {
//          $this->redirect('/user/login/logout');
        } else if (Yii::app()->errorHandler->error['code'] == 500) {
//          $this->redirect('/user/login/logout');
        } else if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionHelp() {
        $html = $this->renderPartial('help', array(), true);

        echo json_encode(array('ok' => true, 'html' => $html));
    }

    public function actionSendHelp() {
        $html = $this->renderPartial('okhelp', array(), true);

        $message = Yii::app()->request->getParam('message');
        $subject = Yii::app()->request->getParam('subject');

        $mUser = EnterpriseUserRole::getEnterpriseAgent( Utils::getSessionVar('idEnterprise') );
        if ( !$mUser ) {                                    // ** Si es un agente en la seccion enterprise no tendra idEnterprise asignado.
            $mailAgent = User::getAbaManagerMails();

            $nombreAgente = Utils::getSessionVar('name') . ' ' . Utils::getSessionVar('surname');
            $mailCliente = Utils::getSessionVar('userMail');
            $htmlMail = $this->renderPartial('mailHelpAgenteBody', array(   'nombreAgente' => $nombreAgente,
              'mailAgente' => $mailCliente,
              'subject' => $subject,
              'message' => $message,
            ), true, true);

        } else {
            $mailAgent = $mUser->email;

            $nombreCliente = Utils::getSessionVar('name') . ' ' . Utils::getSessionVar('surname');
            $mailCliente = Utils::getSessionVar('userMail');
            $nombreEmpresa = Utils::getSessionVar('enterpriseName');
            $nombrePais = Utils::getSessionVar('nameCountry');
            $htmlMail = $this->renderPartial('mailHelpRepresentanteBody', array(   'nombreCliente' => $nombreCliente,
              'mailCliente' => $mailCliente,
              'nombreEmpresa' => $nombreEmpresa,
              'nombrePais' => $nombrePais,
              'subject' => $subject,
              'message' => $message,
            ), true, true);
        }

//        'Peticion de ayuda.'."\nSubject: ".$subject."\nMessage: ".$message,

        if ( !isset(Yii::app()->params['SupportMail']) ) Yii::app()->params['SupportMail'] = array('cejarque@abaenglish.com');
        $allOk = HeAbaMail::sendEmail( array(
            'subject' => 'Petición Ayuda Extranet',
            'to' => $mailAgent,
            'msg' => $htmlMail,
            'bcc' => Yii::app()->params['SupportMail'],
        ));

        echo json_encode(array('ok' => true, 'html' => $html));
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                    "Reply-To: {$model->email}\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
//	public function actionLogout()
//	{
//      
//      
////		Yii::app()->user->logout();
////		$this->redirect(Yii::app()->homeUrl);
//	}

    /**
     * @return mixed
     */
    private function testCreateUser() {

        $userId = 1;
        $userType = 'renponsible';
        $expirationDate = 'no expire';
        $signatureWsAbaEnglish = Utils::getWsSignature(array($userId, $userType, $expirationDate,));
        $client = new SoapClient('http://extra.aba.int/Wsoapabauser/ws', array('soap_version' => SOAP_1_1));

        $return = $client->createUser($signatureWsAbaEnglish, $userId, $userType, $expirationDate);

        return $return;
    }

    private function testCreateEnterprise() {
        $url = 'http://extra.aba.int/Wsoapabauser/ws';
        $serviceName = 'createEnterprise';
        $arParams = array(
            'countryCode' => 'es',
            'languageCode' => 'es',
            'userName' => 'Luís',
            'userSurname' => 'López',
            'userMail' => 'llopez@viladrau.com',
            'enterpriseName' => 'Aigües de Viladrau',
        );

        return Utils::soapRequest($url, $serviceName, $arParams);
    }

    public function actionTestPurchase() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.isdeleted = :isDeleted');
        $criteria->addCondition('t.validated = :isValidated');
        $criteria->params = array(':isDeleted' => 0, ':isValidated' => false);
        $criteria->order = "created desc";
        $criteria->limit = 1;
        $mPurchase = Purchase::model()->find($criteria);
        $mPurchase->validated = 1;
        $mPurchase->save();

        var_export($mPurchase->getErrors());

        echo "<br>";
        echo "<br>";
        echo "<br>";

        $criteria = new CDbCriteria();
        $criteria->addCondition('t.idEnterprise = :idEnterprise');
        $criteria->addCondition('t.idLicenseType = :idLicenseType');
        $criteria->params = array(':idEnterprise' => $mPurchase->idEnterprise, ':idLicenseType' => $mPurchase->idLicenseType);
        $criteria->limit = 1;
        $mLicenseAvailable = LicenseAvailable::model()->find($criteria);
        if (!$mLicenseAvailable) {
            $mLicenseAvailable = new LicenseAvailable;
            $mLicenseAvailable->idEnterprise = $mPurchase->idEnterprise;
            $mLicenseAvailable->idLicenseType = $mPurchase->idLicenseType;
            $mLicenseAvailable->numLicenses = 0;
        }
        $mLicenseAvailable->numLicenses += $mPurchase->numLicenses;
        $mLicenseAvailable->lastupdated = date("Y-m-d H:i:s");
        $mLicenseAvailable->save();

        var_export($mLicenseAvailable->getErrors());

        echo "<br>";
        echo "<br>";
        echo "<br>";

//        var_export( $mPurchase );
    }

    public function actionMainContact() {
        $this->render('mainContact', array());
    }

    public function actionMainContact2() {

        $html = var_export($_POST, true);

        $url = Yii::app()->params['extranetURLDomain'].'/Wsoapabauser/ws';
        $serviceName = 'createEnterprise';
        $arParams = array(
            'countryCode' => $_POST['pais'],
            'languageCode' => $_POST['idioma'],
            'userName' => $_POST['nombre'],
            'userSurname' => $_POST['apellidos'],
            'userMail' => $_POST['mail'],
            'password' => $_POST['password'],
            'enterpriseName' => $_POST['nombreEmpresa'],
            'partnerId' => 250000,
            'enterpriseType' => 1,
            'codigoPromocion' => '',
        );

        $ret = Utils::soapRequest($url, $serviceName, $arParams);
//file_put_contents('/tmp/extra.log', "Ret: ".  var_export($ret, true)."\n", FILE_APPEND);
        $html = var_export($ret, true);


        echo json_encode(array('ok' => true, 'html' => $html));
    }

    public static function actionMailSent() {
//        file_put_contents('/tmp/extra.log', __CLASS__.'.'.__FUNCTION__.": MailSent \n", FILE_APPEND);
    }

    public function actionSendmails() {

//    $criteria = new CDbCriteria();
//    $criteria->addCondition('t.isdeleted = 0');
//    $criteria->addCondition('t.status = 0');
//    $criteria->params = array();
//    $criteria->limit = 100;
//    $mlSelligentQueue = SelligentQueue::model()->findAll($criteria);
//    if ( $mlSelligentQueue ) echo "Si\n"; else echo "No\n";
//    die;

        HeAbaSelligent::addToQueue('editUser', array('idUser' => '6', 'idEnterprise' => 'ABA English', 'name' => 'Carles', 'surname' => 'Ejarque', 'email' => 'cejarque@abaenglish.com', 'password' => 'mypass', 'isoLanguage' => 'es'), 'ExtranetController::actionMailSent');

        /*
            $mail = new PHPMailer;

        //  $mail->SMTPDebug = 3;                               // Enable verbose debug output

            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.googlemail.com:465';
            $mail->SMTPSecure = "ssl";

            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'carles74.test1@gmail.com';          // SMTP username
            $mail->Password = 'm2NCBwAa';                     // SMTP password
        //    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        //    $mail->Port = 587;                                    // TCP port to connect to

            $mail->From = 'from@example.com';
            $mail->FromName = 'Mailer';
            $mail->addAddress('cejarque@abaenglish.com', 'Carles');     // Add a recipient

            $mail->WordWrap = 50;                                 // Set word wrap to 50 characters
            $mail->isHTML(true);                                  // Set email format to HTML

            $mail->Subject = 'Here is the subject';
            $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            if(!$mail->send()) {
                echo 'Message could not be sent.';
                echo 'Mailer Error: ' . $mail->ErrorInfo;
            } else {
                echo 'Message has been sent';
            }
        */
        /*
            $criteria = new CDbCriteria();
            $criteria->addCondition('t.isdeleted = 0');
            $criteria->addCondition('t.status = 0');
            $criteria->params = array();
            $criteria->limit = 100;
            $mlSelligentQueue = SelligentQueue::model()->findAll($criteria);
            if ( !$mlSelligentQueue ) $mlSelligentQueue = array();

            foreach( $mlSelligentQueue as $mSelligentElement ) {
              HeAbaSelligent::send($mSelligentElement->action, unserialize($mSelligentElement->data), $mSelligentElement->callback);
            }
        */
    }

    public function actionCrop() {
        $x = Yii::app()->request->getParam('x');
        $y = Yii::app()->request->getParam('y');
        $w = Yii::app()->request->getParam('w');
        $h = Yii::app()->request->getParam('h');
        $dstW = $dstH = 150;

        try {
            $src = getenv('AWS_URL_DOMAIN').Utils::getSessionVar('enterpriseLogo');
            $tmpFile = tempnam(Yii::app()->getRuntimePath(), 'im_');
            file_put_contents($tmpFile, fopen($src, 'r'));

            $imgR = imagecreatefrompng($tmpFile);
            $dstR = ImageCreateTrueColor($dstW, $dstH);
            imagesavealpha($dstR, true);
            $trans_colour = imagecolorallocatealpha($dstR, 0, 0, 0, 127);
            imagefill($dstR, 0, 0, $trans_colour);
            imagecopyresampled($dstR, $imgR, 0, 0, $x, $y, $dstW, $dstH, $w, $h);
            imagepng($dstR, $tmpFile, true);

            $mEnterprise = Enterprise::model()->findByPk(Utils::getSessionVar('idEnterprise'));

            $arDatos = HeUtils::uploadImg(array('inputFile' => $tmpFile, 'scaleX'=>155, 'scaleY'=>155, 'folder'=>'companies'));
            $allOk = ($arDatos['resultMessage'] == '');
            HeAmazon::queueFile2Delete($mEnterprise->urlLogo);
            $urlLogo = $mEnterprise->urlLogo = $arDatos['urlImg'];
            Utils::setSessionVar('enterpriseLogo', $mEnterprise->urlLogo);
            $allOk = $allOk && $mEnterprise->save();

            $urlLogo = getenv('AWS_URL_DOMAIN').$urlLogo.'?x='.rand();
        } catch (Exception $e) {
            $allOk = false;
            $urlLogo = '';
        }

        echo json_encode(array('result' => $allOk, 'urlLogo' => $urlLogo ));
    }

    public function actionConfigs() {
        $resultMessage='';
        $idRole = Yii::app()->request->getParam('id', 0);
        if ( $idRole || !Utils::getSessionVar('idEnterprise') ) {
            if ( !Utils::change2Enterprise($idRole) ) {
                $this->redirect( array( Yii::app()->getModule('user')->loginUrl, 'errorMsg' => Yii::t('app', 'Forbidden, access denied') ) );
            }
        }

        //we get the enterprise
        $enterprise = Enterprise::model()->findByPk(Utils::getSessionVar('idEnterprise'));
        //we get a list of responsible candidates to attach to a company like main contact.
        $possibleMainContacts = User::model()->getResponsibleList(Utils::getSessionVar('idEnterprise'));
        //posible roles to assign to new users.
        $mlRoles = Role::model()->getUserCreationAvailableList(Utils::getSessionVar('idRole'));

        $group=new Group('search');
        $period=new Period('search');

        $mlLanguages = Language::model()->findAllByAttributes(array('enabled'=>1));


        if(isset($_POST['enterpriseName']))
        {
            $enterprise->name = $_POST['enterpriseName'];
            if($enterprise->save())
            {
              $enterprise->updateSelligentData();

                $arRoles = Utils::getSessionVar('arRoles', array());
                foreach($arRoles as $idRole => $roles)
                {
                    if($roles['identerprise'] == $enterprise->id)
                    {
                        $roles['enterpriseName']= $enterprise->name;
                        $arRoles[$idRole] = $roles;
                    }
                }
                Utils::setSessionVar('arRoles', $arRoles);
                Utils::setSessionVar('enterpriseName', $enterprise->name);
                $this->redirect(array('configs'));
            }

        }

        if (isset($_FILES['fileName'])) {
          $arDatos = HeUtils::uploadImg(array('scaleX'=>155, 'scaleY'=>155, 'folder'=>'companies'));
          $resultMessage = $arDatos['resultMessage'];
          HeAmazon::queueFile2Delete($enterprise->urlLogo);
          $enterprise->urlLogo = $arDatos['urlImg'];
          Utils::setSessionVar('enterpriseLogo', $enterprise->urlLogo );
          if($enterprise->save()) {
            $enterprise->updateSelligentData();
            $this->redirect(array('configs'));
          }
        }

        if(isset($_POST['mainContact']))
        {
            $enterprise->idMainContact = $_POST['mainContact'];
            if($enterprise->save()) {
              $enterprise->updateSelligentData();
              $this->redirect(array('configs'));
            }

        }

        $this->render('configs', array(
            'enterprise' => $enterprise,
            'group' => $group,
            'period' => $period,
            'mlLanguages' => $mlLanguages,
            'possibleMainContacts'=>$possibleMainContacts,
            'mlRoles'=>$mlRoles,
            'resultMessage'=>$resultMessage,
        ));
    }

    public function actionAddNewLangFile()
    {
        $routesPath = array('app'=>'/messages/',
                            'enterprise'=>'/modules/enterprise/messages/',
                            'followup'=>'/modules/followup/messages/',
                            'group'=>'/modules/group/messages/',
                            'leveltest'=>'/modules/leveltest/messages/',
                            'period'=>'/modules/period/messages/',
                            'price'=>'/modules/price/messages/',
                            'purchase'=>'/modules/purchase/messages/',
                            'student'=>'/modules/student/messages/',
                            'user'=>'/modules/user/messages/'
                            );



        $dropDownProvider = Language::model()->findAll(array('order'=>'name'));
        $list = CHtml::listData($dropDownProvider, 'iso', 'name');

        if(isset($_FILES['fileName']) && isset($_POST['targetLang']))
        {
            $targLang = $_POST['targetLang'];

            if($_FILES['fileName']['type']=='text/csv')
            {
                $tmpName = $_FILES['fileName']['tmp_name'];
                $arColumns = array();
                $arData = array();
                $line = 1;

                if (($fd = fopen($tmpName, "r")) !== FALSE)
                {
                    while (($dataLine = fgetcsv($fd, 0, ";")) !== FALSE)
                    {
                        $number = count($dataLine);

                        if ( $line == 1 )
                        {
                            for ($c=0; $c < $number; $c++)
                            {
                                $value = strtolower($dataLine[$c]);

                                if ( $value == 'seccion' || $value == 'label' || $value == 'translation' )
                                {
                                    $arColumns[$value] = $c;
                                }
                            }

                            if ( count($arColumns) < 3 )
                            {
                                $this->uploadEndWithError( StudentModule::t('Incorrect number of columns.') );
                            }

                        }
                        else
                        {

                            $translationToSave = array(
                                'Seccion' => $dataLine[$arColumns['seccion']],
                                'Label' => $dataLine[$arColumns['label']],
                                'Translation' => $dataLine[$arColumns['translation']],
                            );

                            switch ($translationToSave['Seccion'])
                            {
                                case 'app':
                                    $appData[] = $translationToSave;
                                    break;
                                case 'enterprise':
                                    $enterpriseData[] = $translationToSave;
                                    break;
                                case 'followup':
                                    $followupData[] = $translationToSave;
                                    break;
                                case 'group':
                                    $groupData[] = $translationToSave;
                                    break;
                                case 'leveltest':
                                    $leveltestData[] = $translationToSave;
                                    break;
                                case 'period':
                                    $periodData[] = $translationToSave;
                                    break;
                                case 'price':
                                    $priceData[] = $translationToSave;
                                    break;
                                case 'purchase':
                                    $purchaseData[] = $translationToSave;
                                    break;
                                case 'student':
                                    $studentData[] = $translationToSave;
                                    break;
                                case 'user':
                                    $userData[] = $translationToSave;
                                    break;
                            }

                        }
                        $line++;
                    }
                    fclose($fd);
                    //time to create the files.

                    if(count($appData) > 0)
                    {
                        $this->createOrAddInFile($targLang, $appData, $routesPath['app'],'/app.php');
                    }
                    else
                    {
                        echo("nada para insertar en app.");
                    }

                    if(count($enterpriseData) > 0)
                    {
                        $this->createOrAddInFile($targLang, $enterpriseData, $routesPath['enterprise'],'/enterprise.php');
                    }
                    else
                    {
                        echo("nada para insertar en enterprise.");
                    }

                    if(count($followupData) > 0)
                    {
                        $this->createOrAddInFile($targLang, $followupData, $routesPath['followup'],'/followup.php');
                    }
                    else
                    {
                        echo("nada para insertar en followup.");
                    }

                    if(count($groupData) > 0)
                    {
                        $this->createOrAddInFile($targLang, $groupData, $routesPath['group'],'/group.php');
                    }
                    else
                    {
                        echo("nada para insertar en group.");
                    }

                    if(count($leveltestData) > 0)
                    {
                        $this->createOrAddInFile($targLang, $leveltestData, $routesPath['leveltest'],'/leveltest.php');
                    }
                    else
                    {
                        echo("nada para insertar en leveltest.");
                    }

                    if(count($periodData) > 0)
                    {
                        $this->createOrAddInFile($targLang, $periodData, $routesPath['period'],'/period.php');
                    }
                    else
                    {
                        echo("nada para insertar en period.");
                    }

                    if(count($priceData) > 0)
                    {
                        $this->createOrAddInFile($targLang, $priceData, $routesPath['price'],'/price.php');
                    }
                    else
                    {
                        echo("nada para insertar en price.");
                    }

                    if(count($purchaseData) > 0)
                    {
                        $this->createOrAddInFile($targLang, $purchaseData, $routesPath['purchase'],'/purchase.php');
                    }
                    else
                    {
                        echo("nada para insertar en purchase.");
                    }

                    if(count($studentData) > 0)
                    {
                        $this->createOrAddInFile($targLang, $studentData, $routesPath['student'],'/student.php');
                    }
                    else
                    {
                        echo("nada para insertar en student.");
                    }

                    if(count($userData) > 0)
                    {
                        $this->createOrAddInFile($targLang, $userData, $routesPath['user'],'/user.php');
                    }
                    else
                    {
                        echo("nada para insertar en user.");
                    }

                }
            }
            else
            {
                die('mostrar mensaje de error cunado el tipo de fichero no es csv');
            }
        }
        $this->render('newLang',array('listOfLang'=>$list));
    }

    public function createOrAddInFile($targLang, $theData, $path, $fileName)
    {
        if(!file_exists(Yii::app()->basepath.$path.$targLang))
        {
            mkdir(Yii::app()->basepath.$path.$targLang);
        }
        if(!file_exists(Yii::app()->basepath.$path.$targLang.$fileName))
        {
            //We got some translations, we create the file.
            $appFile = fopen(Yii::app()->basepath.$path.$targLang.$fileName, "w");
            fwrite($appFile, "<?php\nreturn array(\n");
            $totalTranslations = count($theData);
            for($i=0; $i < $totalTranslations; $i++)
            {
                //fwrite($appFile, "\t'".str_replace("'","\\'",$theData[$i]['Label'])."'=>'".str_replace("'","\\'",$theData[$i]['Translation'])."',\n");
                fwrite($appFile, "\t'".$theData[$i]['Label']."'=>'".$theData[$i]['Translation']."',\n");

            }
            fwrite($appFile, ");");
            fclose($appFile);
        }

        else
        {
            die('el fichero existe.');
        }

    }

    public function uploadEndWithError( $errorMsg ) {
        $this->layout = '';

//      $errorMsg = StudentModule::t('Incorrect number of columns.');
        $this->layout = 'basic';
        $html = $this->render('errorUploadFile', array( 'message' => $errorMsg ), true );
        echo $html;
        exit();
    }

    public function actionDownloadlogs() {

      if ( Yii::app()->user->id != User::_PKCARLES ) return;

        $tmppath = Yii::app()->basePath.'/tmp/';
        if ( !is_dir($tmppath) ) mkdir($tmppath);
        $zip = new ZipArchive();
        $zipFilename = $tmppath.'logFiles_'.date("Ymd").".zip";
        if ($zip->open($zipFilename, ZipArchive::CREATE)!==TRUE) {
            exit("cannot open <$zipFilename>\n");
        }

            // ** application.log
        $fileName = Yii::app()->basePath.'/../runtime/application.log';
        $zip->addFile($fileName,basename($fileName));

        $zip->close();

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($zipFilename).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($zipFilename));
        readfile($zipFilename);

        unlink($zipFilename);
        die;
    }

    public function actionRefresSelligentData() {
        $arLanguages = array(
          1 => 'en',
          2 => 'es',
          3 => 'it',
          4 => 'fr',
        );

//        $criteria = new CDbCriteria();
//        $criteria->addCondition('t.isdeleted = 0');
//        $criteria->addCondition('t.id >= 6');
//        $criteria->params = array();
//        $criteria->limit = 1;
//        $criteria->order = 'id';
//        $mlUsers = User::model()->findAll($criteria);
//        foreach( $mlUsers as $mUser ) {
//            $isoLanguage = $arLanguages[$mUser->idLanguage];
//            $criteria = new CDbCriteria();
//            $criteria->addCondition('t.isdeleted = 0');
//            $criteria->addCondition('t.idUser = :idUser');
//            $criteria->addCondition('t.idRole = :idRole');
//            $criteria->params = array( ':idUser' => $mUser->id, ':idRole' => 3 );
//            $mEnterpriseUserRole = EnterpriseUserRole::model()->find($criteria);
//            if ( !$mEnterpriseUserRole ) continue;
//            $data = array(
//              'idUser' => $mUser->id,
//              'idEnterprise' => $mEnterpriseUserRole->idEnterprise,
//              'name' => $mUser->name,
//              'surname' => $mUser->surname,
//              'email' => $mUser->email,
//              'password' => '',
//              'isoLanguage' => $isoLanguage,
//            );
//
//            $allOk = HeAbaSelligent::send( HeAbaSelligent::_EDITUSER, $data );
//            if ( !$allOk ) { file_put_contents('/tmp/extra.log', "Last Error: ".HeAbaSelligent::getLastErrorMsg()."\n", FILE_APPEND); break; }
//            else file_put_contents('/tmp/extra.log', "All OK\n".var_export($data, true)."\n", FILE_APPEND);
//        }

        $refreshEnterpise = Yii::app()->request->getParam('enterprise', 0);
        if ( $refreshEnterpise ) {
          $criteria = new CDbCriteria();
          $criteria->addCondition('t.isdeleted = 0');
          $criteria->addCondition('t.id >= 0');
          $criteria->params = array();
          $criteria->limit = 1000;
          $criteria->order = 'id';
          $mlEnterprise = Enterprise::model()->findAll($criteria);
          foreach( $mlEnterprise as $mEnterprise ) {
            $allOk = $mEnterprise->updateSelligentData();

            if ( !$allOk ) file_put_contents('/tmp/extra.log', "Last Error: ".HeAbaSelligent::getLastErrorMsg()."\n", FILE_APPEND);
            else file_put_contents('/tmp/extra.log', "All OK\n", FILE_APPEND);
          }
        }

//        $criteria = new CDbCriteria();
//        $criteria->addCondition('t.isdeleted = 0');
//        $criteria->addCondition('t.id >= 0');
//        $criteria->params = array();
//        $criteria->limit = 1000;
//        $criteria->order = 'id';
//        $mlStudent = Student::model()->findAll($criteria);
//        foreach( $mlStudent as $mStudent ) {
//
//            $criteria = new CDbCriteria();
////            $criteria->addCondition('t.isdeleted = 0');
//            $criteria->addCondition('t.id = :idEnterprise');
//            $criteria->params = array( ':idEnterprise' => $mStudent->idEnterprise );
//            $mEnterprise = Enterprise::model()->find($criteria);
//            $isoLanguage = $arLanguages[$mEnterprise->idLanguage];
//
//            $data = array(
//              'idStudent' => $mStudent->id,
//              'idEnterprise' => $mStudent->idEnterprise,
//              'name' => $mStudent->name,
//              'surname' => $mStudent->surname,
//              'email' => $mStudent->email,
//              'isoLanguage' => $isoLanguage,
//            );
//
//            $allOk = HeAbaSelligent::send( HeAbaSelligent::_EDITSTUDENT, $data );
//            if ( !$allOk ) file_put_contents('/tmp/extra.log', "Last Error: ".HeAbaSelligent::getLastErrorMsg()."\n", FILE_APPEND);
//            else file_put_contents('/tmp/extra.log', "All OK\n".var_export($data, true)."\n", FILE_APPEND);
//        }

    }

  public function actionDownloadbackups() {

    if ( Yii::app()->user->id != User::_PKCARLES ) return;
    set_time_limit(0);

    $origen = Yii::app()->request->getParam('origen', 'extranet');

    $tmppath = Yii::app()->basePath.'/../runtime/';
    if ( !is_dir($tmppath) ) mkdir($tmppath);

    $tables = '';
    switch($origen) {
      case 'extranet':
        $connectionString = Yii::app()->dbExtranet->connectionString;
        $connectionString = substr( $connectionString, strlen('mysql:') );
        $connectionString = str_replace( ';', '&', $connectionString );

        parse_str($connectionString, $arValues);
        if ( !isset($arValues['port']) ) $arValues['port'] = 3306;

        $port = $arValues['port'];
        $host = $arValues['host'];
        $user = Yii::app()->dbExtranet->username;
        $pass = Yii::app()->dbExtranet->password;
        $database = $arValues['dbname'];

        break;
      case 'aba_b2c':
        $connectionString = Yii::app()->db->connectionString;
        $connectionString = substr( $connectionString, strlen('mysql:') );
        $connectionString = str_replace( ';', '&', $connectionString );

        parse_str($connectionString, $arValues);
        if ( !isset($arValues['port']) ) $arValues['port'] = 3306;

        $port = $arValues['port'];
        $host = $arValues['host'];
        $user = Yii::app()->dbExtranet->username;
        $pass = Yii::app()->dbExtranet->password;
        $database = $arValues['dbname'];

        $tables = 'products_prices country currencies user followup4 payments';

        break;
      case 'redmine':
        $port = '3306';
        $host = '172.16.1.100';
        $user = 'abaread';
        $pass = 'Ab4R3ad!us3RsT';
        $database = 'redmine';

        break;
      default:
        $port = '3306';
        $host = '172.26.110.16';
        $user = 'abaapps';
        $pass = 'abaenglish';
        $database = 'abaenglish_extranet';
        break;
    }
    $filename = date("YmdHis")."_{$database}.dump";
    $command = "/usr/bin/mysqldump -P $port -h $host  -u $user -p'$pass' --single-transaction --quick --opt --skip-lock-tables $database $tables -r {$tmppath}{$filename}";
    exec($command, $return);

    $zip = new ZipArchive();
    $zipFilename = $tmppath.$filename.".zip";
    if ($zip->open($zipFilename, ZipArchive::CREATE)!==TRUE) {
      exit("cannot open <$zipFilename>\n");
    }

    // ** application.log
    $zip->addFile($tmppath.$filename,$filename);

    $zip->close();

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($zipFilename).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($zipFilename));
    readfile($zipFilename);

    unlink($zipFilename);
    unlink($tmppath.$filename);
    die;
  }

  public function actionDeleteLicenseStudent() {
    $arStudents = array(
      array( 'id' => 0, 'email' => 'GREPACYO@JCYL.ES' ),
      array( 'id' => 0, 'email' => 'GARORDSE@JCYL.ES' ),
      array( 'id' => 0, 'email' => 'FERBARFL@JCYL.ES' ),
      array( 'id' => 0, 'email' => 'TECTORPI@JCYL.ES' ),
    );

    foreach( $arStudents as $arStudent ) {
      $emailStudent = $arStudent['email'];

//      update student set idStudentCampus = 0 where id = 1309;
      $mStudent = Student::model()->findByAttributes( array( 'email' => $emailStudent, 'isdeleted' => 0 ) );
      $mStudent->idStudentCampus = 0;
      $mStudent->save();
      $sql = "update student set idStudentCampus = 0 where id = {$mStudent->id}";
     Yii::trace($sql, 'sql');



//      update license_asigned set deleted = now(), isdeleted = 1 where id = 475;
      $mLicense = LicenseAsigned::model()->findByAttributes( array('idStudent' => $mStudent->id, 'isdeleted' => 0 ) );
      $mLicense->deleted = date("Y-m-d");
      $mLicense->isdeleted = 1;
      $mLicense->save();
      $sql = "update license_asigned set deleted = now(), isdeleted = 1 where id = {$mLicense->id};";
      Yii::trace($sql, 'sql');

//      update student_group set idPeriod = 0, expirationDate = '2000-01-01 00:00:00.0' where id = 1309;
      $mGroup = StudentGroup::model()->findByAttributes( array('idStudent' => $mStudent->id, 'isdeleted' => 0 ) );
      $mGroup->idPeriod = 0;
      $mGroup->expirationDate = '2000-01-01 00:00:00.0';
      $mGroup->save();
      $sql = "update student_group set idPeriod = 0, expirationDate = '2000-01-01 00:00:00.0' where id = {$mGroup->id}";
      Yii::trace($sql, 'sql');

      // update aba_b2c.user set expirationDate = now() where id in (3490814, 3490899, 3490887, 3490958);
      $mUserCampus = UserCampus::model()->findByAttributes( array('email' => $emailStudent) );
      $mUserCampus->expirationDate = date("Y-m-d", time() - (7*24*60*60) );
      $mUserCampus->save();

//      UPDATE `aba_b2c`.`payments` SET `amountDiscount`=`amountOriginal`, `amountPrice`=0, `amountTax`=0, `amountPriceWithoutTax`=0 WHERE `id`='b9700ec3' AND userId=1503601 AND paySuppExtId=5;
//      $mPayment = Payment::model()->findByAttributes( array( 'id' => 'ae9ac80d', 'userId' => $mUserCampus->id, 'paySuppExtId' => 5 ) );
//      $mPayment->amountDiscount = $mPayment->amountDiscount;
//      $mPayment->amountPrice = 0;
//      $mPayment->amountTax = 0;
//      $mPayment->amountPriceWithoutTax = 0;
//      $mPayment->save();


    }

  }

    // ** Genera un fichero con los ultimos test de nivel realizados.
    public function actionKpiLastLevelTestDone() {
        $conn = Yii::app()->dbExtranet;

        $sql = "select e.id as idEnterprise, s.name as studentName, s.surname as studentSurname, ltd.completed as dateCompleted, u.id as idUser, s.id as idStudent, ltd.completed as completed, u.name as rname, u.surname as rsurname, slt.created as created, slt.id as idStudentLevelTest, u.autologincode
            from LevelTest_Data ltd, student_level_test slt, student s, enterprise e, user u
            where slt.idLevelTest = ltd.id
              and slt.informe = 0
--              and (ltd.completed != '2000-01-01 00:00:00' or (ltd.completed = '2000-01-01 00:00:00' and ltd.jsonData = '' and ltd.status = 1 ) )
              and slt.idStudent = s.id
              and s.idEnterprise = e.id
              and e.idMainContact = u.id
              
              and slt.isdeleted = 0
              and s.isdeleted = 0
              and ltd.isdeleted = 0
              and e.isdeleted = 0
              and u.isdeleted = 0
              and ltd.expirationDate > now()
            group by s.id
            order by ltd.completed desc";
        $command = $conn->createCommand( $sql );
        $rows = $command->queryAll();
        $arEnterprise = self::agruparPorEmpresa($rows);

        $arEnviados = array();
        $file = Yii::app()->getRuntimePath().'/selligent.dailyleveltest.csv';
        $fp = fopen( $file, 'w');
        if ( $fp ) {
            $header = false;
            foreach( $arEnterprise as $arTipos ) {
                $contador_AP = 0;
                $contador_NAP = 0;
                $arFields = array(
                  'idEnterprise' => '',
                  'idUser' => '',
                  'nombreRepresentante' => '',
                  'NOMBRESTUDENTAP1' => '', 'FECHAAP1' => '', 'NOMBRESTUDENTAP2' => '', 'FECHAAP2' => '', 'NOMBRESTUDENTAP3' => '', 'FECHAAP3' => '', 'NOMBRESTUDENTAP4' => '', 'FECHAAP4' => '', 'NOMBRESTUDENTAP5' => '', 'FECHAAP5' => '', 'NOMBRESTUDENTAP6' => '', 'FECHAAP6' => '', 'NOMBRESTUDENTAP7' => '', 'FECHAAP7' => '', 'NOMBRESTUDENTAP8' => '', 'FECHAAP8' => '', 'NOMBRESTUDENTAP9' => '', 'FECHAAP9' => '', 'NOMBRESTUDENTAP10' => '', 'FECHAAP10' => '',
                  'NOMBRESTUDENTAP11' => '', 'FECHAAP11' => '', 'NOMBRESTUDENTAP12' => '', 'FECHAAP12' => '', 'NOMBRESTUDENTAP13' => '', 'FECHAAP13' => '', 'NOMBRESTUDENTAP14' => '', 'FECHAAP14' => '', 'NOMBRESTUDENTAP15' => '', 'FECHAAP15' => '', 'NOMBRESTUDENTAP16' => '', 'FECHAAP16' => '', 'NOMBRESTUDENTAP17' => '', 'FECHAAP17' => '', 'NOMBRESTUDENTAP18' => '', 'FECHAAP18' => '', 'NOMBRESTUDENTAP19' => '', 'FECHAAP19' => '', 'NOMBRESTUDENTAP20' => '', 'FECHAAP20' => '',
                  'NOMBRESTUDENTNAP1' => '', 'FECHANAP1' => '', 'NOMBRESTUDENTNAP2' => '', 'FECHANAP2' => '', 'NOMBRESTUDENTNAP3' => '', 'FECHANAP3' => '', 'NOMBRESTUDENTNAP4' => '', 'FECHANAP4' => '', 'NOMBRESTUDENTNAP5' => '', 'FECHANAP5' => '', 'NOMBRESTUDENTNAP6' => '', 'FECHANAP6' => '', 'NOMBRESTUDENTNAP7' => '', 'FECHANAP7' => '', 'NOMBRESTUDENTNAP8' => '', 'FECHANAP8' => '', 'NOMBRESTUDENTNAP9' => '', 'FECHANAP9' => '', 'NOMBRESTUDENTNAP10' => '', 'FECHANAP10' => '',
                  'NOMBRESTUDENTNAP11' => '', 'FECHANAP11' => '', 'NOMBRESTUDENTNAP12' => '', 'FECHANAP12' => '', 'NOMBRESTUDENTNAP13' => '', 'FECHANAP13' => '', 'NOMBRESTUDENTNAP14' => '', 'FECHANAP14' => '', 'NOMBRESTUDENTNAP15' => '', 'FECHANAP15' => '', 'NOMBRESTUDENTNAP16' => '', 'FECHANAP16' => '', 'NOMBRESTUDENTNAP17' => '', 'FECHANAP17' => '', 'NOMBRESTUDENTNAP18' => '', 'FECHANAP18' => '', 'NOMBRESTUDENTNAP19' => '', 'FECHANAP19' => '', 'NOMBRESTUDENTNAP20' => '', 'FECHANAP20' => '',
                  'TOTALAP' => 0,
                  'TOTALNAP' => 0,
                  'dia' => date("Y-m-d H:i:s"),
                  'link' => '',
                );
                foreach ($arTipos as $tipo => $rows) {

                    foreach ($rows as $row) {
                      $arFields['idEnterprise'] = $row['idEnterprise'];
                      $arFields['idUser'] = $row['idUser'];
                      $arFields['link'] = $row['autologincode'];
                      $arFields['nombreRepresentante'] = utf8_encode(utf8_decode($row['rname'] . ' ' . $row['rsurname']));

                      if ( $tipo == 1 ) {
                        $contador_AP++;
                        if ( $contador_AP <= 20 ) {
                          $arFields['FECHAAP'.$contador_AP] = $row['dateCompleted'];
                          $arFields['NOMBRESTUDENTAP'.$contador_AP] = utf8_encode(utf8_decode($row['studentName'] . ' ' . $row['studentSurname']));
                        }
                      } else {
                        if ( empty($contador_AP) ) break;
                        $contador_NAP++;
                        if ( $contador_NAP <= 20 ) {
                          $arFields['FECHANAP' . $contador_NAP] = $row['created'];
                          $arFields['NOMBRESTUDENTNAP' . $contador_NAP] = utf8_encode(utf8_decode($row['studentName'] . ' ' . $row['studentSurname']));
                        }
                      }

                      $arEnviados[$row['idStudent']] = $row['idStudentLevelTest'];
                    }
                }

                if ( !empty($contador_AP) ) {
                    $arFields['TOTALAP'] = $contador_AP;
                    $arFields['TOTALNAP'] = $contador_NAP;

                    if (!$header) {
                        $header = true;
                        fputcsv($fp, array_keys($arFields));
                    }
                    fputcsv($fp, $arFields);
                }
            }

            fclose($fp);
        }

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($file).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);

            // ** Marcamos todos aquellos que se han enviado como eso, ya enviados.
        foreach( $arEnviados as $idStudent => $idLevelTest ) {
          $criteria = new CDbCriteria();
          $criteria->addCondition( ' idStudent = :idStudent ' );
          $criteria->addCondition( ' informe = :informe ' );
          $criteria->params = array( ':idStudent' => $idStudent, ':informe' => 0 );

          StudentLevelTest::model()->updateAll( array(
            'informe' => 1
          ), $criteria );
        }

        unlink($file);
        die;
    }

    static public function agruparPorEmpresa( $rows ) {
        $arFinal = array();
        foreach( $rows as $row ) {
            $tipo = ( $row['completed'] == '2000-01-01 00:00:00' ) ? 2 : 1;

            $row['dateCompleted'] = ( $tipo == 1 ) ? $row['dateCompleted'] : $row['created'];

            $arFinal[ $row['idEnterprise'] ][$tipo][] = $row;
        }

        return $arFinal;
    }

    public function actionImgLocal2Amazon() {
        $mlEnterprise = Enterprise::model()->findAll();
        foreach($mlEnterprise as $mEnterprise) {
            if ($mEnterprise->urlLogo) continue;
            $file = Yii::app()->basePath."/../public/images/enterpriseProfile/profilePicEnterprise_".$mEnterprise->id.".png";
            if (file_exists($file)) {
                $arDatos = HeUtils::uploadImg(array('inputFile' => $file, 'scaleX'=>155, 'scaleY'=>155, 'folder'=>'companies'));
                $allOk = ($arDatos['resultMessage'] == '');
                $mEnterprise->urlLogo = $arDatos['urlImg'];
                $allOk && $mEnterprise->save();
            }
        }

        $mlUser = User::model()->findAll();
        foreach( $mlUser as $mUser) {
            if ($mUser->urlAvatar) continue;
            $file = Yii::app()->basepath."/../public/images/userProfile/profilePicUser_".$mUser->id.".png";
            if (file_exists($file)) {
                $arDatos = HeUtils::uploadImg(array('inputFile' => $file, 'scaleX'=>150, 'scaleY'=>150, 'folder'=>'users'));
                $allOk = ($arDatos['resultMessage'] == '');
                $mUser->urlAvatar = $arDatos['urlImg'];
                $allOk && $mUser->save();
            }
        }
    }

    public function actionPaypallist() {
        $lastUserid = 0;
        file_put_contents('/tmp/extra.log', "Last user: {$lastUserid}"."\n", FILE_APPEND);
        echo "Hola.<br>\n";
        $value = file_get_contents('/tmp/extra.log');
        echo "$value<br>\n";
        set_time_limit(0);
        $filename = '/tmp/payments.csv';

        $connection = new \yii\db\Connection([
          'dsn' => 'mysql:host=172.16.1.68;dbname=aba_b2c',
          'username' => 'abaread',
          'password' => 'AbaWebReadEnglish',
        ]);
        $connection->open();

        $fd = fopen($filename, 'w');
        if ( $fd ) {
            $row = array(
              'idUser',
              'User email',
              'Country',
              'Plan',
              'Periodo',
              'Price',
              'Start date',
              'End date'
            );

            fputcsv($fd, $row);
//            fputcsv($fd, array(4104687,'akorotkov.01@abaenglish.com','Spain','199_360',139.990000000,"2015-10-16 14:05:07","2016-10-10 00:00:00"));
            fputcsv($fd, array(0,'akorotkov.01@abaenglish.com','Spain','199_360',139.990000000,"2015-10-16 14:05:07","2016-10-10 00:00:00"));

            fclose($fd);
        }

        $lastUserid = 0;
        $fd = fopen($filename, 'r');
        if ( $fd ) {
            while( !feof($fd) ) {
                $row = fgetcsv($fd);
                $lastUserid = (int)$row[0];
            }
            fclose($fd);
        }
        file_put_contents('/tmp/extra.log', "Last user: {$lastUserid}"."\n", FILE_APPEND);
//return;

        $sql = "select userid, idProduct, dateStartTransaction, status, paySuppExtId, idPayControlCheck from aba_b2c.payments where paySuppExtId = 2 and paySuppExtProfId != '' and status in (0,30) and dateStartTransaction < '2010-01-01'";
        $sql = "select userid, idProduct, dateStartTransaction, status, paySuppExtId, idPayControlCheck, (amountPriceWithoutTax + amountTax) amount from aba_b2c.payments where paySuppExtId = 2 and isRecurring = true and status = 0 and userid > $lastUserid order by userid";
        $command = $connection->createCommand($sql);
        $rows = $command->queryAll();

        $count = 0;
        foreach( $rows as $row ) {
            $userid = $row['userid'];
            $idProduct = $row['idProduct'];
            $idPayControlCheck = $row['idPayControlCheck'];

            $time = explode('_', $idProduct);
            $countryId = $time[0];
            $period = $time[1];
            $time = date( "Y-m-d H:i:s", mktime(0,0,0, date("m"), date("d") - $period, date("Y") ));

            $sql = "select userid, idProduct, dateStartTransaction, status, paySuppExtId from aba_b2c.payments where userid = $userid and paySuppExtId = 2 and idProduct = '$idProduct' order by dateStartTransaction desc";
            $sql = "select p.userid, p.idProduct, p.dateStartTransaction, p.status, p.paySuppExtId, c.name countryName, u.email from aba_b2c.payments p, aba_b2c.country c, aba_b2c.user u where p.idPayControlCheck = '$idPayControlCheck' and p.dateStartTransaction > '$time' and p.userid = u.id and p.idCountry = c.id order by p.dateStartTransaction desc limit 1";
            $command = $connection->createCommand($sql);
            $row2 = $command->queryRow();


//            $fd = fopen($filename, 'a');
//            if ( !($count++ % 1000) ) fputcsv($fd, array($userid, '-', '-', '-', '-', '-', '-'));
//            fclose($fd);

            if ( !($count++ % 1000) ) file_put_contents('/tmp/extra.log', var_export($row2, true)."\n", FILE_APPEND);

            if ( !$row2 ) continue;
            if ( $row2['status'] != 30 ) continue;
            if ( $row2['email'] == 'student+'.$userid.'@abaenglish.com' ) continue;
            $startTime = strtotime( $row2['dateStartTransaction'] );
            $time2 = date("Y-m-d H:i:s", mktime(0,0,0, date("m", $startTime), date("d", $startTime)+$period, date("Y", $startTime) ) );

            $row3 = array(
              $userid,
              $row2['email'],
              $row2['countryName'],
              $row['idProduct'],
              $period,
              $row['amount'],
              $row2['dateStartTransaction'],
              $time2
            );

            $fd = fopen($filename, 'a');
            fputcsv($fd, $row3);
            fclose($fd);

//            foreach( $rows2 as $row2 ) {
//                echo var_export($row, true)."<br>\n";
//                echo var_export($row2, true) . "<br>\n";
//            }
//            if ($count++ > 10) break;
        }
    }
}
