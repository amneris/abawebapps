<?php

class SelligentemulatorController extends Controller {

//  public function actionEdituser($idUser, $idEnterprise, $name, $surname, $email) {
  public function actionEdituser() {
    $idUser = Yii::app()->request->getParam('idUser');
    $idEnterprise = Yii::app()->request->getParam('idEnterprise');
    $name = Yii::app()->request->getParam('name');
    $surname = Yii::app()->request->getParam('surname');
    $email = Yii::app()->request->getParam('email');
    $pass_tmp = Yii::app()->request->getParam('pass_tmp');
    $pref_lang = Yii::app()->request->getParam('pref_lang');
    
    $msg = '';
    $msg .= 'POST: '.var_export($_POST, true)."<br>\n";
    $msg .= 'GET: '.var_export($_GET, true)."<br>\n";
    
    $msg .= "idUser: $idUser;<br>\n";
    $msg .= "idEnterprise: $idEnterprise;<br>\n";
    $msg .= "name: $name;<br>\n";
    $msg .= "surname: $surname;<br>\n";
    $msg .= "email: $email;<br>\n";
    $msg .= "pass_tmp: $pass_tmp;<br>\n";
    $msg .= "pref_lan: $pref_lang;<br>\n";
    HeAbaMail::sendEmailInternalTest( array('msg' => $msg) );
  }

  public function actionEditstudent() {
    $idStudent = Yii::app()->request->getParam('idStudent');
    $idEnterprise = Yii::app()->request->getParam('idEnterprise');
    $name = Yii::app()->request->getParam('name');
    $surname = Yii::app()->request->getParam('surname');
    $email = Yii::app()->request->getParam('email');
    $pref_lang = Yii::app()->request->getParam('pref_lang');
    
    $msg = '';
    $msg .= 'POST: '.var_export($_POST, true)."<br>\n";
    $msg .= 'GET: '.var_export($_GET, true)."<br>\n";
    
    $msg .= "idStudent: $idStudent;<br>\n";
    $msg .= "idEnterprise: $idEnterprise;<br>\n";
    $msg .= "name: $name;<br>\n";
    $msg .= "surname: $surname;<br>\n";
    $msg .= "email: $email;<br>\n";
    $msg .= "pref_lan: $pref_lang;<br>\n";
    HeAbaMail::sendEmailInternalTest( array('msg' => $msg) );
  }
  
  public function actionEditEnterprise() {
    $idEnterprise = Yii::app()->request->getParam('idEnterprise');
    $name = Yii::app()->request->getParam('name');
    $idUser = Yii::app()->request->getParam('idMainContact');
    $idPartnerEnterprise = Yii::app()->request->getParam('idChannel');
    
    $msg = '';
    $msg .= 'POST: '.var_export($_POST, true)."<br>\n";
    $msg .= 'GET: '.var_export($_GET, true)."<br>\n";
    
    $msg .= "idEnterprise: $idEnterprise;<br>\n";
    $msg .= "name: $name;<br>\n";
    $msg .= "idUser: $idUser;<br>\n";
    $msg .= "idPartnerEnterprise: $idPartnerEnterprise;<br>\n";
    HeAbaMail::sendEmailInternalTest( array('msg' => $msg) );
  }
  
  public function actionsendLevelTest() {
    $idEnterprise = Yii::app()->request->getParam('idEnterprise');
    $name = Yii::app()->request->getParam('name');
    $idUser = Yii::app()->request->getParam('idUser');
    $idPartnerEnterprise = Yii::app()->request->getParam('idChannel');
    
    $msg = '';
    $msg .= 'POST: '.var_export($_POST, true)."<br>\n";
    $msg .= 'GET: '.var_export($_GET, true)."<br>\n";
    
    $msg .= "idEnterprise: $idEnterprise;<br>\n";
    $msg .= "name: $name;<br>\n";
    $msg .= "idUser: $idUser;<br>\n";
    $msg .= "idPartnerEnterprise: $idPartnerEnterprise;<br>\n";
    HeAbaMail::sendEmailInternalTest( array('msg' => $msg) );
  }
  
}
