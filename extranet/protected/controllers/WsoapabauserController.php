<?php

/**
 * !! IMPORTANT, DO NOT MODIFY THE PHP DOC COMMENTS ON THE HEADER OF EVERY FUNCTION.
 * IT IS USED BY Yii IN ORDER TO CREATE WSDL.FORBIDDEN!
 */
class WsoapabauserController extends WsController {

  static private $_version = "0.1";
  static private $_aHistoryVersion = array("0.1" => "2014-10-14: Initial user service.",
      "1.0" => "2014-10-15: First operational version.",
  );

  public function actions() {
    return array(
        'ws' => array(
            'class' => 'CWebServiceAction',
        ),
    );
  }

  /** Returns the version of this API. element one is version and second is description
   *
   * @return array The array has format [version,description]
   * @soap
   */
  public function version() {
    return $this->retSuccessWs(array("version" => self::$_version,
                "description" => self::$_aHistoryVersion[self::$_version]));
  }

  /**
   * @param CWebService $service
   *
   * @return bool
   */
  public function afterWebMethod($service)
  {
    $bResult = parent::afterWebMethod($service);

    // ** Mentres no tingui acces als logs ... per a no estar demanant-li a cada moment al Dani
    $toLog = array(
      'request' => var_export($this->moLogWs->paramsRequest, true),
      'response' => var_export( $this->aResponse, true ),
      'date' => date("Y-m-d H:i:s"),
    );

    if ( $this->isSuccess ) $subjectMail = '(Sistema) Verificacio WebService '.$this->nameService;
    else $subjectMail = '(Sistema) Error WebService '.$this->nameService;

    HeAbaMail::sendEmailInternalTest( array(
      'subject' => $subjectMail,
      'to' => array( array( 'name' => 'Carles', 'mail' => 'cejarque@abaenglish.com' ) ),
      'bcc' => array( array( 'name' => 'Carles', 'mail' => 'carles74.feina.test@gmail.com' ) ),
      'msg' => var_export( $toLog, true ),
    ));

    return $bResult;
  }

  /* !! IMPORTANT, DO NOT MODIFY THE PHP DOC COMMENTS ON THE HEADER OF EVERY FUNCTION. *************
   * IT IS USED BY Yii IN ORDER TO CREATE WSDL. FORBIDDEN! ***************************************
   */

  /** Sends data to selligent and returns true if success.
   *
   * @param string $signature
   * @param string $countryCode
   * @param string $languageCode
   * @param string $userName
   * @param string $userSurname
   * @param string $userMail
   * @param string $password
   * @param string $enterpriseName
   * @param int $partnerId
   * @param int $enterpriseType
   *
   * @return array|bool
   * @soap
   */
  public function createEnterprise($signature, $countryCode, $languageCode, $userName, $userSurname, $userMail, $password, $enterpriseName, $partnerId, $enterpriseType) {
      // ** Signature
    $signatureWsAbaEnglish = Utils::getWsSignature( array( $countryCode, $languageCode, $userName, $userSurname, $userMail, $password, $enterpriseName, $partnerId, $enterpriseType) );
    if ( $signature != $signatureWsAbaEnglish ) return $this->retErrorWs("502");
//return $this->retSuccessWs(array("result" => "success", "details" => "All Ok", 'redirectUrl' => Yii::app()->params['extranetURLDomain'] ));

      // ** Country
    $criteria = new CDbCriteria();
    $criteria->addCondition('t.id = :code', 'OR');
    $criteria->addCondition('t.iso = :code', 'OR');
    $criteria->addCondition('t.iso3 = :code', 'OR');
    $criteria->params = array(':code' => $countryCode);
    $mCountry = Country::model()->find($criteria);
    if (!$mCountry) return $this->retErrorWs("513");
    
      // ** Language
    $criteria = new CDbCriteria();
    $criteria->addCondition('t.iso = :code');
    $criteria->params = array(':code' => $languageCode);
    $mLanguage = Language::model()->find($criteria);
    if (!$mLanguage) return $this->retErrorWs("501", "Language is not valid.");
    
      // ** Enterprise
      // ** Se pueden crear diferentes empresas con el mismo nombre (Fuente: Miguel).
//    $criteria = new CDbCriteria();
//    $criteria->addCondition('t.name = :name');
//    $criteria->addCondition('t.isdeleted = :deleted');
//    $criteria->params = array(':name' => $enterpriseName, ':deleted' => 0);
//    $mEnterprise = Enterprise::model()->find($criteria);
//    if ($mEnterprise) return $this->retErrorWs("501", "Enterprise already exists.");
    
      // ** Mail
    if ( empty($userMail) ) return $this->retErrorWs("507");
    if ( !HeString::isValidEmail($userMail) ) return $this->retErrorWs("501", "Mail is not valid.");
    
      // ** User already exists
    $criteria = new CDbCriteria();
    $criteria->addCondition('t.email = :mail');
    $criteria->addCondition('t.isdeleted = :deleted');
    $criteria->params = array(':mail' => $userMail, ':deleted' => 0);
    $mUser = User::model()->find($criteria);
    if ($mUser) return $this->retErrorWs("506", "User already exists.");
    
    $transaction = Yii::app()->dbExtranet->beginTransaction();
    try {
    
      $mUser = new User;
      $mUser->name = $userName;
      $mUser->surname = $userSurname;
      $mUser->email = $userMail;
      $mUser->idLanguage = $mLanguage->id;
      $mUser->password = User::getNewPassword($password);
      $allOk = $mUser->save();
      if ( !$allOk ) {
        $transaction->rollback();
        return $this->retErrorWs("506", var_export($mUser->getErrors(), true) );
      }
      $userCode = $mUser->autologincode;
      
      $mEnterprise = new Enterprise;
      $mEnterprise->name = $enterpriseName;
      $mEnterprise->idMainContact = $mUser->id;
      $mEnterprise->idLanguage = $mLanguage->id;
      $mEnterprise->idCountry = $mCountry->id;
      $mEnterprise->idType = $enterpriseType;
      $mEnterprise->idPartner = Enterprise::idPartnerNextVal($enterpriseName);
      $mEnterprise->idPartnerEnterprise = $partnerId;
      $allOk = $mEnterprise->save();
      if ( !$allOk ) {
        $transaction->rollback();
        return $this->retErrorWs("506", var_export($mEnterprise->getErrors(), true) );
      }

      $mUserRole = new EnterpriseUserRole;
      $mUserRole->idRole = Role::_RESPONSIBLE;
      $mUserRole->idEnterprise = $mEnterprise->id;
      $mUserRole->idUser = $mUser->id;
      $allOk = $mUserRole->save();
      if ( !$allOk ) {
        $transaction->rollback();
        return $this->retErrorWs("506", var_export($mUserRole->getErrors(), true) );
      }
      
        // ** Seleccionamos el agente de la empresa segun el pais.
      $idAgent = User::getAgentByCountry( $mCountry->iso );
      $mUserRole = new EnterpriseUserRole;
      $mUserRole->idRole = Role::_AGENT;
      $mUserRole->idEnterprise = $mEnterprise->id;
      $mUserRole->idUser = $idAgent;
      $allOk = $mUserRole->save();
      if ( !$allOk ) {
        $transaction->rollback();
        return $this->retErrorWs("506", var_export($mUserRole->getErrors(), true) );
      }
      
        // ** Licencia de prueba
      $mLicenseAvailable = new LicenseAvailable;
      $mLicenseAvailable->idEnterprise = $mEnterprise->id;
      $mLicenseAvailable->idLicenseType = LicenseType::_Trial;
      $mLicenseAvailable->numLicenses = 1;
      $mLicenseAvailable->histLicenses = 1;
      $mLicenseAvailable->lastupdated = date("Y-m-d H:i:s");
      $allOk = $mLicenseAvailable->save();
      if ( !$allOk ) {
        $transaction->rollback();
        return $this->retErrorWs("506", var_export($mLicenseAvailable->getErrors(), true) );
      }

        // ** Creamos el usuario tambien como alumno.
      $allOk = ModuleStudentModel::insertStudent( $mEnterprise->id, $userName, $userSurname, $userMail, '-', $mLanguage->iso, false );
      if ( !$allOk ) {
        $transaction->rollback();
        return $this->retErrorWs("506", var_export( Yii::app()->getModule('student')->t('No se pudo crear el alumno.') , true) );
      }

      HeAbaMail::sendEmail( array(
          'action' => HeAbaSelligent::_EDITUSER,
          'idUser' => $mUser->id, 
          'idEnterprise' => $mEnterprise->id, 
          'name' => $mUser->name, 
          'surname' => $mUser->surname, 
          'email' => $mUser->email, 
          'password' => $password, 
          'isoLanguage' => $mLanguage->iso,
          'callback' => 'ExtranetController::actionMailSent',
      ) );

      $mEnterprise->updateSelligentData();

      $transaction->commit();

    } catch(Exception $e) {
      $transaction->rollback();
      return $this->retErrorWs("506", var_export($e->getMessage(), true) );
    }

    return $this->retSuccessWs(array("result" => "success", "details" => "All Ok", 'redirectUrl' => Yii::app()->params['extranetURLDomain'].'/user/login/login/code/'.$userCode.'/idChannel/'.$partnerId));
  }

}
