<?php
class ExternalController extends WsController
{
	CONST SECRET_WSDL_KEY_EXTRANET = 'Aba English Extranet Key';

	/**
	 * Replaces Website service Aba_Wsdl::createEnterprise()
	 */
	public function actionSignup()
	{
		//TODO: @abaenglish: Hides irritating PHP notice
		error_reporting(E_ALL ^ E_NOTICE);

		$this->layout = "none";

		$postArgs = HeMixed::getPostArgs();

		// TODO: @abaenglish: Implement com security issues with signature
		//$signature = $postArgs['signature'];
		$languageCode = $postArgs['langEnv'];
		$promocode = empty($postArgs['promocode'])? '' : $postArgs['promocode'];
		$enterpriseName = $postArgs['enterpriseName'];
		$enterpriseType = $postArgs['enterpriseType'];
		$userName = $postArgs['name'];
		$userSurname = $postArgs['surname'];
		$userMail = $postArgs['email'];
		$password = $postArgs['password'];
		// TODO: @abaenglish: Store phone in Enterprise model (http://redmine.abaenglish.com/issues/6227)
		//$phone = $postArgs['phone'];
		$countryCode = $postArgs['countryCode'];
		$partnerId = empty($postArgs['partnerId'])? '250001' : $postArgs['partnerId'];

		// ** Country
		$criteria = new CDbCriteria();
		$criteria->addCondition('t.id = :code', 'OR');
		$criteria->addCondition('t.iso = :code', 'OR');
		$criteria->addCondition('t.iso3 = :code', 'OR');
		$criteria->params = array(':code' => $countryCode);
		$mCountry = Country::model()->find($criteria);

		if (!$mCountry) { echo $this->retErrorWs("513"); return; }

		// ** Language
		$criteria = new CDbCriteria();
		$criteria->addCondition('t.iso = :code');
		$criteria->params = array(':code' => $languageCode);
		$mLanguage = Language::model()->find($criteria);
		if (!$mLanguage) { echo $this->retErrorWs("501", "Language is not valid."); return; }

		// ** Enterprise
		// ** Se pueden crear diferentes empresas con el mismo nombre (Fuente: Miguel).
//    $criteria = new CDbCriteria();
//    $criteria->addCondition('t.name = :name');
//    $criteria->addCondition('t.isdeleted = :deleted');
//    $criteria->params = array(':name' => $enterpriseName, ':deleted' => 0);
//    $mEnterprise = Enterprise::model()->find($criteria);
//    if ($mEnterprise) $this->retErrorWs("501", "Enterprise already exists.");

		// ** Mail
		if ( empty($userMail) ) { echo $this->retErrorWs("507"); return; }
		if ( !HeString::isValidEmail($userMail) ) { echo $this->retErrorWs("501", "Mail is not valid.");  return; }

		// ** User already exists
		$criteria = new CDbCriteria();
		$criteria->addCondition('t.email = :mail');
		$criteria->addCondition('t.isdeleted = :deleted');
		$criteria->params = array(':mail' => $userMail, ':deleted' => 0);
		$mUser = User::model()->find($criteria);

		if ($mUser) { echo $this->retErrorWs("506", "User already exists."); return; }

		$transaction = Yii::app()->dbExtranet->beginTransaction();

		try {

			$mUser = new User;
			$mUser->name = $userName;
			$mUser->surname = $userSurname;
			$mUser->email = $userMail;
			$mUser->idLanguage = $mLanguage->id;
			$mUser->password = User::getNewPassword($password);
			$allOk = $mUser->save();
			if ( !$allOk ) {
				$transaction->rollback();
				echo $this->retErrorWs("506", var_export($mUser->getErrors(), true) );
				return;
			}
			$userCode = $mUser->autologincode;

			$mEnterprise = new Enterprise;
			$mEnterprise->name = $enterpriseName;
			$mEnterprise->idMainContact = $mUser->id;
			$mEnterprise->idLanguage = $mLanguage->id;
			$mEnterprise->idCountry = $mCountry->id;
			$mEnterprise->idType = $enterpriseType;
			$mEnterprise->idPartner = Enterprise::idPartnerNextVal($enterpriseName);
			$mEnterprise->idPartnerEnterprise = $partnerId;
			$allOk = $mEnterprise->save();
			if ( !$allOk ) {
				$transaction->rollback();
				echo $this->retErrorWs("506", var_export($mEnterprise->getErrors(), true) );
				return;
			}

			$mUserRole = new EnterpriseUserRole;
			$mUserRole->idRole = Role::_RESPONSIBLE;
			$mUserRole->idEnterprise = $mEnterprise->id;
			$mUserRole->idUser = $mUser->id;
			$allOk = $mUserRole->save();
			if ( !$allOk ) {
				$transaction->rollback();
				echo $this->retErrorWs("506", var_export($mUserRole->getErrors(), true) );
				return;
			}

			// ** Seleccionamos el agente de la empresa segun el pais.
			$idAgent = User::getAgentByCountry( $mCountry->iso );
			$mUserRole = new EnterpriseUserRole;
			$mUserRole->idRole = Role::_AGENT;
			$mUserRole->idEnterprise = $mEnterprise->id;
			$mUserRole->idUser = $idAgent;
			$allOk = $mUserRole->save();
			if ( !$allOk ) {
				$transaction->rollback();
				echo $this->retErrorWs("506", var_export($mUserRole->getErrors(), true) );
				return;
			}

			// ** Licencia de prueba
			$mLicenseAvailable = new LicenseAvailable;
			$mLicenseAvailable->idEnterprise = $mEnterprise->id;
			$mLicenseAvailable->idLicenseType = LicenseType::_Trial;
			$mLicenseAvailable->numLicenses = 1;
			$mLicenseAvailable->histLicenses = 1;
			$mLicenseAvailable->lastupdated = date("Y-m-d H:i:s");
			$allOk = $mLicenseAvailable->save();
			if ( !$allOk ) {
				$transaction->rollback();
				echo $this->retErrorWs("506", var_export($mLicenseAvailable->getErrors(), true) );
				return;
			}

			// ** Creamos el usuario tambien como alumno.
			$allOk = ModuleStudentModel::insertStudent( $mEnterprise->id, $userName, $userSurname, $userMail, '-', $mLanguage->iso, false );
			if ( !$allOk ) {
				$transaction->rollback();
				echo $this->retErrorWs("506", var_export( Yii::app()->getModule('student')->t('No se pudo crear el alumno.') , true) );
				return;
			}

			HeAbaMail::sendEmail( array(
					'action' => HeAbaSelligent::_EDITUSER,
					'idUser' => $mUser->id,
					'idEnterprise' => $mEnterprise->id,
					'name' => $mUser->name,
					'surname' => $mUser->surname,
					'email' => $mUser->email,
					'password' => $password,
					'isoLanguage' => $mLanguage->iso,
					'callback' => 'ExtranetController::actionMailSent',
			) );

			$mEnterprise->updateSelligentData();

			$transaction->commit();

			echo $this->retSuccessWs(array("result" => "success", "details" => "All Ok", 'redirectUrl' => Yii::app()->params['extranetURLDomain'].'/user/login/login/code/'.$userCode.'/idChannel/'.$partnerId));

		} catch(Exception $e) {
			$transaction->rollback();
			echo $this->retErrorWs("506", var_export($e->getMessage(), true) );
		}
	}

	protected function retSuccessWs ($aResponse)
	{
		return json_encode($aResponse);
	}

	protected function retErrorWs( $code=NULL, $additionalErrorMsg="" )
	{
		if (!isset($code)) {
			$code = "555"; // generic error code
		}

		$aResponse = array("result"=>$code, "details"=> self::$aErrorCodes[$code] . " - " . $additionalErrorMsg);

		return json_encode($aResponse);
	}

	protected static $aErrorCodes = array(
		"410"=> "The purchase receipt is not valid",
		"555"=> "Unidentified error",
		"501"=> "Invalid Parameter Value",
		"502"=> "Invalid Signature",
		"503"=> "Operation not completed",
		// Detailed and customized errors, referenced from outside this class:
		"504"=> "User does not exist",
		"505"=> "Selligent communications error",
		"506"=> "User could not be created",
		"507"=> "User email is empty",
		"508"=> "Product not valid",
		"509"=> "Voucher is not valid. Probably it does not exist, or maybe it applies to another product",
		"510"=> "User registered, but error on GROUPON CODE update.",
		"511"=> "User could not be updated",
		"512"=> "Promocode not valid",
		"513"=> "Country Id not valid. Check its value on your call please.",
		"514"=> "Voucher already used. The code probably has been validated: ",
		"515"=> "Payment not valid.",
		"516"=> "Payment gateway did not allow transaction.",
		"517"=> "Payment is too old to be refund. Ask IT.",
		"518"=> "Change of credit form in subscription not allowed. ",
		"519"=> "User already exists and is Premium",
		"520"=> "teacher email not valid",
		"521"=> "Invalid Level",
		"522"=> "Password is not valid.",
		"523"=> "The facebook id is empty or is not valid.",
		"524"=> "User already exists. Gift codes can only be activated by unregistered users.",
		"556"=> "All not valid products",
		"557"=> "Operation completed with errors",
	);

	public function filters()
	{
		return array(
			array('application.filters.CORSFilterNoToken')
		);
	}
}