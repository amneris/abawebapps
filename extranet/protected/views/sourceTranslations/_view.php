<?php
/* @var $this SourceTranslationsController */
/* @var $data SourceTranslations */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idsourceTranslations')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idsourceTranslations), array('view', 'id'=>$data->idsourceTranslations)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seccion')); ?>:</b>
	<?php echo CHtml::encode($data->seccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('label')); ?>:</b>
	<?php echo CHtml::encode($data->label); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('es')); ?>:</b>
	<?php echo CHtml::encode($data->es); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('es_validated')); ?>:</b>
	<?php echo CHtml::encode($data->es_validated); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('en')); ?>:</b>
	<?php echo CHtml::encode($data->en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('en_validated')); ?>:</b>
	<?php echo CHtml::encode($data->en_validated); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('it')); ?>:</b>
	<?php echo CHtml::encode($data->it); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('it_validated')); ?>:</b>
	<?php echo CHtml::encode($data->it_validated); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fr')); ?>:</b>
	<?php echo CHtml::encode($data->fr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fr_validated')); ?>:</b>
	<?php echo CHtml::encode($data->fr_validated); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt')); ?>:</b>
	<?php echo CHtml::encode($data->pt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_validated')); ?>:</b>
	<?php echo CHtml::encode($data->pt_validated); ?>
	<br />

	*/ ?>

</div>