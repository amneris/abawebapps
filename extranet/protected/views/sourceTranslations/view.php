<?php
/* @var $this SourceTranslationsController */
/* @var $model SourceTranslations */
?>
<div class="row-fluid">
    <div class="col-lg-12 marginTop15">
        <br>
        <span class="sectionTitle"><?php echo $model->label; ?></span>
        <br>
        <br>
        <?php $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                'idsourceTranslations',
                'seccion',
                'label',
                'es',
                'es_validated',
                'en',
                'en_validated',
                'it',
                'it_validated',
                'fr',
                'fr_validated',
                'pt',
                'pt_validated',
                'ca',
                'ca_validated',
            ),
        )); ?>
        <br>
        <div style="text-align: center; margin-bottom: 2em">
            <button id="btnBack" type="button" class="abaButton abaButton-Primary"><?php echo Yii::t('app','Back'); ?></button>
        </div>

        <script>
            $(document).ready( function() {
                $('#btnBack').click(function () {
                    var pageURL = '/sourceTranslations/admin';
                    var parameters = {};

                    ABAChangePage(pageURL, parameters);

                });
            });

        </script>
    </div>
</div>
