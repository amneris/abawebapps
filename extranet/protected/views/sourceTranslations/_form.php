<?php
/* @var $this SourceTranslationsController */
/* @var $model SourceTranslations */
/* @var $form CActiveForm */
?>
<style>
    .customTable tr td
    {
        padding-top: 5px;
        padding-bottom: 5px;
        padding-left: 5px;
    }
</style>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'source-translations-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>
    <br>
    <table class="customTable">
        <tr >
            <td>
                <?php echo $form->labelEx($model,'seccion'); ?>
            </td>
            <td>
                <?php
                    if($model->isNewRecord)
                    {
                        echo $form->dropDownList($model, 'seccion' , $listOfSeccion);
                    }
                    else
                    {
                        echo $model->seccion;
                    }
                ?>
            </td>
            <td>

            </td>
        </tr>

        <tr>
            <td>
            <?php echo $form->labelEx($model,'label'); ?>
            </td>
            <td>

                <?php
                if($model->isNewRecord)
                {
                echo $form->textArea($model,'label',array('rows'=>2, 'cols'=>100));
                }
                else
                {
                    echo $model->label;
                }
                ?>
            </td>
            <td>

            </td>
        </tr>

        <tr>
        <td>
            <?php echo $form->labelEx($model,'es'); ?>
        </td>
        <td>
            <?php echo $form->textArea($model,'es',array('rows'=>2, 'cols'=>100, 'id'=>'esTranslation')); ?>
        </td>
        <td>
            <?php echo $form->labelEx($model,'es_validated'); ?>
            <?php
            echo $form->dropDownList($model,'es_validated', array(0 =>'False', 1 =>'True'),array('id'=>'esValidated', 'onchange'=>'js:changeEditionState(\'esTranslation\');'));
            ?>
        </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model,'en'); ?>
            </td>
            <td>
                <?php echo $form->textArea($model,'en',array('rows'=>2, 'cols'=>100, 'id'=>'enTranslation')); ?>
            </td>
            <td>
                <?php echo $form->labelEx($model,'en_validated'); ?>
                <?php
                echo $form->dropDownList($model,'en_validated', array(0 =>'False', 1 =>'True'),array('id'=>'enValidated', 'onchange'=>'js:changeEditionState(\'enTranslation\');'));
                ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model,'it'); ?>
            </td>
            <td>
                <?php echo $form->textArea($model,'it',array('rows'=>2, 'cols'=>100, 'id'=>'itTranslation')); ?>
            </td>
            <td>
                <?php echo $form->labelEx($model,'it_validated'); ?>
                <?php
                echo $form->dropDownList($model,'it_validated', array(0 =>'False', 1 =>'True'),array('id'=>'itValidated', 'onchange'=>'js:changeEditionState(\'itTranslation\');'));
                ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model,'fr'); ?>
            </td>
            <td>
                <?php echo $form->textArea($model,'fr',array('rows'=>2, 'cols'=>100, 'id'=>'frTranslation')); ?>
            </td>
            <td>
                <?php echo $form->labelEx($model,'fr_validated'); ?>
                <?php
                echo $form->dropDownList($model,'fr_validated', array(0 =>'False', 1 =>'True'),array('id'=>'frValidated', 'onchange'=>'js:changeEditionState(\'frTranslation\');'));
                ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model,'pt'); ?>
            </td>
            <td>
                <?php echo $form->textArea($model,'pt',array('rows'=>2, 'cols'=>100, 'id'=>'ptTranslation')); ?>
            </td>
            <td>
                <?php echo $form->labelEx($model,'pt_validated'); ?>
                <?php
                echo $form->dropDownList($model,'pt_validated', array(0 =>'False', 1 =>'True'),array('id'=>'ptValidated', 'onchange'=>'js:changeEditionState(\'ptTranslation\');'));
                ?>
            </td>
        </tr>

        <tr>
          <td>
            <?php echo $form->labelEx($model,'ca'); ?>
          </td>
          <td>
            <?php echo $form->textArea($model,'ca',array('rows'=>2, 'cols'=>100, 'id'=>'caTranslation')); ?>
          </td>
          <td>
            <?php echo $form->labelEx($model,'ca_validated'); ?>
            <?php
            echo $form->dropDownList($model,'ca_validated', array(0 =>'False', 1 =>'True'),array('id'=>'caValidated', 'onchange'=>'js:changeEditionState(\'caTranslation\');'));
            ?>
          </td>
        </tr>


    </table>
<br>
    <div style="text-align: center; margin-bottom: 2em">
        <button id="btnBack" type="button" class="abaButton abaButton-Primary"><?php echo Yii::t('app','Back'); ?></button>
        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app','ADD') : Yii::t('app','Save') ,array('class' => 'abaButton abaButton-Primary')); ?>
    </div>
<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
    $(document).ready( function()
    {
        $('#btnBack').click(function () {
            var pageURL = '/sourceTranslations/admin';
            var parameters = {};

            ABAChangePage(pageURL, parameters);

        });

        if($('#esValidated').val()==true)
        {
            $('#esTranslation').attr('readonly','readonly');
        }

        if($('#enValidated').val()==true)
        {
            $('#enTranslation').attr('readonly','readonly');
        }

        if($('#itValidated').val()==true)
        {
            $('#itTranslation').attr('readonly','readonly');
        }

        if($('#frValidated').val()==true)
        {
            $('#frTranslation').attr('readonly','readonly');
        }

        if($('#ptValidated').val()==true)
        {
            $('#ptTranslation').attr('readonly','readonly');
        }

        if($('#caValidated').val()==true)
        {
          $('#caTranslation').attr('readonly','readonly');
        }

    });
    function changeEditionState(id)
    {
        if($('#'+id).prop('readonly'))
        {
            $('#'+id).attr('readonly', false);
        }
        else
        {
            $('#'+id).attr('readonly', 'readonly');
        }

    }
</script>