<?php
/* @var $this SourceTranslationsController */
/* @var $model SourceTranslations */
?>
<div class="row-fluid">
    <div class="col-lg-12 marginTop15">
        <br>
        <span class="sectionTitle">Update SourceTranslations <?php echo $model->label; ?></span>
        <?php $this->renderPartial('_form', array('model'=>$model, 'listOfSeccion'=>$listOfSeccion)); ?>
    </div>
</div>