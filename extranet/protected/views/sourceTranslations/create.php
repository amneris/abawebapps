<?php
/* @var $this SourceTranslationsController */
/* @var $model SourceTranslations */

$this->breadcrumbs=array(
	'Source Translations'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SourceTranslations', 'url'=>array('index')),
	array('label'=>'Manage SourceTranslations', 'url'=>array('admin')),
);
?>

<h1>Create SourceTranslations</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'listOfSeccion'=>$listOfSeccion)); ?>