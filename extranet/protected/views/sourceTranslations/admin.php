<?php
/* @var $this SourceTranslationsController */
/* @var $model SourceTranslations */

/*Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#source-translations-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");*/

Yii::app()->clientScript->scriptMap=array(
    'jquery.min.js'=>false,
);
Yii::app()->clientScript->coreScriptPosition=CClientScript::POS_END;

?>
<style>
  #divMainBody {
    max-width: none;
  }
</style>
<div class="row-fluid">
    <div class="col-lg-12 marginTop15">
        <br>
        <span class="sectionTitle">Manage Source Translations</span>
        <a href="/sourceTranslations/export" target="_blank"><img id="iconDownload" src="/images/zip_grande.png" class="iconLink" style="float: right;" width="48"></a>
        <br>
        <?php echo CHtml::link('Create new translation',array('sourceTranslations/create')); ?>
        <br>

        <?php /*echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); */?><!--
        <div class="search-form" style="display:none">
            <?php /*$this->renderPartial('_search',array(
                'model'=>$model,
            )); */?>
        </div>-->

        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'source-translations-grid',
            'dataProvider'=>$model->search(),
            'cssFile' => Yii::app()->request->baseUrl . '/css/gridView.css',
            'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
                'prevPageLabel'  => '«',
                'nextPageLabel'  => '»',
                'header'=>''),
            'filter'=>$model,

            'columns'=>array(
                /*'idsourceTranslations',*/
                'seccion',
                'label',
                'es',
                'es_validated'=>
                    array(
                        'name'=>'es_validated',
                        'header'=>'validated?',
                        'filter'=>array(0 =>'False', 1 =>'True'),
                    ),
                'it',
                'it_validated'=>
                    array(
                        'name'=>'it_validated',
                        'header'=>'validated?',
                        'filter'=>array(0 =>'False', 1 =>'True'),
                    ),
                'en',
                'en_validated'=>
                    array(
                        'name'=>'en_validated',
                        'header'=>'validated?',
                        'filter'=>array(0 =>'False', 1 =>'True'),
                    ),
                'fr',
                'fr_validated'=>
                    array(
                        'name'=>'fr_validated',
                        'header'=>'validated?',
                        'filter'=>array(0 =>'False', 1 =>'True'),
                    ),
                'pt',
                'pt_validated'=>
                  array(
                    'name'=>'pt_validated',
                    'header'=>'validated?',
                    'filter'=>array(0 =>'False', 1 =>'True'),
                  ),
                'ca',
                'ca_validated'=>
                  array(
                    'name'=>'ca_validated',
                    'header'=>'validated?',
                    'filter'=>array(0 =>'False', 1 =>'True'),
                  ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{view}{update}',
                ),
            ),
        )); ?>

    </div>
</div>

<script>
    $(document).ready( function() {
        $('#iconDownload').click( function() {

        });
    });
</script>