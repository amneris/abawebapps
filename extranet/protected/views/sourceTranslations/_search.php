<?php
/* @var $this SourceTranslationsController */
/* @var $model SourceTranslations */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idsourceTranslations'); ?>
		<?php echo $form->textField($model,'idsourceTranslations'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'seccion'); ?>
		<?php echo $form->textField($model,'seccion',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'label'); ?>
		<?php echo $form->textArea($model,'label',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'es'); ?>
		<?php echo $form->textArea($model,'es',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'es_validated'); ?>
		<?php echo $form->textField($model,'es_validated'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'en'); ?>
		<?php echo $form->textArea($model,'en',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'en_validated'); ?>
		<?php echo $form->textField($model,'en_validated'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'it'); ?>
		<?php echo $form->textArea($model,'it',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'it_validated'); ?>
		<?php echo $form->textField($model,'it_validated'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fr'); ?>
		<?php echo $form->textArea($model,'fr',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fr_validated'); ?>
		<?php echo $form->textField($model,'fr_validated'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pt'); ?>
		<?php echo $form->textArea($model,'pt',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pt_validated'); ?>
		<?php echo $form->textField($model,'pt_validated'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->