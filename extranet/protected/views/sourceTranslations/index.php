<?php
/* @var $this SourceTranslationsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Source Translations',
);

$this->menu=array(
	array('label'=>'Create SourceTranslations', 'url'=>array('create')),
	array('label'=>'Manage SourceTranslations', 'url'=>array('admin')),
);
?>

<h1>Source Translations</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
