<div id='divFooter' style="width:<?php if(Utils::getSessionVar('idEnterprise')<>''){ echo '100'; } else { echo '50'; } ?>">
    <div class="container">
        <div id="linksFooter">
            <?php
            if(Utils::getSessionVar('idEnterprise')<>'')
            {
                ?>
                <ul>
                    <li>
                        <a href="<?php echo Yii::app()->baseUrl; ?>/student/main""><?php echo Yii::t('app', 'Student'); ?></a>
                    </li>

                    <li>
                        <a href="<?php echo Yii::app()->baseUrl; ?>/price/main""><?php echo Yii::t('app', 'Prices'); ?></a>
                    </li>
                    <li>
                        <a href="<?php echo Yii::app()->baseUrl; ?>/followup/followup/admin""><?php echo Yii::t('app', 'Analytics'); ?></a>
                    </li>

                    <li>
                        <a href="<?php echo Yii::app()->baseUrl; ?>/extranet/configs""><?php echo Yii::t('app', 'Settings'); ?></a>
                    </li>
                </ul>
            <?php
            }
            ?>
            <span><?php echo Yii::app()->getModule('user')->t2('Telf:'); ?> (+34) 934 091 396</span><span>|</span><span><?php echo Yii::app()->getModule('user')->t2('Email:'); ?> corporate@abaenglish.com</span>
        </div>
    </div>
</div>

<style>
<?php if ( Utils::getSessionVar('idEnterprise')<>'' ) { ?>
    #main {
        padding-bottom:106px; /* this needs to be bigger than footer height*/
    }

    .footer {
        margin-top: -106px; /* negative value of footer height */
        height: 106px;
    }
<?php } ?>
</style>