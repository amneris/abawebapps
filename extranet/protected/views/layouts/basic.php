<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo Yii::app()->language; ?>" lang="<?php echo Yii::app()->language; ?>">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	  <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <link id="linkFavicon"  rel="icon" href="/favicon.ico" type="image/x-icon" />
    
    <!-- Bootstrap -->
    <link href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo Yii::app()->baseUrl; ?>/css/extranet.css" rel="stylesheet" />
   
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery-2.1.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap/js/bootstrap.min.js"></script>

    <!-- FancyBox -->
    <link rel="stylesheet" href="/js/fancyapps-fancyBox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <script type="text/javascript" src="/js/fancyapps-fancyBox/jquery.fancybox.pack.js?v=2.1.5"></script>
    
    <script src="<?php echo Yii::app()->baseUrl; ?>/js/aba/core.js"></script>
    <script src="<?php echo Yii::app()->baseUrl; ?>/js/utils.js"></script>
    
  </head>
<body>
<?php echo $content; ?>
<script>
    $(document).ready( function() {
      $(window).resize( function() { ABARedrawBody(); });
      ABARedrawBody();
      
//      abaCore.showDialog();
//      abaCore.launchAjax('/pddfsd', {});
    });
</script>
</body>
</html>
