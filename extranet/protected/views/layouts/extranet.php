<?php
  $selectedIndex = 'none';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo Yii::app()->language; ?>" lang="<?php echo Yii::app()->language; ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <link id="linkFavicon"  rel="icon" href="<?php echo Yii::app()->baseUrl; ?>/favicon.ico" type="image/x-icon" />

    <!-- Bootstrap -->
    <link href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!--    <link href="<?php echo Yii::app()->baseUrl; ?>/css/extranet.css" rel="stylesheet"> -->
    <link href="<?php echo Yii::app()->baseUrl; ?>/css/extranet.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery-2.1.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap/js/bootstrap.min.js"></script>

    <!-- FancyBox -->
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/js/fancyapps-fancyBox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/js/fancyapps-fancyBox/jquery.fancybox.pack.js?v=2.1.5"></script>

    <script src="<?php echo Yii::app()->baseUrl; ?>/js/aba/core.js"></script>
    <script src="<?php echo Yii::app()->baseUrl; ?>/js/utils.js"></script>

      <!-- Jcrop, plugin to crop images -->
      <link href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.Jcrop.css" rel="stylesheet">
      <script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.Jcrop.min.js"></script>

      <!-- Plug in to make nice tooltips -->
      <link href="<?php echo Yii::app()->baseUrl; ?>/css/tooltipster.css" rel="stylesheet">
      <script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery.tooltipster.js"></script>

    <!-- little library to detect browser versions -->
    <script src="<?php echo Yii::app()->baseUrl; ?>/js/bowser.min.js"></script>
      
  </head>
  <body>

    <?php
    preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);

    if (count($matches) < 2) {
        preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
    }

    if (count($matches) > 1) {
        $version = $matches[1];

        switch (true) {
            case ($version <= 11):
                ?>
              <div class="bs-example">
                  <div class="alert alert-danger fade in error-explorer">
                      <a href="#" class="close" data-dismiss="alert">&times;</a>
                      <p><?php echo Yii::t('app', 'no-iexplorer' ); ?></p>
                  </div>
              </div>
            <?php
        default:
        }
    }
    ?>

    <?php $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); ?>
    <div id="divMainBody">
        <?php echo $content; ?>
      <div id="push"></div>
    </div>
    <?php $this->renderPartial('//login/footer'); ?>
    

    <div id="divGlobalHelp" class="hiddenGlobalHelp" style="position:fixed; width: 20px; height: auto; background-color: #009DDF; cursor: pointer;">
        <div><span id="divGlobalHelpSpan" style2="visibility: hidden;"><?php echo Yii::t('app', 'HELP'); ?></span></div>
    </div>

    <div id="divHiddenPopup" style="display: none; text-align: center;">
        <table height="100%" width='100%'>
            <tr>
                <td>
                    <div style="line-height: 6em;text-align: center;"><img src="/images/loading.gif"></div>
                    <div style="text-align: center; font-weight: bold; font-size: 1.3em; line-height: 3em;"><?php echo Yii::t('app', 'Loading' ); ?> ...</div>
                    <div style="text-align: center; font-size: 1em;"><?php echo Yii::t('app', 'Please, wait some seconds.' ); ?></div>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
        </table>
    </div>

    <div id="hiddenMsgOk" style="display:none;">
      <table height="100%" width='100%'>
      <tr>
        <td>
          <div style="font-size: 20px; color:green; text-align: center;">
            <img src="/images/tick3.png" width="48" style="margin: 20px; width: 3.25em;">
          </div>

          <div style="text-align: center;">
            <span id="hiddenMsgOk_titulo" style='font-family: "MuseoSlab500", Arial, Sans-serif; font-weight: bold; font-size: 1.479166667em;'></span><br><br>
            <span id="hiddenMsgOk_mensaje" style='font-family: "MuseoSlab700", Arial, Sans-serif; font-size: 1.155em;'></span><br><br>
          </div>

        </td>
      </tr>
      <tr><td>&nbsp;</td></tr>
      </table>
    </div>
    
    <div id="hiddenMsgError" style="display:none;">
      <table height="100%" width='100%'>
      <tr>
        <td>
          <div style="font-size: 20px; color:green; text-align: center;">
            <img src="/images/redalert.png" width="48" style="margin: 20px; width: 3.25em;">
          </div>

          <div style="text-align: center;">
            <span id="hiddenMsgError_titulo" style='font-family: "MuseoSlab500", Arial, Sans-serif; font-weight: bold; font-size: 1.479166667em;'></span><br><br>
            <span id="hiddenMsgError_mensaje" style='font-family: "MuseoSlab700", Arial, Sans-serif; font-size: 1.155em;'></span><br><br>
          </div>

        </td>
      </tr>
      <tr><td>&nbsp;</td></tr>
      </table>
    </div>
    
    
  </body>
</html>

<script>



    $(document).ready( function()
    {
        //LUIS: we check browser and we show a banner if it is internet explorer not updated.
        if(bowser.msie == true)
        {
            if(bowser.version != '11.0')
            {
                shouldNotShow = getCookie("browserUpgradeMessage");
                if(shouldNotShow!=1)
                {
                    $('#browserUpgrade').show();
                }
            }
        }
        $('#closeBrowserUpgrade').click( function()
        {
            $('#browserUpgrade').hide();
            createCookie("browserUpgradeMessage", 1, 1)
        });


        fixedDivGloblaHelp();
        $( window ).resize(function() {
            fixedDivGloblaHelp();
        });

      $.each( $('.navbar-collapse ul.navbar-nav li'), function(index,value) {
        var name = $(this).data('item');
//        console.log('ID: '+index+'| Value: '+name);
        $(this).removeClass('active');

          if (typeof selectedIndex != 'undefined')
          {
              if ( selectedIndex == name ) $(this).addClass('active');
          }


      });

//  console.log('hola');
//  setTimeout( function() {
//    ABACustomDialog( 'Imagen errornea', 'El tipo de la imagen debe ser PNG', false, true );
//  }, 3000);

        $("#divGlobalHelp").click( function() {
            openContact();

//        if ( $('#divGlobalHelp').hasClass('hiddenGlobalHelp') ) {
//            $('#divGlobalHelp').removeClass('hiddenGlobalHelp');
//            showHiddenGlobalHelp();
//        } else {
//            $('#divGlobalHelp').addClass('hiddenGlobalHelp');
//            hideHiddenGlobalHelp();
//            openContact();
//        }

        });

//  console.log( "Size1: " + $('#divMainBody').height() );
//  console.log( "Size1b: " + $(document).height() );
//  console.log( "Size1c: " + $(document.body).height() );
//  $('#divMainBody').height( $(document).height() );
//  console.log( "Size2: " + $('#divMainBody').height() );

//  $(window).resize( function() {
//    var div = document.getElementById('divMainBody');
//    var hasVerticalScrollbar = (div.scrollHeight > div.clientHeight);
//    var hasHorizontalScrollbar = (div.scrollWidth > div.clientWidth );

//console.log('resize');    
//    $('#divMainBody').height( $(document).height() + 10 );
//  });

  $(window).resize( function() { ABARedrawBody(); });
  ABARedrawBody();

    });




</script>
