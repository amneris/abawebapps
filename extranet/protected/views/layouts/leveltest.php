<?php 
  
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo Yii::app()->language; ?>" lang="<?php echo Yii::app()->language; ?>">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ABA English Extranet</title>

    <link id="linkFavicon"  rel="icon" href="/images/campusfavicon.ico" type="image/x-icon" />
    
    <!-- Bootstrap -->
    <link href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->baseUrl; ?>/css/extranet.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo Yii::app()->baseUrl; ?>/js/jquery-2.1.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap/js/bootstrap.min.js"></script>

    <!-- FancyBox -->
    <link rel="stylesheet" href="/js/fancyapps-fancyBox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <script type="text/javascript" src="/js/fancyapps-fancyBox/jquery.fancybox.pack.js?v=2.1.5"></script>
    
    <script src="<?php echo Yii::app()->baseUrl; ?>/js/utils.js"></script>

    <script src="//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
    <script src="/js/aba/core.js"></script>

  </head>
  <body>
    <!--<div class="row-fluid">
      <div class="col-lg-12">-->
        <?php echo $content; ?>
      <!--</div>
    </div>-->

<div id="divHiddenPopup" style="display: none; text-align: center;">
  <table height="100%" width='100%'>
  <tr>
    <td>
      <div style="line-height: 6em;text-align: center;"><img src="/images/loading.gif"></div>
      <div style="text-align: center; font-weight: bold; font-size: 1.3em; line-height: 3em;"><?php echo Yii::t('app', 'Loading' ); ?> ...</div>
      <div style="text-align: center; font-size: 1em;"><?php echo Yii::t('app', 'Please, wait some seconds' ); ?></div>
    </td>
  </tr>
  <tr><td>&nbsp;</td></tr>
  </table>
</div>
      
  </body>
</html>