<div id="divHiddenContact">
  <div id="divContact_Content" style="text-align: left;">
    <h4><?php echo Yii::t('app', 'Contact with us'); ?></h4>
    <p><?php echo Yii::t('app', 'Contact us filling this form and we will contact you as soon as posible.'); ?></p>
    
    <div><label for="contactSubject"><?php echo Yii::t('app', 'Subject').'*'; ?></label></div>
    <div>
      <input id="contactSubject" type="text" class="form-control">
<!--      <select id="contactSubject" class="form-control">
        <option value="0"><?php echo Yii::t('app', 'Select a subject'); ?></option>
        <option value="1">Pregunta 1</option>
        <option value="2">Pregunta 2</option>
        <option value="3">Pregunta 3</option>
        <option value="4">Pregunta 4</option>
      </select> -->
    </div>
    
    <div><label for="contactMessage"><?php echo Yii::t('app', 'Message').'*'; ?></label></div>
    <div><textarea id="contactMessage" style="width: 100%; height: 100px;" class="form-control"></textarea></div>
    
    <div id='divBtnSendContact'>
      <button id='contactBtnSend' class="abaButton abaButton-Primary"><?php echo Yii::t('app', 'Send'); ?></button>
    </div>
  </div>
</div>

<script>
  
  function contactBtnSend() {
    var subject = $('#contactSubject').val();
    var message = $('#contactMessage').val();
    var accept = $('#contactAccept').prop( "checked" );
    
    if (subject == 0) {
      alert('<?php echo CJavaScript::quote(Yii::t('app', 'You must select some subject.')); ?>');
      return false;
    }
    
    if ( message == '' ) {
      alert('<?php echo CJavaScript::quote(Yii::t('app', 'Message mustnt be empty.')); ?>');
      $('#contactMessage').focus();
      return false;
    }
    
    ABAShowLoading();
    var parametros = {
      subject: subject,
      message: message,
    };

    $.ajax({
      data:  parametros,
      url: "/extranet/sendhelp",
      context: document.body,
      type: 'POST',
      dataType:"json",
      success: function(data) {
        if ( data.ok ) {
          ABAShowDialog( data.html, true );
        } else {
          ABAShowDialog( data.html, true );
        }
      },
      error: function(jqXHR, textStatus, error) {
//          alert('error');
      },        
    }).done(function() {
      // 
    });
    
  }

  $(document).ready( function() {
    
    $('#contactBtnSend').click( function() {
      contactBtnSend();
    });
    
  });

</script>