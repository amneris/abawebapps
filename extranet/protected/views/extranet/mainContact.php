<?php

  $selectedIndex = 'None';
  
?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>

<div>
  Pais: 
  <select name="pais" id="pais">
    <option value="es">España</option>
    <option value="en">Inglaterra</option>
    <option value="it">Italia</option>
    <option value="fr">Francia</option>
    <option value="jp">Japon</option>
    <option value="ar">Argentina</option>
  </select>
</div>

<div>
  Idioma: 
  <select id="idioma" name="idioma">
    <option>es</option>
    <option>en</option>
    <option>it</option>
    <option>fr</option>
    <option>jp</option>
    <option>ar</option>
  </select>
</div>

<div>
  Nombre: 
  <input type="text" name="nombre" id="nombre">
</div>

<div>
  Apellidos: 
  <input type="text" name="apellidos" id="apellidos">
</div>

<div>
  Mail: 
  <input type="text" name="mail" id="mail">
</div>

<div>
  Pass: 
  <input type="text" name="pass" id="pass">
</div>

<div>
  Nombre Empresa: 
  <input type="text" name="nombreEmpresa" id="nombreEmpresa">
</div>

<button class="abaButton abaButton-Primary" id="btnSend"><?php Yii::t('app', 'Send'); ?></button>

<script>
  
  $(document).ready(function() {
    
    $('#btnSend').click( function() {
      
      var parametros = {
        pais : $('#pais').val(),
        idioma : $('#idioma').val(),
        nombre : $('#nombre').val(),
        apellidos : $('#apellidos').val(),
        mail : $('#mail').val(),
        nombreEmpresa : $('#nombreEmpresa').val(),
        password: $('#pass').val(),
      };

      $.ajax({
        data:  parametros,
        url: "/extranet/mainContact2",
        context: document.body,
        type: 'POST',
        dataType:"json",
        success: function(data) {
          ABAShowDialog( data.html, true );
        },
        error: function(jqXHR, textStatus, error) {
          ABAShowDialog( jqXHR + ' ' + textStatus + ' ' + error, true );
  //          alert('error');
        },        
      }).done(function() {
        // 
      });
    });
    
  });
  
</script>