<table height="100%" width='100%'>
<tr>
  <td>
    <div style="font-size: 20px; color:green; text-align: center;">
      <img src="/images/tick3.png" width="48" style="margin: 20px;">
    </div>

    <div style="text-align: center;">
      <span><?php echo StudentModule::t('Email sends successfully.'); ?></span><br>
    </div>

    <div id="divBtnCloseOkLicense">
      <button id="btnCloseOkLicense" class="abaButton abaButton-Primary abaExtranetButton-Ok">
        <?php echo StudentModule::t('Ok'); ?>
      </button>
    </div>
  </td>
</tr>
<tr><td>&nbsp;</td></tr>
</table>

<script>
  $(document).ready( function() {
    $('#btnCloseOkLicense').click( function() {
      ABACloseDialog();
    });
  });
</script>
