<?php

  $selectedIndex = 'Alumno';
  
  $numLicencias = 1;

?>
<?php $this->renderPartial('menu', array('selectedIndex' => $selectedIndex) ); ?>

<style>
  #imgDisplayFiltros {
    margin-top: -2.5px;
  }
  
  .newStudentCell {
    border: none !important;
    padding-left: 20px !important;
    padding-right: 20px !important;
  }
  
  .newStudentCell label {
    font-size: 18px;
  }
   
  .alignRight {
    text-align: right;
  }
  
  .imgQuery {
    margin-top: -15px;
  }
  
</style>

<div class="row-fluid">
  <div class="col-lg-12">

    <div class="row-fluid">
      <div class="col-lg-6" style="font-size: 2em; padding: 20px;">
        <?php echo CHtml::encode( Yii::t( 'app', 'Alumnos') ); ?>
      </div>
      <div class="col-lg-6 alignRight" style="font-size: 2em; padding: 20px;">
        <?php echo CHtml::encode( Yii::t( 'app', 'Licencias disponibles') ); ?>: <?php echo $numLicencias; ?>
      </div>
    </div>    
    
    <div class="row-fluid">
      <div class="col-lg-12">

        <?php echo CHtml::encode( Yii::t( 'app', 'AÑADIR NUEVO ALUMNO') ); ?> <img id='imgDisplayFiltros' src='<?php echo Yii::app()->baseUrl; ?>/images/filterMinus.png' width="16">

      </div>
    </div>    
        
    <div class="row-fluid">
      <div class="col-lg-12">
        
        <div class="table-responsive"> 

          <table id='divFiltrosAlumno' class="table table-bordered" style="background-color: #f3f3f3;">
            <tr>
              <td class='newStudentCell'>
                
                <div class="row-fluid">
                  <div class="col-lg-3">
                    <label for='newStudentName'><?php echo CHtml::encode( Yii::t( 'app', 'NOMBRE' ) ); ?></label><br>
                    <input id='newStudentName' type="text" value="">
                  </div>
                  <div class="col-lg-3">
                    <label for='newStudentSurname'><?php echo CHtml::encode( Yii::t( 'app', 'APELLIDOS' ) ); ?></label><br>
                    <input id='newStudentSurname' type="text" value="">
                  </div>
                  <div class="col-lg-3">
                    <label for='newStudentEmail'><?php echo CHtml::encode( Yii::t( 'app', 'EMAIL' ) ); ?></label><br>
                    <input id='newStudentEmail' type="text" value="">
                  </div>
                  <div class="col-lg-3">
                    <label for='newStudentGroup'><?php echo CHtml::encode( Yii::t( 'app', 'GRUPO' ) ); ?> 
                    <img class='imgQuery' src='<?php echo Yii::app()->baseUrl; ?>/images/query_grey.png' data-toggle="tooltip" data-original-title="<?php echo CHtml::encode( Yii::t( 'app', 'Ajuda per a Grup' ) ); ?>" ></label>
                    
                    <br>
                    <input id='newStudentGroup' type="text" value="">
                  </div>
                  
                </div>    
                
              </td>
            </tr>
            
            <tr>
              <td class='newStudentCell'>
                
                <div class="row-fluid">
                  <div class="col-lg-6">
                    <button type="button" class="abaButton abaButton-Primary">
                      <?php echo CHtml::encode( Yii::t( 'app', 'AÑADIR' ) ); ?>
                    </button>
                  </div>
                  <div class="col-lg-6 alignRight">
                    <?php echo CHtml::encode( Yii::t( 'app', 'O si lo prefieres, puedes ' ) ); ?>
                    <a href="#"><?php echo CHtml::encode( Yii::t( 'app', 'importar un CSV ' ) ); ?></a>
                    <img class='imgQuery' src='<?php echo Yii::app()->baseUrl; ?>/images/query_blue.png' data-toggle="tooltip" data-original-title="<?php echo CHtml::encode( Yii::t( 'app', 'Ajuda per a CSV' ) ); ?>">
                  </div>
                </div>
                
              </td>
            </tr>
          </table>
          
        </div>
        
      </div>
    </div>    
    
    
    <div class="row-fluid">
      <div class="col-lg-12">
        
        <div class="table-responsive"> 

          <table id='divFiltrosAlumno' class="table table-bordered" style="background-color: #f3f3f3;">
            <tr>
              <td>
                
                <div class="row-fluid">
                  <div class="col-lg-6">
                    
                    <span style="font-size:1.3em; font-weight: bold;">53 <?php echo CHtml::encode( Yii::t( 'app', 'Alumnos' ) ); ?></span><br>
                    <span style="font-size:1em; font-weight: bold;">23 <?php echo CHtml::encode( Yii::t( 'app', 'activos' ) ); ?></span>
                  </div>
                    
                  <div class="col-lg-6 alignRight tableButtonDiv">
                    <button type="button" class="btn btn-primary">
                      <?php echo CHtml::encode( Yii::t( 'app', 'ENVIAR TEST' ) ); ?>
                    </button>
                    <button type="button" class="btn btn-danger">
                      <?php echo CHtml::encode( Yii::t( 'app', 'ACTIVAR LICENCIA' ) ); ?>
                    </button>
                    <div class="tableIconDiv"><span class="glyphicon glyphicon-trash"></span></div>
                    <div class="tableIconDiv"><span class="glyphicon glyphicon-envelope"></span></div>
                  </div>
                    
                  </div>
                </div>
                
              </td>
            </tr>
            
            <tr>
              <td style="padding:0px;">
                
                <div class="table-responsive"> 
                  <table id='divTableAlumno' class="table table-bordered" style="background-color: #f3f3f3;">
                    <tr>
                      <th width="20"><input type="checkbox" id="allChecks"></th>
                      <th><?php echo CHtml::encode( Yii::t( 'app', 'NOMBRE' ) ); ?><img class='imgQuery' src='<?php echo Yii::app()->baseUrl; ?>/images/query_grey.png' data-toggle="tooltip" data-original-title="<?php echo CHtml::encode( Yii::t( 'app', 'Ajuda per al nom' ) ); ?>" ></th>
                      <th style="border-right:1px solid #cccccc;"><?php echo CHtml::encode( Yii::t( 'app', 'APELLIDOS' ) ); ?><img class='imgQuery' src='<?php echo Yii::app()->baseUrl; ?>/images/query_grey.png' data-toggle="tooltip" data-original-title="<?php echo CHtml::encode( Yii::t( 'app', 'Ajuda per als cognoms' ) ); ?>" ></th>
                      <th><?php echo CHtml::encode( Yii::t( 'app', 'EMAIL' ) ); ?><img class='imgQuery' src='<?php echo Yii::app()->baseUrl; ?>/images/query_grey.png' data-toggle="tooltip" data-original-title="<?php echo CHtml::encode( Yii::t( 'app', 'Ajuda per a mail' ) ); ?>" ></th>
                      <th><?php echo CHtml::encode( Yii::t( 'app', 'NIVEL' ) ); ?><img class='imgQuery' src='<?php echo Yii::app()->baseUrl; ?>/images/query_grey.png' data-toggle="tooltip" data-original-title="<?php echo CHtml::encode( Yii::t( 'app', 'Ajuda per al nivell' ) ); ?>" ></th>
                      <th><?php echo CHtml::encode( Yii::t( 'app', 'GRUPO' ) ); ?><img class='imgQuery' src='<?php echo Yii::app()->baseUrl; ?>/images/query_grey.png' data-toggle="tooltip" data-original-title="<?php echo CHtml::encode( Yii::t( 'app', 'Ajuda per al Grup' ) ); ?>" ></th>
                      <th><?php echo CHtml::encode( Yii::t( 'app', 'CONVOCATORIA' ) ); ?><img class='imgQuery' src='<?php echo Yii::app()->baseUrl; ?>/images/query_grey.png' data-toggle="tooltip" data-original-title="<?php echo CHtml::encode( Yii::t( 'app', 'Ajuda per a la convocatoria' ) ); ?>" ></th>
                      <th><?php echo CHtml::encode( Yii::t( 'app', 'CADUCIDAD' ) ); ?><img class='imgQuery' src='<?php echo Yii::app()->baseUrl; ?>/images/query_grey.png' data-toggle="tooltip" data-original-title="<?php echo CHtml::encode( Yii::t( 'app', 'Ajuda per a la caducitat' ) ); ?>" ></th>
                    </tr>
                    
                    <?php $arAlumnos = array( 
                                              array('nombre' => 'Fran', 'apellidos' => 'Pérez Pérez', 'email' => 'fepepe@gmail.com', 'nivel' => 'Advanced', 'grupo' => 'A', 'convocatoria' => 'Marzo', 'caducidad' => 'Sin licencia'),
                                              array('nombre' => 'Fran', 'apellidos' => 'Pérez Pérez', 'email' => 'fepepe@gmail.com', 'nivel' => 'Beginners', 'grupo' => 'A', 'convocatoria' => 'Julio', 'caducidad' => '13 / 08 / 2014'),
                                              array('nombre' => 'Fran', 'apellidos' => 'Pérez Pérez', 'email' => 'fepepe@gmail.com', 'nivel' => 'Business', 'grupo' => 'C', 'convocatoria' => ' - ', 'caducidad' => 'Sin licencia'),
                                              array('nombre' => 'Fran', 'apellidos' => 'Pérez Pérez', 'email' => 'fepepe@gmail.com', 'nivel' => 'Todos los niveles', 'grupo' => 'D', 'convocatoria' => 'Enero', 'caducidad' => '07 / 03 / 2013'),
                                              array('nombre' => 'Fran', 'apellidos' => 'Pérez Pérez', 'email' => 'fepepe@gmail.com', 'nivel' => 'Intermediate', 'grupo' => 'B', 'convocatoria' => 'Marzo', 'caducidad' => '01 / 01 / 2014'),
                                                ); ?>
                    <?php foreach( $arAlumnos as $alumno ) { ?>
                    <tr>
                      <td><input type="checkbox" id="check_"></td>
                      <td><?php echo $alumno['nombre']; ?></td>
                      <td style="border-right: 1px solid #cccccc;"><?php echo $alumno['apellidos']; ?></td>
                      <td style="background-color: #fcfcfc;"><?php echo $alumno['email']; ?></td>
                      <td style="background-color: #fcfcfc;"><?php echo $alumno['nivel']; ?></td>
                      <td style="background-color: #fcfcfc;">Grupo <?php echo $alumno['grupo']; ?></td>
                      <td style="background-color: #fcfcfc;"><?php echo $alumno['convocatoria']; ?></td>
                      <td style="background-color: #fcfcfc;"><?php echo $alumno['caducidad']; ?></td>
                    </tr>
                    <?php } ?>
                    
                  </table>
                </div>
                
              </td>
            </tr>
            
          </table>
        </div>
        
      </div>
    </div>
    
    
    
    
    

  </div>
</div>

<script>
  
  $(document).ready( function() {
    
    $('#imgDisplayFiltros').click( function() {
      
      $('#divFiltrosAlumno').toggle();
      
      if ( $('#divFiltrosAlumno').is(':visible') ) {
        $('#imgDisplayFiltros').attr('src', '<?php echo Yii::app()->baseUrl; ?>/images/filterMinus.png');
      } else {
        $('#imgDisplayFiltros').attr('src', '<?php echo Yii::app()->baseUrl; ?>/images/filterPlus.png');
      }
      
    });
    
    $(".imgQuery").tooltip({
        placement : 'top'
    });    
    
  });
  
  
</script>