<?php

  $selectedIndex = 'Alumno';
  
  $numLicencias = 1;

?>
<?php $this->renderPartial('menu', array('selectedIndex' => $selectedIndex) ); ?>

<style>
  #imgDisplayFiltros {
    margin-top: -2.5px;
  }
  
  .newStudentCell {
    border: none !important;
    padding-left: 20px !important;
    padding-right: 20px !important;
  }
  
  .newStudentCell label {
    font-size: 18px;
  }
   
  .alignRight {
    text-align: right;
  }
  
  .imgQuery {
    margin-top: -15px;
  }
  
</style>

<div class="row-fluid">
  <div class="col-lg-12">

    <div class="row-fluid">
      <div class="col-lg-6" style="font-size: 2em; padding: 20px;">
        <?php echo CHtml::encode( Yii::t( 'app', 'Alumnos') ); ?>
      </div>
      <div class="col-lg-6 alignRight" style="font-size: 2em; padding: 20px;">
        <?php echo CHtml::encode( Yii::t( 'app', 'Licencias disponibles') ); ?>: <?php echo $numLicencias; ?>
      </div>
    </div>    
    
    <div class="row-fluid">
      <div class="col-lg-12">

        <?php echo CHtml::encode( Yii::t( 'app', 'AÑADIR NUEVO ALUMNO') ); ?> <img id='imgDisplayFiltros' src='<?php echo Yii::app()->baseUrl; ?>/images/filterMinus.png' width="16">

      </div>
    </div>    
        
    <div class="row-fluid">
      <div class="col-lg-12">
        
        <div class="table-responsive"> 

          <table id='divFiltrosAlumno' class="table table-bordered" style="background-color: #f3f3f3;">
            <tr>
              <td class='newStudentCell'>
                
                <div class="row-fluid">
                  <div class="col-lg-3">
                    <label for='newStudentName'><?php echo CHtml::encode( Yii::t( 'app', 'NOMBRE' ) ); ?></label><br>
                    <input id='newStudentName' type="text" value="">
                  </div>
                  <div class="col-lg-3">
                    <label for='newStudentSurname'><?php echo CHtml::encode( Yii::t( 'app', 'APELLIDOS' ) ); ?></label><br>
                    <input id='newStudentSurname' type="text" value="">
                  </div>
                  <div class="col-lg-3">
                    <label for='newStudentEmail'><?php echo CHtml::encode( Yii::t( 'app', 'EMAIL' ) ); ?></label><br>
                    <input id='newStudentEmail' type="text" value="">
                  </div>
                  <div class="col-lg-3">
                    <label for='newStudentGroup'><?php echo CHtml::encode( Yii::t( 'app', 'GRUPO' ) ); ?> 
                    <img class='imgQuery' src='<?php echo Yii::app()->baseUrl; ?>/images/query_grey.png' data-toggle="tooltip" data-original-title="<?php echo CHtml::encode( Yii::t( 'app', 'Ajuda per a Grup' ) ); ?>" ></label>
                    
                    <br>
                    <input id='newStudentGroup' type="text" value="">
                  </div>
                  
                </div>    
                
              </td>
            </tr>
            
            <tr>
              <td class='newStudentCell'>
                
                <div class="row-fluid">
                  <div class="col-lg-6">
                    <button type="button" class="abaButton abaButton-Primary">
                      <?php echo CHtml::encode( Yii::t( 'app', 'AÑADIR' ) ); ?>
                    </button>
                  </div>
                  <div class="col-lg-6 alignRight">
                    <?php echo CHtml::encode( Yii::t( 'app', 'O si lo prefieres, puedes ' ) ); ?>
                    <a href="#"><?php echo CHtml::encode( Yii::t( 'app', 'importar un CSV ' ) ); ?></a>
                    <img class='imgQuery' src='<?php echo Yii::app()->baseUrl; ?>/images/query_blue.png' data-toggle="tooltip" data-original-title="<?php echo CHtml::encode( Yii::t( 'app', 'Ajuda per a CSV' ) ); ?>">
                  </div>
                </div>
                
              </td>
            </tr>
          </table>
          
        </div>
        
      </div>
    </div>    
    
    
    <div class="row-fluid">
      <div class="col-lg-12">
        
        <div class="table-responsive"> 

          <table id='divFiltrosAlumno' class="table table-bordered" style="background-color: #f3f3f3;">
            <tr>
              <td>
                
                <div class="row-fluid">
                  <div class="col-lg-6">
                    
                    <span style="font-size:1.3em; font-weight: bold;">53 <?php echo CHtml::encode( Yii::t( 'app', 'Alumnos' ) ); ?></span><br>
                    <span style="font-size:1em; font-weight: bold;">23 <?php echo CHtml::encode( Yii::t( 'app', 'activos' ) ); ?></span>
                  </div>
                    
                  <div class="col-lg-2">
                    <button type="button" class="abaButton abaButton-Primary">
                      <?php echo CHtml::encode( Yii::t( 'app', 'ENVIAR TEST' ) ); ?>
                    </button>
                  </div>
                    
                  <div class="col-lg-2">
                    <button type="button" class="btn btn-danger">
                      <?php echo CHtml::encode( Yii::t( 'app', 'ACTIVAR LICENCIA' ) ); ?>
                    </button>
                  </div>
                    
                  <div class="col-lg-1">
                    <span class="glyphicon glyphicon-trash"></span>
                  </div>
                  <div class="col-lg-1">
                    <span class="glyphicon glyphicon-envelope"></span>
                  </div>
                    
                  </div>
                </div>
                
              </td>
            </tr>
            
            <tr>
              <td>
                
              </td>
            </tr>
          </table>
        </div>
        
      </div>
    </div>
    
    
    
    
    

  </div>
</div>

<script>
  
  $(document).ready( function() {
    
    $('#imgDisplayFiltros').click( function() {
      
      $('#divFiltrosAlumno').toggle();
      
      if ( $('#divFiltrosAlumno').is(':visible') ) {
        $('#imgDisplayFiltros').attr('src', '<?php echo Yii::app()->baseUrl; ?>/images/filterMinus.png');
      } else {
        $('#imgDisplayFiltros').attr('src', '<?php echo Yii::app()->baseUrl; ?>/images/filterPlus.png');
      }
      
    });
    
    $(".imgQuery").tooltip({
        placement : 'left'
    });    
    
  });
  
  
</script>