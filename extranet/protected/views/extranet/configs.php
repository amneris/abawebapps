<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 30/10/14
 * Time: 16:15
 */
$selectedIndex = 'Configuracion';
?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>

<div class="row-fluid">
    <div class="col-lg-12 marginTop15" style="padding-left: 0; padding-right: 0">
        <br>
        <?php
        $this->widget('zii.widgets.jui.CJuiTabs', array(
            'headerTemplate' => '<li><a href="{url}" title="{title}">{title}</a></li>',
            'tabs'=>array(
                Yii::t('app', 'General settings')=>$this->renderPartial('application.modules.enterprise.views.enterprise._enterprises', array('model'=>$enterprise, 'possibleMainContacts'=>$possibleMainContacts, 'resultMessage'=>$resultMessage),true),
                Yii::t('app', 'Users')=>$this->renderPartial('application.modules.user.views.user.index', array('mlLanguages' => $mlLanguages, 'mlRoles'=>$mlRoles),true),
                Yii::t('app', 'Groups')=>$this->renderPartial('application.modules.group.views.group.index', array('model'=>$group),true),
                Yii::t('app', 'Convocations')=>$this->renderPartial('application.modules.period.views.period.index', array('model'=>$period),true),
//          'StaticTab 2'=>array('content'=>'Content for tab 2', 'id'=>'tab2'),
            ),
            'themeUrl'=>'/css',
            'theme'=>'',
            'cssFile' => 'tabStyle.css',
            'options'=>array(
                'collapsible'=>false,
//            'selected'=>'General',
            ),

            'htmlOptions'=>array(
                'style'=>'width:100%;'
            ),
        ));
        ?>
    </div>
</div>

<script>
var selectedIndex = '<?php echo CJavaScript::quote($selectedIndex); ?>';
    function ABAsconfig_saveValue(id, idInput)
    {
        ABAShowLoading();

        var ar = idInput.split('_');
        var module = ar[0];
        var key = ar[1];
        if ( key == 'password' ) {
            var value_old = $( '#old'+idInput+'INPUT' ).val();
            var value_new = $( '#new'+idInput+'INPUT' ).val();
            var value_re = $( '#re'+idInput+'INPUT' ).val();
            if ( value_new != value_re ) { alert('<?php echo CJavaScript::quote( Yii::t('app', 'New password and retype are different.') ); ?>'); return; }
            var parameters = {
                id: id,
                key: key,
                oldpass: value_old,
                value: value_new,
            };
        }
        else {
            var value = $( '#'+idInput+'INPUT' ).val();
            var parameters = {
                id: id,
                key: key,
                value: value,
            };
        }


        var url = '<?php echo Yii::app()->baseUrl; ?>/'+module+'/'+module+'/ajaxUpdateValue/';
        var successF = function(data) {
            ABAStopLoading();
            if ( data.ok ) {
                $( '#'+idInput+'SPAN' ).html( value );
                $( '#'+idInput+'BOT' ).hide();
                $( '#'+idInput+'IMG' ).hide();
                $( '#'+idInput+'IMG' ).addClass("yeap");
            } else {
                alert( data.msgError );
            }
        };
        var errorF = function() { console.log('Ajax request failed.'); ABAStopLoading(); };
        var doneF = function() { };

        ABALaunchAjax(url, parameters, successF, errorF, doneF);
    }

    function ABAsconfig_Transform(id, idInput,value) {
        var content = $('#' + idInput +'SPAN').html();
        if ( content.substring(0, 6)  == '<input' ) return;

        $('#' + idInput + 'IMG').show();
        $('#' + idInput + 'IMG').removeClass("yeap");

        if(idInput =='user_password_')
        {
            $('#' + idInput +'SPAN').html("<input placeholder='Old password' size='35' type='password' id='old"+idInput+"INPUT' name='old"+idInput+"' class='form-control'><input style='margin-top:3px' placeholder='New password' size='35' type='password' id='new"+idInput+"INPUT' name='new"+idInput+"' class='form-control'> <input style='margin-top:3px' placeholder='Re type new password' size='35' type='password' id='re"+idInput+"INPUT' name='re"+idInput+"' class='form-control'>");
        }
        else
        {
            $('#' + idInput +'SPAN').html("<input size='35' type='text' id='"+idInput+"INPUT' name='"+idInput+"' value='"+value+"' class='form-control'> ");
        }

        $('#' + idInput + 'BOT').click( function() {
            ABAsconfig_saveValue( id, idInput );
        });
        $('#' + idInput + 'BOT').show();
    }

    function ABAconfig_QueryItem( id, module ) {
        ABAShowLoading();

        var url = '<?php echo Yii::app()->baseUrl; ?>/'+module+'/'+module+'/ajaxUpdate/id/'+id;
        var parameters = {};
        var successF = function(data) {
            if ( data.ok ) {
                ABAStopLoading();
                $('#divTab'+module.charAt(0).toUpperCase() + module.slice(1) ).html(data.html);
            }
        };
        var errorF = function() { console.log('Level Test error'); ABAStopLoading(); };
        var doneF = function() { };

        ABALaunchAjax(url, parameters, successF, errorF, doneF);
    }

</script>

