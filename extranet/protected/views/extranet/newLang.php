<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 09/12/14
 * Time: 12:02
 */
$selectedIndex = 'Configuracion';
?>
<?php /*$this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>


<table width="100%">
    <tr>
        <td style="padding-top: 20px; padding-left: 30px">
            <span class="sectionTitle"><?php echo Yii::t('app', 'Add a new translation language '); ?></span>
        </td>
    </tr>
</table>
<hr class="hrOverTitle">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'newLang-form',
    'enableAjaxValidation'=>false,
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
<div class="withBorderBottom">
    <table class="userTable">
        <tr>
            <td width="40px" class="paddingTop10">
            </td>
            <td class="paddingTop10">
                <span class="formLabel"><?php echo Yii::t('app', 'Select a target language'); ?></span> <br>
            </td>
        </tr>
        <tr>
            <td class="paddingBottom10">
            </td>
            <td class="paddingBottom10">
                <?php
                echo chtml::dropDownList('targetLang',1, $listOfLang);
                ?>
            </td>

        </tr>
    </table>
</div>

<div>
    <table class="userTable">
        <tr>
            <td width="40px" class="paddingTop10">
            </td>
            <td class="paddingTop10">
                <span class="formLabel"><?php echo Yii::t('app', 'Select source file (should be an .csv file)'); ?></span> <br>
            </td>
        </tr>
        <tr>
            <td class="paddingBottom10">
            </td>
            <td class="paddingBottom10">
                <input type="file" name="fileName" id="fileName" size="40">
            </td>
        </tr>
    </table>
</div>
<br>
<div style="text-align: center; margin-bottom: 2em">
    <button id="btnBack" type="submit" class="abaButton abaButton-Primary"><?php echo Yii::t('app','ADD'); ?></button>
</div>
<?php $this->endWidget(); ?>
