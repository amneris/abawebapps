<?php
$idEnterprise = Utils::getSessionVar('idEnterprise');
if ($idEnterprise > 0) {
    if (!empty(Utils::getSessionVar('enterpriseLogo'))) {
        $fileLogoEnterprise = getenv('AWS_URL_DOMAIN').Utils::getSessionVar('enterpriseLogo');
    } else {
        $fileLogoEnterprise = 'noLogo';
    }
}

if (!empty(Utils::getSessionVar('userAvatar'))) {
    $fileProfilePicture = getenv('AWS_URL_DOMAIN').Utils::getSessionVar('userAvatar');
} else {
    $fileProfilePicture = Yii::app()->baseUrl . "/images/userProfile/default.png";
}

$idRole = Utils::getSessionVar('idRole');
if ($idRole == Role::_RESPONSIBLE) {
    $homeUrl = Yii::app()->baseUrl . '/student/main';
} else {
    $homeUrl = Yii::app()->baseUrl . '/';
}

$studentName = Utils::getSessionVar('name', 'Undefined') . ' ' . Utils::getSessionVar('surname', '');

if (!isset($selectedIndex)) {
    $selectedIndex = 'none';
}

?>

<style>

</style>

<nav class="navbar-default rowHeader1" role="navigation">
    <div class="container-fluid navBarCentered">

        <div class="navbar-header pRelative">
            <button type="button" class="navbar-toggle collapsed rowHeader1" data-toggle="collapse"
                    data-target="#navbar-collapse-header">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="divHeaderLogo" data-url="<?php echo $homeUrl; ?>"><img class="logo"
                                                                               src="<?php echo Yii::app()->baseUrl; ?>/images/logo-aba-english-red.png">
            </div>
        </div>

        <div class="collapse navbar-collapse" id="navbar-collapse-header">
            <?php if ($idEnterprise > 0) { ?>
                <ul class="nav navbar-nav">
                    <li id="liHeaderLogo">&nbsp;</li>
                    <li class="verticalLineL <?php if ($selectedIndex == 'Alumno') {
                        echo "active"; } ?>" data-item="Alumno"
                        data-url="<?php echo Yii::app()->baseUrl; ?>/student/main">
                        <span><?php echo Yii::t('app', 'Student'); ?></span>
                    </li>

                    <?php /*
                    <li id="idLiMenuPrices" class="verticalLineL <?php if ($selectedIndex == 'Prices') {
                        echo "active"; } ?>" data-item="Prices" data-url="<?php echo Yii::app()->baseUrl; ?>/price/main">
                        <span><?php echo Yii::t('app', 'Prices'); ?></span>
                    </li>
                    */ ?>

                    <?php /*if($idRole == Role::_RESPONSIBLE) { ?>
        <?php } else if ( $idRole == Role::_ABAMANAGER || $idRole == Role::_AGENT ) { ?>
        <li class="dropdown  <?php if ( $selectedIndex == 'Prices' ) echo "active"; ?>">
          <div class="dropdown-toggle" data-toggle="dropdown"><span><?php echo CHtml::encode( Yii::t( 'app', 'Prices') ); ?></span> <span class="caret"></span></div>
          <ul class="dropdown-menu myDropDown" role="menu">
            <li class="borderList"><a href="<?php echo Yii::app()->baseUrl; ?>/price/main"><?php echo Yii::t('app', 'Request licences'); ?></a></li>
            <li class="borderList"><a href="<?php echo Yii::app()->baseUrl; ?>/purchase/purchase/admin"><?php echo Yii::t('app', 'Manage purchase'); ?></a></li>
          </ul>
        </li>
        <?php } */ ?>

                    <li class="verticalLineL <?php if ($selectedIndex == 'Analitica') {
                        echo "active"; } ?>" data-item="Analitica"
                        data-url="<?php echo Yii::app()->baseUrl; ?>/followup/followup/admin">
                        <span><?php echo Yii::t('app', 'Analytics'); ?></span>
                    </li>

                    <li class="verticalLineL <?php if ($selectedIndex == 'Configuracion') {
                        echo "active"; } ?>" data-item="Configuracion"
                        data-url="<?php echo Yii::app()->baseUrl; ?>/extranet/configs">
                        <span><?php echo Yii::t('app', 'Settings'); ?></span>
                    </li>

                    <li class="verticalLineL">
                        <form action="<?php echo Yii::app()->baseUrl; ?>/student/main"
                              class="navbar-form navbar-left formHeader" role="search" action="" method="post"
                              id="searchStudent">
                            <input type="text" class="form-control inputSearch"
                                   placeholder="<?php echo Yii::t('app', 'Find by email') ?>" name="email">
                        </form>
                    </li>
                </ul>
            <?php } ?>

            <!--User menu-->
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown verticalLineL">
                    <div id="divUserManu" class="dropdown-toggle divUserName" data-toggle="dropdown"><img class="img-circle" id="userIMG" src="<?php echo $fileProfilePicture; ?>">
                        &nbsp; <?php echo CHtml::encode($studentName); ?> <span class="caret"></span></div>
                    <ul class="dropdown-menu myDropDown" role="menu">
                        <?php if (Utils::getSessionVar('id') == User::_PKCARLES) { ?>
                            <li class="borderList"><a href="<?php echo Yii::app()->baseUrl; ?>/extranet/mainContact"">Test Crear Main Contact</a></li>
                            <li class="borderList"><a href="<?php echo Yii::app()->baseUrl; ?>/sourceTranslations/admin"">Translations</a></li>
                            <li class="borderList"><a href="<?php echo Yii::app()->baseUrl; ?>/extranet/downloadlogs"">Logs</a></li>
                            <li class="borderList"><a href="<?php echo Yii::app()->baseUrl; ?>/extranet/downloadbackups"">Backups</a></li>
                        <?php } ?>
                        <li class="borderList"><a href="<?php echo Yii::app()->baseUrl; ?>/user/user/update/?id=<?php echo Utils::getSessionVar('id'); ?>""><?php echo CHtml::encode(yii::t('app', 'Update account')); ?></a></li>
                        <li class="borderList"><a href="<?php echo Utils::getSessionVar('logoutUrl'); ?>"><?php echo CHtml::encode(yii::t('app', 'Sign out')); ?></a></li>
                    </ul>
                </li>
            </ul>
            <!--End user menu-->

        </div>
    </div>

    <div id="navbar-collapse-header2" class="container-fluid">
        <div class='divTextLogoEnterprise'>
            <?php if ($idEnterprise > 0) { ?>
                <div class='divImgLogoEnterprise'>
                    <?php
                    if ($fileLogoEnterprise == 'noLogo') {
                        echo "<div style='padding-top: 5px;'><div id='addLogo' class='noEnterpriseIMG'>" . Yii::t('app',
                            'logo') . "</div></div>";
                    } else {
                        echo "<img id='logoIMG' src='$fileLogoEnterprise'>";
                    }
                    ?>
                </div>
                <span class='spanTextLogoEnterprise'><?php echo Utils::getSessionVar('enterpriseName'); ?></span>
            <?php } ?>
        </div>
    </div>

</nav>

<script>
    $(document).ready(function () {
        $('#navbar-collapse-header li').click(function () {
            var url = $(this).data("url");
            if (typeof url == 'undefined') return;
            var parameters = {};

            ABAChangePage(url, parameters);
        });

        $('.divHeaderLogo').click(function () {
            var url = $(this).data("url");
            var parameters = {};

            ABAChangePage(url, parameters);
        });

        $('#addLogo').click(function () {
            var url = '/extranet/configs';
            if (typeof url == 'undefined') return;
            var parameters = {};

            ABAChangePage(url, parameters);
        });
    });
</script>    
