-- Tabla de Empresas.

CREATE TABLE enterprise (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,

    idMainContact INTEGER NOT NULL,
    idCountry INTEGER NOT NULL,
    idLanguage INTEGER NOT NULL,
    idType tinyint(4) unsigned NOT NULL default '0',
    idPartner INTEGER NOT NULL,                           -- Id Partner de la empresa en extranet
    idPartnerEnterprise INTEGER NOT NULL,                 -- Id Partner de intranet

    lastupdated TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER enterprise_insert BEFORE INSERT ON enterprise
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the udpate date
Set new.lastupdated = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER enterprise_update BEFORE UPDATE ON enterprise
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;

INSERT INTO enterprise (name,idMainContact) VALUES ('Enterprise1',1);
