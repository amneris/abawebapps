-- Tabla de Roles asignados a los usuarios.

CREATE TABLE enterprise_user_role (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idEnterprise INTEGER NOT NULL,
    idUser INTEGER NOT NULL,
    idRole INTEGER NOT NULL,

    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER enterprise_user_role_insert BEFORE INSERT ON enterprise_user_role
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER t_link_update BEFORE UPDATE ON enterprise_user_role
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;
