-- Tabla de Empresas.

CREATE TABLE blog_creator (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,

    name VARCHAR(50) NOT NULL,

    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER blog_creator_insert BEFORE INSERT ON blog_creator
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER blog_creator_update BEFORE UPDATE ON blog_creator
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;

