-- Tabla de datos de los test de nivel.

CREATE TABLE test_questions (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,

    idLevel INTEGER NOT NULL,
    question varchar(255) NOT NULL,
    answer INTEGER NOT NULL,
    option1 varchar(255) NOT NULL,
    option2 varchar(255) NOT NULL,
    option3 varchar(255) NOT NULL,
    type INTEGER NOT NULL,                      -- 1: Grammar, 2: Listenning, 3: Grammar-Alt 4: Unknown

    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER test_questions_insert BEFORE INSERT ON test_questions
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER test_questions_update BEFORE UPDATE ON test_questions
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;

