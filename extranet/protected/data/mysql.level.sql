-- Tabla de Empresas.

CREATE TABLE level (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,

    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER level_insert BEFORE INSERT ON level
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER level_update BEFORE UPDATE ON level
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;

INSERT INTO level (name) VALUES ('Begginner');
INSERT INTO level (name) VALUES ('Low intermediate');
INSERT INTO level (name) VALUES ('Intermediate');
INSERT INTO level (name) VALUES ('Upper intermediate');
INSERT INTO level (name) VALUES ('Advanced');
INSERT INTO level (name) VALUES ('Business');
