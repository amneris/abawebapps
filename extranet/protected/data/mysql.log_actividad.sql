-- Tabla de Logs de actividad sobre la web.

CREATE TABLE log_actividad (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    
    idUser integer default 0 not null,
    idEnterprise integer default 0 not null,
    idModule varchar(50) not null,
    controller varchar(50) NOT NULL,
    action varchar(50) not null,
    data VARCHAR(10000) NOT NULL,

    created DATETIME DEFAULT '2000-01-01 00:00:00' NOT NULL
);


-- alter table log rename to log_actividad;
-- alter table log_actividad add column idUser integer default 0 not null;
-- alter table log_actividad add column idEnterprise integer default 0 not null;
-- alter table log_actividad MODIFY data VARCHAR(10000) default '' not null;
-- alter table log_actividad add column idModule varchar(50) default '' not null;