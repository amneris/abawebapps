-- Tabla de roles

CREATE TABLE role (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,

    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER role_insert BEFORE INSERT ON role
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER role_update BEFORE UPDATE ON role
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;

INSERT INTO role (name) VALUES ('ABA Manager');
INSERT INTO role (name) VALUES ('Teacher');
INSERT INTO role (name) VALUES ('Responsible');
INSERT INTO role (name) VALUES ('Agent');
