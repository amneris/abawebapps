delete from LevelTest_Data;
delete from LevelTest_Log;
delete from egroup;
delete from enterprise where id not in (1);
delete from enterprise;
delete from enterprise_user_role where id not in (1);
delete from enterprise_user_role;
delete from license_asigned;
delete from license_available;
delete from log_actividad;
delete from period;
delete from purchase;
delete from selligent_queue;
delete from student;
delete from student_group;
delete from student_level_test;
delete from student_period;
delete from student_teacher;
delete from user where id not in (2,3,4,6);
delete from user;

INSERT INTO abaenglish_extranet.enterprise (id,name,idMainContact,idCountry,idLanguage,idPartner,idPartnerEnterprise,lastupdated,created,deleted,isdeleted) VALUES (1,'ABA English',6,199,2,200000,200001,{ts '2014-12-10 12:09:17'},{ts '2014-11-19 19:05:35'},{ts '2014-11-19 19:05:35'},0);
INSERT INTO abaenglish_extranet.user (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted,autologincode) VALUES (2,'Demo','Agent','0cQ9KwNMl97gY','demoagent@abaenglish.com',5,{ts '2014-11-19 19:17:00'},{ts '2014-11-19 19:17:00'},{ts '2014-11-19 19:17:00'},0,'');
INSERT INTO abaenglish_extranet.user (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted,autologincode) VALUES (3,'Demo','','0cQ9KwNMl97gY','demo@abaenglish.com',5,{ts '2014-11-19 19:17:00'},{ts '2014-11-19 19:17:00'},{ts '2014-11-19 19:17:00'},0,'');
INSERT INTO abaenglish_extranet.user (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted,autologincode) VALUES (4,'Luis','Assandri','0cQ9KwNMl97gY','lassandri@abaenglish.com',2,{ts '2014-11-20 09:57:59'},{ts '2014-11-19 19:12:28'},{ts '2014-11-19 19:12:28'},0,'279af896c1e7bcbece6cc866ffb55009');
INSERT INTO abaenglish_extranet.user (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted,autologincode) VALUES (6,'Carles','Day','0cQ9KwNMl97gY','cejarque@abaenglish.com',1,{ts '2014-12-10 12:27:38'},{ts '2014-10-15 09:38:33'},{ts '2014-10-15 09:38:33'},0,'860884ea645a1d2fb2de13e61df6fcd7');
INSERT INTO abaenglish_extranet.enterprise_user_role (id,idEnterprise,idUser,idRole,created,deleted,isdeleted) VALUES (1,1,2,4,{ts '2014-11-19 19:05:35'},{ts '2014-11-19 19:05:35'},0);
INSERT INTO abaenglish_extranet.enterprise_user_role (id,idEnterprise,idUser,idRole,created,deleted,isdeleted) VALUES (2,1,3,4,{ts '2014-11-19 19:05:35'},{ts '2014-11-19 19:05:35'},0);
INSERT INTO abaenglish_extranet.enterprise_user_role (id,idEnterprise,idUser,idRole,created,deleted,isdeleted) VALUES (3,1,4,4,{ts '2014-11-19 19:05:35'},{ts '2014-11-19 19:05:35'},0);
INSERT INTO abaenglish_extranet.enterprise_user_role (id,idEnterprise,idUser,idRole,created,deleted,isdeleted) VALUES (4,1,6,4,{ts '2014-11-19 19:05:35'},{ts '2014-11-19 19:05:35'},0);

ALTER TABLE LevelTest_Data AUTO_INCREMENT=1;
ALTER TABLE LevelTest_Log AUTO_INCREMENT=1;
ALTER TABLE egroup AUTO_INCREMENT=1;
ALTER TABLE enterprise AUTO_INCREMENT=2;
ALTER TABLE enterprise_user_role AUTO_INCREMENT=5;
ALTER TABLE license_asigned AUTO_INCREMENT=1;
ALTER TABLE license_available AUTO_INCREMENT=1;
ALTER TABLE log_actividad AUTO_INCREMENT=1;
ALTER TABLE period AUTO_INCREMENT=1;
ALTER TABLE purchase AUTO_INCREMENT=1;
ALTER TABLE student AUTO_INCREMENT=1;
ALTER TABLE student_group AUTO_INCREMENT=1;
ALTER TABLE student_level_test AUTO_INCREMENT=1;
ALTER TABLE student_period AUTO_INCREMENT=1;
ALTER TABLE student_teacher AUTO_INCREMENT=1;
ALTER TABLE user AUTO_INCREMENT=7;



--delete from license_available where idLicenseType != 5;



-- INSERT INTO `abaenglish_extranet`.`enterprise` (id,name,idMainContact,idCountry,idLanguage,idPartner,idPartnerEnterprise,lastupdated,created,deleted,isdeleted) VALUES (1,'AbaManager',4,1,1,0,0,{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},0);
-- INSERT INTO `abaenglish_extranet`.`enterprise` (id,name,idMainContact,idCountry,idLanguage,idPartner,idPartnerEnterprise,lastupdated,created,deleted,isdeleted) VALUES (2,'Aemet',4,199,1,7001,0,{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},0);
-- INSERT INTO `abaenglish_extranet`.`enterprise` (id,name,idMainContact,idCountry,idLanguage,idPartner,idPartnerEnterprise,lastupdated,created,deleted,isdeleted) VALUES (3,'Junta Extremadura',13,199,2,7004,0,{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},0);
-- INSERT INTO `abaenglish_extranet`.`enterprise` (id,name,idMainContact,idCountry,idLanguage,idPartner,idPartnerEnterprise,lastupdated,created,deleted,isdeleted) VALUES (4,'MC Mutual',4,1,1,119,0,{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},0);
-- INSERT INTO `abaenglish_extranet`.`enterprise` (id,name,idMainContact,idCountry,idLanguage,idPartner,idPartnerEnterprise,lastupdated,created,deleted,isdeleted) VALUES (5,'Hays Francia',13,1,1,121,0,{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},0);
-- INSERT INTO `abaenglish_extranet`.`enterprise` (id,name,idMainContact,idCountry,idLanguage,idPartner,idPartnerEnterprise,lastupdated,created,deleted,isdeleted) VALUES (6,'Colegio Vedruna Gracia',4,1,1,7003,0,{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},0);
-- INSERT INTO `abaenglish_extranet`.`enterprise` (id,name,idMainContact,idCountry,idLanguage,idPartner,idPartnerEnterprise,lastupdated,created,deleted,isdeleted) VALUES (7,'AMC Networks',4,1,1,7027,0,{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},0);
-- INSERT INTO `abaenglish_extranet`.`enterprise` (id,name,idMainContact,idCountry,idLanguage,idPartner,idPartnerEnterprise,lastupdated,created,deleted,isdeleted) VALUES (8,'Test1',19,10,3,0,0,{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},0);
-- INSERT INTO `abaenglish_extranet`.`enterprise` (id,name,idMainContact,idCountry,idLanguage,idPartner,idPartnerEnterprise,lastupdated,created,deleted,isdeleted) VALUES (9,'Olimpiadas',20,199,2,0,0,{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},0);
-- INSERT INTO `abaenglish_extranet`.`enterprise` (id,name,idMainContact,idCountry,idLanguage,idPartner,idPartnerEnterprise,lastupdated,created,deleted,isdeleted) VALUES (10,'ABA English',21,199,2,0,0,{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},0);
-- INSERT INTO `abaenglish_extranet`.`enterprise` (id,name,idMainContact,idCountry,idLanguage,idPartner,idPartnerEnterprise,lastupdated,created,deleted,isdeleted) VALUES (11,'Verdana',22,199,1,0,0,{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},{ts '2014-11-13 17:33:26'},0);
-- INSERT INTO `abaenglish_extranet`.`enterprise` (id,name,idMainContact,idCountry,idLanguage,idPartner,idPartnerEnterprise,lastupdated,created,deleted,isdeleted) VALUES (16,'Atletas de España',25,199,2,200000,200001,{ts '2014-11-13 17:44:54'},{ts '2014-11-13 17:44:54'},{ts '2014-11-13 17:44:54'},0);
-- INSERT INTO `abaenglish_extranet`.`enterprise` (id,name,idMainContact,idCountry,idLanguage,idPartner,idPartnerEnterprise,lastupdated,created,deleted,isdeleted) VALUES (17,'Nombre3',28,199,2,200002,200003,{ts '2014-11-17 16:56:58'},{ts '2014-11-17 16:56:58'},{ts '2014-11-17 16:56:58'},0);
-- INSERT INTO `abaenglish_extranet`.`enterprise` (id,name,idMainContact,idCountry,idLanguage,idPartner,idPartnerEnterprise,lastupdated,created,deleted,isdeleted) VALUES (18,'Verdenegro2',29,199,2,200004,200005,{ts '2014-11-17 17:10:38'},{ts '2014-11-17 17:04:25'},{ts '2014-11-17 17:04:25'},0);
-- INSERT INTO `abaenglish_extranet`.`enterprise` (id,name,idMainContact,idCountry,idLanguage,idPartner,idPartnerEnterprise,lastupdated,created,deleted,isdeleted) VALUES (19,'Verdenegro',30,199,2,200006,200007,{ts '2014-11-17 17:10:44'},{ts '2014-11-17 17:10:44'},{ts '2014-11-17 17:10:44'},0);
-- INSERT INTO `abaenglish_extranet`.`enterprise` (id,name,idMainContact,idCountry,idLanguage,idPartner,idPartnerEnterprise,lastupdated,created,deleted,isdeleted) VALUES (20,'Test Anna',31,199,2,200008,200009,{ts '2014-11-19 18:10:26'},{ts '2014-11-19 18:10:26'},{ts '2014-11-19 18:10:26'},0);
-- INSERT INTO `abaenglish_extranet`.`enterprise` (id,name,idMainContact,idCountry,idLanguage,idPartner,idPartnerEnterprise,lastupdated,created,deleted,isdeleted) VALUES (21,'Abc',32,199,2,200010,200011,{ts '2014-11-19 18:12:11'},{ts '2014-11-19 18:12:11'},{ts '2014-11-19 18:12:11'},0);
-- INSERT INTO `abaenglish_extranet`.`enterprise` (id,name,idMainContact,idCountry,idLanguage,idPartner,idPartnerEnterprise,lastupdated,created,deleted,isdeleted) VALUES (22,'Abc2',33,199,2,200012,200013,{ts '2014-11-19 18:14:09'},{ts '2014-11-19 18:14:09'},{ts '2014-11-19 18:14:09'},0);


-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (4,'Luis','Assandri','0cQ9KwNMl97gY','lassandri@abaenglish.com',5,{ts '2014-10-29 12:02:18'},{ts '2014-10-15 09:38:33'},{ts '2014-10-15 09:38:33'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (5,'Miguel','Barahona','0cQ9KwNMl97gY','mbarahona@abaenglish.com',1,{ts '2014-10-20 16:52:36'},{ts '2014-10-15 09:38:33'},{ts '2014-10-15 09:38:33'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (6,'Carles','Day','0cQ9KwNMl97gY','cejarque@abaenglish.com',1,{ts '2014-11-18 14:28:06'},{ts '2014-10-15 09:38:33'},{ts '2014-10-15 09:38:33'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (7,'Pilar','Cortés','0cQ9KwNMl97gY','pcortes@abaenglish.com',1,{ts '2014-10-20 16:52:36'},{ts '2014-10-15 09:38:33'},{ts '2014-10-15 09:38:33'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (15,'testing','role asignament','dafdasd','asdfasd',1,{ts '2014-10-28 13:04:48'},{ts '2014-10-28 13:04:48'},{ts '2014-10-28 13:04:48'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (13,'Luís','López','0cQ9KwNMl97gY','llopez@abaenglish.com',2,{ts '2014-10-20 16:52:36'},{ts '2014-10-15 12:07:45'},{ts '2014-10-15 12:07:45'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (18,'Demo','ABA Manager','0cQ9KwNMl97gY','demomanager@abaenglish.com',5,{ts '2014-10-28 13:46:26'},{ts '2014-10-28 13:46:26'},{ts '2014-10-28 13:46:26'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (17,'Demo','','0cQ9KwNMl97gY','demo@abaenglish.com',5,{ts '2014-10-28 13:46:26'},{ts '2014-10-28 13:46:26'},{ts '2014-10-28 13:46:26'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (16,'Demo','Agent','0cQ9KwNMl97gY','demoagent@abaenglish.com',5,{ts '2014-10-28 13:46:26'},{ts '2014-10-28 13:46:26'},{ts '2014-10-28 13:46:26'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (19,'dfgdf','fgdf','$1$hyijV7Zu$m2p4VfqMRyRnYaeN8XmdM/','gdfgd@sdjhf.es',3,{ts '2014-10-29 17:08:04'},{ts '2014-10-29 17:08:04'},{ts '2014-10-29 17:08:04'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (20,'Sebastian','Martos','0cQ9KwNMl97gY','smartos@aespanya.com',2,{ts '2014-11-13 15:21:51'},{ts '2014-11-13 15:18:53'},{ts '2014-11-13 15:18:53'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (21,'Luis','Assandri','0cQ9KwNMl97gY','lassandri2@abaenglish.com',2,{ts '2014-11-13 15:40:49'},{ts '2014-11-13 15:40:11'},{ts '2014-11-13 15:40:11'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (22,'Marcos','Selligent','$1$y1N3q4eD$Daj/doX.tk62M.WyBw34e.','mselligent@gmail.com',1,{ts '2014-11-13 17:03:38'},{ts '2014-11-13 17:03:38'},{ts '2014-11-13 17:03:38'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (23,'Igor','Caceres','$1$rF1FOHeG$ZsLZuR3Euk8Sl/S/2KwqC0','icaceres@aespanya.com',2,{ts '2014-11-13 17:43:00'},{ts '2014-11-13 17:43:00'},{ts '2014-11-13 17:43:00'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (24,'Igor','Caceres','$1$JSLQyn5P$UgzMnLsrkgsYnR9GgwYqy/','icaceres2@aespanya.com',2,{ts '2014-11-13 17:44:05'},{ts '2014-11-13 17:44:05'},{ts '2014-11-13 17:44:05'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (25,'Igor','Caceres','$1$UEMsv2uJ$h5UELVxR32KOjEkCTApew0','icaceres3@aespanya.com',2,{ts '2014-11-13 17:44:54'},{ts '2014-11-13 17:44:54'},{ts '2014-11-13 17:44:54'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (26,'NombreResponsable','ApellidosResponsable','$1$zgKWCYNX$E9fiLymeUDeA7O3omLjdc.','nn@mail.com',2,{ts '2014-11-17 16:48:50'},{ts '2014-11-17 16:48:50'},{ts '2014-11-17 16:48:50'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (27,'dfsd','fsdfsd','$1$XabmKpHl$mcHnEoZDXQO8b7hYW.yrn1','mail@mail.com',2,{ts '2014-11-17 16:53:27'},{ts '2014-11-17 16:53:27'},{ts '2014-11-17 16:53:27'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (28,'sedfds','fsdfsd','$1$ujvruVNb$nGmnLzGkJOF0Qen2DZp7O.','mail2@mail.com',2,{ts '2014-11-17 16:56:58'},{ts '2014-11-17 16:56:58'},{ts '2014-11-17 16:56:58'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (29,'Test','Test','0cQ9KwNMl97gY','test1@gmail.com',2,{ts '2014-11-17 17:07:46'},{ts '2014-11-17 17:04:25'},{ts '2014-11-17 17:04:25'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (30,'Carles','Ejarque','0cQ9KwNMl97gY','carles74.test1@gmail.com',2,{ts '2014-11-17 17:12:40'},{ts '2014-11-17 17:10:44'},{ts '2014-11-17 17:10:44'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (31,'Anna','Mora','$1$Zf/NsUyT$zhLgP.dg93GR6W3cDC5qq/','test.b2b@yopmail.com',2,{ts '2014-11-19 18:10:26'},{ts '2014-11-19 18:10:26'},{ts '2014-11-19 18:10:26'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (32,'Abc','Abc','$1$xjqXjMfw$/9b/emBXgPIKk/sd7QSCs1','Abc@yopmail.com',2,{ts '2014-11-19 18:12:11'},{ts '2014-11-19 18:12:11'},{ts '2014-11-19 18:12:11'},0);
-- INSERT INTO `abaenglish_extranet`.`user` (id,name,surname,password,email,idLanguage,lastupdated,created,deleted,isdeleted) VALUES (33,'Abcdf','Abcdf','$1$YlwPiEwj$WxPTtoSEJPLbRKLOuty.S/','Abc2@yopmail.com',2,{ts '2014-11-19 18:14:09'},{ts '2014-11-19 18:14:09'},{ts '2014-11-19 18:14:09'},0);


-- ALTER TABLE `egroup` AUTO_INCREMENT=1;