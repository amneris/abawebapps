-- Tabla con el limite horario en el que NO pueden entrar los alumnos en el campus.

CREATE TABLE enterprise_timerange (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,

    idEnterprise INTEGER NOT NULL,
    diasemana  INTEGER NOT NULL,          -- Formato N
    horainicio  INTEGER NOT NULL,         -- Formato Gi
    horafin  INTEGER NOT NULL,            -- Formato Gi

    lastupdated TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER enterprise_timerange_insert BEFORE INSERT ON enterprise_timerange
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the udpate date
Set new.lastupdated = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER enterprise_timerange_update BEFORE UPDATE ON enterprise_timerange
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;