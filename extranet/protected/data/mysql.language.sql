-- Tabla de Idiomas

CREATE TABLE language (
  id int(11) NOT NULL auto_increment,
  name varchar(128) NOT NULL,
  iso varchar(5) NOT NULL,
  enabled tinyint(4) default '1',
  courseLang tinyint(4) default '0',
);

insert into language (id, name, iso) values (1, 'English', 'en');
insert into language (id, name, iso) values (1, 'Spanish', 'es');
insert into language (id, name, iso) values (1, 'Italian', 'it');
insert into language (id, name, iso) values (1, 'French', 'fr');
insert into language (id, name, iso) values (1, 'German', 'de');
insert into language (id, name, iso) values (1, 'Portuguese', 'pt');
