-- Tabla de Empresas.

CREATE TABLE blog_subscriber (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,

    email VARCHAR(128) NOT NULL,
    idBlog INTEGER NOT NULL,
    confirmed INTEGER NOT NULL,
    idStudentOnCampus INTEGER DEFAULT 0 NOT NULL,
    studentType INTEGER DEFAULT 3 NOT NULL COMMENT '1:Free., 2:Premium,  3:Not User',
    updated DATETIME DEFAULT 0 NOT NULL,

    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER blog_subscriber_insert BEFORE INSERT ON blog_subscriber
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER blog_subscriber_update BEFORE UPDATE ON blog_subscriber
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;

