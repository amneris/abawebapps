-- Tabla de Convocatorias

CREATE TABLE period (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idEnterprise INTEGER NOT NULL,
    name VARCHAR(128) NOT NULL,

    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER period_insert BEFORE INSERT ON period
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER period_update BEFORE UPDATE ON period
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;

INSERT INTO period (name, idEnterprise) VALUES ('Convocatoria1', 1);
