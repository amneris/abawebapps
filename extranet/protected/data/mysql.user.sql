-- Tabla de usuarios

CREATE TABLE user (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    surname VARCHAR(128) NOT NULL,
    password VARCHAR(128) NOT NULL,
    email VARCHAR(128) NOT NULL,
    idLanguage INTEGER NOT NULL,

    autologincode varchar(255) not null,

    lastupdated TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER user_insert BEFORE INSERT ON user
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the udpate date
Set new.lastupdated = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER user_update BEFORE UPDATE ON user
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;

-- INSERT INTO user (name, surname, password, email) VALUES ('user1', 'surname1', 'pass1', 'user1@mail.com');
-- update user set deleted = now() where id = 1;
-- update user set deleted = '2014-09-26 10:58:36.0' where id = 1;
-- update user set deleted = created where id = 1;
-- 
-- INSERT INTO user (name,surname,password,email) VALUES ('user1','surname1','pass1','user1@mail.com');
-- INSERT INTO user (name,surname,password,email) VALUES ('user1','surname1','pass1','user1@mail.com');
-- INSERT INTO user (name,surname,password,email) VALUES ('user1','surname1','pass1','user1@mail.com');
-- INSERT INTO user (name,surname,password,email) VALUES ('user1','surname1','pass1','user1@mail.com');
-- INSERT INTO user (name,surname,password,email) VALUES ('user1','surname1','pass1','user1@mail.com');
-- INSERT INTO user (name,surname,password,email) VALUES ('user1','surname1','pass1','user1@mail.com');
-- INSERT INTO user (name,surname,password,email) VALUES ('user1','surname1','pass1','user1@mail.com');
-- INSERT INTO user (name,surname,password,email) VALUES ('user1','surname1','pass1','user1@mail.com');
-- INSERT INTO user (name,surname,password,email) VALUES ('user1','surname1','pass1','user1@mail.com');
-- INSERT INTO user (name,surname,password,email) VALUES ('user1','surname1','pass1','user1@mail.com');
-- INSERT INTO user (name,surname,password,email) VALUES ('Administrador','','nopass','admin@abaenglish.com');
