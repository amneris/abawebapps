-- Vista de la intranet, seccion Moodle->Users

CREATE VIEW MoodleUserList AS
  select p.ID_preinsc as idPreinscription, s.id_learner as idLearner, s.sc_course_id as idCourse, s.sc_date_limit, p.password, l.expiration_date, l.last_status, s.sc_email
  from preinscription p, learner l, t_scorm_enjoy s
  where p.ID_learner = l.ID_learner
    and p.ID_learner = s.ID_learner;


