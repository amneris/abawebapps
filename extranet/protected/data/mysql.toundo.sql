-- Tabla

CREATE TABLE toundo (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,

    idElement INTEGER NOT NULL,
    code VARCHAR(256) NOT NULL,

    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL,
);

CREATE TRIGGER toundo_insert BEFORE INSERT ON toundo
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER toundo_update BEFORE UPDATE ON toundo
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;

