-- Tabla de Publicaciones.

CREATE TABLE blog_newsletter_publication (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,

    idBlog INTEGER NOT NULL,
    status INTEGER DEFAULT 1 NOT NULL COMMENT '1:New., 2:In progress,  3:Closed',
    dateToSend DATETIME DEFAULT 0 NOT NULL,

    textSubject varchar(60) DEFAULT '' NOT NULL,
    textHeader varchar(1000) DEFAULT '' NOT NULL,
    textTweet varchar(80) DEFAULT '' NOT NULL,
    linkTweet varchar(80) DEFAULT '' NOT NULL,

    idPost1 INTEGER DEFAULT 0 NOT NULL,
    descriptionPost1 varchar(120) DEFAULT '' NOT NULL,
    linkPost1 varchar(15) DEFAULT '' NOT NULL,
    idPost2 INTEGER DEFAULT 0 NOT NULL,
    descriptionPost2 varchar(120) DEFAULT '' NOT NULL,
    linkPost2 varchar(15) DEFAULT '' NOT NULL,
    idPost3 INTEGER DEFAULT 0 NOT NULL,
    descriptionPost3 varchar(120) DEFAULT '' NOT NULL,
    linkPost3 varchar(15) DEFAULT '' NOT NULL,
    idPost4 INTEGER DEFAULT 0 NOT NULL,
    descriptionPost4 varchar(120) DEFAULT '' NOT NULL,
    linkPost4 varchar(15) DEFAULT '' NOT NULL,
    idPost5 INTEGER DEFAULT 0 NOT NULL,
    descriptionPost5 varchar(120) DEFAULT '' NOT NULL,
    linkPost5 varchar(15) DEFAULT '' NOT NULL,

    lastupdated DATETIME DEFAULT 0 NOT NULL,

    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER blog_newsletter_publication_insert BEFORE INSERT ON blog_newsletter_publication
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER blog_newsletter_publication_update BEFORE UPDATE ON blog_newsletter_publication
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;

-- insert into blog_newsletter_publication ( idBlog, status, dateToSend, lastupdated ) values ( 1, 1, '2015-05-09', now() );
-- insert into blog_newsletter_publication ( idBlog, status, dateToSend, lastupdated ) values ( 2, 1, '2015-05-09', now() );
-- insert into blog_newsletter_publication ( idBlog, status, dateToSend, lastupdated ) values ( 3, 1, '2015-05-09', now() );
-- insert into blog_newsletter_publication ( idBlog, status, dateToSend, lastupdated ) values ( 4, 1, '2015-05-09', now() );
-- insert into blog_newsletter_publication ( idBlog, status, dateToSend, lastupdated ) values ( 5, 1, '2015-05-09', now() );



