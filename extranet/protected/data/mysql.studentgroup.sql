-- Tabla que relaciona los Alumnos con las empresas.

CREATE TABLE student_group (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idStudent INTEGER NOT NULL,
    idGroup INTEGER NOT NULL,
    idEnterprise INTEGER NOT NULL,
    status SMALLINT(2) NOT NULL default 1,

    idPeriod INTEGER NOT NULL,
    expirationDate DATETIME DEFAULT 0 NOT NULL,

    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER student_group_insert BEFORE INSERT ON student_group
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER student_group_update BEFORE UPDATE ON student_group
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;


ALTER TABLE abaenglish_extranet.student_group
ADD COLUMN `status` SMALLINT(2) NOT NULL default 1 AFTER `idPeriod`;

