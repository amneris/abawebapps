-- Tabla de compras de licencias.

CREATE TABLE purchase (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idUser INTEGER NOT NULL,
    idEnterprise INTEGER NOT NULL,

    idLicenseType integer not null,
    numLicenses integer not null,

    bought datetime default 0 not null,
    validated tinyint(1) default 0 not null,

    value double default 0 not null,
    valueLocal double default 0 not null,
    discount FLOAT DEFAULT 0 NOT NULL,
    idCurrency varchar(10) default 'EUR' not null,

    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER purchase_insert BEFORE INSERT ON purchase
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER purchase_update BEFORE UPDATE ON purchase
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;


