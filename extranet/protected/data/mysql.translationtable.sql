-- Tabla de grupo de alumnos de una empresa.

CREATE TABLE translationtable (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idController VARCHAR(128) DEFAULT '' NOT NULL,
    idAction VARCHAR(128) DEFAULT '' NOT NULL,

    category VARCHAR(128) NOT NULL,
    message VARCHAR(2000) NOT NULL,

    params VARCHAR(2000) NOT NULL,
    source VARCHAR(2000) NOT NULL,
    language VARCHAR(2000) NOT NULL,

    created DATETIME DEFAULT 0 NOT NULL
);
