-- Tabla de Alumnos.

CREATE TABLE student (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Id',
    name VARCHAR(128) NOT NULL,
    surname VARCHAR(128) NOT NULL,
    email VARCHAR(128) NOT NULL,
    idEnterprise integer default 0 not null,
    idLevel INTEGER DEFAULT -2 NOT NULL COMMENT 'FK to Level Table.',
    idStudentCampus INTEGER DEFAULT 0 NOT NULL COMMENT 'FK to Campus Student Table.',

    lastupdated TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER student_insert BEFORE INSERT ON student
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the udpate date
Set new.lastupdated = now();

-- Set the deleted date
Set new.deleted = now();

END;

CREATE TRIGGER student_update BEFORE UPDATE ON student
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;

INSERT INTO student (name, surname, email) VALUES ('test1', 'surtest1', 'test1@mail.com');
update student set deleted = now() where id = 1;
update student set deleted = '2014-09-26 10:58:36.0' where id = 1;
update student set deleted = created where id = 1;

INSERT INTO student (name,surname,email) VALUES ('test1','surtest1','test1@mail.com');
INSERT INTO student (name,surname,email) VALUES ('bla','ble','bli');
INSERT INTO student (name,surname,email) VALUES ('dfsd','fds','fsd');
INSERT INTO student (name,surname,email) VALUES ('dfsd','fds','fsd');
INSERT INTO student (name,surname,email) VALUES ('dfsd','fds','fsd');
INSERT INTO student (name,surname,email) VALUES ('dfsd','fds','fsd');
INSERT INTO student (name,surname,email) VALUES ('Name1','Surname1','email1');
INSERT INTO student (name,surname,email) VALUES ('Name2','Surname2','Email2');
INSERT INTO student (name,surname,email) VALUES ('Name3','Surname3','Email3');
INSERT INTO student (name,surname,email) VALUES ('Alumno','dfsd','fdsfsd');
