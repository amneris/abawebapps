-- Tabla que relaciona los Alumnos con las empresas.

CREATE TABLE student_period (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idStudent INTEGER NOT NULL,
    idPeriod INTEGER NOT NULL,
    startDate DATETIME DEFAULT '2000-01-01 00:00:00.0' NOT NULL,
    expirationDate DATETIME DEFAULT '2000-01-01 00:00:00.0' NOT NULL,

    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER student_period_insert BEFORE INSERT ON student_period
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER student_period_update BEFORE UPDATE ON student_period
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;

ALTER TABLE abaenglish_extranet.student_period
ADD COLUMN expirationDate DATETIME DEFAULT '2000-01-01 00:00:00.0' NOT NULL AFTER `idPeriod`;

ALTER TABLE abaenglish_extranet.student_period
ADD COLUMN startDate DATETIME DEFAULT '2000-01-01 00:00:00.0' NOT NULL AFTER `idPeriod`;


