-- Tabla de Empresas.

CREATE TABLE student_level (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idLevel INTEGER,
    idStudent INTEGER,

    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER student_level_insert BEFORE INSERT ON student_level
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER student_level_update BEFORE UPDATE ON student_level
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;

