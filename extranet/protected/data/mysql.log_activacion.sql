-- Tabla de Logs de actividad sobre la web.

CREATE TABLE log_activacion (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    
    idStudent integer default 0 not null,
    oldTeacherEmail varchar(255) default '' not null,
    oldIdPartnerCurrent integer default 0 not null,

    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER log_activacion_insert BEFORE INSERT ON log_activacion
FOR EACH ROW BEGIN
-- Set the creation date
  SET new.created = now();

-- Set the deleted date
  Set new.deleted = now();
END;

CREATE TRIGGER log_activacion_update BEFORE UPDATE ON log_activacion
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else
    set new.isdeleted = 0;
  end if;

END;
