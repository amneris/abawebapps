-- Tabla de licencias disponibles por empresa.

CREATE TABLE license_asigned (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idStudent INTEGER NOT NULL,
    idEnterprise INTEGER NOT NULL,
    idPeriod INTEGER NOT NULL,
    idLicenseType integer not null,

    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER license_asigned_insert BEFORE INSERT ON license_asigned
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER license_asigned_update BEFORE UPDATE ON license_asigned
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;
