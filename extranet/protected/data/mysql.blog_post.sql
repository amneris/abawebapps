-- Tabla de Empresas.

CREATE TABLE blog_post (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,

    idBlog INTEGER NOT NULL,
    idPost INTEGER NOT NULL,
    idPublication INTEGER DEFAULT 0 NOT NULL,

    title VARCHAR(256) NOT NULL,
    link VARCHAR(256) NOT NULL,
    urlImage varchar(256) NOT NULL,
    linkComments VARCHAR(256) NOT NULL,
    pubDate DATETIME DEFAULT 0 NOT NULL,
    idCreator INTEGER NOT NULL,
    description VARCHAR(2000) NOT NULL,

    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER blog_post_insert BEFORE INSERT ON blog_post
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER blog_post_update BEFORE UPDATE ON blog_post
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;

