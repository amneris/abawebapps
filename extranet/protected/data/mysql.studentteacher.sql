-- Tabla que relaciona los Alumnos con las empresas.

CREATE TABLE student_teacher (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idStudent INTEGER NOT NULL,
    idEnterprise INTEGER NOT NULL,
    idUser INTEGER NOT NULL,

    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER student_teacher_insert BEFORE INSERT ON student_teacher
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER student_teacher_update BEFORE UPDATE ON student_teacher
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;



