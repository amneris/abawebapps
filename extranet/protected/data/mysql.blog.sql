-- Tabla de Empresas.

CREATE TABLE blog (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,

    name VARCHAR(50) NOT NULL,
    rssUrl VARCHAR(256) NOT NULL,

    idLanguage INTEGER NOT NULL,

    created DATETIME DEFAULT 0 NOT NULL,
    deleted DATETIME DEFAULT 0 NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER blog_insert BEFORE INSERT ON blog
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER blog_update BEFORE UPDATE ON blog
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;

INSERT INTO blog (name,rssUrl,idLanguage) VALUES ('BlogES','https://www.abaenglish.com/blog/es/feed',2);
INSERT INTO blog (name,rssUrl,idLanguage) VALUES ('BlogIT','https://www.abaenglish.com/blog/it/feed',3);
INSERT INTO blog (name,rssUrl,idLanguage) VALUES ('BlogFR','https://www.abaenglish.com/blog/fr/feed',4);
INSERT INTO blog (name,rssUrl,idLanguage) VALUES ('BlogPT','https://www.abaenglish.com/blog/pt/feed',6);
INSERT INTO blog (name,rssUrl,idLanguage) VALUES ('ABAEnglish','https://www.abaenglish.com/blog/feed',1);
