-- Tabla de tipos de licencias.

CREATE TABLE license_type (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name varchar(50) not null,
    idPeriodPay varchar(10) not null,
    strDesc varchar(150) not null,
    isEnabled tinyint(1) not null
);

insert into license_type (name,idPeriodPay,strDesc,isEnabled) values ('Trimestral', 90, 'paidment each 3 months', true);
insert into license_type (name,idPeriodPay,strDesc,isEnabled) values ('Semestral', 180, 'paidment each 6 months', true);
insert into license_type (name,idPeriodPay,strDesc,isEnabled) values ('Annual', 360, 'paidment each 12 months', true);
insert into license_type (name,idPeriodPay,strDesc,isEnabled) values ('Two years', 720, 'paidment each 2 years', true);
