-- Tabla de la cola de envios a Selligent.

create table selligent_queue (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    action VARCHAR(128) NOT NULL,
    data varchar(2000) not null,
    
    status integer Default 0 not null,                                      -- 0: Pendiente de ser enviado. 1: Enviado. 2: Error.
    sent DATETIME DEFAULT '2000-01-01 00:00:00' NOT NULL,
    result integer default 0 not null,                                      -- 
    result_txt varchar(2000) default '' Not null,
    callback varchar(200) Not null,

    created DATETIME DEFAULT '2000-01-01 00:00:00' NOT NULL,
    deleted DATETIME DEFAULT '2000-01-01 00:00:00' NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER selligent_queue_insert BEFORE INSERT ON selligent_queue
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the deleted date
Set new.deleted = now();
END;

CREATE TRIGGER selligent_queue_update BEFORE UPDATE ON selligent_queue
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

END;
