-- Tabla de licencias disponibles por empresa.

CREATE TABLE license_available (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idEnterprise INTEGER NOT NULL,

    idLicenseType integer not null,
    discount FLOAT DEFAULT 0 NOT NULL,
    numLicenses integer not null,
    histLicenses integer not null,

    lastupdated TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);