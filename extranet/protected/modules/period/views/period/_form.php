<?php
/* @var $this PeriodController */
/* @var $model Period */
/* @var $form CActiveForm */
?>
<div class="row-fluid">
    <div class="col-lg-12">
        <div class="form">

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'period-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation'=>false,
        )); ?>

            <p class="note">Fields with <span class="required">*</span> are required.</p>

            <?php echo $form->errorSummary($model); ?>

            <div class="row">
                <?php echo $form->labelEx($model,'idEnterprise'); ?>
                <?php
                    echo $form->dropDownList($model,'idEnterprise', $dropDownEnterprise);
                ?>
                <?php echo $form->error($model,'idEnterprise'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'name'); ?>
                <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>128)); ?>
                <?php echo $form->error($model,'name'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'created'); ?>
                <?php echo $form->textField($model,'created',array('disabled'=>true)); ?>
                <?php echo $form->error($model,'created'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'deleted'); ?>
                <?php echo $form->textField($model,'deleted',array('disabled'=>true)); ?>
                <?php echo $form->error($model,'deleted'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'isdeleted'); ?>
                <?php echo $form->textField($model,'isdeleted'); ?>
                <?php echo $form->error($model,'isdeleted'); ?>
            </div>

            <div class="row buttons">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
            </div>

        <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>
</div>
