<?php
/* @var $this PeriodController */
/* @var $model Period */
/* @var $form CActiveForm */
?>
<div class="row-fluid">
    <div class="col-lg-12">
        <div class="wide form">

        <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
        )); ?>

            <div class="row">
                <?php echo $form->label($model,'id'); ?>
                <?php echo $form->textField($model,'id'); ?>
            </div>

            <div class="row">
                <?php echo $form->label($model,'idEnterprise'); ?>
                <?php echo $form->textField($model,'idEnterprise'); ?>
            </div>

            <div class="row">
                <?php echo $form->label($model,'name'); ?>
                <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>128)); ?>
            </div>

            <div class="row">
                <?php echo $form->label($model,'created'); ?>
                <?php echo $form->textField($model,'created'); ?>
            </div>

            <div class="row">
                <?php echo $form->label($model,'deleted'); ?>
                <?php echo $form->textField($model,'deleted'); ?>
            </div>

            <div class="row">
                <?php echo $form->label($model,'isdeleted'); ?>
                <?php echo $form->textField($model,'isdeleted'); ?>
            </div>

            <div class="row buttons">
                <?php echo CHtml::submitButton('Search'); ?>
            </div>

        <?php $this->endWidget(); ?>

        </div><!-- search-form -->
    </div>
</div>
