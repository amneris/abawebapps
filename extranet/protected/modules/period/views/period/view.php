<?php
/* @var $this PeriodController */
/* @var $model Period */

$selectedIndex = 'Configuracion';

?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>
<div class="row-fluid">
    <div class="col-lg-12 marginTop15">
         <span class="sectionTitle">View Period <?php echo $model->name; ?></span>

        <?php $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                'id',
                array(
                    'name'=>$model->getAttributeLabel('idEnterprise'),
                    'value'=>$model->enterprise->name,
                ),
                'name',
                'created',
                'deleted',
                'isdeleted',
            ),
        )); ?>
    </div>
</div>

