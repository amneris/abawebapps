<?php
/* @var $this GroupController */
/* @var $model Group */

$selectedIndex = 'Configuracion';
?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>

<div class="row-fluid">
    <div class="col-lg-12 marginTop15">
        <span class="sectionTitle"><?php echo 'Configuración' ?></span>
        <br><br>
        <?php
        $this->widget('zii.widgets.jui.CJuiTabs', array(
            'tabs'=>array(
                'Periods'=>$this->renderPartial('_periods', array('model'=>$model),true),
            ),
            'themeUrl'=>'/css',
            'theme'=>'',
            'cssFile' => 'tabStyle.css',
            'options'=>array(
                'collapsible'=>true,
                'selected'=>0,
            ),

            'htmlOptions'=>array(
                'style'=>'width:100%;'
            ),
        ));
        ?>
    </div>
</div>
