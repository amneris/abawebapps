<?php
/* @var $this PeriodController */
/* @var $model Period */

$selectedIndex = 'Configuracion';

?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>
<div class="row-fluid">
    <div class="col-lg-12 marginTop15">
         <span class="sectionTitle">Create Period</span>
        <?php $this->renderPartial('_form', array('model'=>$model, 'dropDownEnterprise'=>$enterprise)); ?>
    </div>
</div>
