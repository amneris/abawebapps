<?php
return array(
	'ADD NEW PERIOD' => 'AGGIUNGERE NUOVA SESSIONE', 
	'Period successfully added.' => 'Sessione aggiunta correttamente.', 
	'Period/s' => 'Sessione/i', 
	'Error adding period.' => 'Errore nell\'aggiunta della sessione.', 
	'The selected Period has been removed.' => 'La sessione selezionata è stata eliminata.', 
	'Error, something went wrong while trying to save the period.' => 'Si è verificato un errore nel salvataggio del periodo di tempo.', 
);
