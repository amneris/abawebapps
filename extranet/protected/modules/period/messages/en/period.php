<?php
return array(
	'ADD NEW PERIOD' => 'ADD NEW PERIOD', 
	'Period successfully added.' => 'Period successfully added.', 
	'Period/s' => 'Period(s)', 
	'Error adding period.' => 'Error while adding the period.', 
	'The selected Period has been removed.' => 'The selected period has been deleted.', 
	'Error, something went wrong while trying to save the period.' => 'An error occurred while trying to save the period.', 
);
