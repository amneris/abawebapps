<?php
return array(
	'ADD NEW PERIOD' => 'AÑADIR NUEVA CONVOCATORIA', 
	'Period successfully added.' => 'Convocatoria añadida correctamente.', 
	'Period/s' => 'Convocatoria/s', 
	'Error adding period.' => 'Error añadiendo la convocatoria.', 
	'The selected Period has been removed.' => 'La convocatoria seleccionada se ha eliminado', 
	'Error, something went wrong while trying to save the period.' => 'Error al tratar de guardar la convocatoria.', 
);
