<?php
return array(
	'ADD NEW PERIOD' => 'ADICIONAR NOVO PERÍODO DE EXAME', 
	'Period successfully added.' => 'Período de exames adicionado com sucesso', 
	'Period/s' => 'Período/s de exames', 
	'Error adding period.' => 'Erro ao adicionar período de exames', 
	'The selected Period has been removed.' => 'O período de exames selecionado foi deletado', 
	'Error, something went wrong while trying to save the period.' => 'Erro ao guardar o período de exames', 
);
