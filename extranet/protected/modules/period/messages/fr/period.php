<?php
return array(
	'ADD NEW PERIOD' => 'AJOUTER NOUVELLE SESSION', 
	'Period successfully added.' => 'Session correctement ajoutée.', 
	'Period/s' => 'Session/s', 
	'Error adding period.' => 'Erreur dans l\'ajout de la session.', 
	'The selected Period has been removed.' => 'La session sélectionnée a été supprimée', 
	'Error, something went wrong while trying to save the period.' => 'Erreur dans la sauvegarde de la session.', 
);
