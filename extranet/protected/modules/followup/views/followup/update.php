<?php
/* @var $this B2cFollowupController */
/* @var $model B2cFollowup */

$this->breadcrumbs=array(
	'B2c Followups'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List B2cFollowup', 'url'=>array('index')),
	array('label'=>'Create B2cFollowup', 'url'=>array('create')),
	array('label'=>'View B2cFollowup', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage B2cFollowup', 'url'=>array('admin')),
);
?>

<h1>Update B2cFollowup <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>