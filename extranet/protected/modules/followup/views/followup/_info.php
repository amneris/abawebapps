<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'studentName-form',
    'enableAjaxValidation'=>false,
)); ?>
<div class="withBorderBottom">
    <table class="userTable">
        <tr onmouseover="js:showEditImg('nameIMG')" onmouseout="js:hideEditImg('nameIMG')">
            <td width="40px" class="paddingTop10">
            </td>
            <td width="250px" class="paddingTop10">
                <span class="formLabel"><?php echo FollowupModule::t('Name'); ?></span> <br>
            </td>
            <td class="paddingTop10">

            </td>
        </tr>
        <tr onmouseover="js:showEditImg('nameIMG')" onmouseout="js:hideEditImg('nameIMG')">
            <td class="paddingBottom10">
                <a href="#" style="cursor: hand"> <img onclick="js:transform('name','<?php echo $studentModel->name; ?>')" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px" id="nameIMG" style="display: none" class="yeap"></a>
            </td>
            <td class="paddingBottom10">
                <span class="userTableValue" onclick="js:transform('name','<?php echo $studentModel->name; ?>')" id="nameINPUT"><?php echo $studentModel->name; ?></span>
            </td>
            <td class="paddingBottom10">
                <button id='nameBOT' class='abaButton abaButton-Primary' style="margin-left:40px; display: none"><?php echo Yii::t('app','Save'); ?></button>
            </td>
        </tr>
    </table>
</div>
<?php $this->endWidget(); ?>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'studentSurname-form',
    'enableAjaxValidation'=>false,
)); ?>
<div class="withBorderBottom">
<table class="userTable">
    <tr onmouseover="js:showEditImg('surnameIMG')" onmouseout="js:hideEditImg('surnameIMG')">
        <td width="40px" class="paddingTop10">

        </td>
        <td width="250px" class="paddingTop10">
            <span class="formLabel"><?php echo FollowupModule::t('Surnames'); ?></span><br>
        </td>
        <td class="paddingTop10">

        </td>
    </tr>
    <tr onmouseover="js:showEditImg('surnameIMG')" onmouseout="js:hideEditImg('surnameIMG')">
        <td class="paddingBottom10">
            <a href="#" style="cursor: hand"><img onclick="js:transform('surname','<?php echo $studentModel->surname; ?>')" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px" id="surnameIMG" style="display: none" class="yeap"></a>
        </td>
        <td class="paddingBottom10">
            <span class="userTableValue" onclick="js:transform('surname','<?php echo $studentModel->surname; ?>')" id="surnameINPUT"><?php echo $studentModel->surname; ?></span>
        </td>
        <td class="paddingBottom10">
            <button id='surnameBOT' class='abaButton abaButton-Primary' style="margin-left:40px; display: none"><?php echo Yii::t('app','Save'); ?></button>
        </td>
    </tr>
</table>
</div>
<?php $this->endWidget(); ?>

<?php if ( empty($studentModel->idStudentCampus) || Yii::app()->user->isagent || Yii::app()->user->isabamanager ) { ?>
<div class="withBorderBottom">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'studentEmail-form',
        'enableAjaxValidation'=>false,
    )); ?>
    <table class="userTable">
        <tr onmouseover="js:showEditImg('emailIMG')" onmouseout="js:hideEditImg('emailIMG')">
            <td width="40px" class="paddingTop10">

            </td>
            <td width="250px" class="paddingTop10">
                <span class="formLabel"><?php echo FollowupModule::t('Email'); ?></span><br>
            </td>
            <td class="paddingTop10">

            </td>
        </tr>
        <tr onmouseover="js:showEditImg('emailIMG')" onmouseout="js:hideEditImg('emailIMG')">
            <td class="paddingBottom10">
                <a href="#" style="cursor: hand"><img onclick="js:transform('email','<?php echo $studentModel->email; ?>')" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px" id="emailIMG" style="display: none" class="yeap"></a>
            </td>
            <td class="paddingBottom10">
                <span class="userTableValue" onclick="js:transform('email','<?php echo $studentModel->email; ?>')" id="emailINPUT"><?php echo $studentModel->email; ?></span>
            </td>
            <td class="paddingBottom10">
                <button id='emailBOT' class='abaButton abaButton-Primary' style="margin-left:40px; display: none"><?php echo Yii::t('app','Save'); ?></button>
            </td>
        </tr>
    </table>
</div>
<?php $this->endWidget(); ?>
<?php } else { ?>
    <div class="withBorderBottom">
        <table class="userTable">
            <tr>
                <td width="40px" class="paddingTop10">

                </td>
                <td width="250px" class="paddingTop10">
                    <span class="formLabel"><?php echo FollowupModule::t('Email'); ?></span><br>
                </td>
                <td class="paddingTop10">

                </td>
            </tr>
            <tr>
                <td class="paddingBottom10">

                </td>
                <td class="paddingBottom10">
                    <span class="userTableValue" id="emailINPUT"><?php echo $studentModel->email; ?></span>
                </td>
                <td class="paddingBottom10">

                </td>
            </tr>
        </table>
    </div>
<?php } ?>

<div class="withBorderBottom">
  <?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'studentLevel-form',
    'enableAjaxValidation'=>false,
  )); ?>
  <table class="userTable">
    <tr onmouseover="js:showEditImg('level2IMG')" onmouseout="js:hideEditImg('level2IMG')">
	    <?php $form=$this->beginWidget('CActiveForm', array(
		    'id'=>'level-form',
		    'enableAjaxValidation'=>false,
	    )); ?>
          <td width="40px" class="paddingTop10">

          </td>
          <td width="250px" class="paddingTop10">
              <span class="formLabel"><?php echo FollowupModule::t('Level'); ?></span><br>
          </td>
	      <td class="paddingTop10">

          </td>
      </tr>
    <tr onmouseover="js:showEditImg('level2IMG')" onmouseout="js:hideEditImg('level2IMG')">
          <td class="paddingBottom10">
            <a href="#" style="cursor: hand"><img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px" id="level2IMG" style="display: none" class="yeap"></a>
          </td>
<!--          <td class="paddingBottom10">-->
<!--	          <span class="userTableValue" id="LevelINPUT"><?php //echo (empty($user->Level)) ? '-' : $user->Level; ?></span>-->
<!--          </td>-->
          <td class="paddingBottom10">
            <?php
               echo $form->dropDownList($studentModel,'idLevel', $levels,array('onchange'=>'js:showAcceptBut(\'level\')', 'prompt'=>'-'));
            ?>
          </td>
          <td class="paddingBottom10">
            <button id='levelBOT' class='abaButton abaButton-Primary' style="margin-left:40px; display: none"><?php echo Yii::t('app','Save'); ?></button>
          </td>
	    <?php $this->endWidget(); ?>
      </tr>
  </table>
  <?php $this->endWidget(); ?>
</div>

<div class="withBorderBottom">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'student-form',
    'enableAjaxValidation'=>false,
)); ?>
<table class="userTable">
    <tr onmouseover="js:showEditImg('groupIMG')" onmouseout="js:hideEditImg('groupIMG')">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'group-form',
            'enableAjaxValidation'=>false,
        )); ?>
        <td width="40px" class="paddingTop10">

        </td>
        <td width="250px" class="paddingTop10">
            <span class="formLabel"><?php echo FollowupModule::t('Group'); ?></span><br>
        </td>
        <td class="paddingTop10">

        </td>
    </tr>
    <tr onmouseover="js:showEditImg('groupIMG')" onmouseout="js:hideEditImg('groupIMG')">
        <td class="paddingBottom10">
            <a href="#" style="cursor: hand"><img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px" id="groupIMG" style="display: none" class="yeap"></a>
        </td>
        <td class="paddingBottom10">
            <?php
                echo $form->dropDownList($studentGroupModel,'idGroup', $groups,array('onchange'=>'js:showAcceptBut(\'group\')'));
            ?>
        </td>
        <td class="paddingBottom10">
            <button id='groupBOT' class='abaButton abaButton-Primary' style="margin-left:40px; display: none"><?php echo Yii::t('app','Save'); ?></button>
        </td>
        <?php $this->endWidget(); ?>
    </tr>
</table>
</div>
<?php $this->endWidget(); ?>

<div class="withBorderBottom">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'student-form',
    'enableAjaxValidation'=>false,
)); ?>
<table class="userTable">
    <tr onmouseover="js:showEditImg('periodIMG')" onmouseout="js:hideEditImg('periodIMG')">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'period-form',
            'enableAjaxValidation'=>false,
        )); ?>
        <td width="40px" class="paddingTop10">

        </td>
        <td width="250px" class="paddingTop10">
            <span class="formLabel"><?php echo FollowupModule::t('Period'); ?></span><br>
        </td>
        <td class="paddingTop10">

        </td>
    </tr>
    <tr onmouseover="js:showEditImg('periodIMG')" onmouseout="js:hideEditImg('periodIMG')">
        <td class="paddingBottom10">
            <a href="#" style="cursor: hand"><img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px" id="periodIMG" style="display: none" class="yeap"></a>
        </td>
        <td class="paddingBottom10">
            <?php
            echo $form->dropDownList($studentPeriodModel,'idPeriod', $periods,array('onchange'=>'js:showAcceptBut(\'period\')'));
            ?>
        </td>
        <td class="paddingBottom10">
            <button id='periodBOT' class='abaButton abaButton-Primary' style="margin-left:40px; display: none"><?php echo Yii::t('app','Save'); ?></button>
        </td>
        <?php $this->endWidget(); ?>
    </tr>
</table>
</div>
<?php $this->endWidget(); ?>

<?php if ( count($arTeacher) > 1 ) { ?>
<div class="withBorderBottom">
    <?php $form=$this->beginWidget('CActiveForm', array(
      'id'=>'student-form',
      'enableAjaxValidation'=>false,
    )); ?>
    <table class="userTable">
        <tr onmouseover="js:showEditImg('teacherIMG')" onmouseout="js:hideEditImg('teacherIMG')">
            <?php $form=$this->beginWidget('CActiveForm', array(
              'id'=>'teacher-form',
              'enableAjaxValidation'=>false,
            )); ?>
            <td width="40px" class="paddingTop10">

            </td>
            <td width="250px" class="paddingTop10">
                <span class="formLabel"><?php echo FollowupModule::t('Teacher'); ?></span><br>
            </td>
            <td class="paddingTop10">

            </td>
        </tr>
        <tr onmouseover="js:showEditImg('teacherIMG')" onmouseout="js:hideEditImg('teacherIMG')">
            <td class="paddingBottom10">
                <a href="#" style="cursor: hand"><img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px" id="teacherIMG" style="display: none" class="yeap"></a>
            </td>
            <td class="paddingBottom10">
                <?php
                echo $form->dropDownList($studentModel,'idTeacher', $arTeacher,array('onchange'=>'js:showAcceptBut(\'teacher\')'));
                ?>
            </td>
            <td class="paddingBottom10">
                <button id='teacherBOT' class='abaButton abaButton-Primary' style="margin-left:40px; display: none"><?php echo Yii::t('app','Save'); ?></button>
            </td>
            <?php $this->endWidget(); ?>
        </tr>
    </table>
</div>
<?php $this->endWidget(); ?>
<?php } ?>

<div class="withBorderBottom">
    <?php $form=$this->beginWidget('CActiveForm', array(
      'id'=>'student-form',
      'enableAjaxValidation'=>false,
    )); ?>
    <table class="userTable">
        <tr onmouseover="js:showEditImg('statusIMG')" onmouseout="js:hideEditImg('statusIMG')">
            <?php $form=$this->beginWidget('CActiveForm', array(
              'id'=>'status-form',
              'enableAjaxValidation'=>false,
            )); ?>
            <td width="40px" class="paddingTop10">

            </td>
            <td width="250px" class="paddingTop10">
                <span class="formLabel"><?php echo strtoupper(Yii::app()->getModule('student')->t('lbl: Status')); ?></span><br>
            </td>
            <td class="paddingTop10">

            </td>
        </tr>
        <tr onmouseover="js:showEditImg('statusIMG')" onmouseout="js:hideEditImg('statusIMG')">
            <td class="paddingBottom10">
                <a href="#" style="cursor: hand"><img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px" id="statusIMG" style="display: none" class="yeap"></a>
            </td>
            <td class="paddingBottom10">
                <?php
                echo $form->dropDownList($studentGroupModel,'status', $arStatus,array('onchange'=>'js:showAcceptBut(\'status\')'));
                ?>
            </td>
            <td class="paddingBottom10">
                <button id='statusBOT' class='abaButton abaButton-Primary' style="margin-left:40px; display: none"><?php echo Yii::t('app','Save'); ?></button>
            </td>
            <?php $this->endWidget(); ?>
        </tr>
    </table>
</div>
<?php $this->endWidget(); ?>

<table class="userTable">
    <tr>
        <td width="40px" class="paddingTop10">

        </td>

        <td width="250px" class="paddingTop10">
            <span class="formLabel"><?php echo FollowupModule::t('Expiration date'); ?></span><br>
        </td>
        <td rowspan="2" class="paddingTop10">

        </td>

    </tr>
    <tr>
        <td class="paddingBottom10">

        </td>
        <td class="paddingBottom10">
            <?php $expirationDate = $studentGroupModel->expirationDate; ?>
            <?php echo ( strtotime($expirationDate) > time() ) ? date("Y-m-d", strtotime($expirationDate) ) : StudentModule::t('No license'); ?>
        </td>
    </tr>
</table>

<br>
<br>
<div style="text-align: center; margin-bottom: 2em; position: relative;">
  <div id="btnBack" class="abaButton abaButton-Primary"><?php echo Yii::t('app','Back'); ?></div>
  <?php if ( $user->idStudentCampus > 0 && strtotime($expirationDate) > time() ) { ?>
  <div id="btnResendWelcomeEmail" class="abaButton abaButton-Primary" style="position:absolute; right: 0; top:-8px;"><?php echo Yii::t('app','lbl: Resend welcome email'); ?></div>
  <?php } ?>
</div>


<script>
    $(document).ready( function() {

        $('#btnBack').click(function () {
            var pageURL = '/student/main';
            var parameters = {};

            ABAChangePage(pageURL, parameters);

        });

        $('#btnResendWelcomeEmail').click(function () {
          var pageURL = '/student/main/resendwelcomeemail';
          var parameters = {
            id: <?php echo $user->id ?>
          };

          var successF = function(data) {
            if ( data.ok ) {
              abaCore.showDialog( data.html, true, '560px', '270px' );
            } else {

            }
          };

          abaCore.launchAjax( pageURL, parameters, successF );
        });
    });
    function showEditImg(editField)
    {
        $( "#"+editField ).show()
    }
    function hideEditImg(editField)
    {
        if($( "#"+editField).hasClass("yeap"))
        {
            $( "#"+editField ).hide();
        }
    }
    function transform(id,value)
    {
        $('#' + id + 'IMG').show();
        $('#' + id + 'IMG').removeClass("yeap");
        $('#' + id +'INPUT').replaceWith("<input class='form-control addCampStudent' size='35' type='text' id='"+id+"' name='"+id+"' value='"+value+"'> ");
        $('#' + id + 'BOT').show();

    }
    function showAcceptBut(id)
    {
        $('#' + id + 'IMG').show();
        $('#' + id + 'IMG').removeClass("yeap");
        $('#' + id + 'BOT').show();
    }
    function disableAcceptButAndImg(id)
    {
        $('#' + id + 'IMG').hide();
        $('#' + id + 'BOT').hide();
    }
</script>