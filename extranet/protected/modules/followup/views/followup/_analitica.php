<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 11/11/14
 * Time: 13:15
 */
$iterator = $model->getProgressForUser($idCampus);

$iterator2= $iterator;
$iterator3= $iterator;
$totalNotes=0;
$note=0;

//we check is there is some progress to show.. otherwise.. we just show a message.
if(!empty($iterator))
{
    foreach($iterator3 as $record3)
    {
        if($record3['AssessmentPct']>0)
        {
            $note += $record3['AssessmentPct'];
            $totalNotes++;
        }
    }

    echo "<div class='studentHeader'><table width='100%'><tr><td>&nbsp;</td><td width='180px'><div class='roundedCal'><span class='cal'>";
    if($totalNotes>0)
    {
        $note = $note / $totalNotes;
        if($note==10)
        {
            echo $note;
        }
        else
        {
            echo number_format($note,2,',','');
        }
    }
    echo "</span><span class='calNote'>";
    if($note==0)
    {
        echo FollowupModule::t('No exam taken');
    }
    else
    {
        echo " ";
        echo FollowupModule::t('Average score');
    }
    echo "</span></div></td></tr></table></div>";

    $currentLevel='';
    $unitXlevel=24;

    echo "<div class='myTable' id='progressTable'>";
    echo "<br>";
    echo "<table width='100%' style='border-collapse: collapse;'>";

    foreach($iterator as $record)
    {
        $total=0;
        if($currentLevel <> $record['level'])
        {
            //we calculate the total porcent.
            foreach($iterator2 as $record2)
            {
                if($record2['level']==$record['level'])
                {
                    $total += $record2['TotalUnitPct'];
                }
            }
            echo "<tr>";
            echo " <td rowspan='2' width='80px' style='border-right: 0px; border-bottom: 0px;'>";
            echo "<img src='".Yii::app()->baseUrl."/images/levels/".$record['level'].".png' width='60' height='60'>";
            echo "</td>";
            echo "<td style='border-left: 0px; border-bottom: 0px; border-right: 0px;'>";
            echo "<span class='levelTitle'>".$record['level']."</span>";
            echo "</td>";
            echo "<td style='border-left: 0px; border-bottom: 0px;'>";
            echo "<div class='roundedPorcent'>".number_format($total / $unitXlevel,0)."%</div>";
            echo "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td width='80%' style='border-top: 0px; border-left: 0px; border-right: 0px;'>";
            echo "<div class='progress customProgressBar'>";
            echo "<div class='progress-bar customProgressBarProgress' role='progressbar' aria-valuenow='".intval($total / $unitXlevel)."' aria-valuemin='0' aria-valuemax='100' style='width: ".intval($total / $unitXlevel)."%;'/>";
            echo "</div>";
            echo "</td>";
            echo "<td style='text-align: right; border-top: 0px; border-left: 0px; white-space: nowrap'>";
            echo "<a href='#' onclick=\"showTableByClass('".str_replace(' ', '',$record['level'])."');\"><span class='expandLabel'>".FollowupModule::t('Show units')."</span><img id='a".str_replace(' ', '',$record['level'])."' class='botPlus' width='16' height='16'></a>";
            echo "</td>";
            echo "</tr>";
            $currentLevel = $record['level'];
        }

        echo "<tr class='".str_replace(' ', '',$currentLevel)."' style='display:none;'>";
        echo "<td colspan='3' style='padding: 0px;'>";
        echo "<table width='100%'  style='border-collapse: collapse; border-style: hidden;'>";
        echo "<tr>";
        echo "<td width='80px' style='border-right: 0px;'>";
        if($record['TotalUnitPct']==100)
        {
            echo "<div class='unitBall'>".$record['IdUnit']."</div>";
        }
        else
        {
            echo "<div class='unitBallIncomplete'>".$record['IdUnit']."</div>";
        }

        echo "</td>";
        echo "<td width='700px' style='border-left: 0px; border-right: 0px;'>";
        echo HeUnits::getUnitNameByIdUnit($record['IdUnit']);
        echo "</td>";
        echo "<td width='20px' style='border-left: 0px; border-right: 0px;'>";
        if(number_format($record['TotalUnitPct'],0)>0)
        {
            echo "<b>".number_format($record['TotalUnitPct'],0)."%</b>";
        }
        else
        {
            echo number_format($record['TotalUnitPct'],0)."%";
        }
        echo "</td>";
        echo "<td width='200px' style='border-left: 0px; border-right: 0px;'>";
        echo "<div class='progress customProgressBar'>";
        echo "<div class='progress-bar customProgressBarProgress' role='progressbar' aria-valuenow='".intval($record['TotalUnitPct'])."' aria-valuemin='0' aria-valuemax='100' style='width: ".intval($record['TotalUnitPct'])."%;'/>";
        echo "</div>";
        echo "</td>";
        echo "<td style='text-align: right; border-left: 0px;'>";
        echo "<a href='#' onclick=\"showTable('".$record['IdUnit']."');\"><img id='a".$record['IdUnit']."' class='botPlus' width='16' height='16'></a>";
        echo "</td>";
        echo "</tr>";
        echo "<tr id='".$record['IdUnit']."' style='display: none;'>";
        echo "<td colspan='5' style='padding: 0px'>";
        echo "<table width='100%' style='border-collapse: collapse; border-style: hidden;'>";
        echo "<tr>";

        echo "<td width='80px' style='background-color:#eaeaea; border-right: 0px;'>";
        if($record['AbaFilmPct']==100)
        {?>
            <div class="unitBallExer" style="background: #1e90c7 url(<?php echo Yii::app()->baseUrl ?>'/images/followup/film.png') no-repeat center;"></div>
        <?php
        }
        else
        {?>
            <div class="unitBallExerIncomplete" style="background: #bdbdbd url(<?php echo Yii::app()->baseUrl ?>'/images/followup/film.png') no-repeat center;"></div>
        <?php
        }
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px; border-right: 0px;'>";
        echo $model->getAttributeLabel('sit_por');
        echo "</td>";
        echo "<td width='50px' style='background-color:#eaeaea; border-left: 0px; border-right: 0px; text-align: right'>";
        if($record['AbaFilmPct']>0)
        {
            echo "<b>".$record['AbaFilmPct']."%</b>";
        }
        else
        {
            echo $record['AbaFilmPct']."%";
        }
        echo "</td>";
        echo "<td width='370px' style='background-color:#eaeaea; border-left: 0px; border-right: 0px;'>";
        echo "<div class='progress customProgressBar'>";
        echo "<div class='progress-bar customProgressBarProgress' role='progressbar' aria-valuenow='".intval($record['AbaFilmPct'])."' aria-valuemin='0' aria-valuemax='100' style='width: ".intval($record['AbaFilmPct'])."%;'/>";
        echo "</div>";
        echo "</td>";
        echo "<td width='150px' style='background-color:#eaeaea; border-left: 0px;'>";
        echo "</td>";
        echo "</tr>";

        echo "<tr>";
        echo "<td style='background-color:#eaeaea; border-right: 0px;'>";
        if($record['SpeakPct']==100)
        {?>
            <div class="unitBallExer" style="background: #1e90c7 url(<?php echo Yii::app()->baseUrl ?>'/images/followup/speak.png') no-repeat center;"></div>
        <?php
        }
        else
        {?>
            <div class="unitBallExerIncomplete" style="background: #bdbdbd url(<?php echo Yii::app()->baseUrl ?>'/images/followup/speak.png') no-repeat center;"></div>
        <?php
        }
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px; border-right: 0px;'>";
        echo $model->getAttributeLabel('stu_por');
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px; border-right: 0px; text-align: right'>";
        if($record['SpeakPct']>0)
        {
            echo "<b>".$record['SpeakPct']."%</b>";
        }
        else
        {
            echo $record['SpeakPct']."%";
        }
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px; border-right: 0px;'>";
        echo "<div class='progress customProgressBar'>";
        echo "<div class='progress-bar customProgressBarProgress' role='progressbar' aria-valuenow='".intval($record['SpeakPct'])."' aria-valuemin='0' aria-valuemax='100' style='width: ".intval($record['SpeakPct'])."%;'/>";
        echo "</div>";
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px;'>";
        echo "</td>";
        echo "</tr>";

        echo "<tr>";
        echo "<td style='background-color:#eaeaea; border-right: 0px;'>";
        if($record['ExercisesPct']==100)
        {?>
            <div class="unitBallExer" style="background: #1e90c7 url(<?php echo Yii::app()->baseUrl ?>'/images/followup/write.png') no-repeat center;"></div>
        <?php
        }
        else
        {?>
            <div class="unitBallExerIncomplete" style="background: #bdbdbd url(<?php echo Yii::app()->baseUrl ?>'/images/followup/write.png') no-repeat center;"></div>
        <?php
        }
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px; border-right: 0px;'>";
        echo $model->getAttributeLabel('dic_por');
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px; border-right: 0px; text-align: right'>";
        if($record['ExercisesPct']>0)
        {
            echo "<b>".$record['ExercisesPct']."%</b>";
        }
        else
        {
            echo $record['ExercisesPct']."%";
        }
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px; border-right: 0px;'>";
        echo "<div class='progress customProgressBar'>";
        echo "<div class='progress-bar customProgressBarProgress' role='progressbar' aria-valuenow='".intval($record['ExercisesPct'])."' aria-valuemin='0' aria-valuemax='100' style='width: ".intval($record['ExercisesPct'])."%;'/>";
        echo "</div>";
        echo "</td>";
        echo "<td style='background-color:#eaeaea; text-align: right; ; border-left: 0px;'>";
        echo "<span class='errorAndHelp'>";
        echo $model->getAttributeLabel('dic_ans').": ".$record['WriteAns']." ";
        echo $model->getAttributeLabel('dic_err').": ".$record['WriteErr'];
        echo "</div>";
        echo "</td>";
        echo "</tr>";

        echo "<tr>";
        echo "<td style='background-color:#eaeaea; border-right: 0px;'>";
        if($record['InterpretPct']==100)
        {?>
            <div class="unitBallExer" style="background: #1e90c7 url(<?php echo Yii::app()->baseUrl ?>'/images/followup/interpret.png') no-repeat center;"></div>
        <?php
        }
        else
        {?>
            <div class="unitBallExerIncomplete" style="background: #bdbdbd url(<?php echo Yii::app()->baseUrl ?>'/images/followup/interpret.png') no-repeat center;"></div>
        <?php
        }
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px; border-right: 0px;'>";
        echo $model->getAttributeLabel('rol_por');
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px; border-right: 0px; text-align: right'>";
        if($record['InterpretPct']>0)
        {
            echo "<b>".$record['InterpretPct']."%</b>";
        }
        else
        {
            echo $record['InterpretPct']."%";
        }
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px; border-right: 0px;'>";
        echo "<div class='progress customProgressBar'>";
        echo "<div class='progress-bar customProgressBarProgress' role='progressbar' aria-valuenow='".intval($record['InterpretPct'])."' aria-valuemin='0' aria-valuemax='100' style='width: ".intval($record['InterpretPct'])."%;'/>";
        echo "</div>";
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px;'>";
        echo "</td>";
        echo "</tr>";

        echo "<tr>";
        echo "<td style='background-color:#eaeaea; border-right: 0px;'>";
        if($record['VideoClassPct']==100)
        {?>
            <div class="unitBallExer" style="background: #1e90c7 url(<?php echo Yii::app()->baseUrl ?>'/images/followup/grammar.png') no-repeat center;"></div>
        <?php
        }
        else
        {?>
            <div class="unitBallExerIncomplete" style="background: #bdbdbd url(<?php echo Yii::app()->baseUrl ?>'/images/followup/grammar.png') no-repeat center;"></div>
        <?php
        }
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px; border-right: 0px;'>";
        echo $model->getAttributeLabel('gra_vid');
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px; border-right: 0px; text-align: right'>";
        if($record['VideoClassPct']>0)
        {
            echo "<b>".$record['VideoClassPct']."%</b>";
        }
        else
        {
            echo $record['VideoClassPct']."%";
        }
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px; border-right: 0px;'>";
        echo "<div class='progress customProgressBar'>";
        echo "<div class='progress-bar customProgressBarProgress' role='progressbar' aria-valuenow='".intval($record['VideoClassPct'])."' aria-valuemin='0' aria-valuemax='100' style='width: ".intval($record['VideoClassPct'])."%;'/>";
        echo "</div>";
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px;'>";
        echo "</td>";
        echo "</tr>";

        echo "<tr>";
        echo "<td style='background-color:#eaeaea; border-right: 0px;'>";
        if($record['ExercisesPct']==100)
        {?>
            <div class="unitBallExer" style="background: #1e90c7 url(<?php echo Yii::app()->baseUrl ?>'/images/followup/exercise.png') no-repeat center;"></div>
        <?php
        }
        else
        {?>
            <div class="unitBallExerIncomplete" style="background: #bdbdbd url(<?php echo Yii::app()->baseUrl ?>'/images/followup/exercise.png') no-repeat center;"></div>
        <?php
        }
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px; border-right: 0px;'>";
        echo $model->getAttributeLabel('wri_por');
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px; border-right: 0px; text-align: right'>";
        if($record['ExercisesPct']>0)
        {
            echo "<b>".$record['ExercisesPct']."%</b>";
        }
        else
        {
            echo $record['ExercisesPct']."%";
        }
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px; border-right: 0px;'>";
        echo "<div class='progress customProgressBar'>";
        echo "<div class='progress-bar customProgressBarProgress' role='progressbar' aria-valuenow='".intval($record['ExercisesPct'])."' aria-valuemin='0' aria-valuemax='100' style='width: ".intval($record['ExercisesPct'])."%;'/>";
        echo "</div>";
        echo "</td>";
        echo "<td style='background-color:#eaeaea; text-align: right; border-left: 0px;'>";
        echo "<span class='errorAndHelp'>";
        echo $model->getAttributeLabel('wri_ans').": ".$record['ExercisesAns']." ";
        echo $model->getAttributeLabel('wri_err').": ".$record['ExercisesErr'];
        echo "</span>";
        echo "</td>";
        echo "</tr>";

        echo "<tr>";
        echo "<td style='background-color:#eaeaea; border-right: 0px;'>";
        if($record['VocabularyPct']==100)
        {?>
            <div class="unitBallExer" style="background: #1e90c7 url(<?php echo Yii::app()->baseUrl ?>'/images/followup/vocabulary.png') no-repeat center;"></div>
        <?php
        }
        else
        {?>
            <div class="unitBallExerIncomplete" style="background: #bdbdbd url(<?php echo Yii::app()->baseUrl ?>'/images/followup/vocabulary.png') no-repeat center;"></div>
        <?php
        }
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px; border-right: 0px;'>";
        echo $model->getAttributeLabel('new_por');
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px; border-right: 0px; text-align: right'>";
        if($record['VocabularyPct']>0)
        {
            echo "<b>".$record['VocabularyPct']."%</b>";
        }
        else
        {
            echo $record['VocabularyPct']."%";
        }
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px; border-right: 0px;'>";
        echo "<div class='progress customProgressBar'>";
        echo "<div class='progress-bar customProgressBarProgress' role='progressbar' aria-valuenow='".intval($record['VocabularyPct'])."' aria-valuemin='0' aria-valuemax='100' style='width: ".intval($record['VocabularyPct'])."%;'/>";
        echo "</div>";
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px;'>";
        echo "</td>";
        echo "</tr>";

        echo "<tr>";
        echo "<td style='background-color:#eaeaea; border-right: 0px;'>";
        if($record['AssessmentPct']>=8)
        {?>
            <div class="unitBallExer" style="background: #1e90c7 url(<?php echo Yii::app()->baseUrl ?>'/images/followup/assessment.png') no-repeat center;"></div>
        <?php
        }
        else
        {?>
            <div class="unitBallExerIncomplete" style="background: #bdbdbd url(<?php echo Yii::app()->baseUrl ?>'/images/followup/assessment.png') no-repeat center;"></div>
        <?php
        }
        echo "</td>";
        echo "<td colspan='3' style='background-color:#eaeaea; border-left: 0px; border-right: 0px;'>";
        echo $model->getAttributeLabel('eva_por');
        echo "</td>";
        echo "<td style='background-color:#eaeaea; border-left: 0px; border-right: 0px; text-align: right'>";
        if($record['AssessmentPct']>0)
        {
            echo "<b>".$record['AssessmentPct']."</b>";
        }
        else
        {
            echo $record['AssessmentPct'];
        }
        echo "</td>";
        echo "</tr>";

        echo "</table>";
        echo "</td>";
        echo "</tr>";
        echo "</table>";
        echo "</td>";
        echo "</tr>";

    }
    echo "</table>";
    echo "</div>";
}
else
{
    echo "<div class='row-fluid'>";
    echo "<div class='col-lg-12'>";
    echo "<div class='divSectionTittle' style='text-align: center; margin-bottom: 30px'>";
    echo "<span class='sectionTitle'>".FollowupModule::t('No progress to show yet.')."</span>";
    echo "</div></div></div>";
}
?>
<br>
<div style="text-align: center; margin-bottom: 2em">
    <button id="btnBackAnalitica" type="button" class="abaButton abaButton-Primary"><?php echo Yii::t('app','Back'); ?></button>
</div>
<script>

    $(document).ready( function() {

        $('#btnBackAnalitica').click(function () {
            var pageURL = '/student/main';
            var parameters = {};

            ABAChangePage(pageURL, parameters);

        });
    });

    function showTable(table)
    {
        $( "#"+table ).toggle();
        $( "#a"+table ).toggleClass('botMinus','botPlus');
    }
    function showTableByClass(table)
    {
        $( "."+table ).toggle();
        $( "#a"+table ).toggleClass('botMinus','botPlus');
    }
</script>