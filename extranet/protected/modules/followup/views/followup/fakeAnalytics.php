<?php
  $selectedIndex = 'Analitica';
?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>
<style>
  #divAnaliticaBody {
    text-align: center;
    width: 100%;
/*    height: 47.25em; */
    background-image: url('/images/tmp/backAnalitica.png');
    background-position: center center;
    background-repeat: no-repeat;
  }
  
  #divProxi {
    line-height: 47em;
    font-weight: bold;
  }

  #divProxi span {
    font-family: 'MuseoSlab700Regular';
    font-size: 3em;
  }
</style>

<div class="row-fluid">
  <div class="col-lg-12">
    
    <div class="row-fluid">
      <div class="col-lg-12">
          <div class="divSectionTittle">
              <span class="sectionTitle"> <?php echo FollowupModule::t( 'Analytics'); ?></span>
          </div>
      </div>
    </div>    
    
    <div class="row-fluid">
      <div class="col-lg-12">
        &nbsp;
      </div>
    </div>    

    <div class="row-fluid">
      <div class="col-lg-12">

        <div id="divAnaliticaBody">
        <!--  <img src="/images/tmp/backAnalitica.png"> -->
          <div id="divProxi"><span><?php echo FollowupModule::t('Próximamente') ?></span></div>
        </div>
        
      </div>
    </div>    

    <div class="row-fluid">
      <div class="col-lg-12">
        &nbsp;
      </div>
    </div>    
    
  </div>
</div>

<script>
  var selectedIndex = '<?php echo CJavaScript::quote($selectedIndex); ?>';
</script>
