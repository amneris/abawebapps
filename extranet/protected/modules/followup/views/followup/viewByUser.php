<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 30/10/14
 * Time: 16:15
 */
$selectedIndex = 'Alumno';
?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>


<div class="row-fluid">
<div class="col-lg-12 marginTop15">
    <br>
    <?php
    $this->widget('zii.widgets.jui.CJuiTabs', array(
        'headerTemplate' => '<li><a href="{url}" title="{title}">{title}</a></li>',
        'tabs'=>array(
            FollowupModule::t('User information')=>$this->renderPartial('_info', array('user'=>$user, 'levels' => $levels, 'groups'=>$groups, 'periods'=>$periods, 'studentGroupModel'=>$studentGroupModel,
                'studentPeriodModel'=>$studentPeriodModel, 'studentModel'=>$studentModel, 'arStatus' => $arStatus, 'arTeacher' => $arTeacher),true),

            FollowupModule::t('Analytics')=>$this->renderPartial('_analitica', array('user'=>$user, 'model'=>$model, 'email'=>$email,'idCampus'=>$idCampus, 'selectedPartner'=>$selectedPartner),true),
        ),
        'themeUrl'=>'/css',
        'theme'=>'',
        'cssFile' => 'tabStyle.css',
        'options'=>array(
            'collapsible'=>false,
            'selected'=>FollowupModule::t('User information'),
        ),

        'htmlOptions'=>array(
            'style'=>'width:100%;'
        ),
    ));
    ?>
</div>
</div>
<script>
  var selectedIndex = '<?php echo CJavaScript::quote($selectedIndex); ?>';
</script>


