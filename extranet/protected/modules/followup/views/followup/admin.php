<?php
/* @var $this FollowupController */
/* @var $model Followup */

$selectedIndex = 'Analitica';

?>

<script>
    var selectedIndex = '<?php echo CJavaScript::quote($selectedIndex); ?>';
</script>

<div class="row-fluid">
    <div class="col-lg-12">

        <div class="row-fluid">
            <div class="col-lg-12">
                <div class="divSectionTittle">
                    <span class="sectionTitle"><?php echo FollowupModule::t('Study report'); ?></span>
                    <br>
                    <br>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="col-lg-12">
                <div class="table-responsive divTableFiltros">
                    <?php echo CHtml::form(Yii::app()->createUrl('followup/followup/admin'), 'get'); ?>
                    <table class="table tableFiltros">
                        <tr>
                            <td class="tdNewreg">
                                <label><?php echo FollowupModule::t('Study report for level'); ?></label>
                                <br>
                                <?php echo CHtml::dropDownList('level', $level, $levelList,array('class'=>'form-control')); ?>
                            </td>
                            <td class="tdNewreg">
                                <label><?php echo strtolower(StudentModule::t( 'GROUP' )); ?></label>
                                <br>
                                <?php echo CHtml::dropDownList('group', $group, $groups,array('empty'=>'--', 'class'=>'form-control')); ?>
                            </td>
                            <td class="tdNewreg">
                                <label><?php echo strtolower(Yii::t('app','PERIOD')); ?></label>
                                <br>
                                <?php echo CHtml::dropDownList('period', $period, $periods,array('empty'=>'--', 'class'=>'form-control')); ?>
                            </td>
                            <td class="tdNewreg">
                                <label><?php echo FollowupModule::t('filter by email'); ?></label>
                                <br>
                                <?php echo CHtml::textField('criterio', $criterio, array('size'=>'40px','placeholder'=>'emailexample@abaenglish.com','class'=>'form-control')); ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdNewreg" style="padding-top: 5px;">
                                <label><?php echo FollowupModule::t('between');?></label>
                                <br>
                                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'name' => 'startDate',
                                    'value' => $startDate,
                                    'options' => array(
                                        'dateFormat' => 'yy-mm-dd',
                                        'changeMonth'=>true,
                                        'changeYear'=>true,
                                    ),
                                    'htmlOptions' => array(
                                        'size' => '10',
                                        'maxlength' => '10',
                                        'class'=>'form-control',
                                    ),
                                )); ?>
                            </td>
                            <td class="tdNewreg" style="padding-top: 5px;">
                                <label><?php echo FollowupModule::t('and'); ?></label>
                                <br>
                                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                    'name' => 'endDate',
                                    'value' => $endDate,
                                    'options' => array(
                                        'dateFormat' => 'yy-mm-dd',
                                        'changeMonth'=>true,
                                        'changeYear'=>true,
                                    ),
                                    'htmlOptions' => array(
                                        'size' => '10',
                                        'maxlength' => '10',
                                        'class'=>'form-control',
                                    ),
                                )); ?>
                            </td>
                            <?php if ( Yii::app()->user->isagent || Yii::app()->user->isabamanager ) { ?>
                                <td class="tdNewreg" style="padding-top: 0.6em;">
                                    <label><?php echo Yii::app()->getModule('student')->t('lbl: Other options'); ?></label>
                                    <br>
                                    <div><?php echo CHtml::checkBox('status', $status, array('id' => 'cb_status', 'class'=>'form-control2')); ?><label style="margin-left: 0.3em;"><?php echo Yii::app()->getModule('student')->t('lbl: Mostrar inactivos'); ?></label></div>
                                    <div><label style="display: block;padding-left: 15px;text-indent: -15px;"><?php echo CHtml::checkBox('withoutprogress', $withoutprogress, array('id' => 'cb_withoutprogress', 'class'=>'form-control2', 'style' => 'width: 13px;height: 13px;padding: 0px;margin: 0;vertical-align: bottom;position: relative;top: -1px;margin-right: 0.3em;')); ?><?php echo Yii::app()->getModule('student')->t('lbl: Sin progreso'); ?></label></div>
                                </td>
                            <?php } else { ?>
                            <td class="tdNewreg" style="padding-top: 0.6em;">
                                <label><?php echo Yii::app()->getModule('student')->t('lbl: Mostrar inactivos'); ?></label>
                                <br>
                                <?php echo CHtml::checkBox('status', $status, array('id' => 'cb_status', 'class'=>'form-control2')); ?>
                                <br>
                                <?php echo CHtml::checkBox('withoutprogress', $withoutprogress, array('id' => 'cb_withoutprogress', 'class'=>'form-control2', 'style' => 'display:none;' )); ?>
                            </td>
                            <?php } ?>


                            <td class="tdNewreg" style="padding-top: 5px;">
                                <?php ?>
                                <br>
                                <button type="submit" class="abaButton abaButton-Primary" style="margin-top: 4px;"><?php echo Yii::t('app','Search'); ?></button>
                            </td>
                        </tr>
                        <?php echo CHtml::endForm(); ?>
                        <tr>
                            <!--<td colspan="5" style="padding-left: 1.4em;">
                                <?php /*echo FollowupModule::t('Note: We recommend that you only enter an end date and leave the start date empty to get a report for all the users as if not, some users could be missed if they registered by themselves beforehand.'); */?>
                            </td>-->
                        </tr>
                        <tr>
                            <td colspan="5" style="padding-left: 1.4em;">
                                <?php echo CHtml::link(FollowupModule::t('Export units to CSV'),array('followup/export/?level='.$level.'&criterio='.$criterio.'&startDate='.$startDate.'&endDate='.$endDate.'&selectedPartner='.$selectedPartner.'&group='.$group.'&period='.$period.'&status='.$status.'&withoutprogress='.$withoutprogress)); ?>
                            </td>
                        </tr>
                        <?php if (Utils::getSessionVar('idLanguageEnterprise') == Language::_CHINESE ) { ?>
                        <tr>
                            <td colspan="5" style="padding-left: 1.4em;">
                                <?php echo CHtml::link(FollowupModule::t('Export units to CSV (Chinese)'),array('followup/export/?level='.$level.'&criterio='.$criterio.'&startDate='.$startDate.'&endDate='.$endDate.'&selectedPartner='.$selectedPartner.'&group='.$group.'&period='.$period.'&status='.$status.'&withoutprogress='.$withoutprogress.'&charset=GB2312')); ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
                <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                    'id'=>'b2c-followup-grid',
                    'selectableRows' => 1,
                    'selectableRows' => 1,
                    'cssFile' => Yii::app()->request->baseUrl . '/css/gridView.css',
                    'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
                        'prevPageLabel'  => '«',
                        'nextPageLabel'  => '»',
                        'header'=>''),
                    'dataProvider'=>$model->findByLevel($level, $pages, $criterio, $selectedPartner, $startDate, $endDate, $group, $period, $status),
                    'columns'=>array(
                        /*'idFollowup',*/
                        'name'=>array(
                            'header'=>$model->getAttributeLabel('name'),
                            'name'=>'name',
                        ),
                        'surnames'=>array(
                            'header'=>$model->getAttributeLabel('surnames'),
                            'name'=>'surnames',
                        ),
                        'email'=>array(
                            'header'=>$model->getAttributeLabel('email'),
                            'name'=>'email',
                        ),
                        //'telephone',
                        /*'userType'=>array(
                            'header'=>$model->getAttributeLabel('userType'),
                            'name'=>'userType',
                        ),*/

                        /*'currentLevel',*/

                        /*'entryDate'=>array(
                            'header'=>$model->getAttributeLabel('entryDate'),
                            'name'=>'entryDate',
                        ),*/
                        'expirationDate'=>array(
                            'header'=>$model->getAttributeLabel('expirationDate'),
                            'name'=>'expirationDate',
                        ),
                        //'idFollowup',

                        //'userid',
                        /*'lastchange'=>array(
                            'header'=>$model->getAttributeLabel('lastchange'),
                            'name'=>'lastchange',
                        ),*/

                        'level'=>array(
                            'header'=>$model->getAttributeLabel('level'),
                            'name'=>'level',
                        ),
                        'themeid'=>array(
                            'header'=>$model->getAttributeLabel('themeid'),
                            'name'=>'themeid',
                        ),

                        //'all_err',
                        //'all_ans',
                        /*'all_time'=>array(
                            'header'=>$model->getAttributeLabel('all_time'),
                            'name'=>'all_time',
                        ),*/

                        /*'Speak'=>array(
                            'header'=>$model->getAttributeLabel('stu_por'),
                            'name'=>'Speak',
                        ),
                        'Write'=>array(
                            'header'=>$model->getAttributeLabel('dic_por'),
                            'name'=>'Write',
                        ),*/
                        //'dic_ans',
                        //'dic_err',
                        /*'Interpret'=>array(
                            'header'=>$model->getAttributeLabel('rol_por'),
                            'name'=>'Interpret',
                        ),
                        'Video class'=>array(
                            'header'=>$model->getAttributeLabel('gra_vid'),
                            'name'=>'Video class',
                        ),*/
                        //'gra_ans',
                        //'gra_err',
                        /*'Exercises'=>array(
                            'header'=>$model->getAttributeLabel('wri_por'),
                            'name'=>'Exercises',
                        ),*/
                        //'wri_ans',
                        //'wri_err',
                        /*'Vocabulary'=>array(
                            'header'=>$model->getAttributeLabel('new_por'),
                            'name'=>'Vocabulary',
                        ),*/

                        'Total'=>array(
                            'header'=>$model->getAttributeLabel('all_por'),
                            'name'=>'Total',
                        ),

                        'eva_por'=>array(
                            'header'=>$model->getAttributeLabel('eva_por'),
                            'name'=>'eva_por',
                        ),
                        //'spe_ans',
                        //'time_aux',
                        //'gra_vid',
                        //'rol_on',
                        //'exercises',
                        //'eva_por',
                        /*'View'=>array(
                            'header'=>Yii::t('mainApp','Options'),
                            'name'=>'idFollowup',
                            'type'=>'@'.Yii::t('mainApp','View').'@index.php?r=b2cFollowup/view&id=*idFollowup*&level='.$level.'&criterio='.urlencode($criterio),
                        ),*/

                    ),
                )); ?>
            </div>
        </div>
    </div>
</div>