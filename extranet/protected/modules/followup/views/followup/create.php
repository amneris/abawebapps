<?php
/* @var $this B2cFollowupController */
/* @var $model B2cFollowup */

$this->breadcrumbs=array(
	'B2c Followups'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List B2cFollowup', 'url'=>array('index')),
	array('label'=>'Manage B2cFollowup', 'url'=>array('admin')),
);
?>

<h1>Create B2cFollowup</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>