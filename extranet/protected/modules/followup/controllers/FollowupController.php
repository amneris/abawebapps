<?php

class FollowupController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout='extranet';

    public $levelList = array('All levels'=>'All levels','Beginners'=>'Beginners','Lower intermediate'=>'Lower intermediate','Intermediate'=>'Intermediate',
        'Upper Intermediate'=>'Upper Intermediate', 'Advanced'=>'Advanced', 'Business'=>'Business');

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view','admin','update', 'delete', 'create','export', 'viewByUser', 'Analytics'),
                'roles'=>array(Role::_ABAMANAGER, Role::_AGENT, Role::_RESPONSIBLE),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
	/**
   * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     * @param $level
     * @param $criterio
     * @param $defaultPartner
     * @param $defaultPartnerGroup
     */
    public function actionView($id, $level, $criterio)
    {
        $model = new Followup();
        $model = $model->findByIdFollowup($id);

        $this->render('view',array('model'=>$model, 'level'=>$level, 'criterio'=>$criterio, 'totalsPercent'=>$this->getTotals($model)));
    }

    public function getTotals($model)
    {
        $totalsPercent = array();

        //ABA Film percent
        if($model->sit_por > 50)
            $totalsPercent['sitPercent']=100;
        else
            $totalsPercent['sitPercent'] = $model->sit_por;

        //Speak percent
        $totalsPercent['stuPercent'] = $model->stu_por * 2;
        if($totalsPercent['stuPercent']>100)
            $totalsPercent['stuPercent']=100;

        //Write percent
        $totalsPercent['dicPercent'] = $model->dic_por;
        if($totalsPercent['dicPercent']>100)
            $totalsPercent['dicPercent']=100;

        //Interpret percent
        $totalsPercent['rolPercent'] = $model->rol_por;
        if($totalsPercent['rolPercent']>100)
            $totalsPercent['rolPercent']=100;

        //Video Class percent
        if($model->gra_vid > 0)
            $totalsPercent['gra_vid']=100;
        else
            $totalsPercent['gra_vid']=$model->gra_vid;

        //Exercises percent
        $totalsPercent['wriPercent'] = $model->wri_por;
        if($totalsPercent['wriPercent']>100)
            $totalsPercent['wriPercent']=100;

        //Vocabulary percent
        $totalsPercent['newPercent'] = $model->new_por * 2;
        if($totalsPercent['newPercent']>100)
            $totalsPercent['newPercent']=100;

        //Assessment percent
        $totalsPercent['evaPercent'] = $model->eva_por;

        //Total percent
        $totalsPercent['total'] = floor(($totalsPercent['sitPercent'] +  $totalsPercent['stuPercent'] + $totalsPercent['dicPercent'] +
                $totalsPercent['rolPercent'] + $totalsPercent['gra_vid'] + $totalsPercent['wriPercent'] + $totalsPercent['newPercent']) / 7);

        return $totalsPercent;

    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Followup;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Followup']))
		{
			$model->attributes=$_POST['Followup'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Followup']))
		{
			$model->attributes=$_POST['Followup'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Followup');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
    public function actionAdmin()
    {
        //we get from database all avalaible groups.
        $groups = Group::model()->getAvailableGroups(Utils::getSessionVar('idEnterprise'));
        //we get from database all avalaible periods.
        $periods = Period::model()->getAvailablePeriods(Utils::getSessionVar('idEnterprise'));

        $selectedPartner = Utils::getSessionVar('idPartner');
        //$selectedPartner=7002;
        if(isset($_POST['level']))
        {
            $level = $_POST['level'];
            $criterio = $_POST['criterio'];
        }
        else if(isset($_GET['level']))
        {
            $level = $_GET['level'];
            $criterio = $_GET['criterio'];
        }
        else
        {
            $level = $this->levelList['All levels'];
            $criterio = '';
        }

        if(isset($_GET['startDate']))
        {
            $startDate = $_GET['startDate'];
        }
        else
        {
            $startDate = date('Y-m-d');
        }
        if(isset($_GET['endDate']))
        {
            $endDate = $_GET['endDate'];
        }
        else
        {
            $endDate = date('Y-m-d');
        }

        if(isset($_GET['group']))
        {
            $group = $_GET['group'];
        }
        else
        {
            $group = '';
        }

        if(isset($_GET['period']))
        {
            $period = $_GET['period'];
        }
        else
        {
            $period = '';
        }

        if(isset($_GET['status']))
        {
            $status = $_GET['status'];
        }
        else
        {
            $status = '';
        }

        if(isset($_GET['withoutprogress']))
        {
            $withoutprogress = $_GET['withoutprogress'];
        }
        else
        {
            $withoutprogress = '';
        }

        $model = new Followup('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Followup']))
            $model->attributes=$_GET['Followup'];


        $contador = $model->countAll($level, $criterio, $selectedPartner, $startDate, $endDate, $group, $period, $status);
        $pages = new CPagination($contador);
        $pages->setPageSize(Yii::app()->params['pageSize']);

        $this->render('admin',array(
            'model'=>$model, 'level'=>$level, 'levelList'=>$this->levelList, 'pages'=>$pages, 'criterio'=>$criterio, 'startDate'=>$startDate, 'endDate'=>$endDate,
            'selectedPartner'=>$selectedPartner,'group'=>$group, 'groups'=>$groups,'period'=>$period, 'periods'=>$periods, 'status' => $status, 'withoutprogress' => $withoutprogress
        ));

    }

    //this actios is called by ajax to pupulate a dropdownlist
    public function actionGetGroupPartnerList()
    {
        $defaultPartnerGroup = $_POST['defaultPartnerGroup'];
        $partnerSelection = AbaPartnersList::model()->getListPartnerFromGroup($defaultPartnerGroup);
        $dropDownProvider = AbaPartnersList::model()->getFullGroupList($partnerSelection);
        foreach($dropDownProvider as $key=>$value)
        {
            echo CHtml::tag('option', array('value'=>$key),CHtml::encode($value),true);
        }
        echo CHtml::tag('option', array('value'=>''),CHtml::encode('All partners'),true);
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Followup::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='b2c-followup-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionExport()
    {
        set_time_limit(600);

        $charset = Yii::app()->request->getParam('charset', 'UTF-8');
        $charset = ( $charset !== 'GB2312' ) ? 'UTF-8' : $charset;

        $noProgress = Yii::app()->request->getParam('withoutprogress', false);

        if(isset($_GET['level']))
            $level = $_GET['level'];
        else
            $level = $this->levelList['Beginners'];

        if(isset($_GET['criterio']))
            $criterio = $_GET['criterio'];
        else
            $criterio = '';

        if(isset($_GET['selectedPartner']))
            $selectedPartner = $_GET['selectedPartner'];


        if(isset($_GET['startDate']) && isset($_GET['endDate']))
        {
            $startDate = $_GET['startDate'];
            $endDate = $_GET['endDate'];
        }
        else
        {
            $startDate = date('Y-m-d');
            $endDate = $startDate;
        }

        if(isset($_GET['group']))
        {
            $group = $_GET['group'];
        }
        else
        {
            $group = '';
        }

        if(isset($_GET['period']))
        {
            $period = $_GET['period'];
        }
        else
        {
            $period = '';
        }

      if(isset($_GET['status']))
      {
        $status = $_GET['status'];
      }
      else
      {
        $status = '';
      }

      $model = new Followup();
        $dataReader = $model->searchExport($level, $criterio, $selectedPartner, $startDate, $endDate, $group, $period, $status);
        $data = array();
        $xls = new JPhpCsv($charset, false, 'Users');
        $flagFirst=true;
        $fieldWithPorcen = array("ABA film","Speak","Write","Interpret","Video class","Exercises","Vocabulary","Total");

        $randomCode = 'zTmp_'.date("YmdHis").'_'.md5( Utils::getSessionVar('idUser').'_'.Utils::getSessionVar('idEnterprise').'_'.microtime(true).'_'.rand(0,100) );
        foreach($dataReader as $row)
        {
            if($flagFirst)
            {
                $flagFirst= false;
                $theRow=array();
                foreach ($row as $key =>$value)
                {
                    if($key!=='tmpCurrentWatchedVideoClass')
                    {
                        $key = mb_convert_encoding($key, $charset, 'UTF-8');
                        array_push($theRow, $key);
                    }
                }
                array_push($data, $theRow);
            }
            $theRow=array();
            foreach ($row as $key =>$value)
            {
                if($key!=='tmpCurrentWatchedVideoClass')
                {
                    if(in_array($key, $fieldWithPorcen))
                    {
                        array_push($theRow, $value."%");
                    }
                    else
                    {
                        $value = mb_convert_encoding($value, $charset, 'UTF-8');
                        array_push($theRow, $value);
                    }
                }
            }
            array_push($data, $theRow);

            Followup::insertException( $row['idStudent'], $randomCode );
        }

        if ( $noProgress ) {
            $data = $this->addUsersWithNoProgress($data, $randomCode);
        }

        if($flagFirst==true)
        {
            $theRow=array();
            array_push($theRow, "DATA NOT FOUND (Data not exported)");
            array_push($data, $theRow);
        }
        $xls->addArray($data);
        $xls->generateXML("StudyReport-".$level."-".date("d-m-Y"));
    }

    //new functionality here
    public function actionViewByUser($id='')
    {
        $idEnterprise = Utils::getSessionVar('idEnterprise');

        $studentModel = Student::model()->findByPk($id);
        if ( !$studentModel || $studentModel->idEnterprise != Utils::getSessionVar('idEnterprise') ) {
            // ** TODO: launch exception.
            throw Exception('Error.');
        }

        if ( !empty($studentModel->idStudentCampus) ) {
            $mStudentCampus = UserCampus::model()->findByPk($studentModel->idStudentCampus);

            $studentModel->idLevel = ($mStudentCampus) ? $mStudentCampus->currentLevel : $studentModel->idLevel;
        }


        if(isset($_POST['name']))
        {
          $studentModel->name=$_POST['name'];
          $studentModel->save(false);
          $this->updateSelligentData( $studentModel );
        }

        if(isset($_POST['surname']))
        {
            $studentModel->surname=$_POST['surname'];
            $studentModel->save(false);
          $this->updateSelligentData( $studentModel );
        }

        if(isset($_POST['email']))
        {
          $errorMail = false;
          $moAnyUserEmail = UserCampus::model()->findByAttributes(array('email' => $_POST['email']));
          if($moAnyUserEmail == NULL) {
            $mStudent_tmp = Student::model()->findByAttributes( array('email' => $_POST['email'], 'isdeleted' => 0) );
//            if ( empty($studentModel->idStudentCampus) && (!$mStudent_tmp && $studentModel || $mStudent_tmp->id == $studentModel->id) ) {
              if ( !empty($studentModel->idStudentCampus) ) {
                $mStudentCampus2 = UserCampus::model()->findByAttributes(array('email' => $_POST['email']));
                if ( $mStudentCampus == NULL || $mStudentCampus2 != NULL ) {
                    $errorMail = true;
                } else {
                    $mStudentCampus->email = $_POST['email'];
                    $errorMail = !$mStudentCampus->save();
                }
              }

              if ( !$errorMail ) {
                $studentModel->email = $_POST['email'];
                $studentModel->save(false);
                $this->updateSelligentData($studentModel);
              }
//              }
          }
        }

        if(isset($_POST['Student']) && isset($_POST['Student']['idLevel']) )
        {
          $newIdLevel = $_POST['Student']['idLevel'];

          if ( $studentModel->idLevel != $newIdLevel && ( $newIdLevel > 0 || $studentModel->idLevel >= 0 ) ) {
            $studentModel->idLevel = $newIdLevel;
			      $studentModel->save(false);

            if ( $studentModel->idStudentCampus > 0 ) {
              $mStudentOnCampus = UserCampus::model()->findByPk( $studentModel->idStudentCampus );
              if ( $mStudentOnCampus ) {
                $mStudentOnCampus->currentLevel =$newIdLevel;
                $mStudentOnCampus->save();
              }
            }

          }

        }

        $studentGroupModel = new StudentGroup();
        $studentGroupModel= $studentGroupModel->findByAttributes(array('idStudent'=>$id));
        if(isset($_POST['StudentGroup']))
        {
            $studentGroupModel->attributes=$_POST['StudentGroup'];
            $studentGroupModel->save(false);
        }

        $studentPeriodModel= StudentPeriod::model()->findByAttributes(array('idStudent'=>$id));
        if ( !$studentPeriodModel ) $studentPeriodModel = new StudentPeriod();
        if(isset($_POST['StudentPeriod']))
        {
            $studentPeriodModel->attributes=$_POST['StudentPeriod'];
            $studentPeriodModel->save(false);

            $studentGroupModel->attributes=$_POST['StudentPeriod'];
            $studentGroupModel->save(false);
        }

        $levels = Level::model()->getAvailableLevels();

        //we get the avalible groups of an enterprise.
        $dropDownProvider = Group::model()->findAll(array(
            'order'=>'name',
            'condition'=>'idEnterprise=:id',
            'params'=>array(':id'=>Utils::getSessionVar('idEnterprise'))
        ));
        $groupList = CHtml::listData($dropDownProvider, 'id', 'name');

        //we get the avalible periods.
        $dropDownProvider = Period::model()->findAll(array(
            'order'=>'name',
            'condition'=>'idEnterprise=:id',
            'params'=>array(':id'=>Utils::getSessionVar('idEnterprise'))
        ));
        $periodList = CHtml::listData($dropDownProvider, 'id', 'name');

        // ** We get the avalible teachers.
        if(isset($_POST['Student']) && isset($_POST['Student']['idTeacher']) )
        {
            $idTeacher = $_POST['Student']['idTeacher'];

            if ( $studentModel->idStudentCampus > 0 ) {
                if ( $idTeacher ) {
                    $mUserTeacher = User::model()->findByPk($idTeacher);
                    $mStudentCampus3 = UserCampus::model()->findByAttributes(array('email' => $mUserTeacher->email ));
                } else {
                    $mStudentCampus3 = UserCampus::getTeacherByLanguage( Utils::getSessionVar('isoLanguageEnterprise') );
                }

                $mStudentOnCampus = UserCampus::model()->findByPk( $studentModel->idStudentCampus );
                if ( $mStudentOnCampus ) {
                    $mStudentOnCampus->teacherId =$mStudentCampus3->id;
                    $mStudentOnCampus->save();
                }
            }

            $studentModel->idTeacher = $idTeacher;
            $studentModel->save(false);

        }
        $arTeacher = User::getTeachers( $idEnterprise, true );
        $arTeacher = CHtml::listData($arTeacher, 'id', 'fullname');

        $arStatus = array(
          array('id' => StudentGroup::_STATUS_ACTIVE, 'name' => Yii::app()->getModule('student')->t('lbl: Activo') ),
          array('id' => StudentGroup::_STATUS_INACTIVE, 'name' => Yii::app()->getModule('student')->t('lbl: Inactivo') ),
        );
        $availableStatus = CHtml::listData($arStatus, 'id', 'name');

        $selectedPartner = Utils::getSessionVar('idPartner');
        $model = new Followup();

        $this->render('viewByUser', array('model'=>$model,'email'=>$studentModel['email'], 'idCampus'=>$studentModel['idStudentCampus'], 'selectedPartner'=>$selectedPartner, 'user'=>$studentModel,
                    'groups'=>$groupList, 'periods'=>$periodList, 'arStatus' => $availableStatus, 'studentGroupModel'=>$studentGroupModel, 'studentPeriodModel'=>$studentPeriodModel, 'levels' => $levels,
                    'studentModel'=>$studentModel, 'arTeacher' => $arTeacher));
    }

  public function actionAnalytics() {
    $this->render('fakeAnalytics');
  }

  protected function updateSelligentData( $mStudent ) {

    $idEnterprise = Utils::getSessionVar('idEnterprise');

    $allOk = HeAbaMail::sendEmail( array(
      'action' => HeAbaSelligent::_EDITSTUDENT,
      'idStudent' => $mStudent->id,
      'idStudentCampus' => $mStudent->idStudentCampus,
      'idEnterprise' => $idEnterprise,
      'name' => $mStudent->name,
      'surname' => $mStudent->surname,
      'email' => $mStudent->email,
      'isoLanguage' => Utils::getSessionVar('isoLanguageEnterprise'),
      'callback' => 'ExtranetController::actionMailSent',
    ) );

  }

  protected function addUsersWithNoProgress($data, $randomCode) {

      $level = Yii::app()->request->getParam('level', $this->levelList['Beginners']);
      $criterio = Yii::app()->request->getParam('criterio', '');
      $selectedPartner = Yii::app()->request->getParam('selectedPartner', '');
      $startDate = Yii::app()->request->getParam('startDate', date('Y-m-d'));
      $endDate = Yii::app()->request->getParam('endDate', $startDate);
      $group = Yii::app()->request->getParam('group', '');
      $period = Yii::app()->request->getParam('period', '');
      $status = Yii::app()->request->getParam('status', '');

      $newData = Followup::model()->getDataUsers( array(
        'level' => $level,
        'email' => $criterio,
        'selectedPartner' => $selectedPartner,
        'startDate' => $startDate,
        'endDate' => $endDate,
        'group' => $group,
        'period' => $period,
        'status' => $status,
        'excludeList' => $randomCode,
        'data' => $data,
      ));

      return $newData;
  }

}
