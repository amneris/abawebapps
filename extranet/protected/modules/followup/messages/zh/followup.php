<?php
return array(
	'Surnames' => '', 
	'helps' => '', 
	'All helps' => '', 
	'Exercises' => '', 
	'errors' => '', 
	'All errors' => '', 
	'Write' => '', 
	'Evaluation' => '', 
	'Expiration date' => '', 
	'Group' => '', 
	'Speak' => '', 
	'Id' => '', 
	'Id followup' => '', 
	'User information' => '', 
	'Interpret' => '', 
	'Level' => '', 
	'Average score' => '', 
	'Total percentage' => '', 
	'Telephone' => '', 
	'Total time' => '', 
	'Unit' => '', 
	'Video class' => '', 
	'Vocabulary' => '', 
	'Próximamente' => '', 
	'Show units' => '', 
	'Note: We recommend that you only enter an end date and leave the start date empty to get a report for all the users as if not, some users could be missed if they registered by themselves beforehand.' => '', 
	'Export units to CSV' => '', 
	'filter by email' => '', 
	'and' => '', 
	'between' => '', 
	'Study report for level' => '', 
	'Study report' => '', 
	'No exam taken' => '', 
	'No progress to show yet.' => '', 
);
