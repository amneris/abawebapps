<?php

/**
 * This is the model class for table "followup4".
 *
 * The followings are the available columns in table 'followup4':
 * @property integer $id
 * @property integer $themeid
 * @property integer $userid
 * @property string $lastchange
 * @property integer $all_por
 * @property integer $all_err
 * @property integer $all_ans
 * @property integer $all_time
 * @property integer $sit_por
 * @property integer $stu_por
 * @property integer $dic_por
 * @property integer $dic_ans
 * @property integer $dic_err
 * @property integer $rol_por
 * @property integer $gra_por
 * @property integer $gra_ans
 * @property integer $gra_err
 * @property integer $wri_por
 * @property integer $wri_ans
 * @property integer $wri_err
 * @property integer $new_por
 * @property integer $spe_por
 * @property integer $spe_ans
 * @property integer $time_aux
 * @property integer $gra_vid
 * @property integer $rol_on
 * @property string $exercises
 * @property integer $eva_por
 */
class Followup extends AbaFollowup
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Followup the static model class
     */

    public $name;
    public $surnames;
    public $email;
    public $telephone;
    public $entryDate;
    public $expirationDate;
    public $idFollowup;
    public $themeid;
    public $currentLevel;
    public $userType;
    public $level;

    public $levelRange = array('begStart'=>1, 'begEnd'=>24, 'lowStart'=>25, 'lowEnd'=>48, 'intStart'=>49, 'intEnd'=>72, 'uppStart'=>73, 'uppEnd'=>96,
        'advStart'=>97, 'advEnd'=>120, 'busStart'=>121, 'busEnd'=>144);


    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return CDbConnection database connection
     */
    public function getDbConnection()
    {
        return Yii::app()->dbSlave;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'name'=> FollowupModule::t('Name'),
            'surnames'=> FollowupModule::t('Surnames'),
            'email'=> FollowupModule::t('Email'),
            'telephone'=> FollowupModule::t('Telephone'),
            'entryDate'=> Yii::t('app','Entry date'),
            'expirationDate'=> FollowupModule::t('Expiration date'),
            'idFollowup'=> FollowupModule::t('Id followup'),
            'themeid'=> FollowupModule::t('Unit'),
            'id' => FollowupModule::t('Id'),
            'userid' => FollowupModule::t('User'),
            'lastchange' => FollowupModule::t('Last change'),
            'all_por' => FollowupModule::t('Total percentage'),
            'all_err' => FollowupModule::t('All errors'),
            'all_ans' => FollowupModule::t('All helps'),
            'all_time' => FollowupModule::t('Total time'),
            'sit_por' => FollowupModule::t('ABA film'),
            'stu_por' => FollowupModule::t('Speak'),
            'dic_por' => FollowupModule::t('Write'),
            'dic_ans' => FollowupModule::t('helps'),
            'dic_err' => FollowupModule::t('errors'),
            'rol_por' => FollowupModule::t('Interpret'),
            'gra_por' => FollowupModule::t('Name'),
            'gra_ans' => FollowupModule::t('helps'),
            'gra_err' => FollowupModule::t('errors'),
            'wri_por' => FollowupModule::t('Exercises'),
            'wri_ans' => FollowupModule::t('helps'),
            'wri_err' => FollowupModule::t('errors'),
            'new_por' => FollowupModule::t('Vocabulary'),
            'spe_por' => FollowupModule::t('Name'),
            'spe_ans' => FollowupModule::t('helps'),
            'time_aux' => FollowupModule::t('Name'),
            'gra_vid' => FollowupModule::t('Video class'),
            'rol_on' => FollowupModule::t('Name'),
            'exercises' => FollowupModule::t('Name'),
            'eva_por' => FollowupModule::t('Evaluation'),
            'userType' => FollowupModule::t('Name'),
            'level' => FollowupModule::t('Level'),
		);
	}

    public function findByLevel($level, $pageObject, $criterio, $defaultPartner, $startDate, $endDate, $group, $period, $status=0)
    {
        $sql="SELECT i.name, i.surnames, i.email, i.telephone, i.currentLevel, i.entryDate, i.expirationDate, f.id as idFollowup, f.lastchange,
        f.dic_ans, f.dic_err, f.gra_ans, f.gra_err, f.wri_ans, f.wri_err, f.all_ans, f.all_err,
        IF(f.themeid >= ".$this->levelRange['begStart']." AND f.themeid <= ".$this->levelRange['begEnd'].", 'Beginner',
        IF(f.themeid >= ".$this->levelRange['lowStart']." AND f.themeid <= ".$this->levelRange['lowEnd'].", 'Lower Intermediate',
        IF(f.themeid >= ".$this->levelRange['intStart']." AND f.themeid <= ".$this->levelRange['intEnd'].", 'Intermediate',
        IF(f.themeid >= ".$this->levelRange['uppStart']." AND f.themeid <= ".$this->levelRange['uppEnd'].", 'Upper Intermediate',
        IF(f.themeid >= ".$this->levelRange['advStart']." AND f.themeid <= ".$this->levelRange['advEnd'].", 'Advanced',
        IF(f.themeid >= ".$this->levelRange['busStart']." AND f.themeid <= ".$this->levelRange['busEnd'].", 'Business','')))))) as 'level',
        f.themeid, f.eva_por, f.all_por as Total, IF(i.userType=2, 'Premium', IF(i.userType=0, 'Deleted', IF(i.cancelReason IS NULL AND i.userType=1, 'Free' , 'Ex-Premium'))) as 'userType'
              FROM aba_b2c_summary.followup4_summary f
              INNER JOIN  aba_b2c.user i ON f.userid = i.id
              INNER JOIN abaenglish_extranet.student s ON i.id = s.idStudentCampus
              INNER JOIN abaenglish_extranet.enterprise e ON s.idEnterprise = e.id ";

        $sql.="INNER JOIN abaenglish_extranet.student_group sg ON s.id = sg.idStudent ";

        if($period<>'')
        {
            $sql.="INNER JOIN abaenglish_extranet.student_period sp ON s.id = sp.idStudent ";
        }

        $sql.=" WHERE ";

        if($group<>'')
        {
            $sql.="sg.idGroup = ".$group." AND ";
        }

        if($period<>'')
        {
            $sql.="sp.idPeriod = ".$period." AND ";
            $sql.= "f.lastchange BETWEEN sp.startDate AND sp.expirationDate AND ";
        }

        if(empty($status))
        {
            $sql.="sg.status = ".StudentGroup::_STATUS_ACTIVE." AND ";
        }


        if($startDate<>'')
        {
            if($endDate<>'')
            {
                $sql.= " f.lastchange BETWEEN '$startDate' AND '$endDate 23:59:59' AND ";
            }
            else
            {
                $sql.= " f.lastchange >= '$startDate' AND ";
            }
        }
        elseif($endDate<>'')
        {
            $sql.= " f.lastchange <= '$endDate 23:59:59' AND ";
        }

        switch ($level)
        {
            case 'Beginners':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['begStart']." AND ".$this->levelRange['begEnd']." ";
                break;
            case 'Lower intermediate':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['lowStart']." AND ".$this->levelRange['lowEnd']." ";
                break;
            case 'Intermediate':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['intStart']." AND ".$this->levelRange['intEnd']." ";
                break;
            case 'Upper Intermediate':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['uppStart']." AND ".$this->levelRange['uppEnd']." ";
                break;
            case 'Advanced':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['advStart']." AND ".$this->levelRange['advEnd']." ";
                break;
            case 'Business':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['busStart']." AND ".$this->levelRange['busEnd']." ";
                break;
            case 'All levels':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['begStart']." AND ".$this->levelRange['busEnd']." ";
                break;
        }

        if($criterio<>'')
        {
            $sql .="AND i.email like '%$criterio%' ";
        }
//        $sql .="AND i.idPartnerCurrent = $defaultPartner ";
        $sql .="AND e.idPartner = $defaultPartner ";

        //avoid deleted users from extranet
        $sql .= "AND s.isdeleted = 0";

        $initPage= $pageObject->getCurrentPage() * $pageObject->getPageSize();
        $endPage= $pageObject->getPageSize();

        $sql .= " LIMIT ".$initPage.", $endPage;";

        $dataProvider = new CSqlDataProvider($sql,
            array(
                'totalItemCount'=>$pageObject->getItemCount(),
                'pagination'=>array('pageSize'=>$pageObject->getPageSize(),
                )));
        $dataProvider->keyField = 'idFollowup';
        $dataProvider->db = Yii::app()->dbSlave;

        return $dataProvider;

    }

    public function countAll($level, $criterio, $defaultPartner, $startDate, $endDate, $group, $period, $status = 0)
    {
        $count = 0;
        $sql="SELECT count(*) FROM aba_b2c_summary.followup4_summary f INNER JOIN aba_b2c.user i ON f.userid = i.id INNER JOIN abaenglish_extranet.student s ON i.id = s.idStudentCampus INNER JOIN abaenglish_extranet.enterprise e ON s.idEnterprise = e.id ";

        $sql.="INNER JOIN abaenglish_extranet.student_group sg ON s.id = sg.idStudent ";

        if($period<>'')
        {
            $sql.="INNER JOIN abaenglish_extranet.student_period sp ON s.id = sp.idStudent ";
        }

        $sql.=" WHERE ";

        if($group<>'')
        {
            $sql.="sg.idGroup = ".$group." AND ";
        }

        if($period<>'')
        {
            $sql.="sp.idPeriod = ".$period." AND ";
            $sql.= "f.lastchange BETWEEN sp.startDate AND sp.expirationDate AND ";
        }

        if(empty($status))
        {
            $sql.="sg.status = ".StudentGroup::_STATUS_ACTIVE." AND ";
        }

        if($startDate<>'')
        {
            if($endDate<>'')
            {
                $sql.= " f.lastchange BETWEEN '$startDate' AND '$endDate 23:59:59' AND";
            }
            else
            {
                $sql.= " f.lastchange >= '$startDate' AND";
            }
        }
        elseif($endDate<>'')
        {
            $sql.= " f.lastchange <= '$endDate 23:59:59' AND";
        }

        switch ($level)
        {
            case 'Beginners':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['begStart']." AND ".$this->levelRange['begEnd']." ";
                break;
            case 'Lower intermediate':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['lowStart']." AND ".$this->levelRange['lowEnd']." ";
                break;
            case 'Intermediate':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['intStart']." AND ".$this->levelRange['intEnd']." ";
                break;
            case 'Upper Intermediate':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['uppStart']." AND ".$this->levelRange['uppEnd']." ";
                break;
            case 'Advanced':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['advStart']." AND ".$this->levelRange['advEnd']." ";
                break;
            case 'Business':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['busStart']." AND ".$this->levelRange['busEnd']." ";
                break;
            case 'All levels':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['begStart']." AND ".$this->levelRange['busEnd']." ";
                break;
        }

        if($criterio<>'')
        {
            $sql .="AND i.email like '%$criterio%' ";
        }
//        $sql .="AND i.idPartnerCurrent = $defaultPartner ";
        $sql .="AND e.idPartner = $defaultPartner ";

        //avoid deleted users from extranet
        $sql .= "AND s.isdeleted = 0;";

        $count += Yii::app()->dbSlave->createCommand($sql)->queryScalar();
        return $count;
    }

    public function searchExport($level, $criterio, $selectedPartner, $startDate, $endDate, $group, $period, $status = 0)
    {
        $sql="SELECT s.id as idStudent, i.name as Name, i.surnames as Surnames, i.email as Email, eg.name as 'Group', ep.name as 'Period', DATE_FORMAT(i.entryDate, '%d-%m-%Y %k:%i:%s') as 'Entry date', DATE_FORMAT(i.expirationDate, '%d-%m-%Y') as 'Expiration date',
        DATE_FORMAT(f.lastchange, '%d-%m-%Y %k:%i:%s') as 'Last change',
        IF(f.themeid >= ".$this->levelRange['begStart']." AND f.themeid <= ".$this->levelRange['begEnd'].", 'Beginner',
        IF(f.themeid >= ".$this->levelRange['lowStart']." AND f.themeid <= ".$this->levelRange['lowEnd'].", 'Lower Intermediate',
        IF(f.themeid >= ".$this->levelRange['intStart']." AND f.themeid <= ".$this->levelRange['intEnd'].", 'Intermediate',
        IF(f.themeid >= ".$this->levelRange['uppStart']." AND f.themeid <= ".$this->levelRange['uppEnd'].", 'Upper Intermediate',
        IF(f.themeid >= ".$this->levelRange['advStart']." AND f.themeid <= ".$this->levelRange['advEnd'].", 'Advanced',
        IF(f.themeid >= ".$this->levelRange['busStart']." AND f.themeid <= ".$this->levelRange['busEnd'].", 'Business','')))))) as 'level',
        f.themeid as Unit, f.sit_por as 'ABA film',  f.stu_por as 'Speak', f.dic_por as 'Write', f.rol_por as 'Interpret', gra_vid as 'Video class',f.wri_por as 'Exercises', f.new_por as 'Vocabulary', f.all_por as Total, f.eva_por as Assessment
              FROM aba_b2c_summary.followup4_summary f INNER JOIN  aba_b2c.user i ON f.userid = i.id INNER JOIN abaenglish_extranet.student s ON i.id = s.idStudentCampus INNER JOIN abaenglish_extranet.enterprise e ON s.idEnterprise = e.id ";


        $sql.="INNER JOIN abaenglish_extranet.student_group sg ON s.id = sg.idStudent ";
        if($group<>'')
        {
            $sql.="INNER JOIN abaenglish_extranet.egroup eg ON sg.idGroup = eg.id ";
        }
        else
        {
            $sql.="LEFT JOIN abaenglish_extranet.egroup eg ON sg.idGroup = eg.id ";
        }

        if($period<>'')
        {
            $sql.="INNER JOIN abaenglish_extranet.student_period sp ON s.id = sp.idStudent ";
            $sql.="INNER JOIN abaenglish_extranet.period ep ON sp.idPeriod = ep.id ";
        }
        else
        {
            $sql.="LEFT JOIN abaenglish_extranet.period ep ON sg.idPeriod = ep.id ";
        }

        $sql.=" WHERE ";

        if($group<>'')
        {
            $sql.="sg.idGroup = ".$group." AND ";
        }

        if($period<>'')
        {
            $sql.="sp.idPeriod = ".$period." AND ";
            $sql.= "f.lastchange BETWEEN sp.startDate AND sp.expirationDate AND ";
        }

        if( empty($status) )
        {
            $sql.="sg.status = ".StudentGroup::_STATUS_ACTIVE." AND ";
        }

        if($startDate<>'')
        {
            if($endDate<>'')
            {
                $sql.= " f.lastchange BETWEEN '$startDate' AND '$endDate 23:59:59' AND";
            }
            else
            {
                $sql.= " f.lastchange >= '$startDate' AND";
            }
        }
        elseif($endDate<>'')
        {
            $sql.= " f.lastchange <= '$endDate 23:59:59' AND";
        }

        switch ($level)
        {
            case 'Beginners':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['begStart']." AND ".$this->levelRange['begEnd']." ";
                break;
            case 'Lower intermediate':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['lowStart']." AND ".$this->levelRange['lowEnd']." ";
                break;
            case 'Intermediate':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['intStart']." AND ".$this->levelRange['intEnd']." ";
                break;
            case 'Upper Intermediate':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['uppStart']." AND ".$this->levelRange['uppEnd']." ";
                break;
            case 'Advanced':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['advStart']." AND ".$this->levelRange['advEnd']." ";
                break;
            case 'Business':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['busStart']." AND ".$this->levelRange['busEnd']." ";
                break;
            case 'All levels':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['begStart']." AND ".$this->levelRange['busEnd']." ";
                break;
        }

        if($criterio<>'')
        {
            $sql .="AND i.email like '%$criterio%' ";
        }
//        $sql .="AND i.idPartnerCurrent = $selectedPartner ";
        $sql .="AND e.idPartner = $selectedPartner ";

        //avoid deleted users
        $sql .= "AND s.isdeleted = 0;";

        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();

        $aMockExport = array();

        while(($row = $dataReader->read())!==false)
        {
            $aMockExport[] = $row;

        }
        return $aMockExport;
    }

    /**
     * Returns the progress percentages for all sections for the given user (extranet reports)
     *
     * @param integer $idUser
     *
     * @return array type containing all relevant section progress percentages
     */
    public function getProgressForUser($idUser) /* Single view report */
    {
        $responseArray = array();
        $sql = "SELECT f.themeid, f.lastchange, f.all_por, f.sit_por, f.stu_por, f.dic_por, f.rol_por, f.wri_por, f.new_por, f.gra_vid, f.eva_por, f.rol_on, f.spe_por, f.dic_ans, f.dic_err, f.wri_ans, f.wri_err, f.eva_por,
                IF(f.themeid >= ".$this->levelRange['begStart']." AND f.themeid <= ".$this->levelRange['begEnd'].", 'Beginner',
                IF(f.themeid >= ".$this->levelRange['lowStart']." AND f.themeid <= ".$this->levelRange['lowEnd'].", 'Lower Intermediate',
                IF(f.themeid >= ".$this->levelRange['intStart']." AND f.themeid <= ".$this->levelRange['intEnd'].", 'Intermediate',
                IF(f.themeid >= ".$this->levelRange['uppStart']." AND f.themeid <= ".$this->levelRange['uppEnd'].", 'Upper Intermediate',
                IF(f.themeid >= ".$this->levelRange['advStart']." AND f.themeid <= ".$this->levelRange['advEnd'].", 'Advanced',
                IF(f.themeid >= ".$this->levelRange['busStart']." AND f.themeid <= ".$this->levelRange['busEnd'].", 'Business','')))))) as 'level'
                FROM " . $this->tableName($idUser) . " f WHERE f.userid=:idUser_param AND f.themeid<>0";

        $params = array(":idUser_param"=> $idUser );
        $dataReader=$this->querySQL($sql, $params);
        $rows = $dataReader->readAll();
        foreach ($rows as $row){
            $responseArray[] = array('level'=>$row['level'], 'IdUnit'=>$row['themeid'], 'LastChange'=>$row['lastchange'], 'TotalUnitPct'=>intval($row['all_por']), 'AbaFilmPct'=>$row['sit_por'],
                'SpeakPct'=>$row['stu_por'], 'WritePct'=>$row['dic_por'], 'WriteAns'=>$row['dic_ans'], 'WriteErr'=>$row['dic_err'], 'InterpretPct'=>$row['rol_por'],
                'VideoClassPct'=>$row['gra_vid'], 'ExercisesPct'=>$row['wri_por'], 'ExercisesAns'=>$row['wri_ans'], 'ExercisesErr'=>$row['wri_err'],
                'VocabularyPct'=>$row['new_por'], 'AssessmentPct'=>($row['eva_por']));
        }
        if(count($responseArray)==0)
        {
            return false;
        }
        else
        {
            return $responseArray;
        }

    }

    public function getDataUsers( $arParams ) {
        $data               = ( isset($arParams['data']) ) ? $arParams['data'] : array();
        $level              = ( isset($arParams['level']) ) ? $arParams['level'] : '';
        $email              = ( isset($arParams['criterio']) ) ? $arParams['criterio'] : '';
        $selectedPartner    = ( isset($arParams['selectedPartner']) ) ? $arParams['selectedPartner'] : '';
        $startDate          = ( isset($arParams['startDate']) ) ? $arParams['startDate'] : '';
        $endDate            = ( isset($arParams['endDate']) ) ? $arParams['endDate'] : '';
        $group              = ( isset($arParams['group']) ) ? $arParams['group'] : '';
        $period             = ( isset($arParams['period']) ) ? $arParams['period'] : '';
        $status             = ( isset($arParams['status']) ) ? $arParams['status'] : '';
        $excludeList        = ( isset($arParams['excludeList']) ) ? $arParams['excludeList'] : false;
        $limit              = ( isset($arParams['limit']) ) ? $arParams['limit'] : 10000;
        $offset             = ( isset($arParams['offset']) ) ? $arParams['offset'] : 0;

        $arBindParams = array();
        if ( $group !== '' ) {
            $sGroup = ' INNER JOIN abaenglish_extranet.egroup eg ON sg.idGroup = eg.id ';
            $fGroup = ' and sg.idGroup = :group ';
            $arBindParams[] = array( 'name' => ':group', 'value' => $group, 'type' => PDO::PARAM_INT);
        } else {
            $sGroup = ' LEFT JOIN abaenglish_extranet.egroup eg ON sg.idGroup = eg.id ';
            $fGroup = '';
        }

        if ( $period !== '' || $excludeList ) {
            $sPeriod = ' INNER JOIN abaenglish_extranet.student_period sp ON s.id = sp.idStudent ';
            $sPeriod .= ' INNER JOIN abaenglish_extranet.period ep ON sp.idPeriod = ep.id ';

            if ( $period !== '' ) {
                $fPeriod = ' and sp.idPeriod = :period ';
                $fPeriod .=  ' and f.lastchange BETWEEN sp.startDate AND sp.expirationDate ';
                $arBindParams[] = array( 'name' => ':period', 'value' => $period, 'type' => PDO::PARAM_INT);
            } else {
                $fPeriod = '';
            }
        } else {
            $sPeriod = ' LEFT JOIN abaenglish_extranet.period ep ON sg.idPeriod = ep.id ';
            $fPeriod = '';
        }

        switch ($level)
        {
            case 'Beginners':
                $fLevel = " f.themeid BETWEEN ".$this->levelRange['begStart']." AND ".$this->levelRange['begEnd']." ";
                break;
            case 'Lower intermediate':
                $fLevel = " f.themeid BETWEEN ".$this->levelRange['lowStart']." AND ".$this->levelRange['lowEnd']." ";
                break;
            case 'Intermediate':
                $fLevel = " f.themeid BETWEEN ".$this->levelRange['intStart']." AND ".$this->levelRange['intEnd']." ";
                break;
            case 'Upper Intermediate':
                $fLevel = " f.themeid BETWEEN ".$this->levelRange['uppStart']." AND ".$this->levelRange['uppEnd']." ";
                break;
            case 'Advanced':
                $fLevel = " f.themeid BETWEEN ".$this->levelRange['advStart']." AND ".$this->levelRange['advEnd']." ";
                break;
            case 'Business':
                $fLevel = " f.themeid BETWEEN ".$this->levelRange['busStart']." AND ".$this->levelRange['busEnd']." ";
                break;
            case 'All levels':
            default:
//                $sLevel = " f.themeid BETWEEN ".$this->levelRange['begStart']." AND ".$this->levelRange['busEnd']." ";
                $fLevel = '';
                break;
        }

        if ( $status !== '' ) {
            $fStatus = " and sg.status = :status ";
            $arBindParams[] = array( 'name' => ':status', 'value' => StudentGroup::_STATUS_ACTIVE, 'type' => PDO::PARAM_INT);
        } else {
            $fStatus = '';
        }

        if(!empty($email))
        {
            $fEmail = " and AND i.email like :email ";
            $arBindParams[] = array( 'name' => ':email', 'value' => "%{$email}%", 'type' => PDO::PARAM_STR);
        } else {
            $fEmail = '';
        }

        $fDate = '';
        $fDate2 = '';
        if ( $excludeList ) {
            if ($startDate !== '') {
                if ( $endDate !== '' ) {
                    $fDate2 = ' and sp.expirationDate >= :startDate AND sp.startDate <= :endDate ';
//                    $arBindParams[] = array( 'name' => ':startDate', 'value' => date("Y-m-d", strtotime($startDate)).' 00:00:00', 'type' => PDO::PARAM_STR);
//                    $arBindParams[] = array( 'name' => ':endDate', 'value' => date("Y-m-d", strtotime($endDate)).' 23:59:59', 'type' => PDO::PARAM_STR);
                } else {
                    $fDate2 = ' and sp.startDate >= :startDate ';
//                    $arBindParams[] = array( 'name' => ':startDate', 'value' => date("Y-m-d", strtotime($startDate)).' 00:00:00', 'type' => PDO::PARAM_STR);
                }

            } else if ( $endDate !== '' ) {
                $fDate2 = ' and sp.startDate <= :endDate ';
//                $arBindParams[] = array( 'name' => ':endDate', 'value' => date("Y-m-d", strtotime($endDate)).' 23:59:59', 'type' => PDO::PARAM_STR);
            }
        } else {
            $fDate2 = '';
        }


        // else {
            if ($startDate !== '') {
                if ( $endDate !== '' ) {
                    $fDate = ' and f.lastchange BETWEEN :startDate AND :endDate ';
                    $arBindParams[] = array( 'name' => ':startDate', 'value' => date("Y-m-d", strtotime($startDate)).' 00:00:00', 'type' => PDO::PARAM_STR);
                    $arBindParams[] = array( 'name' => ':endDate', 'value' => date("Y-m-d", strtotime($endDate)).' 23:59:59', 'type' => PDO::PARAM_STR);
                } else {
                    $fDate = ' and f.lastchange >= :startDate ';
                    $arBindParams[] = array( 'name' => ':startDate', 'value' => date("Y-m-d", strtotime($startDate)).' 00:00:00', 'type' => PDO::PARAM_STR);
                }

            } else if ( $endDate !== '' ) {
                $fDate = ' and f.lastchange <= :endDate ';
                $arBindParams[] = array( 'name' => ':endDate', 'value' => date("Y-m-d", strtotime($endDate)).' 23:59:59', 'type' => PDO::PARAM_STR);
            }
//        }


        if ( $excludeList ) {
            $sExcludeList = ' LEFT JOIN  aba_b2c_summary.followup4_summary f ON f.userid = i.id '.$fDate;

            $fExcludeList = ' and s.id not in ( select idelement from abaenglish_extranet.toundo where code = :excludeListCode ) ';
            $arBindParams[] = array( 'name' => ':excludeListCode', 'value' => $excludeList, 'type' => PDO::PARAM_STR);

            $groupBy = 'GROUP BY idStudent, Name, Surnames, Email, \'Group\', Period, \'Entry date\', \'Expiration date\', \'Last change\', level, Unit, \'ABA film\', \'Speak\', \'Write\', \'Interpret\', \'Video class\', \'Exercises\', \'Vocabulary\', Total, Assessment';

            $fDate = $fDate2;
        } else {
            $sExcludeList = ' INNER JOIN  aba_b2c_summary.followup4_summary f ON f.userid = i.id ';
            $fExcludeList = '';
            $groupBy = '';
        }

        $fPartner = ' and e.idPartner = :idPartner ';
        $arBindParams[] = array( 'name' => ':idPartner', 'value' => $selectedPartner, 'type' => PDO::PARAM_INT);

        $sql = "SELECT s.id as idStudent,
                        i.name as Name,
                        i.surnames as Surnames,
                        i.email as Email,
                        eg.name as 'Group',
                        ep.name as 'Period',
                        DATE_FORMAT(i.entryDate, '%d-%m-%Y %k:%i:%s') as 'Entry date',
                        DATE_FORMAT(i.expirationDate, '%d-%m-%Y') as 'Expiration date',
                        DATE_FORMAT(f.lastchange, '%d-%m-%Y %k:%i:%s') as 'Last change',
                        IF(f.themeid >= ".$this->levelRange['begStart']." AND f.themeid <= ".$this->levelRange['begEnd'].", 'Beginner',
                            IF(f.themeid >= ".$this->levelRange['lowStart']." AND f.themeid <= ".$this->levelRange['lowEnd'].", 'Lower Intermediate',
                            IF(f.themeid >= ".$this->levelRange['intStart']." AND f.themeid <= ".$this->levelRange['intEnd'].", 'Intermediate',
                            IF(f.themeid >= ".$this->levelRange['uppStart']." AND f.themeid <= ".$this->levelRange['uppEnd'].", 'Upper Intermediate',
                            IF(f.themeid >= ".$this->levelRange['advStart']." AND f.themeid <= ".$this->levelRange['advEnd'].", 'Advanced',
                            IF(f.themeid >= ".$this->levelRange['busStart']." AND f.themeid <= ".$this->levelRange['busEnd'].", 'Business',''
                        )))))) as 'level',
                        f.themeid as Unit,
                        f.sit_por as 'ABA film',
                        f.stu_por as 'Speak',
                        f.dic_por as 'Write',
                        f.rol_por as 'Interpret',
                        gra_vid as 'Video class',
                        f.wri_por as 'Exercises',
                        f.new_por as 'Vocabulary',
                        f.all_por as Total,
                        f.eva_por as Assessment
              FROM abaenglish_extranet.student s
                INNER JOIN aba_b2c.user i ON s.idStudentCampus = i.id
                INNER JOIN abaenglish_extranet.enterprise e ON s.idEnterprise = e.id
                INNER JOIN abaenglish_extranet.student_group sg ON s.id = sg.idStudent
                {$sGroup}
                {$sPeriod}
                {$sExcludeList}
              WHERE s.isdeleted = 0
                {$fPartner}
                {$fLevel}
                {$fGroup}
                {$fPeriod}
                {$fStatus}
                {$fDate}

                {$fExcludeList}
                {$groupBy}

                limit {$offset}, {$limit}
               ";

        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        foreach( $arBindParams as $bParam ) {
            $command->bindParam( $bParam['name'], $bParam['value'], $bParam['type'] );
//            echo "Param1: {$bParam['name']} => {$bParam['value']}\n";
        }

//        echo $sql."\n";
//        die;

        $dataReader = $command->query();

        while(($row = $dataReader->read())!==false)
        {
            $data[] = $row;

        }
        return $data;
    }

    public static function insertException( $idStudent, $randomCode ) {
        $sql = "insert into abaenglish_extranet.toundo (idelement, code) values (:idStudent, :randomCode) ";

        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $command->bindParam( ':idStudent', $idStudent, PDO::PARAM_INT );
        $command->bindParam( ':randomCode', $randomCode, PDO::PARAM_STR );
        $command->execute();
    }


}