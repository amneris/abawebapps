<?php

/**
 * This is the model class for table "egroup".
 *
 * The followings are the available columns in table 'egroup':
 * @property integer $id
 * @property integer $idEnterprise
 * @property string $name
 * @property string $created
 * @property string $deleted
 * @property integer $isdeleted
 * @property Enterprise enterprise
 *
 */
class Group extends BaseGroup
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'egroup';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idEnterprise, name', 'required'),
			array('idEnterprise, isdeleted', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>128),
			array('created, deleted', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idEnterprise, name, created, deleted, isdeleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        Yii::import('application.modules.enterprise.models.*');
		return array(
            'enterprise'=>array(self::BELONGS_TO, 'Enterprise', 'idEnterprise'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idEnterprise' => 'Id Enterprise',
			'name' => 'Name',
			'created' => 'Created',
			'deleted' => 'Deleted',
			'isdeleted' => 'Isdeleted',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idEnterprise',$this->idEnterprise);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('deleted',$this->deleted,true);
		$criteria->compare('isdeleted',0);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbExtranet;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Group the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /* Returns a listData object, necessary to populate a select drop-down and many more components. */
    public function getAvailableGroups($idEnterprise = '')
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('isdeleted = 0');

        if($idEnterprise <>'')
        {
            $criteria->addCondition('idEnterprise = :inEnterprise');
            $criteria->params = array(':inEnterprise' => $idEnterprise);
        }
        $criteria->order = 'name ASC';

        $availableGroups = Group::model()->findAll($criteria);
        $availableGroups = CHtml::listData($availableGroups, 'id', 'name');
        return $availableGroups;
    }
}
