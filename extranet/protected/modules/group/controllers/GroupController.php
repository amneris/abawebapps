<?php

class GroupController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout='extranet';
    private $pageLimit = 10;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
        'actions'=>array('index','view','admin','update', 'delete', 'insert', 'refresh', 'ajaxUpdate', 'ajaxUpdateValue', 'AjaxList'),
				'roles'=>array(Role::_ABAMANAGER, Role::_AGENT, Role::_RESPONSIBLE),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionInsert()
	{
    $allOk = false; $errorMsg = ''; $okMsg = GroupModule::t('Group successfully added.');
    $name = Yii::app()->request->getParam('newGroupName');
    $idEnterprise = Utils::getSessionVar('idEnterprise');
    
    $criteria = new CDbCriteria();
    $criteria->addCondition( 't.idEnterprise = :idEnterprise' );
    $criteria->addCondition( 't.name = :name' );
    $criteria->addCondition( 't.isdeleted = 0' );
    $criteria->params = array_merge( $criteria->params, array( ':idEnterprise' => $idEnterprise, ':name' => $name ) );
    $mGroup = Group::model()->find($criteria);
    if ( $mGroup ) {
      $allOk = false;
      $errorMsg = GroupModule::t('Group already exists.');
    } else {
      $mGroup = new Group;
      $mGroup->name = $name;
      $mGroup->idEnterprise = $idEnterprise;
      $allOk = $mGroup->save();
      if ( !$allOk ) $errorMsg = GroupModule::t('Error, something went wrong while trying to save the group.');
    }
    
    echo json_encode(array( 'ok' => $allOk, 'errorMsg' => $errorMsg, 'okMsg' => $okMsg ));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id, $action = 'update')
	{

        $dropDownProvider = Enterprise::model()->findAll(array('order'=>'name'));
        $list = CHtml::listData($dropDownProvider, 'id', 'name');

		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

        if(isset($_POST['name']))
        {
            $model->name = $_POST['name'];
            if($model->save())
                $this->redirect(array('update','id'=>$model->id));
        }

    if ( $action == 'update' )  {
      $this->render('update',array(
        'model'=>$model, 'enterprise'=>$list,
      ));
    } else {
      $html = $this->renderPartial( $action, array(
        'model'=>$model,'dropDownLang'=>$list,
      ), true);
      echo json_encode( array( 'ok' => true, 'html' => $html ) );
    }
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        $model->deleted = date('Y-m-d H:i:s');
        $model->save();

        $this->redirect(array('/group/group/admin'));
    }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Group');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Group('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Group']))
			$model->attributes=$_GET['Group'];

		$this->render('index2',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Group the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Group::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Group $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='group-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    
	/**
	 * 
	 */
	public function actionRefresh()
	{
      $idEnterprise = Utils::getSessionVar('idEnterprise');
      
      $checkedList = Yii::app()->request->getParam('checkedList', '0');
      $checkedList = Utils::adminCheckedList( $checkedList );
      
      $pag = Yii::app()->request->getParam('pag', 1);
      $limit = $this->pageLimit;
      $offset = $limit * ($pag - 1);
      
      $orderBy = Yii::app()->request->getParam('OrderBy', 't.created desc');
      $orderBy = str_replace( array('th', 'GroupList'), '', $orderBy );
      $orderBy = str_replace( '_', ' ', $orderBy );
//      $orderBy = str_replace( 'group name', 'group_name', $orderBy );
      $orderBy = strtolower($orderBy);
//      $orderBy = ( $orderBy == 'created' ) ? 's.created' : $orderBy;
//      $orderBy = ( $orderBy == 'created desc' ) ? 's.created desc' : $orderBy;
//      $orderBy = ( $orderBy == 'name' ) ? 'name, surname' : $orderBy;
//      $orderBy = ( $orderBy == 'name desc' ) ? 'name desc, surname desc' : $orderBy;

      $filters = Yii::app()->request->getParam('filters', array());
      $arFilters = array();
      foreach( $filters as $k=>$filter ) {
        if ( empty($filter['value']) ) continue;
        if ( substr($filter['key'], 0, strlen('filter_') ) != 'filter_' ) continue;
        
        $key = substr($filter['key'], strlen('filter_') );
        $arFilters[$key] = $filter['value'];
      }
      
      $criteria = new CDbCriteria();
      foreach( $arFilters as $key => $value ) $criteria->addSearchCondition($key, $value);
      $criteria->addCondition( 't.idEnterprise = :idEnterprise' );
      $criteria->addCondition( 't.isdeleted = 0' );
      $criteria->addCondition( 'name != \'-\' ' );
      $criteria->params = array_merge( $criteria->params, array( ':idEnterprise' => $idEnterprise ) );
      $count = Group::model()->count($criteria);
      
      $criteria->limit = $limit;
      $criteria->offset = $offset;
      $criteria->order = $orderBy;
      $mlGroups = Group::model()->findAll($criteria);
      
      if ( !empty($limit) ) {
        $maxPag = (int) ($count / $limit);
        if (($count % $limit) > 0) $maxPag = $maxPag + 1;
      } else $maxPag = 1;
      
      $html = $this->renderPartial('list', array( 'mlGroups' => $mlGroups, 'orderBy' => $orderBy, 'pag' => $pag, 'maxPag' => $maxPag, 'checkedList' => $checkedList, 'numGroups' => $count, 'filters' => $arFilters ), true );
      
      echo json_encode( array( 'ok' => 'true', 'html' => $html ) );
	}

  public function actionAjaxList() {
    $mlLanguages = Language::model()->findAll();
    $html = $this->renderPartial('application.modules.group.views.group.index', array('model'=>''),true);
    
    echo json_encode(array( 'ok' => true, 'html' => $html ));
  }
  
  public function actionAjaxUpdate($id) {
    $this->actionUpdate($id, '_groups');
  }
  
  public function actionAjaxUpdateValue() {
    $allOk = false;
    
    $id = Yii::app()->request->getParam('id');
    $key = Yii::app()->request->getParam('key');
    $newvalue = Yii::app()->request->getParam('value');
    
    $model = Group::model()->findByPk($id);
    $model->$key = $newvalue;
    $allOk = $model->save();
//file_put_contents('/tmp/extra.log', "Error Group: ".var_export($model->getErrors(), true), FILE_APPEND);
    
    echo json_encode(array( 'ok' => $allOk ));
  }
  
}
