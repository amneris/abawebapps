<?php
return array(
	'ADD NEW GROUP' => 'AGGIUNGERE NUOVO GRUPPO', 
	'Error adding group.' => 'Errore nell\'aggiunta del gruppo.', 
	'Group successfully added.' => 'Gruppo aggiunto correttamente.', 
	'Group/s' => 'Gruppo/i', 
);
