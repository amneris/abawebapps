<?php
return array(
	'ADD NEW GROUP' => 'ADICIONAR NOVO GRUPO', 
	'Error adding group.' => 'Erro ao adicionar grupo', 
	'Group successfully added.' => 'Grupo adicionado com sucesso.', 
	'Group/s' => 'Grupo/s', 
);
