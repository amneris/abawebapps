<?php
return array(
	'ADD NEW GROUP' => 'AJOUTER NOUVEAU GROUPE', 
	'Error adding group.' => 'Erreur dans l\'ajout du groupe', 
	'Group successfully added.' => 'Groupe ajouté correctement.', 
	'Group/s' => 'Groupe/s', 
);
