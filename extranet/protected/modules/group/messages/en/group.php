<?php
return array(
	'ADD NEW GROUP' => 'ADD NEW GROUP', 
	'Error adding group.' => 'Error while adding group.', 
	'Group successfully added.' => 'Group successfully added.', 
	'Group/s' => 'Group(s)', 
);
