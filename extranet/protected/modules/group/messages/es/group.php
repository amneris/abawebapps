<?php
return array(
	'ADD NEW GROUP' => 'AÑADIR NUEVO GRUPO', 
	'Error adding group.' => 'Error añadiendo grupo.', 
	'Group successfully added.' => 'Grupo añadido correctamente.', 
	'Group/s' => 'Grupo/s', 
);
