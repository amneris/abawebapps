<?php 
  $module = 'group';
?>
<div id="divTabGroup">
<div class="row-fluid">
  <div class="col-lg-12">

    <div class="row-fluid">
      <div class="col-lg-4">
        <div id="divDisplayFiltros_<?php echo $module; ?>" class='divDisplayFiltros'><span class="newRegLabel"><?php echo GroupModule::t('ADD NEW GROUP'); ?> <img id='imgDisplayFiltros_<?php echo $module; ?>' src='<?php echo Yii::app()->baseUrl; ?>/images/less.png' width="11" height="11"></span></div>
      </div>
      <div class="col-lg-8">
        <div><span class="spanMsgList spanMsgAddList" id="msgList_1_<?php echo $module; ?>" style="display:none;"></span></div>
      </div>
    </div>

    <div class="row-fluid">
      <div class="col-lg-12">
        <div class="table-responsive divTableFiltros" id="divTableFiltros_<?php echo $module; ?>">
          <table class="table tableFiltros">
            <tr>
              <td class="tdNewreg">
                <?php echo CHtml::label( CHtml::encode( Yii::t('app','NAME') ) , 'newGroupName'); ?><br>
                <?php echo CHtml::textField('newGroupName', '', array( 'id' => 'newGroupName', 'class' => 'form-control addCamp_'.$module )); ?>
              </td>
              <td class="tdNewreg">
                <button id="addButton_<?php echo $module; ?>" type="button" class="abaButton abaButton-Primary" style="margin-top: 21px; width: 100%;">
                  <?php echo Yii::t('app', 'ADD'); ?>
                </button>
              </td>
            </tr>
          </table>
        </div>
        
      </div>
    </div>
    
    <div class="row-fluid">
        <div class="col-lg-12 divTableList" id="divTableList_<?php echo $module; ?>">
          &nbsp;
        </div>
    </div>
    
    <div class="row-fluid">
      <div class="col-lg-12">
        &nbsp;
      </div>
    </div>

  </div>
</div>

<script>
  var OrderBy_<?php echo $module; ?> = 'created desc';
  var checkedList_<?php echo $module; ?> = '';
  
  $(document).ready( function() {
    
    ABAActivarQueryTooltips();
    
    $('#divDisplayFiltros_<?php echo $module; ?>').click( function() {

        $('#divTableFiltros_<?php echo $module; ?>').parent().parent().toggle();

        if ( $('#divTableFiltros_<?php echo $module; ?>').parent().parent().is(':visible') ) {
            $('#imgDisplayFiltros_<?php echo $module; ?>').attr('src', '<?php echo Yii::app()->baseUrl; ?>/images/less.png');
        } else {
            $('#imgDisplayFiltros_<?php echo $module; ?>').attr('src', '<?php echo Yii::app()->baseUrl; ?>/images/plus.png');
        }

    });
//    $('#divDisplayFiltros_<?php echo $module; ?>').trigger('click');
    
    $('.addCamp_<?php echo $module; ?>').keydown( function (e) {
        if ( e.keyCode == 13 ) $('#addButton_<?php echo $module; ?>').trigger('click');
    });

    $('#addButton_<?php echo $module; ?>').click( function(e) {
        e.preventDefault();
        var module = '<?php echo $module; ?>';

        if ( $('#newGroupName').val() == '' ) {
            alert('<?php echo CJavaScript::quote(Yii::t('app','Name must not be empty.')); ?>');
            $('#newGroupName').focus();
            return false;
        }

        ABAShowLoading();
        var parametros = {
          "newGroupName" : $('#newGroupName').val(),
        };

        $.ajax({
            data:  parametros,
            url: "/"+module+"/"+module+"/insert",
            context: document.body,
            type: 'POST',
            dataType:"json",
            success: function(data) {

                if ( data.ok ) {
                  ABAShowOkMsgList( '<?php echo $module;  ?>', data.okMsg, 1 );
                  $('#newGroupName').val('');
                  
                  ABARefreshList( '<?php echo $module; ?>' );
                } else {
                  ABAShowErrorMsgList( '<?php echo $module;  ?>', data.errorMsg, 1 );
                }

                ABAStopLoading();
            },
            error: function(jqXHR, textStatus, error) {
                alert(jqXHR + ' ' + textStatus + ' ' + error);
                ABAStopLoading();
            },
        }).done(function() {
            //
        });
    });

    ABARefreshList( '<?php echo $module; ?>' );
  });
  
</script>
</div>