<?php
  $module = 'group';

  if ( !isset($pag) ) $pag = 1;
  if ( !isset($maxPag) ) $maxPag = 1;

  if ( $numGroups != 1 || empty($numGroups) ) $tNumberElements = Yii::t('app','Groups');
  else $tNumberElements = Yii::t('app','Group');
  $numElements = $numGroups;

  $displayNewElement = ( $numElements == 0 && empty($filters) );
  $displayNewElement = false;

?>
    
    <div class="table-responsive divTableListInner">
      <table class="table tableList">
      <tr>
        <td colspan="8" class="tdHeaderList">

          <div class="table-responsive">
            <table class="table tableHeaderList">
            <tr>
              <td class='liHeaderTableListTitle' style="border-top:none;"><span class="tableTitle"><?php echo $numElements; ?> <?php echo $tNumberElements; ?></span></td>
              <td class="tdMsgList" style="border-top:none;"><span class="spanMsgList" id="msgList_2_<?php echo $module; ?>" style="display:none;"></span></td>
      <!--        <td class='right listAction'><span id="iconDeleteStudentList"><img id="iconDeleteStudentList" class="logo" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/iconmail_active.png"/></span></td> -->
<!--              <td class='listAction' style2="width:60px;"><span id="iconDeleteList_<?php echo $module; ?>"><img id="iconDeleteList_<?php echo $module; ?>" class="iconLink" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/icontrash_active.png"/></span></td> -->
            </tr>  
            </table>
          </div>
          
        </td>
      </tr>
      <tr>
        <td class="thTable">
          
          <div class="table-responsive">
            <table class="table tableListTH">
            <tr><td>&nbsp;</td></tr>  
            <tr>
              <td><!--<input type="checkbox" id="check_all_<?php echo $module; ?>" data-module='<?php echo $module; ?>' <?php if ($checkedList === 'all') echo "checked"; ?>>--></td>
            </tr>  
            </table>
          </div>
      
        </td>
        <td class="thTable<?php if ( isset($filters['name']) ) echo " active"; ?>" style="border-right: 1px solid #ccc;">
          
          <div class="table-responsive">
            <table class="table tableListTH">
            <tr><td colspan="2"><input type="text" class="inputSearchList tableListFilter" data-module="<?php echo $module; ?>" id="filter_name" value="<?php if ( isset($filters['name']) ) echo $filters['name']; ?>"></td></tr>  
            <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_name">
              <td class="tdTitleTH">
                <span><?php echo Yii::t('app', 'NAME'); ?></span>
                <img class="imgQuery hidden" src="/images/query_grey3.png" data-toggle="tooltip" data-original-title='<?php echo GroupModule::t('Info del nombre'); ?>'>
              </td>
              <?php if ( $orderBy == 'name_desc' || $orderBy == 'surname_desc' ) { ?>
              <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
              <?php } else { ?>
              <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
              <?php } ?>
            </tr>  
            </table>
          </div>
          
        </td>
        <td class="thTable<?php if ( isset($filters['created']) ) echo " active"; ?>">

          <div class="table-responsive">
            <table class="table tableListTH">
            <tr><td colspan="2"><input type="text" class="inputSearchList tableListFilter" data-module="<?php echo $module; ?>" id="filter_created" value="<?php if ( isset($filters['created']) ) echo $filters['created']; ?>"></td></tr>  
            <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_created">
              <td class="tdTitleTH">
                <span><?php echo Yii::t('app', 'CREATED'); ?></span>
                <img class="imgQuery hidden" src="/images/query_grey3.png" data-toggle="tooltip" data-original-title='<?php echo GroupModule::t('Info del apellido'); ?>'>
              </td>
              <?php if ( $orderBy == 'created_desc' ) { ?>
              <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
              <?php } else { ?>
              <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
              <?php } ?>
            </tr>  
            </table>
          </div>
          
        </td>
      </tr>
      
      <?php if ( !empty($mlGroups) ) { ?>
      <?php foreach( $mlGroups as $mGroup ) { ?>
      <tr class="rowList" data-module="<?php echo $module; ?>" data-rowid="<?php echo $mGroup->id; ?>">
        <td class="rowListLeft"><!--<input type="checkbox" data-module='<?php echo $module; ?>' id="check_<?php echo $mGroup->id; ?>" <?php /* if( $mUser->checked ) echo "checked"; */ ?>> --></td>
        <td class="rowListLeft rowListLeftLast tdListLink">
          <table>
          <tr>
            <td><span><?php echo $mGroup->name; ?></span></td>
            <td><div class="divQueryListElement" data-module="<?php echo $module; ?>" data-id="<?php echo $mGroup->id; ?>"><img src="/images/arrow.png" alt="Group info"/></div></td>
          </tr>
          </table>
        </td>
        <td><span><?php echo $mGroup->created; ?></span></td>
      </tr>
      <?php } ?>
      <?php } else { ?>
      <tr class="rowList">
        <td colspan="2" style="background-color: #f3f3f3;"></td>
        <td colspan="1">
          
          <table width="100%" height="100%" style="visibility:hidden;">
            <tr><td style="width: 100%; text-align: center; padding: 2em;"><img src="/images/emptyListBackground.png"></td></tr>
            <tr><td style="width: 100%; text-align: center;"><span style="font-family: 'MuseoSans900', Arial, sans-serif; font-size: 1em;"><?php echo GroupModule::t('No has añadido a ningún alumno todavia'); ?></span></td></tr>
            <tr><td style="width: 100%; text-align: center;"><span style="font-family: 'MuseoSans700', Arial, sans-serif; font-size: 1em;"><?php echo GroupModule::t('Recuerda que puedes añadirlos uno a uno o importarlos todos directamente desde un CSV'); ?></span></td></tr>
          </table>
          
        </td>
      </tr>
      <?php } ?>

      <?php if ( !( $pag == 1 && $maxPag == 1 ) && !empty($mlGroups) ) { ?>
      <tr>
        <td colspan="8" align="right">

          <ul id="paginationList" class="pagination">
              <?php if ( $pag == 1 ) { ?>
                  <li class="disabled"><span>&laquo;</span></li>
              <?php } else { ?>
                  <li><a id="aListPage_<?php echo ($pag-1); ?>" href="#" data-module="<?php echo $module; ?>">&laquo;</a></li>
              <?php } ?>

              <?php for( $i = $pag - 5; $i <= ($pag + 4); $i++ ) { ?>
                  <?php if ( $i <= 0 ) continue; ?>
                  <?php if ( $i > $maxPag ) continue; ?>
                  <?php if ( $i == $pag ) { ?>
                      <li class="active"><span><?php echo $i; ?> <span class="sr-only">(current)</span></span></li>
                  <?php } else { ?>
                      <li><a id="aListPage_<?php echo $i; ?>" href="#" data-module="<?php echo $module; ?>"><?php echo $i; ?></a></li>
                  <?php } ?>
              <?php } ?>

              <?php if ( $pag == $maxPag ) { ?>
                  <li class="disabled"><span>&raquo;</span></li>
              <?php } else { ?>
                  <li><a id="aListPage_<?php echo ($pag+1); ?>" href="#" data-module="<?php echo $module; ?>">&raquo;</a></li>
              <?php } ?>
          </ul>

        </td>
      </tr>
      <?php } ?>
      
      
      </table>
    </div>
    

<script>
var checkedList_<?php echo $module; ?> = '<?php echo CJavaScript::quote($checkedList); ?>';
var pag_<?php echo $module; ?> = '<?php echo $pag; ?>';
var undoSetTimeout_<?php echo $module; ?> = null;
var numElements_<?php echo $module; ?> = '<?php echo $numElements; ?>';
var OrderBy_<?php echo $module; ?> = '<?php echo $orderBy; ?>';

$(document).ready( function() {

    <?php if ( $displayNewElement ) { ?>
    $('#divDisplayFiltros_<?php echo $module; ?>').trigger('click');
    <?php } ?>

    ABAActivarQueryElement();
    ABAActivarFiltros();
    ABAActivarPaginador();
    ABAActivarQueryTooltips();
    ABAActivarOrderBy();
    ABAActivarChecks();


    $('#iconDeleteList_<?php echo $module; ?>').click( function() {
      var module = '<?php echo $module; ?>';
      
      var filters = [];
      $('.tableListFilter').each( function() {
        var module_tmp = $(this).data('module');
        if ( module == module_tmp ) {
          filters.push( { key: this.id, value: $(this).val() } );
        }
      });
        
      var url = '/student/main/delete';
      var parametros = {
        checkedList: checkedList_<?php echo $module; ?>,
        filters: filters,
      };

      var successF = function(data) {
        if ( data.ok ) {
          ABAShowOkMsgList( '<?php echo $module; ?>', data.okMsg );
          ABAActivarUndoList( '<?php echo $module; ?>' );
        } else {
          ABAShowErrorMsgList( '<?php echo $module; ?>', data.errorMsg );
        }

        ABAStopLoading();
      }
      
      ABAShowLoading();
      ABALaunchAjax( url, parametros, successF );

    });

  ABARedrawBody();
});    
    
    
</script>