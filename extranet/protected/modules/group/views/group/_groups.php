<div id="divTabGroup">
<table width="100%">
    <tr>
        <td>
            <span class="sectionTitle"><?php echo $model->name; ?></span>
        </td>
        <td> <!--style="text-align: right; border-left: 1px solid #CCC; width: 40px; padding-right: 10px;"-->
             <!--<a href="<?php /*echo Yii::app()->request->getBaseUrl(true); */?>/group/group/delete/id/<?php /*echo $model->id; */?>""><img class="logo" src="<?php /*echo Yii::app()->request->getBaseUrl(true); */?>/images/icons/delete.png">-->
        </td>
    </tr>
</table>
    <hr class="hrOverTitle">

<div>
    <table class="userTable">
        <tr onmouseover="js:showEditImg('group_name_IMG')" onmouseout="js:hideEditImg('group_name_IMG')">
            <td width="40px" class="paddingTop10">
            </td>
            <td width="250px" class="paddingTop10">
                <span class="formLabel"><?php echo GroupModule::t('Name'); ?></span> <br>
            </td>
            <td class="paddingTop10">

            </td>
        </tr>
        <tr onmouseover="js:showEditImg('group_name_IMG')" onmouseout="js:hideEditImg('group_name_IMG')">
            <td class="paddingBottom10">
               <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px" id="group_name_IMG" style="display: none" class="yeap configTableEditLink">
            </td>
            <td class="paddingBottom10">
                <span class="configTableValue configTableEditLink" id="group_name_SPAN"><?php echo $model->name; ?></span>
            </td>
            <td class="paddingBottom10">
                <button id='group_name_BOT' class='abaButton abaButton-Primary' style="margin-left:40px; display: none"><?php echo GroupModule::t('Aceptar'); ?></button>
            </td>
        </tr>
    </table>
</div>

<div style="text-align: center">
  <button id="btnLinkVolverGroup" class="abaButton abaButton-Primary"><?php echo Yii::t('app','Back'); ?></button>
</div>

<script>
  $(document).ready( function() {
    
    $('#btnLinkVolverGroup').click( function() {
      ABAShowLoading();

      var url = '<?php echo Yii::app()->baseUrl; ?>/group/group/AjaxList';
      var parameters = {};
      var successF = function(data) {
        if ( data.ok ) {
          ABAStopLoading();
          $('#divTabGroup').html(data.html);
        }
      };
      var errorF = function() { console.log('Level Test error'); ABAStopLoading(); };
      var doneF = function() { };

      ABALaunchAjax(url, parameters, successF, errorF, doneF);
    });
    
    $('.configTableEditLink').click( function() {
      var idInput = this.id.replace('SPAN', '');
      var idInput = idInput.replace('IMG', '');
      
      ABAsconfig_Transform( <?php echo $model->id; ?>, idInput, $( '#'+idInput+'SPAN' ).html() );
    });
    
  });
</script>
</div>