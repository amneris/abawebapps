<?php

class PurchaseController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='extranet';
    private $pageLimit = 7;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('index', 'view', 'validate','admin','update', 'delete', 'create', 'refresh', 'undodelete'),
                'roles'=>array(Role::_ABAMANAGER, Role::_AGENT),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('admin'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    public function actionValidate()
    {
        $okMsg = Yii::app()->getModule('purchase')->t('lbl: Purchase validated successfully!');
        $errorMsg = Yii::app()->getModule('purchase')->t('lbl: Purchase validated unsuccessfully!');
        $allOk=false;

        $checkedList = Utils::adminCheckedList();
        if (!empty($checkedList))
        {
            $numLicenciasActivadas = 0;
            $arFilter = Utils::getActualFilters();

            $list = Purchase::getListToValidate($checkedList, $arFilter);
            foreach ($list as $id) {
                $transaction = Yii::app()->dbExtranet->beginTransaction();
                try {
                    $model = $this->loadModel($id);
                    if (!$model) throw new Exception('');
                    $idLicenseType = $model->idLicenseType;
                    $idEnterprise = $model->idEnterprise;
                    $licenses = $model->numLicenses;
                    $discount = $model->discount;

                    $model->validated = 1;
                    $allOk = $model->save();
                    if (!$allOk) throw new Exception('');
                    $numLicenciasActivadas += $licenses;

                    // ** Licencias disponibles.
                    $criteria = new CDbCriteria();
                    $criteria->addCondition('t.idLicenseType = :idLicenseType');
                    $criteria->addCondition('t.idEnterprise = :idEnterprise');
                    $criteria->addCondition('t.discount = :discount');
                    $criteria->params = array(':idEnterprise' => $idEnterprise, ':idLicenseType' => $idLicenseType, ':discount' => $discount);
                    $mLicenseAvailable = LicenseAvailable::model()->find($criteria);
                    if (!$mLicenseAvailable) {
                        $mLicenseAvailable = new LicenseAvailable();
                        $mLicenseAvailable->idLicenseType = $idLicenseType;
                        $mLicenseAvailable->idEnterprise = $idEnterprise;
                        $mLicenseAvailable->numLicenses = 0;
                        $mLicenseAvailable->histLicenses = 0;
                        $mLicenseAvailable->lastupdated = date('Y-m-d H:i:s');
                        $mLicenseAvailable->discount = $discount;
                    }

                    $mLicenseAvailable->numLicenses += $licenses;
                    $mLicenseAvailable->histLicenses += $licenses;
                    $allOk = $mLicenseAvailable->save();
                    if (!$allOk) throw new Exception('');

                    $mEnterprise = Enterprise::model()->findByPk($idEnterprise);
                    if (!$mEnterprise) throw new Exception('');
                    $mLicenseType = LicenseType::model()->findByPk($idLicenseType);

                    $tLicencias = ($licenses > 1) ? Yii::t('app', 'licencias') : Yii::t('app', 'licencia');
                    $html = $this->renderPartial('mailValidateLicensesBody', array(
                        'nombreAgente' => Utils::getSessionVar('name') . ' ' . Utils::getSessionVar('surname'),
                        'numeroLicencias' => $licenses,
                        'licencias' => $tLicencias,
                        'nombreEmpresa' => $mEnterprise->name,
                        'tipoLicencia' => Yii::t('app', $mLicenseType->name),
                    ), true);

                    $arMailsAbaManager = User::getAbaManagerMails();
                    $allOk = HeAbaMail::sendEmail(array(
                        'to' => Utils::getSessionVar('userMail'),
                        'bcc' => $arMailsAbaManager,
                        'subject' => PurchaseModule::t('Licencias validadas'),
                        'msgHtml' => $html,
                        'isHtml' => true,
                    ));
                    if (!$allOk) throw new Exception('');

                    $allOk = true;
                    $transaction->commit();
                } catch (Exception $ex) {
                  $allOk = false;
                  $transaction->rollback();
                  break;
                }
            }
        } else {
          $errorMsg = Yii::app()->getModule('purchase')->t('lbl: No purchase has been selected.');
        }


        if ( $allOk ) {
          if ( $numLicenciasActivadas == 0 || $numLicenciasActivadas > 1 ) {
            $okMsg = Yii::app()->getModule('purchase')->t('lbl: Purchases validated successfully!');
          } else {
            $okMsg = Yii::app()->getModule('purchase')->t('lbl: Purchase validated successfully!');
          }
        }
        echo json_encode( array('ok' => $allOk, 'errorMsg' => $errorMsg, 'okMsg' => $okMsg ) );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model=new Purchase;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Purchase']))
        {
            $model->attributes=$_POST['Purchase'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Purchase']))
        {
            $model->attributes=$_POST['Purchase'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    /*public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }*/

    /**
     *
     */
    /**
     *
     */
    public function actionDelete()
    {
        $allOk = true;
        $errorMsg = '';
        $okMsg = '';

        //$idEnterprise = Utils::getSessionVar('idEnterprise');
        $checkedList = Utils::adminCheckedList();
        $arFilter = Utils::getActualFilters();

        $undoCode = false;
        if (empty($checkedList)) {
            $allOk = false;
            $errorMsg = PurchaseModule::t('lbl: No purchase has been selected.');
        } else {

            $undoCode = Purchase::deleteItem($checkedList, $arFilter);

            $allOk = ($undoCode != false);
            if (!$allOk) $errorMsg = Purchase::getLastErrorMsg();
            else $okMsg = PurchaseModule::t('The selected purchase has been removed. ') . '<a id=\'undoDeleteList_purchase\' data-undocode=\'' . $undoCode . '\' href=\'#\'>' . PurchaseModule::t('Undo') . '</a>';
        }

        echo json_encode( array( 'ok' => $allOk, 'undoCode' => $undoCode, 'errorMsg' => $errorMsg, 'okMsg' => $okMsg ) );
    }

    public function actionUndodelete()
    {
        $errorMsg = '';

        $undoCode = Yii::app()->request->getParam( 'undoCode', 0 );

        $allOk = Purchase::undoDelete( $undoCode );
        if (!$allOk) $errorMsg = Purchase::getLastErrorMsg();

        echo json_encode(array('ok' => $allOk, 'errorMsg' => $errorMsg));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider=new CActiveDataProvider('Purchase');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $idRole = Yii::app()->request->getParam('id', 0);
        if ( $idRole || !Utils::getSessionVar('idEnterprise') ) {
            if ( !Utils::change2Enterprise($idRole) ) {
                $this->redirect( array( Yii::app()->getModule('user')->loginUrl, 'errorMsg' => Yii::t( 'app', 'Forbidden, access denied') ) );
            }
        }

        // ** Te ha de echar si no eres de un rol valido.

        $model=new Purchase('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Purchase']))
            $model->attributes=$_GET['Purchase'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Purchase the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Purchase::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Purchase $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='purchase-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


    /**
     *
     */
    public function actionRefresh()
    {
        //we need all avalaible licences types to populata dropdown filter.
        $licenseTypes = LicenseType::model()->getAvailableLicenseTypes();
        $idEnterprise = Utils::getSessionVar('idEnterprise');
        $checkedList = Yii::app()->request->getParam('checkedList', '0');
        $checkedList = Utils::adminCheckedList($checkedList);
        $pag = Yii::app()->request->getParam('pag', 1);
        $limit = $this->pageLimit;
        $offset = $limit * ($pag - 1);
        $orderBy = Yii::app()->request->getParam('OrderBy', 'created desc');
        $orderBy = str_replace( '_', ' ', $orderBy );
        $filters = Yii::app()->request->getParam('filters', array());

        $arFilters = array();
        foreach( $filters as $k=>$filter )
        {
            if ( empty($filter['value']) )
                continue;
            if ( substr($filter['key'], 0, strlen('filter_') ) != 'filter_' )
                continue;

            $key = substr($filter['key'], strlen('filter_'));
            $arFilters[$key] = $filter['value'];
        }

        $count = Purchase::model()->countPendingPurchaseListItems($idEnterprise, $limit, $offset, $orderBy, $arFilters);
        $mlPurchases = Purchase::model()->getPendingPurchaseList($idEnterprise, $limit, $offset, $orderBy, $arFilters, $checkedList);

        if ( !empty($limit) ) {
            $maxPag = (int) ($count / $limit);
            if (($count % $limit) > 0) $maxPag = $maxPag + 1;
        } else $maxPag = 1;

        $idCountry = Utils::getSessionVar('idCountry', 300);
        if ( $idCountry == Country::_SPAIN ) {
          $taxRates = TaxRates::getTaxRateByCountry( $idCountry );
        } else $taxRates = 1;

        $html = $this->renderPartial('list', array( 'mlPurchases' => $mlPurchases, 'orderBy' => $orderBy, 'pag' => $pag, 'maxPag' => $maxPag, 'checkedList' => $checkedList, 'numPurchases' => $count, 'filters' => $arFilters, 'licenseTypes' => $licenseTypes, 'taxRates' => $taxRates ), true );

        echo json_encode( array( 'ok' => 'true', 'html' => $html ) );
    }



}
