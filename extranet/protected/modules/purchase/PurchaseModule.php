<?php

class PurchaseModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'purchase.models.*',
			'purchase.components.*',
		));
		$path = realpath(Yii::app()->basePath . '/views/layouts');
		$this->setLayoutPath( $path );
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}

	public static function t($str='',$params=array(),$dic='purchase') {
    $translation = Yii::t("PurchaseModule", $str);
    if ( $translation == $str ) $translation = Yii::t("PurchaseModule.".$dic, $str, $params);
    if ( $translation == $str ) $translation = Yii::t("app", $str, $params);

    return $translation;
    
//		if (Yii::t("PurchaseModule", $str)==$str)
//			return Yii::t("PurchaseModule.".$dic, $str, $params);
//		else
//			return Yii::t("PurchaseModule", $str, $params);
	}
}
