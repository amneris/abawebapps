<?php

/**
 * This is the model class for table "purchase".
 *
 * The followings are the available columns in table 'purchase':
 * @property integer $id
 * @property integer $idUser
 * @property integer $idEnterprise
 * @property integer $idLicenseType
 * @property integer $numLicenses
 * @property string $bought
 * @property integer $validated
 * @property double $value
 * @property double $valueLocal
 * @property string $idCurrency
 * @property string $created
 * @property string $deleted
 * @property integer $isdeleted
 */
class BasePurchase extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'purchase';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idUser, idEnterprise, idLicenseType, numLicenses', 'required'),
			array('idUser, idEnterprise, idLicenseType, numLicenses, validated, isdeleted', 'numerical', 'integerOnly'=>true),
			array('value, valueLocal', 'numerical'),
			array('idCurrency', 'length', 'max'=>10),
			array('bought, created, deleted', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idUser, idEnterprise, idLicenseType, numLicenses, bought, validated, value, valueLocal, idCurrency, created, deleted, isdeleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idUser' => 'Id User',
			'idEnterprise' => 'Id Enterprise',
			'idLicenseType' => 'Id License Type',
			'numLicenses' => 'Num Licenses',
			'bought' => 'Bought',
			'validated' => 'Validated',
			'value' => 'Value',
			'valueLocal' => 'Value Local',
			'idCurrency' => 'Id Currency',
			'created' => 'Created',
			'deleted' => 'Deleted',
			'isdeleted' => 'Isdeleted',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('idEnterprise',$this->idEnterprise);
		$criteria->compare('idLicenseType',$this->idLicenseType);
		$criteria->compare('numLicenses',$this->numLicenses);
		$criteria->compare('bought',$this->bought,true);
		$criteria->compare('validated',$this->validated);
		$criteria->compare('value',$this->value);
		$criteria->compare('valueLocal',$this->valueLocal);
		$criteria->compare('idCurrency',$this->idCurrency,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('deleted',$this->deleted,true);
		$criteria->compare('isdeleted',$this->isdeleted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbExtranet;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BasePurchase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
