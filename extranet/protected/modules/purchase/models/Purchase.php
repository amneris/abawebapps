<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 27/10/14
 * Time: 12:51
 */

class Purchase extends BasePurchase
{

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BasePurchase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user'=>array(self::BELONGS_TO, 'User', 'idUser'),
			'enterprise'=>array(self::BELONGS_TO, 'Enterprise', 'idEnterprise'),
			'licenseType'=>array(self::BELONGS_TO, 'LicenseType', 'idLicenseType'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idUser',$this->idUser);
		/*$criteria->compare('idEnterprise',$this->idEnterprise);*/
		$criteria->compare('idEnterprise',Utils::getSessionVar('idEnterprise'));
		$criteria->compare('idLicenseType',$this->idLicenseType);
		$criteria->compare('numLicenses',$this->numLicenses);
		$criteria->compare('bought',$this->bought,true);
		/*$criteria->compare('validated',$this->validated);*/
		$criteria->compare('validated',0);
		$criteria->compare('value',$this->value);
		$criteria->compare('valueLocal',$this->valueLocal);
		$criteria->compare('idCurrency',$this->idCurrency,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('deleted',$this->deleted,true);
		$criteria->compare('isdeleted',$this->isdeleted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => PurchaseModule::t('ID purchase'),
			'idUser' => PurchaseModule::t('User'),
			'idEnterprise' =>  PurchaseModule::t('Company'),
			'idLicenseType' =>  PurchaseModule::t('License type'),
			'numLicenses' =>  PurchaseModule::t('Amount'),
			'bought' =>  PurchaseModule::t('Bought'),
			'validated' =>  PurchaseModule::t('Validated'),
			'value' =>  PurchaseModule::t('lbl: Value'),
			'valueLocal' =>  PurchaseModule::t('lbl: Local value'),
			'idCurrency' =>  PurchaseModule::t('Currency'),
			'created' =>  PurchaseModule::t('Created'),
			'deleted' =>  PurchaseModule::t('Deleted'),
			'isdeleted' =>  PurchaseModule::t('Deleted?'),
		);
	}


    public function getPendingPurchase($idEnterprise)
    {
        $count = $this->countBySql("select count(id) from purchase where idEnterprise=$idEnterprise and validated=0 and isdeleted=0;");
        return $count;
    }

    public static function countPendingPurchaseListItems($idEnterprise, $limit, $offset, $orderBy, $arFilters)
    {
        $sql="SELECT COUNT(*) FROM abaenglish_extranet.purchase p
              INNER JOIN abaenglish_extranet.user u ON p.idUser = u.id
              INNER JOIN abaenglish_extranet.enterprise e ON p.idEnterprise = e.id
              INNER JOIN abaenglish_extranet.license_type l ON p.idLicenseType = l.id
              WHERE p.idEnterprise = ".$idEnterprise." AND p.isdeleted = 0 AND p.validated = false ";

        foreach($arFilters as $key => $value)
        {
            if($key=='bought' || $key=='value' || $key=='valueLocal')
            {
                $sql.=" AND ".$key." like '%".$value."%'";
            }
            else if($key=='name' || $key=='surname')
            {
                $sql.=" AND u.".$key." like '%".$value."%'";
            }
            else
            {
                $sql.=" AND ".$key."=".$value;
            }

        }

        switch($orderBy)
        {
            case 'name':
                $orderBy ="u.name";
                break;
            case 'name desc':
                $orderBy ="u.name desc";
                break;
            case 'surname':
                $orderBy ="u.surname";
                break;
            case 'surname desc':
                $orderBy ="u.surname desc";
                break;
            case 'licenceType':
                $orderBy ="l.name";
                break;
            case 'licenceType desc':
                $orderBy ="l.name desc";
                break;
            default:
                $orderBy = "p.".$orderBy;
        }

        $sql .=";";

        $count = Yii::app()->dbExtranet->createCommand($sql)->queryScalar();
        return $count;
    }

    public static function getPendingPurchaseList($idEnterprise, $limit, $offset, $orderBy, $arFilters, $checkedList)
    {
        $sql="SELECT p.*, u.name, u.surname, u.email, e.name as enterpriseName, l.name as licenseType FROM abaenglish_extranet.purchase p
              INNER JOIN abaenglish_extranet.user u ON p.idUser = u.id
              INNER JOIN abaenglish_extranet.enterprise e ON p.idEnterprise = e.id
              INNER JOIN abaenglish_extranet.license_type l ON p.idLicenseType = l.id
              WHERE p.idEnterprise = ".$idEnterprise." AND p.isdeleted = 0 AND p.validated = false ";

        foreach($arFilters as $key => $value)
        {
            if($key=='bought' || $key=='value' || $key=='valueLocal')
            {
                $sql.=" AND ".$key." like '%".$value."%'";
            }
            else if($key=='name' || $key=='surname')
            {
                $sql.=" AND u.".$key." like '%".$value."%'";
            }
            else
            {
                $sql.=" AND ".$key."=".$value;
            }
        }

        switch($orderBy)
        {
            case 'name':
                $orderBy ="u.name";
                break;
            case 'name desc':
                $orderBy ="u.name desc";
                break;
            case 'surname':
                $orderBy ="u.surname";
                break;
            case 'surname desc':
                $orderBy ="u.surname desc";
                break;
            case 'licenceType':
                $orderBy ="l.name";
                break;
            case 'licenceType desc':
                $orderBy ="l.name desc";
                break;
            default:
                $orderBy = "p.".$orderBy;
        }

        $sql .=" ORDER BY ".$orderBy." LIMIT ".$offset.",".$limit.";";

        $connection = Yii::app()->dbExtranet;
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();

        $purchasesCollection = array();
        $arSelectedItems = array_flip(explode(',', $checkedList));

        while(($row = $dataReader->read())!==false)
        {
            $mPurchase_tmp = new stdClass();
            $mPurchase_tmp->id = $row['id'];
            $mPurchase_tmp->name = $row['name'];
            $mPurchase_tmp->surname = $row['surname'];
            $mPurchase_tmp->enterpriseName = $row['enterpriseName'];
            $mPurchase_tmp->licenseType = $row['licenseType'];
            $mPurchase_tmp->numLicenses = $row['numLicenses'];
            $mPurchase_tmp->created = $row['created'];
            $mPurchase_tmp->value = $row['value'];
            $mPurchase_tmp->valueLocal = $row['valueLocal'];
            $mPurchase_tmp->checked = ( isset($arSelectedItems[$mPurchase_tmp->id]) xor isset($arSelectedItems['all']) );

            $purchasesCollection[] = $mPurchase_tmp;
        }
        return $purchasesCollection;
    }

    public static function deleteItem($checkedList, $arFilters)
    {

        if ( $checkedList == 'all' )
        {
            $excludeList = '';
            $checkedList = '';
        }
        else if ( str_replace('all', '', $checkedList) != $checkedList )
        {
            $excludeList = str_replace('all', '0', $checkedList);
            $checkedList = '';
        }
        else
        {
            $excludeList = '';
        }
        if ( empty($checkedList) )
        {
            $fInclude = '';
        }

        else
        {
            $fInclude = " and p.id in ({$checkedList}) ";
        }

        if ( empty($excludeList) )
        {
            $fExclude = '';
        }
        else
        {
            $fExclude = " and p.id not in ({$excludeList}) ";
        }

        $randomCode = 'zTmp_'.date("YmdHis").'_'.md5( Utils::getSessionVar('idUser').'_'.Utils::getSessionVar('idEnterprise').'_'.microtime(true).'_'.rand(0,100) );
        $sql = "  insert into toundo (idelement, code)
                  select p.id, '{$randomCode}'
                  FROM abaenglish_extranet.purchase p
                  INNER JOIN abaenglish_extranet.user u ON p.idUser = u.id
                  INNER JOIN abaenglish_extranet.enterprise e ON p.idEnterprise = e.id
                  INNER JOIN abaenglish_extranet.license_type l ON p.idLicenseType = l.id
                  WHERE p.idEnterprise = ".Utils::getSessionVar('idEnterprise')." AND p.isdeleted = 0 AND p.validated = false
                      {$fInclude} {$fExclude}
                    ";

        foreach($arFilters as $key => $value)
        {
            if($key=='bought' || $key=='value')
            {
                $sql.=" AND ".$key." like '%".$value."%'";
            }
            else if($key=='name' || $key=='surname')
            {
                $sql.=" AND u.".$key." like '%".$value."%'";
            }

            else
            {
                $sql.=" AND ".$key."=".$value;
            }

        }
        $connection = Yii::app()->dbExtranet;
        $command = $connection->createCommand($sql);
        $command->execute();

        $criteria = new CDbCriteria();
        $criteria->condition = ' id in (select idElement from toundo where code = \''.$randomCode.'\') ';

        $sql = Purchase::model()->updateAll( array(
            'deleted' => date('Y-m-d H:i:s')
        ), $criteria );

        return $randomCode;
    }

    static public function undoDelete( $undoCode )
    {

        $connection = Yii::app()->dbExtranet;

        $sql = "UPDATE purchase as p INNER JOIN (SELECT idElement FROM toundo WHERE code = '{$undoCode}') AS t ON p.id = t.idElement SET deleted=created";
        $command = $connection->createCommand($sql);
        $command->execute();

        return true;
    }

    static public function getListToValidate($checkedList, $arFilters)
    {

        if ($checkedList == 'all')
        {
            $excludeList = '';
            $checkedList = '';
        }
        else if (str_replace('all', '', $checkedList) != $checkedList)
        {
            $excludeList = str_replace('all', '0', $checkedList);
            $checkedList = '';
        }
        else
        {
            $excludeList = '';
        }
        if (empty($checkedList))
        {
            $fInclude = '';
        }
        else
        {
            $fInclude = " and p.id in ({$checkedList}) ";
        }

        if (empty($excludeList))
        {
            $fExclude = '';
        }
        else
        {
            $fExclude = " and p.id not in ({$excludeList}) ";
        }

        $sql = "  SELECT p.id
                  FROM abaenglish_extranet.purchase p
                  INNER JOIN abaenglish_extranet.user u ON p.idUser = u.id
                  INNER JOIN abaenglish_extranet.enterprise e ON p.idEnterprise = e.id
                  INNER JOIN abaenglish_extranet.license_type l ON p.idLicenseType = l.id
                  WHERE p.idEnterprise = " . Utils::getSessionVar('idEnterprise') . " AND p.isdeleted = 0 AND p.validated = false
                      {$fInclude} {$fExclude}
                    ";

        foreach ($arFilters as $key => $value)
        {
            if ($key == 'bought' || $key == 'value')
            {
                $sql .= " AND " . $key . " like '%" . $value . "%'";
            }
            else if ($key == 'name' || $key == 'surname')
            {
                $sql .= " AND u." . $key . " like '%" . $value . "%'";
            }
            else
            {
                $sql .= " AND " . $key . "=" . $value;
            }

        }
        $connection = Yii::app()->dbExtranet;
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();

        return $result;
    }

} 