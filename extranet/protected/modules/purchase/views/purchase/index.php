<?php
/* @var $this PurchaseController */
/* @var $dataProvider CActiveDataProvider */

?>

<h1>Purchases</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
