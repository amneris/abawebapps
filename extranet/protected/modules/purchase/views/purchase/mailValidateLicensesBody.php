<?php $msg = PurchaseModule::t('¡ Gracias {nombreAgente} !<br>Has activado {numeroLicencias} {licencias} del tipo {tipoLicencia} de la empresa {nombreEmpresa} con éxito.', array(
  '{nombreAgente}' => $nombreAgente,
  '{numeroLicencias}' => $numeroLicencias,
  '{licencias}' => $licencias,
  '{nombreEmpresa}' => $nombreEmpresa,
  '{tipoLicencia}' => $tipoLicencia,
)); ?>
<?php echo $msg; ?>