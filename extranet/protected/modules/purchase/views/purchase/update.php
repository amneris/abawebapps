<?php
/* @var $this PurchaseController */
/* @var $model Purchase */
$selectedIndex = 'Prices';
?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>
<div class="row-fluid">
	<div class="col-lg-12 marginTop15">
		<span class="sectionTitle">Update Purchase <?php echo $model->id; ?></span>
		<?php $this->renderPartial('_form', array('model'=>$model)); ?>
	</div>
</div>
