<?php
/* @var $this PurchaseController */
/* @var $data Purchase */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idUser')); ?>:</b>
	<?php echo CHtml::encode($data->idUser); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idEnterprise')); ?>:</b>
	<?php echo CHtml::encode($data->idEnterprise); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idLicenseType')); ?>:</b>
	<?php echo CHtml::encode($data->idLicenseType); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numLicenses')); ?>:</b>
	<?php echo CHtml::encode($data->numLicenses); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bought')); ?>:</b>
	<?php echo CHtml::encode($data->bought); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('validated')); ?>:</b>
	<?php echo CHtml::encode($data->validated); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('value')); ?>:</b>
	<?php echo CHtml::encode($data->value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valueLocal')); ?>:</b>
	<?php echo CHtml::encode($data->valueLocal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCurrency')); ?>:</b>
	<?php echo CHtml::encode($data->idCurrency); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deleted')); ?>:</b>
	<?php echo CHtml::encode($data->deleted); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isdeleted')); ?>:</b>
	<?php echo CHtml::encode($data->isdeleted); ?>
	<br />

	*/ ?>

</div>