<?php
$module = 'purchase';

if ( !isset($pag) ) $pag = 1;
if ( !isset($maxPag) ) $maxPag = 1;

if ( $numPurchases != 1 || empty($numPurchases) ) $tNumberElements = PurchaseModule::t('lbl: Reservas');
else $tNumberElements = PurchaseModule::t('lbl: Reserva');
$numElements = $numPurchases;

$currencySymbol = Utils::getSessionVar('currencySymbol', '€');

$displayNewElement = ( $numElements == 0 && empty($filters) );

?>


<div class="table-responsive divTableListInner">
<table class="table tableList">

<!-- head of table with label activate button and erase button. -->
<tr>
    <td colspan="8" class="tdHeaderList">

        <div class="table-responsive">
            <table class="table tableHeaderList">
                <tr>
                    <td class='liHeaderTableListTitle' style="border-top:none;">
                        <span class="tableTitle"><?php echo $numElements; ?> <?php echo $tNumberElements; ?></span>
                    </td>
                    <td class="tdMsgList" style="border-top:none;">
                        <span class="spanMsgList" id="msgList_2_<?php echo $module; ?>" style="display:none;"></span>
                    </td>

                    <td class='listAction' style="border-top:none;">
                        <button type="button" class="abaButton abaButton-Primary btnHeaderList" id="iconValidatePurchase" style="margin-right: 10px;">
                            <?php echo CHtml::encode( StudentModule::t( 'Activate licenses' ) ); ?>
                        </button>
                    </td>
                    <td class='listAction verticalLineL' style2="width:60px;" style="border-top:none;">
                        <span id="iconDeleteList_<?php echo $module; ?>">
                          <img style="margin-top: 0.8em; margin-right: 10px; margin-left: 10px" id="iconDeleteList_<?php echo $module; ?>" class="iconLink" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/icontrash_active.png"/>
                        </span>
                    </td>
                </tr>
            </table>
        </div>
    </td>
</tr>
<!-- end of head of table with label activate button and erase button. -->

<tr>
    <!-- General checkbox -->
    <td class="thTable filterList">
        <div class="table-responsive">
            <table class="table tableListTH">
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <input type="checkbox" id="check_all_<?php echo $module; ?>" data-module='<?php echo $module; ?>' <?php if ($checkedList === 'all') echo "checked"; ?>>
                    </td>
                </tr>
            </table>
        </div>
    </td>
    <!-- End of General checkbox -->

    <td class="thTable<?php if ( isset($filters['name']) ) echo " active"; ?>">
        <div class="table-responsive">
            <table class="table tableListTH">
                <tr><td colspan="2"><input type="text" class="inputSearchList tableListFilter" data-module="<?php echo $module; ?>" id="filter_name" value="<?php if ( isset($filters['name']) ) echo $filters['name']; ?>"></td></tr>
                <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_name">
                    <td class="tdTitleTH">
                        <span><?php echo Yii::t('app', 'NAME' ); ?></span>
                        <img class="imgQuery hide" src="/images/query_grey3.png" data-toggle="tooltip" data-original-title='<?php echo PurchaseModule::t('Info del nombre'); ?>'>
                    </td>
                    <?php if ( $orderBy == 'name desc' ) { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
                    <?php } else { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
                    <?php } ?>
                </tr>
            </table>
        </div>
    </td>

    <td class="thTable<?php if ( isset($filters['surname']) ) echo " active"; ?>">
        <div class="table-responsive">
            <table class="table tableListTH">
                <tr><td colspan="2"><input type="text" class="inputSearchList tableListFilter" data-module="<?php echo $module; ?>" id="filter_surname" value="<?php if ( isset($filters['surname']) ) echo $filters['surname']; ?>"></td></tr>
                <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_surname">
                    <td class="tdTitleTH">
                        <span><?php echo Yii::t('app', 'SURNAMES' ); ?></span>
                        <img class="imgQuery hide" src="/images/query_grey3.png" data-toggle="tooltip" data-original-title='<?php echo PurchaseModule::t('Info del nombre'); ?>'>
                    </td>
                    <?php if ( $orderBy == 'surname desc' ) { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
                    <?php } else { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
                    <?php } ?>
                </tr>
            </table>
        </div>
    </td>

    <td class="thTable<?php if ( isset($filters['idLicenseType']) ) echo " active"; ?>">
        <?php
        $defaultLicenseType = (isset($filters['idLicenseType'])) ? $filters['idLicenseType'] : 0;
        ?>
        <div class="table-responsive">
            <table class="table tableListTH">
                <tr>
                    <td colspan="2">
                        <?php
                        foreach($licenseTypes as $key => $item)
                        {
                            $licenseTypes[$key]=Yii::t('app',$item);
                        }
                        echo chtml::dropDownList('filter_idLicenseType',$defaultLicenseType, $licenseTypes ,array('empty'=>'--', 'data-module'=>$module, 'class'=>'customSelect tableListFilter'));
                        ?>
                    </td>
                </tr>
                <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_idLicenseType">
                    <td class="tdTitleTH">
                        <span><?php echo PurchaseModule::t( 'LICENSE TYPE' ); ?>&nbsp;</span>
                        <img class="imgQuery hide" src="/images/query_grey3.png" data-toggle="tooltip" data-original-title='<?php echo PurchaseModule::t('Info del nombre'); ?>'>
                    </td>
                    <?php if ( $orderBy == 'idLicenseType desc' ) { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
                    <?php } else { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
                    <?php } ?>
                </tr>
            </table>
        </div>
    </td>

    <td class="thTable<?php if ( isset($filters['numLicenses']) ) echo " active"; ?>">
        <div class="table-responsive">
            <table class="table tableListTH">
                <tr><td colspan="2"><input type="text" class="inputSearchList tableListFilter" data-module="<?php echo $module; ?>" id="filter_numLicenses" value="<?php if ( isset($filters['numLicenses']) ) echo $filters['numLicenses']; ?>"></td></tr>
                <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_numLicenses">
                    <td class="tdTitleTH">
                        <span><?php echo PurchaseModule::t( 'CANTIDAD' ); ?></span>
                        <img class="imgQuery hide" src="/images/query_grey3.png" data-toggle="tooltip" data-original-title='<?php echo PurchaseModule::t('Info del nombre'); ?>'>
                    </td>
                    <?php if ( $orderBy == 'numLicenses desc' ) { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
                    <?php } else { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
                    <?php } ?>
                </tr>
            </table>
        </div>
    </td>

    <td class="thTable<?php if ( isset($filters['bought']) ) echo " active"; ?>">
        <div class="table-responsive">
            <table class="table tableListTH">
                <tr><td colspan="2"><input type="text" class="inputSearchList tableListFilter" data-module="<?php echo $module; ?>" id="filter_bought" value="<?php if ( isset($filters['bought']) ) echo $filters['bought']; ?>"></td></tr>
                <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_bought">
                    <td class="tdTitleTH">
                        <span><?php echo PurchaseModule::t( 'lbl: Fecha de reserva' ); ?></span>
                        <img class="imgQuery hide" src="/images/query_grey3.png" data-toggle="tooltip" data-original-title='<?php echo PurchaseModule::t('Info del nombre'); ?>'>
                    </td>
                    <?php if ( $orderBy == 'bought desc' ) { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
                    <?php } else { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
                    <?php } ?>
                </tr>
            </table>
        </div>

    </td>
    <td class="thTable<?php if ( isset($filters['value']) ) echo " active"; ?>">
        <div class="table-responsive">
            <table class="table tableListTH">
                <tr><td colspan="2"><input type="text" class="inputSearchList tableListFilter" data-module="<?php echo $module; ?>" id="filter_value" value="<?php if ( isset($filters['value']) ) echo $filters['value']; ?>"></td></tr>
                <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_value">
                    <td class="tdTitleTH">
                      <span><?php echo Yii::app()->getModule('purchase')->t( 'lbl: Value' ).' '; ?>
                      <?php if ( Utils::getSessionVar('idCountry') == Country::_SPAIN ) { ?>
                        <sup>*</sup>
                      <?php } ?>
                      </span>
                        <img class="imgQuery hide" src="/images/query_grey3.png" data-toggle="tooltip" data-original-title='<?php echo PurchaseModule::t('Info del nombre'); ?>'>
                    </td>
                    <?php if ( $orderBy == 'value desc' ) { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
                    <?php } else { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
                    <?php } ?>
                </tr>
            </table>
        </div>
    </td>
    <td class="thTable<?php if ( isset($filters['valueLocal']) ) echo " active"; ?>">
      <div class="table-responsive">
        <table class="table tableListTH">
          <tr><td colspan="2"><input type="text" class="inputSearchList tableListFilter" data-module="<?php echo $module; ?>" id="filter_valueLocal" value="<?php if ( isset($filters['valueLocal']) ) echo $filters['valueLocal']; ?>"></td></tr>
          <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_valueLocal">
            <td class="tdTitleTH">
                        <span><?php echo Yii::app()->getModule('purchase')->t( 'lbl: Local value' ).' '; ?>
                          <?php if ( Utils::getSessionVar('idCountry') == Country::_SPAIN ) { ?>
                            <sup>*</sup>
                          <?php } ?>
                        </span>
              <img class="imgQuery hide" src="/images/query_grey3.png" data-toggle="tooltip" data-original-title='<?php echo PurchaseModule::t('Info del nombre'); ?>'>
            </td>
            <?php if ( $orderBy == 'valueLocal desc' ) { ?>
              <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
            <?php } else { ?>
              <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
            <?php } ?>
          </tr>
        </table>
      </div>
    </td>

</tr>

<?php if ( !empty($mlPurchases) ) { ?>
    <?php
    foreach( $mlPurchases as $mPurchase )
    {
        ?>

        <tr class="rowList<?php if( $mPurchase->checked ) echo " highlight" ?>">
            <td class="rowListLeft">
                <input type="checkbox" data-module='<?php echo $module; ?>' id="check_<?php echo $mPurchase->id; ?>" <?php if( $mPurchase->checked ) echo "checked" ?>>
            </td>

            <td class="rowListLeft"><span><?php echo $mPurchase->name; ?></span></td>
            <td class="rowListLeft"><span><?php echo $mPurchase->surname; ?></span></td>

            <td class="rowListLeftLast">
                <table>
                    <tr>
                        <td><span><?php echo Yii::t( 'app', $mPurchase->licenseType); ?></span></td>
                        <td>&nbsp;</td>
                        <!--            <td><div class="divQueryListElement" data-module="<?php echo $module; ?>" data-id="<?php echo $mPurchase->id; ?>"><img src="/images/arrow.png" alt="Group info"/></div></td> -->
                    </tr>
                </table>
            </td>
            <td><span><?php echo $mPurchase->numLicenses; ?></span></td>
            <td><span><?php echo date( "Y-m-d", strtotime($mPurchase->created) ); ?></span></td>
          <td><span><?php echo number_format( $mPurchase->value / $taxRates, 2, ',', '.' ). ' ' . '€'; ?></span></td>
          <td><span><?php echo number_format( $mPurchase->valueLocal / $taxRates, 2, ',', '.' ). ' ' . $currencySymbol; ?></span></td>
        </tr>
    <?php } ?>
<?php } else if ( !empty( $filters ) ) { ?>
  <tr class="rowList">
    <td colspan="3" style="background-color: #f3f3f3;"></td>
    <td colspan="5">

      <table width="100%" height="100%">
        <tr><td style="width: 100%; text-align: center; padding: 2em; padding-bottom: 1em;"><img src="/images/NoSearchStudent.png"></td></tr>
        <tr><td style="width: 100%; text-align: center;"><span style="font-family: 'MuseoSans700', Arial, sans-serif; font-size: 1em; text-transform: uppercase; color: #962428;"><?php echo Yii::app()->getModule('purchase')->t('lbl: No se ha encontrado ninguna reserva'); ?></span></td></tr>
        <tr><td style="width: 100%; text-align: center;"><span style="font-family: 'MuseoSlab500', Arial, sans-serif; font-size: 1em;"><?php echo StudentModule::t('lbl: Por favor, verifica que has escrito correctamente lo que buscas.'); ?></span></td></tr>
        <tr><td><br></td></tr>
      </table>

    </td>
  </tr>
<?php } else { ?>
  <tr class="rowList">
    <td colspan="3" style="background-color: #f3f3f3;"></td>
    <td colspan="5">

      <table width="100%" height="100%">
        <tr><td style="width: 100%; text-align: center; padding: 2em;"><img src="/images/emptyListBackground.png"></td></tr>
        <tr><td style="width: 100%; text-align: center;"><span style="font-family: 'MuseoSans700', Arial, sans-serif; font-size: 1em; text-transform: uppercase; color: #45555f;"><?php echo StudentModule::t('lbl: No hay pendiente ninguna reserva'); ?></span></td></tr>
<!--        <tr><td style="width: 100%; text-align: center;"><span style="font-family: 'MuseoSlab500', Arial, sans-serif; font-size: 1em;"><?php echo StudentModule::t('Recuerda que puedes añadirlos uno a uno o importarlos todos directamente desde un CSV'); ?></span></td></tr> -->
      </table>

    </td>
  </tr>
<?php } ?>

<?php if ( !( $pag == 1 && $maxPag == 1 ) && !empty($mlPurchases)  ) { ?>
    <tr>
        <td colspan="8" align="right">

            <ul id="paginationList" class="pagination">
                <?php if ( $pag == 1 ) { ?>
                    <li class="disabled"><span>&laquo;</span></li>
                <?php } else { ?>
                    <li><a id="aListPage_<?php echo ($pag-1); ?>" href="#" data-module="<?php echo $module; ?>">&laquo;</a></li>
                <?php } ?>

                <?php for( $i = $pag - 5; $i <= ($pag + 4); $i++ ) { ?>
                    <?php if ( $i <= 0 ) continue; ?>
                    <?php if ( $i > $maxPag ) continue; ?>
                    <?php if ( $i == $pag ) { ?>
                        <li class="active"><span><?php echo $i; ?> <span class="sr-only">(current)</span></span></li>
                    <?php } else { ?>
                        <li><a id="aListPage_<?php echo $i; ?>" href="#" data-module="<?php echo $module; ?>"><?php echo $i; ?></a></li>
                    <?php } ?>
                <?php } ?>

                <?php if ( $pag == $maxPag ) { ?>
                    <li class="disabled"><span>&raquo;</span></li>
                <?php } else { ?>
                    <li><a id="aListPage_<?php echo ($pag+1); ?>" href="#" data-module="<?php echo $module; ?>">&raquo;</a></li>
                <?php } ?>
            </ul>

        </td>
    </tr>
<?php }  ?>

</table>
</div>

<div class="row-fluid">
    <hr>
    <?php if ( Utils::getSessionVar('idCountry') == Country::_SPAIN ) { ?>
    <div class="right"><sup>*</sup> <?php echo Yii::app()->getModule('purchase')->t('lbl: Importe sin IVA'); ?></div>
    <?php } ?>
    <button id="btnBack" type="button" class="abaButton abaButton-Primary">
        <img src="/images/arrowleft.png" style="margin-right: 10px; margin-top: -4px;">
        <?php echo Yii::t('app','Back'); ?></button>
</div>

<script>
    var checkedList_<?php echo $module; ?> = '<?php echo CJavaScript::quote($checkedList); ?>';
    var pag_<?php echo $module; ?> = '<?php echo $pag; ?>';
    var undoSetTimeout_<?php echo $module; ?> = null;
    var numElements_<?php echo $module; ?> = '<?php echo $numElements; ?>';
    var OrderBy_<?php echo $module; ?> = '<?php echo $orderBy; ?>';

    $(document).ready( function() {

        $(".rowList").hover(function() {
            if(!$(this).hasClass("highlight"))
            {
                $(this).addClass("mouseOverColor");
            }

        }, function(){
            $(this).removeClass("mouseOverColor");
        });

        $(".filterList").hover(function() {
            if(!$(this).hasClass("highlightFilter"))
            {
                $(this).addClass("highlightFilter");
            }

        }, function(){
            $(this).removeClass("highlightFilter");
        });

        $(":checkbox", this).click(function() {
            var item2 = $(this).closest("tr");
            item2.removeClass("mouseOverColor");
            var selected = item2.hasClass("highlight");
            if(!selected)
            {
                if($(this).attr('id')!='check_all_<?php echo $module; ?>')
                {
                    item2.addClass("highlight");
                }
                $(":checkbox", this).prop('checked', true);
            }
            else
            {
                item2.removeClass("highlight");
                $(":checkbox", this).prop('checked', false);
            }
        });


        $('#btnBack').click(function () {
            var pageURL = '<?php echo Yii::app()->baseUrl.'/'; ?>';
            var parameters = {};

            ABAChangePage(pageURL, parameters);

        });

        <?php if ( $displayNewElement ) { ?>
        $('#divDisplayFiltros_<?php echo $module; ?>').trigger('click');
        <?php } ?>

        ABAActivarQueryElement();
        ABAActivarFiltros();
        ABAActivarPaginador();
        ABAActivarQueryTooltips();
        ABAActivarOrderBy();
        ABAActivarChecks();


        $('#iconDeleteList_<?php echo $module; ?>').click( function()
        {
            if(window.confirm("<?php echo PurchaseModule::t('¿ Está seguro de querer borrar esta/s compra/s ?'); ?>"))
            {
                var module = '<?php echo $module; ?>';

                var filters = [];
                $('.tableListFilter').each( function() {
                    var module_tmp = $(this).data('module');
                    if ( module == module_tmp ) {
                        filters.push( { key: this.id, value: $(this).val() } );
                    }
                });

                var url = '/purchase/purchase/delete';
                var parametros = {
                    checkedList: checkedList_<?php echo $module; ?>,
                    filters: filters
                };

                var successF = function(data)
                {
                    if ( data.ok )
                    {
                        ABAShowOkMsgList( '<?php echo $module; ?>', data.okMsg );
                        ABAActivarUndoList( '<?php echo $module; ?>' );
                    }
                    else
                    {
                        ABAShowErrorMsgList( '<?php echo $module; ?>', data.errorMsg );
                    }

                    ABAStopLoading();
                }

                ABAShowLoading();
                ABALaunchAjax( url, parametros, successF );
            }

        });

        $('#iconValidatePurchase').click( function(e)
        {
            var module = '<?php echo $module; ?>';

            var filters = [];
            $('.tableListFilter').each( function() {
                var module_tmp = $(this).data('module');
                if ( module == module_tmp ) {
                    filters.push( { key: this.id, value: $(this).val() } );
                }
            });

            var url = '/purchase/purchase/validate';
            var parametros = {
                checkedList: checkedList_<?php echo $module; ?>,
                filters: filters
            };

            var successF = function(data) {
              if ( data.ok ) {
                ABAShowOkMsgList( '<?php echo $module; ?>', data.okMsg );
                setTimeout( function() {
                  ABARefreshList( '<?php echo $module; ?>' );
                }, 10000 );
              } else {
                ABAShowErrorMsgList( '<?php echo $module; ?>', data.errorMsg );
              }
              ABAStopLoading();
            }

            ABAShowLoading();
            ABALaunchAjax( url, parametros, successF );

        });

    });


</script>