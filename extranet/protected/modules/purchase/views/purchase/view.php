<?php
/* @var $this PurchaseController */
/* @var $model Purchase */

$selectedIndex = 'Prices';
?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>
<div class="row-fluid">
	<div class="col-lg-12 marginTop15">
        <span class="sectionTitle"><?php echo PurchaseModule::t('ID purchase'); ?> <?php echo $model->id; ?></span>
		<?php $this->widget('zii.widgets.CDetailView', array(
			'data'=>$model,
			'attributes'=>array(
				'id',
                array(
                    'name'=>$model->getAttributeLabel('idUser'),
                    'value'=>$model->user->surname.", ".$model->user->name,
                ),
                array(
                    'name'=>$model->getAttributeLabel('idEnterprise'),
                    'value'=>$model->enterprise->name,
                ),
                array(
                    'name'=>$model->getAttributeLabel('idLicenseType'),
                    'value'=>$model->licenseType->name,
                ),
				'numLicenses',
				'bought',
				'validated',
				'value',
				'valueLocal',
				'idCurrency',
				'created',
				'deleted',
				'isdeleted',
			),
		)); ?>
	</div>
</div>
