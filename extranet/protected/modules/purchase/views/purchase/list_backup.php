<?php
  $module = 'purchase';

  if ( !isset($pag) ) $pag = 1;
  if ( !isset($maxPag) ) $maxPag = 1;

  if ( $numPurchases != 1 || empty($numPurchases) ) $tNumberElements = PurchaseModule::t('Purchases');
  else $tNumberElements = PurchaseModule::t('Purchase');
  $numElements = $numPurchases;

  $displayNewElement = ( $numElements == 0 && empty($filters) && empty($orderBy) );

?>
    
    <div class="table-responsive divTableListInner">
      <table class="table tableList">
      <tr>
        <td colspan="8" class="tdHeaderList">

          <div class="table-responsive">
            <table class="table tableHeaderList">
            <tr>
              <td class='liHeaderTableListTitle'><span class="tableTitle"><?php echo $numElements; ?> <?php echo $tNumberElements; ?></span></td>
              <td class="tdMsgList"><span class="spanMsgList" id="msgList_2_<?php echo $module; ?>" style="display:none;"></span></td>
      <!--        <td class='right listAction'><span id="iconDeleteStudentList"><img id="iconDeleteStudentList" class="logo" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/iconmail_active.png"/></span></td> -->
<!--              <td class='listAction' style2="width:60px;"><span id="iconDeleteList_<?php echo $module; ?>"><img id="iconDeleteList_<?php echo $module; ?>" class="iconLink" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/icontrash_active.png"/></span></td> -->
            </tr>  
            </table>
          </div>
          
        </td>
      </tr>
      <tr>
        <td class="thTable">
          
          <div class="table-responsive">
            <table class="table tableListTH">
            <tr><td>&nbsp;</td></tr>  
            <tr>
              <td><!-- <input type="checkbox" id="check_all_<?php echo $module; ?>" data-module='<?php echo $module; ?>' <?php if ($checkedList === 'all') echo "checked"; ?>> --></td>
            </tr>  
            </table>
          </div>
      
        </td>
        <td class="thTable<?php if ( isset($filters['name']) ) echo " active"; ?>">
          
          <div class="table-responsive">
            <table class="table tableListTH">
            <tr><td colspan="2"><input type="text" class="inputSearchList tableListFilter hide" data-module="<?php echo $module; ?>" id="filter_name" value="<?php if ( isset($filters['name']) ) echo $filters['name']; ?>"></td></tr>  
            <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_name">
              <td class="tdTitleTH">
                <span><?php echo PurchaseModule::t( 'USER' ); ?></span>
                <img class="imgQuery hide" src="/images/query_grey3.png" data-toggle="tooltip" data-original-title='<?php echo PurchaseModule::t('Info del nombre'); ?>'>
              </td>
              <?php if ( $orderBy == 'name_desc' || $orderBy == 'surname_desc' ) { ?>
              <td class="tdTitleOrder hide"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
              <?php } else { ?>
              <td class="tdTitleOrder hide"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
              <?php } ?>
            </tr>  
            </table>
          </div>
          
        </td>
<!--        
        <td class="thTable<?php if ( isset($filters['name']) ) echo " active"; ?>">
          
          <div class="table-responsive">
            <table class="table tableListTH">
            <tr><td colspan="2"><input type="text" class="inputSearchList tableListFilter hide" data-module="<?php echo $module; ?>" id="filter_name" value="<?php if ( isset($filters['name']) ) echo $filters['name']; ?>"></td></tr>  
            <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_name">
              <td class="tdTitleTH">
                <span><?php echo PurchaseModule::t( 'ENTERPRISE' ); ?></span>
                <img class="imgQuery" src="/images/query_grey3.png" data-toggle="tooltip" data-original-title='<?php echo PurchaseModule::t('Info del nombre'); ?>'>
              </td>
              <?php if ( $orderBy == 'name_desc' || $orderBy == 'surname_desc' ) { ?>
              <td class="tdTitleOrder hide"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
              <?php } else { ?>
              <td class="tdTitleOrder hide"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
              <?php } ?>
            </tr>  
            </table>
          </div>
          
        </td>
-->
        <td class="thTable<?php if ( isset($filters['name']) ) echo " active"; ?>">
          
          <div class="table-responsive">
            <table class="table tableListTH">
            <tr><td colspan="2"><input type="text" class="inputSearchList tableListFilter hide" data-module="<?php echo $module; ?>" id="filter_name" value="<?php if ( isset($filters['name']) ) echo $filters['name']; ?>"></td></tr>  
            <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_name">
              <td class="tdTitleTH">
                <span><?php echo PurchaseModule::t( 'LICENSE TYPE' ); ?></span>
                <img class="imgQuery hide" src="/images/query_grey3.png" data-toggle="tooltip" data-original-title='<?php echo PurchaseModule::t('Info del nombre'); ?>'>
              </td>
              <?php if ( $orderBy == 'name_desc' || $orderBy == 'surname_desc' ) { ?>
              <td class="tdTitleOrder hide"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
              <?php } else { ?>
              <td class="tdTitleOrder hide"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
              <?php } ?>
            </tr>  
            </table>
          </div>
          
        </td>
        <td class="thTable<?php if ( isset($filters['name']) ) echo " active"; ?>">
          
          <div class="table-responsive">
            <table class="table tableListTH">
            <tr><td colspan="2"><input type="text" class="inputSearchList tableListFilter hide" data-module="<?php echo $module; ?>" id="filter_name" value="<?php if ( isset($filters['name']) ) echo $filters['name']; ?>"></td></tr>  
            <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_name">
              <td class="tdTitleTH">
                <span><?php echo PurchaseModule::t( 'CANTIDAD' ); ?></span>
                <img class="imgQuery hide" src="/images/query_grey3.png" data-toggle="tooltip" data-original-title='<?php echo PurchaseModule::t('Info del nombre'); ?>'>
              </td>
              <?php if ( $orderBy == 'name_desc' || $orderBy == 'surname_desc' ) { ?>
              <td class="tdTitleOrder hide"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
              <?php } else { ?>
              <td class="tdTitleOrder hide"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
              <?php } ?>
            </tr>  
            </table>
          </div>
          
        </td>
        <td class="thTable<?php if ( isset($filters['name']) ) echo " active"; ?>">
          
          <div class="table-responsive">
            <table class="table tableListTH">
            <tr><td colspan="2"><input type="text" class="inputSearchList tableListFilter hide" data-module="<?php echo $module; ?>" id="filter_name" value="<?php if ( isset($filters['name']) ) echo $filters['name']; ?>"></td></tr>  
            <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_name">
              <td class="tdTitleTH">
                <span><?php echo PurchaseModule::t( 'DATE' ); ?></span>
                <img class="imgQuery hide" src="/images/query_grey3.png" data-toggle="tooltip" data-original-title='<?php echo PurchaseModule::t('Info del nombre'); ?>'>
              </td>
              <?php if ( $orderBy == 'name_desc' || $orderBy == 'surname_desc' ) { ?>
              <td class="tdTitleOrder hide"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
              <?php } else { ?>
              <td class="tdTitleOrder hide"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
              <?php } ?>
            </tr>  
            </table>
          </div>
          
        </td>
        <td class="thTable<?php if ( isset($filters['value']) ) echo " active"; ?>">
          
          <div class="table-responsive">
            <table class="table tableListTH">
            <tr><td colspan="2"><input type="text" class="inputSearchList tableListFilter hide" data-module="<?php echo $module; ?>" id="filter_name" value="<?php if ( isset($filters['name']) ) echo $filters['name']; ?>"></td></tr>  
            <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_name">
              <td class="tdTitleTH">
                <span><?php echo PurchaseModule::t( 'VALUE' ); ?></span>
                <img class="imgQuery hide" src="/images/query_grey3.png" data-toggle="tooltip" data-original-title='<?php echo PurchaseModule::t('Info del nombre'); ?>'>
              </td>
              <?php if ( $orderBy == 'name_desc' || $orderBy == 'surname_desc' ) { ?>
              <td class="tdTitleOrder hide"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
              <?php } else { ?>
              <td class="tdTitleOrder hide"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
              <?php } ?>
            </tr>  
            </table>
          </div>
          
        </td>
        <td class="thTable">

          <div class="table-responsive">
            <table class="table tableListTH">
              <tr><td>&nbsp;</td></tr>
            </table>
          </div>
          
        </td>
      </tr>
      
      <?php if ( !empty($mlPurchases) ) { ?>
      <?php foreach( $mlPurchases as $mPurchase ) { ?>
      <tr class="rowList">
        <td class="rowListLeft"> <!-- <input type="checkbox" data-module='<?php echo $module; ?>' id="check_<?php echo $mPurchase->id; ?>" <?php /* if( $mUser->checked ) echo "checked"; */ ?>> --></td>
        <td class="rowListLeft"><span><?php echo $mPurchase->name; ?></span></td>
        <td class="rowListLeft rowListLeftLast">
          <table>
          <tr>
            <td><span><?php echo Yii::t( 'app', $mPurchase->licenseType); ?></span></td>
            <td>&nbsp;</td>
<!--            <td><div class="divQueryListElement" data-module="<?php echo $module; ?>" data-id="<?php echo $mPurchase->id; ?>"><img src="/images/arrow.png" alt="Group info"/></div></td> -->
          </tr>
          </table>
        </td>
        <td><span><?php echo $mPurchase->numLicenses; ?></span></td>
        <td><span><?php echo date( "Y-m-d", strtotime($mPurchase->created) ); ?></span></td>
        <td><span><?php echo number_format( $mPurchase->value, 2, ',', '.' ) . '€'; ?></span></td>
        <td><img class='iconValidatePurchase iconLink' data-id='<?php echo $mPurchase->id; ?>' src='<?php echo Yii::app()->request->baseUrl.'/images/icons/check.png';?>' alt="<?php echo PurchaseModule::t( 'Validar' ); ?>" title="<?php echo PurchaseModule::t( 'Validar' ); ?>"></td>
      </tr>
      <?php } ?>
      <?php } else { ?>
      <tr class="rowList">
        <td colspan="3" style="background-color: #f3f3f3;"></td>
        <td colspan="4">
          
          <table width="100%" height="100%" style="visibility:hidden;">
            <tr><td style="width: 100%; text-align: center; padding: 2em;"><img src="/images/emptyListBackground.png"></td></tr>
            <tr><td style="width: 100%; text-align: center;"><span style="font-family: 'MuseoSans900', Arial, sans-serif; font-size: 1em;"><?php echo UserModule::t('No tienes pendiente ninguna compra'); ?></span></td></tr>
          </table>
          
        </td>
      </tr>
      <?php } ?>

      <?php if ( !( $pag == 1 && $maxPag == 1 ) && !empty($mlPurchases)  ) { ?>
      <tr>
        <td colspan="8" align="right">

          <ul id="paginationList" class="pagination">
              <?php if ( $pag == 1 ) { ?>
                  <li class="disabled"><span>&laquo;</span></li>
              <?php } else { ?>
                  <li><a id="aListPage_<?php echo ($pag-1); ?>" href="#" data-module="<?php echo $module; ?>">&laquo;</a></li>
              <?php } ?>

              <?php for( $i = $pag - 5; $i <= ($pag + 4); $i++ ) { ?>
                  <?php if ( $i <= 0 ) continue; ?>
                  <?php if ( $i > $maxPag ) continue; ?>
                  <?php if ( $i == $pag ) { ?>
                      <li class="active"><span><?php echo $i; ?> <span class="sr-only">(current)</span></span></li>
                  <?php } else { ?>
                      <li><a id="aListPage_<?php echo $i; ?>" href="#" data-module="<?php echo $module; ?>"><?php echo $i; ?></a></li>
                  <?php } ?>
              <?php } ?>

              <?php if ( $pag == $maxPag ) { ?>
                  <li class="disabled"><span>&raquo;</span></li>
              <?php } else { ?>
                  <li><a id="aListPage_<?php echo ($pag+1); ?>" href="#" data-module="<?php echo $module; ?>">&raquo;</a></li>
              <?php } ?>
          </ul>

        </td>
      </tr>
      <?php } else { ?>
      <tr>
        <td colspan="8"></td>
      </tr>
      <?php } ?>
      
      
      </table>
    </div>
    

<script>
var checkedList_<?php echo $module; ?> = '<?php echo CJavaScript::quote($checkedList); ?>';
var pag_<?php echo $module; ?> = '<?php echo $pag; ?>';
var undoSetTimeout_<?php echo $module; ?> = null;
var numElements_<?php echo $module; ?> = '<?php echo $numElements; ?>';
var OrderBy_<?php echo $module; ?> = '<?php echo $orderBy; ?>';

$(document).ready( function() {

    <?php if ( $displayNewElement ) { ?>
    $('#divDisplayFiltros_<?php echo $module; ?>').trigger('click');
    <?php } ?>

    ABAActivarQueryElement();
    ABAActivarFiltros();
    ABAActivarPaginador();
    ABAActivarQueryTooltips();
    ABAActivarOrderBy();
    ABAActivarChecks();


    $('#iconDeleteList_<?php echo $module; ?>').click( function() {
      var module = '<?php echo $module; ?>';
      
      var filters = [];
      $('.tableListFilter').each( function() {
        var module_tmp = $(this).data('module');
        if ( module == module_tmp ) {
          filters.push( { key: this.id, value: $(this).val() } );
        }
      });
        
      var url = '/student/main/delete';
      var parametros = {
        checkedList: checkedList_<?php echo $module; ?>,
        filters: filters,
      };

      var successF = function(data) {
        if ( data.ok ) {
          ABAShowOkMsgList( '<?php echo $module; ?>', data.okMsg );
          ABAActivarUndoList( '<?php echo $module; ?>' );
        } else {
          ABAShowErrorMsgList( '<?php echo $module; ?>', data.errorMsg );
        }

        ABAStopLoading();
      }
      
      ABAShowLoading();
      ABALaunchAjax( url, parametros, successF );

    });

    $('.iconValidatePurchase').click( function(e) {
      e.preventDefault();
      
      if ( !confirm('<?php echo CJavaScript::quote(PurchaseModule::t('Esta seguro/a de validar esta compra ?')) ?>') ) return false;
      $( this ).unbind( e );  // ** Si finalmente da error, deberiamos reactivar el evento. De momento, supondremos que no damos error.

      var id = $( this ).data( "id" );
  //		var id = $(this).attr('href').split("/");
  //		id = id[id.length-1];

      var parametros = {
        id: id
      };

      $.ajax({
        data:  parametros,
        url: "/purchase/purchase/validate",
        context: document.body,
        type: 'GET',
        dataType:"json",
        success: function(data) {
          if ( data.ok ) {
            ABAShowDialog( data.html );
          } else {
            ABAShowDialog( data.html );
          }
        },
        error: function(jqXHR, textStatus, error) {
  //          alert('error');
        },
      }).done(function() {
        //
      });
      return false;
    });

});    
    
    
</script>