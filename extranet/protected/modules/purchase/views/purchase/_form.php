<?php
/* @var $this PurchaseController */
/* @var $model Purchase */
/* @var $form CActiveForm */
?>
<div class="row-fluid">
	<div class="col-lg-12 marginTop15">
		<div class="form">

		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'purchase-form',
			// Please note: When you enable ajax validation, make sure the corresponding
			// controller action is handling ajax validation correctly.
			// There is a call to performAjaxValidation() commented in generated controller code.
			// See class documentation of CActiveForm for details on this.
			'enableAjaxValidation'=>false,
		)); ?>

			<p class="note">Fields with <span class="required">*</span> are required.</p>

			<?php echo $form->errorSummary($model); ?>

			<div class="row">
				<?php echo $form->labelEx($model,'idUser'); ?>
				<?php echo $form->textField($model,'idUser'); ?>
				<?php echo $form->error($model,'idUser'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'idEnterprise'); ?>
				<?php echo $form->textField($model,'idEnterprise'); ?>
				<?php echo $form->error($model,'idEnterprise'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'idLicenseType'); ?>
				<?php echo $form->textField($model,'idLicenseType'); ?>
				<?php echo $form->error($model,'idLicenseType'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'numLicenses'); ?>
				<?php echo $form->textField($model,'numLicenses'); ?>
				<?php echo $form->error($model,'numLicenses'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'bought'); ?>
				<?php echo $form->textField($model,'bought'); ?>
				<?php echo $form->error($model,'bought'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'validated'); ?>
				<?php echo $form->textField($model,'validated'); ?>
				<?php echo $form->error($model,'validated'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'value'); ?>
				<?php echo $form->textField($model,'value'); ?>
				<?php echo $form->error($model,'value'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'valueLocal'); ?>
				<?php echo $form->textField($model,'valueLocal'); ?>
				<?php echo $form->error($model,'valueLocal'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'idCurrency'); ?>
				<?php echo $form->textField($model,'idCurrency',array('size'=>10,'maxlength'=>10)); ?>
				<?php echo $form->error($model,'idCurrency'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'created'); ?>
				<?php echo $form->textField($model,'created'); ?>
				<?php echo $form->error($model,'created'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'deleted'); ?>
				<?php echo $form->textField($model,'deleted'); ?>
				<?php echo $form->error($model,'deleted'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'isdeleted'); ?>
				<?php echo $form->textField($model,'isdeleted'); ?>
				<?php echo $form->error($model,'isdeleted'); ?>
			</div>

			<div class="row buttons">
				<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
			</div>

		<?php $this->endWidget(); ?>

		</div><!-- form -->
	</div>
</div>