<?php
/* @var $this PurchaseController */
/* @var $model Purchase */
/* @var $form CActiveForm */
?>
<div class="row-fluid">
	<div class="col-lg-12">
		<div class="wide form">

		<?php $form=$this->beginWidget('CActiveForm', array(
			'action'=>Yii::app()->createUrl($this->route),
			'method'=>'get',
		)); ?>

			<div class="row">
				<?php echo $form->label($model,'id'); ?>
				<?php echo $form->textField($model,'id'); ?>
			</div>

			<div class="row">
				<?php echo $form->label($model,'idUser'); ?>
				<?php echo $form->textField($model,'idUser'); ?>
			</div>

			<div class="row">
				<?php echo $form->label($model,'idEnterprise'); ?>
				<?php echo $form->textField($model,'idEnterprise'); ?>
			</div>

			<div class="row">
				<?php echo $form->label($model,'idLicenseType'); ?>
				<?php echo $form->textField($model,'idLicenseType'); ?>
			</div>

			<div class="row">
				<?php echo $form->label($model,'numLicenses'); ?>
				<?php echo $form->textField($model,'numLicenses'); ?>
			</div>

			<div class="row">
				<?php echo $form->label($model,'bought'); ?>
				<?php echo $form->textField($model,'bought'); ?>
			</div>

			<div class="row">
				<?php echo $form->label($model,'validated'); ?>
				<?php echo $form->textField($model,'validated'); ?>
			</div>

			<div class="row">
				<?php echo $form->label($model,'value'); ?>
				<?php echo $form->textField($model,'value'); ?>
			</div>

			<div class="row">
				<?php echo $form->label($model,'valueLocal'); ?>
				<?php echo $form->textField($model,'valueLocal'); ?>
			</div>

			<div class="row">
				<?php echo $form->label($model,'idCurrency'); ?>
				<?php echo $form->textField($model,'idCurrency',array('size'=>10,'maxlength'=>10)); ?>
			</div>

			<div class="row">
				<?php echo $form->label($model,'created'); ?>
				<?php echo $form->textField($model,'created'); ?>
			</div>

			<div class="row">
				<?php echo $form->label($model,'deleted'); ?>
				<?php echo $form->textField($model,'deleted'); ?>
			</div>

			<div class="row">
				<?php echo $form->label($model,'isdeleted'); ?>
				<?php echo $form->textField($model,'isdeleted'); ?>
			</div>

			<div class="row buttons">
				<?php echo CHtml::submitButton('Search'); ?>
			</div>

		<?php $this->endWidget(); ?>

		</div><!-- search-form -->
	</div>
</div>