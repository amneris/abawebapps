<?php

$selectedIndex = 'Prices';
$module = 'purchase';

?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>

<div class="row-fluid">
    <div class="col-lg-12">

        <div class="row-fluid">
            <div class="col-lg-12">
              <div class="divSectionTittle">
                <span class="sectionTitle"><?php echo CHtml::encode( PurchaseModule::t( 'lbl: Reservas') ); ?></span>
              </div>
            </div>
        </div>

        <div class="row-fluid">
            <div class="col-lg-12">
              &nbsp;
            </div>
        </div>

        <div class="row-fluid">
            <div class="col-lg-12" id="divTableList_<?php echo $module; ?>">

                <?php /* $this->renderPartial('list', array( 'mlStudents' => $mlStudents, 'orderBy' => 'name, surname' ) ); */ ?>

            </div>
        </div>

    </div>
</div>

<script>
var selectedIndex = '<?php echo CJavaScript::quote($selectedIndex); ?>';
var OrderBy = 'created desc';
var checkedList = '0';
var checkedList = '';

$(document).ready( function() {

    $('.addCampStudent').keydown( function (e) {
        if ( e.keyCode == 13 ) $('#addButton').trigger('click');
    });

    $('#linkUploadCSV').click(function() {
        $("#inputUploadCSV:hidden").trigger('click');

        return false;
    });

    $('#inputUploadCSV').change( function() {

        if ( !($( "#hidddenIframe" ).length > 0) ) {
            var ifr=$('<iframe/>', {
                id:'hidddenIframe',
                name:'hidddenIframe',
                src:'about:blank',
                style:'display:none',
                load:function(){
                    var content = $( "#hidddenIframe" ).contents().find("html").html();
                    ABAShowDialog( content );
//              console.log( 'Iframe: '+$( "#hidddenIframe" ).contents().find("html").html() );
                }
            });
            $('body').append(ifr);
        }

        ABAShowLoading();
        $('#formUploadCSV').submit();

    });

    $('#divDisplayFiltros').click( function() {

        $('#divFiltrosAlumno').toggle();

        if ( $('#divFiltrosAlumno').is(':visible') ) {
            $('#imgDisplayFiltros').attr('src', '<?php echo Yii::app()->baseUrl; ?>/images/filterMinus.png');
        } else {
            $('#imgDisplayFiltros').attr('src', '<?php echo Yii::app()->baseUrl; ?>/images/filterPlus.png');
        }

    });

    $(".imgQuery").tooltip({
        placement : 'top'
    });

    $('#addButton').click( function(e) {
        e.preventDefault();
        hideGroupLiveSearch();

        if ( $('#newStudentName').val() == '' ) {
            alert('<?php echo CJavaScript::quote(Yii::t('app','Name must not be empty.')); ?>');
            $('#newStudentName').focus();
            return false;
        }

        if ( $('#newStudentSurname').val() == '' ) {
            alert('<?php echo CJavaScript::quote(Yii::t('app','Surname must not be empty.')); ?>');
            $('#newStudentSurname').focus();
            return false;
        }

        if ( $('#newStudentEmail').val() == '' ) {
            alert('<?php echo CJavaScript::quote(Yii::t('app','Email must not be empty.')); ?>');
            $('#newStudentEmail').focus();
            return false;
        }

        if ( !ABAValidateEmail( $('#newStudentEmail').val() ) ) {
            alert('<?php echo CJavaScript::quote(Yii::t('app','Incorrect email.')); ?>');
            $('#newStudentEmail').focus();
            return false;
        }

        if ( $('#newStudentGroup').val() == '' ) {
            alert('<?php echo CJavaScript::quote(Yii::t('app','Group must not be empty.')); ?>');
            $('#newStudentGroup').focus();
            return false;
        }

        ABAShowLoading();
        var parametros = {
            "newStudentName" : $('#newStudentName').val(),
            "newStudentSurname" : $('#newStudentSurname').val(),
            "newStudentEmail" : $('#newStudentEmail').val(),
            "newStudentGroup" : $('#newStudentGroup').val(),
        };

        $.ajax({
            data:  parametros,
            url: "/student/main/insert",
            context: document.body,
            type: 'POST',
            dataType:"json",
            success: function(data) {

                if ( data.ok ) {
                    $('#alertMessage').fadeIn('slow');
                    setTimeout( "$('#alertMessage').fadeOut('slow');" , 3000);

                    $('#newStudentName').val('');
                    $('#newStudentSurname').val('');
                    $('#newStudentEmail').val('');
                    $('#newStudentGroup').val('');

                    refreshList();
                } else {
                    $('#alertMessage4').fadeIn('slow');
                    setTimeout( "$('#alertMessage4').fadeOut('slow');" , 3000);
                }

                ABAStopLoading();
            },
            error: function(jqXHR, textStatus, error) {
                alert(jqXHR + ' ' + textStatus + ' ' + error);
                ABAStopLoading();
            },
        }).done(function() {
            //
        });
    });

    $('#newStudentEmail').change( function(e) {
        if ( !ABAValidateEmail( $(this).val() ) ) {
            e.preventDefault();

            $('#newStudentEmail').focus();
            return false;
        }
    });

    ABARefreshList( '<?php echo $module; ?>' );
    groupLiveSearchTrapOn();
});

function refreshList(newPag, email) {
    if ( typeof newPag == 'undefined' ) newPag = 1;
    ABAShowLoading();

    var filters = [];
    if ( typeof email != 'undefined' ) newPag = 1;
    filters.push( { key: 'filter_email', value: email } );
    $('.studentFilter').each( function() {
        filters.push( { key: this.id, value: $(this).val() } );
    });
    console.log(filters);
    var parametros = {
        OrderBy : OrderBy,
        pag: newPag,
        checkedList: checkedList,
        filters: filters,
    };


    $.ajax({
        data:  parametros,
        url: "/student/main/refresh",
        context: document.body,
        type: 'POST',
        dataType:"json",
        success: function(data) {
            if ( data.ok ) {
                $('#divTableList').html( data.html );
            }
            ABAStopLoading();
        },
        error: function(jqXHR, textStatus, error) {
            alert( jqXHR + ' ' + textStatus + ' ' + error );
            ABAStopLoading();
        },
    }).done(function() {
        //
    });

}

var keyPressTimeout = null;
var keyPressLongTimeout = null;
var lastGroupLiveSearch = '';
function groupLiveSearchTrapOn() {
    $('#newStudentGroup').on('keydown', function(e) {
        if ( keyPressTimeout ) clearTimeout(keyPressTimeout);

        if ( e.keyCode == 9 ) {
            if ( $('#divGroupLiveSearch').html() != '' ) {

                $('#divGroupLiveSearch li').each( function() {
                    if ( $(this).hasClass('active') ) {
                        $('#newStudentGroup').val( $(this).text() );
                        lastGroupLiveSearch = $(this).text();
                        e.preventDefault();
                        return false;
                    }
                });
                return false;
            }
        }

    });


    $('#newStudentGroup').on('keyup', function(e) {
        var value = $('#newStudentGroup').val();

        if ( keyPressTimeout ) clearTimeout(keyPressTimeout);

        // ** Excepciones.
        if (e.keyCode == 27) {
            hideGroupLiveSearch();
            return false;
        }

        if ( value == '' && ( e.keyCode == 8 || e.keyCode == 46 ) ) {
            hideGroupLiveSearch();
            return false;
        }

        if ( value == '' && ( e.keyCode < 65 || e.keyCode > 90 ) ) {
            hideGroupLiveSearch();
            return false;
        }

        if ( e.keyCode == 13 ) {
            $('#divGroupLiveSearch li').each( function() {
                if ( $(this).hasClass('active') ) {
                    $('#newStudentGroup').val( $(this).text() );
                    return false;
                }
            });
            hideGroupLiveSearch();
            return false;
        }

        if ( e.keyCode == 40 || e.keyCode == 38 ) {
            var isActive = 0;
            var contador = 0;
            $('#divGroupLiveSearch li').each( function() {
                contador++;
                if ( $(this).hasClass('active') ) {
                    $(this).removeClass('active');
                    isActive = contador;
                    return false;
                }
            });

            if ( e.keyCode == 40 ) isActive++;
            else if ( e.keyCode == 38 ) isActive--;

            if ( isActive <= 0 ) {
                hideGroupLiveSearch();
                return false;
            } else {
                var contador = 0;
                $('#divGroupLiveSearch li').each( function() {
                    if ( ++contador == isActive ) {
                        $(this).addClass('active');
                        return false;
                    }
                });
            }

        }

        if ( value == lastGroupLiveSearch ) return false;

        lastGroupLiveSearch = value;
        if ( !keyPressLongTimeout ) keyPressLongTimeout = setTimeout( 'doGroupLiveSearch();', 2000 );

        var time = 250;
//        if ( value.length < 3 ) time = 1000;
        keyPressTimeout = setTimeout( 'doGroupLiveSearch();', time );
    });

}

function doGroupLiveSearch() {
    if ( keyPressTimeout ) { clearTimeout(keyPressTimeout); keyPressTimeout = null; }
    if ( keyPressLongTimeout ) { clearTimeout(keyPressLongTimeout); keyPressLongTimeout = null; }

    var parametros = {
        partialWord : $('#newStudentGroup').val(),
    };

    $.ajax({
        data:  parametros,
        url: "/student/main/groupsearch",
        context: document.body,
        type: 'POST',
        dataType:"json",
        success: function(data) {
            if ( data.ok ) {
                if ( data.result.length > 0 ) showLiveSearchResults(data.result);
                else hideGroupLiveSearch();
            }
        },
        error: function(jqXHR, textStatus, error) {
//          alert('error');
        },
    }).done(function() {
        //
    });

}

function showLiveSearchResults( arResults ) {
    var txt = '';

    $.each( arResults, function( index, value ) {
        txt += '<li>'+value+'</li>';
    });
    txt = '<ul>'+txt+'</ul>';

    $('#divGroupLiveSearch').html( txt );
    $('#divGroupLiveSearch li').click( function() {
        $('#newStudentGroup').val( $(this).text() );
        hideGroupLiveSearch();
    });
    $('#divGroupLiveSearch li').mouseover( function() {
        console.log( "Mouse Log: "+$(this).text() );

        $('#divGroupLiveSearch li').removeClass('active');
        $(this).addClass('active');
    });
    $('#divGroupLiveSearch').show();
}

function hideGroupLiveSearch() {
    if ( keyPressTimeout ) { clearTimeout(keyPressTimeout); keyPressTimeout = null; }
    if ( keyPressLongTimeout ) { clearTimeout(keyPressLongTimeout); keyPressLongTimeout = null; }

    $('#divGroupLiveSearch').hide();
    $('#divGroupLiveSearch').html( '' );
}

</script>

<style>
    #inputUploadCSV {
        display:none;
    }
</style>

