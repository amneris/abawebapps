<table height="100%" width='100%'>
<tr>
  <td>
    <div style="font-size: 20px; color:green; text-align: center;">
      <img src="/images/redalert.png" width="48" style="margin: 20px;">
    </div>

    <div style="text-align: center;">
      <span style="font-weight: bold; font-size: 1.3em;"><?php echo PurchaseModule::t('Error!'); ?></span><br>
      <span><?php echo PurchaseModule::t('lbl: Purchase validated unsuccessfully!'); ?></span><br><br>
    </div>

    <div id="divBtnCloseOkLicense">
      <button id="btnCloseOkLicense" class="abaButton abaButton-Primary abaExtranetButton-Ok">
        <?php echo PurchaseModule::t('Close'); ?>
      </button>
    </div>
  </td>
</tr>
<tr><td>&nbsp;</td></tr>
</table>

<script>
  $(document).ready( function() {
    $('#btnCloseOkLicense').click( function() {
      ABACloseDialog();      
    });
  });
</script>  