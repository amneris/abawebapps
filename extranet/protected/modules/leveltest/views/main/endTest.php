<?php
  $idLevel = LeveltestModule::getSessionVar('curLevel');
  $idLevel = ( $idLevel > 1 && !LeveltestModule::getSessionVar('qCorrectAnswerOnActualLevel', false) ) ? $idLevel - 1 : $idLevel;
?>
<script>
  $(document).ready(function ()
  {
      var pageURL = '/leveltest/main/showlevel';
      var parameters = { level: <?php echo $idLevel; ?>  };
      ABAChangePage( pageURL, parameters )
  });
</script>  
