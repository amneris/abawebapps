<table height="100%" width='100%'>
<tr>
  <td>
    <div style="font-size: 20px; color:green; text-align: center;">
      <img src="/images/redalert.png" width="48" style="margin: 20px; width: 3.25em;">
    </div>

    <div style="text-align: center;">
      <span style='font-family: "MuseoSlab500", Arial, Sans-serif; font-weight: bold; font-size: 1.479166667em; color:red;'><?php echo Yii::app()->getModule('leveltest')->t('lbl: No se ha detectado el Flash.'); ?></span><br>
      <span style='font-family: "MuseoSlab700", Arial, Sans-serif; font-size: 1.155em;'><?php echo Yii::app()->getModule('leveltest')->t('lbl: Para completar el test de nivel o el curso, necesitas tener instalada la última versión de Flash Player.'); ?></span><br>
      <span style='font-family: "MuseoSlab700", Arial, Sans-serif; font-size: 1.155em;'><?php echo Yii::app()->getModule('leveltest')->t('lbl: Descarga tu versión de Flash Player desde {link}.', array('{link}' => '<a href="https://www.adobe.com/go/getflash" target="_blank">'.Yii::app()->getModule('leveltest')->t('lbl: aquí').'</a>' ) ); ?></span><br><br>
    </div>

    <div id="divBtnCloseOkLicense">
      <button id="btnCloseOkLicense" class="abaButton abaButton-Primary abaExtranetButton-Ok">
        <?php echo Yii::t('app', 'lbl: Ok'); ?>
      </button>
    </div>
  </td>
</tr>
<tr><td>&nbsp;</td></tr>
</table>

<script>
  $(document).ready( function() {
    $('#btnCloseOkLicense').click( function() {
      abaCore.closeDialog();
    });
  });
</script>