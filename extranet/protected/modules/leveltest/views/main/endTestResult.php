<?php

?>
<link rel="stylesheet" type="text/css" href="/css/leveltest.css" />

<style>
</style>

<div id="divBodyLevelTest" >

    <div id="divHeaderLevelTest2">
        <div id="divTitleLevelTest"><span><?php echo $levelTitle; ?></span></div>
        <div id="divLogoLevelTest"><img src="/images/logo-aba-english.png"></div>
        <div id="divMovieTitleLevetTest2"><span>Ken, ABA Film 138</span></div>
    </div>
    <div id="divTextLevelTest">
        <?php $welcomeText = $testResultString.' '.$levelTitle; ?>
        <div id="divTextLevelTest_welcome"><span><?php echo $welcomeText; ?></span></div>
    </div>

    <div id="divLevelTestMiddle">
        <div class="divLevelTestBloque">
            <div id="divLeveltestAnalisis"></div>
            <div id="divLeveltestABAEnglishTitle" class="divLeveltestMiddleTitle"><span><?php echo $analysisString; ?></span></div>
            <div class="divLeveltestMiddleText"><span><?php echo $analysis; ?></span></div>
            <br>
        </div>
        <div class="divLevelTestBloque">
            <div id="divLeveltestObjetivos"></div>
            <div id="divLeveltestABAEnglishTitle" class="divLeveltestMiddleTitle"><span><?php echo $objectivesString; ?></span></div>
            <div class="divLeveltestMiddleText"><span><?php echo $objectives; ?></span></div>
            <br>
        </div>
        <div class="divLevelTestBloque">
            <div id="divLeveltestABAEnglish"></div>
            <div id="divLeveltestABAEnglishTitle" class="divLeveltestMiddleTitle"><span><?php echo $abaEnglishString; ?></span></div>
            <div id="divLeveltestABAEnglishText" class="divLeveltestMiddleText"><span><?php echo $abaEnglish; ?></span></div>
            <br>
        </div>
    </div>

    <div id="divBtnLevelTest"><button id="btnCloseTest" class="btn btn-primary"><?php echo Yii::app()->getModule('leveltest')->t2('CLOSE'); ?></button></div>

</div>

<script type="text/javascript">
    $(document).ready( function() {
        $('#btnCloseTest').click( function(e) {
            e.preventDefault();
            window.close();
        });
    });
</script>
