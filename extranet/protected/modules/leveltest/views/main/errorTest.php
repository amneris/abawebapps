
<?php 
  
?>
<link rel="stylesheet" type="text/css" href="/css/leveltest.css" />

<div id="divBodyLevelTest" >

  <div id="divHeaderLevelTest">
    <div id="divTitleLevelTest"><span><?php echo Yii::app()->getModule('leveltest')->t2('Level test not found'); ?></span></div>
    <div id="divLogoLevelTest"><img src="/images/logo-aba-english.png"></div>
    <div id="divMovieTitleLevetTest"><span>Oliver & Ken, ABA Film 138</span></div>
  </div>
  
  <div id="divTextLevelTest">
    <div><span><?php echo Yii::app()->getModule('leveltest')->t2('Sorry, we have not found a Level test with this code.'); ?></span></div>
  </div>

</div>
