<?php ?>

<div class="modal-test">

  <div class="title-page cent">
    <h1>Level test!</h1>
  </div>
  
  <div><br></div>
  
  <div class="divTestOptions">
    <?php if ( $mQuestion->type == LevelTestQuestions::_TYPE_TEXT ) { ?>
    <h4><?php echo LeveltestModule::getSessionVar('curQuestion').'.- '.LeveltestModule::t( $mQuestion->question ); ?>&nbsp;&nbsp;<span class="ico-success ok"></span><span class="ico-ko ko"></span></h4>
    <?php } else if ( $mQuestion->type == LevelTestQuestions::_TYPE_AUDIO ) { ?>
    <h4>
      <?php echo LeveltestModule::getSessionVar('curQuestion').'.- ' ?>
      <object type="application/x-shockwave-flash" data="/plugins/dewplayer/dewplayer.swf" width="200" height="20" id="dewplayer" name="dewplayer"> <param name="wmode" value="transparent" /><param name="movie" value="dewplayer.swf" /> <param name="flashvars" value="mp3=/media/leveltest/<?php echo 'audio_'.$mQuestion->id.'.mp3' ?>&amp;showtime=1" /> </object>
      &nbsp;&nbsp;
      <span class="ico-success ok"></span>
      <span class="ico-ko ko"></span>
    </h4>
    <?php } ?>
    <ul style="padding-left: 20px;">
      <li><input id="Opcion1" type="radio" name="radioTestOptions" value="1"><label for="Opcion1"><?php echo LeveltestModule::t( $mQuestion->option1 ); ?></label></li>
      <li><input id="Opcion2" type="radio" name="radioTestOptions" value="2"><label for="Opcion2"><?php echo LeveltestModule::t( $mQuestion->option2 ); ?></label></li>
      <li><input id="Opcion3" type="radio" name="radioTestOptions" value="3"><label for="Opcion3"><?php echo LeveltestModule::t( $mQuestion->option3 ); ?></label></li>
    </ul>
    <?php if ( 1==2 && Yii::app()->user->id == 6 ) { ?>
    <br><br>
    <?php echo "Nivell: ".LeveltestModule::getSessionVar('curLevel'); ?><br>
    <?php echo "Errors: ".LeveltestModule::getSessionVar('error'); ?><br>
    <?php echo "Preguntes: ".LeveltestModule::getSessionVar('curQuestion'); ?><br>
    <?php echo "Preguntes Text: ".LeveltestModule::getSessionVar('qTextDone'); ?><br>
    <?php echo "Preguntes Audio: ".LeveltestModule::getSessionVar('qAudioDone'); ?><br>
    <?php echo "Preguntes fins ara: ".var_export(LeveltestModule::getSessionVar('curQuestionList'), true); ?><br>
    <?php echo "Progress: ".LeveltestModule::getSessionVar('curProgress'); ?><br>
    <?php } ?>
    
    <div>
      <a title="Realizar el test" href="#" id="form_submit" class="btn-blue test">Go <span></span></a>
    </div>
    
    <div aria-valuenow="<?php echo LeveltestModule::getSessionVar('curProgress'); ?>" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="ui-progressbar ui-widget ui-widget-content ui-corner-all" id="progress_bar">
      <div style="width: <?php echo LeveltestModule::getSessionVar('curProgress'); ?>%;" class="ui-progressbar-value ui-widget-header ui-corner-left"></div>
    </div>
  </div>
<!--  
  <div style="position:absolute; bottom: 3em; width: 100%; height: auto;">
    
    <div class="progress" style="margin:20px;">
      <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo LeveltestModule::getSessionVar('curProgress'); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo LeveltestModule::getSessionVar('curProgress'); ?>%;">
        <span class="sr-only"><?php echo LeveltestModule::getSessionVar('curProgress'); ?>% Complete</span>
      </div>
    </div>
    
    <div>
      <button id="btnCloseLevelTest" class="btn btn-default">
        <?php echo LeveltestModule::t('Close'); ?>
      </button>

      <button id="btnNextLevelTest" class="btn btn-primary">
        <?php echo LeveltestModule::t('Next'); ?>
      </button>
    </div>

  </div>
-->    

</div>

<script>
  $(document).ready(function () {    
    $('#form_submit').click( function() {
      if ( nextBtnActive == false ) return false;
      var selectedOption = $('input[name=radioTestOptions]:checked').val();
      if ( typeof selectedOption == 'undefined' ) { console.log('You must select one answer before go to next question.'); return false; }
      nextBtnActive = false;

      var parameters = { answer: selectedOption };
      var numPregunta = '<?php echo LeveltestModule::getSessionVar('curQuestion');  ?>';
      ABAStartLevelTest( numPregunta, parameters );
    });
  });
</script>  



<!--
.table-plans .ok, .ico-success.ok {
    background-position: 0px 0px;
    height: 17px;
    width: 21px;
}
-->