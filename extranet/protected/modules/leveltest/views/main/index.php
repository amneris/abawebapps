<?php

?>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/leveltest.css" />
<link rel="stylesheet" type="text/css" href="<?php echo getenv('CAMPUS_URL'); ?>/themes/ABAenglish/css/leveltest/leveltest.css" />
<script src="<?php echo getenv('CAMPUS_URL'); ?>/themes/ABAenglish/js/leveltest.js"></script>

<div class="row-fluid">
    <div class="col-lg-12">

        <div id="divBodyLevelTest" >

            <div id="divHeaderLevelTest">
                <div id="divTitleLevelTest"><span><?php echo Yii::app()->getModule('leveltest')->t2('Level test'); ?></span></div>
                <div id="divLogoLevelTest"><img src="/images/logo-aba-english.png"></div>
                <div id="divMovieTitleLevetTest"><span>Oliver & Ken, ABA Film 138</span></div>
            </div>

            <div id="divTextLevelTest">
                <?php $welcomeText = Yii::app()->getModule('leveltest')->t('Hello {$nameStudent}, welcome to {$nameEnterprise}\'s english test.'); eval("\$welcomeText = \"$welcomeText\";"); ?>
                <div id="divTextLevelTest_welcome"><span><?php echo $welcomeText; ?></span></div>

                <div><span><?php echo Yii::app()->getModule('leveltest')->t('The test takes approximately 5-10 minutes and requires speakers to do so. The number of test questions is variable depending on the level and automatically adjusts depending on student responses.'); ?></span></div>

                <?php $welcomeText2 = Yii::app()->getModule('leveltest')->t('Your data will be saved and will be visible only by the user {$nameResponsible}.'); eval("\$welcomeText2 = \"$welcomeText2\";"); ?>
                <div><span><?php echo $welcomeText2; ?></span></div>
            </div>

            <div id="divBtnLevelTest"><button id="btnStartTest" class="abaButton abaButton-Primary"><?php echo Yii::app()->getModule('leveltest')->t('Start level test'); ?></button></div>

        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready( function() {
      abaLeveltest.setURLCampus(<?php echo CJavaScript::encode( getenv('CAMPUS_URL') ); ?>);
      abaLeveltest.setCode( <?php echo CJavaScript::encode( $code ); ?> );
      abaLeveltest.setLanguage( <?php echo CJavaScript::encode( $isoLanguage ); ?> );

      $('#btnStartTest').click( function(e) {
        e.preventDefault();
          var parameters = { code: abaLeveltest.getCode(), language: abaLeveltest.getLanguage() }
          abaLeveltest.setCallback(function(code, level, isoLanguage) { extranetLtCallback(code, level, isoLanguage); });
          abaLeveltest.setErrorCallback(function(code, level, data) { extranetLtErrorCallback(code, level, data); });
          abaLeveltest.start( parameters );
      });

      var hasFlash = abaCore.swfFlashCheck();
      if( !hasFlash ) {
        var html = <?php echo CJavaScript::encode( $this->renderPartial('koFlash', array(), true) ); ?>;
        abaCore.showDialog(html, true, '45%');
      }

    });

    function extranetLtCallback(code, level, isoLanguage) {
        console.log('Extranet fi del test, nivell '+level);

        var url = '/leveltest/main/showLevel';
        var parameters = {
            level: level,
            isoLanguage: isoLanguage,
            code: code,
        };

        ABAChangePage(url, parameters, true);
    }

    function extranetLtErrorCallback(code, msg, data) {
        console.log('Error en el test ('+code+').');

        if ( code == -2 ) {
            var url = '/leveltest/main/showLevel';
            var parameters = {
                level: data.level,
                isoLanguage: data.isoLanguage,
            };

            ABAChangePage(url, parameters, true);
        }
    }

</script>