<?php

class LevelTestComunication {
  
  private $db = null;
  private $dbTableName = '';
  private $initialized = false;
  
//      if(self::$db instanceof CDbConnection)
  
  
  public function __construct( $arParameters = array() ) {
    if (!is_array($arParameters) ) throw new Exception (__CLASS__.': Error en la creacion de la clase.');
      // ** Comprobacion de los parametros
    if ( !isset( $arParameters['db'] ) ) $this->db = Yii::app()->db; else $this->db = $arParameters['db'];
    if ( !isset( $arParameters['TableName'] ) ) $this->dbTableName = 'LevelTest_Data'; else $this->dbTableName = $arParameters['TableName'];
    
    $this->initialized = true;
  }

}