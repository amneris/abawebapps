<?php

/**
 * ABA Level Test module
 * 
 * @author Carles Ejarque <cejarque@abaenglish.com> 
 * @link http://www.abaenglish.com/
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @version $Id: LeveltestModule.php 1 2014-11-03
 */
class LeveltestModule extends CWebModule {

  public $defaultController='main';
    // ** Configuracion niveles.
  public static $level = array(
            '1' => array(
              'levelName' => 'Beginner',
              'textQuestions' => 4,
              'audioQuestions' => 0,
              'redirectUrl' => '',
            ),
            '2' => array(
              'levelName' => 'Low intermediate',
              'textQuestions' => 6,
              'audioQuestions' => 2,
              'redirectUrl' => '',
            ),
            '3' => array(
              'levelName' => 'Intermediate',
              'textQuestions' => 6,
              'audioQuestions' => 1,
              'redirectUrl' => '',
            ),
            '4' => array(
              'levelName' => 'Upper intermediate',
              'textQuestions' => 4,
              'audioQuestions' => 1,
              'redirectUrl' => '',
            ),
            '5' => array(
              'levelName' => 'Advanced',
              'textQuestions' => 4,
              'audioQuestions' => 2,
              'redirectUrl' => '',
            ),
          );
    // ** Configuracion de la base de datos.
  public static $db = null;
  public static $tableNameData = null;
  public static $tableNameQuestions = null;
  public static $tableNameLog = null;
  public $callback = 'LevelTestModule::endFunction';
  
  public function init() {
    parent::init();
    
    // import the module-level models and components
    $this->setImport(array(
        'leveltest.components.*',
        'leveltest.models.*',
        'leveltest.models._selfgenerated.*',
    ));

    Yii::app()->setComponents(
            array(
            )
    );

    $path = realpath(Yii::app()->basePath . '/views/layouts');
    $this->setLayoutPath($path);
  }

  /**
   * @param $str
   * @param $params
   * @param $dic
   * @return string
   */
  public static function t($str = '', $params = array(), $dic = 'leveltest') {
    $translation = Yii::t("LeveltestModule", $str);
    if ( $translation == $str ) $translation = Yii::t("LeveltestModule.".$dic, $str, $params);
    if ( $translation == $str ) $translation = Yii::t("app", $str, $params);

    return $translation;
  }

  public function t2($str='',$params=array(),$dic='leveltest') {
    return self::t( $str, $params, $dic );
  }

  /**
   * 
   */
  static public function ressetSession( ) {
    Yii::app()->session['_ABASession_LevelTest'] = new stdClass();
  }
  
  /**
   * 
   */
  static public function getSession( ) {
    if ( !isset(Yii::app()->session['_ABASession_LevelTest']) ) Yii::app()->session['_ABASession_LevelTest'] = new stdClass();
    
    return Yii::app()->session['_ABASession_LevelTest'];
  }
  
  /**
   * 
   */
  static public function setSessionVar( $name, $val ) {
    if ( !isset(Yii::app()->session['_ABASession_LevelTest']) ) Yii::app()->session['_ABASession_LevelTest'] = new stdClass();
    
    Yii::app()->session['_ABASession_LevelTest']->$name = $val;
    
    return $val;
  }
  
  /**
   * 
   */
  static public function getSessionVar( $name, $val = false ) {
    if ( !isset(Yii::app()->session['_ABASession_LevelTest']) ) Yii::app()->session['_ABASession_LevelTest'] = new stdClass();
    
    if ( isset(Yii::app()->session['_ABASession_LevelTest']->$name) ) return Yii::app()->session['_ABASession_LevelTest']->$name;
    else return $val;
    
  }

  /**
   * 
   */
  static public function newLevelTestData() {
    $model = new appCampus\models\leveltest\LevelTestData();
    if ( self::$db ) $model->setDbConnection( self::$db );
    if ( self::$tableNameData ) $model->setTableName( self::$tableNameData );
    
    return $model;
  }
  
  /**
   * 
   */
  static public function newLevelTestQuestions() {
    $model = new LevelTestQuestions();
    if ( self::$db ) $model->setDbConnection( self::$db );
    if ( self::$tableNameData ) $model->setTableName( self::$tableNameQuestions );
    
    return $model;
  }
  
  /**
   * 
   */
  static public function newLevelTestLog() {
    $model = new LevelTestLog();
    if ( self::$db ) $model->setDbConnection( self::$db );
    if ( self::$tableNameData ) $model->setTableName( self::$tableNameLog );
    
    return $model;
  }

  /**
   * 
   */
  static public function levelTestData_find( $criteria ) {
    $model = new LevelTestData();
    if ( self::$db ) $model->setDbConnection( self::$db );
    if ( self::$tableNameData ) $model->setTableName( self::$tableNameData );
    
    return $model->model()->find($criteria);
  }
  
  static public function log( $id, $msg ) {
    $mLog = LeveltestModule::newLevelTestLog();
    $mLog->idLevelTest = $id;
    $mLog->jsonData = json_encode( $msg );
    $mLog->save();
  }

  static public function endFunction( ) {
  }
  
}
