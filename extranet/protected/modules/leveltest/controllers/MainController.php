<?php

class MainController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'leveltest';

    function init()
    {
        parent::init();
        Yii::app()->language = LeveltestModule::getSessionVar('language', Yii::app()->language);
    }

    /**
     *
     */
    public function actionIndex() {
        LeveltestModule::ressetSession( );
        $code = Yii::app()->request->getParam('code', 0);

        $allOk = true;
        if ( $allOk ) {
            $criteria = new CDbCriteria();
            $criteria->addCondition( 't.code = :code' );
            $criteria->addCondition( 't.isdeleted = :isdeleted' );
            $criteria->params = array_merge( $criteria->params, array( ':code' => $code, ':isdeleted' => 0 ) );
            $allOk = $mStudentLevelTest = StudentLevelTest::model()->find($criteria);
        }

        if ( $allOk ) {
            $criteria = new CDbCriteria();
            $criteria->addCondition( 't.id >= :idStudent' );
            $criteria->addCondition( 't.isdeleted = :isdeleted' );
            $criteria->params = array_merge( $criteria->params, array( ':idStudent' => $mStudentLevelTest->idStudent, ':isdeleted' => 0 ) );
            $allOk = $mStudent = Student::model()->find($criteria);
        }

        if ( $allOk ) {
            $criteria = new CDbCriteria();
            $criteria->addCondition( 't.id >= :idEnterprise' );
            $criteria->addCondition( 't.isdeleted = :isdeleted' );
            $criteria->params = array_merge( $criteria->params, array( ':idEnterprise' => $mStudent->idEnterprise, ':isdeleted' => 0 ) );
            $allOk = $mEnterprise = Enterprise::model()->find($criteria);

            $criteria = new CDbCriteria();
            $criteria->addCondition( 't.id >= :idLanguage' );
            $criteria->params = array_merge( $criteria->params, array( ':idLanguage' => $mEnterprise->idLanguage ) );
            $mLanguage = Language::model()->find($criteria);

            Yii::app()->language = $isoLanguage = $mLanguage->iso;
        }

        if ( $allOk ) {
            $criteria = new CDbCriteria();
            $criteria->addCondition( 't.id >= :idUser' );
            $criteria->addCondition( 't.isdeleted = :isdeleted' );
            $criteria->params = array_merge( $criteria->params, array( ':idUser' => $mEnterprise->idMainContact, ':isdeleted' => 0 ) );
            $allOk = $mUser = User::model()->find($criteria);
        }

        if ( $allOk ) {
            $this->render('index', array(
              'nameStudent' => $mStudent->name,
              'nameEnterprise' => $mEnterprise->name,
              'nameResponsible' => $mUser->name . ' ' . $mUser->surname,
              'code' => $code,
              'isoLanguage' => $isoLanguage,
            ));
        } else {
            $this->render('errorTest', array(
              'code' => $code,
              'isoLanguage' => Yii::app()->language,
            ));
        }

    }
    public static function actualizeLevelTest($idLevel, $code)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.code = :code');
        $criteria->params = array(':code' => $code);
        $mStudentLevelTest = StudentLevelTest::model()->find($criteria);

        $mStudent = Student::model()->findByPk( $mStudentLevelTest->idStudent );
        if($mStudent->idLevel < $idLevel)
        {
            $mStudent->idLevel = $idLevel;
            $mStudent->save();
        }
    }

    public function actionShowLevel($idLevel = 0)
    {
        /* Current level result */
        $idLevel = Yii::app()->request->getParam('level', $idLevel);
        $isoLanguage = Yii::app()->request->getParam('isoLanguage', Yii::app()->language);
        Yii::app()->language = $isoLanguage;        // ** Activamos el idioma de cada usuario en el momento de mostrar los resultados.
        $code = Yii::app()->request->getParam('code', '');
        if ($code != ""){
            $this->actualizeLevelTest($idLevel, $code);
        }
        /* Common text for all results */
        $testResultString = Yii::app()->getModule('leveltest')->t2('Test result:');
        $analysisString = Yii::app()->getModule('leveltest')->t2('Analysis');
        $objectivesString = Yii::app()->getModule('leveltest')->t2('Objectives');
        $abaEnglishString = Yii::app()->getModule('leveltest')->t2('ABA English');

        switch ($idLevel)
        {
            case 1:
                $levelTitle = Yii::app()->getModule('leveltest')->t2('Beginners');
                $analysis = Yii::app()->getModule('leveltest')->t2('You can understand simple phrases and expressions.');
                $objectives = Yii::app()->getModule('leveltest')->t2('ABA English advises you to reach the Lower Intermediate level in 3 months! Upon completion of the BEGINNERS level you will be more confident in everyday conversations by improving your grammar skills, reinforcing your vocabulary and above all, by practising from the very start!');
                $abaEnglish = Yii::app()->getModule('leveltest')->t2('The ABA English course consists of 6 levels which will take your English from the Beginners (A1) * level to Business (C1)*. In each level you will find 24 units. Our course is available 24 hours a day throughout the duration of your subscription.');
                break;
            case 2:
                $levelTitle = Yii::app()->getModule('leveltest')->t2('Lower intermediate');
                $analysis = Yii::app()->getModule('leveltest')->t2('You can understand simple phrases and expressions.');
                $objectives = Yii::app()->getModule('leveltest')->t2('ABA English advises you to reach the Lower Intermediate level in 3 months! Upon completion of the LOWER INTERMEDIATE level you will be able to communicate with simple phrases related to everyday situations and speak using simple language in an English speaking country, all this by improving your grammar skills, reinforcing your vocabulary and, above all, by practising from the start!');
                $abaEnglish = Yii::app()->getModule('leveltest')->t2('The ABA English course consists of 6 levels which will take your English from the Beginners (A1) * level to Business (C1)*. In each level you will find 24 units. Our course is available 24 hours a day throughout the duration of your subscription.');
                break;
            case 3:
                $levelTitle = Yii::app()->getModule('leveltest')->t2('Intermediate');
                $analysis = Yii::app()->getModule('leveltest')->t2('You have proved that you already have some experience in English.');
                $objectives = Yii::app()->getModule('leveltest')->t2('ABA English advises you to reach the Upper Intermediate level in 3 months! Upon completion of the INTERMEDIATE level you will be able to describe situations and events, write texts and speak coherently, all this by improving your grammar skills, reinforcing your vocabulary and, above all, by practising from the very start!');
                $abaEnglish = Yii::app()->getModule('leveltest')->t2('The ABA English course consists of 6 levels which will take your English from the Beginners (A1) * level to Business (C1)*. In each level you will find 24 units. Our course is available 24 hours a day throughout the duration of your subscription.');
                break;
            case 4:
                $levelTitle = Yii::app()->getModule('leveltest')->t2('Upper intermediate');
                $analysis = Yii::app()->getModule('leveltest')->t2('You have proved to have a good level of English.');
                $objectives = Yii::app()->getModule('leveltest')->t2('ABA English advises you to reach the Advanced level in 3 months! Upon completion of the UPPER INTERMEDIATE level you will be able to understand the key points of complex texts by yourself and to create an impromptu speech, all this by improving your grammar skills, reinforcing your vocabulary and, above all, by practising from the very start!');
                $abaEnglish = Yii::app()->getModule('leveltest')->t2('The ABA English course consists of 6 levels which will take your English from the Beginners (A1) * level to Business (C1)*. In each level you will find 24 units. Our course is available 24 hours a day throughout the duration of your subscription.');
                break;
            case 5:
                $levelTitle = Yii::app()->getModule('leveltest')->t2('Advanced');
                $analysis = Yii::app()->getModule('leveltest')->t2('You have proved to have a high level of English.');
                $objectives = Yii::app()->getModule('leveltest')->t2('ABA English advises you to reach the Business level in 3 months! Upon completion of the ADVANCED level you will be able to understand complex texts and communicate fluently in English with native speakers, all this by improving your grammar skills, reinforcing your vocabulary and, above all, by practising from the very start!');
                $abaEnglish = Yii::app()->getModule('leveltest')->t2('The ABA English course consists of 6 levels which will take your English from the Beginners (A1) * level to Business (C1)*. In each level you will find 24 units. Our course is available 24 hours a day throughout the duration of your subscription.');
                break;
            case 6:
                $levelTitle = Yii::app()->getModule('leveltest')->t2('Business');
                $analysis = Yii::app()->getModule('leveltest')->t2('You have proved to have a high level of English.');
                $objectives = Yii::app()->getModule('leveltest')->t2('ABA English gives you the opportunity to perfect your level and study Business English in depth! Upon completion of the BUSINESS level you will understand complex texts and communicate fluently in conversations related to the business world, all this by improving your grammar skills, reinforcing your vocabulary and, above all, by practising from very start!');
                $abaEnglish = Yii::app()->getModule('leveltest')->t2('The ABA English course consists of 6 levels which will take your English from the Beginners (A1) * level to Business (C1)*. In each level you will find 24 units. Our course is available 24 hours a day throughout the duration of your subscription.');
                break;
        }

        $this->render('endTestResult', array(   'testResultString'=>$testResultString,
            'levelTitle'=>$levelTitle,
            'analysisString'=>$analysisString,
            'analysis'=>$analysis,
            'objectivesString'=>$objectivesString,
            'objectives'=>$objectives,
            'abaEnglishString'=>$abaEnglishString,
            'abaEnglish'=>$abaEnglish
        ));

    }
}
