<?php
return array(
	'Analysis' => '分析', 
	'CLOSE' => '关闭', 
	'ABA English advises you to reach the Lower Intermediate level in 3 months! Upon completion of the BEGINNERS level you will be more confident in everyday conversations by improving your grammar skills, reinforcing your vocabulary and above all, by practising from the very start!' => 'ABA English 建议你三个月内达到初中级水平(Lower Intermediate level) !  
当你完成初级水平(BEGINNERS) 后，通过提高语法技巧、词汇强化且最重要的是从初级练起, 你会在日常表达中变得更加自信。', 
	'The ABA English course consists of 6 levels which will take your English from the Beginners (A1) * level to Business (C1)*. In each level you will find 24 units. Our course is available 24 hours a day throughout the duration of your subscription.' => 'ABA English 英语课程共包含6个级别，从入门级别(Beginners A1*)到商务级别  (Business C1*), 每个级别设置24个单元。在您订阅期内，每天24小时随时随地均可进行课程学习.', 
	'The test takes approximately 5-10 minutes and requires speakers to do so. The number of test questions is variable depending on the level and automatically adjusts depending on student responses.' => '测试时长为<b>5-10分钟 </b>且过程中会使用到扬声器。根据答题者对不同等级问题的作答, 测试题目数量会做自动调整。', 
	'Start level test' => '开始英语水平测试', 
	'You have proved that you already have some experience in English.' => '你已证明具备一些英语学习经验。', 
	'You have proved to have a good level of English.' => '你已证明拥有良好的英语水平。', 
	'You have proved to have a high level of English.' => '你已证明拥有较高水平英语。', 
	'You can understand simple phrases and expressions.' => '你已证明有一定英语基础。', 
	'Hello {$nameStudent}, welcome to {$nameEnterprise}\'s english test.' => '您好{$nameStudent}, 欢迎来到 {$nameEnterprise}英语水平测试.', 
	'Go to result' => '查看结果', 
	'Objectives' => '目标', 
	'Test result:' => '测试结果:', 
	'Next' => '下一个', 
	'Level test' => '水平测试', 
	'Finished test.' => '完成的测试', 
	'Your data will be saved and will be visible only by the user {$nameResponsible}.' => '您的答题结果会被自动保存且只由 <b>{$nameResponsible}</b>可见。', 
	'ABA English advises you to reach the Lower Intermediate level in 3 months! Upon completion of the LOWER INTERMEDIATE level you will be able to communicate with simple phrases related to everyday situations and speak using simple language in an English speaking country, all this by improving your grammar skills, reinforcing your vocabulary and, above all, by practising from the start!' => 'ABA English 建议你在三个月内英文达到中级(Intermediate)水平！ 当你完成初中级(LOWER INTERMEDIATE)水平后，通过对语法的提高、词汇的加强和英语一点一滴练习，你能够用简单的语句在英语国家进行日常生活场景交流。', 
	'ABA English advises you to reach the Upper Intermediate level in 3 months! Upon completion of the INTERMEDIATE level you will be able to describe situations and events, write texts and speak coherently, all this by improving your grammar skills, reinforcing your vocabulary and, above all, by practising from the very start!' => 'ABA English 建议你3个月内达到中高级水平(Upper Intermediate)! 
当你完成中级水平后，通过语法的提高和词汇的加强和每天一点一滴的英语练习，你会有能力对场景和活动进行描述，拥有一定的写作能力和流利的口语表达。 ', 
	'ABA English advises you to reach the Advanced level in 3 months! Upon completion of the UPPER INTERMEDIATE level you will be able to understand the key points of complex texts by yourself and to create an impromptu speech, all this by improving your grammar skills, reinforcing your vocabulary and, above all, by practising from the very start!' => 'ABA English 建议你3个月内达到高级水平(Advanced)！
当你完成中高级水平 (UPPER INTERMEDIATE)后，通过对语法的提高、词汇的加强和每天一点一滴的英语练习，你自己有能力理解复杂文本中的关键点并可以进行即兴演讲。', 
	'ABA English advises you to reach the Business level in 3 months! Upon completion of the ADVANCED level you will be able to understand complex texts and communicate fluently in English with native speakers, all this by improving your grammar skills, reinforcing your vocabulary and, above all, by practising from the very start!' => 'ABA English 建议你3个月内达到商务水平(Business level)! 
当你完成英语高级水平（ADVANCED）后，通过对语法技巧的提高、词汇的加强和每天一点一滴的英语练习，你能够完全理解复杂文本同英语为母语人士流利交流。', 
	'ABA English gives you the opportunity to perfect your level and study Business English in depth! Upon completion of the BUSINESS level you will understand complex texts and communicate fluently in conversations related to the business world, all this by improving your grammar skills, reinforcing your vocabulary and, above all, by practising from very start!' => 'ABA English 给你深度学习商务英语的机会，让你的英语变完美！当完成商务级别（BUSINESS）后，通过对语法的加强、词汇的巩固和每天不懈的努力练习，你能够读懂复杂文本并且在商务环境下流利交流。', 
	'lbl: No se ha detectado el Flash.' => '系统未检测出 Adobe Flash 播放器', 
	'lbl: Para completar el test de nivel o el curso, necesitas tener instalada la última versión de Flash Player.' => '进行英语级别测验，你需要安装最新版本的 Adobe Flash 播放器.', 
	'lbl: Descarga tu versión de Flash Player desde {link}.' => '下载 Adobe Flash 播放器 {link}.', 
	'lbl: aquí' => '这里', 
);
