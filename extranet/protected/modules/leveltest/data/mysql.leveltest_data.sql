-- Tabla de datos de los test de nivel.

CREATE TABLE LevelTest_Data (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,

    code varchar(255) NOT NULL,                 -- Codigo de acceso.
    status integer default 1 NOT NULL,          -- 1: Nuevo, 2: Empezado, 3: Acabado.
    spentTime integer default 0 NOT NULL,       -- Tiempo transcurrido.
    jsonData varchar(2000) default '' NOT NULL,

    idLevel integer default 1 not null,         -- Nivel actual.
    numErrors integer default 0 not null,       -- Numero de errores cometidos.
    numQuestions integer default 0 not null,    -- Numero de preguntas respondidas.
    language VARCHAR(2) NOT NULL,               -- Language

    completed datetime default '2000-01-01 00:00:00' NOT NULL,
    expirationDate datetime default '2000-01-01 00:00:00' NOT NULL,

    created DATETIME DEFAULT '2000-01-01 00:00:00' NOT NULL,
    deleted DATETIME DEFAULT '2000-01-01 00:00:00' NOT NULL,
    isdeleted INT DEFAULT 0 NOT NULL
);

CREATE TRIGGER LevelTest_Data_insert BEFORE INSERT ON LevelTest_Data
FOR EACH ROW BEGIN
-- Set the creation date
SET new.created = now();

-- Set the deleted date
Set new.deleted = now();

-- Set the expiration date.
if ( new.expirationDate = '2000-01-01 00:00:00' ) then
  set new.expirationDate = adddate(now(), 30);
end if;

END;

CREATE TRIGGER LevelTest_Data_update BEFORE UPDATE ON LevelTest_Data
FOR EACH ROW BEGIN

  if (NEW.created != NEW.deleted) THEN
    set new.isdeleted = 1;
  else 
    set new.isdeleted = 0;
  end if;

  -- El test ya esta empezado.
  if ( new.spentTime > 0 ) then
    if ( new.status = 1 ) then
      set new.status = 2;
    end if;
  end if;

END;


-- Alter table LevelTest_Data add column language VARCHAR(2) DEFAULT 'en' NOT NULL after numQuestions;