-- Tabla de log de los test de nivel.

CREATE TABLE LevelTest_Log (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,

    idLevelTest integer NOT NULL,
    jsonData varchar(2000) NOT NULL,

    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

