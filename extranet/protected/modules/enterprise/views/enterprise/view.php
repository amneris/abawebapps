<?php
/* @var $this EnterpriseController */
/* @var $model Enterprise */
$selectedIndex = 'Configuracion';
?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>
<div class="row-fluid">
    <div class="col-lg-12 marginTop15">
        <span class="sectionTitle">View Enterprise <?php echo $model->name; ?></span>
        <?php $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                'id',
                'name',
                array(
                    'name'=>$model->getAttributeLabel('idMainContact'),
                    'value'=>$model->mainContact->surname.", ".$model->mainContact->name,
                ),
                'lastupdated',
                'created',
                'deleted',
                'isdeleted',
            ),
        )); ?>
    </div>
</div>