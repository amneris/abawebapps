<?php
/* @var $this EnterpriseController */
/* @var $model Enterprise */

$selectedIndex = 'Configuracion';

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#enterprise-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php /*$this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>
<div class="row-fluid">
    <div class="col-lg-12 marginTop15">
        <span class="sectionTitle">Manage Enterprises</span>
	    <br>
        <?php echo CHtml::link('Create new enterprise',array('enterprise/create')); ?>
        <br>

        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'enterprise-grid',
            'dataProvider'=>$dataProvider,
            'selectableRows' => 1,
            'cssFile' => Yii::app()->request->baseUrl . '/css/gridView.css',
            'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
                           'prevPageLabel'  => '«',
                           'nextPageLabel'  => '»',
                           'header'=>''),
            //'filter'=>$model,
            'columns'=>array(
                /*'id',*/
                'name',
                array(
                    'name'=>'idMainContact',
                    'value'=>'$data->mainContact->surname.", ".$data->mainContact->name',
                ),
                'lastupdated',
                'created',
                /*'deleted',*/
                /*
                'isdeleted',
                */
                array(
                    'class'=>'CButtonColumn',
                    'htmlOptions' => array('style' => 'width: 70px; text-align: center;'),
                    'header'=>'Options',
                    'buttons' => array(
                        'update'=>array(
                            'imageUrl'=>Yii::app()->request->baseUrl.'/images/icons/update.png',
                        ),
                        'delete'=>array(
                            'imageUrl'=>Yii::app()->request->baseUrl.'/images/icons/delete.png',
                        ),
                        'view'=>array(
                            'imageUrl'=>Yii::app()->request->baseUrl.'/images/icons/view.png',
                        ),
                    ),
                ),
            ),
        )); ?>
    </div>
</div>

