<?php
/* @var $this EnterpriseController */
/* @var $model Enterprise */

$selectedIndex = 'Configuracion';
?>
<?php /*$this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>
<div class="row-fluid">
    <div class="col-lg-12 marginTop15">
         <span class="sectionTitle">Create Enterprise</span>
        <?php $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
</div>
