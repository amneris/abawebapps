<?php
$module = 'enterprise';

if ( !isset($pag) ) $pag = 1;
if ( !isset($maxPag) ) $maxPag = 1;

if ( $numUsers > 1 || empty($numUsers) ) $tNumberElements = EnterpriseModule::t('Enterprises');
else $tNumberElements = EnterpriseModule::t('Enterprise');
$numElements = $numUsers;

$displayNewElement = ( $numElements == 0 && empty($filters) );
$displayNewElement = false;

$idUser = Utils::getSessionVar('id');

?>

<div class="table-responsive" style="overflow-x: auto;">
<table class="table tableList">
<tr>
    <td colspan="7" style="background-color: #f3f3f3; border-right: solid 1px #ddd; border-left: solid 1px #ddd">
        <span class="tableTitle"><?php echo $numElements; ?> <?php echo $tNumberElements; ?></span>

        <span class="tdMsgList" style="padding-left: 20px;">
          <span class="spanMsgList" id="msgList_2_<?php echo $module; ?>" style="display:none;"></span>
        </span>
    </td>
<!--    <td colspan="5" class="tdMsgList" style="border-top:none;"></td> -->
    <td style="border-top:none;"></td>
</tr>
<tr>
    <td class="thTable" style="border-left: solid 1px #ddd"></td>
    <td class="thTable<?php if ( isset($filters['name']) ) echo " active"; ?>">
        <div class="table-responsive">
            <table class="table tableListTH">
                <tr>
                    <td colspan="2">
                        <input type="text" class="inputSearchList tableListFilter" data-module="<?php echo $module; ?>" id="filter_name" value="<?php if ( isset($filters['name']) ) echo $filters['name']; ?>">
                    </td>
                </tr>
                <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_name">
                    <td class="tdTitleTH">
                        <span><?php echo Yii::t( 'app','NAME' ); ?></span>
                        <img class="imgQuery hidden" src="/images/query_grey3.png" data-toggle="tooltip" data-original-title='<?php echo EnterpriseModule::t('Info del nombre'); ?>'>
                    </td>
                    <?php if ( $orderBy == 'name desc' ) { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
                    <?php } else { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
                    <?php } ?>
                </tr>
            </table>
        </div>
    </td>

    <td class="thTable<?php if ( isset($filters['contact']) ) echo " active"; ?>">
        <div class="table-responsive">
            <table class="table tableListTH">
                <tr>
                    <td colspan="2">
                        <input type="text" class="inputSearchList tableListFilter" data-module="<?php echo $module; ?>" id="filter_contact" value="<?php if ( isset($filters['contact']) ) echo $filters['contact']; ?>">
                    </td>
                </tr>
                <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_contact">
                    <td class="tdTitleTH">
                        <span><?php echo EnterpriseModule::t('Contact'); ?></span>
                    </td>
                    <?php if ( $orderBy == 'contact desc' ) { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
                    <?php } else { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
                    <?php } ?>
                </tr>
            </table>
        </div>
    </td>

    <td class="thTable<?php if ( isset($filters['email']) ) echo " active"; ?>">
        <div class="table-responsive">
            <table class="table tableListTH">
                <tr>
                    <td colspan="2">
                        <input type="text" class="inputSearchList tableListFilter" data-module="<?php echo $module; ?>" id="filter_email" value="<?php if ( isset($filters['email']) ) echo $filters['email']; ?>">
                    </td>
                </tr>
                <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_email">
                    <td class="tdTitleTH">
                        <span><?php echo Yii::t('app', 'Email'); ?></span>
                    </td>
                    <?php if ( $orderBy == 'email desc' ) { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
                    <?php } else { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
                    <?php } ?>
                </tr>
            </table>
        </div>
    </td>

    <td class="thTable<?php if ( isset($filters['created']) ) echo " active"; ?>">
        <div class="table-responsive">
            <table class="table tableListTH">
                <tr>
                    <td colspan="2">
                        <input type="text" class="inputSearchList tableListFilter" data-module="<?php echo $module; ?>" id="filter_created" value="<?php if ( isset($filters['created']) ) echo $filters['created']; ?>">
                    </td>
                </tr>
                <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_created">
                    <td class="tdTitleTH">
                        <span><?php echo Yii::t('app', 'Entry date'); ?></span>
                    </td>
                    <?php if ( $orderBy == 'created desc' ) { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
                    <?php } else { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
                    <?php } ?>
                </tr>
            </table>
        </div>
    </td>

    <td class="thTable<?php if ( isset($filters['country']) ) echo " active"; ?>">
        <div class="table-responsive">
            <table class="table tableListTH">
                <tr>
                    <td colspan="2">
                        <input type="text" class="inputSearchList tableListFilter" data-module="<?php echo $module; ?>" id="filter_country" value="<?php if ( isset($filters['country']) ) echo $filters['country']; ?>">
                    </td>
                </tr>
                <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_country">
                    <td class="tdTitleTH">
                        <span><?php echo Yii::t('app', 'COUNTRY'); ?></span>
                    </td>
                    <?php if ( $orderBy == 'country desc' ) { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
                    <?php } else { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
                    <?php } ?>
                </tr>
            </table>
        </div>
    </td>

    <?php if ( Yii::app()->user->isagent || Yii::app()->user->isabamanager ) { ?>
    <td class="thTable<?php if ( isset($filters['total']) ) echo " active"; ?>" style="border-right: solid 1px #ddd;">
        <div class="table-responsive">
            <table class="table tableListTH">
                <tr><td>&nbsp;</td></tr>
                <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_total">
                    <td class="tdTitleTH">
                        <span><?php echo Yii::t('app', 'Reservations'); ?></span>
                        &nbsp;
                    </td>
                    <?php if ( $orderBy == 'total desc' ) { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
                    <?php } else { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
                    <?php } ?>
                </tr>
            </table>
        </div>
    </td>
    <?php } else { ?>
    <td class="thTable">&nbsp;</td>
    <?php } ?>

    <td style="border-top: none;"></td>
    <td style="border-top: none;"></td>

</tr>

<?php foreach( $mlRoles as $mRole ) { ?>
    <?php

    if (!empty($mRole->urlLogo)) {
        $fileLogoEnterprise = getenv('AWS_URL_DOMAIN').$mRole->urlLogo;
    } else {
        $fileLogoEnterprise = Yii::app()->baseUrl.'/images/logos/enterprise_default.png';
    }
    ?>

    <tr class="rowList <?php if ( !empty($mRole->idUserCreator) && $mRole->idUserCreator != $idUser ) echo "rowAnotherPartner"; ?>" data-module="<?php echo $module; ?>" data-rowid="<?php echo $mRole->idEnterprise; ?>">
        <td class="rowListLeft" style="border-left: solid 1px #ddd; border-bottom: solid 1px #ddd;">
            <img src="<?= $fileLogoEnterprise ?>" width="25" height="25" alt="<?php echo EnterpriseModule::t('Logo'); ?>" title="<?php echo EnterpriseModule::t('Logo'); ?>">
        </td>
        <td class="rowListLeft rowListLeftLast tdListLink" style="border-bottom: solid 1px #ddd;">
            <table>
                <tr>
                    <td><span><?php echo $mRole->nameEnterprise; ?></span></td>
                    <td><img src="/images/arrow.png" alt="<?php echo EnterpriseModule::t('User info'); ?>"/></td>
                </tr>
            </table>
        </td>
        <td style="border-bottom: solid 1px #ddd;">
            <?php echo $mRole->contactName ?>
        </td>
        <td style="border-bottom: solid 1px #ddd;">
            <?php echo $mRole->Email ?>
        </td>
        <td style="border-bottom: solid 1px #ddd;">
            <?php echo $mRole->Creation ?>
        </td>
        <td style="border-bottom: solid 1px #ddd;">
            <?php echo $mRole->Country ?>
        </td>
        <td style="border-right: solid 1px #ddd; border-bottom: solid 1px #ddd;">
            <?php if ( Yii::app()->user->isagent || Yii::app()->user->isabamanager ) { ?>
            <?php
            if($mRole->purchases > 0)
            {
                if($mRole->purchases == 1)
                {
                    $buttonText = EnterpriseModule::t('Request');
                }
                else
                {
                    $buttonText = EnterpriseModule::t('Requests');
                }
                ?>
                <div id="divLinkPayments_<?php echo $mRole->idEnterprise; ?>" class="divLinkPayments" title="<?php echo EnterpriseModule::t('Payments pending'); ?>">
                    <button type="button" class="abaButton abaButton-Primary">
                        <?php echo $mRole->purchases." ".$buttonText; ?>
                    </button>
                </div>
            <?php } ?>
          <?php } ?>
        </td>
        <td class="icoListButton">
            <div id="divLinkSettings_<?php echo $mRole->idEnterprise; ?>" class="divLinkSettings" title="<?php echo EnterpriseModule::t('Settings'); ?>"><img src="/images/settings.png"></div>
        </td>
        <?php if ( Yii::app()->user->isagent || Yii::app()->user->isabamanager ) { ?>
        <td class="icoListButton">
            <div id="divLinkPilar_<?php echo $mRole->idEnterprise; ?>" class="divLinkPilar iconLink" title="<?php echo EnterpriseModule::t('Indiscriminado'); ?>"><img src="/images/anonimo.png" width="17"></div>
        </td>
        <?php } else { ?>
        <td class="icoListButton">
            <div style="width: 17px;">&nbsp;</div>
        </td>
        <?php } ?>
        <td class="icoListButton">
          <div id="divLinkDelete_<?php echo $mRole->idEnterprise; ?>" class="divLinkDelete iconLink" title="<?php echo EnterpriseModule::t('Settings'); ?>"><img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/icontrash_active.png"></div>
        </td>
    </tr>

<?php } ?>

<?php if ( !( $pag == 1 && $maxPag == 1 ) ) { ?>
    <tr>
        <td colspan="4" align="right">

            <ul id="paginationList" class="pagination">
                <?php if ( $pag == 1 ) { ?>
                    <li class="disabled"><span>&laquo;</span></li>
                <?php } else { ?>
                    <li><a id="aListPage_<?php echo ($pag-1); ?>" href="#" data-module="<?php echo $module; ?>">&laquo;</a></li>
                <?php } ?>

                <?php for( $i = $pag - 5; $i <= ($pag + 4); $i++ ) { ?>
                    <?php if ( $i <= 0 ) continue; ?>
                    <?php if ( $i > $maxPag ) continue; ?>
                    <?php if ( $i == $pag ) { ?>
                        <li class="active"><span><?php echo $i; ?> <span class="sr-only">(current)</span></span></li>
                    <?php } else { ?>
                        <li><a id="aListPage_<?php echo $i; ?>" href="#" data-module="<?php echo $module; ?>"><?php echo $i; ?></a></li>
                    <?php } ?>
                <?php } ?>

                <?php if ( $pag == $maxPag ) { ?>
                    <li class="disabled"><span>&raquo;</span></li>
                <?php } else { ?>
                    <li><a id="aListPage_<?php echo ($pag+1); ?>" href="#" data-module="<?php echo $module; ?>">&raquo;</a></li>
                <?php } ?>
            </ul>

        </td>
    </tr>
<?php }  ?>


</table>
</div>

<script>
    var checkedList_<?php echo $module; ?> = '<?php echo CJavaScript::quote($checkedList); ?>';
    var pag_<?php echo $module; ?> = '<?php echo $pag; ?>';
    var undoSetTimeout_<?php echo $module; ?> = null;
    var numElements_<?php echo $module; ?> = '<?php echo $numElements; ?>';
    var OrderBy_<?php echo $module; ?> = '<?php echo $orderBy; ?>';

    $(document).ready( function()
    {

        $('.tooltipGreat').tooltipster({
            position: 'top-right',
            offsetX: 9
        });

        <?php if ( $displayNewElement ) { ?>
        $('#divDisplayFiltros_<?php echo $module; ?>').trigger('click');
        <?php } ?>

        ABAActivarFiltros();
        ABAActivarPaginador();
        ABAActivarQueryTooltips();
        ABAActivarOrderBy();
        ABAActivarChecks();

        $('.divLinkPayments').click( function() {
            var urlPage = '/purchase/purchase/admin';
            var parameters = {
                id: this.id.replace('divLinkPayments_', ''),
            };
            ABAChangePage( urlPage, parameters );
        });

        $('.divLinkSettings').click( function() {
            var urlPage = '/extranet/configs';
            var parameters = {
                id: this.id.replace('divLinkSettings_', ''),
            };
            ABAChangePage( urlPage, parameters );
        });

        $('.divLinkDelete').click( function() {
          if ( !confirm(<?php echo CJavaScript::encode( Yii::app()->getModule('enterprise')->t('Esta seguro de querer borrar la empresa ?') ); ?>) ) return;

          var url = '/<?php echo $module;?>/<?php echo $module;?>/delete';
          var parametros = {
            idEnterprise: this.id.replace('divLinkDelete_', ''),
          };

          var successF = function(data) {
            if ( data.ok ) {
              ABAShowOkMsgList( '<?php echo $module; ?>', data.okMsg );
              ABAActivarUndoList( '<?php echo $module; ?>' );
            } else {
              ABAShowErrorMsgList( '<?php echo $module; ?>', data.errorMsg );
            }

            ABAStopLoading();
          }

          ABAShowLoading();
          ABALaunchAjax( url, parametros, successF );
        });


        $('.divLinkPilar').click( function() {
            var url = '/price/main/minipanel';
            var parametros = {
                idEnterprise: this.id.replace('divLinkPilar_', ''),
            };

            var successF = function(data) {
                ABAShowDialog( data.html, true, '50%', '280px' );
            }

            ABAShowLoading();
            ABALaunchAjax( url, parametros, successF );
        });

        $('.tdListLink').each( function() {
            var parent = $(this).parent();
            var module = $(parent).data("module");
            if ( module == '<?= $module ?>' ) {
                var id = $(parent).data("rowid");

                $(this).click( function() {
                    var pageURL = '<?= Yii::app()->baseUrl; ?>/student/main/'+id;
                    var parameters = {
//            email: email,
                    };
                    ABAChangePage( pageURL, parameters );
                })
            }
        });

        ABARedrawBody();
    });

    function sendPurchase() {
        var urlPage = '/price/main/compraradiscrecion';

        var parameters = {
            idRequest: 1,
            idEnterprise: 33,
            idLicenseType: $('#price_'+$('#sMonths').val()).data('id'),
            numLicenses: $('#sLicenses').val(),
            value: 0,
            valueLocal: 0,
            discount: 100
        };
        ABAChangePage( urlPage, parameters );
    }

</script>
