<?php

$selectedIndex = 'None';
$module = 'enterprise';

?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>

<div class="row-fluid">
    <div class="col-lg-12">

        <div class="row-fluid">
            <div class="col-lg-10">
                <div class="divSectionTittle">
                    <span class="sectionTitle"><?php echo CHtml::encode( EnterpriseModule::t( 'Organizations') ); ?></span>
                </div>
            </div>
            <div class="col-lg-2">
                <div id="divExportToCsv"><img src="/images/csv-icon.png" alt="<?php echo Yii::t('app','Export to CSV'); ?>" title="<?php echo Yii::t('app','Export to CSV'); ?>"></div>
            </div>
        </div>

        <div class="row-fluid" style="display: none;">
            <div class="col-lg-4">
                <?php echo CHtml::link(Yii::t('app','Export to CSV'),array('enterprise/export/')); ?>
            </div>
            <br><br>
        </div>

        <div class="row-fluid">
            <div class="col-lg-4">
                <div id="divDisplayFiltros_<?php echo $module; ?>" class='divDisplayFiltros'><span class="newRegLabel"><?php echo EnterpriseModule::t( 'ADD NEW ENTERPRISE' ); ?> <img id='imgDisplayFiltros_<?php echo $module; ?>' src='<?php echo Yii::app()->baseUrl; ?>/images/less.png' width="11" height="11"></span></div>
            </div>
            <div class="col-lg-8">
                <div><span class="spanMsgList spanMsgAddList" id="msgList_1_<?php echo $module; ?>" style="display:none;"></span></div>
            </div>
        </div>

        <div class="row-fluid">
            <div class="col-lg-12">
                <div class="table-responsive" id="divTableFiltros_<?php echo $module; ?>">
                    <table class="table tableFiltros">
                        <tr>
                            <td class="tdNewreg" style="border-top: solid 1px #ddd; border-bottom: solid 1px #ddd; border-left: solid 1px #ddd; background-color: #F3F3F3;">
                                <?php echo CHtml::label( CHtml::encode(EnterpriseModule::t('NAME') ) , 'newEnterpriseName'); ?><br>
                                <?php echo CHtml::textField('newEnterpriseName', '', array( 'id' => 'newEnterpriseName', 'class' => 'form-control addCamp_'.$module )); ?>
                            </td>
                            <td class="tdNewreg" style="border-top: solid 1px #ddd; border-bottom: solid 1px #ddd; background-color: #F3F3F3;">
                                <?php echo CHtml::label( CHtml::encode(Yii::t('app','COUNTRY') ) , 'newEnterpriseCountry'); ?><br>
                                <?php echo chtml::dropDownList('newEnterpriseCountry',Utils::getSessionVar('idUserCountry'), $countries, array('class'=>'form-control addCamp_'.$module, 'empty'=>'Select Country')); ?>
                            </td>
                            <td class="tdNewreg" style="border-top: solid 1px #ddd; border-bottom: solid 1px #ddd; background-color: #F3F3F3;">
                                <?php echo CHtml::label( CHtml::encode(UserModule::t('COURSE LANGUAGE') ) , 'newEnterpriseLang'); ?>
                                <img class="tooltipTop" src="/images/query_grey3.png" title="<?php echo EnterpriseModule::t('Select default language for the course.'); ?>">
                                <br>
                                <?php
                                    foreach($courseLangs as $key => $item)
                                    {
                                        $courseLangs[$key]=Yii::t('app',$item);
                                    }
                                ?>
                                <?php echo chtml::dropDownList('newEnterpriseLang',Utils::getSessionVar('idLanguage'), $courseLangs, array('class'=>'form-control addCamp_'.$module)); ?>
                            </td>
                            <td class="tdNewreg" style="border-top: solid 1px #ddd; border-bottom: solid 1px #ddd; background-color: #F3F3F3;">
                                <?php echo CHtml::label( CHtml::encode(EnterpriseModule::t('ENTERPRISE TYPE') ) , 'newEnterpriseType'); ?><br>
                                <?php echo chtml::dropDownList('newEnterpriseType',1, $enterpriseTypes, array('class'=>'form-control addCamp_'.$module)); ?>
                            </td>
                            <td class="tdNewreg" style="border-right: solid 1px #ddd; border-top: solid 1px #ddd; border-bottom: solid 1px #ddd; background-color: #F3F3F3;">
                                <button id="addButton_<?php echo $module; ?>" type="button" class="abaButton abaButton-Primary" style="margin-top: 21px; width: 100%;">
                                    <?php echo Yii::t('app', 'ADD'); ?>
                                </button>
                            </td>
                            <td class="tdNewreg" style="width: 34px; background-color: transparent;">
                            </td>

                            <td class="tdNewreg" style="width: 34px; background-color: transparent;">
                            </td>

                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="row-fluid">
            <table style="border: solid 1px #ddd; float: right; margin-right: 82px; margin-top: 30px;">
                <tr>
                    <td id="orgMenu">
                        <ul>
                            <li id="fil0" class="entFilter selected"><a href="#" onclick="setValueFilter(0);"><?php echo Yii::t('app','Show all'); ?></a></li>
                            <li id="fil1" class="entFilter"><a class="orgMenuLinkImg" href="#" onclick="setValueFilter(1);"><?php echo EnterpriseModule::t('Enterprise')?></a></li>
                            <li id="fil2" class="entFilter"><a class="orgMenuLinkImg" href="#" onclick="setValueFilter(2);"><?php echo Yii::t('app','Education'); ?></a></li>
                            <li id="fil3" class="entFilter"><a class="orgMenuLinkImg" href="#" onclick="setValueFilter(3);"><?php echo Yii::t('app','Public institution'); ?></a></li>
                            <input type="hidden" id="filter_idType" class="tableListFilter" data-module="<?php echo $module; ?>" value="0"/>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>

        <div class="row-fluid">
            <div class="col-lg-12">
                <div class="divTableList" id="divTableList_<?php echo $module; ?>">
                    &nbsp;
                </div>
            </div>
        </div>

        <div class="row-fluid">
            <div class="col-lg-12">
                &nbsp;
            </div>
        </div>

    </div>
</div>

<script>

    var OrderBy_<?php echo $module; ?> = '(total>0) desc, created desc';
    var checkedList_<?php echo $module; ?> = '';

    //set the value of the filter for all, enterprises, education, public institution.
    function setValueFilter(type)
    {
        $('.entFilter').removeClass('selected');
        $('#fil1').children().addClass('orgMenuLinkImg');
        $('#fil2').children().addClass('orgMenuLinkImg');
        $('#fil3').children().addClass('orgMenuLinkImg');
        switch(type)
        {
            case 1:
                $('#fil1').addClass('selected');
                $('#fil1').children().removeClass('orgMenuLinkImg');
                break;
            case 2:
                $('#fil2').addClass('selected');
                $('#fil2').children().removeClass('orgMenuLinkImg');
                break;
            case 3:
                $('#fil3').addClass('selected');
                $('#fil3').children().removeClass('orgMenuLinkImg');
                break;
            default:
                $('#fil0').addClass('selected');
        }
        $('#filter_idType').attr('value',type);
        ABARefreshList('<?php echo $module; ?>');
    }

    $(document).ready( function()
    {
        $('#divExportToCsv').click( function() {
            var pageURL = '<?= Yii::app()->baseUrl; ?>/enterprise/enterprise/export/';
            var parameters = {
                "idType" : $('#filter_idType').val()
            };
            ABAChangePage( pageURL, parameters, false );
        });

        $('.tooltipTop').tooltipster({
            position: 'top-right',
            offsetX: 9
        });

        $('.addCamp_<?php echo $module; ?>').keydown( function (e) {
            if ( e.keyCode == 13 ) $('#addButton_<?php echo $module; ?>').trigger('click');
        });

        $('#divDisplayFiltros_<?php echo $module; ?>').click( function() {

            $('#divTableFiltros_<?php echo $module; ?>').parent().parent().toggle();

            if ( $('#divTableFiltros_<?php echo $module; ?>').parent().parent().is(':visible') ) {
                $('#imgDisplayFiltros_<?php echo $module; ?>').attr('src', '<?php echo Yii::app()->baseUrl; ?>/images/less.png');
            } else {
                $('#imgDisplayFiltros_<?php echo $module; ?>').attr('src', '<?php echo Yii::app()->baseUrl; ?>/images/plus.png');
            }

        });
        $('#divDisplayFiltros_<?php echo $module; ?>').trigger('click');

        $('#addButton_<?php echo $module; ?>').click( function(e) {
            e.preventDefault();
            var module = '<?php echo $module; ?>';

            if ( $('#newEnterpriseName').val() == '' ) {
                alert('<?php echo CJavaScript::quote(Yii::t('app','Name must not be empty.')); ?>');
                $('#newEnterpriseName').focus();
                return false;
            }

            ABAShowLoading();
            var parametros = {
                "newEnterpriseName" : $('#newEnterpriseName').val(),
                "newEnterpriseCountry" : $('#newEnterpriseCountry').val(),
                "newEnterpriseLang" : $('#newEnterpriseLang').val(),
                "newEnterpriseType" : $('#newEnterpriseType').val()
            };

            $.ajax({
                data:  parametros,
                url: "/"+module+"/"+module+"/insert",
                context: document.body,
                type: 'POST',
                dataType:"json",
                success: function(data) {

                    if ( data.ok ) {
                        ABAShowOkMsgList( '<?php echo $module;  ?>', data.okMsg, 1 );
                        $('#newEnterpriseName').val('');

                        ABARefreshList( '<?php echo $module; ?>' );
                    } else {
                        ABAShowErrorMsgList( '<?php echo $module;  ?>', data.errorMsg, 1 );
                    }

                    ABAStopLoading();
                },
                error: function(jqXHR, textStatus, error) {
                    alert(jqXHR + ' ' + textStatus + ' ' + error);
                    ABAStopLoading();
                },
            }).done(function() {
                //
            });
        });

        ABARefreshList( '<?php echo $module; ?>' );

    });

</script>