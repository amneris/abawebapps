<?php
$fileLogoEnt = Utils::getSessionVar('enterpriseLogo');
?>

<table width="100%">
    <tr>
        <td>
            <span class="sectionTitle"><?php echo Yii::t('app', 'General settings'); ?></span>
        </td>
    </tr>
</table>
<hr class="hrOverTitle">

<?php $form = $this->beginWidget('CActiveForm', array(
  'id' => 'group-form',
  'enableAjaxValidation' => false,
)); ?>
<div class="withBorderBottom">
    <table class="userTable">
        <tr onmouseover="js:showEditImg('enterpriseNameIMG')" onmouseout="js:hideEditImg('enterpriseNameIMG')">
            <td width="40px" class="paddingTop10">
            </td>
            <td width="250px" class="paddingTop10">
                <span class="formLabel"><?php echo EnterpriseModule::t('Company name'); ?></span> <br>
            </td>
            <td class="paddingTop10">

            </td>
        </tr>
        <tr onmouseover="js:showEditImg('enterpriseNameIMG')" onmouseout="js:hideEditImg('enterpriseNameIMG')">
            <td class="paddingBottom10">
                <a href="#" style="cursor: hand"><img
                      src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px"
                      id="enterpriseNameIMG" style="display: none" class="yeap"
                      onclick="js:transform('enterpriseName','<?php echo $model->name; ?>')"></a>
            </td>
            <td class="paddingBottom10">
                <span class="userTableValue" onclick="js:transform('enterpriseName','<?php echo $model->name; ?>')"
                      id="enterpriseNameINPUT"><?php echo $model->name; ?></span>
            </td>
            <td class="paddingBottom10">
                <button id='enterpriseNameBOT' class='abaButton abaButton-Primary'
                        style="margin-left:40px; display: none"><?php echo EnterpriseModule::t('Save'); ?></button>
            </td>
        </tr>
    </table>
</div>
<?php $this->endWidget(); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
  'id' => 'group-form',
  'enableAjaxValidation' => false,
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="withBorderBottom">
    <table class="userTable">
        <tr onmouseover="js:showEditImg('mainContactIMG')" onmouseout="js:hideEditImg('mainContactIMG')">
            <td width="40px" class="paddingTop10">
            </td>
            <td width="250px" class="paddingTop10">
                <span class="formLabel"><?php echo EnterpriseModule::t('Main contact'); ?></span> <br>
            </td>
            <td class="paddingTop10">

            </td>
        </tr>
        <tr onmouseover="js:showEditImg('mainContactIMG')" onmouseout="js:hideEditImg('mainContactIMG')">
            <td class="paddingBottom10">
                <a href="#" style="cursor: hand"><img
                      src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px"
                      id="mainContactIMG" style="display: none" class="yeap"
                      onclick="js:transform('mainContact','<?php echo $model->idMainContact; ?>')"></a>
            </td>
            <td class="paddingBottom10">
                <?php
                echo chtml::dropDownList('mainContact', $model->idMainContact, $possibleMainContacts,
                  array('onchange' => 'js:showAcceptBut(\'mainContact\')'));
                ?>
            </td>
            <td class="paddingBottom10">
                <button id='mainContactBOT' class='abaButton abaButton-Primary'
                        style="margin-left:40px; display: none"><?php echo EnterpriseModule::t('Save'); ?></button>
            </td>
        </tr>
    </table>
</div>
<?php $this->endWidget(); ?>

<div>
    <?php $form = $this->beginWidget('CActiveForm', array(
      'id' => 'logo-form',
      'enableAjaxValidation' => false,
      'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>
    <table class="userTable">
        <tr onmouseover="js:showEditImg('langIMG')" onmouseout="js:hideEditImg('langIMG')">
            <td width="40px" class="paddingTop10">

            </td>
            <td width="250px" class="paddingTop10">
                <span class="formLabel">
                    <?php
                    if (!empty($fileLogoEnt)) {
                        echo Yii::t('app', 'Select a new logo');
                    } else {
                        echo Yii::t('app', 'Select a logo');
                    }
                    ?>
                </span>
                <br>
            </td>
            <td class="paddingTop10">

            </td>
        </tr>
        <tr onmouseover="js:showEditImg('profileIMG')" onmouseout="js:hideEditImg('profileIMG')">
            <td>
                <a href="#" style="cursor: hand"><img
                      src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px"
                      id="profileIMG" style="display: none" class="yeap" onclick="js:submitCrop();"></a>
            </td>
            <td>
                <input type="file" name="fileName" id="fileName" size="40" onchange="showAcceptBut('profile')">
            </td>
            <td>
                <button id='profileBOT' class='abaButton abaButton-Primary'
                        style="margin-left:40px; display: none"><?php echo Yii::t('app', 'Save'); ?></button>
            </td>
        </tr>
    </table>
</div>
<?php $this->endWidget(); ?>
<br><br>
<?php

if (!empty($fileLogoEnt)) { ?>
    <div style="align-content: center">
        <p align="center">
            <?php echo Yii::t('app',
              'If you need it, you can crop de image to adjust the logo and keep a good appearance.'); ?>
            <br><br>
            <img id="cropbox"
                 src="<?php echo getenv('AWS_URL_DOMAIN') . $fileLogoEnt; ?>?x=<?php echo rand(); ?>"
                 style="margin-right: 0.5em; display2: none;">
            <br>
            <button id="cropBut" class='abaButton abaButton-Primary'><?php echo Yii::t('app', 'Crop'); ?></button>
        </p>
        <input type="hidden" id="x" name="x"/>
        <input type="hidden" id="y" name="y"/>
        <input type="hidden" id="w" name="w"/>
        <input type="hidden" id="h" name="h"/>
    </div>
<?php } else { ?>
    <img id="cropbox" src="" style="display: none;">
<?php } ?>
<br>

<div style="text-align: center; margin-bottom: 2em">
    <button id="btnBack" type="button" class="abaButton abaButton-Primary"><?php echo Yii::t('app', 'Back'); ?></button>
</div>


<script>
    function showEditImg(editField) {
        $("#" + editField).show()
    }
    function hideEditImg(editField) {
        if ($("#" + editField).hasClass("yeap")) {
            $("#" + editField).hide();
        }
    }
    function transform(id, value) {
        $('#' + id + 'IMG').show();
        $('#' + id + 'IMG').removeClass("yeap");
        $('#' + id + 'INPUT').replaceWith("<input class='form-control addCampStudent' size='35' type='text' id='" + id + "' name='" + id + "' value='" + value + "'> ");
        $('#' + id + 'BOT').show();

    }
    function submitCrop() {
        $('#fileName').click();
    }
    function showAcceptBut(id) {
        $('#' + id + 'IMG').show();
        $('#' + id + 'IMG').removeClass("yeap");
        $('#' + id + 'BOT').show();
    }
    function disableAcceptButAndImg(id) {
        $('#' + id + 'IMG').hide();
        $('#' + id + 'BOT').hide();
    }

    function updateCoords(c) {
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
    }
    ;

    function checkCoords() {
        if (parseInt($('#w').val())) return true;
        alert('<?php echo CJavaScript::quote(Yii::t('app','Please select a crop region then press CROP.')); ?>');
        return false;
    }
    ;

    function createCropObject() {
        var jcrop_api;

        jcrop_api = $.Jcrop('#cropbox', {
            onSelect: updateCoords,
            bgColor: 'white',
            bgOpacity: .3,
            aspectRatio: 1
        });

        $('#cropBut').click(function (e) {
            e.preventDefault();

            if (checkCoords()) {
                //ABAShowLoading();
                var parametros = {
                    x: $('#x').val(),
                    y: $('#y').val(),
                    w: $('#w').val(),
                    h: $('#h').val()
                };

                $.ajax({
                    data: parametros,
                    url: "/extranet/crop",
                    context: document.body,
                    type: 'POST',
                    dataType: "json",
                    success: function (data) {
                        if (data.result) {
                            jcrop_api.setImage(data.urlLogo);
                            $('#logoIMG').attr('src', data.urlLogo);
                        }
                        else {
                            alert('<?php echo CJavaScript::quote(Yii::t('app','There was a problem cropping the image.')); ?>');
                        }

                        //ABAStopLoading();
                    },
                    error: function (jqXHR, textStatus, error) {
                        ABAStopLoading();
                        alert(jqXHR + textStatus + error);
                    }
                }).done(function () {
                    //
                });
            }
        });
    }


    $(document).ready(function () {

        <?php
        if($resultMessage<>'') {
        ?>
        setTimeout(function () {
            ABACustomDialog('ERROR', '<?php echo $resultMessage; ?>', false, true);
        }, 1000);
        <?php
        }
        ?>

        if (typeof $('#cropbox') != 'undefined') {
            $('#cropbox').onloadend
            {
                setTimeout(createCropObject, 200);
            }
        }

        $('#btnBack').click(function () {
            var pageURL = '/student/main';
            var parameters = {};

            ABAChangePage(pageURL, parameters);

        });

    });


</script>