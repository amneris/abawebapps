<?php

class EnterpriseController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='extranet';
    private $pageLimit = 10;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view','admin','update', 'delete', 'undodelete', 'create', 'refresh','insert','export'),
                'roles'=>array(Role::_ABAMANAGER, Role::_AGENT, Role::_PARTNER),
            ),

            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('index'),
                'roles'=>array(Role::_RESPONSIBLE),
            ),

            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'refresh','insert','export'),
                'users' => array('@'),
            ),

            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Enterprise;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Enterprise']))
        {
            $model->attributes=$_POST['Enterprise'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionInsert() {
        $timeNow = date("Y-m-d H:i:s");

        $allOk = false; $errorMsg = ''; $okMsg = EnterpriseModule::t('Enterprise successfully added.');
        $name = Yii::app()->request->getParam('newEnterpriseName');
        $idCountry = Yii::app()->request->getParam('newEnterpriseCountry');
        $idCourseLang = Yii::app()->request->getParam('newEnterpriseLang');
        $idMainContact = Utils::getSessionVar('id');
        $idType = Yii::app()->request->getParam('newEnterpriseType');
        $idPartnerEnterprise = Yii::app()->params['defaultB2bChannel'];

        $transaction = Yii::app()->dbExtranet->beginTransaction();
        try {
            $mUser = User::model()->findByPK($idMainContact);
            if ( !$mUser ) {
                throw new Exception(Yii::app()->getModule( 'enterprise')->t('Error, something went wrong while trying to find the main contact user.'));
            }

            $mEnterprise = new Enterprise;
            $mEnterprise->name = $name;
            $mEnterprise->idMainContact = $idMainContact;
            $mEnterprise->idUserCreator = $idMainContact;
            $mEnterprise->idLanguage = $idCourseLang;
            $mEnterprise->idCountry = $idCountry;
            $mEnterprise->idType = $idType;
            $mEnterprise->idPartner = Enterprise::idPartnerNextVal($name);
            $mEnterprise->idPartnerEnterprise = $idPartnerEnterprise;
            $mEnterprise->lastupdated = $timeNow;
            $mEnterprise->created = $timeNow;
            $mEnterprise->deleted = $timeNow;
            $mEnterprise->isdeleted = 0;
            $allOk = $mEnterprise->save();
            if (!$allOk) {
                throw new Exception(EnterpriseModule::t('Error, something went wrong while trying to save the enterprise.'));
            }

            $mUserRole = new EnterpriseUserRole;
            $mUserRole->idRole = Role::_RESPONSIBLE;
            $mUserRole->idEnterprise = $mEnterprise->id;
            $mUserRole->idUser = $idMainContact;
            $mUserRole->created = $timeNow;
            $mUserRole->deleted = $timeNow;
            $mUserRole->isdeleted = 0;
            $allOk = $mUserRole->save();
            if (!$allOk) {
                throw new Exception(EnterpriseModule::t('Error, something went wrong while trying to save the user role.'));
            }

            $mUserRole = new EnterpriseUserRole;
            if ( Yii::app()->user->isagent || Yii::app()->user->isabamanager ) $mUserRole->idRole = Role::_AGENT;
            else if ( Yii::app()->user->ispartner ) $mUserRole->idRole = Role::_PARTNER;
            else throw new Exception(EnterpriseModule::t('Error, invalid role.'));
            $mUserRole->idEnterprise = $mEnterprise->id;
            $mUserRole->idUser = $idMainContact;
            $mUserRole->created = $timeNow;
            $mUserRole->deleted = $timeNow;
            $mUserRole->isdeleted = 0;
            $allOk = $mUserRole->save();
            if (!$allOk) {
                throw new Exception(EnterpriseModule::t('Error, something went wrong while trying to save the user role.'));
            }

            $mCountry = Country::model()->findByPk( $idCountry );
            if ( $mCountry ) {
              $idAgent = User::getAgentByCountry( $mCountry->iso );
              if ( $idAgent != $idMainContact ) {
                $mUserRole = new EnterpriseUserRole;
                $mUserRole->idRole = Role::_AGENT;
                $mUserRole->idEnterprise = $mEnterprise->id;
                $mUserRole->idUser = $idAgent;
                $mUserRole->created = $timeNow;
                $mUserRole->deleted = $timeNow;
                $mUserRole->isdeleted = 0;
                $allOk = $mUserRole->save();
                if (!$allOk) {
                  throw new Exception(EnterpriseModule::t('Error, something went wrong while trying to save the user role.').'(2)');
                }
              }
            }

            $mLicenseAvailable = new LicenseAvailable;
            $mLicenseAvailable->idEnterprise = $mEnterprise->id;
            $mLicenseAvailable->idLicenseType = LicenseType::_Trial;
            $mLicenseAvailable->numLicenses = 1;
            $mLicenseAvailable->histLicenses = 1;
            $mLicenseAvailable->lastupdated = date("Y-m-d H:i:s");
            $allOk = $mLicenseAvailable->save();
            if (!$allOk) {
                throw new Exception(PurchaseModule::t('Error, something went wrong while trying to add licenses.'));
            }

            $mEnterprise->updateSelligentData();

            $transaction->commit();
        }
        catch(Exception $e)
        {
            $transaction->rollback();
            $allOk = false;
            $errorMsg = $e->getMessage();
        }

        echo json_encode(array( 'ok' => $allOk, 'errorMsg' => $errorMsg, 'okMsg' => $okMsg ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['name']))
        {
            $model->name = $_POST['name'];
            if($model->save()) {
              $model->updateSelligentData();
              $this->redirect(array('update','id'=>$model->id));
            }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete() {

      $errorMsg = '';
      $okMsg = '';
      $idEnterprise = Yii::app()->request->getParam('idEnterprise');

      $undoCode = Enterprise::deleteItem( $idEnterprise );
      $allOk = ( $undoCode != false );
      if ( !$allOk ) $errorMsg = Enterprise::$lastErrorMsg;
      else $okMsg = Yii::app()->getModule('enterprise')->t('lbl: The selected enterprise has been removed.').'<a id=\'undoDeleteList_enterprise\' data-undocode=\''.$undoCode.'\' href=\'#\'>'.Yii::t( 'app', 'Undo').'</a>';

      echo json_encode(array( 'ok' => $allOk, 'okMsg' => $okMsg, 'errorMsg' => $errorMsg ));
    }

    /**
     *
     */
    public function actionUndodelete()
    {
      $errorMsg = '';
      $undoCode = Yii::app()->request->getParam( 'undoCode', 0 );

      $allOk = Enterprise::undoDelete( $undoCode );
      if (!$allOk) $errorMsg = Enterprise::$lastErrorMsg;

      echo json_encode(array('ok' => $allOk, 'errorMsg' => $errorMsg));
    }


    /**
     * Lists all models.
     */
    public function actionIndex()
    {
//        if ( Yii::app()->user->isGuest ) $this->redirect( '/user/login/logout' );     // ** No hay sesion.
        if ( Utils::getSessionVar('idRole') == Role::_RESPONSIBLE ) {
            $this->redirect( '/student/main/'.Utils::getSessionVar('idEnterprise') );
        }

        Utils::setSessionVar('idEnterprise', 0);
        Utils::setSessionVar('role', 0);

        /* we get a list of all countries. */
        $countriesList = Country::model()->getAvailableCountries();
        /* we get a list of all course languages. */
        $langList = Language::model()->getAvailableLanguagesCourse();

        $enterpriseTypes = array(1=>Yii::t('app','Company'),2=>Yii::t('app','Education'),3=>Yii::t('app','Public institution'));

        $this->render('index',array('countries'=>$countriesList, 'courseLangs'=>$langList, 'enterpriseTypes'=>$enterpriseTypes,
        ));

    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $enterprises = EnterpriseUserRole::model()->getEnterprisesByUser(Utils::getSessionVar('id'));

        $criteria = new CDbCriteria();
        $criteria->condition = 'id IN ('.$enterprises.')';
        $dataProvider=new CActiveDataProvider('Enterprise', array(
            'criteria'=>$criteria,
        ));

        $this->render('admin',array(
            'dataProvider'=>$dataProvider,
        ));

    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Enterprise the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Enterprise::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Enterprise $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='enterprise-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


    /**
     *
     */
    public function actionRefresh() {
        $idUser = Utils::getSessionVar('id');

        $checkedList = Yii::app()->request->getParam('checkedList', '0');
        $checkedList = Utils::adminCheckedList($checkedList);

        $pag = Yii::app()->request->getParam('pag', 1);
        $limit = $this->pageLimit;
        $offset = $limit * ($pag - 1);

        $orderBy = Yii::app()->request->getParam('OrderBy', 'total desc');

        $orderBy = str_replace(array('th', 'EnterpriseList'), '', $orderBy);
        $orderBy = str_replace('_', ' ', $orderBy);
        $orderBy = strtolower($orderBy);

        $filters = Yii::app()->request->getParam('filters', array());
        $arFilters = array();
        foreach ($filters as $k => $filter) {
            if (empty($filter['value']))
                continue;
            if (substr($filter['key'], 0, strlen('filter_')) != 'filter_')
                continue;

            $key = substr($filter['key'], strlen('filter_'));
            $arFilters[$key] = $filter['value'];
        }

//        $count = User::countUserRoles($idUser, $arFilters);
//        $mlRoles_tmp = User::getUserRoles($idUser, $arFilters, $orderBy, $limit, $offset);
        $count = Enterprise::countEnterpriseByUserRole($idUser, $arFilters);
        $mlRoles_tmp = Enterprise::getEnterpriseByUserRole($idUser, $arFilters, $orderBy, $limit, $offset);

        $mlRoles = array();
        foreach ($mlRoles_tmp as $mRole)
        {
            $mRole->purchases = Purchase::model()->getPendingPurchase($mRole->idEnterprise);
            $mlRoles[] = $mRole;
        }

        if (!empty($limit)) {
            $maxPag = (int) ($count / $limit);
            if (($count % $limit) > 0)
                $maxPag = $maxPag + 1;
        } else
            $maxPag = 1;

        $html = $this->renderPartial('list', array('mlRoles' => $mlRoles, 'orderBy' => $orderBy, 'pag' => $pag, 'maxPag' => $maxPag, 'checkedList' => $checkedList, 'numUsers' => $count, 'filters' => $arFilters), true);

        echo json_encode(array('ok' => 'true', 'html' => $html));
    }

    public function actionExport()
    {
        $enterpriseList = Enterprise::model()->enterpriseExport(Utils::getSessionVar('id'));

        $data = array();
        $xls = new JPhpCsv('UTF-8', false, 'Users');
        $flagFirst=true;
        $field = array("Enterprise","Creation","Country","Name","Surname","Email");

        foreach($enterpriseList as $row)
        {
            if($flagFirst)
            {
                $flagFirst= false;
                $theRow=array();
                foreach ($row as $key =>$value)
                {
                    array_push($theRow, $key);
                }
                array_push($data, $theRow);
            }
            $theRow=array();
            foreach ($row as $key =>$value)
            {
                if(in_array($key, $field))
                {
                    array_push($theRow, $value);
                }
                else
                {
                    array_push($theRow, $value);
                }
            }
            array_push($data, $theRow);
        }
        if($flagFirst==true)
        {
            $theRow=array();
            array_push($theRow, "DATA NOT FOUND (Data not exported)");
            array_push($data, $theRow);
        }
        $xls->addArray($data);
        $xls->generateXML("EnterpriseList-".date("d-m-Y"));

    }
}
