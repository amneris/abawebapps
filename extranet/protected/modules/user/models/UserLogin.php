<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class UserLogin extends CFormModel
{
	public $username;
	public $password;
	public $rememberMe;
    
	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('username, password', 'required'),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),
			// password needs to be authenticated
			array('password', 'authenticate'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'rememberMe'=>UserModule::t("Remember me next time"),
			'username'=>UserModule::t("Username or email"),
			'password'=>UserModule::t("Password"),
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
      $allOk = false;
      if(!$this->hasErrors())  // we only want to authenticate when no input errors
      {
          // Login a user with the provided username and password.
        $identity=new UserIdentity($this->username,$this->password);
        if($identity->authenticate() && $identity->errorCode == UserIdentity::ERROR_NONE ) {
          if ( $this->rememberMe ) {
						Utils::setSessionVar('histrenemberme', true );
						$allOk = Yii::app()->user->login($identity, 3600*24*365);
          } else {
						Utils::setSessionVar('histrenemberme', false );
						$allOk = Yii::app()->user->login($identity);
          }

//          $allOk = true;
        }
      } else Yii::log( __CLASS__.'.'.__FILE__.": Error: ".var_export( $this->getErrors(), true), CLogger::LEVEL_ERROR);

			if ( !$allOk ) $this->addError($attribute, Yii::t('app', 'User or password incorrect.') );

      return $allOk;
	}

}
