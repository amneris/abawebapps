<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property string $password
 * @property string $email
 * @property integer $idLanguage
 * @property string $lastupdated
 * @property string $created
 * @property string $deleted
 * @property integer $isdeleted
 * @property string $autologincode
 */
class BaseUser extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, surname, password, email, idLanguage', 'required'),
			array('idLanguage, isdeleted', 'numerical', 'integerOnly'=>true),
			array('name, surname, password, email', 'length', 'max'=>128),
			array('autologincode', 'length', 'max'=>255),
			array('lastupdated, created, deleted', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, surname, password, email, idLanguage, lastupdated, created, deleted, isdeleted, autologincode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'surname' => 'Surname',
			'password' => 'Password',
			'email' => 'Email',
			'idLanguage' => 'Id Language',
			'lastupdated' => 'Lastupdated',
			'created' => 'Created',
			'deleted' => 'Deleted',
			'isdeleted' => 'Isdeleted',
			'autologincode' => 'Autologincode',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('surname',$this->surname,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('idLanguage',$this->idLanguage);
		$criteria->compare('lastupdated',$this->lastupdated,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('deleted',$this->deleted,true);
		$criteria->compare('isdeleted',$this->isdeleted);
		$criteria->compare('autologincode',$this->autologincode,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbExtranet;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
