<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property Enterprise enterprise
 */
class User extends BaseUser
{

  const _PKPILAR = '4';
  const _PKCARLES = '6';
  const _PKGINO = '17';

  private $modelAttributes = array();
  public $fileName;

  static $lastErrorMsg = '';

//    public function init()
//    {
//        parent::init();
////        $this->modelAttributes = array( 'id', 'name', 'surname', 'password', 'email', 'lastupdated', 'created', 'deleted', 'isdeleted', );
//    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */

	public function attributeLabels()
	{
		return array(
			'id' => UserModule::t('ID'),
			'name' => UserModule::t('Name'),
			'surname' => UserModule::t('Surname'),
			'password' => UserModule::t('Password'),
			'email' => UserModule::t('Email'),
			'idLanguage' => UserModule::t('Language'),
			'lastupdated' => UserModule::t('Last updated'),
			'created' => UserModule::t('Created'),
			'deleted' => UserModule::t('Deleted'),
			'isdeleted' => UserModule::t('Is deleted'),
		);
	}

    /**
     * @return array relational rules.
     */

    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
          'enterprise'=>array(self::HAS_ONE, 'Enterprise', 'idMainContact'),
          'language'=>array(self::BELONGS_TO, 'Language', 'idLanguage'),
          'enterpriseAndRole'=>array(self::HAS_MANY, 'EnterpriseUserRole', 'idUser'),
          'purchases'=>array(self::HAS_MANY, 'Purchase', 'idUser'),
        );
    }

//    public function setModelAttribute($attribute, $value)
//    {
//        if( isset($this->modelAttributes[$attribute]))
//        {
//            $this->modelAttributes[$attribute] = $value;
//        }
//    }
//
//    public function setModelAttributes($arAttributes)
//    {
//        foreach($arAttributes as $attribute => $value)
//        {
//            $this->setModelAttribute($attribute, $value);
//        }
//    }
//  
//    public function getModelAttribute($attribute)
//    {
//        if (isset($this->modelAttributes[$attribute]))
//        {
//            return $this->modelAttributes[$attribute];
//        }
//        else
//        {
//            return false;
//        }
//    }
//  
//    public function getModelAttributes()
//    {
//        return $this->modelAttributes;
//    }
//  
//
//	/**
//	* @return CDbConnection database connection
//	*/
//	public function getDbConnection()
//	{
//		return Yii::app()->dbExtranet;
//	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     *
     */
    public function validatePassword( $user_input )
    {
//        if(hash_equals($this->password, crypt($user_input, $this->password)))
        if( crypt($user_input, $this->password) == $this->password || $user_input == $this->password )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     *
     */
    public static function getNewPassword( $password ) {
      return crypt($password);
    }

    /*
     * 
     */
    public static function getUser($mailUser)
    {
      $oTmp = new User();
      
      //$rowCount=$command->execute();   // ejecuta una sentencia SQL sin resultados
      //$dataReader=$command->query();   // ejecuta una consulta SQL
      //$rows=$command->queryAll();      // consulta y devuelve todas las filas de resultado
      //$row=$command->queryRow();       // consulta y devuelve la primera fila de resultado
      //$column=$command->queryColumn(); // consulta y devuelve la primera columna de resultado
      //$value=$command->queryScalar();  // consulta y devuelve el primer campo en la primer fila

        $mUser = false;
        $connection = $oTmp->getDbConnection();
        $sql = "select id from user where email like '$mailUser' and isdeleted = 0 ";
        $command = $connection->createCommand($sql);
        Yii::trace($command->getText(), "sql.select");
        try
        {
            $idUser = $command->queryScalar();
            if($idUser)
            {
              $mUser = User::model()->findByPk($idUser);
            }
        } catch(Exception $e)
        {
          Yii::log( 'Error to execute last SQL.', 'error', "sql.select" );
        }
        return $mUser;
    }

    public static function countUsersEnterprise($idEnterprise, $arFilters) {
      $oTmp = new User();
      $idRole = Role::_RESPONSIBLE;
      $count = 0;
      
      $connection = $oTmp->getDbConnection();
      
      $command = $connection->createCommand()
        ->select('count(*)')
        ->from('enterprise_user_role eur')
        ->join('user u', 'eur.idUser = u.id and u.isdeleted = 0')
        ->join('language l', 'u.idLanguage = l.id')
        ->where('eur.idRole = :idRole
             and eur.idEnterprise = :idEnterprise
             and eur.isdeleted = 0 ');
//      if ( !empty($orderBy) ) {
//        $orderBy_tmp = str_replace(array('created', 'name', 'language'), array('u.created', 'l.name', 'r.name'), $orderBy);
//        $command->order( $orderBy_tmp );
//      }
      if ( !empty($arFilters) ) {
        $contador = 0;
        foreach( $arFilters as $key => $value ) {
          if ( $key == 'name' ) $key = 'u.name';
          else if ( $key == 'language' ) $key = 'l.name';
          else if ( $key == 'created' ) $key = 'u.created';

          $value_tmp = addcslashes($value, '%_');
          $value_tmp = "%$value_tmp%";
          $command->andWhere( $key.' like :value'.$contador );
          $command->bindParam(':value'.$contador, $value_tmp);
          $contador++;
        }

      }
      $command->bindParam(":idEnterprise", $idEnterprise, PDO::PARAM_INT);
      $command->bindParam(":idRole", $idRole, PDO::PARAM_INT);

//      $connection = Yii::app()->dbExtranet;
//      $sql = "select count(*)
//              from user u, enterprise_user_role eur
//              where eur.idEnterprise = :idEnterprise
//                and eur.idUser = u.id
//                and eur.idRole = :idRole
//                and eur.isdeleted = 0
//                and u.isdeleted = 0 ";
//      $command = $connection->createCommand($sql);
//      $command->bindParam(":idEnterprise", $idEnterprise, PDO::PARAM_INT);
//      $command->bindParam(":idRole", $idRole, PDO::PARAM_INT);
      Yii::trace($command->getText(), "sql.select");
      try
      {
          $count = $command->queryScalar();
      } catch(Exception $e)
      {
          Yii::log( 'Error to execute last SQL.', 'error', "sql.select" );
      }
      return $count;
    }

    public static function getUsersEnterprise($idEnterprise, $arFilters, $orderBy, $checkedList ) {
      $oTmp = new User();

      $idRole = Role::_RESPONSIBLE;
      $mlUsers = false;
      $connection = $oTmp->getDbConnection();
      
      $command = $connection->createCommand()
        ->select('u.*, l.name as language')
        ->from('enterprise_user_role eur')
        ->join('user u', 'eur.idUser = u.id and u.isdeleted = 0')
        ->join('language l', 'u.idLanguage = l.id')
        ->where('eur.idRole = :idRole
             and eur.idEnterprise = :idEnterprise
             and eur.isdeleted = 0 ');
      if ( !empty($orderBy) ) {
        $orderBy_tmp = $orderBy;
        if ( $orderBy_tmp == 'name' ) $orderBy_tmp = 'u.name, u.surname';
        else if ( $orderBy_tmp == 'name desc' ) $orderBy_tmp = 'u.name desc, u.surname desc';
        else if ( $orderBy_tmp == 'surname desc' ) $orderBy_tmp = 'u.surname desc, u.name desc';
        else if ( $orderBy_tmp == 'surname' ) $orderBy_tmp = 'u.surname, u.name';
        $orderBy_tmp = str_replace(array('created', 'language'), array('u.created', 'l.name'), $orderBy_tmp);
        $command->order( $orderBy_tmp );
      }
      if ( !empty($arFilters) ) {
        $contador = 0;
        foreach( $arFilters as $key => $value ) {
          if ( $key == 'name' ) $key = 'u.name';
          else if ( $key == 'language' ) $key = 'l.id';
          else if ( $key == 'created' ) $key = 'u.created';

            if($key<>"l.id")
            {
                $value_tmp = addcslashes($value, '%_');
                $value_tmp = "%$value_tmp%";
                $command->andWhere( $key.' like :value'.$contador );
                $command->bindParam(':value'.$contador, $value_tmp);
            }
            else
            {
                $command->andWhere( $key.' = :value'.$contador );
                $command->bindParam(':value'.$contador, $value);
            }

          $contador++;
        }

      }
      $command->bindParam(":idEnterprise", $idEnterprise, PDO::PARAM_INT);
      $command->bindParam(":idRole", $idRole, PDO::PARAM_INT);
//      $connection = Yii::app()->dbExtranet;
//      $sql = "select u.*, l.name as language
//              from user u, enterprise_user_role eur, language l
//              where eur.idEnterprise = :idEnterprise
//                and eur.idUser = u.id
//                and eur.idRole = :idRole
//                and u.idLanguage = l.id
//                and eur.isdeleted = 0
//                and u.isdeleted = 0 ";
//      $command = $connection->createCommand($sql);
//      $command->bindParam(":idEnterprise", $idEnterprise, PDO::PARAM_INT);
//      $command->bindParam(":idRole", $idRole, PDO::PARAM_INT);
      Yii::trace($command->getText(), "sql.select");
//file_put_contents('/tmp/extra.log', "SQL: ".$command->getText()."\n", FILE_APPEND);
//file_put_contents('/tmp/extra.log', "SQL2: $sql\n", FILE_APPEND);
      try
      {
          $arSelectedItems = array_flip(explode(',', $checkedList));
          $mlUsers_tmp = $command->queryAll();
          $mlUsers = array();
          foreach( $mlUsers_tmp as $rowUser ) {
            $mUser = new stdClass();
            foreach( $rowUser as $key => $value ) $mUser->$key = $value;
            $mUser->checked = ( isset($arSelectedItems[$mUser->id]) xor isset($arSelectedItems['all']) );
            $mlUsers[] = $mUser;
          }

      } catch(Exception $e)
      {
//file_put_contents('/tmp/extra.log', "SQL3: ".$e->getMessage()."\n", FILE_APPEND);
          Yii::log( 'Error to execute last SQL.', 'error', "sql.select" );
      }
      return $mlUsers;
    }

    public function getConcatened( $withEmail = true )
    {
      if ( $withEmail ) return $this->name.' '.$this->surname.' - '.$this->email;
      else return $this->name.' '.$this->surname;
    }

    /* This function return the list of responsible users ready to populate a dropdown menu. */
    public function getResponsibleList($idEnterprise)
    {
        $responsibleList = EnterpriseUserRole::model()->getUsersByRoleAndEnterprise(Role::_RESPONSIBLE, $idEnterprise);
        $criteria = new CDbCriteria();
        $criteria->condition = 'id IN ('.$responsibleList.')';
        $criteria->addCondition('isdeleted = 0');
        $responsibleList = User::model()->findAll($criteria);
        $responsibleList = CHtml::listData($responsibleList, 'id', 'concatened');
        return $responsibleList;

    }

    public static function countUserRoles($idUser, $arFilters) {
      $oTmp = new User();

      $count = 0;
      $mlRoles = false;
      $connection = $oTmp->getDbConnection();

      $command = $connection->createCommand()
        ->select('count(*)')
        ->from('enterprise_user_role eur')
        ->join('role r', 'eur.idRole = r.id')
        ->join('enterprise e', 'eur.idEnterprise = e.id and e.isdeleted = 0')
        ->where('eur.idUser = :idUser and eur.isdeleted = 0 and e.id != 1 and eur.idRole <>'.Role::_RESPONSIBLE);
//      if ( !empty($orderBy) ) {
//        $orderBy_tmp = str_replace(array('created', 'name', 'role'), array('e.created', 'e.name', 'r.name'), $orderBy);
//        $command->order( $orderBy_tmp );
//      }
      if ( !empty($arFilters) ) {
        $contador = 0;
        foreach( $arFilters as $key => $value ) {
          if ( $key == 'name' ) $key = 'e.name';
          else if ( $key == 'role' ) $key = 'r.name';

          $value_tmp = addcslashes($value, '%_');
          $value_tmp = "%$value_tmp%";
          $command->andWhere( $key.' like :value'.$contador );
          $command->bindParam(':value'.$contador, $value_tmp);
          $contador++;
        }

      }
      $command->bindParam(":idUser", $idUser, PDO::PARAM_INT);
      Yii::trace($command->getText(), "sql.select");
      try {
        $count = $command->queryScalar();
      } catch(Exception $e) {
        Yii::log( 'Error to execute last SQL.', 'error', "sql.select" );
      }
      return $count;
    }

    public static function getUserRoles($idUser, $arFilters = array(), $orderBy = 'created', $limit = 10, $offset = 0 ) {
      $oTmp = new User();
      
      $mlRoles = array();
      $connection = $oTmp->getDbConnection();

      $command = $connection->createCommand()
        ->select('r.id as idRole, r.name as nameRole, e.id as idEnterprise, e.name as nameEnterprise, eur.id as idRoleEnterprise')
        ->from('enterprise_user_role eur')
        ->join('role r', 'eur.idRole = r.id')
        ->join('enterprise e', 'eur.idEnterprise = e.id and e.isdeleted = 0')
        ->where('eur.idUser = :idUser and eur.isdeleted = 0 and e.id != 1 and eur.idRole <>'.Role::_RESPONSIBLE)
        ->limit($limit, $offset);
      if ( !empty($orderBy) ) {
        $orderBy_tmp = str_replace(array('created', 'name', 'role'), array('e.created', 'e.name', 'r.name'), $orderBy);
        $command->order( $orderBy_tmp );
      }
      if ( !empty($arFilters) ) {
        $contador = 0;
        foreach( $arFilters as $key => $value ) {
          if ( $key == 'name' ) $key = 'e.name';
          else if ( $key == 'role' ) $key = 'r.name';

          $value_tmp = addcslashes($value, '%_');
          $value_tmp = "%$value_tmp%";
          $command->andWhere( $key.' like :value'.$contador );
          $command->bindParam(':value'.$contador, $value_tmp);
          $contador++;
        }

      }
      $command->bindParam(":idUser", $idUser, PDO::PARAM_INT);
      Yii::trace($command->getText(), "sql.select");
//file_put_contents('/tmp/extra.log', "SQL: ".$command->getText()."\n", FILE_APPEND);
//file_put_contents('/tmp/extra.log', "SQL2: $sql\n", FILE_APPEND);
      try
      {
          $mlRoles_tmp = $command->queryAll();
          $mlRoles = array();
          foreach( $mlRoles_tmp as $rowRole ) {
            $mRole = new stdClass();
            foreach( $rowRole as $key => $value ) $mRole->$key = $value;
            $mlRoles[] = $mRole;
          }

      } catch(Exception $e)
      {
//file_put_contents('/tmp/extra.log', "SQL3: ".$e->getMessage()."\n", FILE_APPEND);
          Yii::log( 'Error to execute last SQL.', 'error', "sql.select" );
      }
      return $mlRoles;
    }

    /**
     *
     * @return boolean
     */
    protected function beforeValidate() {
      $bReturn = parent::beforeValidate();

      if ( empty($this->autologincode) ) $this->autologincode = $this->autologinCode();

      return $bReturn;
    }

    /**
     *
     * @return boolean
     */
    protected function beforeSave() {
      $bReturn = parent::beforeSave();

      if ( empty($this->autologincode) ) $this->autologincode = $this->autologinCode();

      return $bReturn;
    }

    static public function autologinCode() {

      do {
        $code = md5( rand(0,10000).microtime(true) );
        $mUser = User::model()->findByAttributes( array('autologincode' => $code) );

        if ( !$mUser ) break;
      } while(1);


      return $code;
    }

  static public function undoDeleteSelection( $undoCode ) {
    $connection = Yii::app()->dbExtranet;

    $sql = "UPDATE user as u inner Join (select idElement from toundo where code = '{$undoCode}a') as t on u.id = t.idElement SET deleted=created";
    $command = $connection->createCommand($sql);
    $command->execute();

    $sql = "UPDATE enterprise_user_role as eur inner Join (select idElement from toundo where code = '{$undoCode}b') as t on eur.id = t.idElement SET deleted=created";
    $command = $connection->createCommand($sql);
    $command->execute();

    return true;
  }

  static public function deleteSelection( $checkedList, $idEnterprise, $filters = array(), $undo = false ) {
    if ( empty($checkedList) ) {
      self::$lastErrorMsg = Yii::app()->getModule('user')->t('No one user has been selected.');
      return false;
    }

    if ( empty($filters) ) $filters = Utils::getActualFilters( $filters );

    $fName = ''; $fName2 = '';
    $fSurname = ''; $fSurname2 = '';
    $fEmail = ''; $fEmail2 = '';
    $fLanguage = ''; $fLanguage2 = '';
    if ( isset($filters['name']) && !empty($filters['name']) ) { $fName = " and u.name like :fName "; $fName2 = "%{$filters['name']}%"; }
    if ( isset($filters['surname']) && !empty($filters['surname']) ) { $fSurname = " and u.surname like :fSurname "; $fSurname2 = "%{$filters['surname']}%"; }
    if ( isset($filters['email']) && !empty($filters['email']) ) { $fEmail = " and u.email like :fEmail "; $fEmail2 = "%{$filters['email']}%"; }
    if ( isset($filters['language']) && !empty($filters['language']) ) { $fLanguage = " and u.idLanguage = :fLanguage "; $fLanguage2 = $filters['language']; }

    if ( $checkedList == 'all' ) {
      $excludeList = '';
      $checkedList = '';
    } else if ( str_replace('all', '', $checkedList) != $checkedList ) {
      $excludeList = str_replace('all', '0', $checkedList);
      $checkedList = '';
    } else {
      $excludeList = '';
    }
    if ( empty($checkedList) ) $fInclude = ''; else $fInclude = " and u.id in ({$checkedList}) ";
    if ( empty($excludeList) ) $fExclude = ''; else $fExclude = " and u.id not in ({$excludeList}) ";

    $randomCode = 'zTmp_'.date("YmdHis").'_'.md5( Utils::getSessionVar('idUser').'_'.Utils::getSessionVar('idEnterprise').'_'.microtime(true).'_'.rand(0,100) );
      // ** Usuarios
    $sql = "insert into toundo (idelement, code)
                      select u.id, '{$randomCode}a'
                      from user u, enterprise_user_role eur
                      where u.isdeleted = 0
                        and eur.isdeleted = 0
                        and eur.idEnterprise = {$idEnterprise}
                        and eur.idUser = u.id
                        and eur.idRole = ".Role::_RESPONSIBLE."
                        and ( select count(*) as num from enterprise_user_role eur2 where eur2.idUser = u.id and eur2.isdeleted = 0 ) = 1 -- Solo tiene este rol
                          $fName
                          $fSurname
                          $fEmail
                          $fLanguage
                          {$fInclude} {$fExclude}
                        ";
    $connection = Yii::app()->dbExtranet;
    $command = $connection->createCommand($sql);
    if ( !empty($fName2) ) $command->bindParam(":fName", $fName2, PDO::PARAM_STR);
    if ( !empty($fSurname2) ) $command->bindParam(":fSurname", $fSurname2, PDO::PARAM_STR);
    if ( !empty($fEmail2) ) $command->bindParam(":fEmail", $fEmail2, PDO::PARAM_STR);
    if ( !empty($fLanguage2) ) $command->bindParam(":fLanguage", $fLanguage2, PDO::PARAM_INT);

    $command->execute();

      // ** Roles.
    $sql = "insert into toundo (idelement, code)
                      select eur.id, '{$randomCode}b'
                      from user u, enterprise_user_role eur
                      where u.isdeleted = 0
                        and eur.isdeleted = 0
                        and eur.idEnterprise = {$idEnterprise}
                        and eur.idUser = u.id
                        and eur.idRole = ".Role::_RESPONSIBLE."
                          $fName
                          $fSurname
                          $fEmail
                          $fLanguage
                          {$fInclude} {$fExclude}
                        ";
    $connection = Yii::app()->dbExtranet;
    $command = $connection->createCommand($sql);
    if ( !empty($fName2) ) $command->bindParam(":fName", $fName2, PDO::PARAM_STR);
    if ( !empty($fSurname2) ) $command->bindParam(":fSurname", $fSurname2, PDO::PARAM_STR);
    if ( !empty($fEmail2) ) $command->bindParam(":fEmail", $fEmail2, PDO::PARAM_STR);
    if ( !empty($fLanguage2) ) $command->bindParam(":fLanguage", $fLanguage2, PDO::PARAM_INT);

    $command->execute();

    $sql = "update " . User::model()->tableName() . " u
              inner join abaenglish_extranet.toundo t on
                u.id = t.idElement and t.code = '" . $randomCode . "'
            set
              u.deleted = now(),
              u.isdeleted = 1";
    $command = Yii::app()->dbExtranet->createCommand($sql);
    $command->execute();

    $sql = "update " . EnterpriseUserRole::model()->tableName() . " eur
              inner join abaenglish_extranet.toundo t on
                eur.id = t.idElement and t.code = '" . $randomCode . "b' and eur.idEnterprise = $idEnterprise
            set
              eur.deleted = now(),
              eur.isdeleted = 1";
    $command = Yii::app()->dbExtranet->createCommand($sql);
    $command->execute();

    return $randomCode;

  }

  public static function getLastErrorMsg() {
    return User::$lastErrorMsg;
  }

  public static function getAgentByCountry( $isoCountry ) {
    if (isset(Yii::app()->params['agentByCountry'])) {
      $arAgentByCountry = Yii::app()->params['agentByCountry'];
    } else $arAgentByCountry = array( 'default' => 4 );
    if ( !isset($arAgentByCountry['default']) ) $arAgentByCountry['default'] = 4;

    $isoCountry = strtolower($isoCountry);
    $idAgent = ( isset($arAgentByCountry[$isoCountry]) ) ? $arAgentByCountry[$isoCountry] : $arAgentByCountry['default'];

    return $idAgent;
  }

  static public function getABAManagerList() {

    $connection = Yii::app()->dbExtranet;
    $sql = "select u.* from user u, enterprise_user_role eur where eur.idUser = u.id and eur.idRole = ".Role::_ABAMANAGER." and u.isdeleted = 0 and eur.isdeleted = 0 ";
    $command = $connection->createCommand($sql);
    $mlUsers_tmp = $command->queryAll();

    foreach( $mlUsers_tmp as $rowUser ) {
      $mUser = new stdClass();
      foreach( $rowUser as $key => $value ) $mUser->$key = $value;
      $mlUsers[] = $mUser;
    }

    return $mlUsers;
  }

  static public function getAbaManagerMails() {
    $mlUsers = self::getABAManagerList();
    $arMailsAbaManager = array();

    foreach( $mlUsers as $mUser ) {
      $arMailsAbaManager[] = array( 'name' => $mUser->name.' '.$mUser->surname, 'mail' => $mUser->email );
    }

    return $arMailsAbaManager;
  }

  static public function getTeachers( $idEnterprise, $setDefaultTeacher = false ) {
    $idRole = Role::_TEACHER;

    $sql = "select u.id, u.name, u.surname, CONCAT(u.name, ' ', u.surname) as fullname
            from user u, enterprise_user_role eur
            where eur.idEnterprise = :idEnterprise
              and eur.idRole = :idRole
              and eur.idUser = u.id
              and eur.isdeleted = 0 ";
    $connection = Yii::app()->dbExtranet;
    $command = $connection->createCommand($sql);
    $command->bindParam(":idEnterprise", $idEnterprise, PDO::PARAM_INT);
    $command->bindParam(":idRole", $idRole, PDO::PARAM_INT);
    $mlTeachers = $command->queryAll();

    if ( $setDefaultTeacher ) {
      array_unshift($mlTeachers, [
        'id' => 0,
        'name' => Yii::app()->getModule('student')->t('Default Teacher'),
        'surname' => '',
        'fullname' => Yii::app()->getModule('student')->t('Default Teacher'),
      ]);
    }

    return $mlTeachers;
  }

}
