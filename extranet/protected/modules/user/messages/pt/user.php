<?php
return array(
	'You are not registered yet?' => 'Ainda não está registrado?', 
	'Forgot your password?' => 'Esqueceu sua senha?', 
	'ADD NEW USER' => 'ADICIONAR NOVO USUÁRIO', 
	'Surname' => 'Sobrenome', 
	'Password' => 'Senha', 
	'PASSWORD' => 'SENHA', 
	'Old password' => 'Antiga senha', 
	'New password' => 'Nova senha', 
	'Detail for user' => 'Detalhes do usuário', 
	'Email:' => 'E-mail:', 
	'Error while sending your email.' => 'Erro ao enviar-lhe o e-mail', 
	'Error adding user.' => 'Erro ao adicionar usuário', 
	'This is your new password:' => 'Esta é sua nova senha:', 
	'Language' => 'Língua', 
	'COURSE LANGUAGE' => 'LÍNGUA DO CURSO', 
	'Profile picture' => 'Imagem da conta', 
	'LOGIN' => 'INICIAR SESSÃO', 
	'Update account' => 'Alterar conta', 
	'Remember me next time' => 'Lembrar-me da próxima vez', 
	'Re type new password' => 'Repita a nova senha', 
	'Telf:' => 'Tel.:', 
	'User successfully added.' => 'Usuário adicionado com sucesso', 
	'User/s' => 'Usuário/s', 
	'RECOVER PASSWORD' => 'RECUPERAR SENHA', 
	'Your email' => 'Seu email', 
	'RECOVER' => 'RECUPERAR', 
	'Return to login page' => 'Voltar à página de início', 
	'Error while sending your email.' => 'Erro ao enviar-lhe o email', 
	'We have sent a new password.' => 'Acabamos de lhe enviar uma nova senha', 
	'Select a new profile picture' => 'Selecione foto de perfil', 
	'User is unkown.' => 'O usuário não existe', 
	'Acceda a su curso de inglés para empresas' => 'Aceda a seu curso de inglês para empresas', 
	'Recuperar contraseña' => 'Recuperar senha', 
	'Ficha usuario' => 'Ficha usuário', 
	'Select profile picture' => 'Selecionar foto de perfil', 
);
