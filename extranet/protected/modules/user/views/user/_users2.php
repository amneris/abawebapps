<div id="divTabUser">
    <table width="100%">
        <tr>
            <td>
                <span class="sectionTitle"><?php echo $model->name." ".$model->surname; ?></span>
            </td>
        </tr>
    </table>
    <hr class="hrOverTitle">
  
<div class="withBorderBottom">
    <table class="userTable">
        <tr onmouseover="js:showEditImg('user_name_IMG')" onmouseout="js:hideEditImg('user_name_IMG')">
            <td width="40px" class="paddingTop10">
            </td>
            <td width="250px" class="paddingTop10">
              <span class="formLabel"><?php echo UserModule::t('Name'); ?></span> <br>
            </td>
            <td class="paddingTop10">

            </td>
        </tr>
        <tr onmouseover="js:showEditImg('user_name_IMG')" onmouseout="js:hideEditImg('user_name_IMG')">
            <td class="paddingBottom10">
               <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px" id="user_name_IMG" style="display: none" class="yeap configTableEditLink">
            </td>
            <td class="paddingBottom10">
                <span class="configTableValue configTableEditLink" id="user_name_SPAN"><?php echo $model->name; ?></span>
            </td>
            <td class="paddingBottom10">
              <button id='user_name_BOT' class='abaButton abaButton-Primary' style="margin-left:40px; display: none"><?php echo Yii::t('app','Save'); ?></button>
            </td>
        </tr>
    </table>
</div>

<div class="withBorderBottom">
    <table class="userTable">
        <tr onmouseover="js:showEditImg('user_surname_IMG')" onmouseout="js:hideEditImg('user_surname_IMG')">
            <td width="40px" class="paddingTop10">
            </td>
            <td width="250px" class="paddingTop10">
                <span class="formLabel"><?php echo UserModule::t('Surname'); ?></span> <br>
            </td>
            <td class="paddingTop10">

            </td>
        </tr>
        <tr onmouseover="js:showEditImg('user_surname_IMG')" onmouseout="js:hideEditImg('user_surname_IMG')">
            <td class="paddingBottom10">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px" id="user_surname_IMG" style="display: none" class="yeap configTableEditLink">
            </td>
            <td class="paddingBottom10">
                <span class="configTableValue configTableEditLink" id="user_surname_SPAN"><?php echo $model->surname; ?></span>
            </td>
            <td class="paddingBottom10">
                <button id='user_surname_BOT' class='abaButton abaButton-Primary' style="margin-left:40px; display: none"><?php echo Yii::t('app','Save'); ?></button>
            </td>
        </tr>
    </table>
</div>

<div class="withBorderBottom">

    <table class="userTable">
        <tr onmouseover="js:showEditImg('user_email_IMG')" onmouseout="js:hideEditImg('user_email_IMG')">
            <td width="40px" class="paddingTop10">
            </td>
            <td width="250px" class="paddingTop10">
                <span class="formLabel"><?php echo UserModule::t('Email'); ?></span> <br>
            </td>
            <td class="paddingTop10">

            </td>
        </tr>
        <tr onmouseover="js:showEditImg('user_email_IMG')" onmouseout="js:hideEditImg('user_email_IMG')">
            <td class="paddingBottom10">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px" id="user_email_IMG" style="display: none" class="yeap configTableEditLink">
            </td>
            <td class="paddingBottom10">
                <span class="configTableValue configTableEditLink" id="user_email_SPAN"><?php echo $model->email; ?></span>
            </td>
            <td class="paddingBottom10">
                <button id='user_email_BOT' class='abaButton abaButton-Primary' style="margin-left:40px; display: none"><?php echo Yii::t('app','Save'); ?></button>
            </td>
        </tr>
    </table>
</div>

<div class="withBorderBottom">

    <table class="userTable">
        <tr onmouseover="js:showEditImg('user_password_IMG')" onmouseout="js:hideEditImg('user_password_IMG')">
            <td width="40px" class="paddingTop10">
            </td>
            <td width="250px" class="paddingTop10">
                <span class="formLabel"><?php echo UserModule::t('Password'); ?></span> <br>
            </td>
            <td class="paddingTop10">

            </td>
        </tr>
        <tr onmouseover="js:showEditImg('user_password_IMG')" onmouseout="js:hideEditImg('user_password_IMG')">
            <td class="paddingBottom10">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px" id="user_password_IMG" style="display: none" class="yeap configTableEditLink">
            </td>
            <td class="paddingBottom10">
                <span class="configTableValue configTableEditLink" id="user_password_SPAN">********</span>
            </td>
            <td class="paddingBottom10">
                <button id='user_password_BOT' class='abaButton abaButton-Primary' style="margin-left:40px; display: none"><?php echo Yii::t('app','Save'); ?></button>
            </td>
        </tr>
    </table>
</div>

<div>
    <table class="userTable">
        <tr onmouseover="js:showEditImg('user_idLanguage_IMG')" onmouseout="js:hideEditImg('user_idLanguage_IMG')">
            <td width="40px" class="paddingTop10">

            </td>
            <td width="250px" class="paddingTop10">
                <span class="formLabel"><?php echo UserModule::t('Language'); ?></span><br>
            </td>
            <td class="paddingTop10">

            </td>
        </tr>
        <tr onmouseover="js:showEditImg('user_idLanguage_IMG')" onmouseout="js:hideEditImg('user_idLanguage_IMG')">
            <td class="paddingBottom10">
                <a href="#" style="cursor: hand"><img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px" id="user_idLanguage_IMG" style="display: none" class="yeap" onclick="js:showAcceptBut('user_idLanguage_')"></a>
            </td>
            <td class="paddingBottom10">
                <?php
                    foreach($dropDownLang as $key => $item)
                    {
                        $dropDownLang[$key]=Yii::t('app',$item);
                    }
                    echo chtml::dropDownList('user_idLanguage_INPUT',$model->idLanguage, $dropDownLang,array('onchange'=>"js:showAcceptBut('user_idLanguage_')"));
                ?>
            </td>
            <td class="paddingBottom10">
                <button id='user_idLanguage_BOT' class='abaButton abaButton-Primary' style="margin-left:40px; display: none" onclick="js:saveLang();"><?php echo Yii::t('app','Save'); ?></button>
            </td>
        </tr>
    </table>
</div>
<br>

<div style="text-align: center">
  <button id="btnLinkVolverUser" class="abaButton abaButton-Primary"><?php echo Yii::t('app','Back') ?></button>
</div>

<script>
  $(document).ready( function() {
    
    $('#btnLinkVolverUser').click( function() {
      ABAShowLoading();

      var url = '<?php echo Yii::app()->baseUrl; ?>/user/user/AjaxList';
      var parameters = {};
      var successF = function(data) {
        if ( data.ok ) {
          ABAStopLoading();
          $('#divTabUser').html(data.html);
        }
      };
      var errorF = function() { console.log('Level Test error'); ABAStopLoading(); };
      var doneF = function() { };

      ABALaunchAjax(url, parameters, successF, errorF, doneF);
    });
    
    $('.configTableEditLink').click( function() {
      var idInput = this.id.replace('SPAN', '');
      var idInput = idInput.replace('IMG', '');
      
      ABAsconfig_Transform( <?php echo $model->id; ?>, idInput, $( '#'+idInput+'SPAN' ).html() );
    });
    
  });

    function saveLang()
    {
        var newLang = $('#idLanguage').val();
        ABAsconfig_saveValue(<?php echo $model->id ?>, 'user_idLanguage_')
    }

</script>
</div>