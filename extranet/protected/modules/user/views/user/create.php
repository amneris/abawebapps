<?php
/* @var $this UserController */
/* @var $model BaseUser */

$selectedIndex = 'Configuracion';
?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>
<div class="row-fluid">
    <div class="col-lg-12 marginTop15">
        <span class="sectionTitle">Create user</span>

        <?php $this->renderPartial('_creationForm', array('model'=>$model, 'dropDownLang'=>$language, 'dropDownRole'=>$role, 'dropDownEnterprise'=>$enterprise)); ?>
    </div>
</div>