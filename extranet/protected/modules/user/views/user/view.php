<?php
/* @var $this UserController */
/* @var $model BaseUser */

$selectedIndex = 'Configuracion';
?>
<?php /*$this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) );*/ ?>

<div class="row-fluid">
    <div class="col-lg-12 marginTop15">
        <span class="sectionTitle"><?php echo UserModule::t('Detail for user'); echo ' '.$model->email; ?></span>

        <?php $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                'id',
                'name',
                'surname',
                /*'password',*/
                'email',
                array(
                    'name'=>$model->getAttributeLabel('idLanguage'),
                    'value'=>$model->language->name,
                ),
                'lastupdated',
                'created',
                'deleted',
                'isdeleted',
            ),
        )); ?>
    </div>
</div>

