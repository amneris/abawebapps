<?php
/* @var $this GroupController */
/* @var $model Group */

$selectedIndex = 'Configuracion';
?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>

<div class="row-fluid">
    <div class="col-lg-12 marginTop15">
        <br>
        <?php
            $this->renderPartial('_users', array('model'=>$model,'dropDownLang'=>$dropDownLang,));
        ?>
    </div>
</div>

<?php
if($resultMessage<>'')
{
    ?>
    <script>
        $(document).ready( function()
        {
            ABACustomDialog( 'ERROR', '<?php echo $resultMessage; ?>', false, true );
        });
    </script>
<?php
}
?>
