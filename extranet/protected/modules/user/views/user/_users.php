<?php
$fileLogoUser = Utils::getSessionVar('userAvatar');

?>
<table width="100%">
    <tr>
        <td>
            <span class="sectionTitle"><?php echo $model->name." ".$model->surname; ?></span>
        </td>
    </tr>
</table>
<hr class="hrOverTitle">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'name-form',
    'enableAjaxValidation'=>false,
)); ?>
<div class="withBorderBottom">
    <table class="userTable">
        <tr onmouseover="js:showEditImg('nameIMG')" onmouseout="js:hideEditImg('nameIMG')">
            <td width="40px" class="paddingTop10">
            </td>
            <td width="250px" class="paddingTop10">
                <span class="formLabel"><?php echo UserModule::t('Name'); ?></span> <br>
            </td>
            <td class="paddingTop10">

            </td>
        </tr>
        <tr onmouseover="js:showEditImg('nameIMG')" onmouseout="js:hideEditImg('nameIMG')">
            <td class="paddingBottom10">
                <a href="#" style="cursor: hand"><img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px" id="nameIMG" style="display: none" class="yeap" onclick="js:transform('name','<?php echo $model->name; ?>')"></a>
            </td>
            <td class="paddingBottom10">
                <span class="userTableValue" onclick="js:transform('name','<?php echo $model->name; ?>')" id="nameINPUT"><?php echo $model->name; ?></span>
            </td>
            <td class="paddingBottom10">
                <button id='nameBOT' class='abaButton abaButton-Primary' style="margin-left:40px; display: none"><?php echo Yii::t('app','Save'); ?></button>
            </td>
        </tr>
    </table>
</div>
<?php $this->endWidget(); ?>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'surname-form',
    'enableAjaxValidation'=>false,
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="withBorderBottom">
    <table class="userTable">
        <tr onmouseover="js:showEditImg('surnameIMG')" onmouseout="js:hideEditImg('surnameIMG')">
            <td width="40px" class="paddingTop10">
            </td>
            <td width="250px" class="paddingTop10">
                <span class="formLabel"><?php echo UserModule::t('Surname'); ?></span> <br>
            </td>
            <td class="paddingTop10">

            </td>
        </tr>
        <tr onmouseover="js:showEditImg('surnameIMG')" onmouseout="js:hideEditImg('surnameIMG')">
            <td class="paddingBottom10">
                <a href="#" style="cursor: hand"><img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px" id="surnameIMG" style="display: none" class="yeap" onclick="js:transform('surname','<?php echo $model->surname; ?>')"></a>
            </td>
            <td class="paddingBottom10">
                <span class="userTableValue" onclick="js:transform('surname','<?php echo $model->surname; ?>')" id="surnameINPUT"><?php echo $model->surname; ?></span>
            </td>
            <td class="paddingBottom10">
                <button id='surnameBOT' class='abaButton abaButton-Primary' style="margin-left:40px; display: none"><?php echo Yii::t('app','Save'); ?></button>
            </td>
        </tr>
    </table>
</div>
<?php $this->endWidget(); ?>


<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'email-form',
    'enableAjaxValidation'=>false,
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="withBorderBottom">

    <table class="userTable">
        <tr onmouseover="js:showEditImg('emailIMG')" onmouseout="js:hideEditImg('emailIMG')">
            <td width="40px" class="paddingTop10">
            </td>
            <td width="250px" class="paddingTop10">
                <span class="formLabel"><?php echo UserModule::t('Email'); ?></span> <br>
            </td>
            <td class="paddingTop10">

            </td>
        </tr>
        <tr onmouseover="js:showEditImg('emailIMG')" onmouseout="js:hideEditImg('emailIMG')">
            <td class="paddingBottom10">
                <a href="#" style="cursor: hand"><img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px" id="emailIMG" style="display: none" class="yeap" onclick="js:transform('email','<?php echo $model->email; ?>')"></a>
            </td>
            <td class="paddingBottom10">
                <span class="userTableValue" onclick="js:transform('email','<?php echo $model->email; ?>')" id="emailINPUT"><?php echo $model->email; ?></span>
            </td>
            <td class="paddingBottom10">
                <button id='emailBOT' class='abaButton abaButton-Primary' style="margin-left:40px; display: none"><?php echo Yii::t('app','Save'); ?></button>
            </td>
        </tr>
    </table>
</div>
<?php $this->endWidget(); ?>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'password-form',
    'enableAjaxValidation'=>false,
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="withBorderBottom">

    <table class="userTable">
        <tr onmouseover="js:showEditImg('passwordIMG')" onmouseout="js:hideEditImg('passwordIMG')">
            <td width="40px" class="paddingTop10">
            </td>
            <td width="250px" class="paddingTop10">
                <span class="formLabel"><?php echo UserModule::t('Password'); ?></span> <br>
            </td>
            <td class="paddingTop10">

            </td>
        </tr>
        <tr onmouseover="js:showEditImg('passwordIMG')" onmouseout="js:hideEditImg('passwordIMG')">
            <td class="paddingBottom10">
                <a href="#" style="cursor: hand"><img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px" id="passwordIMG" style="display: none" class="yeap" onclick="js:transform('password','<?php echo $model->password; ?>')"></a>
            </td>
            <td class="paddingBottom10">
                <span class="formLabel" onclick="js:transform('password','<?php echo $model->password; ?>')" id="passwordINPUT">********</span>
            </td>
            <td class="paddingBottom10">
                <button id='passwordBOT' class='abaButton abaButton-Primary' style="margin-left:40px; display: none"><?php echo Yii::t('app','Save'); ?></button>
            </td>
        </tr>
    </table>
</div>
<?php $this->endWidget(); ?>

<div class="withBorderBottom">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'lang-form',
        'enableAjaxValidation'=>false,
    )); ?>
    <table class="userTable">
        <tr onmouseover="js:showEditImg('langIMG')" onmouseout="js:hideEditImg('langIMG')">
            <td width="40px" class="paddingTop10">

            </td>
            <td width="250px" class="paddingTop10">
                <span class="formLabel"><?php echo UserModule::t('Language'); ?></span><br>
            </td>
            <td class="paddingTop10">

            </td>
        </tr>
        <tr onmouseover="js:showEditImg('langIMG')" onmouseout="js:hideEditImg('langIMG')">
            <td class="paddingBottom10">
                <a href="#" style="cursor: hand"><img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px" id="langIMG" style="display: none" class="yeap"></a>
            </td>
            <td class="paddingBottom10">
                <?php
                  foreach($dropDownLang as $key => $item) {
                      $dropDownLang[$key]=Yii::t('app',$item);
                  }
                echo chtml::dropDownList('lang',$model->idLanguage, $dropDownLang,array('onchange'=>'js:showAcceptBut(\'lang\')'));
                ?>
            </td>
            <td class="paddingBottom10">
                <button id='langBOT' class='abaButton abaButton-Primary' style="margin-left:40px; display: none"><?php echo Yii::t('app','Save'); ?></button>
            </td>
        </tr>
    </table>
</div>
<?php $this->endWidget(); ?>

<div>
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'lang-form',
        'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    )); ?>
    <table class="userTable">
        <tr onmouseover="js:showEditImg('langIMG')" onmouseout="js:hideEditImg('langIMG')">
            <td width="40px" class="paddingTop10">

            </td>
            <td width="250px" class="paddingTop10">
                <span class="formLabel">
                    <?php
                    if (!empty($fileLogoUser)) {
                        echo UserModule::t('Select a new profile picture');
                    } else {
                        echo UserModule::t('Select profile picture');
                    }
                    ?>
                </span>
                <br>
            </td>
            <td class="paddingTop10">

            </td>
        </tr>
        <tr onmouseover="js:showEditImg('profileIMG')" onmouseout="js:hideEditImg('profileIMG')">
            <td>
                <a href="#" style="cursor: hand"><img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/update.png" width="12px" id="profileIMG" style="display: none" class="yeap"></a>
            </td>
            <td>
                <input type="file" name="fileName" id="fileName" size="40" onchange="showAcceptBut('profile')">
            </td>
            <td>
                <button id='profileBOT' class='abaButton abaButton-Primary' style="margin-left:40px; display: none"><?php echo Yii::t('app','Save'); ?></button>
            </td>
        </tr>
    </table>
</div>
<?php $this->endWidget(); ?>
<br><br>

<?php if (!empty($fileLogoUser)) { ?>
<div style="align-content: center">
    <p align="center">
        <?php echo Yii::t('app','If you need it, you can crop de image to adjust the logo and keep a good appearance.'); ?>
        <br><br>
        <img id="cropbox" src="<?php echo getenv('AWS_URL_DOMAIN').$fileLogoUser; ?>?x=<?php echo rand();?>" style="margin-right: 0.5em; display: none;">
        <br>
        <button id="cropBut" class='abaButton abaButton-Primary'><?php echo Yii::t('app','Crop'); ?></button>
    </p>
    <input type="hidden" id="x" name="x" />
    <input type="hidden" id="y" name="y" />
    <input type="hidden" id="w" name="w" />
    <input type="hidden" id="h" name="h" />
</div>
<?php } else { ?>
    <img id="cropbox" src="" style="display: none;">
<?php } ?>

<br>
<div style="text-align: center; margin-bottom: 2em">
    <button id="btnBack" type="button" class="abaButton abaButton-Primary"><?php echo Yii::t('app','Back'); ?></button>
</div>

<script>

    function createCropObject()
    {
        var jcrop_api;

        jcrop_api = $.Jcrop('#cropbox', {
            onSelect: updateCoords,
            bgColor: 'white',
            bgOpacity: .3,
            aspectRatio: 1
        });

        $('#cropBut').click( function(e)
        {
            e.preventDefault();

            if(checkCoords())
            {
                //ABAShowLoading();
                var parametros = {
                    x : $('#x').val(),
                    y: $('#y').val(),
                    w : $('#w').val(),
                    h : $('#h').val()
                };

                $.ajax({
                    data:  parametros,
                    url: "/user/user/crop",
                    context: document.body,
                    type: 'POST',
                    dataType:"json",
                    success: function(data) {
                        if (data.result)
                        {
                            jcrop_api.setImage(data.urlAvatar);
                            $('#userIMG').attr('src',data.urlAvatar);
                        }
                        else
                        {
                            ABACustomDialog( '!!!', '<?php echo CJavaScript::quote(Yii::t('app','There was a problem cropping the image.')); ?>', false, true );
                        }

                        //ABAStopLoading();
                    },
                    error: function(jqXHR, textStatus, error) {
                        ABAStopLoading();
                        alert(jqXHR + textStatus + error);
                    },
                }).done(function() {
                    //
                });
            }});
    }

    function showEditImg(editField)
    {
        $( "#"+editField ).show()
    }
    function hideEditImg(editField)
    {
        if($( "#"+editField).hasClass("yeap"))
        {
            $( "#"+editField ).hide();
        }
    }
    function transform(id,value)
    {
        $('#' + id + 'IMG').show();
        $('#' + id + 'IMG').removeClass("yeap");
        if(id =='password')
        {
            $('#' + id +'INPUT').replaceWith("<input class='form-control addCampStudent' placeholder='<?php echo UserModule::t('Old password'); ?>' size='35' type='password' id='old"+id+"' name='old"+id+"'><input class='form-control addCampStudent' style='margin-top:3px' placeholder='<?php echo UserModule::t('New password'); ?>' size='35' type='password' id='new"+id+"' name='new"+id+"'> <input class='form-control addCampStudent' style='margin-top:3px' placeholder='<?php echo UserModule::t('Re type new password'); ?>' size='35' type='password' id='re"+id+"' name='re"+id+"'>");
        }
        else
        {
            $('#' + id +'INPUT').replaceWith("<input class='form-control addCampStudent' size='35' type='text' id='"+id+"' name='"+id+"' value='"+value+"'> ");
        }
        $('#' + id + 'BOT').show();

    }
    function showAcceptBut(id)
    {
        $('#' + id + 'IMG').show();
        $('#' + id + 'IMG').removeClass("yeap");
        $('#' + id + 'BOT').show();
    }
    function disableAcceptButAndImg(id)
    {
        $('#' + id + 'IMG').hide();
        $('#' + id + 'BOT').hide();
    }

    function updateCoords(c)
    {
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
    };

    function checkCoords()
    {
        if (parseInt($('#w').val())) return true;
        alert('<?php echo CJavaScript::quote(Yii::t('app','Please select a crop region then press CROP.')); ?>');
        return false;
    };

    $(document).ready( function()
    {

        if(typeof $('#cropbox') != 'undefined')
        {
            $('#cropbox').onloadend
            {
                setTimeout(function(){createCropObject();ABARedrawBody();}, 200);

            }
        }

        $('#btnBack').click(function () {
            var pageURL = '/';
            var parameters = {};

            ABAChangePage(pageURL, parameters);

        });
    });

</script>