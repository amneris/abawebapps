<?php
/* @var $this UserController */
/* @var $model User */

$selectedIndex = 'Configuracion';

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#base-user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>

<div class="row-fluid">
    <div class="col-lg-12 marginTop15">

        <span class="sectionTitle">User magnament</span>
	    <br>
        <?php echo CHtml::link('Create new user',array('user/create')); ?>
        <br>

        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'base-user-grid',
            'dataProvider'=>$dataProvider,
            'selectableRows' => 1,
            'cssFile' => Yii::app()->request->baseUrl . '/css/gridView.css',
            'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
                           'prevPageLabel'  => '«',
                           'nextPageLabel'  => '»',
                           'header'=>''),
            //'filter'=>$model,
            'columns'=>array(
                //'id',
	            array(
		            'name'=>'surname',
		            'value'=>'$data->surname.", ".$data->name',
	            ),
                /*'password',*/
                'email',
                array(
                    'name'=>'idLanguage',
                    'value'=>'$data->language->name',
                ),
                'lastupdated',
                /*
                'created',
                'deleted',
                'isdeleted',
                */
                array(
                    'class'=>'CButtonColumn',
                    'htmlOptions' => array('style' => 'width: 70px; text-align: center;'),
                    'header'=>'Options',
                    'buttons' => array(
                        'update'=>array(
                            'imageUrl'=>Yii::app()->request->baseUrl.'/images/icons/update.png',
                        ),
                        'delete'=>array(
                            'imageUrl'=>Yii::app()->request->baseUrl.'/images/icons/delete.png',
                        ),
                        'view'=>array(
                            'imageUrl'=>Yii::app()->request->baseUrl.'/images/icons/view.png',
                        ),
                    ),
                ),
            ),
        )); ?>
    </div>
</div>