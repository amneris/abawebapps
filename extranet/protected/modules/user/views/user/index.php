<?php 
  $module = 'user';
?>
<div id="divTabUser">
<div class="row-fluid">
  <div class="col-lg-12">

    <div class="row-fluid">
      <div class="col-lg-4">
        <div id="divDisplayFiltros_<?php echo $module; ?>" class='divDisplayFiltros'><span class="newRegLabel"><?php echo UserModule::t('ADD NEW USER'); ?> <img id='imgDisplayFiltros_<?php echo $module; ?>' src='<?php echo Yii::app()->baseUrl; ?>/images/less.png' width="11" height="11"></span></div>
      </div>
      <div class="col-lg-8">
        <div><span class="spanMsgList spanMsgAddList" id="msgList_1_<?php echo $module; ?>" style="display:none;"></span></div>
      </div>
    </div>

    <div class="row-fluid">
      <div class="col-lg-12">
        <div class="table-responsive divTableFiltros" id="divTableFiltros_<?php echo $module; ?>">
          <table class="table tableFiltros">
            <tr>
              <td class="tdNewreg">
                <?php echo CHtml::label( CHtml::encode( Yii::t('app', 'NAME') ) , 'newUserName'); ?><br>
                <?php echo CHtml::textField('newUserName', '', array( 'id' => 'newUserName', 'class' => 'form-control addCamp_'.$module )); ?>
              </td>
              <td class="tdNewreg">
                <?php echo CHtml::label( CHtml::encode( Yii::t('app', 'SURNAMES') ) , 'newUserSurname'); ?><br>
                <?php echo CHtml::textField('newUserSurname', '', array( 'id' => 'newUserSurname', 'class' => 'form-control addCamp_'.$module )); ?>
              </td>
              <td class="tdNewreg">
                <?php echo CHtml::label( CHtml::encode( UserModule::t('EMAIL') ) , 'newUserEmail'); ?><br>
                <?php echo CHtml::textField('newUserEmail', '', array( 'id' => 'newUserEmail', 'class' => 'form-control addCamp_'.$module )); ?>
              </td>
              <td class="tdNewreg">
                <?php echo CHtml::label( CHtml::encode( UserModule::t('PASSWORD') ) , 'newUserPassword'); ?><br>
                <?php echo CHtml::passwordField('newUserPassword', '', array( 'id' => 'newUserPassword', 'class' => 'form-control addCamp_'.$module )); ?>
              </td>
              
              <td class='tdNewreg'>
                  <?php echo CHtml::label( strtoupper(UserModule::t('Language')) , 'newUserLanguage'); ?><br>
                  <?php
                  $listLang = CHtml::listData( $mlLanguages,'id', 'name');
                  foreach($listLang as $key => $item) {
                    $listLang[$key]=Yii::t('app',$item);
                  }
                  ?>
                  <?php echo CHtml::dropDownList('newUserLanguage', Language::_ENGLISH, $listLang, array( 'id' => 'newUserLanguage', 'style' => 'min-width: 9em;', 'class' => 'form-control addCamp_'.$module )); ?>
              </td>
              <td class="tdNewreg">
                <button id="addButton_<?php echo $module; ?>" type="button" class="abaButton abaButton-Primary" style="margin-top: 21px; width: 100%; padding-left: 2em; padding-right: 2em;">
                  <?php echo Yii::t('app','ADD'); ?>
                </button>
              </td>
            </tr>
          </table>
        </div>
        
      </div>
    </div>
    
    <div class="row-fluid">
        <div class="col-lg-12 divTableList" id="divTableList_<?php echo $module; ?>">
          &nbsp;
        </div>
    </div>
    
    <div class="row-fluid">
      <div class="col-lg-12">
        &nbsp;
      </div>
    </div>

  </div>
</div>

<script>
  var OrderBy_<?php echo $module; ?> = 'created desc';
  var checkedList_<?php echo $module; ?> = '';
  
  $(document).ready( function() {
    
    ABAActivarQueryTooltips();
    
    $('#divDisplayFiltros_<?php echo $module; ?>').click( function() {

        $('#divTableFiltros_<?php echo $module; ?>').parent().parent().toggle();

        if ( $('#divTableFiltros_<?php echo $module; ?>').parent().parent().is(':visible') ) {
            $('#imgDisplayFiltros_<?php echo $module; ?>').attr('src', '<?php echo Yii::app()->baseUrl; ?>/images/less.png');
        } else {
            $('#imgDisplayFiltros_<?php echo $module; ?>').attr('src', '<?php echo Yii::app()->baseUrl; ?>/images/plus.png');
        }

    });
//    $('#divDisplayFiltros_<?php echo $module; ?>').trigger('click');
    
    $('.addCamp_<?php echo $module; ?>').keydown( function (e) {
        if ( e.keyCode == 13 ) $('#addButton_<?php echo $module; ?>').trigger('click');
    });

    $('#addButton_<?php echo $module; ?>').click( function(e) {
      e.preventDefault();
      var module = '<?php echo $module; ?>';

      if ( $('#newUserName').val() == '' ) {
          alert('<?php echo CJavaScript::quote(Yii::t('app','Name must not be empty.')); ?>');
          $('#newStudentName').focus();
          return false;
      }

      if ( $('#newUserSurname').val() == '' ) {
          alert('<?php echo CJavaScript::quote(Yii::t('app','Surname must not be empty.')); ?>');
          $('#newStudentSurname').focus();
          return false;
      }

      if ( $('#newUserEmail').val() == '' ) {
          alert('<?php echo CJavaScript::quote(Yii::t('app','Email must not be empty.')); ?>');
          $('#newUserEmail').focus();
          return false;
      }

      if ( $('#newUserPassword').val() == '' ) {
        alert('<?php echo CJavaScript::quote(Yii::t('app','Password must not be empty.')); ?>');
        $('#newUserPassword').focus();
        return false;
      }

      if ( !ABAValidateEmail( $('#newUserEmail').val() ) ) {
          alert('<?php echo CJavaScript::quote(Yii::t('app','Incorrect email.')); ?>');
          $('#newUserEmail').focus();
          return false;
      }

      ABAShowLoading();
      var url = "/"+module+"/"+module+"/insert";
      var parametros = {
        newUserName     : $('#newUserName').val(),
        newUserSurname  : $('#newUserSurname').val(),
        newUserEmail    : $('#newUserEmail').val(),
        newUserLanguage : $('#newUserLanguage').val(),
        newUserPassword : $('#newUserPassword').val(),
      };

      var successF = function(data) {
        if ( data.ok ) {
          ABAShowOkMsgList( '<?php echo $module;  ?>', data.okMsg, 1 );
          $('#newUserName').val('');
          $('#newUserSurname').val('');
          $('#newUserEmail').val('');
          $('#newUserLanguage').val('');
          $('#newUserPassword').val(0);

          ABARefreshList( '<?php echo $module; ?>' );
        } else {
          ABAShowErrorMsgList( '<?php echo $module;  ?>', data.errorMsg, 1 );
        }

        ABAStopLoading();
      }

      ABALaunchAjax(url, parametros, successF);
    });

    ABARefreshList( '<?php echo $module; ?>' );
  });
  
</script>
</div>