<?php
//  Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl.'/css/login.css');
?>

<?php /*$form=$this->beginWidget('CActiveForm', array(
    'id'=>'student-recovery-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // See class documentation of CActiveForm for details on this,
    // you need to use the performAjaxValidation()-method described there.
    'enableAjaxValidation'=>false,
)); */?>

<div id='divMainBody'>
  <div id="divMarco">
    <div id="divHeaderMarco"></div>
    <div id="divLogoMarco"></div>
    
    <div id='divTituloMarco'><span><?php echo Yii::app()->getModule('user')->t2('RECOVER PASSWORD'); ?></span></div>
    <?php if ( !empty($errorMsg) ) { ?>
    <?php if ( $allOk ) { ?>
    <div id='divMsgMarco2' class="alert alert-success"><?php echo $errorMsg; ?></div>
    <?php } else { ?>
    <div id='divMsgMarco2' class="alert alert-danger"><?php echo $errorMsg; ?></div>
    <?php } ?>
    <?php } ?>
    
    <div class='divInputMarco'>
      <?php /*echo $form->textField($model, 'username', array('size'=>60,'maxlength'=>128, 'placeholder'=> Yii::app()->getModule('user')->t2('Your email'), 'class'=>'form-control', 'id' => 'inputMail')); */ ?>
      <?php echo CHtml::textField('UserLogin[username]', $model->username, array('size'=>60,'maxlength'=>128, 'placeholder'=> Yii::app()->getModule('user')->t2('Your email'), 'class'=>'form-control', 'id' => 'inputMail')); ?>
    </div>
    <div id='divButton'><button id='btnLogin' class="abaButton abaButton-Primary"><?php echo Yii::app()->getModule('user')->t2('RECOVER'); ?></button></div>
    <div id="divFooterMarco2">
      <div><img src='/images/icologinarrow.png' width="6" height="8">&nbsp;&nbsp;<span><?php echo Yii::app()->getModule('user')->t2('Return to login page'); ?></span></div>
    </div>
  </div>
</div>
<script>
  $(document).ready( function() {
    
    $('#btnLogin').click( function() {
      var pageURL = '<?php echo Yii::app()->getModule('user')->recoveryUrl; ?>';
      var parameters = {
        'UserLogin[username]': $('#inputMail').val(),
      };
      ABAChangePage( pageURL, parameters );
    });
    
    $('#divFooterMarco2 div span').click( function() {
      var pageURL = '<?php echo Yii::app()->getModule('user')->loginUrl; ?>';
      var parameters = {};
      ABAChangePage( pageURL, parameters );
    });
  });
</script>