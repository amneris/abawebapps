<?php
  $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login");
//  Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl.'/css/login.css');
  
  $selectedIndex = 1;
  
?>
<?php /* $this->renderPartial('//extranet/menulogin', array('selectedIndex' => $selectedIndex) ); */ ?>

<div id="content" style="clear: both;">
  <div class="testbox">

      <hr>
      <hr>
      <div class="title"><?php echo UserModule::t("Recovery info has been send"); ?></div>
      <p style="font-size: 20px; color:green; text-align: center;">
        <img src="/images/ok.png" width="48" style="margin: 20px;">
      </p>

      <p style="padding: 20px; padding-top: 0px; text-align: justify;">
        <?php echo UserModule::t('Revise su mail. En breve recibirá un mail con instrucciones para la recuperación de la contraseña.'); ?>
      </p>

      <div style="text-align: center">
          <?php echo CHtml::submitButton(UserModule::t("Ok"),array('class'=>'button', 'id' => 'btnOk')); ?>
      </div>
      
  </div>
</div>
<script>

  $(document).ready( function() {
    $('#btnOk').click( function () {
      var pageURL = '/user/login/';
      var parameters = {};
      
      ABAChangePage( pageURL, parameters );
    });
  });
</script>  