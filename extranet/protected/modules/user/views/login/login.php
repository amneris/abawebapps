<?php ?>
<div id='divMainBody'>
  <div>
    <div id="divMarco">
      <div id="divHeaderMarco"></div>
      <div id="divLogoMarco"></div>

      <div id='divTituloMarco'><span><?php echo Yii::app()->getModule('user')->t2('LOGIN'); ?></span></div>

  <?php $form=$this->beginWidget('CActiveForm', array(
      'id'=>'student-login-form',
      // Please note: When you enable ajax validation, make sure the corresponding
      // controller action is handling ajax validation correctly.
      // See class documentation of CActiveForm for details on this,
      // you need to use the performAjaxValidation()-method described there.
      'enableAjaxValidation'=>false,
  )); ?>

      <?php if ( !empty($errorMsg) ) { ?>
      <div id='divMsgMarco' class="alert alert-danger"><?php echo $errorMsg; ?></div>
      <?php } ?>

      <div class='divInputMarco'>
        <?php echo $form->textField($model, 'username', array('size'=>60,'maxlength'=>128, 'placeholder'=> Yii::app()->getModule('user')->t2('Your email'), 'class'=>'form-control', 'id' => 'inputUser')); ?>
      </div>
      <div class='divInputMarco'>
        <?php echo $form->passwordField($model, 'password', array('placeholder'=>Yii::app()->getModule('user')->t2('Password'), 'class'=>'form-control', 'id' => 'inputPass')); ?>
      </div>

      <div id='divRecordar'>
        <?php  $model->rememberMe = Yii::app()->user->histrenemberme; ?>
        <?php echo $form->checkBox($model, 'rememberMe', array('id' => 'inputRenemberMe') ); ?>
        <?php echo CHtml::label( Yii::app()->getModule('user')->t2('Remember me next time'), 'inputRenemberMe', array('style' => 'font-weight: normal; font-size:0.9em;') ); ?>
      </div>
      <div id='divButton'><button id='btnLogin' class="abaButton abaButton-Primary"><?php echo Yii::app()->getModule('user')->t2('LOGIN'); ?></button></div>
      <div id="divFooterMarco">
        <div><span id='spanRecovery'><?php echo Yii::app()->getModule('user')->t2('Forgot your password?'); ?></span></div>
        <div><span id="spanRegister"><?php echo Yii::app()->getModule('user')->t2('You are not registered yet?'); ?></span></div>
      </div>
    </div>
  </div>
<?php $this->endWidget(); ?>
  <div id="push"></div>
</div>

<script>
  $(document).ready( function() {
    $('#divFooterMarco div span#spanRecovery').click( function() {
      var pageURL = '<?php echo Yii::app()->getModule('user')->recoveryUrl; ?>';
      var parameters = {};
      ABAChangePage( pageURL, parameters );
    });
    
    $('#divFooterMarco div span#spanRegister').click( function() {
      var pageURL = '<?php echo Yii::app()->getModule('user')->registerLandingUrl; ?>';
      var parameters = {};
      ABAChangePage( pageURL, parameters );
    });
    
    $('#inputUser').focus();
    
    
  });
</script>