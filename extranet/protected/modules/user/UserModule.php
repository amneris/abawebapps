<?php
/**
 * Yii-User module
 * 
 * @author Mikhail Mangushev <mishamx@gmail.com> 
 * @link http://yii-user.2mx.org/
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @version $Id: UserModule.php 132 2011-10-30 10:45:01Z mishamx $
 */

class UserModule extends CWebModule
{

  public $returnUrl = '/extranet';
  public $recoveryUrl = '/user/login/recovery';
  public $registrationUrl = '';
  public $loginUrl = '/user/login';
  public $logoutUrl = '/user/login/logout';
  public $registerLandingUrl = 'http://www.abaenglish.com/es/corporate/';
  
	public function init()
    {
        parent::init();
        
        // import the module-level models and components
        $this->setImport(array(
              'user.components.*',
              'user.models.*',
              'user.models._selfgenerated.*',
        ));

        Yii::app()->setComponents( array() );
        
      $path = realpath(Yii::app()->basePath . '/views/layouts');
      $this->setLayoutPath( $path );
	}
	
	/**
	 * @param $str
	 * @param $params
	 * @param $dic
	 * @return string
	 */
	public static function t($str='',$params=array(),$dic='user') {
    $translation = Yii::t("UserModule", $str);
    if ( $translation == $str ) $translation = Yii::t("UserModule.".$dic, $str, $params);
    if ( $translation == $str ) $translation = Yii::t("app", $str, $params);

    return $translation;
    
//		if (Yii::t("UserModule", $str)==$str)
//		    return Yii::t("UserModule.".$dic, $str, $params);
//        else
//            return Yii::t("UserModule", $str, $params);
	}
    
	public function t2($str='',$params=array(),$dic='user') {
    return self::t( $str, $params, $dic );
	}
    
}
