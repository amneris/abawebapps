<?php

class LoginController extends Controller
{
  public $defaultAction = 'login';

  public function init() {
    parent::init();

    $this->layout = 'login';
  }

  /**
   * 
   */
  private function setPOST2AutoLogin() {
    if ( isset($_GET['code']) ) {
      $code = $_GET['code'];
      $mUser = User::model()->findByAttributes( array('autologincode' => $code) );
      if( $mUser ) {
        $bool = $mUser->validatePassword( $mUser->password );
        
        if ( !isset($_POST['UserLogin']) ) $_POST['UserLogin'] = array();
        if( !isset($_POST['UserLogin']['username']) ) $_POST['UserLogin']['username'] = $mUser->email;
        if( !isset($_POST['UserLogin']['password']) ) $_POST['UserLogin']['password'] = $code;
        if( !isset($_POST['UserLogin']['rememberMe']) ) $_POST['UserLogin']['rememberMe'] = 0;
      }
    }
  }
  
  /**
   * Displays the login page
   */
  public function actionLogin() {
    $idChannel = Yii::app()->request->getQuery('idChannel', 0);

    $errorMsg = Yii::app()->request->getParam('errorMsg', '');
      // ** Si ya esta validado.
    if ( !Yii::app()->user->isGuest ) {
      if ( Utils::getSessionVar('idEnterprise', 0) ) {
        $this->redirect( '/student/main' );
      } else $this->redirect( Utils::getSessionVar('homeUrl') );
    }

      // ** Si se hace autologin
    $this->setPOST2AutoLogin();

      // ** Do login.
    if(isset($_POST['UserLogin'])) {
      $model=new UserLogin;
      $model->attributes=$_POST['UserLogin'];
        // ** validate user input and redirect to previous page if valid
      if($model->validate()) {
//        $this->loadSessionData( $model->id );
        if ( Utils::getSessionVar('idEnterprise', 0) ) $this->redirect( '/student/main/index/idChannel/'.$idChannel );
        else $this->redirect( Utils::getSessionVar('homeUrl') );
      }
      $errorMsg = Yii::t('app','User or password is incorrect.');
    }

      // ** display the login form
    $model=new UserLogin;
    $this->render('login',array('model'=>$model, 'errorMsg' => $errorMsg ));
  }
	
  
  /**
   * Logs out the current user and redirect to homepage.
   */
  public function actionLogout()
  {
    $loginUrl = Yii::app()->getModule('user')->loginUrl;

      // Logout the current user
    Yii::app()->user->logout();

    $this->redirect( $loginUrl );
  }
  
  /**
   * 
   */
  public function actionRecovery()
  {
    $errorMsg = '';
    $allOk = true;
    if(isset($_POST['UserLogin'])) {
      $model=new UserLogin;
      $model->attributes=$_POST['UserLogin'];
      $mUser = User::getUser( $model->username );
      
      if ( $mUser ) {
        $allOk = $this->sendNewPassword( $mUser );
        if ( !$allOk ) $errorMsg = Yii::app()->getModule('user')->t2('Error while sending your email.');
        else $errorMsg = Yii::app()->getModule('user')->t2("We have sent a new password.");
      } else { $errorMsg = UserModule::t('User is unkown.'); $allOk = false; }
    }

      // ** display the login form
    if ( $allOk ) $model=new UserLogin;
    $this->render('recovery',array('model'=>$model, 'errorMsg' => $errorMsg, 'allOk' => $allOk ));
  }

  protected function sendNewPassword(User $mUser) {
    $newPass = $this->generateNewPassword();
    $mUser->password = User::getNewPassword($newPass);
    $allOk = $mUser->save();
//file_put_contents('/tmp/extra.log', __CLASS__.'.'.__FUNCTION__.": $newPass\n", FILE_APPEND);
    
    $arParameters = array(
      'action' => HeAbaSelligent::_RECOVERYPASS,
      'idUser' => $mUser->id,
      'password' => $newPass,
      'to' => $mUser->email,
      'msg' => Yii::app()->getModule('user')->t2('This is your new password: ').$newPass,
    );
    return $allOk && HeAbaMail::sendEmail($arParameters);
  }
  
  protected function generateNewPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $alphabet = "abcdefghijklmnpqrstuwxyzABCDEFGHIJKLMNPQRSTUWXYZ123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
  }
  
}