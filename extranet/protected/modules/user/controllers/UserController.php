<?php

class UserController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='extranet';
    private $pageLimit = 7;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }

  /**
   * Specifies the access control rules.
   * This method is used by the 'accessControl' filter.
   * @return array access control rules
   */
  public function accessRules() {

    return array(
      array('allow',
        'actions'=>array('index','view','admin','update','ajaxUpdate','create','delete','undodelete','refresh', 'AjaxList', 'ajaxUpdateValue', 'insert', 'crop'),
        'roles'=>array(Role::_RESPONSIBLE, Role::_ABAMANAGER, Role::_AGENT, Role::_TEACHER),
      ),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin', 'update', 'crop'),
				'users'=>array('@'),
			),
      array('deny',  // deny all users
        'users'=>array('*'),
      ),
    );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
    {
		$model = new User;

/*        $dropDownProviderLang = Language::model()->findAll(array('order'=>'name')); */
        $dropDownProviderLang = Language::model()->findAllByAttributes(array('enabled'=>1));
        $listLang = CHtml::listData($dropDownProviderLang, 'id', 'name');

        $dropDownProviderRole = Role::model()->findAllByAttributes(array('isdeleted'=>0),array('order'=>'name'));
        $listRole = CHtml::listData($dropDownProviderRole, 'id', 'name');

        $dropDownProviderEnterprise = Enterprise::model()->findAllByAttributes(array('isdeleted'=>0),array('order'=>'name'));
        $listEnterprise = CHtml::listData($dropDownProviderEnterprise, 'id', 'name');


        if(isset($_POST['User']))
        {
            $idRole = Yii::app()->request->getParam('idRole','');
            $idEnterprise = Yii::app()->request->getParam('idEnterprise','');

            $model->attributes=$_POST['User'];
            if($model->save())
            {
                //we create the relationship with enterprise and role.
                $userRole = new EnterpriseUserRole();
                $userRole->idEnterprise = $idEnterprise;
                $userRole->idUser = $model->id;
                $userRole->idRole = $idRole;
                if($userRole->save())
                {
                    $this->redirect(array('view','id'=>$model->id));
                }
            }

        }

        $this->render('create',array(
            'model'=>$model, 'language'=>$listLang, 'role'=>$listRole, 'enterprise'=>$listEnterprise
        ));
    }

    public function actionInsert() {
      $allOk = true; $errorMsg = ''; $okMsg = GroupModule::t('User successfully added.');
      
      $idEnterprise = Utils::getSessionVar('idEnterprise');
      $idRole = Yii::app()->request->getParam('newUserRole', Role::_RESPONSIBLE);
      $name = Yii::app()->request->getParam('newUserName');
      $surname = Yii::app()->request->getParam('newUserSurname');
      $email = Yii::app()->request->getParam('newUserEmail');
      $lang = Yii::app()->request->getParam('newUserLanguage');
      $password = Yii::app()->request->getParam('newUserPassword');

      // ** Language
      $criteria = new CDbCriteria();
      $criteria->addCondition('t.id = :code');
      $criteria->params = array(':code' => $lang);
      $mLanguage = Language::model()->find($criteria);
      if (!$mLanguage) {
          $allOk = false;
          $errorMsg = Yii::t( 'app', 'Language is not valid.');
      }

      if ( $allOk ) {
          $criteria = new CDbCriteria();
          $criteria->addCondition('t.email = :email');
          $criteria->addCondition('t.isdeleted = :deleted');
          $criteria->params = array(':email' => $email, ':deleted' => 0);
          $mUser = User::model()->find($criteria);

          if ( $mUser ) {
              $criteria = new CDbCriteria();
              $criteria->addCondition('t.idUser = :idUser');
              $criteria->addCondition('t.idRole = :idRole');
              $criteria->addCondition('t.isdeleted = :deleted');
              $criteria->params = array(':idUser' => $mUser->id, ':deleted' => 0, 'idRole' => Role::_RESPONSIBLE);
              $mEnterpriseUserRole = EnterpriseUserRole::model()->find($criteria);
          }

          if ($mUser && $mEnterpriseUserRole) {
              $allOk = false;
              $errorMsg = UserModule::t('User already exists.');
          } else {
              if ( !$mUser ) {
                  $mUser = new User;
                  $mUser->name = $name;
                  $mUser->surname = $surname;
                  $mUser->email = $email;
                  $mUser->idLanguage = $lang;
                  $mUser->password = User::getNewPassword($password);
              }

              $transaction = Yii::app()->dbExtranet->beginTransaction();
              try {
                  $allOk = $mUser->save();
                  if (!$allOk) throw new Exception(UserModule::t('Error, something went wrong while trying to save the user.'));

                  //we create the relationship with enterprise and role.
                  $userRole = new EnterpriseUserRole();
                  $userRole->idEnterprise = $idEnterprise;
                  $userRole->idUser = $mUser->id;
                  $userRole->idRole = $idRole;
                  $allOk = $userRole->save();
                  if (!$allOk) throw new Exception(UserModule::t('Role could not be added.'));

                  HeAbaMail::sendEmail(array(
                      'action' => HeAbaSelligent::_EDITUSER,
                      'idUser' => $mUser->id,
                      'idEnterprise' => $idEnterprise,
                      'name' => $mUser->name,
                      'surname' => $mUser->surname,
                      'email' => $mUser->email,
                      'password' => $password,
                      'isoLanguage' => $mLanguage->iso,
                      'callback' => 'ExtranetController::actionMailSent',
                  ));

                  $transaction->commit();
              } catch (Exception $e) {
                  $transaction->rollBack();
                  $errorMsg = $e->getMessage();
                  $allOk = false;
              }
          }
      }

      echo json_encode(array('ok' => $allOk, 'errorMsg' => $errorMsg, 'okMsg' => $okMsg));
    }

  /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id, $action = 'update')
    {

        $resultMessage='';
        /*$dropDownProvider = Language::model()->findAll(array('order'=>'name'));*/
        $dropDownProvider = Language::model()->findAllByAttributes(array('enabled'=>1));
        $list = CHtml::listData($dropDownProvider, 'id', 'name');

        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['name']))
        {
            $model->name = $_POST['name'];
          if($model->save()) {
            $this->updateSelligentData( $model );
            $this->redirect(array('update','id'=>$model->id));
          }
        }

        if(isset($_POST['surname']))
        {
            $model->surname = $_POST['surname'];
          if($model->save()) {
            $this->updateSelligentData( $model );
            $this->redirect(array('update','id'=>$model->id));
          }

        }

        if(isset($_POST['email']))
        {
            $model->email = $_POST['email'];
          if($model->save()) {
            $this->updateSelligentData( $model );
            $this->redirect(array('update','id'=>$model->id));
          }
        }

        if(isset($_POST['lang']))
        {
            $model->idLanguage = $_POST['lang'];
            if($model->save())
            {
              $this->updateSelligentData( $model );
                $lang = new Language();
                $lang = $lang->findByPk($model->idLanguage);
                Utils::setSessionVar('userLanguage', $lang['iso']);
                $this->redirect(array('update','id'=>$model->id));
            }

        }

        if (isset($_FILES['fileName'])) {
            $arDatos = HeUtils::uploadImg(array('scaleX'=>150, 'scaleY'=>150, 'folder'=>'users'));
            $resultMessage = $arDatos['resultMessage'];
            HeAmazon::queueFile2Delete($model->urlAvatar);
            $model->urlAvatar = $arDatos['urlImg'];
            Utils::setSessionVar('userAvatar', $model->urlAvatar );
            if($model->save()) {
//                $this->updateSelligentData( $model );
                $this->redirect(array('update','id'=>$model->id));
            }
        }

        if(isset($_POST['oldpassword']))
        {
            //we validate that old passwords match
            if($model->validatePassword($_POST['oldpassword']))
            {
                //we validate that new password match
                if($_POST['newpassword']==$_POST['repassword'])
                {
                  $model->password = crypt($_POST['newpassword']);
                  if($model->save()) {
                    $this->updateSelligentData( $model );
                    $this->redirect(array('update','id'=>$model->id));
                  }
                }
            }

//            $model->idLanguage = $_POST['lang'];
            if($model->save())
                $this->redirect(array('update','id'=>$model->id,'resultMessage'=>$resultMessage));
        }

        if ( $action == 'update' ) {
            $this->render($action,array(
                'model'=>$model,'dropDownLang'=>$list,
                'resultMessage'=>$resultMessage,
            ));
        } else {
            $html = $this->renderPartial( $action, array(
                'model'=>$model,'dropDownLang'=>$list,
                'resultMessage'=>$resultMessage,
            ), true);
            echo json_encode( array( 'ok' => true, 'html' => $html ) );
        }
    }

    protected function updateSelligentData( $mUser ) {

      $idEnterprise = Utils::getSessionVar('idEnterprise');

      // ** Language
      $criteria = new CDbCriteria();
      $criteria->addCondition('t.id = :code');
      $criteria->params = array(':code' => $mUser->idLanguage );
      $mLanguage = Language::model()->find($criteria);

      HeAbaMail::sendEmail(array(
        'action' => HeAbaSelligent::_EDITUSER,
        'idUser' => $mUser->id,
        'idEnterprise' => $idEnterprise,
        'name' => $mUser->name,
        'surname' => $mUser->surname,
        'email' => $mUser->email,
        'password' => '',
        'isoLanguage' => $mLanguage->iso,
        'callback' => 'ExtranetController::actionMailSent',
      ));
      
    }

    public function actionCrop() {
        $x = Yii::app()->request->getParam('x');
        $y = Yii::app()->request->getParam('y');
        $w = Yii::app()->request->getParam('w');
        $h = Yii::app()->request->getParam('h');
        $dstW = $dstH = 150;

        try {
            $src = getenv('AWS_URL_DOMAIN').Utils::getSessionVar('userAvatar');
            $tmpFile = tempnam(Yii::app()->getRuntimePath(), 'im_');
            file_put_contents($tmpFile, fopen($src, 'r'));

            $imgR = imagecreatefrompng($tmpFile);
            $dstR = ImageCreateTrueColor($dstW, $dstH);
            imagesavealpha($dstR, true);
            $trans_colour = imagecolorallocatealpha($dstR, 0, 0, 0, 127);
            imagefill($dstR, 0, 0, $trans_colour);
            imagecopyresampled($dstR, $imgR, 0, 0, $x, $y, $dstW, $dstH, $w, $h);
            imagepng($dstR, $tmpFile, true);

            $mUser = User::model()->findByPk(Utils::getSessionVar('id'));

            $arDatos = HeUtils::uploadImg(array('inputFile' => $tmpFile, 'scaleX'=>150, 'scaleY'=>150, 'folder'=>'users'));
            $allOk = ($arDatos['resultMessage'] == '');
            HeAmazon::queueFile2Delete($mUser->urlAvatar);
            $urlLogo = $mUser->urlAvatar = $arDatos['urlImg'];
            Utils::setSessionVar('userAvatar', $mUser->urlAvatar);
            $allOk = $allOk && $mUser->save();

            $urlLogo = getenv('AWS_URL_DOMAIN').$urlLogo.'?x='.rand();
        } catch (Exception $e) {
            $allOk = false;
            $urlLogo = '';
        }

        echo json_encode(array('result' => $allOk, 'urlAvatar' => $urlLogo ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete() {
      $errorMsg = '';
      $okMsg = '';

      $idEnterprise = Utils::getSessionVar('idEnterprise');
      $checkedList = Utils::adminCheckedList( );
      $arFilter = Utils::getActualFilters( );
      if ( empty($checkedList) ) {
        $allOk = false;
        $errorMsg = Yii::app()->getModule('user')->t('No one user has been selected.');
      } else {
        $undoCode = User::deleteSelection( $checkedList, $idEnterprise, $arFilter );

        $allOk = ( $undoCode != false );
        if ( !$allOk ) $errorMsg = User::getLastErrorMsg();
        else $okMsg = Yii::app()->getModule('user')->t('The selected user has been removed.').'<a id=\'undoDeleteList_user\' data-undocode=\''.$undoCode.'\' href=\'#\'>'.Yii::app()->getModule('user')->t('Undo').'</a>';
      }

      echo json_encode( array( 'ok' => $allOk, 'errorMsg' => $errorMsg, 'okMsg' => $okMsg ) );
    }

  public function actionUndodelete()
  {
    $undoCode = Yii::app()->request->getParam( 'undoCode', 0 );
    $errorMsg = '';
    $okMsg = UserModule::t('The selected user has been restored.');

    $allOk = User::undoDeleteSelection( $undoCode );
    if ( !$allOk ) $errorMsg = User::getLastErrorMsg();

    echo json_encode(array('ok' => $allOk, 'errorMsg' => $errorMsg, 'okMsg' => $okMsg));
  }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider=new CActiveDataProvider('User');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new User('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['User']))
            $model->attributes=$_GET['User'];

/*        $mlLanguages = Language::model()->findAll(); */
        $mlLanguages = Language::model()->findAllByAttributes(array('enabled'=>1));

        $this->render('index2',array(
            'model'=>$model,
            'mlLanguages' => $mlLanguages,
        ));
//
//    
//    
//        switch (Utils::getSessionVar('idRole'))
//        {
//            case Role::_ABAMANAGER:
//                $rolePermission= Role::_ABAMANAGER.', '.Role::_RESPONSIBLE.', '.Role::_AGENT.', '.Role::_TEACHER;
//                break;
//            case Role::_AGENT:
//                $rolePermission= Role::_RESPONSIBLE.', '.Role::_AGENT.', '.Role::_TEACHER;
//                break;
//            case Role::_RESPONSIBLE:
//                $rolePermission= Role::_RESPONSIBLE.', '.Role::_TEACHER;
//                break;
//        }
//
//        $listOfUsersToShow = EnterpriseUserRole::model()->getUsersByRoleAndEnterprise($rolePermission, Utils::getSessionVar('idEnterprise'));
//
//        $criteria = new CDbCriteria();
//        $criteria->condition = 'id IN ('.$listOfUsersToShow.')';
//        $dataProvider=new CActiveDataProvider('User', array(
//            'criteria'=>$criteria,
//        ));
//
//		$this->render('admin',array(
//			'dataProvider'=>$dataProvider,
//		));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=User::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='base-user-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     *
     */
    public function actionRefresh()
    {
        $idEnterprise = Utils::getSessionVar('idEnterprise');
        $mEnterprise = Enterprise::model()->findByPk($idEnterprise);

        //we get from database all avalaible languages.
        $languages = Language::model()->getAvailableLanguages();

        $checkedList = Yii::app()->request->getParam('checkedList', '0');
        $checkedList = Utils::adminCheckedList( $checkedList );

        $pag = Yii::app()->request->getParam('pag', 1);
        $limit = $this->pageLimit;
        $offset = $limit * ($pag - 1);

        $orderBy = Yii::app()->request->getParam('OrderBy', 't.created desc');
        $orderBy = str_replace( array('th', 'UserList'), '', $orderBy );
        $orderBy = str_replace( '_', ' ', $orderBy );
        $orderBy = strtolower($orderBy);

        $filters = Yii::app()->request->getParam('filters', array());
        $arFilters = array();
        foreach( $filters as $k=>$filter ) {
            if ( empty($filter['value']) ) continue;
            if ( substr($filter['key'], 0, strlen('filter_') ) != 'filter_' ) continue;

            $key = substr($filter['key'], strlen('filter_') );
            $arFilters[$key] = $filter['value'];
        }

        $count = User::countUsersEnterprise($idEnterprise, $arFilters);
        $mlUsers = User::getUsersEnterprise($idEnterprise, $arFilters, $orderBy, $checkedList);

        if ( !empty($limit) ) {
            $maxPag = (int) ($count / $limit);
            if (($count % $limit) > 0) $maxPag = $maxPag + 1;
        } else $maxPag = 1;

        $html = $this->renderPartial('list', array( 'mEnterprise' => $mEnterprise,  'mlUsers' => $mlUsers, 'orderBy' => $orderBy, 'pag' => $pag, 'maxPag' => $maxPag, 'checkedList' => $checkedList,
            'numUsers' => $count, 'filters' => $arFilters, 'languages'=>$languages ), true );

        echo json_encode( array( 'ok' => 'true', 'html' => $html ) );
    }

    public function actionAjaxList() {
        $mlLanguages = Language::model()->findAll();
        $mlRoles = Role::model()->getUserCreationAvailableList(Utils::getSessionVar('idRole'));
        $html = $this->renderPartial('application.modules.user.views.user.index', array('mlLanguages' => $mlLanguages, 'mlRoles' => $mlRoles),true);

        echo json_encode(array( 'ok' => true, 'html' => $html ));
    }

    public function actionAjaxUpdate($id) {
        $this->actionUpdate($id, '_users2');
    }

    public function actionAjaxUpdateValue() {
        $allOk = true;
        $msgError = '';

        $id = Yii::app()->request->getParam('id');
        $key = Yii::app()->request->getParam('key');

        $newvalue = Yii::app()->request->getParam('value');

        $mUser = User::model()->findByPk($id);
        if ( $key == 'password') {
            $oldPassword = Yii::app()->request->getParam('oldpass');
            if ( $mUser->validatePassword($oldPassword) ) {
                $mUser->$key = User::getNewPassword($newvalue);
            } else {
                $msgError = UserModule::t('Password mismatch');
                $allOk = false;
            }
        } else {
            $mUser->$key = $newvalue;
        }
        $allOk = ($allOk && $mUser->save());

        if ( $allOk ) $this->updateSelligentData( $mUser );

        echo json_encode(array( 'ok' => $allOk, 'msgError' => $msgError ));
    }

}
