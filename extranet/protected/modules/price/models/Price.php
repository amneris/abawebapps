<?php

/**
 * This is the model class for table "products_prices".
 *
 * The followings are the available columns in table 'products_prices':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $idPeriodPay
 * @property string $priceOfficialCry
 * @property string $idCountry
 * @property integer $enabled
 * @property string $modified
 * @property Country country
 */
class Price extends BasePrice
{
    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'country'=>array(self::BELONGS_TO, 'Country', 'idCountry'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Price the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     *
     */
    public static function getPrices( $idCountry = 0, $idLicenseType = 0 ) {
        // ** Trial Prices.
      if ( $idLicenseType == LicenseType::_Trial ) {
        $mPrice = new stdClass();
        $mPrice->id = 5;
        $mPrice->name = 'Trial';
        $mPrice->idPeriodPay = '30';
        $mPrice->strDesc = 'Trial License';
        $mPrice->price = 0;
        $mPrice->priceLocal = 0;
        return array( 0 => $mPrice );
      }


        if ( !empty($idCountry) ) $fCountry = " and pp.idCountry = {$idCountry} "; else $fCountry = '';
        if ( !empty($idLicenseType) ) $fLicenseType = " and lt.id = $idLicenseType "; else $fLicenseType = '';

        $mlPrices = array();
        $connection = Yii::app()->dbExtranet;
        $sql = "select lt.id, lt.name, lt.idPeriodPay, lt.strDesc, (pp.priceExtranet * cu.unitEur) as priceExtranetLocal2, pp.priceExtranet as priceExtranetLocal, (pp.priceExtranet / cu.unitEur) as priceExtranet
              from license_type lt, aba_b2c.products_prices pp, aba_b2c.country c, aba_b2c.currencies cu
              where lt.isEnabled = true
                {$fLicenseType}
                and lt.idPeriodPay = pp.idPeriodPay
                {$fCountry}
                and pp.enabledExtranet = true
                and pp.idCountry = c.id
                and c.ABAIdCurrency = cu.id
              ";
        $command = $connection->createCommand( $sql );
        Yii::trace($command->getText(), "sql.select");
        try {
            $rs = $command->queryAll();

            foreach ( $rs as $row ) {
                $mPrice = new stdClass();
                $mPrice->id = $row['id'];
                $mPrice->name = $row['name'];
                $mPrice->idPeriodPay = $row['idPeriodPay'];
                $mPrice->strDesc = $row['strDesc'];
                $mPrice->price = $row['priceExtranet'];
                $mPrice->priceLocal = $row['priceExtranetLocal'];

                $mlPrices[] = $mPrice;
            }
        } catch (Exception $e) {
            Yii::log( 'Error to execute last SQL.', 'error', "sql.select" );
        }

        return $mlPrices;
    }

}
