<?php

/**
 * This is the model class for table "products_prices".
 *
 * The followings are the available columns in table 'products_prices':
 * @property string $idProduct
 * @property string $idCountry
 * @property string $idPeriodPay
 * @property string $priceOfficialCry
 * @property integer $enabled
 * @property string $modified
 * @property string $descriptionText
 * @property string $userType
 * @property string $hierarchy
 * @property integer $visibleWeb
 * @property string $priceExtranet
 * @property integer $enabledExtranet
 */
class BasePrice extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products_prices';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idProduct, idCountry, idPeriodPay, priceOfficialCry, modified, hierarchy', 'required'),
			array('enabled, visibleWeb, enabledExtranet', 'numerical', 'integerOnly'=>true),
			array('idProduct', 'length', 'max'=>15),
			array('idCountry', 'length', 'max'=>4),
			array('idPeriodPay', 'length', 'max'=>5),
			array('priceOfficialCry, priceExtranet', 'length', 'max'=>18),
			array('descriptionText', 'length', 'max'=>20),
			array('userType, hierarchy', 'length', 'max'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idProduct, idCountry, idPeriodPay, priceOfficialCry, enabled, modified, descriptionText, userType, hierarchy, visibleWeb, priceExtranet, enabledExtranet', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idProduct' => 'Id Product',
			'idCountry' => 'Id Country',
			'idPeriodPay' => 'Id Period Pay',
			'priceOfficialCry' => 'Price Official Cry',
			'enabled' => 'Enabled',
			'modified' => 'Modified',
			'descriptionText' => 'Description Text',
			'userType' => 'User Type',
			'hierarchy' => 'Hierarchy',
			'visibleWeb' => 'Visible Web',
			'priceExtranet' => 'Price Extranet',
			'enabledExtranet' => 'Enabled Extranet',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idProduct',$this->idProduct,true);
		$criteria->compare('idCountry',$this->idCountry,true);
		$criteria->compare('idPeriodPay',$this->idPeriodPay,true);
		$criteria->compare('priceOfficialCry',$this->priceOfficialCry,true);
		$criteria->compare('enabled',$this->enabled);
		$criteria->compare('modified',$this->modified,true);
		$criteria->compare('descriptionText',$this->descriptionText,true);
		$criteria->compare('userType',$this->userType,true);
		$criteria->compare('hierarchy',$this->hierarchy,true);
		$criteria->compare('visibleWeb',$this->visibleWeb);
		$criteria->compare('priceExtranet',$this->priceExtranet,true);
		$criteria->compare('enabledExtranet',$this->enabledExtranet);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BasePrice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
