<table height="100%" width='100%'>
<tr>
  <td>
    <div style="font-size: 20px; color:green; text-align: center;">
      <img src="/images/tick3.png" width="48" style="margin: 20px; width: 3.25em;">
    </div>

    <div style="text-align: center;">
      <span style='font-family: "MuseoSlab500", Arial, Sans-serif; font-weight: bold; font-size: 1.479166667em;'><?php echo PriceModule::t('Agent request done.'); ?></span><br><br>
      <span style='font-family: "MuseoSlab700", Arial, Sans-serif; font-size: 1.155em;'><?php echo PriceModule::t('A business agent will contact you as soon as possible.'); ?></span><br><br>
    </div>

    <div id="divBtnCloseOkLicense">
      <button id="btnCloseOkLicense" class="abaButton abaButton-Primary abaExtranetButton-Ok">
        <?php echo PriceModule::t('Ok'); ?>
      </button>
    </div>
  </td>
</tr>
<tr><td>&nbsp;</td></tr>
</table>

<script>
  $(document).ready( function() {
    $('#btnCloseOkLicense').click( function() {
      ABACloseDialog();
    });
  });
</script>  