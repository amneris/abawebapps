<?php

$selectedIndex = 'Prices';

$decimalPoint = Utils::getSessionVar('decimalPoint', ',');
$millarSeparator = ($decimalPoint == ',')? '.': ',';
$currencySymbol = Utils::getSessionVar('currencySymbol', '€');
$idCurrency = Utils::getSessionVar('idCurrency', 'EUR');
$numLicencias = $mlLicenseAvailable['Total']->numLicenses;
unset($mlLicenseAvailable['Total']);

?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>

<style>
  .spanPriceINeed {
    font-family: "MuseoSans700";
    padding-left: 2.142857143em;
    font-size: 1.166666667em;
  }
  
  .spanPriceINeed select {
    margin-left: 0.75em;
    margin-right: 0.75em;
    width: 3.2em;
  }

  .divPriceBox .divPriceBoxInner {
    text-align: center;
    border: 1px solid #cccccc; 
    height: 170px; 
  }
  
  .divPriceBox .divPriceBoxInner.active {
    border: 2px solid #2a9cda;
    margin-top: -1px;
  }
  
  .spanPriceFirst {
    text-transform: uppercase;
    font-family: "MuseoSans700";
    font-size: 1.166666667em;
  }
  
  .spanPriceSecond {
    text-transform: uppercase;
    font-family: "MuseoSans700";
    font-size: 1.166666667em;
  }
  
  .divPriceSecond {
    margin-top: 2.66667em;
  }
  
  .spanIfYouNeed {
    font-family: "MuseoSans500Regular";
/*    font-size: 1.166666667em; */
    padding-left: 1.666666667em;
    color: #FFFFFF;
    line-height: 3em;
  }
  
  #btnRequestAgent {
/*    font-family: "MuseoSans900"; */
    float: right;
    margin-top: 0.20em;
/*    font-size: 0.928333333em; */
  }
  
  .divContainerIfYouNeed {
    border: 1px solid #cccccc; 
    padding: 0.416666667em;  
    margin: 0px; 
    margin-top: 0.5em; 
    margin-bottom: 20px; 
    background-color: #36515A; 
    text-align: left; 
    min-width: 665px; 
    padding-right:10px;
  }

  .divPriceBoxInfo {
    clear:both;
    text-align: center;
  }
  
  .divPriceBoxInfo span {
    font-family: "MuseoSans700";
    font-size: 1em; 
    color:#455560;
  }

  .divIVA {
    float: right;
    font-size: 0.7em;
    margin-top: 0.8em;
    margin-right:0.2em;
  }

</style>

<div class="row-fluid">
    <div class="col-lg-12">

        <div class="row-fluid">
            <div class="col-lg-6">
                <div class="divSectionTittle">
                    <span class="sectionTitle"><?php echo CHtml::encode(Yii::t('app', 'Prices')); ?></span>
                </div>
            </div>
            <div class="col-lg-6 alignRight">
                <div class="divLicensesPannel">
                    <div class="divTitleAvailableLicenses"><span><?php echo CHtml::encode(StudentModule::t('Available licenses')); ?>: <?php echo $numLicencias; ?></span></div>
                    <?php foreach ($mlLicenseAvailable as $mLicenseAvailable) { ?>
                        <?php if ($mLicenseAvailable->id == LicenseType::_Trial) continue; ?>
                        <div class="licenseAvailable"><span><?php echo Yii::t('app', $mLicenseAvailable->name); ?>: <?php echo $mLicenseAvailable->numLicenses; ?></span></div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="col-lg-12">
              &nbsp;
            </div>
        </div>

        <div class="row-fluid">
          <div class="col-lg-12">
            <span class="spanPriceFirst"><?php echo CHtml::encode(PriceModule::t('1 - 20 Alumnos') ); ?></span>
          </div>
        </div>

        <div class="row-fluid">
        <?php
        $valCol= floor(12 / sizeof($mlPrices));
        foreach ( $mlPrices as $mPrice ) { ?>
            <?php
            if ( !empty($taxRates) ) $mPrice->priceLocal = $mPrice->priceLocal / $taxRates;

            $arLabels[$mPrice->idPeriodPay] = $mPrice->name;
            $price = (($mPrice->priceLocal / $mPrice->idPeriodPay) * 30);
            $units = (int)$price;
            $decimal = number_format( (number_format( ($price - $units), 2 ) * 100), 0);
            if ( strlen($decimal) == 3 ) $decimal = substr( $decimal, 0, 2 );
            if ( $units < 10 ) {
              $units = '&nbsp;&nbsp;'.$units;
            } else {
              $units = number_format($units, 0, $decimalPoint, $millarSeparator);
            }
            ?>
            <div class="<?php echo "col-lg-".$valCol ?>">
                <div class="divPriceBox" data-idperiodpay="<?php echo $mPrice->idPeriodPay; ?>">
                  <div id="price_<?php echo $mPrice->idPeriodPay; ?>" data-id='<?php echo $mPrice->id; ?>' data-price='<?php echo $mPrice->priceLocal; ?>' style="display:none;"></div>
                  <div class="divPriceBoxInner">
                      <div style="height: 5em; background-color: #36515A; text-align: center; line-height: 5em;">
                        <span style='font-family: "MuseoSans700"; font-size: 1.325833333em; color:#FFFFFF; text-transform: uppercase;'><?php echo Yii::t('app', $mPrice->name); ?></span>
                      </div>
                      <div style="margin:auto; width2: 11.666666667em; line-height2: 6.25em;">
                          <div style='display:inline-block;'><span style='font-family:"MuseoSans700"; font-size: 3.559166667em; color:#455560;'><?php echo $units; ?></span></div>
                          <div style="margin-top: 1.083333333em; margin-left2: 0.333333333em; line-height: 1.666666667em; padding-top: 0.37em;display:inline-block;">
                              <div><span style='font-family:"MuseoSans900"; font-size: 1.358333333em; color:#455560;'><?php echo $decimalPoint.str_pad($decimal, 2, '0'); ?></span></div>
                              <div><span style='font-family:"MuseoSans500Regular"; font-size: 1.254166667em; color:#455560;'>/<?php echo Yii::t('app', 'Month'); ?></span></div>
                          </div>
                      </div>
                      <?php $number1 = number_format( $mPrice->priceLocal, 2, $decimalPoint, $millarSeparator ); ?>
                      <?php $number2 = ($number1 * 1); ?>
                      <?php if ( "$number1" === "{$number2}{$decimalPoint}00" ) $number1 = $number2; ?>
                      <div class="divPriceBoxInfo" style="clear:both;text-align: center;">
                        <span style='font-size: 1em; color:#455560;'><?php echo HeViews::formatAmountInCurrency($number1,$idCurrency).'&nbsp;'; ?><?php echo PriceModule::t($mPrice->strDesc); if (!empty($taxRates)) echo '<sup>*</sup>'; ?></span>
                        <?php if ( !empty($taxRates) ) { ?>
                          <div class="divIVA"><span><sup>*</sup> <?php echo HeViews::formatAmountInCurrency(($mPrice->priceLocal * $taxRates),$idCurrency); echo " ".Yii::app()->getModule('price')->t('Precio con IVA.'); ?></span></div>
                        <?php } ?>
                      </div>
                  </div>
                </div>
            </div>
        <?php } ?>
        </div>

        <div class="row-fluid">
            <div class="col-lg-12">
                <div class="table-responsive" style="height: 50px; border: 1px solid #cccccc; padding: 0px; margin: 0px; margin-top: 20px;">
                    <table style="margin: 0px; padding: 0px; width: 100%; height: 100%; min-width: 665px;">
                        <tr>
                            <td id="tdPriceTotalLeft" width="100%" style="background-color: #f3f3f3; border-right: 1px solid #ccc;">
                              <?php 
                                $selectNumber = '<select id="sLicenses">'; 
                                for( $i=1; $i <= 20; $i++ ) {
                                  if ( $i == 1 ) $selectNumber .= '<option selected>'.$i.'</option>';
                                  else $selectNumber .= '<option>'.$i.'</option>';
                                }
                                $selectNumber .= '</select>';
                              ?>
                              
                              <?php 
                                $selectDuration = '<select id="sMonths">'; 
                                foreach( $arLabels as $number => $label ) {
                                  $months = (int)$number / 30;
                                  if ( $number == 360 ) $selectDuration .= '<option value='.$number.' selected>'.$months.'</option>';
                                  else $selectDuration .= '<option value='.$number.'>'.$months.'</option>';
                                }
                                $selectDuration .= '</select>';
                              ?>
                              
                              <?php $msg = PriceModule::t('I need {$selectNumber} license/s {$selectDuration} months duration.'); ?>
                              <?php eval("\$msg = \"$msg\";"); ?>
                              <span class="spanPriceINeed"><?php echo $msg; ?></span>
                                                            
                              
<!--                              
                            <span style="margin-left: 20px"><?php echo PriceModule::t('I need'); ?>
                                <select id="sLicenses">
                                    <?php for( $i=1; $i <= 20; $i++ ) { ?>
                                        <option <?php if ( $i == 1 ) echo "selected"; ?>><?php echo $i; ?></option>
                                    <?php } ?>
                                </select>
                                <?php echo PriceModule::t('licence/s'); ?>
                                <select id="sMonths">
                                    <?php foreach( $arLabels as $number => $label ) { ?>
                                        <?php $months = (int)$number / 30; ?>
                                        <option value='<?php echo $number; ?>' <?php if ( $number == 360 ) echo "selected"; ?>><?php echo $months ?></option>
                                    <?php } ?>
                                </select>
                                <?php echo PriceModule::t('months duration'); ?>
                            </span>
-->
                            </td>
                            <td style="background-color: #FFFFFF; white-space:nowrap; padding-right: 10px;">
                               <span style="margin-right: 20px; padding-left: 20px; font-size: 1.166666667em;">
                                   <?php echo PriceModule::t('TOTAL'); ?>&nbsp;&nbsp;
                                   <?php
                                   if($idCurrency === 'USD' || $idCurrency === 'BRL'){
                                        echo $currencySymbol;
                                   }
                                   ?>
                                   <span id='spanTotal' style='font-family:"MuseoSans900";'>0</span>
                                   <?php
                                   if($idCurrency === 'EUR' || $idCurrency === 'MXN'){
                                       echo $currencySymbol;
                                   }
                                   ?>
                               </span>
                                <button id='btnBuy' class='abaButton abaButton-Primary' style="width: 15em;">
                                    <?php echo PriceModule::t('REQUEST'); ?>
                                </button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="row-fluid">
          <div class="col-lg-12">
            
            <div class="divPriceSecond">
              <span class="spanPriceSecond"><?php echo CHtml::encode(PriceModule::t('Más de 20 alumnos') ); ?></span>
            </div>
            
          </div>
        </div>
      

        <div class="row-fluid">
            <div class="col-lg-12">
                <div class="divContainerIfYouNeed">
                  <span class="spanIfYouNeed"><?php echo PriceModule::t('If you need more than 20 licences, please, first contact to us.'); ?></span>
                  <button id='btnRequestAgent' class='abaButton abaButton-Primary'><?php echo PriceModule::t('SEND REQUEST TO AGENT'); ?></button>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
var selectedIndex = '<?php echo CJavaScript::quote($selectedIndex); ?>';
    $(document).ready( function() {

        $('#sLicenses').change( function() {
            recalcularTotal();
        });

        $('#sMonths').change( function() {
            recalcularTotal();
            setProduct( $(this).val() );
        });

        $('#btnBuy').click(function() {
            buyLicenses();
        });

        $('#btnRequestAgent').click(function() {
            requestAgent();
        });

        $('.divPriceBox').click( function() {
          setProduct( $(this).data("idperiodpay") );
        });

        recalcularTotal();
        
        ABARedrawBody();
    });

    function recalcularTotal() {

        var numLicenses = $('#sLicenses').val();
        var period = $('#sMonths').val();
        var price = $('#price_'+period).data('price');

        var total1 = parseFloat(numLicenses * price).toFixed(2);
        var total2 = parseInt( numLicenses * price);
        if ( total1 == total2 ) var total = total2; else var total = total1;
//        var total = parseFloat( numLicenses * price).toFixed(2);

        total = total + '';
        total = total.replace( '.', <?php echo CJavaScript::encode($decimalPoint); ?> );

        $('#spanTotal').html(total);
    }

    function setProduct(id) {
      $('#sMonths').val(id);

      $('.divPriceBox .divPriceBoxInner').each( function() {
        $( this ).removeClass("active");
      });

      $('.divPriceBox').each( function() {
        if ( $(this).data("idperiodpay") == id ) {
//            console.log("Preu "+id);
          $( this ).find( ".divPriceBoxInner" ).addClass("active");
        }
      });
      
      recalcularTotal();
    }

    function buyLicenses() {
        ABAShowLoading();
        var parametros = {
            licenses: $('#sLicenses').val(),
            period:  $('#price_'+$('#sMonths').val()).data('id'),
        };

        $.ajax({
            data:  parametros,
            url: "/price/main/buylicenses",
            context: document.body,
            type: 'POST',
            dataType:"json",
            success: function(data) {
                if ( data.ok ) {
                    ABAShowDialog( data.html, true );
                } else {
                    ABAShowDialog( data.html, true );
                }
            },
            error: function(jqXHR, textStatus, error) {
//          alert('error');
            },
        }).done(function() {
            //
        });
    }

    function requestAgent() {
        ABAShowLoading();

        var parametros = {
        };

        $.ajax({
            data:  parametros,
            url: "/price/main/requestAgent",
            context: document.body,
            type: 'POST',
            dataType:"json",
            success: function(data) {
                if ( data.ok ) {
                    ABAShowDialog( data.html, true );
                } else {
                    ABAShowDialog( data.html, true );
                }
            },
            error: function(jqXHR, textStatus, error) {
//          alert('error');
            },
        }).done(function() {
            //
        });
    }

</script>