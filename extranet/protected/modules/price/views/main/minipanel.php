<?php

$selectedIndex = 'Prices';

$decimalPoint = Utils::getSessionVar('decimalPoint', ',');
$millarSeparator = ($decimalPoint == ',')? '.': ',';
$currencySymbol = Utils::getSessionVar('currencySymbol', '€');
$idCurrency = Utils::getSessionVar('idCurrency', 'EUR');
$numLicencias = $mlLicenseAvailable['Total']->numLicenses;
unset($mlLicenseAvailable['Total']);

?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>

<style>

  .spanPriceINeed {
    font-family: "MuseoSans700";
    padding-left: 2.142857143em;
    font-size: 1.166666667em;
  }
  
  .spanPriceINeed select {
    margin-left: 0.25em;
    margin-right: 0.25em;
    width: 3.4em;
  }

  .divPriceBox .divPriceBoxInner {
    text-align: center;
    border: 1px solid #cccccc; 
    height: 154px;
  }
  
  .divPriceBox .divPriceBoxInner.active {
    border: 2px solid #2a9cda;
    margin-top: -1px;
  }
  
  .spanPriceFirst {
    text-transform: uppercase;
    font-family: "MuseoSans700";
    font-size: 1.166666667em;
  }
  
  .spanPriceSecond {
    text-transform: uppercase;
    font-family: "MuseoSans700";
    font-size: 1.166666667em;
  }
  
  .divPriceSecond {
    margin-top: 2.66667em;
  }
  
  .spanIfYouNeed {
    font-family: "MuseoSans500Regular";
/*    font-size: 1.166666667em; */
    padding-left: 1.666666667em;
    color: #FFFFFF;
    line-height: 3em;
  }
  
  #btnRequestAgent {
/*    font-family: "MuseoSans900"; */
    float: right;
    margin-top: 0.20em;
/*    font-size: 0.928333333em; */
  }
  
  .divContainerIfYouNeed {
    border: 1px solid #cccccc; 
    padding: 0.416666667em;  
    margin: 0px; 
    margin-top: 0.5em; 
    margin-bottom: 20px; 
    background-color: #36515A; 
    text-align: left; 
    min-width: 665px; 
    padding-right:10px;
  }

  .divPriceBoxInfo {
    clear:both;
    text-align: center;
  }
  
  .divPriceBoxInfo span {
    font-family: "MuseoSans700";
    font-size: 1em; 
    color:#455560;
  }
  
</style>

<div class="row-fluid">
    <div class="col-lg-12">

        <div class="row-fluid">
          <div class="col-lg-12">
            <span class="spanPriceFirst"><?php echo CHtml::encode(PriceModule::t('Comprar directa') ); ?></span>
          </div>
        </div>

        <div class="row-fluid">
        <!-- Trial License -->
        <div id="price_30" data-id='5' data-price='0' style="display:none;"></div>
        <?php
        $valCol= floor(12 / sizeof($mlPrices));
        foreach ( $mlPrices as $mPrice ) { ?>
            <?php
            if ( !empty($taxRates) ) $mPrice->priceLocal = $mPrice->priceLocal / $taxRates;

            $arLabels[$mPrice->idPeriodPay] = $mPrice->name;
            $price = (($mPrice->priceLocal / $mPrice->idPeriodPay) * 30);
            $units = (int)$price;
            $decimal = number_format( (number_format( ($price - $units), 2 ) * 100), 0);
            if ( strlen($decimal) == 3 ) $decimal = substr( $decimal, 0, 2 );
            if ( $units < 10 ) {
              $units = '&nbsp;&nbsp;'.$units;
            } else {
              $units = number_format($units, 0, $decimalPoint, $millarSeparator);
            }
            ?>
            <div class="<?php echo "col-lg-".$valCol ?>">
                <div class="divPriceBox" data-idperiodpay="<?php echo $mPrice->idPeriodPay; ?>">
                  <div id="price_<?php echo $mPrice->idPeriodPay; ?>" data-id='<?php echo $mPrice->id; ?>' data-price='<?php echo $mPrice->priceLocal; ?>' style="display:none;"></div>
                  <div class="divPriceBoxInner">
                      <div style="height: 5em; background-color: #36515A; text-align: center; line-height: 5em;">
                        <span style='font-family: "MuseoSans700"; font-size: 1.325833333em; color:#FFFFFF; text-transform: uppercase;'><?php echo Yii::t('app', $mPrice->name); ?></span>
                      </div>
                      <div style="margin:auto; width2: 11.666666667em; line-height2: 6.25em;">
                          <div style='display:inline-block;'><span style='font-family:"MuseoSans700"; font-size: 3.559166667em; color:#455560;'><?php echo $units; ?></span></div>
                          <div style="margin-top: 1.083333333em; margin-left2: 0.333333333em; line-height: 1.666666667em; padding-top: 0.37em;display:inline-block;">
                              <div><span style='font-family:"MuseoSans900"; font-size: 1.358333333em; color:#455560;'><?php echo $decimalPoint.str_pad($decimal, 2, '0'); ?></span></div>
                              <div><span style='font-family:"MuseoSans500Regular"; font-size: 1.254166667em; color:#455560;'>/<?php echo Yii::t('app', 'Month'); ?></span></div>
                          </div>
                      </div>
                      <?php $number1 = number_format( $mPrice->priceLocal, 2, $decimalPoint, $millarSeparator ); ?>
                      <?php $number2 = ($number1 * 1); ?>
                      <?php if ( "$number1" === "{$number2}{$decimalPoint}00" ) $number1 = $number2; ?>
                      <div class="divPriceBoxInfo" style="clear:both;text-align: center;"><span style='font-size: 1em; color:#455560;'><?php echo HeViews::formatAmountInCurrency($number1,$idCurrency).'&nbsp;'; ?><?php echo PriceModule::t($mPrice->strDesc); ?></span></div>
                  </div>
                </div>
            </div>
        <?php } ?>
        </div>

        <div class="row-fluid">
            <div class="col-lg-12">
                <div class="table-responsive" style="height: 50px; border: 1px solid #cccccc; padding: 0px; margin: 0px; margin-top: 20px;">
                    <table style="margin: 0px; padding: 0px; width: 100%; height: 100%; min-width: 665px;">
                        <tr>
                            <td id="tdPriceTotalLeft" width="100%" style="background-color: #f3f3f3; border-right: 1px solid #ccc;">
                              <table>
                              <tr>
                                <td>
                                  <?php
                                    $selectNumber = '<input type="text" id="sLicenses" style="width: 30px;text-align: right;" value="0">';
                                  ?>

                                  <?php
                                    $arLabels[30] = 'Monthly';
                                    ksort($arLabels);
                                    $selectDuration = '<select id="sMonths">';
                                    foreach( $arLabels as $number => $label ) {
                                      $months = (int)$number / 30;
                                      if ( $number == 360 ) $selectDuration .= '<option value='.$number.' selected>'.$months.'</option>';
                                      else $selectDuration .= '<option value='.$number.'>'.$months.'</option>';
                                    }
                                    $selectDuration .= '</select>';
                                  ?>

                                  <?php $msg = PriceModule::t('I need {$selectNumber} license/s {$selectDuration} months duration.'); ?>
                                  <?php eval("\$msg = \"$msg\";"); ?>
                                  <span class="spanPriceINeed"><?php echo $msg; ?></span>
                                </td>
                              </tr>
                                <tr>
                                  <td>
                                      <span class="spanPriceINeed">
                                          <?php echo PriceModule::t('Precio por licencia'); ?>:
                                          <?php
                                          if($idCurrency === 'USD' || $idCurrency === 'BRL'){
                                              echo $currencySymbol;
                                          }
                                          ?>
                                          <input type="text" id="precioFinal" style="width: 60px;text-align: right; margin-left: 5px;" value="0">
                                          <?php
                                          if($idCurrency === 'EUR' || $idCurrency === 'MXN'){
                                              echo $currencySymbol;
                                          }
                                          ?>
                                      </span>
                                  </td>
                                </tr>
                              </table>
                            </td>
                            <td style="background-color: #FFFFFF; white-space:nowrap; padding-right: 10px;">
                               <span style="margin-right: 20px; padding-left: 20px; font-size: 1.166666667em;">
                                   <?php echo PriceModule::t('TOTAL'); ?>&nbsp;&nbsp;
                                   <?php
                                   if($idCurrency === 'USD' || $idCurrency === 'BRL'){
                                       echo $currencySymbol;
                                   }
                                   ?>
                                   <span id='spanTotal' style='font-family:"MuseoSans900";'>0</span>
                                   <?php
                                   if($idCurrency === 'EUR' || $idCurrency === 'MXN'){
                                       echo $currencySymbol;
                                   }
                                   ?>
                               </span>
                                <button id='btnBuy' class='abaButton abaButton-Primary' style="width: 9em;">
                                    <?php echo PriceModule::t('REQUEST'); ?>
                                </button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
var selectedIndex = '<?php echo CJavaScript::quote($selectedIndex); ?>';
    $(document).ready( function() {

      $('#sLicenses').change( function() {
          recalcularTotal();
      });

      $('#sMonths').change( function() {
        var price = $('#price_'+$(this).val()).data('price');
        price = parseFloat(price, 10).toFixed(2);
        setPrice( price );
        recalcularTotal();
        setProduct( $(this).val() );
      });

      $('#precioFinal').change( function() {
        recalcularTotal();
      });

      $('#precioFinal').change( function() {
        recalcularTotal();
      });

      $( "#precioFinal" ).keyup(function() {
        var precioInicial = getPrice();
        if ( precioInicial == '' || precioInicial == '0' ) { setPrice( precioInicial ); recalcularTotal(); return; }
        if ( precioInicial == parseFloat(precioInicial, 10).toFixed(2)+'' ) { setPrice( precioInicial ); recalcularTotal(); return; }

        var precio = parseFloat( precioInicial.replace(',', '.') , 10);
        var precioF = parseFloat( precio.toFixed(2), 10 );

        if ( precio != precioF ) precio = precioF;
        if ( precio != precioInicial ) setPrice( precio ); else setPrice( precioInicial );

        recalcularTotal();
      });

      $('#sLicenses').keyup(function() {
        var number_tmp = $(this).val();
        var number = parseInt( number_tmp, 10);

        if ( isNaN(number) ) number = 0;
        if ( number < 0 ) number = 0;
        if ( number >= 1000 ) number = 999;
        number = parseInt(number, 10);

        if ( number + '' != number_tmp + '' ) $(this).val( number );
        recalcularTotal();
      });

      $('#btnBuy').click(function() {
          buyLicenses();
      });

      $('.divPriceBox').click( function() {
        var price = $('#price_'+$(this).data("idperiodpay")).data('price');
        price = parseFloat(price, 10).toFixed(2);
        setPrice( price );
        setProduct( $(this).data("idperiodpay") );
      });


      var precio = $('#price_'+$('#sMonths').val()).data('price');
      precio = parseFloat(precio, 10).toFixed(2);
      setPrice( precio );
      recalcularTotal();
      setProduct( $('#sMonths').val() );
    });

    function getPrice(loc) {

      if ( loc == 'total' ) {
        var price = $('#spanTotal').html()+'';
      } else {
        var price = $('#precioFinal').val()+'';
      }
      price = price.replace( <?php echo CJavaScript::encode($decimalPoint); ?>, '.' );

      return price;
    }

    function setPrice( price, loc ) {
      if ( isNaN(price) ) price = 0;
      price = price + '';
      price = price.replace( '.', <?php echo CJavaScript::encode($decimalPoint); ?> );

      if ( loc == 'total' ) {
        $('#spanTotal').html( price );
      } else {
        $('#precioFinal').val( price );
      }

    }

    function recalcularTotal() {

        var numLicenses = $('#sLicenses').val();
        var period = $('#sMonths').val();
        var price = $('#price_'+period).data('price');
        var precioFinal = parseFloat( getPrice(), 10);
        if ( isNaN(precioFinal) ) precioFinal = 0;

        var total = (numLicenses * precioFinal);

        var total1 = parseFloat(total).toFixed(2);
        var total2 = parseInt(total);
        if ( total1 == total2 ) var total = total2; else var total = total1;

        setPrice( total, 'total' );
    }

    function setProduct(id) {
      $('#sMonths').val(id);

      $('.divPriceBox .divPriceBoxInner').each( function() {
        $( this ).removeClass("active");
      });

      $('.divPriceBox').each( function() {
        if ( $(this).data("idperiodpay") == id ) {
//            console.log("Preu "+id);
          $( this ).find( ".divPriceBoxInner" ).addClass("active");
        }
      });
      
      recalcularTotal();
    }

    function buyLicenses() {
      if ( $('#sLicenses').val() == '0' ) return false;
      if ( !confirm('<?php echo CJavaScript::quote( PriceModule::t('¿ Estas seguro de realizar la petición de reserva actual ?') ); ?>') ) return false;

      ABAShowLoading();

      var precioFinal = getPrice();
      if ( isNaN(precioFinal) ) precioFinal = 0;
      precioFinal = parseFloat( precioFinal, 10);
      var price = $('#price_'+$('#sMonths').val()).data('price');

      var parametros = {
          idRequest: 1,
          idEnterprise: <?php echo $idEnterprise; ?>,
          idCountry: <?php echo $idCountry; ?>,
          licenses: $('#sLicenses').val(),
          period:  $('#price_'+$('#sMonths').val()).data('id'),
        originalPrice: price,
        finalPrice: precioFinal,
      };

      $.ajax({
          data:  parametros,
          url: "/price/main/buylicenses",
          context: document.body,
          type: 'POST',
          dataType:"json",
          success: function(data) {
              if ( data.ok ) {
                  ABAShowDialog( data.html, true );
              } else {
                  ABAShowDialog( data.html, true );
              }
          },
          error: function(jqXHR, textStatus, error) {
//          alert('error');
          },
      }).done(function() {
          //
      });
    }

</script>