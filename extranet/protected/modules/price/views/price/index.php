<?php
/* @var $this PriceController */
/* @var $dataProvider CActiveDataProvider */
?>

<span class="sectionTitle">Base Prices</span>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
