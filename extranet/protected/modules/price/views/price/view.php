<?php
/* @var $this PriceController */
/* @var $model BasePrice */

$selectedIndex = 'Configuracion';

?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>

<div class="row-fluid">
    <div class="col-lg-12 marginTop15">
         <span class="sectionTitle">View price #<?php echo $model->idProduct; ?></span>

        <?php $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                'idProduct',
                array(
                    'name'=>$model->getAttributeLabel('idCountry'),
                    'value'=>$model->country->name,
                ),
                'idPeriodPay',
                'priceOfficialCry',
                'enabled',
                'modified',
                'descriptionText',
                'userType',
                'hierarchy',
                'visibleWeb',
                'priceExtranet',
                'enabledExtranet',
            ),
        )); ?>
    </div>
</div>