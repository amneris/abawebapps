<?php
/* @var $this PriceController */
/* @var $model BasePrice */
/* @var $form CActiveForm */
?>
<div class="row-fluid">
    <div class="col-lg-12">

        <div class="form">

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'base-price-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation'=>false,
        )); ?>

            <p class="note">Fields with <span class="required">*</span> are required.</p>

            <?php echo $form->errorSummary($model); ?>

            <div class="row">
                <?php echo $form->labelEx($model,'idProduct'); ?>
                <?php echo $form->textField($model,'idProduct',array('size'=>15,'maxlength'=>15)); ?>
                <?php echo $form->error($model,'idProduct'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'idCountry'); ?>
                <?php
                    echo $form->dropDownList($model,'idCountry', $dropDownCountry);
                ?>
                <?php echo $form->error($model,'idCountry'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'idPeriodPay'); ?>
                <?php echo $form->textField($model,'idPeriodPay',array('size'=>5,'maxlength'=>5)); ?>
                <?php echo $form->error($model,'idPeriodPay'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'priceOfficialCry'); ?>
                <?php echo $form->textField($model,'priceOfficialCry',array('size'=>18,'maxlength'=>18)); ?>
                <?php echo $form->error($model,'priceOfficialCry'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'enabled'); ?>
                <?php echo $form->textField($model,'enabled'); ?>
                <?php echo $form->error($model,'enabled'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'modified'); ?>
                <?php echo $form->textField($model,'modified'); ?>
                <?php echo $form->error($model,'modified'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'descriptionText'); ?>
                <?php echo $form->textField($model,'descriptionText',array('size'=>20,'maxlength'=>20)); ?>
                <?php echo $form->error($model,'descriptionText'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'userType'); ?>
                <?php echo $form->textField($model,'userType',array('size'=>2,'maxlength'=>2)); ?>
                <?php echo $form->error($model,'userType'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'hierarchy'); ?>
                <?php echo $form->textField($model,'hierarchy',array('size'=>2,'maxlength'=>2)); ?>
                <?php echo $form->error($model,'hierarchy'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'visibleWeb'); ?>
                <?php echo $form->textField($model,'visibleWeb'); ?>
                <?php echo $form->error($model,'visibleWeb'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'priceExtranet'); ?>
                <?php echo $form->textField($model,'priceExtranet',array('size'=>18,'maxlength'=>18)); ?>
                <?php echo $form->error($model,'priceExtranet'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'enabledExtranet'); ?>
                <?php echo $form->textField($model,'enabledExtranet'); ?>
                <?php echo $form->error($model,'enabledExtranet'); ?>
            </div>

            <div class="row buttons">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
            </div>

        <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>
</div>
