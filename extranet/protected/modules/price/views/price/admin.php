<?php
/* @var $this PriceController */
/* @var $model Price */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#base-price-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
$selectedIndex = 'Configuracion';

?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>

<div class="row-fluid">
    <div class="col-lg-12 marginTop15">

         <span class="sectionTitle">Manage prices</span>
		<br>
        <?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
        <div class="search-form" style="display:none">
        <?php $this->renderPartial('_search',array(
            'model'=>$model,
        )); ?>
        </div><!-- search-form -->

        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'base-price-grid',
            'dataProvider'=>$model->search(),
            'selectableRows' => 1,
            'cssFile' => Yii::app()->request->baseUrl . '/css/gridView.css',
            'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
                           'prevPageLabel'  => '«',
                           'nextPageLabel'  => '»',
                           'header'=>''),
            //'filter'=>$model,
            'columns'=>array(
                /*'idProduct',*/
                array(
                    'name'=>'idCountry',
                    'value'=>'$data->country->name',
                ),
                'idPeriodPay',
                'descriptionText',
                /*'priceOfficialCry',*/
                'priceExtranet',
                'enabledExtranet',
                /*'enabled',*/
                'modified',
                /*'userType',*/
                /*'hierarchy',*/
                /*'visibleWeb',*/
                array(
                    'class'=>'CButtonColumn',
                    'htmlOptions' => array('style' => 'width: 40px; text-align: center;'),
                    'header'=>'Options',
                    'template'=>'{view} {update}',
                    'buttons' => array(
                        'view'=>array(
                            'imageUrl'=>Yii::app()->request->baseUrl.'/images/icons/view.png',
                        ),
                        'update'=>array(
                            'imageUrl'=>Yii::app()->request->baseUrl.'/images/icons/update.png',
                        ),
                        /*'delete'=>array(
                            'imageUrl'=>Yii::app()->request->baseUrl.'/images/icons/delete.png',
                        ),*/

                    ),
                ),
            ),
        )); ?>
    </div>
</div>
