<?php
/* @var $this PriceController */
/* @var $model BasePrice */

$selectedIndex = 'Configuracion';

?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>

<div class="row-fluid">
    <div class="col-lg-12 marginTop15">
         <span class="sectionTitle">Update price <?php echo $model->idProduct; ?></span>
        <?php $this->renderPartial('_form', array('model'=>$model,'dropDownCountry'=>$country)); ?>
    </div>
</div>
