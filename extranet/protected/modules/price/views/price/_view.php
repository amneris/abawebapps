<?php
/* @var $this PriceController */
/* @var $data BasePrice */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idProduct')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idProduct), array('view', 'id'=>$data->idProduct)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCountry')); ?>:</b>
	<?php echo CHtml::encode($data->idCountry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPeriodPay')); ?>:</b>
	<?php echo CHtml::encode($data->idPeriodPay); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('priceOfficialCry')); ?>:</b>
	<?php echo CHtml::encode($data->priceOfficialCry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('enabled')); ?>:</b>
	<?php echo CHtml::encode($data->enabled); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified')); ?>:</b>
	<?php echo CHtml::encode($data->modified); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descriptionText')); ?>:</b>
	<?php echo CHtml::encode($data->descriptionText); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('userType')); ?>:</b>
	<?php echo CHtml::encode($data->userType); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hierarchy')); ?>:</b>
	<?php echo CHtml::encode($data->hierarchy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('visibleWeb')); ?>:</b>
	<?php echo CHtml::encode($data->visibleWeb); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('priceExtranet')); ?>:</b>
	<?php echo CHtml::encode($data->priceExtranet); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('enabledExtranet')); ?>:</b>
	<?php echo CHtml::encode($data->enabledExtranet); ?>
	<br />

	*/ ?>

</div>