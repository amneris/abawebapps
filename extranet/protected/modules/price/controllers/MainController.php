<?php

class MainController extends Controller {

    /**
     *
     */
    public $layout='extranet';

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions'=>array('index', 'RequestAgent', 'BuyLicenses'),
                'roles'=>array(Role::_RESPONSIBLE, Role::_ABAMANAGER, Role::_AGENT),
            ),
            array('allow',
              'actions' => array('compraradiscrecion', 'minipanel'),
              'expression' => array('MainController','allowOnlyAgentOrABAManager')
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {

        $idCountry = Utils::getSessionVar('idCountry', 300);

        $mlPrices = Price::getPrices( $idCountry );

        $idEnterprise = Utils::getSessionVar('idEnterprise', 0);
        $mlLicenseAvailable = ModuleStudentModel::availableLicences($idEnterprise);

        if ( $idCountry == Country::_SPAIN ) {
          $taxRates = TaxRates::getTaxRateByCountry( $idCountry );
        } else $taxRates = 0;

        $this->render('index', array(
            'mlPrices' => $mlPrices,
            'mlLicenseAvailable' => $mlLicenseAvailable,
            'idCountry' => $idCountry,
            'taxRates' => $taxRates,
        ));
    }

    public function actionRequestAgent() {
        $allOk = false;

        $idEnterprise = Utils::getSessionVar('idEnterprise');

        $mUser = EnterpriseUserRole::getEnterpriseAgent($idEnterprise);
        $mRepresentante = User::model()->findByPk( Yii::app()->user->id );
        if ( $mUser ) {
            $mail = $mUser->email;
            $html = $this->renderPartial( 'mailRequestAgentBody', array( 'nombreRepresentante' => $mRepresentante->getConcatened(), 'nombreEmpresa' => Utils::getSessionVar('enterpriseName') ), true );
            $arMailsAbaManager = User::getAbaManagerMails();

            $allOk = HeAbaMail::sendEmail( array(
                'to' => $mUser->email,
                'bcc' => $arMailsAbaManager,
                'subject' => PriceModule::t('Petición de licencias ABA Corporate.'),
                'msgHtml' => $html,
                'isHtml' => true,
            ));
        }

        if ( $allOk ) {
            $html = $this->renderPartial( 'okAgent', array(), true );
        } else {
            $html = $this->renderPartial( 'koAgent', array(), true );
        }

        echo json_encode( array('ok' => $allOk, 'html' => $html) );
    }

    public function actionBuyLicenses() {
        $allOk = false;

        $idRequest = Yii::app()->request->getParam('idRequest', 0);

        if ( empty($idRequest) ) {
            $idEnterprise = Utils::getSessionVar('idEnterprise');
            $licenses = Yii::app()->request->getParam( 'licenses', 0 );
            $idLicenseType = Yii::app()->request->getParam( 'period', 0 );
            $idCountry = Utils::getSessionVar('idCountry', 300);
            $discount = 0;
        } else {
            $idEnterprise = Yii::app()->request->getParam('idEnterprise', 0);
            $licenses = Yii::app()->request->getParam( 'licenses', 0 );
            $idLicenseType = Yii::app()->request->getParam( 'period', 0 );
            $idCountry = Yii::app()->request->getParam( 'idCountry', 0 );
            $originalPrice = Yii::app()->request->getParam( 'originalPrice', 0 );
            $finalPrice = Yii::app()->request->getParam( 'finalPrice', 0 );

            // ** Si el pais es España, le tengo que sumar el IVA.
            if ( $idCountry == Country::_SPAIN ) {
                $taxRates = TaxRates::getTaxRateByCountry( $idCountry );

                $originalPrice *= $taxRates;
                $finalPrice *= $taxRates;
            }

            if ( !empty($originalPrice) ) $discount = 100 - (($finalPrice * 100) / $originalPrice);
            else $discount = 0;
        }

        if ( $idLicenseType != LicenseType::_Trial ) {
            $mlPrices = Price::getPrices($idCountry, $idLicenseType);
            $mPrice = $mlPrices[0];
            $value = $licenses * $mPrice->price;
            $value = $value - ($value * $discount / 100);
            $valueLocal = $licenses * $mPrice->priceLocal;
            $valueLocal = $valueLocal - ($valueLocal * $discount / 100);
        } else {
            $value = $licenses * $finalPrice;
            $valueLocal = $licenses * $finalPrice;
        }
        if ( $discount < 0 ) $discount = 0;

        $mPurchase = new Purchase;
        $mPurchase->idUser = Utils::getSessionVar('id');
        $mPurchase->idEnterprise = $idEnterprise;
        $mPurchase->idLicenseType = $idLicenseType;
        $mPurchase->discount = $discount;
        $mPurchase->numLicenses = $licenses;
        $mPurchase->bought = date("Y-m-d H:i:s");
        $mPurchase->validated = 0;
        $mPurchase->value = $value;
        $mPurchase->valueLocal = $valueLocal;
        $mPurchase->idCurrency = Utils::getSessionVar('idCurrency');
        try {
            $allOk = $mPurchase->save();

            // ** Solo se avisa al Partner/Agent/ABA Manager en caso de ser una compra normal.
            if ($allOk && empty($idRequest)) {
                $mUser = EnterpriseUserRole::getEnterpriseAgent($idEnterprise);
                $mRepresentante = User::model()->findByPk( Yii::app()->user->id );
                if ( $mUser ) {
                    $mail = $mUser->email;
                    $html = $this->renderPartial( 'mailPurchaseLicensesBody', array(
                        'nombreRepresentante' => $mRepresentante->getConcatened(),
                        'nombreEmpresa' => Utils::getSessionVar('enterpriseName'),
                        'numLicencias' => $licenses,
                    ), true );

                    $arMailsAbaManager = User::getAbaManagerMails();
                    $allOk = HeAbaMail::sendEmail( array(
                      'to' => $mUser->email,
                      'bcc' => $arMailsAbaManager,
                      'subject' => PriceModule::t('Petición de licencias ABA Corporate.'),
                      'msgHtml' => $html,
                      'isHtml' => true,
                    ));
                }
            }
        } catch (Exception $e) {
            $allOk = false;
        }

        if ( $allOk ) {
            $html = $this->renderPartial( 'okLicense', array(), true );
        } else {
            $html = $this->renderPartial( 'koLicense', array(), true );
        }
//    $html = "All OK";

//    $allOk = true;
        echo json_encode( array('ok' => $allOk, 'html' => $html) );
    }

    public function actionMinipanel() {
        $allOk = true;

        $idEnterprise = Yii::app()->request->getParam('idEnterprise');
        Utils::change2Enterprise($idEnterprise);
        $idCountry = Utils::getSessionVar('idCountry', 300);
        $mlPrices = Price::getPrices( $idCountry );
        $mlLicenseAvailable = ModuleStudentModel::availableLicences($idEnterprise);

        if ( $idCountry == Country::_SPAIN ) {
          $taxRates = TaxRates::getTaxRateByCountry( $idCountry );
        } else $taxRates = 0;

        $html = $this->renderPartial('minipanel', array(
          'idEnterprise' => $idEnterprise,
          'mlPrices' => $mlPrices,
          'mlLicenseAvailable' => $mlLicenseAvailable,
          'idCountry' => $idCountry,
          'taxRates' => $taxRates,
        ), true, true);

        echo json_encode(array( 'ok' => $allOk, 'html' => $html ));
    }
/*
    public function actionCompraradiscrecion( ) {
        $idEnterprise = Yii::app()->request->getParam('idEnterprise');
        $idLicenseType = Yii::app()->request->getParam('idLicenseType', 5);
        $licenses = Yii::app()->request->getParam('numLicenses', 100);
        $value = Yii::app()->request->getParam('value', 0);
        $valueLocal = Yii::app()->request->getParam('valueLocal', 0);
        $discount = Yii::app()->request->getParam('discount', 0);

        $mPurchase = new Purchase;
        $mPurchase->idUser = Utils::getSessionVar('id');
        $mPurchase->idEnterprise = $idEnterprise;
        $mPurchase->idLicenseType = $idLicenseType;
        $mPurchase->numLicenses = $licenses;
        $mPurchase->bought = date("Y-m-d H:i:s");
        $mPurchase->validated = 0;
        $mPurchase->value = $value - ($value * $discount/100);
        $mPurchase->valueLocal = $valueLocal  - ($valueLocal * $discount/100);
        $mPurchase->idCurrency = Utils::getSessionVar('idCurrency');
        try {
            $allOk = $mPurchase->save();

            if ($allOk) {
                $mUser = EnterpriseUserRole::getEnterpriseAgent($idEnterprise);
                $mRepresentante = User::model()->findByPk( Yii::app()->user->id );
                if ( $mUser ) {
                    $mail = $mUser->email;
                    $html = $this->renderPartial( 'mailPurchaseLicensesBody', array(
                      'nombreRepresentante' => $mRepresentante->getConcatened(),
                      'nombreEmpresa' => Utils::getSessionVar('enterpriseName'),
                      'numLicencias' => $licenses,
                    ), true );

                    $arMailsAbaManager = User::getAbaManagerMails();
                    $allOk = HeAbaMail::sendEmail( array(
                      'to' => $mUser->email,
                      'bcc' => $arMailsAbaManager,
                      'subject' => PriceModule::t('Petición de licencias ABA Corporate.'),
                      'msgHtml' => $html,
                      'isHtml' => true,
                    ));
                }
            }
        } catch (Exception $e) {

        }


        Yii::trace("Enterprise = $idEnterprise");
        $this->redirect('/');
    }
*/
    static public function allowOnlyAgentOrABAManager() {
        if(Yii::app()->user->isagent || Yii::app()->user->isabamanager) return true; else return false;
    }

}
