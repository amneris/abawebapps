<?php

class PriceModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'price.models.*',
			'price.components.*',
		));

        $path = realpath(Yii::app()->basePath . '/views/layouts');
        $this->setLayoutPath( $path );
	}

	/**
	 * @param $str
	 * @param $params
	 * @param $dic
	 * @return string
	 */
	public static function t($str='',$params=array(),$dic='price') {
    $translation = Yii::t("PriceModule", $str);
    if ( $translation == $str ) $translation = Yii::t("PriceModule.".$dic, $str, $params);
    if ( $translation == $str ) $translation = Yii::t("app", $str, $params);

    return $translation;
    
//		if (Yii::t("PriceModule", $str)==$str)
//		    return Yii::t("PriceModule.".$dic, $str, $params);
//        else
//            return Yii::t("PriceModule", $str, $params);
	}
}
