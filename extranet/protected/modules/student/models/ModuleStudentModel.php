<?php

/**
 * This is the model class for "student".
 *
 */
class ModuleStudentModel extends AbaActiveRecord {
  public static $lastErrorMsg = '';

  static public function getLastErrorMsg() {
    return self::$lastErrorMsg;
  }
  
  public function insertStudentsArray( $arData ) {
    $idEnterprise = Utils::getSessionVar('idEnterprise');
    $isoLanguage = Utils::getSessionVar('isoLanguageEnterprise');
    $allOk = true;
    
    $transaction = Yii::app()->dbExtranet->beginTransaction();
    try {
      foreach( $arData as $k=>$arStudent ) {
        $allOk = ModuleStudentModel::insertStudent( $idEnterprise, $arStudent['name'], $arStudent['surname'], $arStudent['email'], $arStudent['group'], $isoLanguage, false );
        if (!$allOk ) throw new Exception('Undefined error.');
      }
      
      $transaction->commit();
    } catch (Exception $e) {
      $transaction->rollback();
    }
    
    
//    $allOk = true;
//    $errorMsg = '';
//    
//    $connection = Yii::app()->dbExtranet;
//    $transaction = $connection->beginTransaction();
//    try {
//      foreach( $arData as $k => $arStudent ) {
//        $mStudent = new Student;
//        $mStudent->name = $arStudent['name'];
//        $mStudent->surname = $arStudent['surname'];
//        $mStudent->email = $arStudent['email'];
//        $allOk = $mStudent->save();
//        if ( !$allOk ) break;
//      }
//
//      if ( $allOk ) $transaction->commit();
//      else {
//        $allOk = false;
//        $transaction->rollBack();
//        $this->$lastErrorMsg = StudentModule::t('Incorrect data.');
////        $this->uploadEndWithError( StudentModule::t('Incorrect data.') );
//      }
//
//    } catch (Exception $e) {
//      $allOk = false;
//      $transaction->rollBack();
//      $this->$lastErrorMsg = StudentModule::t('Incorrect data.');
////      $this->uploadEndWithError( StudentModule::t('Incorrect data.') );
//    }
    
    return $allOk;
  }

  static public function getStudentList($arParameters) {
    if (!isset($arParameters['connection'])) $arParameters['connection'] = Yii::app()->dbExtranet;
    if (!isset($arParameters['orderBy'])) $arParameters['orderBy'] = 's.created desc';
    if (!isset($arParameters['limit'])) $arParameters['limit'] = 10; else settype( $arParameters['limit'], 'integer');
    if (!isset($arParameters['pag'])) $arParameters['pag'] = 1; else settype( $arParameters['pag'], 'integer');
    if (!isset($arParameters['checkedList'])) $arParameters['checkedList'] = '0';
    if (!isset($arParameters['idEnterprise'])) $arParameters['idEnterprise'] = '0'; else settype( $arParameters['idEnterprise'], 'integer');
    if (!isset($arParameters['filters'])) $arParameters['filters'] = array();
    if (!isset($arParameters['onlySelected'])) $arParameters['onlySelected'] = false;
    if (!isset($arParameters['onlyCount'])) $arParameters['onlyCount'] = false;
    extract($arParameters);

    $idRoleTeacher = Role::_TEACHER;

      // ** Para los listados con seleccion.
    if ( $onlySelected ) {
      if ( $checkedList == 'all' ) {
        $filterExcludeList = '';
        $filterCheckedList = '';
      } else if ( str_replace('all', '', $checkedList) != $checkedList ) {
        $filterExcludeList = str_replace('all', '0', $checkedList);
        $filterCheckedList = '';
      } else {
        $filterExcludeList = '';
        $filterCheckedList = $checkedList;
      }
      if ( empty($filterCheckedList) ) $fInclude = ''; else $fInclude = " and s.id in ({$filterCheckedList}) ";
      if ( empty($filterExcludeList) ) $fExclude = ''; else $fExclude = " and s.id not in ({$filterExcludeList}) ";
    } else {
      $fInclude = '';
      $fExclude = '';
    }

    $orderBy_org = $orderBy;
    $orderBy = str_replace( '_', ' ', $orderBy );
    $orderBy = strtolower($orderBy);
    $orderBy = ( $orderBy == 'created' ) ? 's.created' : $orderBy;
    $orderBy = ( $orderBy == 'created desc' ) ? 's.created desc' : $orderBy;
    $orderBy = ( $orderBy == 'name' ) ? 'name, surname' : $orderBy;
    $orderBy = ( $orderBy == 'name desc' ) ? 'name desc, surname desc' : $orderBy;
    $orderBy = ( $orderBy == 'surname' ) ? 'surname, name' : $orderBy;
    $orderBy = ( $orderBy == 'surname desc' ) ? 'surname desc, name desc' : $orderBy;
    $orderBy = ( $orderBy == 'group' ) ? 'group_name' : $orderBy;
    $orderBy = ( $orderBy == 'group desc' ) ? 'group_name desc' : $orderBy;
    $orderBy = ( $orderBy == 'expires' ) ? 'expirationDate' : $orderBy;
    $orderBy = ( $orderBy == 'expires desc' ) ? 'expirationDate desc' : $orderBy;
    $orderBy = ( $orderBy == 'status' ) ? 'status' : $orderBy;
    $orderBy = ( $orderBy == 'status desc' ) ? 'status desc' : $orderBy;

    $offset = $limit * ($pag - 1);
    $fName = ''; $fName2 = '';
    $fSurname = ''; $fSurname2 = '';
    $fEmail = ''; $fEmail2 = '';
    $fLevel = ''; $fLevel2 = '';
    $fPeriod = ''; $fPeriod2 = '';
    $fExpires = ''; $fExpires2 = '';
    $fGroup = ''; $fGroup2 = '';
    $fStatus = ''; $fStatus2 = '';
    if ( isset($filters['name']) && !empty($filters['name']) ) { $fName = " and s.name like :fName "; $fName2 = "%{$filters['name']}%"; }
    if ( isset($filters['surname']) && !empty($filters['surname']) ) { $fSurname = " and s.surname like :fSurname "; $fSurname2 = "%{$filters['surname']}%"; }
    if ( isset($filters['email']) && !empty($filters['email']) ) { $fEmail = " and s.email like :fEmail "; $fEmail2 = "%{$filters['email']}%"; }
    if ( isset($filters['level']) && !empty($filters['level']) ) { $fLevel = " and ((uc.currentLevel = :fLevel) or (l.id = :fLevel and uc.currentLevel is null)) "; $fLevel2 = $filters['level']; }
    if ( isset($filters['group']) && !empty($filters['group']) ) { $fGroup = " and g.id = :fGroup "; $fGroup2 = $filters['group']; }
    if ( isset($filters['period']) && !empty($filters['period']) ) { $fPeriod = " and p.id = :fPeriod "; $fPeriod2 = $filters['period']; }
    if ( isset($filters['status']) && !empty($filters['status']) ) { $fStatus = " and sg.status = :fStatus "; $fStatus2 = $filters['status']; }
    if ( isset($filters['expires']) && !empty($filters['expires']) ) {
      $noLicenseText = strtolower(Yii::app()->getModule('student')->t('No license'));
      if ( str_replace( strtolower($filters['expires']), '', $noLicenseText ) != $noLicenseText ) {
        $fExpires = " and sg.expirationDate = :fExpires ";
        $fExpires2 = Utils::_ZeroExpirationDate;
      } else {
        $fExpires = " and sg.expirationDate like :fExpires ";
        $fExpires2 = "%{$filters['expires']}%";
      }
    }

    $mlStudents = array();
    $count = 0;
    $transaction = $connection->beginTransaction();
    try {
      $arFields = [
        [ 'field' => 's.id' ],
        [ 'field' => 's.name' ],
        [ 'field' => 's.surname' ],
        [ 'field' => 's.email' ],
        [ 'field' => 'l.name', 'name' => 'level' ],
        [ 'field' => 'g.name', 'name' => 'group_name' ],
        [ 'field' => 'p.name', 'name' => 'period' ],
        [ 'field' => 'p.id', 'name' => 'idPeriod' ],
        [ 'field' => 'lc.name', 'name' => 'levelCampus' ],
        [ 'field' => 'g.id', 'name' => 'idGroup' ],
        [ 'field' => 'sg.expirationDate', 'name' => 'expirationDate' ],
        [ 'field' => 's.idLevel' ],
        [ 'field' => 's.idStudentCampus' ],
        [ 'field' => 'sg.status' ],
        [ 'field' => 'CONCAT( u_t.name, \' \',  u_t.surname )', 'name' => 'nameTeacher' ],
        [ 'field' => 'u_t.email', 'name' => 'emailTeacher' ],
      ];

      $arSelectFields = $strGroupByFields = '';
      foreach( $arFields as $field ) {
        $strField = (!isset($field['name'])) ? $field['field'] : $field['field'].' as '.$field['name'];
        $arSelectFields[] = $strField;

        $arGroupByFields[] = (!isset($field['name'])) ? $field['field'] : $field['name'];
      }
      $strSelectFields = implode(',', $arSelectFields);
      $strGroupByFields = implode(',', $arGroupByFields);

      $sql_Select = "select {$strSelectFields} ";
      $sql_GroupBy = "group by {$strGroupByFields} ";

      $sql_Condition = "from egroup g, student_group sg
                        inner join student s on s.isdeleted = 0
                        left join level l on s.idLevel = l.id
                        left join period p on sg.idPeriod = p.id
                        left join aba_b2c.user uc on ( s.idStudentCampus = uc.id )
                        left join level lc on ( uc.currentLevel = lc.id )
                        left join user u_t on ( s.idTeacher = u_t.id )
                        left join enterprise_user_role eur_t on ( eur_t.idEnterprise = {$idEnterprise} and eur_t.idRole = :idRoleTeacher and u_t.id = eur_t.idUser )
                        where sg.isdeleted = 0
                          $fName
                          $fSurname
                          $fEmail
                          $fLevel
                          $fGroup
                          $fPeriod
                          $fExpires
                          $fStatus
                          and sg.idStudent = s.id
                          and sg.idGroup = g.id
                          and g.idEnterprise = {$idEnterprise}
                          {$fInclude} {$fExclude}
                        ";

      if ( !empty($orderBy) ) $sql_OrderBy = " order by {$orderBy} "; else $sql_OrderBy = '';
      if ( !empty($limit) ) $sql_Limit = " limit {$offset}, {$limit} "; else $sql_Limit = "";

      $sqlCount = "select count(*) as num from (" . $sql_Select . $sql_Condition . $sql_GroupBy .") as a";
      $sqlActiveCount = "select count(*) as num from ( " . $sql_Select . $sql_Condition . ' and sg.expirationDate > now() '. $sql_GroupBy .") as a";
      $sql = $sql_Select . $sql_Condition . $sql_GroupBy . $sql_OrderBy . $sql_Limit;

      // ** Contador.
      $command = $connection->createCommand($sqlCount);
      if ( !empty($fName2) ) $command->bindParam(":fName", $fName2, PDO::PARAM_STR);
      if ( !empty($fSurname2) ) $command->bindParam(":fSurname", $fSurname2, PDO::PARAM_STR);
      if ( !empty($fEmail2) ) $command->bindParam(":fEmail", $fEmail2, PDO::PARAM_STR);
      if ( !empty($fLevel2) ) $command->bindParam(":fLevel", $fLevel2, PDO::PARAM_INT);
      if ( !empty($fGroup2) ) $command->bindParam(":fGroup", $fGroup2, PDO::PARAM_INT);
      if ( !empty($fPeriod2) ) $command->bindParam(":fPeriod", $fPeriod2, PDO::PARAM_INT);
      if ( !empty($fStatus2) ) $command->bindParam(":fStatus", $fStatus2, PDO::PARAM_INT);
      if ( !empty($fExpires2) ) $command->bindParam(":fExpires", $fExpires2, PDO::PARAM_STR);
      $command->bindParam(":idRoleTeacher", $idRoleTeacher, PDO::PARAM_INT);
      $count = $command->query()->readColumn(0);
      if ( $onlyCount ) return $count;

      // ** Contador licencias activas.
      $command = $connection->createCommand($sqlActiveCount);
      if ( !empty($fName2) ) $command->bindParam(":fName", $fName2, PDO::PARAM_STR);
      if ( !empty($fSurname2) ) $command->bindParam(":fSurname", $fSurname2, PDO::PARAM_STR);
      if ( !empty($fEmail2) ) $command->bindParam(":fEmail", $fEmail2, PDO::PARAM_STR);
      if ( !empty($fLevel2) ) $command->bindParam(":fLevel", $fLevel2, PDO::PARAM_INT);
      if ( !empty($fGroup2) ) $command->bindParam(":fGroup", $fGroup2, PDO::PARAM_INT);
      if ( !empty($fPeriod2) ) $command->bindParam(":fPeriod", $fPeriod2, PDO::PARAM_INT);
      if ( !empty($fStatus2) ) $command->bindParam(":fStatus", $fStatus2, PDO::PARAM_INT);
      if ( !empty($fExpires2) ) $command->bindParam(":fExpires", $fExpires2, PDO::PARAM_STR);
      $command->bindParam(":idRoleTeacher", $idRoleTeacher, PDO::PARAM_INT);
      $activeCount = $command->query()->readColumn(0);

      // ** Query
      $command = $connection->createCommand($sql);
      if ( !empty($fName2) ) $command->bindParam(":fName", $fName2, PDO::PARAM_STR);
      if ( !empty($fSurname2) ) $command->bindParam(":fSurname", $fSurname2, PDO::PARAM_STR);
      if ( !empty($fEmail2) ) $command->bindParam(":fEmail", $fEmail2, PDO::PARAM_STR);
      if ( !empty($fLevel2) ) $command->bindParam(":fLevel", $fLevel2, PDO::PARAM_INT);
      if ( !empty($fGroup2) ) $command->bindParam(":fGroup", $fGroup2, PDO::PARAM_INT);
      if ( !empty($fPeriod2) ) $command->bindParam(":fPeriod", $fPeriod2, PDO::PARAM_INT);
      if ( !empty($fStatus2) ) $command->bindParam(":fStatus", $fStatus2, PDO::PARAM_INT);
      if ( !empty($fExpires2) ) $command->bindParam(":fExpires", $fExpires2, PDO::PARAM_STR);
      $command->bindParam(":idRoleTeacher", $idRoleTeacher, PDO::PARAM_INT);
      Yii::trace($command->getText(), "sql.select");
      $rs = $command->queryAll();

      $arSelectedItems = array_flip(explode(',', $checkedList));
      foreach ($rs as $k => $student) {
        $mStudent = new stdClass();
        $mStudent->id = $student['id'];
        $mStudent->name = $student['name'];
        $mStudent->surname = $student['surname'];
        $mStudent->email = $student['email'];
        $mStudent->Level = ( empty($student['levelCampus']) )? $student['level']: $student['levelCampus'];
        if ( !empty($mStudent->Level) ) {
          $mLevel = Level::model()->findByAttributes(array('name' => $mStudent->Level));
          $mStudent->idLevel = ( $mLevel ) ? $mLevel->id : 0;
        } else {
          $mStudent->Level = 'lbl: Sin definir';
          $mStudent->idLevel = 0;
        }
        $mStudent->Level = Yii::app()->getModule('student')->t($mStudent->Level);
        $mStudent->levelTest = Yii::app()->getModule('student')->t($student['level']);
        $mStudent->period = $student['period'];
        $mStudent->idPeriod = $student['idPeriod'];
        $mStudent->group = $student['group_name'];
        $mStudent->idGroup = $student['idGroup'];
        $mStudent->expires = ( !empty($student['expirationDate']) )? date('Y-m-d', strtotime($student['expirationDate'])) : StudentModule::t('No license');
        $mStudent->checked = ( isset($arSelectedItems[$mStudent->id]) xor isset($arSelectedItems['all']) );
        $mStudent->idStudentCampus = $student['idStudentCampus'];
        $mStudent->status = $student['status'];
        $mStudent->nameTeacher = $student['nameTeacher'];
        $mStudent->emailTeacher = $student['emailTeacher'];
        $mlStudents[] = $mStudent;
      }

      $transaction->commit();
    } catch (Exception $e) {
      $transaction->rollBack();
    }
    if ( !empty($limit) ) {
      $maxPag = (int) ($count / $limit);
      if (($count % $limit) > 0) $maxPag = $maxPag + 1;
    } else $maxPag = 1;

    $arDatos = array(
      'mlStudents' => $mlStudents,
      'orderBy' => $orderBy_org,
      'pag' => $pag,
      'maxPag' => $maxPag,
      'checkedList' => $checkedList,
      'count' => $count,
      'activeCount' => $activeCount,
    );

    return $arDatos;
  }

  static public function undoDelete( $undoCode ) {
    $connection = Yii::app()->dbExtranet;

    $sql = "UPDATE student as s inner Join (select idElement from toundo where code = '{$undoCode}') as t on s.id = t.idElement SET deleted=created";
    $command = $connection->createCommand($sql);
    $command->execute();

    return true;
  }
  
  public function deleteItem( $checkedList, $idEnterprise, $filters = array(), $undo = false ) {
    if ( empty($checkedList) ) {
      self::$lastErrorMsg = StudentModule::t('No one student has been selected.');
      return false;
    }

    $allOk = true;
//    Example::model()->updateAll(array('status' => 1, 'updated' => date('Y-m-d H:i:s')), 'type_id = 1 AND status = 0');

    if ( empty($filters) ) $filters = Utils::getActualFilters( $filters );

    $fName = ''; $fName2 = '';
    $fSurname = ''; $fSurname2 = '';
    $fEmail = ''; $fEmail2 = '';
    $fLevel = ''; $fLevel2 = '';
    $fPeriod = ''; $fPeriod2 = '';
    $fExpires = ''; $fExpires2 = '';
    $fStatus = ''; $fStatus2 = '';
    $fGroup = ''; $fGroup2 = '';
    if ( isset($filters['name']) && !empty($filters['name']) ) { $fName = " and s.name like :fName "; $fName2 = "%{$filters['name']}%"; }
    if ( isset($filters['surname']) && !empty($filters['surname']) ) { $fSurname = " and s.surname like :fSurname "; $fSurname2 = "%{$filters['surname']}%"; }
    if ( isset($filters['email']) && !empty($filters['email']) ) { $fEmail = " and s.email like :fEmail "; $fEmail2 = "%{$filters['email']}%"; }
    if ( isset($filters['level']) && !empty($filters['level']) ) { $fLevel = " and ((uc.currentLevel = :fLevel) or (l.id = :fLevel and uc.currentLevel is null)) "; $fLevel2 = $filters['level']; }
    if ( isset($filters['group']) && !empty($filters['group']) ) { $fGroup = " and g.id = :fGroup "; $fGroup2 = $filters['group']; }
    if ( isset($filters['period']) && !empty($filters['period']) ) { $fPeriod = " and p.id = :fPeriod "; $fPeriod2 = $filters['period']; }
    if ( isset($filters['status']) && !empty($filters['status']) ) { $fStatus = " and sg.status = :fStatus "; $fStatus2 = $filters['status']; }
    if ( isset($filters['expires']) && !empty($filters['expires']) ) { $fExpires = " and sg.expirationDate like :fExpires "; $fExpires2 = "%{$filters['expires']}%"; }


    if ( $checkedList == 'all' ) {
      $excludeList = '';
      $checkedList = '';
    } else if ( str_replace('all', '', $checkedList) != $checkedList ) {
      $excludeList = str_replace('all', '0', $checkedList);
      $checkedList = '';
    } else {
      $excludeList = '';
    }
    if ( empty($checkedList) ) $fInclude = ''; else $fInclude = " and s.id in ({$checkedList}) ";
    if ( empty($excludeList) ) $fExclude = ''; else $fExclude = " and s.id not in ({$excludeList}) ";

    $randomCode = 'zTmp_'.date("YmdHis").'_'.md5( Utils::getSessionVar('idUser').'_'.Utils::getSessionVar('idEnterprise').'_'.microtime(true).'_'.rand(0,100) );
    $sql = "insert into toundo (idelement, code)
                      select s.id, '{$randomCode}'
                      from egroup g, student_group sg
                        inner join student s on s.isdeleted = 0
                        left join level l on s.idLevel = l.id
                        left join period p on sg.idPeriod = p.id
                        left join aba_b2c.user uc on ( s.idStudentCampus = uc.id )
                        left join level lc on ( uc.currentLevel = lc.id )
                        where sg.isdeleted = 0
                          $fName
                          $fSurname
                          $fEmail
                          $fLevel
                          $fGroup
                          $fPeriod
                          $fStatus
                          $fExpires
                          and sg.idStudent = s.id
                          and sg.idGroup = g.id
                          and g.idEnterprise = {$idEnterprise}
                          {$fInclude} {$fExclude}
                        ";

    $connection = Yii::app()->dbExtranet;
    $command = $connection->createCommand($sql);
    if ( !empty($fName2) ) $command->bindParam(":fName", $fName2, PDO::PARAM_STR);
    if ( !empty($fSurname2) ) $command->bindParam(":fSurname", $fSurname2, PDO::PARAM_STR);
    if ( !empty($fEmail2) ) $command->bindParam(":fEmail", $fEmail2, PDO::PARAM_STR);
    if ( !empty($fLevel2) ) $command->bindParam(":fLevel", $fLevel2, PDO::PARAM_INT);
    if ( !empty($fGroup2) ) $command->bindParam(":fGroup", $fGroup2, PDO::PARAM_INT);
    if ( !empty($fPeriod2) ) $command->bindParam(":fPeriod", $fPeriod2, PDO::PARAM_INT);
    if ( !empty($fExpires2) ) $command->bindParam(":fExpires", $fExpires2, PDO::PARAM_STR);
    if ( !empty($fStatus2) ) $command->bindParam(":fStatus", $fStatus2, PDO::PARAM_INT);

    $command->execute();
    Yii::trace("SQL: ".$command->getText(), 'sql');

    if ( $this->hayLicenciasActivas($randomCode) ) {
      self::$lastErrorMsg = StudentModule::t('No se ha podido eliminar el/los alumno/os.<br>Comprueba que no haya ningún alumno con alguna licencia activada.');
      return false;
    }

    $sql = "update ".Student::model()->tableName()." s
              inner join abaenglish_extranet.toundo t on
                s.id = t.idElement and t.code = '".$randomCode."'
            set
              s.deleted = now(), s.isdeleted = 1, s.lastupdated = now()";
    $command = Yii::app()->dbExtranet->createCommand($sql);
    $command->execute();

    return $randomCode;
  }

  public function hideItem( $checkedList, $idEnterprise, $filters = array() ) {
    if ( empty($checkedList) ) {
      self::$lastErrorMsg = StudentModule::t('No one student has been selected.');
      return false;
    }

    $allOk = true;

    if ( empty($filters) ) $filters = Utils::getActualFilters( $filters );

    $fName = ''; $fName2 = '';
    $fSurname = ''; $fSurname2 = '';
    $fEmail = ''; $fEmail2 = '';
    $fLevel = ''; $fLevel2 = '';
    $fPeriod = ''; $fPeriod2 = '';
    $fExpires = ''; $fExpires2 = '';
    $fStatus = ''; $fStatus2 = '';
    $fGroup = ''; $fGroup2 = '';
    if ( isset($filters['name']) && !empty($filters['name']) ) { $fName = " and s.name like :fName "; $fName2 = "%{$filters['name']}%"; }
    if ( isset($filters['surname']) && !empty($filters['surname']) ) { $fSurname = " and s.surname like :fSurname "; $fSurname2 = "%{$filters['surname']}%"; }
    if ( isset($filters['email']) && !empty($filters['email']) ) { $fEmail = " and s.email like :fEmail "; $fEmail2 = "%{$filters['email']}%"; }
    if ( isset($filters['level']) && !empty($filters['level']) ) { $fLevel = " and ((uc.currentLevel = :fLevel) or (l.id = :fLevel and uc.currentLevel is null)) "; $fLevel2 = $filters['level']; }
    if ( isset($filters['group']) && !empty($filters['group']) ) { $fGroup = " and g.id = :fGroup "; $fGroup2 = $filters['group']; }
    if ( isset($filters['period']) && !empty($filters['period']) ) { $fPeriod = " and p.id = :fPeriod "; $fPeriod2 = $filters['period']; }
    if ( isset($filters['status']) && !empty($filters['status']) ) { $fStatus = " and sg.status = :fStatus "; $fStatus2 = $filters['status']; }
    if ( isset($filters['expires']) && !empty($filters['expires']) ) { $fExpires = " and sg.expirationDate like :fExpires "; $fExpires2 = "%{$filters['expires']}%"; }


    if ( $checkedList == 'all' ) {
      $excludeList = '';
      $checkedList = '';
    } else if ( str_replace('all', '', $checkedList) != $checkedList ) {
      $excludeList = str_replace('all', '0', $checkedList);
      $checkedList = '';
    } else {
      $excludeList = '';
    }
    if ( empty($checkedList) ) $fInclude = ''; else $fInclude = " and s.id in ({$checkedList}) ";
    if ( empty($excludeList) ) $fExclude = ''; else $fExclude = " and s.id not in ({$excludeList}) ";

    $randomCode = 'zTmpI_'.date("YmdHis").'_'.md5( Utils::getSessionVar('idUser').'_'.Utils::getSessionVar('idEnterprise').'_'.microtime(true).'_'.rand(0,100) );
    $sql = "insert into toundo (idelement, code)
                      select s.id, '{$randomCode}'
                      from egroup g, student_group sg
                        inner join student s on s.isdeleted = 0
                        left join level l on s.idLevel = l.id
                        left join period p on sg.idPeriod = p.id
                        left join aba_b2c.user uc on ( s.idStudentCampus = uc.id )
                        left join level lc on ( uc.currentLevel = lc.id )
                        where sg.isdeleted = 0
                          $fName
                          $fSurname
                          $fEmail
                          $fLevel
                          $fGroup
                          $fPeriod
                          $fStatus
                          $fExpires
                          and sg.idStudent = s.id
                          and sg.idGroup = g.id
                          and g.idEnterprise = {$idEnterprise}
                          {$fInclude} {$fExclude}
                        ";

    $connection = Yii::app()->dbExtranet;
    $command = $connection->createCommand($sql);
    if ( !empty($fName2) ) $command->bindParam(":fName", $fName2, PDO::PARAM_STR);
    if ( !empty($fSurname2) ) $command->bindParam(":fSurname", $fSurname2, PDO::PARAM_STR);
    if ( !empty($fEmail2) ) $command->bindParam(":fEmail", $fEmail2, PDO::PARAM_STR);
    if ( !empty($fLevel2) ) $command->bindParam(":fLevel", $fLevel2, PDO::PARAM_INT);
    if ( !empty($fGroup2) ) $command->bindParam(":fGroup", $fGroup2, PDO::PARAM_INT);
    if ( !empty($fPeriod2) ) $command->bindParam(":fPeriod", $fPeriod2, PDO::PARAM_INT);
    if ( !empty($fExpires2) ) $command->bindParam(":fExpires", $fExpires2, PDO::PARAM_STR);
    if ( !empty($fStatus2) ) $command->bindParam(":fStatus", $fStatus2, PDO::PARAM_INT);

    $command->execute();
    Yii::trace("SQL: ".$command->getText(), 'sql');

    if ( $this->hayLicenciasActivas($randomCode) ) {
      self::$lastErrorMsg = Yii::app()->getModule('student')->t('lbl: No se ha podido desactivar el/los alumno/os.<br>Comprueba que no haya ningún alumno con alguna licencia activada.');
      return false;
    }

    $sql = "update ".StudentGroup::model()->tableName()." sg
              inner join abaenglish_extranet.toundo t on
                sg.idStudent = t.idElement and t.code = '".$randomCode."' and sg.idEnterprise = $idEnterprise
            set
              sg.status = ".StudentGroup::_STATUS_INACTIVE;
    $command = Yii::app()->dbExtranet->createCommand($sql);
    $command->execute();

    return $randomCode;
  }

  static function insertStudent( $idEnterprise, $name, $surname, $email, $group, $isoLanguage, $doTRansaction = true ) {
    if ( empty($idEnterprise) ) {
      self::$lastErrorMsg = Yii::t('app', 'Something wrong inserting student.').'(1)';
      return false;
    }

    $allOk = false;
    if ( empty($group) ) $group = '-';
    
    $criteria = new CDbCriteria();
    $criteria->addCondition('t.email = :mail');
    $criteria->addCondition('t.isdeleted = :deleted');
    $criteria->params = array(':mail' => $email, ':deleted' => 0);
    $mStudent = Student::model()->find($criteria);

    $criteria = new CDbCriteria();
    $criteria->addCondition('t.name = :name');
    $criteria->addCondition('t.idEnterprise = :idEnterprise');
    $criteria->addCondition('t.isdeleted = :deleted');
    $criteria->params = array(':name' => $group, ':idEnterprise' => $idEnterprise, ':deleted' => 0);
    $mGroup = Group::model()->find($criteria);

    $mStudentGroup = null;
    if ( $mStudent && $mGroup ) {
      $criteria = new CDbCriteria();
      $criteria->addCondition('t.idGroup = :idGroup');
      $criteria->addCondition('t.idStudent = :idStudent');
      $criteria->addCondition('t.isdeleted = :deleted');
      $criteria->params = array(':idStudent' => $mStudent->id, ':idGroup' => $mGroup->id, ':deleted' => 0);
      $mStudentGroup = StudentGroup::model()->find($criteria);
    }

    if ( !$mStudentGroup ) {

      if ($doTRansaction) $transaction = Yii::app()->dbExtranet->beginTransaction();
      try {

        if ( !$mGroup ) {
          $mGroup = new Group;
          $mGroup->name = $group;
          $mGroup->idEnterprise = $idEnterprise;
          $allOk = $mGroup->save();
          if ( !$allOk ) {
            throw new Exception( Yii::app()->getModule('user')->t('Something wrong inserting group {$group}.', array('{$group}' => $group) ) );
          }
        }

        if ( !$mStudent ) {
          $mStudent = new Student;
          $mStudent->name = trim($name);
          $mStudent->surname = trim($surname);
          $mStudent->email = trim($email);
          $mStudent->idLevel = Level::_UNKNOWN;
          $mStudent->idStudentCampus = 0;
          $mStudent->idEnterprise = $idEnterprise;
          $allOk = $mStudent->save();
          if ( !$allOk ) throw new Exception(Yii::t('app', 'Something wrong inserting student.'));
        } else if ( $mStudent->idEnterprise != $idEnterprise ) {                      // ** Un estudiante, con un mismo mail, solo puede estar en una empresa.
          $errorMsg = Yii::app()->getModule('student')->t('Student {$email} already exists.').'(1)';
          eval("\$errorMsg = \"$errorMsg\";");
          throw new Exception($errorMsg);
        }

        $criteria = new CDbCriteria();
        $criteria->addCondition('t.idEnterprise = :idEnterprise');
        $criteria->addCondition('t.idStudent = :idStudent');
        $criteria->addCondition('t.isdeleted = :deleted');
        $criteria->params = array(':idStudent' => $mStudent->id, ':idEnterprise' => $idEnterprise, ':deleted' => 0);
        $mStudentGroup = StudentGroup::model()->find($criteria);
        if ( !$mStudentGroup ) {
          $mStudentGroup = new StudentGroup;
          $mStudentGroup->idStudent = $mStudent->id;
          $mStudentGroup->idEnterprise = $idEnterprise;
          $mStudentGroup->idPeriod = 0;
          $mStudentGroup->status = StudentGroup::_STATUS_ACTIVE;
          $mStudentGroup->expirationDate = Utils::_ZeroExpirationDate;
        } else if ( $mStudentGroup->status == StudentGroup::_STATUS_INACTIVE ) {
          $mStudentGroup->status = StudentGroup::_STATUS_ACTIVE;
        } else {
          $errorMsg = Yii::app()->getModule('student')->t('Student {$email} already exists.').'(2)';
          eval("\$errorMsg = \"$errorMsg\";");
          throw new Exception($errorMsg);
        }
        $mStudentGroup->idGroup = $mGroup->id;
        
        $allOk = $mStudentGroup->save();
        if ( !$allOk ) throw new Exception('Something wrong inserting relation between student and group.');

//        HeAbaSelligent::addToQueue(HeAbaSelligent::_EDITSTUDENT, array('idStudent' => $mStudent->id, 'idEnterprise' => $idEnterprise, 'name' => $mStudent->name, 'surname' => $mStudent->surname, 'email' => $mStudent->email, 'isoLanguage' => Utils::getSessionVar('isoLanguageEnterprise') ), 'ExtranetController::actionMailSent');
        $allOk = HeAbaMail::sendEmail( array(
            'action' => HeAbaSelligent::_EDITSTUDENT,
            'idStudent' => $mStudent->id, 
            'idEnterprise' => $idEnterprise, 
            'name' => $mStudent->name, 
            'surname' => $mStudent->surname, 
            'email' => $mStudent->email, 
            'isoLanguage' => $isoLanguage,
            'callback' => 'ExtranetController::actionMailSent',
        ) );
        if ( !$allOk ) throw new Exception('Something wrong sending mail.');
        
        if ($doTRansaction) $transaction->commit();
      } catch (Exception $e) {
        if ($doTRansaction) $transaction->rollback();
        $allOk = false;
        self::$lastErrorMsg = $e->getMessage();
      }

    } else {

      if ( $mStudentGroup->status != StudentGroup::_STATUS_ACTIVE ) {
        $mStudentGroup->status = StudentGroup::_STATUS_ACTIVE;
        $allOk = $mStudentGroup->save();

        if (!$allOk) {
          self::$lastErrorMsg = Yii::t( 'student', 'Something wrong inserting student.').'(2)';
        }

      } else {
        $allOk = false;
        $errorMsg = Yii::app()->getModule('student')->t('Student {$email} already exists.').'(3)';
        eval("\$errorMsg = \"$errorMsg\";");
        self::$lastErrorMsg = $errorMsg;
      }

    }
    
    return $allOk;
  }

  
  static public function availableLicences($idEnterprise, $idLicenseType = '', $minLicenses = 1 ) {
    
    if ( !empty($idLicenseType) ) $fLicenseType = "and lt.id = {$idLicenseType} "; else $fLicenseType = "";
    
    $sql = "select lt.id, lt.name, lt.idPeriodPay, sum(la.numLicenses) as numLicenses
            from license_available la, license_type lt
            where la.idLicenseType = lt.id
              and la.idEnterprise = {$idEnterprise}
--              and la.numLicenses >= {$minLicenses}
              {$fLicenseType}
            group by lt.id, lt.name, lt.idPeriodPay
            having sum(la.numLicenses) >= {$minLicenses}
            order by  LENGTH(lt.idPeriodPay), lt.idPeriodPay asc
            ";
    $command = Yii::app()->dbExtranet->createCommand($sql);
    Yii::trace($command->getText(), "sql.select");
    $rs = $command->queryAll();

    $mlLicenses = array();
    $mLicense = new stdClass();
    $mLicense->id = 0;
    $mLicense->name = 'Total';
    $mLicense->numLicenses = 0;
    $mlLicenses['Total'] = $mLicense;
    
    foreach( $rs as $row ) {
      $mLicense = new stdClass();
      $mLicense->id = $row['id'];
      $mLicense->name = $row['name'];
      $mLicense->numLicenses = $row['numLicenses'];
      $mLicense->idPeriodPay = $row['idPeriodPay'];
      $mlLicenses[ $row['id'] ] = $mLicense;
      $mlLicenses['Total']->numLicenses += $row['numLicenses'];
    }
    
    return $mlLicenses;
  }

    public static  function getLevelProgressByEnterprisse($idEnterprise)
    {
        //we set to zero the level progress.. to show at list a nice chart at zero progress. 
        $toReturn[0]= array('Level', CHtml::encode(StudentModule::t('Students')));
        $toReturn[1]= array(Utils::getLevelString(1), 0);
        $toReturn[2]= array(Utils::getLevelString(2), 0);
        $toReturn[3]= array(Utils::getLevelString(3), 0);
        $toReturn[4]= array(Utils::getLevelString(4), 0);
        $toReturn[5]= array(Utils::getLevelString(5), 0);
        $toReturn[6]= array(Utils::getLevelString(6), 0);

        //first we get the levels on campus.. for premium users.
        $sqlCampus ="SELECT u.currentLevel as level, COUNT(*) AS count
                     FROM student s
                     INNER JOIN aba_b2c.user u ON s.idStudentCampus = u.id
                     INNER JOIN abaenglish_extranet.student_group sg ON s.id = sg.idStudent
                     WHERE s.idEnterprise = $idEnterprise AND  s.isdeleted = 0 and sg.status = ".StudentGroup::_STATUS_ACTIVE." GROUP BY u.currentLevel;";

        $command = Yii::app()->dbExtranet->createCommand($sqlCampus);
        $resultCampus = $command->queryAll();

        foreach($resultCampus as $currentLevel)
        {
            $toReturn[$currentLevel['level']][1] += (int)$currentLevel['count'];
        }

        //now we get the levels from extranet.. for not registred users.
        $sqlExtranet ="SELECT s.idLevel as level, COUNT(*) AS count
                       FROM student s
                       INNER JOIN abaenglish_extranet.student_group sg ON s.id = sg.idStudent
                       WHERE s.idEnterprise = $idEnterprise AND  s.isdeleted = 0 AND idStudentCampus = 0 and sg.status = ".StudentGroup::_STATUS_ACTIVE." GROUP BY s.idLevel;";

        $command = Yii::app()->dbExtranet->createCommand($sqlExtranet);
        $resultExtranet = $command->queryAll();

        foreach($resultExtranet as $currentLevel)
        {
            if($currentLevel['level']<=0)
            {
                continue;
            }
            $toReturn[$currentLevel['level']][1] += (int)$currentLevel['count'];
        }

        $tmp = $toReturn;
        $toReturn[0]= $tmp[0];
        $toReturn[1]= $tmp[6];
        $toReturn[2]= $tmp[5];
        $toReturn[3]= $tmp[4];
        $toReturn[4]= $tmp[3];
        $toReturn[5]= $tmp[2];
        $toReturn[6]= $tmp[1];

        return $toReturn;
    }

    public function hayLicenciasActivas($randomCode) {
      $sql = "select count(sg.id)
              from abaenglish_extranet.toundo tu, abaenglish_extranet.student_group sg
              where code = '$randomCode'
                and tu.idelement = sg.idStudent
                and sg.expirationDate > now() ";
      $command = Yii::app()->dbExtranet->createCommand($sql);
      $count = $command->queryScalar();

      return ($count > 0);
    }


  public static function generateNewLevelTestCode( $idStudent ) {
    $code = self::generateNewLeveltestData();

    $mStudentLevelTest = new StudentLevelTest;
    $mStudentLevelTest->idStudent = $idStudent;
    $mStudentLevelTest->idLevelTest = 1;
    $mStudentLevelTest->code = $code;
    $mStudentLevelTest->save();

    $mStudentReal = Student::model()->findByPk( $idStudent );
    if ( $mStudentReal->idLevel <= 0 ) {
      $mStudentReal->idLevel = Level::_SENT;
      $mStudentReal->save();
    }

    return $code;
  }

  public static function generateNewLeveltestData() {
    $code = false;

    $json = file_get_contents(getenv('CAMPUS_URL').'/leveltest/newLeveltestData');
    try{
      $tmp = json_decode($json);
      $code = $tmp->code;
    } catch(Exception $e) {
    }

    return $code;
  }

}
