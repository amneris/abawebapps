<?php
/**
 * ABA Student module
 * 
 * @author Name <name@gmail.com> 
 * @link http://web.org/
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @version $Id: StudentModule.php 1 2011-09-29 17:34
 */

class StudentModule extends CWebModule
{
  
	public function init() {
      parent::init();
        
      // import the module-level models and components
      $this->setImport(array(
          'student.components.*',
          'student.models.*',
          'student.models._selfgenerated.*',
          'leveltest.*',
          'leveltest.models.*',
          'leveltest.models._selfgenerated.*',
          'leveltest.rules.LevelTestComunication'
      ));

      Yii::app()->setComponents( 
            array(
              'student'=>array(
                  'none',
              )                
            )
          );
      
      $path = realpath(Yii::app()->basePath . '/views/layouts');
      $this->setLayoutPath( $path );
	}
	
	/**
	 * @param $str
	 * @param $params
	 * @param $dic
	 * @return string
	 */
	public static function t($str='',$params=array(),$dic='student') {
    $translation = Yii::t("StudentModule", $str);
    if ( $translation == $str ) $translation = Yii::t("StudentModule.".$dic, $str, $params);
    if ( $translation == $str ) $translation = Yii::t("app", $str, $params);
    
    return $translation;
//		if (Yii::t("StudentModule", $str)==$str) return Yii::t("StudentModule.".$dic, $str, $params);
//    else return Yii::t("StudentModule", $str, $params);
	}

  public static function actualizeLevelTest($oSession) {
    $idLevelTest = $oSession->idLevelTestData;
    
    $criteria = new CDbCriteria();
    $criteria->addCondition('t.idLevelTest = :id');
    $criteria->params = array(':id' => $idLevelTest);
    $mStudentLevelTest = StudentLevelTest::model()->find($criteria);
    
    $mStudent = Student::model()->findByPk( $mStudentLevelTest->idStudent );
    if ( $mStudent->idLevel < $oSession->curLevel ) {
      $mStudent->idLevel = $oSession->curLevel;
      $mStudent->save();
    }
  }
  
}
