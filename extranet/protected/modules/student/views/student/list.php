<?php
$module = 'student';

if ( !isset($pag) ) $pag = 1;
if ( !isset($maxPag) ) $maxPag = 1;

if ( $numStudents > 1 || empty($numStudents) ) $tNumberElements = StudentModule::t('Students');
else $tNumberElements = StudentModule::t('Student');
if ( $numActiveStudents > 1 || empty($numActiveStudents) ) $tNumberActiveElements = Yii::app()->getModule('student')->t('lbl: Activados');
else $tNumberActiveElements = Yii::app()->getModule('student')->t('lbl: Activado');

$numElements = $numStudents;

$displayNewElement = ( $numElements == 0 && empty($filters) );
$displayNewElement = false;

?>
<!--<div class="row-fluid">
  <div class="col-lg-12"> -->

<div class="table-responsive divTableListInner">
<table class="table tableList">
<tr>
    <td colspan="9" class="tdHeaderList">

        <div class="table-responsive">
            <table class="table tableHeaderList">
                <tr>
                    <td class='liHeaderTableListTitle' style="border-top:none;">
                      <span class="tableTitle"><?php echo $numElements; ?> <?php echo $tNumberElements; ?></span><br>
                      <span class="tableSubTitle" style="color: #8D0005; font-size: 0.8em;"> <?php echo $numActiveStudents; ?> <?php echo $tNumberActiveElements; ?></span>
                    </td>
                    <td class="tdMsgList" style="border-top:none;"><span class="spanMsgList" id="msgList_2_<?php echo $module; ?>" style="display:none;"></span></td>
                    <td class='listAction' style="border-top:none;">
                        <button type="button" class="abaButton abaButton-Primary btnHeaderList" id="btnEnviarTest" style="margin-right: 10px;">
                            <?php echo CHtml::encode( StudentModule::t( 'SEND TEST' ) ); ?>
                        </button>
                    </td>
                    <td class='listAction verticalLineL' style2="width:180px;" style="border-top:none;">
                        <button type="button" class="abaButton abaButton-Danger btnHeaderList" id="btnActivateLicense" style="margin-right: 10px;margin-left: 10px;">
                            <?php echo CHtml::encode( StudentModule::t( 'ACTIVATE LICENSE' ) ); ?>
                        </button>
                    </td>
                    <!--        <td class='right listAction'><span id="iconDeleteStudentList"><img id="iconDeleteStudentList" class="logo" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/iconmail_active.png"/></span></td> -->
                    <td class='listAction verticalLineL' style2="width:60px;" style="border-top:none;">
                  <span id="iconDeleteList_<?php echo $module; ?>">
                      <img style="margin-top: 0.8em; margin-right: 10px; margin-left: 10px" id="iconDeleteList_<?php echo $module; ?>" class="iconLink" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/icontrash_active.png" title="<?= Yii::t('app', 'lbl: Borrar'); ?>" alt="<?= Yii::t('app', 'lbl: Borrar'); ?>"/>
                  </span>
                    </td>
                    <td class='listAction verticalLineL' style2="width:60px;" style="border-top:none;">
                  <span id="iconHideList_<?php echo $module; ?>">
                      <img style="margin-top: 0.8em; margin-right: 10px; margin-left: 10px" id="iconHideList_<?php echo $module; ?>" class="iconLink" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/images/icons/icon_nodispobible.png" title="<?= Yii::app()->getModule('student')->t('lbl: Marcar como inactivo'); ?>" alt="<?= Yii::app()->getModule('student')->t('lbl: Marcar como inactivo'); ?>"/>
                  </span>
                    </td>
                </tr>
            </table>
        </div>

    </td>
</tr>
<tr>
    <td class="thTable filterList">

        <div class="table-responsive">
            <table class="table tableListTH">
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <input type="checkbox" id="check_all_<?php echo $module; ?>" data-module='<?php echo $module; ?>' <?php if ($checkedList === 'all') echo "checked"; ?>>
                    </td>
                </tr>
            </table>
        </div>

    </td>

    <td class="thTable<?php if ( isset($filters['name']) ) echo " active"; ?> filterList">
        <div class="table-responsive">
            <table class="table tableListTH">
                <tr>
                    <td colspan="2"><input type="text" class="inputSearchList tableListFilter" data-module="<?php echo $module; ?>" id="filter_name" value="<?php if ( isset($filters['name']) ) echo $filters['name']; ?>"></td></tr>
                <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_name">
                    <td class="tdTitleTH">
                        <span class="tableHeaders"><?php echo Yii::t('app', 'NAME'); ?></span>
                        <img class="imgQuery hidden" src="/images/query_grey3.png" data-toggle="tooltip" data-original-title='<?php echo StudentModule::t('Info del nombre'); ?>'>
                    </td>
                    <?php if ( $orderBy == 'name_desc' || $orderBy == 'surname_desc' ) { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
                    <?php } else { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
                    <?php } ?>
                </tr>
            </table>
        </div>

    </td>
    <td class="thTable<?php if ( isset($filters['surname']) ) echo " active"; ?> filterList" style="border-right: 1px solid #dddddd;">

        <div class="table-responsive">
            <table class="table tableListTH">
                <tr><td colspan="2"><input type="text" class="inputSearchList tableListFilter" data-module="<?php echo $module; ?>" id="filter_surname" value="<?php if ( isset($filters['surname']) ) echo $filters['surname']; ?>"></td></tr>
                <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_surname">
                    <td class="tdTitleTH">
                        <span><?php echo StudentModule::t( 'SURNAME' ); ?></span>
                        <img class="imgQuery hidden" src="/images/query_grey3.png" data-toggle="tooltip" data-original-title='<?php echo StudentModule::t('Info del apellido'); ?>'>
                    </td>
                    <?php if ( $orderBy == 'name_desc' || $orderBy == 'surname_desc' ) { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
                    <?php } else { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
                    <?php } ?>
                </tr>
            </table>
        </div>

    </td>
    <td class="thTable<?php if ( isset($filters['email']) ) echo " active"; ?> filterList">

        <div class="table-responsive">
            <table class="table tableListTH">
                <tr><td colspan="2"><input type="text" class="inputSearchList tableListFilter" data-module="<?php echo $module; ?>" id="filter_email" value="<?php if ( isset($filters['email']) ) echo $filters['email']; ?>"></td></tr>
                <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_email">
                    <td class="tdTitleTH">
                        <span><?php echo StudentModule::t( 'EMAIL' ); ?></span>
                        <img class="imgQuery hidden" src="/images/query_grey3.png" data-toggle="tooltip" data-original-title='<?php echo StudentModule::t('Info del apellido'); ?>'>
                    </td>
                    <?php if ( $orderBy == 'email_desc' ) { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
                    <?php } else { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
                    <?php } ?>
                </tr>
            </table>
        </div>

    </td>
    <td class="thTable<?php if ( isset($filters['level']) ) echo " active"; ?> filterList">

        <div class="table-responsive">
            <table class="table tableListTH">
                <tr>
                    <td colspan="2">
                        <?php
                        if(isset($filters['level']))
                        {
                            $currentLevel=$filters['level'];
                        }
                        else
                        {
                            $currentLevel=0;
                        }
                        echo chtml::dropDownList('filter_level',$currentLevel, $levels ,array('empty'=>'--', 'data-module'=>$module, 'class'=>'customSelect tableListFilter'));
                        ?>
                    </td>
                </tr>
                <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_level">
                    <td class="tdTitleTH">
                        <span><?php echo StudentModule::t( 'LEVEL' ); ?></span>
                        <img src="/images/query_grey3.png" class="tooltipGreat" title="<?php echo StudentModule::t('Una vez el alumno haga el test, verás sus resultados'); ?>">
                    </td>
                    <?php if ( $orderBy == 'level_desc' ) { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
                    <?php } else { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
                    <?php } ?>
                </tr>
            </table>
        </div>

    </td>
    <td class="thTable<?php if ( isset($filters['expires']) ) echo " active"; ?> filterList">

      <div class="table-responsive">
        <table class="table tableListTH">
          <tr><td colspan="2"><input type="text" class="inputSearchList tableListFilter" data-module="<?php echo $module; ?>" id="filter_expires" value="<?php if ( isset($filters['expires']) ) echo $filters['expires']; ?>"></td></tr>
          <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_expires">
            <td class="tdTitleTH">
              <span><?php echo StudentModule::t( 'EXPIRES' ); ?></span>
              <img class="tooltipGreat" src="/images/query_grey3.png" title="<?php echo StudentModule::t('Observa cuando finalizan las licencias de tus alumnos'); ?>">
            </td>
            <?php if ( $orderBy == 'expires_desc' ) { ?>
              <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
            <?php } else { ?>
              <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
            <?php } ?>
          </tr>
        </table>
      </div>

    </td>
    <td class="thTable<?php if ( isset($filters['group']) ) echo " active"; ?> filterList">

        <div class="table-responsive">
            <table class="table tableListTH">
                <tr>
                    <td colspan="2">
                        <?php
                        if(isset($filters['group']))
                        {
                            $currentGroup=$filters['group'];
                        }
                        else
                        {
                            $currentGroup=0;
                        }
                        echo chtml::dropDownList('filter_group',$currentGroup, $groups ,array('empty'=>'--', 'data-module'=>$module, 'class'=>'customSelect tableListFilter'));
                        ?>
                    </td>
                </tr>
                <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_group">
                    <td class="tdTitleTH">
                        <span><?php echo StudentModule::t( 'GROUP' ); ?></span>
                        <img class="tooltipGreat" src="/images/query_grey3.png" title="<?php echo StudentModule::t('Ordena a tus alumnos por departamentos o delegaciones'); ?>">
                    </td>
                    <?php if ( $orderBy == 'group_desc' ) { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
                    <?php } else { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
                    <?php } ?>
                </tr>
            </table>
        </div>

    </td>
    <td class="thTable<?php if ( isset($filters['period']) ) echo " active"; ?> filterList">

        <div class="table-responsive">
            <table class="table tableListTH">
                <tr>
                    <td colspan="2">
                  <span class='css-select-moz'>
                    <?php
                    $currentPeriod = (isset($filters['period'])) ? $filters['period'] : 0 ;
                    echo chtml::dropDownList('filter_period',$currentPeriod, $periods ,array('empty'=>'--', 'data-module'=>$module, 'class'=>'customSelect tableListFilter'));
                    ?>
                  </span>
                    </td>
                </tr>
                <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_period">
                    <td class="tdTitleTH">
                        <span><?php echo Yii::t('app','PERIOD'); ?></span>
                        <img class="tooltipGreat" src="/images/query_grey3.png" title="<?php echo StudentModule::t('Ordena a tus alumnos por periodos de tiempo o convocatorias'); ?>">
                    </td>
                    <?php if ( $orderBy == 'period_desc' ) { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
                    <?php } else { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
                    <?php } ?>
                </tr>
            </table>
        </div>

    </td>
    <td class="thTable<?php if ( isset($filters['status']) ) echo " active"; ?> filterList">

        <div class="table-responsive">
            <table class="table tableListTH">
                <tr>
                    <td colspan="2">
                  <span class='css-select-moz'>
                    <?php
                    $arStatus = array(
                        StudentGroup::_STATUS_ACTIVE => Yii::app()->getModule('student')->t('lbl: Activo'),
                        StudentGroup::_STATUS_INACTIVE => Yii::app()->getModule('student')->t('lbl: Inactivo')
                    );
                    $availableStatus = array(
                      array('id' => StudentGroup::_STATUS_ACTIVE, 'name' => Yii::app()->getModule('student')->t('lbl: Activos') ),
                      array('id' => StudentGroup::_STATUS_INACTIVE, 'name' => Yii::app()->getModule('student')->t('lbl: Inactivos') ),
                    );
                    $availableStatus = CHtml::listData($availableStatus, 'id', 'name');
                    $currentStatus = (isset($filters['status'])) ? $filters['status'] : 0 ;
                    echo chtml::dropDownList('filter_status',$currentStatus, $availableStatus ,array('empty'=>'--', 'data-module'=>$module, 'class'=>'customSelect tableListFilter'));
                    ?>
                  </span>
                    </td>
                </tr>
                <tr class="orderedColumn" data-module="<?php echo $module; ?>" id="ordered_status">
                    <td class="tdTitleTH">
                        <span><?php echo strtoupper(Yii::app()->getModule('student')->t('lbl: Status')); ?></span>
                        <img class="tooltipGreat" src="/images/query_grey3.png" title="<?php echo Yii::app()->getModule('student')->t('lbl: Ordena a tus alumnos por su estado'); ?>">
                    </td>
                    <?php if ( $orderBy == 'status_desc' ) { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby_desc.png"></td>
                    <?php } else { ?>
                        <td class="tdTitleOrder"><img class="imgOrderBy" src="/images/icon_orderby.png"></td>
                    <?php } ?>
                </tr>
            </table>
        </div>

    </td>
</tr>

<?php if ( !empty($mlStudents) ) { ?>
    <?php foreach( $mlStudents as $mStudent ) { ?>
        <tr class="rowList<?php if( $mStudent->checked ) echo " highlight" ?>" data-module="<?php echo $module; ?>" data-rowid="<?php echo $mStudent->id; ?>">
            <td class="rowListLeft">
                <input type="checkbox" data-module='<?php echo $module; ?>' id="check_<?php echo $mStudent->id; ?>" <?php if( $mStudent->checked ) echo "checked" ?>>
            </td>
            <td class="rowListLeft tdListLink">
              <span>
                  <?php echo $mStudent->name; ?>
              </span>
            </td>
            <td class="rowListLeft rowListLeftLast tdListLink">
                <table>
                    <tr>
                        <td>
                      <span>
                          <?php echo $mStudent->surname; ?>
                      </span>
                        </td>
                        <!--              <td><a href="<?php echo Yii::app()->baseUrl; ?>/followup/followup/viewByUser/?id=<?php echo $mStudent->id; ?>"><img src="/images/arrow.png" alt="<?php echo StudentModule::t('User info'); ?>"/></a></td> -->
                        <td>
                            <img src="<?php echo Yii::app()->baseUrl; ?>/images/arrow.png">
                        </td>
                    </tr>
                </table>
            </td>
            <!--          <td title="<?php echo CJavaScript::quote($mStudent->email);?>"><span><?php if ( strlen($mStudent->email) > 20 ) echo substr($mStudent->email, 0, 17).'...'; else echo $mStudent->email; ?></span></td> -->
            <td title="<?php echo CJavaScript::quote($mStudent->email);?>">
              <span>
                  <?php echo $mStudent->email; ?>
              </span>
            </td>
            <td>
              <span>
                  <?php
                  if($mStudent->Level=='')
                  {
                      echo '-';
                  }
                  else
                  {
                      echo $mStudent->Level;
                  }
                  ?>
              </span>
            </td>
            <td>
              <?php
              if($mStudent->expires=='2000-01-01')
              {
                echo "<span>";
                echo StudentModule::t('No license');
                echo "</span>";
              }
              elseif((time()-(60*60*24)) > strtotime($mStudent->expires))
              {
                echo "<span style='color: #ff0000'>";
                echo $mStudent->expires;
                echo "</span>";
              }
              else
              {
                echo "<span>";
                echo $mStudent->expires;
                echo "</span>";
              }
              ?>
            </td>
            <td><span><?php echo $mStudent->group; ?></span></td>
            <td>
              <span>
                  <?php
                  if($mStudent->period=='')
                  {
                      echo '-';
                  }
                  else
                  {
                      echo $mStudent->period;
                  }
                  ?>
              </span>
            </td>
            <td>
              <span>
                  <?php
                  if($mStudent->status=='')
                  {
                      echo '-';
                  }
                  else
                  {
                      echo $arStatus[$mStudent->status];
                  }
                  ?>
              </span>
            </td>
        </tr>
    <?php } ?>
<?php } else if ( !empty( $filters ) ) { ?>
    <tr class="rowList">
        <td colspan="3" style="background-color: #f3f3f3;"></td>
        <td colspan="6">

            <table width="100%" height="100%">
                <tr><td style="width: 100%; text-align: center; padding: 2em; padding-bottom: 1em;"><img src="/images/NoSearchStudent.png"></td></tr>
                <tr><td style="width: 100%; text-align: center;"><span style="font-family: 'MuseoSans700', Arial, sans-serif; font-size: 1em; text-transform: uppercase; color: #962428;"><?php echo StudentModule::t('No se ha encontrado al alumno'); ?></span></td></tr>
                <tr><td style="width: 100%; text-align: center;"><span style="font-family: 'MuseoSlab500', Arial, sans-serif; font-size: 1em;"><?php echo StudentModule::t('Por favor, verifica que has escrito correctamente lo que buscas.'); ?></span></td></tr>
                <tr><td><br></td></tr>
            </table>

        </td>
    </tr>
<?php } else { ?>
    <tr class="rowList">
        <td colspan="3" style="background-color: #f3f3f3;"></td>
        <td colspan="6">

            <table width="100%" height="100%">
                <tr><td style="width: 100%; text-align: center; padding: 2em;"><img src="/images/emptyListBackground.png"></td></tr>
                <tr><td style="width: 100%; text-align: center;"><span style="font-family: 'MuseoSans700', Arial, sans-serif; font-size: 1em; text-transform: uppercase; color: #45555f;"><?php echo StudentModule::t('No has añadido a ningún alumno todavia'); ?></span></td></tr>
                <tr><td style="width: 100%; text-align: center;"><span style="font-family: 'MuseoSlab500', Arial, sans-serif; font-size: 1em;"><?php echo StudentModule::t('Recuerda que puedes añadirlos uno a uno o importarlos todos directamente desde un CSV'); ?></span></td></tr>
            </table>

        </td>
    </tr>
<?php } ?>

<?php if ( !( $pag == 1 && $maxPag == 1 ) && !empty($mlStudents) ) { ?>
    <tr>
        <td colspan="9">
            <div class="table-responsive">
                <table class="table tableFooterList">
                    <tr>
                        <td>
                            <div id="divExportToCsv" style="float: left; margin-top: 1em;"><img src="/images/csv-icon.png" alt="Exportar a CSV" title="Exportar a CSV"></div>

                            <ul id="paginationList" class="pagination">
                                <?php if ( $pag == 1 ) { ?>
                                    <li class="disabled"><span>&laquo;</span></li>
                                <?php } else { ?>
                                    <li><a id="aListPage_<?php echo ($pag-1); ?>" href="#" data-module="<?php echo $module; ?>">&laquo;</a></li>
                                <?php } ?>

                                <?php for( $i = $pag - 5; $i <= ($pag + 4); $i++ ) { ?>
                                    <?php if ( $i <= 0 ) continue; ?>
                                    <?php if ( $i > $maxPag ) continue; ?>
                                    <?php if ( $i == $pag ) { ?>
                                        <li class="active"><span><?php echo $i; ?> <span class="sr-only">(current)</span></span></li>
                                    <?php } else { ?>
                                        <li><a id="aListPage_<?php echo $i; ?>" href="#" data-module="<?php echo $module; ?>"><?php echo $i; ?></a></li>
                                    <?php } ?>
                                <?php } ?>

                                <?php if ( $pag == $maxPag ) { ?>
                                    <li class="disabled"><span>&raquo;</span></li>
                                <?php } else { ?>
                                    <li><a id="aListPage_<?php echo ($pag+1); ?>" href="#" data-module="<?php echo $module; ?>">&raquo;</a></li>
                                <?php } ?>
                            </ul>

                        </td>
                    </tr>
                </table>
            </div>

        </td>
    </tr>
<?php } else { ?>
    <tr>
    <td colspan="9">
        <div class="table-responsive">
            <table class="table tableFooterList">
                <tr>
                    <td>
                        <div id="divExportToCsv" style="float: left; margin-top: 1em;"><img src="/images/csv-icon.png" alt="Exportar a CSV" title="Exportar a CSV"></div>
                    </td>
                </tr>
            </table>
        </div>

    </td>
    </tr>
<?php } ?>


</table>
</div>
<!--    
  </div>
</div> -->

<script>
var checkedList_<?php echo $module; ?> = '<?php echo CJavaScript::quote($checkedList); ?>';
var pag_<?php echo $module; ?> = '<?php echo $pag; ?>';
var undoSetTimeout_<?php echo $module; ?> = null;
var numElements_<?php echo $module; ?> = '<?php echo $numElements; ?>';
var OrderBy_<?php echo $module; ?> = '<?php echo $orderBy; ?>';

$(document).ready( function() {

    $("#divExportToCsv").click(function() {
        var module = '<?php echo $module; ?>';
        var filters = [];
        $('.tableListFilter').each( function() {
            var module_tmp = $(this).data('module');
            if ( module == module_tmp ) {
                filters.push( { key: this.id, value: $(this).val() } );
            }
        });

        var parameters = {
            checkedList: checkedList_<?php echo $module; ?>,
            filters: JSON.stringify(filters)
        };
        var pageURL = '/<?php echo $module; ?>/main/export';
        ABAChangePage( pageURL, parameters, false, '_blank' );
    });

    $(".rowList").hover(function() {
        if(!$(this).hasClass("highlight"))
        {
            $(this).addClass("mouseOverColor");
        }

    }, function(){
        $(this).removeClass("mouseOverColor");
    });

    $(".filterList").hover(function() {
        if(!$(this).hasClass("highlightFilter"))
        {
            $(this).addClass("highlightFilter");
        }

    }, function(){
        $(this).removeClass("highlightFilter");
    });




    $(":checkbox", this).click(function() {
        var item2 = $(this).closest("tr");
        item2.removeClass("mouseOverColor");
        var selected = item2.hasClass("highlight");
        if(!selected)
        {
            if($(this).attr('id')!='check_all_<?php echo $module; ?>')
            {
                item2.addClass("highlight");
            }
            $(":checkbox", this).prop('checked', true);
        }
        else
        {
            item2.removeClass("highlight");
            $(":checkbox", this).prop('checked', false);
        }
    });


    $('.tooltipGreat').tooltipster({
        position: 'top-right',
        offsetX: 9
    });

    <?php if ( $displayNewElement ) { ?>
    $('#divDisplayFiltros_<?php echo $module; ?>').trigger('click');
    <?php } ?>

    ABAActivarFiltros();
    ABAActivarPaginador();
    ABAActivarQueryTooltips();
    ABAActivarOrderBy();
    ABAActivarChecks();

    $('.tdListLink').each( function() {
        var parent = $(this).parent();
        var module = $(parent).data("module");
        if ( module == '<?= $module ?>' ) {
            var rowId = $(parent).data("rowid");

            $(this).click( function() {
                var pageURL = '<?= Yii::app()->baseUrl; ?>/followup/followup/viewByUser?id='+rowId;
                var parameters = {
//            email: email,
                };
                ABAChangePage( pageURL, parameters );
            })
        }
    });

    $('#btnEnviarTest').click( function() {
        var module = '<?php echo $module; ?>';

        var filters = [];
        $('.tableListFilter').each( function() {
            var module_tmp = $(this).data('module');
            if ( module == module_tmp ) {
                filters.push( { key: this.id, value: $(this).val() } );
            }
        });

        var url = '/student/main/SendLevelTest';
        var parameters = {
            checkedList: checkedList_<?php echo $module; ?>,
            filters: filters,
            step: 0,
        };
        var successF = function(data) {
            if ( data.ok ) {
                ABAShowDialog( data.html, true );
            } else {
                ABAShowErrorMsgList( '<?php echo $module; ?>', data.errorMsg );
                ABAStopLoading();
            }
        }

        ABAShowLoading();
        ABALaunchAjax( url, parameters, successF );
    });

    $('#iconDeleteList_<?php echo $module; ?>').click( function() {
        var module = '<?php echo $module; ?>';

        if ( !confirm('<?php echo StudentModule::t('Esta seguro de querer borrar este/os alumno/s ?'); ?>')) return;

        var filters = [];
        $('.tableListFilter').each( function() {
            var module_tmp = $(this).data('module');
            if ( module == module_tmp ) {
                filters.push( { key: this.id, value: $(this).val() } );
            }
        });

        var url = '/student/main/delete';
        var parametros = {
            checkedList: checkedList_<?php echo $module; ?>,
            filters: filters
        };

        var successF = function(data) {
            if ( data.ok ) {
                ABAShowOkMsgList( '<?php echo $module; ?>', data.okMsg );
                ABAActivarUndoList( '<?php echo $module; ?>' );
                ABAStopLoading();
            } else {
                ABAStopLoading();
                ABACustomDialog('ERROR', data.errorMsg, false, true);
            }
        }

        ABAShowLoading();
        ABALaunchAjax( url, parametros, successF );

    });

    $('#iconHideList_<?php echo $module; ?>').click( function() {
        var module = '<?php echo $module; ?>';

        if ( !confirm('<?php echo Yii::app()->getModule('student')->t('lbl: Esta seguro de querer desactivar este/os alumno/s ?'); ?>')) return;

        var filters = [];
        $('.tableListFilter').each( function() {
            var module_tmp = $(this).data('module');
            if ( module == module_tmp ) {
                filters.push( { key: this.id, value: $(this).val() } );
            }
        });

        var url = '/student/main/hide';
        var parametros = {
            checkedList: checkedList_<?php echo $module; ?>,
            filters: filters
        };

        var successF = function(data) {
            if ( data.ok ) {
                ABAShowOkMsgList( '<?php echo $module; ?>', data.okMsg );
                ABAStopLoading();
                setTimeout( function() {
                    ABARefreshList(module);
                }, 3000 );
            } else {
                ABAStopLoading();
                ABACustomDialog('ERROR', data.errorMsg, false, true);
            }
        }

        ABAShowLoading();
        ABALaunchAjax( url, parametros, successF );
    });

    $('#btnActivateLicense').click( function() {
        var module = '<?php echo $module; ?>';

        var filters = [];
        $('.tableListFilter').each( function() {
            var module_tmp = $(this).data('module');
            if ( module == module_tmp ) {
                filters.push( { key: this.id, value: $(this).val() } );
            }
        });

        var url = "/<?php echo $module; ?>/main/preAsignlicense";
        var parametros = {
            checkedList: checkedList_<?php echo $module; ?>,
            filters: filters
        };

        var successF = function(data) {
            ABAStopLoading();
            var module = '<?php echo $module; ?>';

            if ( !data.ok && data.html != '' ) {
                ABAShowDialog( data.html, true );
                return false;
            }

            if ( data.checkedList == '' || data.checkedList == '0' ) {
                ABAShowErrorMsgList( '<?php echo $module; ?>', <?php echo CJavaScript::encode(StudentModule::t('You have no selection.')); ?> );
                return false;
            }

            var filters = [];
            $('.tableListFilter').each( function() {
                var module_tmp = $(this).data('module');
                if ( module == module_tmp ) {
                    filters.push( { key: this.id, value: $(this).val() } );
                }
            });

            var parameters = {
                checkedList: checkedList_<?php echo $module; ?>,
                filters: JSON.stringify(filters)
            };
            var pageURL = '/<?php echo $module; ?>/license';
            ABAChangePage( pageURL, parameters );
        }


        ABALaunchAjax( url, parametros, successF );
    });

    ABARedrawBody();
});

function sendLevelTestStep2() {
    var module = '<?php echo $module; ?>';

    var filters = [];
    $('.tableListFilter').each( function() {
        var module_tmp = $(this).data('module');
        if ( module == module_tmp ) {
            filters.push( { key: this.id, value: $(this).val() } );
        }
    });

    var url = '/student/main/SendLevelTest';
    var parameters = {
        checkedList: checkedList_<?php echo $module; ?>,
        filters: filters,
        step: 2,
    };
    var successF = function(data) {
        if ( data.ok ) {
            ABAShowOkMsgList( '<?php echo $module; ?>', data.okMsg );
        } else {
            ABAShowErrorMsgList( '<?php echo $module; ?>', data.errorMsg );
        }
        ABAStopLoading();
    }

    ABAShowLoading();
    ABALaunchAjax( url, parameters, successF );
}

</script>
