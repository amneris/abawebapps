<table height="100%" width='100%'>
<tr>
  <td>
    <div style="font-size: 20px; color:green; text-align: center;">
      <img src="/images/tick3.png" width="48" style="margin: 20px;">
    </div>

    <div style="text-align: center;">
      <span style="font-weight: bold; font-size: 1.3em;"><?php echo Yii::app()->getModule('student')->t('¡Enhorabuena {nombre}!', array('{nombre}' => $nombre)); ?></span><br>
      <span style="font-weight: bold; font-size: 1.3em;"><?php echo Yii::app()->getModule('student')->t('Ya te has registrado en la plataforma corporativa de ABA English.'); ?></span><br>
      <span style="font-weight: bold; font-size: 1.3em;"><?php echo Yii::app()->getModule('student')->t('Activa tu licencia mensual de prueba y conoce nuestro método.'); ?></span><br>
      <br>
    </div>

    <div id="divBtnCloseOkLicense">
      <button id="btnCloseOkLicense" class="abaButton abaButton-Primary abaExtranetButton-Ok">
        <?php echo Yii::app()->getModule('student')->t('Ok'); ?>
      </button>
    </div>
  </td>
</tr>
<tr><td>&nbsp;</td></tr>
</table>

<script>
  $(document).ready( function() {
    $('#btnCloseOkLicense').click( function() {
      ABACloseDialog();
    });
    
    $('.fancybox-close').click( function() {
//      ABARefreshList( 'student' );
      var pageURL = '/student/main'
      var parameters = {};

      ABAChangePage( pageURL, parameters );
    });
    
  });
</script>  
