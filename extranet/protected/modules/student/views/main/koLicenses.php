<table height="100%" width='100%'>
<tr>
  <td>
    <div style="font-size: 20px; color:green; text-align: center;">
      <img src="/images/redalert.png" width="48" style="margin: 20px; width: 3.25em;">
    </div>

    <div style="text-align: center;">
      <span style='font-family: "MuseoSlab500", Arial, Sans-serif; font-weight: bold; font-size: 1.479166667em; color:red;'><?php echo StudentModule::t('No dispone de licencias suficientes.'); ?></span><br>
      <span style='font-family: "MuseoSlab700", Arial, Sans-serif; font-size: 1.155em;'><?php echo StudentModule::t('Para activar licencias, antes necesita adquirirlas.'); ?></span><br>
<!--      <span style='font-family: "MuseoSlab700", Arial, Sans-serif; font-size: 1.155em;'><?php echo StudentModule::t('Visite nuestra página de Planes y elija el que mejor le convenga.'); ?></span><br><br> -->
    </div>

    <!--
    <div id="divBtnCloseOkLicense">
      <button id="btnCloseOkLicense" class="abaButton abaButton-Primary abaExtranetButton-Ok">
        <?php echo StudentModule::t('Ver precios'); ?>
      </button>
    </div>
    -->
  </td>
</tr>
<tr><td>&nbsp;</td></tr>
</table>

<script>
  $(document).ready( function() {
    $('#btnCloseOkLicense').click( function() {
      var pageURL = '/price/main';
      var parameters = {};
      
      ABAChangePage( pageURL, parameters );
    });
  });
</script>