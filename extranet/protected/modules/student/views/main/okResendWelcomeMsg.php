<table height="100%" width='100%'>
<tr>
  <td>
    <div style="font-size: 20px; color:green; text-align: center;">
      <img src="/images/tick3.png" width="48" style="margin: 20px;">
    </div>

    <div style="text-align: center;">
      <span style="font-weight: bold; font-size: 1.3em;"><?php echo Yii::app()->getModule('student')->t('lbl: Email sent successfully to the student.'); ?></span><br>
    </div>

    <div id="divBtnCloseOkLicense" style="display: none;">
      <button id="btnCloseOkLicense" class="abaButton abaButton-Primary abaExtranetButton-Ok">
        <?php echo Yii::app()->getModule('student')->t('lbl: Ok'); ?>
      </button>
    </div>
  </td>
</tr>
<tr><td>&nbsp;</td></tr>
</table>

<script>
  $(document).ready( function() {
    $('#btnCloseOkLicense').click( function() {
      abaCore,closeDialog();
    });
    
    $('.fancybox-close').click( function() {
      abaCore,closeDialog();
    });
    
  });
</script>  
