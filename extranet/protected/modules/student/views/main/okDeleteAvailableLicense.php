<table height="100%" width='100%'>
  <tr>
    <td>
      <div style="font-size: 20px; color:green; text-align: center;">
        <img src="/images/tick3.png" width="48" style="margin: 20px; width: 3.25em;">
      </div>

      <div style="text-align: center;">
        <span style='font-family: "MuseoSlab500", Arial, Sans-serif; font-weight: bold; font-size: 1.479166667em;'><?php echo Yii::app()->getModule('student')->t('lbl: Licencias suprimidas'); ?></span><br><br>
        <!--      <span style='font-family: "MuseoSlab700", Arial, Sans-serif; font-size: 1.155em;'><?php echo Yii::app()->getModule('student')->t(''); ?></span><br> -->
        <span style='font-family: "MuseoSlab700", Arial, Sans-serif; font-size: 1.155em;'><?php echo Yii::app()->getModule('student')->t('lbl: Las licencias disponibles se han eliminado.'); ?></span><br><br>
      </div>

      <div id="divBtnCloseOkLicense">
        <button id="btnCloseOkLicense" class="abaButton abaButton-Primary abaExtranetButton-Ok">
          <?php echo Yii::app()->getModule('student')->t('lbl: Ok'); ?>
        </button>
      </div>
    </td>
  </tr>
  <tr><td>&nbsp;</td></tr>
</table>

<script>
  $(document).ready( function() {
    $('#btnCloseOkLicense').click( function() {
      var pageURL = '<?= Yii::app()->baseUrl; ?>/student/main';
      var parameters = {};
      ABAChangePage( pageURL, parameters );
    });
  });
</script>