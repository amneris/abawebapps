<?php

?>
<style>
  .tablePreview {
    border: 1px solid #000;
  }

  table.tablePreview > tbody > tr th, table.tablePreview > tbody > tr td {
    padding-left: 5px;
  }
</style>
<table height='100%' width='100%'>
  <tr>
    <td><h4><?php echo Yii::app()->getModule('student')->t('lbl: Preview'); ?></h4></td>
  </tr>
  <tr>
    <td valign="top">
      <div>
        <label for="sCharset"><?php echo Yii::app()->getModule('student')->t('lbl: Formato origen'); ?> : &nbsp;</label>
        <select id='sCharset'>
          <option value='auto'<?php if ( $charset == 'auto' ) echo " selected"; ?>>auto</option>
          <option value='ISO-8859-1'<?php if ( $charset == 'ISO-8859-1' ) echo " selected"; ?>>ISO-8859-1</option>
          <option value='UTF-8'<?php if ( $charset == 'UTF-8' ) echo " selected"; ?>>UTF-8</option>
          <option value='MACINTOSH'<?php if ( $charset == 'MACINTOSH' ) echo " selected"; ?>>MACINTOSH</option>
          <option value='GB2312'<?php if ( $charset == 'GB2312' ) echo " selected"; ?>>GB2312</option>
          <option value='GB18030'<?php if ( $charset == 'GB18030' ) echo " selected"; ?>>GB18030</option>
        </select><br>
        <span style="font-size: 10px;">* <?php echo Yii::app()->getModule('student')->t('lbl: Revise la vista previa para confirmar que el formato de origen es correcto.');?></span>
      </div>

      <div style="height: 150px; overflow: auto; margin-top: 10px; margin-bottom: 10px;">
        <table class="tablePreview" border="1">
          <tr>
            <th><?php echo Yii::app()->getModule('student')->t('Name'); ?></th>
            <th><?php echo ucfirst(strtolower(Yii::app()->getModule('student')->t('SURNAME'))); ?></th>
            <th><?php echo Yii::app()->getModule('student')->t('Email'); ?></th>
            <th><?php echo Yii::app()->getModule('student')->t('Group'); ?></th>
          </tr>
          <?php $count = 0; ?>
          <?php foreach( $arData as $line ) { ?>
          <?php if ( $count++ > 100 ) break; ?>
          <tr>
            <td><?php echo $line['name']; ?></td>
            <td><?php echo $line['surname']; ?></td>
            <td><?php echo $line['email']; ?></td>
            <td><?php echo $line['group']; ?></td>
          </tr>
          <?php } ?>
        </table>
      </div>

      <div id='divBtnCloseOkLicense'>
        <button id='btnCloseOkLicense' class='abaButton abaButton-Primary abaExtranetButton-Ok'>
          <span id="spanOkButton"><?php echo Yii::app()->getModule('student')->t('Ok'); ?></span>
          <img id="imgLoadingButton" src="/images/loading.gif" style="display: none;">
        </button>
      </div>
    </td>
  </tr>
  <tr><td>&nbsp;</td></tr>
</table>

<script>
  var codeUploadFile = <?php echo CJavaScript::encode($code); ?>;

    $(document).ready( function() {
      $('#sCharset').change( function() {
        var url = '/student/main/changecharset';
        var parametros = {
          code: codeUploadFile,
          charset:  $('#sCharset').val()
        };

        var successF = function(data) {
          abaCore.showDialog( data.html, true, '50%', '50%' );
        };

        abaCore.launchAjax( url, parametros, successF );
      });

      var disableButton = false;
      $('#btnCloseOkLicense').click( function() {
        if ( disableButton ) return;
        disableButton = true;

        var url = '/student/main/Uploadfileok';
        var parametros = {
          code: codeUploadFile,
          charset:  $('#sCharset').val()
        };

        var successF = function(data) {
          abaCore.showDialog( data.html, true, '50%', '50%' );
        };

        $('#btnCloseOkLicense').css('background', '#fff');
        $('#btnCloseOkLicense').css('border', '1px solid #000');
        $('#btnCloseOkLicense').css('cursor', 'auto');
        $('#imgLoadingButton').css('margin-top', '-3px' );
        $('#imgLoadingButton').css('display', 'inherit' );
        $('#spanOkButton').css('display', 'none' );

        abaCore.launchAjax( url, parametros, successF );
      });

//    $('.fancybox-close').click( function() {
////      ABARefreshList( 'student' );
//      var pageURL = '/student/main'
//      var parameters = {};
//
//      ABAChangePage( pageURL, parameters );
//    });

    });
</script>