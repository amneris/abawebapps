<?php

  if ( $numero == 1 ) $personas = StudentModule::t('persona');
  else $personas = StudentModule::t('personas');

?>

<style>
  .popupTitleFont {
    font-family: "MuseoSlab500";
    font-weight: bold;
    font-size: 1.479166667em;
  }

  .popupTextFont {
    max-width: 36.666666667em;
    margin:auto;
  }

  .popupTextFont span {
    font-family: "MuseoSlab500";
    font-size: 1.166666667em;
  }
</style>

<table height="100%" width='100%'>
<tr>
  <td>
    <div style="font-size: 20px; color:green; text-align: center;">
      <img src="/images/icoleveltest.png" width="73" style="margin: 20px;">
    </div>

    <div style="text-align: center;">
      <span class="popupTitleFont"><?php echo StudentModule::t('Envio de test'); ?></span><br>
      <div class="popupTextFont">
        <span><?php echo StudentModule::t('A continuación se enviará un test de nivel de {numero} personas.', array( '{numero}' => $numero, '{personas}' => $personas )); ?></span><br><br>
      </div>
      <div class="popupTextFont">
        <span><?php echo StudentModule::t('Recuerda que recibirás un email en tu bandeja de correo con la notificación y descripción del envío.'); ?></span><br><br>
      </div>
    </div>

    <div id="divBtnCloseOkLicense">
      <button id="btnCloseOkLicense" class="abaButton abaButton-Primary abaExtranetButton-Ok">
        <?php echo StudentModule::t('Enviar test ahora'); ?>
      </button>
    </div>
  </td>
</tr>
<tr><td>&nbsp;</td></tr>
</table>

<script>
  $(document).ready( function() {
    $('#btnCloseOkLicense').click( function() {
      ABACloseDialog();
      sendLevelTestStep2();
    });
    
    $('.fancybox-close').click( function() {
      sendLevelTestStep2();

//      var pageURL = '/student/main'
//      var parameters = {};

//      ABAChangePage( pageURL, parameters );
    });
    
  });
</script>  
