<?php
$selectedIndex = 'Alumno';
$numLicencias = $mlLicenseAvailable['Total']->numLicenses;
unset($mlLicenseAvailable['Total']);

$module = 'student';
?>

<div class="row-fluid">
    <div class="col-lg-12">

        <div class="row-fluid">

            <div class="col-lg-6">
                <div class="divSectionTittle">
                    <span class="sectionTitle"><?php echo CHtml::encode(StudentModule::t('Students')); ?></span>
                </div>
            </div>

            <div class="col-lg-6 alignRight">
                <div class="divLicensesPannel">
                    <div class="divTitleAvailableLicenses"><span><?php echo CHtml::encode(StudentModule::t('Available licenses')); ?>: <?php echo $numLicencias; ?></span></div>

                  <?php foreach ($mlLicenseAvailable as $mLicenseAvailable) { ?>
                    <?php /* if ($mLicenseAvailable->id == LicenseType::_Trial) continue; */ ?>
                    <div class="licenseAvailable"><span><?php echo StudentModule::t($mLicenseAvailable->name); ?>: <?php echo $mLicenseAvailable->numLicenses; ?></span>
                      <?php if ( Yii::app()->user->isAgent || Yii::app()->user->isPartner || Yii::app()->user->isAbaManager ) { ?>
                        <div class="glyphicon glyphicon-trash deleteLicenseAvailable" data-id="<?php echo $mLicenseAvailable->id;?>"></div>
                      <?php } ?>
                    </div>
                  <?php } ?>

                </div>
            </div>
        </div>

        <!-- enterprise english level -->
        <?php
        /* we calculate the level with max student.. to set the grid in the chart.. also.. with this var.. if it is iqual or biger that one.. we show or not the chart. */
        $max=0;
        if($levelProgress[1][1]>$max)
        {
            $max = $levelProgress[1][1];
        }
        if($levelProgress[2][1]>$max)
        {
            $max = $levelProgress[2][1];
        }
        if($levelProgress[3][1]>$max)
        {
            $max = $levelProgress[3][1];
        }
        if($levelProgress[4][1]>$max)
        {
            $max = $levelProgress[4][1];
        }
        if($levelProgress[5][1]>$max)
        {
            $max = $levelProgress[5][1];
        }
        if($levelProgress[6][1]>$max)
        {
            $max = $levelProgress[6][1];
        }

        $progressCollapsed = ($max >= 1) ? false : true;

        ?>

            <div class="row-fluid">
                <div class="col-lg-4">
                    <div id="divDisplay_progress" class='divDisplayFiltros'>
                        <span class="newRegLabel"><?php echo CHtml::encode(StudentModule::t('Enterprise english level')); ?>
                            <img id='imgDisplay_progress' src='<?php echo Yii::app()->baseUrl; if(!$progressCollapsed) echo "/images/less.png"; else echo "/images/plus.png"; ?>' width="11" height="11">
                            <img src="/images/graph.png" width="20" height="20">
                        </span>
                    </div>
                </div>
            </div>
            <div class="row-fluid" id="progressDiv" style="width: 100%" <?php if($progressCollapsed) echo "hidden='true'";?>>
                <div class="col-lg-12" style="width: 100%">
                    <div class="table-responsive divTableFiltros" id="divTable_progress" style="width: 100%">
                        <?php

                        $divisor = round($max / 10);

                        if ($divisor == 0) {
                            $divisor = 1;
                        }
                        $ticksSt = [$divisor, $divisor * 2, $divisor * 3, $divisor * 4, $divisor * 5, $divisor * 6, $divisor * 7, $divisor * 8, $divisor * 9, $divisor * 10, $divisor * 11];

                        $this->widget('ext.Hzl.google.HzlVisualizationChart', array('visualization' => 'BarChart',
                            'data' => $levelProgress,
                            'options' => array(
                                'width' => 936,
                                'animation' => array(
                                    'duration' => 2000,
                                    'startup' => 'true',
                                    'easing' => 'out'
                                ),
                                'backgroundColor' => '#ffffff',
                                'colors' => array('#00b7e5'),
                                'legend' => array('position' => 'none'),
                                'fontSize' => 11,
                                'fontName' => 'MuseoSans700',

                                'tooltip' => array(
                                    'textStyle' => array(
                                        'color' => '#455560',
                                        'italic' => 'false',
                                    ),
                                ),

                                'vAxis' => array(
                                    'textStyle' => array(
                                        'color' => '#455560',
                                        'italic' => 'false',
                                    ),
                                    'gridlines' => array(
                                        'color' => 'transparent'
                                    )),

                                'hAxis' => array(
                                    'ticks' => $ticksSt,
                                    'baselineColor' => '#f6f6f6',
                                    'title' => CHtml::encode(StudentModule::t('Students')),
                                    'titleTextStyle' => array(
                                        'color' => '#455560',
                                        'italic' => 'false'
                                    ),
                                    'textStyle' => array(
                                        'color' => '#455560',
                                        'italic' => 'false',
                                    ),
                                    'gridlines' => array(
                                        'color' => '#f6f6f6',
                                        'count' => -1
                                    )
                                ),

                                'bar' => array('groupWidth' => '61.8%'),
                                'chartArea' => array('left' => 130, 'top' => 10, 'width' => '80%', 'height' => '75%'),
                            )
                        ));

                        ?>
                    </div>

                </div>
            </div>

        <!--end enterprise english level-->

        <div class="row-fluid">
            <div class="col-lg-4">
                <div id="divDisplayFiltros_<?php echo $module; ?>" class='divDisplayFiltros'><span class="newRegLabel"><?php echo CHtml::encode(StudentModule::t('ADD NEW STUDENT')); ?> <img id='imgDisplayFiltros_<?php echo $module; ?>' src='<?php echo Yii::app()->baseUrl; ?>/images/less.png' width="11" height="11"></span></div>
            </div>
            <div class="col-lg-8">
                <div><span class="spanMsgList spanMsgAddList" id="msgList_1_<?php echo $module; ?>" style="display:none"></span></div>
            </div>
        </div>

        <div class="row-fluid">
            <div class="col-lg-12">
                <div class="table-responsive divTableFiltros" id="divTableFiltros_<?php echo $module; ?>">
                    <table class="table tableFiltros">
                        <tr>
                            <td class="tdNewreg">
                                <?php echo CHtml::label(CHtml::encode(Yii::t('app','NAME')), 'newStudentName'); ?>*<br>
                                <?php echo CHtml::textField('newStudentName', '', array('id' => 'newStudentName', 'class' => 'form-control addCamp_'.$module, 'onblur'=>'js:validateField("newStudentName")')); ?>
                                <span id="newStudentNameError" class="abaValidatorError" style="display: none;"><?php echo Yii::t('app', 'Name field required.'); ?></span>
                            </td>
                            <td class="tdNewreg">
                                <?php echo CHtml::label(CHtml::encode(StudentModule::t('SURNAME')), 'newStudentSurname'); ?>*<br>
                                <?php echo CHtml::textField('newStudentSurname', '', array('id' => 'newStudentSurname', 'class' => 'form-control addCamp_'.$module, 'onblur'=>'js:validateField("newStudentSurname")')); ?>
                                <span id="newStudentSurnameError" class="abaValidatorError" style="display: none;"><?php echo Yii::t('app', 'Surnames field required.'); ?></span>
                            </td>
                            <td class="tdNewreg">
                                <?php echo CHtml::label(CHtml::encode(StudentModule::t('EMAIL')), 'newStudentEmail'); ?>*<br>
                                <?php echo CHtml::textField('newStudentEmail', '', array('id' => 'newStudentEmail', 'class' => 'form-control addCamp_'.$module, 'onblur'=>'js:validateEmail("newStudentEmail")')); ?>
                                <span id="newStudentEmailError" class="abaValidatorError" style="display: none;"><?php echo Yii::t('app', 'The email is not valid or blank.'); ?></span>
                            </td>
                            <td class="tdNewreg">
                                <?php echo CHtml::label(CHtml::encode(StudentModule::t('GROUP')), 'newStudentGroup'); ?>
                                <img class="tooltipTop" src="/images/query_grey3.png" title="<?php echo StudentModule::t('Asigna a tus alumnos por departamentos o delegaciones. Es opcional.'); ?>">
                                <br>
                                <?php echo CHtml::textField('newStudentGroup', '', array('id' => 'newStudentGroup', 'class' => 'form-control addCamp_'.$module)); ?>
                                <div id="divGroupLiveSearch"></div>
                            </td>
                            <td class="tdNewreg">
                                <button id="addButton_<?php echo $module; ?>" type="button" class="abaButton abaButton-Primary" style="margin-top: 21px; width: 100%;">
                                    <?php echo CHtml::encode(StudentModule::t('ADD')); ?>
                                </button>
                            </td>
                        </tr>

                        <tr>
                            <td id='tdImportMessage' class="alignRight" colspan="5">
                                <form id="formUploadCSV" method="post" enctype="multipart/form-data" action="/student/main/uploadfile" target="hidddenIframe" class="hidden"><input name="fileName" id="inputUploadCSV" type="file"/></form>
                                <?php $importCSV = StudentModule::t('import a CSV'); ?>
                                <?php $linkImportFromCSV = '<a id=\"linkUploadCSV\" href=\"#\">{$importCSV}</a>'; eval("\$linkImportFromCSV = \"$linkImportFromCSV\";"); ?>
                                <?php $textToImport = StudentModule::t('Or if you prefer, you can {$linkImportFromCSV}'); eval("\$textToImport = \"$textToImport\";"); ?>
                                <span><?php echo $textToImport; ?></span>
                                <div class="imgQuery imgQueryBlue" id="my-tooltip"></div>
                            </td>
                        </tr>
                    </table>
                </div>

            </div>
        </div>

        <div class="row-fluid">
            <div class="col-lg-12">
                <div class="divTableList" id="divTableList_<?php echo $module; ?>">
                    &nbsp;
                </div>
            </div>
        </div>

        <div class="row-fluid">
            <div class="col-lg-12">
                <hr>
          <span>
               <span class="abaTips"><?php echo StudentModule::t('learningTips'); ?></span>
          </span>
                <br>
                <div class="table-responsive divTableFiltros" style="padding: 1.2em; margin-top: 1.2em;" id="abaTips">
                    <ul style=" list-style-type: none;" id="expList">

                        <li class="expandIcon">
                            <?php echo StudentModule::t('firstLabel'); ?>
                            <ul>
                                <li id="liBody">
                                    <?php echo StudentModule::t('firstText'); ?>
                                    <br>
                                </li>
                            </ul>
                        </li>
                        <br>

                        <li class="expandIcon">
                            <?php echo StudentModule::t('secondLabel'); ?>
                            <ul>
                                <li>
                                    <?php echo StudentModule::t('secondText'); ?>
                                    <br>
                                </li>
                            </ul>
                        </li>
                        <br>

                        <li class="expandIcon">
                            <?php echo StudentModule::t('thirdLabel'); ?>
                            <ul>
                                <li>
                                    <?php echo StudentModule::t('thirdText'); ?>
                                    <br>
                                </li>
                            </ul>
                        </li>
                        <br>

                        <li class="expandIcon">
                            <?php echo StudentModule::t('fourthLabel'); ?>
                            <ul>
                                <li>
                                    <?php echo StudentModule::t('fourthText'); ?>
                                    <br>
                                </li>
                            </ul>
                        </li>
                        <br>

                        <li class="expandIcon">
                            <?php echo StudentModule::t('fifthLabel'); ?>
                            <ul>
                                <li>
                                    <?php echo StudentModule::t('fifthText'); ?>
                                </li>
                            </ul>
                    </ul>
                </div>

                &nbsp;
            </div>
        </div>

    </div>
</div>

<script>

var selectedIndex = '<?php echo CJavaScript::quote($selectedIndex); ?>';
var OrderBy_<?php echo $module; ?> = 'created desc';
var checkedList_<?php echo $module; ?> = '';


$(document).ready( function() {
    $(".deleteLicenseAvailable").click( function() {
      if ( !confirm("<?php echo Yii::app()->getModule('student')->t('lbl: ¿ Está seguro/a de querer eliminar estas licencias ya validadas ?'); ?>") ) return false;

      var id = $(this).data('id');

      var url = '/student/main/deleteavailablelicense';
      var parametros = {
        id: id,
      };

      var successF = function(data) {
        ABAShowDialog( data.html, true, '50%', '50%' );
      }

      ABAShowLoading();
      ABALaunchAjax( url, parametros, successF );
    });

    prepareList();

    $('#my-tooltip').tooltipster({
        content: $('<span><?php echo StudentModule::t('The CSV must contain columns Name, Surname, Email and Group. Group it is optional.'); ?></span>'),
        position: 'top-right',
        offsetX: 5
    });

    $('.tooltipTop').tooltipster({
        position: 'top-right',
        offsetX: 9
    });

    $('#divDisplayFiltros_<?php echo $module; ?>').click( function()
    {

        $('#divTableFiltros_<?php echo $module; ?>').parent().parent().toggle();

        if ( $('#divTableFiltros_<?php echo $module; ?>').parent().parent().is(':visible') )
        {
            $('#imgDisplayFiltros_<?php echo $module; ?>').attr('src', '<?php echo Yii::app()->baseUrl; ?>/images/less.png');
        }
        else
        {
            $('#imgDisplayFiltros_<?php echo $module; ?>').attr('src', '<?php echo Yii::app()->baseUrl; ?>/images/plus.png');
        }

    });

    $('#divDisplay_progress').click( function()
    {

        $('#divTable_progress').parent().parent().toggle();

        if ( $('#divTable_progress').parent().parent().is(':visible') )
        {
            $('#imgDisplay_progress').attr('src', '<?php echo Yii::app()->baseUrl; ?>/images/less.png');
        }
        else
        {
            $('#imgDisplay_progress').attr('src', '<?php echo Yii::app()->baseUrl; ?>/images/plus.png');
        }

    });

//    $('#divDisplayFiltros_<?php echo $module; ?>').trigger('click');

    $('.addCamp_<?php echo $module; ?>').keydown( function (e) {
        if ( e.keyCode == 13 ) $('#addButton_<?php echo $module; ?>').trigger('click');
    });

    $('#linkUploadCSV').click(function() {
        $("#inputUploadCSV:hidden").trigger('click');

        return false;
    });

    $('#inputUploadCSV').change( function() {

        if ( !($( "#hidddenIframe" ).length > 0) ) {
            var ifr=$('<iframe/>', {
                id:'hidddenIframe',
                name:'hidddenIframe',
                src:'about:blank',
                style:'display:none',
                load:function(){
                    var content = $("#hidddenIframe").contents().text();
                    do {
                      var content2 = content;
                      content = content2.replace( "|||", "<" );
                    } while( content2 != content );
                    var data = JSON.parse(content);
                    abaCore.stopLoading();
                    abaCore.showDialog( data.html, true, '50%', '50%' );
                }
            });
            $('body').append(ifr);
        }

        abaCore.showLoading();
        $('#formUploadCSV').submit();
    });

    $(".imgQuery").tooltip({
        placement : 'top'
    });


    $('#addButton_<?php echo $module; ?>').click( function(e) {
        e.preventDefault();
        hideGroupLiveSearch();

        if ( $('#newStudentName').val() == '' ) {

            $('#newStudentName').focus();
            for(i=0;i<3;i++)
            {
                $('#newStudentNameError').fadeTo(100, 0.1).fadeTo(100, 1.0);
            }
            return false;
        }

        if ( $('#newStudentSurname').val() == '' ) {
            $('#newStudentSurname').focus();
            for(i=0;i<3;i++)
            {
                $('#newStudentSurnameError').fadeTo(100, 0.1).fadeTo(100, 1.0);
            }
            return false;
        }

        if ( ($('#newStudentEmail').val() == '') || (!ABAValidateEmail( $('#newStudentEmail').val())) )
        {
            $('#newStudentEmail').focus();
            for(i=0;i<3;i++)
            {
                $('#newStudentEmailError').fadeTo(100, 0.1).fadeTo(100, 1.0);
            }
            return false;
        }

//        if ( $('#newStudentGroup').val() == '' ) {
//            alert('<?php echo StudentModule::t('Group mustn\\\'n empty.'); ?>');
//            $('#newStudentGroup').focus();
//            return false;
//        }

        ABAShowLoading();
        var parametros = {
            "newStudentName" : $('#newStudentName').val(),
            "newStudentSurname" : $('#newStudentSurname').val(),
            "newStudentEmail" : $('#newStudentEmail').val(),
            "newStudentGroup" : $('#newStudentGroup').val(),
        };

        $.ajax({
            data:  parametros,
            url: "/student/main/insert",
            context: document.body,
            type: 'POST',
            dataType:"json",
            success: function(data) {

                if ( data.ok ) {
                    ABAShowOkMsgList( '<?php echo $module;  ?>', data.okMsg, 1 );

                    $('#newStudentName').val('');
                    $('#newStudentSurname').val('');
                    $('#newStudentEmail').val('');
                    $('#newStudentGroup').val('');
                    $('#newStudentName').focus();

                    ABARefreshList( '<?php echo $module; ?>' );
                } else {
                    ABAShowErrorMsgList( '<?php echo $module;  ?>', data.errorMsg, 1 );
                }

                ABAStopLoading();
            },
            error: function(jqXHR, textStatus, error) {
                alert(jqXHR + ' ' + textStatus + ' ' + error);
                ABAStopLoading();
            },
        }).done(function() {
            //
        });
    });

    ABARefreshList( '<?php echo $module; ?>', 1, { email: '<?php echo CJavaScript::quote($email); ?>', status: '<?php echo StudentGroup::_STATUS_ACTIVE; ?>' });
//    groupLiveSearchTrapOn();


    <?php if( Yii::app()->request->getQuery('idChannel') ) { ?>
    <?php $popup = $this->renderPartial('licenciaprueba', array('nombre' => Utils::getSessionVar('name')), true); ?>

        setTimeout( function() {
            abaCore.showDialog(<?php echo CJavaScript::encode($popup); ?>, true, '660px', '370px');
        }, 1000 );
//        alert('hola');
    <?php } ?>

});


function prepareList()
{
    jQuery('#expList').find('li:has(ul)')
        .addClass('el_expanded');
        //.children('ul').hide();
    jQuery('#expList').find('.expandIcon')
        .click( function(evt) {
            if (this == evt.target) {
                if(jQuery(this).hasClass("el_expanded"))
                {
                    jQuery(this).removeClass('el_expanded');
                    jQuery(this).addClass('el_collapsed');
                }
                else
                {
                    jQuery(this).removeClass('el_collapsed');
                    jQuery(this).addClass('el_expanded');
                }
                jQuery(this).children('ul').toggle('medium');
            }
            // Impedir la propagación de eventos
            if (!evt) var evt = window.event;
            evt.cancelBubble = true; // in IE
            if (evt.stopPropagation) evt.stopPropagation();
        });
};

var keyPressTimeout = null;
var keyPressLongTimeout = null;
var lastGroupLiveSearch = '';
function groupLiveSearchTrapOn() {
    $('#newStudentGroup').on('keydown', function(e) {
        if ( keyPressTimeout ) clearTimeout(keyPressTimeout);

        if ( e.keyCode == 9 ) {
            if ( $('#divGroupLiveSearch').html() != '' ) {

                $('#divGroupLiveSearch li').each( function() {
                    if ( $(this).hasClass('active') ) {
                        $('#newStudentGroup').val( $(this).text() );
                        lastGroupLiveSearch = $(this).text();
                        e.preventDefault();
                        return false;
                    }
                });
                return false;
            }
        }

    });


    $('#newStudentGroup').on('keyup', function(e) {
        var value = $('#newStudentGroup').val();

        if ( keyPressTimeout ) clearTimeout(keyPressTimeout);

        // ** Excepciones.
        if (e.keyCode == 27) {
            hideGroupLiveSearch();
            return false;
        }

        if ( value == '' && ( e.keyCode == 8 || e.keyCode == 46 ) ) {
            hideGroupLiveSearch();
            return false;
        }

        if ( value == '' && ( e.keyCode < 65 || e.keyCode > 90 ) ) {
            hideGroupLiveSearch();
            return false;
        }

        if ( e.keyCode == 13 ) {
            $('#divGroupLiveSearch li').each( function() {
                if ( $(this).hasClass('active') ) {
                    $('#newStudentGroup').val( $(this).text() );
                    return false;
                }
            });
            hideGroupLiveSearch();
            return false;
        }

        if ( e.keyCode == 40 || e.keyCode == 38 ) {
            var isActive = 0;
            var contador = 0;
            $('#divGroupLiveSearch li').each( function() {
                contador++;
                if ( $(this).hasClass('active') ) {
                    $(this).removeClass('active');
                    isActive = contador;
                    return false;
                }
            });

            if ( e.keyCode == 40 ) isActive++;
            else if ( e.keyCode == 38 ) isActive--;

            if ( isActive <= 0 ) {
                hideGroupLiveSearch();
                return false;
            } else {
                var contador = 0;
                $('#divGroupLiveSearch li').each( function() {
                    if ( ++contador == isActive ) {
                        $(this).addClass('active');
                        return false;
                    }
                });
            }

        }

        if ( value == lastGroupLiveSearch ) return false;

        lastGroupLiveSearch = value;
        if ( !keyPressLongTimeout ) keyPressLongTimeout = setTimeout( 'doGroupLiveSearch();', 2000 );

        var time = 250;
//        if ( value.length < 3 ) time = 1000;
        keyPressTimeout = setTimeout( 'doGroupLiveSearch();', time );
    });

}

function doGroupLiveSearch() {
    if ( keyPressTimeout ) { clearTimeout(keyPressTimeout); keyPressTimeout = null; }
    if ( keyPressLongTimeout ) { clearTimeout(keyPressLongTimeout); keyPressLongTimeout = null; }

    var parametros = {
        partialWord : $('#newStudentGroup').val(),
    };

    $.ajax({
        data:  parametros,
        url: "/student/main/groupsearch",
        context: document.body,
        type: 'POST',
        dataType:"json",
        success: function(data) {
            if ( data.ok ) {
                if ( data.result.length > 0 ) showLiveSearchResults(data.result);
                else hideGroupLiveSearch();
            }
        },
        error: function(jqXHR, textStatus, error) {
//          alert('error');
        },
    }).done(function() {
        //
    });

}

function showLiveSearchResults( arResults ) {
    var txt = '';

    $.each( arResults, function( index, value ) {
        txt += '<li>'+value+'</li>';
    });
    txt = '<ul>'+txt+'</ul>';

    $('#divGroupLiveSearch').html( txt );
    $('#divGroupLiveSearch li').click( function() {
        $('#newStudentGroup').val( $(this).text() );
        hideGroupLiveSearch();
    });
    $('#divGroupLiveSearch li').mouseover( function() {
        console.log( "Mouse Log: "+$(this).text() );

        $('#divGroupLiveSearch li').removeClass('active');
        $(this).addClass('active');
    });
    $('#divGroupLiveSearch').show();
}

function hideGroupLiveSearch() {
    if ( keyPressTimeout ) { clearTimeout(keyPressTimeout); keyPressTimeout = null; }
    if ( keyPressLongTimeout ) { clearTimeout(keyPressLongTimeout); keyPressLongTimeout = null; }

    $('#divGroupLiveSearch').hide();
    $('#divGroupLiveSearch').html( '' );
}


</script>

<?php if( Yii::app()->request->getQuery('idChannel') ) { ?>
<!-- Whit this line we include all registration pixels. -->
<div class="hiddenPixel"><?php HeAnalyticsExtranet::registeredUsersPixel(Yii::app()->request->getQuery('idChannel')); ?></div>
<?php } ?>
