<?php

  $selectedIndex = 'Alumno';
  
  $numLicencias = 1;

?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>

<div class="row-fluid">
  <div class="col-lg-12">
    
    <div class="row-fluid">
      <div class="col-lg-12">
          <div class="divSectionTittle">
              <span class="sectionTitle"> <?php echo StudentModule::t( 'Activate licenses'); ?></span>
          </div>
      </div>
    </div>    
    
    <div class="row-fluid">
      <div class="col-lg-12">
        &nbsp;
      </div>
    </div>    
    
    <?php $this->renderPartial('progress', array( 'fase' => 3 ) ); ?>
        
    <div class="row-fluid">
      <div class="col-lg-12" style="margin-top: 40px;">
        
        <div class="row-fluid">
          <div class="col-lg-5" style="float: none; margin: auto;">
            
            <div style="text-align:center; margin-bottom: 20px; font-size: 16px; font-weight: bold;">
              <?php echo CHtml::encode( StudentModule::t( 'ELIGE UN TIPO DE ACCESO AL CURSO') ); ?>
            </div>
            
            <div class="table-responsive"> 

              <table id='tableLicense' class="table table-bordered" style="background-color: #f3f3f3; margin:auto; padding: 40px;">
              <tr>
                <td class="rowLicense active" id="licenseMethod_mselect">
                  <div style="position:relative;">
                    <img src="/images/circledocument.png" style="float: left;">
                    <div style="display: block; font-size: 20px; margin-left: 90px;"><?php echo StudentModule::t( 'Quiero <b>escoger los niveles</b> a los que tendrá acceso el alumno' ); ?></div>
                  </div>
                </td>
              </tr>
              <tr>
                <td class="rowLicense" id="licenseMethod_sselect">
                  <div style="position:relative;">
                    <img src="/images/circlepeople.png" style="float: left;">
                    <div style="display: block; font-size: 20px; margin-left: 90px;"><?php echo StudentModule::t( 'Quiero que <b>el alumno escoja</b> los niveles a los que tendrá acceso' ); ?></div>
                  </div>
                </td>
              </tr>
              <tr>
                <td class="rowLicense" id="licenseMethod_tselect">
                  <div style="position:relative;">
                    <img src="/images/circletest.png" style="float: left;">
                    <div style="display: block; font-size: 20px; margin-left: 90px;"><?php echo StudentModule::t( 'Quiero que <b>al alumno se le asigne</b> un nivel acorde a su resultado del test' ); ?></div>
                  </div>
                </td>
              </tr>
              </table>
              
            </div>

            <div style="text-align:center; margin-top: 60px;">
              
              <button id="btnBackLicense1" type="button" class="abaButton abaButton-Primary" style="float: left;">
                <img src="/images/arrowleft.png" style="margin-right: 10px; margin-top: -4px;"><?php echo CHtml::encode( StudentModule::t( 'BACK') ); ?>
              </button>
              <button id="btnNextLicense1" type="button" class="abaButton abaButton-Primary" style="float: right;">
                <?php echo CHtml::encode( StudentModule::t( 'NEXT') ); ?><img src="/images/arrowright.png" style="margin-left: 10px; margin-top: -4px;">
              </button>
              
            </div>
            
            
          </div>
        </div>
        
      </div>
    </div>    
    
    <div class="row-fluid">
      <div class="col-lg-12" id="divTableList">
        
        <br style="font-size: 40px;">
        
      </div>
    </div>

  </div>
</div>

<script>
  var selectedIndex = '<?php echo CJavaScript::quote($selectedIndex); ?>';
  var checkedList = '<?php echo $checkedList; ?>';
  var selectedLicenseType = '<?php echo $selectedLicenseType; ?>';
  var selectedLicenseMethod = 'mselect';
  
  $(document).ready( function() {
    
    $('#btnBackLicense1').click( function() {
      var pageURL = '/student/license/activate2';
      var parameters = {checkedList: checkedList, selectedLicenseType: selectedLicenseType};
      
      ABAChangePage( pageURL, parameters );
    });
    
    $('#btnNextLicense1').click( function() {
      
      if ( selectedLicenseMethod == 'mselect' ) {
        var pageURL = '/student/license/activate4';
        var parameters = {checkedList: checkedList, selectedLicenseType: selectedLicenseType, selectedLicenseMethod: selectedLicenseMethod};
      } else {
        var pageURL = '/student/license/finalactivate';
        var parameters = {checkedList: checkedList, selectedLicenseType: selectedLicenseType, selectedLicenseMethod: selectedLicenseMethod};
      }
      
      ABAChangePage( pageURL, parameters );
      
    });

    $('.rowLicense').click( function() {
      
      $('.rowLicense').removeClass( 'active' );
      $(this).addClass( 'active' );
      
      selectedLicenseMethod = this.id.replace( 'licenseMethod_', '' );
      
      if ( selectedLicenseMethod == 'mselect' ) {
        $('#btnNextLicense1').html('<?php echo CHtml::encode( StudentModule::t( 'NEXT') ); ?><img src="/images/arrowright.png" style="margin-left: 10px; margin-top: -4px;">');
      } else {
        $('#btnNextLicense1').html('<?php echo CHtml::encode( StudentModule::t( 'SAVE') ); ?>');
      }
      
    });

  });
  
</script>
