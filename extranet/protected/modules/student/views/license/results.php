<table height="100%" width='100%'>
    <tr>
        <td>
            <div style="font-size: 20px; color:green; text-align: center;">
                <img src="/images/tick3.png" width="48" style="margin: 20px;">
            </div>

            <div style="text-align: center;">
                <span style="font-weight: bold; text-align: center;"><?php echo StudentModule::t('Account activation result'); ?></span><br>
            </div>
            <?php
            $totalSucces=0;
            $totalFail=0;
            $listSucces = array();
            $listFail = array();

            foreach($result as $singleResult)
            {
                if($singleResult['error']=='')
                {
                    $listSucces[$totalSucces] =ucfirst(strtolower($singleResult['surnames'])).", ".ucfirst(strtolower($singleResult['name']))." (".  strtolower($singleResult['email']).")";
                    $totalSucces++;

                }
                else
                {
                    $listFail[$totalFail] = ucfirst(strtolower($singleResult['surnames'])).", ".ucfirst(strtolower($singleResult['name']))." (".  strtolower($singleResult['email']).")";
                    $totalFail++;
                }
            }
            ?>
            <div id="listContainer">
                <ul id="expList">
                    <?php
                    if($totalSucces>0)
                    {
                        echo "<li>".StudentModule::t('Successfully accunts activation').": ".$totalSucces;
                        echo "<ul>";
                        foreach($listSucces as $cuenta)
                        {
                            echo "<li>";
                            echo "<span id=\"accountsLabel\">".$cuenta."</span>";
                            echo "</li>";
                        }
                        echo "</ul>";
                        echo "</li>";

                    }
                    if($totalFail>0)
                    {
                        echo "<li>".StudentModule::t('Failed accounts activation').": ".$totalFail;
                        echo "<ul>";
                        foreach($listFail as $cuenta)
                        {
                            echo "<li>";
                            echo "<span id=\"accountsLabel\">".$cuenta."</span>";
                            echo "</li>";
                        }
                        echo "</ul>";
                        echo "</li>";

                    }
                    ?>
                </ul>
            </div>
            <br>

            <div id="divBtnCloseOkLicense">
                <button id="btnCloseOkLicense" class="abaButton abaButton-Primary abaExtranetButton-Ok">
                    <?php echo StudentModule::t('Ok'); ?>
                </button>
            </div>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
</table>



<script>
    function prepareList() {
        $('#expList').find('li:has(ul)')
            .click( function(event) {
                if (this == event.target) {
                    $(this).toggleClass('expanded');
                    $(this).children('ul').toggle('medium');
                }
                return false;
            })
            .addClass('collapsed')
            .children('ul').hide();
    };

    $(document).ready( function() {
        $('#btnCloseOkLicense').click( function() {
            var pageURL = '/student/main';
            var parameters = {};
            ABAChangePage( pageURL, parameters );
        });

        prepareList();
    });
</script>  