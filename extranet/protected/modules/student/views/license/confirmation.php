<?php

  $selectedIndex = 'Alumno';
  $numLicencias = count($mlStudents);
  
?>
<?php /*  $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>

<style>
  #tablePreview {
    border-spacing: 5px;
    border-collapse: separate;
  }
</style>

<div class="row-fluid">
  <div class="col-lg-12">
    
    <div class="row-fluid">
      <div class="col-lg-12">
          <div class="divSectionTittle">
              <span class="sectionTitle"> <?php echo StudentModule::t( 'Activate licenses'); ?></span>
          </div>
      </div>
    </div>    
    
    <div class="row-fluid">
      <div class="col-lg-12">
        &nbsp;
      </div>
    </div>    
    
    <?php $this->renderPartial('progress', array( 'fase' => 4 ) ); ?>
        
    <div class="row-fluid">
      <div class="col-lg-12" style="margin-top: 40px;">
        
        <div class="row-fluid">
          <div class="col-lg-9" style="float: none; margin: auto;">
            
            <div style="text-align:center; margin-bottom: 20px; font-size: 16px; font-weight: bold;">
              <?php echo CHtml::encode( StudentModule::t( 'CONFIRM YOUR SELECTION') ); ?>
            </div>
            
            <div class="table-responsive"> 

              <table id='tableLicense' class="table table-bordered" style="background-color: #f3f3f3; margin:auto; padding: 40px;">
              <tr>
                <td class="rowLicense" id="licenseLevel_all" style="background-color: #F3F3F3;">
                  
                  <table id='tablePreview' width="85%">
                  <tr>
                    <td style="border-bottom: 1px solid #333333; font-weight: bold;"><?php echo StudentModule::t( 'NUMBER'); ?></td>
                  </tr>
                  <tr>
                    <td><?php echo count($mlStudents); ?> <?php if ( count($mlStudents) != 1 ) echo Yii::t('app', 'licenses'); else echo Yii::t('app','License'); ?> </td>
                  </tr>
                  <tr><td>&nbsp;</td></tr>
                  <tr>
                    <td style="border-bottom: 1px solid #333333; font-weight: bold;"><?php echo StudentModule::t( 'TYPE'); ?></td>
                  </tr>
                  <tr>
                    <td><?php echo StudentModule::t($mLicenseType->name) ?></td>
                  </tr>
                  <tr><td>&nbsp;</td></tr>
                  <tr>
                    <td style="border-bottom: 1px solid #333333; font-weight: bold;"><?php echo Yii::t('app', 'CONVOCATION'); ?></td>
                  </tr>
                  <tr>
                    <td><?php echo $convocation; ?></td>
                  </tr>
                  <tr><td>&nbsp;</td></tr>
                  <?php if ( !empty($idTeacher) ) { ?>
                  <tr>
                    <td style="border-bottom: 1px solid #333333; font-weight: bold; text-transform: uppercase;"><?php echo Yii::t('app', 'Teacher'); ?></td>
                  </tr>
                  <tr>
                    <td><?php echo $teacher; ?></td>
                  </tr>
                  <tr><td>&nbsp;</td></tr>
                  <?php } ?>

<!--                  <tr>
                    <td style="border-bottom: 1px solid #333333; font-weight: bold;"><?php echo StudentModule::t( 'ACCESS'); ?></td>
                  </tr>
                  <tr>
                    <td>Annual</td>
                  </tr>
                  <tr><td>&nbsp;</td></tr>-->
                  
                  <tr>
                    <td style="border-bottom: 1px solid #333333; font-weight: bold;"><?php echo StudentModule::t( 'STUDENTS'); ?>*</td>
                  </tr>
                  <?php foreach( $mlStudents as $mStudent ) { ?>
                  <tr>
                    <td>
                        <?php echo $mStudent->name.' '.$mStudent->surname;  ?>
                    </td>
                  </tr>
                  <?php } ?>
                  <tr><td>&nbsp;</td></tr>
                  </table>
                </td>
              </tr>
              </table>
              <span>* <?php echo StudentModule::t( 'This students will receive and email with proper instructions to start the course.'); ?></span><br>
              <span>** <?php echo Yii::app()->getModule('student')->t('Una vez se activen las licencias, si se completa un nuevo test de nivel, el resultado no se verá contemplado en la Plataforma Corporativa ABA English ni en el campus del alumno.'); ?></span>
              
            </div>

            <div style="text-align:center; margin-top: 60px;">
              
              <button id="btnBackLicense1" type="button" class="abaButton abaButton-Primary" style="float: left; min-width: 15em;">
                <img src="/images/arrowleft.png" style="margin-right: 10px; margin-top: -4px;"><?php echo CHtml::encode( StudentModule::t( 'BACK') ); ?>
              </button>
              <button id="btnNextLicense1" type="button" class="abaButton abaButton-Primary" style="float: right; min-width: 15em;">
                <?php echo CHtml::encode( StudentModule::t( 'Activar') ); ?>
              </button>
              
            </div>
            
            
          </div>
        </div>
        
      </div>
    </div>    
    
    <div class="row-fluid">
      <div class="col-lg-12" id="divTableList">
        
        <br style="font-size: 40px;">
        
      </div>
    </div>

  </div>
</div>

<script>
  var selectedIndex = '<?php echo CJavaScript::quote($selectedIndex); ?>';
  var checkedList = '<?php echo $checkedList; ?>';
  var selectedLicenseType = '<?php echo $selectedLicenseType; ?>';
  var convocation = '<?php echo CJavaScript::quote($convocation); ?>';
  var teacher = '<?php echo $idTeacher; ?>';
  var selectedLicenseMethod = '';
  var filters = '<?php echo json_encode($arFilters); ?>';

  $(document).ready( function() {
    
    $('#btnBackLicense1').click( function() {
      var pageURL = '/student/license/activate2';
      var parameters = {checkedList: checkedList, selectedLicenseType: selectedLicenseType, convocation: convocation, teacher: teacher, filters: filters};

      ABAChangePage( pageURL, parameters );
    });
    
    $('#btnNextLicense1').click( function() {
//      var pageURL = '/student/license/finalactivate';
//      var parameters = {checkedList: checkedList, selectedLicenseType: selectedLicenseType, convocation: convocation };
//      
//      ABAChangePage( pageURL, parameters );
  
      var parametros = {
        checkedList: checkedList, 
        selectedLicenseType: selectedLicenseType, 
        convocation: convocation,
        teacher: teacher,
        filters: filters
      };

      $.ajax({
        data:  parametros,
        url: "/student/license/finalactivate",
        context: document.body,
        type: 'POST',
        dataType:"json",
        success: function(data) {
          if ( data.ok ) {
            ABAShowDialog( data.html );
          } else {
            ABAShowDialog( data.html );
          }
        },
        error: function(jqXHR, textStatus, error) {
            alert(error);
            alert(jqXHR);
            alert(textStatus);
        },        
      }).done(function() {
        // 
      });
  
      ABAShowLoading();
    });

    $('.rowLicense').click( function() {
      if ( $(this).hasClass('active') ) $(this).removeClass( 'active' );
      else $(this).addClass( 'active' );
    });

  });
  
</script>
