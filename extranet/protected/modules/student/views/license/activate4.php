<?php

  $selectedIndex = 'Alumno';
  
  $numLicencias = 1;

?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>

<div class="row-fluid">
  <div class="col-lg-12">
    
    <div class="row-fluid">
      <div class="col-lg-12">
          <div class="divSectionTittle">
              <span class="sectionTitle"> <?php echo StudentModule::t( 'Activate licenses'); ?></span>
          </div>
      </div>
    </div>    
    
    <div class="row-fluid">
      <div class="col-lg-12">
        &nbsp;
      </div>
    </div>    
    
    <?php $this->renderPartial('progress', array( 'fase' => 3 ) ); ?>
        
    <div class="row-fluid">
      <div class="col-lg-12" style="margin-top: 40px;">
        
        <div class="row-fluid">
          <div class="col-lg-5" style="float: none; margin: auto;">
            
            <div style="text-align:center; margin-bottom: 20px; font-size: 16px; font-weight: bold;">
              <?php echo CHtml::encode( StudentModule::t( 'ELIGE UN TIPO DE ACCESO AL CURSO') ); ?>
            </div>
            
            <div class="table-responsive"> 

              <table id='tableLicense' class="table table-bordered" style="background-color: #f3f3f3; margin:auto; padding: 40px;">
              <tr>
                <td class="rowLicense" id="licenseLevel_all">
                  <div style="position:relative;">
                    <div style="display: block; font-size: 20px; margin-left: 90px;"><?php echo StudentModule::t( 'Acceso completo al curso' ); ?></div>
                  </div>
                </td>
              </tr>
              
              <?php 
                $arLevels = array();
                $oTmp = new stdClass(); 
                $oTmp->id = 1;
                $oTmp->name = 'Beginners';
                $arLevels[] = $oTmp;
                $oTmp = new stdClass(); 
                $oTmp->id = 1;
                $oTmp->name = 'Lower intermediate';
                $arLevels[] = $oTmp;
                $oTmp = new stdClass(); 
                $oTmp->id = 1;
                $oTmp->name = 'Intermediate';
                $arLevels[] = $oTmp;
                $oTmp = new stdClass(); 
                $oTmp->id = 1;
                $oTmp->name = 'Upper intermediate';
                $arLevels[] = $oTmp;
                $oTmp = new stdClass(); 
                $oTmp->id = 1;
                $oTmp->name = 'Advanced';
                $arLevels[] = $oTmp;
                $oTmp = new stdClass(); 
                $oTmp->id = 1;
                $oTmp->name = 'Business';
              ?>
              <?php foreach( $arLevels as $level ) { ?>
              <tr>
                <td class="rowLicense" id="licenseLevel_<?php echo $level->id; ?>">
                  <div style="position:relative;">
                    <div style="display: block; font-size: 20px; margin-left: 90px;"><?php echo $level->name; ?></div>
                  </div>
                </td>
              </tr>
              <?php } ?>
              
              </table>
              
            </div>

            <div style="text-align:center; margin-top: 60px;">
              
              <button id="btnBackLicense1" type="button" class="abaButton abaButton-Primary" style="float: left;">
                <img src="/images/arrowleft.png" style="margin-right: 10px; margin-top: -4px;"><?php echo CHtml::encode( StudentModule::t( 'BACK') ); ?>
              </button>
              <button id="btnNextLicense1" type="button" class="abaButton abaButton-Primary" style="float: right; text-transform: uopercase; ">
                <?php echo CHtml::encode( StudentModule::t( 'Activar') ); ?><img src="/images/arrowright.png" style="margin-left: 10px; margin-top: -4px;">
              </button>
              
            </div>
            
            
          </div>
        </div>
        
      </div>
    </div>    
    
    <div class="row-fluid">
      <div class="col-lg-12" id="divTableList">
        
        <br style="font-size: 40px;">
        
      </div>
    </div>

  </div>
</div>

<script>
  var selectedIndex = '<?php echo CJavaScript::quote($selectedIndex); ?>';
  var checkedList = '<?php echo $checkedList; ?>';
  var selectedLicenseType = '<?php echo $selectedLicenseType; ?>';
  var selectedLicenseMethod = '';
  
  $(document).ready( function() {
    
    $('#btnBackLicense1').click( function() {
      var pageURL = '/student/license/activate2';
      var parameters = {checkedList: checkedList, selectedLicenseType: selectedLicenseType};
      
      ABAChangePage( pageURL, parameters );
  
    });
    
    $('#btnNextLicense1').click( function() {
      var pageURL = '/student/license/finalactivate';
      var parameters = {checkedList: checkedList, selectedLicenseType: selectedLicenseType};
      
      ABAChangePage( pageURL, parameters );
  
    });

    $('.rowLicense').click( function() {
      if ( $(this).hasClass('active') ) $(this).removeClass( 'active' );
      else $(this).addClass( 'active' );
    });

  });
  
</script>
