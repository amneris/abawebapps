<?php
$color1 = ( $fase == 1 ) ? 'grey' : 'blue';
$color2 = ( $fase <= 2 ) ? 'grey' : 'blue';
$color3 = ( $fase <= 3 ) ? 'grey' : 'blue';

$active1 = ( $fase == 1 ) ? true : true;
$active2 = ( $fase < 2 ) ? false : true;
$active3 = ( $fase < 3 ) ? false : true;



?>

<style>
  .breadcrumb-trail {
      margin: 0px auto;
      display: block;
      box-sizing: border-box;
      width: 560px;
      padding: 30px 11px 0px;
      color: #455560;
      font-family: "MuseoSlab300Regular";
  }

  .breadcrumb-trail .breadcrumbs {
      display: inline-table;
      table-layout: fixed;
      position: relative;
      width: 100%;
      margin: 0px;
      background: url("/images/breadcrumb_icons.png") repeat-x scroll 0px -88px transparent;
  }

  .breadcrumb-trail .breadcrumbs .breadcrumb-cell.active {
      background: url("/images/breadcrumb_icons.png") repeat-x scroll 0px -110px transparent;
  }

  .breadcrumb-trail .breadcrumbs .breadcrumb-cell.lastactive {
      background: url("/images/breadcrumb_icons.png") repeat-x scroll 0px -88px transparent;
  }

  .breadcrumb-trail .breadcrumbs .breadcrumb-cell {
      display: table-cell;
      position: relative;
      width: 25%;
      height: 22px;
      padding: 0px;
      margin: 0px;
  }

  .breadcrumb-trail .breadcrumbs .breadcrumb {
      list-style-type: none;
      position: relative;
      left: -11px;
      width: 22px;
      padding: 0px;
      margin: 0px;
      background: none repeat scroll 0% 0% transparent;
      overflow: visible;
  }

  .breadcrumb {
      padding: 8px 15px;
      margin: 0px 0px 24px;
      list-style: outside none none;
      background-color: #F5F5F5;
      border-radius: 4px;
  }
  
  .breadcrumb-trail .breadcrumbs .breadcrumb-cell .breadcrumb .breadcrumb-icon {
      width: 22px;
      height: 22px;
      background: url("/images/breadcrumb_icons.png") repeat scroll 0px -44px transparent;
  }  

  .breadcrumb-trail .breadcrumbs .breadcrumb-cell.active .breadcrumb .breadcrumb-icon {
      background: url("/images/breadcrumb_icons.png") repeat scroll 0px 0px transparent;
  }

  .breadcrumb-trail .breadcrumbs .breadcrumb-cell.checked .breadcrumb .breadcrumb-icon {
      background: url("/images/breadcrumb_icons.png") repeat scroll 0px 109px transparent;
  }

  .breadcrumb-trail .breadcrumbs .breadcrumb .breadcrumb-label-Visible {
      display: block;
      position: absolute;
      top: -20px;
      left: -59px;
      width: 140px;
      text-align: center;
      white-space: nowrap;
  }

  .breadcrumb-trail .breadcrumbs .breadcrumb .breadcrumb-label-Hidden {
      display: none;
      position: absolute;
      top: -20px;
      left: -59px;
      width: 140px;
      text-align: center;
      white-space: nowrap;
  }
  
  .breadcrumb-trail .breadcrumbs .breadcrumb-cell.last {
      width: 0px;
  }

</style>

<div class="row-fluid">
    <div class="col-lg-10" style="float: none; margin: auto;">

      <div class="breadcrumb-trail">
        <div class="breadcrumbs">
          <div class="breadcrumb-cell <?php if ($active1 && !$active2) echo 'active'; elseif($active2) echo 'checked'; ?>">
            <div class="breadcrumb checked" data-id="type">
              <div class="breadcrumb-icon"></div>
              <span class="breadcrumb-label<?php if ($active1 && !$active2) echo '-Visible'; else echo '-Hidden'; ?>"><?php echo Yii::t('app','License Type'); ?></span>
            </div>
          </div>
          <div class="breadcrumb-cell <?php if ($active2 && !$active3) echo 'active'; elseif ($active3) echo 'checked'; ?>">
            <div class="breadcrumb checked" data-id="period">
              <div class="breadcrumb-icon"></div>
              <span class="breadcrumb-label<?php if ($active2 && !$active3) echo '-Visible'; else echo '-Hidden'; ?>"><?php echo Yii::t('app','Convocation'); ?></span>
            </div>
          </div>
          <div class="breadcrumb-cell <?php if ($active3) echo 'active'; ?> last">
            <div class="breadcrumb" data-id="confirmation">
              <div class="breadcrumb-icon"></div>
              <span class="breadcrumb-label<?php if ($active3) echo '-Visible'; else echo '-Hidden'; ?>"><?php echo Yii::t('app','Confirmation'); ?></span>
            </div>
          </div>
        </div>
      </div>

    </div>
</div>
<div class="row-fluid" style="clear:both;">
    <div class="col-lg-12">&nbsp;</div>
</div>

<script>
  $('.breadcrumb-trail .breadcrumbs .breadcrumb-cell.active').last().addClass('lastactive')
</script>