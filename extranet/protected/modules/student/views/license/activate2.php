<?php

  $selectedIndex = 'Alumno';
  
  $numLicencias = 1;

?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>

<style>
  .spanLicenseConvocatoriaTitle {
    line-height: 2em;
    font-size: 1.5em;
    text-transform: uppercase;
  }
  .spanLicenseConvocatoriaSubtitle {
    line-height: 2em;
    font-size: 1.3em;
  }
</style>

<div class="row-fluid">
  <div class="col-lg-12">
    
    <div class="row-fluid">
      <div class="col-lg-12">
          <div class="divSectionTittle">
              <span class="sectionTitle"> <?php echo StudentModule::t( 'Activate licenses'); ?></span>
          </div>
      </div>
    </div>    
    
    <div class="row-fluid">
      <div class="col-lg-12">
        &nbsp;
      </div>
    </div>    
    
    <?php $this->renderPartial('progress', array( 'fase' => 2 ) ); ?>
        
    <div class="row-fluid">
      <div class="col-lg-12" style="margin-top: 40px;">

        <div class="row-fluid">
          <div class="col-lg-9" style="float: none; margin: auto; text-align: center;">
            <span class="spanLicenseConvocatoriaTitle"><?php echo StudentModule::t('Escoge una convocatoria'); ?> *</span>
          </div>
        </div>
        
        <div class="row-fluid">
          <div class="col-lg-9" style="float: none; margin: auto; text-align: center;">
            <span class="spanLicenseConvocatoriaSubtitle"><?php echo StudentModule::t( 'Agrupa sus alumnos por convocatorias para extraer informes más concretos.'); ?></span>
          </div>
        </div>
        
        <div class="row-fluid">
          <div class="col-lg-9" style="float: none; margin: auto; text-align: center;">
            &nbsp;
          </div>
        </div>
        
        <div class="row-fluid">
          <div class="col-lg-5" style="float: none; margin: auto; text-align: center;">

            <div class="btn-group" role="group" aria-label="...">
              <div class="btn-group" role="group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <span id="idSelectedPeriod" style="padding-left: 4em; padding-right: 4em;"><?php echo $selectedPeriod; ?></span>
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                  <?php $count = 0; ?>
                  <?php foreach( $arPeriod as $idPeriod => $labelPeriod ) { ?>
                    <?php if ( empty($labelPeriod) ) continue; ?>
                    <?php if ( $count++ > 0 ) { ?>
                      <li class="divider"></li>
                    <?php } ?>
                    <li class="optionPeriod"><a href="#" id="liPeriodId_<?php echo $idPeriod; ?>"><?php echo $labelPeriod; ?></a></li>
                  <?php } ?>
                </ul>
              </div>
            </div>

          </div>
        </div>

        <?php if ( !empty($mlTeachers) ) { ?>
        <div class="row-fluid">
          <div class="col-lg-5" style="float: none; margin: auto; text-align: center;">
            &nbsp;
          </div>
        </div>

        <div class="row-fluid">
          <div class="col-lg-9" style="float: none; margin: auto; text-align: center;">
            <span class="spanLicenseConvocatoriaSubtitle"><?php echo Yii::app()->getModule('student')->t( 'Selecciona el profesor que se le asignara a los alumnos.'); ?></span>
          </div>
        </div>

        <div class="row-fluid">
          <div class="col-lg-5" style="float: none; margin: auto; text-align: center;">
            &nbsp;
          </div>
        </div>
        <div class="row-fluid">
          <div class="col-lg-5" style="float: none; margin: auto; text-align: center;">

            <div class="btn-group" role="group" aria-label="...">
              <div class="btn-group" role="group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <span id="idSelectedTeacher" style="padding-left: 4em; padding-right: 4em;"><?php echo $selectedTeacher; ?></span>
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                  <li class="optionTeacher"><a href="#" id="liTeacherId_0" data-id="0"><?php echo Yii::app()->getModule('student')->t('Default Teacher'); ?></a></li>
                  <li class="divider"></li>
                  <?php $count = 0; ?>
                  <?php foreach( $mlTeachers as $mTeacher ) { ?>
                    <?php if ( $count++ > 0 ) { ?>
                      <li class="divider"></li>
                    <?php } ?>
                    <li class="optionTeacher"><a href="#" id="liTeacherId_<?php echo $mTeacher['id']; ?>" data-id="<?php echo $mTeacher['id']; ?>"><?php echo $mTeacher['name'].' '.$mTeacher['surname']; ?></a></li>
                  <?php } ?>
                </ul>
              </div>
            </div>

          </div>
        </div>
        <?php } ?>


        <div class="row-fluid">
          <div class="col-lg-8" style="float: none; margin: auto; text-align: center;">

            <div style="text-align:left; margin-top: 60px;">
              
              <button id="btnBackLicense1" type="button" class="abaButton abaButton-Primary" style="float: left; min-width: 15em;">
                <img src="/images/arrowleft.png" style="margin-right: 10px; margin-top: -4px;"><?php echo CHtml::encode( StudentModule::t( 'BACK') ); ?>
              </button>
              <button id="btnNextLicense1" type="button" class="abaButton abaButton-Primary" style="float: right; min-width: 15em;">
                <?php echo CHtml::encode( StudentModule::t( 'NEXT') ); ?><img src="/images/arrowright.png" style="margin-left: 10px; margin-top: -4px;">
              </button>
              
              <div style="clear:both;"><br></div>
              <span style='font-size: 1em; font-family: "MuseoSans500Regular",Arial,sans-serif;'>* <?php echo StudentModule::t('Recomendamos usar la convocatoria del mes anterior en el caso de tener alumnos pendientes por agrupar en una convocatoria anterior'); ?></span>
            </div>
            
            
          </div>
        </div>

        <div class="row-fluid">
          <div class="col-lg-10" id="divTableList" style="float: none; margin: auto; text-align: center;">


          </div>
        </div>
        
      </div>
    </div>    

    <div class="row-fluid">
      <div class="col-lg-12" id="divTableList">
        
        <br style="font-size: 40px;">
        
      </div>
    </div>

  </div>
</div>

<script>
  var selectedIndex = '<?php echo CJavaScript::quote($selectedIndex); ?>';
  var checkedList = '<?php echo $checkedList; ?>';
  var selectedLicenseType = '<?php echo $selectedLicenseType; ?>';
  var filters = '<?php echo json_encode($arFilters); ?>';
  var selectedTeacher = 0;
  
  $(document).ready( function() {

    $('#idSelectedPeriod').click( function() {
      $('#idSelectedPeriod').toggleClass( "open" );
    });

    $('#idSelectedTeacher').click( function() {
      $('#idSelectedTeacher').toggleClass( "open" );
    });

      $('#btnBackLicense1').click( function() {
      var pageURL = '/student/license';
      var parameters = {checkedList: checkedList, selectedLicenseType: selectedLicenseType, filters: filters};
      
      ABAChangePage( pageURL, parameters );
    });
    
    $('#btnNextLicense1').click( function() {
      var pageURL = '/student/license/confirmation';
      var parameters = {checkedList: checkedList, selectedLicenseType: selectedLicenseType, filters: filters, convocation: $('#idSelectedPeriod').html(), teacher: selectedTeacher };
      
      ABAChangePage( pageURL, parameters );
  
    });

    $('.optionPeriod a').click( function() {
      $('#idSelectedPeriod').html( $(this).html() );
    });

    $('.optionTeacher a').click( function() {
      selectedTeacher = $(this).data('id');
      $('#idSelectedTeacher').html( $(this).html() );
    });

  });
  
</script>
