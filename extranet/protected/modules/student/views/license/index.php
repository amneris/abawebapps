<?php

$selectedIndex = 'Alumno';
$numLicencias = $mlLicenseAvailable['Total']->numLicenses;
unset($mlLicenseAvailable['Total']);
$numLicencias = 1;
$lastOption = 0;
//$selectedLicenseType = 0;

?>
<?php /* $this->renderPartial('//extranet/menu', array('selectedIndex' => $selectedIndex) ); */ ?>

<div class="row-fluid">
    <div class="col-lg-12">

        <div class="row-fluid">
            <div class="col-lg-6">
                <div class="divSectionTittle">
                  <span class="sectionTitle"> <?php echo StudentModule::t( 'Activate licenses'); ?></span>
                </div>
            </div>

            <div class="col-lg-6 alignRight">
                <div class="divLicensesPannel">
                    <div class="divTitleAvailableLicenses"><span><?php echo CHtml::encode(StudentModule::t('Available licenses')); ?>: <?php echo $numLicencias; ?></span></div>

                    <?php foreach ($mlLicenseAvailable as $mLicenseAvailable) { ?>
                        <?php /* if ($mLicenseAvailable->id == LicenseType::_Trial) continue; */ ?>
                        <div class="licenseAvailable"><span><?php echo StudentModule::t($mLicenseAvailable->name); ?>: <?php echo $mLicenseAvailable->numLicenses; ?></span></div>
                    <?php } ?>

                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="col-lg-12">
                &nbsp;
            </div>
        </div>


        <?php $this->renderPartial('progress', array( 'fase' => 1 ) ); ?>


        <div class="row-fluid">
            <div class="col-lg-12" style="margin-top: 40px;">

                <div class="row-fluid">
                    <div class="col-lg-8" style="float: none; margin: auto;">

                        <div style="text-align:center; margin-bottom: 20px; font-size: 16px; font-weight: bold; text-transform: uppercase;">
                            <?php echo CHtml::encode( StudentModule::t( 'Choose licence type') ); ?>
                        </div>

                        <div class="table-responsive">

                            <table id='tableLicense' class="table table-bordered" style="background-color: #f3f3f3; margin:auto; padding: 40px;">
                                <tr>
                                    <td class="rowLicenseError">
                                        <img src="/images/circlealert.png"> <?php echo CHtml::encode( StudentModule::t( 'You didn\'t made any selection.' ) ); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rowLicenseError" <?php if ( $errorNum == 1 ) { echo "style='display:table-cell;'"; } ?>>
                                        <img src="/images/circlealert.png"> <?php echo CHtml::encode( StudentModule::t( 'You don\'t have enough licenses.' ) ); ?>
                                    </td>
                                </tr>

                               <?php $activeOption = false; ?>
                                <?php foreach( $mlLicenseType as $mLicenseType ) { ?>
                                    <?php if ( $mLicenseType->id == 0 ) continue; ?>
                                    <?php if ($selectedLicenseType == $mLicenseType->id) $activeOption = true; else $lastOption = (empty($lastOption)) ? $mLicenseType->id : $lastOption; ?>
                                    <tr class="cursorPointer">
                                        <td class="rowLicense <?php if ($selectedLicenseType == $mLicenseType->id) echo "active"; ?>" id="licenseType_<?php echo $mLicenseType->id; ?>">
                                            <div style="position:relative;">
                                                <div class="circle blue product">
                                                    <p style="margin-top: 20px;"><?php echo (int)($mLicenseType->idPeriodPay/30); ?><br>
                                                        <?php echo CHtml::encode( StudentModule::t( 'Month/s') ); ?></p>
                                                </div>
                                                <div style="display: inline; font-size: 20px; margin-left: 20px;">
                                                    <?php
                                                    $string = StudentModule::t('I want to use {$word} license type');
                                                    $word = StudentModule::t($mLicenseType->name);
                                                    eval("\$string = \"$string\";");
                                                    echo $string;
                                                    ?>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="rowLicenseCheck <?php if ($selectedLicenseType == $mLicenseType->id) echo "active"; ?>"><span class="abaCheck abaExtranetLicenseCheck"></span></td>
                                    </tr>
                                <?php } ?>

                            </table>

                        </div>

                        <div style="text-align:center; margin-top: 60px;">

                            <button id="btnBackLicense1" type="button" class="abaButton abaButton-Primary" style="float: left; min-width: 15em;">
                                <?php echo CHtml::encode( StudentModule::t( 'Volver') ); ?>
                            </button>
                            <button id="btnNextLicense1" type="button" class="abaButton abaButton-Primary" style="float: right; min-width: 15em;">
                                <?php echo CHtml::encode( StudentModule::t( 'NEXT') ); ?><img src="/images/arrowright.png" style="margin-left: 10px; margin-top: -4px;">
                            </button>

                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row-fluid">
            <div class="col-lg-12" id="divTableList">

                <br style="font-size: 40px;">

            </div>
        </div>

    </div>
</div>

<script>

  var selectedIndex = '<?php echo CJavaScript::quote($selectedIndex); ?>';
  var checkedList = '<?php echo $checkedList; ?>';
  var selectedLicenseType = '<?php echo $selectedLicenseType; ?>';
  var filters = '<?php echo json_encode($arFilters); ?>';

    $(document).ready( function() {

        $('#btnBackLicense1').click( function() {
            var pageURL = '/student/main';
            var parameters = {checkedList: checkedList};

            ABAChangePage( pageURL, parameters );
        });

        $('#btnNextLicense1').click( function() {
            var pageURL = '/student/license/activate2';
            var parameters = {checkedList: checkedList, selectedLicenseType: selectedLicenseType, filters: filters};

            ABAChangePage( pageURL, parameters );

        });

        $('.rowLicense').click( function() {
            $('.rowLicense').removeClass( 'active' );
            $('.rowLicenseCheck').removeClass( 'active' );
            $(this).parent().find('td').addClass( 'active' );

            var id = $(this).parent().find('td:first').attr('id');
            selectedLicenseType = id.replace( 'licenseType_', '' );
        });

        $('.rowLicenseCheck').click( function() {
            $('.rowLicense').removeClass( 'active' );
            $('.rowLicenseCheck').removeClass( 'active' );
            $(this).parent().find('td').addClass( 'active' );

            var id = $(this).parent().find('td:first').attr('id');
            selectedLicenseType = id.replace( 'licenseType_', '' );
        });

        <?php if ( !$activeOption ) { ?>
          $('#licenseType_<?php echo $lastOption; ?>').parent().find('td').addClass( 'active' );
          selectedLicenseType = <?php echo $lastOption; ?>
        <?php } ?>
    });

</script>

