<?php

class LicenseController extends Controller
{
    public $defaultAction = 'index';

    public function init() {
        parent::init();

        $this->layout = 'extranet';
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
              'actions'=>array('index','activate2','activate3','activate4','confirmation','finalactivate',),
              'roles'=>array(Role::_ABAMANAGER, Role::_AGENT, Role::_RESPONSIBLE),
              
//              'deniedCallback'=>'redirectToDeniedMethod',
            ),

            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
    
    /**
     *
     */
    public function actionIndex() {
        $errorNum = Yii::app()->request->getParam('errorNum', '0');
        $checkedList = Utils::adminCheckedList( );
        $selectedLicenseType = Yii::app()->request->getParam('selectedLicenseType', 0);
        $arFilters = Utils::getActualFilters();
        $idEnterprise = Utils::getSessionVar('idEnterprise', 0);
        $mlLicenseAvailable = ModuleStudentModel::availableLicences($idEnterprise);
        $numSelectedStudents = ModuleStudentModel::getStudentList( array( 'checkedList' => $checkedList, 'idEnterprise' => $idEnterprise, 'filters' => $arFilters, 'onlySelected' => true, 'onlyCount' => true ) );

        $mlLicenseType = ModuleStudentModel::availableLicences($idEnterprise, '', $numSelectedStudents);

        $this->render('index', array(
            'checkedList' => $checkedList,
            'arFilters' => $arFilters,
            'selectedLicenseType' => $selectedLicenseType,
            'mlLicenseType' => $mlLicenseType,
            'errorNum' => $errorNum,
            'mlLicenseAvailable'=>$mlLicenseAvailable,
        ));
    }

    /**
     *
     */
    public function actionActivate2() {
        $checkedList = Utils::adminCheckedList();
        $selectedLicenseType = Yii::app()->request->getParam('selectedLicenseType', 3);

        $idEnterprise = Utils::getSessionVar('idEnterprise', 0);
        $mlLicenseAvailable = ModuleStudentModel::availableLicences($idEnterprise, $selectedLicenseType);
        $arFilters = Utils::getActualFilters();
        $numSelectedStudents = ModuleStudentModel::getStudentList( array( 'checkedList' => $checkedList, 'idEnterprise' => $idEnterprise, 'filters' => $arFilters, 'onlySelected' => true, 'onlyCount' => true ) );
        if ( !isset($mlLicenseAvailable[$selectedLicenseType]) || (!empty($mlLicenseAvailable) && $mlLicenseAvailable[$selectedLicenseType]->numLicenses < $numSelectedStudents ) ) {
            Yii::app()->controller->redirect( array( '/student/license', 'errorNum' => 1, 'selectedLicenseType' => $selectedLicenseType, 'checkedList' => $checkedList ) );
        }

        $criteria = new CDbCriteria();
        $criteria->addCondition( 't.idEnterprise = :idEnterprise' );
        $criteria->addCondition( 't.isdeleted = :isdeleted' );
        $criteria->params = array( ':idEnterprise' => $idEnterprise, ':isdeleted' => 0 );
        $criteria->order = "created desc";
        $mlPeriod = Period::model()->findAll($criteria);

        $encontrado = false;
        $actual = date("Y / m");
        $arPeriod = array( );
        foreach( $mlPeriod as $mPeriod ) {
            if ( $mPeriod->name == $actual ) $encontrado = true;
            $arPeriod[$mPeriod->id] = $mPeriod->name;
        }
        if ( !$encontrado ) { $arPeriod[0] = $actual; ksort($arPeriod); }
        $selectedPeriod = date("Y / m");

        $mlTeachers = User::getTeachers( $idEnterprise );
        $selectedTeacher = Yii::app()->getModule('student')->t('Default Teacher');

        $this->render('activate2', array(
            'checkedList' => $checkedList,
            'arFilters' => $arFilters,
            'selectedLicenseType' => $selectedLicenseType,
            'arPeriod' => $arPeriod,
            'selectedPeriod' => $selectedPeriod,
            'mlTeachers' => $mlTeachers,
            'selectedTeacher' => $selectedTeacher,
        ));
    }

    /**
     *
     */
    public function actionActivate3() {
        $checkedList = Yii::app()->request->getParam('checkedList', '0');
        $checkedList = Utils::adminCheckedList( $checkedList );
        $selectedLicenseType = Yii::app()->request->getParam('selectedLicenseType', 'semiannual');

        $this->render('activate3', array(
            'checkedList' => $checkedList,
            'selectedLicenseType' => $selectedLicenseType,
        ) );
    }

    /**
     *
     */
    public function actionActivate4() {
        $checkedList = Yii::app()->request->getParam('checkedList', '0');
        $checkedList = Utils::adminCheckedList( $checkedList );
        $selectedLicenseType = Yii::app()->request->getParam('selectedLicenseType', 'semiannual');
        $selectedLicenseMethod = Yii::app()->request->getParam('selectedLicenseMethod', '');

        $this->render('activate4', array(
            'checkedList' => $checkedList,
            'selectedLicenseType' => $selectedLicenseType,
            'selectedLicenseMethod' => $selectedLicenseMethod,
        ) );
    }

    /**
     *
     */
    public function actionConfirmation() {
        $checkedList = Utils::adminCheckedList();
        $selectedLicenseType = Yii::app()->request->getParam('selectedLicenseType', 'semiannual');
        $selectedLicenseMethod = Yii::app()->request->getParam('selectedLicenseMethod', '');
        $convocation = Yii::app()->request->getParam('convocation', '');
        $idTeacher = Yii::app()->request->getParam('teacher', '');
        $idEnterprise = Utils::getSessionVar('idEnterprise');
        $arFilters    = Utils::getActualFilters();

        $arDatos = ModuleStudentModel::getStudentList( array(
          'limit' => 0,
          'orderBy' => '',
          'checkedList' => $checkedList,
          'idEnterprise' => $idEnterprise,
          'filters' => $arFilters,
          'onlySelected' => true,
        ));
        $mlStudents = $arDatos['mlStudents'];

        $criteria = new CDbCriteria();
        $criteria->addCondition( 't.id = :idLicenseType' );
        $criteria->params = array( ':idLicenseType' => $selectedLicenseType );
        $mLicenseType = LicenseType::model()->find($criteria);

        $mTeacher = User::model()->findByPk($idTeacher);
        if ( $mTeacher ) $teacher = $mTeacher->name . ' ' . $mTeacher->surname; else $teacher = '';

        $this->render('confirmation', array(
            'checkedList' => $checkedList,
            'selectedLicenseType' => $selectedLicenseType,
            'selectedLicenseMethod' => $selectedLicenseMethod,
            'mlStudents' => $mlStudents,
            'convocation' => $convocation,
            'mLicenseType' => $mLicenseType,
            'teacher' => $teacher,
            'idTeacher' => $idTeacher,
            'arFilters' => $arFilters,
        ) );
    }

    private function activarLicenciaExtranet($idEnterprise, $mlStudents, $row, $mLicenseType, $mPeriod, $mLicenseAvailable, $idTeacher = 0) {
        $allOk = false;
        $mail = $row['email'];

        $idLicenseType = $mLicenseType->id;

        $transaction = Yii::app()->dbExtranet->beginTransaction();
        try {

            if ( isset($row['response']['result']) && $row['response']['result'] == 'success' ) {

                // ** Buscamos la correspondencia entre nuestro listado de Estudiantes.
                foreach( $mlStudents as $mStudent ) {
                    if ( $mail == $mStudent->email ) {
                        // ** Solo necesito actualizar si es nuevo.
                        if ( (isset($row['response']['result']) && $row['response']['result'] == 'success') ) {
                            $mStudent->idStudentCampus = $row['response']['userId'];
                            $mStudent->idTeacher = $idTeacher;
                            $allOk = $mStudent->save();
                        } else $allOk = true;
                        break;
                    }
                }
                if ( !$allOk ) throw new Exception('');

                // ** Se asigna la licencia
                $mLicenseAssigned = new LicenseAsigned;
                $mLicenseAssigned->idEnterprise = $idEnterprise;
                $mLicenseAssigned->idLicenseType = $idLicenseType;
                $mLicenseAssigned->idPeriod = $mPeriod->id;
                $mLicenseAssigned->idStudent = $mStudent->id;
                $allOk = $allOk && $mLicenseAssigned->save();
                if ( !$allOk ) throw new Exception('');

                // ** Finalizar todas las otras convocatorias activas en estos momentos.
                $extratime = 0;
                $criteria = new CDbCriteria();
                $criteria->addCondition( 't.idStudent = :idStudent' );
                $criteria->addCondition( 't.expirationDate > now()' );
                $criteria->params = array( ':idStudent' => $mStudent->id );
                $mlStudentPeriod = StudentPeriod::model()->findAll($criteria);
                foreach( $mlStudentPeriod as $mStudentPeriod ) {
                    if ( $mStudentPeriod->idPeriod == $mPeriod->id ) continue;  // ** Solo si son de otro periodo.
                    $extratime += strtotime($mStudentPeriod->expirationDate) - time();
                    $mStudentPeriod->expirationDate = date("Y-m-d H:i:s");
                    $allOk = $allOk && $mStudentPeriod->save();
                }
                $extratime = (int) ($extratime/86400);  // ** Tiempo en dias.

                // ** Lo asignamos a su convocatoria.
                $criteria = new CDbCriteria();
                $criteria->addCondition( 't.idPeriod = :idPeriod' );
                $criteria->addCondition( 't.idStudent = :idStudent' );
                $criteria->addCondition( 't.expirationDate > now()' );
                $criteria->params = array( ':idPeriod' => $mPeriod->id, ':idStudent' => $mStudent->id );
                $mStudentPeriod = StudentPeriod::model()->find($criteria);
                if ( !$mStudentPeriod ) {
                    $mStudentPeriod = new StudentPeriod;
                    $mStudentPeriod->idPeriod = $mPeriod->id;
                    $mStudentPeriod->idStudent = $mStudent->id;
                    $mStudentPeriod->startDate = date('Y-m-d H:i:s');
                    $mStudentPeriod->expirationDate = Utils::newExpirationDate( 'add', Utils::_ZeroExpirationDate, $mLicenseType->idPeriodPay );
                    if ( $extratime ) $mStudentPeriod->expirationDate = Utils::newExpirationDate( 'add', $mStudentPeriod->expirationDate, $extratime );
                    $allOk = $allOk && $mStudentPeriod->save();
                } else {
                    $mStudentPeriod->expirationDate = Utils::newExpirationDate( 'add', $mStudentPeriod->expirationDate, $mLicenseType->idPeriodPay );
                    $allOk = $allOk && $mStudentPeriod->save();
                }
                if ( !$allOk ) throw new Exception('');

                // ** Actualizamos los datos de la relacion del usuario con la empresa.
                $criteria = new CDbCriteria();
                $criteria->addCondition('t.idEnterprise = :idEnterprise');
                $criteria->addCondition('t.idStudent = :idStudent');
                $criteria->addCondition('t.isdeleted = :deleted');
                $criteria->params = array(':idStudent' => $mStudent->id, ':idEnterprise' => $idEnterprise, ':deleted' => 0);
                $mStudentGroup = StudentGroup::model()->find($criteria);
                if ( $mStudentGroup ) {
                    $mStudentGroup->idPeriod = $mPeriod->id;

                    if ( strtotime($mStudentGroup->expirationDate) > time() ) $mStudentGroup->expirationDate = Utils::newExpirationDate( 'add', $mStudentGroup->expirationDate, $mLicenseType->idPeriodPay );
                    else $mStudentGroup->expirationDate = Utils::newExpirationDate( 'add', Utils::_ZeroExpirationDate, $mLicenseType->idPeriodPay );
                } else throw new Exception('');
                $allOk = $mStudentGroup->save();
                if ( !$allOk ) throw new Exception('');

                  // ** Antiguos valores del alumno en B2C
                $mLogActivacion = new LogActivacion();
                $mLogActivacion->idStudent = $mStudent->id;
                $mLogActivacion->oldTeacherEmail = $row['response']['oldTeacherEmail'];
                $mLogActivacion->oldIdPartnerCurrent = $row['response']['oldIdPartnerCurrent'];
                $allOk = $mLogActivacion->save();
                if ( !$allOk ) throw new Exception('');

                try {
                    if ( $mStudent->idLevel > 0 ) {
                        $mStudentCampus = UserCampus::model()->findByPk($mStudent->idStudentCampus);
                        $mStudentCampus->firstLoginDone = 1;
                        $allOk = $mStudentCampus->save();
                    }
                } catch(Exception $e) {
                    throw new Exception('');
                }

//                if ( $allOk && $mLicenseAvailable->numLicenses > 0 ) $mLicenseAvailable->numLicenses--;

            } else {
                $allOk = false;
                $arUsers[$mail]['error'] = !$allOk;
            }

            $allOk = $allOk && $mLicenseAvailable->save();
            if ( !$allOk ) throw new Exception('');

            $transaction->commit();
        } catch (Exception $e) {
            $allOk = false;
            $transaction->rollBack();
        }

        return $allOk;
    }

    /**
     *
     */
    public function actionFinalactivate() {
        set_time_limit(600);
        $allOk = false;

        $checkedList = Yii::app()->request->getParam('checkedList', '0');
        $checkedList = Utils::adminCheckedList( $checkedList );
        $selectedLicenseType = Yii::app()->request->getParam('selectedLicenseType', 'semiannual');
        $selectedLicenseMethod = Yii::app()->request->getParam('selectedLicenseMethod', '');
        $convocation = Yii::app()->request->getParam('convocation', '');
        $convocation = trim($convocation);
        $idTeacher = Yii::app()->request->getParam('teacher', 0);
        $arFilters    = Utils::getActualFilters();

        // ** Datos empresa simples.
        $isoLanguageEnterprise = Utils::getSessionVar('isoLanguageEnterprise');
        $idCountry = Utils::getSessionVar('idCountry');
        $idPartner = Utils::getSessionVar('idPartner');
        $idEnterprise = Utils::getSessionVar('idEnterprise');

        // ** Estudiantes seleccionados.
        $arDatos = ModuleStudentModel::getStudentList( array(
          'limit' => 0,
          'orderBy' => '',
          'checkedList' => $checkedList,
          'idEnterprise' => $idEnterprise,
          'filters' => $arFilters,
          'onlySelected' => true,
        ));
        $mlStudents = $arDatos['mlStudents'];

        // ** Convocatoria
        $criteria = new CDbCriteria();
        $criteria->addCondition( 't.name = :name' );
        $criteria->addCondition( 't.idEnterprise = :idEnterprise' );
        $criteria->params = array(':idEnterprise' => $idEnterprise, ':name' => $convocation );
        $mPeriod = Period::model()->find($criteria);
        if ( !$mPeriod ) {
            $mPeriod = new Period;
            $mPeriod->idEnterprise = $idEnterprise;
            $mPeriod->name = trim($convocation);
            $allOk = $mPeriod->save();
        }

        // ** Datos licencia.
        $criteria = new CDbCriteria();
        $criteria->addCondition( 't.id = :idLicenseType' );
        $criteria->params = array( ':idLicenseType' => $selectedLicenseType );
        $mLicenseType = LicenseType::model()->find($criteria);
        $idPeriod = $mLicenseType->idPeriodPay;
        $idLicenseType = $mLicenseType->id;

        // ** Teacher
        $mailTeacher = '';
        if ( !empty($idTeacher) ) {
          $mTeacher = User::model()->findByPk($idTeacher);
          if ( $mTeacher ) $mailTeacher = $mTeacher->email;
        }

        // ** Pendents de definir.
        $password = HeString::generateNewPassword();
//        $mailTeacher = Utils::getSessionVar('mailTeacher', 'eabuel@abaenglish.com');
        $codeDeal = 'EXTRANET'.$idPeriod;

          // ** Datos de los alumnos.
        $arUsers = array();
        foreach( $mlStudents as $k=>$mStudent ) {
            $startLevel = ( $mStudent->idLevel > 0 ) ? $mStudent->idLevel : 1;

            $arStudent = array(
              'email' => $mStudent->email,
              'pwd' => $password,
              'langEnv' => $isoLanguageEnterprise,
              'name' => $mStudent->name,
              'surnames' => $mStudent->surname,
              'currentLevel' => $startLevel,
              'teacherEmail' => $mailTeacher,
              'codeDeal' => $codeDeal,
              'partnerId' => $idPartner,
              'periodId' => (int)($idPeriod / 30),
              'idCountry' => $idCountry,
              'device' => 'd',
              'error' => true,
            );

            $mStudent = Student::model()->findByPk( $mStudent->id );
            $mlStudents[$k] = $mStudent;

            $arUsers[$mStudent->email] = $arStudent;
        }

          // ** Activamos las licencias.
        $arResponseTotal = array();
        foreach( $arUsers as $mail=>$arStudent ) {
            // ** Licencias disponibles.
            // ** Tengo que consultarlo cada vez, pues ahora hay mas de un registro de estos y podriamos no tener suficientes licencias en el primero, pero si en los siguientes.
            $criteria = new CDbCriteria();
            $criteria->addCondition('t.idLicenseType = :idLicenseType');
            $criteria->addCondition('t.idEnterprise = :idEnterprise');
            $criteria->addCondition('t.numLicenses > 0');
            $criteria->params = array(':idEnterprise' => $idEnterprise, ':idLicenseType' => $idLicenseType);
            $criteria->order = 'discount desc';
            $mLicenseAvailable = LicenseAvailable::model()->find($criteria);
            if (!$mLicenseAvailable) continue;    // ** Bug, consumir mes llicencies de les que hi ha.

              // ** Calculamos el descuento.
            $codeDeal = $mLicenseAvailable->discount;
              // ** Descuento del 100% para las licencias de prueba.
            if ( $selectedLicenseType == LicenseType::_Trial ) $codeDeal = '100';
            if ( $idEnterprise == 34 ) $codeDeal = '100';                               // ** Empresa de test.

            $arStudent['codeDeal'] = $codeDeal;
            $arUsers_tmp = array( $mail => $arStudent );

            $arResponse = $this->activateCampusLicenses( $arUsers_tmp );
            $arResponseTotal[$mail] = $arResponse;
            foreach( $arResponse as $row ) {
                if ( isset($row['email']) ) {
                    $mail = $row['email'];
                    $allOk = $this->activarLicenciaExtranet($idEnterprise, $mlStudents, $row, $mLicenseType, $mPeriod, $mLicenseAvailable, $idTeacher);
                    $arUsers[$mail]['error'] = !$allOk;
                      // ** Descontamos la licencia consumida.
                    if ($allOk) $mLicenseAvailable->numLicenses--;
                    if ($mLicenseAvailable->numLicenses < 0) $mLicenseAvailable->numLicenses = 0;
                    $mLicenseAvailable->save();
                }
            }
        }

        $html = $this->renderPartial(
            'results',
            array(
                'result' => $arUsers,
            ),
            true
        );

        echo json_encode( array( 'ok' => $allOk, 'html' => $html, 'response' => var_export($arResponseTotal, true), ) );
    }

    /**
     *
     */
    private function  activateCampusLicenses($aUsers) {
        ini_set("soap.wsdl_cache_enabled", 0);
        ini_set("default_socket_timeout", 600);
        $sUsers = json_encode($aUsers);
        Yii::trace("Active: ".var_export($sUsers, true));
        $signatureWsAbaEnglish = md5('Aba English Extranet Key' . $sUsers);
        $client = new SoapClient( Yii::app()->params['campusURLDomain'].'/wsoapaba/usersapi/', array('soap_version' => SOAP_1_1, 'port' => 80, 'trace' => 1) );
        $response = $client->registerUserPremiumB2bExtranet( $signatureWsAbaEnglish, $sUsers);
        Yii::trace("Response Active: ".var_export($response, true));

        return $response;
    }
}