<?php

class StudentController extends Controller
{

    public $defaultAction = 'index';
    private $pageLimit = 10;

    public function init()
    {
        parent::init();

        $this->layout = 'extranet';
    }

//    /**
//     * @return array action filters
//     */
//    public function filters()
//    {
//        return array(
//          'accessControl', // perform access control for CRUD operations
//            //'postOnly + delete', // we only allow deletion via POST request
//        );
//    }
//
//    /**
//     * Specifies the access control rules.
//     * This method is used by the 'accessControl' filter.
//     * @return array access control rules
//     */
//    public function accessRules()
//    {
//        return array(
//          array('allow',  // allow all users to perform 'index' and 'view' actions
//            'actions'=>array('refresh, undodelete'),
//            'roles'=>array(Role::_ABAMANAGER, Role::_AGENT, Role::_RESPONSIBLE, Role::_PARTNER),
//          ),
//
////          array('allow',  // allow all users to perform 'index' and 'view' actions
////            'users'=>array('*'),
////          ),
//
//          array('deny',  // deny all users
//            'users'=>array('*'),
//          ),
//        );
//    }

    /**
     *
     */
    public function actionRefresh()
    {
        $idEnterprise = Utils::getSessionVar('idEnterprise');

        //we get from database all avalaible levels.
        $levels = Level::model()->getAvailableLevels('forfilter');
        //we get from database all avalaible groups.
        $groups = Group::model()->getAvailableGroups(Utils::getSessionVar('idEnterprise'));
        //we get from database all avalaible periods.
        $periods = Period::model()->getAvailablePeriods(Utils::getSessionVar('idEnterprise'));

        $checkedList = Yii::app()->request->getParam('checkedList', '0');
        $checkedList = Utils::adminCheckedList($checkedList);

        $pag = Yii::app()->request->getParam('pag', 1);

        $orderBy = Yii::app()->request->getParam('OrderBy', 's.created desc');
        $arFilters = Utils::getActualFilters();

        $arDatos = ModuleStudentModel::getStudentList(array(
            'limit' => $this->pageLimit,
            'pag' => $pag,
            'orderBy' => $orderBy,
            'checkedList' => $checkedList,
            'idEnterprise' => $idEnterprise,
            'filters' => $arFilters,
        ));
        extract($arDatos);

        $html = $this->renderPartial('list', array('mlStudents' => $mlStudents, 'orderBy' => $orderBy, 'pag' => $pag, 'maxPag' => $maxPag,
            'checkedList' => $checkedList, 'numStudents' => $count, 'filters' => $arFilters, 'levels'=>$levels, 'groups'=>$groups, 'periods'=>$periods, 'numActiveStudents' => $activeCount ), true);

        echo json_encode(array('ok' => 'true', 'html' => $html));
    }

    /**
     *
     */
    public function actionUndodelete()
    {
        $errorMsg = '';

        $undoCode = Yii::app()->request->getParam( 'undoCode', 0 );

        $allOk = ModuleStudentModel::undoDelete( $undoCode );
        if (!$allOk) $errorMsg = ModuleStudentModel::getLastErrorMsg();

        echo json_encode(array('ok' => $allOk, 'errorMsg' => $errorMsg));
    }

}
