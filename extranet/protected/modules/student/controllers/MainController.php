<?php

class MainController extends Controller
{
    public $defaultAction = 'index';

    private $pageLimit = 10;

    public function init() {
        parent::init();

        $this->layout = 'extranet';

    }

//    public function beforeAction($action) {
//
//
//        Yii::trace("Funcio: ".var_export($action->id, true));
//        Yii::trace("Role: ".var_export(Yii::app()->user->role, true));
//        Yii::trace("Role2: ".var_export(Utils::getSessionVar('idEnterprise'), true));
//
//        return parent::beforeAction($action);
//    }


//    /**
//     * @return array action filters
//     */
//    public function filters()
//    {
//        return array(
//          'accessControl', // perform access control for CRUD operations
//            //'postOnly + delete', // we only allow deletion via POST request
//        );
//    }
//
//    /**
//     * Specifies the access control rules.
//     * This method is used by the 'accessControl' filter.
//     * @return array access control rules
//     */
//    public function accessRules()
//    {
//        return array(
//          array('allow',  // allow all users to perform 'index' and 'view' actions
//            'actions'=>array('index, insert, delete, uploadfile, preasignlicense, sendleveltest, groupsearch'),
//            'roles'=>array(Role::_ABAMANAGER, Role::_AGENT, Role::_RESPONSIBLE, Role::_PARTNER),
//          ),
//
//          array('allow',  // allow all users to perform 'index' and 'view' actions
//            'actions'=>array('deleteavailablelicense'),
//            'roles'=>array(Role::_ABAMANAGER, Role::_AGENT, Role::_PARTNER),
//          ),
//
////          array('allow',  // allow all users to perform 'index' and 'view' actions
////            'users'=>array('*'),
////          ),
//
//          array('deny',  // deny all users
//            'users'=>array('*'),
//          ),
//        );
//    }

    /**
     *
     */
    public function actionIndex() {
        $this->layout = 'extranet';

        $email = Yii::app()->request->getParam('email','');

        $idRole = Yii::app()->request->getParam('id', 0);
        if ( $idRole || !Utils::getSessionVar('idEnterprise') ) {
            if ( !Utils::change2Enterprise($idRole) ) {
                $this->redirect( array( Yii::app()->getModule('user')->loginUrl, 'errorMsg' => Yii::t( 'app', 'Forbidden, access denied') ) );
            }
        }

        $idEnterprise = Utils::getSessionVar('idEnterprise', 0);
        $mlLicenseAvailable = ModuleStudentModel::availableLicences($idEnterprise);
        $levelProgress = ModuleStudentModel::getLevelProgressByEnterprisse($idEnterprise);

        $this->render('index', array(
            'mlLicenseAvailable' => $mlLicenseAvailable, 'email'=>$email, 'levelProgress'=>$levelProgress,
        ));
    }

    /**
     *
     */
    public function actionInsert() {
        $idEnterprise = Utils::getSessionVar('idEnterprise');
        $name = Yii::app()->request->getParam('newStudentName');
        $surname = Yii::app()->request->getParam('newStudentSurname');
        $email = Yii::app()->request->getParam('newStudentEmail');
        $group = Yii::app()->request->getParam('newStudentGroup');
        $isoLanguage = Utils::getSessionVar('isoLanguageEnterprise');

        $allOk = ModuleStudentModel::insertStudent($idEnterprise, $name, $surname, $email, $group, $isoLanguage);

        echo json_encode( array( 'ok' => $allOk, 'errorMsg' => ModuleStudentModel::$lastErrorMsg, 'okMsg' => StudentModule::t('Student successfully added.') ) );
    }

    /**
     *
     */
    public function actionDelete()
    {
        $allOk = true;
        $errorMsg = '';
        $okMsg = '';

        $idEnterprise = Utils::getSessionVar('idEnterprise');
        $checkedList = Utils::adminCheckedList( );
        $arFilter = Utils::getActualFilters( );

        $undoCode = false;
        if ( empty($checkedList) ) {
            $allOk = false;
            $errorMsg = StudentModule::t('No one student has been selected.');
        } else {
            $sm = new ModuleStudentModel;
            $undoCode = $sm->deleteItem( $checkedList, $idEnterprise, $arFilter );

            $allOk = ( $undoCode != false );
            if ( !$allOk ) $errorMsg = $sm->getLastErrorMsg();
            else $okMsg = StudentModule::t('The selected student has been removed.').'<a id=\'undoDeleteList_student\' data-undocode=\''.$undoCode.'\' href=\'#\'>'.StudentModule::t('Undo').'</a>';
        }

        echo json_encode( array( 'ok' => $allOk, 'undoCode' => $undoCode, 'errorMsg' => $errorMsg, 'okMsg' => $okMsg ) );
    }

  /**
   *
   */
  public function actionHide()
  {
    $allOk = true;
    $errorMsg = '';
    $okMsg = '';

    $idEnterprise = Utils::getSessionVar('idEnterprise');
    $checkedList = Utils::adminCheckedList( );
    $arFilter = Utils::getActualFilters( );

    $undoCode = false;
    if ( empty($checkedList) ) {
      $allOk = false;
      $errorMsg = StudentModule::t('No one student has been selected.');
    } else {
      $sm = new ModuleStudentModel;
      $allOk = $sm->hideItem( $checkedList, $idEnterprise, $arFilter );

      if ( !$allOk ) $errorMsg = $sm->getLastErrorMsg();
      else $okMsg = Yii::app()->getModule('student')->t('lbl: The selected student has been deactivated.');
    }

    echo json_encode( array( 'ok' => $allOk, 'errorMsg' => $errorMsg, 'okMsg' => $okMsg ) );
  }

  public function actionPreAsignlicense() {
        $checkedList  = Utils::adminCheckedList();
        $filters    = Utils::getActualFilters();
        $idEnterprise = Utils::getSessionVar('idEnterprise', 0);

        if ( empty( $checkedList ) ) {
            $disponibles = false;
            $html = '';
        } else {
            $numSelectedStudents = ModuleStudentModel::getStudentList(array('checkedList' => $checkedList, 'idEnterprise' => $idEnterprise, 'filters' => $filters, 'onlySelected' => true, 'onlyCount' => true));

            if (!empty($numSelectedStudents)) {
                $mlLicenseAvailable = ModuleStudentModel::availableLicences($idEnterprise);
                $disponibles = false;
                foreach ($mlLicenseAvailable as $mLicense) {
                    if (empty($mLicense->id)) continue;         // ** No usar el total.
                    if ($mLicense->numLicenses >= $numSelectedStudents) {
                        $disponibles = true;
                        break;
                    }
                }
            } else $disponibles = false;

            $html = '';
            if ($numSelectedStudents == 0) $html = 'noSelected'; // $this->renderPartial('koSeleccion', array(), true);
            else if (!$disponibles) $html = $this->renderPartial('koLicenses', array(), true);
        }

        echo json_encode( array( 'ok' => $disponibles, 'checkedList' => $checkedList, 'html' => $html, 'filters' => $filters ) );
    }

    /**
     *
     */
    public function actionRefresh()
    {
        $idEnterprise = Utils::getSessionVar('idEnterprise');

        $checkedList = Yii::app()->request->getParam('checkedList', '0');
        $checkedList = Utils::adminCheckedList( $checkedList );

        $pag = Yii::app()->request->getParam('pag', 1);

        $orderBy = Yii::app()->request->getParam('OrderBy', 's.created desc');
        $arFilters = Utils::getActualFilters();

        $allItems = '';
        $arDatos = ModuleStudentModel::getStudentList( array(
            'limit' => $this->pageLimit,
            'pag' => $pag,
            'orderBy' => $orderBy,
            'checkedList' => $checkedList,
            'idEnterprise' => $idEnterprise,
            'filters' => $arFilters,
        ));
        extract($arDatos);

        $html = $this->renderPartial('list', array( 'mlStudents' => $mlStudents, 'orderBy' => $orderBy, 'pag' => $pag, 'maxPag' => $maxPag, 'checkedList' => $checkedList, 'numStudents' => $count, 'filters' => $arFilters, 'all' => $allItems ), true );

        echo json_encode( array( 'ok' => 'true', 'html' => $html ) );
    }

    public function actionUploadfile( ) {
      if (!isset($_FILES['fileName'])) $this->uploadEndWithError( Yii::app()->getModule('student')->t('No file to load.'), true );
      if ( !($_FILES['fileName']['error'] === UPLOAD_ERR_OK) ) $this->uploadEndWithError( Yii::app()->getModule('student')->t('No file to load.'), true );

      $tmpName = $_FILES['fileName']['tmp_name'];
      $code = md5($tmpName);
//      $allOk = copy( $tmpName, Yii::app()->basepath.'/../../campus/runtime/students_'.$code.'.csv' );
//      $allOk = copy( $tmpName, '/var/www/abawebapps_shared_tmp/students_'.$code.'.csv' );
      $allOk = copy( $tmpName, getenv('_SHAREDFOLDER').'students_'.$code.'.csv' );


      if ( !$allOk ) $this->uploadEndWithError( Yii::app()->getModule('student')->t('lbl: Error when trying to upload the file.'), true );

      $charset = Yii::app()->request->getParam('charset', 'auto');
      $arData = $this->parseCSVFile( $code, $charset );

      $html = $this->renderPartial('uploadFilePreview', array( 'code' => $code, 'charset' => $charset, 'arData' => $arData ), true );
      $html = str_replace('<', '|||', $html);
      $json = json_encode(array('ok' => true, 'html' => $html));
        // ** Con codificacion China 'peta' el json_encode. Mostrar como si fuese UTF-8.
      if ( empty($json) && json_last_error() == 5 ) {
          $charset = 'UTF-8';
          $arData = $this->parseCSVFile( $code, $charset );
          $html = $this->renderPartial('uploadFilePreview', array( 'code' => $code, 'charset' => $charset, 'arData' => $arData ), true );
          $json = json_encode(array('ok' => true, 'html' => $html));
      }
      echo $json;
    }

    public function actionChangecharset() {
      $code = Yii::app()->request->getParam('code', 'none');
      $charset = Yii::app()->request->getParam('charset', 'UTF-8');
      $arData = $this->parseCSVFile( $code, $charset );

      $html = $this->renderPartial('uploadFilePreview', array( 'code' => $code, 'charset' => $charset, 'arData' => $arData ), true );
      $json = json_encode(array('ok' => true, 'html' => $html));

        // ** Con codificacion China 'peta' el json_encode. Mostrar como si fuese UTF-8.
      if ( empty($json) && json_last_error() == 5 ) {
          $charset = 'UTF-8';
          $arData = $this->parseCSVFile( $code, $charset );
          $html = $this->renderPartial('uploadFilePreview', array( 'code' => $code, 'charset' => $charset, 'arData' => $arData ), true );
          $json = json_encode(array('ok' => true, 'html' => $html));
      }

      if ( empty($json) ) $json = json_encode(array('ok' => false, 'html' => 'error'));
      echo $json;
    }

    public function actionUploadfileok( ) {
      $code = Yii::app()->request->getParam('code', 'none');
      $charset = Yii::app()->request->getParam('charset', 'UTF-8');
      $arData = $this->parseCSVFile( $code, $charset, 'UTF-8', false );

      $sm = new ModuleStudentModel;
      $allOk = $sm->insertStudentsArray($arData);

      if ( $allOk ) {
        $errorMsg = StudentModule::t('All ok.');
        $this->layout = 'basic';
        $html = $this->render('okUploadFile', array( 'message' => $errorMsg ), true );
        echo json_encode( array( 'ok' => true, 'html' => $html ) );
      } else {
        $this->uploadEndWithError( $sm->getLastErrorMsg() );
      }
    }

    protected function parseCSVFile( $code, $charset, $toCharset = 'UTF-8', $firstLoad = true ) {
//      $filename = Yii::app()->basepath.'/../../campus/runtime/students_'.$code.'.csv';
      $filename = getenv('_SHAREDFOLDER').'students_'.$code.'.csv';

      $arColumns = array();
      $arData = array();
      $line = 1;
      ini_set('auto_detect_line_endings',TRUE);
      if (($fd = fopen($filename, "r")) !== FALSE) {
        while (($str = fgets($fd)) !== FALSE) {
          if ( $firstLoad && $line > 100 ) break;

          // ** Columnas separadas por ',' o por ';'.
          $dataLine1 = str_getcsv($str, ',');
          $dataLine2 = str_getcsv($str, ';');
          if ( count($dataLine1) > count($dataLine2) ) $dataLine = $dataLine1; else $dataLine = $dataLine2;
          $number = count($dataLine);

          if ( $line == 1 ) {
            for ($c=0; $c < $number; $c++) {
              $value = trim(strtolower($dataLine[$c]));
              if ( $c == 0 && str_replace(chr(239).chr(187).chr(191), '', $value) !== $value ) $value = str_replace( chr(239).chr(187).chr(191), '', $value );
              if ( $value == strtolower(StudentModule::t('NAME')) ) $value = 'name';
              else if ( $value == strtolower(StudentModule::t('SURNAME')) ) $value = 'surname';
              else if ( $value == strtolower(StudentModule::t('SURNAMES')) ) $value = 'surname';
              else if ( $value == strtolower(StudentModule::t('EMAIL')) ) $value = 'email';
              else if ( $value == strtolower(StudentModule::t('GROUP')) ) $value = 'group';
              else if ( $value == 'surnames' ) $value = 'surname';

              if ( $value == 'name' || $value == 'surname' || $value == 'email' || $value == 'group' ) $arColumns[$value] = $c;
            }

            if ( !isset($arColumns['name']) ) $this->uploadEndWithError( StudentModule::t('You need a column called name'), $firstLoad );
            else if ( !isset($arColumns['surname']) ) $this->uploadEndWithError( StudentModule::t('You need a column called surname.'), $firstLoad );
            else if ( !isset($arColumns['email']) ) $this->uploadEndWithError( StudentModule::t('You need a column called email.'), $firstLoad );
            if ( !isset($arColumns['group']) ) $arColumns['group'] = -1;

          } else {
            if ( !isset($dataLine[$arColumns['name']]) || !isset($dataLine[$arColumns['surname']]) || !isset($dataLine[$arColumns['email']]) ) continue;
            if ( !isset($dataLine[$arColumns['group']]) || empty($dataLine[$arColumns['group']]) ) $dataLine[$arColumns['group']] = '-';

            $artmp = array(
              'name' => Utils::toCharset($dataLine[$arColumns['name']], $charset, $toCharset),
              'surname' => Utils::toCharset($dataLine[$arColumns['surname']], $charset, $toCharset),
              'email' => Utils::toCharset($dataLine[$arColumns['email']], $charset, $toCharset),
              'group' => Utils::toCharset($dataLine[$arColumns['group']], $charset, $toCharset),
            );
            $arData[] = $artmp;

          }
          $line++;
        }
        fclose($fd);
      }
      ini_set('auto_detect_line_endings',FALSE);

      return $arData;
    }

    public function uploadEndWithError( $errorMsg, $firstLoad = false ) {
      $html = $this->renderPartial('errorUploadFile', array('message' => $errorMsg), true);
      if ($firstLoad) {
        $html = str_replace('<', '|||', $html);
      }
      echo json_encode(array('ok' => false, 'html' => $html));
      exit();
    }

    public function actionGroupSearch( ) {
        $partialWord = Yii::app()->request->getParam('partialWord', '0');

        $idEnterprise = Utils::getSessionVar('idEnterprise');

        $criteria = new CDbCriteria();
        $criteria->addSearchCondition('name', $partialWord);
        $criteria->addCondition( 't.idEnterprise = :idEnterprise' );
        $criteria->params = array_merge( $criteria->params, array( ':idEnterprise' => $idEnterprise ) );
        $criteria->limit = 10;
        $mlGroup = Group::model()->findAll($criteria);

        $arRs = array();
        foreach( $mlGroup as $mGroup ) {
            $arRs[] = $mGroup->name;
        }

//    for( $i = 0; $i < rand(4, 6); $i++ ) {
//      $arRs[] = 'Valor '.rand(20, 30);
//    }

        echo json_encode( array( 'ok' => true, 'result' => $arRs ) );
    }

    public function actionSendLevelTest() {
        $errorMsg = '';
        $okMsg = StudentModule::t('Test/s sent successfully.');

        $idEnterprise = Utils::getSessionVar('idEnterprise');
        $checkedList = Utils::adminCheckedList( );
        $arFilters = Utils::getActualFilters( );
        $step = Yii::app()->request->getParam('step', 0);

        $arDatos = ModuleStudentModel::getStudentList(array(
            'limit' => 10000,
            'orderBy' => '',
            'checkedList' => $checkedList,
            'idEnterprise' => $idEnterprise,
            'filters' => $arFilters,
            'onlySelected' => true,
        ));
        $count = $arDatos['count'];
        $mlStudents = $arDatos['mlStudents'];

        $dataHTML = '';
        if ( empty( $checkedList ) ) {
          $errorMsg = StudentModule::t('You have no selection.');
          $allOk = false;
        } else if ( $count > 0 && $step == 2 ) {
            $allOk = true;

            $mEnterprise = Enterprise::model()->findByPk( $idEnterprise );
            $mUser = $mEnterprise->mainContact;

            foreach ($mlStudents as $mStudent) {
                $this->sendTestToMail($mStudent->id, $mUser, '');
            }

        } else if ( $count > 0 ) {
            $allOk = true;
            $dataHTML = $this->renderPartial('popupLevelTest', array('numero' => $count), true);
        } else {
            $errorMsg = StudentModule::t('You have no selection.');
            $allOk = false;
        }

        echo json_encode( array( 'ok' => $allOk, 'errorMsg' => $errorMsg, 'okMsg' => $okMsg, 'html' => $dataHTML ) );
    }

    protected function sendTestToMail( $idStudent, $mUser, $code = '' ) {
        $arParameters = array(
          'action' => HeAbaSelligent::_SENDLEVELTEST,
          'idStudent' => $idStudent,
          'idUser' => $mUser->id,
          'code' => $code,
        );
        $allOk = HeAbaMail::sendEmail( $arParameters );
    }

    public function actionDeleteavailablelicense() {
        $allOk = true;

        $idEnterprise = Utils::getSessionVar('idEnterprise');
        $idLicenseType = Yii::app()->request->getParam('id');

        $criteria = new CDbCriteria();
        $criteria->addCondition('idEnterprise = :idEnterprise');
        $criteria->addCondition('idLicenseType = :idLicenseType');
        $criteria->addCondition('numLicenses > 0');
        $criteria->params = array(':idLicenseType' => $idLicenseType, ':idEnterprise' => $idEnterprise );

        $sql = LicenseAvailable::model()->updateAll( array(
          'numLicenses' => 0,
          'lastupdated' => date('Y-m-d H:i:s')
        ), $criteria );


        if ( $allOk ) {
            $dataHTML = $this->renderPartial('okDeleteAvailableLicense', array(), true);
        } else {
            $dataHTML = $this->renderPartial('koDeleteAvailableLicense', array(), true);
        }


        echo json_encode( array( 'ok' => $allOk, 'html' => $dataHTML ) );
    }

  public function actionResendwelcomeemail() {
    $allOk = true;
    $errorMsg = '';
    $html = '';

    $idStudent = Yii::app()->request->getParam('id');

    $mStudent = Student::model()->findByPk($idStudent);
    $mStudentCampus = UserCampus::model()->findByPk( $mStudent->idStudentCampus );
    $newPassword = HeString::generateNewPassword();
    $mStudentCampus->password=md5($newPassword);
    $mStudentCampus->save();

    $arParameters = array(
      'action' => HeAbaSelligent::_RESENDWELCOMEMSG,
      'idStudent' => $mStudent->idStudentCampus,
      'password' => $newPassword,
    );
    $allOk = HeAbaMail::sendEmail( $arParameters );
    if ( $allOk ) {
      $html = $html = $this->renderPartial('okResendWelcomeMsg', array(), true);
    }

    echo json_encode( array( 'ok' => $allOk, 'html' => $html, 'errorMsg' => $errorMsg ) );
  }

  private function generateNewLeveltestData() {
      $code = false;

      $json = file_get_contents(getenv('CAMPUS_URL').'/leveltest/newLeveltestData');
      try{
          $tmp = json_decode($json);
          $code = $tmp->code;
      } catch(Exception $e) {
      }

      return $code;
  }

    /**
     *
     */
    public function actionExport()
    {
        $idEnterprise = Utils::getSessionVar('idEnterprise');
        $checkedList = Utils::adminCheckedList( );
        $arFilters = Utils::getActualFilters( );
        $orderBy = Yii::app()->request->getParam('OrderBy', 's.created desc');

        $arDatos = ModuleStudentModel::getStudentList(array(
          'limit' => 10000,
          'orderBy' => $orderBy,
          'checkedList' => $checkedList,
          'idEnterprise' => $idEnterprise,
          'filters' => $arFilters,
        ));
        extract($arDatos);

        $data = array();
        $xls = new JPhpCsv('UTF-8', false, 'Users');
        $flagFirst=true;
        $field = array("name","surname","email","levelTest","Level","period","group","expires","status", "nameTeacher", "emailTeacher");
        $arLabel = array("Name","SURNAME","Email","Leveltest","Current Level","Period","Group","EXPIRES","Status", "Name Teacher", "Email Teacher");
        foreach($arLabel as $k=>$value) $arLabel[$k] = ucfirst( strtolower( Yii::app()->getModule('student')->t($value) ) );

        foreach($mlStudents as $row)
        {
            if($flagFirst)
            {
                $flagFirst= false;
                $theRow=array();
                foreach ($row as $key =>$value)
                {
                    if(in_array($key, $field)) {
                        $keyLabel = array_search($key,$field);
                        array_push($theRow, $arLabel[$keyLabel]);
                    }
                }
                array_push($data, $theRow);
            }
            $theRow=array();
            foreach ($row as $key =>$value)
            {
                if(in_array($key, $field))
                {
                    array_push($theRow, $this->exportTransformField($key, $value));
//                }
//                else
//                {
//                    array_push($theRow, $value);
                }
            }
            array_push($data, $theRow);
        }
        if($flagFirst==true)
        {
            $theRow=array();
            array_push($theRow, "DATA NOT FOUND (Data not exported)");
            array_push($data, $theRow);
        }
        $xls->addArray($data);
        $xls->generateXML("StudentList-".date("d-m-Y"));

    }

    private function exportTransformField( $key, $value ) {

        switch($key) {
            case 'Level':
            case 'levelTest':
                $value = Yii::app()->getModule('student')->t($value);
                break;
            case 'status':
                $value = ($value==StudentGroup::_STATUS_ACTIVE) ? 'Activo' : 'Inactivo';
                $value = Yii::app()->getModule('student')->t($value);
                break;
            default:
//                $value = ucfirst($value);
                break;
        }

        return $value;
    }

}
