<?php

/**
 * This is the model class for table "country".
 *
 * The followings are the available columns in table 'country':
 * @property integer $id
 */
class Country extends CmAbaCountry
{
  const _SPAIN = 199;

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'price'=>array(self::HAS_MANY, 'Price', 'idCountry'),
            'orderedprices'=>array(self::HAS_MANY, 'Price', 'idCountry', 'order'=>'idPeriodPay'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'days' => 'Days',
            'ID_month' => 'Id Month',
            'iso' => 'Iso',
            'name_m' => 'Name M',
            'name' => 'Name',
            'iso3' => 'Iso3',
            'numcode' => 'Numcode',
            'officialIdCurrency' => 'Official Id Currency',
            'officialIdCurrency_tmpDisabled' => 'Official Id Currency Tmp Disabled',
            'ABAIdCurrency' => 'Abaid Currency',
            'ABALanguage' => 'Abalanguage',
            'ABAOldCampusIdCurrency' => 'Abaold Campus Id Currency',
            'decimalPoint' => 'Decimal Point',
        );
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseCountry the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /* Returns a listData object, necessary to populate a select drop-down and many more components. */
    public function getAvailableCountries()
    {
        $criteria = new CDbCriteria();
      //$criteria->addCondition('invoice = 1');
        $criteria->addNotInCondition('id', array(300, 301, 302, 303) );
        $availableCountries = Country::model()->findAll($criteria);
        $availableCountries = CHtml::listData($availableCountries, 'id', 'name');
        return $availableCountries;
    }

  static public function getIdCountryByIP( $ip = '' ) {

    $mCountry = new Country();
    if (!empty($ip)) {
      $mCountry = $mCountry->getCountryCodeByIP($ip);
    } else {
      $mCountry = $mCountry->getCountryCodeByIP();
    }

    if ( $mCountry ) {
      $idUserCountry = $mCountry->getId();
    } else {
      $idUserCountry = Country::_SPAIN;
    }

    return $idUserCountry;
  }

}
