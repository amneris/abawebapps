<?php

/**
 * This is the model class for table "language".
 *
 * The followings are the available columns in table 'language':
 * @property integer $id
 */
class Role extends BaseRole
{
  
  const _ABAMANAGER = 1;
  const _TEACHER = 2;
  const _RESPONSIBLE = 3;
  const _AGENT = 4;
  const _PARTNER = 5;

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return BaseLanguage the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(

        );
    }

    public function getUserCreationAvailableList($currentUser)
    {
        $criteria = new CDbCriteria();
        switch ($currentUser)
        {
          case Role::_ABAMANAGER:
                if(Utils::getSessionVar('idEnterprise')==0)
                $criteria->addCondition('id in ('.Role::_ABAMANAGER.','.Role::_TEACHER.','.Role::_RESPONSIBLE.','.Role::_AGENT.','.Role::_PARTNER.')');
                break;
          case Role::_AGENT:
                $criteria->addCondition('id in('.Role::_TEACHER.','.Role::_RESPONSIBLE.','.Role::_AGENT.')');
                break;
          case Role::_PARTNER:
          case Role::_RESPONSIBLE:
                $criteria->addCondition('id in('.Role::_TEACHER.','.Role::_RESPONSIBLE.')');
                break;
          case Role::_TEACHER:
                $criteria->addCondition('id in("")');
                break;
        }
        $roles = Role::model()->findAll($criteria);
        return $roles;

    }
}
