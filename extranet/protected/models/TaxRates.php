<?php

/**
 * This is the model class for table "tax_rates".
 *
 * The followings are the available columns in table 'tax_rates':
 * @property string $idTaxRate
 * @property string $idCountry
 * @property string $taxRateValue
 * @property string $dateStartRate
 * @property string $dateEndRate
 */
class TaxRates extends BaseTaxRates
{
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseTaxRates the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

  public static function getTaxRateByCountry( $idCountry ) {

//    select (taxRateValue + 100) / 100 from aba_b2c.tax_rates where idCountry = 199 and dateStartRate < now() and (dateEndRate > now() or dateEndRate is null)

    $criteria = new CDbCriteria();
    $criteria->addCondition('idCountry = :idCountry');
    $criteria->addCondition('dateStartRate < now()');
    $criteria->addCondition('(dateEndRate > now() or dateEndRate is null)');
    $criteria->params = array_merge( $criteria->params, array( ':idCountry' => $idCountry ) );
    $mTaxRate = TaxRates::model()->find($criteria);
    if ( $mTaxRate ) {
      $taxRate = ($mTaxRate->taxRateValue + 100) / 100;
    } else $taxRate = 0;

    return $taxRate;
  }

}
