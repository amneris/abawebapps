<?php

/**
 * This is the model class for table "log_activacion".
 *
 * The followings are the available columns in table 'log_activacion':
 * @property integer $id
 * @property integer $idStudent
 * @property string $oldTeacherEmail
 * @property integer $oldIdPartnerCurrent
 * @property string $created
 * @property string $deleted
 * @property integer $isdeleted
 */
class LogActivacion extends BaseLogActivacion
{
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseLogActivacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
