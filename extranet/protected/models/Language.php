<?php

/**
 * This is the model class for table "language".
 *
 * The followings are the available columns in table 'language':
 * @property integer $id
 */
class Language extends BaseLanguage
{
    const _ENGLISH = 1;
    const _CHINESE = 7;

  static $arIso = array( 1 => 'en', 'es', 'it', 'fr', 'de', 'pt' );

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user'=>array(self::HAS_MANY, 'User', 'idLanguage'),

		);
	}
  
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseLanguage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /* Returns a listData object, necessary to populate a select drop-down and many more components. */
    public function getAvailableLanguages()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('enabled = 1');
        $availableLanguages = Language::model()->findAll($criteria);
        $availableLanguages = CHtml::listData($availableLanguages, 'id', 'name');
        return $availableLanguages;
    }

    /* Returns a listData object, necessary to populate a select drop-down and many more components. */
    /**
     * @return array relational rules.
     */
    public function getAvailableLanguagesCourse()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('courseLang = 1');
        $availableLanguagesCourse = Language::model()->findAll($criteria);
        $availableLanguagesCourse = CHtml::listData($availableLanguagesCourse, 'id', 'name');
        return $availableLanguagesCourse;
    }
}
