<?php

/**
 * This is the model class for table "level".
 *
 * The followings are the available columns in table 'level':
 * @property integer $id
 * @property string $name
 * @property string $created
 * @property string $deleted
 * @property integer $isdeleted
 */
class Level extends BaseLevel
{

    const _BEGINNER = 1;
    const _LOWINTERMEDIATE = 2;
    const _INTERMEDIATE = 3;
    const _UPPERINTERMEDIATE = 4;
    const _ADVANCED = 5;
    const _BUSINESS = 6;

    const _UNKNOWN = -2;
    const _SENT = -1;

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'created' => 'Created',
            'deleted' => 'Deleted',
            'isdeleted' => 'Isdeleted',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return BaseLevel the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /* Returns a listData object, necessary to populate a select drop-down and many more components. */
    public function getAvailableLevels( $type = 'forselector' ) {
      $criteria = new CDbCriteria();
      $criteria->addCondition('isdeleted = 0');
      $criteria->addCondition(" $type = 1 ");
      $availableLevels = Level::model()->findAll($criteria);
      foreach( $availableLevels as $mLevel ) $mLevel->name = Yii::app()->getModule('student')->t($mLevel->name);
      $availableLevels = CHtml::listData($availableLevels, 'id', 'name');
      return $availableLevels;
    }

}
