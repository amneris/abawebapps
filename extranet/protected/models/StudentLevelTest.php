<?php

/**
 * This is the model class for table "student_level_test".
 *
 * The followings are the available columns in table 'student_level_test':
 * @property integer $id
 */
class StudentLevelTest extends BaseStudentLevelTest
{
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseStudentLevelTest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
}
