<?php

/**
 * This is the model class for table "queue_AmazonDeleteFiles".
 *
 * The followings are the available columns in table 'queue_AmazonDeleteFiles':
 * @property integer $id
 * @property string $filename
 */
class QueueAmazonDeleteFiles extends BaseQueueAmazonDeleteFiles
{
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QueueAmazonDeleteFiles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
