<?php

/**
 * This is the model class for table "licenseavailable".
 *
 * The followings are the available columns in table 'licenseavailable':
 * @property integer $id
 */
class LicenseAvailable extends BaseLicenseAvailable
{
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseLicenseavailable the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'licenseType'=>array(self::BELONGS_TO, 'LicenseType', 'idLicenseType'),
        );


    }
    
}
