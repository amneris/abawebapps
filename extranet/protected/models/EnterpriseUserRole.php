<?php

/**
 * This is the model class for table "enterprise_user_role".
 *
 * The followings are the available columns in table 'enterprise_user_role':
 * @property integer $id
 */
class EnterpriseUserRole extends BaseEnterpriseUserRole
{
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseEnterpriseUserRole the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'role'=>array(self::BELONGS_TO, 'Role', 'idRole'),
            'enterprise'=>array(self::BELONGS_TO, 'Enterprise', 'idEnterprise'),
            'users'=>array(self::BELONGS_TO, 'User', 'idUser'),
        );
    }

    public function getUsersByRoleAndEnterprise($idRole, $idEnterprise)
    {
        $userList = '';
        $connection = Yii::app()->dbExtranet;
        $sql = "select idUser from enterprise_user_role where idRole in ($idRole) and idEnterprise = $idEnterprise and isdeleted = 0 ; ";
        $dataReader = $connection->createCommand($sql)->queryAll();
        foreach($dataReader as $row)
        {
            $userList .= $row['idUser'].",";
        }
        $userList = rtrim($userList, ",");
        return $userList;
    }

    public function getEnterprisesByUser($idUser)
    {
        $enterpriseList = '';
        $connection = Yii::app()->dbExtranet;
        $sql = "select idEnterprise from enterprise_user_role where idUser in ('$idUser'); ";
        $dataReader = $connection->createCommand($sql)->queryAll();
        foreach($dataReader as $row)
        {
            $enterpriseList .= $row['idEnterprise'].",";
        }
        $enterpriseList = rtrim($enterpriseList, ",");
        return $enterpriseList;
    }
    
  static public function getEnterpriseAgent($idEnterprise) {
    $criteria = new CDbCriteria();
    $criteria->addCondition('t.idEnterprise = :idEnterprise');
    $criteria->addInCondition('t.idRole', array(Role::_AGENT, Role::_PARTNER) );
    $criteria->addCondition('t.isdeleted = :deleted');
    $criteria->params = array_merge( $criteria->params, array(':idEnterprise' => $idEnterprise, ':deleted' => 0) );
    $criteria->limit = 1;
    $criteria->order = 'idRole desc, id';
    $mEnterpriseUserRole = EnterpriseUserRole::model()->find($criteria);
    if ( $mEnterpriseUserRole ) $mUser = $mEnterpriseUserRole->users;
    else {
      $mUser = null;
    }
    return $mUser;
  }
}
