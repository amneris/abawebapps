<?php

/**
 * This is the model class for table "license_type".
 *
 * The followings are the available columns in table 'license_type':
 * @property integer $id
 */
class LicenseType extends BaseLicenseType
{
  const _Quarterly = 1;
  const _Semiannual = 2;
  const _Annual = 3;
  const _Twoyears = 4;
  const _Trial = 5;
  
  
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseLicenseType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    /* Returns a listData object, necessary to populate a select drop-down and many more components. */
    public function getAvailableLicenseTypes()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('isEnabled = 1');
        $availableLicenseType = LicenseType::model()->findAll($criteria);
        $availableLicenseType = CHtml::listData($availableLicenseType, 'id', 'name');
        return $availableLicenseType;
    }

}
