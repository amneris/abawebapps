<?php

/**
 * This is the model class for table "tax_rates".
 *
 * The followings are the available columns in table 'tax_rates':
 * @property string $idTaxRate
 * @property string $idCountry
 * @property string $taxRateValue
 * @property string $dateStartRate
 * @property string $dateEndRate
 */
class BaseTaxRates extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tax_rates';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCountry, dateStartRate', 'required'),
			array('idCountry', 'length', 'max'=>4),
			array('taxRateValue', 'length', 'max'=>6),
			array('dateEndRate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idTaxRate, idCountry, taxRateValue, dateStartRate, dateEndRate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idTaxRate' => 'Id Tax Rate',
			'idCountry' => 'Id Country',
			'taxRateValue' => 'Tax Rate Value',
			'dateStartRate' => 'Date Start Rate',
			'dateEndRate' => 'Date End Rate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idTaxRate',$this->idTaxRate,true);
		$criteria->compare('idCountry',$this->idCountry,true);
		$criteria->compare('taxRateValue',$this->taxRateValue,true);
		$criteria->compare('dateStartRate',$this->dateStartRate,true);
		$criteria->compare('dateEndRate',$this->dateEndRate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseTaxRates the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
