<?php

/**
 * This is the model class for table "license_available".
 *
 * The followings are the available columns in table 'license_available':
 * @property integer $id
 * @property integer $idEnterprise
 * @property integer $idLicenseType
 * @property integer $numLicenses
 * @property integer $histLicenses
 * @property string $lastupdated
 */
class BaseLicenseAvailable extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'license_available';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idEnterprise, idLicenseType, numLicenses, histLicenses', 'required'),
			array('idEnterprise, idLicenseType, numLicenses, histLicenses', 'numerical', 'integerOnly'=>true),
			array('lastupdated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idEnterprise, idLicenseType, numLicenses, histLicenses, lastupdated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idEnterprise' => 'Id Enterprise',
			'idLicenseType' => 'Id License Type',
			'numLicenses' => 'Num Licenses',
			'histLicenses' => 'Hist Licenses',
			'lastupdated' => 'Lastupdated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idEnterprise',$this->idEnterprise);
		$criteria->compare('idLicenseType',$this->idLicenseType);
		$criteria->compare('numLicenses',$this->numLicenses);
		$criteria->compare('histLicenses',$this->histLicenses);
		$criteria->compare('lastupdated',$this->lastupdated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbExtranet;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseLicenseAvailable the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
