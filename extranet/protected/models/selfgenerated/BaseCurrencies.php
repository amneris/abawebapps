<?php

/**
 * This is the model class for table "currencies".
 *
 * The followings are the available columns in table 'currencies':
 * @property string $id
 * @property string $extCode
 * @property string $description
 * @property string $symbol
 * @property string $unitEur
 * @property string $outEur
 * @property string $unitUsd
 * @property string $outUsd
 * @property string $decimalPoint
 * @property string $dateLastUpdate
 */
class BaseCurrencies extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'currencies';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, extCode, unitEur, outEur, unitUsd, dateLastUpdate', 'required'),
			array('id, extCode, decimalPoint', 'length', 'max'=>3),
			array('description', 'length', 'max'=>100),
			array('symbol', 'length', 'max'=>5),
			array('unitEur, outEur, unitUsd, outUsd', 'length', 'max'=>14),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, extCode, description, symbol, unitEur, outEur, unitUsd, outUsd, decimalPoint, dateLastUpdate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'extCode' => 'Ext Code',
			'description' => 'Description',
			'symbol' => 'Symbol',
			'unitEur' => 'Unit Eur',
			'outEur' => 'Out Eur',
			'unitUsd' => 'Unit Usd',
			'outUsd' => 'Out Usd',
			'decimalPoint' => 'Decimal Point',
			'dateLastUpdate' => 'Date Last Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('extCode',$this->extCode,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('symbol',$this->symbol,true);
		$criteria->compare('unitEur',$this->unitEur,true);
		$criteria->compare('outEur',$this->outEur,true);
		$criteria->compare('unitUsd',$this->unitUsd,true);
		$criteria->compare('outUsd',$this->outUsd,true);
		$criteria->compare('decimalPoint',$this->decimalPoint,true);
		$criteria->compare('dateLastUpdate',$this->dateLastUpdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseCurrencies the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
