<?php

/**
 * This is the model class for table "sourceTranslations".
 *
 * The followings are the available columns in table 'sourceTranslations':
 * @property integer $idsourceTranslations
 * @property string $seccion
 * @property string $label
 * @property string $es
 * @property integer $es_validated
 * @property string $en
 * @property integer $en_validated
 * @property string $it
 * @property integer $it_validated
 * @property string $fr
 * @property integer $fr_validated
 * @property string $pt
 * @property integer $pt_validated
 */
class BaseSourceTranslations extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sourceTranslations';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('es_validated, en_validated, it_validated, fr_validated, pt_validated, ca_validated', 'numerical', 'integerOnly'=>true),
			array('seccion', 'length', 'max'=>45),
			array('label, es, en, it, fr, pt, ca', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idsourceTranslations, seccion, label, es, es_validated, en, en_validated, it, it_validated, fr, fr_validated, pt, pt_validated, ca, ca_validated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idsourceTranslations' => 'Idsource Translations',
			'seccion' => 'Seccion',
			'label' => 'Label',
			'es' => 'Es',
			'es_validated' => 'Es Validated',
			'en' => 'En',
			'en_validated' => 'En Validated',
			'it' => 'It',
			'it_validated' => 'It Validated',
			'fr' => 'Fr',
			'fr_validated' => 'Fr Validated',
			'pt' => 'Pt',
			'pt_validated' => 'Pt Validated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idsourceTranslations',$this->idsourceTranslations);
		$criteria->compare('seccion',$this->seccion,true);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('es',$this->es,true);
		$criteria->compare('es_validated',$this->es_validated);
		$criteria->compare('en',$this->en,true);
		$criteria->compare('en_validated',$this->en_validated);
		$criteria->compare('it',$this->it,true);
		$criteria->compare('it_validated',$this->it_validated);
		$criteria->compare('fr',$this->fr,true);
		$criteria->compare('fr_validated',$this->fr_validated);
		$criteria->compare('pt',$this->pt,true);
		$criteria->compare('pt_validated',$this->pt_validated);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbExtranet;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseSourceTranslations the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
