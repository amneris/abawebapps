<?php

/**
 * This is the model class for table "enterprise_user_role".
 *
 * The followings are the available columns in table 'enterprise_user_role':
 * @property integer $id
 * @property integer $idEnterprise
 * @property integer $idUser
 * @property integer $idRole
 * @property string $created
 * @property string $deleted
 * @property integer $isdeleted
 */
class BaseEnterpriseUserRole extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'enterprise_user_role';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idEnterprise, idUser, idRole', 'required'),
			array('idEnterprise, idUser, idRole, isdeleted', 'numerical', 'integerOnly'=>true),
			array('created, deleted', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idEnterprise, idUser, idRole, created, deleted, isdeleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idEnterprise' => 'Id Enterprise',
			'idUser' => 'Id User',
			'idRole' => 'Id Role',
			'created' => 'Created',
			'deleted' => 'Deleted',
			'isdeleted' => 'Isdeleted',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idEnterprise',$this->idEnterprise);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('idRole',$this->idRole);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('deleted',$this->deleted,true);
		$criteria->compare('isdeleted',$this->isdeleted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbExtranet;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseEnterpriseUserRole the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
