<?php

/**
 * This is the model class for table "log_actividad".
 *
 * The followings are the available columns in table 'log_actividad':
 * @property integer $id
 * @property string $controller
 * @property string $action
 * @property string $data
 * @property string $created
 * @property integer $idUser
 * @property integer $idEnterprise
 * @property string $idModule
 */
class BaseLogActividad extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'log_actividad';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('controller, action', 'required'),
			array('idUser, idEnterprise', 'numerical', 'integerOnly'=>true),
			array('controller, action', 'length', 'max'=>50),
			array('data', 'length', 'max'=>10000),
			array('idModule', 'length', 'max'=>255),
			array('created', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, controller, action, data, created, idUser, idEnterprise, idModule', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'controller' => 'Controller',
			'action' => 'Action',
			'data' => 'Data',
			'created' => 'Created',
			'idUser' => 'Id User',
			'idEnterprise' => 'Id Enterprise',
			'idModule' => 'Id Module',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('controller',$this->controller,true);
		$criteria->compare('action',$this->action,true);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('idEnterprise',$this->idEnterprise);
		$criteria->compare('idModule',$this->idModule,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbExtranet;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseLogActividad the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
