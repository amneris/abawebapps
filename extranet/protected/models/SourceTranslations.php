<?php

/**
 * This is the model class for table "sourceTranslations".
 *
 * The followings are the available columns in table 'sourceTranslations':
 * @property integer $idsourceTranslations
 * @property string $seccion
 * @property string $label
 * @property string $es
 * @property integer $es_validated
 * @property string $en
 * @property integer $en_validated
 * @property string $it
 * @property integer $it_validated
 * @property string $fr
 * @property integer $fr_validated
 * @property string $pt
 * @property integer $pt_validated
 */
class SourceTranslations extends BaseSourceTranslations
{

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idsourceTranslations' => 'Idsource Translations',
			'seccion' => 'Seccion',
			'label' => 'Label',
			'es' => 'Es',
			'es_validated' => 'is validated (ES)?',
			'en' => 'En',
			'en_validated' => 'is validated (EN)?',
			'it' => 'It',
			'it_validated' => 'is validated (IT)?',
			'fr' => 'Fr',
			'fr_validated' => 'is validated (FR)?',
			'pt' => 'Pt',
			'pt_validated' => 'is validated (PT)?',
      'ca' => 'Ca',
      'ca_validated' => 'is validated (CA)?',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SourceTranslations the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	static public function getSeccionNames()
    {
        $sql= "Select seccion from abaenglish_extranet.sourceTranslations group by seccion;";
        $connection = Yii::app()->dbExtranet;
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();

        while(($row = $dataReader->read())!==false)
        {
            $listSeccion[$row['seccion']] = $row['seccion'];

        }
        return $listSeccion;
    }

}
