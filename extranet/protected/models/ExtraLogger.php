<?php

/**
 * This is the model class for table "log".
 *
 * The followings are the available columns in table 'log':
 * @property integer $id
 */
class ExtraLogger extends BaseLogActividad
{
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    protected function beforeValidate() {
      if ( empty($this->created) || $this->created == '2000-01-01 00:00:00' ) $this->created = date("Y-m-d H:i:s");
      
      return parent::beforeValidate();
    }
}
