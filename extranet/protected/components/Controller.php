<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
  
  /**
   *
   * @var string The default page title.
   */
  public $pageTitle = 'ABA English Extranet';
  
  /**
   * @var string the default layout for the controller view. Defaults to '//layouts/column1',
   * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
   */
  public $layout='//layouts/column1';
  /**
   * @var array context menu items. This property will be assigned to {@link CMenu::items}.
   */
  public $menu=array();
  /**
   * @var array the breadcrumbs of the current page. The value of this property will
   * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
   * for more details on how to specify this property.
   */
  public $breadcrumbs=array();

  function init()
  {
    parent::init();

    $initLanguage = HeMixed::getAutoLangDecision();
    Yii::app()->language = Utils::getSessionVar('userLanguage', $initLanguage);
  }

  protected function beforeAction($action) {

    $bAction = parent::beforeAction( $action );

    $controllerId = $this->id;
    $actionId = $action->id;
    if ( isset($this->module) ) $module = $this->module->id; else $module = '';
    Utils::setSessionVar('idAction', $actionId);
    Utils::setSessionVar('idController', $controllerId);
    Utils::setSessionVar('idModule', $module);
      // ** Logger
    Utils::log($module, $controllerId, $actionId);

      // ** Aixo ho vull treure.
    if( !isset($_SESSION) && $controllerId != 'login' )
    {
        $this->redirect('/user/login/logout');
    }

      // ** Cargar JS y CSS personalizados para este modulo/controlador/seccion.
    if ( file_exists('../public/js/sections/'.$module.'/'.$controllerId.'/'.$actionId.'.js') )
      Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/sections/'.$module.'/'.$controllerId.'/'.$actionId.'.js',CClientScript::POS_END);
    if ( file_exists('../public/css/sections/'.$module.'/'.$controllerId.'/'.$actionId.'.css') )
      Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css//sections/'.$module.'/'.$controllerId.'/'.$actionId.'.css');

    return $bAction;
  }

}