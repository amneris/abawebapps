<?php

class WebUser extends CWebUser
{
  public $loginUrl=array('/');
  public $registrationUrl;
  public $recoveryUrl;
  public $preLoginUrl;
  
    /**
     * Overrides a Yii method that is used for roles in controllers (accessRules).
     *
     * @param string $operation Name of the operation required (here, a role).
     * @param mixed $params (opt) Parameters for this operation, usually the object to access.
     * @return bool Permission granted?
     */
    public function checkAccess($operation, $params=array())
    {
      if (empty($this->id)) {
            // Not identified => no rights
            return false;
        }
        $role = $this->getState("role");
        if ($role === 'admin') {
            return true; // admin role has access to everything
        }

        $idRole = $this->getState("idRole");

        // allow access if the operation request is the current user's role
        return ($operation === $idRole);
    }
    
    public function getLanguage( $defaultLanguage = 'es' ) {
      return $this->getState('userLanguage', $defaultLanguage);
    }

    public function getHistrenemberme() {
      return $this->getState('histrenemberme', true);
    }
    
    public function getIsAgent() {
      return Utils::getSessionVar('isAgent', false);;
    }
  
    public function getIsABAManager() {
      return Utils::getSessionVar('isAbaManager', false);;
    }
  
    public function getIsPartner() {
      return Utils::getSessionVar('isPartner', false);;
    }
  
    protected function afterLogin($fromCookie) {
        parent::afterLogin($fromCookie);
        $this->loadSessionData( $this->id );
    }

    public function loadSessionData( $idUser ) {

        $criteria = new CDbCriteria();
        $criteria->addCondition('t.id = :idUser');
        $criteria->addCondition('t.isdeleted = :deleted');
        $criteria->params = array(':idUser' => $idUser, ':deleted' => 0);
        $mUser = User::model()->find($criteria);
        if( !$mUser ) return false;

        $userId = $mUser->id;
        $serName = $mUser->name;
        $userSurname = $mUser->surname;
        $userLanguage = $mUser->idLanguage;
        $userMail = $mUser->email;
        $userAvatar = $mUser->urlAvatar;
        $mLanguage = Language::model()->findByPk($userLanguage);
        $isoLanguage = $mLanguage->iso;
        $idUserCountry = Country::getIdCountryByIP(Yii::app()->request->getUserHostAddress());

        $idEnterprise = 0;
        $idRole = 0;

        $connection = Yii::app()->dbExtranet;
        $sql = "select e.id as identerprise, e.name as enterpriseName, c.id as idcountry, r.id as idrole, cu.id as idCurrency, cu.decimalPoint, cu.symbol, le.id as idLanguageEnterprise, eur.id as idEnterpriseUserRole, e.idpartner, le.iso as isoLanguageEnterprise, r.name as roleName
                from enterprise_user_role eur, role r, enterprise e, aba_b2c.country c, aba_b2c.currencies cu, language le
                where eur.idUser = {$idUser}
                  and eur.idRole = r.id
                  and eur.idEnterprise = e.id
                  and e.idLanguage = le.id
                  and e.idCountry = c.id
                  and c.ABAIdCurrency = cu.id

                  and eur.isdeleted = 0
                  and r.isdeleted = 0
                  and e.isdeleted = 0
                ";
        $command = $connection->createCommand($sql);
        Yii::trace($command->getText(), "sql.select");
        try {
            $rows=$command->queryAll();

            if ( !empty($rows) && count($rows) == 1 && $rows[0]['idrole'] == Role::_RESPONSIBLE ) { //
                $row = $rows[0];
                $idEnterprise = $row['identerprise'];
                $idRole = (int) $row['idrole'];
            }

            foreach( $rows as $row ) {
                if ( $row['idrole'] == Role::_ABAMANAGER ) Utils::setSessionVar('isAbaManager', true);
                else if ( $row['idrole'] == Role::_AGENT ) Utils::setSessionVar('isAgent', true);
                else if ( $row['idrole'] == Role::_PARTNER ) Utils::setSessionVar('isPartner', true);
            }

          if ( Yii::app()->user->isAbaManager ) $idRole = Role::_ABAMANAGER;
          else if ( Yii::app()->user->isAgent ) $idRole = Role::_AGENT;
          else if ( Yii::app()->user->isPartner ) $idRole = Role::_PARTNER;
          else $idRole = Role::_RESPONSIBLE;

        } catch (Exception $ex) {
            return false;
        }

        Utils::resetSessionData();
        Utils::setSessionVar('id', $userId);
        Utils::setSessionVar('name', $serName);
        Utils::setSessionVar('surname', $userSurname);
        Utils::setSessionVar('idLanguage', $userLanguage);
        Utils::setSessionVar('userLanguage', $isoLanguage);
        Utils::setSessionVar('idUserCountry', $idUserCountry);
        Utils::setSessionVar('userMail', $userMail);
        Utils::setSessionVar('userAvatar', $userAvatar);

        Utils::setSessionVar('idRole', $idRole);
        Utils::setSessionVar('role', $idRole);

        Utils::setSessionVar('idEnterprise', $idEnterprise);
        if ( !empty($idEnterprise) ) Utils::change2Enterprise( $idEnterprise );

        return true;
    }

}