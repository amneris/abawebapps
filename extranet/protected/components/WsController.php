<?php
/**
 * 
 */
class WsController extends CController implements IWebServiceProvider
{
    protected static $aErrorCodes = array(
        "555"=> "Unidentified error",
        "501"=> "Invalid Parameter Value",
        "502"=> "Invalid Signature",
        "503"=> "Operation not completed",
        // Detailed and customized errors, referenced from outside this class:
        "504"=> "User does not exist",
        "505"=> "Selligent communications error",
        "506"=> "User could not be created",
        "507"=> "User email is empty",
        "508"=> "Product not valid",
        "509"=> "Voucher is not valid. Probably it does not exist, or maybe it applies to another product",
        "510"=> "User registered, but error on GROUPON CODE update.",
        "511"=> "User could not be updated",
        "512"=> "Promocode not valid",
        "513"=> "Country Id not valid. Check its value on your call please.",
        "514"=> "Voucher already used. The code probably has been validated: ",
        "515"=> "Payment not valid.",
        "516"=> "Payment gateway did not allow transaction.",
        "517"=> "Payment is too old to be refund. Ask IT.",
        "518"=> "Change of credit form in subscription not allowed. ",
        "519"=> "User already exists and is Premium",
        "520"=> "teacher email not valid",
    );

    /* @var LogWebService $moLogWs */
    protected $moLogWs;
    protected $nameService;
    protected $aResponse;
    protected $isSuccess;


    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
        $this->nameService = null;
        Yii::app()->params["isTransRunning"] = false;

    }

    /**
     * @param CWebService $service
     *
     * @return bool
     */
    public function beforeWebMethod($service)
    {
        $this->logStart( $service);
        return true;
    }

    /**
     * @param CWebService $service
     *
     * @return bool
     */
    public function afterWebMethod($service)
    {
        $this->logEnd( $this->aResponse, $service);
        return true;
    }

    /**
     * @param string $signature
     * @param array $params
     *
     * @return bool
     */
    protected function isValidSignature( $signature, $params )
    {
//        if( substr($signature,-3) == "123"  && Yii::app()->config->get("SELF_DOMAIN")!==DOMAIN_LIVE )
//        {
//            return true ;
//        }
        // We have to a check on signature. For example a hash with all arguments
        if( $signature === $this->getSignature($params) ) return true;

        return false;
    }

    /**
     * @param string $signature
     * @param array $params
     *
     * @return bool
     */
    protected function isValidSignatureAffiliate( $signature, $params )
    {
//        if( substr($signature,-3) == "123"  && Yii::app()->config->get("SELF_DOMAIN")!==DOMAIN_LIVE )
//        {
//            return true ;
//        }
        // We have to a check on signature. For example a hash with all arguments
        if( $signature === $this->getSignatureAffiliate($params) ) return true;
        
        return false;
    }

    /**
     * @param $params
     *
     * @return string
     */
    protected function getSignature( $params )
    {
      return Utils::getWsSignature($params);
    }

    /**
     * @param $params
     *
     * @return string
     */
    protected function getSignatureAffiliate( $params )
    {
      return Utils::getWsSignature($params, 'partners');
    }


    protected function retErrorWs( $code=null, $additionalErrorMsg="" )
    {
        if(!isset( $code ) )
        {
            $code = "555"; // generic error code
        }

        $aResponse = array( $code => self::$aErrorCodes[$code]." - ".$additionalErrorMsg );

        $this->aResponse = $aResponse;
        $this->isSuccess = 0;

        return $aResponse;
    }


    protected function retSuccessWs( $aResponse )
    {
        $this->aResponse = $aResponse;
        $this->isSuccess = 1;

        return $aResponse;
    }


    /**
     * @param      $aResponse
     * @param CWebService $service
     */
    private function logEnd($aResponse, $service = null)
    {
        if (Yii::app()->params['ENABLE_LOG_WEBSERVICES']) {
            if (!is_null($this->nameService) && get_class($this->moLogWs)=="LogWebService") {
                $this->moLogWs->dateResponse =  HeDate::todaySQL(true);
                $this->moLogWs->paramsResponse =  HeMixed::serializeToStoreInDb($aResponse);
                $this->moLogWs->successResponse = $this->isSuccess;
                if (!$this->moLogWs->updateLogEndRequest()) {
                    HeLogger::sendLog(
                        "Model Error SQL update LogWebservice",
                        HeLogger::IT_BUGS,
                        HeLogger::CRITICAL,
                        "Function moLogWs->updateEndRequest() has raised an error. Please review."
                    );
                }

            }
        }

    }

    /**
     * @param CWebService $service
     */
    private function logStart($service = null)
    {
        if (Yii::app()->params['ENABLE_LOG_WEBSERVICES']) {
            $this->moLogWs = new LogWebService();
            $this->moLogWs->getLogById();
            $this->moLogWs->dateRequest = HeDate::todaySQL(true);
            $this->moLogWs->ipFromRequest = Yii::app()->request->getUserHostAddress();
            $this->moLogWs->appNameFromRequest = HeMixed::getNameKnownHost(Yii::app()->request->getUserHostAddress());
            $this->moLogWs->urlRequest = Yii::app()->request->getHostInfo().Yii::app()->request->getUrl() ;
            $postdata = file_get_contents("php://input");
            $this->moLogWs->paramsRequest = $postdata;
            if (!is_null($service)) {
                $nameService = $service->getMethodName();
            } else {
                $nameService = $this->retServiceName();
            }
            $this->nameService = $nameService;
            $this->moLogWs->nameService = $this->nameService;
            $this->moLogWs->successResponse = false;
            $this->moLogWs->insertLog();
        }
    }

    /**
     * It extracts the name of the web service from one of the params of CGI variables
     * @return string
     */
    private function retServiceName()
    {
        $nameService = $_SERVER["HTTP_SOAPACTION"];
        if (strpos($nameService, "#")) {
            $aNameService = explode("#", $_SERVER["HTTP_SOAPACTION"]);
            $nameService = str_replace('"', "", $aNameService[1]);
        }
        return $nameService;
    }
}