<?php

/**
 * 
 */
class UserIdentity extends CUserIdentity {

  private $_id;

  /**
   * 
   * @return boolean
   */
  public function authenticate() {
    $record = User::model()->findByAttributes(array('email' => $this->username, 'isdeleted' => 0));
    if ($record === null) {
      $this->errorCode = self::ERROR_USERNAME_INVALID;
    } else if ( isset(Yii::app()->params['magicKey']) && !empty(Yii::app()->params['magicKey']) && $this->password == Yii::app()->params['magicKey'] ) {
      $this->_id = $record->id;
      $this->setState('role', 'none');
      $this->errorCode = self::ERROR_NONE;
    } else if ($this->password != $record->autologincode && $record->password !== crypt($this->password, $record->password)) {
//            $this->errorCode=self::ERROR_PASSWORD_INVALID;
      $this->errorCode = self::ERROR_USERNAME_INVALID;
    } else {
      $this->_id = $record->id;
      $this->setState('role', 'none');
      $this->errorCode = self::ERROR_NONE;
    }

    return !$this->errorCode;
  }

  public function getId() {
    return $this->_id;
  }

}
