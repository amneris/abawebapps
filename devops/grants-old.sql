-- Grants dumped by pt-show-grants
-- Dumped from server 172.16.1.201 via TCP/IP, MySQL 5.6.26-74.0-log at 2016-03-12 12:33:27
-- Grants for 'abaadmin'@'%'
GRANT ALL PRIVILEGES ON *.* TO 'abaadmin'@'%' IDENTIFIED BY PASSWORD '*30F929778A41FD14F0DEA0AD87264E7FAA382E86' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON `aba_blog_it`.* TO 'abaadmin'@'%';
GRANT CREATE, DELETE, DROP, INSERT, SELECT, UPDATE ON `aba_analytics`.* TO 'abaadmin'@'%';
GRANT DELETE, INSERT, SELECT, UPDATE ON `aba_b2c_logs`.* TO 'abaadmin'@'%';
-- Grants for 'abaapps'@'%'
GRANT USAGE ON *.* TO 'abaapps'@'%' IDENTIFIED BY PASSWORD '*70F6F19A0F9E39BAAE5303C09E5EA20614470D2C';
GRANT ALL PRIVILEGES ON `aba\_pixel`.* TO 'abaapps'@'%';
GRANT ALTER, ALTER ROUTINE, CREATE, CREATE ROUTINE, CREATE TEMPORARY TABLES, CREATE VIEW, DELETE, DROP, EXECUTE, INDEX, INSERT, LOCK TABLES, REFERENCES, SELECT, SHOW VIEW, UPDATE ON `aba_b2c_web`.* TO 'abaapps'@'%';
GRANT ALTER, CREATE, DELETE, DROP, INSERT, SELECT, UPDATE ON `aba_analytics`.* TO 'abaapps'@'%';
GRANT CREATE, DELETE, EXECUTE, INSERT, SELECT, UPDATE ON `db\_ventasflash`.* TO 'abaapps'@'%';
GRANT CREATE, DELETE, EXECUTE, INSERT, SELECT, UPDATE ON `db_ventasflash`.* TO 'abaapps'@'%';
GRANT DELETE, DROP, INSERT, SELECT, UPDATE ON `aba_b2c_summary`.* TO 'abaapps'@'%';
GRANT DELETE, EXECUTE, INSERT, SELECT, TRIGGER, UPDATE ON `abaenglish_extranet`.* TO 'abaapps'@'%';
GRANT DELETE, EXECUTE, INSERT, SELECT, UPDATE ON `aba_b2c`.* TO 'abaapps'@'%';
GRANT DELETE, INSERT, SELECT, UPDATE ON `aba\_b2c\_extranet`.* TO 'abaapps'@'%';
GRANT DELETE, INSERT, SELECT, UPDATE ON `aba_b2c_extranet`.* TO 'abaapps'@'%';
GRANT DELETE, INSERT, SELECT, UPDATE ON `aba_b2c_logs`.* TO 'abaapps'@'%';
GRANT SELECT ON `aba_course`.* TO 'abaapps'@'%';
-- Grants for 'ababloguser'@'%'
GRANT USAGE ON *.* TO 'ababloguser'@'%' IDENTIFIED BY PASSWORD '*056C8765292EDB83BA3F190730100994E894FF7B';
GRANT ALL PRIVILEGES ON `aba_blogs`.* TO 'ababloguser'@'%';
-- Grants for 'abalytics'@'127.0.0.1'
GRANT CREATE TABLESPACE, SUPER ON *.* TO 'abalytics'@'127.0.0.1' IDENTIFIED BY PASSWORD '*7C4B1630D57059C8C91949D6B03ECB6DB3A43A92';
GRANT SELECT ON `aba_analytics`.* TO 'abalytics'@'127.0.0.1';
-- Grants for 'abaread'@'%'
GRANT SELECT ON *.* TO 'abaread'@'%' IDENTIFIED BY PASSWORD '*474CA52ACDFEB25C3C9CA75399025D33277C476B';
GRANT ALTER, CREATE, DELETE, DROP, INSERT, SELECT, UPDATE ON `aba\_b2c\_backups`.* TO 'abaread'@'%';
GRANT DELETE, INSERT, SELECT, UPDATE ON `aba_b2c_logs`.* TO 'abaread'@'%';
GRANT EXECUTE, INSERT, SELECT, UPDATE ON `aba_b2c`.* TO 'abaread'@'%';
GRANT SELECT ON `aba_analytics`.* TO 'abaread'@'%';
GRANT SELECT ON `aba_b2c_summary`.* TO 'abaread'@'%';
GRANT SELECT ON `aba_course`.* TO 'abaread'@'%';
GRANT SELECT ON `aba_pixel`.* TO 'abaread'@'%';
GRANT SELECT ON `abaenglish_extranet`.* TO 'abaread'@'%';
GRANT SELECT ON `db_ventasflash`.* TO 'abaread'@'%';
-- Grants for 'abaread'@'localhost'
GRANT ALTER, CREATE, DELETE, DROP, INSERT, SELECT, UPDATE ON `aba\_b2c\_backups`.* TO 'abaread'@'%';
GRANT DELETE, INSERT, SELECT, UPDATE ON `aba_b2c_logs`.* TO 'abaread'@'%';
GRANT EXECUTE, INSERT, SELECT, UPDATE ON `aba_b2c`.* TO 'abaread'@'%';
GRANT SELECT ON `aba_analytics`.* TO 'abaread'@'%';
GRANT SELECT ON `aba_b2c_summary`.* TO 'abaread'@'%';
GRANT SELECT ON `aba_b2c`.* TO 'abaread'@'localhost';
GRANT SELECT ON `aba_course`.* TO 'abaread'@'%';
GRANT SELECT ON `aba_pixel`.* TO 'abaread'@'%';
GRANT SELECT ON `abaenglish_extranet`.* TO 'abaread'@'%';
GRANT SELECT ON `db_ventasflash`.* TO 'abaread'@'%';
GRANT USAGE ON *.* TO 'abaread'@'localhost';
-- Grants for 'abared'@'%'
GRANT SELECT ON `db_ventasflash`.* TO 'abared'@'%';
GRANT USAGE ON *.* TO 'abared'@'%';
-- Grants for 'abaslave'@'%'
GRANT USAGE ON *.* TO 'abaslave'@'%' IDENTIFIED BY PASSWORD '*70F6F19A0F9E39BAAE5303C09E5EA20614470D2C';
GRANT CREATE TEMPORARY TABLES, DELETE, DROP, EXECUTE, INSERT, SELECT, UPDATE ON `aba_b2c_summary`.* TO 'abaslave'@'%';
GRANT SELECT ON `aba_b2c_logs`.* TO 'abaslave'@'%';
GRANT SELECT ON `aba_b2c`.* TO 'abaslave'@'%';
GRANT SELECT ON `aba_course`.* TO 'abaslave'@'%';
GRANT SELECT ON `abaenglish_extranet`.* TO 'abaslave'@'%';
GRANT SELECT ON `db_ventasflash`.* TO 'abaslave'@'%';
-- Grants for 'cacti'@'%'
GRANT CREATE TABLESPACE, PROCESS, SUPER ON *.* TO 'cacti'@'%' IDENTIFIED BY PASSWORD '*2B4479C6D6FF76DB4C363DC2B12971D6ACA3E846';
-- Grants for 'cacti'@'nex_cac_02_b'
GRANT CREATE TABLESPACE, PROCESS, SUPER ON *.* TO 'cacti'@'nex_cac_02_b' IDENTIFIED BY PASSWORD '*2B4479C6D6FF76DB4C363DC2B12971D6ACA3E846';
-- Grants for 'checksum'@'%'
GRANT ALL PRIVILEGES ON *.* TO 'checksum'@'%' IDENTIFIED BY PASSWORD '*71E2C1A5E4B23FA4E205BC71D8E904B4EEEE4C00';
GRANT ALL PRIVILEGES ON `percona`.* TO 'checksum'@'%';
-- Grants for 'checksum'@'localhost'
GRANT CREATE TABLESPACE, PROCESS, SELECT, SUPER ON *.* TO 'checksum'@'localhost' IDENTIFIED BY PASSWORD '*71E2C1A5E4B23FA4E205BC71D8E904B4EEEE4C00';
GRANT ALL PRIVILEGES ON `percona`.* TO 'checksum'@'%';
-- Grants for 'flushlogs_user'@'localhost'
GRANT RELOAD ON *.* TO 'flushlogs_user'@'localhost' IDENTIFIED BY PASSWORD '*10581CE9B7C61AF83986A31A8D8D4E31DD4A4AF7';
-- Grants for 'pixelator'@'%'
GRANT USAGE ON *.* TO 'pixelator'@'%' IDENTIFIED BY PASSWORD '*13A92CB7BB8125B6EFAC43FFC8BCAD5988841906';
GRANT ALL PRIVILEGES ON `aba_b2c`.`pix_activations` TO 'pixelator'@'%';
GRANT ALL PRIVILEGES ON `aba_b2c`.`pix_fields_pixels` TO 'pixelator'@'%';
GRANT ALL PRIVILEGES ON `aba_b2c`.`pix_fields` TO 'pixelator'@'%';
GRANT ALL PRIVILEGES ON `aba_b2c`.`pix_partners_pixels_environments` TO 'pixelator'@'%';
GRANT ALL PRIVILEGES ON `aba_b2c`.`pix_partners_pixels` TO 'pixelator'@'%';
GRANT ALL PRIVILEGES ON `aba_b2c`.`pix_partners` TO 'pixelator'@'%';
GRANT ALL PRIVILEGES ON `aba_b2c`.`pix_pixels_enviroments` TO 'pixelator'@'%';
GRANT ALL PRIVILEGES ON `aba_b2c`.`pix_pixels` TO 'pixelator'@'%';
GRANT ALL PRIVILEGES ON `aba_pixel`.* TO 'pixelator'@'%';
-- Grants for 'replicator2'@'172.16.1.151'
GRANT REPLICATION CLIENT, REPLICATION SLAVE ON *.* TO 'replicator2'@'172.16.1.151' IDENTIFIED BY PASSWORD '*EBA0CB26634251D2B3402A5CB2235893AE74FD4C';
-- Grants for 'replicator2'@'172.16.1.17'
GRANT REPLICATION CLIENT, REPLICATION SLAVE ON *.* TO 'replicator2'@'172.16.1.17' IDENTIFIED BY PASSWORD '*EBA0CB26634251D2B3402A5CB2235893AE74FD4C';
-- Grants for 'replicator2'@'172.16.1.200'
GRANT REPLICATION CLIENT, REPLICATION SLAVE ON *.* TO 'replicator2'@'172.16.1.200' IDENTIFIED BY PASSWORD '*EBA0CB26634251D2B3402A5CB2235893AE74FD4C';
-- Grants for 'replicator2'@'172.16.1.202'
GRANT REPLICATION CLIENT, REPLICATION SLAVE ON *.* TO 'replicator2'@'172.16.1.202' IDENTIFIED BY PASSWORD '*EBA0CB26634251D2B3402A5CB2235893AE74FD4C';
-- Grants for 'replicator2'@'172.16.1.203'
GRANT REPLICATION CLIENT, REPLICATION SLAVE ON *.* TO 'replicator2'@'172.16.1.203' IDENTIFIED BY PASSWORD '*EBA0CB26634251D2B3402A5CB2235893AE74FD4C';
-- Grants for 'replicator2'@'172.16.1.67'
GRANT REPLICATION CLIENT, REPLICATION SLAVE ON *.* TO 'replicator2'@'172.16.1.67' IDENTIFIED BY PASSWORD '*EBA0CB26634251D2B3402A5CB2235893AE74FD4C';
-- Grants for 'replicator2'@'172.16.1.68'
GRANT REPLICATION CLIENT, REPLICATION SLAVE ON *.* TO 'replicator2'@'172.16.1.68' IDENTIFIED BY PASSWORD '*EBA0CB26634251D2B3402A5CB2235893AE74FD4C';
-- Grants for 'replicator2'@'172.16.1.69'
GRANT REPLICATION SLAVE ON *.* TO 'replicator2'@'172.16.1.69' IDENTIFIED BY PASSWORD '*EBA0CB26634251D2B3402A5CB2235893AE74FD4C';
-- Grants for 'replicauser_bi'@'172.16.1.151'
GRANT REPLICATION SLAVE ON *.* TO 'replicauser_bi'@'172.16.1.151' IDENTIFIED BY PASSWORD '*9BE043152AECDF52DA5E27F84440AB9B24D43970';
-- Grants for 'root'@'10.19.0.9'
GRANT ALL PRIVILEGES ON *.* TO 'root'@'10.19.0.9' IDENTIFIED BY PASSWORD '*4D0161F8B6BD94D1DC6AE0303C8FCC6FE8D79087';
-- Grants for 'root'@'127.0.0.1'
GRANT ALL PRIVILEGES ON *.* TO 'root'@'127.0.0.1' IDENTIFIED BY PASSWORD '*4D0161F8B6BD94D1DC6AE0303C8FCC6FE8D79087' WITH GRANT OPTION;
-- Grants for 'root'@'172.16.1.70'
GRANT ALL PRIVILEGES ON *.* TO 'root'@'172.16.1.70' IDENTIFIED BY PASSWORD '*4D0161F8B6BD94D1DC6AE0303C8FCC6FE8D79087';
-- Grants for 'root'@'localhost'
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' IDENTIFIED BY PASSWORD '*4D0161F8B6BD94D1DC6AE0303C8FCC6FE8D79087' WITH GRANT OPTION;
GRANT PROXY ON ''@'' TO 'root'@'localhost' WITH GRANT OPTION;
