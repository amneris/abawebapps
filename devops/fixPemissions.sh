#!/usr/bin/env bash

find . -name assets -type d | xargs chmod 777
find . -name runtime -type d | xargs chmod 777
find . -name certs -type d | xargs chmod 777

chmod 777 campus/protected/extensions/mpdf/tmp
