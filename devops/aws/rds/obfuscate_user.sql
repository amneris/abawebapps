-- --------------------------------------------------------
-- Host:                         dbint.cfbep2aydaua.eu-west-1.rds.amazonaws.com
-- Versi�n del servidor:         5.6.22-log - MySQL Community Server (GPL)
-- SO del servidor:              Linux
-- HeidiSQL Versi�n:             9.2.0.4981
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para procedimiento aba_b2c.obfuscate_user
DELIMITER //
CREATE DEFINER=`abaadmin`@`%` PROCEDURE `obfuscate_user`()
BEGIN

	UPDATE `aba_b2c`.`user` SET email= CONCAT('student+', id,'@abaenglish.com'), name='Student', surnames=id, PASSWORD='e7ee2a8281efc413f5789e4409e2bc8c',expirationDate ='2020-09-25 02:00:02', birthDate='1981-03-02 00:00:01', telephone='555-1234' WHERE expirationDate <>'2020-09-25 02:00:02';
	UPDATE `aba_b2c`.`user_credit_forms` SET `cardNumber`= concat('cardNumber',`userId`) , `cardYear`='1970', `cardMonth`='09', `cardCvc`='025', `cardName`= `userId`;
	UPDATE `aba_b2c`.`payments_control_check` SET `cardNumber`= concat('cardNumber',`userId`) , `cardYear`='1970', `cardMonth`='09', `cardCvc`='025', `cardName`= `userId`;

END//
DELIMITER ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

