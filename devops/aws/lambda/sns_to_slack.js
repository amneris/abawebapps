var https = require('https');
var util = require('util');

exports.handler = function (event, context) {
//    console.log(JSON.stringify(event, null, 2));
    console.log('From SNS:', event.Records[0].Sns.Message);

    var postData = {
        "channel": "#deploy",
        "username": "aws",
        "text": event.Records[0].Sns.Subject,
        "icon_emoji": ":aws:"
    };

    var message = event.Records[0].Sns.Message;

    try {
        var maybeAnAlert = JSON.parse(message);
        if (maybeAnAlert && (maybeAnAlert.AlarmName !== undefined)) {
            // this script is not for CloudWatch alerts!!
            context.done(null);
            return;
        }
    } catch (e) {
        console.error(e , message);
    }

    var butWithErrors = message.indexOf("but with errors");
    var stateRed = message.indexOf("to RED");
    var stateYellow = message.indexOf("to YELLOW");
    var noPermission = message.indexOf("You do not have permission");
    var failedDeploy = message.indexOf("Failed to deploy application");
    var removedInstance = message.indexOf("Removed instance");
    var addingInstance = message.indexOf("Adding instance");
    var failed = message.indexOf("failed") != -1;

    var color = "good";

    if (stateRed != -1 || butWithErrors != -1 || noPermission != -1 || failedDeploy != -1 || failed) {
        color = "danger";
    }
    if (stateYellow != -1 || removedInstance != -1 || addingInstance != -1) {
        color = "warning";
    }

    var fields = [];
    var fieldsJSon = {};
    var newApplicationDeployed = false;
    var lines = message.split("\n");
    for (var iterator in lines) {
        var field = {};
        var index = lines[iterator].indexOf(":");
        field.title = lines[iterator].substr(0, index).trim();
        field.value = lines[iterator].substr(index + 1).trim();
        field.short = field.value.trim().length < 50;
        fieldsJSon[field.title.toLowerCase()] = field.value;
        if (field.title !== "Message") {
            if (field.title.length > 0) fields.push(field);
        } else {
            fields.unshift(field);
            newApplicationDeployed = (field.value === 'New application version was deployed to running EC2 instances.');
        }
    }

    if (newApplicationDeployed) {
        postData.text = '';
        postData.attachments = [
            {
                "color": color,
                "text": util.format("A new version of *%s* was deployed to <%s|%s>", fieldsJSon['application'], fieldsJSon['environment url'], fieldsJSon['environment']),
                "mrkdwn_in": ["text", "pretext"]
            }
        ];

    } else {
        postData.attachments = [
            {
                "color": color,
                "fields": fields
            }
        ];
    }
    var jsonString = util.format("%j", postData);

    var options = {
        method: 'POST',
        hostname: 'hooks.slack.com',
        port: 443,
        path: '/services/T04GQSFE3/B076U10LT/aRrKwbqXAgT80lqdqzmImrax',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': jsonString.length
        }
    };

    var req = https.request(options, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            context.done(null);
        });
    });

    req.on('error', function (e) {
        console.log('problem with request: ' + e.message);
    });

    req.end(jsonString);
};