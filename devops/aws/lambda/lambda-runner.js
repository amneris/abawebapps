#!/usr/bin/env node

var filename = 'sns_to_slack.js';
var handlername = 'handler';

var event = require(process.cwd() + '/event.json');
var handler = require(process.cwd() + '/' + filename)[handlername];

var context = {
    succeed: function (result) {
        console.log('succeed: ' + result);
        process.exit(0);
    },
    fail: function (error) {
        console.log('fail: ' + error);
        process.exit(-1);
    },
    done: function () {
        console.log("DONE!");
        process.exit(0);
    }
};

handler(event, context);