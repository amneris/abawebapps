
CREATE DATABASE percona;
GRANT SELECT, SUPER, PROCESS, REPLICATION SLAVE ON *.* TO 'checksum'@'%' IDENTIFIED BY 'TJxozD4gieHkiMMo';
GRANT SELECT, SUPER, PROCESS ON *.* TO 'checksum'@'localhost' IDENTIFIED BY 'TJxozD4gieHkiMMo';
GRANT ALL ON percona.* TO 'checksum'@'%';

