#!/usr/bin/env bash

# list tables to ignore:
# select `table_name` from information_schema.tables where table_schema = 'aba_b2c' and `table_name` like 'followup%';

# Create a replicate dump remotely from master with : mysqldump -C -h 172.16.1.20 --master-data
#
# the --master-data option which automatically appends the CHANGE MASTER TO statement required on the slave to start
# the replication process:
# The --master-data option automatically turns off --lock-tables. It also turns on --lock-all-tables,
# unless --single-transaction also is specified, in which case, a global read lock is acquired only for a short time
# at the beginning of the dump (see the description for --single-transaction). In all cases, any action on logs happens
# at the exact moment of the dump.
#
# It is also possible to set up a slave by dumping an existing slave of the master
# https://dev.mysql.com/doc/refman/5.0/en/mysqldump.html#option_mysqldump_master-data
#
# this is the command but read the documentation first there is previous steps in the procedure.
# remotely from slave  : mysqldump -C -h 172.16.1.68 --master-data=2

mysqldump -C -h 172.16.1.20 --master-data -uabaadmin -p --quick --single-transaction --hex-blob  --result-file=mydbr_dump.sql  --verbose --ignore-table=aba_b2c.followup4 --ignore-table=aba_b2c.followup4_1 --ignore-table=aba_b2c.followup4_10 --ignore-table=aba_b2c.followup4_11  --ignore-table=aba_b2c.followup4_12 --ignore-table=aba_b2c.followup4_13 --ignore-table=aba_b2c.followup4_14 --ignore-table=aba_b2c.followup4_15 --ignore-table=aba_b2c.followup4_16  --ignore-table=aba_b2c.followup4_17 --ignore-table=aba_b2c.followup4_18 --ignore-table=aba_b2c.followup4_19 --ignore-table=aba_b2c.followup4_2 --ignore-table=aba_b2c.followup4_20  --ignore-table=aba_b2c.followup4_21  --ignore-table=aba_b2c.followup4_22  --ignore-table=aba_b2c.followup4_23  --ignore-table=aba_b2c.followup4_24  --ignore-table=aba_b2c.followup4_25  --ignore-table=aba_b2c.followup4_26  --ignore-table=aba_b2c.followup4_27  --ignore-table=aba_b2c.followup4_28  --ignore-table=aba_b2c.followup4_29  --ignore-table=aba_b2c.followup4_3  --ignore-table=aba_b2c.followup4_30  --ignore-table=aba_b2c.followup4_31  --ignore-table=aba_b2c.followup4_32  --ignore-table=aba_b2c.followup4_33  --ignore-table=aba_b2c.followup4_34  --ignore-table=aba_b2c.followup4_35  --ignore-table=aba_b2c.followup4_36  --ignore-table=aba_b2c.followup4_37  --ignore-table=aba_b2c.followup4_38  --ignore-table=aba_b2c.followup4_39  --ignore-table=aba_b2c.followup4_4  --ignore-table=aba_b2c.followup4_40  --ignore-table=aba_b2c.followup4_5  --ignore-table=aba_b2c.followup4_6  --ignore-table=aba_b2c.followup4_7  --ignore-table=aba_b2c.followup4_8  --ignore-table=aba_b2c.followup4_9  --ignore-table=aba_b2c.followup_html4  --ignore-table=aba_b2c.followup_html4_1  --ignore-table=aba_b2c.followup_html4_2  --ignore-table=aba_b2c.followup_html4_3  --ignore-table=aba_b2c.followup_html4_30  --ignore-table=aba_b2c.followup_html4_31  --ignore-table=aba_b2c.followup_html4_32  --ignore-table=aba_b2c.followup_html4_33  --ignore-table=aba_b2c.followup_html4_34  --ignore-table=aba_b2c.followup_html4_35  --ignore-table=aba_b2c.followup_html4_4  --ignore-table=aba_b2c.followup_html4_5  --ignore-table=aba_b2c.followup_version --ignore-table=aba_b2c.messages --ignore-table=aba_b2c.YiiSession --ignore-table=aba_analytics.event --ignore-table=aba_analytics.event_session --databases aba_course aba_b2c aba_analytics aba_b2c_summary abaenglish_extranet percona
