SELECT f.themeid as 'Unit',  f.sit_por as 'ABAFilm',
        f.stu_por as 'Speak',
        f.dic_por as 'Write_ABA',
        f.rol_por as 'Interpret',
        f.gra_vid as 'VideoClass',
        f.wri_por as 'Exercises',
        f.new_por as 'Vocabulary',f.eva_por as 'Assessment',ROUND(FLOOR(
                    f.sit_por +
                    f.stu_por +
                    f.dic_por+
                    f.rol_por +
                    f.gra_vid +
                    f.wri_por +
                    f.new_por

                 )/7,2) as 'Total', i.id as 'iduser_web', IF( DATE(f.lastchange) = DATE('0000-00-00'), '', f.lastchange) as lastchange
        FROM aba_b2c.user i INNER JOIN aba_b2c_summary.followup4_summary f ON i.id = f.userid
        WHERE i.userType not in(0,2) and i.entryDate between DATE_SUB(NOW(),INTERVAL 4 MONTH) and NOW();
