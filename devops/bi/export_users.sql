select distinct u.userid iduser_web ,fecha_renovacion,renovaciones
from
    (
        select userid,max(date(datetopay)) as fecha_renovacion
        from aba_b2c.payments p where p.`idPeriodPay` = 30
        group by userid
        having max(date(datetopay))>date(now())
    ) u
    left join (select u.*, p.idPeriodPay product from aba_b2c.user u left join aba_b2c.payments p ON p.userId = u.id) aux
        on aux.id=u.userid
    left join (SELECT
        `p`.`userId` AS `userid`,count(`p`.`id`) AS `renovaciones`
        FROM (`aba_b2c`.`payments` `p` left join `aba_b2c`.`user` `u` on((`u`.`id` = `p`.`userId`))) where ((`p`.`status` = 30) and (`p`.`idPeriodPay` = 30) and (`p`.`paySuppExtId` in (1,10,11))) group by `p`.`userId` having ((count(`p`.`id`) < 3) and (count(`p`.`id`) >= 1))) d
        on d.userid=u.userid

where aux.product=30 and datediff(date(u.fecha_renovacion),date(now()))=7 and u.userid in (select userid from (SELECT
        `p`.`userId` AS `userid`,count(`p`.`id`) AS `renovaciones`
        FROM (`aba_b2c`.`payments` `p` left join `aba_b2c`.`user` `u` on((`u`.`id` = `p`.`userId`))) where ((`p`.`status` = 30) and (`p`.`idPeriodPay` = 30) and (`p`.`paySuppExtId` in (1,10,11))) group by `p`.`userId` having ((count(`p`.`id`) < 3) and (count(`p`.`id`) >= 1))) a );