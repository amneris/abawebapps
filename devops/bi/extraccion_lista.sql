
select distinct u.userid iduser_web, fecha_renovacion, renovaciones
from
    (
        select userid, p.`idPeriodPay`, max(date(datetopay)) as fecha_renovacion
        from aba_b2c.payments p where p.`idPeriodPay` = 180
        and `p`.`paySuppExtId` in (1,10,11)
        group by userid, p.`idPeriodPay`
        having max(date(datetopay))>date(now())
    ) u
	
    join (SELECT `p`.`userId` AS `userid`,count(`p`.`id`) AS `renovaciones`
				FROM `aba_b2c`.`payments` `p`
				where ((`p`.`status` = 30) and (`p`.`idPeriodPay` = 180) and (`p`.`paySuppExtId` in (1,10,11))) 
				group by `p`.`userId` having ((count(`p`.`id`) < 9) and (count(`p`.`id`) >= 1))) d
    on d.userid=u.userid
where datediff(date(u.fecha_renovacion),date(now()))=30;
