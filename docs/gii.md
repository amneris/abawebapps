Como acceder a Gii.

Gii es el generador de modelos, cruds, modulos de Yii.  Para poder acceder a él iremos al fichero:

/abawebapps/extranet/protected/config/base.php

Modificaremos el modulo gii, y añadiremos un valor mas en ipFilters:

'ipFilters' => array('127.0.0.1', '::1', '*.*.*.*’),

Una vez tengamos modificado el archivo solo faltara acceder por URL :
http://extranet.aba.local/gii/
Nos pedirá una pass para acceder, la pass está puesta en el documento que hemos modificado.
Podemos acceder a cualquier BBDD que tengamos configurado en el database de common.