### General
1. Install PEAR at your system
2. Install Composer
3. Install node and npm
4. Install Ruby

### PHP
#### Pre validation
PHPStorm -> Choose settings -> Editor -> Code style -> PHP -> Set from...(top right corner) -> Predefined style -> PSR1/PSR2

#### Post validation

##### 1) PHP Code Sniffer
1. `pear install PHP_CodeSniffer`
2. PHPStorm -> Choose settings -> Languages and Frameworks -> PHP -> Code Sniffer -> phpcs path -> Mostly always inside `pear` folder or `pear/bin` -> phpcs or phpcs.bat(Windows) -> Validate
3. PHPStorm -> Choose settings -> Editor -> Inspections -> PHP -> check PHP Code Sniffer Validation -> Coding standart -> PSR2(if empty - click reload button)

##### 2) PHP Mess Detector
1. `git clone git://github.com/phpmd/phpmd.git (in whatever location)`
2. `cd phpmd`
3. `composer install`
4.  PHPStorm -> Choose settings -> Languages and Frameworks -> PHP -> Mess Detector -> phpmd path -> <Your Path to php cloned repo>/bin/src/phpmd (or phpmd.bat for Windows) -> Validate
5.  PHPStorm -> Choose settings -> Editor -> Inspections -> PHP -> check PHP Mess Detector Validation -> Custom ruleset  -> <Path to our project>/linters/phpmd.xml


### JS/Coffee

1.  JSCS
  1.  npm install jscs -g
  2.  PHPStorm -> Choose settings -> Languages and Frameworks -> JavaScript -> Code Quality Tools -> JSCS -> Enable
  3.  (MAC) Node interpreter -> /usr/local/bin/node
  4.  (MAC) JSCS package -> /usr/local/lib/node_modules/jscs
  5.  Code style preset -> Airbnb

2.  CoffeeLint (npm)
  1.  `npm install -g coffeelint`
  2.  PHPStorm -> Choose settings -> Plugins -> Browse Repositories -> Find CoffeeLint -> Install
  3.  PHPStorm -> Choose settings -> Other Settings -> CoffeeLint -> Enable
  4.  (MAC) Node interpreter -> /usr/local/bin/node
  5.  (MAC) JSCS package -> /usr/local/lib/node_modules/coffeelint
  6.  CoffeeLint config file path -> use specific config file -> linters/coffeelint.json

3.  ES6 Linter
  1.  `npm install -g eslint`
  2.  PHPStorm -> Choose settings -> Languages and Frameworks -> JavaScript -> Code Quality Tools -> ESLint -> Enable
  3.  (MAC) Node interpreter -> /usr/local/bin/node
  4.  (MAC) JSCS package -> /usr/local/lib/node_modules/eslint
  5.  Config file -> linters/.eslintrc

### SASS
1.  SCSS Linter (Gem/Ruby)
  1.  `gem install scss_lint`
  2.  PHPStorm -> Choose settings -> Plugins -> Browse Repositories -> Find SCSS Lint -> Install
  3.  PHPStorm -> Choose settings -> Other Settings -> SCSS Lint -> Enable
  4.  (MAC) SCSS Lint exe -> /usr/bin/scss-lint
  5.  Use specific config file -> linters/scss-lint.yml
  6.  !!! Treat all scss-lint issues as warnings -> CHECK