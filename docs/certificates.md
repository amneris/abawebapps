
# Local & Development Wildcard SSL Certificates 

Como crear las wildcards. En un principio el desarrollador no hace falta que genere ningún tipo de wildcard porque ya están creadas en todos los proyectos. Solo tendrá que importar el CA que esta en la carpeta:
``` /abawebapps/devops/ssl/rootCA.crt ```

Clicamos el archivo y tenemos que confiar en este certificado. Lo tenemos que añadir en la sección System.

Dejo aqui la información de como generar las wildcards.
Create the Root Certificate (Done Once):

- Create the Root Key
	* Without Password
	
	``` openssl -config ~/work/SSL1/openssl.cnf genrsa -out rootCA.key 2048 ```
	
	* With Password
	
	``` openssl genrsa -des3 -out rootCA.key 2048 -Self-sign ``` 

this certificate
	
``` openssl req -x509 -new -nodes -key rootCA.key -days 3650 -out rootCA.pem -config ~/work/SSL1/openssl.cnf ``` 


In the Device:

- Create a private key and the certificate signing request

	* Both together
	
	``` openssl req -new -newkey rsa:2048 -nodes -out star_int_aba_land.csr -keyout star_int_aba_land.key -subj "/C=ES/ST=Catalunya/L=Barcelona/O=ABAEnglish/OU=IT/CN=*.int.aba.land” -config ~/work/SSL1/openssl.cnf ``` 

	* Separated
	
	``` 
	openssl genrsa -out star_int_aba_land.key 2048
	openssl req -new -sha256 -key star_int_aba_land.key -out star_int_aba_land.csr
	```  

``` 
openssl x509 -req -in star_int_aba_land.csr -CA rootCA.pem -CAkey rootCA.key -CAcreateserial -out star_int_aba_land.crt -days 1095
``` 
