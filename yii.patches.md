

- Fix for #3288: CTestCase expects PHPUnit to be included PEAR-style
    https://github.com/yiisoft/yii/issues/3288
    https://github.com/yiisoft/yii/commit/a0a0180869dba2b9f9232d2efcd646ae3b839d0f#diff-ca102d741a44ccd3d9289886cd396b25 