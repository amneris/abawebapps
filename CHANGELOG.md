<a name="4.202.2"></a>
## 4.202.2 (2017-04-26)

#### Hotfix

* Reset abamoments cookie when a user logs in

<a name="4.202.1"></a>
## 4.202.1 (2017-04-26)

#### Hotfix

* Cookie called abamoments that stores the aba moments progress endpoint response during 6 hours

<a name="4.202.0"></a>
## 4.202.0 (2017-04-25)

#### Features

* ABA MOMENTS

<a name="4.201.0"></a>
## 4.201.0 (2017-04-21)


#### Features

* **SPIDER-272**
  *  Fix if ([d3bd5dcf](https://github.com/abaenglish/abawebapps/commit/d3bd5dcf5c9169d1e449b853357e2778e4a14f1d))
  * Added 200 response for all perfect ([d4001b26](https://github.com/abaenglish/abawebapps/commit/d4001b26e5c509f29db42fe6c78d2df2ad438c00))
  * Added updaterenewalscancels endpoint ([28aa0217](https://github.com/abaenglish/abawebapps/commit/28aa02171f18d8fcf48e833b1b2f6bca2417649e))



<a name="4.200.0"></a>
## 4.200.0 (2017-04-10)


#### Features

* **TECH-167**
  *  new api controller ([067af5c2](https://github.com/abaenglish/abawebapps/commit/067af5c27a113ea8979f5fa43da063be7e07f9d9))
  *  new api controller ([b6f85736](https://github.com/abaenglish/abawebapps/commit/b6f85736b37a1d0b367d3a8965512a8a7a559b03))
  *  new api controller ([2fe8ff7f](https://github.com/abaenglish/abawebapps/commit/2fe8ff7fa40763a41bb54527d6e1d135e6d5ab02))



<a name="4.199.0"></a>
## 4.199.0 (2017-04-07)

* remove links to abaStatistics and abaStatisticsB2b
* remove Moodle models, views and controllers and access to DB_LEARNING
* fix the DB and ActiveRecord setup


<a name="4.198.0"></a>
## 4.198.0 (2017-03-29)


#### Bug Fixes

* **INO-171**
  *  mezclar aba-infra, config-manager y devops-k8s-deploy ([7efa967e](https://github.com/abaenglish/abawebapps/commit/7efa967e3884d82c53c96027972c5757f65cf740))
  *  mezclar aba-infra, config-manager y devops-k8s-deploy ([cddf8507](https://github.com/abaenglish/abawebapps/commit/cddf8507ce1b64ab784788f153ea28d63e878c39))
  *  mezclar aba-infra, config-manager y devops-k8s-deploy ([815f676a](https://github.com/abaenglish/abawebapps/commit/815f676a8108cbd26be78d9ad893948c463e383c))
  *  mezclar aba-infra, config-manager y devops-k8s-deploy ([1cf36cb0](https://github.com/abaenglish/abawebapps/commit/1cf36cb04464688bddc1078e6e19e4eb591fb654))

#### Features

* **ACQ-448**  :bomb: Migrating Alexa Certify tracker to GTM ([b9a924e5](https://github.com/abaenglish/abawebapps/commit/b9a924e5f9a38534813506dd15c42a458610beed))
* **INO-199**
  *   Crear un pipeline para el proyecto course para la frat de content-factory ([bd9f2d23](https://github.com/abaenglish/abawebapps/commit/bd9f2d23a582464bac0375e8e224d3d6e06c2927))
  *   Crear un pipeline para el proyecto course para la frat de content-factory ([d0750e23](https://github.com/abaenglish/abawebapps/commit/d0750e237ecdab40597fbeb61c43ac0efa2f4e2b))
  *   Crear un pipeline para el proyecto course para la frat de content-factory ([e84b221f](https://github.com/abaenglish/abawebapps/commit/e84b221fd591649a8343a15330332aa34bf6f2d2))
* **INO-208**  Create abawebapps dev environment in AWS ([848ccd36](https://github.com/abaenglish/abawebapps/commit/848ccd3652e19426f8b919cff446e8cf11bb226e))
* **SPIDER-196**
  *  User To Premium ([f602594b](https://github.com/abaenglish/abawebapps/commit/f602594bd341f45b060be3ca91736d8e75a5328d))
  *  User To Premium ([86669a30](https://github.com/abaenglish/abawebapps/commit/86669a308888cfec68d02e0455ff715c25919e8d))
* **SPIDER-197**
  * Changed currency send method 1 ([59ea595a](https://github.com/abaenglish/abawebapps/commit/59ea595a7a1f2573a18e7501901fe707c98b7bf8))
  * Changed currency send method ([2246c170](https://github.com/abaenglish/abawebapps/commit/2246c170ca59e5bd8b0da016d7b31242bcac5f4d))
  * redone send ([e940c99e](https://github.com/abaenglish/abawebapps/commit/e940c99e96a8ae87f6039778ffc04a7907e95bc2))
  * merge develop ([74190dac](https://github.com/abaenglish/abawebapps/commit/74190daca8d2d055ea7d0d59b78dc3ab821a6f52))
  * Added getCurrency ([a1b5e424](https://github.com/abaenglish/abawebapps/commit/a1b5e424831acf672413c737cf8422d1e6c07741))
  * First release ([7ad79841](https://github.com/abaenglish/abawebapps/commit/7ad79841b135d6128b724f373c0cf139d1ac98e0))
* **SPIDER-198**
  *  insert user data endpoint ([2fdb865c](https://github.com/abaenglish/abawebapps/commit/2fdb865c692c1f7eb60bd22d65a589ab99a7208d))
  *  insert user data endpoint ([44b9ec6b](https://github.com/abaenglish/abawebapps/commit/44b9ec6beb6013c58fe75fc30b7ff50c19bdf09f))
* **SPIDER-199**  change return messages ([798bd52c](https://github.com/abaenglish/abawebapps/commit/798bd52c59070aaafa6c092b8c0b2075b5e16bc0))



<a name="4.197.0"></a>
## 4.197.0 (2017-03-01)


#### Features

* **MIU-23**
  *  Wrong Information in Campus Subscription Summary ([98fb8fe2](https://github.com/abaenglish/abawebapps/commit/98fb8fe2cf2fffa7d957f3dbccd3c4cc479f18f3))
  *  Wrong Information in Campus Subscription Summary ([d5a6fc1b](https://github.com/abaenglish/abawebapps/commit/d5a6fc1bd46fa5898a1756037a85c86d1c4707a3))



<a name="4.197.0"></a>
## 4.197.0 (2017-03-01)


#### Features

* **MIU-23**
  *  Wrong Information in Campus Subscription Summary ([98fb8fe2](https://github.com/abaenglish/abawebapps/commit/98fb8fe2cf2fffa7d957f3dbccd3c4cc479f18f3))
  *  Wrong Information in Campus Subscription Summary ([d5a6fc1b](https://github.com/abaenglish/abawebapps/commit/d5a6fc1bd46fa5898a1756037a85c86d1c4707a3))



<a name="4.196.2"></a>
## 4.196.2 (2017-02-27)

#### Bug Fixes

* Solve problem send partner to Selligent

<a name="4.196.0"></a>
## 4.196.0 (2017-02-23)


#### Bug Fixes

* **INO-168**  revisar el pipeline en jenkins de abawebapps porque en QA aparece el error de composer ([3fd3cf1e](https://github.com/abaenglish/abawebapps/commit/3fd3cf1e436276c5deaa9eb20604435962407a9a))
* **composer**  post-build.sh call relies on hashbang ([58417a0a](https://github.com/abaenglish/abawebapps/commit/58417a0af430a30715343a080ab40fb59c947767))

#### Features

* **GOLDBUSTER-14**
  *  new version course ([b2da74e8](https://github.com/abaenglish/abawebapps/commit/b2da74e86162f37d9eceb1c87dd27b281ac8a30a))
  *  Hide popup memberGetMember ([194fb0cf](https://github.com/abaenglish/abawebapps/commit/194fb0cfa903d62e59b7a01864dbec58488475c3))



<a name="4.195.0"></a>
#### Features

* **GOLDBUSTER-14**  Hide popup memberGetMember ([194fb0cf](https://github.com/abaenglish/abawebapps/commit/194fb0cfa903d62e59b7a01864dbec58488475c3))


## 4.195.0 (2017-02-20)


#### Features

* **SPIDER-33**
  *  remove user progress tool ([9e2d0459](https://github.com/abaenglish/abawebapps/commit/9e2d0459f9adeae9fee19813bab1bd8ae6a117dc))
  *  remove user progress tool ([e43f0099](https://github.com/abaenglish/abawebapps/commit/e43f0099b47ab5eaf370756fcaa461c7aec88b25))



<a name="4.194.0"></a>
## 4.194.0 (2017-02-15)


#### Features

* **JMJ-1**
  *  Changed response in all paymentsZuora ([6219804e](https://github.com/abaenglish/abawebapps/commit/6219804e04adb0d8c6963c5ad9bbd60e9eabd96e))
  *  Changed response in actionRefundPaymentsZuora ([e950f1a1](https://github.com/abaenglish/abawebapps/commit/e950f1a151b49ab69b12601136c9307bbd99f29f))
* **MEAN-43**
  *  Send request selligent when create new partner or update in Intranet ([a6bea7b9](https://github.com/abaenglish/abawebapps/commit/a6bea7b93ac131edbe53b200ec6498cd042d77f5))
  *  Remove requests to selligent whent register user ([3a64e445](https://github.com/abaenglish/abawebapps/commit/3a64e445e01b445f32b67ab4e9bcca09448f067b))
  *  Added more conditionals for how obtain partner ([bd03bef4](https://github.com/abaenglish/abawebapps/commit/bd03bef41b154f032d3a17d401d27954fdeb7e2b))
  *  Send more information about partner in registation ([e6480958](https://github.com/abaenglish/abawebapps/commit/e64809583106660ff9ce691feda6690e4edf44f0))
* **ZOR-558**
  *  changed error text in refunds 2 ([ab07e60e](https://github.com/abaenglish/abawebapps/commit/ab07e60eef92de8c090698a3ce7c3f37e1f377df))
  *  changed error text in refunds ([4e4861f3](https://github.com/abaenglish/abawebapps/commit/4e4861f3f360b1915bb048cd8f2cf1b58af65446))
  *  Refactor code in refund Zuora ([08fd2e3c](https://github.com/abaenglish/abawebapps/commit/08fd2e3cd543e0c9dc90efe8b2f6f623051439d9))
  *  Check correct dates ([67700769](https://github.com/abaenglish/abawebapps/commit/67700769b842b5e0ecc1f8380642bc7e3713c4f8))
  *  Check some errors ([7182f31b](https://github.com/abaenglish/abawebapps/commit/7182f31b73c62338414db959b3158ff5b9b86861))
  *  Change error message try catch ([a9d8e9eb](https://github.com/abaenglish/abawebapps/commit/a9d8e9eb42666ce21728aa7441a95f8f7a148bfa))
  *  Check params in controller ([efa3412b](https://github.com/abaenglish/abawebapps/commit/efa3412b233d67b0e334e2d8fbd8b84db11704b6))
  *  Refactor code ([a64d665f](https://github.com/abaenglish/abawebapps/commit/a64d665fc5e8af63bc4f2665ac298d1b5e84266f))
  *  Remove some method in Renew payment Zuora ([81c50c68](https://github.com/abaenglish/abawebapps/commit/81c50c68a67afdc1b0dd24d17a49a591479c76e6))
  *  Created new endpoint renew payments zuora ([4bc86542](https://github.com/abaenglish/abawebapps/commit/4bc86542a21dc89a122f9ab53f47b9e6331c36ce))
  *  Rename variables cancellation and renew endpoints ([9141f0a3](https://github.com/abaenglish/abawebapps/commit/9141f0a3aaa4a3628feb587c7f1b776f880663d4))
  *  Change logic webservice renewsubscriptionzuora ([ba10eb7c](https://github.com/abaenglish/abawebapps/commit/ba10eb7c525e35765d4988f1565b85ee3031e93d))
  *  Change endpoint cancel subscription Zuora in abawebapps ([afd6eec9](https://github.com/abaenglish/abawebapps/commit/afd6eec91f4ebd9a6a2a526ecb08c1ae4fe7edac))
  *  change termEndDate by effectiveDate ([7b49ddf5](https://github.com/abaenglish/abawebapps/commit/7b49ddf531f1433c77eaa2340ef38a1e04d18b48))



<a name="4.193.1"></a>
## 4.193.1 (2017-02-08)


#### Features

* **INO-157**  :moyai: Añadir el script del plugin de "browser" de NewRelic ([35170b35](https://github.com/abaenglish/abawebapps/commit/35170b35e7e168116997cf32eb9d677845fdd46c))



<a name="4.193.0"></a>
## 4.193.0 (2017-02-06)


#### Features

* **ZOR-563**
  *  Created new endpoint obtain all payments by account id ([c2395f0e](https://github.com/abaenglish/abawebapps/commit/c2395f0ee49eadb91b4114e2c4d0ec814c2ac49d))
  *  Change response getUser webservice ([ce71ca8b](https://github.com/abaenglish/abawebapps/commit/ce71ca8b33cae73a990b34141d14cbd18db74408))
  *  Added info user ([64299435](https://github.com/abaenglish/abawebapps/commit/64299435422e912007ac481a4e981bd103a4cc6f))
  *  Created new endpoint get payments by subscription id ([ef956000](https://github.com/abaenglish/abawebapps/commit/ef9560008ad8f2de06a82dfbfc5bf594e28c6aaa))
  *  Better response in cancelled payments ([75899708](https://github.com/abaenglish/abawebapps/commit/75899708a834ba3a13a05cb99b1bd41720d4b50d))
  *  Obtain all payments cancelled in abawebapps by Zuora ([948df3fe](https://github.com/abaenglish/abawebapps/commit/948df3feb0008655e70e13a359eedab0d4179433))
  *  Created new endpoint show cancelled Zuora payments in abawebapps ([419f08f7](https://github.com/abaenglish/abawebapps/commit/419f08f7d8f3ee8ac9ab6c63cb1fb0cc4bedd5f6))



<a name="4.192.0"></a>
## 4.192.0 (2017-02-03)


#### Features

* **ZOR-588**
  *  Added more changes ([e10e169b](https://github.com/abaenglish/abawebapps/commit/e10e169b1b7ea7a9f4f38a1207e4be75a55ec67c))
  *  Cambio horario atencion Soporte web ([a8d194f1](https://github.com/abaenglish/abawebapps/commit/a8d194f118d1860155ee2a2b0a5cfd4322131877))



<a name="4.190.1"></a>
## 4.190.1 (2017-01-18)




<a name="4.190.0"></a>
## 4.190.0 (2017-01-12)


#### Features

* **ZOR-542**  Correct information Subscription MyAccount Zuora ([00e00c5c](https://github.com/abaenglish/abawebapps/commit/00e00c5c417a10c063a27560cf7aa8eb3c035756))



<a name="4.189.0"></a>
## 4.189.0 (2017-01-10)




<a name="4.188.0"></a>
## 4.188.0 (2017-01-02)




<a name="4.187.0"></a>
## 4.187.0 (2016-12-30)


#### Features

* **ZOR-546**
  *  Fix revenue 0 on abawebapps ([64553ce3](https://github.com/abaenglish/abawebapps/commit/64553ce3155208aef3d9db0b6016768c6d6a81ea))
  *  Fix revenue 0 on abawebapps ([076356d2](https://github.com/abaenglish/abawebapps/commit/076356d24dc8d0616f22a12d64657c5b293baa2a))



<a name="4.186.0"></a>
## 4.186.0 (2016-12-21)


#### Features

* **ABAWEBAPPS-1008**  Añadir el sourceID a la URL de redirección que viene como respuesta al signup. ([e300936f](https://github.com/abaenglish/abawebapps/commit/e300936f2b95726515fa8e2a70ffcd7eee83a53c))
* **ZOR-526**  renewals Allpago Brazil 100% january ([29c8d129](https://github.com/abaenglish/abawebapps/commit/29c8d1296c39d85571f8a44425f88166ac2e2cda))



<a name="4.185.3"></a>

## 4.185.3 (2016-12-20)


#### Features

* **ABAWEBAPP-1008**  Añadir el sourceID a la URL de redirección ([62df8d0](https://github.com/abaenglish/abawebapps/commit/62df8d05af207e7643e9994be7312c694ca51f39))
[e300936](https://github.com/abaenglish/abawebapps/commit/e300936f2b95726515fa8e2a70ffcd7eee83a53c)


## 4.185.0 (2016-11-15)


#### Features

* **ABAWEBAPP-980**  Resolve problems with actionCancelSubscriptionsZuora ([c88109e7](https://github.com/abaenglish/abawebapps/commit/c88109e79fb0b6c5457a8343b6866ae72cd3b769))



<a name="4.184.0"></a>
## 4.184.0 (2016-11-04)


#### Features

* **ABAWEBAPPS-865**  move log tables to aba_b2c ([a102c318](https://github.com/abaenglish/abawebapps/commit/a102c31857d16ecd38e44d8077ce5fd1df65ec66))
* **ZOR-291**
  *  check surnames for subscription service ([0c1ef3ac](https://github.com/abaenglish/abawebapps/commit/0c1ef3ac4ce1f36dfc1f722a96b1d21450f43c6c))
  *  check surnames for subscription service ([d27e4fd2](https://github.com/abaenglish/abawebapps/commit/d27e4fd2a016b560e9ef348f54de4c076bfd7ee7))
  *  check surnames for subscription service ([05da4f34](https://github.com/abaenglish/abawebapps/commit/05da4f341fd7aff6a5650527364878b23b01e749))
  *  check surnames for subscription service ([45a1d3fa](https://github.com/abaenglish/abawebapps/commit/45a1d3face5206d4b80eebeaf25a2b6020c7100a))
  *  check surnames for subscription service ([a090509f](https://github.com/abaenglish/abawebapps/commit/a090509f1ad5d1df7b2a13ea87f6c87b811f5f73))
  *  check surnames for subscription service ([da1ed8f4](https://github.com/abaenglish/abawebapps/commit/da1ed8f4288e3a4298d9e608115083cbd68ff923))
  *  check surnames for subscription service ([9b83ffc3](https://github.com/abaenglish/abawebapps/commit/9b83ffc337a20d3a0e53d16503a19aa03ef57ccd))
  *  check surnames for subscription service ([5e69a7d1](https://github.com/abaenglish/abawebapps/commit/5e69a7d14c0130829168fcced43bb5719245f4a6))
  *  check surnames for subscription service ([d4d881ae](https://github.com/abaenglish/abawebapps/commit/d4d881ae7befd2aa10f4cb14dbc23289cd913b72))
* **ZOR-303**
  *  return List of Objects ([f0caf5fb](https://github.com/abaenglish/abawebapps/commit/f0caf5fbd03edef95d9ec11d9dd15f5fabe38f65))
  *  refund payments service ([4bd3cdb9](https://github.com/abaenglish/abawebapps/commit/4bd3cdb916c7ad6d11135076c5c6b8751e8163a6))
* **ZOR-306**
  *  subscription response is not having paymentId ([f1eec178](https://github.com/abaenglish/abawebapps/commit/f1eec178b9c309ae669f8935808fb63215427f56))
  *  subscription response is not having paymentId - get last success payment ([b5d78cd5](https://github.com/abaenglish/abawebapps/commit/b5d78cd5463d28b07bcca14ba5cc338b87ff6c80))
* **ZOR-372**  Complete assessment with the progress tool ([4a32d382](https://github.com/abaenglish/abawebapps/commit/4a32d3823ffe8292426fcc5dbd76c08dd076a3c4))



<a name="4.183.0"></a>
## 4.183.0 (2016-10-25)


#### Features

* **ABAWEBAPPS-971**  :moyai: añadir el CountryID en el signup si el cliente de la API no lo envia como parámetro. ([af8491b9](https://github.com/abaenglish/abawebapps/commit/af8491b9c032a92cd33563bd11c6f25483ed8fb8))

#### Bug Fixes

* **ABAWEBAPPS-883**  :arrow_up upgrade to course 4.35.1 ([76fe7144](https://github.com/abaenglish/abawebapps/commit/76fe71445c757d3047abea14a337887aac71736d))



<a name="4.182.0"></a>
## 4.182.0 (2016-10-11)


#### Features

* **ZOR-372**  Complete assessment with the progress tool ([88410d15](https://github.com/abaenglish/abawebapps/commit/88410d157abc7142b656ba5ad4a1c2fb50197438))



<a name="4.181.1"></a>
## 4.181.1 (2016-10-03)




<a name="4.181.0"></a>
## 4.181.0 (2016-10-03)


#### Features

* **ZOR-285**
  *  50% users renew in Octuber with same price in Brasil ([b5634705](https://github.com/abaenglish/abawebapps/commit/b5634705c599903c836e1a3137c3e8a0f077dd28))
  *  50% users renew in Octuber with same price in Brasil ([af7c7c31](https://github.com/abaenglish/abawebapps/commit/af7c7c31d3fb6fdb415d0b63e173282f2f49dafe))



<a name="4.180.0"></a>
## 4.180.0 (2016-09-28)


#### Features

* **ZOR-366**  added partnerId to free2premiumZ response ([c46fc61c](https://github.com/abaenglish/abawebapps/commit/c46fc61c9ee70c05b1e03024645ec498b03dcfb8))



<a name="4.179.0"></a>
## 4.179.0 (2016-09-26)


#### Features

* **ABAWEBAPPS-954**  CORS filter added to login ([6ea8b70](https://github.com/abaenglish/abawebapps/commit/6ea8b700f25a348d858f9c418a44a5d313f32be9))



<a name="4.178.0"></a>
## 4.178.0 (2016-09-09)


#### Features

* **MIU-11**  Error pagina en función User de Intranet ([bf67a4db](https://github.com/abaenglish/abawebapps/commit/bf67a4db6898c19e53b9db110cc10cf43a9edca7))
* **ZOR-303**  return List of Objects ([88f7a699](https://github.com/abaenglish/abawebapps/commit/88f7a699ce29982001fa911bd150fdd411ebff22))



<a name="4.177.0"></a>
## 4.177.0 (2016-09-07)


#### Features

* **ZOR-303**
  *  refund payments service - add currency & period ([9edaf884](https://github.com/abaenglish/abawebapps/commit/9edaf8841bbfbcfea6621a50a94dba413ccdc2fc))
  *  refund payments service - add currency & period ([36614699](https://github.com/abaenglish/abawebapps/commit/36614699398b281475fa5c0c116c3af55ce67b18))
  *  refund payments service - add currency & period ([b52376f7](https://github.com/abaenglish/abawebapps/commit/b52376f70a52424abe8fb2d52ef98971836d7cde))
  *  merge with develop ([253af248](https://github.com/abaenglish/abawebapps/commit/253af248bb72eb6d520c3715809fd9b33b6fd6f9))
  *  refund payments service ([6a6690f3](https://github.com/abaenglish/abawebapps/commit/6a6690f3fca456bf5ae810cafbaabc85d74b44c7))



<a name="4.176.0"></a>
## 4.176.0 (2016-08-31)


#### Features

* **ABAWEBAPPS-865**  move log tables to aba_b2c ([a102c318](https://github.com/abaenglish/abawebapps/commit/a102c31857d16ecd38e44d8077ce5fd1df65ec66))
* **ZOR-276**  change specific payment phone number ([2e7b238c](https://github.com/abaenglish/abawebapps/commit/2e7b238ce442b46b95d4e561b5f3b99e7f315439))
* **ZOR-277**  change cancel subscription messages ([38bf3f6f](https://github.com/abaenglish/abawebapps/commit/38bf3f6f5160cd996222b77b5069228823e07e37))
* **ZOR-291**
  *  check surnames for subscription service ([0c1ef3ac](https://github.com/abaenglish/abawebapps/commit/0c1ef3ac4ce1f36dfc1f722a96b1d21450f43c6c))
  *  check surnames for subscription service ([d27e4fd2](https://github.com/abaenglish/abawebapps/commit/d27e4fd2a016b560e9ef348f54de4c076bfd7ee7))
  *  check surnames for subscription service ([05da4f34](https://github.com/abaenglish/abawebapps/commit/05da4f341fd7aff6a5650527364878b23b01e749))
  *  check surnames for subscription service ([45a1d3fa](https://github.com/abaenglish/abawebapps/commit/45a1d3face5206d4b80eebeaf25a2b6020c7100a))
  *  check surnames for subscription service ([a090509f](https://github.com/abaenglish/abawebapps/commit/a090509f1ad5d1df7b2a13ea87f6c87b811f5f73))
  *  check surnames for subscription service ([da1ed8f4](https://github.com/abaenglish/abawebapps/commit/da1ed8f4288e3a4298d9e608115083cbd68ff923))
  *  check surnames for subscription service ([9b83ffc3](https://github.com/abaenglish/abawebapps/commit/9b83ffc337a20d3a0e53d16503a19aa03ef57ccd))
  *  check surnames for subscription service ([5e69a7d1](https://github.com/abaenglish/abawebapps/commit/5e69a7d14c0130829168fcced43bb5719245f4a6))
  *  check surnames for subscription service ([d4d881ae](https://github.com/abaenglish/abawebapps/commit/d4d881ae7befd2aa10f4cb14dbc23289cd913b72))



<a name="4.176.0"></a>
## 4.176.0 (2016-08-31)


#### Features

* **ZOR-276**  change specific payment phone number ([2e7b238c](https://github.com/abaenglish/abawebapps/commit/2e7b238ce442b46b95d4e561b5f3b99e7f315439))
* **ZOR-277**  change cancel subscription messages ([38bf3f6f](https://github.com/abaenglish/abawebapps/commit/38bf3f6f5160cd996222b77b5069228823e07e37))



<a name="4.175.0"></a>
## 4.175.0 (2016-08-29)


#### Features

* **ZOR-267**
  *  renewals ([29f215b5](https://github.com/abaenglish/abawebapps/commit/29f215b5be7fc2d436cfc6cc1153b11a5bacab4b))
  *  zuora insert user ([f91bea8f](https://github.com/abaenglish/abawebapps/commit/f91bea8fa41ff7740df5686b5807cf6fb248deae))
  *  zuora insert user ([356fa7ab](https://github.com/abaenglish/abawebapps/commit/356fa7ab1d3711c8997c17d95f531c056e95b549))
  *  zuora insert user ([05a0dda9](https://github.com/abaenglish/abawebapps/commit/05a0dda9303b27368e3ff4bce6311c38517df13f))
  *  zuora renewals ([b4b67080](https://github.com/abaenglish/abawebapps/commit/b4b67080cc1cdea9fea546a85f6104c04ad3965d))
  *  zuora renewals ([9b7b28ad](https://github.com/abaenglish/abawebapps/commit/9b7b28ad0a35ee68c61936f46b6bb5ad60d2833b))
  *  zuora renewals - send info to selligent ([46356256](https://github.com/abaenglish/abawebapps/commit/46356256375421a81cebf241fe6ee67613d69014))
  *  zuora renewals - send info to selligent ([c625a4fa](https://github.com/abaenglish/abawebapps/commit/c625a4fa5ee66331778e946e6e3cfc436eac3d82))
  *  zuora renewals - abawebapps ws ([f6378536](https://github.com/abaenglish/abawebapps/commit/f637853620b7301673b228154c33c402da4560fa))
* **ZOR-268**
  *  zuora cancels - abawebapps ws ([f4e226a0](https://github.com/abaenglish/abawebapps/commit/f4e226a044c05e1e41465e505b266177c325c9fd))
  *  zuora cancels - abawebapps ws ([df847c7e](https://github.com/abaenglish/abawebapps/commit/df847c7e78ca44334273bcbfe6d4fe21c0995954))
  *  zuora cancels - add subscriptionId field to user_zuora ([f73876d7](https://github.com/abaenglish/abawebapps/commit/f73876d78e20fea1c20d7cb78e83c79ab66bcdf9))
  *  zuora cancels - add subscriptionId field to user_zuora ([bf5d41ba](https://github.com/abaenglish/abawebapps/commit/bf5d41ba582dd659de8a823cfd63f3a6c6d394d9))
  *  zuora cancels - add subscriptionId field to user_zuora ([f046f5c1](https://github.com/abaenglish/abawebapps/commit/f046f5c1ca78a5fe4c0404a78207290ecd103f64))
  *  zuora cancels - add subscriptionId field to user_zuora ([f1d47dad](https://github.com/abaenglish/abawebapps/commit/f1d47dad5d5cf84fa5c688ec9529a04cb1992a1a))
  *  zuora cancels - add subscriptionId field to user_zuora ([43c6b6d5](https://github.com/abaenglish/abawebapps/commit/43c6b6d5f9ca8f3003dd636fbe99517e398e7b3d))
  *  zuora cancels - add subscriptionId field to user_zuora ([56efb688](https://github.com/abaenglish/abawebapps/commit/56efb688ad62ed0f9f2f7e1fe6fc5e02669acdbc))
* **ZOR-270**  block link invoices ([755a18c3](https://github.com/abaenglish/abawebapps/commit/755a18c3cbdbf313b38867a7b4f9d83892cbdc47))



<a name="4.174.0"></a>
## 4.174.0 (2016-08-12)


#### Features

* **ZOR-255**
  *  Manage all the payment issues in a centralised way - add email field ([6d36bddb](https://github.com/abaenglish/abawebapps/commit/6d36bddb3b9cfc34df1377f0b617f8b8ab2f7d67))
  *  Manage all the payment issues in a centralised way ([581f45e7](https://github.com/abaenglish/abawebapps/commit/581f45e74ceb155dce023144eaae58497edcaf4a))
  *  Manage all the payment issues in a centralised way ([54f41bab](https://github.com/abaenglish/abawebapps/commit/54f41bab3cb82519aa09b5281e453a788b8bf0d1))
  *  Manage all the payment issues in a centralised way ([16c1ff46](https://github.com/abaenglish/abawebapps/commit/16c1ff4656f03912831f7201d3dfff9ea0ee9914))



<a name="4.173.0"></a>
## 4.173.0 (2016-08-02)


#### Features

* **ABAWEBAPPS-870**  unificar partner pagos recurrentes android ([2c40fe04](https://github.com/abaenglish/abawebapps/commit/2c40fe0439489d5e497df0077e52cfb79e3659d8))
* **zfreetopremium**
  *  create payment aba & change user type ([d6358736](https://github.com/abaenglish/abawebapps/commit/d6358736660365b1fba16fd8fe7b4c537fa885c0))
  *  create payment aba & change user type ([12f9fd65](https://github.com/abaenglish/abawebapps/commit/12f9fd65bf60f1465fa54533f465a5ca282c2d9a))
  *  create payment aba & change user type & add partner zuora ([7e2704a4](https://github.com/abaenglish/abawebapps/commit/7e2704a469110e6bc34daec4354db9e260eafbfe))
  *  create payment aba & change user type ([48f6a595](https://github.com/abaenglish/abawebapps/commit/48f6a5953245e30dcc4301e0e56be142d2fdbb0d))
  *  create payment aba & change user type ([8f17146a](https://github.com/abaenglish/abawebapps/commit/8f17146a139dc9d392141e1982acd6e78eb0edaa))
  *  create payment aba & change user type ([15ea1c50](https://github.com/abaenglish/abawebapps/commit/15ea1c50511d6dc113b83ca488b25ee599721fbe))
  *  create payment aba & change user type ([efc9b67c](https://github.com/abaenglish/abawebapps/commit/efc9b67ce8646078bb0691ed2d958e3a852bf8d1))



<a name="4.172.0"></a>
## 4.172.0 (2016-07-28)




<a name="4.171.0"></a>
## 4.171.0 (2016-07-25)


#### Features

* **ZOR-202**
  *  add idCountry to getCountryByUserID ws ([04c872a2](https://github.com/abaenglish/abawebapps/commit/04c872a2c2865e0c1c0cf9afa3ab8833650d47d0))
  *  add idCountry to getCountryByUserID ws ([d72dbf2d](https://github.com/abaenglish/abawebapps/commit/d72dbf2d9ba8a13e36cd0bc241afeed07f0ec8ae))



<a name="4.170.0"></a>
## 4.170.0 (2016-07-20)


#### Features

* **ZOR-114**
  *  Peticion POST REST para hacer insert :) ([07e0b1e6](https://github.com/abaenglish/abawebapps/commit/07e0b1e6baf989ed777476b89aa0a86f9f5bb8f9))
  *  Modified endpoint insert user_zuora ([1d557484](https://github.com/abaenglish/abawebapps/commit/1d557484e63de4926882456f9568aa5591ac0667))
  *  :moyai: Create two new endpoints check user exist in Zuora and Insert user in Zuora ([7150347d](https://github.com/abaenglish/abawebapps/commit/7150347d598553909212a79b3e725eb48a7fc2e0))
* **ZOR-131**
  *  Added email response existUserZuora ([4eb31a51](https://github.com/abaenglish/abawebapps/commit/4eb31a51d7c7a43ca917b457974963af6fbb85b8))
  *  Change content type response ([6949f961](https://github.com/abaenglish/abawebapps/commit/6949f96158a2006c3547a49d157132f371be5f13))
  *  Change name country for iso3 ([1dd5657d](https://github.com/abaenglish/abawebapps/commit/1dd5657d4ad5ccbca383c9111a7e49a4a603e1a9))
  *  :art: Create class OOP in PHP :tada: change response user exist in zuora ([88bd18aa](https://github.com/abaenglish/abawebapps/commit/88bd18aa85812d873a892383ab7a683fb35cc542))
* **ZOR-132**  :moyai: Create new endpoint getcountrybyuserid. ([dbb5c92e](https://github.com/abaenglish/abawebapps/commit/dbb5c92ef96a9cfa6a96de7112cf3bdf891d4a44))



<a name="4.169.0"></a>
## 4.169.0 (2016-07-05)


#### Features

* **ABAWEBAPPS-771**  Insert and update ([d9ea78d5](https://github.com/abaenglish/abawebapps/commit/d9ea78d5a917593c4aa161a63ef1f5916c2ef51b))
* **ABAWEBAPPS-832**  Han fallado algunos crons ([d364fe2f](https://github.com/abaenglish/abawebapps/commit/d364fe2f9f429e72a6806fa511ff70522fd80815))



<a name="4.168.0"></a>
## 4.168.0 (2016-06-29)


#### Features

* **ABAWEBAPPS-805**
  *  renewals android cron ([c5b936f2](https://github.com/abaenglish/abawebapps/commit/c5b936f279645934830e0e1c2b56db6ddd25562d))
  *  renewals android cron ([7c61fb6b](https://github.com/abaenglish/abawebapps/commit/7c61fb6b0e6802e51ee75b51127bf22d83d168d8))
  *  renewals android cron ([5fa091d5](https://github.com/abaenglish/abawebapps/commit/5fa091d54ebab91340788d857b885302cf7c6f5e))
  *  renewals android ([a5bedd66](https://github.com/abaenglish/abawebapps/commit/a5bedd665f52257c09fbe20a70433f66ee01fa70))
  *  android renewals ([94870c3c](https://github.com/abaenglish/abawebapps/commit/94870c3c6397ae4577f3b525c36575931b364e29))



<a name="4.167.0"></a>
## 4.167.0 (2016-06-17)


#### Features

* **ABAWEBAPPS-771**
  *  Save on Config table last update. ([2ad7df74](https://github.com/abaenglish/abawebapps/commit/2ad7df747e78505cc8f1b5d7566eefde4aa65880))
  *  DB Production. ([03286bce](https://github.com/abaenglish/abawebapps/commit/03286bce8622255d33d8053c7c35ea3561f5df85))
  *  Parametrized command. ([b5c9f9ac](https://github.com/abaenglish/abawebapps/commit/b5c9f9ac5a9ea02b6b1a850e944800a03f409366))
  *  New cron. ([b5351805](https://github.com/abaenglish/abawebapps/commit/b5351805615fe1ffeca08b75db4f221f993f96dd))
* **ABAWEBAPPS-794**  Fix Email with plus ([c56dbdff](https://github.com/abaenglish/abawebapps/commit/c56dbdff0ad7b1932fa5ec5193bca0a3b63f31c2))
* **ABAWEBAPPS-796**  Certs dirs ([e41ff5e0](https://github.com/abaenglish/abawebapps/commit/e41ff5e0ca6ba3ebe9904b7356aee6dfa8cdddaa))



<a name="4.167.0"></a>
## 4.167.0 (2016-06-17)


#### Features

* **ABAWEBAPPS-771**
  *  Save on Config table last update. ([2ad7df74](https://github.com/abaenglish/abawebapps/commit/2ad7df747e78505cc8f1b5d7566eefde4aa65880))
  *  DB Production. ([03286bce](https://github.com/abaenglish/abawebapps/commit/03286bce8622255d33d8053c7c35ea3561f5df85))
  *  Parametrized command. ([b5c9f9ac](https://github.com/abaenglish/abawebapps/commit/b5c9f9ac5a9ea02b6b1a850e944800a03f409366))
  *  New cron. ([b5351805](https://github.com/abaenglish/abawebapps/commit/b5351805615fe1ffeca08b75db4f221f993f96dd))
* **ABAWEBAPPS-794**  Fix Email with plus ([c56dbdff](https://github.com/abaenglish/abawebapps/commit/c56dbdff0ad7b1932fa5ec5193bca0a3b63f31c2))
* **ABAWEBAPPS-796**  Certs dirs ([e41ff5e0](https://github.com/abaenglish/abawebapps/commit/e41ff5e0ca6ba3ebe9904b7356aee6dfa8cdddaa))



<a name="4.166.4"></a>
## 4.166.4 (2016-06-13)




<a name="4.166.1"></a>
## 4.166.1 (2016-06-09)




<a name="4.166.0"></a>
## 4.166.0 (2016-06-08)


#### Bug Fixes

* **urlrules**  fix wrong usage of the URL rules. ([da1404bd](https://github.com/abaenglish/abawebapps/commit/da1404bdbdc09f021d0080d05e519875ca62ffdf))

#### Features

* **ABAWEBAPPS-792**  New Student Guide ([b08e75eb](https://github.com/abaenglish/abawebapps/commit/b08e75ebdc09ba603ba1398f6dc86aee3ec73240))
* **ABAWEBAPPS-793**  New teacher ([2beef5a9](https://github.com/abaenglish/abawebapps/commit/2beef5a9ec50a82784d8ae960c7d872c6774e400))
* **ABAWEBAPPS-794**  Change expirationDate. ([d5efd874](https://github.com/abaenglish/abawebapps/commit/d5efd87484add3d556bd44bd77637b9c836a5c65))



<a name="4.165.2"></a>
## 4.165.2 (2016-06-06)




<a name="4.165.1"></a>
## 4.165.1 (2016-06-04)


#### Bug Fixes

* **ABAWEBAPPS-789**  error al visualizar subtitulos en todos los idiomas ([3afb6f75](https://github.com/abaenglish/abawebapps/commit/3afb6f75d039adda812477ec0e5fdb628ac45d1a))



<a name="4.165.0"></a>
## 4.165.0 (2016-06-03)


#### Features

* **syslog**  merge with master ([6a05a6f2](https://github.com/abaenglish/abawebapps/commit/6a05a6f215cce0c9aa5e3566d4ba9d13c1229a4c))



<a name="4.164.0"></a>
## 4.164.0 (2016-06-01)


#### Features

* **syslog**  merge with master ([6a05a6f2](https://github.com/abaenglish/abawebapps/commit/6a05a6f215cce0c9aa5e3566d4ba9d13c1229a4c))



<a name="4.164.0"></a>
## 4.164.0 (2016-06-01)


#### Features

* **syslog**  merge with master ([6a05a6f2](https://github.com/abaenglish/abawebapps/commit/6a05a6f215cce0c9aa5e3566d4ba9d13c1229a4c))



<a name="4.162.0"></a>
## 4.162.0 (2016-05-23)


#### Features

* **ABAWEBAPPS-784**  fails renewal ([fefbea1b](https://github.com/abaenglish/abawebapps/commit/fefbea1bf34a2f910b4264acb5f3e3f9e3608fc6))



<a name="4.162.0"></a>
## 4.162.0 (2016-05-23)


#### Features

* **ABAWEBAPPS-784**  fails renewal ([fefbea1b](https://github.com/abaenglish/abawebapps/commit/fefbea1bf34a2f910b4264acb5f3e3f9e3608fc6))



<a name="4.161.0"></a>
## 4.161.0 (2016-05-17)


#### Features

* **ABAWEBAPPS-642/ABAWEBAPPS-647**  kill abaanalytics ([edddb4dd](https://github.com/abaenglish/abawebapps/commit/edddb4dd9f99b8c8fca33b3c565c171325bce2ff))
* **ABAWEBAPPS-744**
  *  Header ([385d73b4](https://github.com/abaenglish/abawebapps/commit/385d73b470c27970d3d38e91eeca5157304b1ddd))
  *  Order by ([fa696c22](https://github.com/abaenglish/abawebapps/commit/fa696c229d2336acdc0de17f0d9195a370f999f7))



<a name="4.161.0"></a>
## 4.161.0 (2016-05-17)


#### Features

* **ABAWEBAPPS-642/ABAWEBAPPS-647**  kill abaanalytics ([edddb4dd](https://github.com/abaenglish/abawebapps/commit/edddb4dd9f99b8c8fca33b3c565c171325bce2ff))
* **ABAWEBAPPS-744**
  *  Header ([385d73b4](https://github.com/abaenglish/abawebapps/commit/385d73b470c27970d3d38e91eeca5157304b1ddd))
  *  Order by ([fa696c22](https://github.com/abaenglish/abawebapps/commit/fa696c229d2336acdc0de17f0d9195a370f999f7))



<a name="4.160.0"></a>
## 4.160.0 (2016-05-12)


#### Features

* **ABAWEBAPPS-744**  B2B Partner creation ([58db62dd](https://github.com/abaenglish/abawebapps/commit/58db62ddd04c6b8a3eb1778b06044a358f994d74))



<a name="4.160.0"></a>
## 4.160.0 (2016-05-12)


#### Features

* **ABAWEBAPPS-744**  B2B Partner creation ([58db62dd](https://github.com/abaenglish/abawebapps/commit/58db62ddd04c6b8a3eb1778b06044a358f994d74))



<a name="4.159.0"></a>
## 4.159.0 (2016-05-12)


#### Features

* **ABAWEBAPPS-765**  New teacher ([928dbb1f](https://github.com/abaenglish/abawebapps/commit/928dbb1f30239d1f41616fd90b7b7cd574189d0d))



<a name="4.159.0"></a>
## 4.159.0 (2016-05-12)


#### Features

* **ABAWEBAPPS-765**  New teacher ([928dbb1f](https://github.com/abaenglish/abawebapps/commit/928dbb1f30239d1f41616fd90b7b7cd574189d0d))



<a name="4.158.0"></a>
## 4.158.0 (2016-05-05)


#### Features

* **ABAWEBAPPS-722**
  *  cooladata minified ([496ed4e8](https://github.com/abaenglish/abawebapps/commit/496ed4e8387ab3b0d69b4e065a66264b397ab8f6))
  *  cooladata minified ([41cf8ef2](https://github.com/abaenglish/abawebapps/commit/41cf8ef2f5749f63fcdd89b32457ef0b1532514b))
  *  cooladata minified ([19e9f165](https://github.com/abaenglish/abawebapps/commit/19e9f16539fb7b1e5244d560247108988a3e4e45))
  *  test cooladata minified ([9cf7737a](https://github.com/abaenglish/abawebapps/commit/9cf7737a4e92f9dd758b7874030ea85b97853ab2))
* **ABAWEBAPPS-733**  Cambiar de profesor ([bef4ddd5](https://github.com/abaenglish/abawebapps/commit/bef4ddd5a2b54fa0e28068b50530bf151b47d980))



<a name="4.158.0"></a>
## 4.158.0 (2016-05-05)


#### Features

* **ABAWEBAPPS-722**
  *  cooladata minified ([496ed4e8](https://github.com/abaenglish/abawebapps/commit/496ed4e8387ab3b0d69b4e065a66264b397ab8f6))
  *  cooladata minified ([41cf8ef2](https://github.com/abaenglish/abawebapps/commit/41cf8ef2f5749f63fcdd89b32457ef0b1532514b))
  *  cooladata minified ([19e9f165](https://github.com/abaenglish/abawebapps/commit/19e9f16539fb7b1e5244d560247108988a3e4e45))
  *  test cooladata minified ([9cf7737a](https://github.com/abaenglish/abawebapps/commit/9cf7737a4e92f9dd758b7874030ea85b97853ab2))
* **ABAWEBAPPS-733**  Cambiar de profesor ([bef4ddd5](https://github.com/abaenglish/abawebapps/commit/bef4ddd5a2b54fa0e28068b50530bf151b47d980))



<a name="4.157.0"></a>
## 4.157.0 (2016-05-02)


#### Features

* **ABAWEBAPPS-692**  Remove page load ([5484485c](https://github.com/abaenglish/abawebapps/commit/5484485c4417bccf13f88338ac7e9ce38793afdb))
* **ABAWEBAPPS-693**
  *  Implement navigation events - OPENED_VIDEOCLASSES_INDEX ([9816795c](https://github.com/abaenglish/abawebapps/commit/9816795cadcc171df319989e41e9f8566d2c20f3))
  *  Implement navigation events - OPENED_HELP ([fbc39cb5](https://github.com/abaenglish/abawebapps/commit/fbc39cb536421c62e554aea936dd757ced445aaa))
  *  Implement navigation events - OPENED_PRICES ([d8a577f1](https://github.com/abaenglish/abawebapps/commit/d8a577f16288f45eeefea3326c2a82356e07fba4))
  *  Implement navigation events - OPENDED_VIDEOCLASSES_INDEX ([c7c1a1cc](https://github.com/abaenglish/abawebapps/commit/c7c1a1ccb7b703b37db556dd5e5473a423401707))
  *  Implement navigation events - ENTERED_SECTION ([5166311c](https://github.com/abaenglish/abawebapps/commit/5166311c258914c08c3bfd7b19c422a3c9d15292))
  *  Implement navigation events - ENTERED_UNIT ([2d48bfab](https://github.com/abaenglish/abawebapps/commit/2d48bfab888ebc22084ec76eb68adde292ff5e95))
  *  Implement navigation events - ENTERED_COURSE_INDEX ([3c3a3156](https://github.com/abaenglish/abawebapps/commit/3c3a31568a08a691a12203802544b1f0275fb0d2))
  *  Implement navigation events - ENTERED_CAMPUS_HOME ([3b9b6ee4](https://github.com/abaenglish/abawebapps/commit/3b9b6ee46838d447e77f92e57be5532b5147f9fa))
  *  Implement navigation events - LOGGED_IN ([e2607174](https://github.com/abaenglish/abawebapps/commit/e2607174d39b507d00e0d265760db6065b9ac6b7))
* **ABAWEBAPPS-694**
  *  Change session_id to cooladata internal ([34855ef2](https://github.com/abaenglish/abawebapps/commit/34855ef259691f14071e1e9869667c3e5feceb7f))
  *  Change session_id to cooladata internal ([3a77117e](https://github.com/abaenglish/abawebapps/commit/3a77117e007ea25852380e5704d189766b759216))
* **ABAWEBAPPS-695**
  *  Implement navigation events - PROGRESS_UPDATE_TOTAL ([a09ea6d6](https://github.com/abaenglish/abawebapps/commit/a09ea6d667ed38593ba31f30f38e9383077c775f))
  *  Implement navigation events - PROGRESS_UPDATE_TOTAL ([7e7c8d56](https://github.com/abaenglish/abawebapps/commit/7e7c8d564a80f322a02a3854a7b13aa60d0f1f65))
  *  Implement other events - SENT_PROFESSOR_MESSAGE, SELECTED_PRICE_PLAN ([e1144f39](https://github.com/abaenglish/abawebapps/commit/e1144f3925b1da173b6941c0affa2824ae330ba1))
  *  Implement navigation events - SELECTED_PRICE_PLAN ([07017b0c](https://github.com/abaenglish/abawebapps/commit/07017b0c0efb60745ff8ba3703280e07b3085316))
  *  Implement navigation events - ENTERED_PROFESSOR_MESSAGE_SECTION, ENTERED_INTERACTIVE_GRAMMAR ([a87637c2](https://github.com/abaenglish/abawebapps/commit/a87637c235c28106077c50f8200c85602a53e8e0))
  *  Implement other events - REVIEWED_INSTRUCTIONS ([1317ee68](https://github.com/abaenglish/abawebapps/commit/1317ee682a806c855722ac50ab3f9d26be584d15))
  *  Implement other events - SENT_PROFESSOR_MESSAGE ([8ecc86b9](https://github.com/abaenglish/abawebapps/commit/8ecc86b9b39da8f90b655ee719cb7e59729341b7))
  *  Implement other events - DOWNLOADED_CERTIFICATE ([8d8b2dd3](https://github.com/abaenglish/abawebapps/commit/8d8b2dd39f6ffb6f70ad3416d3477de08dea560c))
  *  Implement study events - REMOVE OLD EVENTS ([0d3d23ee](https://github.com/abaenglish/abawebapps/commit/0d3d23eea78182f84a024044711da42cc004a31a))
  *  Implement profile events - REMOVE OLD EVENTS ([6e683329](https://github.com/abaenglish/abawebapps/commit/6e6833296ebf7e706c046c502b3f49fcea5c2c4f))
  *  Implement profile events - USER_REGISTERED ([3effd6a5](https://github.com/abaenglish/abawebapps/commit/3effd6a57f62126af10b0b22534e39fdb0a2b693))
  *  Implement profile events - CHANGED_LEVEL ([41ea40c3](https://github.com/abaenglish/abawebapps/commit/41ea40c3d6fbc708fef3a561bb734a3edb5b431c))
  *  Implement profile events - CHANGED_PERSONAL_DETAILS ([ad166f79](https://github.com/abaenglish/abawebapps/commit/ad166f79d9cb9925ed3ddfbb3f3f3bb8d84ea04f))
  *  Implement study events - ADDED_LINKEDIN, LIKED_FACEBOOK event test ([5029d36a](https://github.com/abaenglish/abawebapps/commit/5029d36a4c5dcdd9eb6426532bb13658aec4f0c7))
  *  Implement study events - REVIEWED_TEST_RESULTS ([ccfa6052](https://github.com/abaenglish/abawebapps/commit/ccfa6052fd37acf392456399c730a38fd2321a4b))
  *  Implement study events - REVIEWED_TEST_RESULTS ([c980d7dd](https://github.com/abaenglish/abawebapps/commit/c980d7dd7086527547073fa4e048184127031396))
  *  Implement study events - STARTED_INTERPRETATION ([c6ae32e5](https://github.com/abaenglish/abawebapps/commit/c6ae32e5a28195f8bcbe5f9fa1506537e8476d9d))
  *  Implement study events - VERIFIED_TEXT ([b39e9f81](https://github.com/abaenglish/abawebapps/commit/b39e9f81a20ac5b05c40a0d3ca3551557330dcd0))
  *  Implement study events - RECORDED_AUDIO ([9a6aef2a](https://github.com/abaenglish/abawebapps/commit/9a6aef2ac755a248fdd54706d566f1f042ea43b0))
  *  Implement study events - LISTENED_AUDIO ([90685d9a](https://github.com/abaenglish/abawebapps/commit/90685d9ac2d16a12dde03a90adf7debd336f24af))
  *  Implement study events - END_VIDEO ([664050d7](https://github.com/abaenglish/abawebapps/commit/664050d746dfef6e7476b788744c737548093721))
* **ABAWEBAPPS-701**
  *  Implement events ([a65468ef](https://github.com/abaenglish/abawebapps/commit/a65468ef422369a7aa1be4ef08a0d339a40eeadc))
  *  Implement events ([2f69d84e](https://github.com/abaenglish/abawebapps/commit/2f69d84eeb0d879c1851d2cd1cba6e7709261bdc))
* **Cooladata**  Entered Section fix ([344eade3](https://github.com/abaenglish/abawebapps/commit/344eade3b0587660c758bb1160687dc1e67e9afa))



<a name="4.157.0"></a>
## 4.157.0 (2016-05-02)


#### Features

* **ABAWEBAPPS-692**  Remove page load ([5484485c](https://github.com/abaenglish/abawebapps/commit/5484485c4417bccf13f88338ac7e9ce38793afdb))
* **ABAWEBAPPS-693**
  *  Implement navigation events - OPENED_VIDEOCLASSES_INDEX ([9816795c](https://github.com/abaenglish/abawebapps/commit/9816795cadcc171df319989e41e9f8566d2c20f3))
  *  Implement navigation events - OPENED_HELP ([fbc39cb5](https://github.com/abaenglish/abawebapps/commit/fbc39cb536421c62e554aea936dd757ced445aaa))
  *  Implement navigation events - OPENED_PRICES ([d8a577f1](https://github.com/abaenglish/abawebapps/commit/d8a577f16288f45eeefea3326c2a82356e07fba4))
  *  Implement navigation events - OPENDED_VIDEOCLASSES_INDEX ([c7c1a1cc](https://github.com/abaenglish/abawebapps/commit/c7c1a1ccb7b703b37db556dd5e5473a423401707))
  *  Implement navigation events - ENTERED_SECTION ([5166311c](https://github.com/abaenglish/abawebapps/commit/5166311c258914c08c3bfd7b19c422a3c9d15292))
  *  Implement navigation events - ENTERED_UNIT ([2d48bfab](https://github.com/abaenglish/abawebapps/commit/2d48bfab888ebc22084ec76eb68adde292ff5e95))
  *  Implement navigation events - ENTERED_COURSE_INDEX ([3c3a3156](https://github.com/abaenglish/abawebapps/commit/3c3a31568a08a691a12203802544b1f0275fb0d2))
  *  Implement navigation events - ENTERED_CAMPUS_HOME ([3b9b6ee4](https://github.com/abaenglish/abawebapps/commit/3b9b6ee46838d447e77f92e57be5532b5147f9fa))
  *  Implement navigation events - LOGGED_IN ([e2607174](https://github.com/abaenglish/abawebapps/commit/e2607174d39b507d00e0d265760db6065b9ac6b7))
* **ABAWEBAPPS-694**
  *  Change session_id to cooladata internal ([34855ef2](https://github.com/abaenglish/abawebapps/commit/34855ef259691f14071e1e9869667c3e5feceb7f))
  *  Change session_id to cooladata internal ([3a77117e](https://github.com/abaenglish/abawebapps/commit/3a77117e007ea25852380e5704d189766b759216))
* **ABAWEBAPPS-695**
  *  Implement navigation events - PROGRESS_UPDATE_TOTAL ([a09ea6d6](https://github.com/abaenglish/abawebapps/commit/a09ea6d667ed38593ba31f30f38e9383077c775f))
  *  Implement navigation events - PROGRESS_UPDATE_TOTAL ([7e7c8d56](https://github.com/abaenglish/abawebapps/commit/7e7c8d564a80f322a02a3854a7b13aa60d0f1f65))
  *  Implement other events - SENT_PROFESSOR_MESSAGE, SELECTED_PRICE_PLAN ([e1144f39](https://github.com/abaenglish/abawebapps/commit/e1144f3925b1da173b6941c0affa2824ae330ba1))
  *  Implement navigation events - SELECTED_PRICE_PLAN ([07017b0c](https://github.com/abaenglish/abawebapps/commit/07017b0c0efb60745ff8ba3703280e07b3085316))
  *  Implement navigation events - ENTERED_PROFESSOR_MESSAGE_SECTION, ENTERED_INTERACTIVE_GRAMMAR ([a87637c2](https://github.com/abaenglish/abawebapps/commit/a87637c235c28106077c50f8200c85602a53e8e0))
  *  Implement other events - REVIEWED_INSTRUCTIONS ([1317ee68](https://github.com/abaenglish/abawebapps/commit/1317ee682a806c855722ac50ab3f9d26be584d15))
  *  Implement other events - SENT_PROFESSOR_MESSAGE ([8ecc86b9](https://github.com/abaenglish/abawebapps/commit/8ecc86b9b39da8f90b655ee719cb7e59729341b7))
  *  Implement other events - DOWNLOADED_CERTIFICATE ([8d8b2dd3](https://github.com/abaenglish/abawebapps/commit/8d8b2dd39f6ffb6f70ad3416d3477de08dea560c))
  *  Implement study events - REMOVE OLD EVENTS ([0d3d23ee](https://github.com/abaenglish/abawebapps/commit/0d3d23eea78182f84a024044711da42cc004a31a))
  *  Implement profile events - REMOVE OLD EVENTS ([6e683329](https://github.com/abaenglish/abawebapps/commit/6e6833296ebf7e706c046c502b3f49fcea5c2c4f))
  *  Implement profile events - USER_REGISTERED ([3effd6a5](https://github.com/abaenglish/abawebapps/commit/3effd6a57f62126af10b0b22534e39fdb0a2b693))
  *  Implement profile events - CHANGED_LEVEL ([41ea40c3](https://github.com/abaenglish/abawebapps/commit/41ea40c3d6fbc708fef3a561bb734a3edb5b431c))
  *  Implement profile events - CHANGED_PERSONAL_DETAILS ([ad166f79](https://github.com/abaenglish/abawebapps/commit/ad166f79d9cb9925ed3ddfbb3f3f3bb8d84ea04f))
  *  Implement study events - ADDED_LINKEDIN, LIKED_FACEBOOK event test ([5029d36a](https://github.com/abaenglish/abawebapps/commit/5029d36a4c5dcdd9eb6426532bb13658aec4f0c7))
  *  Implement study events - REVIEWED_TEST_RESULTS ([ccfa6052](https://github.com/abaenglish/abawebapps/commit/ccfa6052fd37acf392456399c730a38fd2321a4b))
  *  Implement study events - REVIEWED_TEST_RESULTS ([c980d7dd](https://github.com/abaenglish/abawebapps/commit/c980d7dd7086527547073fa4e048184127031396))
  *  Implement study events - STARTED_INTERPRETATION ([c6ae32e5](https://github.com/abaenglish/abawebapps/commit/c6ae32e5a28195f8bcbe5f9fa1506537e8476d9d))
  *  Implement study events - VERIFIED_TEXT ([b39e9f81](https://github.com/abaenglish/abawebapps/commit/b39e9f81a20ac5b05c40a0d3ca3551557330dcd0))
  *  Implement study events - RECORDED_AUDIO ([9a6aef2a](https://github.com/abaenglish/abawebapps/commit/9a6aef2ac755a248fdd54706d566f1f042ea43b0))
  *  Implement study events - LISTENED_AUDIO ([90685d9a](https://github.com/abaenglish/abawebapps/commit/90685d9ac2d16a12dde03a90adf7debd336f24af))
  *  Implement study events - END_VIDEO ([664050d7](https://github.com/abaenglish/abawebapps/commit/664050d746dfef6e7476b788744c737548093721))
* **ABAWEBAPPS-701**
  *  Implement events ([a65468ef](https://github.com/abaenglish/abawebapps/commit/a65468ef422369a7aa1be4ef08a0d339a40eeadc))
  *  Implement events ([2f69d84e](https://github.com/abaenglish/abawebapps/commit/2f69d84eeb0d879c1851d2cd1cba6e7709261bdc))
* **Cooladata**  Entered Section fix ([344eade3](https://github.com/abaenglish/abawebapps/commit/344eade3b0587660c758bb1160687dc1e67e9afa))



<a name="4.156.0"></a>
## 4.156.0 (2016-04-26)




<a name="4.156.0"></a>
## 4.156.0 (2016-04-25)


#### Features

* **ABAWEBAPPS-703**  Editar mail ([643889b4](https://github.com/abaenglish/abawebapps/commit/643889b4393d9a3cabc80cbdbc7b4dd4b09dfb61))
* **ABAWEBAPPS-706**  Error subida csv ([b538df92](https://github.com/abaenglish/abawebapps/commit/b538df9222d5b7d48aa961285522a835d4bf25c1))
* **ABAWEBAPPS-708**  Menu reservas ([a4835778](https://github.com/abaenglish/abawebapps/commit/a483577832078c4fb8d8173629800009836f37b1))



<a name="4.155.3"></a>
## 4.155.3 (2016-04-21)




<a name="4.155.2"></a>
## 4.155.2 (2016-04-20)




<a name="4.155.0"></a>
## 4.155.0 (2016-04-19)


#### Features

* **ABAWEBAPPS-681**
  *  add cooladata user scope data to api functions ([58dac7b3](https://github.com/abaenglish/abawebapps/commit/58dac7b3944676822f8e985a6899a1e6f84136ad))
  *  add cooladata user scope data to api functions ([69a56257](https://github.com/abaenglish/abawebapps/commit/69a56257d9e68f9cd4ae00c44f86624b1d304008))
  *  add cooladata user scope data to api functions ([ff0d385d](https://github.com/abaenglish/abawebapps/commit/ff0d385db74f5eb79766a265aec7e5e5bd6ef31f))
  *  add cooladata user scope data to api functions ([7903b779](https://github.com/abaenglish/abawebapps/commit/7903b779c2c31dd76c6008f61dd33928a98721a6))
  *  add cooladata user scope data to api functions ([35054947](https://github.com/abaenglish/abawebapps/commit/35054947149d8bf38e5319704b1d1033966d0dad))
  *  add cooladata user scope data to api functions ([27b9e281](https://github.com/abaenglish/abawebapps/commit/27b9e28138a8f1cfd627c85822b1628e2934b509))
* **ABAWEBAPPS-685**  Bug intranet ([71cc0401](https://github.com/abaenglish/abawebapps/commit/71cc040128fdc9928e749f08fdbea2f938ff5ab2))
* **ABAWEBAPPS-689**
  *  Enterprise List ([a5d0aa77](https://github.com/abaenglish/abawebapps/commit/a5d0aa771db3feb812036903e7580dd8fa52f35d))
  *  Create new user when this user already exists but is not representable ([005b1eb5](https://github.com/abaenglish/abawebapps/commit/005b1eb59a31b1e1b973678051f01e53f2f16e60))



<a name="4.154.0"></a>
## 4.154.0 (2016-04-12)


#### Features

* **ABAWEBAPPS-656**
  *  register login form ([ff149834](https://github.com/abaenglish/abawebapps/commit/ff149834a37508cb16f2c2272250d76182ee5afe))
  *  register login form ([a6f2a829](https://github.com/abaenglish/abawebapps/commit/a6f2a82922709845f8e46673eaab6898569e7eb3))
  *  register login form ([513be305](https://github.com/abaenglish/abawebapps/commit/513be305b8ac7361e20c5f9fc811bf9675cb8bea))
  *  register login form ([85526722](https://github.com/abaenglish/abawebapps/commit/8552672276f7337a434a6b0ed124b2eff937cf96))
  *  register login form ([a058e7af](https://github.com/abaenglish/abawebapps/commit/a058e7afea4d81f35264793df2da262a25ef65a4))
  *  register login form ([0f5847ba](https://github.com/abaenglish/abawebapps/commit/0f5847ba6fbc99aaefc98f077c26e3a25bf26a4c))
  *  register login form ([a310894a](https://github.com/abaenglish/abawebapps/commit/a310894a24075a1651ab08809350920b99da75bc))
  *  register login form ([3dbdf64f](https://github.com/abaenglish/abawebapps/commit/3dbdf64f6b2348de6f465a983b21ede703f80616))
  *  register login form ([c9a0086a](https://github.com/abaenglish/abawebapps/commit/c9a0086af62857cc132ca128b052ec9a929edba1))
  *  register login form ([be3c70d8](https://github.com/abaenglish/abawebapps/commit/be3c70d8cf0a5b2eaf670949b1ac0f173e71c279))
  *  register login form ([e40c76e7](https://github.com/abaenglish/abawebapps/commit/e40c76e7d1dc0b37a0c10081dbc20753d08636b7))
* **ABAWEBAPPS-659**  Profesor enjoy ([d2017ef6](https://github.com/abaenglish/abawebapps/commit/d2017ef670b0c7df4e3ade0a542ffdfdd4055415))
* **ABAWEBAPPS-665**
  *  Translations changes ([2b7c3541](https://github.com/abaenglish/abawebapps/commit/2b7c35418c80cdcb3ceb5b3611635e0076a1ca13))
  *  Translations ([99f3587d](https://github.com/abaenglish/abawebapps/commit/99f3587d0ff652f159bb5b9294259b5f0adf0f73))
* **ABAWEBAPPS-668**
  *  Refactoring extranet delete enterprise and user. ([fedd3632](https://github.com/abaenglish/abawebapps/commit/fedd3632d9777e8fef4b384eea4f2956ed864dde))
  *  Refactoring extranet delete enterprise and user. ([e12b6875](https://github.com/abaenglish/abawebapps/commit/e12b6875e47f2d63b56324272e20052bc676c5f3))
* **ABAWEBAPPS-675**  set_time_limit(600) ([117641d0](https://github.com/abaenglish/abawebapps/commit/117641d0b5cf97308b19f23532274e34a0a657ad))



<a name="4.153.0"></a>
## 4.153.0 (2016-04-06)


#### Features

* **ABAWEBAPPS-253**  Splash page ([00bae8e4](https://github.com/abaenglish/abawebapps/commit/00bae8e49b0280cc525166e6a519168ab816680d))
* **ABAWEBAPPS-653**  change partnerId length ([2ecc291b](https://github.com/abaenglish/abawebapps/commit/2ecc291bcf7129bfba22e50d25969566d7ae6991))
* **ABAWEBAPPS-661**  Prroblema registro empresa MAX MARA ([52a8f113](https://github.com/abaenglish/abawebapps/commit/52a8f1135ae47e11a07497eeaa85b20fb13ae18c))
* **ABAWEBAPPS-663**  enable playstore expire users cron ([bb4f832f](https://github.com/abaenglish/abawebapps/commit/bb4f832fadee5768eee35b8fd09d8cfb21fcf7ae))
* **ABAWEBAPPS-665**  usuarios sin progreso ([3673406f](https://github.com/abaenglish/abawebapps/commit/3673406fe7778c4228c0c3a2e287e6598e9200a7))



<a name="4.152.0"></a>
## 4.152.0 (2016-04-04)


#### Features

* **ABAWEBAPPS-597**
  *  cooladata user login ([22266376](https://github.com/abaenglish/abawebapps/commit/22266376bef8323c9d106895d3ff682a8489bffe))
  *  cooladata ([97dd3389](https://github.com/abaenglish/abawebapps/commit/97dd33894567eb73f84e84f26b765753bbadcbe7))
* **ABAWEBAPPS-630**  Disable onchange ([34600d88](https://github.com/abaenglish/abawebapps/commit/34600d886ab4f2bd5809ccaf2052c3b064f34d24))



<a name="4.150.0"></a>
## 4.150.0 (2016-03-24)


#### Features

* **593**  remove all harcoded pixels & DB pixels ([6fe2b481](https://github.com/abaenglish/abawebapps/commit/6fe2b48103a4c8642bfdddfd63cbd90060d864be))
* **ABAWEBAPPS-598**  Fix onw German translation. ([7911154e](https://github.com/abaenglish/abawebapps/commit/7911154ebc3605c0ffb98f205f7659995997b0d1))
* **ABAWEBAPPS-615**  Translations ([757c787f](https://github.com/abaenglish/abawebapps/commit/757c787f7f955ba280f73bb4fa68672289b612d9))



<a name="4.149.0"></a>
## 4.149.0 (2016-03-22)


#### Features

* **ABAWEBAPPS-617**
  *  On footer too. ([12bc1309](https://github.com/abaenglish/abawebapps/commit/12bc130928c261b4acdea548a262c4ad2cf09827))
  *  enable all languages zendesk ([d5cac7ac](https://github.com/abaenglish/abawebapps/commit/d5cac7ac548595f45425bb563bc720d6fa67f3c5))
  *  enable all languages zendesk ([e92f02b8](https://github.com/abaenglish/abawebapps/commit/e92f02b8f6d3fe5e35cf8350b06d3e3e2f38efa0))
  *  enable all languages zendesk ([ae0c8c3e](https://github.com/abaenglish/abawebapps/commit/ae0c8c3e958a00368b519165bc496a9b358538e2))
  *  enable all languages zendesk ([dcc2c716](https://github.com/abaenglish/abawebapps/commit/dcc2c7165e48224fdde4fed63df84b8cca4f593a))
* **ABAWEBAPPS-628**  Bug when change the level on campus over Extranet ([7728b75e](https://github.com/abaenglish/abawebapps/commit/7728b75e40d8af403ca1894909ca8040d1c16893))



<a name="4.148.0"></a>
## 4.148.0 (2016-03-22)


#### Features

* **ABAWEBAPPS-622**  remove harcoded pixels ([7a980be3](https://github.com/abaenglish/abawebapps/commit/7a980be3e48a2e31b48a106d70c0802beba07a02))
* **ABAWEBAPPS-627**  Change support email for extranet. ([5cab1125](https://github.com/abaenglish/abawebapps/commit/5cab11254fde3132e3a2894c9900a6973d7c8bd3))



<a name="4.146.0"></a>
## 4.146.0 (2016-03-17)




<a name="4.145.0"></a>
## 4.145.0 (2016-03-17)




<a name="4.143.0"></a>
## 4.143.0 (2016-03-12)

#### Performance improvements

* **ABAWEBAPPS-604** - reducir el uso de "user_dictionary" por cada transacción.
* **ABAWEBAPPS-605** - Configurar una caché en memoria para EConfig.php


<a name="4.142.2"></a>
## 4.142.2 (2016-03-10)


#### Bug Fixes

* **ABAWEBAPPS-600**  - El SOAP del webtheme añade parámetros al querystring corrompiendo el último valor. ([0d9365b3](https://github.com/abaenglish/abawebapps/commit/0d9365b33ac474a5b2305f53fd4e344974d7702b))



<a name="4.142.1"></a>
## 4.142.1 (2016-03-10)


#### Bug Fixes

* **ABAWEBAPPS-599**  el mail de Bienvenida tiene un blucle en el acceso a campus... ([f4910ea5](https://github.com/abaenglish/abawebapps/commit/f4910ea5770d9d5a082543d36188fa46950e34c2))



<a name="4.142.0"></a>
## 4.142.0 (2016-03-10)


#### Features

* **ABAWEBAPPS-592**
  *  - refactoring the configuration of the URL Manager ([176b06d8](https://github.com/abaenglish/abawebapps/commit/176b06d8eb6cb7e2549cc5eab3fbdf2a43458f4b))
  *  Fix language filter and other minor fixes. ([625c9fce](https://github.com/abaenglish/abawebapps/commit/625c9fceb3c291a9fd5df90b5b940b8b91698e37))



<a name="4.141.0"></a>
## 4.141.0 (2016-03-01)


#### Features

* **ABAWEBAPPS-527**  Fix dashboard small width. ([99a70d34](https://github.com/abaenglish/abawebapps/commit/99a70d3416072e6a0db4102c355778e68c7f283e))



<a name="4.139.0"></a>
## 4.139.0 (2016-02-25)


#### Features

* **ABAWEBAPPS-552**  Cambiando la funcion de campus a common. ((be814470))



<a name="4.138.0"></a>
## 4.138.0 (2016-02-18)


#### Features

* **ABAWEBAPPS-470**
  *  New vagrant config.yaml ((3e1b531f))
  *  Adding changes again. ((b7967a3f))
* **ABAWEBAPPS-496**
  *  Ready to production. ((f213e139))
  *  Fix an error with autologin. ((42e27420))
* **ABAWEBAPPS-521**
  *  Fix some values. ((a3a12a94))
  *  Add idPromoCode value ((350aabc6))
  *  Problema pixel criteo ((f121d2df))
* **ABAWEBAPPS-527**  Pending ((8e9ffc1e))



<a name="4.137.0"></a>
## 4.137.0 (2016-02-18)


#### Bug Fixes

* **#6194**  Al subir el fichero de reconciliación de laCaixa desde intranet, aparece el siguiente mensaje: 500 - mkdir() ((49dfcb89))
* **6334**  diccionário del curso no funciona :bug: ((0e774015))
* **ABASERVER-53**  En corporate, las fechas de caducidad no se actualizan ((d2f64c03))
* **ABAWEBAPPS-233**  GeoIP: Undefined offset: 253 :bug: ((52dbb882))
* **ABAWEBAPPS-445**  - :flag-de: Modificar texto 2x1 de página de pag ((77183787))
* **RegisterSelligentUsers**
  *  - still trying to improve the performance of the RegisterSelligentUsers cron ((bebcee37))
  *  - still trying to improve the performance of the RegisterSelligentUsers cron ((2a346657))
* **base.php**  revert :bomb: simplify environment configuration ((5753a893))

#### Features

* **4.129.0**  Release abawebapps 4.129.0 ((e0f584df))
* **5978**
  *  CORS & OPTIONS support ((d7ba885f))
  *  CORS & OPTIONS support ((ff4124c4))
* **ABAAnalytics**
  *  Guardar el idSession cuando se hace un pago por el Campus. ((6c816a26))
  *  añadido nuevo parámetro en Session. ((4cdee98d))
* **ABAWEBAPPS-138**
  *  Fixed some styles ((2b4a60a2))
  *  Recover css extranet ((f585adca))
  *  Maquetacion y mejora .js test de nivel ((f0344726))
  *  Pending ((03f75bf6))
  *  Pending ((80ba5f0f))
  *  Pushing to Enric ((df6b6035))
  *  Resolve conflicts ((41f0d254))
* **ABAWEBAPPS-209**  Add ruso language ((81e64601))
* **ABAWEBAPPS-210**
  *  Añadir tracking en campus leveltest ((ae6d308e))
  *  Eliminar el A/B test del test de nivel. 	- Borrar cookie levelTestDone 	- Modificar footer para que aparezca Test de nivel 	- Modificar popup welcome para que aparezca Test de nivel ((33dd427a))
* **ABAWEBAPPS-219**  Don't duplicate REF sufix on allPago refund ref. ((384c13bc))
* **ABAWEBAPPS-229**
  *  Add default language ((145a5a08))
  *  Add language ((83e28ca7))
  *  Add language ((15ae5c65))
* **ABAWEBAPPS-237**  Cambiar 'from' del mail de ayuda en la extranet. ((8a76de01))
* **ABAWEBAPPS-245**
  *  Changin name on env for extranet to match with prod servers ((ba8d795b))
  *  Fixing CORS filter in extranet ((87c31f11))
* **ABAWEBAPPS-248**
  *  sadly no environment variables in JS (Work in progess) ((bb09c512))
  *  testing CORS (more) ((aaac4595))
  *  testing CORS (more) ((6a1775e1))
  *  testing CORS (more) ((519b465d))
  *  testing CORS ((7bf9c160))
  *  fixing some var typos ((507232a7))
* **ABAWEBAPPS-259**
  *  Changing target url ((d5586b28))
  *  Adding userId ((0d296118))
  *  Change help menu target. ((63eb794e))
* **ABAWEBAPPS-261**
  *  Intranet URL from env. ((4b2731bf))
  *  JWT Login ((57be8641))
  *  Pending ((5868250e))
* **ABAWEBAPPS-282**  Implementación en extranet de una mejora en el currency. Ahora mismo esta haciendo el mismo funcionamiento que Campus. Modificado el modelo de HeViews, donde se hacia la llamada a un modelo del campus modificado y ahora apunta a uno que está en common. ((09f7d8c8))
* **ABAWEBAPPS-296**
  *  Eliminar los campos dinamicos de Zendesk. ((dc322f71))
  *  Some optimizations ((15059bdf))
  *  Some optimizations ((08c77e8f))
  *  Cron to update zendesk users. ((65b269f9))
  *  Cron to update zendesk users. ((8023ffe6))
  *  Working list of users ((72b8723f))
  *  Pending ((066b9007))
  *  Pending ((93ee6dfc))
  *  Pending ((63e349af))
* **ABAWEBAPPS-303**
  *  Solved ((db068b9d))
  *  Delete some extra code. ((921f645a))
  *  Change production private key ((50561fbc))
  *  Change production private key ((87ec0ef8))
  *  Show Zendesk Help Center for english students. ((ffaeb31e))
  *  To test on Integration ((59b0fd28))
* **ABAWEBAPPS-304 **
  *  Another fix (sorry lads for so many commits here) ((2af02043))
  *  Fixing parameters names from website ((bc469803))
  *  Fixing problem (round 1) ((880b022b))
  *  Env variable case ((32f872d1))
  *  Fixing endpoints for different environments ((25dcf526))
* **ABAWEBAPPS-324**
  *  Export Chinese CSV on Extranet. ((a1f14016))
  *  Another test ((8320b3dd))
  *  Changing shared folder on custom_grafments ((5f4b8136))
  *  Changing chinese charset code ((d76139ab))
  *  Changing tmp dir value (II) ((c8147fa1))
  *  Changing tmp dir value (II) ((f5b49ba9))
  *  Changing tmp dir value ((52454beb))
  *  Changing a var value ((5dd79c3c))
  *  Add new option to export of CSV in chinese charset ((67b0309c))
* **ABAWEBAPPS-326**
  *  Languages ((c5bf221b))
  *  New error msg deleting students ((2a5b40d8))
* **ABAWEBAPPS-361**
  *  Cambiar DB de consulta en cron Corporate ((c8dc22ab))
  *  Use DBSlave ((2b47968b))
* **ABAWEBAPPS-397**  Adding GTM to campus login page ((c71da4ce))
* **ABAWEBAPPS-428**
  *  :moyai: Student level can now be selected from extranet by admins (and stored in extranet database) ((600b6a83))
  *  :moyai: Student level can now be selected from extranet by admins ((a0785bed))
* **ABAWEBAPPS-441**
  *  Modificar Configuración Promocodes Intranet ((da555f59))
  *  Pending ((89175923))
* **ABAWEBAPPS-442**  add product plan variable to datalayer of tag manager ((9077601c))
* **ABAWEBAPPS-461 **  Adding CoolaData tracker to campus ((8b02f4e8))
* **ABAWEBAPPS-468**  Problema con la extranet. ((38103189))
* **ABAWEBAPPS-470**
  *  New vagrant config.yaml ((3e1b531f))
  *  Adding changes again. ((b7967a3f))
* **ABAWEBAPPS-483 **
  *  Deleting debug dumps ((80ec9973))
  *  Defining null value as null and not like a string ((27c2063c))
  *  Adding trackers for testing ((1e1af32f))
* **ABAWEBAPPS-495**
  *  Update student mail. Add Selligent update ((a5bb8045))
  *  Update student mail ((a48c47b5))
  *  Cambio emails en extranet ((cc94ad3e))
  *  Cambio emails en extranet ((fb2fa459))
* **ABAWEBAPPS-496**
  *  Ready to production. ((f213e139))
  *  Fix an error with autologin. ((42e27420))
  *  Error autolog ((4ab7f4d2))
  *  Error autolog ((1b7a3ca3))
  *  Error with incorrect autologin key ((1003565e))
  *  Error with incorrect autologin key ((b24e594f))
* **ABAWEBAPPS-506**  Delete extranet ((bc209391))
* **ABAWEBAPPS-521**
  *  Fix some values. ((a3a12a94))
  *  Add idPromoCode value ((350aabc6))
  *  Problema pixel criteo ((f121d2df))
* **ABAWEBAPPS-527**  Pending ((8e9ffc1e))
* **Hotfix 4.130.1**  Update generate NewPassword, suprime L,l,i,... ((573f654c))
* **RELEASE**  Release ABAWEBAPPS 4.130.0 ((bdbff4bd))
* **phpunit**
  *  applying patch for fix in YII 1.1.16 for Yii issue #3288 ((700bd70b))
  *  recovering the tests ((ed35da19))
* **tinypng**  optimizing campus images ((8a587714))



<a name="4.109.2"></a>
## 4.109.2 (2015-09-15)


#### Bug Fixes

* **6067**  Missing flash in "Is your computer ready for ABA English" page ((3b57926a))

<a name=""></a>
##  (2015-09-15)

#### Features

* **5659**
  *  Hacer funcionar la detección de IP detrás del Elastic Load Balancer ((499803a8))
  *  Hacer funcionar la detección de IP detrás del Elastic Load Balancer ((0c936ce7))
  *  Hacer funcionar la detección de IP detrás del Elastic Load Balancer ((edd7ea99))
  *  Hacer funcionar la detección de IP detrás del Elastic Load Balancer ((7ee08ec4))
  *  Hacer funcionar la detección de IP detrás del Elastic Load Balancer ((32c5c75b))
  *  Hacer funcionar la detección de IP detrás del Elastic Load Balancer ((259b522f))
  *  Hacer funcionar la detección de IP detrás del Elastic Load Balancer ((c41140ce))
  *  Hacer funcionar la detección de IP detrás del Elastic Load Balancer ((b4dfa0c8))
  *  Hacer funcionar la detección de IP detrás del Elastic Load Balancer ((9c3d4d3b))
* **5861**  Verificar si el ZEUS respeta la cabecera HTTP_X_FORWARDED_PROTO ((5b682ef4))
* **5862**
  *  recuperar los filtros abahttpfiler y abahttpsfilter ((3bb4b736))
  *  recuperar los filtros abahttpfiler y abahttpsfilter ((c147d64a))
  *  recuperar los filtros abahttpfiler y abahttpsfilter ((8c2e5d72))
  *  recuperar los filtros abahttpfiler y abahttpsfilter ((b1d2f679))
* **5863**
  *  Configurar el puphpet para que permita conexiones por el 80 además de las del 443 ((f0142571))
  *  Configurar el puphpet para que permita conexiones por el 80 además de las del 443 ((d77bcf3a))
* **5864**  Porqué "language" es una cookie? ((a7e41350))
* **5986**
  *  Reportar los errores HTTP al cliente, a GoogleAnalytics y a los sistemas de logs correspondientes. ((4cb07fb6))
  *  Reportar los errores HTTP al cliente, a GoogleAnalytics y a los sistemas de logs correspondientes. ((007bd817))
  *  Reportar los errores HTTP al cliente, a GoogleAnalytics y a los sistemas de logs correspondientes. ((4c8189c2))
  *  Reportar los errores HTTP al cliente, a GoogleAnalytics y a los sistemas de logs correspondientes. ((9aa28ebd))
* **6033**  Add robots.txt ((d0492001))

#### Bug Fixes

* **5429**
  *  Está funcionando el rememberMe? ((032b25e6))
  *  Está funcionando el rememberMe? ((5c685090))
  *  Está funcionando el rememberMe? ((5ea151f0))
* **5429): Está funcionando el rememberMe? fix(5813): Revisar autologin campus desde la web fix(4401**
  *  Cookies duplicadas ((fdd03bbc))
  *  Cookies duplicadas ((2e16a2fb))
* **5644**  Precios Web España ((add0047e))
* **5725**  Cuando YII_DEBUG está habilitado y se utiliza showInFireBug en el CWebLogRoute aparece código HTML al final de cada JSON de cada llamada REST ((0675c709))
* **5726**  Add support for PHP with FastCGI Process Manager ((d069d5ab))
* **5875**  error autologin monitis ((c00332f0))
* **5921**
  *  Error 500 en contenidos de profile/progress ((9e0a9d28))
  *  Error 500 en contenidos de profile/progress ((023e0c5b))
* **android-adjust-tracking**  add the proper value for the partnerId for Android users ((45c8e3be))
* **assets**  add CDN for all the major JS & CSS libraries ((dad2707e))
* **assets_hash**  trying to have the same asset hash for all the cluster nodes ((d8542fe0))
* **etags**
  *  Remove INode from the hash used to generate ETags. ((86d3d192))
  *  Remove INode from the hash used to generate ETags. ((18168fa1))
* **hashByName**  Force to always hashByName instead of using the dirname of the path being published AND path mtime. ((8c5c94ba))



<a name="4.100.5"></a>
## 4.100.5 (2015-07-23)


#### Bug Fixes

* **5644**  Precios Web España ((add0047e))
* **android-adjust-tracking**  add the proper value for the partnerId for Android users ((45c8e3be))

