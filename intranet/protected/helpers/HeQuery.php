<?php

class HeQuery
{

    static function isParcialSuccess($payId)
    {
        if (substr($payId, 0, 1) == 'S') {
            return true;
        }
        return false;
    }

    static function testRegisterSuccess($userId, $payId)
    {

        $model = AbaPayments::getPayment($userId, $payId);

        if ($model->status == PAY_SUCCESS) {
            //
            // ...aun que no se de el caso...
            //
            if (self::isParcialSuccess($payId) AND $model->amountOriginal == $model->amountPrice) {
                //filter the SUCCESS from refund;
                return false;
            }

            $modelReferer = AbaPayments::getRefererRefundPayment($userId, $payId);
            if ($modelReferer->idPayControlCheck == $payId) {
                return false;
            }
            return true;
        }
        return false;
    }

    static function testRegisterPending($id, $payment)
    {
        $model = AbaPayments::getPayment($id, $payment);
        if ($model->status == HeList::findValue(HeList::statusDescription(), "PENDING")) {
            return true;
        }
        return false;
    }

    static function checkPaymentCanReconcile($userId, $idPayment)
    {
        $moPayment = AbaPayments::getPayment($userId, $idPayment);
        if ($moPayment->status == PAY_SUCCESS || $moPayment->status == PAY_REFUND) {
            if ($moPayment->paySuppLinkStatus == PAY_RECONC_ST_NOT_MATCHED || empty($moPayment->paySuppLinkStatus)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $paySuppExtId
     * @param $status
     *
     * @return bool
     */
    static function checkForDirectPayment($paySuppExtId, $status)
    {
        if( $paySuppExtId == PAY_SUPPLIER_CAIXA AND ($status == PAY_PENDING OR $status == PAY_FAIL OR $status == PAY_CANCEL) ) {

            $stUser = explode(",", trim(Yii::app()->config->get("INTRANET_DIRECTPAYMENT_USERS")));
            // $oAbaWebUser = new AbaWebUser();
            if(trim(Yii::app()->user->getId()) == "root" OR in_array(Yii::app()->user->idUser, $stUser)) { // $oAbaWebUser->idUser
                return true;
            }
        }
        return false;
    }

    /** Used from view payments/admin to know if still the
     * recurring payment has not been executed.
     *
     * @param $userId
     * @param $idPayment
     * @return bool
     */
    static function checkPaymentCanChangeCredit($userId, $idPayment)
    {
        $moPayment = AbaPayments::getPayment($userId, $idPayment);
        if ($moPayment->status == PAY_PENDING && HeDate::compareDateString(HeDate::todaySQL(), $moPayment->dateToPay)
          && $moPayment->paySuppExtId != PAY_SUPPLIER_ADYEN
          && $moPayment->paySuppExtId != PAY_SUPPLIER_ADYEN_HPP
          && $moPayment->paySuppExtId != PAY_SUPPLIER_ALLPAGO_BR
          && $moPayment->paySuppExtId != PAY_SUPPLIER_ALLPAGO_MX
        ) {
            return true;
        }
        else if($moPayment->paySuppExtId != PAY_SUPPLIER_CAIXA) {
            return true;
        }
        return false;
    }

    /**
     * @param $userId
     * @param $idPayment
     *
     * @return bool
     */
    static function checkPaymentHasCreditCard($userId, $idPayment)
    {
        $moPayment = AbaPayments::getPayment($userId, $idPayment);

        if ($moPayment->paySuppExtId != PAY_SUPPLIER_ADYEN AND $moPayment->paySuppExtId != PAY_SUPPLIER_ADYEN_HPP AND $moPayment->paySuppExtId != PAY_SUPPLIER_ALLPAGO_BR AND $moPayment->paySuppExtId != PAY_SUPPLIER_ALLPAGO_MX AND $moPayment->paySuppExtId != PAY_SUPPLIER_CAIXA) {
            return true;
        }
        return false;
    }

    /**
     * @param $userId
     *
     * @return bool
     */
    static function checkPaymentCanCancelRecurring($userId)
    {
        $moPayment = AbaPayments::getModelCancelFailUser($userId);

        if(isset($moPayment->paySuppExtId) AND ($moPayment->paySuppExtId == PAY_SUPPLIER_PAYPAL OR $moPayment->paySuppExtId == PAY_SUPPLIER_APP_STORE)) {
            return false;
        }
        return true;
    }

    /**
     * @param $status
     *
     * @return bool
     */
    static function checkForChargebackRefund($status, $eventCode)
    {
        if( $status <> AbaPaymentsAdyenFraud::STATUS_MANUALLY_REFUNDED AND $eventCode <> AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_CHARGEBACK_REVERSED ) {
            return true;
        }
        return false;
    }

    /**
     * @param $sStr
     * @param int $iLength
     * @param bool|false $bWithLink
     * @param string $eventCode
     *
     * @return string
     */
    static function formatField($sStr, $iLength=50, $bWithLink=false, $eventCode="")
    {
        $sNstr = $sStr;

        if(mb_strlen($sNstr) > $iLength) {
            $sNstr = mb_substr($sStr, 0, $iLength) . "...";
        }
        if($bWithLink) {
            if(preg_match("/REPORT/i", $eventCode)) {
                $sNstr = '<a href="' . $sStr . '" target="_blank"> ' . $sNstr . "</a>";
            }
        }
        return $sNstr;
    }

}
