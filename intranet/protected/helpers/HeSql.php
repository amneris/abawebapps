<?php

class HeSql
{
    static function getDBValue($table,$keyfield, $field, $value)
	{
		$connection = Yii::app()->db;
		$sql= "SELECT $field, count(*) as num FROM $table WHERE $keyfield='$value'";
		$command = $connection->createCommand($sql);		
        $dataReader=$command->query();
		if(($row = $dataReader->read())!==false)
			return $row["$field"];
		return "";
	}
	 static function getDBRow($table,$keyfield, $value)
	{
		$connection = Yii::app()->db;
		$sql= "SELECT *, count(*) as num FROM $table WHERE $keyfield='$value'";
		$command = $connection->createCommand($sql);		
        $dataReader=$command->query();
		if(($row = $dataReader->read())!==false)
			return $row;
		return array();
	}
	static function getDBFields($table,$keyfield, $value)
	{
		$connection = Yii::app()->db;
		$sql= "SELECT *, count(*) as num FROM $table WHERE $keyfield='$value'";
		$command = $connection->createCommand($sql);		
        $dataReader=$command->query();
		$row = $dataReader->read(); 
		return $row;
	}
	static function getUser($id)
	{
		//return HeSql::getDBValue('user','id', 'email', $id);
		$row=HeSql::getDBRow('user','id', $id);
		$theRet="<table class='tooltipGrid'><tr><td>id: </td><td>$id</td></tr>";
		$theRet.="<tr><td>email: </td><td>".$row['email']."</td></tr>";
		$countries=HeList::getCountryList();
		$userTypes=HeList::userTypeDescription();
		$theRet.="<tr><td>country:</td><td>".$countries[$row['countryId']]."</td></tr>";
		$theRet.="<tr><td>UserType:</td><td>".$userTypes[$row['userType']]."</td></tr>";
		$theRet.="</table>";
		return $theRet;
	}
	static function tooltipAES2($inText)
	{	
		try
		{
			$var =AbaUserCreditForms::AES_decode($inText);
			if($var==$inText)
			{
				$var='null';
			}
		}	
		catch(Exception $e)
		{	
			return "";
		}
		return $var;	
	}
	static function tooltipAES($id, $inField, $inTable='payments_control_check', $inFieldKey="id")
	{	
		try
		{		
			$connection = Yii::app()->db;
			$sql="SELECT AES_DECRYPT($inField, '".WORD4CARDS."') as data FROM payments_control_check p where $inFieldKey = '$id'";
		
			$command = $connection->createCommand($sql);		
			$dataReader=$command->query();
			if(($row = $dataReader->read())!==false) 
			{ 
				if($row["data"] == null)
					return "null";
				return $row["data"];
			}			
		}
		catch(Exception $e)
		{	
		}
		return "";	
	}
	static function getBank($id)
	{	
		try
		{		
			$connection = Yii::app()->db;
			$sql="SELECT AES_DECRYPT(cardNumber, '".WORD4CARDS."') as data FROM payments_control_check p where id = '$id'";
		
			$command = $connection->createCommand($sql);		
			$dataReader=$command->query();
			if(($row = $dataReader->read())!==false) 
			{ 
				if($row["data"] == null)
					return 'Unknow';
				$card=$row["data"];
				$sql="SELECT description FROM pay_card_known_types p where prefix = SUBSTRING('$card',1, LENGTH(prefix));";
				$command = $connection->createCommand($sql);		
				$dataReader=$command->query();
				if(($row = $dataReader->read())!==false) 
					return $row['description'];
			}
		}
		catch(Exception $e)
		{	
		}
		return 'Unknow';	
	}
}
