<?php

class HeList
{
    static function getRenderNames()
    {
        $modelPartnerList = new AbaPartnersList;
        $modelSoursesList = new SourcesList();
        $partnerList = $modelPartnerList->getFullList();
        $sourcesList = $modelSoursesList->getFullList();
        $modelCountry = new AbaCountry;
        $countryList = $modelCountry->getFullList();
        $countryUeList = $modelCountry->getFullUeList();
        $modelTeachers = new AbaUserTeacher;
        $teacherList = $modelTeachers->getFullList();
        $theRenders = array(
          'statusDescription' => HeList::statusDescription(),
          'statusDescriptionFull' => HeList::statusDescriptionFull(),
          'countryDescription' => $countryList,
          'countryUeDescription' => $countryUeList,
          'levelDescription' => HeList::levelsDescription(),
          'completeDescription' => HeList::completeDescription(),
          'userTypeDescription' => HeList::userTypeDescription(),
          'languageDescription' => HeList::languageDescription(),
          'languageEnvDescription' => HeList::languageEnvDescription(),
          'userTypeList' => HeList::userTypeDescription(),
          'cardsList' => HeList::creditCardDescription(),
          'teacherList' => $teacherList,
          'partnerList' => $partnerList,
          'countryList' => $countryList,
          'countryUeList' => $countryUeList,
          'sourceList' => $sourcesList,
          'boletosStates' => HeList::boletosStates(),
          'partnerGroupList' => HeList::partnerGroupList(),
          'channelGroupList' => HeList::channelGroupList(),
          'typePromoList' => HeList::typePromoList(),
          'booleanList' => HeList::booleanList(),
          'nextYearsList' => HeList::nextYears(),
          'numMonthList' => HeList::numMonth(),
          'genderList' => HeList::getGenderList(),
          'typeCpfList' => HeList::getTypeCpfList(),
          'supplierExtGatewayList' => HeList::supplierExtGatewayList(),
          'reconciliationStatusList' => HeList::reconciliationStatusList(),
          'recurringList' => HeList::recurringList(),
          'cancelOriginList' => HeList::cancelOriginList(),
          'YesNoBoolList' => HeList::getYesNoAliasBool(),
          'promosDescriptionList' => HeList::getPromosDescriptionList(),
          'getPromoTypes' => HeList::getPromoTypes(),
          'adyenFraudsStates' => HeList::adyenFraudsStates(),
          'statusExperimentsVariations' => HeList::statusExperimentsVariations(),
          'experimentsVariationsList' => HeList::getExperimentsVariationsList(),
          'userExperimentsVariations' => HeList::statusUserExperimentsVariations(),
          'experimentsList' => HeList::getExperimentsList(),
          'experimentsCategoriesList' => HeList::experimentsCategoriesList(),
          'experimentsStatusList' => HeList::experimentsStatusList(),
          'experimentsRandomModeList' => HeList::experimentsRandomModeList(),
          'experimentsAvailableModeList' => HeList::experimentsAvailableModeList(),
          'experimentsTypesList' => HeList::experimentsTypesList(),
          'adyenHppPaymentsMethodList' => HeList::adyenHppPaymentsMethodList(),
          'userProgressQueuesList' => HeList::userProgressQueuesList(),
          'userProgressQueuesListFormat' => HeList::userProgressQueuesListFormat(),
          'userProgressQueuesDetailsList' => HeList::userProgressQueuesDetailsList(),
          'userProgressQueuesDetailsListFormat' => HeList::userProgressQueuesDetailsListFormat(),
          'userProgressQueuesCurrentListFormat' => HeList::userProgressQueuesCurrentListFormat(),

          'partnerCategoryList' => HeList::partnerCategoryList(),
          'partnerBusinessAreaList' => HeList::partnerBusinessAreaList(),
          'partnerTypeList' => HeList::partnerTypeList(),
          'partnerNewGroupList' => HeList::partnerNewGroupList(),
          'partnerChannelList' => HeList::partnerChannelList(),
          'partnerCountryList' => HeList::partnerCountryList(),
        );
        return $theRenders;
    }

    static function findByKey($list, $key, $defaultReturn = 0, $strictSearch = false)
    {
        if (array_key_exists($key, $list)) {
            return $list[$key];
        }
        return $defaultReturn;
    }

    static function findValue($list, $value, $defaultReturn = 0, $strictSearch = false)
    {
        if (in_array($value, $list)) {
            return array_search($value, $list, $strictSearch);
        }
        return $defaultReturn;
    }

    static function nextYears($len = 20)
    {
        $theArray = array();
        $year = Date("Y");
        for ($i = 0; $i < $len; $i++) {
            $key = $year + 1;
            $value = $key;
            $theArray[$key] = $value;
        }
        return $theArray;
    }

    static function numMonth()
    {
        return array(
          1 => "1",
          2 => "2",
          3 => "3",
          4 => "4",
          5 => "5",
          6 => "6",
          7 => "7",
          8 => "8",
          9 => "9",
          10 => "10",
          11 => "11",
          12 => "12"
        );
    }

    static function actionsPayments()
    {
        if (Yii::app()->user->getId() == "support") {
            return array('CANCEL' => 'CANCEL');
        } else {
            return array('CANCEL' => 'CANCEL', 'FAIL' => "FAIL");
        }
    }

    static function booleanList()
    {
        return array(1 => 'TRUE', 0 => 'FALSE');
    }

    static function typePromoList()
    {
        return array('FINALPRICE' => 'FINALPRICE', 'PERCENTAGE' => 'PERCENTAGE', 'AMOUNT' => 'AMOUNT');
    }

    static function statusDescription()
    {
        return array(0 => 'PENDING', 10 => 'FAIL', 30 => 'SUCCESS', 40 => 'REFUND', 50 => 'CANCEL');
    }

    static function statusInvoicesDescription()
    {
        return array(30 => 'SUCCESS', 40 => 'REFUND');
    }

    static function statusDescriptionFull()
    {
        return array(0 => 'PENDING', 5 => 'INITIATE', 10 => 'FAIL', 30 => 'SUCCESS', 40 => 'REFUND', 50 => 'CANCEL');
    }

    static function creditCardDescription()
    {
        return array(
          KIND_CC_VISA => 'VISA',
          KIND_CC_MASTERCARD => 'MASTERCARD',
          KIND_CC_AMERICAN_EXPRESS => 'AMERICAN EXPRESS',
          KIND_CC_DINERS_CLUB => 'DINERS CLUB',
          KIND_CC_PAYPAL => 'PAYPAL',
          KIND_CC_MAESTRO => 'MAESTRO',
          KIND_CC_VISA_ELECTRON => 'VISA ELECTRON',
          KIND_CC_DISCOVER => 'DISCOVER',
          KIND_CC_JCB => 'JCB',
          KIND_CC_ELO => 'ELO',
          KIND_CC_BOLETO => 'BOLETO-BR',
          KIND_CC_NO_CARD => 'UNKNOWN',
          KIND_CC_APPSTORE => 'APP STORE',
          KIND_CC_UATP => 'ADYEN UATP',
          KIND_CC_CUP => 'ADYEN China Union Pay',
          KIND_CC_VIAS => 'ADYEN VIAS'
        );
    }

    static function creditCardShortDescription()
    {
        return array(
          KIND_CC_VISA => 'VS',
          KIND_CC_MASTERCARD => 'MD',
          KIND_CC_AMERICAN_EXPRESS => 'AE',
          KIND_CC_DINERS_CLUB => 'DC',
          KIND_CC_PAYPAL => 'PP',
          KIND_CC_MAESTRO => 'MO',
          KIND_CC_VISA_ELECTRON => 'VE',
          KIND_CC_DISCOVER => 'DR',
          KIND_CC_JCB => 'JC',
          KIND_CC_ELO => 'EL',
          KIND_CC_BOLETO => 'BLBR',
          KIND_CC_NO_CARD => 'UKNW',
          KIND_CC_APPSTORE => 'APST',
          KIND_CC_UATP => 'ADUATP',
          KIND_CC_CUP => 'ADCUP',
          KIND_CC_VIAS => 'ADVIAS'
        );
    }

    static function partnerGroupList()
    {
//        $modelExperiments = new AbaPartnergroups();
//        return $modelExperiments->getPartnerGroups();

        return array(
              PARTNER_ABA_G => ucfirst(PARTNER_ABA_W),
              PARTNER_GROUPON_G => ucfirst(PARTNER_GROUPON_W),
              PARTNER_SOCIAL_G => ucfirst(PARTNER_SOCIAL_W),
              PARTNER_AFFILIATES_G => ucfirst(PARTNER_AFFILIATES_W),
              PARTNER_B2B_G => ucfirst(PARTNER_B2B_W),
              PARTNER_POSTAFFILIATES_G => ucfirst(PARTNER_POSTAFFILIATES_W),
              PARTNER_ABAMOBILE_G => ucfirst(PARTNER_ABAMOBILE_W),
              PARTNER_OTHER_G => ucfirst(PARTNER_OTHER_W)
        );
    }

    static function boletosStates()
    {
        return array('all' => 'All states', '0' => 'Default', '10' => 'Expired', '30' => 'Used');
    }

    static function invoicesPartnerGroupList()
    {
        return array(PARTNER_GROUP_B2B => "B2B", PARTNER_GROUP_B2C => "B2C");
    }

    static function channelGroupList()
    {
        return array(
          CHANNEL_ABA_G => ucfirst(CHANNEL_ABA_W)
        );
    }

    static function levelsDescription()
    {
        return array(
          0 => 'Unknown',
          1 => 'Beginners',
          2 => 'Lower intermediate',
          3 => 'Intermediate',
          4 => 'Upper intermediate',
          5 => 'Advanced',
          6 => 'Business'
        );
    }

    /** List of possible values to be selected for column user.userType. In case
     *  a current userType is passed by, then we apply some filter to which could be selected.
     *
     * @return array
     */
    static function userTypeDescription()
    {
        return array(0 => 'Deleted', 1 => 'Free', 2 => 'Premium', 4 => 'Teacher');
    }

    /** Edition list values available depending on current user Type.
     * @param null $curUserType
     *
     * @return array
     */
    static function userTypeForEdition($curUserType = null)
    {
        if (is_null($curUserType)) {
            return array(0 => 'Deleted', 1 => 'Free', 2 => 'Premium', 4 => 'Teacher');
        } elseif ($curUserType == DELETED) {
            return array(0 => 'Deleted');
        } elseif ($curUserType == FREE) {
            return array(1 => 'Free');
        } elseif ($curUserType == PREMIUM) {
            return array(1 => 'Free', 2 => 'Premium');
        } else {
            return array(0 => 'Deleted', 1 => 'Free', 2 => 'Premium', 4 => 'Teacher');
        }
    }

    /** Returns description of cancellation
     * @return array
     */
    static function cancelReasonDescription()
    {
        return array(
          null => 'NULL',
          PAY_CANCEL_USER => 'User cancellation',
          PAY_CANCEL_DATEOFF_GROUPON => 'Expiration Groupon',
          PAY_CANCEL_FAILED_RENEW => 'Cancellation failed or expired',
          PAY_CANCEL_UNDEFINED => 'Undefined'
        );
    }

    /**
     * Returns sources of registration
     *
     * @return array
     */
    static function registrationSourceDescription()
    {
        return array(
          null => 'NULL',
          REGISTER_USER => 'Web registration',
          REGISTER_USER_FROM_LEVELTEST => 'Level Test',
          REGISTER_USER_FROM_PARTNER => 'Partner, affiliate or groupon',
          REGISTER_USER_FROM_B2B => 'Partner, affiliate or B2B',
          REGISTER_USER_FROM_B2B_EXTRANET => 'Partner, affiliate or extranet',
          REGISTER_USER_FROM_FACEBOOK => 'Register with facebook'
        );
    }

    static function languageDescription()
    {
        return AbaCountry::getDefaultLanguages();
    }

    static function languageEnvDescription()
    {
        return AbaCountry::getDefaultLanguages();
    }

    static function completeDescription()
    {
        return array('YES' => 'YES', 'NO' => 'NO');
    }

    static function getCountryList()
    {
        $modelCountry = new AbaCountry;
        return $modelCountry->getFullList();
    }

    static function getUeCountryList()
    {
        $modelCountry = new AbaCountry;
        return $modelCountry->getFullUeList();
    }

    static function getPartnerList()
    {
        $modelPartnerList = new AbaPartnersList;
        return $modelPartnerList->getFullList();
    }

    static function getPartnerListWithoutFlashSales()
    {
        $modelPartnerList = new AbaPartnersList;
        return $modelPartnerList->getListWithoutFlashSales();
    }

    static function getTeacherList()
    {
        $modelTeachers = new AbaUserTeacher;
        return $modelTeachers->getFullList();
    }

    static function getGenderList()
    {
        return array('M' => 'Male', 'F' => 'Female');
    }

    static function getPromoTypes()
    {
        return array(
          AbaProductsPromos::PROMO_TYPE_GENERAL => 'General',
          AbaProductsPromos::PROMO_TYPE_FAMILYPLAN => 'Family Plan',
          AbaProductsPromos::PROMO_TYPE_ALL => 'All'
        );
    }

    /**
     * @param bool $bForListData
     * @param null $id
     *
     * @return array|string
     */
    static function getPromosDescriptionList($bForListData = false, $id = null)
    {
        $modelPromosDescription = new AbaProductsPromosDescription();

        if (is_numeric($id)) {
            if ($bForListData) {
                foreach ($modelPromosDescription as $iKey => $stDescription) {
                    if ($stDescription['id'] == $id) {
                        return $stDescription['title'];
                    }
                }
            } else {
                foreach ($modelPromosDescription as $iKey => $stDescription) {
                    if ($iKey == $id) {
                        return $stDescription;
                    }
                }
            }
            return "";
        } else {
            if ($bForListData) {
                return $modelPromosDescription->getDescriptionTypes();
            } else {
                return $modelPromosDescription->getFullList();
            }
        }
    }

    /**
     * @param bool $bForListData
     *
     * @return array
     */
    static function getExperimentsVariationsList($bForListData = false)
    {
        $modelExperimentsVariations = new AbaExperimentsVariations();

        if ($bForListData) {
            return $modelExperimentsVariations->getExperimentsVariations();
        } else {
            return $modelExperimentsVariations->getFullList();
        }
    }

    static function getExperimentsList($bForListData = false)
    {
        $modelExperiments = new AbaExperiments();

        if ($bForListData) {
            return $modelExperiments->getExperiments();
        } else {
            return $modelExperiments->getFullList();
        }
    }

    /** Returns the methods of payment.
     * @return array
     */
    static function supplierExtGatewayList()
    {
        $moSuppliers = new AbaPaySuppliers();
        $aNames = $moSuppliers->returnAllSuppliers();

        foreach ($aNames as $aMethod) {
            $aMethods[$aMethod['id']] = $aMethod['name'];
        }

        return $aMethods;
    }

    /**
     * @param $data
     *
     * @return string
     */
    static function getPaymentIcon($data)
    {

        if ($data->status == PAY_REFUND) {
            return "";
        }

        if ((bool)preg_match('/^DP.+$/', trim($data->id))) {
            return Yii::app()->request->baseUrl . "/images/direct_payment.png";
        }

        if ($data->isExtend == 1) {
            return Yii::app()->request->baseUrl . "/images/small-extra-payment.png";
        }

        if ($data->isRecurring == 1) {
            if (AbaPayments::didChangeProduct($data->userId, $data->idUserCreditForm)) {
                return Yii::app()->request->baseUrl . "/images/updatePro.png";
            } else {
                return Yii::app()->request->baseUrl . "/images/recurring_credit.gif";
            }
        } else {
            return Yii::app()->request->baseUrl . "/images/new.gif";
        }
    }

    /**
     * @param $data
     *
     * @return string
     */
    static function getPaymentIconCss($data)
    {

        if ((bool)preg_match('/^DP.+$/', trim($data->id))) {
            return "width: 48px; height: 36px;";
        }

        if ($data->isRecurring <> 1) {
            return "width: 45px; height: 34px;";
        }

        return "width: 36px; height: 34px;";
    }

    static function getReference($data)
    {
        return $data->paySuppExtProfId;
    }

    /** Returns the possible status of reconciliation for a payment.
     * @return array
     */
    static function reconciliationStatusList()
    {
        $status = array(
          PAY_RECONC_ST_MATCHED => "MATCHED",
          PAY_RECONC_ST_NOT_MATCHED => "NOT MATCHED",
          PAY_RECONC_ST_NOT_PROCESSED => "NOT MATCHED*",
          null => "NOT MATCHED*",
        );

        return $status;
    }

    /** It returns possible values for field payment.isRecurring
     * @return array
     */
    static function recurringList()
    {
        $yesNo = array(1 => 'Yes', 0 => 'No', null => 'Unknown');

        return $yesNo;
    }


    /** It returns Yes no for Bool MYSQL values
     * @return array
     */
    static function getYesNoAliasBool()
    {
        $yesNo = array(1 => 'Yes', 0 => 'No');

        return $yesNo;
    }

    /**
     * @return array
     */
    static function getFileLogReconciliationStatus()
    {
        $allStatus = array(
          PAY_FILE_STATUS_MATCHED => PAY_FILE_STATUS_MATCHED,
          PAY_FILE_STATUS_INCONSISTENT => PAY_FILE_STATUS_INCONSISTENT,
          PAY_FILE_STATUS_NOTFOUND => PAY_FILE_STATUS_NOTFOUND,
          PAY_FILE_STATUS_PREV_MATCHED => PAY_FILE_STATUS_PREV_MATCHED,
          PAY_FILE_STATUS_ERROR => PAY_FILE_STATUS_ERROR
        );

        return $allStatus;
    }

    /**
     * @return array
     */
    static function getTypeCpfList()
    {
        $tipos = array('CPF' => 'CPF', 'CNPJ' => 'CNPJ', 'NIF' => 'NIF');

        return $tipos;
    }

    /**
     * @return array
     */
    static function cancelOriginList()
    {
        $allStatus = array(
          STATUS_CANCEL_USER => 'Student cancellation',
          STATUS_CANCEL_EXPFAIL => 'Failure on renewal process',
          STATUS_CANCEL_IPN => 'IPN notification Paypal',
          STATUS_CANCEL_INTRANET => 'Intranet user manually',
          STATUS_CANCEL_CHANGEPROD => 'Upgrade/Downgrade',
          STATUS_CANCEL_CONTRACT_NEW_PLAN => 'Change to Family Plan',
        );
        return $allStatus;
    }

    /**
     * @return array
     */
    static function adyenFraudsStates()
    {
        $stStatus = array(
          AbaPaymentsAdyenFraud::STATUS_PENDING => 'Pending',
          AbaPaymentsAdyenFraud::STATUS_MANUALLY_REFUNDED => 'Refunded',
          AbaPaymentsAdyenFraud::STATUS_INITIATED_DISPUTE => 'Iniciated Dispute',
          AbaPaymentsAdyenFraud::STATUS_SUCCESS_AFTER_DISPUTE => 'Success After Dispute',
        );
        return $stStatus;
    }

    /**
     * @return array
     */
    static function statusExperimentsVariations()
    {
        return array(
          AbaExperimentsVariations::GROUP_STATUS_INTEST => 'IN LIVE',
          AbaExperimentsVariations::GROUP_STATUS_FAIL_TEST => 'FAIL TEST',
          AbaExperimentsVariations::GROUP_STATUS_SUCCESS_TEST => 'SUCCESS TEST'
        );
    }

    /**
     * @return array
     */
    static function statusUserExperimentsVariations()
    {
        return array(
          AbaUserExperimentsVariations::USER_EXPERIMENT_VARIATION_STATUS_DEFAULT => 'Default',
        );
    }

    /**
     * @param bool|false $bForListData
     *
     * @return array
     */
    static function experimentsCategoriesList($bForListData = false)
    {
        $modelExperimentsUserCategories = new AbaExperimentsUserCategories();

        if ($bForListData) {
            return $modelExperimentsUserCategories->getExperimentsUserCategories();
        } else {
            return $modelExperimentsUserCategories->getFullList();
        }
    }

    /**
     * @param bool|false $bForListData
     *
     * @return array
     */
    static function experimentsTypesList($bForListData = false)
    {
        $modelExperimentsTypes = new AbaExperimentsTypes();

        if ($bForListData) {
            return $modelExperimentsTypes->getExperimentsTypes();
        } else {
            return $modelExperimentsTypes->getFullList();
        }
    }

    /**
     * @return array
     */
    static function experimentsStatusList()
    {
        return array(
          AbaExperiments::EXPERIMENT_STATUS_ENABLED => "Enabled",
          AbaExperiments::EXPERIMENT_STATUS_PAUSED => "Paused",
        );
    }

    /**
     * @return array
     */
    static function experimentsRandomModeList()
    {
        return array(
          AbaExperiments::EXPERIMENT_RANDOM_MODE_SEQUENTIAL => "Sequential",
          AbaExperiments::EXPERIMENT_RANDOM_MODE_PHPRANDOM => "PhpRandom",
        );
    }

    /**
     * @param bool|false $bForListData
     *
     * @return array
     */
    static function experimentsAvailableModeList($bForListData = false)
    {
        $modelExperimentsAvailabilityModes = new AbaExperimentsAvailabilityModes();

        if ($bForListData) {
            return $modelExperimentsAvailabilityModes->getExperimentsAvailabilityModes();
        } else {
            return $modelExperimentsAvailabilityModes->getFullList();
        }
    }


    /**
     * @param bool|false $bForListData
     *
     * @return array
     */
    static function adyenHppPaymentsMethodList($bForListData = false)
    {
        $modelAdyenHppPaymentMethods = new AbaAdyenHppPaymentMethods();

        if ($bForListData) {
            return $modelAdyenHppPaymentMethods->getAdyenHppPaymentMethods();
        } else {
            return $modelAdyenHppPaymentMethods->getFullList();
        }
    }

    /**
     * @return array
     */
    static function userProgressQueuesList()
    {
        return array(
          AbaUserProgressQueues::QUEUE_STATUS_PENDING => "Pending",
          AbaUserProgressQueues::QUEUE_STATUS_INPROCESS => "In progress",
          AbaUserProgressQueues::QUEUE_STATUS_FAIL => "Fail",
          AbaUserProgressQueues::QUEUE_STATUS_PROCESSED => "Done",
        );
    }

    /**
     * @return array
     */
    static function userProgressQueuesListFormat()
    {
        return array(
          AbaUserProgressQueues::QUEUE_STATUS_PENDING => "<div class='queueprogressstatuscontainer'><span class='queueprogressstatus pending'>Pending</span></div>",
          AbaUserProgressQueues::QUEUE_STATUS_INPROCESS => "<div class='queueprogressstatuscontainer'><span class='queueprogressstatus inprogress'>In progress</span></div>",
          AbaUserProgressQueues::QUEUE_STATUS_FAIL => "<div class='queueprogressstatuscontainer'><span class='queueprogressstatus fail'>Fail</span></div>",
          AbaUserProgressQueues::QUEUE_STATUS_PROCESSED => "<div class='queueprogressstatuscontainer'><span class='queueprogressstatus done'>Done</span></div>",
          AbaUserProgressQueues::QUEUE_STATUS_DELETED => "<div class='queueprogressstatuscontainer'><span class='queueprogressstatus deleted'>Deleted</span></div>",
        );
    }

    /**
     * @return array
     */
    static function userProgressQueuesDetailsList()
    {
        return array(
          AbaUserProgressQueuesDetails::QUEUEDETAIL_STATUS_PENDING => "Pending",
          AbaUserProgressQueuesDetails::QUEUEDETAIL_STATUS_INPROCESS => "In progress",
          AbaUserProgressQueuesDetails::QUEUEDETAIL_STATUS_FAIL => "Fail",
          AbaUserProgressQueuesDetails::QUEUEDETAIL_STATUS_PROCESSED => "Done",
        );
    }

    /**
     * @return array
     */
    static function userProgressQueuesDetailsListFormat()
    {
        return array(
          AbaUserProgressQueuesDetails::QUEUEDETAIL_STATUS_PENDING => "<div class='queueprogressstatuscontainer'><span class='queueprogressstatus pending'>Pending</span></div>",
          AbaUserProgressQueuesDetails::QUEUEDETAIL_STATUS_INPROCESS => "<div class='queueprogressstatuscontainer'><span class='queueprogressstatus inprogress'>In progress</span></div>",
          AbaUserProgressQueuesDetails::QUEUEDETAIL_STATUS_FAIL => "<div class='queueprogressstatuscontainer'><span class='queueprogressstatus fail'>Fail</span></div>",
          AbaUserProgressQueuesDetails::QUEUEDETAIL_STATUS_PROCESSED => "<div class='queueprogressstatuscontainer'><span class='queueprogressstatus done'>Done</span></div>",
          AbaUserProgressQueuesDetails::QUEUEDETAIL_STATUS_DELETED => "<div class='queueprogressstatuscontainer'><span class='queueprogressstatus deleted'>Deleted</span></div>",
        );
    }

    /**
     * @return array
     */
    static function userProgressQueuesCurrentListFormat()
    {
        return array(
          "" => "<span>&nbsp;</span>",
          0 => "<span>&nbsp;</span>",
          1 => "<div class='queueprogressstatuscontainer'><span class='queueprogressstatus inprogress'>Yes</span></div>",
        );
    }

    /**
     * @param $data
     *
     * @return string
     */
    static function getExternalUrl($data)
    {

        $sUrl = '#';

        $oUser = new AbaUser();

        if($oUser->getUserById($data->userId)) {

            $stUnits = explode(",", $data->units);

            if(count($stUnits) > 0) {

                $sUnit = $stUnits[0];

                if($stUnits[0] < 10) {
                    $sUnit = '00' . $stUnits[0];
                }
                else if($stUnits[0] < 100) {
                    $sUnit = '0' . $stUnits[0];
                }

                $sUrl = 'https://campus' . Yii::app()->config->get("COMMON_DOMAIN") . '/course/AllUnits/unit/' . $sUnit . '?' . MAGIC_LOGIN_KEY . '=' . $oUser->keyExternalLogin;
            }
            else {
                $sUrl = 'https://campus' . Yii::app()->config->get("COMMON_DOMAIN") . '/course/AllUnits?' . MAGIC_LOGIN_KEY . '=' . $oUser->keyExternalLogin;
            }
        }

        return $sUrl;
    }

    /**
     * @param bool|false $bForListData
     *
     * @return array
     */
    static function partnerCategoryList($bForListData = false)
    {
        $modelAbaPartnercategories = new AbaPartnercategories();

        if ($bForListData) {
            return $modelAbaPartnercategories->getPartnerCategories();
        } else {
            return $modelAbaPartnercategories->getFullList();
        }
    }

    /**
     * @param bool|false $bForListData
     *
     * @return array
     */
    static function partnerBusinessAreaList($bForListData = false)
    {
        $modelAbaPartnerbusinessareas = new AbaPartnerbusinessareas();

        if ($bForListData) {
            return $modelAbaPartnerbusinessareas->getPartnerBusinessAreas();
        } else {
            return $modelAbaPartnerbusinessareas->getFullList();
        }
    }

    /**
     * @param bool|false $bForListData
     *
     * @return array
     */
    static function partnerTypeList($bForListData = false)
    {
        $modelAbaPartnertypes = new AbaPartnertypes();

        if ($bForListData) {
            return $modelAbaPartnertypes->getPartnerTypes();
        } else {
            return $modelAbaPartnertypes->getFullList();
        }
    }

    /**
     * @param bool|false $bForListData
     *
     * @return array
     */
    static function partnerNewGroupList($bForListData = false)
    {
        $modelAbaPartnergroups = new AbaPartnergroups();

        if ($bForListData) {
            return $modelAbaPartnergroups->getPartnerGroups();
        } else {
            return $modelAbaPartnergroups->getFullList();
        }
    }

    /**
     * @param bool|false $bForListData
     *
     * @return array
     */
    static function partnerChannelList($bForListData = false)
    {
        $modelAbaPartnerchannels = new AbaPartnerchannels();

        if ($bForListData) {
            return $modelAbaPartnerchannels->getPartnerChannels();
        } else {
            return $modelAbaPartnerchannels->getFullList();
        }
    }

    /**
     * @param bool|false $bForListData
     *
     * @return array
     */
    static function partnerCountryList($bForListData = false)
    {
        $modelAbaCountries = new AbaCountry();
        $artmp = $modelAbaCountries->getFullList();
        $arResult = array();
        array_unshift($arResult, Yii::t('app', 'Not classified') );

        foreach ($artmp as $id=>$value ) {
            $arResult[$id] = $value;
        }

        return $arResult;
    }

}
