<?php
// Once everything about development is finished, please set to 1
ini_set("soap.wsdl_cache_enabled", 0);

class HeWebServices
{
    /**
     * @param string $operation
     * @param array  $aFields
     *
     * @return array|bool
     */
    static function Call($operation = "cancellations", $aFields)
    {
        $return = null;

        // Composition of secure signature:
        $joinParams =   implode("", $aFields);
        $joinParams =   WORD4WS . $joinParams;
        $signature =    md5($joinParams);

        try {
            // Once everything about development is finished, please set trace to 0 ++++++++++
            $client = new SoapClient(Yii::app()->config->get("URL_CAMPUS_WSSELLIGENT"), array('soap_version' => SOAP_1_1, 'port' => 80, 'trace' => 1));

//            $client->__setCookie("XDEBUG_SESSION", "netbeans-xdebug");
            switch ($operation) {
                case 'cancellations':
                    $response = $client->$operation($signature, $aFields["userId"], $aFields["userType"], $aFields["expirationDate"]);
                    return $response;
                    break;
                case 'passwordChange':
                    $response = $client->$operation($signature, $aFields["userId"], $aFields["password"]);
                    return $response;
                    break;
                case 'refundPayment':
                    $response = $client->$operation($signature, $aFields["paymentId"], $aFields["amountPrice"], $aFields["paySuppOrderId"]);
                    return $response;
                    break;
                case 'directPayment':
                    $response = $client->$operation($signature, $aFields["paymentId"], $aFields["amountPrice"], $aFields["dateToPay"]);
                    return $response;
                    break;
                case 'createCreditForm':
                    $response = $client->$operation($signature,
                        $aFields["userId"],
                        $aFields["creditCardType"],
                        $aFields["creditCardNumber"],
                        $aFields["creditCardYear"] ,
                        $aFields["creditCardMonth"] ,
                        $aFields["CVC"]  ,
                        $aFields["creditCardName"]  ,
                        $aFields["cpfBrasil"]  ,
                        $aFields["typeCpf"],
                        $aFields["paySuppExtId"]
                    );
                    return $response;
                    break;
                case 'changeCreditInPayment':
                    $response = $client->$operation($signature, $aFields["paymentId"], $aFields["newIdUserCreditForm"]);
                    return $response;
                    break;
                case 'synchroUser':
                    $response = $client->$operation($signature, $aFields["userId"]);
                    return $response;
                    break;
                case 'unsubscribe':
                    $response = $client->$operation($signature, $aFields["userId"]);
                    return $response;
                    break;
                case 'cancelPayment':
                    $response = $client->$operation($signature, $aFields["paymentId"], $aFields["paySuppOrderId"]);
                    return $response;
                    break;
                case 'partnerSource':
                    $response = $client->$operation($signature, $aFields);
                    return $response;
                    break;
                default:
                    return false;
                    break;
            }
        } catch (Exception $exc) {
            HeLogger::sendLog(
                HeLogger::PREFIX_ERR_UNKNOWN_L . ": Intranet call to Campus call failed, web service " . $operation,
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                "Failed to request operation " . $operation
            );
            if (Yii::app()->config->get("SELF_DOMAIN") == DOMAIN_LIVE) {
                echo "Communications with Campus web service Selligent for operation $operation has failed. ".
                    "Details= \n ";
                echo $exc->getMessage();
                echo " ----------------------------------------------------------------- ";

                echo " ---------------------------------- REQUEST TRACE DEBUG INFO----------------------------";
                echo "\n ------Request HEaders =  \n ";
                var_dump($client->__getLastRequestHeaders());
                echo "\n -----Last Request Body= \n ";
                var_dump($client->__getLastRequest());


                echo "\n ---------------------------------- RESPONSE TRACE DEBUG INFO----------------------------";
                echo "\n Retorno =  \n";
                var_dump($return);
                echo "\n ------Response Headers = \n";
                var_dump($client->__getLastResponseHeaders());

                echo "\n ------Response Body= \n";
                var_dump($client->__getLastResponse());
            }
            return false;
        }
    }

    /**
     * @param $userId
     * @param $userType
     * @param $expirationDate
     *
     * @return array|bool
     */
    static function CancelUser($userId, $userType, $expirationDate)
	{
		$return = HeWebServices::Call( "cancellations", array("userId"=>$userId, "userType"=>$userType,
                                                              "expirationDate"=>$expirationDate) );
		//var_dump($return);
		return $return;
	}

    /**
     * @param $userId
     * @param $password
     *
     * @return array|bool
     */
    static function ChangePassword($userId, $password)
	{
		$return = HeWebServices::Call( "passwordChange", array ("userId"=>$userId, "password"=>$password) );
		return $return;
	}

    /**
     * @param $userId
     *
     * @return array|bool
     */
    static function synchroUser( $userId )
    {
        $return = HeWebServices::Call( "synchroUser", array ("userId"=>$userId));

        return $return;
    }

    /** Call Campus web service in order to also delete this user from Selligent
     * @param $userId
     *
     * @return array|bool
     */
    static function unsubscribe( $userId )
    {
        if(!HeWebServices::synchroUser($userId)){
            return false;
        }

        $return = HeWebServices::Call( "unsubscribe", array ("userId"=>$userId));
        return $return;
    }

    /**
     * @param string $paymentId
     * @param float $amountPrice
     * @param string $paySuppOrderId
     *
     * @return bool
     */
    static function refundPayment($paymentId, $amountPrice, $paySuppOrderId)
    {
        $return = HeWebServices::Call("refundPayment", array("paymentId" => $paymentId, "amountPrice" => $amountPrice, "paySuppOrderId" => $paySuppOrderId));
        return $return;
    }

    /**
     * @param $paymentId
     * @param $paySuppOrderId
     *
     * @return array|bool
     */
    static function cancelPayment($paymentId, $paySuppOrderId)
    {
        $return = HeWebServices::Call("cancelPayment", array("paymentId" => $paymentId, "paySuppOrderId" => $paySuppOrderId));
        return $return;
    }

    /**
     * @param $paymentId
     * @param $amountPrice
     * @param $dateToPay
     *
     * @return array|bool
     */
    static function directPayment($paymentId, $amountPrice, $dateToPay)
    {
        $return = HeWebServices::Call("directPayment", array("paymentId" => $paymentId, "amountPrice" => $amountPrice, "dateToPay" => $dateToPay));
        return $return;
    }

    /** Calls to Campus platform to create the new credit card.
     *
     * @param AbaUserCreditForms $moUserCreditForm
     *
     * @return array|bool
     */
    public static function createCreditForm(AbaUserCreditForms $moUserCreditForm, $paySuppExtId='')
    {
        $aDataPayMethod = array();
        $aDataPayMethod["userId"] =             $moUserCreditForm->userId;
        $aDataPayMethod["creditCardType"] =     $moUserCreditForm->kind;
        $aDataPayMethod["creditCardNumber"] =   $moUserCreditForm->cardNumber;
        $aDataPayMethod["creditCardYear"] =     $moUserCreditForm->cardYear;
        $aDataPayMethod["creditCardMonth"] =    $moUserCreditForm->cardMonth;
        $aDataPayMethod["CVC"] =                $moUserCreditForm->cardCvc;
        $aDataPayMethod["creditCardName"] =     $moUserCreditForm->cardName;
        $aDataPayMethod["cpfBrasil"] =          $moUserCreditForm->cpfBrasil;
        $aDataPayMethod["typeCpf"] =            $moUserCreditForm->typeCpf;
        $aDataPayMethod["paySuppExtId"] =       $paySuppExtId;
        $return = HeWebServices::Call( "createCreditForm", $aDataPayMethod );
        return $return;
    }

    /**
     * @param string $paymentId
     * @param integer $newIdUserCreditForm
     * @return array|bool
     */
    public static function changeCreditInPayment($paymentId, $newIdUserCreditForm)
    {
        $aData= array();
        $aData["paymentId"] = $paymentId;
        $aData["newIdUserCreditForm"] = $newIdUserCreditForm;

        $return = HeWebServices::Call( "changeCreditInPayment", $aData );
        return $return;
    }


    /**
     * @param AbaPartnersList $partnersList
     * @return array|bool
     */
    public static function createPartner(AbaPartnersList $partnersList)
    {
        $aData= array();
        $aData["idPartner"] = $partnersList->idPartner;
        $aData["partnerName"] = $partnersList->name;
        $aData["idPartnerGroup"] = $partnersList->idPartnerGroup;
        $aData["nameGroup"] = $partnersList->nameGroup;
        $aData["idCategory"] = $partnersList->idCategory;
        $aData["idBusinessArea"] = $partnersList->idBusinessArea;
        $aData["idType"] = $partnersList->idType;
        $aData["idGroup"] = $partnersList->idGroup;
        $aData["idChannel"] = $partnersList->idChannel;
        $aData["idCountry"] = $partnersList->idCountry;

        $return = HeWebServices::Call( "partnerSource", $aData );
        return $return;
    }
}
