<?php
/**
 * This is the model class for table "log_user_activity".
 *
 * The followings are the available columns in table 'log_user_activity':
 * @property string $userId
 * @property string $time
 * @property string $table
 * @property string $fields
 * @property string $description
 * @property string $action
 */
class AbaLogUserActivity extends AbaActiveRecord
{
    public $userId;
    private $aOldUserFields = array();
    private $aNewUserFields = array();
    private $aChangedFields = array();

    /**
     * @param AbaUser $abaUser
     */
    public function __construct($abaUser=null)
    {
        parent::__construct();

        if(!empty($abaUser)){
            $this->aOldUserFields = $this->getFieldsClass($abaUser);
            $this->userId = $abaUser->getId();
        }
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /** Yii makes it kind of mandatory
     *
     * @return string
     */
    public function tableName()
    {
        return Yii::app()->params['dbCampusLogs'].".log_user_activity";
    }

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('userId, time, description, action', 'required'),
            array('userId', 'length', 'max'=>10),
            array('table, description, action', 'length', 'max'=>45),
            array('fields', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('userId, time, table, fields, description, action', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'userId' => 'User',
            'time' => 'Time',
            'table' => 'Table',
            'fields' => 'Fields',
            'description' => 'Description',
            'action' => 'Action',
        );
    }

    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('userId',$this->userId,true);
        $criteria->compare('time',$this->time,true);
        $criteria->compare('table',$this->table,true);
        $criteria->compare('fields',$this->fields,true);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('action',$this->action,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * @param AbaUser $abaUser
     *
     * @return bool
     */
    private function getDiffFields(AbaUser $abaUser)
    {
        $fDiffs = false;
        $this->aNewUserFields = $this->getFieldsClass($abaUser);

        foreach ($this->aNewUserFields as $fieldName => $value) {
            if ($this->aOldUserFields[$fieldName] !== $value) {
                $this->aChangedFields[$fieldName] = array($this->aOldUserFields[$fieldName], $value);
                $fDiffs = true;
            }
        }

        return $fDiffs;
    }

    /**
     * @param AbaUser $abaUser
     * @param string  $desc
     * @param string  $action
     *
     * @return bool
     */
    public function saveUserChangesData(AbaUser $abaUser, $desc = "saveUserChangesData", $action = "Modificar Datos")
    {
        if(Yii::app()->config->get("ENABLE_LOG_USERACTIVITY") != 1) {
            return true;
        }

        $saveLog = false;
        if ($this->getDiffFields($abaUser))
        {
            $fieldsChanged = "";
            foreach ($this->aChangedFields as $fieldName => $aValues) {
                $fieldsChanged .= ", [" . $fieldName . "] changed from " . $aValues[0] . " to " . $aValues[1]; //str_replace("'","\'",$aValues[1])
            }
            $saveLog = $this->insertLog($abaUser->tableName(), $fieldsChanged, $desc, $action);
        }
        else {
            // There are no changes at all, but we consider that everything was successful.
            $saveLog = true;
        }
        return $saveLog;
    }

    /**
     * @param $tableName
     * @param $desc
     * @param $action
     * @param integer $userId
     *
     * @return bool
     */
    public function saveUserLogActivity($tableName, $desc, $action, $userId=null)
    {
        if(Yii::app()->config->get("ENABLE_LOG_USERACTIVITY") != 1) {
            return true;
        }

        if (isset($userId)){
            $moUser = new AbaUser();
            if( !$moUser->getUserById($userId) ){
                return false;
            }
            $this->userId = $moUser->id;
        }

        $saveLog = $this->insertLog($tableName, "", $desc, $action);
        return $saveLog;
    }

    /**
     * --------------********************SQL Direct functions, NO LOGIC ************-----------------------------
     */

    /**
     * @param $table
     * @param $fields
     * @param $desc
     * @param $action
     *
     * @return bool
     */
    public function insertLog($table, $fields, $desc, $action)
    {
        $machine = Yii::app()->request->getUserHostAddress();

//        $stLogData = array(
//          "userId" => $this->userId,
//          "table" => $table,
//          "fields" => $fields,
//          "description" => $desc,
//          "action" => $action,
//          "ip" => $machine,
//        );
//
//        return HeLogger::sendLog("User activity", HeLogger::USERACTIVITY, HeLogger::INFO,
//          $stLogData);

        $sql = " INSERT INTO ".$this->tableName().
            " (userId, `time`, `table`, `fields`, description, `action`, `ip`) ".
            " VALUES( ".$this->userId .", NOW(),'".$table ."',:FIELD ,'".$desc."','".$action."','".$machine."'); ";
        $this->id = $this->executeSQL($sql, array(":FIELD" => $fields));
        if ( $this->id > 0){
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * @param integer|null $userId
     *
     * @return bool|string
     */
    public function getLastIp($userId=NULL)
    {
        if( empty($userId)){
            $userId = $this->userId;
        }

        $sql = " SELECT l.ip as `lastIp` FROM ".$this->tableName()." l
                 WHERE l.`userId` = $userId ORDER BY l.`id` DESC LIMIT 1 ";
        $dataReader=$this->querySQL($sql);
        if(($row = $dataReader->read())!==false)
        {
            return $row["lastIp"];
        }

        return false;
    }
}

