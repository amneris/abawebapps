<?php

/**
 * This is the model class for table "messages".
 *
 * The followings are the available columns in table 'messages':
 * @property integer $id
 * @property integer $sender_id
 * @property integer $receiver_id
 * @property string $subject
 * @property string $body
 * @property string $is_read
 * @property string $deleted_by
 * @property string $created_at
 */
class AbaMessages extends CActiveRecord
{
//	public $mStartDate, $mEndDate, $mAction;
//    public $langEnv, $email, $teacherid, $alumnos_totales, $preguntas, $respuestas, $no_respondidas;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AbaMessages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'messages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sender_id, receiver_id, created_at', 'required'),
			array('sender_id, receiver_id', 'numerical', 'integerOnly'=>true),
			array('subject', 'length', 'max'=>256),
			array('is_read', 'length', 'max'=>1),
			array('deleted_by', 'length', 'max'=>8),
			array('body', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, sender_id, receiver_id, subject, body, is_read, deleted_by, created_at', 'safe', 'on'=>'search'),
//			array('langEnv, email, teacherid, alumnos_totales, preguntas, respuestas, preguntas, noreponse', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sender_id' => 'Sender',
			'receiver_id' => 'Receiver',
			'subject' => 'Subject',
			'body' => 'Body',
			'is_read' => 'Is Read',
			'deleted_by' => 'Deleted By',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sender_id',$this->sender_id);
		$criteria->compare('receiver_id',$this->receiver_id);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('is_read',$this->is_read,true);
		$criteria->compare('deleted_by',$this->deleted_by,true);
		$criteria->compare('created_at',$this->created_at,true);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	static function getPeriodMessages($starDate, $endDate, $idLearner)
	{
		$Exclosed=AbaTeachersMessages::getExclosedSubjects();
		$fields="m.created_at as sent, u1.langCourse, m.id as messagesid, m.sender_id as sender, u0.email as senderEmail, m.receiver_id as receiver, u1.email as receiverEmail, is_read, subject, body ";
		/**todos los mensasjes sin tener en cuenta el maching de origen**/
		//				AND (subject like 'RE:%' OR $Exclosed) AND created_at BETWEEN  '$starDate' AND '$endDate' 
	/*	$sql="SELECT $fields FROM messages m INNER JOIN user u0 ON u0.id=m.sender_id INNER JOIN user u1 ON u1.id=m.receiver_id WHERE m.sender_id=$idLearner 
				AND  subject like 'RE:%' AND created_at BETWEEN  '$starDate' AND '$endDate' 
			  UNION
			  SELECT $fields FROM messages m INNER JOIN user u0 ON u0.id=m.sender_id INNER JOIN user u1 ON u1.id=m.receiver_id WHERE receiver_id=$idLearner AND created_at BETWEEN  '$starDate' AND '$endDate'"; */
			  
		$sql="SELECT $fields FROM messages m INNER JOIN user u0 ON u0.id=m.sender_id INNER JOIN user u1 ON u1.id=m.receiver_id WHERE m.sender_id=$idLearner 
				AND $Exclosed AND created_at BETWEEN  '$starDate' AND '$endDate' order by m.created_at, u1.langCourse"; 			  
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);		
        $dataReader=$command->query();
		return $dataReader;	
	}
}