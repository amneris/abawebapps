<?php

class AbaExperiments extends CmAbaExperiments
{
    public function __construct($scenario = 'insert', $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaProductPrice the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'experimentId' => 'ID',
          'userCategoryId' => 'User category',
          'experimentTypeId' => 'Type',
          'experimentIdentifier' => 'Identifier',
          'experimentDescription' => 'Description',
          'counterUsers' => 'Users counter',
          'limitUsers' => 'Users limit',
          'randomMode' => 'Random mode',
          'availableModeId' => 'Availability',
          'dateStart' => 'Start date',
          'dateEnd' => 'End date',
          'languages' => 'Languages',
          'experimentStatus' => 'Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('experimentId', $this->experimentId, true);
        $criteria->compare('userCategoryId', $this->userCategoryId, true);
        $criteria->compare('experimentTypeId', $this->experimentTypeId, true);
        $criteria->compare('experimentIdentifier', $this->experimentIdentifier, true);
        $criteria->compare('experimentDescription', $this->experimentDescription, true);
        $criteria->compare('counterUsers', $this->counterUsers);
        $criteria->compare('limitUsers', $this->limitUsers);
        $criteria->compare('randomMode', $this->randomMode);
        $criteria->compare('availableModeId', $this->availableModeId);
        $criteria->compare('dateStart', $this->dateStart, true);
        $criteria->compare('dateEnd', $this->dateEnd, true);
        $criteria->compare('languages', $this->languages, true);
        $criteria->compare('experimentStatus', $this->experimentStatus);

        $criteria->order = 'experimentId';

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
          'pagination' => array(
            'pageSize' => 100,
          ),
        ));
    }

    /**
     * @param bool $runValidation
     * @param null $attributes
     *
     * @return bool
     */
    public function save($runValidation = true, $attributes = null)
    {
        if ($this->validateExperimentIdentifier()) {
            return parent::save($runValidation, $attributes);
        }
        return false;
    }

    /**
     * @return bool
     */
    public function validateExperimentIdentifier()
    {
        //
        if (trim($this->experimentIdentifier) == '') {
            $this->addError('experimentIdentifier', 'The field experimentIdentifier is mandatory');
            return false;
        }

        //
        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " ex ";
        $sSql .= " WHERE ex.experimentIdentifier = '" . addslashes($this->experimentIdentifier) . "' ";

        if(is_numeric($this->experimentId)) {
            $sSql .= " AND ex.experimentId <> '" . $this->experimentId . "' ";
        }

        $sSql .= " LIMIT 0, 1 ";

        $dataReader = $this->querySQL($sSql);
        if (($row = $dataReader->read()) !== false) {
            $this->addError('experimentIdentifier', 'This Identifier already exists in the database');
            return false;
        }
        return true;
    }

    /**
     * @return array
     */
    public function getExperiments()
    {
        $stAllDescriptions = $this->getAllExperiments();

        $stFormattedDescription = array();

        foreach ($stAllDescriptions as $stDescription) {
            $stFormattedDescription[] = array(
              'id' => $stDescription['experimentId'],
              'title' => $stDescription['experimentDescription']
            );
        }

        return $stFormattedDescription;
    }

    /**
     * @param bool $bEmpty
     *
     * @return array
     */
    public function getFullList($bEmpty = false)
    {
        $stAllDescriptions = $this->getAllExperiments();

        $stFormattedDescription = array();

        if ($bEmpty) {
            $stFormattedDescription[""] = "";
        }

        foreach ($stAllDescriptions as $stDescription) {
            $stFormattedDescription[$stDescription['experimentId']] = $stDescription['experimentIdentifier'];
        }

        return $stFormattedDescription;
    }

}
