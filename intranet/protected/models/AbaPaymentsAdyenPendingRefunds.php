<?php

/**
 * This is the model class for table "pending refunds".
 *
 * This is the model class for table "payments_adyen_pending_refunds".
 * The followings are the available columns in table 'payments_adyen_pending_refunds':
 *
 */
class AbaPaymentsAdyenPendingRefunds extends CmAbaPaymentsAdyenPendingRefunds
{
    public function __construct($scenario='insert', $dbConnName=NULL) {
        parent::__construct($scenario, $dbConnName);
    }
}
