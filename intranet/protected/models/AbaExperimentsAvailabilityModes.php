<?php

class AbaExperimentsAvailabilityModes extends CmAbaExperimentsAvailabilityModes
{
    public function __construct($scenario = 'insert', $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaProductPrice the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'availableModeId' => 'ID',
          'availableModeDescription' => 'Mode description',
          'dateAdd' => 'Create date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('availableModeId', $this->availableModeId, true);
        $criteria->compare('availableModeDescription', $this->availableModeDescription, true);
        $criteria->compare('dateAdd', $this->dateAdd, true);

        $criteria->order = 'availableModeId';

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
          'pagination' => array(
            'pageSize' => 10,
          ),
        ));
    }

    /**
     * @return array
     */
    public function getExperimentsAvailabilityModes()
    {
        $stAllDescriptions = $this->getAllExperimentsAvailabilityModes();

        $stFormattedDescription = array();

        foreach ($stAllDescriptions as $stDescription) {
            $stFormattedDescription[] = array(
              'id' => $stDescription['availableModeId'],
              'title' => $stDescription['availableModeDescription']
            );
        }

        return $stFormattedDescription;
    }

    /**
     * @param bool $bEmpty
     *
     * @return array
     */
    public function getFullList($bEmpty = false)
    {
        $stAllDescriptions = $this->getAllExperimentsAvailabilityModes();

        $stFormattedDescription = array();

        if ($bEmpty) {
            $stFormattedDescription[""] = "";
        }

        foreach ($stAllDescriptions as $stDescription) {
            $stFormattedDescription[$stDescription['availableModeId']] = $stDescription['availableModeDescription'];
        }

        return $stFormattedDescription;
    }

}
