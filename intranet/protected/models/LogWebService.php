<?php
/**
 * @property integer id
 * @property string appNameFromRequest
 * @property string dateRequest
 * @property string dateResponse
 * @property string ipFromRequest
 * @property string nameService
 * @property string paramsRequest
 * @property string paramsResponse
 * @property integer successResponse
 * @property integer errorCode
 * @property string urlRequest
 */

// @property integer nSuccess

class LogWebService extends CmLogWebService
{

    public $startDate;

    public function __construct($scenario=NULL, $dbConnName=NULL)
    {
        parent::__construct();
        $this->connection = Yii::app()->getComponent(AbaActiveRecord::DB_LOGS);
    }

    public $endDate;

    public $errorCode;
    public $nSuccess;

    public $percentage;

    protected static $aErrorCodes = array(
//         200 => "OK",
        400 => "Bad Request",
        402 => "Payment Required",
        401 => "Unauthorized",
        403 => "Forbidden",
        404 => "Not Found",
        405 => "Bad signature",
        410 => "The purchase receipt is not valid",
        500 => "Internal Server Error",
        501 => "Invalid Parameter Value",
        502 => "Invalid Signature",
        503 => "Operation not completed",
        504 => "User does not exist",
        505 => "Selligent communications error",
        506 => "User could not be created",
        507 => "User email is empty",
        508 => "Product not valid",
        509 => "Voucher is not valid. Probably it does not exist, or maybe it applies to another product",
        510 => "User registered, but error on GROUPON CODE update.",
        511 => "User could not be updated",
        512 => "Promocode not valid",
        513 => "Country Id not valid. Check its value on your call please.",
        514 => "Voucher already used. The code probably has been validated: ",
        515 => "Payment not valid.",
        516 => "Payment gateway did not allow transaction.",
        517 => "Payment is too old to be refund. Ask IT.",
        518 => "Change of credit form in subscription not allowed. ",
        519 => "User already exists and is Premium",
        520 => "teacher email not valid",
        521 => "Invalid Level",
        522 => "Password is not valid.",
        523 => "The facebook id is empty or is not valid.",
        524 => "User already exists",
        555 => "Unidentified error",
        556 => "All not valid products",
        557 => "Operation completed with errors",
    );

    protected static $aServiceNames = array(

        "updateattribution" =>   "updateattribution",
        "updateattribution" =>   "updateattribution",
        "directPayment" =>   "directPayment",
        "createCreditForm" =>   "createCreditForm",

        "list" =>           "list",
        "login" =>          "login",
        "refundPayment" =>  "refundPayment",
        "register" =>       "register",

        "registerUser" =>                       "registerUser",
        "registerUserPartner" =>                "registerUserPartner",
        "registerUserFromLevelTest" =>          "registerUserFromLevelTest",
        "registerUserAffiliate" =>              "registerUserAffiliate",
        "registerUserPremiumB2b" =>             "registerUserPremiumB2b",
        "registerUserPremiumB2bExtranet" =>     "registerUserPremiumB2bExtranet",
        "registerUserFacebook" =>               "registerUserFacebook",
        "registerUserLinkedin" =>               "registerUserLinkedin",
        "registerUserObjectives" =>             "registerUserObjectives",
        "loginUserExternal" =>                  "loginUserExternal",
        "registerTeacher" =>                    "registerTeacher",
        "checkIfUserExists" =>                  "checkIfUserExists",
        "renewalsPlanNotification" =>           "renewalsPlanNotification",
        "renewalsPlanPayment" =>                "renewalsPlanPayment",
        "pagosPlanNotification" =>              "pagosPlanNotification",
        "recoverPassword" =>                    "recoverPassword",

        "view" =>                   "view",
        "listProductsByCountry" =>  "listProductsByCountry",
        "version" =>                "version",
        "update" =>                 "update",
        "synchroUser" =>            "synchroUser",
        "unsubscribe" =>            "unsubscribe",
        "passwordChange" =>         "passwordChange",
//        "recoverpassword" =>        "recoverpassword",
        "registerfacebook" =>       "registerfacebook",
        "index" =>                  "index",
        "cancelPayment" =>          "cancelPayment",
    );

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'startDate' =>          'Start date',
            'endDate' =>            'End date',
            'nameService' =>        'Name service',
            'successResponse' =>    'Success response',
            'errorCode' =>          'Error code',
            'nSuccess' =>           'Num. success',
        );
    }

    /**
     * @return array
     */
    public function rules()
    {
        return array(
            array('id, dateRequest, ipFromRequest, appNameFromRequest, urlRequest, paramsRequest, nameService, dateResponse, paramsResponse, successResponse, errorCode', 'safe', 'on'=>'search'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $sErrorCode = 'IF(t.successResponse = 0, IFNULL(t.errorCode, 555), t.errorCode)';

        $criteria->select = array(
            't.nameService',
            't.successResponse',
            new CDbExpression($sErrorCode . ' AS errorCode'),
            new CDbExpression('COUNT(*) AS nSuccess'),
            new CDbExpression("MIN(dateRequest) AS startDate"),
            new CDbExpression("MAX(dateRequest) AS endDate"),
        );

        $criteria->compare('t.nameService', $this->nameService, true);
        $criteria->compare('t.successResponse', $this->successResponse, true);
        $criteria->compare('t.errorCode', $this->errorCode, true);

        if(trim($this->startDate) != '') {
            $criteria->addCondition(" t.dateRequest >= '" . $this->startDate . "'");
        }

        if(trim($this->endDate)) {
            $criteria->addCondition(" t.dateRequest <= '" . $this->endDate . "'");
        }

        $criteria->group = 't.nameService, t.successResponse, ' . $sErrorCode;

        if(isset($_GET['LogWebService_sort']) AND trim($_GET['LogWebService_sort']) != '') {

            $sPageOrder = $_GET['LogWebService_sort'];

            if($sPageOrder == 'nSuccess.desc') {
                $sPageOrder = 'nSuccess DESC, t.nameService, t.successResponse, t.errorCode';
            }
            elseif($sPageOrder == 'nSuccess.asc') {
                $sPageOrder = 'nSuccess ASC, t.nameService, t.successResponse, t.errorCode';
            }
            elseif($sPageOrder == '') {
                $criteria->order = $sPageOrder . 't.nameService, t.successResponse, t.errorCode';
            }
        }
        else {
            $criteria->order = 't.nameService, t.successResponse, t.errorCode';
        }

        $oCActiveDataProvider = new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 100,
            ),
        ));
        return $oCActiveDataProvider;
    }

    /**
     * @return array
     */
    public static function getWsErrorCodes()
    {
        $aErrorCodes = array();
        foreach(self::$aErrorCodes AS $iErrorCode => $sErrorMsg) {
            $aErrorCodes[] = array('id' => $iErrorCode, 'title' => $sErrorMsg);
        }
        return $aErrorCodes;
    }

    /**
     * @return array
     */
    public static function getWsNames()
    {
        $aServiceNames = array();
        foreach(self::$aServiceNames AS $sServiceKey => $sServiceName) {
            $aServiceNames[] = array('id' => $sServiceKey, 'title' => $sServiceName);
        }
        return $aServiceNames;
    }

    /**
     * @param $sErrorCode
     * @return string
     */
    public static function getWsErrorCode($sErrorCode)
    {
        $aWsErrorCodes = self::getWsErrorCodes();

        foreach($aWsErrorCodes as $iKey => $aWsErrorCode) {
            if($aWsErrorCode['id'] == $sErrorCode) {
                return $aWsErrorCode['title'];
            }
        }
        return '';
    }

    /**
     * @param $sName
     * @return string
     */
    public static function getWsName($sName)
    {
        $aWsNames = self::getWsNames();

        foreach($aWsNames as $iKey => $aWsName) {
            if($aWsName['id'] == $sName) {
                return $aWsName['title'];
            }
        }
        return '';
    }

}
