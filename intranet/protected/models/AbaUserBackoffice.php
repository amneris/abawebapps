<?php

/**
 * SELECT u.`id`, u.`name`, u.`surnames`, u.`email`, u.`password`,
 * u.`complete`, u.`countryId`,
 * u.`birthDate`, u.`telephone` FROM `user` u;
 */
class AbaUserBackoffice extends CActiveRecord
{
    const UNKNOW = 0;
    const ROOT = 1;
    const SUPPORT = 2;
    const TEACHER = 3;
    const MARKETING = 4;

    static function isValid($users)
    {
        foreach ($users as $user) {
            if (Yii::app()->user->getId() == $user) {
                return true;
            }
        }
        return false;
    }

    public function getUserId()
    {
        return $this->id;
    }

    public static function getUserTypes()
    {
        return array(
          array('id' => '1', 'title' => 'Root'),
          array('id' => '2', 'title' => 'Support'),
          array('id' => '3', 'title' => 'Teacher'),
          array('id' => '4', 'title' => 'Marketing'),
        );
    }

    public static function getUserType($role)
    {
        switch ($role) {
            case 1:
                return 'Root';
            case 2:
                return 'Support';
            case 3:
                return 'Teacher';
            case 4:
                return 'Marketing';
        }
    }

    public function getUserByEmail($email, $password = "", $usePassword = false)
    {
        $testPassword = $usePassword == true ? " AND u.`password`='" ./*HeAbaSHA::_md5(*/
          $password/*)*/ . "'" : "";
        $sql = "SELECT * FROM " . $this->tableName() . " u WHERE u.`email` = '$email' $testPassword";
        //die ($sql);
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();
        if (($row = $dataReader->read()) !== false) {

            $this->id = $row['id'];
            $this->email = $row['email'];
            $this->role = $row['role'];
            $this->password = $row['password'];
            return true;
        }
        return false;
    }

    public function hashPassword($password)
    {
        return md5($password);
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_backoffice';
    }

    public function getRole()
    {
        return $this->role;
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
          array('role, email, password', 'required'),
          array('role', 'length', 'max' => 2),
          array('email', 'length', 'max' => 200),
            //array('password, passwordFlashMedia', 'length', 'max'=>50),
          array('password', 'length', 'max' => 50),
          array('id, email, password, role', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        /*return array(
        );*/


        return array('payments' => array(self::HAS_MANY, 'AbaPayments', 'userId'));

    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'id' => 'ID',
          'email' => 'Email',
          'password' => 'Password',
          'role' => 'Role',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('role', $this->role, true);

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
        ));
    }
}

?>
