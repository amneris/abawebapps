<?php

class AbaProductsPromosDescription extends CmAbaProductsPromosDescription
{
    public function __construct($scenario='insert', $dbConnName=NULL) {
        parent::__construct($scenario, $dbConnName);
    }

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idDescription' =>      'Id',
            'translationKey' =>     'Key',
            'descriptionText' =>    'Description',
            'createDate' =>         'Create time',
        );
    }

    /**
    * Retrieves a list of models based on the current search/filter conditions.
    *
    * Typical usecase:
    * - Initialize the model fields with values from filter form.
    * - Execute this method to get CActiveDataProvider instance which will filter
    * models according to data in model fields.
    * - Pass data provider to CGridView, CListView or any similar widget.
    *
    * @return CActiveDataProvider the data provider that can return the models
    * based on the search/filter conditions.
    */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('idDescription',     $this->idDescription, true);
        $criteria->compare('translationKey',    $this->translationKey, true);
        $criteria->compare('descriptionText',   $this->descriptionText, true);
        $criteria->compare('createDate',        $this->createDate, true);

        $criteria->order = 'idDescription ASC';

        return new CActiveDataProvider($this, array(
            'criteria' =>   $criteria,
            'pagination' => array('pageSize' => 30)
        ));
    }

    /**
     * @return array
     */
    public function getDescriptionTypes()
    {
        $stAllDescriptions = $this->getAllDescriptions();

        $stFormattedDescription = array();

        foreach($stAllDescriptions as $stDescription) {
            $stFormattedDescription[] = array('id' => $stDescription['idDescription'], 'title' => $stDescription['translationKey']);
        }

        return $stFormattedDescription;
    }

    /**
     * @return array
     */
    public function getFullList()
    {
        $stAllDescriptions = $this->getAllDescriptions();

        $stFormattedDescription = array();

        foreach($stAllDescriptions as $stDescription) {
            $stFormattedDescription[$stDescription['idDescription']] = $stDescription['translationKey'];
        }

        return $stFormattedDescription;
    }

//    public function getPromosDescriptionList_($type)
//    {
//        switch ($type)
//        {
//            case '1':
//                return 'Visible & real';
//            case 'B':
//                return 'Never expire & not visible';
//            case 'C':
//                return 'Plus 3 days';
//        }
//    }
//
//    /**
//     * @return array
//     */
//    static function getPromosDescriptionList($bForListData = false, $id = null)
//    {
//        $modelPromosDescription = new AbaProductsPromosDescription();
//
//        if(is_numeric($id)) {
//            if ($bForListData) {
//                foreach($modelPromosDescription as $stDescription) {
//                    if($stDescription['id'] == $id) {
//                        return $stDescription['title'];
//                    }
//                }
//            } else {
//                foreach($modelPromosDescription as $iKey => $stDescription) {
//                    if($iKey == $id) {
//                        return $stDescription;
//                    }
//                }
//            }
//            return "";
//        }
//        else {
//            if ($bForListData) {
//                return $modelPromosDescription->getDescriptionTypes();
//            } else {
//                return $modelPromosDescription->getFullList();
//            }
//        }
//    }



}
