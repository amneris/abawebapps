<?php
/**
 * This is the model class for table "aba_b2c_summary.users_progress_lastweek".
 *
 * The followings are the available columns in table 'aba_b2c_summary.users_progress_lastweek':
 * @property string $userId
 * @property integer $unitId
 * @property string $dateCapture
 * @property integer $yearRef
 * @property integer $weekRef
 * @property integer $currentLevel
 * @property integer $totalProgress
 * @property integer $abaFilm
 * @property integer $speak
 * @property integer $write
 * @property integer $interpret
 * @property integer $videoClass
 * @property integer $exercises
 * @property integer $vocabulary
 * @property integer $assessment
 */
class AbaUsersProgressLastweek extends AbaActiveRecord
{
    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return Yii::app()->params['dbCampusSummary'] . '.users_progress_lastweek';
    }

    /** List of all fields available in table
     * @return string
     */
    public function mainFields()
    {
        return "*";
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'userId' => 'User',
			'unitId' => 'Unit',
			'dateCapture' => 'Date Capture',
			'yearRef' => 'Year Ref',
			'weekRef' => 'Week Ref',
			'currentLevel' => 'Current Level',
			'totalProgress' => 'Total Progress',
			'abaFilm' => 'Aba Film',
			'speak' => 'Speak',
			'write' => 'Write',
			'interpret' => 'Interpret',
			'videoClass' => 'Video Class',
			'exercises' => 'Exercises',
			'vocabulary' => 'Vocabulary',
			'assessment' => 'Assessment',
		);
	}

    /** Returns the last row inserted into table.
     * @return bool
     */
    public function getLastRow()
    {
        $sql = ' SELECT upl.' . $this->mainFields() .
            ' FROM ' . $this->tableName() . ' upl  ORDER BY upl.dateCapture DESC LIMIT 1; ';

        $dataReader = $this->querySQL($sql);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return true;
        }

        return false;
    }



    /**
     * @param B2cFollowup $moFollowUp
     * @param $dateFromLastChange
     * @param $start
     * @param $limit
     *
     * @return bool|int|string
     */
    public function collectProgressFromDate(B2cFollowup $moFollowUp, $dateFromLastChange, $start, $limit )
    {
        $sql = "REPLACE INTO ".$this->tableName()."
                ( `userId`,  `unitId`,  `dateCapture`,  `yearRef`,  `weekRef`,  `currentLevel`,
                `totalProgress`,  `abaFilm`,  `speak`,  `write`,  `interpret`,
                `videoClass`,  `exercises`,  `vocabulary`,  `assessment` )
               SELECT f.`userid` as 'userId', f.`themeid` as 'unit', DATE(NOW()) as 'dateShot', YEAR(NOW()),
                (WEEK(DATE(NOW()),1)-1) as 'weekReference', u.currentLevel as 'currentLevel',".
                $moFollowUp->getSQLComputeFullProgress(true)." as `totalProgress`,".
                $moFollowUp->getSQLComputedSections(true, true).
            " FROM aba_b2c_summary.followup4_summary f
             INNER JOIN aba_b2c.`user` u ON f.userid=u.id
             WHERE DATE(f.`lastchange`)>DATE(:DATESINCE)
             GROUP BY f.userid, f.`themeid`
             ORDER BY f.userid ASC, f.`themeid` ASC
             LIMIT $start, $limit ";

        $aBrowserValues = array( ':DATESINCE' => $dateFromLastChange );
        $qRows = $this->executeSQL($sql, $aBrowserValues, true );
        if ( $qRows > 0) {
            return $qRows;
        }

        return false;
    }

    /** Deletes by batch rows from this table.
     *
     * @param integer $batchRows
     * @return bool|int
     */
    public function deleteLastRows($batchRows=0)
    {
        $sqlLimit = 'LIMIT '.$batchRows;
        if($batchRows==0){
            $sqlLimit = '';
        }

        // ! NOT TO MODIFY THE ORDER BY IN THIS DELETE!!
        $sql = " DELETE FROM ".$this->tableName().
            " ORDER BY `userId`, `unitId`, `dateCapture` ".
            " $sqlLimit; ";

        $fDeleted = $this->deleteSQL($sql);
        if($fDeleted>0){
            return $fDeleted;
        }

        return false;
    }

}
