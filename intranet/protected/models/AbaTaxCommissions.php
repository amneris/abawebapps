<?php

/**
 * This is the model class for table "tax_commissions".
 *
 * The followings are the available columns in table 'tax_commissions':
 * @property integer $idCommission
 * @property integer $paySuppExtId
 * @property integer $idCountry
 * @property float $taxCommissionValue
 * @property string $dateStartCommission
 * @property string $dateEndCommission
 */
class AbaTaxCommissions extends CmAbaTaxCommissions
{
    public function __construct($scenario = 'insert', $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'idCommission' => 'Id',
          'paySuppExtId' => 'Pay supplier',
          'idCountry' => 'Country',
          'taxCommissionValue' => 'Tax Commission Value (%)',
          'dateStartCommission' => 'Start time',
          'dateEndCommission' => 'End time',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idCommission', $this->idCommission, true);
        $criteria->compare('paySuppExtId', $this->paySuppExtId, true);
        $criteria->compare('idCountry', $this->idCountry, true);
        $criteria->compare('taxCommissionValue', $this->taxCommissionValue, true);
        $criteria->compare('dateStartCommission', $this->dateStartCommission, true);
        $criteria->compare('dateEndCommission', $this->dateEndCommission, true);

        $criteria->order = 'dateStartCommission DESC, paySuppExtId, idCountry';

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
          'pagination' => array('pageSize' => 30)
        ));
    }

    /**
     * @param bool $runValidation
     * @param null $attributes
     *
     * @return bool
     */
    public function save($runValidation = true, $attributes = null)
    {
        if ($this->validateDateInterval()) {
            return parent::save($runValidation, $attributes);
        }
        return false;
    }

    /**
     * @return bool
     */
    public function validateDateInterval()
    {

        $sNow = HeDate::todaySQL(true);

        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " tr ";
        $sSql .= " WHERE tr.idCountry = " . $this->idCountry . " ";

        if (!$this->getIsNewRecord()) {
            $sSql .= " AND tr.idCommission <> " . $this->idCommission . " ";
        }

        if (trim($this->dateEndCommission) == '') {

            $sSql .= " AND (";
            $sSql .= " (NULLIF(tr.dateEndCommission, '') IS NULL)"; //  OR tr.dateEndCommission == '0000-00-00 00:00:00'
            $sSql .= " OR ('" . $this->dateStartCommission . "' BETWEEN tr.dateStartCommission AND IFNULL(NULLIF(tr.dateEndCommission, ''), '" . $sNow . "')) ";
            $sSql .= " ) ";

//echo $sSql; exit();

            $dataReader = $this->querySQL($sSql);
            if (($row = $dataReader->read()) !== false) {

                $this->addError('dateEndCommission', 'Ya existe un registro.............');

                return false;
            }
        } else {

            $sSql .= " AND (";
            $sSql .= " ('" . $this->dateStartCommission . "' BETWEEN tr.dateStartCommission AND IFNULL(NULLIF(tr.dateEndCommission, ''), '" . $sNow . "')) ";
            $sSql .= " OR ('" . $this->dateEndCommission . "' BETWEEN tr.dateStartCommission AND IFNULL(NULLIF(tr.dateEndCommission, ''), '" . $sNow . "'))";
            $sSql .= " ) ";

//echo $sSql; exit();

            $dataReader = $this->querySQL($sSql);
            if (($row = $dataReader->read()) !== false) {

                $this->addError('dateEndCommission', 'Ya existe un registro.............');

                return false;
            }
        }

        return true;
    }


}
