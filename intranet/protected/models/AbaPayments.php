<?php

/**
 * This is the moFrmPayment class for table "payments".
 *
 * The followings are the available columns in table 'payments':
 * @property string $id
 * @property string $idUserProdStamp
 * @property string $userId
 * @property string $idProduct
 * @property string $idCountry
 * @property string $idPeriodPay
 * @property string $idUserCreditForm
 * @property string $paySuppExtId
 * @property string $paySuppOrderId
 * @property string $status
 * @property string $dateStartTransaction
 * @property string $dateEndTransaction
 * @property string $dateToPay
 * @property string $xRateToEUR
 * @property string $xRateToUSD
 * @property string $currencyTrans
 * @property string $foreignCurrencyTrans
 * @property string $idPromoCode
 * @property string $amountOriginal
 * @property string $amountDiscount
 * @property string $amountPrice
 * @property string $idPayControlCheck
 * @property string $idPartner
 * @property string $paySuppExtProfId
 * @property string $autoId
 * @property string $lastAction
 * @property integer $isRecurring
 * @property string $paySuppExtUniId
 * @property integer $paySuppLinkStatus
 * @property integer $isPeriodPayChange
 * @property integer $cancelOrigin
 *
 * @property float $taxRateValue
 * @property float $amountTax
 * @property float $amountPriceWithoutTax
 * @property integer $isExtend
 * @property integer $experimentVariationAttributeId
 *
 */
class AbaPayments extends CActiveRecord
{
	public $idPartnerGroup;
    public $action;
    public $endDateTransaction;
	/**
	 * Returns the static moFrmPayment of the specified AR class.
	 * @param string $className active record class name.
	 * @return AbaPayments the static moFrmPayment class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payments';
	}

	/**
	 * @return array validation rules for moFrmPayment attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, paySuppOrderId, userId, idProduct, idCountry, idPeriodPay, idUserCreditForm, dateStartTransaction, xRateToEUR, xRateToUSD, amountPrice', 'required'),
			array('id, idProduct, idPayControlCheck, idPartner', 'length', 'max'=>15),
            array('action','length', 'max'=>4), //if canncel or Fail
			array('paySuppOrderId', 'length', 'max'=>60),
			array('idUserCreditForm', 'length', 'max'=>10),
			array('id', 'length', 'max'=>12),
			array('userId', 'length', 'max'=>64),
			array('userId, idCountry', 'length', 'max'=>4),
			array('idPeriodPay', 'length', 'max'=>5),
			array('paySuppExtId, status, paySuppLinkStatus', 'length', 'max'=>2),
			array('xRateToEUR, xRateToUSD, amountOriginal, amountDiscount, amountPrice, taxRateValue, amountTax, amountPriceWithoutTax', 'length', 'max'=>18),
			array('currencyTrans, foreignCurrencyTrans', 'length', 'max'=>3),
			array('idPromoCode', 'length', 'max'=>50),
			array('keyExternalLogin', 'length', 'max'=>100),
			array('lastAction', 'length', 'max'=>500),
            array('paySuppExtUniId', 'length', 'max'=>50),
			array('dateEndTransaction, dateToPay', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, userId, idProduct, idCountry, idPeriodPay, idUserCreditForm, paySuppExtId, paySuppOrderId,
			status, dateStartTransaction, dateEndTransaction, dateToPay, xRateToEUR, xRateToUSD, currencyTrans,
			foreignCurrencyTrans, idPromoCode, amountOriginal, amountDiscount, amountPrice, idPayControlCheck,
			idPartner, paySuppExtProfId, autoId, lastAction, isRecurring, paySuppExtUniId, paySuppLinkStatus,
			isPeriodPayChange, cancelOrigin, taxRateValue, amountTax, amountPriceWithoutTax, isExtend, experimentVariationAttributeId',
                'safe', 'on'=>'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
	/*	return array(
		);*/
		return array(
			'Usuario'=>array(self::BELONGS_TO, 'AbaUser', 'id')
		);
	}

		/**
		 * @return array customized attribute labels (name=>label)
		 */
		public function attributeLabels()
		{
				return array(
					'id' => 'ID',
					'userId' => 'User',
					'idProduct' => 'Product',
					'idCountry' => 'Country',
					'idPeriodPay' => 'Id Period Pay',
					'idUserCreditForm' => 'Id User Credit Form',
					'paySuppExtId' => 'Gateway payment platform',
					'paySuppOrderId' => 'Pay Supp Order',
					'status' => 'Status',
					'dateStartTransaction' => 'Date Start Transaction',
					'dateEndTransaction' => 'Date End Transaction',
					'dateToPay' => 'Date To Pay',
					'xRateToEUR' => 'X Rate To Eur',
					'xRateToUSD' => 'X Rate To Usd',
					'currencyTrans' => 'Currency Trans',
					'foreignCurrencyTrans' => 'Foreign Currency Trans',
					'idPromoCode' => 'Id Promo Code',
					'amountOriginal' => 'Amount Original',
					'amountDiscount' => 'Amount Discount',
					'amountPrice' => 'Amount Price',
					'idPayControlCheck' => 'Id payment reference(previous)',
					'idPartner' => 'Id Partner',
					'lastAction' => 'last Action',
					'action' => 'Action',
					'isRecurring' => 'Recurring',
					'paySuppExtUniId' => 'External gateway identifier',
					'paySuppLinkStatus' => 'Reconciliation status',
					'isPeriodPayChange' => 'Previous periodicity or plan',
					'cancelOrigin' => 'Cancel action origin',
					'taxRateValue' => 'Tax rate value',
					'amountTax' => 'Tax amount',
					'amountPriceWithoutTax' => 'Amount Original without Tax',
					'isExtend' => 'Is an extended payment',
					'experimentVariationAttributeId' => 'Scenario payment ID',
					'paySuppExtProfId' => 'Reference',
				);
		}

		/**
		 * Retrieves a list of models based on the current search/filter conditions.
		 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
		 */
		public function search()
		{
				// Warning: Please modify the following code to remove attributes that
				// should not be searched.

				$criteria = new CDbCriteria;
				$criteria->compare('id', $this->id, true);
				$theUserId = $this->userId;
				if (HeString::isValidEmail(trim($this->userId))) {
						$sql = "SELECT count(*) as num, p.id FROM user p WHERE p.email='" . trim($this->userId) . "' limit 1";
						$connection = Yii::app()->db;
						$command = $connection->createCommand($sql);
						$dataReader = $command->query();
						if (($row = $dataReader->read()) !== false) {
								if ($row["num"] !== 0) {
										$theUserId = $row["id"];
								}
						}
				}
				$criteria->compare('userId', $theUserId, false);
				$criteria->compare('idProduct', $this->idProduct, true);
				$criteria->compare('idCountry', $this->idCountry, false);
				$criteria->compare('idPeriodPay', $this->idPeriodPay, true);
				$criteria->compare('idUserCreditForm', $this->idUserCreditForm, true);
				$criteria->compare('paySuppExtId', $this->paySuppExtId, false); // true="LIKE", false="="
				$criteria->compare('paySuppOrderId', $this->paySuppOrderId, true);
				$criteria->compare('status', $this->status, false);
				$criteria->compare('dateStartTransaction', $this->dateStartTransaction, true);
				$criteria->compare('dateEndTransaction', $this->dateEndTransaction, true);
				$criteria->compare('dateToPay', $this->dateToPay, true);
				$criteria->compare('xRateToEUR', $this->xRateToEUR, true);
				$criteria->compare('xRateToUSD', $this->xRateToUSD, true);
				$criteria->compare('currencyTrans', $this->currencyTrans, true);
				$criteria->compare('foreignCurrencyTrans', $this->foreignCurrencyTrans, true);
				$criteria->compare('idPromoCode', $this->idPromoCode, true);
				$criteria->compare('amountOriginal', $this->amountOriginal, true);
				$criteria->compare('amountDiscount', $this->amountDiscount, true);
				$criteria->compare('amountPrice', $this->amountPrice, true);
				$criteria->compare('idPayControlCheck', $this->idPayControlCheck, true);
				$criteria->compare('idPartner', $this->idPartner, true);
				$criteria->compare('lastAction', $this->lastAction, true);
				$criteria->compare('isRecurring', $this->isRecurring, true);
				$criteria->compare('isExtend', $this->isExtend, true);
				$criteria->compare('paySuppExtUniId', $this->paySuppExtUniId, true);
				$criteria->compare('paySuppLinkStatus', $this->paySuppLinkStatus, true);
				$criteria->compare('paySuppExtProfId', $this->paySuppExtProfId, true);
				if (empty($_GET["AbaPayments_sort"])) {
						$criteria->order = 'autoId ASC';
				}

				return new CActiveDataProvider($this, array(
					'criteria' => $criteria,
				));
		}

	/** search by userId
	**/
	public function searchByUserId()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('userId',"=".$this->userId,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * @param $status
     * @param $idPartner
     * @param $idPartnerGroup
     * @param $startDate
     * @param $endDate
     * @param $endDateTransaction
     * @return CDbDataReader
     */
    public function searchExport($status, $idPartner, $idPartnerGroup, $startDate, $endDate, $endDateTransaction, $includeCampaign=false)
	{
        if($endDateTransaction) {
            $filter = "WHERE p.dateEndTransaction >= '$startDate' AND p.dateEndTransaction <= '$endDate' ";
            //die($filter);
        }
        else {
            $filter = "WHERE p.dateToPay BETWEEN '$startDate' AND '$endDate' ";
            //die($filter);
        }

		if(count($status))
		{
			$filter .= $filter!= "" ? " and ": "";
			$filter.="(";
			$count=0;
			foreach($status as $value)
			{
				if($count)
					$filter .= " OR p.status=$value";
				else	
					$filter .= " p.status=$value";
				$count++;
			}	
			$filter.=")";
		}
		if(count($idPartner))
		{
			$filter .= $filter!= "" ? " and ": "";			
			$count=0;
			$filter.="(";
			foreach($idPartner as $value)
			{
				if($count)
					$filter .= " OR p.idPartner=$value";
				else	
					$filter .= " p.idPartner=$value";
				$count++;
			}	
			$filter.=")";
		}
		else
			if(count($idPartnerGroup))
			{
				$filter .= $filter!= "" ? " and ": "";			
				$count=0;
				$filter.="(";
				foreach($idPartnerGroup as $value)
				{
					if($count)
						$filter .= " OR ap.idPartnerGroup=$value";
					else	
						$filter .= " ap.idPartnerGroup=$value";
					$count++;
				}	
				$filter.=")";
			}

		$table = Yii::app()->params["mainDbName"].".";

        //#5113
        if($includeCampaign) {

$sql="
SELECT u.id as userid, u.name, u.surnames, u.email,
ELT(u.userType+1,'DELETED', 'FREE', 'PREMIUM', 'PLUS', 'TEACHER')as userTypeStr, u.expirationDate, apSrc.name as 'Partner Source', apSrc.nameGroup as 'Source group',
p.id as paymentsid, '' as idUserProdStamp, p.idProduct, c.name as countryName, p.idCountry, p.idPeriodPay,
p.idUserCreditform, p.paySuppExtId, p.paySuppOrderId,
ELT(FIELD(p.status, '0', '10', '30', '40', '50'),'PENDING','FAIL', 'SUCCESS', 'REFUND', 'CANCEL') as statusStr,
p.dateStartTransaction, p.dateEndTransaction, p.dateToPay,
REPLACE(CAST(p.xRateToEUR AS CHAR), '.', ',')as xRateToEUR, p.currencyTrans, p.foreignCurrencyTrans,
p.idPromoCode, REPLACE(CAST(p.amountOriginal AS CHAR), '.', ',') as amountOriginal,
REPLACE(CAST(p.amountDiscount AS CHAR), '.', ',') as amountDiscount, REPLACE(CAST(amountPrice AS CHAR), '.', ',') as amountPrice, REPLACE(CAST(amountPriceWithoutTax AS CHAR), '.', ',') as amountPriceWithoutTax,
ap.name as idPartner, UCASE(IF(ap.nameGroup='', ap.idPartnerGroup, ap.nameGroup)) as idPartnerGroupStr
,apPre.name as idPartnerPrePay, UCASE(IF(apPre.nameGroup='', apPre.idPartnerGroup, apPre.nameGroup)) as idPartnerPrePayGroupStr,
p.paySuppExtUniId, IF(p.paySuppLinkStatus=1,'MATCHED','NOT MATCHED') as `paySuppLinkStatus`,
p.isPeriodPayChange as `PreviousProduct`, p.`cancelOrigin` as `ActionCancel`, ucf.`kind` AS creditCardType,
p.isExtend,
'' AS campaign, p.idPartner AS paymentPartnerId,
vfs.tableName AS vfTableName, vfs.idventasFlashScript AS vfIdventasFlashScript, u.idPartnerSource AS vfIdPartnerSource,
vfs.prefijoMensual, vfs.prefijoBimensual, vfs.prefijoTrimestral, vfs.prefijoSemestral, vfs.prefijoAnual, vfs.prefijo18Meses, vfs.prefijo24Meses
FROM ".$table."payments p
INNER JOIN ".$table."user u ON u.id= p.userId
INNER JOIN ".$table."country c ON c.id = p.idCountry
INNER JOIN ".$table."user uu ON uu.id = u.teacherId
LEFT JOIN ".$table."aba_partners_list ap ON ap.idPartner = p.idPartner
LEFT JOIN ".$table."aba_partners_list apPre ON p.idPartnerPrePay = apPre.idPartner
LEFT JOIN ".$table."aba_partners_list apSrc ON u.idPartnerSource = apSrc.idPartner
LEFT JOIN ".$table."user_credit_forms ucf ON ucf.userId = p.userId AND p.idUserCreditForm = ucf.id
LEFT JOIN db_ventasflash.ventasFlashScript AS vfs ON vfs.idventasFlashScript = p.idPartner
$filter";

        }
        else {

$sql="
SELECT u.id as userid, u.name, u.surnames, u.email,
ELT(u.userType+1,'DELETED', 'FREE', 'PREMIUM', 'PLUS', 'TEACHER')as userTypeStr, u.expirationDate, apSrc.name as 'Partner Source', apSrc.nameGroup as 'Source group',
p.id as paymentsid, '' as idUserProdStamp, p.idProduct, c.name as countryName, p.idCountry, p.idPeriodPay,
p.idUserCreditform, p.paySuppExtId, p.paySuppOrderId,
ELT(FIELD(p.status, '0', '10', '30', '40', '50'),'PENDING','FAIL', 'SUCCESS', 'REFUND', 'CANCEL') as statusStr,
p.dateStartTransaction, p.dateEndTransaction, p.dateToPay,
REPLACE(CAST(p.xRateToEUR AS CHAR), '.', ',')as xRateToEUR, p.currencyTrans, p.foreignCurrencyTrans,
p.idPromoCode, REPLACE(CAST(p.amountOriginal AS CHAR), '.', ',') as amountOriginal,
REPLACE(CAST(p.amountDiscount AS CHAR), '.', ',') as amountDiscount, REPLACE(CAST(amountPrice AS CHAR), '.', ',') as amountPrice, REPLACE(CAST(amountPriceWithoutTax AS CHAR), '.', ',') as amountPriceWithoutTax,
ap.name as idPartner, UCASE(IF(ap.nameGroup='', ap.idPartnerGroup, ap.nameGroup)) as idPartnerGroupStr
,apPre.name as idPartnerPrePay, UCASE(IF(apPre.nameGroup='', apPre.idPartnerGroup, apPre.nameGroup)) as idPartnerPrePayGroupStr,
p.paySuppExtUniId, IF(p.paySuppLinkStatus=1,'MATCHED','NOT MATCHED') as `paySuppLinkStatus`,
p.isPeriodPayChange as `PreviousProduct`, p.`cancelOrigin` as `ActionCancel`, ucf.`kind` AS creditCardType,
p.isExtend
FROM ".$table."payments p
INNER JOIN ".$table."user u ON u.id= p.userId
INNER JOIN ".$table."country c ON c.id = p.idCountry
INNER JOIN ".$table."user uu ON uu.id = u.teacherId
LEFT JOIN ".$table."aba_partners_list ap ON ap.idPartner = p.idPartner
LEFT JOIN ".$table."aba_partners_list apPre ON p.idPartnerPrePay = apPre.idPartner
LEFT JOIN ".$table."aba_partners_list apSrc ON u.idPartnerSource = apSrc.idPartner
LEFT JOIN ".$table."user_credit_forms ucf ON ucf.userId = p.userId AND p.idUserCreditForm = ucf.id
$filter";

        }

        $connection = Yii::app()->dbSlave;
        /* @var $connection CDbConnection */
		$command = $connection->createCommand($sql);
        $dataReader=$command->query();
		return $dataReader;
	}

    /**
     * @param $status
     * @param $idPartnerGroup
     * @param $startDate
     * @param $endDate
     *
     * @return CDbDataReader
     */
    public function searchExportInvoices($status, $idPartnerGroup, $startDate, $endDate)
    {
        $filter = "WHERE ui.dateInvoice BETWEEN '$startDate' AND '$endDate' ";

        if(count($status))
        {
            $filter .= $filter!= "" ? " AND ": "";
            $filter.="(";
            $count=0;
            foreach($status as $value)
            {
                if($count)
                    $filter .= " OR p.status=$value";
                else
                    $filter .= " p.status=$value";
                $count++;
            }
            $filter.=")";
        }
        else {
            $filter .= $filter != "" ? " and " : "";
            $filter .= " (p.status IN (" . PAY_SUCCESS . ", " . PAY_REFUND . ")) ";
        }

        if(count($idPartnerGroup) == 1)
        {
            $filter .= $filter!= "" ? " AND ": "";
            foreach($idPartnerGroup as $value)
            {
                if($value == PARTNER_GROUP_B2B) {
                    $filter .= " ( ap.idPartnerGroup = " . PARTNER_B2B_G . " AND p.paySuppExtId <> " . PAY_SUPPLIER_B2B . " ) ";
                }
                elseif($value == PARTNER_GROUP_B2C) {
                    $filter .= " ( ap.idPartnerGroup <> " . PARTNER_B2B_G . " ) ";
                }
                break;
            }
        }

        $partners = HeList::invoicesPartnerGroupList();
        $table =    Yii::app()->params["mainDbName"].".";

        //#4480
        //#5971
        // ucf.`kind` AS creditCardType,
        $sql = "
						SELECT
								u.id as userid,
								p.id as paymentsid,
								ELT(FIELD(p.status, '0', '10', '30', '40', '50'),'PENDING','FAIL', 'SUCCESS', 'REFUND', 'CANCEL') as statusStr,
								ui.dateInvoice,
								IF(IFNULL(TRIM(ui.userName), '')='', u.name, ui.userName) AS name,
								IF(IFNULL(TRIM(ui.userLastName), '')='', u.surnames, ui.userLastName) AS surnames,
								ui.userVatNumber,
								c.name as countryName,
								ui.numberInvoice,
								REPLACE(CAST(ui.amountPriceWithoutTax AS CHAR), '.', ',') as amountPriceWithoutTax,
								REPLACE(CAST(ui.taxRateValue AS CHAR), '.', ',') as taxRateValue,
								REPLACE(CAST(ui.amountTax AS CHAR), '.', ',') as amountTax,

								REPLACE(CAST(ui.amountPrice AS CHAR), '.', ',') as amountPrice,
								REPLACE(CAST(IFNULL(tcom.taxCommissionValue, 0) AS CHAR), '.', ',') AS taxCommissionValue,
								REPLACE(CAST(IF(IFNULL(TRIM(tcom.taxCommissionValue), 0) = 0, ui.amountPrice,
									ui.amountPrice - ((ui.amountPrice * tcom.taxCommissionValue) / 100)
								) AS CHAR), '.', ',') AS netAmountPrice,

								ui.currencyTrans,
								CASE WHEN p.paySuppExtId = " . PAY_SUPPLIER_APP_STORE . " THEN '" . KIND_CC_APPSTORE . "' ELSE ucf.`kind` END AS creditCardType,
								ps.name AS suplierName,
								CASE WHEN ap.idPartnerGroup = " . PARTNER_B2B_G . " AND p.paySuppExtId <> " . PAY_SUPPLIER_B2B . " THEN ps.name ELSE UCASE(IF(ap.nameGroup='', ap.idPartnerGroup, ap.nameGroup)) END AS idPartnerGroupStr,
								CASE WHEN ap.idPartnerGroup = " . PARTNER_B2B_G . " AND p.paySuppExtId <> " . PAY_SUPPLIER_B2B . " THEN '" . (isset($partners[PARTNER_GROUP_B2B]) ? $partners[PARTNER_GROUP_B2B] : '') . "' WHEN ap.idPartnerGroup <> " . PARTNER_B2B_G . " THEN '" . (isset($partners[PARTNER_GROUP_B2C]) ? $partners[PARTNER_GROUP_B2C] : '') . "' END AS partnerGroup,
								p.isRecurring
						FROM ".$table."payments p
						INNER JOIN ".$table."user u ON u.id= p.userId
						INNER JOIN ".$table."user_invoices ui ON ui.idPayment = p.id
						LEFT JOIN ".$table."country c ON c.id = p.idCountry
						LEFT JOIN ".$table."pay_suppliers ps ON ps.id = p.paySuppExtId
						LEFT JOIN ".$table."aba_partners_list ap ON ap.idPartner = p.idPartner
						LEFT JOIN ".$table."aba_partners_list apPre ON p.idPartnerPrePay = apPre.idPartner
						LEFT JOIN ".$table."user_credit_forms ucf ON ucf.userId = p.userId AND p.idUserCreditForm = ucf.id

						LEFT JOIN ".$table."tax_commissions AS tcom ON tcom.paySuppExtId = p.paySuppExtId AND tcom.idCountry = p.idCountry

						$filter
						ORDER BY ui.dateInvoice DESC
        ";

        $connection = Yii::app()->dbSlave;
        /* @var $connection CDbConnection */
        $command = $connection->createCommand($sql);
        $dataReader=$command->query();
        return $dataReader;
    }

	static function generateIdPayment($id, $dateToPay=NULL )
    {
        $timeNow = $dateToPay;
        if( !isset($dateToPay) )
        {
            $timeNow = time();
            $timeNow = date('YmdHis', $timeNow );
        }
        else
        {
            $timeNow = new DateTime($dateToPay.date('H:i:s', time() ) );
            $timeNow = date('YmdHis', $timeNow->format("YmdHis") );
        }
        $theString=$id/*$this->user->id.$this->user->email*/.$timeNow ;
		$theStringRet = sprintf("%s_%02X", hash( "crc32b", $theString ), strlen($theString));
        return $theStringRet;
    }

	public function searchLastPayment($userId, $status=-1, $distintToRefund=false)
	{
        $statusStr = $status == -1 ? "" : " AND status=$status";
        $statusRefundStr = $distintToRefund == false ? "" : " AND status<>" . PAY_REFUND;
        $sql = "SELECT p.id as id
                FROM payments p
                WHERE p.userId=$userId $statusStr $statusRefundStr
		        ORDER BY p.dateStartTransaction DESC LIMIT 1 ";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();
        if (($row = $dataReader->read()) !== false) {
            return $row["id"];
        }

        return "";
	}

    /** Get last PENDING PAYMENT for this user.
     *
     * @param integer $userId
     * @return integer|bool
     */
    public function getLastPayPendingByUserId($userId) {
        $sql = " SELECT p.* FROM payments p
                    WHERE p.`userId`=$userId AND p.`status` = ".PAY_PENDING."
                    ORDER BY p.dateToPay DESC, p.`dateEndTransaction` DESC,
                             p.`dateStartTransaction` DESC, p.`autoId` DESC
                    LIMIT 1; ";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();
        if(($row = $dataReader->read()) !== false) {
            return $row["id"];
        }
        return false;
    }

    /**
     * @param $id
     *
     * @return AbaPayments
     * @throws CHttpException
     */
    static function loadModelById($id)
	{
		$modelLast=AbaPayments::model()->findByPk($id);
		if($modelLast===NULL)
			throw new CHttpException(404,'The requested page does not exist.');
		return $modelLast;
	}
	
	static function getModelBySql($sql)
	{
		$connection = Yii::app()->db;
		//$sql="SELECT * FROM payments p WHERE p.userId='$userid' and p.id='$idpayment'";
		$command = $connection->createCommand($sql);		
        $dataReader=$command->query();
		$model= new AbaPayments(); 
		if(($row = $dataReader->read())!==false)
		{
			foreach($row as $key=>$value)
				$model->$key = $row[$key];
		}
		return $model;
	}
	static function getPayment($userid, $idpayment)
	{
		$sql="SELECT * FROM payments p WHERE p.userId='$userid' and p.id='$idpayment'";
		return  AbaPayments::getModelBySql($sql);
	}

    /**
     * @param integer $userid
     * @param string $idpayment
     *
     * @return AbaPayments
     */
    static function getRefererRefundPayment($userid, $idpayment)
	{
		$sql="SELECT * FROM payments p ".
            "WHERE p.userId='$userid' AND p.idPayControlCheck='$idpayment' AND status='".PAY_REFUND."'";
		return  AbaPayments::getModelBySql($sql);
	}

    /**
     * @static
     *
     * @param AbaPayments $moFrmPayment form
     * @param string $successPayment id of Payment
     * @param string $idPayment
     * @param string $paySuppExtUnid
     * @param string $paySuppOrderId
     *
     * @throws CHttpException
     * @return bool
     */
    static function refundUser($moFrmPayment, $successPayment='', $idPayment, $paySuppExtUnid='', $paySuppOrderId="")
	{
        if ($successPayment != "") {
            $modelSuccess = AbaPayments::getPayment($moFrmPayment->userId, $successPayment); //load the SuccessPayment
            $amountToRefund = floatval($moFrmPayment->amountPrice);
            $theNow = HeDate::now();
            //Referrer to the success payment. The source of this refund.
            $moFrmPayment->id = $idPayment;
            $moFrmPayment->idPayControlCheck = $modelSuccess->id;
            $moFrmPayment->status = intval(PAY_REFUND);
            $moFrmPayment->dateStartTransaction = $theNow;
            $moFrmPayment->dateEndTransaction=$theNow;
            $moFrmPayment->amountPrice=$modelSuccess->amountPrice;
            $moFrmPayment->amountOriginal= $modelSuccess->amountOriginal;
            $paySuppOrderId = ($modelSuccess->paySuppExtId != PAY_SUPPLIER_PAYPAL AND $modelSuccess->paySuppExtId != PAY_SUPPLIER_ADYEN AND $modelSuccess->paySuppExtId != PAY_SUPPLIER_ADYEN_HPP) ? $paySuppOrderId : $modelSuccess->paySuppOrderId;
            $moFrmPayment->paySuppOrderId = $paySuppOrderId;
            if ($modelSuccess->paySuppExtId == PAY_SUPPLIER_PAYPAL OR $modelSuccess->paySuppExtId == PAY_SUPPLIER_CAIXA) {
                $moFrmPayment->paySuppExtProfId = $moFrmPayment->paySuppExtProfId;
            }

            //
            //#3132
            if($modelSuccess->paySuppExtId == PAY_SUPPLIER_ADYEN OR $modelSuccess->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP) {
                $moFrmPayment->paySuppExtUniId =    $modelSuccess->paySuppExtUniId;
                $moFrmPayment->paySuppExtProfId =   $paySuppExtUnid;
            }
            else {
                $moFrmPayment->paySuppExtUniId = $paySuppExtUnid;
            }

            //
            $moFrmPayment->paySuppLinkStatus = 0;
            $moFrmPayment->isRecurring = $modelSuccess->isRecurring;
            $moFrmPayment->isExtend = $modelSuccess->isExtend;

            $moFrmPayment->taxRateValue =           $modelSuccess->taxRateValue;
            $moFrmPayment->amountTax =              $modelSuccess->amountTax;
            $moFrmPayment->amountPriceWithoutTax =  $modelSuccess->amountPriceWithoutTax;

            //#5739
            $moFrmPayment->experimentVariationAttributeId =  $modelSuccess->experimentVariationAttributeId;

            //
            //
            $moFrmPayment->setAmountWithoutTax($theNow);
            //
            //

						//
						if($moFrmPayment->paySuppExtId == PAY_SUPPLIER_Z AND  trim($moFrmPayment->idUserCreditForm == "")) {
								$moFrmPayment->idUserCreditForm = 0;
						}

            if ($modelSuccess->amountPrice == $amountToRefund) {
                // FULL REFUND of the SUCCESS PAYMENT:*******************
                $moFrmPayment = AbaPayments::refundFull($moFrmPayment, $modelSuccess);
                return $moFrmPayment;

            } elseif ($modelSuccess->amountPrice > $amountToRefund) {
                // PARTIAL REFUND of the SUCCESS PAYMENT***************
                $moFrmPayment = AbaPayments::refundPartial($moFrmPayment, $modelSuccess, $amountToRefund);
                if(!$moFrmPayment){
                    throw new CHttpException(404, 'the refund is not possible, saving the REFUND raised an error. Check with IT');
                }
                return $moFrmPayment;
            } else {
                throw new CHttpException(404, 'the refund is not possible, the value specified is greater than the amount charged');
            }
        } else {
            throw new CHttpException(404, 'The refund is not possible, there is no successful payments for user id => ' . $moFrmPayment->userId);
        }

        return false;
    }

    /**
     * @param AbaPayments $moFrmPayment
     * @param AbaPayments $modelSuccess
     * @param float $amountToRefund
     * @return AbaPayments|bool
     *
     * @throws CHttpException
     */
    static function refundPartial( AbaPayments $moFrmPayment, AbaPayments $modelSuccess, $amountToRefund)
    {
        $idPaymentPending =             $moFrmPayment->searchLastPayment($moFrmPayment->userId, intval(PAY_PENDING));
        $theNewImport =                 $amountToRefund;
        $theOriginalPrice =             $modelSuccess->amountPrice;
//        $moFrmPayment->id="R".(AbaPayments::generateIdPayment($moFrmPayment->userId));
        $moFrmPayment->isNewRecord =    true;

        if($moFrmPayment->save(false)) {
            //
            //
            AbaPayments::paymentProcess($moFrmPayment);
            //
            //
            $theNow = HeDate::now();
            $theNow = HeDate::AddSeconds(1, $theNow);
            $moSuccessPartial = clone($moFrmPayment);
            $moSuccessPartial->id =                     "S".(AbaPayments::generateIdPayment($moFrmPayment->userId));
            $moSuccessPartial->status =                 intval(PAY_SUCCESS);
            $moSuccessPartial->dateToPay =              $theNow;
            $moSuccessPartial->dateEndTransaction =     $theNow;
            $moSuccessPartial->dateStartTransaction =   $theNow;
            $moSuccessPartial->amountOriginal =         $modelSuccess->amountOriginal;
            $moSuccessPartial->amountPrice =            $theOriginalPrice-$theNewImport;
            $moSuccessPartial->amountDiscount =         $moFrmPayment->amountOriginal-$theNewImport;
            $moSuccessPartial->idPayControlCheck =      $modelSuccess->id; // refered to the success payment
            $moSuccessPartial->isRecurring =            $moFrmPayment->isRecurring;
            $moSuccessPartial->isNewRecord =            true;

            //
            //
            $moSuccessPartial->setAmountWithoutTax($theNow);
            //
            //

            if ($moSuccessPartial->save(false)) {
                //
                //
                AbaPayments::paymentProcess($moSuccessPartial);
                //
                //
                return $moFrmPayment;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    /**
     * @param AbaPayments $moFrmPayment
     * @param AbaPayments $modelSuccess
     *
     * @return AbaPayments|bool
     */
    static function refundFull(AbaPayments $moFrmPayment, AbaPayments $modelSuccess)
    {
//        $moFrmPayment->id="A".(AbaPayments::generateIdPayment($moFrmPayment->userId));
        $moFrmPayment->isNewRecord = true;

        if ($moFrmPayment->save(false)) {
            //
            //
            AbaPayments::paymentProcess($moFrmPayment);
            //
            //
            return $moFrmPayment;
        }else{
            return false;
        }
    }


    /**
     * Set tax value, tax amount and total amount without tax
     *
     * @return bool
     */
    public function setAmountWithoutTax($dateEnd='') {

        //#4480
        if($this->paySuppExtId == PAY_SUPPLIER_APP_STORE) {

            $this->taxRateValue =           0;
            $this->amountTax =              0;
            $this->amountPriceWithoutTax =  $this->amountPrice;

            return true;
        }

        try {
            $oTaxRate = new AbaTaxRates();
            $oTaxRate->getTaxRateByCountry($this->idCountry, $dateEnd);

            $iAmountPriceWithoutTax =       $oTaxRate->getPriceWithoutTax($this->amountPrice);

            $this->taxRateValue =           $oTaxRate->getTaxRateValue();
            $this->amountTax =              HeMixed::getRoundAmount($this->amountPrice - $iAmountPriceWithoutTax);
            $this->amountPriceWithoutTax =  HeMixed::getRoundAmount($iAmountPriceWithoutTax);
        }
        catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param $moFrmPayment
     *
     * @return bool
     */
    public static function paymentProcess($moFrmPayment) {
        try {
            if(($moFrmPayment->status == PAY_SUCCESS || $moFrmPayment->status == PAY_REFUND) && $moFrmPayment->validateInvoicePartner()) {

                $moInvoice = new AbaUserInvoices();

                if($moInvoice->createPreInvoice($moFrmPayment, $moFrmPayment->userId)) {
                    return true;
                }

				HeLogger::sendLog(
					HeLogger::PREFIX_ERR_LOGIC_L . ' Intranet. Process create pre invoice failed',
					HeLogger::IT_BUGS_PAYMENTS,
					HeLogger::CRITICAL,
					'paymentProcess - createPreInvoice'
				);
            }
        } catch (Exception $e) {

			HeLogger::sendLog(
				HeLogger::PREFIX_ERR_LOGIC_L . ' Intranet. Process create pre invoice failed',
				HeLogger::IT_BUGS_PAYMENTS,
				HeLogger::CRITICAL,
				'paymentProcess'
			);
            return false;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function validateInvoicePartner() {

//        if($this->paySuppExtId == PAY_SUPPLIER_GROUPON || $this->paySuppExtId == PAY_SUPPLIER_B2B || $this->paySuppExtId == PAY_SUPPLIER_APP_STORE) {
        if($this->paySuppExtId == PAY_SUPPLIER_GROUPON || $this->paySuppExtId == PAY_SUPPLIER_B2B) {
            return false;
        }
        return true;
    }

		/**
		 * @param $userid
		 * @param $idpayment
		 *
		 * @return AbaPayments
		 */
		static function getModelRefundUser($userid, $idpayment)
		{
				return AbaPayments::getPayment($userid, $idpayment);
		}

		/**
		 * @param $userid
		 * @param $idpayment
		 *
		 * @return AbaPayments
		 */
		static function getModelDirectPaymentUser($userid, $idpayment)
		{
				return AbaPayments::getPayment($userid, $idpayment);
		}


    /** Returns THE PENDING payment
     * @param integer $userId
     *
     * @return AbaPayments
     * @throws CHttpException
     */
    static function getModelCancelFailUser($userId) {
        $moPayment = new AbaPayments;
        $idPayment = $moPayment->getLastPayPendingByUserId($userId);
        if(!$idPayment) {
            return false ; //throw new CHttpException(404, 'Put Cancel/Cancel is not posible, there is no PENDING payment for this user.');
        } else {
            $moPayPending = AbaPayments::loadModelById($idPayment);
        }
        return $moPayPending;
    }


    /**
     * @param AbaPayments $moPayment
     * 
     * @return bool
     * @throws CHttpException
     */
    static function cancelUser(AbaPayments $moPayment)
	{
        $modelUser = AbaUser::model()->findByPk($moPayment->userId);
        if ($modelUser===NULL){
            throw new CHttpException(404,'Request user not found.');
        }

        if ($moPayment->id==""){
            throw new CHttpException(404,'Payment not found.');
        } else {
            $moPayment->dateStartTransaction = HeDate::now();
            $moPayment->dateEndTransaction = HeDate::now();
            $moPayment->foreignCurrencyTrans=$moPayment->currencyTrans;
            $moPayment->isRecurring = 1;
            $moPayment->cancelOrigin = STATUS_CANCEL_INTRANET;
            $moPayment->lastAction = '['.HeDate::todaySQL(true).'], User '.
                Yii::app()->user->name.' intranet mark CANCEL, '.$moPayment->lastAction;
            $moPayment->isNewRecord=false;
        }

        $moPayment->status=PAY_CANCEL;
        if($moPayment->save(false)) {
            $modelUser->cancelReason = PAY_CANCEL_USER;
            $modelUser->save(false);
            return true;
        }
        return false;
	}

    /**
     * @param AbaPayments $moPayment
     * @return bool
     * @throws CHttpException
     */
    static function failUser(AbaPayments $moPayment)
    {
        $modelUser = AbaUser::model()->findByPk($moPayment->userId);
        if ($modelUser===NULL){
            throw new CHttpException(404,'Request user not found.');
        }

        if ($moPayment->id==""){
            throw new CHttpException(404,'Payment not found.');
        } else {
            $moPayment->dateStartTransaction = HeDate::now();
            $moPayment->dateEndTransaction = HeDate::now();
            $moPayment->foreignCurrencyTrans=$moPayment->currencyTrans;
            $moPayment->isRecurring = 1;
            $moPayment->cancelOrigin = STATUS_CANCEL_INTRANET;
            $moPayment->lastAction ='['.HeDate::todaySQL(true).'], User '.
                Yii::app()->user->name.' intranet mark FAIL, '.$moPayment->lastAction;
            $moPayment->isNewRecord=false;
        }

        $moPayment->status=PAY_FAIL;
        if($moPayment->save(false)) {
            $modelUser->cancelReason = PAY_CANCEL_FAILED_RENEW;
            $modelUser->save(false);
            return true;
        }
        return false;
    }


    /** After updating any data we send a request to update Selligent.
     * @throws CHttpException
     */
    protected function afterSave()
    {
        if(!HeWebServices::synchroUser($this->userId))
        {
            throw new CHttpException(404,'Can\'t update info in Selligent, web service response was negative.');
        }
        parent::afterSave();
    }

    /**
     * @param $idUser
     * @param $idUserCreditForm
     *
     * @return bool
     */
    public static function didChangeProduct($idUser, $idUserCreditForm)
    {
        $sql =              "SELECT * FROM payments WHERE userid = '" . $idUser . "' AND isRecurring = 1 AND idUserCreditForm = '" . $idUserCreditForm . "' ORDER BY dateToPay desc;";
        $connection =       Yii::app()->db;
        $command =          $connection->createCommand($sql);
        $result =           $command->queryAll();
        $currentProduct =   '';
        foreach($result as $singlePayLine) {
            if($currentProduct <> $singlePayLine['idPeriodPay']) {
                if($currentProduct <> '') {
                    return true;
                }
                $currentProduct = $singlePayLine['idPeriodPay'];
            }
        }
        return false;
    }

    /**
     * @param $model
     * @param $aRetCampusRefund
     *
     * @return bool
     */
    public static function checkAdyenRefundNotification($model, $aRetCampusRefund) {
        //
        //#4986
        $oSuccess = array("success" => true, "msg" => "");

        if($model->paySuppExtId == PAY_SUPPLIER_ADYEN OR $model->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP) {

//            ini_set('max_execution_time', 300);

            $oSuccess = array("success" => false, "msg" => "");
            //
            // ADYEN_HPP_ATTEMPTS_MAX_REFUND x ADYEN_HPP_ATTEMPTS_SLEEP == 20segs.
            for($iIteration = 0; $iIteration < ADYEN_HPP_ATTEMPTS_MAX_REFUND; $iIteration++) {

                $abaPaymentsAdyenNotifications = new AbaPaymentsAdyenNotifications();

                if($abaPaymentsAdyenNotifications->getAdyenNotificationByReferences($aRetCampusRefund["paySuppExtUnid"], $aRetCampusRefund["paySuppOrderId"])) {

                    $hppData = array('pspReference' => $aRetCampusRefund["paySuppExtUnid"], 'merchantReference' => $aRetCampusRefund["paySuppOrderId"]);

                    if($abaPaymentsAdyenNotifications->validateRefundNotification($hppData)) {

                        $oSuccess = array("success" => true, "msg" => "");

                        // @TODO - check original SUCCESS payment in status 30 with paySuppExtUniId == originalReference
                        // @TODO - E-mail BUGS
                        // @TODO - check original SUCCESS payment in status 30 with paySuppExtUniId == originalReference
                    }
                    else {
                        $oSuccess = array("success" => false, "msg" => trim($abaPaymentsAdyenNotifications->reason));
                    }
                    break;
                }
                else {
                    sleep(ADYEN_HPP_ATTEMPTS_SLEEP);
                    continue;
                }
            }
        }

        return $oSuccess;
    }

    /** Save pending adyen refund after supplier plataform comunication
     *
     * @param $model
     * @param $aRetCampusRefund
     *
     * @return bool
     */
    public static function saveAdyenRefundAttempt($model, $aRetCampusRefund, $paymentId) {

        $oAbaPaymentsAdyenPendingRefunds = new AbaPaymentsAdyenPendingRefunds();

        if(isset($aRetCampusRefund["idPayment"]) AND ($model->paySuppExtId == PAY_SUPPLIER_ADYEN OR $model->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP)) {

            $oAbaPaymentsAdyenPendingRefunds->idPayment =           $aRetCampusRefund["idPayment"];
            $oAbaPaymentsAdyenPendingRefunds->idPaymentOrigin =     $paymentId;
            $oAbaPaymentsAdyenPendingRefunds->pspReference =        $aRetCampusRefund["paySuppExtUnid"];

            $oAbaPaymentsAdyenPendingRefunds->paySuppOrderId =      $model->paySuppOrderId;
            $oAbaPaymentsAdyenPendingRefunds->amountPrice =         $model->amountPrice;
            $oAbaPaymentsAdyenPendingRefunds->dateToPay =           $model->dateToPay;

            $oAbaPaymentsAdyenPendingRefunds->merchantReference =   $aRetCampusRefund["paySuppOrderId"];
            $oAbaPaymentsAdyenPendingRefunds->status =              PAY_ADYEN_PENDING_REFUND_PENDING;
            $oAbaPaymentsAdyenPendingRefunds->requestDate =         HeDate::todaySQL(true);

            return $oAbaPaymentsAdyenPendingRefunds->insertPaymentAdyen();
        }

        return true;
    }

    /**
     * @param $id
     *
     * @return AbaPaymentsAdyenPendingRefunds|bool
     */
    public static function getAdyenRefundAttempt($id) {
        $oAbaPaymentsAdyenPendingRefunds = new AbaPaymentsAdyenPendingRefunds();
        if($oAbaPaymentsAdyenPendingRefunds->getAdyenPendingRefundById($id)) {
            return $oAbaPaymentsAdyenPendingRefunds;
        }
        return false;
    }

    /**
     * @param $id
     * @param array $status
     *
     * @return AbaPaymentsAdyenPendingRefunds|bool
     */
    public static function getAdyenRefundAttemptByPaymentOrigin($id, $status=array()) {
        $oAbaPaymentsAdyenPendingRefunds = new AbaPaymentsAdyenPendingRefunds();
        if($oAbaPaymentsAdyenPendingRefunds->getAdyenPendingRefundByPaymentIdOrigin($id, $status)) {
            return $oAbaPaymentsAdyenPendingRefunds;
        }
        return false;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public static function updateAdyenRefundAttempt($id) {
        $oAbaPaymentsAdyenPendingRefunds = new AbaPaymentsAdyenPendingRefunds();
        $oAbaPaymentsAdyenPendingRefunds->getAdyenPaymentById($id);
        return $oAbaPaymentsAdyenPendingRefunds->updateToRefund();
    }

}
