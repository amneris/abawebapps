<?php

/**
 * This is the model class for table "products_promos".
 *
 * The followings are the available columns in table 'products_promos':
 * @property string $idPromoCode
 * @property string $idPeriodPay
 * @property string $idProduct
 * @property string $idPartner
 * @property string $descriptionText
 * @property string $type
 * @property string $value
 * @property string $dateStart
 * @property string $dateEnd
 * @property integer $enabled
 * @property integer $visibleWeb
 * @property string $showValidationType
 * @property integer $idDescription
 * @property integer $idPromoType
 */
class AbaProductsPromos extends CActiveRecord
{

    const PROMO_TYPE_GENERAL = 1;
    const PROMO_TYPE_FAMILYPLAN = 2;
    const PROMO_TYPE_ALL = 3;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AbaProductsPromos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products_promos';
	}

    public static function getValidationTypes()
    {
        return array(
            array('id'=>'A', 'title'=>'Visible & real'),
            array('id'=>'B', 'title'=>'Never expire & not visible'),
						array('id'=>'C', 'title'=>'Plus 3 days'),
						array('id'=>'D', 'title'=>'Plus 1 day'),
        );
    }

    public static function getPromoTypes()
    {
        return array(
            array('id' => self::PROMO_TYPE_GENERAL, 'title' => 'General'),
            array('id' => self::PROMO_TYPE_FAMILYPLAN, 'title' => 'Family Plan'),
            array('id' => self::PROMO_TYPE_ALL, 'title' => 'All'),
        );
    }

    public static function getPromoType($type)
    {
        switch ($type)
        {
            case self::PROMO_TYPE_GENERAL:
                return 'General';
                break;
            case self::PROMO_TYPE_FAMILYPLAN:
                return 'Family Plan';
                break;
            case self::PROMO_TYPE_ALL:
                return 'All';
                break;
        }
    }

    public static function getValidationType($type)
    {
        switch ($type)
        {
            case 'A':
                return 'Visible & real';
            case 'B':
                return 'Never expire & not visible';
            case 'C':
                return 'Plus 3 days';
						case 'D':
							return 'Plus 1 day';
        }
    }

    public static function getTypes()
    {
        return array(
            array('id'=>'PERCENTAGE', 'title'=>'Percentage'),
            array('id'=>'FINALPRICE', 'title'=>'Final Price'),
        );
    }

    public static function getType($type)
    {
        switch ($type)
        {
            case 'PERCENTAGE':
                return 'Percentage';

            case 'FINALPRICE':
                return 'Final Price';

        }
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idPromoCode, value', 'required'),
			array('enabled, visibleWeb, idDescription, idPromoType', 'numerical', 'integerOnly'=>true),
			array('idPromoCode', 'length', 'max'=>50),
			array('idProduct, idPeriodPay', 'length', 'max'=>15),
			array('descriptionText', 'length', 'max'=>20),
			array('type', 'length', 'max'=>10),
			array('value', 'length', 'max'=>18),
      array('showValidationType', 'length', 'max'=>1),
			array('dateStart, dateEnd', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idPromoCode, idProduct, descriptionText, type, value, dateStart, dateEnd, enabled, visibleWeb, showValidationType, idPeriodPay, idDescription, idPromoType', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idPromoCode' => 'Id Promo Code',
			'idProduct' => 'Id Product',
      'idPeriodPay' => 'Id Period Pay',
			'descriptionText' => 'Description Text',
			'type' => 'Type',
			'value' => 'Value',
			'dateStart' => 'Date Start',
			'dateEnd' => 'Date End',
			'enabled' => 'Enabled',
			'visibleWeb' => 'Visible Web',
      'showValidationType' => 'Validation type',
      'idDescription' => 'Web description',
      'idPromoType' => 'Promo type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idPromoCode',$this->idPromoCode,true);
		$criteria->compare('idProduct',$this->idProduct,true);
    $criteria->compare('idPeriodPay',$this->idPeriodPay,true);
		$criteria->compare('descriptionText',$this->descriptionText,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('dateStart',$this->dateStart,true);
		$criteria->compare('dateEnd',$this->dateEnd,true);
		$criteria->compare('enabled',$this->enabled);
		$criteria->compare('visibleWeb',$this->visibleWeb);
    $criteria->compare('showValidationType',$this->showValidationType);
    $criteria->compare('idDescription',$this->idDescription);
    $criteria->compare('idPromoType',$this->idPromoType);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}