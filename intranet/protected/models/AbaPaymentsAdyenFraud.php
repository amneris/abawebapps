<?php

/**
 * This is the model class for table "payments_adyen_frauds".
 * The followings are the available columns in table 'payments_adyen_frauds':
 */
class AbaPaymentsAdyenFraud extends CmAbaPaymentsAdyenFraud
{
    public function __construct($scenario = 'insert', $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'idFraud' => 'Id Fraud',
          'userId' => 'User ID',
          'idPayment' => 'Payment ID',
          'amountCurrency' => 'Currency',
          'amountValue' => 'Amount',
          'eventCode' => 'Event code',
          'merchantReference' => 'Merchant reference',
          'originalReference' => 'Adyen original reference',
          'pspReference' => 'Adyen reference',
          'reason' => 'Description',
          'paymentMethod' => 'Pyment method',
          'status' => 'Status',
          'dateAdd' => 'The time the event was generated.',
        );
    }

    /**
     * @return CActiveDataProvider
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idFraud', $this->idFraud, false);
        $criteria->compare('userId', $this->userId, true);
        $criteria->compare('idPayment', $this->idPayment, true);
        $criteria->compare('amountCurrency', $this->amountCurrency, true);
        $criteria->compare('amountValue', $this->amountValue, false);
        $criteria->compare('eventCode', $this->eventCode, false);
        $criteria->compare('merchantReference', $this->merchantReference, true);
        $criteria->compare('originalReference', $this->originalReference, true);
        $criteria->compare('pspReference', $this->pspReference, true);
        $criteria->compare('reason', $this->reason, true);
        $criteria->compare('paymentMethod', $this->paymentMethod, true);
        $criteria->compare('status', $this->status, false);
        $criteria->compare('dateAdd', $this->dateAdd, true);

        $criteria->order = 'idFraud DESC';

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
          'pagination' => array('pageSize' => 50)
        ));
    }

    /**
     * @return array
     */
    public static function adyenFraudsStates()
    {
        return array(
          array('id' => self::STATUS_PENDING, 'title' => 'Pending'),
          array('id' => self::STATUS_MANUALLY_REFUNDED, 'title' => 'Refunded'),
          array('id' => self::STATUS_INITIATED_DISPUTE, 'title' => 'Iniciated Dispute'),
          array('id' => self::STATUS_SUCCESS_AFTER_DISPUTE, 'title' => 'Success After Dispute'),
        );
    }

    /**
     * @param $state
     *
     * @return string
     */
    public static function adyenFraudsState($state)
    {
        switch ($state) {
            case self::STATUS_PENDING:
                return 'Pending';
                break;
            case self::STATUS_MANUALLY_REFUNDED:
                return 'Refunded';
                break;
            case self::STATUS_INITIATED_DISPUTE:
                return 'Iniciated Dispute';
                break;
            case self::STATUS_SUCCESS_AFTER_DISPUTE:
                return 'PendSuccess After Disputeing';
                break;
        }
    }
}

