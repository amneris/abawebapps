<?php

/**
 * Class AbaValidateCreditCardForm
 */
class AbaValidateCreditCardForm extends CFormModel
{
    public $cardNumber;
    public $cardNumberTypeId;
    public $cardNumberType;

    /**
     * @return array
     */
    public function rules()
    {
        return array(
          array('cardNumber', 'required'),
          array('cardNumber', 'length', 'min' => 12, 'max' => 18),
          array('cardNumberTypeId', 'type', 'type' => 'integer', 'allowEmpty' => true),
          array('cardNumberType', 'type', 'type' => 'string', 'allowEmpty' => true),
          array(
            'cardNumber, cardNumberTypeId, cardNumberType',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
          'cardNumber' => 'Credit card number',
          'cardNumberTypeId' => 'Credit card type ID',
          'cardNumberType' => 'Credit card type',
        );
    }

    const UNKNOWN_CREDIT_CARD_TYPE = 'UNKNOWN CREDIT CARD TYPE';

    /**
     * @return bool
     */
    public function validateCardType()
    {
        $stCreditCardTypes = HeList::creditCardDescription();

        if (!$this->validate()) {
            $this->addError('cardNumber', self::UNKNOWN_CREDIT_CARD_TYPE);
            return false;
        }

        if (!(bool)preg_match('/^[0-9]*$/', trim($this->cardNumber))) {  /* '/^[A-Za-z0-9]+$/u' */  /* '/^\w*$/' */
            $this->addError('cardNumber', 'Card number can contain only numeric characters.');
            $this->addError('cardNumber', self::UNKNOWN_CREDIT_CARD_TYPE);
            return false;
        }

        if (!isset($stCreditCardTypes[$this->cardNumberTypeId])) {
            $this->addError('cardNumber', self::UNKNOWN_CREDIT_CARD_TYPE);
            return false;
        }

        $this->cardNumberType = $stCreditCardTypes[$this->cardNumberTypeId];

        return true;
    }

}
