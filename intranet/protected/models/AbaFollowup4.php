<?php

/**
 * This is the model class for table "followup4".
 *
 * The followings are the available columns in table 'followup4':
 * @property string $id
 * @property string $themeid
 * @property string $userid
 * @property string $lastchange
 * @property string $all_por
 * @property string $all_err
 * @property string $all_ans
 * @property string $all_time
 * @property string $sit_por
 * @property string $stu_por
 * @property string $dic_por
 * @property string $dic_ans
 * @property string $dic_err
 * @property string $rol_por
 * @property string $gra_por
 * @property string $gra_ans
 * @property string $gra_err
 * @property string $wri_por
 * @property string $wri_ans
 * @property string $wri_err
 * @property string $new_por
 * @property string $spe_por
 * @property string $spe_ans
 * @property string $time_aux
 * @property string $gra_vid
 * @property string $rol_on
 * @property string $exercises
 * @property string $eva_por
 */
class AbaFollowup4 extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AbaFollowup4 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'followup4';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('themeid, all_por, all_err, all_ans, all_time, sit_por, stu_por, dic_por, dic_ans, dic_err, rol_por, gra_por, gra_ans, gra_err, wri_por, wri_ans, wri_err, new_por, spe_por, spe_ans, time_aux, eva_por, gra_vid', 'length', 'max'=>4),
			array('userid', 'length', 'max'=>60),			
			array('rol_on', 'length', 'max'=>1),
			array('exercises', 'length', 'max'=>25),
			array('lastchange', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, themeid, userid, lastchange, all_por, all_err, all_ans, all_time, sit_por, stu_por, dic_por, dic_ans, dic_err, rol_por, gra_por, gra_ans, gra_err, wri_por, wri_ans, wri_err, new_por, spe_por, spe_ans, time_aux, gra_vid, rol_on, exercises, eva_por', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'themeid' => 'Unit number',
			'userid' => 'User id',
			'lastchange' => 'Last change',
			'all_por' => 'All percentage',
			'all_err' => 'All errors',
			'all_ans' => 'All answers',
			'all_time' => 'All Time',
			'sit_por' => 'Situation percentage',
			'stu_por' => 'Study percentage',
			'dic_por' => 'Dictation percentage',
			'dic_ans' => 'Dictation answers',
			'dic_err' => 'Dictation errors',
			'rol_por' => 'Rolplay percentage',
			'gra_por' => 'Grammar percentage',
			'gra_ans' => 'Grammar answers',
			'gra_err' => 'Grammar errors',
			'wri_por' => 'Writing percentage',
			'wri_ans' => 'Writing answers',
			'wri_err' => 'Writing errors',
			'new_por' => 'New percentage',
			'spe_por' => 'Speech percentage',
			'spe_ans' => 'Speech answers',
			'time_aux' => 'Time Aux',
			'gra_vid' => 'Grammar Video',
			'rol_on' => 'Rol On',
			'exercises' => 'Exercises',
			'eva_por' => 'Evaluation percentage',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$theValue = $this->userid;
		if(HeString::isValidEmail($this->userid))
		{
			$sql="SELECT count(*) as num, p.id FROM user p WHERE p.email='{$this->userid}' limit 1";
			
			$connection = Yii::app()->db;
			$command = $connection->createCommand($sql);		
			$dataReader=$command->query();
			if(($row = $dataReader->read())!==false)
			{
				if($row["num"]!==0)
					$theValue=$row["id"]; 	
				else
					$theValue="-1";					
			}	
		}
		$criteria=new CDbCriteria;



//echo "<br />" . $theValue . "<br />" . $this->userid . "<br />";

//if(isset($_GET['userid']) AND is_numeric($_GET['userid'])) {
//
//    $attributes['userid'] = (int) $_GET['userid'];
//
//    $model->setAttributes($attributes);
//}

		$criteria->compare('id',$this->id,true);
		$criteria->compare('themeid',$this->themeid,true);
//		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('userid',$theValue,false);
		$criteria->compare('lastchange',$this->lastchange,true);
		$criteria->compare('all_por',$this->all_por,true);
		$criteria->compare('all_err',$this->all_err,true);
		$criteria->compare('all_ans',$this->all_ans,true);
		$criteria->compare('all_time',$this->all_time,true);
		$criteria->compare('sit_por',$this->sit_por,true);
		$criteria->compare('stu_por',$this->stu_por,true);
		$criteria->compare('dic_por',$this->dic_por,true);
		$criteria->compare('dic_ans',$this->dic_ans,true);
		$criteria->compare('dic_err',$this->dic_err,true);
		$criteria->compare('rol_por',$this->rol_por,true);
		$criteria->compare('gra_por',$this->gra_por,true);
		$criteria->compare('gra_ans',$this->gra_ans,true);
		$criteria->compare('gra_err',$this->gra_err,true);
		$criteria->compare('wri_por',$this->wri_por,true);
		$criteria->compare('wri_ans',$this->wri_ans,true);
		$criteria->compare('wri_err',$this->wri_err,true);
		$criteria->compare('new_por',$this->new_por,true);
		$criteria->compare('spe_por',$this->spe_por,true);
		$criteria->compare('spe_ans',$this->spe_ans,true);
		$criteria->compare('time_aux',$this->time_aux,true);
		$criteria->compare('gra_vid',$this->gra_vid,true);
		$criteria->compare('rol_on',$this->rol_on,true);
		$criteria->compare('exercises',$this->exercises,true);
		$criteria->compare('eva_por',$this->eva_por,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /** After updating any data we send a request to update Selligent.
     * @throws CHttpException
     */
    protected function afterSave()
    {
        if(!HeWebServices::synchroUser($this->userid))
        {
            throw new CHttpException(404,'Can\'t send the change password notification to Selligent');
        }

        parent::afterSave();
    }
}