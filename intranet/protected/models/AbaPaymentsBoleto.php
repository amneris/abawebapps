<?php

/**
 * This is the model class for table "payments_boleto".
 *
 * The followings are the available columns in table 'payments_boleto':
 * @property string $id
 * @property string $idPayment
 * @property string $idPaySupplier
 * @property string $paySuppExtUniId
 * @property string $url
 * @property string $requestDate
 * @property string $dueDate
 * @property string $status
 * @property string $xmlOrderId
 */
class AbaPaymentsBoleto extends CActiveRecord
{
    public $name;
    public $email;
    public $userType;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payments_boleto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idPayment, idPaySupplier', 'required'),
			array('idPayment', 'length', 'max'=>15),
			array('idPaySupplier', 'length', 'max'=>1),
			array('paySuppExtUniId', 'length', 'max'=>45),
			array('status', 'length', 'max'=>2),
			array('xmlOrderId', 'length', 'max'=>20),
			array('url, requestDate, dueDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idPayment, idPaySupplier, paySuppExtUniId, url, requestDate, dueDate, status, xmlOrderId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idPayment' => 'Id Payment',
			'idPaySupplier' => 'Id Pay Supplier',
			'paySuppExtUniId' => 'Pay Supp Ext Uni',
			'url' => 'Url',
			'requestDate' => 'Request Date',
			'dueDate' => 'Due Date',
			'status' => 'Status',
			'xmlOrderId' => 'Xml Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idPayment',$this->idPayment,true);
		$criteria->compare('idPaySupplier',$this->idPaySupplier,true);
		$criteria->compare('paySuppExtUniId',$this->paySuppExtUniId,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('requestDate',$this->requestDate,true);
		$criteria->compare('dueDate',$this->dueDate,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('xmlOrderId',$this->xmlOrderId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AbaPaymentsBoleto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function countAll($startDate, $endDate, $boletoStatus, $criterio, $idPayment)
    {
        $count = 0;
        $sql = "SELECT COUNT(*) FROM aba_b2c.payments_boleto b INNER JOIN aba_b2c.payments_control_check c ON b.idPayment = c.id INNER JOIN aba_b2c.user u ON c.userId = u.id WHERE u.id = u.id";

        if($startDate<>'')
        {
            if($endDate<>'')
            {
                $sql.= " AND b.requestDate BETWEEN '$startDate' AND '$endDate 23:59:59'";
            }
            else
            {
                $sql.= " AND b.requestDate >= '$startDate'";
            }
        }
        elseif($endDate<>'')
        {
            $sql.= " AND b.requestDate <= '$endDate 23:59:59'";
        }

        if($idPayment<>'')
        {
            $sql .= " AND b.idPayment like '%".$idPayment."%'";
        }

        if($boletoStatus<>'all')
        {
            $sql .= " AND b.status = ".$boletoStatus;
        }

        if($criterio<>'')
        {
            $sql .= " AND u.email like '%".$criterio."%'";
        }

        $count += Yii::app()->db->createCommand($sql)->queryScalar();
        return $count;
    }

    public function getAllBoletosWithUserInfo($pages, $startDate, $endDate, $boletoStatus, $criterio, $idPayment)
    {
        $initPage = $pages->getCurrentPage() * $pages->getPageSize();
        $endPage = $pages->getPageSize();

        $sql = "SELECT b.*, CONCAT(u.name,', ', u.surnames) AS name, u.email, IF(u.userType=2, 'Premium', IF(u.userType=0, 'Deleted', IF(u.cancelReason IS NULL AND u.userType=1, 'Free' , 'Ex-Premium'))) as 'userType'
FROM aba_b2c.payments_boleto b INNER JOIN aba_b2c.payments_control_check c ON b.idPayment = c.id INNER JOIN aba_b2c.user u ON c.userId = u.id WHERE u.id = u.id";


        if($startDate<>'')
        {
            if($endDate<>'')
            {
                $sql.= " AND b.requestDate BETWEEN '$startDate' AND '$endDate 23:59:59'";
            }
            else
            {
                $sql.= " AND b.requestDate >= '$startDate'";
            }
        }
        elseif($endDate<>'')
        {
            $sql.= " AND b.requestDate <= '$endDate 23:59:59'";
        }

        if($idPayment<>'')
        {
            $sql .= " AND b.idPayment like '%".$idPayment."%'";
        }

        if($boletoStatus<>'all')
        {
            $sql .= " AND b.status = ".$boletoStatus;
        }

        if($criterio<>'')
        {
            $sql .= " AND u.email like '%".$criterio."%'";
        }


        $sql.=  " ORDER BY b.requestDate DESC LIMIT ".$initPage.",".$endPage.";";

        $dataProvider = new CSqlDataProvider($sql,
            array(
                'totalItemCount'=>$pages->getItemCount(),
                'pagination'=>array('pageSize'=>$pages->getPageSize(),
                )));
        $dataProvider->keyField = 'id';
        $dataProvider->db = Yii::app()->db;
        return $dataProvider;

    }

    public function getBoletosWithUserInfoById($id)
    {
        $sql = "SELECT b.*, CONCAT(u.name,', ', u.surnames) AS name, u.email, IF(u.userType=2, 'Premium', IF(u.userType=0, 'Deleted', IF(u.cancelReason IS NULL AND u.userType=1, 'Free' , 'Ex-Premium'))) as 'userType'
FROM aba_b2c.payments_boleto b INNER JOIN aba_b2c.payments_control_check c ON b.idPayment = c.id INNER JOIN aba_b2c.user u ON c.userId = u.id
                  WHERE b.id =".$id." ORDER BY b.requestDate DESC;";

        $result = Yii::app()->db->createCommand($sql)->queryRow();

        $model = new AbaPaymentsBoleto();

        $model->name = $result['name'];
        $model->email = $result['email'];
        $model->id = $result['id'];
        $model->idPayment = $result['idPayment'];
        $model->idPaySupplier = $result['idPaySupplier'];
        $model->paySuppExtUniId = $result['paySuppExtUniId'];
        $model->url = $result['url'];
        $model->requestDate = $result['requestDate'];
        $model->dueDate = $result['dueDate'];
        $model->status = $result['status'];
        $model->userType = $result['userType'];
        $model->xmlOrderId = $result['xmlOrderId'];

        return $model;
    }

    /**
     * @return array
     */
    public function getLastTotals()
    {
        $sSql = "
            SELECT COUNT(*) AS numPayments, DATE(pa.dateStartTransaction) AS dataPayments,
                GROUP_CONCAT(DISTINCT
                    CASE pa.`status`
                        WHEN '" . PAY_PENDING . "' THEN 'PENDING'
                        WHEN '" . PAY_INITIATED . "' THEN 'INITIATED'
                        WHEN '" . PAY_FAIL . "' THEN 'FAIL'
                        WHEN '" . PAY_CANCEL . "' THEN 'CANCEL'
                        WHEN '" . PAY_SUCCESS . "' THEN 'SUCCESS'
                        WHEN '" . PAY_REFUND . "' THEN 'REFUND'
                        ELSE '' END
                    ) AS statusPayments
            FROM `aba_b2c`.`payments` AS pa
            WHERE
                pa.paySuppExtId = " . PAY_SUPPLIER_ALLPAGO_BR_BOL . "
                AND DATE(pa.dateStartTransaction) >= DATE_ADD(NOW(), INTERVAL -2 MONTH)
            GROUP BY DATE(pa.dateStartTransaction)
            ORDER BY DATE(pa.dateStartTransaction) DESC
            ;
        ";

        $resultData =   array();
        $connection =   Yii::app()->dbSlave;
        $command =      $connection->createCommand($sSql);
        $result =       $command->query();
        while(($row = $result->read()) !== false) {
            if(isset($row['numPayments'])) {
                $resultData[] = $row;
            }
        }
        return $resultData;
    }

}
