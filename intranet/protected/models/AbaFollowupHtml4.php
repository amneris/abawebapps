<?php

/**
 * This is the model class for table "followup_html4".
 *
 * The followings are the available columns in table 'followup_html4':
 * @property string $id
 * @property string $userid
 * @property string $themeid
 * @property string $page
 * @property string $section
 * @property string $action
 * @property string $audio
 * @property integer $bien
 * @property string $mal
 * @property string $lastchange
 * @property string $wtext
 * @property string $difficulty
 * @property string $time_aux
 * @property string $contador
 */
class AbaFollowupHtml4 extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AbaFollowupHtml4 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'followup_html4';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid, themeid, page, section, action, audio, bien, mal, lastchange, wtext, difficulty, time_aux, contador', 'required'),
			array('bien', 'numerical', 'integerOnly'=>true),
			array('themeid, page, mal, difficulty, contador', 'length', 'max'=>10),
			array('userid', 'length', 'max'=>60),
			array('section, action, audio', 'length', 'max'=>20),
			array('wtext', 'length', 'max'=>100),
			array('time_aux', 'length', 'max'=>4),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, userid, themeid, page, section, action, audio, bien, mal, lastchange, wtext, difficulty, time_aux, contador', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userid' => 'Userid',
			'themeid' => 'Unit number',
			'page' => 'Page',
			'section' => 'Section',
			'action' => 'Action',
			'audio' => 'Audio',
			'bien' => 'Bien',
			'mal' => 'Mal',
			'lastchange' => 'Lastchange',
			'wtext' => 'Wtext',
			'difficulty' => 'Difficulty',
			'time_aux' => 'Time Aux',
			'contador' => 'Contador',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$theValue=$this->userid;
		if(HeString::isValidEmail($this->userid))
		{
			$sql="SELECT count(*) as num, p.id FROM user p WHERE p.email='{$this->userid}' limit 1";
			
			$connection = Yii::app()->db;
			$command = $connection->createCommand($sql);		
			$dataReader=$command->query();
			if(($row = $dataReader->read())!==false)
			{
				if($row["num"]!==0)
					$theValue=$row["id"]; 	
				else
					$theValue="-1";					
			}	
		}
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
//		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('userid',$theValue,false);
		$criteria->compare('themeid',$this->themeid,true);
		$criteria->compare('page',$this->page,true);
		$criteria->compare('section',$this->section,true);
		$criteria->compare('action',$this->action,true);
		$criteria->compare('audio',$this->audio,true);
		$criteria->compare('bien',$this->bien);
		$criteria->compare('mal',$this->mal,true);
		$criteria->compare('lastchange',$this->lastchange,true);
		$criteria->compare('wtext',$this->wtext,true);
		$criteria->compare('difficulty',$this->difficulty,true);
		$criteria->compare('time_aux',$this->time_aux,true);
		$criteria->compare('contador',$this->contador,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}