<?php

class AbaUserProgressQueuesDetails extends CmAbaUserProgressQueuesDetails
{
    public function __construct($scenario = 'insert', $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaUserProgressQueues the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'queueDetailId' => 'ID queue detail',
          'queueId' => 'ID queue',
          'unit' => 'Unit',
          'dateStart' => 'Process start',
          'dateEnd' => 'Process end',
          'status' => 'Current status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('queueDetailId', $this->queueDetailId, true);
        $criteria->compare('queueId', $this->queueId, true);
        $criteria->compare('unit', $this->units, true);
        $criteria->compare('dateStart', $this->dateStart, true);
        $criteria->compare('dateEnd', $this->dateEnd, true);
        $criteria->compare('status', $this->status, true);

        $criteria->order = "queueDetailId ASC ";

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
        ));
    }

    public function searchQueueDetails($pages, $stParams=array()) {

        $iQueueId = (isset($stParams['queueId']) ? $stParams['queueId'] : null);
        $iUserId = (isset($stParams['userId']) ? $stParams['userId'] : null);

        $sSql = "
            SELECT
                upqd.*, upq.dateAdd,
                IF(upqd.queueId = '" . $iQueueId . "', 1, 0) AS currentQueue
            " . "
            FROM " . $this->tableName() . " AS upqd
            JOIN user_progress_queues upq
                ON upq.queueId = upqd.queueId
            WHERE
                upq.userId = '" . $iUserId . "'
            ORDER BY
                upqd.queueId DESC, queueDetailId ASC
        ";

// echo $sSql; exit();

        $dataProvider = new CSqlDataProvider($sSql ,
          array(
            'totalItemCount' => 1000,
            'pagination' => array('pageSize' => 1000)
          )
        );
        $dataProvider->keyField = 'queueDetailId';
        $dataProvider->db = Yii::app()->db;

        return $dataProvider;
    }

    /**
     * @param bool|true $runValidation
     * @param null $attributes
     *
     * @return bool
     */
    public function save($runValidation = true, $attributes = null)
    {
        if ($this->validateUserProgressQueueDetail()) {
            return parent::save($runValidation, $attributes);
        }
        return false;
    }

    /**
     * @return bool
     */
    public function validateUserProgressQueueDetail()
    {

//        $this->units = trim(str_replace(" ", "", $this->units));
//
//        if($this->units == '') {
//            $this->addError('units', 'ERRORRRRR');
//            return false;
//        }
//
//        $stUnits = explode(",", $this->units);
//
//        if(count($stUnits) <= 0) {
//            $this->addError('units', 'ERRORRRRR');
//            return false;
//        }
//
//        foreach($stUnits as $iKey => $iUnit) {
//            if(!is_numeric($iUnit) OR $iUnit == 0) {
//                $this->addError('units', 'ERRORRRRR !!!!!');
//                return false;
//            }
//        }

        return true;
    }

}
