<?php

/**
 * This is the model class for table "user_products_stamp".
 *
 * The followings are the available columns in table 'user_products_stamp':
 * @property string $id
 * @property integer $userId
 * @property string $idProduct
 * @property string $idCountry
 * @property string $idPeriodPay
 * @property string $priceOfficialCry
 * @property string $idCurrency
 * @property string $dateCreation
 * @property string $idPromoCode
 * @property integer $upToDatePaid
 */
class AbaUserProductsStamp extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AbaUserProductsStamp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_products_stamp';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idProduct, idCountry, idPeriodPay, priceOfficialCry, idCurrency, dateCreation', 'required'),
			array('userId, upToDatePaid', 'numerical', 'integerOnly'=>true),
			array('idProduct, priceOfficialCry', 'length', 'max'=>10),
			array('idCountry', 'length', 'max'=>4),
			array('idPeriodPay', 'length', 'max'=>5),
			array('idCurrency', 'length', 'max'=>3),
			array('idPromoCode', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, userId, idProduct, idCountry, idPeriodPay, priceOfficialCry, idCurrency, dateCreation, idPromoCode, upToDatePaid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userId' => 'User',
			'idProduct' => 'Id Product',
			'idCountry' => 'Id Country',
			'idPeriodPay' => 'Id Period Pay',
			'priceOfficialCry' => 'Price Official Cry',
			'idCurrency' => 'Id Currency',
			'dateCreation' => 'Date Creation',
			'idPromoCode' => 'Id Promo Code',
			'upToDatePaid' => 'Up To Date Paid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('userId',$this->userId);
		$criteria->compare('idProduct',$this->idProduct,true);
		$criteria->compare('idCountry',$this->idCountry,true);
		$criteria->compare('idPeriodPay',$this->idPeriodPay,true);
		$criteria->compare('priceOfficialCry',$this->priceOfficialCry,true);
		$criteria->compare('idCurrency',$this->idCurrency,true);
		$criteria->compare('dateCreation',$this->dateCreation,true);
		$criteria->compare('idPromoCode',$this->idPromoCode,true);
		$criteria->compare('upToDatePaid',$this->upToDatePaid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}