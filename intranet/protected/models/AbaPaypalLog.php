<?php

/**
 * This is the model class for table "paypal_log".
 *
 * The followings are the available columns in table 'paypal_log':
 * @property string $a
 * @property string $status
 * @property string $receiver_email
 * @property string $payer_email
 * @property string $txn_type
 * @property string $subscr_id
 * @property string $username
 * @property string $tipopago
 * @property string $recomendacion
 * @property string $cp
 * @property string $producto
 * @property string $timestamp
 */
class AbaPaypalLog extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AbaPaypalLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'paypal_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status, receiver_email, payer_email, txn_type, subscr_id, username, tipopago, recomendacion, cp, producto, timestamp', 'required'),
			array('status, receiver_email, payer_email, txn_type, subscr_id, username, tipopago, recomendacion, cp', 'length', 'max'=>255),
			array('producto', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('a, status, receiver_email, payer_email, txn_type, subscr_id, username, tipopago, recomendacion, cp, producto, timestamp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'a' => 'A',
			'status' => 'Status',
			'receiver_email' => 'Receiver Email',
			'payer_email' => 'Payer Email',
			'txn_type' => 'Txn Type',
			'subscr_id' => 'Subscr',
			'username' => 'Username',
			'tipopago' => 'Tipopago',
			'recomendacion' => 'Recomendacion',
			'cp' => 'Cp',
			'producto' => 'Producto',
			'timestamp' => 'Timestamp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('a',$this->a,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('receiver_email',$this->receiver_email,true);
		$criteria->compare('payer_email',$this->payer_email,true);
		$criteria->compare('txn_type',$this->txn_type,true);
		$criteria->compare('subscr_id',$this->subscr_id,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('tipopago',$this->tipopago,true);
		$criteria->compare('recomendacion',$this->recomendacion,true);
		$criteria->compare('cp',$this->cp,true);
		$criteria->compare('producto',$this->producto,true);
		$criteria->compare('timestamp',$this->timestamp,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}