<?php

class AbaPartnerbusinessareas extends CmAbaPartnerbusinessareas
{
    public function __construct($scenario = 'insert', $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AbaPartnerbusinessareas the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'idBusinessArea' => 'ID',
          'nameBusinessArea' => 'Business area name',
          'dateAdd' => 'Last update',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('idBusinessArea', $this->idBusinessArea, true);
        $criteria->compare('nameBusinessArea', $this->nameBusinessArea, true);
        $criteria->compare('dateAdd', $this->dateAdd, true);

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
          'pagination' => array(
            'pageSize' => 100,
          ),
        ));
    }

    /**
     * @param bool $runValidation
     * @param null $attributes
     *
     * @return bool
     */
    public function save($runValidation = true, $attributes = null)
    {
        if ($this->validatenameBusinessArea()) {
            return parent::save($runValidation, $attributes);
        }
        return false;
    }

    /**
     * @return bool
     */
    public function validatenameBusinessArea()
    {
        //
        if (trim($this->nameBusinessArea) == '') {
            $this->addError('nameBusinessArea', 'The field nameBusinessArea is mandatory');
            return false;
        }

        //
        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " pgba ";
        $sSql .= " WHERE pgba.nameBusinessArea = '" . addslashes($this->nameBusinessArea) . "' ";

        if (is_numeric($this->idBusinessArea)) {
            $sSql .= " AND pgba.idBusinessArea <> '" . $this->idBusinessArea . "' ";
        }

        $sSql .= " LIMIT 0, 1 ";

        $dataReader = $this->querySQL($sSql);
        if (($row = $dataReader->read()) !== false) {
            $this->addError('nameBusinessArea', 'This Identifier already exists in the database');
            return false;
        }
        return true;
    }

    /**
     * @return array
     */
    public function getPartnerBusinessAreas()
    {
        $stAllDescriptions = $this->getAllPartnerBusinessAreas();

        $stFormattedDescription = array();

        foreach ($stAllDescriptions as $stDescription) {
            $stFormattedDescription[$stDescription['idBusinessArea']] = $stDescription['nameBusinessArea'];
        }

        return $stFormattedDescription;
    }

    /**
     * @param bool $bEmpty
     *
     * @return array
     */
    public function getFullList($bEmpty = false)
    {
        $stAllDescriptions = $this->getAllPartnerBusinessAreas();

        $stFormattedDescription = array();

        if ($bEmpty) {
            $stFormattedDescription[""] = "";
        }

        foreach ($stAllDescriptions as $stDescription) {
            $stFormattedDescription[$stDescription['idBusinessArea']] = $stDescription['nameBusinessArea'];
        }

        return $stFormattedDescription;
    }

}
