<?php

class AbaExperimentsVariations extends CmAbaExperimentsVariations
{
    public function __construct($scenario = 'insert', $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaProductPrice the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'experimentVariationId' => 'ID',
          'experimentId' => 'Experiment',
          'experimentVariationIdentifier' => 'Unique Identifier',
          'experimentVariationDescription' => 'Description',
          'counterUsers' => 'Users counter',
          'limitUsers' => 'Users limit',
          'isDefault' => 'By default',
          'experimentVariationStatus' => 'Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('experimentVariationId', $this->experimentVariationId, true);
        $criteria->compare('experimentId', $this->experimentId, true);
        $criteria->compare('experimentVariationIdentifier', $this->experimentVariationIdentifier, true);
        $criteria->compare('experimentVariationDescription', $this->experimentVariationDescription, true);
        $criteria->compare('counterUsers', $this->counterUsers);
        $criteria->compare('limitUsers', $this->limitUsers);
        $criteria->compare('isDefault', $this->isDefault);
        $criteria->compare('experimentVariationStatus', $this->experimentVariationStatus);

        $criteria->order = 'experimentVariationId DESC';

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
          'pagination' => array(
            'pageSize' => 75,
          ),
        ));
    }

    /**
     * @param bool $runValidation
     * @param null $attributes
     *
     * @return bool
     */
    public function save($runValidation = true, $attributes = null)
    {
        if ($this->validateExperimentsVariationIdentifier()) {
            return parent::save($runValidation, $attributes);
        }
        return false;
    }

    /**
     * @return bool
     */
    public function validateExperimentsVariationIdentifier()
    {
        //
        if (trim($this->experimentVariationIdentifier) == '') {
            $this->addError('experimentVariationIdentifier', 'The field experimentVariationIdentifier is mandatory');
            return false;
        }

        //
        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " exv ";
        $sSql .= " WHERE exv.experimentVariationIdentifier = '" . addslashes($this->experimentVariationIdentifier) . "' ";

        if (is_numeric($this->experimentVariationId)) {
            $sSql .= " AND exv.experimentVariationId <> '" . $this->experimentVariationId . "' ";
        }

        $sSql .= " LIMIT 0, 1 ";

        $dataReader = $this->querySQL($sSql);
        if (($row = $dataReader->read()) !== false) {
            $this->addError('experimentVariationIdentifier', 'This Identifier already exists in the database');
            return false;
        }
        return true;
    }


    /**
     * @return array
     */
    public function getExperimentsVariations()
    {
        $stAllDescriptions = $this->getAllExperimentsVariations();

        $stFormattedDescription = array();

        foreach ($stAllDescriptions as $stDescription) {
            $stFormattedDescription[] = array(
              'id' => $stDescription['experimentVariationId'],
              'title' => $stDescription['experimentVariationIdentifier']
            );
        }

        return $stFormattedDescription;
    }

    /**
     * @param bool $bEmpty
     *
     * @return array
     */
    public function getFullList($bEmpty = false)
    {
        $stAllDescriptions = $this->getAllExperimentsVariations();

        $stFormattedDescription = array();

        if ($bEmpty) {
            $stFormattedDescription[""] = "";
        }

        foreach ($stAllDescriptions as $stDescription) {
            $stFormattedDescription[$stDescription['experimentVariationId']] = $stDescription['experimentVariationIdentifier'];
        }

        return $stFormattedDescription;
    }

}
