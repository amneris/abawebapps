<?php

class AbaExperimentsVariationsAttributes extends CmAbaExperimentsVariationsAttributes
{
    public function __construct($scenario='insert', $dbConnName=NULL) {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaProductPrice the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'experimentVariationAttributeId' => 'ID',
            'experimentVariationId' => 'Experiment variation',
            'idProduct' => 'Product',
            'idCountry' => 'Country',
            'price' => 'Price',
            'enabled' => 'Enabled',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('experimentVariationAttributeId', $this->experimentVariationAttributeId, true);
        $criteria->compare('experimentVariationId', $this->experimentVariationId, true);
        $criteria->compare('idProduct', $this->idProduct, true);
        $criteria->compare('idCountry', $this->idCountry);
        $criteria->compare('price', $this->price, true);
        $criteria->compare('enabled', $this->enabled);

        $criteria->order = 'experimentVariationId DESC, idCountry ASC, experimentVariationAttributeId DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 100,
            ),
        ));
    }

}
