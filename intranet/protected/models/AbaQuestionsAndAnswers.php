<?php

/**
 * This is the model class for table "messages".
 *
 * The followings are the available columns in table 'messages':
 * @property integer $id
 * @property integer $sender_id
 * @property integer $receiver_id
 * @property string  $subject
 * @property string  $body
 * @property string  $is_read
 * @property string  $deleted_by
 * @property string  $created_at
 */
class AbaQuestionsAndAnswers extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return AbaQuestionsAndAnswers the static model class
     */

    public $senderInfo = array();
    public $receiverInfo = array();

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'messages';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('sender_id, receiver_id, created_at', 'required'),
            array('sender_id, receiver_id', 'numerical', 'integerOnly' => true),
            array('subject', 'length', 'max' => 256),
            array('is_read', 'length', 'max' => 1),
            array('deleted_by', 'length', 'max' => 8),
            array('body', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array(
                'id, sender_id, receiver_id, subject, body, is_read, deleted_by, created_at',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'          => 'ID',
            'sender_id'   => 'Sender ID',
            'receiver_id' => 'Receiver ID',
            'subject'     => 'Subject',
            'body'        => 'Body',
            'is_read'     => 'Is Read',
            'deleted_by'  => 'Deleted By',
            'created_at'  => 'Created At',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('sender_id', $this->sender_id);
        $criteria->compare('receiver_id', $this->receiver_id);
        $criteria->compare('subject', $this->subject, true);
        $criteria->compare('body', $this->body, true);
        $criteria->compare('is_read', $this->is_read, true);
        $criteria->compare('deleted_by', $this->deleted_by, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->addCondition(" subject NOT IN ('Bem-vindo ao campus!','Welcome to the campus!','Bienvenue sur le campus!', 'Benvenuto al campus!','Bienvenido al campus!','Willkommen auf dem Campus!') ");

        $sql = "
          SELECT m.id as primaryKey,m.id, m.sender_id,m.receiver_id,m.subject,m.body
          FROM messages m
          WHERE $criteria->condition
          LIMIT 50;";

        $command = Yii::app()->db->createCommand($sql)->setFetchMode(PDO::FETCH_OBJ);

        foreach ($criteria->params as $key => $value) {
            $command->bindParam("$key", $value, PDO::PARAM_STR);
        }

        $rawData = $command->queryAll();

        $dataProvider = new CArrayDataProvider($rawData, array(
                'id'         => 'id',
                'pagination' => array('pageSize' => 10)
            )
        );

        return $dataProvider;

        //return new CActiveDataProvider($this, array('criteria'=>$criteria));
    }

    public function _getQuestionsAndAnswers2014($startDate, $endDate)
    {
        $table = Yii::app()->params["mainDbName"];

        $sql = "SELECT u0.email, u0.name, u0.surnames, u0.id as teacherid, u0.period_preguntas as period_questions, u1.period_respondidas as period_answers
              FROM
              (
                SELECT count(*) as period_preguntas, m.receiver_id as id, u.email, u.name, u.surnames FROM " . $table . ".messages m
                INNER JOIN " . $table . ".user u ON u.id= m.receiver_id and u.userType=4
                WHERE m.created_at between '$startDate' AND '$endDate 23:59:59' group by m.receiver_id
              ) u0
              LEFT JOIN
              (
                SELECT count(*) as period_respondidas, m.sender_id as id FROM " . $table . ".messages m
                INNER JOIN " . $table . ".user_teacher ut ON ut.userid=m.sender_id
                WHERE m.created_at between '$startDate' AND '$endDate 23:59:59' AND (m.subject like 'RE: %' OR m.subject
                NOT IN ('Bem-vindo ao campus!','Welcome to the campus!','Bienvenue sur le campus!', 'Benvenuto al campus!','Bienvenido al campus!','Willkommen auf dem Campus!'))
                group by m.sender_id) u1 ON u1.id=u0.id;";

        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();

        return $result;
    }

    public function _getTotalQuestionsAndAnswers2014()
    {
        $table = Yii::app()->params["mainDbName"];

        $sql = "SELECT u0.email, u0.name, u0.surnames, u0.id as teacherid, u2.total_users, u0.total_preguntas as total_questions, u1.total_respuestas as total_answers
              FROM
              (
                SELECT count(*) as total_preguntas, m.receiver_id as id, u.email, u.name, u.surnames FROM " . $table . ".messages m
	            INNER JOIN " . $table . ".user u ON u.id= m.receiver_id and u.userType=4
                group by m.receiver_id
              ) u0
              LEFT JOIN
              (
                SELECT count(*) as total_respuestas, m.sender_id as id FROM " . $table . ".messages m
                INNER JOIN " . $table . ".user_teacher ut ON ut.userid=m.sender_id
                WHERE (m.subject like 'RE: %' OR m.subject NOT IN ('Bem-vindo ao campus!','Welcome to the campus!','Bienvenue sur le campus!', 'Benvenuto al campus!','Bienvenido al campus!','Willkommen auf dem Campus!'))
                group by m.sender_id) u1 ON u1.id=u0.id
              LEFT JOIN
              (
	            SELECT count(*) as total_users, teacherid  as id FROM " . $table . ".user ui WHERE ui.userType=2 group by teacherid) u2 ON u2.id=u0.id;";

        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();

        return $result;
    }

    public function getQuestionsAndAnswers($startDate, $endDate)
    {
        $table = Yii::app()->params["mainDbName"];

        $sql = "SELECT u.email, u.name, u.surnames,ut.userid as teacherid,c1.period_preguntas AS period_questions,c2.period_respondidas AS period_answers
              FROM `aba_b2c`.`user_teacher` ut
              INNER JOIN `aba_b2c`.`user` u
              LEFT JOIN (
                    SELECT COUNT(*) AS period_preguntas,m.receiver_id AS recid
              FROM aba_b2c.messages m
              WHERE m.created_at BETWEEN '$startDate' AND '$endDate 23:59:59'
              GROUP BY m.receiver_id
              ) c1 ON c1.recid=u.id
              LEFT JOIN (
                    SELECT COUNT(*) AS period_respondidas, m.sender_id AS senid
              FROM aba_b2c.messages m
              WHERE m.created_at BETWEEN '$startDate' AND '$endDate 23:59:59' AND m.sendFromRole IS NOT NULL
              GROUP BY m.sender_id
              ) c2 ON c2.senid=u.id
              WHERE ut.userid=u.id;";

//        $sql="SELECT u0.email, u0.name, u0.surnames, u0.id as teacherid, u0.period_preguntas as period_questions, u1.period_respondidas as period_answers
//              FROM
//              (
//                SELECT count(*) as period_preguntas, m.receiver_id as id, u.email, u.name, u.surnames FROM ".$table.".messages m
//                INNER JOIN ".$table.".user u ON u.id= m.receiver_id and u.userType=4
//                WHERE m.created_at between '$startDate' AND '$endDate 23:59:59' group by m.receiver_id
//              ) u0
//              LEFT JOIN
//              (
//                SELECT COUNT(*) as period_respondidas, tt1.id
//                FROM
//                (
//                    SELECT m.sender_id as id
//                    FROM ".$table.".messages m
//                    WHERE
//                        m.created_at BETWEEN '$startDate' AND '$endDate 23:59:59'
//                        AND m.sendFromRole IS NOT NULL
//                ) AS tt1
//                INNER JOIN ".$table.".user_teacher ut ON ut.userid=tt1.id
//                GROUP BY tt1.id
//              ) u1 ON u1.id=u0.id;";

        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();

        return $result;
    }

    public function getTotalQuestionsAndAnswers()
    {
        $table = Yii::app()->params["mainDbName"];
        $sql = "
        SELECT u.email, u.name, u.surnames,ut.userid as teacherid,c3.total_users, c1.total_preguntas AS total_questions, c2.total_respuestas AS total_answers
              FROM `$table`.`user_teacher` ut
              INNER JOIN `$table`.`user` u
              LEFT JOIN (
        SELECT COUNT(*) AS total_preguntas ,m.receiver_id AS recid
              FROM $table.messages m
              GROUP BY m.receiver_id
              ) c1 ON c1.recid=u.id
              LEFT JOIN (
        SELECT COUNT(*) AS total_respuestas, m.sender_id AS senid
              FROM $table.messages m
              WHERE m.sendFromRole IS NOT NULL
              GROUP BY m.sender_id
              ) c2 ON c2.senid=u.id
              LEFT JOIN (
        SELECT COUNT(*) as total_users, teacherid  as id
						  FROM $table.user u
						  WHERE u.userType=2
						  group by teacherid
              ) c3 ON c3.id=u.id
              WHERE ut.userid=u.id;";

//        $sql="SELECT u0.email, u0.name, u0.surnames, u0.id as teacherid, u2.total_users, u0.total_preguntas as total_questions, u1.total_respuestas as total_answers
//              FROM
//              (
//                SELECT COUNT(*) as total_preguntas, m.receiver_id as id, u.email, u.name, u.surnames FROM ".$table.".messages m
//	            INNER JOIN ".$table.".user u ON u.id= m.receiver_id and u.userType=4
//                group by m.receiver_id
//              ) u0
//              LEFT JOIN
//              (
//                SELECT COUNT(*) as total_respuestas, tt1.id
//                FROM
//                (
//                    SELECT m.sender_id as id
//                    FROM ".$table.".messages m
//                    WHERE
//                        m.sendFromRole IS NOT NULL
//                ) AS tt1
//                INNER JOIN ".$table.".user_teacher ut ON ut.userid=tt1.id
//                GROUP BY tt1.id
//              ) u1 ON u1.id=u0.id
//              LEFT JOIN
//              (
//	            SELECT COUNT(*) as total_users, teacherid  as id FROM ".$table.".user ui WHERE ui.userType=2 group by teacherid
//              ) u2 ON u2.id=u0.id;";

        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();

        return $result;
    }

    public function getSenderOrReceiver($userId)
    {
        $table = Yii::app()->params["mainDbName"];

        $sql = "SELECT name, surnames, email FROM " . $table . ".user where id=$userId;";

        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result = $command->queryAll();

        return $result;
    }
}