<?php
class AbaAgrupadorSales extends CModel
{
    
    public $agrupador=1;
    public $startDate;
    public $endDate;
    public $showDate=0;
   
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
	public function rules()
    {
        return array(
            array('agrupador, startDate, endDate, showDate', 'required'),
        );
    }
    
	public function search()
	{
		
	}
	
	public function relations()
	{
		return array();
	}

    public function attributeLabels()
	{

		return array(
            'agrupador'=> 'Grouper',
            'startDate'=> 'Start date',
            'endDate'=> 'End date',
            'showDate'=> 'Show date',
		);
	}   
    
    public function attributeNames()
	{
		return array(
			'agrupador',
            'startDate',
            'endDate',
            'showDate',
		);
	}  
	
}
?>