<?php

class AbaExperimentsUserCategories extends CmAbaExperimentsUserCategories
{
    public function __construct($scenario = 'insert', $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaProductPrice the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'userCategoryId' => 'ID',
          'userCategoryDescription' => 'Category description',
          'dateAdd' => 'Create date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('userCategoryId', $this->userCategoryId, true);
        $criteria->compare('userCategoryDescription', $this->userCategoryDescription, true);
        $criteria->compare('dateAdd', $this->dateAdd, true);

        $criteria->order = 'userCategoryId';

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
          'pagination' => array(
            'pageSize' => 10,
          ),
        ));
    }

    /**
     * @return array
     */
    public function getExperimentsUserCategories()
    {
        $stAllDescriptions = $this->getAllExperimentsUserCategories();

        $stFormattedDescription = array();

        foreach ($stAllDescriptions as $stDescription) {
            $stFormattedDescription[] = array(
              'id' => $stDescription['userCategoryId'],
              'title' => $stDescription['userCategoryDescription']
            );
        }

        return $stFormattedDescription;
    }

    /**
     * @param bool $bEmpty
     *
     * @return array
     */
    public function getFullList($bEmpty = false)
    {
        $stAllDescriptions = $this->getAllExperimentsUserCategories();

        $stFormattedDescription = array();

        if ($bEmpty) {
            $stFormattedDescription[""] = "";
        }

        foreach ($stAllDescriptions as $stDescription) {
            $stFormattedDescription[$stDescription['userCategoryId']] = $stDescription['userCategoryDescription'];
        }

        return $stFormattedDescription;
    }

}
