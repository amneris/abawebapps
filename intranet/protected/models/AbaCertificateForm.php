<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class AbaCertificateForm extends CFormModel
{
	public $email;
	public $level;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('level, email', 'required'),
			// email has to be a valid email address
			array('email', 'email'),
            array('level', 'type', 'type' => 'integer', 'allowEmpty' => false),
            array('level', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => 6),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
            'email' => 'User email',
            'level' => 'Choose a level to certificate',
		);
	}
}