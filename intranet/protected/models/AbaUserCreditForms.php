<?php

/**
 * This is the model class for table "user_credit_forms".
 *
 * The followings are the available columns in table 'user_credit_forms':
 * @property string $userId
 * @property string $id
 * @property integer $kind
 * @property string $cardNumber
 * @property string $cardYear
 * @property string $cardMonth
 * @property string $cardCvc
 * @property string $cardName
 * @property integer $default
 * @property integer $cpfBrasil
 * @property integer $typeCpf
 * @property string $registrationId
 */
class AbaUserCreditForms extends CActiveRecord
{
    const WORD4CARDS = "Yii ABA English Key";

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return AbaUserCreditForms the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_credit_forms';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
          array('userId, kind', 'required'),
          array('default', 'numerical', 'integerOnly' => true),
          array('userId', 'length', 'max' => 64),
          array('kind', 'length', 'max' => 2),
          array('cardName', 'length', 'max' => 45),
          array('cardNumber, cardYear, cardMonth, cardCvc, typeCpf, cpfBrasil, registrationId', 'safe'),
            /* The following rules on create scenario */
          array('cardNumber, cardMonth, cardYear, cardCvc', 'required', 'on' => 'create'),
          array('kind', 'isKindCreditCards', 'on' => 'create'),
          array('userId', 'numerical', 'min' => 5, 'on' => 'create'),
          array('cardName', 'length', 'min' => 5, 'max' => 45, 'on' => 'create'),
          array('cardCvc', 'length', 'min' => 3, 'max' => 4, 'on' => 'create'),
          array('cardYear', 'length', 'min' => 4, 'max' => 4, 'on' => 'create'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
          array(
            'userId, id, kind, cardNumber, cardYear, cardMonth, cardCvc, cardName, default, cpfBrasil, typeCpf',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /** Validation function called from create form.
     * @param $attribute
     * @param $params
     */
    public function isKindCreditCards($attribute, $params)
    {
        if ($this->kind == KIND_CC_PAYPAL) {
            $this->addError($attribute, Yii::t('mainApp', 'Only credit cards allowed allowed at the moment.'));
        }

    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        /*	return array(
          );*/
        return array(
          'Usuario' => array(self::BELONGS_TO, 'AbaUser', 'id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'userId' => 'User',
          'id' => 'ID',
          'kind' => 'Kind',
          'cardNumber' => 'Card Number',
          'cardYear' => 'Card Year',
          'cardMonth' => 'Card Month',
          'cardCvc' => 'Card Cvc',
          'cardName' => 'Card Name',
          'default' => 'Default',
          'cpfBrasil' => 'CPF/CPNJ',
          'typeCpf' => 'Type of identification(Brazil)',
          'registrationId' => 'Registration'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $theUserId = $this->userId;
        if (HeString::isValidEmail(trim($this->userId))) {
            $sql = "SELECT count(*) as num, p.id FROM user p WHERE p.email='" . trim($this->userId) . "' limit 1";
            $connection = Yii::app()->db;
            $command = $connection->createCommand($sql);
            $dataReader = $command->query();
            if (($row = $dataReader->read()) !== false) {
                if ($row["num"] !== 0) {
                    $theUserId = $row["id"];
                }
            }
        }
        $criteria->compare('userId', $theUserId/*$this->userId*/, false);
        $criteria->compare('id', $this->id, false);
        $criteria->compare('kind', $this->kind, false);
        $aesCode = (AbaUserCreditForms::AES_encode($this->cardNumber));
        if ($this->cardNumber != "") {
            $criteria->addInCondition('cardNumber', array($aesCode), 'AND');
        } else {
            $criteria->compare('cardNumber', $this->cardNumber, true);
        }
        $criteria->compare('cardYear', $this->cardYear, true);
        $criteria->compare('cardMonth', $this->cardMonth, true);
        $criteria->compare('cardCvc', $this->cardCvc, true);
        $criteria->compare('cardName', $this->cardName, true);
        $criteria->compare('default', $this->default);
        $criteria->compare('cpfBrasil', $this->cpfBrasil, true);

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
        ));
    }

    static function AES_decode($string)
    {
        $connection = Yii::app()->db;
        $string = addslashes($string); //I no recomended the use.... but mysql_real_escape_string need the link to the connection: next improvement
        $sql = "SELECT  AES_DECRYPT('$string', '" . WORD4CARDS . "') as data";
        try {
            $command = $connection->createCommand($sql);
            $dataReader = $command->query();
            if (($row = $dataReader->read()) !== false) {
                if ($row["data"] == null) {
                    return '';
                }
                return $row["data"];
            }
        } catch (Exception $e) {
        }
        return "<ERROR>";
    }

    static function AES_encode($string)
    {
        $connection = Yii::app()->db;
        $sql = "SELECT  AES_ENCRYPT('$string', '" . WORD4CARDS . "') as data";
        try {
            $command = $connection->createCommand($sql);
            $dataReader = $command->query();
            if (($row = $dataReader->read()) !== false) {
                return $row["data"];
            }
        } catch (Exception $e) {
        }
        return "<ERROR>";
    }

}