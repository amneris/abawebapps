<?php
/**
 *
 *
 **/
class LogCronDeletion extends AbaActiveRecord
{
    public $id;
    public $dynTable;

    /**
     * @param string $tableName
     *
     * @return LogCronDeletion
     */
    public function __construct($tableName)
    {
        $this->dynTable = $tableName;
        parent::__construct();
    }


    public function tableName()
    {
        return Yii::app()->params['dbCampusLogs'].'.'.$this->dynTable;
    }

   /* public function mainFields()
    {
        return "   l.`id`, l.`dateSent`, l.`url`, l.`webservice`, l.`fields`, l.`response`, l.`userId`, l.`sourceAction` ";
    }*/


    /**
     * @param $colSearch
     * @param $dateFilter
     *
     * @return int
     */
    public function deleteLog( $colSearch, $dateFilter )
    {
        $sql= " DELETE FROM ".$this->tableName()." WHERE ".
                     " DATE(".$colSearch.") < DATE('".$dateFilter."')";


        $rowsAffected = $this->executeSQL( $sql, array(), true);
        if ( $rowsAffected > 0)
        {
            return $rowsAffected;
        }
        else
        {
            return 0;
        }
    }
}