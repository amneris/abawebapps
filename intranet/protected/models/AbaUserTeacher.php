<?php

/**
 * This is the model class for table "user_teacher".
 *
 * The followings are the available columns in table 'user_teacher':
 * @property integer $userid
 * @property string $nationality
 * @property string $language
 * @property string $photo
 */
class AbaUserTeacher extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AbaUserTeacher the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_teacher';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid', 'numerical', 'integerOnly'=>true),
			array('nationality', 'length', 'max'=>100),
			array('language, photo', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('userid, nationality, language, photo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'userid' => 'Userid',
			'nationality' => 'Nationality',
			'language' => 'Language',
			'photo' => 'Photo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('userid',$this->userid);
		$criteria->compare('nationality',$this->nationality,true);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('photo',$this->photo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function getFullList($idTeacher=-1)
	{
		$sql="SELECT u.id, u.email FROM user u INNER JOIN user_teacher t ON u.id=t.userid";
		if($idTeacher!= -1)
		$sql.=" WHERE id=$idTeacher";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);		
        $dataReader=$command->query();
		$theArray=array();
		while(($row = $dataReader->read())!==false)
			$theArray[$row['id']]=$row['email'];
		return $theArray;
	}
}