<?php

/**
 * This is the model class for table "followup4".
 *
 * The followings are the available columns in table 'followup4':
 * @property integer $id
 * @property string $progressVersion
 * @property string $idFollowup
 * @property integer $themeid
 * @property integer $userid
 * @property string $lastchange
 * @property integer $all_por
 * @property integer $all_err
 * @property integer $all_ans
 * @property integer $all_time
 * @property integer $sit_por
 * @property integer $stu_por
 * @property integer $dic_por
 * @property integer $dic_ans
 * @property integer $dic_err
 * @property integer $rol_por
 * @property integer $gra_por
 * @property integer $gra_ans
 * @property integer $gra_err
 * @property integer $wri_por
 * @property integer $wri_ans
 * @property integer $wri_err
 * @property integer $new_por
 * @property integer $spe_por
 * @property integer $spe_ans
 * @property integer $time_aux
 * @property integer $gra_vid
 * @property integer $rol_on
 * @property string $exercises
 * @property integer $eva_por
 */
class B2cFollowup extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return B2cFollowup the static model class
	 */

    public $name;
    public $surnames;
    public $email;
    public $telephone;
    public $entryDate;
    public $expirationDate;
    public $idFollowup;
    public $themeid;
    public $currentLevel;
    public $userType;

    public $levelRange = array('begStart'=>1, 'begEnd'=>24, 'lowStart'=>25, 'lowEnd'=>48, 'intStart'=>48, 'intEnd'=>72, 'uppStart'=>73, 'uppEnd'=>96,
        'advStart'=>97, 'advEnd'=>120, 'busStart'=>121, 'busEnd'=>144);


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
//		return 'followup4';
        return Yii::app()->params['dbCampusSummary'].".followup4_summary";
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('themeid, userid, all_por, all_err, all_ans, all_time, sit_por, stu_por, dic_por, dic_ans, dic_err, rol_por, gra_por, gra_ans, gra_err, wri_por, wri_ans, wri_err, new_por, spe_por, spe_ans, time_aux, eva_por', 'length', 'max'=>4),
			array('gra_vid, rol_on', 'length', 'max'=>1),
			array('exercises', 'length', 'max'=>25),
			array('lastchange', 'safe'),
            array('progressVersion, idFollowup', 'type', 'type' => 'string', 'allowEmpty' => false),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, progressVersion, idFollowup, themeid, userid, lastchange, all_por, all_err, all_ans, all_time, sit_por, stu_por, dic_por, dic_ans, dic_err, rol_por, gra_por, gra_ans, gra_err, wri_por, wri_ans, wri_err, new_por, spe_por, spe_ans, time_aux, gra_vid, rol_on, exercises, eva_por', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'progressVersion' => 'Progress version (table)',
            'idFollowup' => 'ID',
            'name'=> 'Name',
            'surnames'=> 'Surnames',
            'email'=> 'Email',
            'telephone'=> 'Phone',
            'entryDate'=> 'Entry date',
            'expirationDate'=> 'Expiration date',
            'idFollowup'=> 'Folowup id',
            'themeid'=> 'Unit',
			'id' => 'ID',
			'userid' => 'User id',
			'lastchange' => 'Last change',
			'all_por' => 'Total %',
			'all_err' => 'Errors',
			'all_ans' => 'Answer',
			'all_time' => 'Total time',
			'sit_por' => 'ABA film %',
			'stu_por' => 'Speak %',
			'dic_por' => 'Write %',
			'dic_ans' => 'Answer',
			'dic_err' => 'Errors',
			'rol_por' => 'Interpret %',
			'gra_por' => 'Grammar %',
			'gra_ans' => 'Answer',
			'gra_err' => 'Errors',
			'wri_por' => 'Exercises %',
			'wri_ans' => 'Answer',
			'wri_err' => 'Errors',
			'new_por' => 'Vocabulary %',
			'spe_por' => 'Speak %',
			'spe_ans' => 'Answer',
			'time_aux' => 'Time aux',
			'gra_vid' => 'Video class %',
			'rol_on' => 'Rol on',
			'exercises' => 'Exercises',
			'eva_por' => 'Assessment',
            'userType' => 'User type'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
        $criteria->compare('progressVersion',$this->progressVersion,true);
        $criteria->compare('idFollowup',$this->idFollowup,true);
		$criteria->compare('themeid',$this->themeid,true);
		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('lastchange',$this->lastchange,true);
		$criteria->compare('all_por',$this->all_por,true);
		$criteria->compare('all_err',$this->all_err,true);
		$criteria->compare('all_ans',$this->all_ans,true);
		$criteria->compare('all_time',$this->all_time,true);
		$criteria->compare('sit_por',$this->sit_por,true);
		$criteria->compare('stu_por',$this->stu_por,true);
		$criteria->compare('dic_por',$this->dic_por,true);
		$criteria->compare('dic_ans',$this->dic_ans,true);
		$criteria->compare('dic_err',$this->dic_err,true);
		$criteria->compare('rol_por',$this->rol_por,true);
		$criteria->compare('gra_por',$this->gra_por,true);
		$criteria->compare('gra_ans',$this->gra_ans,true);
		$criteria->compare('gra_err',$this->gra_err,true);
		$criteria->compare('wri_por',$this->wri_por,true);
		$criteria->compare('wri_ans',$this->wri_ans,true);
		$criteria->compare('wri_err',$this->wri_err,true);
		$criteria->compare('new_por',$this->new_por,true);
		$criteria->compare('spe_por',$this->spe_por,true);
		$criteria->compare('spe_ans',$this->spe_ans,true);
		$criteria->compare('time_aux',$this->time_aux,true);
		$criteria->compare('gra_vid',$this->gra_vid,true);
		$criteria->compare('rol_on',$this->rol_on,true);
		$criteria->compare('exercises',$this->exercises,true);
		$criteria->compare('eva_por',$this->eva_por,true);
        $criteria->compare('userType',$this->userType,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


//$obj->progressVersion=$row['progressVersion'];
//$obj->idFollowup=$row['idFollowup'];


    public function findByLevel($level, $pageObject, $criterio, $defaultPartner, $defaultPartnerGroup, $startDate, $endDate, $entryDate)
    {
        $sql="SELECT i.name, i.surnames, i.email, i.telephone, i.currentLevel, i.entryDate, i.expirationDate, f.id as idFollowup, f.*, IF(i.userType=2, 'Premium',
              IF(i.userType=0, 'Deleted', IF(i.cancelReason IS NULL AND i.userType=1, 'Free' , 'Ex-Premium'))) as 'userType'
              FROM " . $this->tableName() . " f INNER JOIN  aba_b2c.user i ON f.userid = i.id WHERE ";
//              FROM aba_b2c.followup4 f INNER JOIN  aba_b2c.user i ON f.userid = i.id WHERE ";
        if($entryDate)
            $sql.= "i.entryDate BETWEEN '$startDate' AND '$endDate 23:59:59' AND";
        else
            $sql.= "i.expirationDate BETWEEN '$startDate' AND '$endDate 23:59:59' AND";

        switch ($level)
        {
            case 'Beginners':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['begStart']." AND ".$this->levelRange['begEnd']." ";
                break;
            case 'Lower intermediate':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['lowStart']." AND ".$this->levelRange['lowEnd']." ";
                break;
            case 'Intermediate':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['intStart']." AND ".$this->levelRange['intEnd']." ";
                break;
            case 'Upper Intermediate':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['uppStart']." AND ".$this->levelRange['uppEnd']." ";
                break;
            case 'Advanced':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['advStart']." AND ".$this->levelRange['advEnd']." ";
                break;
            case 'Business':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['busStart']." AND ".$this->levelRange['busEnd']." ";
                break;
            case 'All levels':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['begStart']." AND ".$this->levelRange['busEnd']." ";
                break;
        }

        if($criterio<>'')
        {
            $sql .="AND i.email like '%$criterio%' ";
        }

        if($defaultPartner<>'')
        {
            $sql .="AND i.idPartnerCurrent in ($defaultPartner) ";
        }
        else
        {
            $partnerSelection = AbaPartnersList::model()->getListPartnerFromGroup($defaultPartnerGroup);
            $sql .="AND i.idPartnerCurrent in (".implode(',', $partnerSelection).") ";
        }

        $initPage= $pageObject->getCurrentPage() * $pageObject->getPageSize();
        $endPage= $pageObject->getPageSize();

        $sql .= " LIMIT ".$initPage.", $endPage;";

        $dataProvider = new CSqlDataProvider($sql,
            array(
                'totalItemCount'=>$pageObject->getItemCount(),
                'pagination'=>array('pageSize'=>$pageObject->getPageSize(),
                )));
        $dataProvider->keyField = 'idFollowup';
        $dataProvider->db = Yii::app()->dbSlave;
        return $dataProvider;

    }

    public function countAll($level, $criterio, $defaultPartner, $defaultPartnerGroup, $startDate, $endDate, $entryDate)
    {
        $count = 0;
        $sql="SELECT count(*) FROM " . $this->tableName() . " f INNER JOIN aba_b2c.user i ON f.userid = i.id WHERE ";
        if($entryDate)
            $sql.= "i.entryDate BETWEEN '$startDate' AND '$endDate 23:59:59' AND";
        else
            $sql.= "i.expirationDate BETWEEN '$startDate' AND '$endDate 23:59:59' AND";

        switch ($level)
        {
            case 'Beginners':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['begStart']." AND ".$this->levelRange['begEnd']." ";
                break;
            case 'Lower intermediate':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['lowStart']." AND ".$this->levelRange['lowEnd']." ";
                break;
            case 'Intermediate':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['intStart']." AND ".$this->levelRange['intEnd']." ";
                break;
            case 'Upper Intermediate':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['uppStart']." AND ".$this->levelRange['uppEnd']." ";
                break;
            case 'Advanced':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['advStart']." AND ".$this->levelRange['advEnd']." ";
                break;
            case 'Business':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['busStart']." AND ".$this->levelRange['busEnd']." ";
                break;
            case 'All levels':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['begStart']." AND ".$this->levelRange['busEnd']." ";
                break;
        }

        if($criterio<>'')
        {
            $sql .="AND i.email like '%$criterio%' ";
        }
        if($defaultPartner<>'')
        {
            $sql .="AND i.idPartnerCurrent in ($defaultPartner) ";
        }
        else
        {
            $partnerSelection = AbaPartnersList::model()->getListPartnerFromGroup($defaultPartnerGroup);
            $sql .="AND i.idPartnerCurrent in (".implode(',', $partnerSelection).") ";
        }

        $count += Yii::app()->db->createCommand($sql)->queryScalar();
        return $count;
    }

    /**
     * @param $id
     * @return B2cFollowup
     */
    public function findByIdFollowup($id)
    {
        $query="SELECT i.name, i.surnames, i.email, i.telephone, i.currentLevel, i.entryDate, i.expirationDate, f.id as idFollowup, f.*, IF(i.userType=2, 'Premium',
IF( i.cancelReason IS NULL AND i.userType=1, 'Free' , 'Ex-Premium')) as 'userType'
              FROM " . $this->tableName() . " f INNER JOIN  aba_b2c.user i ON f.userid = i.id
              WHERE i.expirationDate >= now() AND f.id = '$id'";

        $obj= new b2cFollowup();
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($query);
        $result = $command->query();
        if(($row = $result->read())!==false)
        {
            $obj->progressVersion=$row['progressVersion'];
            $obj->idFollowup=$row['idFollowup'];
            $obj->name=$row['name'];
            $obj->surnames=$row['surnames'];
            $obj->email=$row['email'];
            $obj->telephone=$row['telephone'];
            $obj->currentLevel=$row['currentLevel'];
            $obj->entryDate=$row['entryDate'];
            $obj->expirationDate=$row['expirationDate'];
            $obj->idFollowup=$row['idFollowup'];
            $obj->themeid=$row['themeid'];
            $obj->userid=$row['userid'];
            $obj->userType=$row['userType'];
            $obj->all_por=$row['all_por'];
            $obj->all_err=$row['all_err'];
            $obj->all_ans=$row['all_ans'];
            $obj->all_time=$row['all_time'];
            $obj->sit_por=$row['sit_por'];
            $obj->stu_por=$row['stu_por'];
            $obj->dic_por=$row['dic_por'];
            $obj->dic_ans=$row['dic_ans'];
            $obj->dic_err=$row['dic_err'];
            $obj->rol_por=$row['rol_por'];
            $obj->gra_por=$row['gra_por'];
            $obj->gra_ans=$row['gra_ans'];
            $obj->gra_err=$row['gra_err'];
            $obj->wri_por=$row['wri_por'];
            $obj->wri_ans=$row['wri_ans'];
            $obj->wri_err=$row['wri_err'];
            $obj->new_por=$row['new_por'];
            $obj->spe_por=$row['spe_por'];
            $obj->spe_ans=$row['spe_ans'];
            $obj->time_aux=$row['time_aux'];
            $obj->gra_vid=$row['gra_vid'];
            $obj->eva_por=$row['eva_por'];
        }
        return $obj;
    }

    public function searchExport($level, $criterio, $defaultPartnerGroup, $defaultPartner, $startDate, $endDate, $entryDate)
    {
        $sql="SELECT i.name as Name, i.surnames as 'Surnames', i.email as 'Email', i.telephone as 'Phone', i.entryDate as 'Entry date', i.expirationDate as 'Expiration date',
              IF(i.userType=2, 'Premium', IF(i.userType=0, 'Deleted', IF(i.cancelReason IS NULL AND i.userType=1, 'Free' , 'Ex-Premium'))) as 'userType',
              f.themeid as 'Unit', f.sit_por as 'ABA Film', f.stu_por as 'Speak', f.dic_por as 'Write', f.rol_por as 'Interpret', f.gra_vid as 'Video Class', f.wri_por as 'Exercises',
              f.new_por as 'Vocabulary', f.eva_por as 'Assessment'
              FROM " . $this->tableName() . " f INNER JOIN  aba_b2c.user i ON f.userid = i.id WHERE ";
        if($entryDate)
            $sql.= "i.entryDate BETWEEN '$startDate' AND '$endDate 23:59:59' AND";
        else
            $sql.= "i.expirationDate BETWEEN '$startDate' AND '$endDate 23:59:59' AND";

        switch ($level)
        {
            case 'Beginners':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['begStart']." AND ".$this->levelRange['begEnd']." ";
                break;
            case 'Lower intermediate':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['lowStart']." AND ".$this->levelRange['lowEnd']." ";
                break;
            case 'Intermediate':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['intStart']." AND ".$this->levelRange['intEnd']." ";
                break;
            case 'Upper Intermediate':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['uppStart']." AND ".$this->levelRange['uppEnd']." ";
                break;
            case 'Advanced':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['advStart']." AND ".$this->levelRange['advEnd']." ";
                break;
            case 'Business':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['busStart']." AND ".$this->levelRange['busEnd']." ";
                break;
            case 'All levels':
                $sql .=" f.themeid BETWEEN ".$this->levelRange['begStart']." AND ".$this->levelRange['busEnd']." ";
                break;
        }

        if($criterio<>'')
        {
            $sql .="AND i.email like '%$criterio%' ";
        }
        if($defaultPartner<>'')
        {
            $sql .="AND i.idPartnerCurrent in ($defaultPartner) ";
        }
        else
        {
            $partnerSelection = AbaPartnersList::model()->getListPartnerFromGroup($defaultPartnerGroup);
            $sql .="AND i.idPartnerCurrent in (".implode(',', $partnerSelection)."); ";
        }

        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();

        $aMockExport = array();

        while(($row = $dataReader->read())!==false)
        {
            //ABA Film percent
            if($row['ABA Film'] > 50)
                $row['ABA Film']=100;

            //Speak percent
            $row['Speak'] =  $row['Speak'] * 2;
            if($row['Speak']>100)
                $row['Speak']=100;

            //Write percent
            //$row['Write'] =  $row['Write']

            //Interpret percent
            //$row['Interpret'] = $row['Interpret'];
            if( $row['Interpret']>100)
                $row['Interpret']=100;

            //Video Class percent
            if($row['Video Class'] > 0)
                $row['Video Class']=100;

            //Exercises percent
            //$row['Exercises'] = $row['Exercises'];
            if( $row['Exercises']>100)
                $row['Exercises']=100;

            //Vocabulary percent
            $row['Vocabulary'] =  $row['Vocabulary'] * 2;
            if( $row['Vocabulary']>100)
                $row['Vocabulary']=100;

            //Assessment percent
            //$row['Assessment'] = $row['Assessment']

            //Total percent
            $row['Total'] = floor(($row['ABA Film'] +  $row['Speak'] + $row['Write'] + $row['Interpret'] + $row['Video Class'] + $row['Exercises'] + $row['Vocabulary']) / 7);

            $aMockExport[] = $row;

        }
        return $aMockExport;
    }

    public function searchExportCron() {

        $sql="SELECT f.themeid as 'Unit', ".$this->getSQLComputedSections(true).",".
            $this->getSQLComputeFullProgress(false)." as 'Total', i.id as 'iduser_web', IF( DATE(f.lastchange) = DATE('0000-00-00'), NULL, f.lastchange) as lastchange
            FROM aba_b2c.user i INNER JOIN " . $this->tableName() . " f ON i.id = f.userid
            WHERE i.userType not in(".DELETED.",".PREMIUM.") and i.entryDate between DATE_SUB(NOW(),INTERVAL 4 MONTH) and NOW();";
//            WHERE i.userType not in(".DELETED.",".PREMIUM.") and i.entryDate between DATE_SUB(NOW(),INTERVAL 2 MONTH) and NOW();";

        $connection = Yii::app()->dbSlave;
//        ini_set('memory_limit','2048m');
        ini_set('memory_limit','4096m');
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();

        return $dataReader;
    }

    public function searchExportCronPremium()

    {
        $sql="SELECT f.themeid as 'Unit', ".$this->getSQLComputedSections(true).",".
            $this->getSQLComputeFullProgress(false)." as 'Total', i.id as 'iduser_web', IF( DATE(f.lastchange) = DATE('0000-00-00'), NULL, f.lastchange) as lastchange
            FROM aba_b2c.user i INNER JOIN " . $this->tableName() . " f ON i.id = f.userid
            WHERE i.userType in(".PREMIUM.");";

        $connection = Yii::app()->dbSlave;
//        ini_set('memory_limit','2048m');
        ini_set('memory_limit','4096m');
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();

        return $dataReader;
    }

    /**
     * @param bool $withExam
     * @param bool $assessmentPerc
     *
     * @return string
     */
    public function getSQLComputedSections( $withExam=true, $assessmentPerc = false)
    {
        $sqlColAssessment = '';
        if ($withExam) {
            if(!$assessmentPerc){
                $sqlColAssessment = "f.eva_por as 'Assessment'";
            } else{
                $sqlColAssessment = "IF((f.eva_por*10)>=80,100,0) as 'Assessment'";
            }
        }

//        $sqlColumns = " IF(f.sit_por > 50, 100, f.sit_por) as 'ABAFilm',
//            IF(f.stu_por * 2 >100, 100, f.stu_por * 2) as 'Speak',
//            f.dic_por as 'Write_ABA',
//            IF(f.rol_por>100, 100, f.rol_por) as 'Interpret',
//            IF(f.gra_vid>0, 100, f.gra_vid) as 'VideoClass',
//            IF(f.wri_por>100, 100, f.wri_por) as 'Exercises',
//            IF(f.new_por * 2>100, 100, f.new_por * 2) as 'Vocabulary',".
//            $sqlColAssessment;
        $sqlColumns = " f.sit_por as 'ABAFilm',
            f.stu_por as 'Speak',
            f.dic_por as 'Write_ABA',
            f.rol_por as 'Interpret',
            f.gra_vid as 'VideoClass',
            f.wri_por as 'Exercises',
            f.new_por as 'Vocabulary',".
            $sqlColAssessment;

        return $sqlColumns;
    }

    /** Returns the summary of columns and formula of computing the progress in SQL.
     * @param bool $withExam
     *
     * @return string
     */
    public function getSQLComputeFullProgress($withExam=true)
    {
        $numTabs = 8;
        $strExamColumn = "+ IF((f.eva_por*10)>=80,100,0) ";
        if(!$withExam){
            $numTabs = 7;
            $strExamColumn = " ";
        }
//        $sqlFields = "ROUND(FLOOR(
//                        IF(f.sit_por > 50, 100, f.sit_por)+
//                        IF(f.stu_por * 2 >100, 100, f.stu_por * 2)+
//                        f.dic_por+
//                        IF(f.rol_on>0, 100, f.rol_por) +
//                        IF(f.gra_vid>0, 100, f.gra_vid) +
//                        IF(f.wri_por>100, 100, f.wri_por)+
//                        IF(f.new_por * 2>100, 100, f.new_por * 2)
//                        $strExamColumn
//                     )/".$numTabs.',2)';
        $sqlFields = "ROUND(FLOOR(
                        f.sit_por +
                        f.stu_por +
                        f.dic_por+
                        f.rol_por +
                        f.gra_vid +
                        f.wri_por +
                        f.new_por
                        $strExamColumn
                     )/".$numTabs.',2)';

        return $sqlFields;
    }

}