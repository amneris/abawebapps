<?php

class AbaPartnerchannels extends CmAbaPartnerchannels
{
    public function __construct($scenario = 'insert', $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AbaPartnerchannels the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'idChannel' => 'ID',
          'nameChannel' => 'Channel name',
          'dateAdd' => 'Last update',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('idChannel', $this->idChannel, true);
        $criteria->compare('nameChannel', $this->nameChannel, true);
        $criteria->compare('dateAdd', $this->dateAdd, true);

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
          'pagination' => array(
            'pageSize' => 100,
          ),
        ));
    }

    /**
     * @param bool $runValidation
     * @param null $attributes
     *
     * @return bool
     */
    public function save($runValidation = true, $attributes = null)
    {
        if ($this->validatenameChannel()) {
            return parent::save($runValidation, $attributes);
        }
        return false;
    }

    /**
     * @return bool
     */
    public function validatenameChannel()
    {
        //
        if (trim($this->nameChannel) == '') {
            $this->addError('nameChannel', 'The field nameChannel is mandatory');
            return false;
        }

        //
        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " pgch ";
        $sSql .= " WHERE pgch.nameChannel = '" . addslashes($this->nameChannel) . "' ";

        if(is_numeric($this->idChannel)) {
            $sSql .= " AND pgch.idChannel <> '" . $this->idChannel . "' ";
        }

        $sSql .= " LIMIT 0, 1 ";

        $dataReader = $this->querySQL($sSql);
        if (($row = $dataReader->read()) !== false) {
            $this->addError('nameChannel', 'This Identifier already exists in the database');
            return false;
        }
        return true;
    }

    /**
     * @return array
     */
    public function getPartnerChannels()
    {
        $stAllDescriptions = $this->getAllPartnerChannels();

        $stFormattedDescription = array();

        foreach ($stAllDescriptions as $stDescription) {
            $stFormattedDescription[$stDescription['idChannel']] = $stDescription['nameChannel'];
        }

        return $stFormattedDescription;
    }

    /**
     * @param bool $bEmpty
     *
     * @return array
     */
    public function getFullList($bEmpty = false)
    {
        $stAllDescriptions = $this->getAllPartnerChannels();

        $stFormattedDescription = array();

        if ($bEmpty) {
            $stFormattedDescription[""] = "";
        }

        foreach ($stAllDescriptions as $stDescription) {
            $stFormattedDescription[$stDescription['idChannel']] = $stDescription['nameChannel'];
        }

        return $stFormattedDescription;
    }

}
