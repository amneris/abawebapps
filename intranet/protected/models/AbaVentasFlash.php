<?php

/**
 * This is the model class for table "abaenglishFriends_m".
 *
 * The followings are the available columns in table 'abaenglishFriends_m':
 * @property string $code1
 * @property string $code2
 * @property string $email
 * @property integer $used
 * @property string $date
 * @property string $campaign
 * @property integer $count
 */
class AbaVentasFlash extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return AbaVentasFlash the static model class
     */
    private static $_tableName;
    public $product;
    public $tableName='mitabla';
    public $agrupador=0;
    public $producto=0;
    public $agrupadorName='default';


    public function __construct($tableName)
    {
        if(strlen($tableName) == 0)
        {
            return false;
        }

        if(strlen($tableName)>0)
        {
            self::$_tableName = $tableName;
        }

        self::setIsNewRecord(true);
    }
   
    
    public static function model($tableName = '')
    {
        if(strlen($tableName) == 0)
        {
            return false;
        }

        $className=__CLASS__;

        if(strlen($tableName)>0){
            self::$_tableName = $tableName;
        }

        return parent::model($className);
    }

    
    /**
     * @return CDbConnection database connection
     */
    public function getDbConnection()
    {
        return Yii::app()->db2;
    }
    
    public function setTableName($tableName)
    {
        self::$_tableName = $tableName;
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return self::$_tableName;
    }
    
    public function setAgrupadorName($agrupadorName)
    {
         $this->agrupadorName = $agrupadorName; 
    }
    
    public function getAgrupadorName()
    {
         return $this->agrupadorName;
    }
    
    public function setFecha($fecha)
    {
         $this->fecha = $fecha; 
    }
    
    public function getFecha()
    {
         return $this->fecha;
    }
    
    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                    array('used, count, producto, agrupador', 'numerical', 'integerOnly'=>true),
                    array('code1, code2, campaign', 'length', 'max'=>45),
                    array('email', 'length', 'max'=>100),
                    array('date', 'safe'),
                    array('code1, campaign, count, producto, agrupador', 'required'),
                    // The following rule is used by search().
                    // Please remove those attributes that should not be searched.
                    array('code1, code2, email, used, date, campaign, count', 'safe', 'on'=>'search'),
            );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                    'code1' => 'Coupon code',
                    'code2' => 'Security code',
                    'email' => 'E-mail',
                    'used' => 'Used',
                    'date' => 'Validation date',
                    'campaign' => 'Campaign',
                    'count' => 'Validation alowed',
                    'm1' => '1 Month',
                    'm2' => '2 Months',
                    'm3' => '3 Months',
                    'm6' => '6 Months',
                    'm12' => '12 Months',
                    'm18' => '18 Months',
                    'm24' => '24 Months',
                    'fecha' => 'Date',
                    'fileName' => 'File',
                    'agrupador'=> 'Grouper',
                    'producto'=> 'Product',
                
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
            $criteria=new CDbCriteria;
            $criteria->compare('code1',$this->code1,true);
            $criteria->compare('code2',$this->code2,true);
            $criteria->compare('email',$this->email,true);
            $criteria->compare('used',$this->used);
            $criteria->compare('date',$this->date,true);
            $criteria->compare('campaign',$this->campaign,true);
            $criteria->compare('count',$this->count);
            

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'model'=>$this,
                    
            ));
    }
    
    public function findByCode($id, $table)
    {
        $query = "SELECT * FROM `$table` WHERE code1='$id';" ;
        $obj= new AbaVentasFlash($table);
        $connection = Yii::app()->db2;
        $command = $connection->createCommand($query);		
        $result = $command->query();
        if(($row = $result->read())!==false)
        {
            $obj->code1=$row['code1'];
            $obj->code2=$row['code2'];
            $obj->email=$row['email'];
            $obj->used=$row['used'];
            $obj->date=$row['date'];
            $obj->campaign=$row['campaign'];
            $obj->count=$row['count'];
        }
         return $obj;
    }
    
    public function findAllCodes($allTables, $pageObject, $criterio)
    {
        
        $band = 0;
        $sql = "";
        $addCriterio = "WHERE email like '%$criterio%' OR code1 like '%$criterio%' or code2 like '%$criterio%' ";

        $page = (isset($_GET['page']) ? $_GET['page'] : 1);
       
        if($allTables[0]<>'no')
        {
            $sql .= "SELECT code1, code2, email, used, date, '1 mes' as product, '$allTables[0]' as tableName FROM ";
            $sql .= "`$allTables[0]`";
            if($criterio<>'')
                $sql .= $addCriterio;
            $band = 1;
        }

        if($allTables[1]<>'no')
        {
            if($band)
                $sql .= " UNION ";
            else
                $band =1;

            $sql .= "SELECT code1, code2, email, used, date, '2 meses' as product, '$allTables[1]' as tableName FROM ";
            $sql .= "`$allTables[1]`";
            if($criterio<>'')
                $sql .= $addCriterio;
        }

        if($allTables[2]<>'no')
        {
            if($band)
               $sql .= " UNION ";
            else 
                $band =1;
           
            $sql .= "SELECT code1, code2, email, used, date, '3 meses' as product, '$allTables[2]' as tableName FROM ";
            $sql .= "`$allTables[2]`";
            if($criterio<>'')
                $sql .= $addCriterio;
        }
        
        if($allTables[3]<>'no')
        {
            if($band)
               $sql .= " UNION ";
            else 
                $band =1;
             
            $sql .= "SELECT code1, code2, email, used, date, '6 meses' as product, '$allTables[3]' as tableName FROM ";
            $sql .= "`$allTables[3]`";
            if($criterio<>'')
               $sql .= $addCriterio;
        }
        
        if($allTables[4]<>'no')
        {
            if($band)
                $sql .= " UNION ";
            else 
                $band =1;
             
            $sql .= "SELECT code1, code2, email, used, date, '12 meses' as product, '$allTables[4]' as tableName FROM ";
            $sql .= "`$allTables[4]`";
            if($criterio<>'')
                $sql .= $addCriterio;
        }
        
        if($allTables[5]<>'no')
        {
            if($band)
                $sql .= " UNION ";
            else 
                $band =1;
             
            $sql .= "SELECT code1, code2, email, used, date, '18 meses' as product, '$allTables[5]' as tableName FROM ";
            $sql .= "`$allTables[5]`";
            if($criterio<>'')
                $sql .= $addCriterio;
        }
        
        if($allTables[6]<>'no')
        {
            if($band)
                $sql .= " UNION ";
            else 
                $band =1;
             
            $sql .= "SELECT code1, code2, email, used, date, '24 meses' as product, '$allTables[6]' as tableName FROM ";
            $sql .= "`$allTables[6]`";
            if($criterio<>'')
                $sql .= $addCriterio;
        } 
   
        $initPage= $pageObject->getCurrentPage() * $pageObject->getPageSize();
        $endPage= $pageObject->getPageSize();
        
        $sql .= " LIMIT ".$initPage.", $endPage;"; 
        
        $dataProvider = new CSqlDataProvider($sql, 
                array(
                'totalItemCount'=>$pageObject->getItemCount(),
                'pagination'=>array('pageSize'=>$pageObject->getPageSize(),)
                 ,));
           
        $dataProvider->db = Yii::app()->db2;
        $dataProvider->keyField = 'code1';
        
        return $dataProvider;
        
    }
    
    public function countAllCodes($allTables, $criterio)
    {
        $count = 0;
        $addCriterio = "WHERE email like '%$criterio%' OR code1 like '%$criterio%' or code2 like '%$criterio%' ";
        
        if($allTables[0]<>'no')
        {
            $sql ="SELECT COUNT(*) FROM `$allTables[0]` ";
            if($criterio<>'')
                $sql .= $addCriterio;
                
            $count += Yii::app()->db2->createCommand($sql)->queryScalar();
        }

        if($allTables[1]<>'no')
        {
            $sql ="SELECT COUNT(*) FROM `$allTables[1]` ";
            if($criterio<>'')
                $sql .= $addCriterio;

            $count += Yii::app()->db2->createCommand($sql)->queryScalar();
        }

        if($allTables[2]<>'no')
        {
           $sql ="SELECT COUNT(*) FROM `$allTables[2]` ";
            if($criterio<>'')
                $sql .= $addCriterio;
            
            $count += Yii::app()->db2->createCommand($sql)->queryScalar();
        }
        
        if($allTables[3]<>'no')
        {
           $sql ="SELECT COUNT(*) FROM `$allTables[3]` ";
            if($criterio<>'')
                $sql .= $addCriterio;
            
            $count += Yii::app()->db2->createCommand($sql)->queryScalar();
        }
        
        if($allTables[4]<>'no')
        {
            $sql ="SELECT COUNT(*) FROM `$allTables[4]` ";
            if($criterio<>'')
                $sql .= $addCriterio;
            
            $count += Yii::app()->db2->createCommand($sql)->queryScalar();
        }
        
        if($allTables[5]<>'no')
        {
            $sql ="SELECT COUNT(*) FROM `$allTables[5]` ";
            if($criterio<>'')
                $sql .= $addCriterio;
            
            $count += Yii::app()->db2->createCommand($sql)->queryScalar();
        }
        
        if($allTables[6]<>'no')
        {
           $sql ="SELECT COUNT(*) FROM `$allTables[6]` ";
            if($criterio<>'')
                $sql .= $addCriterio;
          
            $count += Yii::app()->db2->createCommand($sql)->queryScalar();
        } 
        
        return $count; 
    }
    
    
    public function findAllCodesByDate($currentProduct, $salesSearch, $deal, $agrupador)
    {
       
        $sql = "";
        
        
        if($salesSearch->showDate)
        {
            $sql .= "SELECT g.code1, g.code2, g.email, p.name, p.surnames, p.telephone, g.date FROM
                db_ventasflash.`$currentProduct` g LEFT JOIN aba_b2c.user p ON p.email = SUBSTRING_INDEX(g.email, ',', 1)
                where g.date >= '$salesSearch->startDate' and g.date <= '$salesSearch->endDate 23:59:59' and g.email <> ''
                and campaign = $deal order by g.date";
        }
        else
        {
            $sql .= "SELECT * from (SELECT g.code1, g.code2, g.email, p.name, p.surnames, p.telephone FROM
                db_ventasflash.`$currentProduct` g LEFT JOIN aba_b2c.user p ON p.email = SUBSTRING_INDEX(g.email, ',', 1)
                where g.date >= '$salesSearch->startDate' and g.date < '$salesSearch->endDate' and g.email <> ''
                and campaign = $deal order by g.date) a ";
        }
        $dataProvider = new CSqlDataProvider($sql);
        $dataProvider->db = Yii::app()->db2;
        $dataProvider->keyField = 'code1';
        $dataProvider->pagination = false;
        
        return $dataProvider;
        
    }
    
    public function updateDealsCampaigns($xAgrupadores, $xModel)
    {
        foreach($xAgrupadores as $agrupador)
        {
            $maximo=0;
            echo "<br><hr/><br>Updating campaigns: ";
            echo "<b>".$agrupador->agrupadorName."</b><br><br>";
            
            if($agrupador->prefijoMensual<>'no')
            {
                $tableName = $agrupador->tableName . $agrupador->prefijoMensual;
                $query = "SELECT max(CAST(`campaign` AS SIGNED)) as maxM FROM db_ventasflash.`$tableName`";
                $connection = Yii::app()->db2;
                $command = $connection->createCommand($query);
                $dataReader = $command->query();
                $rows = $dataReader->read();
                echo "Last 1 month campaign: ";
                echo $rows['maxM']."<br>";
                if($rows['maxM'] > $maximo)
                    $maximo = $rows['maxM'];
            }//fin prefijo mensual

            if($agrupador->prefijoBimensual<>'no')
            {
                $tableName = $agrupador->tableName . $agrupador->prefijoBimensual;
                $query = "SELECT max(CAST(`campaign` AS SIGNED)) as maxM FROM db_ventasflash.`$tableName`";
                $connection = Yii::app()->db2;
                $command = $connection->createCommand($query);
                $dataReader = $command->query();
                $rows = $dataReader->read();
                echo "Last 2 months campaign: ";
                echo $rows['maxM']."<br>";
                if($rows['maxM'] > $maximo)
                    $maximo = $rows['maxM'];
            }//fin prefijo bimensual

            if($agrupador->prefijoTrimestral<>'no')
            {
                $tableName = $agrupador->tableName . $agrupador->prefijoTrimestral;
                $query = "SELECT max(CAST(`campaign` AS SIGNED)) as maxM FROM db_ventasflash.`$tableName`";
                $connection = Yii::app()->db2;
                $command = $connection->createCommand($query);
                $dataReader = $command->query();
                $rows = $dataReader->read();
                echo "Last 3 months campaign: ";
                echo $rows['maxM']."<br>";
                if($rows['maxM'] > $maximo)
                    $maximo = $rows['maxM'];
            }//fin prefijo trimestral
            
            if($agrupador->prefijoSemestral<>'no')
            {
                $tableName = $agrupador->tableName . $agrupador->prefijoSemestral;
                $query = "SELECT max(CAST(`campaign` AS SIGNED)) as maxM FROM db_ventasflash.`$tableName`";
                $connection = Yii::app()->db2;
                $command = $connection->createCommand($query);
                $dataReader = $command->query();
                $rows = $dataReader->read();
                echo "Last 6 months campaign: ";
                echo $rows['maxM']."<br>";
                if($rows['maxM'] > $maximo)
                    $maximo = $rows['maxM'];
            }//fin prefijo semestral
            
            if($agrupador->prefijoAnual<>'no')
            {
                $tableName = $agrupador->tableName . $agrupador->prefijoAnual;
                $query = "SELECT max(CAST(`campaign` AS SIGNED)) as maxM FROM db_ventasflash.`$tableName`";
                $connection = Yii::app()->db2;
                $command = $connection->createCommand($query);
                $dataReader = $command->query();
                $rows = $dataReader->read();
                echo "Last 12 months campaign: ";
                echo $rows['maxM']."<br>";
                if($rows['maxM'] > $maximo)
                    $maximo = $rows['maxM'];
            }//fin prefijo anual
            
            if($agrupador->prefijo18Meses<>'no')
            {
                $tableName = $agrupador->tableName . $agrupador->prefijo18Meses;
                $query = "SELECT max(CAST(`campaign` AS SIGNED)) as maxM FROM db_ventasflash.`$tableName`";
                $connection = Yii::app()->db2;
                $command = $connection->createCommand($query);
                $dataReader = $command->query();
                $rows = $dataReader->read();
                echo "Last 18 months campaign: ";
                echo $rows['maxM']."<br>";
                if($rows['maxM'] > $maximo)
                    $maximo = $rows['maxM'];
            }//fin prefijo 18 meses
            
            if($agrupador->prefijo24Meses<>'no')
            {
                $tableName = $agrupador->tableName . $agrupador->prefijo24Meses;
                $query = "SELECT max(CAST(`campaign` AS SIGNED)) as maxM FROM db_ventasflash.`$tableName`";
                $connection = Yii::app()->db2;
                $command = $connection->createCommand($query);
                $dataReader = $command->query();
                $rows = $dataReader->read();
                echo "Last 24 months campaign: ";
                echo $rows['maxM']."<br>";
                if($rows['maxM'] > $maximo)
                    $maximo = $rows['maxM'];
            }//fin prefijo 24 meses
            
            echo "<br><b>Max campaign: </b>".$maximo."<br>";
            
            $query = "UPDATE db_ventasflash.ventasFlashScript SET totalDeals = $maximo WHERE idVentasFlashScript = $agrupador->idventasFlashScript ;";
            $connection = Yii::app()->db2;
            $command = $connection->createCommand($query);
            $result = $command->query();
            
            if($result)
					echo "<br><span style=\"color:green; font-weight: bold \">Campaign was updated successfully.</span><br>";
				else
					echo "<br><span style=\"color:red; font-weight: bold\">Could not update the campaigns. Notify IT.</span><br>";
            
        }
    }


    /**#5113
     *
     * @param $tableName
     *
     * @param $codes
     * @return array
     */
    public function getExcelData($tableName, $codes)
    {
        $sCodes = $codes;
        if(is_array($codes)) {
            $sCodes = "'" . implode("','", $codes) . "'";
        }

        $sSql = "SELECT code1, campaign FROM db_ventasflash.`" . $tableName . "` WHERE code1 IN (" . $sCodes . ") ";

        $connection =   Yii::app()->dbSlave;
        /* @var $connection CDbConnection */
        $command =      $connection->createCommand($sSql);
        $codesData =    array();
        try {
            $dataReader = $command->query();
            while(($row = $dataReader->read()) !== false) {
                $codesData[$row['code1']] = $row;
            }
        }
        catch(Exception $e) { }

        return $codesData;
    }


}