<?php

class ProductsPricesAdyenHpp extends CmAbaProductsPricesAdyenHpp
{

    public function __construct($scenario='insert', $dbConnName=NULL) {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProductsPricesAdyenHpp the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'idProductPrice' => 'ID',
          'idProduct' => 'Product ID',
          'idPaymentMethod' => 'Payment method',
          'currencyAdyenHpp' => 'Adyen currency',
          'priceAdyenHpp' => 'Adyen price',
          'enabled' => 'Enabled',
          'modified' => 'Modified date',
          'priceAdyenHppExtranet' => 'Adyen price extranet',
          'enabledExtranet' => 'Enabled extranet',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
//        $criteria->select = array(
//          't.idProductTier',
//            new CDbExpression('COUNT(*) AS nSuccess'),
//            new CDbExpression("MIN(dateRequest) AS startDate"),
//        );
        $criteria->compare('idProductPrice', $this->idProductPrice, true);
        $criteria->compare('idProduct', $this->idProduct, true);
        $criteria->compare('idPaymentMethod', $this->idPaymentMethod, true);
        $criteria->compare('currencyAdyenHpp', $this->currencyAdyenHpp, true);
        $criteria->compare('priceAdyenHpp', $this->priceAdyenHpp, true);
        $criteria->compare('enabled', $this->enabled, true);
        $criteria->compare('modified', $this->modified, true);
        $criteria->compare('priceAdyenHppExtranet', $this->priceAdyenHppExtranet, true);
        $criteria->compare('enabledExtranet', $this->enabledExtranet, true);

        $criteria->order = 'idProductPrice';

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
          'pagination' => array(
                'pageSize' => 100,
          ),
        ));
    }

}
