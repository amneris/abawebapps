<?php
/** Magic properties extracted from table in database dynamically
 *
 * @property string id
 * @property float amountDiscount
 * @property float amountOriginal
 * @property float amountPrice
 * @property string autoId
 * @property string currencyTrans
 * @property string dateEndTransaction
 * @property string dateStartTransaction
 * @property string dateToPay
 * @property string foreignCurrencyTrans
 * @property string idCountry
 * @property string idPartner
 * @property string idPayControlCheck
 * @property string idPeriodPay
 * @property string idProduct
 * @property string idPromoCode
 * @property string idUserCreditForm
 * @property string idUserProdStamp
 * @property string lastAction
 * @property string paySuppExtId
 * @property string paySuppExtProfId
 * @property string paySuppOrderId
 * @property integer status
 * @property integer userId
 * @property float xRateToEUR
 * @property float xRateToUSD
 * @property integer $idPartnerPrePay
 * @property integer $isRecurring
 * @property string  $paySuppExtUniId
 * @property integer $paySuppLinkStatus
 * @property integer $isPeriodPayChange
 * @property integer $cancelOrigin  NULL - No cancel,
 *                                  1 - All payments until 01/10/2014 or user cancelled,
 *                                  2 - Intranet employee cancellation
 *                                  3 - PayPal cancellation
 *                                  4 - Expiration
 *                                  5 - Upgrade / Downgrade
 *
 * @property float $taxRateValue
 * @property float $amountTax
 * @property float $amountPriceWithoutTax
 * @property int $isExtend
 * @property int $experimentVariationAttributeId
 * @property string $idSession
 *
 */
class Payment extends AbaActiveRecord
{
    public $id;

    public function __construct( )
    {
        parent::__construct();
    }

    public function tableName()
    {
        return 'payments';
    }

  /**
   * @static
   *
   * @param string $className
   *
   * @return array|CActiveRecord
   */
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }

    public function mainFields()
    {
        return "
         p.`id`, p.`idUserProdStamp`, p.`userId`, p.`idProduct`, p.`idCountry`, p.`idPeriodPay`,
            p.`idUserCreditForm`, p.`paySuppExtId`, p.`paySuppOrderId`, p.`status`, p.`dateStartTransaction`,
            p.`dateEndTransaction`,p.`dateToPay`, p.`xRateToEUR`, p.`xRateToUSD`, p.`currencyTrans`,
            p.`foreignCurrencyTrans`,p.`idPromoCode`, p.`amountOriginal`, p.`amountDiscount`, p.`amountPrice`,
            p.`idPayControlCheck`,p.`idPartner`, p.`idPartnerPrePay`, p.`paySuppExtProfId`, p.autoId, p.`lastAction`,
            p.`isRecurring`, p.`paySuppExtUniId`, p.`paySuppLinkStatus`, p.`isPeriodPayChange`, p.`cancelOrigin`,
            p.`taxRateValue`, p.`amountTax`, p.`amountPriceWithoutTax`, p.`isExtend`, p.`experimentVariationAttributeId`,
            p.`idSession`";
    }

    public function rules()
    {
        return array(
            array('idProduct',         'required'),
            array('idCountry',         'required'),
            array('idPeriodPay',       'required'),
        );
    }

    public function getPaymentById( $id=false )
    {
        $sql = "SELECT " . $this->mainFields() .
            " FROM " . $this->tableName() . " p WHERE p.`id`= :ID";

        $paramsToSQL = array(":ID" => $id);
        $dataReader = $this->querySQL($sql,$paramsToSQL);
        if(($row = $dataReader->read()) !== false)
        {
            $this->fillDataColsToProperties($row);
            return $this;
        }

        return false;
    }

    /**
     * @param $userId
     * @param $idProduct
     * @param $paySuppOrderId
     * @param $status
     *
     * @return $this|bool
     */
    public function getRefundPayment( $userId, $idProduct, $paySuppOrderId, $status)
    {
        $sSql = "   SELECT " . $this->mainFields() . "
                    FROM " . $this->tableName() . " p
                    WHERE p.`userId`= :USERID
                        AND p.`idProduct`= :IDPRODUCT
                        AND p.`paySuppOrderId`= :PAYSUPORDERID
                        AND p.`status`= :STATUS
        ";
        $paramsToSQL =  array(
            ":USERID" =>        $userId,
            ":IDPRODUCT" =>     $idProduct,
            ":PAYSUPORDERID" => $paySuppOrderId,
            ":STATUS" =>        $status
        );
        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function validateInvoicePartner() {

        //#4480
        // if($this->paySuppExtId == PAY_SUPPLIER_GROUPON || $this->paySuppExtId == PAY_SUPPLIER_B2B || $this->paySuppExtId == PAY_SUPPLIER_APP_STORE) {
        if($this->paySuppExtId == PAY_SUPPLIER_GROUPON || $this->paySuppExtId == PAY_SUPPLIER_B2B) {
            return false;
        }
        return true;
    }

    /** Get last PENDING PAYMENT for this user.
     *
     * @param integer $userId
     * @return $this|bool
     */
    public function getLastPayPendingByUserId($userId)
    {
        $sql = " SELECT ".$this->mainFields()."
                    FROM ".$this->tableName()." p
                      LEFT JOIN `pay_suppliers` ps ON p.paySuppExtId=ps.id
                    WHERE p.`userId`=$userId AND p.`status` = ".PAY_PENDING."
                    ORDER BY p.dateToPay DESC, p.`dateEndTransaction` DESC,
                             p.`dateStartTransaction` DESC, p.`autoId` DESC
                    LIMIT 1; ";

        $dataReader=$this->querySQL($sql);
        if(($row = $dataReader->read())!==false)
        {
            $this->fillDataColsToProperties($row);
            return $this;
        }

        return false;
    }

}
