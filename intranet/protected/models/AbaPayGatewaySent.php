<?php

/**
 * This is the model class for table "pay_gateway_sent".
 *
 * The followings are the available columns in table 'pay_gateway_sent':
 * @property string $id
 * @property string $idPayment
 * @property string $fecha
 * @property string $Order_number
 * @property string $Currency
 * @property string $TransactionType
 * @property string $MerchantData
 * @property string $Amount
 * @property string $MerchantName
 * @property string $MerchantSignature
 * @property string $MerchantCode
 * @property string $CreditTarget
 * @property string $ExpiryDate
 * @property string $Cvv2
 * @property string $XML
 */
class AbaPayGatewaySent extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AbaPayGatewaySent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pay_gateway_sent';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idPayment, Order_number, Currency, TransactionType, MerchantData, Amount, MerchantName, MerchantSignature, MerchantCode, CreditTarget, ExpiryDate, Cvv2, XML', 'required'),
			array('idPayment, Order_number', 'length', 'max'=>15),
			array('Currency', 'length', 'max'=>10),
			array('TransactionType, MerchantData, Amount, MerchantName, MerchantSignature, MerchantCode, CreditTarget, ExpiryDate, Cvv2', 'length', 'max'=>45),
			array('fecha', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, idPayment, fecha, Order_number, Currency, TransactionType, MerchantData, Amount, MerchantName, MerchantSignature, MerchantCode, CreditTarget, ExpiryDate, Cvv2, XML', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idPayment' => 'Id Payment',
			'fecha' => 'Fecha',
			'Order_number' => 'Order Number',
			'Currency' => 'Currency',
			'TransactionType' => 'Transaction Type',
			'MerchantData' => 'Merchant Data',
			'Amount' => 'Amount',
			'MerchantName' => 'Merchant Name',
			'MerchantSignature' => 'Merchant Signature',
			'MerchantCode' => 'Merchant Code',
			'CreditTarget' => 'Credit Target',
			'ExpiryDate' => 'Expiry Date',
			'Cvv2' => 'Cvv2',
			'XML' => 'Xml',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idPayment',$this->idPayment,true);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('Order_number',$this->Order_number,true);
		$criteria->compare('Currency',$this->Currency,true);
		$criteria->compare('TransactionType',$this->TransactionType,true);
		$criteria->compare('MerchantData',$this->MerchantData,true);
		$criteria->compare('Amount',$this->Amount,true);
		$criteria->compare('MerchantName',$this->MerchantName,true);
		$criteria->compare('MerchantSignature',$this->MerchantSignature,true);
		$criteria->compare('MerchantCode',$this->MerchantCode,true);
		$criteria->compare('CreditTarget',$this->CreditTarget,true);
		$criteria->compare('ExpiryDate',$this->ExpiryDate,true);
		$criteria->compare('Cvv2',$this->Cvv2,true);
		$criteria->compare('XML',$this->XML,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}