<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $name
 * @property string $surnames
 * @property string $email
 * @property string $password
 * @property string $complete
 * @property string $city
 * @property integer $countryId
 * @property integer $countryIdCustom
 * @property string $birthDate
 * @property string $telephone
 * @property string $langEnv
 * @property string $langCourse
 * @property string $userType
 * @property string $currentLevel
 * @property string $entryDate
 * @property string $expirationDate
 * @property string $teacherId
 * @property string $idPartnerFirstPay
 * @property integer $idPartnerCurrent
 * @property integer $firstLoginDone
 * @property integer $savedUserObjective
 * @property string $releaseCampus
 * @property string $idPartnerSource
 * @property string $keyExternalLogin
 * @property string $gender
 * @property integer $cancelReason
 * @property string $registerUserSource
 * @property integer $hasWorkedCourse
 * @property datetime $dateLastSentSelligent
 * @property string $lastActionFixSql
 * @property integer $followUpAtFirstPay
 * @property integer $watchedVideosAtFirstPay
 * @property integer $idSourceList
 * @property string $deviceTypeSource
 *
 */
class AbaUser extends AbaActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return AbaUser the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
          array('userType', 'required'),
          array('countryId, firstLoginDone, savedUserObjective, idSourceList', 'numerical', 'integerOnly' => true),
          array('name, surnames, email, city, keyExternalLogin, lastActionFixSql', 'length', 'max' => 200),
          array('password', 'length', 'max' => 50),
          array('complete', 'length', 'max' => 3),
          array('deviceTypeSource', 'length', 'max' => 1),
    			array('idPartnerFirstPay, idPartnerSource, idPartnerCurrent', 'length', 'max'=>20),
          array('telephone', 'length', 'max' => 20),
          array('langEnv, langCourse, currentLevel, followUpAtFirstPay', 'length', 'max' => 5),
          array(
            'cancelReason, userType, registerUserSource, registerUserSource, hasWorkedCourse',
            'length',
            'max' => 2
          ),
          array('teacherId', 'length', 'max' => 8),
          array('releaseCampus', 'length', 'max' => 10),
          array('birthDate, entryDate, expirationDate, dateLastSentSelligent, gender, countryIdCustom', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
          array('id', 'length', 'max' => 12), //para aceptar operadores de busqueda <>
          array(
            'id, name, surnames, email, password, passwordFlashMedia, complete, city, countryId, birthDate, telephone, langEnv, langCourse, userType,
			currentLevel, entryDate, expirationDate, teacherId, idPartnerFirstPay, firstLoginDone, releaseCampus, idPartnerSource, idPartnerCurrent, gender,
			countryIdCustom, firstLoginDone, savedUserObjective, releaseCampus, keyExternalLogin, cancelReason, registerUserSource, hasWorkedCourse,
			dateLastSentSelligent, lastActionFixSql, followUpAtFirstPay, watchedVideosAtFirstPay, idSourceList, deviceTypeSource',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        /*return array(
        );*/


        return array('payments' => array(self::HAS_MANY, 'AbaPayments', 'userId'));

    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'id' => 'ID',
          'name' => 'Name',
          'surnames' => 'Surnames',
          'email' => 'Email',
          'password' => 'Password',
          'complete' => 'Complete',
          'city' => 'City',
          'countryId' => 'Country',
          'countryIdCustom' => 'Custom Country',
          'birthDate' => 'Birth Date',
          'telephone' => 'Telephone',
          'langEnv' => 'Lang Env',
          'langCourse' => 'Lang Course',
          'userType' => 'User Type',
          'currentLevel' => 'Current Level',
          'entryDate' => 'Entry Date',
          'expirationDate' => 'Expiration Date',
          'teacherId' => 'Teacher',
          'idPartnerFirstPay' => 'Id Partner First Pay',
          'idPartnerCurrent' => 'Id Current Partner',
          'firstLoginDone' => 'First Login Done',
          'savedUserObjective' => 'Saved User Objetive',
          'releaseCampus' => 'Release Campus',
          'idPartnerSource' => 'Id Partner Source',
          'keyExternalLogin' => 'External Login Key',
          'gender' => 'Gender',
          'cancelReason' => 'Cancellation Reason',
          'registerUserSource' => 'Register User Source',
          'hasWorkedCourse' => 'Worked Course',
          'dateLastSentSelligent' => 'Last Sent to Selligent',
          'lastActionFixSql' => 'Last Action Fix SQL',
          'followUpAtFirstPay' => 'Follow Up At First Pay',
          'watchedVideosAtFirstPay' => 'Videoclasses watched when first became Premium',
          'idSourceList' => 'Source List',
          'deviceTypeSource' => 'Device Type'
        );
    }

    public function emptySearch()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('id', '-666');
        $criteria->compare('email', 'zxzxzxzxzx');
        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
        ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', trim($this->id));
        $criteria->compare('name', trim($this->name), true);
        $criteria->compare('surnames', trim($this->surnames), true);
        $criteria->compare('email', trim($this->email), false);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('complete', $this->complete, true);
        $criteria->compare('city', $this->city, true);
        $criteria->compare('countryId', $this->countryId);
        $criteria->compare('countryIdCustom', $this->countryIdCustom);
        $criteria->compare('birthDate', $this->birthDate, true);
        $criteria->compare('telephone', $this->telephone, true);
        $criteria->compare('langEnv', trim($this->langEnv), true);
        $criteria->compare('langCourse', trim($this->langEnv), true);
        $criteria->compare('userType', $this->userType);
        $criteria->compare('currentLevel', $this->currentLevel);
        $criteria->compare('entryDate', $this->entryDate, true);
        $criteria->compare('expirationDate', trim($this->expirationDate), true);
        $criteria->compare('teacherId', $this->teacherId, false);
        $criteria->compare('idPartnerFirstPay', $this->idPartnerFirstPay);
        $criteria->compare('idPartnerCurrent', $this->idPartnerCurrent);
        $criteria->compare('firstLoginDone', $this->firstLoginDone);
        $criteria->compare('savedUserObjective', $this->savedUserObjective);
        $criteria->compare('releaseCampus', $this->releaseCampus);
        $criteria->compare('idPartnerSource', $this->idPartnerSource);
        $criteria->compare('keyExternalLogin', $this->keyExternalLogin);
        $criteria->compare('gender', $this->gender);
        $criteria->compare('cancelReason', $this->cancelReason);
        $criteria->compare('registerUserSource', $this->registerUserSource);
        $criteria->compare('hasWorkedCourse', $this->hasWorkedCourse);
        $criteria->compare('dateLastSentSelligent', $this->dateLastSentSelligent);
        $criteria->compare('lastActionFixSql', $this->lastActionFixSql);
        $criteria->compare('followUpAtFirstPay', $this->followUpAtFirstPay);
        $criteria->compare('watchedVideosAtFirstPay', $this->watchedVideosAtFirstPay);
        $criteria->compare('idSourceList', $this->idSourceList);
        $criteria->compare('deviceTypeSource', $this->deviceTypeSource);

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
        ));
    }

    /**
     * * Retrieves a list of models based on the current search/filter conditions.
     * @param array $attributes
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchExport($attributes)
    {
//        /* @var $connection CDbConnection */
//		$connection = Yii::app()->dbSlave;
        /* @var $connection CDbConnection */
        $connection = Yii::app()->db;

		$startDate= $attributes['startDate']=="" ? HeDate::resetDate(true) : $attributes['startDate'];
		$endDate= $attributes['endDate']==""? HeDate::tomorrow(true) : $attributes['endDate'];
		$userType= isset($attributes['userType'])? $attributes['userType']:array();;
        // @TODO: force userType to be the first part of the WHERE SQL
		$filter = " WHERE u.entryDate BETWEEN '$startDate' AND '$endDate' ";
		if(count($userType))
		{
			$filter .= $filter!= "" ? " AND ": "";
			$filter.="(";
			$count=0;
			foreach($userType as $value)
			{
				if($count){
					$filter .= " OR u.userType=$value";
                }
				else{
					$filter .= " u.userType=$value";
                }
				$count++;
			}
			$filter.=")";
		}
		//Create keyQuery
		$keyQuery = HeDate::now(true);
        $command = $connection->createCommand("TRUNCATE aba_b2c_summary.payments_export_mem_aux;");
        $dataReader = $command->query();
		//Insert items into the aux table:
        $sql="INSERT INTO aba_b2c_summary.payments_export_mem_aux (idQuery, userid, lastDate, firstDate)
                SELECT '".$keyQuery."' as idQuery, userid,
                        MAX(dateStartTransaction) as lastDate,
                        MIN(dateStartTransaction) as firstDate
                FROM ".Yii::app()->params["mainDbName"].".payments p
                WHERE p.status=".PAY_SUCCESS." AND p.paySuppExtId<>".PAY_SUPPLIER_GROUPON."
                GROUP BY p.userid";
		$command = $connection->createCommand($sql);
        $dataReader=$command->query();

        /* @var $connection CDbConnection */
        $connection = Yii::app()->dbSlave;

		$subField= " pp.idProduct, ";
		$subQuery= " LEFT JOIN  aba_b2c_summary.payments_export_mem_aux ppa ON ppa.userid=u.id
		             LEFT JOIN ".Yii::app()->params["mainDbName"].".payments AS pp ON pp.dateStartTransaction=ppa.lastDate AND pp.userid=u.id";

        $sql="SELECT ".$subField." u.id AS userid, /*u.name, u.surnames, u.email,*/ c.name AS countryName,
                u.countryId, u.birthDate, /*u.telephone,*/ u.langEnv, u.langCourse,
                ELT(u.userType+1, 'DELETED', 'FREE', 'PREMIUM', 'PLUS', 'TEACHER' )as userTypeStr,
                ELT(u.currentLevel,
                        'Beginners','Lower intermediate','Intermediate,' ,'Upper intermediate','Advanced','Bussiness')
                                                                                                   as currentLevelStr,
                u.entryDate, u.expirationDate, IF(ppa.firstDate is null, '', ppa.firstDate) as dateInitAsPremium,
                /*CONCAT(uu.name, ' ', uu.surnames) as teacherSurname,*/ u.idPartnerFirstPay,
                /*u.releaseCampus,*/ u.firstLoginDone,
                ap.name as namePartnerSourceStr,
                ap.idPartner as idPartnerSource,
                ap1.name as namePartnerFirstPayStr,
                ap2.name as namePartnerCurrentStr,
                ap3.name as nameSourceListStr,
                ap3.id as idSourceList,
                ap.nameGroup as partnerGroupSource,
                ap1.nameGroup as partnerGroupFirstPay,
                ap2.nameGroup as partnerGroupCurrent,
                IF(ppa.firstDate<='2013-05-30 00:00:00', 'NO',
                            IF(u.userType=1 AND u.cancelReason IS NULL AND u.`followUpAtFirstPay` IS NULL,
                                IF( COUNT(f.id)>0,
                                    MAX( FLOOR(
                                    (IF(f.sit_por>50, 100, f.sit_por)+
                                     IF((f.stu_por*2)>100, 100, (f.stu_por*2))+
                                     f.dic_por+
                                     IF(f.rol_on>0, 100, f.rol_por)+
                                     IF(f.gra_vid>0, 100, 0)+
                                     f.wri_por+
                                     IF((f.new_por*2)>100, 100, (f.new_por*2))
                                     )/7)),'NO'
                                    ),
                                 IF(u.`followUpAtFirstPay` IS NULL,'NO',u.`followUpAtFirstPay`))
                )as `maxFollowUpAtFirstPay`,
                IF(COUNT(f.id)>=1,
                   MAX( FLOOR(
                    (IF(f.sit_por>50, 100, f.sit_por)+
                     IF((f.stu_por*2)>100, 100, (f.stu_por*2))+
                     f.dic_por+
                     IF(f.rol_on>0, 100, f.rol_por)+
                     IF(f.gra_vid>0, 100, 0)+
                     f.wri_por+
                     IF((f.new_por*2)>100, 100, (f.new_por*2))
                     )/7)
                ),0) as `maxFollowUpNow`,
                @varCurrentWatchedVideos:='TEMP DISABLED' as tmpCurrentWatchedVideoClass,
                IF(@varCurrentWatchedVideos IS NULL,'NO',@varCurrentWatchedVideos)  as currentWatchedVideoClass,
                IF(u.watchedVideosAtFirstPay IS NULL, 'NO', u.watchedVideosAtFirstPay) as `watchedVideosAtFirstPay`,
                IF(u.savedUserObjective, 'YES', 'NO') as filledFormObjectives,
                u.deviceTypeSource AS deviceType
               FROM ".Yii::app()->params["mainDbName"].".user u
                    LEFT join ".Yii::app()->params["mainDbName"].".country c on c.id = u.countryId
                    LEFT join ".Yii::app()->params["mainDbName"].".user uu on uu.id = u.teacherId
                    LEFT JOIN ".Yii::app()->params["dbCampusSummary"].".followup4_summary f ON u.id=f.userid
                    LEFT JOIN ".Yii::app()->params["mainDbName"].".aba_partners_list ap on ap.idPartner = u.idPartnerSource
                    LEFT JOIN ".Yii::app()->params["mainDbName"].".aba_partners_list ap1 on ap1.idPartner = u.idPartnerFirstPay
                    LEFT JOIN ".Yii::app()->params["mainDbName"].".aba_partners_list ap2 on ap2.idPartner = u.idPartnerCurrent
                    LEFT JOIN ".Yii::app()->params["mainDbName"].".sources_list ap3 on ap3.id = u.idSourceList ".
                    $subQuery.
               $filter.
               " GROUP BY u.id
                ORDER BY u.entryDate ASC; ";

		$command = $connection->createCommand($sql);
        ini_set('memory_limit', '4096M');
        //$mbMemBefore = memory_get_usage()/1024/1024;
        $dataReader=$command->query();

		return $dataReader;
	}

    /** After updating any data we send a request to update Selligent.
     * @throws CHttpException
     */
    protected function afterSave()
    {
        if(!HeWebServices::synchroUser($this->id))
        {
            throw new CHttpException(404,'Can\'t update info in Selligent, web service response was negative.');
        }

        parent::afterSave();
    }


    public function unsubscribe( )
    {
        $connection = Yii::app()->db;

       /* $sql = " SET @userId:=0";
        $command = $connection->createCommand($sql);
        $dataReader=$command->query();*/

        $sql = " CALL softDeleteUserFromCampus( :EMAIL, @userId )";
        $command = $connection->createCommand($sql);
        $command->bindValues(array(":EMAIL"=>$this->email));
        $dataReader=$command->execute();

        /*$sql = " SELECT @userId as userId; ";
        $command = $connection->createCommand($sql);
        $dataReader=$command->query();*/

        if($dataReader == 0){
            return true;
        }

        return true;
    }


    /**
     * Copy from campus
     *
     */
    public $id;
    public $abaUserLogUserActivity;

    public function __construct() {
        parent::__construct();
    }

    public function mainFields()
    {
        return "u.`id`, u.`name`, u.`surnames`, u.`email`, u.`password` as `password`, u.`complete`, ".
        " u.`city`, u.`countryId`, u.`countryIdCustom`, ".
        "  DATE_FORMAT(u.birthDate,'%d-%m-%Y') as 'birthDate',".
        " u.`telephone`, u.`langEnv`, u.`langCourse`, u.`userType`, u.`currentLevel`, ".
        " DATE_FORMAT(u.entryDate,'%d-%m-%Y') as 'entryDate', ".
        " DATE_FORMAT(u.expirationDate,'%d-%m-%Y') as 'expirationDate', ".
        " u.`teacherId`, u.`idPartnerFirstPay`, u.`idPartnerSource`, u.`idPartnerCurrent`, ".
        " u.`savedUserObjective`, u.`firstLoginDone`, u.`keyExternalLogin`, u.`gender`, ".
        " u.`cancelReason`, u.`registerUserSource`, u.`hasWorkedCourse`, u.`dateLastSentSelligent`, ".
        " u.`followUpAtFirstPay`, u.`watchedVideosAtFirstPay`, u.`idSourceList`, u.`deviceTypeSource`, u.`dateUpdateEmma` ";
    }

    function setId($id) {
        $this->id=$id;
    }

    function getId() {
        return $this->id;
    }

    public function getUserById($inId=0)
    {
        if($inId == 0) {
            $inId = $this->id;
        }
        $query = "SELECT ".$this->mainFields()." FROM ".$this->tableName()." u WHERE id=$inId";
        $dataReader = $this->querySQL($query);
        if(($row = $dataReader->read()) !== false)
        {
            $this->fillDataColsToProperties($row);
            $this->abaUserLogUserActivity = new AbaLogUserActivity($this);
            return true;
        }
        return false;
    }

    /**
     * Initializes the object AbaUser based on email. Can be used to authenticate an user.
     * @param string $email
     * @param string $password
     * @param bool $usePassword
     * @param bool $pwdEncrypted
     *
     * @return bool
     */
    public function getUserByEmail($email, $password = "", $usePassword = false, $pwdEncrypted = false)
    {
        if (is_array($email)) {
            $email = $email['email'];
        }

        $verifyPassword = ($usePassword == true) ? " AND (u.`password`='" . HeAbaSHA::_md5($password) .
          "' OR u.`password`=CONCAT('(',:PASSWORD,')') )" :
          "";
        $verifyPassword = ($usePassword && $pwdEncrypted) ? " AND (u.`password`='" . $password .
          "' OR u.`password`=CONCAT('(',:PASSWORD,')') )" :
          $verifyPassword;

        $sql = "SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " u WHERE u.`email` = :EMAIL  $verifyPassword";
        $paramsToSQL = array(":EMAIL" => $email);
        if ($verifyPassword !== '') {
            $paramsToSQL[":PASSWORD"] = $password;
        }
        $dataReader = $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            $this->abaUserLogUserActivity = new AbaLogUserActivity($this);
            return true;
        }
        return false;
    }

}