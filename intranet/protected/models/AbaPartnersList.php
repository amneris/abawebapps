<?php

/**
 * This is the model class for table "aba_partners_list".
 *
 * The followings are the available columns in table 'aba_partners_list':
 * @property integer idPartner
 * @property integer idPartnerGroup
 * @property string name
 * @property string nameGroup (Magic property, only in some funcitons)
 * @property integer idCategory
 * @property integer idBusinessArea
 * @property integer idType
 * @property integer idGroup
 * @property integer idChannel
 * @property integer idCountry
 * @property integer partnerIdCancel (Magic property, only in some funcitons)
 */
class AbaPartnersList extends CActiveRecord
{
    // 0-Aba English., 1-Ventas Flash,  2-Social, 3-Affiliates, 20-Others';
    private static $PARTNER_ABA_G = 0;
    private static $PARTNER_GROUPON_G = 1;
    private static $PARTNER_SOCIAL_G = 2;
    private static $PARTNER_AFFILIATES_G = 3;
    private static $PARTNER_B2B_G = 4;
    private static $PARTNER_OTHER_G = 20;

    private static $PARTNER_ID_EXABAENGLISH = 300011;
    private static $PARTNER_ID_EXFLASHSALES = 300013;
    private static $PARTNER_ID_EXSOCIALAFFILIATES = 300014;
    private static $PARTNER_ID_EXB2B = 300025;

    public function mainFields()
    {
        return "
            a.`idPartner`, a.`name`, a.`idPartnerGroup`, a.`nameGroup`, a.`idCategory`, 
            a.`idBusinessArea`, a.`idType`, a.`idGroup`, a.`idChannel`, a.`idCountry`
        ";

    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return AbaPartnersList the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'aba_partners_list';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
          array('name , idPartnerGroup', 'required'),
          array('name', 'length', 'max' => 60),
          array('idPartnerGroup', 'length', 'max' => 3),
          array(
            'idCategory, idBusinessArea, idType, idGroup, idChannel, idCountry',
            'numerical',
            'integerOnly' => true
          ),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
          array(
            'idPartner, name, idPartnerGroup, idCategory, idBusinessArea, idType, idGroup, idChannel, idCountry',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @param $idPartnerGroup
     *
     * @return array
     */
    public static function getPartnerGroupRange($idPartnerGroup) {

        $stResponse = array(
            "rangeFrom" => 600001,
            "rangeTo" => 700000,
            "partnerGroupLabel" => PARTNER_OTHER_W,
        );

        switch ($idPartnerGroup) {
            case PARTNER_ABA_G:
                $stResponse["rangeFrom"] = 300001;
                $stResponse["rangeTo"] = 400000;
                $stResponse["partnerGroupLabel"] = PARTNER_ABA_W;
                break;
            case PARTNER_SOCIAL_G:
                $stResponse["rangeFrom"] = 5001;
                $stResponse["rangeTo"] = 6000;
                $stResponse["partnerGroupLabel"] = PARTNER_SOCIAL_W;
                break;
            case PARTNER_AFFILIATES_G:
                $stResponse["rangeFrom"] = 6001;
                $stResponse["rangeTo"] = 7000;
                $stResponse["partnerGroupLabel"] = PARTNER_AFFILIATES_W;
                break;
            case PARTNER_B2B_G:
                $stResponse["rangeFrom"] = 7001;
                $stResponse["rangeTo"] = 8000;
                $stResponse["partnerGroupLabel"] = PARTNER_B2B_W;
                break;
            case PARTNER_POSTAFFILIATES_G:
                $stResponse["rangeFrom"] = 8001;
                $stResponse["rangeTo"] = 9000;
                $stResponse["partnerGroupLabel"] = PARTNER_POSTAFFILIATES_W;
                break;
            case PARTNER_ABAMOBILE_G:
                $stResponse["rangeFrom"] = 400001;
                $stResponse["rangeTo"] = 600000;
                $stResponse["partnerGroupLabel"] = PARTNER_ABAMOBILE_W;
                break;
            default:
                $stResponse["rangeFrom"] = 600001;
                $stResponse["rangeTo"] = 700000;
                $stResponse["partnerGroupLabel"] = PARTNER_OTHER_W;
                break;
        }
        return $stResponse;
    }

    /**
     * @return bool
     */
    public function validateIdPartner()
    {
        //
        if (trim($this->idPartner) == '') {
            $this->addError('idPartner', 'The field idPartner is mandatory');
            return false;
        }

        $preQuery = " SELECT idPartner FROM aba_b2c.aba_partners_list pl WHERE pl.idPartner = '" . $this->idPartner . "' LIMIT 0, 1 ";
        $preConnection = Yii::app()->db2;
        $preCommand = $preConnection->createCommand($preQuery);
        $dataReader = $preCommand->query();
        $rows = $dataReader->read();
        if (isset($rows['idPartner']) AND is_numeric($rows['idPartner'])) {
            $this->addError('idPartner', 'This Identifier already exists in the database');
            return false;
        }
        return true;
    }

    /**
     * @param $xModel
     * @param array $stParams
     *
     * @return mixed
     */
    public function savePartner($xModel, $stParams = array())
    {
        $stPartnerGroupRanges = self::getPartnerGroupRange($xModel->idPartnerGroup);

        $rangeFrom = $stPartnerGroupRanges["rangeFrom"];
        $rangeTo = $stPartnerGroupRanges["rangeTo"];
        $partnerGroupLabel = $stPartnerGroupRanges["partnerGroupLabel"];

        if (isset($stParams['idPartner']) AND is_numeric($stParams['idPartner'])) {
            $xModel->idPartner = $stParams['idPartner'];
        } else {
            $xModel->idPartner = $this->getMaxPartnerID($rangeFrom, $rangeTo);
        }

        $xModel->nameGroup = $partnerGroupLabel;

        if(!$xModel->validateIdPartner()) {
            return false;
        }

        return ($xModel->save());
    }

    public function getMaxPartnerID($rangeFrom, $rangeTo)
    {
        $maxId = null;
        $preQuery = "SELECT max(idPartner) as maxId FROM (select a.idPartner from aba_b2c.aba_partners_list a where a.idPartner >= '$rangeFrom' && a.idPartner < '$rangeTo') a";
        $preConnection = Yii::app()->db2;
        $preCommand = $preConnection->createCommand($preQuery);
        $dataReader = $preCommand->query();
        $rows = $dataReader->read();
        if ($rows['maxId'] == null) {
            if (is_numeric($rangeFrom)) {
                $maxId = $rangeFrom;
            } else {
                $preQuery = "SELECT max(idPartner) + 1 as maxId FROM aba_b2c.aba_partners_list ";
                $preConnection = Yii::app()->db2;
                $preCommand = $preConnection->createCommand($preQuery);
                $dataReader = $preCommand->query();
                $rows = $dataReader->read();

                if (isset($rows['maxId']) AND is_numeric($rows['maxId'])) {
                    $maxId = $rows['maxId'];
                }
            }
        } else {
            $maxId = $rows['maxId'];
            $maxId++;
        }
        return $maxId;
    }

    public function insertPartner($desiredID, $xModel, $partnerGroupLabel)
    {
        $xModel->idPartner = $desiredID;
        $query = "INSERT INTO aba_b2c.aba_partners_list (idPartner, name, idPartnerGroup, nameGroup) VALUES ($xModel->idPartner, '$xModel->name', $xModel->idPartnerGroup, '$partnerGroupLabel');";
        $connection = Yii::app()->db2;
        $command = $connection->createCommand($query);
        $result = $command->query();
        if ($result) {
            return $xModel;
        } else {
            return false;
        }
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'idPartner' => 'Id Partner',
          'name' => 'Name',
          'idPartnerGroup' => 'Partner Group',
          'idCategory' => 'Category',
          'idBusinessArea' => 'Business Area',
          'idType' => 'Type',
          'idGroup' => 'Group',
          'idChannel' => 'Channel',
          'idCountry' => 'Country',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idPartner', $this->idPartner, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('idPartnerGroup', $this->idPartnerGroup, true);

        $criteria->compare('idCategory', $this->idCategory, false);
        $criteria->compare('idBusinessArea', $this->idBusinessArea, false);
        $criteria->compare('idType', $this->idType, false);
        $criteria->compare('idGroup', $this->idGroup, false);
        $criteria->compare('idChannel', $this->idChannel, false);
        $criteria->compare('idCountry', $this->idCountry, false);

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
        ));
    }

    /**
     * @param $group
     *
     * @return array
     */
    public function getFullList($group = -1)
    {
        $sql = "select idPartner, name FROM aba_partners_list";
        if ($group != -1) {
            $sql .= " WHERE idPartnerGroup=$group";
        }
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();
        $theArray = array();
        while (($row = $dataReader->read()) !== false) {
            $theArray[$row['idPartner']] = $row['name'];
        }
        return $theArray;
    }

    public function getListWithoutFlashSales()
    {
        $sql = "select idPartner, name FROM aba_partners_list where idPartnerGroup <> 1 order by name asc";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();
        $theArray = array();
        while (($row = $dataReader->read()) !== false) {
            $theArray[$row['idPartner']] = $row['name'];
        }
        return $theArray;
    }

    /**
     * @static
     * @return array
     */
    static function getFullListPartnerToGroup()
    {
        $sql = "select idPartner, idPartnerGroup FROM aba_partners_list";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();
        $theArray = array();
        while (($row = $dataReader->read()) !== false) {
            $theArray[$row['idPartner']] = $row['idPartnerGroup'];
        }
        return $theArray;
    }

    static function getListPartnerGroup()
    {
        $sql = "select idPartnerGroup, nameGroup FROM aba_partners_list group by idPartnerGroup";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();
        $theArray = array();
        while (($row = $dataReader->read()) !== false) {
            $theArray[$row['idPartnerGroup']] = ucfirst(strtolower($row['nameGroup']));
        }
        return $theArray;
    }

    static function getListPartnerFromGroup($idPartnerGroup)
    {
        if ($idPartnerGroup == '') {
            $sql = "SELECT idPartner FROM aba_partners_list;";
        } else {
            $sql = "SELECT idPartner FROM aba_partners_list WHERE idPartnerGroup = $idPartnerGroup;";
        }
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();
        $theArray = array();
        while (($row = $dataReader->read()) !== false) {
            $theArray[] = $row['idPartner'];
        }
        return $theArray;

    }

    public function getFullGroupList($partnerSelection)
    {
        $sql = "SELECT idPartner, name FROM aba_partners_list WHERE idPartner IN(" . implode(",",
            $partnerSelection) . ")";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();
        $theArray = array();
        while (($row = $dataReader->read()) !== false) {
            $theArray[$row['idPartner']] = $row['name'];
        }
        return $theArray;
    }

    /** Returns the id Partner for Cancellation
     * @return bool
     */
    public function getPartnerCancelById()
    {
        $sql = " SELECT a.* " .
          ", CASE a.`idPartnerGroup` WHEN " . AbaPartnersList::$PARTNER_ABA_G . " THEN " . AbaPartnersList::$PARTNER_ID_EXABAENGLISH .
          " WHEN " . AbaPartnersList::$PARTNER_GROUPON_G . " THEN " . AbaPartnersList::$PARTNER_ID_EXFLASHSALES .
          " WHEN " . AbaPartnersList::$PARTNER_AFFILIATES_G . " THEN " . AbaPartnersList::$PARTNER_ID_EXSOCIALAFFILIATES .
          " WHEN " . AbaPartnersList::$PARTNER_SOCIAL_G . " THEN " . AbaPartnersList::$PARTNER_ID_EXSOCIALAFFILIATES .
          " WHEN " . AbaPartnersList::$PARTNER_B2B_G . " THEN " . AbaPartnersList::$PARTNER_ID_EXB2B .
          " ELSE " . AbaPartnersList::$PARTNER_ID_EXSOCIALAFFILIATES . " END as `partnerIdCancel`" .
          " FROM  " . $this->tableName() .
          " a WHERE a.`idPartner`=" . $this->idPartner . " ; ";

        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();

        $theArray = array();
        if (($row = $dataReader->read()) !== false) {
            return $row['partnerIdCancel'];
        }

        return false;
    }
}