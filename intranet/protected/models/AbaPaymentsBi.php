<?php

/**
 * This is the moFrmPayment class for table "payments".
 *
 * The followings are the available columns in table 'payments':
 * @property string $id
 * @property string $idUserProdStamp
 * @property string $userId
 * @property string $idProduct
 * @property string $idCountry
 * @property string $idPeriodPay
 * @property string $idUserCreditForm
 * @property string $paySuppExtId
 * @property string $paySuppOrderId
 * @property string $status
 * @property string $dateStartTransaction
 * @property string $dateEndTransaction
 * @property string $dateToPay
 * @property string $xRateToEUR
 * @property string $xRateToUSD
 * @property string $currencyTrans
 * @property string $foreignCurrencyTrans
 * @property string $idPromoCode
 * @property string $amountOriginal
 * @property string $amountDiscount
 * @property string $amountPrice
 * @property string $idPayControlCheck
 * @property string $idPartner
 * @property string $paySuppExtProfId
 * @property string $autoId
 * @property string $lastAction
 * @property integer $isRecurring
 * @property string $paySuppExtUniId
 * @property integer $paySuppLinkStatus
 * @property integer $isPeriodPayChange
 * @property integer $cancelOrigin
 * @property integer $isExtend
 *
 */
class AbaPaymentsBi extends AbaActiveRecord
{
    public $idPartnerGroup;
    public $action;
    public $endDateTransaction;

    /**
     * Returns the static moFrmPayment of the specified AR class.
     * @param string $className active record class name.
     * @return AbaPayments the static moFrmPayment class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'payments';
    }


    /**
     * @param string $dateStart
     * @param string $dateEnd
     * @param string $idCountry
     *
     * @return array
     */
    public function getAllPaymentsBySupplier($dateStart = '2013-01-30', $dateEnd = '', $idCountry = "")
    {
        $wEnd = "";
        if ($dateEnd !== '') {
            $wEnd = " AND DATE(dbi.dateJoin)<=DATE('" . $dateEnd . "')";
        }

        $wIdCountry = "";
        if ($idCountry !== "") {
            $wIdCountry = " AND p.idCountry=" . $idCountry;
        }

        $sql = " SELECT DATE(dbi.`dateJoin`) as datePayment,
                    SUM(IF( p_filter.paySuppExtId=" . PAY_SUPPLIER_PAYPAL . ",1,0)) as `PayPal`,
                    SUM(IF( p_filter.paySuppExtId=" . PAY_SUPPLIER_CAIXA . ",1,0)) as `Caixa`,
                    SUM(IF( p_filter.paySuppExtId=" . PAY_SUPPLIER_ALLPAGO_BR . ",1,0)) as `AllPago-Br`,
                    SUM(IF( p_filter.paySuppExtId=" . PAY_SUPPLIER_ALLPAGO_MX . ",1,0)) as `AllPago-Mx`,
                    SUM(IF( p_filter.paySuppExtId=" . PAY_SUPPLIER_ALLPAGO_BR_BOL . ",1,0)) as `AllPago-Boleto`,
                    SUM(IF( p_filter.paySuppExtId=" . PAY_SUPPLIER_ALLPAGO_MX_OXO . ",1,0)) as `Oxo`,
                    SUM(IF( p_filter.paySuppExtId=" . PAY_SUPPLIER_APP_STORE . ",1,0)) as `AppStore`,
                    SUM(IF( p_filter.paySuppExtId=" . PAY_SUPPLIER_ADYEN . ",1,0)) as `Adyen`,
                    SUM(IF( p_filter.paySuppExtId=" . PAY_SUPPLIER_ADYEN_HPP . ",1,0)) as `Adyen-Hpp`,
                    SUM(IF( p_filter.paySuppExtId=" . PAY_SUPPLIER_ANDROID_PLAY_STORE . ",1,0)) as `Android-PlayStore`,
                    SUM(IF( p_filter.paySuppExtId=" . PAY_SUPPLIER_Z . ",1,0)) as `Zuora`
                FROM " . Yii::app()->params["dbCampusSummary"] . ".all_dates_bi dbi
                  LEFT JOIN (
                        SELECT DATE(p.dateEndTransaction) as datePayment, p.paySuppExtId, ps.`name`
                          FROM  " . Yii::app()->params['mainDbName'] . ".payments p
                          LEFT JOIN " . Yii::app()->params['mainDbName'] . ".pay_suppliers ps ON p.paySuppExtId=ps.id
                          WHERE DATE(p.dateEndTransaction)>=DATE('" . $dateStart . "')
                                AND  p.`status`=" . PAY_SUCCESS . "
                                AND  p.isRecurring=0
                                AND p.paySuppExtId IS NOT NULL
                                $wIdCountry
                          GROUP BY p.dateEndTransaction
                  )     as `p_filter` ON dbi.dateJoin=p_filter.datePayment
              WHERE DATE(dbi.dateJoin)>=DATE('" . $dateStart . " ') " .
          $wEnd .
          " GROUP BY dbi.dateJoin
              ORDER BY dbi.dateJoin ASC; ";

//        $connection = Yii::app()->dbSlave;
//        $command = $connection->createCommand($sql);
//        $dataReader=$command->query();

        $dataReader = $this->queryAllSQL($sql, array(), AbaActiveRecord::DB_SLAVE);
        $theArray = array();
        $theArray[] = array(
          'Payment Date',
          'Paypal',
          'Caixa',
          'AllPago-Br',
          'AllPago-Mx',
          'AllPago-Boleto',
          'Oxo',
          'AppStore',
          'Adyen',
          'Adyen-Hpp',
          'Android-PlayStore',
          'Zuora'
        );
//        while(($row = $dataReader->read())!==false){

        if (is_array($dataReader)) {
            foreach ($dataReader as $row) {
                $theArray[] = array(
                  $row["datePayment"],
                  intval($row["PayPal"]),
                  intval($row["Caixa"]),
                  intval($row["AllPago-Br"]),
                  intval($row["AllPago-Mx"]),
                  intval($row["AllPago-Boleto"]),
                  intval($row["Oxo"]),
                  intval($row["AppStore"]),
                  intval($row["Adyen"]),
                  intval($row["Adyen-Hpp"]),
                  intval($row["Android-PlayStore"]),
                  intval($row["Zuora"])
                );
            }
        }

        return $theArray;
    }

    /**
     * @param string $dateStart
     * @param string $dateEnd
     * @param string $idCountry
     * @param string $paySuppExtId
     *
     * @return array
     */
    public function getAttemptsKPI($dateStart = '2013-04-30', $dateEnd = '', $idCountry = '', $paySuppExtId = '')
    {
        $wEnd = " AND DATE(dbi.dateJoin)<=DATE('" . HeDate::todaySQL(false) . "')";

        $wIdCountry = "";
        if ($idCountry !== "") {
            $wIdCountry = " AND pc.idCountry=" . $idCountry;
        }

        $wPaySuppExtId = "";
        if ($paySuppExtId !== "") {
            $wPaySuppExtId = " AND pc.paySuppExtId= " . $paySuppExtId;
        }

        $sql = " SELECT CONCAT(DATE(dbi.dateJoin),'(',COUNT(pc.id),')') as dateAttempt,
                    IF(COUNT(pc.id)=0, 0, ROUND(COUNT(p.id)*100/COUNT(pc.id),2)) as PKIPaymentSessionAttempts,
                    IF(COUNT(pc.id)=0, 0, ROUND(COUNT(p.id)*100/SUM(pc.attempts),2)) as PKIPaymentAbsAttempts
                FROM " .
          Yii::app()->params["dbCampusSummary"] . ".all_dates_bi dbi
                    LEFT JOIN " . Yii::app()->params["mainDbName"] . ".payments_control_check pc
                             ON (dbi.dateJoin=DATE(pc.dateStartTransaction)
                                $wPaySuppExtId
                                $wIdCountry)
                    LEFT JOIN " . Yii::app()->params["mainDbName"] . ".payments p
                             ON (pc.id=p.id AND p.`status`=" . PAY_SUCCESS . ")

                WHERE
                  DATE(dbi.dateJoin)>=DATE('" . $dateStart . "')" .
          $wEnd . "
                GROUP BY DATE(dbi.dateJoin)
                ORDER BY DATE(dbi.dateJoin) ASC; ";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();

        $theArray = array();
        $theArray[] = array(
          'Attempt payment date',
          'PKI End successful payment',
          'PKI Successful after several attempts'
        );
        while (($row = $dataReader->read()) !== false) {
            $theArray[] = array(
              $row["dateAttempt"],
              doubleval($row["PKIPaymentSessionAttempts"]),
              doubleval($row["PKIPaymentAbsAttempts"])
            );
        }

        return $theArray;
    }


}