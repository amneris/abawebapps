<?php

class AbaVFInsertion extends CModel
{
    public $m1;
    public $m2;
    public $m3;
    public $m6;
    public $m12;
    public $m18;
    public $m24;
    public $agrupador;
    public $fileName;
    public $fecha;
    public $count=1;
    public $deal=1;
	
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    public function validate($attributes=null, $clearErrors=true)
    {
         $theRet=  parent::validate($attributes, $clearErrors);
         if(!$theRet)
             return $theRet;
         if($this->m1 == 1 || $this->m2 == 1 || $this->m3 == 1 || $this->m6 == 1 || $this->m12 == 1 || $this->m18 == 1 || $this->m24 == 1)
           return true;
         else
         {
            Yii::app()->user->setFlash('errorCheckBox','Debes al menos seleccionar un producto.');
            return false;
         }
            

    }

    public function rules()
    {
        return array(
          array('fileName', 'file', 'allowEmpty' => true, 'types' => 'xls'),
          array('agrupador, fecha, count, deal, fileName', 'required'),
          array('count, deal', 'numerical', 'integerOnly' => true),
          array('count', 'length', 'max' => 2),
          array('deal', 'length', 'max' => 3),
        );
    }

    public function search()
    {

    }

    public function relations()
    {
        return array();
    }

    public function attributeLabels()
    {

        return array(
          'm1' => 'Monthly',
          'm2' => 'Bimonthly',
          'm3' => 'Quarterly',
          'm6' => 'Semiannual',
          'm12' => 'Annual',
          'm18' => '18 months',
          'm24' => '24 months',
          'agrupador' => 'Grouper',
          'fileName' => 'Filename',
          'fecha' => 'Date',
          'count' => 'Validations allowed',
          'deal' => 'Campaign (Deal)',
        );
    }

    public function attributeNames()
    {
        return array(
          'm1',
          'm2',
          'm3',
          'm6',
          'm12',
          'm18',
          'm24',
          'agrupador',
          'fileName',
          'fecha',
          'count',
          'deal',
        );
    }

}

?>