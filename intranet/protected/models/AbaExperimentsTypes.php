<?php

class AbaExperimentsTypes extends CmAbaExperimentsTypes
{
    public function __construct($scenario = 'insert', $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaProductPrice the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'experimentTypeId' => 'ID',
          'experimentTypeIdentifier' => 'Type identifier',
          'experimentTypeDescription' => 'Type description',
          'dateAdd' => 'Create date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('experimentTypeId', $this->experimentTypeId, true);
        $criteria->compare('experimentTypeIdentifier', $this->experimentTypeIdentifier, true);
        $criteria->compare('experimentTypeDescription', $this->experimentTypeDescription, true);
        $criteria->compare('dateAdd', $this->dateAdd, true);

        $criteria->order = 'experimentTypeId';

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
          'pagination' => array(
            'pageSize' => 10,
          ),
        ));
    }

    /**
     * @param bool $runValidation
     * @param null $attributes
     *
     * @return bool
     */
    public function save($runValidation = true, $attributes = null)
    {
        if ($this->validateExperimenTypeIdentifier()) {
            return parent::save($runValidation, $attributes);
        }
        return false;
    }

    /**
     * @return bool
     */
    public function validateExperimenTypeIdentifier()
    {
        //
        if (trim($this->experimentTypeIdentifier) == '') {
            $this->addError('experimentTypeIdentifier', 'The field experimentTypeIdentifier is mandatory');
            return false;
        }

        //
        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " ext ";
        $sSql .= " WHERE ext.experimentTypeIdentifier = '" . addslashes($this->experimentTypeIdentifier) . "' ";

        if(is_numeric($this->experimentTypeId)) {
            $sSql .= " AND ext.experimentTypeId <> '" . $this->experimentTypeId . "' ";
        }

        $sSql .= " LIMIT 0, 1 ";

        $dataReader = $this->querySQL($sSql);
        if (($row = $dataReader->read()) !== false) {
            $this->addError('experimentTypeIdentifier', 'This Identifier already exists in the database');
            return false;
        }
        return true;
    }

    /**
     * @return array
     */
    public function getExperimentsTypes()
    {
        $stAllDescriptions = $this->getAllExperimentsTypes();

        $stFormattedDescription = array();

        foreach ($stAllDescriptions as $stDescription) {
            $stFormattedDescription[] = array(
              'id' => $stDescription['experimentTypeId'],
              'title' => $stDescription['experimentTypeDescription']
            );
        }

        return $stFormattedDescription;
    }

    /**
     * @param bool $bEmpty
     *
     * @return array
     */
    public function getFullList($bEmpty = false)
    {
        $stAllDescriptions = $this->getAllExperimentsTypes();

        $stFormattedDescription = array();

        if ($bEmpty) {
            $stFormattedDescription[""] = "";
        }

        foreach ($stAllDescriptions as $stDescription) {
            $stFormattedDescription[$stDescription['experimentTypeId']] = $stDescription['experimentTypeDescription'];
        }

        return $stFormattedDescription;
    }

}
