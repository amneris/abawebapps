<?php

class AbaPartnergroups extends CmAbaPartnergroups
{
    public function __construct($scenario = 'insert', $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AbaPartnergroups the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'idGroup' => 'ID',
          'nameGroup' => 'Group name',
          'dateAdd' => 'Last update',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('idGroup', $this->idGroup, true);
        $criteria->compare('nameGroup', $this->nameGroup, true);
        $criteria->compare('dateAdd', $this->dateAdd, true);

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
          'pagination' => array(
            'pageSize' => 100,
          ),
        ));
    }

    /**
     * @param bool $runValidation
     * @param null $attributes
     *
     * @return bool
     */
    public function save($runValidation = true, $attributes = null)
    {
        if ($this->validateNameGroup()) {
            return parent::save($runValidation, $attributes);
        }
        return false;
    }

    /**
     * @return bool
     */
    public function validateNameGroup()
    {
        //
        if (trim($this->nameGroup) == '') {
            $this->addError('nameGroup', 'The field nameGroup is mandatory');
            return false;
        }

        //
        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " pgx ";
        $sSql .= " WHERE pgx.nameGroup = '" . addslashes($this->nameGroup) . "' ";

        if(is_numeric($this->idGroup)) {
            $sSql .= " AND pgx.idGroup <> '" . $this->idGroup . "' ";
        }

        $sSql .= " LIMIT 0, 1 ";

        $dataReader = $this->querySQL($sSql);
        if (($row = $dataReader->read()) !== false) {
            $this->addError('nameGroup', 'This Identifier already exists in the database');
            return false;
        }
        return true;
    }

    /**
     * @return array
     */
    public function getPartnerGroups()
    {
        $stAllDescriptions = $this->getAllPartnerGroups();

        $stFormattedDescription = array();

        foreach ($stAllDescriptions as $stDescription) {
            $stFormattedDescription[$stDescription['idGroup']] = $stDescription['nameGroup'];
        }

        return $stFormattedDescription;
    }

    /**
     * @param bool $bEmpty
     *
     * @return array
     */
    public function getFullList($bEmpty = false)
    {
        $stAllDescriptions = $this->getAllPartnerGroups();

        $stFormattedDescription = array();

        if ($bEmpty) {
            $stFormattedDescription[""] = "";
        }

        foreach ($stAllDescriptions as $stDescription) {
            $stFormattedDescription[$stDescription['idGroup']] = $stDescription['nameGroup'];
//            $stFormattedDescription[$stDescription['nameGroup']] = $stDescription['nameGroup'];
        }

        return $stFormattedDescription;
    }

}
