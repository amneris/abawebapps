<?php

/**
 * This is the model class for table "country".
 *
 * The followings are the available columns in table 'country':
 * @property integer $id
 * @property string $days
 * @property integer $ID_month
 * @property string $iso
 * @property string $name_m
 * @property string $name
 * @property string $iso3
 * @property integer $numcode
 * @property string $officialIdCurrency
 * @property string $officialIdCurrency_tmpDisabled
 * @property string $ABAIdCurrency
 * @property string $ABALanguage
 * @property string decimalPoint
 * @property integer invoice
 */
//class AbaCountry extends CActiveRecord
class AbaCountry extends AbaActiveRecord
{
    /**
     * @param null $id
     */
    public function  __construct($id=NULL)
    {
        parent::__construct();
        if(!is_null($id)){
            return $this->getCountryById($id);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " c.`id`, c.`days`, c.`ID_month`, c.`iso`, c.`name_m`, c.`name`, c.`iso3`, c.`numcode`,
                c.`officialIdCurrency`, c.`ABAIdCurrency`, c.`ABALanguage`, c.`decimalPoint`, c.`invoice` ";
    }

    /**
     * @param $id
     *
     * @return $this|bool
     */
    public function getCountryById($id)
    {
        $sql =          " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " c WHERE c.id=:IDCOUNTRY ";
        $paramsToSQL =  array(":IDCOUNTRY" => $id);
        $dataReader =   $this->querySQL($sql, $paramsToSQL);
        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties( $row );
            return $this;
        }
        return false;
    }

    /**
     * @param $id
     *
     * @return $this|bool
     */
    public function getUeCountryById( $id )
    {
        $sql =          " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " c WHERE c.invoice = 1 AND c.id=:IDCOUNTRY ";
        $paramsToSQL =  array(":IDCOUNTRY" => $id);
        $dataReader =   $this->querySQL($sql, $paramsToSQL);
        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties( $row );
            return $this;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isUeCountry()
    {
        if($this->invoice == 1) {
            return true;
        }
        return false;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AbaCountry the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'country';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('officialIdCurrency', 'required'),
			array('ID_month, numcode', 'numerical', 'integerOnly'=>true),
			array('days', 'length', 'max'=>7),
			array('iso', 'length', 'max'=>2),
			array('name_m, name', 'length', 'max'=>100),
			array('iso3, officialIdCurrency, officialIdCurrency_tmpDisabled, ABAIdCurrency, ABALanguage', 'length', 'max'=>3),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, days, ID_month, iso, name_m, name, iso3, numcode, officialIdCurrency, officialIdCurrency_tmpDisabled, ABAIdCurrency, ABALanguage, decimalPoint', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'days' => 'Days',
			'ID_month' => 'Id Month',
			'iso' => 'Iso',
			'name_m' => 'Name M',
			'name' => 'Name',
			'iso3' => 'Iso3',
			'numcode' => 'Numcode',
			'officialIdCurrency' => 'Official Id Currency',
			'officialIdCurrency_tmpDisabled' => 'Official Id Currency Tmp Disabled',
			'ABAIdCurrency' => 'Abaid Currency',
			'ABALanguage' => 'Abalanguage',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('days',$this->days,true);
		$criteria->compare('ID_month',$this->ID_month);
		$criteria->compare('iso',$this->iso,true);
		$criteria->compare('name_m',$this->name_m,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('iso3',$this->iso3,true);
		$criteria->compare('numcode',$this->numcode);
		$criteria->compare('officialIdCurrency',$this->officialIdCurrency,true);
		$criteria->compare('officialIdCurrency_tmpDisabled',$this->officialIdCurrency_tmpDisabled,true);
		$criteria->compare('ABAIdCurrency',$this->ABAIdCurrency,true);
		$criteria->compare('ABALanguage',$this->ABALanguage,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getFullList($idCountry=-1)
	{
		$sql="select id, name FROM country";
		if($idCountry!= -1)
		$sql.=" WHERE id=$idCountry";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);		
        $dataReader=$command->query();
		$theArray=array();
		while(($row = $dataReader->read())!==false)
			$theArray[$row['id']]=$row['name'];
		return $theArray;
	}

    public function getFullUeList($idCountry=-1)
    {
        $sql =  " SELECT id, name FROM country ";
        $sql .= " WHERE invoice = 1 ";
        if($idCountry != -1) {
            $sql.=" AND id = " . $idCountry . " ";
        }
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();
        $theArray = array();
        while(($row = $dataReader->read()) !== false){
            $theArray[$row['id']] = $row['name'];
        }
        return $theArray;
    }

    /**
     * @return array
     */
    public function getCountriesQUsersPremium()
    {
        $sql="SELECT c.name, count(u.userType) as `qPremium`
                 FROM aba_b2c.country c INNER JOIN aba_b2c.`user` u ON c.id=u.countryId
                 WHERE u.userType=".PREMIUM."
                 GROUP BY c.id";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $dataReader=$command->query();

        $theArray = array();
        $theArray[] = array('Country',   'Popularity');
        while(($row = $dataReader->read())!==false){
            $theArray[]=array( $row["name"], intval($row["qPremium"]));
        }

        return $theArray;
    }

    /**
     * @param bool|false $bEmpty
     * @param array $stParams
     *
     * @return array
     */
    public static function getDefaultLanguages($bEmpty=false, $stParams=array()) {

        $stFormattedLanguages = CmAbaCountry::getDefaultLanguages($bEmpty, $stParams);

        return $stFormattedLanguages;
    }

}