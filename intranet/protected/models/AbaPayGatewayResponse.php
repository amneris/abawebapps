<?php

/**
 * This is the model class for table "pay_gateway_response".
 *
 * The followings are the available columns in table 'pay_gateway_response':
 * @property string $id
 * @property string $idPayment
 * @property string $fecha
 * @property string $Order_number
 * @property string $Codigo
 * @property string $Amount
 * @property string $Currency
 * @property string $Signature
 * @property string $MerchantCode
 * @property string $Terminal
 * @property string $Response
 * @property string $AuthorisationCode
 * @property string $TransactionType
 * @property string $SecurePayment
 * @property string $MerchantData
 * @property string $XML
 */
class AbaPayGatewayResponse extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AbaPayGatewayResponse the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pay_gateway_response';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idPayment, Order_number, Codigo, Amount, Currency, Signature, MerchantCode, Terminal, Response, AuthorisationCode, TransactionType, SecurePayment, MerchantData, XML', 'required'),
			array('idPayment, Order_number', 'length', 'max'=>15),
			array('Codigo, Amount, Currency, Signature, MerchantCode, Terminal, Response, AuthorisationCode, TransactionType, SecurePayment, MerchantData', 'length', 'max'=>45),
			array('fecha', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, idPayment, fecha, Order_number, Codigo, Amount, Currency, Signature, MerchantCode, Terminal, Response, AuthorisationCode, TransactionType, SecurePayment, MerchantData, XML', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idPayment' => 'Id Payment',
			'fecha' => 'Fecha',
			'Order_number' => 'Order Number',
			'Codigo' => 'Codigo',
			'Amount' => 'Amount',
			'Currency' => 'Currency',
			'Signature' => 'Signature',
			'MerchantCode' => 'Merchant Code',
			'Terminal' => 'Terminal',
			'Response' => 'Response',
			'AuthorisationCode' => 'Authorisation Code',
			'TransactionType' => 'Transaction Type',
			'SecurePayment' => 'Secure Payment',
			'MerchantData' => 'Merchant Data',
			'XML' => 'Xml',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idPayment',$this->idPayment,true);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('Order_number',$this->Order_number,true);
		$criteria->compare('Codigo',$this->Codigo,true);
		$criteria->compare('Amount',$this->Amount,true);
		$criteria->compare('Currency',$this->Currency,true);
		$criteria->compare('Signature',$this->Signature,true);
		$criteria->compare('MerchantCode',$this->MerchantCode,true);
		$criteria->compare('Terminal',$this->Terminal,true);
		$criteria->compare('Response',$this->Response,true);
		$criteria->compare('AuthorisationCode',$this->AuthorisationCode,true);
		$criteria->compare('TransactionType',$this->TransactionType,true);
		$criteria->compare('SecurePayment',$this->SecurePayment,true);
		$criteria->compare('MerchantData',$this->MerchantData,true);
		$criteria->compare('XML',$this->XML,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}