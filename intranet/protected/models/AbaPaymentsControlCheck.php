<?php

/**
 * This is the model class for table "payments_control_check".
 *
 * The followings are the available columns in table 'payments_control_check':
 * @property string $id
 * @property string $userId
 * @property string $idProduct
 * @property string $idCountry
 * @property string $idPeriodPay
 * @property string $paySuppExtId
 * @property string $paySuppOrderId
 * @property string $status
 * @property string $dateStartTransaction
 * @property string $dateEndTransaction
 * @property string $dateToPay
 * @property string $xRateToEUR
 * @property string $xRateToUSD
 * @property string $currencyTrans
 * @property string $foreignCurrencyTrans
 * @property string $idPromoCode
 * @property string $amountOriginal
 * @property string $amountDiscount
 * @property string $amountPrice
 * @property string $kind
 * @property string $cardNumber
 * @property string $cardYear
 * @property string $cardMonth
 * @property string $cardCvc
 * @property string $cardName
 * @property string $tokenPayPal
 * @property string $payerPayPalId
 * @property string $lastAction
 * @property string $attempts
 */
class AbaPaymentsControlCheck extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AbaPaymentsControlCheck the static model class
	 */
    public $startDate;
    public $endDate;
    public $showReviewed;
    public $idReviewed;
    public $reviewedValue;
    public $email;
    public $userType;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payments_control_check';
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, userId, idProduct, idCountry, idPeriodPay, paySuppExtId, dateStartTransaction, xRateToEUR, xRateToUSD, foreignCurrencyTrans, amountPrice, kind', 'required'),
			array('id, idProduct', 'length', 'max'=>15),
			array('attempts', 'length', 'max'=>10),
            array('reviewed', 'length', 'max'=>2),
			array('userId', 'length', 'max'=>64),
			array('idCountry', 'length', 'max'=>4),
			array('idPeriodPay', 'length', 'max'=>5),
			array('paySuppExtId, status, kind', 'length', 'max'=>2),
			array('paySuppOrderId', 'length', 'max'=>60),
			array('xRateToEUR, xRateToUSD, amountOriginal, amountDiscount, amountPrice', 'length', 'max'=>18),
			array('currencyTrans, foreignCurrencyTrans', 'length', 'max'=>3),
			array('idPromoCode', 'length', 'max'=>50),
			array('cardName, tokenPayPal, payerPayPalId', 'length', 'max'=>45),
			array('dateStartTransaction, dateEndTransaction, dateToPay, cardNumber, cardYear, cardMonth, cardCvc, lastAction', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, userId, idProduct, idCountry, idPeriodPay, paySuppExtId, paySuppOrderId, status, dateStartTransaction, dateEndTransaction, dateToPay, xRateToEUR, xRateToUSD, currencyTrans, foreignCurrencyTrans, idPromoCode, amountOriginal, amountDiscount, amountPrice, kind, cardNumber, cardYear, cardMonth, cardCvc, cardName, tokenPayPal, payerPayPalId, lastAction, attempts', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userId' => 'User',
			'idProduct' => 'Id Product',
			'idCountry' => 'Id Country',
			'idPeriodPay' => 'Id Period Pay',
            'paySuppExtId' => 'Gateway payment platform',
			'paySuppOrderId' => 'Pay Supp Order',
			'status' => 'Status',
			'dateStartTransaction' => 'Date Start Transaction',
			'dateEndTransaction' => 'Date End Transaction',
			'dateToPay' => 'Date To Pay',
			'xRateToEUR' => 'X Rate To Eur',
			'xRateToUSD' => 'X Rate To Usd',
			'currencyTrans' => 'Currency Trans',
			'foreignCurrencyTrans' => 'Foreign Currency Trans',
			'idPromoCode' => 'Id Promo Code',
			'amountOriginal' => 'Amount Original',
			'amountDiscount' => 'Amount Discount',
			'amountPrice' => 'Amount Price',
			'kind' => 'Kind',
			'cardNumber' => 'Card Number',
			'cardYear' => 'Card Year',
			'cardMonth' => 'Card Month',
			'cardCvc' => 'Card Cvc',
			'cardName' => 'Card Name',
			'tokenPayPal' => 'Token Pay Pal',
			'payerPayPalId' => 'Payer Pay Pal',
			'lastAction' => 'Last Action',
			'attempts' => 'Attempts',
            'startDate'=>'Begin period (by date start transaction)',
            'endDate'=>'End period (by date start transaction)',
            'reviewed'=>'Is reviewed'

		);
	}

    /**
     * @param integer $idReviewed One or many values.
     * @param integer|bool $reviewedValue
     */
    static public function setReviewed($idReviewed, $reviewedValue)
    {
        if( strpos( $idReviewed, ',')>=0 ){
            // trim or any parse necessary.
            $idReviewed = trim($idReviewed);
            $idReviewed = str_replace( ",", "','", $idReviewed );
        }

        $sql = "UPDATE payments_control_check SET reviewed=".$reviewedValue." WHERE id IN ('".$idReviewed."')";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $dataReader=$command->query();
      /*  if(($row = $dataReader->read())!==false)
        {
            if($row["num"]!==0)
                $theUserId=$row["id"];
        }*/
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		$theUserId =$this->userId;
		if(HeString::isValidEmail(trim($this->userId)))
		{
			$sql="SELECT count(*) as num, p.id FROM user p WHERE p.email='".trim($this->userId)."' limit 1";
			$connection = Yii::app()->db;
			$command = $connection->createCommand($sql);		
			$dataReader=$command->query();
			if(($row = $dataReader->read())!==false)
			{
				if($row["num"]!==0)
					$theUserId=$row["id"]; 	
			}	
		}
		
		$criteria->compare('t.id',$this->id);
		$criteria->compare('userId',$theUserId);
		$criteria->compare('idProduct',$this->idProduct,true);
		$criteria->compare('idCountry',$this->idCountry);
		$criteria->compare('idPeriodPay',$this->idPeriodPay,true);
		$criteria->compare('paySuppExtId',$this->paySuppExtId,true);
		$criteria->compare('paySuppOrderId',$this->paySuppOrderId,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('dateStartTransaction',$this->dateStartTransaction,true);
		$criteria->compare('dateEndTransaction',$this->dateEndTransaction,true);
		$criteria->compare('dateToPay',$this->dateToPay,true);
		$criteria->compare('xRateToEUR',$this->xRateToEUR,true);
		$criteria->compare('xRateToUSD',$this->xRateToUSD,true);
		$criteria->compare('currencyTrans',$this->currencyTrans,true);
		$criteria->compare('foreignCurrencyTrans',$this->foreignCurrencyTrans,true);
		$criteria->compare('idPromoCode',$this->idPromoCode,true);
		$criteria->compare('amountOriginal',$this->amountOriginal,true);
		$criteria->compare('amountDiscount',$this->amountDiscount,true);
		$criteria->compare('amountPrice',$this->amountPrice,true);
		$criteria->compare('kind',$this->kind,true);
		$criteria->compare('cardNumber',$this->cardNumber,true);
		$criteria->compare('cardYear',$this->cardYear,true);
		$criteria->compare('cardMonth',$this->cardMonth,true);
		$criteria->compare('cardCvc',$this->cardCvc,true);
		$criteria->compare('cardName',$this->cardName,true);
		$criteria->compare('tokenPayPal',$this->tokenPayPal,true);
		$criteria->compare('payerPayPalId',$this->payerPayPalId,true);
		$criteria->compare('lastAction',$this->lastAction,true);
		$criteria->compare('attempts',$this->attempts);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * @param $startDate
     * @param $endDate
     * @param $showReviewed
     *
     * @return CActiveDataProvider
     */
    public function searchByDate($startDate, $endDate, $showReviewed)
    {
        $criteria=new CDbCriteria;
        $theUserId =$this->userId;
        if(HeString::isValidEmail(trim($this->userId)))
        {
            $sql="SELECT count(*) as num, p.id FROM user p WHERE p.email='".trim($this->userId)."' limit 1";
            $connection = Yii::app()->db;
            $command = $connection->createCommand($sql);
            $dataReader=$command->query();
            if(($row = $dataReader->read())!==false)
            {
                if($row["num"]!==0)
                    $theUserId=$row["id"];
            }
        }
        $criteria->select = ' t.*, u.userType ';
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('userId',$theUserId,true);
        $criteria->compare('idProduct',$this->idProduct,true);
        $criteria->compare('idCountry',$this->idCountry);
        $criteria->compare('idPeriodPay',$this->idPeriodPay,true);
        $criteria->compare('paySuppExtId',$this->paySuppExtId,true);
        $criteria->compare('paySuppOrderId',$this->paySuppOrderId,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('dateToPay',$this->dateToPay,true);
        $criteria->compare('xRateToEUR',$this->xRateToEUR,true);
        $criteria->compare('xRateToUSD',$this->xRateToUSD,true);
        $criteria->compare('currencyTrans',$this->currencyTrans,true);
        $criteria->compare('foreignCurrencyTrans',$this->foreignCurrencyTrans,true);
        $criteria->compare('idPromoCode',$this->idPromoCode,true);
        $criteria->compare('amountOriginal',$this->amountOriginal,true);
        $criteria->compare('amountDiscount',$this->amountDiscount,true);
        $criteria->compare('amountPrice',$this->amountPrice,true);
        $criteria->compare('kind',$this->kind,true);
        $criteria->compare('cardNumber',$this->cardNumber,true);
        $criteria->compare('cardYear',$this->cardYear,true);
        $criteria->compare('cardMonth',$this->cardMonth,true);
        $criteria->compare('cardCvc',$this->cardCvc,true);
        $criteria->compare('cardName',$this->cardName,true);
        $criteria->compare('tokenPayPal',$this->tokenPayPal,true);
        $criteria->compare('payerPayPalId',$this->payerPayPalId,true);
        $criteria->compare('lastAction',$this->lastAction,true);
        $criteria->compare('attempts',$this->attempts);


        $criteria->addCondition(" DATE(dateStartTransaction)>='".$startDate."'");
        $criteria->addCondition(" DATE(dateStartTransaction)<='".$endDate."'");

        $criteria->join="inner join user u on u.id=userId";
        $criteria->addCondition('u.userType=1');

        // By default only displays payments attempts not reviewed.
       if(intval($showReviewed)!==1) {
            $criteria->addCondition('reviewed = 0');
       }

         return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));

    }

    public function searchExport($attributes)
    {
        $connection = Yii::app()->dbSlave;
        $table = Yii::app()->params["mainDbName"].".";
        $startDate= $attributes['startDate']=="" ? HeDate::resetDate(true) : $attributes['startDate'];
        $endDate= $attributes['endDate']==""? HeDate::tomorrow(true) : $attributes['endDate'];

        $wShowReviewed=" ";
        if((array_key_exists("showReviewed", $attributes) && $attributes['showReviewed']!="1") ||
            !array_key_exists("showReviewed", $attributes)){
            $wShowReviewed = " AND p.reviewed=0 ";
        }

        $sql="SELECT  u.email, u.userType, p.* FROM ".
                $table."payments_control_check p LEFT JOIN ".$table."user u ON p.userid = u.id ".
                " WHERE  u.userType=1 ".
                        " AND  DATE(p.dateStartTransaction) BETWEEN '".$startDate."' AND '".$endDate."'".
                        $wShowReviewed.
                        " GROUP BY u.id
                         ORDER BY p.dateStartTransaction; ";
        $command = $connection->createCommand($sql);
        $dataReader=$command->query();

        return $dataReader;
    }
}