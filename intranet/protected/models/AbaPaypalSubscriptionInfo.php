<?php

/**
 * This is the model class for table "paypal_subscription_info".
 *
 * The followings are the available columns in table 'paypal_subscription_info':
 * @property string $id
 * @property string $status
 * @property string $subscr_id
 * @property string $sub_event
 * @property string $subscr_date
 * @property string $subscr_effective
 * @property string $period1
 * @property string $period2
 * @property string $period3
 * @property string $amount1
 * @property string $amount2
 * @property string $amount3
 * @property string $mc_amount1
 * @property string $mc_amount2
 * @property string $mc_amount3
 * @property string $currency
 * @property string $recurring
 * @property string $reattempt
 * @property string $retry_at
 * @property string $recur_times
 * @property string $username
 * @property string $password
 * @property string $payment_txn_id
 * @property string $subscriber_emailaddress
 * @property string $datecreation
 */
class AbaPaypalSubscriptionInfo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AbaPaypalSubscriptionInfo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'paypal_subscription_info';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status, currency', 'required'),
			array('status, currency', 'length', 'max'=>10),
			array('subscr_id, subscr_date, subscr_effective, period1, period2, period3, amount1, amount2, amount3, mc_amount1, mc_amount2, mc_amount3, recurring, reattempt, retry_at, recur_times, username, password, subscriber_emailaddress', 'length', 'max'=>255),
			array('sub_event, payment_txn_id', 'length', 'max'=>50),
			array('datecreation', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, status, subscr_id, sub_event, subscr_date, subscr_effective, period1, period2, period3, amount1, amount2, amount3, mc_amount1, mc_amount2, mc_amount3, currency, recurring, reattempt, retry_at, recur_times, username, password, payment_txn_id, subscriber_emailaddress, datecreation', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'status' => 'Status',
			'subscr_id' => 'Subscr',
			'sub_event' => 'Sub Event',
			'subscr_date' => 'Subscr Date',
			'subscr_effective' => 'Subscr Effective',
			'period1' => 'Period1',
			'period2' => 'Period2',
			'period3' => 'Period3',
			'amount1' => 'Amount1',
			'amount2' => 'Amount2',
			'amount3' => 'Amount3',
			'mc_amount1' => 'Mc Amount1',
			'mc_amount2' => 'Mc Amount2',
			'mc_amount3' => 'Mc Amount3',
			'currency' => 'Currency',
			'recurring' => 'Recurring',
			'reattempt' => 'Reattempt',
			'retry_at' => 'Retry At',
			'recur_times' => 'Recur Times',
			'username' => 'Username',
			'password' => 'Password',
			'payment_txn_id' => 'Payment Txn',
			'subscriber_emailaddress' => 'Subscriber Emailaddress',
			'datecreation' => 'Datecreation',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('subscr_id',$this->subscr_id,true);
		$criteria->compare('sub_event',$this->sub_event,true);
		$criteria->compare('subscr_date',$this->subscr_date,true);
		$criteria->compare('subscr_effective',$this->subscr_effective,true);
		$criteria->compare('period1',$this->period1,true);
		$criteria->compare('period2',$this->period2,true);
		$criteria->compare('period3',$this->period3,true);
		$criteria->compare('amount1',$this->amount1,true);
		$criteria->compare('amount2',$this->amount2,true);
		$criteria->compare('amount3',$this->amount3,true);
		$criteria->compare('mc_amount1',$this->mc_amount1,true);
		$criteria->compare('mc_amount2',$this->mc_amount2,true);
		$criteria->compare('mc_amount3',$this->mc_amount3,true);
		$criteria->compare('currency',$this->currency,true);
		$criteria->compare('recurring',$this->recurring,true);
		$criteria->compare('reattempt',$this->reattempt,true);
		$criteria->compare('retry_at',$this->retry_at,true);
		$criteria->compare('recur_times',$this->recur_times,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('payment_txn_id',$this->payment_txn_id,true);
		$criteria->compare('subscriber_emailaddress',$this->subscriber_emailaddress,true);
		$criteria->compare('datecreation',$this->datecreation,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}