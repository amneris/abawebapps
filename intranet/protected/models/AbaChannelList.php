<?php

/**
 * This is the model class for table "aba_channel_list".
 *
 * The followings are the available columns in table 'aba_channel_list':
 * @property string $idPartner
 * @property string $name
 * @property string $idPartnerGroup
 * @property string $nameGroup
 */
class AbaChannelList extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'aba_channel_list';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idPartner, name', 'required'),
			array('idPartner, idPartnerGroup', 'length', 'max'=>3),
			array('name', 'length', 'max'=>60),
			array('nameGroup', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idPartner, name, idPartnerGroup, nameGroup', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idPartner' => 'Id Partner',
			'name' => 'Name',
			'idPartnerGroup' => 'Id Partner Group',
			'nameGroup' => 'Name Group',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idPartner',$this->idPartner,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('idPartnerGroup',$this->idPartnerGroup,true);
		$criteria->compare('nameGroup',$this->nameGroup,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AbaChannelList the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function saveChannel($xModel)
    {
        $rangeFrom='';
        $rangeTo='';
        $partnerGroupLabel='';
        //switch to choose the correct first free id depending about the group.
        switch ($xModel->idPartnerGroup)
        {
            case CHANNEL_ABA_G:
                $rangeFrom=1;
                $rangeTo=100000;
                $partnerGroupLabel= CHANNEL_ABA_W;
                break;
        }

        $desiredID = $this->getMaxChannelID($rangeFrom, $rangeTo);
        return $this->insertChannel($desiredID, $xModel, $partnerGroupLabel);
    }

    public function getMaxChannelID($rangeFrom, $rangeTo)
    {
        $preQuery = "SELECT max(idPartner) as maxId FROM (select a.idPartner from aba_b2c.aba_channel_list a where a.idPartner >= $rangeFrom && a.idPartner < $rangeTo) a";
        $preConnection = Yii::app()->db2;
        $preCommand = $preConnection->createCommand($preQuery);
        $dataReader = $preCommand->query();
        $rows = $dataReader->read();
        if($rows['maxId']==null)
            $maxId=$rangeFrom;
        else
        {
            $maxId = $rows['maxId'];
            $maxId++;
        }
        return $maxId;
    }

    public function insertChannel($desiredID, $xModel, $partnerGroupLabel)
    {
        $xModel->idPartner = $desiredID;
        $query = "INSERT INTO aba_b2c.aba_channel_list (idPartner, name, idPartnerGroup, nameGroup) VALUES ($xModel->idPartner, '$xModel->name', $xModel->idPartnerGroup, '$partnerGroupLabel');";
        $connection = Yii::app()->db2;
        $command = $connection->createCommand($query);
        $result = $command->query();
        if($result)
            return $xModel;
        else
            return false;
    }

    public function getAllChannel($default='')
    {
        $sql= "SELECT idPartner, name FROM aba_b2c.aba_channel_list;";
        $connection = Yii::app()->db2;
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();
        if($default<>'')
        {
            $toReturn['']=$default;
        }
        while(($row = $dataReader->read())!==false)
            $toReturn[$row['idPartner']]=$row['name'];

        return $toReturn;

    }

}
