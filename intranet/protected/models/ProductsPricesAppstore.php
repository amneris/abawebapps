<?php

class ProductsPricesAppstore extends CmAbaProductsPricesAppstore
{

    public function __construct()
    {
        parent::__construct();
        $this->connection = Yii::app()->getComponent(AbaActiveRecord::DB_DEFAULT);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'idProductTier' => 'ID',
          'idProductApp' => 'Product',
          'idPeriodPay' => 'Period',
//            'currencyAppStore' =>   'Currency',
          'tier' => 'Tier',
          'priceAppStore' => 'Price',
          'isBestOffer' => 'Is Best Offer',
          'idProduct' => 'Product ID',
          'idCountry' => 'Country',
          'ABALanguage' => 'ABALanguage',
          'isDefault' => 'Default',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

//        $sErrorCode = 'IF(t.successResponse = 0, IFNULL(t.errorCode, 555), t.errorCode)';

        $criteria->select = array(
          't.idProductTier',
          't.idProductApp',
          't.idPeriodPay',
//            't.currencyAppStore',
          't.tier',
          't.priceAppStore',
          't.isBestOffer',
          't.idProduct',
          't.idCountry',
          't.ABALanguage',
          't.isDefault',
//            new CDbExpression($sErrorCode . ' AS errorCode'),
//            new CDbExpression('COUNT(*) AS nSuccess'),
//            new CDbExpression("MIN(dateRequest) AS startDate"),
//            new CDbExpression("MAX(dateRequest) AS endDate"),
        );

        $criteria->compare('t.idProductTier', $this->idProductTier);
        $criteria->compare('t.idProductApp', $this->idProductApp, true); // LIKE
        $criteria->compare('t.idPeriodPay', $this->idPeriodPay);
//        $criteria->compare('t.currencyAppStore',    $this->currencyAppStore);
        $criteria->compare('t.tier', $this->tier);
        $criteria->compare('t.priceAppStore', $this->priceAppStore, true); // LIKE
        $criteria->compare('t.isBestOffer', $this->isBestOffer);
        $criteria->compare('t.idProduct', $this->idProduct);
        $criteria->compare('t.idCountry', $this->idCountry);
        $criteria->compare('t.ABALanguage', $this->ABALanguage);
        $criteria->compare('t.isDefault', $this->isDefault);

//        public function compare($column, $value, $partialMatch=false, $operator='AND', $escape=true)

//        if(trim($this->startDate) != '') {
//            $criteria->addCondition(" t.dateRequest >= '" . $this->startDate . "'");
//        }
//
//        if(trim($this->endDate)) {
//            $criteria->addCondition(" t.dateRequest <= '" . $this->endDate . "'");
//        }
//
//        $criteria->group = 't.nameService, t.successResponse, ' . $sErrorCode;

//        if(isset($_GET['LogWebService_sort']) AND trim($_GET['LogWebService_sort']) != '') {
//
//            $sPageOrder = $_GET['LogWebService_sort'];
//
//            if($sPageOrder == 'nSuccess.desc') {
//                $sPageOrder = 'nSuccess DESC, t.nameService, t.successResponse, t.errorCode';
//            }
//            elseif($sPageOrder == 'nSuccess.asc') {
//                $sPageOrder = 'nSuccess ASC, t.nameService, t.successResponse, t.errorCode';
//            }
//            elseif($sPageOrder == '') {
//                $criteria->order = $sPageOrder . 't.nameService, t.successResponse, t.errorCode';
//            }
//        }
//        else {
//            $criteria->order = 't.nameService, t.successResponse, t.errorCode';
//        }

        $oCActiveDataProvider = new CActiveDataProvider($this, array(
          'criteria' => $criteria,
          'pagination' => array(
            'pageSize' => 100,
          ),
        ));
        return $oCActiveDataProvider;
    }

}
