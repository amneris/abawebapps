<?php

/**
 * This is the model class for table "aba_b2c_summary.users_progress_byweek".
 *
 * The followings are the available columns in table 'aba_b2c_summary.users_progress_byweek':
 * @property string $autoId
 * @property string $userId
 * @property integer $unitId
 * @property string $dateCapture
 * @property integer $yearRef
 * @property integer $weekRef
 * @property integer $currentLevel
 * @property integer $totalProgress
 * @property integer $abaFilm
 * @property integer $speak
 * @property integer $write
 * @property integer $interpret
 * @property integer $videoClass
 * @property integer $exercises
 * @property integer $vocabulary
 * @property integer $assessment
 */
class AbaUsersProgressByWeek extends AbaActiveRecord
{

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return Yii::app()->params['dbCampusSummary'] . '.users_progress_byweek';
    }

    /** List of all fields available in table
     * @return string
     */
    public function mainFields()
    {
        return "*";
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'autoId' => 'Auto',
            'userId' => 'User',
            'unitId' => 'Unit',
            'dateCapture' => 'Date Capture',
            'yearRef' => 'Year Ref',
            'weekRef' => 'Week Ref',
            'currentLevel' => 'Current Level',
            'totalProgress' => 'Total Progress',
            'abaFilm' => 'Aba Film',
            'speak' => 'Speak',
            'write' => 'Write',
            'interpret' => 'Interpret',
            'videoClass' => 'Video Class',
            'exercises' => 'Exercises',
            'vocabulary' => 'Vocabulary',
            'assessment' => 'Assessment',
        );
    }

    /** Returns the last row inserted into table.
     * @return bool
     */
    public function getLastRow()
    {
        $sql = ' SELECT ' . $this->mainFields() .
            ' FROM ' . $this->tableName() . ' upw  ORDER BY upw.autoId DESC LIMIT 1; ';
        $dataReader = $this->querySQL($sql);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return true;
        }

        return false;
    }

    /** Collects from the temporary table UsersProgressLastWeek a certain quantity
     * and inserts them into this table.
     *
     * @param integer $batchRows
     *
     * @return bool|int|string
     */
    public function pickLastRows($batchRows=0)
    {
        $sqlLimit = 'LIMIT '.$batchRows;
        if($batchRows==0){
            $sqlLimit = '';
        }

        $sql = " INSERT INTO ".$this->tableName()." (`userId`, `unitId`, `dateCapture`,
                `yearRef`, `weekRef`, `currentLevel`, `totalProgress`,
                `abaFilm`, `speak`, `write`, `interpret`, `videoClass`, `exercises`, `vocabulary`, `assessment`)
                 SELECT upl.`userId`, upl.`unitId`, upl.`dateCapture`, upl.`yearRef`, upl.`weekRef`,
                      upl.`currentLevel`, upl.`totalProgress`, upl.`abaFilm`, upl.`speak`, upl.`write`,
                      upl.`interpret`, upl.`videoClass`, upl.`exercises`, upl.`vocabulary`, upl.`assessment`
                 FROM ".Yii::app()->params['dbCampusSummary'] .".users_progress_lastweek upl
                 ORDER BY upl.`userId`, upl.`unitId`, upl.`dateCapture` ".$sqlLimit;

        $aBrowserValues = array();
        $qRows = $this->executeSQL($sql, $aBrowserValues, true );
        if ( $qRows > 0) {
            return $qRows;
        }

        return false;
    }


}