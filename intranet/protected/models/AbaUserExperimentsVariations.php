<?php

class AbaUserExperimentsVariations extends CmAbaUserExperimentsVariations
{
    public function __construct($scenario = 'insert', $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaProductPrice the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'userExperimentVariationId' => 'ID',
            'userId' => 'User ID',
            'experimentVariationId' => 'Experiment variation',
            'status' => 'Status',
            'dateAdd' => 'Create date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('userExperimentVariationId', $this->userExperimentVariationId, true);
        $criteria->compare('userId', $this->userId, true);
        $criteria->compare('experimentVariationId', $this->experimentVariationId, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('dateAdd', $this->dateAdd);

        $criteria->order = 'userExperimentVariationId DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 100,
            ),
        ));
    }

}
