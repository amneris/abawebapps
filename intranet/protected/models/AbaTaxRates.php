<?php

/**
 * This is the model class for table "tax_rates".
 *
 * The followings are the available columns in table 'tax_rates':
 * @property integer $idTaxRate
 * @property integer $idCountry
 * @property float $taxRateValue
 * @property string $dateStartRate
 * @property string $dateEndRate
 */
class AbaTaxRates extends CmAbaTaxRates
{
    public function __construct($scenario = 'insert', $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'idTaxRate' => 'Id',
          'idCountry' => 'Country',
          'taxRateValue' => 'Tax Rate Value (%)',
          'dateStartRate' => 'Start time',
          'dateEndRate' => 'End time',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idTaxRate', $this->idTaxRate, true);
        $criteria->compare('idCountry', $this->idCountry, true);
        $criteria->compare('taxRateValue', $this->taxRateValue, true);
        $criteria->compare('dateStartRate', $this->dateStartRate, true);
        $criteria->compare('dateEndRate', $this->dateEndRate, true);

        $criteria->order = 'dateStartRate DESC, idCountry';

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
          'pagination' => array('pageSize' => 30)
        ));
    }

    /**
     * @param bool $runValidation
     * @param null $attributes
     *
     * @return bool
     */
    public function save($runValidation = true, $attributes = null)
    {
        if ($this->validateDateInterval()) {
            return parent::save($runValidation, $attributes);
        }
        return false;
    }

    /**
     * @return bool
     */
    public function validateDateInterval()
    {

        $sNow = HeDate::todaySQL(true);

        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " tr ";
        $sSql .= " WHERE tr.idCountry = " . $this->idCountry . " ";

        if (!$this->getIsNewRecord()) {
            $sSql .= " AND tr.idTaxRate <> " . $this->idTaxRate . " ";
        }

        if (trim($this->dateEndRate) == '') {

            $sSql .= " AND (";
            $sSql .= " (NULLIF(tr.dateEndRate, '') IS NULL)"; //  OR tr.dateEndRate == '0000-00-00 00:00:00'
            $sSql .= " OR ('" . $this->dateStartRate . "' BETWEEN tr.dateStartRate AND IFNULL(NULLIF(tr.dateEndRate, ''), '" . $sNow . "')) ";
            $sSql .= " ) ";

//echo $sSql; exit();

            $dataReader = $this->querySQL($sSql);
            if (($row = $dataReader->read()) !== false) {

                $this->addError('dateEndRate', 'Ya existe un registro.............');

                return false;
            }
        } else {

            $sSql .= " AND (";
            $sSql .= " ('" . $this->dateStartRate . "' BETWEEN tr.dateStartRate AND IFNULL(NULLIF(tr.dateEndRate, ''), '" . $sNow . "')) ";
            $sSql .= " OR ('" . $this->dateEndRate . "' BETWEEN tr.dateStartRate AND IFNULL(NULLIF(tr.dateEndRate, ''), '" . $sNow . "'))";
            $sSql .= " ) ";

//echo $sSql; exit();

            $dataReader = $this->querySQL($sSql);
            if (($row = $dataReader->read()) !== false) {

                $this->addError('dateEndRate', 'Ya existe un registro.............');

                return false;
            }
        }

        return true;
    }


}
