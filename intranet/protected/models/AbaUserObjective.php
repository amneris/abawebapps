<?php

/**
 * This is the model class for table "user_objective".
 *
 * The followings are the available columns in table 'user_objective':
 * @property string $userId
 * @property string $id
 * @property integer $travel
 * @property integer $estudy
 * @property integer $work
 * @property integer $level
 * @property string $selEngPrev
 * @property integer $engPrev
 * @property string $dedication
 * @property string $birthDate
 * @property string $gender
 * @property integer $others
 */
class AbaUserObjective extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AbaUserObjective the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_objective';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userId', 'required'),
			array('travel, estudy, work, level, engPrev, others', 'numerical', 'integerOnly'=>true),
			array('userId', 'length', 'max'=>4),
			array('selEngPrev, dedication, birthDate', 'length', 'max'=>50),
			array('gender', 'length', 'max'=>1),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('userId, id, travel, estudy, work, level, selEngPrev, engPrev, dedication, birthDate, gender, others', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'userId' => 'User',
			'id' => 'ID',
			'travel' => 'Travel',
			'estudy' => 'Estudy',
			'work' => 'Work',
			'level' => 'Level',
			'selEngPrev' => 'Sel Eng Prev',
			'engPrev' => 'Eng Prev',
			'dedication' => 'Dedication',
			'birthDate' => 'Birth Date',
			'gender' => 'Gender',
			'others' => 'Others',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('userId',$this->userId,true);
		$criteria->compare('id',$this->id,true);
		$criteria->compare('travel',$this->travel);
		$criteria->compare('estudy',$this->estudy);
		$criteria->compare('work',$this->work);
		$criteria->compare('level',$this->level);
		$criteria->compare('selEngPrev',$this->selEngPrev,true);
		$criteria->compare('engPrev',$this->engPrev);
		$criteria->compare('dedication',$this->dedication,true);
		$criteria->compare('birthDate',$this->birthDate,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('others',$this->others);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}