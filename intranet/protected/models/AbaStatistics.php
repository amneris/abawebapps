<?php

//callback function to order an array (matrix).
function cmpAmountDesc($amount1, $amount2)
{
    //if they are the same we return 0.
    if($amount1["totalAmount"] == $amount2["totalAmount"])
        return 0;
    //if amount1 > 2 we return 1 otherwise -1
    if($amount1["totalAmount"] < $amount2["totalAmount"])
        return 1;
    return -1;
}

class AbaStatistics extends CActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'payments';
    }

    public function attributeLabels()
    {
        return array(

        );
    }

    public function totalSalesDashboardCount($startDate, $endDate)
    {
        $amountTotal = array();

        //total sales USD
        $sql="  SELECT SUM(p.amountPrice) as totalUSD, SUM(p.amountPriceWithoutTax) as totalUSDnoVat, count(*) as countUSD
                FROM ".Yii::app()->params["mainDbName"].".payments p
                where p.status =30 and idPartner<>300006 and p.paySuppExtId<>".PAY_SUPPLIER_B2B." and p.amountPrice > 0 and p.currencyTrans='USD'
                and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59';";

        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();

        if(($row = $result->read())!==false)
        {
            $amountTotal['totalUSD']=$row['totalUSD'];
            $amountTotal['totalUSDnoVat']=$row['totalUSDnoVat'];
            $amountTotal['countUSD']=$row['countUSD'];
        }

        //total renovations USD
        $sql="  SELECT SUM(p.amountPrice) as totalRenovationUSD, SUM(p.amountPriceWithoutTax) as totalRenovationUSDnoVat, count(*) as countRenovationUSD
                FROM ".Yii::app()->params["mainDbName"].".payments p
                where p.status =30 and idPartner=300006 and p.paySuppExtId<>".PAY_SUPPLIER_B2B." and p.amountPrice > 0 and p.currencyTrans='USD'
                and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59';";

        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();

        if(($row = $result->read())!==false)
        {
            $amountTotal['totalRenovationUSD']=$row['totalRenovationUSD'];
            $amountTotal['totalRenovationUSDnoVat']=$row['totalRenovationUSDnoVat'];
            $amountTotal['countRenovationUSD']=$row['countRenovationUSD'];
        }

        //total refund USD
        $sql="  SELECT SUM(p.amountPrice) as totalRefundUSD, SUM(p.amountPriceWithoutTax) as totalRefundUSDnoVat, count(*) as contRefundUSD
                FROM ".Yii::app()->params["mainDbName"].".payments p
                where p.status =40  and p.amountPrice > 0 and p.currencyTrans='USD' and p.paySuppExtId<>".PAY_SUPPLIER_B2B."
                and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59';";

        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();

        if(($row = $result->read())!==false)
        {
            $amountTotal['totalRefundUSD']=$row['totalRefundUSD'];
            $amountTotal['totalRefundUSDnoVat']=$row['totalRefundUSDnoVat'];
            $amountTotal['contRefundUSD']=$row['contRefundUSD'];
        }

        //total failed USD
        $sql="  SELECT SUM(p.amountPrice) as totalFailedUSD, SUM(p.amountPriceWithoutTax) as totalFailedUSDnoVat, count(*) as countFailedUSD
                FROM ".Yii::app()->params["mainDbName"].".payments p
                where p.status =10 and paySuppExtId <> 3 and p.paySuppExtId<>".PAY_SUPPLIER_B2B." and p.amountPrice > 0 and p.currencyTrans='USD'
                and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59';";

        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();

        if(($row = $result->read())!==false)
        {
            $amountTotal['totalFailedUSD']=$row['totalFailedUSD'];
            $amountTotal['totalFailedUSDnoVat']=$row['totalFailedUSDnoVat'];
            $amountTotal['countFailedUSD']=$row['countFailedUSD'];
        }

        //total canceled USD
        $sql="  SELECT SUM(p.amountPrice) as totalCanceledUSD, SUM(p.amountPriceWithoutTax) as totalCanceledUSDnoVat, count(*) as countCanceledUSD
                FROM ".Yii::app()->params["mainDbName"].".payments p
                where p.status =50 and paySuppExtId <> 3 and p.paySuppExtId<>".PAY_SUPPLIER_B2B." and p.amountPrice > 0 and p.currencyTrans='USD'
                and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59';";

        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();

        if(($row = $result->read())!==false)
        {
            $amountTotal['totalCanceledUSD']=$row['totalCanceledUSD'];
            $amountTotal['totalCanceledUSDnoVat']=$row['totalCanceledUSDnoVat'];
            $amountTotal['countCanceledUSD']=$row['countCanceledUSD'];
        }

        //total sales BRL
        $sql="  SELECT SUM(p.amountPrice) as totalBRL, SUM(p.amountPriceWithoutTax) as totalBRLnoVat, count(*) as countBRL
                FROM ".Yii::app()->params["mainDbName"].".payments p
                where p.status =30 and idPartner<>300006 and p.paySuppExtId<>".PAY_SUPPLIER_B2B." and p.amountPrice > 0 and p.currencyTrans='BRL'
                and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59';";

        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();

        if(($row = $result->read())!==false)
        {
            $amountTotal['totalBRL']=$row['totalBRL'];
            $amountTotal['totalBRLnoVat']=$row['totalBRLnoVat'];
            $amountTotal['countBRL']=$row['countBRL'];
        }

        //total renovations BRL
        $sql="  SELECT SUM(p.amountPrice) as totalRenovationBRL, SUM(p.amountPriceWithoutTax) as totalRenovationBRLnoVat, count(*) as countRenovationBRL
                FROM ".Yii::app()->params["mainDbName"].".payments p
                where p.status =30 and idPartner=300006 and p.paySuppExtId<>".PAY_SUPPLIER_B2B." and p.amountPrice > 0 and p.currencyTrans='BRL'
                and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59';";

        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();

        if(($row = $result->read())!==false)
        {
            $amountTotal['totalRenovationBRL']=$row['totalRenovationBRL'];
            $amountTotal['totalRenovationBRLnoVat']=$row['totalRenovationBRLnoVat'];
            $amountTotal['countRenovationBRL']=$row['countRenovationBRL'];
        }

        //total refund BRL
        $sql="  SELECT SUM(p.amountPrice) as totalRefundBRL, SUM(p.amountPriceWithoutTax) as totalRefundBRLnoVat, count(*) as contRefundBRL
                FROM ".Yii::app()->params["mainDbName"].".payments p
                where p.status =40  and p.amountPrice > 0 and p.currencyTrans='BRL' and p.paySuppExtId<>".PAY_SUPPLIER_B2B."
                and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59';";

        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();

        if(($row = $result->read())!==false)
        {
            $amountTotal['totalRefundBRL']=$row['totalRefundBRL'];
            $amountTotal['totalRefundBRLnoVat']=$row['totalRefundBRLnoVat'];
            $amountTotal['contRefundBRL']=$row['contRefundBRL'];
        }

        //total failed BRL
        $sql="  SELECT SUM(p.amountPrice) as totalFailedBRL, SUM(p.amountPriceWithoutTax) as totalFailedBRLnoVat, count(*) as countFailedBRL
                FROM ".Yii::app()->params["mainDbName"].".payments p
                where p.status =10 and paySuppExtId <> 3 and p.amountPrice > 0 and p.currencyTrans='BRL' and p.paySuppExtId<>".PAY_SUPPLIER_B2B."
                and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59';";

        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();

        if(($row = $result->read())!==false)
        {
            $amountTotal['totalFailedBRL']=$row['totalFailedBRL'];
            $amountTotal['totalFailedBRLnoVat']=$row['totalFailedBRLnoVat'];
            $amountTotal['countFailedBRL']=$row['countFailedBRL'];
        }

        //total canceled BRL
        $sql="  SELECT SUM(p.amountPrice) as totalCanceledBRL, SUM(p.amountPriceWithoutTax) as totalCanceledBRLnoVat, count(*) as countCanceledBRL
                FROM ".Yii::app()->params["mainDbName"].".payments p
                where p.status =50 and paySuppExtId <> 3 and p.amountPrice > 0 and p.currencyTrans='BRL' and p.paySuppExtId<>".PAY_SUPPLIER_B2B."
                and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59';";

        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();

        if(($row = $result->read())!==false)
        {
            $amountTotal['totalCanceledBRL']=$row['totalCanceledBRL'];
            $amountTotal['totalCanceledBRLnoVat']=$row['totalCanceledBRLnoVat'];
            $amountTotal['countCanceledBRL']=$row['countCanceledBRL'];
        }

        //total sales EUR
        $sql="  SELECT SUM(p.amountPrice) as totalEUR, SUM(p.amountPriceWithoutTax) as totalEURnoVat, count(*) as countEUR
                FROM ".Yii::app()->params["mainDbName"].".payments p
                where p.status =30 and idPartner<>300006 and p.paySuppExtId<>".PAY_SUPPLIER_B2B." and p.amountPrice > 0 and p.currencyTrans='EUR'
                and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59';";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();
        if(($row = $result->read())!==false)
        {
            $amountTotal['totalEUR']=$row['totalEUR'];
            $amountTotal['totalEURnoVat']=$row['totalEURnoVat'];
            $amountTotal['countEUR']=$row['countEUR'];
        }

        //total renovations EUR
        $sql="  SELECT SUM(p.amountPrice) as totalRenovationEUR, SUM(p.amountPriceWithoutTax) as totalRenovationEURnoVat, count(*) as countRenovationEUR
                FROM ".Yii::app()->params["mainDbName"].".payments p
                where p.status =30 and idPartner=300006 and p.paySuppExtId<>".PAY_SUPPLIER_B2B." and p.amountPrice > 0 and p.currencyTrans='EUR'
                and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59';";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();
        if(($row = $result->read())!==false)
        {
            $amountTotal['totalRenovationEUR']=$row['totalRenovationEUR'];
            $amountTotal['totalRenovationEURnoVat']=$row['totalRenovationEURnoVat'];
            $amountTotal['countRenovationEUR']=$row['countRenovationEUR'];
        }

        //total refund EUR
        $sql="  SELECT SUM(p.amountPrice) as totalRefundEUR, SUM(p.amountPriceWithoutTax) as totalRefundEURnoVat, count(*) as contRefundEUR
                FROM ".Yii::app()->params["mainDbName"].".payments p
                where p.status =40  and p.amountPrice > 0 and p.currencyTrans='EUR' and p.paySuppExtId<>".PAY_SUPPLIER_B2B."
                and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59';";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();
        if(($row = $result->read())!==false)
        {
            $amountTotal['totalRefundEUR']=$row['totalRefundEUR'];
            $amountTotal['totalRefundEURnoVat']=$row['totalRefundEURnoVat'];
            $amountTotal['contRefundEUR']=$row['contRefundEUR'];
        }

        //total failed EUR
        $sql="  SELECT SUM(p.amountPrice) as totalFailedEUR, SUM(p.amountPriceWithoutTax) as totalFailedEURnoVat, count(*) as countFailedEUR
                FROM ".Yii::app()->params["mainDbName"].".payments p
                where p.status =10 and paySuppExtId <> 3 and p.amountPrice > 0 and p.currencyTrans='EUR' and p.paySuppExtId<>".PAY_SUPPLIER_B2B."
                and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59';";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();
        if(($row = $result->read())!==false)
        {
            $amountTotal['totalFailedEUR']=$row['totalFailedEUR'];
            $amountTotal['totalFailedEURnoVat']=$row['totalFailedEURnoVat'];
            $amountTotal['countFailedEUR']=$row['countFailedEUR'];
        }

        //total canceled EUR
        $sql="  SELECT SUM(p.amountPrice) as totalCanceledEUR, SUM(p.amountPriceWithoutTax) as totalCanceledEURnoVat, count(*) as countCanceledEUR
                FROM ".Yii::app()->params["mainDbName"].".payments p
                where p.status =50 and paySuppExtId <> 3 and p.amountPrice > 0 and p.currencyTrans='EUR' and p.paySuppExtId<>".PAY_SUPPLIER_B2B."
                and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59';";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();
        if(($row = $result->read())!==false)
        {
            $amountTotal['totalCanceledEUR']=$row['totalCanceledEUR'];
            $amountTotal['totalCanceledEURnoVat']=$row['totalCanceledEURnoVat'];
            $amountTotal['countCanceledEUR']=$row['countCanceledEUR'];
        }


        //total sales MXN
        $sql="  SELECT SUM(p.amountPrice) as totalMXN, SUM(p.amountPriceWithoutTax) as totalMXNnoVat, count(*) as countMXN
                FROM ".Yii::app()->params["mainDbName"].".payments p
                where p.status =30 and idPartner<>300006 and p.paySuppExtId<>".PAY_SUPPLIER_B2B." and p.amountPrice > 0 and p.currencyTrans='MXN'
                and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59';";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();
        if(($row = $result->read())!==false)
        {
            $amountTotal['totalMXN']=$row['totalMXN'];
            $amountTotal['totalMXNnoVat']=$row['totalMXNnoVat'];
            $amountTotal['countMXN']=$row['countMXN'];
        }

        //total renovations MXN
        $sql="  SELECT SUM(p.amountPrice) as totalRenovationMXN, SUM(p.amountPriceWithoutTax) as totalRenovationMXNnoVat, count(*) as countRenovationMXN
                FROM ".Yii::app()->params["mainDbName"].".payments p
                where p.status =30 and idPartner=300006 and p.paySuppExtId<>".PAY_SUPPLIER_B2B." and p.amountPrice > 0 and p.currencyTrans='MXN'
                and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59';";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();
        if(($row = $result->read())!==false)
        {
            $amountTotal['totalRenovationMXN']=$row['totalRenovationMXN'];
            $amountTotal['totalRenovationMXNnoVat']=$row['totalRenovationMXNnoVat'];
            $amountTotal['countRenovationMXN']=$row['countRenovationMXN'];
        }

        //total refund MXN
        $sql="  SELECT SUM(p.amountPrice) as totalRefundMXN, SUM(p.amountPriceWithoutTax) as totalRefundMXNnoVat, count(*) as contRefundMXN
                FROM ".Yii::app()->params["mainDbName"].".payments p
                where p.status =40  and p.amountPrice > 0 and p.currencyTrans='MXN' and p.paySuppExtId<>".PAY_SUPPLIER_B2B."
                and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59';";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();
        if(($row = $result->read())!==false)
        {
            $amountTotal['totalRefundMXN']=$row['totalRefundMXN'];
            $amountTotal['totalRefundMXNnoVat']=$row['totalRefundMXNnoVat'];
            $amountTotal['contRefundMXN']=$row['contRefundMXN'];
        }

        //total failed MXN
        $sql="  SELECT SUM(p.amountPrice) as totalFailedMXN, SUM(p.amountPriceWithoutTax) as totalFailedMXNnoVat, count(*) as countFailedMXN
                FROM ".Yii::app()->params["mainDbName"].".payments p
                where p.status =10 and paySuppExtId <> 3 and p.amountPrice > 0 and p.currencyTrans='MXN' and p.paySuppExtId<>".PAY_SUPPLIER_B2B."
                and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59';";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();
        if(($row = $result->read())!==false)
        {
            $amountTotal['totalFailedMXN']=$row['totalFailedMXN'];
            $amountTotal['totalFailedMXNnoVat']=$row['totalFailedMXNnoVat'];
            $amountTotal['countFailedMXN']=$row['countFailedMXN'];
        }

        //total canceled MXN
        $sql="  SELECT SUM(p.amountPrice) as totalCanceledMXN, SUM(p.amountPriceWithoutTax) as totalCanceledMXNnoVat, count(*) as countCanceledMXN
                FROM ".Yii::app()->params["mainDbName"].".payments p
                where p.status =50 and paySuppExtId <> 3 and p.amountPrice > 0 and p.currencyTrans='MXN' and p.paySuppExtId<>".PAY_SUPPLIER_B2B."
                and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59';";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();
        if(($row = $result->read())!==false)
        {
            $amountTotal['totalCanceledMXN']=$row['totalCanceledMXN'];
            $amountTotal['totalCanceledMXNnoVat']=$row['totalCanceledMXNnoVat'];
            $amountTotal['countCanceledMXN']=$row['countCanceledMXN'];
        }

        return $amountTotal;
    }

    public function totalSalesDashboardCountByCountry($startDate, $endDate)
    {
        $dollar = new AbaCurrency();
        $dollar->getCurrencyById('USD');
        $dollarToday = $dollar->outEur;

        $real = new AbaCurrency();
        $real->getCurrencyById('BRL');
        $realToday = $real->outEur;

        $peso = new AbaCurrency();
        $peso->getCurrencyById('MXN');
        $pesoToday = $peso->outEur;

        $result = array();

        //total sales USD group by country
        $sql="  SELECT c.name as Country, count(*) as countUSD, SUM(p.amountPrice) as totalUSD, SUM(p.amountPriceWithoutTax) as totalUSDnoVat, c.iso as iso
                FROM ".Yii::app()->params["mainDbName"].".payments p
                inner join ".Yii::app()->params["mainDbName"].".country c on p.idCountry = c.id
                where p.status =30 and idPartner<>300006 and p.amountPrice > 0
                and p.currencyTrans='USD' and p.paySuppExtId<>".PAY_SUPPLIER_B2B." and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59' group by idCountry order by c.name asc;";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result['saleUSD'] = $command->query();

        //total renovations USD group by country
        $sql="  SELECT c.name as Country, count(*) as countRenovationUSD, SUM(p.amountPrice) as totalRenovationUSD, SUM(p.amountPriceWithoutTax) as totalRenovationUSDnoVat, c.iso as iso
                FROM ".Yii::app()->params["mainDbName"].".payments p
                inner join ".Yii::app()->params["mainDbName"].".country c on p.idCountry = c.id
                where p.status =30 and idPartner=300006 and p.amountPrice > 0
                and p.currencyTrans='USD' and p.paySuppExtId<>".PAY_SUPPLIER_B2B." and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59' group by idCountry order by c.name asc;";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result['renovationUSD'] = $command->query();

        //total sales BRL group by country
        $sql="  SELECT c.name as Country, count(*) as countBRL, SUM(p.amountPrice) as totalBRL, SUM(p.amountPriceWithoutTax) as totalBRLnoVat, c.iso as iso
                FROM ".Yii::app()->params["mainDbName"].".payments p
                inner join ".Yii::app()->params["mainDbName"].".country c on p.idCountry = c.id
                where p.status =30 and idPartner<>300006 and p.amountPrice > 0
                and p.currencyTrans='BRL' and p.paySuppExtId<>".PAY_SUPPLIER_B2B." and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59' group by idCountry order by c.name asc;";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result['saleBRL'] = $command->query();

        //total renovations BRL group by country
        $sql="  SELECT c.name as Country, count(*) as countRenovationBRL, SUM(p.amountPrice) as totalRenovationBRL, SUM(p.amountPriceWithoutTax) as totalRenovationBRLnoVat, c.iso as iso
                FROM ".Yii::app()->params["mainDbName"].".payments p
                inner join ".Yii::app()->params["mainDbName"].".country c on p.idCountry = c.id
                where p.status =30 and idPartner=300006 and p.amountPrice > 0
                and p.currencyTrans='BRL' and p.paySuppExtId<>".PAY_SUPPLIER_B2B." and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59' group by idCountry order by c.name asc;";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result['renovationBRL'] = $command->query();

        //total sales EUR group by country
        $sql="  SELECT c.name as Country, count(*) as countEUR, SUM(p.amountPrice) as totalEUR, SUM(p.amountPriceWithoutTax) as totalEURnoVat, c.iso as iso
                FROM ".Yii::app()->params["mainDbName"].".payments p
                inner join ".Yii::app()->params["mainDbName"].".country c on p.idCountry = c.id
                where p.status =30 and idPartner<>300006 and p.amountPrice > 0
                and p.currencyTrans='EUR' and p.paySuppExtId<>".PAY_SUPPLIER_B2B." and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59' group by idCountry order by c.name asc;";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result['salesEUR'] = $command->query();

        //total renovations EUR group by country
        $sql="  SELECT c.name as Country, count(*) as countRenovationEUR, SUM(p.amountPrice) as totalRenovationEUR, SUM(p.amountPriceWithoutTax) as totalRenovationEURnoVat, c.iso as iso
                FROM ".Yii::app()->params["mainDbName"].".payments p
                inner join ".Yii::app()->params["mainDbName"].".country c on p.idCountry = c.id
                where p.status =30 and idPartner=300006 and p.amountPrice > 0
                and p.currencyTrans='EUR' and p.paySuppExtId<>".PAY_SUPPLIER_B2B." and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59' group by idCountry order by c.name asc;";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result['renovationEUR'] = $command->query();


        //total sales MXN group by country
        $sql="  SELECT c.name as Country, count(*) as countMXN, SUM(p.amountPrice) as totalMXN, SUM(p.amountPriceWithoutTax) as totalMXNnoVat, c.iso as iso
                FROM ".Yii::app()->params["mainDbName"].".payments p
                inner join ".Yii::app()->params["mainDbName"].".country c on p.idCountry = c.id
                where p.status =30 and idPartner<>300006 and p.amountPrice > 0
                and p.currencyTrans='MXN' and p.paySuppExtId<>".PAY_SUPPLIER_B2B." and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59' group by idCountry order by c.name asc;";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result['saleMXN'] = $command->query();

        //total renovations MXN group by country
        $sql="  SELECT c.name as Country, count(*) as countRenovationMXN, SUM(p.amountPrice) as totalRenovationMXN, SUM(p.amountPriceWithoutTax) as totalRenovationMXNnoVat, c.iso as iso
                FROM ".Yii::app()->params["mainDbName"].".payments p
                inner join ".Yii::app()->params["mainDbName"].".country c on p.idCountry = c.id
                where p.status =30 and idPartner=300006 and p.amountPrice > 0
                and p.currencyTrans='MXN' and p.paySuppExtId<>".PAY_SUPPLIER_B2B." and p.dateEndTransaction BETWEEN '$startDate' AND '$endDate 23:59:59' group by idCountry order by c.name asc;";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result['renovationMXN'] = $command->query();


        //the next code it is just to put all together in the array.
        $country = array();
        $toReturn = array();
        $i=0;

        foreach($result['saleUSD'] as $value)
        {
            $country['image'] = "/images/flagsIso/".$value['iso'].".png";
            $country['name'] = $value['Country'];
            $country['saleQuantity'] = $value['countUSD'];
            $country['saleAmount'] = $value['totalUSD'] * $dollarToday;
            $country['saleAmountnoVat'] = $value['totalUSDnoVat'] * $dollarToday;
            $country['renovationQuantity'] = 0;
            $country['renovationAmount'] = 0;
            $country['renovationAmountnoVat'] = 0;
            $toReturn[$i] = $country;
            $i++;
        }

        foreach($result['renovationUSD'] as $value)
        {
            $band = false;

            for($x=0; $x<$i; $x++)
            {
                if($value['Country'] == $toReturn[$x]['name'])
                {
                    $toReturn[$x]['renovationQuantity'] = $value['countRenovationUSD'];
                    $toReturn[$x]['renovationAmount'] = $value['totalRenovationUSD'] * $dollarToday;
                    $toReturn[$x]['renovationAmountnoVat'] = $value['totalRenovationUSDnoVat'] * $dollarToday;
                    $band = true;
                    //find the way to scape the for loop now.. there is no need to keep searching.
                }
            }
            //we didn't find any match for the country.. we add it.
            if(!$band)
            {
                $country['image'] = "/images/flagsIso/".$value['iso'].".png";
                $country['name'] = $value['Country'];
                $country['saleQuantity'] = 0;
                $country['saleAmount'] = 0;
                $country['saleAmountnoVat'] = 0;
                $country['renovationQuantity'] = $value['countRenovationUSD'];
                $country['renovationAmount'] = $value['totalRenovationUSD'] * $dollarToday;
                $country['renovationAmountnoVat'] = $value['totalRenovationUSDnoVat'] * $dollarToday;
                $toReturn[$i] = $country;
                $i++;
            }
        }

        foreach($result['saleBRL'] as $value)
        {
            $band = false;

            for($x=0; $x<$i; $x++)
            {
                if($value['Country'] == $toReturn[$x]['name'])
                {
                    $toReturn[$x]['saleQuantity'] += $value['countBRL'];
                    $toReturn[$x]['saleAmount'] += $value['totalBRL'] * $realToday;
                    $toReturn[$x]['saleAmountnoVat'] += $value['totalBRLnoVat'] * $realToday;
                    $band = true;
                    //find the way to scape the for loop now.. there is no need to keep searching.
                }
            }
            //we didn't find any match for the country.. we add it.
            if(!$band)
            {
                $country['image'] = "/images/flagsIso/".$value['iso'].".png";
                $country['name'] = $value['Country'];
                $country['saleQuantity'] = $value['countBRL'];
                $country['saleAmount'] = $value['totalBRL'] * $realToday;
                $country['saleAmountnoVat'] = $value['totalBRLnoVat'] * $realToday;
                $country['renovationQuantity'] = 0;
                $country['renovationAmount'] = 0;
                $country['renovationAmountnoVat'] = 0;
                $toReturn[$i] = $country;
                $i++;
            }
        }

        foreach($result['renovationBRL'] as $value)
        {
            $band = false;

            for($x=0; $x<$i; $x++)
            {
                if($value['Country'] == $toReturn[$x]['name'])
                {
                    $toReturn[$x]['renovationQuantity'] += $value['countRenovationBRL'];
                    $toReturn[$x]['renovationAmount'] += $value['totalRenovationBRL'] * $realToday;
                    $toReturn[$x]['renovationAmountnoVat'] += $value['totalRenovationBRLnoVat'] * $realToday;
                    $band = true;
                    //find the way to scape the for loop now.. there is no need to keep searching.
                }
            }
            //we didn't find any match for the country.. we add it.
            if(!$band)
            {
                $country['image'] = "/images/flagsIso/".$value['iso'].".png";
                $country['name'] = $value['Country'];
                $country['saleQuantity'] = 0;
                $country['saleAmount'] = 0;
                $country['saleAmountnoVat'] = 0;
                $country['renovationQuantity'] = $value['countRenovationBRL'];
                $country['renovationAmount'] = $value['totalRenovationBRL'] * $realToday;
                $country['renovationAmountnoVat'] = $value['totalRenovationBRLnoVat'] * $realToday;
                $toReturn[$i] = $country;
                $i++;
            }
        }

        foreach($result['salesEUR'] as $value)
        {
            $band = false;

            for($x=0; $x<$i; $x++)
            {
                if($value['Country'] == $toReturn[$x]['name'])
                {
                    $toReturn[$x]['saleQuantity'] += $value['countEUR'];
                    $toReturn[$x]['saleAmount'] += $value['totalEUR'];
                    $toReturn[$x]['saleAmountnoVat'] += $value['totalEURnoVat'];
                    $band = true;
                    //find the way to scape the for loop now.. there is no need to keep searching.
                }
            }
            //we didn't find any match for the country.. we add it.
            if(!$band)
            {
                $country['image'] = "/images/flagsIso/".$value['iso'].".png";
                $country['name'] = $value['Country'];
                $country['saleQuantity'] = $value['countEUR'];
                $country['saleAmount'] = $value['totalEUR'];
                $country['saleAmountnoVat'] = $value['totalEURnoVat'];
                $country['renovationQuantity'] = 0;
                $country['renovationAmount'] = 0;
                $country['renovationAmountnoVat'] = 0;
                $toReturn[$i] = $country;
                $i++;
            }
        }

        foreach($result['renovationEUR'] as $value)
        {
            $band = false;

            for($x=0; $x<$i; $x++)
            {
                if($value['Country'] == $toReturn[$x]['name'])
                {
                    $toReturn[$x]['renovationQuantity'] += $value['countRenovationEUR'];
                    $toReturn[$x]['renovationAmount'] += $value['totalRenovationEUR'];
                    $toReturn[$x]['renovationAmountnoVat'] += $value['totalRenovationEURnoVat'];
                    $band = true;
                    //find the way to scape the for loop now.. there is no need to keep searching.
                }
            }
            //we didn't find any match for the country.. we add it.
            if(!$band)
            {
                $country['image'] = "/images/flagsIso/".$value['iso'].".png";
                $country['name'] = $value['Country'];
                $country['saleQuantity'] = 0;
                $country['saleAmount'] = 0;
                $country['saleAmountnoVat'] = 0;
                $country['renovationQuantity'] = $value['countRenovationEUR'];
                $country['renovationAmount'] = $value['totalRenovationEUR'];
                $country['renovationAmountnoVat'] = $value['totalRenovationEURnoVat'];
                $toReturn[$i] = $country;
                $i++;
            }
        }


        foreach($result['saleMXN'] as $value)
        {
            $band = false;

            for($x=0; $x<$i; $x++)
            {
                if($value['Country'] == $toReturn[$x]['name'])
                {
                    $toReturn[$x]['saleQuantity'] += $value['countMXN'];
                    $toReturn[$x]['saleAmount'] += $value['totalMXN'] * $pesoToday;
                    $toReturn[$x]['saleAmountnoVat'] += $value['totalMXNnoVat'] * $pesoToday;
                    $band = true;
                    //find the way to scape the for loop now.. there is no need to keep searching.
                }
            }
            //we didn't find any match for the country.. we add it.
            if(!$band)
            {
                $country['image'] = "/images/flagsIso/".$value['iso'].".png";
                $country['name'] = $value['Country'];
                $country['saleQuantity'] = $value['countMXN'];
                $country['saleAmount'] = $value['totalMXN'] * $pesoToday;
                $country['saleAmountnoVat'] = $value['totalMXNnoVat'] * $pesoToday;
                $country['renovationQuantity'] = 0;
                $country['renovationAmount'] = 0;
                $country['renovationAmountnoVat'] = 0;
                $toReturn[$i] = $country;
                $i++;
            }
        }

        foreach($result['renovationMXN'] as $value)
        {
            $band = false;

            for($x=0; $x<$i; $x++)
            {
                if($value['Country'] == $toReturn[$x]['name'])
                {
                    $toReturn[$x]['renovationQuantity'] += $value['countRenovationMXN'];
                    $toReturn[$x]['renovationAmount'] += $value['totalRenovationMXN'] * $pesoToday;
                    $toReturn[$x]['renovationAmountnoVat'] += $value['totalRenovationMXNnoVat'] * $pesoToday;
                    $band = true;
                    //find the way to scape the for loop now.. there is no need to keep searching.
                }
            }
            //we didn't find any match for the country.. we add it.
            if(!$band)
            {
                $country['image'] = "/images/flagsIso/".$value['iso'].".png";
                $country['name'] = $value['Country'];
                $country['saleQuantity'] = 0;
                $country['saleAmount'] = 0;
                $country['saleAmountnoVat'] = 0;
                $country['renovationQuantity'] = $value['countRenovationMXN'];
                $country['renovationAmount'] = $value['totalRenovationMXN'] * $pesoToday;
                $country['renovationAmountnoVat'] = $value['totalRenovationMXNnoVat'] * $pesoToday;
                $toReturn[$i] = $country;
                $i++;
            }
        }


        //calculate sub totals.
        $x=0;
        foreach($toReturn as $value)
        {
            $toReturn[$x]['totalMovement'] = $value['saleQuantity'] + $value['renovationQuantity'];
            $toReturn[$x]['totalAmount'] = $value['saleAmount'] + $value['renovationAmount'];
            $toReturn[$x]['totalAmountnoVat'] = $value['saleAmountnoVat'] + $value['renovationAmountnoVat'];
            $x++;
        }

        usort($toReturn,"cmpAmountDesc");

        return $toReturn;
    }


    public function registrationDashboard($startDate, $endDate, $channel)
    {
        $partner = array();
        $x=0;
        $id = $channel['idPartnerGroup'];
        $sql="  SELECT p.name as partnerName, count(*) as total
                FROM ".Yii::app()->params["mainDbName"].".user u
                inner join  ".Yii::app()->params["mainDbName"].".aba_partners_list p on u.idPartnerSource = p.idPartner
                where p.idPartnerGroup = $id and u.entryDate between '$startDate' and '$endDate 23:59:59' group by u.idPartnerSource order by total desc;";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result = $command->query();
        while(($row = $result->read())!==false)
        {
            if($row['partnerName']!='' && $row['total']!='')
            {
                $partner[$x]['partnerName'] = $row['partnerName'];
                $partner[$x]['total'] = $row['total'];
                $x++;
            }
        }
        return $partner;
    }

    public function getChannels($flashSales)
    {
        //get all the channels.
        $i=0;
        $channels = array();

        if($flashSales)
            $sql="SELECT nameGroup, idPartnerGroup FROM ".Yii::app()->params["mainDbName"].".aba_partners_list group by idPartnerGroup;";
        else
            $sql="SELECT nameGroup, idPartnerGroup FROM ".Yii::app()->params["mainDbName"].".aba_partners_list where idPartnerGroup <> 1 group by idPartnerGroup;";

        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result=$command->query();
        foreach($result as $row)
        {
            $channels[$i]['nameGroup']=$row['nameGroup'];
            $channels[$i]['idPartnerGroup']=$row['idPartnerGroup'];
            $i++;
        }
        return $channels;
    }

    public function totalSalesDashboardCountByPartner($startDate, $endDate, $entryDate)
    {
        $dollar = new AbaCurrency();
        $dollar->getCurrencyById('USD');
        $dollarToday = $dollar->outEur;

        $real = new AbaCurrency();
        $real->getCurrencyById('BRL');
        $realToday = $real->outEur;

        $peso = new AbaCurrency();
        $peso->getCurrencyById('MXN');
        $pesoToday = $peso->outEur;

        $result = array();

        //total sales USD group by partner
        $sql="  SELECT c.name as partnerName, c.nameGroup AS partnerGroup, count(*) AS countUSD, SUM(p.amountPrice) AS totalUSD, SUM(p.amountPriceWithoutTax) as totalUSDnoVat
                FROM ".Yii::app()->params["mainDbName"].".payments p
                INNER JOIN ".Yii::app()->params["mainDbName"].".user u ON p.userId = u.id
                INNER JOIN ".Yii::app()->params["mainDbName"].".aba_partners_list c ON u.idPartnerSource = c.idPartner
                WHERE p.status =30 AND p.idPartner<>300006 AND p.amountPrice > 0 AND p.currencyTrans='USD' and p.paySuppExtId<>".PAY_SUPPLIER_B2B." AND";
                if($entryDate) $sql .= " u.entryDate "; else $sql .= " p.dateEndTransaction ";
                $sql .= "BETWEEN '$startDate' AND '$endDate 23:59:59' GROUP BY c.idPartner ORDER BY c.name ASC;";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result['saleUSD'] = $command->query();

        //total renovations USD group by partner
        $sql="  SELECT c.name as partnerName, c.nameGroup AS partnerGroup, COUNT(*) AS countRenovationUSD, SUM(p.amountPrice) AS totalRenovationUSD, SUM(p.amountPriceWithoutTax) as totalRenovationUSDnoVat
                FROM ".Yii::app()->params["mainDbName"].".payments p
                INNER JOIN ".Yii::app()->params["mainDbName"].".user u ON p.userId = u.id
                INNER JOIN ".Yii::app()->params["mainDbName"].".aba_partners_list c on u.idPartnerSource = c.idPartner
                WHERE p.status =30 AND p.idPartner=300006 AND p.amountPrice > 0 AND p.currencyTrans='USD' and p.paySuppExtId<>".PAY_SUPPLIER_B2B." AND";
                if($entryDate) $sql .= " u.entryDate "; else $sql .= " p.dateEndTransaction ";
                $sql .= "BETWEEN '$startDate' AND '$endDate 23:59:59' GROUP BY c.idPartner ORDER BY c.name ASC;";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result['renovationUSD'] = $command->query();

        //total sales BRL group by partner
        $sql="  SELECT c.name as partnerName, c.nameGroup AS partnerGroup, count(*) AS countBRL, SUM(p.amountPrice) AS totalBRL, SUM(p.amountPriceWithoutTax) as totalBRLnoVat
                FROM ".Yii::app()->params["mainDbName"].".payments p
                INNER JOIN ".Yii::app()->params["mainDbName"].".user u ON p.userId = u.id
                INNER JOIN ".Yii::app()->params["mainDbName"].".aba_partners_list c ON u.idPartnerSource = c.idPartner
                WHERE p.status =30 AND p.idPartner<>300006 AND p.amountPrice > 0 AND p.currencyTrans='BRL' and p.paySuppExtId<>".PAY_SUPPLIER_B2B." AND";
                if($entryDate) $sql .= " u.entryDate "; else $sql .= " p.dateEndTransaction ";
                $sql .= "BETWEEN '$startDate' AND '$endDate 23:59:59' GROUP BY c.idPartner ORDER BY c.name ASC;";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result['saleBRL'] = $command->query();

        //total renovations BRL group by partner
        $sql="  SELECT c.name as partnerName, c.nameGroup AS partnerGroup, COUNT(*) AS countRenovationBRL, SUM(p.amountPrice) AS totalRenovationBRL, SUM(p.amountPriceWithoutTax) as totalRenovationBRLnoVat
                FROM ".Yii::app()->params["mainDbName"].".payments p
                INNER JOIN ".Yii::app()->params["mainDbName"].".user u ON p.userId = u.id
                INNER JOIN ".Yii::app()->params["mainDbName"].".aba_partners_list c on u.idPartnerSource = c.idPartner
                WHERE p.status =30 AND p.idPartner=300006 AND p.amountPrice > 0 AND p.currencyTrans='BRL' and p.paySuppExtId<>".PAY_SUPPLIER_B2B." AND";
                if($entryDate) $sql .= " u.entryDate "; else $sql .= " p.dateEndTransaction ";
                $sql .= "BETWEEN '$startDate' AND '$endDate 23:59:59' GROUP BY c.idPartner ORDER BY c.name ASC;";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result['renovationBRL'] = $command->query();

        //total sales EUR group by partner
        $sql="  SELECT c.name AS partnerName, c.nameGroup AS partnerGroup, COUNT(*) AS countEUR, SUM(p.amountPrice) AS totalEUR, SUM(p.amountPriceWithoutTax) as totalEURnoVat
                FROM ".Yii::app()->params["mainDbName"].".payments p
                INNER JOIN ".Yii::app()->params["mainDbName"].".user u ON p.userId = u.id
                INNER JOIN ".Yii::app()->params["mainDbName"].".aba_partners_list c ON u.idPartnerSource = c.idPartner
                WHERE p.status=30 AND p.idPartner<>300006 AND p.amountPrice > 0 AND p.currencyTrans='EUR' and p.paySuppExtId<>".PAY_SUPPLIER_B2B." AND";
                if($entryDate) $sql .= " u.entryDate "; else $sql .= " p.dateEndTransaction ";
                $sql .= "BETWEEN '$startDate' AND '$endDate 23:59:59' GROUP BY c.idPartner ORDER BY c.name ASC;";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result['salesEUR'] = $command->query();

        //total renovations EUR group by partner
        $sql="  SELECT c.name AS partnerName, c.nameGroup AS partnerGroup, COUNT(*) AS countRenovationEUR, SUM(p.amountPrice) AS totalRenovationEUR, SUM(p.amountPriceWithoutTax) as totalRenovationEURnoVat FROM ".Yii::app()->params["mainDbName"].".payments p
                INNER JOIN ".Yii::app()->params["mainDbName"].".user u ON p.userId = u.id
                INNER JOIN ".Yii::app()->params["mainDbName"].".aba_partners_list c ON u.idPartnerSource = c.idPartner
                WHERE p.status =30 AND p.idPartner=300006 AND p.amountPrice > 0 AND p.currencyTrans='EUR' and p.paySuppExtId<>".PAY_SUPPLIER_B2B." AND";
                if($entryDate) $sql .= " u.entryDate "; else $sql .= " p.dateEndTransaction ";
                $sql .= "BETWEEN '$startDate' AND '$endDate 23:59:59' GROUP BY c.idPartner ORDER BY c.name ASC;";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result['renovationEUR'] = $command->query();


        //total sales MXN group by partner
        $sql="  SELECT c.name as partnerName, c.nameGroup AS partnerGroup, count(*) AS countMXN, SUM(p.amountPrice) AS totalMXN, SUM(p.amountPriceWithoutTax) as totalMXNnoVat
                FROM ".Yii::app()->params["mainDbName"].".payments p
                INNER JOIN ".Yii::app()->params["mainDbName"].".user u ON p.userId = u.id INNER JOIN ".Yii::app()->params["mainDbName"].".aba_partners_list c ON u.idPartnerSource = c.idPartner
                WHERE p.status =30 AND p.idPartner<>300006 AND p.amountPrice > 0 AND p.currencyTrans='MXN' and p.paySuppExtId<>".PAY_SUPPLIER_B2B." AND";
                if($entryDate) $sql .= " u.entryDate "; else $sql .= " p.dateEndTransaction ";
                $sql .= "BETWEEN '$startDate' AND '$endDate 23:59:59' GROUP BY c.idPartner ORDER BY c.name ASC;";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result['saleMXN'] = $command->query();

        //total renovations MXN group by partner
        $sql="  SELECT c.name as partnerName, c.nameGroup AS partnerGroup, COUNT(*) AS countRenovationMXN, SUM(p.amountPrice) AS totalRenovationMXN, SUM(p.amountPriceWithoutTax) as totalRenovationMXNnoVat
                FROM ".Yii::app()->params["mainDbName"].".payments p
                INNER JOIN ".Yii::app()->params["mainDbName"].".user u ON p.userId = u.id
                INNER JOIN ".Yii::app()->params["mainDbName"].".aba_partners_list c on u.idPartnerSource = c.idPartner
                WHERE p.status =30 AND p.idPartner=300006 AND p.amountPrice > 0 AND p.currencyTrans='MXN' and p.paySuppExtId<>".PAY_SUPPLIER_B2B." AND";
                if($entryDate) $sql .= " u.entryDate "; else $sql .= " p.dateEndTransaction ";
                $sql .= "BETWEEN '$startDate' AND '$endDate 23:59:59' GROUP BY c.idPartner ORDER BY c.name ASC;";
        $connection = Yii::app()->dbSlave;
        $command = $connection->createCommand($sql);
        $result['renovationMXN'] = $command->query();


        //the next code it is just to put all together in the array.
        $partner = array();
        $toReturn = array();
        $i=0;

        foreach($result['saleUSD'] as $value)
        {
            $partner['name'] = $value['partnerName'];
            $partner['nameGroup'] = $value['partnerGroup'];
            $partner['saleQuantity'] = $value['countUSD'];
            $partner['saleAmount'] = $value['totalUSD'] * $dollarToday;
            $partner['saleAmountnoVat'] = $value['totalUSDnoVat'] * $dollarToday;
            $partner['renovationQuantity'] = 0;
            $partner['renovationAmount'] = 0;
            $partner['renovationAmountnoVat'] = 0;
            $toReturn[$i] = $partner;
            $partner = null;
            $i++;
        }

        foreach($result['renovationUSD'] as $value)
        {
            $band = false;

            for($x=0; $x<$i; $x++)
            {
                if($value['partnerName'] == $toReturn[$x]['name'])
                {
                    $toReturn[$x]['renovationQuantity'] = $value['countRenovationUSD'];
                    $toReturn[$x]['renovationAmount'] = $value['totalRenovationUSD'] * $dollarToday;
                    $toReturn[$x]['renovationAmountnoVat'] = $value['totalRenovationUSDnoVat'] * $dollarToday;
                    $band = true;
                    //find the way to scape the for loop now.. there is no need to keep searching.
                }
            }
            //we didn't find any match for the country.. we add it.
            if(!$band)
            {
                $partner['name'] = $value['partnerName'];
                $partner['nameGroup'] = $value['partnerGroup'];
                $partner['saleQuantity'] = 0;
                $partner['saleAmount'] = 0;
                $partner['saleAmountnoVat'] = 0;
                $partner['renovationQuantity'] = $value['countRenovationUSD'];
                $partner['renovationAmount'] = $value['totalRenovationUSD'] * $dollarToday;
                $partner['renovationAmountnoVat'] = $value['totalRenovationUSDnoVat'] * $dollarToday;
                $toReturn[$i] = $partner;
                $partner = null;
                $i++;
            }
        }
        //new
        foreach($result['saleBRL'] as $value)
        {
            $band = false;

            for($x=0; $x<$i; $x++)
            {
                if($value['partnerName'] == $toReturn[$x]['name'])
                {
                    $toReturn[$x]['saleQuantity'] += $value['countBRL'];
                    $toReturn[$x]['saleAmount'] += $value['totalBRL'] * $realToday;
                    $toReturn[$x]['saleAmountnoVat'] += $value['totalBRLnoVat'] * $realToday;
                    $band = true;
                    //find the way to scape the for loop now.. there is no need to keep searching.
                }
            }
            //we didn't find any match for the country.. we add it.
            if(!$band)
            {
                $partner['name'] = $value['partnerName'];
                $partner['nameGroup'] = $value['partnerGroup'];
                $partner['saleQuantity'] = $value['countBRL'];
                $partner['saleAmount'] = $value['totalBRL'] * $realToday;
                $partner['saleAmountnoVat'] = $value['totalBRLnoVat'] * $realToday;
                $partner['renovationQuantity'] = 0;
                $partner['renovationAmount'] = 0;
                $partner['renovationAmountnoVat'] = 0;
                $toReturn[$i] = $partner;
                $partner = null;
                $i++;
            }
        }

        foreach($result['renovationBRL'] as $value)
        {
            $band = false;

            for($x=0; $x<$i; $x++)
            {
                if($value['partnerName'] == $toReturn[$x]['name'])
                {
                    $toReturn[$x]['renovationQuantity'] += $value['countRenovationBRL'];
                    $toReturn[$x]['renovationAmount'] += $value['totalRenovationBRL'] * $realToday;
                    $toReturn[$x]['renovationAmountnoVat'] += $value['totalRenovationBRLnoVat'] * $realToday;
                    $band = true;
                    //find the way to scape the for loop now.. there is no need to keep searching.
                }
            }
            //we didn't find any match for the country.. we add it.
            if(!$band)
            {
                $partner['name'] = $value['partnerName'];
                $partner['nameGroup'] = $value['partnerGroup'];
                $partner['saleQuantity'] = 0;
                $partner['saleAmount'] = 0;
                $partner['saleAmountnoVat'] = 0;
                $partner['renovationQuantity'] = $value['countRenovationBRL'];
                $partner['renovationAmount'] = $value['totalRenovationBRL'] * $realToday;
                $partner['renovationAmountnoVat'] = $value['totalRenovationBRLnoVat'] * $realToday;
                $toReturn[$i] = $partner;
                $partner = null;
                $i++;
            }
        }

        //new
        foreach($result['salesEUR'] as $value)
        {
            $band = false;

            for($x=0; $x<$i; $x++)
            {
                if($value['partnerName'] == $toReturn[$x]['name'])
                {
                    $toReturn[$x]['saleQuantity'] += $value['countEUR'];
                    $toReturn[$x]['saleAmount'] += $value['totalEUR'];
                    $toReturn[$x]['saleAmountnoVat'] += $value['totalEURnoVat'];
                    $band = true;
                    //find the way to scape the for loop now.. there is no need to keep searching.
                }
            }
            //we didn't find any match for the country.. we add it.
            if(!$band)
            {
                $partner['name'] = $value['partnerName'];
                $partner['nameGroup'] = $value['partnerGroup'];
                $partner['saleQuantity'] = $value['countEUR'];
                $partner['saleAmount'] = $value['totalEUR'];
                $partner['saleAmountnoVat'] = $value['totalEURnoVat'];
                $partner['renovationQuantity'] = 0;
                $partner['renovationAmount'] = 0;
                $partner['renovationAmountnoVat'] = 0;
                $toReturn[$i] = $partner;
                $partner = null;
                $i++;
            }
        }

        foreach($result['renovationEUR'] as $value)
        {
            $band = false;

            for($x=0; $x<$i; $x++)
            {
                if($value['partnerName'] == $toReturn[$x]['name'])
                {
                    $toReturn[$x]['renovationQuantity'] += $value['countRenovationEUR'];
                    $toReturn[$x]['renovationAmount'] += $value['totalRenovationEUR'];
                    $toReturn[$x]['renovationAmountnoVat'] += $value['totalRenovationEURnoVat'];
                    $band = true;
                    //find the way to scape the for loop now.. there is no need to keep searching.
                }
            }
            //we didn't find any match for the country.. we add it.
            if(!$band)
            {
                $partner['name'] = $value['partnerName'];
                $partner['nameGroup'] = $value['partnerGroup'];
                $partner['saleQuantity'] = 0;
                $partner['saleAmount'] = 0;
                $partner['saleAmountnoVat'] = 0;
                $partner['renovationQuantity'] = $value['countRenovationEUR'];
                $partner['renovationAmount'] = $value['totalRenovationEUR'];
                $partner['renovationAmountnoVat'] = $value['totalRenovationEURnoVat'];
                $toReturn[$i] = $partner;
                $partner = null;
                $i++;
            }
        }


        //new
        foreach($result['saleMXN'] as $value)
        {
            $band = false;

            for($x=0; $x<$i; $x++)
            {
                if($value['partnerName'] == $toReturn[$x]['name'])
                {
                    $toReturn[$x]['saleQuantity'] += $value['countMXN'];
                    $toReturn[$x]['saleAmount'] += $value['totalMXN'] * $pesoToday;
                    $toReturn[$x]['saleAmountnoVat'] += $value['totalMXNnoVat'] * $pesoToday;
                    $band = true;
                    //find the way to scape the for loop now.. there is no need to keep searching.
                }
            }
            //we didn't find any match for the country.. we add it.
            if(!$band)
            {
                $partner['name'] = $value['partnerName'];
                $partner['nameGroup'] = $value['partnerGroup'];
                $partner['saleQuantity'] = $value['countMXN'];
                $partner['saleAmount'] = $value['totalMXN'] * $pesoToday;
                $partner['saleAmountnoVat'] = $value['totalMXNnoVat'] * $pesoToday;
                $partner['renovationQuantity'] = 0;
                $partner['renovationAmount'] = 0;
                $partner['renovationAmountnoVat'] = 0;
                $toReturn[$i] = $partner;
                $partner = null;
                $i++;
            }
        }

        foreach($result['renovationMXN'] as $value)
        {
            $band = false;

            for($x=0; $x<$i; $x++)
            {
                if($value['partnerName'] == $toReturn[$x]['name'])
                {
                    $toReturn[$x]['renovationQuantity'] += $value['countRenovationMXN'];
                    $toReturn[$x]['renovationAmount'] += $value['totalRenovationMXN'] * $pesoToday;
                    $toReturn[$x]['renovationAmountnoVat'] += $value['totalRenovationMXNnoVat'] * $pesoToday;
                    $band = true;
                    //find the way to scape the for loop now.. there is no need to keep searching.
                }
            }
            //we didn't find any match for the country.. we add it.
            if(!$band)
            {
                $partner['name'] = $value['partnerName'];
                $partner['nameGroup'] = $value['partnerGroup'];
                $partner['saleQuantity'] = 0;
                $partner['saleAmount'] = 0;
                $partner['saleAmountnoVat'] = 0;
                $partner['renovationQuantity'] = $value['countRenovationMXN'];
                $partner['renovationAmount'] = $value['totalRenovationMXN'] * $pesoToday;
                $partner['renovationAmountnoVat'] = $value['totalRenovationMXNnoVat'] * $pesoToday;
                $toReturn[$i] = $partner;
                $partner = null;
                $i++;
            }
        }


        //calculate sub totals.
        $x=0;
        foreach($toReturn as $value)
        {
            $toReturn[$x]['totalMovement'] = $value['saleQuantity'] + $value['renovationQuantity'];
            $toReturn[$x]['totalAmount'] = $value['saleAmount'] + $value['renovationAmount'];
            $toReturn[$x]['totalAmountnoVat'] = $value['saleAmountnoVat'] + $value['renovationAmountnoVat'];
            $x++;
        }

        usort($toReturn,"cmpAmountDesc");

        return $toReturn;

    }
}