<?php

class LogUserProgressRemoved extends CmLogUserProgressRemoved
{
    public function __construct($scenario = 'insert', $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmLogUserProgressRemoved the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'removedProgressId' => 'ID Removed progress',
          'userId' => 'User ID',
          'units' => 'Unit(s)',
          'dateAdd' => 'Executed',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('removedProgressId', $this->removedProgressId, true);
        $criteria->compare('userId', $this->userId, true);
        $criteria->compare('units', $this->units, true);
        $criteria->compare('dateAdd', $this->dateAdd, true);

        $criteria->order = "removedProgressId DESC ";

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
          'pagination' => array(
            'pageSize' => 100,
          ),
        ));
    }

    /**
     * @param array $stParams
     *
     * @return CSqlDataProvider
     */
    public function searchRemovedProgress($stParams=array()) {

        $iUserId = (isset($stParams['userId']) ? $stParams['userId'] : null);

        $sSql = "
            SELECT
                " . $this->mainFields() . "
            FROM " . $this->tableName() . " lupr
            WHERE
                lupr.userId = '" . $iUserId . "'
            ORDER BY
                lupr.removedProgressId DESC
        ";

        $dataProvider = new CSqlDataProvider($sSql ,
          array(
            'totalItemCount' => 1000,
            'pagination' => array('pageSize' => 1000)
          )
        );
        $dataProvider->keyField = 'removedProgressId';
        $dataProvider->db = Yii::app()->db;

        return $dataProvider;
    }

}
