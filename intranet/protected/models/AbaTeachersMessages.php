<?php
class AbaTeachersMessages extends CModel
{
    public $languaje;
	public $email;
	public $teacherid;
	public $total_users;
	
	public $total_questions;
	public $total_answers;
	public $total_noanswers;
	
	public $period_questions;
	public $period_answers;
	public $period_noanswers;
	public $noperiod_answers;
	
	public $startDate;
	public $endDate;
	
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		);
	}
	public function search()
	{
		return AbaTeachersMessages::getTotals($this);
	}
	public function searchTeacherTotals()
	{
		return AbaTeachersMessages::getTeacherTotals($this);
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
	//	array('langCourse, email, teacherid, alumnos_totales, questions, respuestas, questions, noanswers', 'safe', 'on'=>'search'),

		return array(
			'teacherid' => 'Teacher',
			'language' => 'languaje',
			'total_users' => 'Total Learners',
			'email' => 'Email',
			'total_questions' => 'total Preguntas ',
			'total_answers' => 'Respuestas',
			'total_noanswers' => 'No respondidas',
			
			'period_questions' => 'total preguntas ',
			'period_answers' => 'Periodo respuestas',
			'period_noanswers' => 'Periodo No respondidas',
			'noperiod_answers' => 'Respondidas otro periodo',
		
			'startDate' => 'Fecha inicial',
			'endDate' => 'Fecha final',
		);
	}    
	public function attributeNames()
	{
	//	array('langCourse, email, teacherid, alumnos_totales, questions, respuestas, questions, noanswers', 'safe', 'on'=>'search'),
		return array(
			'teacherid',
			'languaje',
			'email',
			//'alumnos_totales',		
			'total_users',
			'total_questions',
			'total_answers',
			'total_noanswers',
			
			'period_questions',
			'period_answers',
			'period_noanswers',
			'noperiod_answers',
			/*
			'questions',
			'answers',
			'noanswers',*/
		);
	}  
	static function getExclosedSubjects($inSchema="m")
	{
		if($inSchema!="")
			$inSchema.=".";
		$sql=" {$inSchema}subject NOT IN ('Bem-vindo ao campus!','Welcome to the campus!','Bienvenue sur le campus!',
		'Benvenuto al campus!','Bienvenido al campus!')";
		return $sql;
	}
	static function createSqlTeacherTotals($attributes) 
	{
		$startDate=$attributes->startDate;//"2013-01-26 00:00:00";
		$endDate=$attributes->endDate;"2013-02-26 00:00:00";
		$teacherID=$attributes->teacherid;
		$Exclosed=self::getExclosedSubjects();
	/*
$sql="SELECT u0.langCourse as languaje, u0.email as email, u0.id as teacherid, u5.total_users, u0.total_preguntas as total_questions,
IF(u1.total_respuestas is null, 0,u1.total_respuestas) as total_answers, (u0.total_preguntas-IF(u1.total_respuestas is null, 0,u1.total_respuestas)) as total_noanswers,
IF(u2.period_preguntas is null, 0, u2.period_preguntas) as period_questions,IF(u3.period_respondidas is null, 0, u3.period_respondidas) as period_answers, (IF(u2.period_preguntas is null,0, u2.period_preguntas) -IF(u3.period_respondidas is null, 0, u3.period_respondidas)) as period_noanswers,
IF(u4.outperiod_respondidas is null , 0, u4.outperiod_respondidas) as noperiod_answers
FROM
(
  SELECT count(*) as total_preguntas, ua.langCourse, m.receiver_id as id, u.email FROM messages m
	INNER JOIN user u ON u.id= m.receiver_id and u.userType=4
	INNER JOIN user ua ON ua.id= m.sender_id WHERE m.receiver_id=$teacherID
  group by m.receiver_id, ua.langCourse 

) u0
LEFT JOIN
(
  SELECT count(*) as total_respuestas, ua.langCourse, m.sender_id as id FROM messages m
  INNER JOIN user_teacher ut ON ut.userid=m.sender_id
	INNER JOIN user ua ON ua.id= m.receiver_id
  WHERE m.subject like 'RE: %' group by m.sender_id, ua.langCourse
) u1 ON u1.id=u0.id and u1.langCourse=u0.langCourse
LEFT JOIN
(
  SELECT count(*) as period_preguntas, ua.langCourse, m.receiver_id as id FROM messages m
	INNER JOIN user u ON u.id= m.receiver_id and u.userType=4
	INNER JOIN user ua ON ua.id= m.sender_id
  WHERE m.created_at between '$startDate' AND '$endDate' group by m.receiver_id, ua.langCourse
) u2 ON u2.id=u1.id  and u2.langCourse=u1.langCourse
LEFT JOIN
(
  SELECT count(*) as period_respondidas, ua.langCourse, m.sender_id as id FROM messages m
  INNER JOIN user_teacher ut ON ut.userid=m.sender_id
	INNER JOIN user ua ON ua.id= m.receiver_id
  WHERE m.created_at between '$startDate' AND '$endDate' AND m.subject like 'RE: %' group by m.sender_id, ua.langCourse
)u3 ON u3.id=u2.id  and u3.langCourse=u2.langCourse
LEFT JOIN
(
  SELECT count(*) as outperiod_respondidas, ua.langCourse, mt.sender_id as id FROM messages mt
  INNER JOIN user_teacher ut ON ut.userid=mt.sender_id
	INNER JOIN user ua ON ua.id= mt.receiver_id
	LEFT JOIN messages ma ON ma.sender_id=mt.receiver_id AND ma.receiver_id=mt.sender_id AND mt.subject=CONCAT('RE: ', ma.subject)
  WHERE mt.created_at between '$startDate' AND '$endDate' and ma.created_at NOT  between '$startDate' AND '$endDate' group by mt.sender_id, ua.langCourse
)u4 ON u4.id=u3.id  and u4.langCourse=u3.langCourse
LEFT JOIN (
		SELECT count(*) as total_users, langCourse, teacherid  as id FROM user ui WHERE ui.userType=2 group by teacherid, langCourse
		) u5 ON u5.id=u0.id  and u5.langCourse=u0.langCourse";// WHERE u0.id=$teacherID;";
		*/
		$sql="SELECT u5.langCourse as languaje, u0.email as email, u5.id as teacherid, u5.total_users, IF(u0.total_preguntas is null, 0, u0.total_preguntas) as total_questions,
IF(u1.total_respuestas is null, 0,u1.total_respuestas) as total_answers, (IF(u0.total_preguntas is null, 0, u0.total_preguntas) -IF(u1.total_respuestas is null, 0,u1.total_respuestas)) as total_noanswers,
IF(u2.period_preguntas is null, 0, u2.period_preguntas) as period_questions,IF(u3.period_respondidas is null, 0, u3.period_respondidas) as period_answers, (IF(u2.period_preguntas is null,0, u2.period_preguntas) -IF(u3.period_respondidas is null, 0, u3.period_respondidas)) as period_noanswers,
IF(u4.outperiod_respondidas is null , 0, u4.outperiod_respondidas) as noperiod_answers
FROM
(
	SELECT count(*) as total_users, langCourse, teacherid  as id FROM user ui WHERE ui.userType=2 and teacherid=$teacherID group by teacherid, langCourse
) u5
LEFT JOIN 
(
  SELECT count(*) as total_preguntas, ua.langCourse, m.receiver_id as id, u.email FROM messages m
	INNER JOIN user u ON u.id= m.receiver_id and u.userType=4
	INNER JOIN user ua ON ua.id= m.sender_id WHERE m.receiver_id=$teacherID
  group by m.receiver_id, ua.langCourse 
) u0 ON u0.id=u5.id  and u0.langCourse=u5.langCourse
LEFT JOIN
(
  SELECT count(*) as total_respuestas, ua.langCourse, m.sender_id as id FROM messages m
  INNER JOIN user_teacher ut ON ut.userid=m.sender_id
	INNER JOIN user ua ON ua.id= m.receiver_id
  WHERE (m.subject like 'RE: %' OR  $Exclosed) group by m.sender_id, ua.langCourse
) u1 ON u1.id=u0.id and u1.langCourse=u0.langCourse
LEFT JOIN
(
  SELECT count(*) as period_preguntas, ua.langCourse, m.receiver_id as id FROM messages m
	INNER JOIN user u ON u.id= m.receiver_id and u.userType=4
	INNER JOIN user ua ON ua.id= m.sender_id
  WHERE m.created_at between '$startDate' AND '$endDate' group by m.receiver_id, ua.langCourse
) u2 ON u2.id=u1.id  and u2.langCourse=u1.langCourse
LEFT JOIN
(
  SELECT count(*) as period_respondidas, ua.langCourse, m.sender_id as id FROM messages m
  INNER JOIN user_teacher ut ON ut.userid=m.sender_id
	INNER JOIN user ua ON ua.id= m.receiver_id
  WHERE m.created_at between '$startDate' AND '$endDate' AND (m.subject like 'RE: %' OR  $Exclosed) group by m.sender_id, ua.langCourse
)u3 ON u3.id=u2.id  and u3.langCourse=u2.langCourse
LEFT JOIN
(
  SELECT count(*) as outperiod_respondidas, ua.langCourse, m.sender_id as id FROM messages m
  INNER JOIN user_teacher ut ON ut.userid=m.sender_id
	INNER JOIN user ua ON ua.id= m.receiver_id
	LEFT JOIN messages ma ON ma.sender_id=m.receiver_id AND ma.receiver_id=m.sender_id AND m.subject=CONCAT('RE: ', ma.subject)
  WHERE m.created_at between '$startDate' AND '$endDate' and ma.created_at NOT  between '$startDate' AND '$endDate'  group by m.sender_id, ua.langCourse
)u4 ON u4.id=u3.id  and u4.langCourse=u3.langCourse
";
/*
(
  SELECT count(*) as outperiod_respondidas, ua.langCourse, m.sender_id as id FROM messages m
  INNER JOIN user_teacher ut ON ut.userid=m.sender_id
	INNER JOIN user ua ON ua.id= m.receiver_id
	LEFT JOIN messages ma ON ma.sender_id=m.receiver_id AND ma.receiver_id=m.sender_id AND m.subject=CONCAT('RE: ', ma.subject)
  WHERE m.created_at between '$startDate' AND '$endDate' and ma.created_at NOT  between '$startDate' AND '$endDate'  OR  (m.created_at between '$startDate' AND '$endDate' AND m.subject NOT like 'RE: %'  AND $Exclosed) group by m.sender_id, ua.langCourse
)u4
*/		
		return $sql;
	}
	public function getTeacherTotalsDataReader() 
	{
		$sql= self::createSqlTeacherTotals($this); 
		$connection = Yii::app()->dbSlave;
		$command = $connection->createCommand($sql);		
        $dataReader=$command->query();
		return $dataReader;			
	}
	static function getTeacherTotals($attributes) 
	{
		$sql= self::createSqlTeacherTotals($attributes); 
		return new CSqlDataProvider($sql);
	}
	static function getTotals($attributes)
	{
		$startDate=$attributes->startDate;
		$endDate=$attributes->endDate;
		$Exclosed=self::getExclosedSubjects();

        $table = Yii::app()->params["mainDbName"].".";

		$sql="SELECT  u0.email, u0.id as teacherid, u5.total_users, u0.total_preguntas as total_questions, u1.total_respuestas as total_answers,
(u0.total_preguntas-u1.total_respuestas) as total_noanswers,
u2.period_preguntas as period_questions, u3.period_respondidas as period_answers,
(u2.period_preguntas-u3.period_respondidas) as period_noanswers,
IF(u4.outperiod_respondidas is null , 0, u4.outperiod_respondidas)as noperiod_answers
FROM
(
  SELECT count(*) as total_preguntas, m.receiver_id as id, u.email FROM ".$table."messages m
	INNER JOIN ".$table."user u ON u.id= m.receiver_id and u.userType=4
  group by m.receiver_id

) u0
LEFT JOIN
(
  SELECT count(*) as total_respuestas, m.sender_id as id FROM ".$table."messages m
  INNER JOIN ".$table."user_teacher ut ON ut.userid=m.sender_id
  WHERE (m.subject like 'RE: %' OR $Exclosed) group by m.sender_id
) u1 ON u1.id=u0.id
LEFT JOIN
(
  SELECT count(*) as period_preguntas, m.receiver_id as id FROM ".$table."messages m
	INNER JOIN ".$table."user u ON u.id= m.receiver_id and u.userType=4
  WHERE m.created_at between '$startDate' AND '$endDate' group by m.receiver_id
) u2 ON u2.id=u1.id
LEFT JOIN
(
  SELECT count(*) as period_respondidas, m.sender_id as id FROM ".$table."messages m
  INNER JOIN ".$table."user_teacher ut ON ut.userid=m.sender_id
  WHERE m.created_at between '$startDate' AND '$endDate' AND (m.subject like 'RE: %' OR $Exclosed) group by m.sender_id
)u3 ON u3.id=u2.id
LEFT JOIN
(
  SELECT count(*) as outperiod_respondidas, m.sender_id as id FROM ".$table."messages m
  INNER JOIN ".$table."user_teacher ut ON ut.userid=m.sender_id
	LEFT JOIN ".$table."messages ma ON ma.sender_id=m.receiver_id AND ma.receiver_id=m.sender_id AND m.subject=CONCAT('RE: ', ma.subject)
  WHERE m.created_at between '$startDate' AND '$endDate' and ma.created_at NOT  between '$startDate' AND '$endDate' group by m.sender_id
)u4 ON u4.id=u3.id
LEFT JOIN (
		SELECT count(*) as total_users, teacherid  as id FROM ".$table."user ui WHERE ui.userType=2 group by teacherid
		) u5 ON u5.id=u0.id";

/*
(
  SELECT count(*) as outperiod_respondidas, m.sender_id as id FROM messages m
  INNER JOIN user_teacher ut ON ut.userid=m.sender_id
    LEFT JOIN messages ma ON ma.sender_id=m.receiver_id AND ma.receiver_id=m.sender_id AND m.subject=CONCAT('RE: ', ma.subject)
  WHERE (m.created_at between '$startDate' AND '$endDate' and ma.created_at NOT  between '$startDate' AND '$endDate') OR  (m.subject NOT like 'RE: %'  AND $Exclosed) group by m.sender_id
)u4 ON u4.id=u3.id
*/
        $dataProvider = new CSqlDataProvider($sql);
        $dataProvider->db = Yii::app()->dbSlave;
        return $dataProvider;
	}

}