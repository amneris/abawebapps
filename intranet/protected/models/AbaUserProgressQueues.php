<?php

class AbaUserProgressQueues extends CmAbaUserProgressQueues
{
    public function __construct($scenario = 'insert', $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaUserProgressQueues the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'queueId' => 'ID queue',
          'userId' => 'User id',
          'units' => 'Unit(s) number(s)',
          'evaluation' => 'Evaluation',
          'dateAdd' => 'Created',
          'dateStart' => 'Process start',
          'dateEnd' => 'Process end',
          'status' => 'Current status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('queueId', $this->queueId, true);
        $criteria->compare('userId', $this->userId, true);
        $criteria->compare('units', $this->units, true);
        $criteria->compare('evaluation', $this->evaluation, true);
        $criteria->compare('dateAdd', $this->dateAdd, true);
        $criteria->compare('dateStart', $this->dateStart, true);
        $criteria->compare('dateEnd', $this->dateEnd, true);
        $criteria->compare('status', $this->status, true);

        $criteria->addCondition("status <> '" . self::QUEUE_STATUS_DELETED . "'");

        $criteria->order = "queueId DESC ";

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
          'pagination' => array(
            'pageSize' => 100,
          ),
        ));
    }


    /**
     * @param bool|true $runValidation
     * @param null $attributes
     *
     * @return bool
     */
    public function save($runValidation = true, $attributes = null)
    {
        if ($this->validateUserProgressQueue()) {
            $stResult = parent::save($runValidation, $attributes);

            $this->saveDetails();

            return $stResult;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function validateUserProgressQueue()
    {

        $this->units = trim(str_replace(" ", "", $this->units));

        if($this->units == '') {
            $this->addError('units', 'ERRORRRRR');
            return false;
        }

        $stUnits = explode(",", $this->units);

        if(count($stUnits) <= 0) {
            $this->addError('units', 'ERRORRRRR');
            return false;
        }

        foreach($stUnits as $iKey => $iUnit) {
            if(!is_numeric($iUnit) OR $iUnit == 0) {
                $this->addError('units', 'ERRORRRRR !!!!!');
                return false;
            }
        }

        return true;
    }

}
