<?php

/**
 * This is the model class for table "ventasFlashScript".
 *
 * The followings are the available columns in table 'ventasFlashScript':
 * @property integer $idventasFlashScript
 * @property string $tableName
 * @property integer $oneOrTwoCodes
 * @property string $prefijoMensual
 * @property string $prefijoBimensual
 * @property string $prefijoTrimestral
 * @property string $prefijoSemestral
 * @property string $prefijoAnual
 * @property string $prefijo18Meses
 * @property string $prefijo24Meses
 * @property integer $totalDeals
 * @property string $agrupadorName
 * @property string $url
 */
class AbaAgrupadores extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AbaAgrupadores the static model class
	 */
        
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ventasFlashScript';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('tableName, agrupadorName, oneOrTwoCodes, totalDeals', 'required'),
			array('oneOrTwoCodes, totalDeals', 'numerical', 'integerOnly'=>true),
			array('tableName, agrupadorName, url', 'length', 'max'=>100),
			array('prefijoMensual, prefijoBimensual, prefijoTrimestral, prefijoSemestral, prefijoAnual, prefijo18Meses, prefijo24Meses', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idventasFlashScript, tableName, oneOrTwoCodes, prefijoMensual, prefijoBimensual, prefijoTrimestral, prefijoSemestral, prefijoAnual, prefijo18Meses, prefijo24Meses, totalDeals, agrupadorName, url', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idventasFlashScript' => 'Idventas Flash Script',
			'tableName' => 'Table name',
			'oneOrTwoCodes' => 'Number of codes',
			'prefijoMensual' => 'Monthly prefix',
            'prefijoBimensual' => 'Bimonthly prefix',
			'prefijoTrimestral' => 'Quarterly prefix',
			'prefijoSemestral' => 'Semiannual prefix',
			'prefijoAnual' => 'Annual prefix',
			'prefijo18Meses' => '18 months prefix',
			'prefijo24Meses' => '24 months prefix',
			'totalDeals' => 'Total deals',
			'agrupadorName' => 'Grouper name',
            'url' => 'Landing Url',
		);
	}
    
    public function saveAgrupador($xModel)
    {
        $preQuery = "SELECT max(idventasFlashScript) as maxId FROM (select a.idventasFlashScript from db_ventasflash.ventasFlashScript a where a.idventasFlashScript >= 1 && a.idventasFlashScript < 5000) a";
        $preConnection = Yii::app()->db2;
        $preCommand = $preConnection->createCommand($preQuery);
        $dataReader = $preCommand->query();
        $rows = $dataReader->read();
        $maxId = $rows['maxId'];
        $maxId++;
        //now the insert
        $xModel->idventasFlashScript = $maxId;
        $query = "INSERT INTO db_ventasflash.ventasFlashScript (idventasFlashScript, tableName, oneOrTwoCodes, prefijoMensual, prefijoBimensual, prefijoTrimestral, prefijoSemestral,
prefijoAnual, prefijo18Meses, prefijo24Meses, totalDeals, agrupadorName, url) VALUES ($xModel->idventasFlashScript, '$xModel->tableName', $xModel->oneOrTwoCodes,
'$xModel->prefijoMensual', '$xModel->prefijoBimensual', '$xModel->prefijoTrimestral', '$xModel->prefijoSemestral', '$xModel->prefijoAnual', '$xModel->prefijo18Meses', '$xModel->prefijo24Meses',
'$xModel->totalDeals', '$xModel->agrupadorName', '$xModel->url');";
        
        $connection = Yii::app()->db2;
        $command = $connection->createCommand($query);
        $result = $command->query();
        if($result)
            return $xModel;
        else 
            return false;
    }
    
    

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.
            
            $criteria=new CDbCriteria;

            $criteria->compare('idventasFlashScript',$this->idventasFlashScript);
            $criteria->compare('tableName',$this->tableName,true);
            $criteria->compare('agrupadorName',$this->agrupadorName,true);
            $criteria->compare('totalDeals',$this->totalDeals,true);
            $criteria->compare('oneOrTwoCodes',$this->oneOrTwoCodes,true);
            $criteria->compare('prefijoMensual',$this->prefijoMensual,true);
            $criteria->compare('prefijoBimensual',$this->prefijoBimensual,true);
            $criteria->compare('prefijoTrimestral',$this->prefijoTrimestral,true);
            $criteria->compare('prefijoSemestral',$this->prefijoSemestral,true);
            $criteria->compare('prefijoAnual',$this->prefijoAnual,true);
            $criteria->compare('prefijo18Meses',$this->prefijo18Meses,true);
            $criteria->compare('prefijo24Meses',$this->prefijo24Meses,true);
            $criteria->compare('url',$this->url,true);

            return new CActiveDataProvider($this, array(

                    'criteria'=>$criteria,
            ));
	}
        
}