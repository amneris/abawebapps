<?php
/**
 * This is the model class for table "aba_b2c_summary.users_progress_lastweek".
 *
 * The followings are the available columns in table 'aba_b2c_summary.users_progress_lastweek':
 * @property string $userId
 * @property integer $unitId
 * @property string $dateCapture
 * @property integer $yearRef
 * @property integer $weekRef
 * @property integer $currentLevel
 * @property integer $totalProgress
 * @property integer $abaFilm
 * @property integer $speak
 * @property integer $write
 * @property integer $interpret
 * @property integer $videoClass
 * @property integer $exercises
 * @property integer $vocabulary
 * @property integer $assessment
 */
class AbaUsersProgressLastweek extends AbaActiveRecord
{
    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return Yii::app()->params['dbCampusSummary'] . '.users_progress_lastweek';
    }

    /** List of all fields available in table
     * @return string
     */
    public function mainFields()
    {
        return "*";
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'userId' => 'User',
			'unitId' => 'Unit',
			'dateCapture' => 'Date Capture',
			'yearRef' => 'Year Ref',
			'weekRef' => 'Week Ref',
			'currentLevel' => 'Current Level',
			'totalProgress' => 'Total Progress',
			'abaFilm' => 'Aba Film',
			'speak' => 'Speak',
			'write' => 'Write',
			'interpret' => 'Interpret',
			'videoClass' => 'Video Class',
			'exercises' => 'Exercises',
			'vocabulary' => 'Vocabulary',
			'assessment' => 'Assessment',
		);
	}

    /** Returns the last row inserted into table.
     * @return bool
     */
    public function getLastRow()
    {
        $sql = ' SELECT upl.' . $this->mainFields() .
            ' FROM ' . $this->tableName() . ' upl  ORDER BY upl.dateCapture DESC LIMIT 1; ';

        $dataReader = $this->querySQL($sql);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return true;
        }

        return false;
    }

    /**
     * @param B2cFollowup $moFollowUp
     * @param $dateFromLastChange
     * @param $start
     * @param $limit
     *
     * @return bool|int|string
     */
    public function collectProgressFromDate(B2cFollowup $moFollowUp, $dateFromLastChange, $start, $limit )
    {
        $sSql = "
            REPLACE INTO " . $this->tableName() . "
            ( `userId`,  `unitId`,  `dateCapture`,  `yearRef`,  `weekRef`,  `currentLevel`,
            `totalProgress`,  `abaFilm`,  `speak`,  `write`,  `interpret`,
            `videoClass`,  `exercises`,  `vocabulary`,  `assessment` )
        ";

        $sSql .= "
            SELECT f.`userid` as 'userId', f.`themeid` as 'unit', DATE(NOW()) as 'dateShot', YEAR(NOW()),
            (WEEK(DATE(NOW()), 1) - 1) as 'weekReference', u.currentLevel as 'currentLevel', ";
        $sSql .= $moFollowUp->getSQLComputeFullProgress(true) . " as `totalProgress`, ";
        $sSql .= $moFollowUp->getSQLComputedSections(true, true);
        $sSql .= "
            FROM
            (
                SELECT fu.*  FROM (
                    SELECT fu4.`userid`, fu4.`themeid`, fu4.sit_por, fu4.stu_por, fu4.dic_por, fu4.rol_on, fu4.gra_vid, fu4.wri_por, fu4.new_por, fu4.eva_por, fu4.rol_por
                    FROM followup4 AS fu4
                    WHERE DATE(fu4.`lastchange`) > DATE(:DATESINCE)
                    LIMIT " . $start . ", " . $limit . "
                ) AS fu
        ";

        $abaFollowUpVersion =   new AbaFollowUpVersion();
        $allVersions =          $abaFollowUpVersion->getAllVersions();

        foreach($allVersions as $iKey => $iVersion) {
            $sSql .= "
                UNION
                SELECT fu" . $iVersion . ".*  FROM (
                    SELECT
                        fu4_" . $iVersion . ".`userid`, fu4_" . $iVersion . ".`themeid`, fu4_" . $iVersion . ".sit_por, fu4_" . $iVersion . ".stu_por,
                        fu4_" . $iVersion . ".dic_por, fu4_" . $iVersion . ".rol_on, fu4_" . $iVersion . ".gra_vid, fu4_" . $iVersion . ".wri_por,
                        fu4_" . $iVersion . ".new_por, fu4_" . $iVersion . ".eva_por, fu4_" . $iVersion . ".rol_por
                    FROM followup4_" . $iVersion . " AS fu4_" . $iVersion . "
                    WHERE DATE(fu4_" . $iVersion . ".`lastchange`) > DATE(:DATESINCE)
                    LIMIT " . $start . ", " . $limit . "
                ) AS fu" . $iVersion . "
            ";
        }
        $sSql .= "
            ) AS f
            INNER JOIN `user` u ON f.userid = u.id
            GROUP BY f.userid, f.themeid
            ORDER BY f.userid ASC, f.themeid ASC
        ";
        $aBrowserValues = array(':DATESINCE' => $dateFromLastChange);

        $qRows = $this->executeSQL($sSql, $aBrowserValues, true);
        if ( $qRows > 0) {
             return $qRows;
        }
        return false;


        $sql = "REPLACE INTO ".$this->tableName()."
                ( `userId`,  `unitId`,  `dateCapture`,  `yearRef`,  `weekRef`,  `currentLevel`,
                `totalProgress`,  `abaFilm`,  `speak`,  `write`,  `interpret`,
                `videoClass`,  `exercises`,  `vocabulary`,  `assessment` )

               SELECT f.`userid` as 'userId', f.`themeid` as 'unit', DATE(NOW()) as 'dateShot', YEAR(NOW()),
                (WEEK(DATE(NOW()),1)-1) as 'weekReference', u.currentLevel as 'currentLevel',".
                $moFollowUp->getSQLComputeFullProgress(true)." as `totalProgress`,".
                $moFollowUp->getSQLComputedSections(true, true).
            " FROM followup4 f INNER JOIN `user` u ON f.userid=u.id
             WHERE DATE(f.`lastchange`)>DATE(:DATESINCE)
             GROUP BY f.userid, f.`themeid`
             ORDER BY f.userid ASC, f.`themeid` ASC
             LIMIT $start, $limit ";

        $aBrowserValues = array( ':DATESINCE' => $dateFromLastChange );

        $qRows = $this->executeSQL($sql, $aBrowserValues, true );
        if ( $qRows > 0) {
            return $qRows;
        }
        return false;
    }


    /** Deletes by batch rows from this table.
     *
     * @param integer $batchRows
     * @return bool|int
     */
    public function deleteLastRows($batchRows=0)
    {
        $sqlLimit = 'LIMIT '.$batchRows;
        if($batchRows==0){
            $sqlLimit = '';
        }

        // ! NOT TO MODIFY THE ORDER BY IN THIS DELETE!!
        $sql = " DELETE FROM ".$this->tableName().
            " ORDER BY `userId`, `unitId`, `dateCapture` ".
            " $sqlLimit; ";

        $fDeleted = $this->deleteSQL($sql);
        if($fDeleted>0){
            return $fDeleted;
        }

        return false;
    }

}
