<?php

/**
 * This is the model class for table "user_history_state".
 *
 * The followings are the available columns in table 'user_history_state':
 * @property integer $userId
 * @property string $userType
 * @property string $date
 * @property string $idPartner
 */
class AbaUserHistoryState extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AbaUserHistoryState the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_history_state';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userId, userType, idPartner', 'required'),
			array('userId', 'numerical', 'integerOnly'=>true),
			array('userType', 'length', 'max'=>2),
			array('idPartner', 'length', 'max'=>4),
			array('date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('userId, userType, date, idPartner', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Usuario'=>array(self::BELONGS_TO, 'AbaUser', 'userId')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'userId' => 'User',
			'userType' => 'User Type',
			'date' => 'Date',
			'idPartner' => 'Id Partner',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$theValue=$this->userId;
		if(HeString::isValidEmail(trim($this->userId)))
		{
			$sql="SELECT count(*) as num, p.id FROM user p WHERE p.email='".trim($this->userId)."' limit 1";
			$connection = Yii::app()->db;
			$command = $connection->createCommand($sql);		
			$dataReader=$command->query();
			if(($row = $dataReader->read())!==false)
			{
				if($row["num"]!="0")
					$theValue=$row["id"]; 			
			}
			else
				$theValue="-1";
		}
		$criteria=new CDbCriteria;
		$criteria->compare('userId',$theValue, false);
//		$criteria->compare('userId',$this->userId, false);
		$criteria->compare('userType',$this->userType,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('idPartner',$this->idPartner,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}