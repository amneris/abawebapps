<?php

class AbaPartnergroupsController extends AbaController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
          'accessControl', // perform access control for CRUD operations
          'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
          array(
            'allow',
            'actions' => array('view', 'create', 'update', 'delete', 'index', 'admin'),
            'users' => array('root', 'marketing'),
          ),
          array(
            'deny', // deny all users
            'users' => array('*'),
          ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render(
          'view',
          array(
            'model' => $this->loadModel($id),
          )
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new AbaPartnergroups();

        if (isset($_POST['AbaPartnergroups'])) {

            $attributes = $_POST['AbaPartnergroups'];

            if (isset($attributes['dateAdd']) && (trim($attributes['dateAdd']) == '' || trim($attributes['dateAdd']) == '0000-00-00 00:00:00')) {
                $attributes['dateAdd'] = HeDate::todaySQL(true);
            }

            $model->attributes = $attributes;

            if ($model->idGroup == '') {
                $model->idGroup = null;
            }
            if (trim($model->dateAdd) == '' || trim($model->dateAdd) == '0000-00-00 00:00:00') {
                $model->dateAdd = HeDate::todaySQL(true);
            }

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->idGroup));
            }
        }
        else {
            $model->dateAdd = HeDate::todaySQL(true);
        }

        $this->render(
          'create',
          array(
            'model' => $model,
            'wAction' => "create",
          )
        );
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (isset($_POST['AbaPartnergroups'])) {

            $attributes = $_POST['AbaPartnergroups'];

            if (isset($attributes['dateAdd']) && (trim($attributes['dateAdd']) == '' || trim($attributes['dateAdd']) == '0000-00-00 00:00:00')) {
                $attributes['dateAdd'] = HeDate::todaySQL(true);
            }

            $model->attributes = $attributes;

            if ($model->idGroup == '') {
                $model->idGroup = null;
            }
            $model->dateAdd = HeDate::todaySQL(true);

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->idGroup));
            }
        }

        $this->render(
          'update',
          array(
            'model' => $model,
            'wAction' => "update",
          )
        );
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('AbaPartnergroups');
        $this->render(
          'index',
          array(
            'dataProvider' => $dataProvider,
            'wAction' => "index",
          )
        );
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new AbaPartnergroups('search');
        $model->unsetAttributes(); // clear any default values

        if (isset($_GET['AbaPartnergroups'])) {
            $model->attributes = $_GET['AbaPartnergroups'];
        }

        $this->render(
          'admin',
          array(
            'model' => $model,
            'wAction' => "admin",
          )
        );
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = AbaPartnergroups::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'aba-partner-groups-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
