<?php

class SiteController extends AbaController
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		if(Yii::app()->user->getId())
		{	
			$this->render('index');
		}	
		else
			$this->redirect(array('site/login'));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validateFile user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

  public function actionSupportWeeklyReport() {
//    http://intranet.aba.local/index.php?r=site/SupportWeeklyReport
//    echo date("Y-m-d H:i:s").": Action Support Weekly Report\n";

    $carlesdev = false;
    if( isset($_SERVER['ABAWEBAPPS_ENV']) ) {
      if ( $_SERVER['ABAWEBAPPS_ENV'] == 'carlesdev' ) $carlesdev = true;
    }

    // create DB connection
    $dsn = 'mysql:host=172.16.1.100;dbname=redmine';
    $username = 'abaread';
    $password = 'Ab4R3ad!us3RsT';
//    $dsn = 'mysql:host=localhost;dbname=redmine';
//    $username = 'abaapps';
//    $password = 'abaenglish';
    $db = new CDbConnection($dsn,$username,$password);
    $db->active=true;

    $fechaReport = date( "Y-m-d", strtotime('monday this week')-86400*7);

    $sql = "select u.id, u.firstname, u.lastname, count(i.id) as num
            from users u, issues i
            where i.author_id = u.id
              and u.id in ( 56, 85, 30, 86, 73, 67, 32, 89, 81, 24 )
              and i.created_on >= '".date("Y-m-d 00:00:00", strtotime($fechaReport))."'
            group by u.id
            order by u.lastname, u.firstname ";
    $command = $db->createCommand($sql);
    $rows = $command->queryAll();

    $tmppath = Yii::app()->basepath.'/../runtime/';
    if ( !is_dir($tmppath) ) mkdir($tmppath);

    $tmpfileCSV = $tmppath.date("Ymd").'_SupportWeeklyReport.csv';
    file_put_contents($tmpfileCSV, '"Nombre";"Issues";"Fecha Inicio"'."\n", FILE_APPEND);
    foreach( $rows as $row ) {
      $lineaCSV = '"'.$row['firstname'].' '.$row['lastname'].'";'.$row['num'].';'.$fechaReport."\n";
      file_put_contents($tmpfileCSV, $lineaCSV, FILE_APPEND);
    }

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($tmpfileCSV).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($tmpfileCSV));
    readfile($tmpfileCSV);

    unlink($tmpfileCSV);
    die;
  }

}