<?php

class B2bFollowupController extends AbaController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
    public $default = 't_scorm_ivap';
    public $list = array('t_scorm_ivap'=>'Ivap','t_scorm_prescal'=>'Prescal','t_scorm_grupoimf'=>'Grupo IMF','t_scorm_corporate'=>'Scorm','t_scorm_enjoy'=>'Enjoy','t_scorm_mf'=>'M. Fomento','t_scorm_getbrit'=>'Getbrit');
    public $levelList = array('All levels'=>'All levels','Beginners'=>'Beginners','Lower intermediate'=>'Lower intermediate','Intermediate'=>'Intermediate',
        'Upper Intermediate'=>'Upper Intermediate', 'Advanced'=>'Advanced', 'Business'=>'Business');

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(

            array('allow',
                'actions'=>array('view', /*'create', 'update', 'delete', 'index',*/ 'admin', 'export', 'exportCompact'),
                'users'=>array('support'),
            ),

            array('allow',
                'actions'=>array('view', /*'create', 'update', 'delete', 'index',*/ 'admin', 'export', 'exportCompact'),
                'users'=>array('marketing'),
            ),

            array('allow',
                'actions'=>array('view', /*'create', 'update', 'delete', 'index',*/ 'admin', 'export', 'exportCompact'),
                'users'=>array('teacher'),
            ),

            array('allow',
                'actions'=>array('view', /*'create', 'update', 'delete', 'index',*/ 'admin', 'export', 'exportCompact'),
                'users'=>array('root'),
            ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id, $company, $level, $criterio)
	{
        $model = new b2bFollowup();
        $model = $model->findByIdFollowup($id, $company);

        $this->render('view',array('model'=>$model, 'level'=>$level, 'company'=>$company, 'criterio'=>$criterio, 'totalsPercent'=>$this->getTotals($model)));

	}

    //
    public function getTotals($model)
    {
        $percentages = array('sit100'=>33,'stu100'=>20,'dic100'=>75,'rol100'=>25,'gra100'=>15,'wri100'=>15,'new100'=>15,'spe100'=>15);
        $totalsPercent = array();

        $totalsPercent['sitPercent'] = round($model->sit_por * 100 / $percentages['sit100']);
        if($totalsPercent['sitPercent']>99)
            $totalsPercent['sitPercent']=100;

        $totalsPercent['stuPercent'] = round($model->stu_por * 100 / $percentages['stu100']);
        if($totalsPercent['stuPercent']>99)
            $totalsPercent['stuPercent']=100;

        $totalsPercent['dicPercent'] = round($model->dic_por * 100 / $percentages['dic100']);
        if($totalsPercent['dicPercent']>99)
            $totalsPercent['dicPercent']=100;

        $totalsPercent['rolPercent'] = round($model->rol_por * 100 / $percentages['rol100']);
        if($totalsPercent['rolPercent']>99)
            $totalsPercent['rolPercent']=100;

        $totalsPercent['graPercent'] = round($model->gra_vid * 100 / $percentages['gra100']);
        if($totalsPercent['graPercent']>99)
            $totalsPercent['graPercent']=100;

        $totalsPercent['wriPercent'] = round($model->wri_por * 100 / $percentages['wri100']);
        if($totalsPercent['wriPercent']>99)
            $totalsPercent['wriPercent']=100;

        $totalsPercent['newPercent'] = round($model->new_por * 100 / $percentages['new100']);
        if($totalsPercent['newPercent']>99)
            $totalsPercent['newPercent']=100;

        $totalsPercent['spePercent'] = round($model->spe_por * 100 / $percentages['spe100']);
        if($totalsPercent['spePercent']>99)
            $totalsPercent['spePercent']=100;


        //Total percent
        $totalsPercent['total'] = floor(($totalsPercent['sitPercent'] +  $totalsPercent['stuPercent'] + $totalsPercent['dicPercent'] +
                $totalsPercent['rolPercent'] + $totalsPercent['graPercent'] + $totalsPercent['wriPercent'] + $totalsPercent['newPercent'] + $totalsPercent['spePercent']) / 8);

        return $totalsPercent;

    }


    //
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new b2bFollowup;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['b2bFollowup']))
		{
			$model->attributes=$_POST['b2bFollowup'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->ID_followup));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['b2bFollowup']))
		{
			$model->attributes=$_POST['b2bFollowup'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->ID_followup));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('b2bFollowup');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
        if(isset($_POST['company']))
        {
            $company = $_POST['company'];
            $level = $_POST['level'];
            $criterio = $_POST['criterio'];
        }
        else if(isset($_GET['company']))
        {
            $company = $_GET['company'];
            $level = $_GET['level'];
            $criterio = $_GET['criterio'];
        }
        else
        {
            $company = $this->default;
            $level = $this->levelList['Beginners'];
            $criterio = '';
        }

		$model=new b2bFollowup('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['b2bFollowup']))
			$model->attributes=$_GET['b2bFollowup'];

        $contador = $model->countAll($level, $company, $criterio);
        $pages = new CPagination($contador);
        $pages->setPageSize(25);

		$this->render('admin',array(
			'model'=>$model, 'company'=>$company, 'list'=>$this->list, 'level'=>$level, 'levelList'=>$this->levelList, 'pages'=>$pages, 'criterio'=>$criterio,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=b2bFollowup::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='b2b-followup-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionExport()
    {
        if(isset($_GET['company']))
        {
            $company = $_GET['company'];
            $level = $_GET['level'];
            $criterio = $_GET['criterio'];
            $complete =  $_GET['complete'];
        }
        else
        {
            $company = $this->default;
            $level = $this->levelList['Beginners'];
            $criterio = '';
            $complete =0;
        }
            $model = new b2bFollowup();
            $dataReader = $model->searchExport($level, $company, $criterio, $complete);
            $data = array();
            $xls = new JPhpCsv('UTF-8', false, 'Users');
            $flagFirst=true;

            foreach($dataReader as $row)
            {
                if($flagFirst)
                {
                    $flagFirst= false;
                    $theRow=array();
                    foreach ($row as $key =>$value)
                    {
                        if($key!=='tmpCurrentWatchedVideoClass')
                        {
                            array_push($theRow, $key);
                        }
                    }
                    array_push($data, $theRow);
                }
                $theRow=array();
                foreach ($row as $key =>$value)
                {
                    if($key!=='tmpCurrentWatchedVideoClass')
                    {
                        array_push($theRow, $value);
                    }
                }
                array_push($data, $theRow);
            }
            if($flagFirst==true)
            {
                $theRow=array();
                array_push($theRow, "DATA NOT FOUND (Data not exported)");
                array_push($data, $theRow);
            }
            $xls->addArray($data);
            $xls->generateXML("StudyReportExtended-".substr(strtoupper($company),8)."-".$level."-".date("d-m-Y"));
    }

    public function actionExportCompact()
    {
        if(isset($_GET['company']))
        {
            $company = $_GET['company'];
            $level = $_GET['level'];
            $criterio = $_GET['criterio'];
        }
        else
        {
            $company = $this->default;
            $level = $this->levelList['Beginners'];
            $criterio = '';
        }
        $model = new b2bFollowup();
        $dataReader = $model->searchExportCompact($level, $company, $criterio);
        $data = array();
        $xls = new JPhpCsv('UTF-8', false, 'Users');
        $flagFirst=true;

        foreach($dataReader as $row)
        {
            if($flagFirst)
            {
                $flagFirst= false;
                $theRow=array();
                foreach ($row as $key =>$value)
                {
                    if($key!=='tmpCurrentWatchedVideoClass')
                    {
                        array_push($theRow, $key);
                    }
                }
                array_push($data, $theRow);
            }
            $theRow=array();
            foreach ($row as $key =>$value)
            {
                if($key!=='tmpCurrentWatchedVideoClass')
                {
                    array_push($theRow, $value);
                }
            }
            array_push($data, $theRow);
        }
        if($flagFirst==true)
        {
            $theRow=array();
            array_push($theRow, "DATA NOT FOUND (Data not exported)");
            array_push($data, $theRow);
        }
        $xls->addArray($data);
        $xls->generateXML("StudyReportCompact-".substr(strtoupper($company),8)."-".$level."-".date("d-m-Y"));
    }
}
