<?php

class AbaUserRemovedProgressController extends AbaController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
          'accessControl', // perform access control for CRUD operations
          'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
          array(
            'allow',
            'actions' => array('view', 'create', 'update', 'delete', 'index', 'admin'),
            'users' => array('root', 'marketing'),
          ),
          array(
            'allow',
            'actions' => array('admin', 'index', 'view', 'create', 'update', 'delete'),
            'users' => array('support'),
          ),
          array(
            'allow',
            'actions' => array('admin', 'index', 'view', 'create', 'update', 'delete'),
            'users' => array('marketing'),
          ),
          array(
            'deny', // deny all users
            'users' => array('*'),
          ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id, $errMsg = "")
    {
        $oModel = $this->loadModel($id);

        $user = AbaUser::model()->findByPk($oModel->userId);

        $this->render(
          'view',
          array(
            'user' => $user,
            'model' => $oModel,
            'errMsg' => $errMsg,
          )
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($userId)
    {
        $user = AbaUser::model()->findByPk($userId);

        $model = new LogUserProgressRemoved();

        $errMsg = "";

        if (isset($_POST['LogUserProgressRemoved'])) {

            $_POST['LogUserProgressRemoved']['dateAdd'] = date("Y-m-d h:i:s", time());

            $stRemoveProgressResponse = $model->removeAllUserProgress($userId);

            if (isset($stRemoveProgressResponse["removedUnits"])) {
                $_POST['LogUserProgressRemoved']['units'] = $stRemoveProgressResponse["removedUnits"];
            }

            if (isset($stRemoveProgressResponse["success"]) AND !$stRemoveProgressResponse["success"]) {
                $errMsg = $stRemoveProgressResponse["removedUnits"];
            }

            $model->attributes = $_POST['LogUserProgressRemoved'];

            if ($model->removedProgressId == '') {
                $model->removedProgressId = null;
            }

            $model->dateAdd = $_POST['LogUserProgressRemoved']['dateAdd'];

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->removedProgressId, 'errMsg' => $errMsg));
            }
        }

        $this->render(
          'create',
          array(
            'user' => $user,
            'model' => $model,
            'wAction' => "create"
          )
        );
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->actionView($id);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->actionView($id);
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('LogUserProgressRemoved');
        $this->render(
          'index',
          array(
            'dataProvider' => $dataProvider,
            'wAction' => "index",
          )
        );
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new LogUserProgressRemoved('search');
        $model->unsetAttributes(); // clear any default values

        if (isset($_GET['LogUserProgressRemoved'])) {
            $model->attributes = $_GET['LogUserProgressRemoved'];
        }

        $this->render(
          'admin',
          array(
            'model' => $model,
            'wAction' => "admin",
          )
        );
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = LogUserProgressRemoved::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'aba-userremovedprogress-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * @param $sUnits
     *
     * @return mixed
     */
    protected static function formatUnits($sUnits)
    {
        $sUnits = trim(str_replace(" ", "", $sUnits));

        return $sUnits;
    }

}
