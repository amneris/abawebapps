<?php
class AbaExternalSalesController extends AbaController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/stats';

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */

    /*public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }*/

    public function accessRules()
    {
        return array(

            array('allow',
                'actions'=>array('totalSalesDashboardMobil'),
                'users'=>array('root'),
            ),

            array('allow',
                'actions'=>array('totalSalesDashboardMobil'),
                'users'=>array('marketing'),
            ),

            array('allow',
                'actions'=>array('totalSalesDashboardMobil'),
                'users'=>array('support'),
            ),

            array('allow',
                'actions'=>array(),
                'users'=>array('teacher'),
            ),

            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $this->layout='//layouts/stats';
        $this->actionTotalSalesDashboardMobil();
    }


    public function actionTotalSalesDashboardMobil()
    {
        //day of the week 1..7 where 1 is equal to monday
        $dw = date('N');
        //$dw = 5;
        $endDate = date('Y-m-d');
        //$endDate = '2013-05-03';

        if($dw>1)
        {
            $weekBefore = strtotime ('-'.($dw-1).'day', strtotime($endDate));
            $weekBefore = date ('Y-m-d', $weekBefore);
        }
        else
            $weekBefore = date('Y-m-d');

        //daily count
        $totalDaily = AbaStatistics::model()->totalSalesDashboardCount($endDate,$endDate);

        //weekly count
        $totalWeekly = AbaStatistics::model()->totalSalesDashboardCount($weekBefore,$endDate);

        $this->render('index',array('totalDaily'=>$totalDaily, 'totalWeekly'=>$totalWeekly, 'endDate'=>$endDate, 'weekBefore'=>$weekBefore, 'dw'=>$dw));
    }


}
