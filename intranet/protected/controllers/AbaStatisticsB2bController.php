<?php
class AbaStatisticsB2bController extends AbaController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column1';

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules()
    {
        return array(

            array('allow',
                'actions'=>array('totalSalesDashboard', 'totalSalesDashboardMobil', 'totalSalesDashboardByCountry', 'registrationDashboard','totalSalesDashboardByPartner'),
                'users'=>array('root'),
            ),

            array('allow',
                'actions'=>array('totalSalesDashboard', 'totalSalesDashboardMobil', 'totalSalesDashboardByCountry', 'registrationDashboard','totalSalesDashboardByPartner'),
                'users'=>array('marketing'),
            ),

            array('allow',
                'actions'=>array('totalSalesDashboard', 'totalSalesDashboardMobil', 'totalSalesDashboardByCountry', 'registrationDashboard','totalSalesDashboardByPartner'),
                'users'=>array('support'),
            ),

            array('allow',
                'actions'=>array(),
                'users'=>array('teacher'),
            ),

            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $this->layout='//layouts/stats';
        $this->actionTotalSalesDashboardMobil();
    }

    //LUIS: the next methods are to show de sales dashboard
    public function actionTotalSalesDashboard()
    {
        if(isset($_POST['startDate']) && isset($_POST['endDate']))
        {
            $startDate = $_POST['startDate'];
            $endDate = $_POST['endDate'];
        }
        else
        {
            //$startDate = '2013-05-15';
            //$endDate = '2013-05-16';
            $startDate = date('Y-m-d');
            $endDate = $startDate;
        }

        $total = AbaStatisticsB2b::model()->totalSalesDashboardCount($startDate,$endDate);
        $this->render('totalSalesDashboard',array('total'=>$total, 'startDate'=>$startDate, 'endDate'=>$endDate));
    }

    public function actionTotalSalesDashboardMobil()
    {
        //day of the week 1..7 where 1 is equal to monday
        $dw = date('N');
        //$dw = 6;
        $endDate = date('Y-m-d');
        //$endDate = '2014-02-15';

        if($dw>1)
        {
            $weekBefore = strtotime ('-'.($dw-1).'day', strtotime($endDate));
            $weekBefore = date ('Y-m-d', $weekBefore);
        }
        else
            $weekBefore = date('Y-m-d');

        //daily count
        $totalDaily = AbaStatisticsB2b::model()->totalSalesDashboardCount($endDate,$endDate);

        //weekly count
        $totalWeekly = AbaStatisticsB2b::model()->totalSalesDashboardCount($weekBefore,$endDate);

        $this->render('totalSalesDashboardMobil',array('totalDaily'=>$totalDaily, 'totalWeekly'=>$totalWeekly, 'endDate'=>$endDate, 'weekBefore'=>$weekBefore, 'dw'=>$dw));
    }

    public function actionTotalSalesDashboardByCountry()
    {
        if(isset($_POST['startDate']) && isset($_POST['endDate']))
        {
            $startDate = $_POST['startDate'];
            $endDate = $_POST['endDate'];
        }
        else
        {
            //$startDate = '2013-05-15';
            //$endDate = '2013-05-16';
            $startDate = date('Y-m-d');
            $endDate = $startDate;
        }

        //$total = AbaStatisticsB2b::model()->totalSalesDashboardCount($startDate,$endDate);
        $totalByCountry = AbaStatisticsB2b::model()->totalSalesDashboardCountByCountry($startDate,$endDate);

        $this->render('totalSalesDashboardByCountry',array(/*'total'=>$total,*/ 'startDate'=>$startDate, 'endDate'=>$endDate, 'totalByCountry'=>$totalByCountry));
    }

    public function actionRegistrationDashboard()
    {

        if(isset($_POST['startDate']) && isset($_POST['endDate']))
        {
            $startDate = $_POST['startDate'];
            $endDate = $_POST['endDate'];
        }
        else
        {
            //$startDate = '2013-05-15';
            //$endDate = '2013-05-16';
            $startDate = date('Y-m-d');
            $endDate = $startDate;
        }
        if(isset($_POST['flashSales']))
             $flashSales = $_POST['flashSales'];
        else
            $flashSales = false;

        //if argument is true, the return of the method will return with flash sales include.
        $channels = AbaStatisticsB2b::model()->getChannels($flashSales);

        $this->render('registrationDashboard',array('channels'=>$channels, 'startDate'=>$startDate, 'endDate'=>$endDate, 'flashSales'=>$flashSales));
    }

    public function getUsersByChannel($startDate, $endDate, $channel)
    {
        return AbaStatisticsB2b::model()->registrationDashboard($startDate, $endDate, $channel);
    }

    //new
    public function actionTotalSalesDashboardByPartner()
    {
        if(isset($_POST['startDate']) && isset($_POST['endDate']))
        {
            $startDate = $_POST['startDate'];
            $endDate = $_POST['endDate'];
        }
        else
        {
            //$startDate = '2013-05-15';
            //$endDate = '2013-05-15';
            $startDate = date('Y-m-d');
            $endDate = $startDate;
        }
        if(isset($_POST['entryDate']))
            $entryDate = $_POST['entryDate'];
        else
            $entryDate = false;

        $totalByPartner = AbaStatisticsB2b::model()->totalSalesDashboardCountByPartner($startDate,$endDate,$entryDate);
        $this->render('totalSalesDashboardByPartner',array('startDate'=>$startDate, 'endDate'=>$endDate, 'totalByPartner'=>$totalByPartner, 'entryDate'=>$entryDate));
    }


}
