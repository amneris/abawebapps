<?php

class AbaPaymentsControlCheckController extends AbaController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
		
		
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete', 'view', 'index', 'update', 'export'),
				'users'=>array('root'),
			),
            
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete', 'view', 'index', 'update', 'export'),
				'users'=>array('support'),
			),

            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete', 'view', 'index', 'update', 'export'),
                'users'=>array('marketing'),
            ),

            array('deny',  // deny all users
                'users'=>array('*'),
            ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new AbaPaymentsControlCheck;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AbaPaymentsControlCheck']))
		{
			$model->attributes=$_POST['AbaPaymentsControlCheck'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AbaPaymentsControlCheck']))
		{
			$model->attributes=$_POST['AbaPaymentsControlCheck'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('AbaPaymentsControlCheck');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
        $model=new AbaPaymentsControlCheck();
		$model->unsetAttributes();  // clear any default values
        if(isset($_GET['AbaPaymentsControlCheck']))
        {

            $model->attributes=$_GET['AbaPaymentsControlCheck'];
            $array=$_GET['AbaPaymentsControlCheck'];
            if(isset($array['endDate'])){
                 $model->endDate= $endDate = $array['endDate'];
            }
            if(isset($array['startDate'])){
                $model->startDate = $startDate = $array['startDate'];
            }
            if(isset($array['showReviewed'])){
                $showReviewed = $array['showReviewed'];
                $model->showReviewed = $showReviewed;
            }
            if(isset($array['idReviewed']) && $array['idReviewed']!="")
            {
                AbaPaymentsControlCheck::setReviewed($array['idReviewed'], $array['reviewedValue']);
            }
        }
        else
        {
            $model->startDate= $startDate=HeDate::calculateMonth(-1);
            $model->endDate=$endDate=HeDate::tomorrow();
            $showReviewed=0;
        }

	    $this->render('admin',array('model'=>$model,
                                    'startDate'=> $startDate,
                                    'endDate'=>$endDate,
                                    'showReviewed'=>$showReviewed
		                            ));
	}

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     *
     * @param int $id the ID of the model to be loaded
     *
     * @throws CHttpException
     * @return AbaPaymentsControlCheck
     */
	public function loadModel($id)
	{
		$model=AbaPaymentsControlCheck::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='aba-payments-control-check-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    /**
     * @param string $startDate
     * @param string $endDate
     * @param int    $showReviewed
     */
    public function actionExport($startDate='', $endDate='', $showReviewed=0)
    {
        if(isset($_POST['AbaPaymentsControlCheck']))
        {
            $model = new AbaPaymentsControlCheck;
            $attributes = $_POST['AbaPaymentsControlCheck'];
            $dataReader = $model->searchExport($attributes);
            $data = array();
            $xls = new JPhpCsv('UTF-8', false, 'Paylines Logs');
            $flagFirst = true;
            while(($row = $dataReader->read()) !== false)
            {
                if($flagFirst)
                {
                    $flagFirst = false;
                    $theRow = array();
                    foreach ($row as $key =>$value)
                    {
                        array_push($theRow, $key);
                    }
                    array_push($data, $theRow);
                }
                $theRow = array();
                foreach ($row as $key =>$value)
                {
                    array_push($theRow, $value);
                }
                array_push($data, $theRow);
            }
            if($flagFirst == true)
            {
                $theRow = array();
                array_push($theRow, "DATA NOT FOUND (Data not exported)");
                array_push($data, $theRow);
            }
            $xls->addArray($data);
            $startDate = $attributes['startDate']=="" ? HeDate::resetDate(true) : $attributes['startDate'];
            $endDate = $attributes['endDate']==""? HeDate::tomorrow(true) : $attributes['endDate'];
            $xls->generateXML("paylinesLogsFrom_".substr($startDate,0,10)."_to_".substr($endDate,0,10)."_".date("His",time()));
        }
        else
        {
            $model = new AbaPaymentsControlCheck;
            $this->render('excel',array(
                                    'model'=>$model,
                                    'startDate'=>$startDate,
                                    'endDate'=>$endDate,
                                    'showReviewed'=>$showReviewed,
                                    ));
        }
    }
}
