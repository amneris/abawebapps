<?php

class AbaB2bEnterpriseController extends AbaController
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(

            array('allow',
                'actions'=>array('admin', 'export'),
                'users'=>array('root'),
            ),

            array('allow',
                'actions'=>array('admin', 'export'),
                'users'=>array('marketing'),
            ),

            array('allow',
                'actions'=>array(),
                'users'=>array('support'),
            ),

            array('allow',
                'actions'=>array(),
                'users'=>array('teacher'),
            ),

            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    public function actionAdmin()
    {
        $channelList = AbaChannelList::model()->getAllChannel(Yii::t('app','All channels'));

        $idPartnerEnterprise='';
        if(isset($_GET['idPartnerEnterprise']))
        {
            $idPartnerEnterprise = $_GET['idPartnerEnterprise'];
        }

        $model=new AbaB2bEnterprise('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['AbaB2bEnterprise']))
            $model->attributes=$_GET['AbaB2bEnterprise'];

        $counter = $model->countAll($idPartnerEnterprise);
        $pages = new CPagination($counter);
        $pages->setPageSize(25);

        $this->render('admin',array(
            'model'=>$model, 'pages'=>$pages, 'channel'=>$idPartnerEnterprise, 'channelList'=>$channelList,
        ));

    }

    public function actionExport()
    {
        $idPartnerEnterprise='';
        if(isset($_GET['channel']))
        {
            $idPartnerEnterprise = $_GET['channel'];
        }

        $model = new AbaB2bEnterprise();
        $dataReader = $model->searchExport($idPartnerEnterprise);
        $data = array();
        $xls = new JPhpCsv('UTF-8', false, 'Users');
        $flagFirst=true;

        foreach($dataReader as $row)
        {
            if($flagFirst)
            {
                $flagFirst= false;
                $theRow=array();
                foreach ($row as $key =>$value)
                {
                    if($key!=='tmpChannel')
                    {
                        array_push($theRow, $key);
                    }
                }
                array_push($data, $theRow);
            }
            $theRow=array();
            foreach ($row as $key =>$value)
            {
                if($key!=='tmpChannel')
                {
                    array_push($theRow, $value);
                }
            }
            array_push($data, $theRow);
        }
        if($flagFirst==true)
        {
            $theRow=array();
            array_push($theRow, "DATA NOT FOUND (Data not exported)");
            array_push($data, $theRow);
        }
        $xls->addArray($data);
        $xls->generateXML("enterpriseChannelsList-".date("d-m-Y"));

    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Enterprise the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=AbaB2bEnterprise::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Enterprise $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='enterprise-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
