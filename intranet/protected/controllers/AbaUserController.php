<?php

class AbaUserController extends AbaController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(

            array('allow',
                'actions' => array('export', 'admin_id', 'view', 'update', 'delete', 'index', 'admin', 'expressCertificates', 'ViewByEmail', 'unsubscribe', 'cancelsubrefund'),
                'users' => array('root'),
            ),

            array('allow',
                'actions' => array('export', 'admin_id', 'view', 'update', 'delete', 'index', 'admin', 'expressCertificates', 'ViewByEmail'),
                'users' => array('marketing'),
            ),

            array('allow',
                'actions' => array('export', 'admin_id', 'view', 'update', 'delete', 'index', 'admin', 'expressCertificates', 'ViewByEmail', 'unsubscribe', 'cancelsubrefund'),
                'users' => array('support'),
            ),

            array('allow',
                'actions' => array('view', 'index', 'admin',),
                'users' => array('teacher'),
            ),

            array('deny',
                'users' => array('*'),
            ),
        );
    }

    /**
     * Public Action URL /index.php?r=abaUser/export
     */
    public function actionExport() {
        if(isset($_POST['AbaUser'])) {
            $model = new AbaUser;
            $attributes = $_POST['AbaUser'];
            unset($_POST);
            $dataReader = $model->searchExport($attributes);
            unset($model);

            $data = array();
            $flagFirst = true;

            while(($row = $dataReader->read()) !== false) {
                if($flagFirst) {
                    $flagFirst = false;
                    $theRow = array();
                    foreach($row as $key => $value) {
                        if($key !== 'tmpCurrentWatchedVideoClass') {
                            $theRow[] = $key;
                        }
                    }
                    $data[] = $theRow;
                }
                $theRow = array();
                foreach($row as $key => $value) {
                    if($key !== 'tmpCurrentWatchedVideoClass') {
                        $theRow[] = $value;
                    }
                }

                $data[] = $theRow;
            }
            unset($dataReader);

            if($flagFirst == true) {
                $theRow = array();
                $theRow[] = "DATA NOT FOUND(Data not exported)";
                $data[] = $theRow;
            }

            $xls = new JPhpCsv('UTF-8', false, 'Users');
//            $xls->addArray($data);
//            unset($data);
            $startDate = $attributes['startDate'] == "" ? HeDate::resetDate(true) : $attributes['startDate'];
            $endDate = $attributes['endDate'] == "" ? HeDate::tomorrow(true) : $attributes['endDate'];
//            $xls->generateXML("users_" . substr($startDate, 0, 10) . "_to_" . substr($endDate, 0, 10));

            $xls->createFile($data, "users_" . substr($startDate, 0, 10) . "_to_" . substr($endDate, 0, 10));
            unset($data);

            unset($xls);
        } else {
            $model = new AbaUser;
            $extrainfo = array('levelsDescription' => HeList::levelsDescription(),
                'userTypeDescription' => HeList::userTypeDescription()
            );
            $this->render('excel', array(
                'model' => $model,
                'extrainfo' => $extrainfo,
            ));
        }
    }


    public function loadModelWidthPayments($id) {
        $model = AbaUser::model()->with('payments')->find("userid=" . $id);
        if($model === NULL)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionAdmin_id($id) {
        $model = AbaUser::model()->findByPk($id);
        try {
            $id =/*"=".*/
                $model->id;
            $model->unsetAttributes();  // clear any default values
            $model->id = $id;
            $this->render('admin', array(
                'model' => $model,
            ));
        } catch(Exception $e) {
            throw new CHttpException(404, 'User not found user id => ' . $id);
        }
    }

    /**
     * Displays a particular model.
     *
     * @param integer $id the ID of the model to be displayed
     * @param bool $showUnsubscribe
     *
     */
    public function actionView($id, $showUnsubscribe = false) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
            'showUnsubscribe' => $showUnsubscribe,
        ));
    }

    /**
     * @param integer $userId
     * @param bool $confirm
     */
    public function actionUnsubscribe($userId, $confirm = false) {
        $confirm = intval($confirm);
        if($confirm) {
            /* @var AbaUser $moUser */
            $moUser = $this->loadModel($userId);
            $moUser->unsubscribe();
            HeWebServices::unsubscribe($userId);
            $this->redirect(Yii::app()->createUrl("abaUser/admin"));
        } else {
            $this->actionView($userId, true);
        }
    }

    public function actionViewByEmail($email) {
        //$model=AbaUser::model()->findByPk($email);
        $model = AbaUser::model()->findByAttributes(array('email' => $email));
        if($model === NULL)
            throw new CHttpException(404, 'The requested page does not exist.');

        $this->render('view', array(
            'model' => $model,));


    }

    /**
     * Function called to cancel a pending payment and refunding the latest successful payment and changing user type
     * from premium to free. If there is no payment to cancel it is still possible to perform a refund and vice versa.
     *
     * @param $idUser
     * @throws CHttpException
     */
    public function actionCancelsubrefund($idUser) {
        $user = CmAbaUser::model()->findByPk($idUser);
        $pendingPayment = AbaPayments::getModelCancelFailUser($idUser);
        $refundPayment = new AbaPayments();
        $paymentId = $refundPayment->searchLastPayment($idUser, PAY_SUCCESS, true);
        if($paymentId) {
            $refundPayment = $refundPayment->loadModelById($paymentId);
        }

        if($refundPayment->paySuppExtId == PAY_SUPPLIER_ADYEN OR $refundPayment->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP) {
            throw new CHttpException(404, '"Cancel & Refund" is not possible for ADYEN TRANSACTION and user id => ' . $idUser);
        }

        if(isset($_POST['AbaPayments']) || isset($_POST['refundPayment'])) {
            // 1: Cancel Existing Payment in DB (A pending subscription payment)
            $successCancel = false;
            if($pendingPayment) {
                $paymentToCancel = new AbaPayments;
                $paymentToCancel->attributes = $_POST['AbaPayments'];
                $successCancel = AbaPayments::cancelUser($paymentToCancel);
            }
            // 2: Refund Payment in DB (A successful payment not older than 90 days)
            $paymentToRefund = new AbaPayments();
            $paymentToRefund->attributes = $_POST['refundPayment'];


            if($paymentToRefund->paySuppExtId == PAY_SUPPLIER_ADYEN OR $paymentToRefund->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP) {
                throw new CHttpException(404, '"Cancel & Refund" is not possible for ADYEN TRANSACTION and user id => ' . $idUser);
            }


            $sendToPayGateway = false;
            $successRefund = true;
            if(array_key_exists('sendToPayGateway', $_POST)) {
                $sendToPayGateway = intval($_POST['sendToPayGateway']);
            }
            $aRetCampusRefund = array("paySuppExtUnid" => $paymentToRefund->paySuppExtUniId,
                "paySuppOrderId" => $paymentToRefund->paySuppOrderId,
                "idPayment" => "A" . (AbaPayments::generateIdPayment($paymentToRefund->userId)));
            if(!HeQuery::testRegisterSuccess($paymentToRefund->userId, $paymentToRefund->id)) {
                $user->addError("paySuppOrderId", "A refund has already been registered for this payment. Perhaps someone else just did it? Please contact IT if you think this is not the case.");
                $this->render('cancelsubrefund', array('user' => $user,
                    'pendingPayment' => $pendingPayment,
                    'refundPayment' => $refundPayment,
                    'enableCancelRefund' => false,
                    'enableCancel' => false,
                    'enableRefund' => false));
                return;
            }
            if(intval($sendToPayGateway)) {
                $aRetCampusRefund = HeWebServices::refundPayment($paymentToRefund->id,
                    floatval($paymentToRefund->amountPrice),
                    $paymentToRefund->paySuppOrderId);
            }
            if(is_array($aRetCampusRefund) && count($aRetCampusRefund) == 3) {

                //
                //#4986
                $oSuccess = array("success" => true, "msg" => "");
                if($sendToPayGateway) {
                    $oSuccess = AbaPayments::checkAdyenRefundNotification($paymentToRefund, $aRetCampusRefund);
                }

                if($oSuccess["success"]) {
                    if(AbaPayments::refundUser($paymentToRefund, $paymentToRefund->id, $aRetCampusRefund["idPayment"],
                        $aRetCampusRefund["paySuppExtUnid"],
                        $aRetCampusRefund["paySuppOrderId"])
                    ) {
                        $idUser = $paymentToRefund->userId;
                        $paymentToRefund->unsetAttributes(); // clear no desirable values
                        $successRefund = true;
                    } else {
                        throw new CHttpException(404, 'Refund is not possible for user id. => ' . $idUser);
                    }
                }
                else {
                    throw new CHttpException(404, 'Refund is not possible for user id => ' . $idUser . ". " . $oSuccess["msg"]);
                }

            } else {
                if(is_array($aRetCampusRefund)) {
                    $errorCode = array_keys($aRetCampusRefund);
                    $errorRefund = $errorCode[0] . " " . $aRetCampusRefund[$errorCode[0]];
                } else {
                    $errorRefund = "Campus web service refundPayment has returned an error.";
                }
                $user->addError("paySuppOrderId", "Automatic refund could not be processed. Error description:" .
                    $errorRefund);
                $this->render('cancelsubrefund', array('user' => $user,
                    'pendingPayment' => $pendingPayment,
                    'refundPayment' => $refundPayment,
                    'enableCancelRefund' => $enableCancelRefund,
                    'enableCancel' => $enableCancel,
                    'enableRefund' => $enableRefund));
            }
            if($successCancel == true && $successRefund == true) {
                $newUser = new CmAbaUser();
                $newUser->populateUserById($idUser);
                $newUser->userType = 1;
                $newUser->save(false);
                $this->redirect(array('view', 'id' => $idUser));
            } else {
                $user->addError("user", "Could not perform operation, if necessary contact IT to resolve the issue");
                $this->render('cancelsubrefund', array('user' => $user,
                    'pendingPayment' => $pendingPayment,
                    'refundPayment' => $refundPayment,
                    'enableCancelRefund' => $enableCancelRefund,
                    'enableCancel' => $enableCancel,
                    'enableRefund' => $enableRefund));
            }
        } else {
            $enableCancel = true;
            $enableRefund = true;
            $enableCancelRefund = true;
            if($user->userType <> 2) {
                $user->addError('user', 'Refund and Cancel can only be executed for Premium users!');
                $enableCancelRefund = false;
                $enableRefund = false;
                $enableCancel = false;
            } else {
                if($pendingPayment) {
                    if(!HePayments::isCreditDebitCard($pendingPayment)) {
                        $user->addError('user', 'Operation(Cancel) only allowed for Users with a pending Credit Card payment');
                        $enableCancelRefund = false;
                        $enableCancel = false;
                    }
                    if(HePayments::isB2b($pendingPayment)) {
                        $user->addError('user', 'Operation(Cancel) not available for B2B users.');
                        $enableCancelRefund = false;
                        $enableCancel = false;
                    }
                } else {
                    $user->addError('user', 'Operation(Cancel) only accessible if user has a pending payment.');
                    $enableCancelRefund = false;
                    $enableCancel = false;
                }
                if($paymentId !== '') {
                    if(!HePayments::isCandidateForRefund($refundPayment, $idUser)) {
                        $user->addError('user', 'Operation(Refund) only allowed for non B2B Users with a successful credit/debit card payment not older than 90 days that still has not been refunded');
                        $enableCancelRefund = false;
                        $enableRefund = false;
                    }
                } else {
                    $user->addError('user', 'No payment to refund');
                    $enableCancelRefund = false;
                    $enableRefund = false;
                }
            }
            $this->render('cancelsubrefund', array('user' => $user,
                'pendingPayment' => $pendingPayment,
                'refundPayment' => $refundPayment,
                'enableCancelRefund' => $enableCancelRefund,
                'enableCancel' => $enableCancel,
                'enableRefund' => $enableRefund));
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new AbaUser;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['AbaUser'])) {
            $model->attributes = $_POST['AbaUser'];
            if($model->save()) {
                //TO DO: call to info webservice alter user profile
                $this->redirect(array('view', 'id' => $model->id));
            }
        }
        $extrainfo = HeList::getRenderNames();
        $this->render('create', array(
            'model' => $model,
            'extrainfo' => $extrainfo,
        ));
    }


    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     *
     * @throws CHttpException
     */
    public function actionUpdate($id) {
        /* @var AbaUser $moOldUser */
        $moOldUser = $this->loadModel($id);
        /* @var AbaUser $newModel */
        $newModel = $this->loadModel($id);
        /* @var AbaPayments $moLastPayment */
        $moLastPayment = new AbaPayments();

        $paymentId = $moLastPayment->searchLastPayment($moOldUser->id, PAY_SUCCESS, true);
        if($paymentId) {
            $moLastPayment = $moLastPayment->loadModelById($paymentId);
        }

        // If the form is being sent with data:
        if(isset($_POST['AbaUser'])) {
            $canSave = 1;
            $doCallWebServiceChangePassword = false;
            $doChangeEmail = false;

            $newModel->attributes = $_POST['AbaUser'];

            // CASE CHANGE PASSWORD:******************
            if(trim($moOldUser->password) !== trim($newModel->password)) {
                $theNoEncodedPassword = $newModel->password;
                $newModel->password = md5($theNoEncodedPassword);
                $doCallWebServiceChangePassword = true;
            }
            // CASE CHANGE E-MAIL:*********************
            if($moOldUser->email !== $newModel->email) {
                $doChangeEmail = true;
            }
            $doChangeUserType = false;
            // CASE CHANGE USERTYPE FROM PREMIUM TO FREE:
            if($moOldUser->userType !== $newModel->userType &&
                $moOldUser->userType == PREMIUM && $newModel->userType == FREE
            ) {
                $doChangeUserType = true;
                $newModel->cancelReason = PAY_CANCEL_USER;
            }

            // Check in case of existing e-mail cancel the save method:
            if($doChangeEmail) {
                $moAnyUserEmail = AbaUser::model()->findByAttributes(array('email' => $newModel->email));
                if($moAnyUserEmail == NULL) {
                    $canSave = 1;
                } else {
                    $canSave = 0;
                    //If the new mail exists... we show information about the user
                    Yii::app()->user->setFlash('delError', 'El email <b>' . $newModel->email . '</b> ya existe en la base de datos.');
                    $this->redirect(array('update', 'id' => $moOldUser->id));
                }
            }

            if($canSave) {
                if($newModel->save()) {
                    if($doCallWebServiceChangePassword) {
                        if(!HeWebServices::ChangePassword($newModel->id, $theNoEncodedPassword)) {
                            throw new CHttpException(404, 'Can\'t send the change password notification to Selligent');
                        }
                    }
                    $this->redirect(array('view', 'id' => $newModel->id));
                }
            }
        }

        $extrainfo = HeList::getRenderNames();
        $this->render('update', array(
            'model' => $moOldUser,
            'moLastPayment' => $moLastPayment,
            'extrainfo' => $extrainfo,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('AbaUser');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new AbaUser('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['AbaUser'])) {
            $model->attributes = $_GET['AbaUser'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     *
     * @param integer $id the ID of the model to be loaded
     *
     * @throws CHttpException
     * @return \CActiveRecord
     */
    public function loadModel($id) {

        $model = AbaUser::model()->findByPk($id);

        if($model === NULL) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        //#5618
        if(isset($model->gender) AND trim($model->gender) == '') {
            $model->gender = null;
        }

        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if(isset($_POST['ajax']) && $_POST['ajax'] === 'aba-user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionExpressCertificates() {
        if(Yii::app()->request->getPost('AbaCertificateForm')) {
            $model = new AbaCertificateForm();
            $model->attributes = Yii::app()->request->getPost('AbaCertificateForm');
            if($model->validate()) {
                $userCertificados = AbaUser::model()->findByAttributes(array('email' => $model->email));
                if(!$userCertificados) die('<p>El usuario no existe o no tiene nombre</p>');
                trim($userCertificados->attributes['name']) == "" ? die('<p>el usuario no existe o no tiene nombre</p>') : '';
                $finalStringLevel = "";
                $words = "";
                $hours = "";
                $color = "";
                switch($model->level) {
                    case 1:
                        $finalStringLevel = "Beginners (A1)*";
                        $words = "1172";
                        $hours = "72";
                        $color = "0852a0";
                        $footerString = array('Student understands words and simple sentences in relation to his or her immediate surroundings.',
                            'Student is able to use simple sentences to describe where he or she lives and to describe people.',
                            'Student can write simple short messages and is able to fill out a personal data form (name, nationality, address).',
                            'Student understands basic sentences, for example on notices or in catalogues.');
                        break;
                    case 2:
                        $finalStringLevel = "Lower intermediate - A2*";
                        $words = "622";
                        $hours = "86";
                        $color = "0852a0";
                        $footerString = array('Student understands the core meaning of short, clear, and simple statements and announcements.',
                            'Student can use a number of phrases and expressions to describe his or her family or other people.',
                            'Student is able to produce simple texts, for example, a simple personal letter to thank someone.',
                            'Student is able to read short texts and to find expected information in daily materials such as flyers, menus and train timetables.');
                        break;
                    case 3:
                        $finalStringLevel = "Intermediate - B1*"; 
                        $words = "634";
                        $hours = "101";
                        $color = "0852a0";
                        $footerString = array('Student is able to understand the main message of a standard speech. He or she understands the main points of radio and TV programmes.',
                            'Student can connect phrases in a simple way to describe his or her experiences, dreams or hopes.',
                            'Student can produce a simple continuous text on a topic that is well known to him or her.',
                            'Student understands general text and description of events, feelings, and wishes often expressed in personal letters.');
                        break;
                    case 4:
                        $finalStringLevel = "Upper Intermediate - B2*";
                        $words = "568";
                        $hours = "115";
                        $color = "0852a0";
                        $footerString = array('Student understands most TV news and the majority of films in standard dialect.',
                            'Student can interact with a degree of fluency with native speakers and can explain his or her point of view.',
                            'Student is able to produce a detailed text about various topics as well as to write essays or compositions.',
                            'Student can read articles concerned with current problems and can also understand modern literary works.');
                        break;
                    case 5:
                        $finalStringLevel = "Advanced - B2-C1*";
                        $words = "584";
                        $hours = "130";
                        $color = "0852a0";
                        $footerString = array('Student can understand extended speech and lectures even if the speech is not clearly structured.',
                            'Student is able to describe clearly and finish the conversation with an appropriate conclusion.',
                            'Student can write well-structured text, expressing points of view in detail.',
                            'Student can understand long and complex literary texts, appreciating distinctions of style.');
                        break;
                    case 6:
                        $finalStringLevel = "Business - C1*";
                        $words = "938";
                        $hours = "144";
                        $color = "0852a0";
                        $footerString = array('Student can understand television programmes and films without too much effort.',
                            'Student can use language effectively for social and professional purposes.',
                            'Student can write essays or reports dealing with complex topics',
                            'Student understand specialized articles and longer technical instructions');
                        break;
                }
                $this->render('expresscertificates', array('model' => $model, 'userCertificados' => $userCertificados, 'finalStringLevel' => $finalStringLevel, 'words' => $words, 'hours' => $hours, 'color' => $color, 'footerString' => $footerString));
            } else {
                $this->render('expresscertificates', array('model' => $model));
            }
        } else {
            $model = new AbaCertificateForm();
            $this->render('expresscertificates', array('model' => $model));
        }
    }
}
