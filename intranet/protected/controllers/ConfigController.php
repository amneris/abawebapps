<?php

class ConfigController extends AbaController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
          'accessControl', // perform access control for CRUD operations
          'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(

          array(
            'allow',
            'actions' => array(/* 'view', */
                /* 'create', */
                /* 'update', */
                /* 'delete', */
                /* 'index', */
                /* 'admin', */
              'updateConfig'
            ),
            'users' => array('root'),
          ),
          array(
            'allow',
            'actions' => array(/* 'view', */
                /* 'create', */
                /* 'update', */
                /* 'delete', */
                /* 'index', */
                /* 'admin', */
              'updateConfig'
            ),
            'users' => array('marketing'),
          ),
          array(
            'allow',
            'actions' => array(/* 'view', */
                /* 'create', */
                /* 'update', */
                /* 'delete', */
                /* 'index', */
                /* 'admin', */
              'updateConfig'
            ),
            'users' => array('support'),
          ),
          array(
            'deny',  // deny all users
            'users' => array('*'),
          ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
          'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Config;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Config'])) {
            $model->attributes = $_POST['Config'];
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->key));
            }
        }

        $this->render('create', array(
          'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Config'])) {
            $model->attributes = $_POST['Config'];
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->key));
            }
        }

        $this->render('update', array(
          'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Config');
        $this->render('index', array(
          'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Config('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Config'])) {
            $model->attributes = $_GET['Config'];
        }

        $this->render('admin', array(
          'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = Config::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'config-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

// TODO: insert into aba_b2c.config (`key`,`value`,description,dateUpdate) values ('ZENDESK_HELP_CENTER', '0', '1-Enabled for all users.', now());
// TODO: insert into aba_b2c.config (`key`,`value`,description,dateUpdate) values ('ZENDESK_HELP_CENTER_ABA', '0', '1-Enabled for aba users.', now());

    /**
     * @throws CHttpException
     */
    public function actionUpdateConfig()
    {
        $modelChat = $this->loadModel('ENABLE_CHAT');
        $modelZenddeskHelpCenter = $this->loadModel('ZENDESK_HELP_CENTER');
        $modelZenddeskHelpCenterABA = $this->loadModel('ZENDESK_HELP_CENTER_ABA');
        $modelZenddesk = $this->loadModel('ZENDESK_CHAT_COUNTRIES');
        $modelCostumerSupportPREMIUM = $this->loadModel('ENABLE_CONTACT_SUPPORT_PREMIUM');
        $modelCostumerSupportEN = $this->loadModel('ENABLE_CONTACT_SUPPORT_EN');
        $modelCostumerSupportES = $this->loadModel('ENABLE_CONTACT_SUPPORT_ES');
        $modelCostumerSupportIT = $this->loadModel('ENABLE_CONTACT_SUPPORT_IT');
        $modelCostumerSupportDE = $this->loadModel('ENABLE_CONTACT_SUPPORT_DE');
        $modelCostumerSupportFR = $this->loadModel('ENABLE_CONTACT_SUPPORT_FR');
        $modelCostumerSupportPT = $this->loadModel('ENABLE_CONTACT_SUPPORT_PT');

        if (isset($_POST['Config'])) {

            $sKey = $_POST['Config']['key'];
            $sValue = 0;

            switch ($sKey) {
                case 'ENABLE_CHAT':
                    $sValue = (isset($_POST['view']) AND trim($_POST['view']) <> '' ? 1 : 0);
                    break;
                case 'ZENDESK_CHAT_COUNTRIES':
                    $sValue = trim(Yii::app()->config->get('ZENDESK_CHAT_COUNTRIES'));
                    if (isset($_POST['Config']['value'])) {
                        $sValue = trim($_POST['Config']['value']);
                    }
                    break;
                case 'ZENDESK_HELP_CENTER':
                    $sValue = (isset($_POST['viewHELPALL']) AND trim($_POST['viewHELPALL']) <> '' ? 1 : 0);
                    break;
                case 'ZENDESK_HELP_CENTER_ABA':
                    $sValue = (isset($_POST['viewHELPABA']) AND trim($_POST['viewHELPABA']) <> '' ? 1 : 0);
                    break;
                case 'ENABLE_CONTACT_SUPPORT_PREMIUM':
                    $sValue = (isset($_POST['viewPREMIUM']) AND trim($_POST['viewPREMIUM']) <> '' ? 1 : 0);
                    break;
                case 'ENABLE_CONTACT_SUPPORT_EN':
                    $sValue = (isset($_POST['viewEN']) AND trim($_POST['viewEN']) <> '' ? 1 : 0);
                    break;
                case 'ENABLE_CONTACT_SUPPORT_ES':
                    $sValue = (isset($_POST['viewES']) AND trim($_POST['viewES']) <> '' ? 1 : 0);
                    break;
                case 'ENABLE_CONTACT_SUPPORT_IT':
                    $sValue = (isset($_POST['viewIT']) AND trim($_POST['viewIT']) <> '' ? 1 : 0);
                    break;
                case 'ENABLE_CONTACT_SUPPORT_DE':
                    $sValue = (isset($_POST['viewDE']) AND trim($_POST['viewDE']) <> '' ? 1 : 0);
                    break;
                case 'ENABLE_CONTACT_SUPPORT_FR':
                    $sValue = (isset($_POST['viewFR']) AND trim($_POST['viewFR']) <> '' ? 1 : 0);
                    break;
                case 'ENABLE_CONTACT_SUPPORT_PT':
                    $sValue = (isset($_POST['viewPT']) AND trim($_POST['viewPT']) <> '' ? 1 : 0);
                    break;
            }

            $modelBase = $this->loadModel($sKey);

            $modelBase->value = $sValue;
            $modelBase->dateUpdate = date("Y-m-d H:i:s");

            if ($modelBase->update()) {
                Yii::app()->user->setFlash('delSucceded', $modelBase->description . ' updated successfully.');
            } else {
                Yii::app()->user->setFlash('delError', 'There was a problem trying to update this feature.');
            }

            $this->redirect(array('updateConfig'));
        }

        $this->render('updateConfig', array(
          'modelChat' => $modelChat,
          'modelZenddesk' => $modelZenddesk,
          'modelZenddeskHelpCenter' => $modelZenddeskHelpCenter,
          'modelZenddeskHelpCenterABA' => $modelZenddeskHelpCenterABA,
          'modelCostumerSupportPREMIUM' => $modelCostumerSupportPREMIUM,
          'modelCostumerSupportEN' => $modelCostumerSupportEN,
          'modelCostumerSupportES' => $modelCostumerSupportES,
          'modelCostumerSupportIT' => $modelCostumerSupportIT,
          'modelCostumerSupportDE' => $modelCostumerSupportDE,
          'modelCostumerSupportFR' => $modelCostumerSupportFR,
          'modelCostumerSupportPT' => $modelCostumerSupportPT,
        ));
    }

}
