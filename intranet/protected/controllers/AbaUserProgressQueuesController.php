<?php

class AbaUserProgressQueuesController extends AbaController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
          'accessControl', // perform access control for CRUD operations
          'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
          array(
            'allow',
            'actions' => array('view', 'create', 'update', 'delete', 'index', 'admin'),
            'users' => array('root', 'marketing'),
          ),
          array(
            'allow',
            'actions' => array('admin', 'index', 'view', 'create', 'update', 'delete'),
            'users' => array('support'),
          ),
          array(
            'allow',
            'actions' => array('admin', 'index', 'view', 'create', 'update', 'delete'),
            'users' => array('marketing'),
          ),
          array(
            'deny', // deny all users
            'users' => array('*'),
          ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $oModel = $this->loadModel($id);

        $user = AbaUser::model()->findByPk($oModel->userId);

        $modelDetails = new AbaUserProgressQueuesDetails();

        $this->render(
          'view',
          array(
            'user' => $user,
            'model' => $oModel,
            'modelDetails' => $modelDetails,
          )
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($userId)
    {
        $user = AbaUser::model()->findByPk($userId);

        $model = new AbaUserProgressQueues();

        $modelDetails = new AbaUserProgressQueuesDetails();

        if (isset($_POST['AbaUserProgressQueues'])) {

            $_POST['AbaUserProgressQueues']['dateAdd'] = date("Y-m-d h:i:s", time());
            $_POST['AbaUserProgressQueues']['status'] = AbaUserProgressQueues::QUEUE_STATUS_PENDING;

            if (isset($_POST['AbaUserProgressQueues']['units'])) {
                $_POST['AbaUserProgressQueues']['units'] = self::formatUnits($_POST['AbaUserProgressQueues']['units']);
            }

            if (isset($_POST['AbaUserProgressQueues']['evaluation']) AND trim($_POST['AbaUserProgressQueues']['evaluation']) == 1) {
                $_POST['AbaUserProgressQueues']['evaluation'] = 1;
            }
            else {
                $_POST['AbaUserProgressQueues']['evaluation'] = 0;
            }

            if (isset($_POST['AbaUserProgressQueues']['dateStart']) && (trim($_POST['AbaUserProgressQueues']['dateStart']) == '' || trim($_POST['AbaUserProgressQueues']['dateStart']) == '0000-00-00 00:00:00')) {
                $_POST['AbaUserProgressQueues']['dateStart'] = null;
            }

            if (isset($_POST['AbaUserProgressQueues']['dateEnd']) && (trim($_POST['AbaUserProgressQueues']['dateEnd']) == '' || trim($_POST['AbaUserProgressQueues']['dateEnd']) == '0000-00-00 00:00:00')) {
                $_POST['AbaUserProgressQueues']['dateEnd'] = null;
            }

            $model->attributes = $_POST['AbaUserProgressQueues'];


            if ($model->queueId == '') {
                $model->queueId = null;
            }
            if (trim($model->dateStart) == '' || trim($model->dateStart) == '0000-00-00 00:00:00') {
                $model->dateStart = null;
            }
            if (trim($model->dateEnd) == '' || trim($model->dateEnd) == '0000-00-00 00:00:00') {
                $model->dateEnd = null;
            }
            if (!is_numeric(trim($model->evaluation))) {
                $model->evaluation = 0;
            }


            $model->dateAdd = $_POST['AbaUserProgressQueues']['dateAdd'];
            $model->status = $_POST['AbaUserProgressQueues']['status'];

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->queueId));
            }
        }

        $this->render(
          'create',
          array(
            'user' => $user,
            'model' => $model,
            'wAction' => "create",
            'modelDetails' => $modelDetails,
          )
        );
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        $user = AbaUser::model()->findByPk($model->userId);

        if (isset($_POST['AbaUserProgressQueues'])) {

            if (isset($_POST['AbaUserProgressQueues']['dateAdd'])) {
                unset($_POST['AbaUserProgressQueues']['dateAdd']);
            }
            if (isset($_POST['AbaUserProgressQueues']['dateStart'])) {
                unset($_POST['AbaUserProgressQueues']['dateStart']);
            }
            if (isset($_POST['AbaUserProgressQueues']['dateEnd'])) {
                unset($_POST['AbaUserProgressQueues']['dateEnd']);
            }
            if (isset($_POST['AbaUserProgressQueues']['status'])) {
                unset($_POST['AbaUserProgressQueues']['status']);
            }
            if (isset($_POST['AbaUserProgressQueues']['evaluation'])) {
                unset($_POST['AbaUserProgressQueues']['evaluation']);
            }

            if (isset($_POST['AbaUserProgressQueues']['units'])) {
                $_POST['AbaUserProgressQueues']['units'] = self::formatUnits($_POST['AbaUserProgressQueues']['units']);
            }

            $model->attributes = $_POST['AbaUserProgressQueues'];

            if ($model->queueId == '') {
                $model->queueId = null;
            }

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->queueId));
            }
        }

        $this->render(
          'update',
          array(
            'user' => $user,
            'model' => $model,
            'wAction' => "update",
          )
        );
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        $model->updateToDeleted($id, '', date("Y-m-d h:i:s", time()));

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('AbaUserProgressQueues');
        $this->render(
          'index',
          array(
            'dataProvider' => $dataProvider,
            'wAction' => "index",
          )
        );
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new AbaUserProgressQueues('search');
        $model->unsetAttributes(); // clear any default values

        if (isset($_GET['AbaUserProgressQueues'])) {
            $model->attributes = $_GET['AbaUserProgressQueues'];
        }

        $this->render(
          'admin',
          array(
            'model' => $model,
            'wAction' => "admin",
          )
        );
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = AbaUserProgressQueues::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'aba-userprogressqueues-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * @param $sUnits
     *
     * @return mixed
     */
    protected static function formatUnits($sUnits)
    {

        $sUnits = trim(str_replace(" ", "", $sUnits));

        return $sUnits;
    }

}
