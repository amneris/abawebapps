<?php

class ExtranetController extends AbaController {

  protected function beforeAction($action) {

    if ( Yii::app()->user->isGuest ) {
      Yii::app()->user->returnUrl = Yii::app()->request->requestUri;
      $this->redirect('/index.php?r=site/login');
    }

    return parent::beforeAction($action);
  }

  /**
   * This is the default 'index' action that is invoked
   * when an action is not explicitly requested by users.
   */
  public function actionIndex()
  {

    $mPublication = new BlogNewsletterPublication();
    $params = Yii::app()->request->getParam('BlogNewsletterPublication', array( 'status' => 0 ));
    $mPublication->attributes = $params;

    $this->render('index', array(
      'mPublish' => $mPublication,
    ));
  }

  /**
   * This is the default 'index' action that is invoked
   * when an action is not explicitly requested by users.
   */
  public function actionUserlist() {

    // Create filter model and set properties
    $filtersForm = new commonNamespace\models\intranet\ExtranetUserListForm();
    $params = Yii::app()->request->getParam('commonNamespace_models_intranet_ExtranetUserListForm', array( 'isdeleted' => '0' ));
    $filtersForm->setFilters( $params );

    // Get rawData and create dataProvider
    $rawData=AbaExtranetModel::getUserList();
    $filteredData=$filtersForm->filter($rawData);
    $dataProvider=new CArrayDataProvider($filteredData, array(
      'id'=>'idLearner',
//      'sort'=>array(
//        'attributes'=>array(
//          'id', 'username', 'email',
//        ),
//      ),
      'pagination'=>array(
        'pageSize'=>10,
      ),
    ));

    $this->render('index', array(
      'accountName' => '',
      'filtersForm' => $filtersForm,
      'dataProvider' => $dataProvider
    ));
  }

  public function actionEditUser() {

  }

  public function actionUnregister() {
    $allOk = true;
    $errorMsg = Yii::t('app', 'No he podido finalizar la licencia.');
    $okMsg = Yii::t('app', 'Licencia finalizada con exito.');

    $transaction = Yii::app()->dbExtranet->beginTransaction();
    $transaction2 = Yii::app()->db->beginTransaction();
    try {
      $idStudent = Yii::app()->request->getParam('idStudent');

//    update abaenglish_extranet.student set idStudentCampus = 0 where id = 1300;
      $mStudent = Student::model()->findByPk( $idStudent );
      if ( !$mStudent ) throw new Exception('El alumno no existe.');
      if ( $mStudent->idStudentCampus == 0 ) throw new Exception('El alumno no tiene un usuario en el campus asociado.');
      $mStudentCampus = UserCampus::model()->findByPk( $mStudent->idStudentCampus );
      if ( !$mStudentCampus ) throw new Exception('El alumno no tiene un usuario en el campus asociado (2).');
      $mStudent->idStudentCampus = 0;
      $allOk = $mStudent->save();
      if ( !$allOk ) throw new Exception('No pude guardar el estudiante.');

//    update abaenglish_extranet.student_group set idPeriod = 0, expirationDate = '2000-01-01 00:00:00.0' where id = 1300;
      $mGroup = StudentGroup::model()->findByAttributes( array('idStudent' => $mStudent->id, 'isdeleted' => 0 ) );
      if ( $mGroup ) {
        $expirationDateExtranet = $mGroup->expirationDate;
        $mGroup->idPeriod = 0;
        $mGroup->expirationDate = '2000-01-01 00:00:00.0';
        $allOk = $mGroup->save();
        if ( !$allOk ) throw new Exception('No pude guardar el grupo.');
      } else $expirationDateExtranet = date("Y-m-d H:i:s");
      $expirationDateCampus = $mStudentCampus->expirationDate;

      $expirationDate = $expirationDateCampus;
//    update abaenglish_extranet.license_asigned set deleted = now(), isdeleted = 1 where id = 466;
      $criteria = new CDbCriteria();
      $criteria->addCondition( 'idStudent = :idStudent' );
      $criteria->addCondition( 'isdeleted = :isdeleted' );
      $criteria->params = array( ':idStudent' => $mStudent->id, ':isdeleted' => 0 );
      $criteria->order = 'created desc';
      $mlLicense = LicenseAsigned::model()->findAll( $criteria );
      foreach( $mlLicense as $mLicense ) {
        $mLicense->deleted = date("Y-m-d H:i:s");
        $mLicense->isdeleted = 1;
        $allOk = $mLicense->save();
        if ( !$allOk ) throw new Exception('No pude guardar la licencia asignada.');

//    UPDATE aba_b2c.payments SET amountDiscount=amountOriginal, amountPrice=0, amountTax=0, amountPriceWithoutTax=0 WHERE id='eed7dc75' AND userId=3490843 AND paySuppExtId=5;
        $criteria = new CDbCriteria();
        $criteria->addCondition( 'userId = :idUser' );
        $criteria->addCondition( 'paySuppExtId = :paySuppExtId' );
        $criteria->addCondition( 'dateStartTransaction <= :date' ); // ** Esto puede fallar en el cambio de hora.
        $criteria->addCondition( 'dateStartTransaction > :date2' ); // ** Esto puede fallar en el cambio de hora.
        $criteria->params = array( ':date' => $mLicense->created, ':date2' => date("Y-m-d H:i:s", strtotime($mLicense->created) - 30 ), ':idUser' => $mStudentCampus->id, ':paySuppExtId' => 5 );
        $criteria->order = 'dateStartTransaction desc';

        $mPayment = Payment::model()->find( $criteria );

        if ( !$mPayment ) throw new Exception('No encontre el pago.');
        $expirationDate = HeUtils::newExpirationDate( 'del', $expirationDate, $mPayment->idPeriodPay );
        $mPayment->amountDiscount = $mPayment->amountOriginal;
        $mPayment->amountPrice = 0;
        $mPayment->amountTax = 0;
        $mPayment->amountPriceWithoutTax = 0;
        $allOk = $mPayment->save();
        if ( !$allOk ) throw new Exception('No pude guardar el pago.');
      }

//    update aba_b2c.user set expirationDate = now() where id in (3490843);
      $mStudentCampus->expirationDate = $expirationDate;
      $allOk = $mStudentCampus->save();
      if ( !$allOk ) throw new Exception('No pude guardar el estudiante del campus. '.var_export($mStudentCampus->getErrors(), true));

      $transaction->commit();
      $transaction2->commit();
    } catch( Exception $e ) {
      $transaction->rollback();
      $transaction2->rollback();
      $errorMsg = $e->getMessage();
      $allOk = false;
    }

    echo json_encode(array( 'ok' => $allOk, 'errorMsg' => $errorMsg, 'okMsg' => $okMsg ));
  }

  public function actionDeleteuser() {

    $email = Yii::app()->request->getParam('email', '');
    $email = str_replace(' ', '+', $email);
    if ( empty($email) ) {
      echo "No email was provided.";
      return;
    }

    $connection = Yii::app()->dbExtranet;
    $sql = "select * from abaenglish_extranet.student where email = :email and isdeleted = 0";
    $command = $connection->createCommand($sql);
    $command->bindParam(":email", $email, PDO::PARAM_STR);
    $arStudent = $command->queryRow();
    if ( empty($arStudent) ) {
      echo "No Student found.";
      return;
    }
    $idStudent = $arStudent['id'];
    $idStudentCampus = $arStudent['idStudentCampus'];

    $sql = "select id from abaenglish_extranet.student_group where idStudent = :idStudent";
    $command = $connection->createCommand($sql);
    $command->bindParam(":idStudent", $idStudent, PDO::PARAM_INT);
    $idStudentGroup = $command->queryScalar();
    if ( empty($idStudentGroup) ) {
      echo "No StudentGroup found.";
      return;
    }

    $sql = "update aba_b2c.user set expirationDate = now(), userType = 1 where id = :idStudentCampus";
    $command = $connection->createCommand($sql);
    $command->bindParam(":idStudentCampus", $idStudentCampus, PDO::PARAM_INT);
    $allOk = $command->query();

    $sql = "update abaenglish_extranet.student set isdeleted = 1, deleted = now() where id = :idStudent";
    $command = $connection->createCommand($sql);
    $command->bindParam(":idStudent", $idStudent, PDO::PARAM_INT);
    $allOk = ($allOk && $command->query());

    $sql = "update abaenglish_extranet.student_group set isdeleted = 1, deleted = now() where id = :idStudentGroup";
    $command = $connection->createCommand($sql);
    $command->bindParam(":idStudentGroup", $idStudentGroup, PDO::PARAM_INT);
    $allOk = ($allOk && $command->query());

    if ( $allOk ) echo "All Ok";
    else echo "Some errors.";

  }

  public function actionChangeExpirationDateUser() {

    $email = Yii::app()->request->getParam('email', '');
    $email = str_replace(' ', '+', $email);
    if ( empty($email) ) {
      echo "No email was provided.";
      return;
    }

    $expirationDate = Yii::app()->request->getParam('expirationDate', '');
    if ( empty($expirationDate) ) {
      echo "No expirationDate was provided.";
      return;
    }
    if ( strtotime($expirationDate) <= 0 ) {
      echo "Not valid expirationDate was provided.";
      return;
    }
    $expirationDate = date("Y-m-d", strtotime($expirationDate))." 00:00:00";
    $pas2 = Yii::app()->request->getParam('pas2', false);
    $pas1 = !$pas2;

    $connection = Yii::app()->dbExtranet;
    $sql = "select * from abaenglish_extranet.student where email = :email and isdeleted = 0";
    $command = $connection->createCommand($sql);
    $command->bindParam(":email", $email, PDO::PARAM_STR);
    $arStudent = $command->queryRow();
    if ( empty($arStudent) ) {
      echo "No Student found ($email).";
      return;
    }
    $idStudent = $arStudent['id'];
    $idStudentCampus = $arStudent['idStudentCampus'];

    $sql = "select id, expirationDate from abaenglish_extranet.student_group where idStudent = :idStudent";
    $command = $connection->createCommand($sql);
    $command->bindParam(":idStudent", $idStudent, PDO::PARAM_INT);
    $arStudentGroup = $command->queryRow();
    if ( empty($arStudentGroup) ) {
      echo "No StudentGroup found.";
      return;
    }
    $idStudentGroup = $arStudentGroup['id'];
    $expirationDate_old = $arStudentGroup['expirationDate'];

    $sql = "select * from aba_b2c.user where id = :idStudentCampus and email = :email";
    $command = $connection->createCommand($sql);
    $command->bindParam(":idStudentCampus", $idStudentCampus, PDO::PARAM_INT);
    $command->bindParam(":email", $email, PDO::PARAM_STR);
    $arStudent = $command->queryRow();
    if ( empty($arStudent) ) {
      echo "No Student on campus found. ($idStudentCampus - $email)";
      return;
    }

    $allOk = true;
    if ( $pas1 ) {
      echo "Alumno: $email<br>";
      echo "Esta seguro de querer cambiar la fecha de expiracion del alumno de '{$expirationDate_old}' a '{$expirationDate}' ?<br><br> ";

      $url = $this->createUrl('mobile/route?pa2=');
      $url = $this->getRoute();
      $url= "/index.php?r={$url}/&email={$email}&expirationDate={$expirationDate}&pas2=1";

//      echo "<form action='{$url}'>
//                <input type='submit' value='Yes'>
//            </form>";
//      echo "<button onclick='$url'>Yes</button>";

      echo "<a href='$url'>Yes</a>";
    } else {
      $sql = "update aba_b2c.user set expirationDate = :expirationDate, userType = 2 where id = :idStudentCampus and userType in (1,2) ";
      $command = $connection->createCommand($sql);
      $command->bindParam(":expirationDate", $expirationDate, PDO::PARAM_STR);
      $command->bindParam(":idStudentCampus", $idStudentCampus, PDO::PARAM_INT);
      $allOk = $command->query();

      $sql = "update abaenglish_extranet.student_group set expirationDate = :expirationDate where id = :idStudentGroup";
      $command = $connection->createCommand($sql);
      $command->bindParam(":expirationDate", $expirationDate, PDO::PARAM_STR);
      $command->bindParam(":idStudentGroup", $idStudentGroup, PDO::PARAM_INT);
      $allOk = ($allOk && $command->query());
    }

    if( $pas2 ) {
      if ( $allOk ) echo "All Ok";
      else echo "Some errors.";
    }

  }

}