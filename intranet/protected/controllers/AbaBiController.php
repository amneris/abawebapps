<?php

class AbaBiController extends AbaController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            
             array('allow',
                    'users'=>array('*'),
                 ),
		);
	}

    /**
     * Displays using Google Geo Graph JS library a map with q. users by country
     */
    public function actionGeoChartUsers()
    {
        $moCountries = new AbaCountry();
        $aCountriesPremium = $moCountries->getCountriesQUsersPremium();
        $this->render("GeoChartUsers",array("aCountriesPremium"=>$aCountriesPremium));
    }

    /**
     * Displays a Line Chart to show evolution of quantity of payments by method of payment.
     */
    public function actionLChartPayBySupplier($lChartSinceDate="", $lChartIdCountry="")
    {
        if($lChartSinceDate==""){
            $lChartSinceDate = HeDate::getDateAdd(HeDate::todaySQL(false),0,-1);
        }


        $moPayments = new AbaPaymentsBi();
        $aPaymentsBySupplier = $moPayments->getAllPaymentsBySupplier($lChartSinceDate, HeDate::todaySQL(false),
                                                                    $lChartIdCountry);

        $this->render("LChartPayBySupplier",array("aPaymentsBySupplier"=>$aPaymentsBySupplier,
                                                            "moPayments"=>$moPayments,
                                                            "lChartSinceDate"=>$lChartSinceDate,
                                                            "lChartIdCountry"=>$lChartIdCountry ));
    }

    /**
     * Displays a Line Chart to show percentage of attempts in payments. Failure versus Success.
     */
    public function actionAChartPayAttempts( $aChartSinceDate='', $aChartIdCountry="", $aChartPaySupExtId="" )
    {
        if($aChartSinceDate==""){
            $aChartSinceDate = HeDate::getDateAdd(HeDate::todaySQL(false),0,-3);
        }

        $moPayments = new AbaPaymentsBi();
        $aPaymentsAttempts = $moPayments->getAttemptsKPI( $aChartSinceDate, '', $aChartIdCountry, $aChartPaySupExtId);

        $this->render("AChartPayAttempts", array("aPaymentsAttempts"=>$aPaymentsAttempts,
                                                "aChartSinceDate"=>$aChartSinceDate,
                                                "aChartIdCountry"=>$aChartIdCountry,
                                                "aChartPaySupExtId"=>$aChartPaySupExtId) );
    }

    /**
     * Displays webservice errors
     */
    public function actionWsErrorsLog()
    {
        $modelLog = new LogWebService();

        $modelLog->unsetAttributes();

        $endNow =       HeDate::now();
        $startDate =    HeDate::getDateTimeAdd($endNow, -1, HeDate::HOURS);
        $endDate =      $endNow;

        if(isset($_GET['LogWebService'])) {

            $modelLog->attributes = $_GET['LogWebService'];

            if(isset($_GET['LogWebService']['startDate'])) {
                $modelLog->startDate = $_GET['LogWebService']['startDate'];
            }
            if(isset($_GET['LogWebService']['endDate'])) {
                $modelLog->endDate = $_GET['LogWebService']['endDate'];
            }

            if(isset($_GET['LogWebService']['nameService'])) {
                $modelLog->nameService = $_GET['LogWebService']['nameService'];
            }
            if(isset($_GET['LogWebService']['successResponse'])) {
                $modelLog->successResponse = $_GET['LogWebService']['successResponse'];
            }
            if(isset($_GET['LogWebService']['errorCode'])) {
                $modelLog->errorCode = $_GET['LogWebService']['errorCode'];
            }
        }
        else {
            $modelLog->startDate = $startDate;
            $modelLog->endDate =   $endDate;
        }

        $this->render("WsErrorsLog", array(
            'startDate' =>  $startDate,
            'endDate' =>    $endDate,
            'model' =>      $modelLog,
        ));

    }

}

