<?php

class B2cFollowupController extends AbaController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

    public $levelList = array('All levels'=>'All levels','Beginners'=>'Beginners','Lower intermediate'=>'Lower intermediate','Intermediate'=>'Intermediate',
        'Upper Intermediate'=>'Upper Intermediate', 'Advanced'=>'Advanced', 'Business'=>'Business');

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
    public function accessRules()
    {
        return array(

            array('allow',
                'actions'=>array('view', /*'create', 'update', 'delete', 'index',*/ 'admin', 'export', 'getGroupPartnerList'),
                'users'=>array('support'),
            ),

            array('allow',
                'actions'=>array('view', /*'create', 'update', 'delete', 'index',*/ 'admin', 'export', 'getGroupPartnerList'),
                'users'=>array('marketing'),
            ),

            array('allow',
                'actions'=>array('view', /*'create', 'update', 'delete', 'index',*/ 'admin', 'export', 'getGroupPartnerList'),
                'users'=>array('teacher'),
            ),

            array('allow',
                'actions'=>array('view', /*'create', 'update', 'delete', 'index',*/ 'admin', 'export','getGroupPartnerList'),
                'users'=>array('root'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

	/**
   * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     * @param $level
     * @param $criterio
     * @param $defaultPartner
     * @param $defaultPartnerGroup
     */
    public function actionView($id, $level, $criterio, $defaultPartner, $defaultPartnerGroup)
    {
        $model = new B2cFollowup();
        $model = $model->findByIdFollowup($id);

        $this->render('view',array('model'=>$model, 'level'=>$level, 'criterio'=>$criterio,
            'defaultPartner'=>$defaultPartner, 'defaultPartnerGroup'=>$defaultPartnerGroup, 'totalsPercent'=>$this->getTotals($model)));
    }

    public function getTotals($model)
    {
        $totalsPercent = array();

        //ABA Film percent
        if($model->sit_por > 50)
            $totalsPercent['sitPercent']=100;
        else
            $totalsPercent['sitPercent'] = $model->sit_por;

        //Speak percent
        $totalsPercent['stuPercent'] = $model->stu_por * 2;
        if($totalsPercent['stuPercent']>100)
            $totalsPercent['stuPercent']=100;

        //Write percent
        $totalsPercent['dicPercent'] = $model->dic_por;
        if($totalsPercent['dicPercent']>100)
            $totalsPercent['dicPercent']=100;

        //Interpret percent
        $totalsPercent['rolPercent'] = $model->rol_por;
        if($totalsPercent['rolPercent']>100)
            $totalsPercent['rolPercent']=100;

        //Video Class percent
        if($model->gra_vid > 0)
            $totalsPercent['gra_vid']=100;
        else
            $totalsPercent['gra_vid']=$model->gra_vid;

        //Exercises percent
        $totalsPercent['wriPercent'] = $model->wri_por;
        if($totalsPercent['wriPercent']>100)
            $totalsPercent['wriPercent']=100;

        //Vocabulary percent
        $totalsPercent['newPercent'] = $model->new_por * 2;
        if($totalsPercent['newPercent']>100)
            $totalsPercent['newPercent']=100;

        //Assessment percent
        $totalsPercent['evaPercent'] = $model->eva_por;

        //Total percent
        $totalsPercent['total'] = floor(($totalsPercent['sitPercent'] +  $totalsPercent['stuPercent'] + $totalsPercent['dicPercent'] +
                $totalsPercent['rolPercent'] + $totalsPercent['gra_vid'] + $totalsPercent['wriPercent'] + $totalsPercent['newPercent']) / 7);

        return $totalsPercent;

    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new B2cFollowup;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['B2cFollowup']))
		{
			$model->attributes=$_POST['B2cFollowup'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['B2cFollowup']))
		{
			$model->attributes=$_POST['B2cFollowup'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('B2cFollowup');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
    public function actionAdmin()
    {
        if(isset($_POST['level']))
        {
            $level = $_POST['level'];
            $criterio = $_POST['criterio'];
            $defaultPartnerGroup = $_POST['defaultPartnerGroup'];
            $defaultPartner = $_POST['defaultPartner'];

        }
        else if(isset($_GET['level']))
        {
            $level = $_GET['level'];
            $criterio = $_GET['criterio'];
            $defaultPartnerGroup = $_GET['defaultPartnerGroup'];
            $defaultPartner = $_GET['defaultPartner'];

        }
        else
        {
            $level = $this->levelList['Beginners'];
            $criterio = '';
            $defaultPartnerGroup=0; //abaenglish
            $defaultPartner='';

        }

        if(isset($_GET['startDate']) && isset($_GET['endDate']))
        {
            $startDate = $_GET['startDate'];
            $endDate = $_GET['endDate'];
        }
        else
        {
            //$startDate = '2013-05-15';
            //$endDate = '2013-05-30';
            $startDate = date('Y-m-d');
            $endDate = $startDate;
        }
        if(isset($_GET['entryDate']))
            $entryDate = $_GET['entryDate'];
        else
            $entryDate = false;

        //load parters groups on dropbox
        $dropDownProviderGroup = AbaPartnersList::model()->getListPartnerGroup();
        $toAddGroup = array(''=>'All partner groups');
        $dropDownProviderGroup = $toAddGroup + $dropDownProviderGroup;
        //pre select partners from choosen parter group
        $partnerSelection = AbaPartnersList::model()->getListPartnerFromGroup($defaultPartnerGroup);
        //load partners selection
        $dropDownProvider = AbaPartnersList::model()->getFullGroupList($partnerSelection);
        $toAdd = array(''=>'All partners');
        $dropDownProvider = $toAdd + $dropDownProvider;

        $model = new B2cFollowup('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['B2cFollowup']))
            $model->attributes=$_GET['B2cFollowup'];

        $contador = $model->countAll($level, $criterio, $defaultPartner, $defaultPartnerGroup, $startDate, $endDate, $entryDate);
        $pages = new CPagination($contador);
        $pages->setPageSize(25);

        $this->render('admin',array(
            'model'=>$model, 'level'=>$level, 'levelList'=>$this->levelList, 'pages'=>$pages, 'criterio'=>$criterio, 'startDate'=>$startDate, 'endDate'=>$endDate, 'entryDate'=>$entryDate,
            'dropDownProvider'=>$dropDownProvider, 'defaultPartner'=>$defaultPartner, 'dropDownProviderGroup'=>$dropDownProviderGroup, 'defaultPartnerGroup'=>$defaultPartnerGroup,
        ));
    }

    //this actios is called by ajax to pupulate a dropdownlist
    public function actionGetGroupPartnerList()
    {
        $defaultPartnerGroup = $_POST['defaultPartnerGroup'];
        $partnerSelection = AbaPartnersList::model()->getListPartnerFromGroup($defaultPartnerGroup);
        $dropDownProvider = AbaPartnersList::model()->getFullGroupList($partnerSelection);
        foreach($dropDownProvider as $key=>$value)
        {
            echo CHtml::tag('option', array('value'=>$key),CHtml::encode($value),true);
        }
        echo CHtml::tag('option', array('value'=>''),CHtml::encode('All partners'),true);
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=B2cFollowup::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='b2c-followup-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionExport()
    {

        if(isset($_GET['level']))
            $level = $_GET['level'];
        else
            $level = $this->levelList['Beginners'];

        if(isset($_GET['criterio']))
            $criterio = $_GET['criterio'];
        else
            $criterio = '';

        if(isset($_GET['defaultPartnerGroup']))
            $defaultPartnerGroup = $_GET['defaultPartnerGroup'];
        else
            $defaultPartnerGroup = '';

        if(isset($_GET['defaultPartner']))
            $defaultPartner = $_GET['defaultPartner'];
        else
            $defaultPartner = '';

        if(isset($_GET['entryDate']))
            $entryDate = $_GET['entryDate'];
        else
            $entryDate = false;

        if(isset($_GET['startDate']) && isset($_GET['endDate']))
        {
            $startDate = $_GET['startDate'];
            $endDate = $_GET['endDate'];
        }
        else
        {
            //$startDate = '2013-05-15';
            //$endDate = '2013-05-30';
            $startDate = date('Y-m-d');
            $endDate = $startDate;
        }

        $model = new B2cFollowup();
        $dataReader = $model->searchExport($level, $criterio, $defaultPartnerGroup, $defaultPartner, $startDate, $endDate, $entryDate);
        $data = array();
        $xls = new JPhpCsv('UTF-8', false, 'Users');
        $flagFirst=true;

        foreach($dataReader as $row)
        {
            if($flagFirst)
            {
                $flagFirst= false;
                $theRow=array();
                foreach ($row as $key =>$value)
                {
                    if($key!=='tmpCurrentWatchedVideoClass')
                    {
                        array_push($theRow, $key);
                    }
                }
                array_push($data, $theRow);
            }
            $theRow=array();
            foreach ($row as $key =>$value)
            {
                if($key!=='tmpCurrentWatchedVideoClass')
                {
                    array_push($theRow, $value);
                }
            }
            array_push($data, $theRow);
        }
        if($flagFirst==true)
        {
            $theRow=array();
            array_push($theRow, "DATA NOT FOUND (Data not exported)");
            array_push($data, $theRow);
        }
        $xls->addArray($data);
        $xls->generateXML("StudyReport-".$level."-".date("d-m-Y"));
    }

}
