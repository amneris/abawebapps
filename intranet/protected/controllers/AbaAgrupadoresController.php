<?php

class AbaAgrupadoresController extends AbaController
{
    public function filters()
    {
            return array(
                    'accessControl', // perform access control for CRUD operations
                    'postOnly + delete', // we only allow deletion via POST request
            );
    }
        
    public function accessRules()
    {
        return array(
                array(  'allow', 
                        'actions'=>array('index', 'create', 'view', 'update'),
                        'users'=>array('root'),
                ),

                array('deny',  // deny all users
                        'users'=>array('*'),
                ),
        );
    }
        
    
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('AbaAgrupadores');

        $model=new AbaAgrupadores();
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['AbaAgrupadores']))
                    $model->attributes=$_GET['AbaAgrupadores'];
        $this->render('index',array(
                'dataProvider'=>$dataProvider, 'model'=>$model
        ));
    }
        
    public function actionCreate()
    {
        $mensajeResult ='';
        $model = new AbaAgrupadores;
        if(isset($_POST['AbaAgrupadores']))
        {

            $model->attributes=$_POST['AbaAgrupadores'];

            if($model->prefijoMensual<>'no')
            {
                $mensajeResult .= $this->createTable($model->tableName, $model->prefijoMensual);
            }

            if($model->prefijoBimensual<>'no')
            {
                $mensajeResult .= $this->createTable($model->tableName, $model->prefijoBimensual);
            }

            if($model->prefijoTrimestral<>'no')
            {
                $mensajeResult .= $this->createTable($model->tableName, $model->prefijoTrimestral);
            }

            if($model->prefijoSemestral<>'no')
            {
                $mensajeResult .= $this->createTable($model->tableName, $model->prefijoSemestral);
            }

            if($model->prefijoAnual<>'no')
            {
                $mensajeResult .= $this->createTable($model->tableName, $model->prefijoAnual);
            }

            if($model->prefijo18Meses<>'no')
            {
                $mensajeResult .= $this->createTable($model->tableName, $model->prefijo18Meses);
            }

            if($model->prefijo24Meses<>'no')
            {
                $mensajeResult .= $this->createTable($model->tableName, $model->prefijo24Meses);
            }
            
            if($model->saveAgrupador($model))
            {
                $mensajeResult .= $this->partnerList($model->agrupadorName);
                $mensajeResult .= "Se inserto el agrupador <b>'$model->agrupadorName'</b> con exito en la tabla: <b>ventasFlashScrip</b>.<br>";
            }
            else
                $mensajeResult .= "No se  pudo insertar el agrupador <b>'$model->agrupadorName'</b> en la tabla: <b>ventasFlashScrip</b>.<br>";
            
            $this->render('resultMessage', array('mensaje'=>$mensajeResult));
        }
        else
            $this->render('create',array('model'=>$model,));

    }  
        
    public function actionView($id)
    {
        $this->render('view',array('model'=>$this->loadModel($id)));
    }
        
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if(isset($_POST['AbaAgrupadores']))
        {
            $model->attributes=$_POST['AbaAgrupadores'];
            $this->updateTableNames($model);
             
            if($model->save())
            {
                $this->redirect(array('view','id'=>$model->idventasFlashScript));
            }
        }
        $this->render('update',array('model'=>$model,));
    }
        
    public function loadModel($id)
    {
            $model = AbaAgrupadores::model()->findByPk($id);
            if($model===null)
                    throw new CHttpException(404,'The requested page does not exist.');
            return $model;
    }
        
    private function createTable($xTableName, $xPrefijo)
    { 
        $creationTableName = $xTableName;
        $creationTableName .= $xPrefijo;
        $createQuery = "CREATE TABLE db_ventasflash.`$creationTableName`(
                                        `code1` varchar(45) NOT NULL default '',
                                          `code2` varchar(45) NOT NULL default '',
                                          `email` varchar(100) NOT NULL default '',
                                          `used` tinyint(1) NOT NULL default '0',
                                          `date` datetime NOT NULL default '0000-00-00 00:00:00',
                                          `campaign` varchar(45) default NULL,
                                          `count` int(11) default NULL,
                                          PRIMARY KEY  (`code1`)
                                        ) ENGINE=InnoDB DEFAULT CHARSET=latin1; ";
        $connection = Yii::app()->db2;
        $command = $connection->createCommand($createQuery);		
        $result = $command->query();

        if($result)
            $mensaje = "Se ha credo con exito la tabla: <b>'$creationTableName'</b>.<br>";
        else
            $mensaje = "No se ha podido crear la tabla: <b>'$creationTableName'</b>.<br>";

        return $mensaje;
    }
       
    private function updateTableName($newTableName, $oldTableName)
{

    $createQuery = "ALTER TABLE db_ventasflash.`$oldTableName` RENAME TO db_ventasflash.`$newTableName`;";

    $connection = Yii::app()->db2;
    $command = $connection->createCommand($createQuery);
    $result = $command->query();

    if($result)
        $mensaje = "Se ha modificado con exito la tabla: <b>'$oldTableName'</b> a sido renombra con el nombre:<b>'$newTableName'</b>.<br>";
    else
        $mensaje = "No se ha podido modificar la tabla: <b>'$oldTableName'</b>.<br>";

    return $mensaje;
}
    
    
    private function partnerList($xName)
    {
        $preQuery="SELECT idventasFlashScript FROM db_ventasflash.`ventasFlashScript` WHERE agrupadorName = '$xName';";  
        $preConnection = Yii::app()->db2;
        $preCommand = $preConnection->createCommand($preQuery);
        $dataReader = $preCommand->query();
        $rows = $dataReader->read();
        $identificador = $rows['idventasFlashScript'];

        if($identificador)
        {
            $createQuery = "INSERT INTO aba_b2c.`aba_partners_list` (idPartner, name, idPartnerGroup) VALUES ('$identificador', '$xName', '1')";
            $connection = Yii::app()->db;
            $command = $connection->createCommand($createQuery);		
            $result = $command->query();
            
            if($result)
                $mensaje = "Se ha insertado con exito el agrupador <b>'$xName'</b> en la tabla: <b>aba_partners_list</b>.<br>";
            else
                $mensaje = "No se ha podido insertar el agrupador <b>'$xName'</b> en la tabla: <b>aba_partners_list</b>.<br>";
        }
        
        return $mensaje;
    }

    private function updateTableNames($xNewAgrupador)
    {
        $agrupadorOriginal = AbaAgrupadores::model()->findByPk($xNewAgrupador->idventasFlashScript);

        //firs condition, checking if there is any change.
        if($xNewAgrupador->prefijoMensual <> $agrupadorOriginal->prefijoMensual)
        {
            //if we detect a change and the orifinal prefix is NO - we create the new table.
            if($agrupadorOriginal->prefijoMensual == 'no')
            {
                $this->createTable($xNewAgrupador->tableName, $xNewAgrupador->prefijoMensual);
            }
            //if we detect a change but the original prefix si not NO - we update the table name.
            else if(($agrupadorOriginal->prefijoMensual <> 'no') && ($xNewAgrupador->prefijoMensual <>'no'))
            {
                $oldTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijoMensual;
                $newTableName = $xNewAgrupador->tableName . $xNewAgrupador->prefijoMensual;
                $this->updateTableName($newTableName, $oldTableName);
            }
            //in case we already have the table but we wants to delete it. Just for precaution.. we will renamed like _old
            else if(($agrupadorOriginal->prefijoMensual <> 'no') && ($xNewAgrupador->prefijoMensual =='no'))
            {
                $oldTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijoMensual;
                $newTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijoMensual . '_old';
                $this->updateTableName($newTableName, $oldTableName);
            }
        }

        //firs condition, checking if there is any change.
        if($xNewAgrupador->prefijoBimensual <> $agrupadorOriginal->prefijoBimensual)
        {
            //if we detect a change and the orifinal prefix is NO - we create the new table.
            if($agrupadorOriginal->prefijoBimensual == 'no')
            {
                $this->createTable($xNewAgrupador->tableName, $xNewAgrupador->prefijoBimensual);
            }
            //if we detect a change but the original prefix si not NO - we update the table name.
            else if(($agrupadorOriginal->prefijoBimensual <> 'no') && ($xNewAgrupador->prefijoBimensual <>'no'))
            {
                $oldTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijoBimensual;
                $newTableName = $xNewAgrupador->tableName . $xNewAgrupador->prefijoBimensual;
                $this->updateTableName($newTableName, $oldTableName);
            }
            //in case we already have the table but we wants to delete it. Just for precaution.. we will renamed like _old
            else if(($agrupadorOriginal->prefijoBimensual <> 'no') && ($xNewAgrupador->prefijoBimensual =='no'))
            {
                $oldTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijoBimensual;
                $newTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijoBimensual . '_old';
                $this->updateTableName($newTableName, $oldTableName);
            }
        }

        //firs condition, checking if there is any change.
        if($xNewAgrupador->prefijoTrimestral <> $agrupadorOriginal->prefijoTrimestral)
        {
            //if we detect a change and the orifinal prefix is NO - we create the new table.
            if($agrupadorOriginal->prefijoTrimestral == 'no')
            {
                $this->createTable($xNewAgrupador->tableName, $xNewAgrupador->prefijoTrimestral);
            }
            //if we detect a change but the original prefix si not NO - we update the table name.
            else if(($agrupadorOriginal->prefijoTrimestral <> 'no') && ($xNewAgrupador->prefijoTrimestral <>'no'))
            {
                $oldTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijoTrimestral;
                $newTableName = $xNewAgrupador->tableName . $xNewAgrupador->prefijoTrimestral;
                $this->updateTableName($newTableName, $oldTableName);
            }
            //in case we already have the table but we wants to delete it. Just for precaution.. we will renamed like _old
            else if(($agrupadorOriginal->prefijoTrimestral <> 'no') && ($xNewAgrupador->prefijoTrimestral =='no'))
            {
                $oldTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijoTrimestral;
                $newTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijoTrimestral . '_old';
                $this->updateTableName($newTableName, $oldTableName);
            }
        }

        //firs condition, checking if there is any change.
        if($xNewAgrupador->prefijoSemestral <> $agrupadorOriginal->prefijoSemestral)
        {
            //if we detect a change and the orifinal prefix is NO - we create the new table.
            if($agrupadorOriginal->prefijoSemestral == 'no')
            {
                $this->createTable($xNewAgrupador->tableName, $xNewAgrupador->prefijoSemestral);
            }
            //if we detect a change but the original prefix si not NO - we update the table name.
            else if(($agrupadorOriginal->prefijoSemestral <> 'no') && ($xNewAgrupador->prefijoSemestral <>'no'))
            {
                $oldTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijoSemestral;
                $newTableName = $xNewAgrupador->tableName . $xNewAgrupador->prefijoSemestral;
                $this->updateTableName($newTableName, $oldTableName);
            }
            //in case we already have the table but we wants to delete it. Just for precaution.. we will renamed like _old
            else if(($agrupadorOriginal->prefijoSemestral <> 'no') && ($xNewAgrupador->prefijoSemestral =='no'))
            {
                $oldTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijoSemestral;
                $newTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijoSemestral . '_old';
                $this->updateTableName($newTableName, $oldTableName);
            }
        }

        //firs condition, checking if there is any change.
        if($xNewAgrupador->prefijoAnual <> $agrupadorOriginal->prefijoAnual)
        {
            //if we detect a change and the orifinal prefix is NO - we create the new table.
            if($agrupadorOriginal->prefijoAnual == 'no')
            {
                $this->createTable($xNewAgrupador->tableName, $xNewAgrupador->prefijoAnual);
            }
            //if we detect a change but the original prefix si not NO - we update the table name.
            else if(($agrupadorOriginal->prefijoAnual <> 'no') && ($xNewAgrupador->prefijoAnual <>'no'))
            {
                $oldTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijoAnual;
                $newTableName = $xNewAgrupador->tableName . $xNewAgrupador->prefijoAnual;
                $this->updateTableName($newTableName, $oldTableName);
            }
            //in case we already have the table but we wants to delete it. Just for precaution.. we will renamed like _old
            else if(($agrupadorOriginal->prefijoAnual <> 'no') && ($xNewAgrupador->prefijoAnual =='no'))
            {
                $oldTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijoAnual;
                $newTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijoAnual . '_old';
                $this->updateTableName($newTableName, $oldTableName);
            }
        }

        //firs condition, checking if there is any change.
        if($xNewAgrupador->prefijo18Meses <> $agrupadorOriginal->prefijo18Meses)
        {
            //if we detect a change and the orifinal prefix is NO - we create the new table.
            if($agrupadorOriginal->prefijo18Meses == 'no')
            {
                $this->createTable($xNewAgrupador->tableName, $xNewAgrupador->prefijo18Meses);
            }
            //if we detect a change but the original prefix si not NO - we update the table name.
            else if(($agrupadorOriginal->prefijo18Meses <> 'no') && ($xNewAgrupador->prefijo18Meses <>'no'))
            {
                $oldTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijo18Meses;
                $newTableName = $xNewAgrupador->tableName . $xNewAgrupador->prefijo18Meses;
                $this->updateTableName($newTableName, $oldTableName);
            }
            //in case we already have the table but we wants to delete it. Just for precaution.. we will renamed like _old
            else if(($agrupadorOriginal->prefijo18Meses <> 'no') && ($xNewAgrupador->prefijo18Meses =='no'))
            {
                $oldTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijo18Meses;
                $newTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijo18Meses . '_old';
                $this->updateTableName($newTableName, $oldTableName);
            }
        }

        //firs condition, checking if there is any change.
        if($xNewAgrupador->prefijo24Meses <> $agrupadorOriginal->prefijo24Meses)
        {
            //if we detect a change and the orifinal prefix is NO - we create the new table.
            if($agrupadorOriginal->prefijo24Meses == 'no')
            {
                $this->createTable($xNewAgrupador->tableName, $xNewAgrupador->prefijo24Meses);
            }
            //if we detect a change but the original prefix si not NO - we update the table name.
            else if(($agrupadorOriginal->prefijo24Meses <> 'no') && ($xNewAgrupador->prefijo24Meses <>'no'))
            {
                $oldTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijo24Meses;
                $newTableName = $xNewAgrupador->tableName . $xNewAgrupador->prefijo24Meses;
                $this->updateTableName($newTableName, $oldTableName);
            }
            //in case we already have the table but we wants to delete it. Just for precaution.. we will renamed like _old
            else if(($agrupadorOriginal->prefijo24Meses <> 'no') && ($xNewAgrupador->prefijo24Meses =='no'))
            {
                $oldTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijo24Meses;
                $newTableName = $agrupadorOriginal->tableName . $agrupadorOriginal->prefijo24Meses . '_old';
                $this->updateTableName($newTableName, $oldTableName);
            }
        }
        
    }
    
}