<?php
    
class AbaVentasFlashController extends AbaController
{
    

    public function filters()
    {
        return array(
                'accessControl', // perform access control for CRUD operations
                'postOnly + delete', // we only allow deletion via POST request
        );
    }
        
    public function accessRules()
    {
        return array(
                array(  'allow', 
                        'actions'=>array('index', 'create', 'view', 'deleteCode', 'freeCode', 'createFromFile', 'salesForAgrupador', 'updateDeals'),
                        'users'=>array('root'),
                ),
            
                array('allow', 
                        'actions'=>array('index', 'create', 'view', 'deleteCode', 'freeCode', /* 'createFromFile', */ /* 'salesForAgrupador', */ /* 'updateDeals' */),
                        'users'=>array('support'),
                ),
            
                array('allow', 
                        'actions'=>array('index', 'create', 'view', 'deleteCode', 'freeCode', 'createFromFile', 'salesForAgrupador', 'updateDeals'),
                        'users'=>array('marketing'),
                ),
            
                array('deny',  // deny all users
                        'users'=>array('*'),
                ),
        );
    }
    
    public function actionIndex()
    {
        //dropbox dataprovider agrupadores.
        $dropDownProvider = AbaAgrupadores::model()->findAll(array('order'=>'agrupadorName'));
        $list = CHtml::listData($dropDownProvider, 
                'idventasFlashScript', 'agrupadorName');

        if(isset($_POST['criterio']))
        {
            $criterio = $_POST['criterio'];
        }
        else
        {
            $criterio ='';
        }
        
        if(isset($_GET['agrupador']))
        {
            $agrupador = $_GET['agrupador'];
        }
        else
            $agrupador = 1;
        
        if(isset($_POST['agrupadores']))
        {
           $_GET['agrupador'] = $_POST['agrupadores'];
           $agrupador = $_POST['agrupadores'];
        }
        
        $table = $this->selectTable($agrupador);
        $model = new AbaVentasFlash('1nadan-eslovenia201208s');
        $contador = $model->countAllCodes($table, $criterio); 
            
        $pages = new CPagination($contador);
        $pages->setPageSize(50);
        
        $this->render('index',array('model'=>$model, 'list'=>$list, 'selected'=>$agrupador, 'allTables'=>$table, 'count'=>$contador, 'pages'=>$pages, 'criterio'=>$criterio));
    }
    
    public function actionCreate()
    {
        $dropDownProvider = AbaAgrupadores::model()->findAll(array('order'=>'agrupadorName'));
        $list = CHtml::listData($dropDownProvider, 
                'idventasFlashScript', 'agrupadorName');
        
        $products = array(1=>'1 Month', 2=>'2 Months', 3=>'3 Months',6=>'6 Months', 12=>'12 Months', 18=>'18 Months', 24=>'24 Months');
        
        $model = new AbaVentasFlash('1nadan-eslovenia201208s');
        
        
		if(isset($_POST['AbaVentasFlash']))
		{
            $table = $this->selectTable($_POST['agrupador']);
            $exist = $this->checkTableExist($table, $_POST['producto']);
                        
            if($exist)
            {
                $model->setTableName($exist);
                $model->attributes = $_POST['AbaVentasFlash'];
                $model->agrupadorName = $list[$_POST['agrupador']]; 
                
                $existModel = new AbaVentasFlash('1nadan-eslovenia201208s');
                
                $existModel = $existModel->findByCode($model->code1, $exist);
                if($existModel->code1<>'' || $existModel->code1 <> null)
                {
                    Yii::app()->user->setFlash('prodExist','The code already exists in the database for this grouping.');
                    $this->render('create',array('model'=>$model, 'list'=>$list, 'selected'=>$_POST['agrupador'], 'products'=>$products, 'selectedProd'=>$_POST['producto']));
                }
                else
                {
                    if($model->save())
                    {
                          // ** Actualizamos la campaña al maximo de los dos valores.
                        $mAgrupador = AbaAgrupadores::model()->findByPk( $_POST['agrupador'] );
                        if ( $mAgrupador ) {
                          $mAgrupador->totalDeals = ( $mAgrupador->totalDeals >= $_POST['AbaVentasFlash']['campaign'] ) ? $mAgrupador->totalDeals : $_POST['AbaVentasFlash']['campaign'];
                          $mAgrupador->save();
                        }

                        $this->redirect(array('view','id'=>$model->code1,'table'=>$exist, 'agrupador'=>$model->agrupadorName, 'selected'=>$_POST['agrupador'], 'criterio'=>$model->code1));
                    } 
                    else
                    {
                        $this->render('create',array('model'=>$model, 'list'=>$list, 'selected'=>$_POST['agrupador'], 'products'=>$products, 'selectedProd'=>$_POST['producto']));
                    }
                }
                
                
            }	
            
            else
            {
               Yii::app()->user->setFlash('prodExist','El producto seleccionado no existe para este agrupador. Por favor seleccione otro producto y vuelva a intentarlo.');
               $this->render('create',array('model'=>$model, 'list'=>$list, 'selected'=>$_POST['agrupador'], 'products'=>$products, 'selectedProd'=>$_POST['producto']));
                            
             
            }
		}	
        else
        {
            $this->render('create',array('model'=>$model, 'list'=>$list, 'selected'=>1, 'products'=>$products,'selectedProd'=>1));
        }
        			 
		
        
    }
    
    public function actionView($id, $table, $agrupador, $selected, $criterio)
    {
        $model = new AbaVentasFlash($table);
        $this->render('view',array('model'=>$model->findByCode($id,$table),'agrupador'=>$agrupador, 'selected'=>$selected, 'criterio'=>$criterio));
    }
    
    public function actionDeleteCode($id, $table, $agrupador, $selected, $criterio)
    {
       $model = new AbaVentasFlash($table);
       $model = $model->findByCode($id, $table);
       $model->setIsNewRecord(false);
       if($model->delete())
       {
            Yii::app()->user->setFlash('delSucceded','Se elimino el cupón con exito.');    
            if(!isset($_GET['ajax']))
               $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index','selected'=>$selected,'criterio'=>$criterio));
       }
       else
       {
            Yii::app()->user->setFlash('delError','No se puedo eliminar el cupón.');
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index','selected'=>$selected,'criterio'=>$criterio));
       }

    }
    
    public function loadModel($id)
    {
            $model = AbaVentasFlash::model()->findByPk($id);
            if($model===null)
                    throw new CHttpException(404,'The requested page does not exist.');
            return $model;
    }
    
    public function actionFreeCode($id, $table, $agrupador, $selected, $criterio)
    {
        $query = "UPDATE `$table` 
                  SET code2='', email='', used=0, date='0000-00-00 00:00:00', count=1 
                  where code1= '$id'; ";
        
        $connection = Yii::app()->db2;
        $command = $connection->createCommand($query);		
        $result = $command->query();
        
        $model = new AbaVentasFlash($table);
         
        if($result)
        {
             Yii::app()->user->setFlash('freeSucceded','Se libero el cupón con exito.');
             $this->render('view',array('model'=>$model->findByCode($id,$table),'agrupador'=>$agrupador, 'selected'=>$selected, 'criterio'=>$criterio));
        }   
        else
             $this->render('resultMessage', 
                     array('mensaje'=>"Ocurrio un error y no se pudo liberar el cupon.<br>
                            Contactar con el administrador."));
    }
    
    public function selectTable($id)
    {
        $tables = array();
        
        $query = "SELECT * FROM ventasFlashScript WHERE idventasFlashScript = '$id'";
        $connection = Yii::app()->db2;
        $command = $connection->createCommand($query);		
        $dataReader = $command->query();
        $rows = $dataReader->read();
        
       if($rows['prefijoMensual']<>'no')
       {
           $table1M = $rows['tableName'];
           $table1M .= $rows['prefijoMensual'];
           $tables[0] = $table1M;
       }
       else
           $tables[0] = 'no';

       if($rows['prefijoBimensual']<>'no')
       {
           $table2M = $rows['tableName'];
           $table2M .= $rows['prefijoBimensual'];
           $tables[1] = $table2M;
       }
       else
           $tables[1] = 'no';
       
       if($rows['prefijoTrimestral']<>'no')
       {
           $table3M = $rows['tableName'];
           $table3M .= $rows['prefijoTrimestral'];
           $tables[2] = $table3M;
       }
       else
           $tables[2] = 'no';
       
       if($rows['prefijoSemestral']<>'no')
       {
           $table6M = $rows['tableName'];
           $table6M .= $rows['prefijoSemestral'];
           $tables[3] = $table6M;
       }
       else
           $tables[3] = 'no';
       
       if($rows['prefijoAnual']<>'no')
       {
           $table12M = $rows['tableName'];
           $table12M .= $rows['prefijoAnual'];
           $tables[4] = $table12M;
       }
       else
           $tables[4] = 'no';
       
       if($rows['prefijo18Meses']<>'no')
       {
           $table18M = $rows['tableName'];
           $table18M .= $rows['prefijo18Meses'];
           $tables[5] = $table18M;
       }
       else
           $tables[5] = 'no';
       
       if($rows['prefijo24Meses']<>'no')
       {
           $table24M = $rows['tableName'];
           $table24M .= $rows['prefijo24Meses'];
           $tables[6] = $table24M;
       }
       else
           $tables[6] = 'no';
       
       return $tables;
    }
    
    public function checkTableExist($allTables, $currentProduct)
    {
        switch ($currentProduct) 
        {
            case 1:
                if($allTables[0]<>'no')
                    return $allTables[0];
                else 
                    return false;
                break;
            case 2:
                if($allTables[1]<>'no')
                    return $allTables[1];
                else
                    return false;
                break;
            case 3:
               if($allTables[2]<>'no')
                    return $allTables[2];
               else 
                    return false;
                break;
            case 6:
               if($allTables[3]<>'no')
                    return $allTables[3];
               else 
                    return false;
                break;
            case 12:
                if($allTables[4]<>'no')
                    return $allTables[4];
                else 
                    return false;
                break;
            case 18:
               if($allTables[5]<>'no')
                    return $allTables[5];
               else 
                    return false;
                break;
            case 24:
                if($allTables[6]<>'no')
                    return $allTables[6];
                else 
                    return false;
                break;
            default:
                return false;
        }
    }

    public function actionCreateFromFile()
    {
        $dropDownProvider = AbaAgrupadores::model()->findAll(array('order'=>'agrupadorName'));
                        $list = CHtml::listData($dropDownProvider, 
                        'idventasFlashScript', 'agrupadorName');
                        
        $model = new AbaVFInsertion();

        if(isset($_POST['AbaVFInsertion']))
		{
            $model->agrupador = $_POST['AbaVFInsertion']['agrupador'];
            $model->fecha = $_POST['AbaVFInsertion']['fecha'];
            $model->count = $_POST['AbaVFInsertion']['count'];
            $model->deal = $_POST['AbaVFInsertion']['deal'];
            $model->m1 = $_POST['AbaVFInsertion']['m1'];
            $model->m2 = $_POST['AbaVFInsertion']['m2'];
            $model->m3 = $_POST['AbaVFInsertion']['m3'];
            $model->m6 = $_POST['AbaVFInsertion']['m6'];
            $model->m12 = $_POST['AbaVFInsertion']['m12'];
            $model->m18 = $_POST['AbaVFInsertion']['m18'];
            $model->m24 = $_POST['AbaVFInsertion']['m24'];
            $model->fileName = CUploadedFile::getInstance($model,'fileName');

            if( $model->validate()) {
                if (is_object($model->fileName) && get_class($model->fileName)==='CUploadedFile') {
                    $tempPath = Yii::app()->getRuntimePath().'/'.$model->fileName;
                    $model->fileName->saveAs($tempPath);
                    $this->insertCodes($model, $tempPath);
                }
            }
            else {
                $this->render('createFromFile',array('model'=>$model, 'list'=>$list));
            }

              // ** Actualizamos la campaña al maximo de los dos valores.
            $mAgrupador = AbaAgrupadores::model()->findByPk( $_POST['agrupador'] );
            if ( $mAgrupador ) {
              $mAgrupador->totalDeals = ( $mAgrupador->totalDeals >= $_POST['AbaVFInsertion']['deal'] ) ? $mAgrupador->totalDeals : $_POST['AbaVFInsertion']['deal'];
              $mAgrupador->save();
            }

        }
        else {
            $model = new AbaVFInsertion();
            $model->agrupador=1;
            $this->render('createFromFile',array('model'=>$model, 'list'=>$list));
        }
    }
   
    public function insertCodes($xModel, $xTempPath)
    {
        $agrupador = AbaAgrupadores::model()->findByPk($xModel->agrupador);
        
        $readerXLS = new codeReader();
        $readerXLS->init($xTempPath);

        $this->render('insertionResult',array('readerXLS'=>$readerXLS, 'agrupador'=>$agrupador, 'insertModel'=>$xModel));
    }
    
    public function actionSalesForAgrupador()
    {
        $agrupador=0;
        $dropDownProvider = AbaAgrupadores::model()->findAll(array('order'=>'agrupadorName'));
                        $list = CHtml::listData($dropDownProvider, 
                        'idventasFlashScript', 'agrupadorName');
                        
        $salesSearch = new AbaAgrupadorSales();
        
        if(isset($_POST['AbaAgrupadorSales']))
		{
            $salesSearch->attributes = $_POST['AbaAgrupadorSales'];
            $agrupador = AbaAgrupadores::model()->findByPk($salesSearch->agrupador);
            $products = $this->selectTable($salesSearch->agrupador);
            $model = $model = new AbaVentasFlash('1nadan-eslovenia201208s');
            $this->render('salesForAgrupadorResult',array('allTables'=>$products, 'salesSearch'=>$salesSearch, 'agrupador'=>$agrupador, 'model'=>$model));
            
        }
        else
        {
            $this->render('salesForAgrupador',array('list'=>$list, 'model'=>$salesSearch));
        }
         
    }
    
    public function actionUpdateDeals()
    {
        $agrupadores = AbaAgrupadores::model()->findAll();
        $model = new AbaVentasFlash('1nadan-eslovenia201208s');
        $this->render('updateDeals',array('agrupadores'=>$agrupadores, 'model'=>$model));
        
    }
}