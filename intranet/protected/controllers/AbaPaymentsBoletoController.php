<?php

class AbaPaymentsBoletoController extends AbaController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
    public function accessRules()
    {
        return array(

            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('admin',/* 'delete','create','update','index',*/  'view'),
                'users'=>array('root'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array(/*'admin', 'delete','create','update', 'index', 'view'*/),
                'users'=>array('marketing'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('admin',/* 'delete','create','update', 'index',*/ 'view'),
                'users'=>array('support'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new AbaPaymentsBoleto;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AbaPaymentsBoleto']))
		{
			$model->attributes=$_POST['AbaPaymentsBoleto'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AbaPaymentsBoleto']))
		{
			$model->attributes=$_POST['AbaPaymentsBoleto'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('AbaPaymentsBoleto');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
        if(isset($_GET['idPayment'])) {
            $idPayment = $_GET['idPayment'];
        }
        else {
            $idPayment = '';
        }

        if(isset($_GET['criterio'])) {
            $criterio = $_GET['criterio'];
        }
        else {
            $criterio = '';
        }

        if(isset($_GET['boletoStatus'])) {
            $boletoStatus = $_GET['boletoStatus'];
        }
        else {
            $boletoStatus = 'all';
        }

        if(isset($_GET['startDate']) && isset($_GET['endDate'])) {
            $startDate =    $_GET['startDate'];
            $endDate =      $_GET['endDate'];
        }
        else {
            $startDate =    date('Y-m-d');
            $endDate =      $startDate;
        }

        $model =    new AbaPaymentsBoleto('search');
        $contador = $model->countAll($startDate, $endDate, $boletoStatus, $criterio, $idPayment);
        $pages =    new CPagination($contador);
        $pages->setPageSize(25);

		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AbaPaymentsBoleto'])) {
            $model->attributes=$_GET['AbaPaymentsBoleto'];
        }

        //
        $abaPaymentsBoleto =    new AbaPaymentsBoleto();
        $totalsPayments =       $abaPaymentsBoleto->getLastTotals();

		$this->render('admin',array(
			'model' =>          $model,
            'pages' =>          $pages,
            'startDate' =>      $startDate,
            'endDate' =>        $endDate,
            'criterio' =>       $criterio,
            'boletoStatus' =>   $boletoStatus,
            'idPayment' =>      $idPayment,
            'totalsPayments' => $totalsPayments,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return AbaPaymentsBoleto the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=AbaPaymentsBoleto::model()->getBoletosWithUserInfoById($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param AbaPaymentsBoleto $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='aba-payments-boleto-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
