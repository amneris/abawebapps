<?php

class AbaTaxRatesController extends AbaController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
          'accessControl', // perform access control for CRUD operations
          'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
          array(
            'allow',
            'actions' => array('admin', 'index', 'view', 'create', 'update', 'delete'),
            'users' => array('root'),
          ),
          array(
            'allow',
            'actions' => array('admin', 'index', 'view', 'create', 'update', 'delete'),
            'users' => array('support'),
          ),
          array(
            'allow',
            'actions' => array('admin', 'index', 'view', 'create', 'update', 'delete'),
            'users' => array('marketing'),
          ),
          array(
            'deny',  // deny all users
            'users' => array('*'),
          ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
          'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new AbaTaxRates();

        if (isset($_POST['AbaTaxRates'])) {

            $attributes = $_POST['AbaTaxRates'];

            if (isset($attributes['dateEndRate']) && (trim($attributes['dateEndRate']) == '' || trim($attributes['dateEndRate']) == '0000-00-00 00:00:00')) {
                $attributes['dateEndRate'] = null;
            }

            $model->attributes = $attributes;
            $model->idTaxRate = null;

            if (trim($model->dateEndRate) == '' || trim($model->dateEndRate) == '0000-00-00 00:00:00') {
                $model->dateEndRate = null;
            }

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->idTaxRate));
            }
        }

        $this->render('create', array(
          'model' => $model
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (isset($_POST['AbaTaxRates'])) {

            $attributes = $_POST['AbaTaxRates'];

            if (isset($attributes['dateEndRate']) && (trim($attributes['dateEndRate']) == '' || trim($attributes['dateEndRate']) == '0000-00-00 00:00:00')) {
                $attributes['dateEndRate'] = null;
            }

            $model->attributes = $attributes;

            if (trim($model->dateEndRate) == '' || trim($model->dateEndRate) == '0000-00-00 00:00:00') {
                $model->dateEndRate = null;
            }

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->idTaxRate));
            }
        }

        $this->render('update', array(
          'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('AbaTaxRates');
        $this->render('index', array(
          'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new AbaTaxRates('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['AbaTaxRates'])) {
            $model->attributes = $_GET['AbaTaxRates'];
        }

        $this->render('admin', array(
          'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return AbaTaxRates the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = AbaTaxRates::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param AbaTaxRates $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'tax-rates-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
