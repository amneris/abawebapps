<?php

class BlognewsletterController extends AbaController
{

    protected function beforeAction($action)
    {

        if (Yii::app()->user->isGuest) {
            Yii::app()->user->returnUrl = Yii::app()->request->requestUri;
            $this->redirect('/index.php?r=site/login');
        }

        return parent::beforeAction($action);
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {

        $mPublication = new BlogNewsletterPublication();
        $params = Yii::app()->request->getParam('BlogNewsletterPublication', array('status' => 0));
        $mPublication->attributes = $params;

        $this->render('index', array(
          'mPublish' => $mPublication,
        ));
    }

    public function actionEdit()
    {
        $idPublication = Yii::app()->request->getParam('id', 0);
        $mPublication = BlogNewsletterPublication::model()->findByPk($idPublication);
        if (!$mPublication) {
            $mPublication = new BlogNewsletterPublication();
        }

        $criteria = new CDbCriteria();
        $criteria->addCondition('t.idBlog = :idBlog');
        $criteria->addCondition('t.isdeleted = :isdeleted');
        $criteria->addInCondition('t.idPublication', array(0, $mPublication->id));
        $criteria->params = CMap::mergeArray($criteria->params,
          array(':idBlog' => $mPublication->idBlog, ':isdeleted' => 0));
        $mlPost = BlogPost::model()->findAll($criteria);

        $this->render('edit', array(
          'mPublish' => $mPublication,
          'mlPost' => $mlPost,
        ));

    }

    public function actionUpdate()
    {
        $this->actionEdit();
    }

    public function actionView()
    {
        $this->actionEdit();
    }

    public function actionSave()
    {
        $errorMsg = Yii::t('app', 'Error al tratar de guardar la publicación');
        $allOk = false;

        $buttonType = Yii::app()->request->getParam('buttonType', 'saveButton');
        $status = ($buttonType == 'finishButton') ? BlogNewsletterPublication::_CLOSED : BlogNewsletterPublication::_INPROGRESS;
        $idPublication = Yii::app()->request->getParam('idPublication', 0);
        $idBlog = Yii::app()->request->getParam('idBlog', 0);

        $inputSubject = Yii::app()->request->getParam('inputSubject', '');
        $textareaHeader = Yii::app()->request->getParam('textareaHeader', '');
        $inputTweet = Yii::app()->request->getParam('inputTweet', '');
        $linkTweet = Yii::app()->request->getParam('linkTweet', '');

        $idPost1 = Yii::app()->request->getParam('idPost1', 0);
        $idPost2 = Yii::app()->request->getParam('idPost2', 0);
        $idPost3 = Yii::app()->request->getParam('idPost3', 0);
        $idPost4 = Yii::app()->request->getParam('idPost4', 0);
        $idPost5 = Yii::app()->request->getParam('idPost5', 0);
        $descPost1 = Yii::app()->request->getParam('descPost1');
        $descPost2 = Yii::app()->request->getParam('descPost2');
        $descPost3 = Yii::app()->request->getParam('descPost3');
        $descPost4 = Yii::app()->request->getParam('descPost4');
        $descPost5 = Yii::app()->request->getParam('descPost5');
        $linkPost1 = Yii::app()->request->getParam('linkPost1', Yii::t('app', 'read more') . ' >');
        $linkPost2 = Yii::app()->request->getParam('linkPost2', Yii::t('app', 'read more') . ' >');
        $linkPost3 = Yii::app()->request->getParam('linkPost3', Yii::t('app', 'read more') . ' >');
        $linkPost4 = Yii::app()->request->getParam('linkPost4', Yii::t('app', 'read more') . ' >');
        $linkPost5 = Yii::app()->request->getParam('linkPost5', Yii::t('app', 'read more') . ' >');

        $mPublication = BlogNewsletterPublication::model()->findByPk($idPublication);
        if (!$mPublication) {
            $mPublication = new BlogNewsletterPublication();
        }

        $mPublication->textSubject = trim($inputSubject);
        $mPublication->textHeader = trim($textareaHeader);
        $mPublication->textTweet = trim($inputTweet);
        $mPublication->linkTweet = trim($linkTweet);

        $mPublication->idBlog = $idBlog;
        $mPublication->status = ($mPublication->status == BlogNewsletterPublication::_CLOSED) ? $mPublication->status : $status;
        $mPublication->idPost1 = $idPost1;
        $mPublication->descriptionPost1 = trim($descPost1);
        $mPublication->linkPost1 = trim($linkPost1);
        $mPublication->idPost2 = $idPost2;
        $mPublication->descriptionPost2 = trim($descPost2);
        $mPublication->linkPost2 = trim($linkPost2);
        $mPublication->idPost3 = $idPost3;
        $mPublication->descriptionPost3 = trim($descPost3);
        $mPublication->linkPost3 = trim($linkPost3);
        $mPublication->idPost4 = $idPost4;
        $mPublication->descriptionPost4 = trim($descPost4);
        $mPublication->linkPost4 = trim($linkPost4);
        $mPublication->idPost5 = $idPost5;
        $mPublication->descriptionPost5 = trim($descPost5);
        $mPublication->linkPost5 = trim($linkPost5);

        $allOk = $mPublication->save();

        echo json_encode(array('ok' => $allOk, 'errorMsg' => $errorMsg));
    }

    public function actionPublish()
    {
        $date = Yii::app()->request->getParam('date');

        $allOk = false;
        $errorMsg = Yii::t('app', 'Error al crear la publicación.');

        $transaction = Yii::app()->dbExtranet->beginTransaction();
        try {
            $mlBlog = Blog::model()->findAllByAttributes(array('isdeleted' => 0));
            foreach ($mlBlog as $mBlog) {
                $mPublication = new BlogNewsletterPublication();
                $mPublication->dateToSend = $date;
                $mPublication->idBlog = $mBlog->id;
                $mPublication->status = BlogNewsletterPublication::_NEW;
                $mPublication->lastupdated = date("Y-m-d");
                $allOk = $mPublication->save();
                if (!$allOk) {
                    throw new Exception();
                }
            }

            $allOk = true;
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();
            $allOk = false;
        }

        echo json_encode(array('ok' => $allOk, 'errorMsg' => $errorMsg));
    }

    public function actionVerifydate()
    {
        $date = Yii::app()->request->getParam('date');

        $mPublication = BlogNewsletterPublication::model()->findAllByAttributes(array('dateToSend' => $date));
        if ($mPublication) {
            $allOk = false;
            $errorMsg = Yii::t('app', 'Publicación ya existente.');
        } else {
            $allOk = true;
            $errorMsg = '';
        }

        echo json_encode(array('ok' => $allOk, 'errorMsg' => $errorMsg));
    }

}