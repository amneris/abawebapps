<?php
class AbaPaymentsController extends AbaController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column1';


    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(

          array(
            'allow',
            'actions' => array(
              'export',
              'exportinvoices',
              'refund',
              'checkrefund',
              'MarkReconcile',
              'view',
              'create',
              'update',
              'delete',
              'index',
              'admin',
              'admin_id',
              'cancel',
              'directpayment',
              'validatecreditcard'
            ),
            'users' => array('root'),
          ),
          array(
            'allow',
            'actions' => array(
              'export',
              'exportinvoices',
              'refund',
              'checkrefund',
              'MarkReconcile',
              'view',
              'create',
              'update',
              'delete',
              'index',
              'admin',
              'admin_id',
              'cancel',
              'directpayment',
              'validatecreditcard'
            ),
            'users' => array('support'),
          ),
          array(
            'allow',
            'actions' => array(
              'export',
              'exportinvoices',
              'refund',
              'checkrefund',
              'view',
              'create',
              'update',
              'delete',
              'index',
              'admin',
              'admin_id',
              'directpayment',
              'validatecreditcard'
            ),
            'users' => array('marketing'),
          ),
          array(
            'deny',  // deny all users
            'users' => array('*'),
          ),
        );
    }

    public function actionExport()
    {
        if(isset($_POST['AbaPayments']))
        {
            $model=new AbaPayments;
            $attributes =$_POST['AbaPayments'];
            $statusArray=isset($attributes['status'])? $attributes['status']:array();
            $partnerArray=isset($attributes['idPartner'])? $attributes['idPartner']:array();
            $partnerGroupArray=isset($attributes['idPartnerGroup'])? $attributes['idPartnerGroup']:array();

            ini_set('memory_limit', '4096M');

            $includeCampaign = false;
            if(isset($_POST['includeCampaign']) AND $_POST['includeCampaign'] == 1) {
                $includeCampaign = true;
            }

            if($includeCampaign) {
                $dataReader =   $model->searchExport($statusArray, $partnerArray, $partnerGroupArray, $attributes['startDate'], $attributes['endDate'], $attributes['endDateTransaction'], $includeCampaign);
                $dataReader2 =  $model->searchExport($statusArray, $partnerArray, $partnerGroupArray, $attributes['startDate'], $attributes['endDate'], $attributes['endDateTransaction'], $includeCampaign);
            }
            else {
                $dataReader=$model->searchExport($statusArray, $partnerArray, $partnerGroupArray, $attributes['startDate'], $attributes['endDate'], $attributes['endDateTransaction']);
            }

            $aCcDescriptions = HeList::creditCardDescription();

//			$dataReader=$model->searchExport($attributes['status'], $attributes['idPartner'], $attributes['idPartnerGroup'], $attributes['startDate'], $attributes['endDate']);
            $data = array();
            //	$xls = new JPhpExcel('UTF-8', false, 'Payments');			
            $xls = new JPhpCsv('UTF-8', false, 'Payments');
            $flagFirst=true;


            //
            //#5113
            //

            if($includeCampaign) {

                $ventasFlashData = array();
                while(($row = $dataReader2->read()) !== false) {

                    if(trim($row['vfTableName']) != '') {

                        $sTable = '';

                        switch($row['idPeriodPay']) {
                            case 30:
                                $sTable = $row['vfTableName'] . $row['prefijoMensual'];
                                break;
                            case 60:
                                $sTable = $row['vfTableName'] . $row['prefijoBimensual'];
                                break;
                            case 90:
                                $sTable = $row['vfTableName'] . $row['prefijoTrimestral'];
                                break;
                            case 180:
                                $sTable = $row['vfTableName'] . $row['prefijoSemestral'];
                                break;
                            case 360:
                                $sTable = $row['vfTableName'] . $row['prefijoAnual'];
                                break;
                            case 540:
                                $sTable = $row['vfTableName'] . $row['prefijo18Meses'];
                                break;
                            case 720:
                                $sTable = $row['vfTableName'] . $row['prefijo24Meses'];
                                break;
                        }

                        $coupons = array();
                        if(isset($ventasFlashData[$row['vfIdventasFlashScript']]['coupons'])) {
                            $coupons = $ventasFlashData[$row['vfIdventasFlashScript']]['coupons'];
                        }

                        if(!in_array($row['idPromoCode'], $coupons)) {
                            $coupons[$row['idPromoCode']] = $row['idPromoCode'];
                        }

                        $ventasFlashData[$row['vfIdventasFlashScript']] = array(
                            "vfTableName" =>            $row['vfTableName'],
                            "vfIdventasFlashScript" =>  $row['vfIdventasFlashScript'],
                            "vfIdPartnerSource" =>      $row['vfIdPartnerSource'],
                            "idPeriodPay" =>            $row['idPeriodPay'],
                            "table" =>                  $sTable,
                            "coupons" =>                $coupons,
                        );
                    }
                }

                foreach ($ventasFlashData as $idventasFlashScript => $stValue) {
                    $ventasFlashData[$idventasFlashScript]['coupons'] = AbaVentasFlash::getExcelData(
                        $stValue['table'],
                        $stValue['coupons']
                    );
                }

                while (($row = $dataReader->read()) !== false) {

                    unset(
                    $row['vfTableName'],
                    $row['vfIdventasFlashScript'],
                    $row['vfIdPartnerSource'],
                    $row['prefijoMensual'],
                    $row['prefijoBimensual'],
                    $row['prefijoTrimestral'],
                    $row['prefijoSemestral'],
                    $row['prefijoAnual'],
                    $row['prefijo18Meses'],
                    $row['prefijo24Meses']
                    );

                    if ($flagFirst) {
                        $flagFirst = false;
                        $theRow = array();
                        foreach ($row as $key => $value) {
                            array_push($theRow, $key);
                        }

                        foreach ($theRow as $key => $value) {
                            if ($value == 'paymentPartnerId') {
                                unset($theRow[$key]);
                                break;
                            }
                        }

                        array_push($data, $theRow);
                    }

                    $theRow = array();

                    $partnerId = '';
                    $idPromoCode = '';

                    foreach ($row as $key => $value) {
                        if ($key == 'paymentPartnerId') {
                            $partnerId = $value;
                        } else {
                            if ($key == 'idPromoCode') {
                                $idPromoCode = $value;
                            }
                        }
                    }

                    unset($row['paymentPartnerId']);

                    foreach ($row as $key => $value) {
                        if ($key == 'creditCardType') {
                            if (array_key_exists($value, $aCcDescriptions)) {
                                $value = $aCcDescriptions[$value];
                            }
                        }

                        if ($key == 'campaign') {
                            foreach ($ventasFlashData as $idventasFlashScript => $stValue) {
                                if ($idventasFlashScript == $partnerId AND isset($stValue['coupons'])) {
                                    foreach ($stValue['coupons'] as $sCoupon => $stCouponData) {
                                        //            if($sCoupon == $idPromoCode) {
                                        if ($stCouponData['code1'] == $idPromoCode) {
                                            $value = $stCouponData['campaign'];
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        array_push($theRow, $value);
                    }
                    array_push($data, $theRow);
                }
            }
            else {
                while(($row = $dataReader->read())!==false)
                {
                    if($flagFirst)
                    {
                        $flagFirst= false;
                        $theRow=array();
                        foreach ($row as $key =>$value)
                            array_push($theRow, $key);
                        array_push($data, $theRow);
                    }
                    $theRow=array();
                    foreach ($row as $key =>$value) {
                        if($key == 'creditCardType') {
                            if(array_key_exists($value, $aCcDescriptions)) {
                                $value = $aCcDescriptions[$value];
                            }
                        }
                        array_push($theRow, $value);
                    }
                    array_push($data, $theRow);
                }
            }


            if($flagFirst == true) {
                $theRow=array();
                array_push($theRow, "DATA NOT FOUND(Data not exported)");
                array_push($data, $theRow);
            }

            $xls->addArray($data);
            $xls->generateXML("payments_".substr($attributes['startDate'],0,10)."_to_".substr($attributes['endDate'],0,10));
        }
        else
        {
            $modelPartnerList= new AbaPartnersList;
            $partnerList=$modelPartnerList->getFullList();
            $model=new AbaPayments;
            $extrainfo=array('statusDescription'=> HeList::statusDescription(),
                'partnerList'=>$partnerList,
                'partnerGroupList'=> HeList::partnerGroupList());
            $this->render('excel',array(
                'model'=>$model,
                'extrainfo'=>$extrainfo,
            ));
        }
    }



    /**
     * @param string $userId
     *
     * @throws CHttpException
     */
    public function actionCancel( $userId )
    {
        if (isset($_POST['AbaPayments'])) {
            $moPayment = new AbaPayments;
            $moPayment->attributes = $_POST['AbaPayments'];
            if ($moPayment->action != 'FAIL' && $moPayment->action != 'CANCEL') {
                Yii::app()->user->setFlash('mustHaveAction', 'You must select any action.');
                $moPayment = AbaPayments::getModelCancelFailUser($moPayment->userId);
                $this->render('cancel', array(
                    'model' => $moPayment, 'userId' => $userId,
                ));
            } else {
                $succAction = false;
                $modelUser = AbaUser::model()->findByPk($moPayment->userId);

                //
                // Cancel Adyen recurring contract
                //
                if($moPayment->paySuppExtId == PAY_SUPPLIER_ADYEN OR $moPayment->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP) {

                    $sendToCancelGateway = false;
                    if (array_key_exists( 'sendToCancelGateway', $_POST)){
                        $sendToCancelGateway = intval($_POST['sendToCancelGateway']);
                    }
                    if(intval($sendToCancelGateway)) {
                        $aRetCampusCancel = HeWebServices::cancelPayment($moPayment->id, $moPayment->paySuppOrderId);
                        HeLogger::sendLog(
                          HeLogger::PREFIX_ERR_LOGIC_L . ' Intranet. Process cancel adyen payment',
                            HeLogger::IT_BUGS_PAYMENTS,
                            HeLogger::CRITICAL,
                            'actionCancel'
                        );
                    }
                }
                if ($modelUser === null) {
                    throw new CHttpException(403, 'The user not exist.');
                }
                if ($moPayment->action == 'FAIL') {
                    $succAction = AbaPayments::failUser($moPayment);
                }
                if ($moPayment->action == 'CANCEL') {
                    $succAction = AbaPayments::cancelUser($moPayment);
                }
                if($succAction){
                    $this->redirect(array('view', 'id' => $moPayment->id));
                    return;
                } else{
                    $this->render('cancel', array('model' => $moPayment, 'userId' => $userId));
                }

            }
        } else {
            $moPayment = AbaPayments::getModelCancelFailUser($userId);
            if (isset($moPayment->attributes['paySuppExtId']) ) {

                if ($moPayment->attributes['paySuppExtId'] == PAY_SUPPLIER_Z
                  || $moPayment->attributes['paySuppExtId'] == PAY_SUPPLIER_APP_STORE
                  || $moPayment->attributes['paySuppExtId'] == PAY_SUPPLIER_ANDROID_PLAY_STORE
                ) {
                    $moPayment->addError('action', 'Please, we do NOT recommend neither to CANCEL nor mark as FAILED this payment
                                        because it belongs to a APPSTORE / PLAYSTORE / ZUORA user.');
                }
            }
            $this->render('cancel', array('model' => $moPayment, 'userId' => $userId));
        }
    }

    /**
     * @param integer $id user id
     * @param $payment id from payment
     *
     * @throws CHttpException
     */
    public function actionRefund($id, $payment, $chargebackrefundid=null) {

        //#4986
        if(isset($_POST['AbaPaymentsAdyenPendingRefunds'])) {
            //
            $model =             new AbaPayments;
            $model->attributes = $_POST['AbaPayments'];

            //
            if($model->paySuppExtId == PAY_SUPPLIER_ADYEN OR $model->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP) {

                $this->checkAllRefunds($model, $id, $payment);

                //
                // Check if exist PENDING REFUND (0)
                //
                $oAdyenPendingRefund = AbaPayments::getAdyenRefundAttemptByPaymentOrigin($payment, array(
                    PAY_ADYEN_PENDING_REFUND_PENDING => PAY_ADYEN_PENDING_REFUND_PENDING
                ));

                if($oAdyenPendingRefund) {
                    $this->redirect(array('checkrefund', 'id' => $id, 'payment' => $payment));
                }
            }
        }

        //
        if (isset($_POST['AbaPayments'])) {

            $model =                new AbaPayments;
            $model->attributes =    $_POST['AbaPayments'];
            $sendToPayGateway =     false;
            if (array_key_exists( 'sendToPayGateway', $_POST)){
                $sendToPayGateway = intval($_POST['sendToPayGateway']);
            }

            $aRetCampusRefund = array("paySuppExtUnid" => $model->paySuppExtUniId,
                "paySuppOrderId"  => $model->paySuppOrderId,
                "idPayment"  => "A".(AbaPayments::generateIdPayment($model->userId)));
            if (!HeQuery::testRegisterSuccess($model->userId, $model->id)){
                $model->addError("paySuppOrderId", "A refund has been already registered for this payment. ".
                    "Probably you opened a new window already. Please Ask IT in case of help.");
                $this->render('createabono', array('model' => $model));
                return;
            }

            if (intval($sendToPayGateway) && $model->paySuppExtId != PAY_SUPPLIER_GROUPON &&
                $model->paySuppExtId != PAY_SUPPLIER_PAYPAL  && $model->paySuppExtId != PAY_SUPPLIER_B2B
            ) {
                //
                //#3132
                $bIsParcialSuccess = HeQuery::isParcialSuccess($model->id);

                // Total refund OR parcial if supplier is Adyen
                if(!$bIsParcialSuccess OR ($bIsParcialSuccess AND ($model->paySuppExtId == PAY_SUPPLIER_ADYEN OR $model->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP))) {
                    $aRetCampusRefund = HeWebServices::refundPayment($model->id, floatval($model->amountPrice), $model->paySuppOrderId);
                }
            }

            //
            //#4986
            if(intval($sendToPayGateway) && ($model->paySuppExtId == PAY_SUPPLIER_ADYEN OR $model->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP)) {

                // save adyen refund attempt
                $oAdyenSuccessRefund = AbaPayments::saveAdyenRefundAttempt($model, $aRetCampusRefund, $payment);

                if($oAdyenSuccessRefund) {
                    $this->redirect(array('checkrefund', 'id' => $id, 'payment' => $payment));
                }
                else {
                    throw new CHttpException(404, 'The refund is executed in ADYEN but due to an unknown error the refund is not visible on the Intranet FOR user id => ' . $id);
                }
            }
            else {

                if (is_array($aRetCampusRefund) && count($aRetCampusRefund) == 3) {

                    if (AbaPayments::refundUser($model, $payment, $aRetCampusRefund["idPayment"], $aRetCampusRefund["paySuppExtUnid"], $aRetCampusRefund["paySuppOrderId"])) {

                        //#6084
                        if (is_numeric($chargebackrefundid) AND $chargebackrefundid > 0) {
                            $oAbaPaymentsAdyenFraud = new AbaPaymentsAdyenFraud();
                            if($oAbaPaymentsAdyenFraud->getAdyenFraudById($chargebackrefundid)) {
                                $oAbaPaymentsAdyenFraud->updateToRefund();
                            }
                        }

                        $id = $model->id;
                        $model->unsetAttributes(); // clear no desirable values
                        $model->id = $id;
                        $this->redirect(array('view', 'id' => $model->id));
                    } else {
                        throw new CHttpException(404, 'Refund is not possible for user id => ' . $id);
                    }
                }
                else {
                    if(is_array($aRetCampusRefund)){
                        $errorCode = array_keys($aRetCampusRefund);
                        $errorRefund = $errorCode[0]." ".$aRetCampusRefund[$errorCode[0]];
                    } else {
                        $errorRefund = "Campus web service refundPayment has returned an error.";
                    }
                    $model->addError("paySuppOrderId", "Automatic refund could not be processed. Error description:" . $errorRefund);
                    $this->render('createabono', array('model' => $model));
                }
            }
        }
        else {

            $model = AbaPayments::getModelRefundUser($id, $payment);

            if ($model != null) {
                //
                if (
                  $model->paySuppExtId <> PAY_SUPPLIER_ANDROID_PLAY_STORE
                    AND $model->paySuppExtId <> PAY_SUPPLIER_Z
                ) {
                    if ($model->paySuppExtId == PAY_SUPPLIER_ADYEN OR $model->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP) {

                        $this->checkAllRefunds($model, $id, $payment);

                        //
                        // Check if exist PENDING REFUND (0)
                        //
                        $oAdyenPendingRefund = AbaPayments::getAdyenRefundAttemptByPaymentOrigin($payment, array(
                          PAY_ADYEN_PENDING_REFUND_PENDING => PAY_ADYEN_PENDING_REFUND_PENDING
                        ));

                        if ($oAdyenPendingRefund) {
                            $this->redirect(array('checkrefund', 'id' => $id, 'payment' => $payment));
                        }
                    }

                    $model->userId = $id;
                    $now = HeDate::today();
                    $model->dateToPay = $now;
                    $model->dateStartTransaction = $now;
                    $model->dateEndTransaction = $now;
                    $model->status = PAY_REFUND;
                    $model->isNewRecord = true;
                    $this->render('createabono', array('model' => $model));
                }else {
                    $model->userId = $id;
                    $now = HeDate::today();
                    $model->dateToPay = $now;
                    $model->dateStartTransaction = $now;
                    $model->dateEndTransaction = $now;
                    $model->status = PAY_REFUND;
                    $model->isNewRecord = true;
                    $this->render('createabono', array('model' => $model));

                 }
            } else {
                throw new CHttpException(404, 'Refund is not possible, no exist payments for user id => ' . $id);
            }
        }
    }

    /**
     * @param $id
     * @param $payment
     *
     * @throws CHttpException
     */
    public function actionCheckRefund($id, $payment) {

        //#4986
        if(isset($_POST['AbaPaymentsAdyenPendingRefunds'])) {
            //
            $model =             new AbaPayments;
            $model->attributes = $_POST['AbaPayments'];

            //
            if($model->paySuppExtId == PAY_SUPPLIER_ADYEN OR $model->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP) {

                $this->checkAllRefunds($model, $id, $payment);

                //
                // Check if exist FAIL REFUND
                //
                $oAdyenCancelRefund = AbaPayments::getAdyenRefundAttemptByPaymentOrigin($payment, array(
                    PAY_ADYEN_PENDING_REFUND_FAIL => PAY_ADYEN_PENDING_REFUND_FAIL
                ));

                if($oAdyenCancelRefund) {
                    $this->redirect(array('refund', 'id' => $id, 'payment' => $payment));
                }
            }
        }

        //
        $model = AbaPayments::getModelRefundUser($id, $payment);

        if($model != null) {

            $model->userId =                $id;
            $now =                          HeDate::today();
            $model->dateToPay =             $now;
            $model->dateStartTransaction =  $now;
            $model->dateEndTransaction =    $now;
            $model->status =                PAY_REFUND;
            $model->isNewRecord =           true;

            $oAdyenPendingRefund = AbaPayments::getAdyenRefundAttemptByPaymentOrigin($payment, array(
                PAY_ADYEN_PENDING_REFUND_PENDING => PAY_ADYEN_PENDING_REFUND_PENDING
            ));

            if(!$oAdyenPendingRefund) {
                $this->redirect(array('refund', 'id' => $id, 'payment' => $payment));
            }

            $this->render('checkrefundoption', array('model' => $model, 'adyenSuccessRefund' => $oAdyenPendingRefund));

        } else {
            throw new CHttpException(404,'NO exist payments for id => '.$id );
        }
    }

    /**
     * @param $model
     * @param $id
     * @param $payment
     * @param $action
     *
     * @throws CHttpException
     */
    protected function checkAllRefunds($model, $id, $payment) {

        //
        if($model->paySuppExtId == PAY_SUPPLIER_ADYEN OR $model->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP) {
            //
            // Check if exists REFUNDED REFUND
            //
            $oAdyenRefundedRefund = AbaPayments::getAdyenRefundAttemptByPaymentOrigin($payment, array(
                PAY_ADYEN_PENDING_REFUND_REFUNDED => PAY_ADYEN_PENDING_REFUND_REFUNDED
            ));

            if($oAdyenRefundedRefund) {
                $this->redirect(array('admin_id', 'id' => $id));
            }

            //
            // Check if exists SUCCESS NOTIFICATION
            //
            $oAdyenSuccessRefund = AbaPayments::getAdyenRefundAttemptByPaymentOrigin($payment, array(
                PAY_ADYEN_PENDING_REFUND_SUCCESS_NOTIFICATION => PAY_ADYEN_PENDING_REFUND_SUCCESS_NOTIFICATION
            ));

            if($oAdyenSuccessRefund) {

// @TODO - email bugs
// @TODO - email bugs
// @TODO - email bugs

                // Partial refunds
                $model->paySuppOrderId =    $oAdyenSuccessRefund->paySuppOrderId;
                $model->amountPrice =       $oAdyenSuccessRefund->amountPrice;

                if(trim($oAdyenSuccessRefund->dateToPay) != '') {
                    $model->dateToPay = $oAdyenSuccessRefund->dateToPay;
                }

                if (AbaPayments::refundUser($model, $payment,
                    $oAdyenSuccessRefund->idPayment,
                    $oAdyenSuccessRefund->pspReference,
                    $oAdyenSuccessRefund->merchantReference))
                {
                    //
                    $bSuccessUpdate = AbaPayments::updateAdyenRefundAttempt($oAdyenSuccessRefund->id);

                    $id = $model->id;
                    $model->unsetAttributes(); // clear no desirable values
                    $model->id = $id;
                    $this->redirect(array('view', 'id' => $model->id));
                }
                else {
                    throw new CHttpException(404, 'The refund is executed in ADYEN but due to an unknown error the refund is not visible on the Intranet FOR user id => ' . $id);
                }
            }
        }
    }

    /**
     * @param $userId
     * @param $idPayment
     * @throws CHttpException
     */
    public function actionMarkReconcile($userId, $idPayment)
    {
        if (isset($_POST['AbaPayments'])) {
            $moNewPayment = new AbaPayments();
            $moNewPayment->attributes = $_POST['AbaPayments'];
            $moOldPayment = new AbaPayments();
            $moOldPayment->loadModelById($moNewPayment->id);
            if ($moNewPayment->paySuppExtUniId == '') {
                $moNewPayment->addError("paySuppExtUniId",
                    "Value can not be empty. If you reconcile this payment you should set the gateway identifier");
                $moNewPayment->loadModelById($moNewPayment->id);
                $this->render('markReconcile', array('model' => $moNewPayment));
                return;
            }

            $moReconcPay = new ReconcPayment();
            $moReconcPay = $moReconcPay->findByPk($moNewPayment->id);
            if (intval($moNewPayment->paySuppLinkStatus) == PAY_RECONC_ST_MATCHED) {
                $moReconcPay->markAsReconciled($moNewPayment->paySuppExtUniId);
            } else {
                $moReconcPay->markAsNotReconciled($moNewPayment->paySuppExtUniId);
            }

            $moNewPayment->loadModelById($moNewPayment->id);
            $this->render('view', array('model' => $moNewPayment));
            return;

        } else {
            $moPayment = AbaPayments::getPayment($userId, $idPayment);
            $this->render('markReconcile', array('model' => $moPayment));
            return;
        }
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {

        $oPaymentModel = $this->loadModel($id);

        //
        $abaPaymentsAdyenNotifications = false;
        if($oPaymentModel->paySuppExtId == PAY_SUPPLIER_ADYEN OR $oPaymentModel->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP) {
            $abaPaymentsAdyenNotifications = new AbaPaymentsAdyenNotifications();
            $abaPaymentsAdyenNotifications->getAdyenNotificationByReferences($oPaymentModel->paySuppExtUniId, $oPaymentModel->paySuppOrderId);
        }

        //
        $this->render('view',array(
            'model' => $oPaymentModel,
            'modelNotifications' => $abaPaymentsAdyenNotifications,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model=new AbaPayments;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['AbaPayments']))
        {
            $model->attributes=$_POST['AbaPayments'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['AbaPayments']))
        {
            $model->attributes=$_POST['AbaPayments'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider=new CActiveDataProvider('AbaPayments');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new AbaPayments('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['AbaPayments'])){
            $model->attributes=$_GET['AbaPayments'];
        }

        $this->render('admin', array( 'model' => $model ));
    }

    public function actionAdmin_id($id)
    {
        $user =     AbaUser::model()->findByPk($id);
        $model =    AbaPayments::model()->with('Usuario')->find("userId=".$id );
        if($model != null)
        {
            $id="=".$model->userId;
            $model->unsetAttributes();  // clear any default values
            $model->userId = $id;

            $this->render('admin',array( 'model'=>$model, 'email'=>$user->email ));
        } else {
            throw new CHttpException(404,'NO exist payments for id => '.$id );
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param string $id
     *
     * @return CActiveRecord
     *
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=AbaPayments::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='aba-payments-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Export payments with taxes info
     *
     */
    public function actionExportInvoices()
    {
        if(isset($_POST['AbaPayments']))
        {
            $model = new AbaPayments;
            $attributes =           $_POST['AbaPayments'];
            $statusArray =          isset($attributes['status'])? $attributes['status']:array();
            $partnerGroupArray =    isset($attributes['idPartnerGroup'])? $attributes['idPartnerGroup']:array();
            $dataReader =           $model->searchExportInvoices($statusArray, $partnerGroupArray, $attributes['startDate'], $attributes['endDate']);

            $aCcDescriptions = HeList::creditCardDescription();

            $data = array();
            $xls = new JPhpCsv('UTF-8', false, 'Payments');
            $flagFirst = true;
            while(($row = $dataReader->read())!==false)
            {
                if($flagFirst)
                {
                    $flagFirst= false;
                    $theRow=array();
                    foreach ($row as $key =>$value)
                        array_push($theRow, $key);
                    array_push($data, $theRow);
                }
                $theRow=array();
                foreach ($row as $key =>$value) {
                    if($key == 'creditCardType') {
                        if(array_key_exists($value, $aCcDescriptions)) {
                            $value = $aCcDescriptions[$value];
                        }
                    }
                    array_push($theRow, $value);
                }
                array_push($data, $theRow);
            }
            if($flagFirst==true)
            {
                $theRow=array();
                array_push($theRow, "DATA NOT FOUND(Data not exported)");
                array_push($data, $theRow);
            }
            $xls->addArray($data);
            $xls->generateXML("payments_".substr($attributes['startDate'],0,10)."_to_".substr($attributes['endDate'],0,10));
        }
        else
        {
            $modelPartnerList = new AbaPartnersList;
            $partnerList =      $modelPartnerList->getFullList();
            $model =            new AbaPayments;
            $extrainfo =        array
            (
                'statusDescription' =>          HeList::statusInvoicesDescription(),
                'partnerList' =>                $partnerList,
                'invoicesPartnerGroupList' =>   HeList::invoicesPartnerGroupList()
            );
            $this->render('excelinvoices',array(
                'model'=>$model,
                'extrainfo'=>$extrainfo,
            ));
        }
    }

    /**
     * @param integer $id user id
     * @param $idPayment id from payment
     *
     * @throws CHttpException
     */
    public function actionDirectPayment($userId, $idPayment)
    {

        //
        if (isset($_POST['AbaPayments'])) {

            $model = new AbaPayments();
            $model->attributes = $_POST['AbaPayments'];

            $dateToPay = '';
            if (array_key_exists('dateToPay', $_POST['AbaPayments'])) {
                $dateToPay = $_POST['AbaPayments']['dateToPay'];
            }

            $aRetCampusDirectPayment = array(
              "paySuppExtUnid" => $model->paySuppExtUniId,
              "paySuppOrderId" => $model->paySuppOrderId,
              "idPayment" => ""
            );

            if ($model->paySuppExtId == PAY_SUPPLIER_CAIXA
            ) {
                $aRetCampusDirectPayment = HeWebServices::directPayment($model->id, floatval($model->amountPrice),
                  $dateToPay);
            }

            if (is_array($aRetCampusDirectPayment) && count($aRetCampusDirectPayment) == 3) {

                $id = $aRetCampusDirectPayment["idPayment"];
                $model->unsetAttributes();
                $model->id = $id;

                $this->redirect(array('view', 'id' => $id));
            } else {
                if (is_array($aRetCampusDirectPayment)) {
                    $errorCode = array_keys($aRetCampusDirectPayment);
                    $errorRefund = $errorCode[0] . " " . $aRetCampusDirectPayment[$errorCode[0]];
                } else {
                    $errorRefund = "Campus web service directPayment has returned an error.";
                }
                $model->addError("paySuppOrderId",
                  "Automatic Direct Payment could not be processed. Error description:" . $errorRefund);
                $this->render('directpayment', array('model' => $model));
            }
        } else {

            $model = AbaPayments::getModelDirectPaymentUser($userId, $idPayment);

            if ($model <> null) {
                $model->userId = $userId;
                $now = HeDate::today();
                $model->dateToPay = $now;
                $model->dateStartTransaction = $now;
                $model->dateEndTransaction = $now;
                $model->status = PAY_PENDING;
                $model->isNewRecord = true;
                $this->render('directpayment', array('model' => $model));
            } else {
                throw new CHttpException(404,
                  'Direct payment is not possible, no exist payments for user id => ' . $userId);
            }
        }
    }

    /**
     *
     */
    public function actionValidateCreditCard()
    {
        $model = new AbaValidateCreditCardForm();

        //
        if (isset($_POST['AbaValidateCreditCardForm'])) {

            $cardNumber = str_replace(" ", "", trim((isset($_POST['AbaValidateCreditCardForm']['cardNumber']) ? $_POST['AbaValidateCreditCardForm']['cardNumber'] : '')));
            $attributes = $_POST['AbaValidateCreditCardForm'];
            $attributes['cardNumber'] = $cardNumber;

            $model->attributes = $attributes;
            $model->cardNumberTypeId = HePayments::getCreditCardType($cardNumber);

            $model->validateCardType();
        }

        $this->render('validatecreditcard', array('model' => $model));
    }

}
