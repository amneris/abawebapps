<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',

	'name'=>'Back Office AbaEnglish',
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.extensions.phpexcel.*',
		'application.extensions.calendar.*',
        'application.extensions.excelReader.*',
		'application.components.widgets.*',
    'application.modules.reconciliationParser.models.*',
    'commonNamespace.models.intranet.selfgenerated.*',
    'commonNamespace.models.intranet.*',
	),

	'language'=>'en',
	'modules'=>array(
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'abaenglish',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			//'ipFilters'=>array('127.0.0.1','::1'),
			'ipFilters'=>array('127.0.0.1','::1','*'),
		),
        'reconciliationParser' => array(
            'fileType' => 'CSV',
            'urlAccess' => true,
        ),
		/**/
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
              'class' => 'AbaWebUser',
		),
		'format'=>array(
			'class'=>'application.components.AbaFormatter',
			'numberFormat'=>array('decimals'=>2, 'decimalSeparator'=>',', 'thousandSeparator'=>'-'),
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'session' => array(
			'sessionName' => 'intranetaba_sn',
			'class' => 'CDbHttpSession',
			'connectionID' => 'db',
			'autoCreateSessionTable' => false,
			'cookieMode' => 'only',
			'timeout' => 1800, // CHttpSession defaults to 1440 seconds. 1440 Seconds = 24 minutes
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'jforcada@abenglish.com',
    'mainDbName' => 'aba_b2c',
    'dbCampusLogs' =>   'aba_b2c_logs',
    'dbCampusSummary' => 'aba_b2c_summary',

    'feedpress_key' => '5539f99f63836',
    'feedpress_token' => '553c2291adef5',

		'logFacility' => (defined('LOG_LOCAL7') ? LOG_LOCAL7 : 8),
		'logGroup' => "intranet",
		'logName' => "[ABAWEBAPPS]",
	),
);
