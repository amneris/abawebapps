<?php

$aFixArrayForTest = CMap::mergeArray(
    require(dirname(__FILE__).'/main.php'),
    array(
        'components'=>array(
            'fixture'=>array(
                'class'=>'system.test.CDbFixtureManager',
            ),
        ),
    )
);
// Language used to assert the text in the Selenium Tests:
$aFixArrayForTest["params"]["langAssert"]="es";
$aFixArrayForTest['import'][]='application.tests.basetest.*';

/* PREPRODUCTION:172.16.1.100  */

/*$aFixArrayForTest["components"]['db']=array(
    'class'=>'CDbConnection',
    'connectionString' => 'mysql:host=172.16.1.100;dbname=aba_b2c',
    'emulatePrepare' => true,
    'username' => 'abaapps',
    'password' => 'abaenglish',
    'charset' => 'utf8',
);*/


return $aFixArrayForTest;
