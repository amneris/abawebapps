<?php

/**
 * Class ProcessUserProgressCommand
 *
 * PROCESS ALL USERS PROGRESS QUEUES
 *
 */

date_default_timezone_set('Europe/Madrid');

class ProcessUserProgressCommand extends AbaConsoleCommand
{
    protected $aListUsersErrors = array();
    protected $aListFollowupsSuccess = array();

    public function getHelp()
    {
        return parent::getHelp() . " First argument: You can set a number to set how many records you want " .
        "to send to SELLIGENT. \n " .
        " Second argument: Optional, days in advance for warning. \n " .
        " Otherwise 200 users and 15 days in advance will be used.";
    }

    //
    // TEST:    /usr/bin/php  intranet/public/script.php  ProcessUserProgress
    //
    public function run($args)
    {
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
        echo " \n START. ProcessUserProgressCommand: executing auto process at " . date("Y-m-d h:i:s", time()) . "\n";

        $allSuccess = true;

        $iLimitOffset = 5;

        if (count($args) >= 1) {

            if (!is_numeric($args[0])) {
                echo " The first argument should be an integer telling how many records we want to process for every iteration.\n";
                return false;
            }

            if(intval($args[0]) <= 0) {
                echo " The number must be greater than zero.\n";
                return false;
            }

            $iLimitOffset = intval($args[0]);
        }

        //
        //
        $oAbaUserProgressQueues = new AbaUserProgressQueues();
        $stUserProgressQueues = $oAbaUserProgressQueues->getUserProgressQueuesForProcess(AbaUserProgressQueues::QUEUE_STATUS_PENDING, $iLimitOffset);


        foreach($stUserProgressQueues AS $iKey => $stUserProgressQueue) {

            /**
             * Validate
             */
            $iUserId = trim($stUserProgressQueue["userId"]);
            if(!is_numeric($iUserId)) {

                echo "\n+ + + ERROR: INVALID USER ID '" . $iUserId . "' FOR QUEUEID '" . trim($stUserProgressQueue["queueId"]) . "' + + + " . date("Y-m-d H:i:s", time()) . " \n";

                continue;
            }

            $sUnits = trim($stUserProgressQueue["units"]);
            if(trim($sUnits) == '') {

                echo "\n+ + + ERROR: INVALID UNITS '" . $sUnits . "' FOR QUEUEID '" . trim($stUserProgressQueue["queueId"]) . "' + + + " . date("Y-m-d H:i:s", time()) . " \n";

                continue;
            }

            echo "\n+ + + START PROCESS IN BACKGROUND FOR QUEUEID '" . trim($stUserProgressQueue["queueId"]) . "' + + + " . date("Y-m-d H:i:s", time()) . " \n";

//            $sUserProgressQueueCronPath = Yii::app()->config->get("USER_PROGRESS_QUEUES_CRON_PATH");
//            $stExecResult = shell_exec(' ' . $sUserProgressQueueCronPath . '  CreateUserProgress  "' . trim($stUserProgressQueue["queueId"]) . '"  > /dev/null & ');
////            $stExecResult = shell_exec(' . /home/rundeck/environment_vars;  /usr/bin/php  /var/app/abawebapps/intranet/public/script.php  CreateUserProgress  "' . trim($stUserProgressQueue["queueId"]) . '"  > /dev/null & ');
            $stExecResult = shell_exec(' /usr/bin/php  intranet/public/script.php  CreateUserProgress  "' . trim($stUserProgressQueue["queueId"]) . '"  > /dev/null & ');

        }

        echo " \n " . date("Y-m-d h:i:s", time()) . " Success in process ProcessUserProgressCommand. END";
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

        return $allSuccess;
    }

    /**
     * @param $actionDetails
     *
     * @return bool
     */
    private function processError($actionDetails)
    {
        HeAbaMail::sendEmailInternal(Yii::app()->config->get("EMAIL_IT_BUGS"),
            "",
            "CRON ProcessUserProgressCommand finished with errors ",
            "CRON ProcessUserProgressCommand finished with errors ",
            true,
            $this,
            HeAbaMail::BUGS_LAYOUT);

        return true;
    }

}

