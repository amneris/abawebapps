<?php

class RefundPaypalPaymentCommand extends AbaConsoleCommand
{
    //
    // TEST:    php -f  /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment 1498094 63b0bfae 30.50
    // TEST:    php -f  /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment 905188 6a01abc0
    //
    // TEST:    /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment 2292927 174c880b
    //
    // TEST:    /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment 1111110 69ea60b4
    //
    // TEST:    /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment 73559 18d7596b
    //
    // TEST:    /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment 2293273 3d99c228_AK1
    // TEST:    /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment 2293273 3d99c228_AK2
    //
    // TEST:    /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment 1157886 1b2c04dc
    //
    // TEST:    /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment 3014403 5f681d70
    //
    // TEST:    /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment 3103315 76467eac
    //
    // TEST:    /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment 1420248 138714f4
    //
    // TEST:    /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment 3563757 5c2e10b2
    //
    // TEST:    /usr/bin/php  /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment 3553235 13ce27d5_AK1
    // TEST:    /usr/bin/php  /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment 3553235 13ce27d5_AK2
    // TEST:    /usr/bin/php  /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment 3553235 13ce27d5_AK3
    // TEST:    /usr/bin/php  /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment 3553235 13ce27d5_AK4
    // TEST:    /usr/bin/php  /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment 3553235 13ce27d5_AK5
    // TEST:    /usr/bin/php  /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment 3553235 13ce27d5_AK6
    // TEST:    /usr/bin/php  /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment 3553235 13ce27d5_AK7
    //

    // TEST:    /usr/bin/php  /var/www/abawebapps/intranet/public/script.php  RefundPaypalPayment  726384  d1c3a5aa  155.35
    //
    // TEST:    /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment 918845 a680ed0a_AK2
    //
    // TEST:    /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment  2152853  AK_7da6eda4_2
    // TEST:    /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment  2152853  AK_7da6eda4_3
    //
    // TEST:    /var/www/abawebapps/intranet/public/script.php RefundPaypalPayment  1723251  cab6604b  129.35
    //
    public function run($args)
    {
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n RefundPaypalPaymentCommand: executing auto process at " . date("Y-m-d H:i:s", time() );

        $userId =           null;
        $successPaymentId = null;
        $totalToRefund =    null;

        //
        // USER ID
        if (count($args) >= 1) {
            if (!is_numeric($args[0])) {
                echo " The first argument should be an integer telling how many records we want to process for every iteration.";
                return false;
            }
            $userId = intval($args[0]);
        }
        else {
            return false;
        }

        //
        // SUCCESS PAYMENT ID TO REFUND
        if (count($args) >= 2) {
            if (trim($args[1]) == '') {
                echo " The second argument should be an string.";
                return false;
            }
            $successPaymentId = trim($args[1]);
        }
        else {
            return false;
        }

        //
        // TOTAL TO REFUND [OPCIONAL]
        if (count($args) >= 3) {
            if (trim($args[2]) != '') {
                if(is_numeric($args[2])) {
                    $totalToRefund = trim($args[2]);
                }
                else {
                    echo " The third argument should be an valid numeric or float value.";
                    return false;
                }
            }
        }

        //
        $moPayment = AbaPayments::getPayment($userId, $successPaymentId);

        if ($moPayment->userId != $userId OR $moPayment->status != PAY_SUCCESS) {
            echo "\n+++++++ Error. Datos incorrectos. +++++++\n";
            return false;
        }

        $moExistingRefundPayment =  new Payment();
        $existsPayment =            $moExistingRefundPayment->getRefundPayment($moPayment->userId, $moPayment->idProduct, $moPayment->paySuppOrderId, PAY_REFUND);
        if($existsPayment) {
            echo "\n+++++++ Error. Refund payment " . $existsPayment->id . " already exists.  +++++++\n";
            return false;
        }

        //
        $moUser = new AbaUser();
        $moUser->getUserById($moPayment->userId);

        /* -------------------------------------------------------------------------------------*/
        // Create draft refund payment
        $moPayDraftRefund =                     clone($moPayment);
        $moPayDraftRefund->dateToPay =          HeDate::todaySQL(true);
        $moPayDraftRefund->id =                 self::generateIdPayment($moUser, $moPayDraftRefund->dateToPay);

        if($totalToRefund != null && is_numeric($totalToRefund)) {
            $moPayment->amountPrice = $totalToRefund;
        }

        if (!AbaPayments::refundUser($moPayment, $successPaymentId, $moPayDraftRefund->id)) {
            echo "\n+++++++ ERROR REFUND PAYMENT +++++++\n";
            return false;
        }

        echo "\n---" . $moPayment->userId . "---" . $moPayment->id . "---" . $moPayDraftRefund->id . "---\n";

        echo " \n  Finished process RefundPaypalPaymentCommand at ".date('Y-M-D H:m:s');
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
    }

    /**
     * @param AbaUser $user
     * @param null $dateToPay
     *
     * @return string
     */
    public static function generateIdPayment( AbaUser $user, $dateToPay=null )
    {
        $timeNow = $dateToPay;
        if( !isset($dateToPay) )
        {
            $timeNow = time();
            $timeNow = date('YmdHis', $timeNow );
        }
        else
        {
            $dateToPay =    substr($dateToPay,0,10);
            $timeNow =      new DateTime($dateToPay.date('H:i:s', time() ) );
            $timeNow =      date('YmdHis', $timeNow->format("YmdHis") );
        }

        $theString = $user->id . $user->email . $timeNow ;
        return hash( "crc32b", $theString );
    }


}
