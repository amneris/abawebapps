<?php

/**
 * Class ProgressSummaryCommand
 */

date_default_timezone_set('Europe/Madrid');

class ProgressSummaryCommand extends AbaConsoleCommand
{
    protected $aListUsersErrors = array();
    protected $aListFollowupsSuccess = array();

    public function getHelp()
    {
        return parent::getHelp() . " First argument: You can set a number to set how many records you want " .
        "to send to SELLIGENT. \n " .
        " Second argument: Optional, days in advance for warning. \n " .
        " Otherwise 200 users and 15 days in advance will be used.";
    }

    //
    // TEST:    php -f  /var/www/abawebapps/intranet/public/script.php  ProgressSummary
    // TEST:    php -f  intranet/public/script.php  ProgressSummary
    //
    public function run($args)
    {
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
        echo " \n START. ProgressSummaryCommand: executing auto process at " . date("Y-m-d h:i:s", time()) . "\n";

        $iHours = Yii::app()->config->get("FOLLOWUP_CRON_PROCESS_SUMMARY_HOURS");

        if (count($args) >= 1) {
            if (!is_numeric($args[0])) {
                echo " The first argument should be an integer telling how many records we want to process for every iteration.";
                return false;
            }
            $iHours = intval($args[0]);
        }

        //#abawebapps-437
        $sDateStart = date("Y-m-d h:i:s", time());

        $AbaFollowUp =      new AbaFollowup();
        $allProgressData =  $AbaFollowUp->getProgressSummary($sDateStart, $iHours, AbaActiveRecord::DB_SLAVE);

        $allSuccess = true;

        foreach($allProgressData as $progressData) {

            $AbaFollowUpSummary = new AbaFollowUpSummary();

            $AbaFollowUpSummary->loadFromFollowup($progressData);

            if ($AbaFollowUpSummary->validate()) {
                $AbaFollowUpSummary->saveFollowup4summary();

                $this->aListFollowupsSuccess[] = implode(", ", array(
                    "uId=>"   . $AbaFollowUpSummary->userid,
                    "pVs=>"   . $AbaFollowUpSummary->progressVersion,
                    "idFu=>"  . $AbaFollowUpSummary->idFollowup,
                ));
            }
            else {
                $this->processError("No validate followup data for table followup4" . (is_numeric(trim($AbaFollowUpSummary->progressVersion)) ? "_" . $AbaFollowUpSummary->progressVersion : '') . " ");
                $allSuccess = false;
            }
        }

        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

        if (!$allSuccess) {
            $this->sendSummaryError( "The process ProgressSummaryCommand has finished but with some errors");
            echo " \n " . date("Y-m-d h:i:s", time()) . " Some errors in process ProgressSummaryCommand. Num. rows: " . count($allProgressData) . ". END";
            echo " \n -----------------------------------------------------------------------------------------------\n";
            return true;
        }

        $this->sendSummarySuccess( "Success in process ProgressSummaryCommand" );

        echo " \n " . date("Y-m-d h:i:s", time()) . " Success in process ProgressSummaryCommand. Num. rows: " . count($allProgressData) . ". END";
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

        return $allSuccess;
    }

    /** Send general error in the process.
     *
     * @param string $actionDetails
     *
     * @return bool
     */
    private function processError($actionDetails)
    {
        HeLogger::sendLog("CRON ProgressSummaryCommand finished with errors ",HeLogger::IT_BUGS,
          HeLogger::CRITICAL,"CRON ProgressSummaryCommand finished with errors ");

        return true;
    }

    /** Sends the e-mail with errors summary.
     *
     * @param $actionDetails
     */
    private function sendSummaryError($actionDetails)
    {
        $bodyDesc = $actionDetails;
        $aSummAll = array_merge($this->aListUsersErrors, $this->aListFollowupsSuccess);
        HeLogger::sendLog(HeLogger::PREFIX_ERR_LOGIC_L . ": ProgressSummaryCommand " . HeDate::todaySQL(true) .
          " The lines of followups below were processed with errors, please review them: ", HeLogger::IT_BUGS,
          HeLogger::CRITICAL, $bodyDesc . " " .
          " CRON Reconciliation ProgressSummaryCommand executed and finished on day ".date("Y-m-d H:i:s", time()).
          " DATA: ". implode("\n", $aSummAll));
    }

    /**
     * @param $actionDetails
     */
    private function sendSummarySuccess($actionDetails)
    {
        $bodyDesc = $actionDetails;
        $aSummAll = $this->aListFollowupsSuccess;

        HeLogger::sendLog(HeLogger::PREFIX_NOTIF_L . ": PROGRESSSUMMARYPROCESS " . HeDate::todaySQL(true) .
          " The lines of users below were processed SUCCESSFULLY. You do not have to do any shit: ",HeLogger::IT_BUGS,
          HeLogger::CRITICAL,
          " CRON Reconciliation ProgressSummaryCommand executed and finished on day ".date("Y-m-d H:i:s", time()).
          " DATA: ". implode("\n", $aSummAll));
    }


}
