<?php

/**
 * Class ProgressSummaryAllCommand
 */
class ProgressSummaryAllCommand extends AbaConsoleCommand
{
    protected $aListUsersErrors = array();
    protected $aListFollowupsSuccess = array();

    public function getHelp()
    {
        return parent::getHelp() . " First argument: You can set a number to set how many records you want " .
        "to send to SELLIGENT. \n " .
        " Second argument: Optional, days in advance for warning. \n " .
        " Otherwise 200 users and 15 days in advance will be used.";
    }

    //
    // TEST:    php -f  /var/www/abawebapps/intranet/public/script.php  ProgressSummaryAll 1 0 1000
    // TEST:    php -f  /var/www/abawebapps/intranet/public/script.php  ProgressSummaryAll 2 0 1000
    //
    // TEST:    php -f  /var/www/abawebapps/intranet/public/script.php  ProgressSummaryAll 1 0 5000
    //
    public function run($args)
    {
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
        echo " \n START. ProgressSummaryCommand: executing auto process at " . date("Y-m-d h:i:s", time()) . "\n";

        $iMode =            1;
        $iInitialStart =    0;
        $iLote =            10000;

        if (count($args) >= 1) {
            if (!is_numeric($args[0])) {
                echo " The first argument should be an integer telling how many records we want to process for every iteration.";
                return false;
            }
            $iMode = intval($args[0]);
        }

        if (count($args) >= 2) {
            if (!is_numeric($args[1])) {
                echo " The second argument should be an integer telling how many records we want to process for every iteration.";
                return false;
            }
            $iInitialStart = intval($args[1]);
        }

        if (count($args) >= 3) {
            if (!is_numeric($args[2])) {
                echo " The xxx argument should be an integer telling how many records we want to process for every iteration.";
                return false;
            }
            $iLote = intval($args[2]);
        }

        $AbaFollowUp =      new AbaFollowup();
        $iTotalFollowup =   $AbaFollowUp->getTotalProgressSummary(AbaActiveRecord::DB_SLAVE);

        $iEnd =         floor($iTotalFollowup / $iLote) + 1;
        $allSuccess =   true;
        $iProcessed =   0;

        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
        echo " TOTAL ROWS: " . $iTotalFollowup . "\n";

        for($iStart = $iInitialStart; $iStart <= $iEnd; $iStart++) {

            if($iMode == 1) {

                $allProgressData = $AbaFollowUp->getLimitProgressSummary(array(($iStart * $iLote), $iLote), AbaActiveRecord::DB_SLAVE);

                if(count($allProgressData) == 0) {
                    break;
                }

                foreach($allProgressData as $progressData) {

                    $AbaFollowUpSummary = new AbaFollowUpSummary();

                    $AbaFollowUpSummary->loadFromFollowup($progressData);

                    if ($AbaFollowUpSummary->validate()) {
                        $AbaFollowUpSummary->saveFollowup4summary();

                        ++$iProcessed;
                    }
                    else {
                        $this->processError("No validate followup data for table followup4" . (is_numeric(trim($AbaFollowUpSummary->progressVersion)) ? "_" . $AbaFollowUpSummary->progressVersion : '') . " ");
                        $allSuccess = false;
                    }
                }

            }
            else if($iMode == 2) {

//                $AbaFollowUp->setLimitProgressSummary(array(($iStart * $iLote), $iLote), AbaActiveRecord::DB_CAMPUS);
                $AbaFollowUp->setLimitProgressSummary(array(($iStart * $iLote), $iLote));

            }
            echo " " . date("Y-m-d h:i:s", time()) . " => LOTE " . ($iStart) . "; LIMIT " . ($iStart * $iLote) . ", " . $iLote . "; CURRENT: " . $iStart .  "; TOTAL PROCESSED " . $iProcessed . "\n";

        }

        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

        if (!$allSuccess) {
            $this->sendSummaryError( "The process ProgressSummaryCommand has finished but with some errors");
            echo " \n " . date("Y-m-d h:i:s", time()) . " Some errors in process ProgressSummaryCommand. END";
            echo " \n -----------------------------------------------------------------------------------------------\n";
            return true;
        }

        $this->sendSummarySuccess( "Success in process ProgressSummaryCommand" );

        echo " \n " . date("Y-m-d h:i:s", time()) . " Success in process ProgressSummaryCommand. END";
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

        return $allSuccess;
    }

    /** Send general error in the process.
     *
     * @param string $actionDetails
     *
     * @return bool
     */
    private function processError($actionDetails)
    {
        HeLogger::sendLog("CRON ProgressSummaryCommand finished with errors ", HeLogger::IT_BUGS, HeLogger::CRITICAL,
          "CRON ProgressSummaryCommand finished with errors ");

        return true;
    }

    /** Sends the e-mail with errors summary.
     *
     * @param $actionDetails
     */
    private function sendSummaryError($actionDetails)
    {
        $bodyDesc = $actionDetails;
        $aSummAll = array_merge($this->aListUsersErrors, $this->aListFollowupsSuccess);
        HeLogger::sendLog(HeLogger::PREFIX_ERR_LOGIC_L . ": ProgressSummaryCommand " . HeDate::todaySQL(true) .
          " The lines of followups below were processed with errors, please review them: ",
          HeLogger::IT_BUGS, HeLogger::CRITICAL,
          $bodyDesc . " " .
          " CRON Reconciliation ProgressSummaryCommand executed and finished on day ".date("Y-m-d H:i:s", time()).
          " DATA: ". implode("\n", $aSummAll));
    }

    /**
     * @param $actionDetails
     */
    private function sendSummarySuccess($actionDetails)
    {
        $bodyDesc = $actionDetails;
        $aSummAll = $this->aListFollowupsSuccess;

        HeLogger::sendLog(HeLogger::PREFIX_NOTIF_L . ": PROGRESSSUMMARYPROCESS " . HeDate::todaySQL(true) .
          " The lines of users below were processed SUCCESSFULLY. You do not have to do any shit: ", HeLogger::IT_BUGS,
          HeLogger::CRITICAL, $bodyDesc . " " .
          " CRON Reconciliation ProgressSummaryCommand executed and finished on day ".date("Y-m-d H:i:s", time()).
          " DATA: ". implode("\n", $aSummAll) );
    }


}
