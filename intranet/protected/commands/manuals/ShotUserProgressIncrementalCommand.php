<?php

/**
 * Created by
 * User: Quino
 * Date: 25/06/2014
 * @property mixed $_moLastRowCopied
 */
class ShotUserProgressIncrementalCommand extends AbaConsoleCommand
{
    private $_daysIncrement = 7;
    /* @var AbaUsersProgressByWeek $_moLastRowCopied */
    private $_moLastRowCopied;

    /**
     * @param array $args
     *
     * @return boolean True if successful, false otherwise
     * @throws Exception
     */
    public function run($args)
    {
        echo '\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++';
        echo ' \n ShotUserProgressIncrementalCommand: Executing auto process at ' . date("Y-m-d H:i:s", time());
        try {
            $allSuccess = true;

            $aNextIteration = $this->getIteration();
            if (!$aNextIteration) {
                // End
                return false;
            } else if ($aNextIteration === true) {
                // End
                return $allSuccess;
            }

            if (!$this->wasCopiedSuccessfully($aNextIteration['dateNext'])) {
                return false;
            }

            $moProgressWeek = new AbaUsersProgressLastweek();
            if ($moProgressWeek->getLastRow()) {
                // Check table [user_progress_lastWeek] should be empty OR  be equal to config.nextDateToShotOnProgress
                if (HeDate::removeTimeFromSQL($moProgressWeek->dateCapture) !== HeDate::removeTimeFromSQL($aNextIteration['dateNext'])) {
                    // The table was not emptied, something was wrong with the second process that copies rows from
                    // one another table:
                    $this->processEndsWithError('The tabla still contains rows from another run, please review the ' .
                        'process that copies rows from one another table.');
                    return false;
                }
            }

            $dateSinceLastChange = $this->_moLastRowCopied->dateCapture;
            $moFollow = new B2cFollowup();
            // Execute Select into
            $totalToUpdate = $aNextIteration['rows'];
            /* Attention: This can return more than LIMIT rows because REPLACE executes in fact a delete and INSERT when duplicate
             keys, so it returns more affected rows. */
            $totalUpdatesSuccess = $moProgressWeek->collectProgressFromDate($moFollow, $dateSinceLastChange,
                $aNextIteration['start'], $aNextIteration['rows']);
            if (!$totalUpdatesSuccess) {
                $this->processEndsWithError('Iteration ' . $aNextIteration['dateNext'] .
                    $aNextIteration['start'] . $aNextIteration['rows'] .
                    ', it has no copied any rows at all, some problems executing SELECT INTO.');
                return false;
            } elseif ($totalUpdatesSuccess < $totalToUpdate) {
                // It has finished, because it has copied less rows than expected, but it has.
                Yii::app()->config->set('NEXT_ITERATION_SHOT_PROGRESS', $aNextIteration['dateNext'] . ',-1,-1');
            } elseif ($totalUpdatesSuccess >= $totalToUpdate) {
                // Copied everything as expected, but still more rows TO GO.
                Yii::app()->config->set('NEXT_ITERATION_SHOT_PROGRESS',
                    $aNextIteration['dateNext'] . ',' . ($aNextIteration['start'] + $aNextIteration['rows']) .
                    ',' . $aNextIteration['rows']);
            }

            return true;
            /* ---------------------------------------------------------------------------------------*/
            /* ---------------------------------------------------------------------------------------*/

        } // try
        catch (Exception $e) {
            $this->errorMessage = "Uncontrolled error on ShotUserProgressIncrementalCommand: " . $e->getMessage();
            throw new Exception($this->errorMessage);
        }
    }


    /** Determines what, if any, iteration this run should execute.
     * @return array|bool
     *
     * @throws CException
     */
    private function getIteration()
    {
        $nextIteration = Yii::app()->config->get('NEXT_ITERATION_SHOT_PROGRESS');
        $aTmpNextIteration = explode(',', $nextIteration);

        if (!is_numeric($aTmpNextIteration[1])) {
            $this->processEndsWithError("Start iteration is not a valid number.");
            throw new CException('Start iteration is not a valid number.');
        }

        if (intval($aTmpNextIteration[1]) < 0 && intval($aTmpNextIteration[1]) < 0) {
            // All iterations are completed for this dateNext
            HeLogger::sendLog(HeLogger::PREFIX_NOTIF_L . ' ShotUserProgressIncrementalCommand has finished ' .
              'iteration for date ' . $aTmpNextIteration[0],
                HeLogger::IT_BUGS, HeLogger::CRITICAL,
              'ShotUserProgressIncrementalCommand has finished ' .
              'iteration for date ' . $aTmpNextIteration[0] .
              '. Next process would be to copy data to final table users_progress_byweek.');
            return true;
        }

        if (trim($aTmpNextIteration[0]) == '') {
            // initial Load, we load all progress from all users since this data:
            $aTmpNextIteration[0] = '2012-12-03';
        }
        if (!HeDate::validDateSQL($aTmpNextIteration[0], false)) {
            $this->processEndsWithError("Date for next iteration is not a valid SQL date.");
            return false;
        }

        $aNextIteration = array('dateNext' => $aTmpNextIteration[0],
            'start' => $aTmpNextIteration[1],
            'rows' => $aTmpNextIteration[2]);
        // Date, Start, Quantity
        return $aNextIteration;
    }

    /** Gets Last Row copied into AbaUsersProgressByWeek and returns true if it s been found.
     * @param string $dateNowIteration YYYY-MM-DD
     *
     * @return bool
     */
    private function wasCopiedSuccessfully($dateNowIteration)
    {
        $this->_moLastRowCopied = new AbaUsersProgressByWeek();
        if (!$this->_moLastRowCopied->getLastRow()) {
            $this->_moLastRowCopied->dateCapture = '2012-11-26';
            return true;
        }

        if (!HeDate::validDateSQL($this->_moLastRowCopied->dateCapture, false)) {
            return false;
        }

        $dateLastCopy = $this->_moLastRowCopied->dateCapture.' 00:00:00';
        $dateNowIteration = $dateNowIteration.' 00:00:00';
        $dSinceLastIncrement = HeDate::getDifferenceDateSQL($dateLastCopy, $dateNowIteration, HeDate::DAY_SECS);
        if ($dSinceLastIncrement == 0) {
            $this->processEndsWithError('Inconsistency on iteration ' . $dateNowIteration);
            return false;
        }

        if ($dSinceLastIncrement > $this->_daysIncrement) {
            HeLogger::sendLog(HeLogger::PREFIX_ERR_LOGIC_L . ' ShotUserProgressIncrementalCommand is being executed but after ' .
              $dSinceLastIncrement, HeLogger::IT_BUGS, HeLogger::CRITICAL,
              'Please check the last exeecutions on ShotUserProgressIncremental process. ' .
              'Something is wrong with the schedule.');
        }

        return true;
    }

    /**
     * @param string $actionDetails
     *
     * @return bool
     */
    private function processEndsWithError($actionDetails)
    {
        HeLogger::sendLog(
          HeLogger::PREFIX_ERR_UNKNOWN_H .
            " Cron ShotUserProgressIncremental was executed and finished but processed some errors.",
            HeLogger::IT_BUGS,
            HeLogger::CRITICAL,
            $actionDetails
        );

        echo " \n ".date("Y-m-d H:i:s", time())." ShotUserProgressIncremental finished with ERRORS : " . $actionDetails;
        echo " \n -----------------------------------------------------------------------------------------------";
        return true;
    }

    /**
     * @param $email
     * @param $actionDetails
     *
     * @return bool
     */
    private function processRowWithError($email, $actionDetails)
    {
        HeLogger::sendLog(
            HeLogger::PREFIX_ERR_UNKNOWN_L .
            " Cron ShotUserProgressIncremental execution on user " . $email . " has raised an error",
            HeLogger::IT_BUGS,
            HeLogger::CRITICAL,
            $actionDetails
        );

        echo " \n " . $actionDetails;
        return true;
    }

}
