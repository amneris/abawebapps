<?php

/**
 * Class CreateUserProgressCommand
 *
 * USER PROGRESS SCRIPT
 *
 */

class CreateUserProgressCommand extends AbaConsoleCommand
{
    protected $aListUsersErrors = array();
    protected $aListFollowupsSuccess = array();

    public function getHelp()
    {
        return parent::getHelp() .
        " First argument: USERID " .
        " Second argument: UNIT(S) ";
    }

    public function run($args)
    {
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
        echo " \n START. CreateUserProgressCommand: executing auto process at " . date("Y-m-d h:i:s", time()) . "\n";

        $iQueueId = null;
        $bSuccess = true;

        if (count($args) >= 1) {
            if (!is_numeric($args[0])) {
                echo " The first argument should be an integer telling how many records we want to process for every iteration.";
                return false;
            }
            $iQueueId = intval($args[0]);
        }


        $oAbaUserProgressQueues = new AbaUserProgressQueues();

        if($oAbaUserProgressQueues->getUserProgressQueueById($iQueueId)) {

            if($oAbaUserProgressQueues->status == AbaUserProgressQueues::QUEUE_STATUS_PENDING) {

                $oAbaUserProgressQueues->startProcessQueue();

                $bSuccess = $oAbaUserProgressQueues->fillTheProgress();

                //
                $oAbaUserProgressQueues->endProcessQueue($bSuccess);
            }
            else {
                $bSuccess = fasle;
            }
        }

        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

        if (!$bSuccess) {
            $this->sendSummaryError( "The process CreateUserProgressCommand has finished but with some errors");
            echo " \n " . date("Y-m-d h:i:s", time()) . " Some errors in process ProgressSummaryCommand. END";
            echo " \n -----------------------------------------------------------------------------------------------\n";
            return true;
        }

        $this->sendSummarySuccess( "Success in process CreateUserProgressCommand" );

        echo " \n " . date("Y-m-d h:i:s", time()) . " Success in process CreateUserProgressCommand. END";
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

        return $bSuccess;
    }

    /** Send general error in the process.
     *
     * @param string $actionDetails
     *
     * @return bool
     */
    private function processError($actionDetails)
    {
        HeAbaMail::sendEmailInternal(Yii::app()->config->get("EMAIL_IT_BUGS"),
            "",
            "CRON CreateUserProgressCommand finished with errors ",
            "CRON CreateUserProgressCommand finished with errors ",
            true,
            $this,
            HeAbaMail::BUGS_LAYOUT);

        return true;
    }

    /**
     * @param $actionDetails
     */
    private function sendSummarySuccess($actionDetails)
    {
        $bodyDesc = $actionDetails;
        $aSummAll = $this->aListFollowupsSuccess;

        HeAbaMail::sendEmailInternal(Yii::app()->config->get("EMAIL_IT_BUGS"),
            "",
            HeAbaMail::PREFIX_NOTIF_L . ": USERPROGRESSPROCESS " . HeDate::todaySQL(true) .
            " The lines of users below were processed SUCCESSFULLY. You do not have to do any shit: ",
            $bodyDesc . " " .
            " CRON CreateUserProgressCommand executed and finished on day ".date("Y-m-d H:i:s", time()).
            " DATA: ". implode("\n", $aSummAll),
            true, NULL, HeAbaMail::ABA_LAYOUT,
            "Campus Aba English", "Campus");
    }

}
