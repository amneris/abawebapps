<?php

class FeedPressCommand extends AbaConsoleCommand {

  public function actionSynchronize() {
    $date = date("Y-m-d H:i:s");
    echo "{$date}: Start action Synchronize\n";

    $key = Yii::app()->params['feedpress_key'];
    $token = Yii::app()->params['feedpress_token'];

//      https://api.feed.press/feeds/newsletter/subscribers.json?key=5539f99f63836&token=553c2291adef5&feed=ABAEnglish
//      https://api.feed.press/feeds/newsletter/subscribers.json?key=5539f99f63836&token=553c2291adef5&feed=BlogES
//      https://api.feed.press/feeds/newsletter/subscribers.json?key=5539f99f63836&token=553c2291adef5&feed=BlogIT
//      https://api.feed.press/feeds/newsletter/subscribers.json?key=5539f99f63836&token=553c2291adef5&feed=BlogFR
//      https://api.feed.press/feeds/newsletter/subscribers.json?key=5539f99f63836&token=553c2291adef5&feed=BlogPT

    $transaction = Yii::app()->dbExtranet->beginTransaction();
    try {

      $mlBlogs = Blog::model()->findAllByAttributes(array('isdeleted' => 0));
      foreach( $mlBlogs as $mBlog ) {

          // ** Idioma del blog.
        $mLanguage = Language::model()->findByPk($mBlog->idLanguage);
        if ( !$mLanguage ) { $mLanguage = new Language(); $mLanguage->iso = 'en'; }

        // ** Recuperar los alumnos dados de alta en los blogs.
        $feed = $mBlog->name;
        $url = "https://api.feed.press/feeds/newsletter/subscribers.json?key={$key}&token={$token}&feed={$feed}";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        ob_start();
        $response = curl_exec($ch);
        $json = ob_get_contents();
        ob_end_clean();
        curl_close($ch);

        $arJson = json_decode($json);
        foreach ($arJson->subscribers as $item) {
          $email = $item->email;
          $confirmed = $item->confirmed;

          $mSubscriber = BlogSubscriber::model()->findByAttributes(array('email' => $email, 'idBlog' => $mBlog->id ));
          if (!$mSubscriber) {
            $mSubscriber = new BlogSubscriber();
            $mSubscriber->email = $email;
            $mSubscriber->idBlog = $mBlog->id;
          } else if ($mSubscriber->isdeleted) {
            $mSubscriber->created = $date;
            $mSubscriber->deleted = $date;
          }
            // ** Preparo para ver si se ha actualizado.
          $mSubscriber->freezeInitialValues();

          $mSubscriber->confirmed = ($confirmed) ? 1 : 0;

            // ** Lo caso con el campus.
          $mStudent = UserCampus::model()->findByAttributes(array('email'=>$email) );
          if ( $mStudent ) {
            $mSubscriber->idStudentOnCampus = $mStudent->id;
            $mSubscriber->studentType = $mStudent->userType;  // ** Puede haber cambiado su estado de una consulta anterior.
            $mSubscriber->name = $mStudent->name;
          } else {
            $mSubscriber->idStudentOnCampus = 0;
            $mSubscriber->studentType = BlogSubscriber::_NOUSER;
            $mSubscriber->name = '';
          }

            // ** Compruebo si ha habido alguna actualizacion.
          $bEqual = $mSubscriber->itWasModified();

          $mSubscriber->updated = $date;
          $mSubscriber->save();

          if ( !$bEqual ) {
            HeAbaMail::sendEmail(array(
              'action' => HeAbaSelligent::_EDITBLOGUSER,
              'email' => $mSubscriber->email,
              'name' => $mSubscriber->name,
              'isoLanguage' => $mLanguage->iso,
              'userType' => ($mSubscriber->studentType) ? $mSubscriber->studentType : BlogSubscriber::_FREE,
              'registered' => ($mSubscriber->studentType == BlogSubscriber::_NOUSER) ? 0 : 1,

              'idUser' => ($mStudent) ? $mStudent->id: 0,
              'idUserBlog' => $mSubscriber->id,

              'callback' => 'ExtranetController::actionMailSent',
            ));
          }

        }

//        https://www.abaenglish.com/blog/feed
//        https://www.abaenglish.com/blog/es/feed
//        https://www.abaenglish.com/blog/it/feed
//        https://www.abaenglish.com/blog/pt/feed
//        https://www.abaenglish.com/blog/fr/feed

        $feed_url = $mBlog->rssUrl;
        $content = file_get_contents($feed_url);
        $xml = new SimpleXmlElement($content, LIBXML_NOCDATA);
        foreach($xml->channel->item as $entry) {

          $arTmp = array();
          $string =  parse_url($entry->guid, PHP_URL_QUERY);
          parse_str($string, $arTmp);
          if ( isset($arTmp['p']) ) $idPost = $arTmp['p']; else $idPost = 0;

          $mPost = null;
          if ( $idPost ) $mPost = BlogPost::model()->findByAttributes(array('idPost' => $idPost));
          if ( $mPost ) continue;       // ** Si ya existia, no he de hacer nada.

          $mPost = new BlogPost();
          $mPost->idBlog = $mBlog->id;
          $mPost->idPost = $idPost;

          $mPost->title = HeString::toCharset($entry->title, true);
          $mPost->link = HeString::toCharset($entry->link, true);
          $mPost->linkComments = HeString::toCharset($entry->comments, true);
          $mPost->pubDate = date("Y-m-d H:i:s", strtotime($entry->pubDate));

          // ** Descripcion
          $description = HeString::toCharset($entry->description, true);
          $pos = strpos($description, '/>');
          if ( $pos ) $description = substr($description, $pos+2);
          $mPost->description = $description;

          // ** Image
          $imgUrl = '';
          $doc = new DOMDocument();
          $doc->loadHTML($entry->description);
          $imgs = $doc->getElementsByTagName('img');
          foreach ($imgs as $img) {
            $imgUrl = HeString::toCharset($img->getAttribute('src'), true);
            break;
          }
          $mPost->urlImage = $imgUrl;

          // ** Creator
          $dc = $entry->children('http://purl.org/dc/elements/1.1/');
          $nameCreator = HeString::toCharset($dc->creator, true);
          $mCreator = BlogCreator::model()->findByAttributes(array('name' => $nameCreator));
          if ( !$mCreator ) {
            $mCreator = new BlogCreator();
            $mCreator->name = $nameCreator;
            $mCreator->save();
          }
          $mPost->idCreator = $mCreator->id;

          $ok = $mPost->save();
          if (!$ok ) { /* ** Error */ }

        }

      }

      $transaction->commit();

    } catch ( Exception $e ) {
      echo "Error: ".$e->getMessage()."\n";
      $transaction->rollback();
    }

    echo date("Y-m-d H:i:s").": End.\n";
  }

  public function actionSendNewsletter() {
    $date = date("Y-m-d H:i:s");
    echo "{$date}: Start action SendNewsletter\n";

    $criteria = new CDbCriteria;
    $criteria->addCondition('status = :status');
    $criteria->addCondition('DATE(dateToSend) = CURDATE()');
    $criteria->params = array(':status' => BlogNewsletterPublication::_CLOSED );
    $mlPublication = BlogNewsletterPublication::model()->findAll($criteria);

    foreach ( $mlPublication as $mPublication ) {
      $idBlog = $mPublication->idBlog;

      $mBlog = Blog::model()->findByPk($idBlog);
      $mLanguage = Language::model()->findByPk($mBlog->idLanguage);

      $arParms = array(
        'action' => HeAbaSelligent::_EDITBLOG,

        'subject' => $mPublication->textSubject,
        'intro' => $mPublication->textHeader,
        'tweet' => $mPublication->textTweet,
        'linkTweet' => $mPublication->linkTweet,
        'enviar' => 1,
        'isoLanguage' => $mLanguage->iso,
        'callback' => 'ExtranetController::actionMailSent',
      );

      for( $i=1; $i < 6; $i++ ) {
        $labelPost = 'idPost'.$i;
        $labelDescPost = 'descriptionPost'.$i;
        $labelLinkPost = 'linkPost'.$i;

        if ( !empty($mPublication->$labelPost) ) {
          $mPost = BlogPost::model()->findByPk($mPublication->$labelPost);
        } else $mPost = new BlogPost();

        $arParms['urlImg'.$i] = $mPost->urlImage;
        $arParms['titPost'.$i] = $mPost->title;
        $arParms['descPost'.$i] = $mPublication->$labelDescPost;
        $arParms['textPost'.$i] = $mPublication->$labelLinkPost;
        $arParms['linkPost'.$i] = $mPost->link;
      }

      HeAbaMail::sendEmail($arParms);
    }

    echo date("Y-m-d H:i:s").": End.\n";
  }

}