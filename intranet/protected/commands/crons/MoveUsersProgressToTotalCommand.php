<?php

/**
 * Created by
 * User: Quino
 * Date: 25/06/2014
 * @property mixed $_moLastRowCopied
 */
class MoveUsersProgressToTotalCommand extends AbaConsoleCommand
{
    private $daysIncrement = 7;
    private $batchRows = 20000;
    /* @var AbaUsersProgressByWeek $_moLastRowCopied */
    private $moLastRowCopied;

    /**
     * @param array $args
     *
     * @return boolean True if successful, false otherwise
     * @throws Exception
     */
    public function run($args)
    {
        echo '\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++';
        echo ' \n MoveUsersProgressToTotalCommand: Executing auto process at ' . date("Y-m-d H:i:s", time());
        try {
            // Set Limit of rows to deal with:
            if (isset($args[0])) {
                $this->batchRows = intval($args[0]);
            }

            $totalToUpdate = $this->batchRows;

            $dateExpectedNext = HeDate::getDateAdd(HeDate::todaySQL(), $this->daysIncrement, 0);
            $nextIteration = Yii::app()->config->get('NEXT_ITERATION_SHOT_PROGRESS');
            list($dateNext, $start, $rows) = explode(',', $nextIteration);
            if (HeDate::isGreaterThanToday($dateExpectedNext) && intval($start) == 0) {
                // It means that already all data has been moved to another table and already next iteration has
                // been set.
                return true;
            } else if (intval($start) >= 0 || intval($rows) >= 0) {
                // Still is not finished or it was interrupted, we mean the process ShotUserProgressIncremental
                $this->processEndsWithError('The config key NEXT_ITERATION_SHOT_PROGRESS indicates that the process ' .
                    ' ShotUserProgressIncremental has not finished yet. This process MoveUsersProgressToTotalCommand' .
                    ' expected to been finished by now.');
                return false;
            }

            // Check last row inserted in table lastRows, check if there is any available and in that case
            // check the date to see if it has passed more than 1 week($_daysIncrement)
            $moLastRow = new AbaUsersProgressLastweek();
            if (!$moLastRow->getLastRow()) {
                $this->processEndsWithError('There are no rows to import in table ' . $moLastRow->tableName());
                return false;
            } elseif (HeDate::getDifferenceDateSQL(HeDate::now(), $moLastRow->dateCapture.' 00:00:00', HeDate::DAY_SECS) >
                $this->daysIncrement
            ) {
                HeLogger::sendLog(
                  HeLogger::PREFIX_NOTIF_L . ', warning on MoveUsersProgressToTotal more than 7 days rows',
                    HeLogger::IT_BUGS, HeLogger::CRITICAL,
                    'MoveUsersProgressToTotal has found rows in ' . $moLastRow->tableName() .
                    ' that belong to an iteration older than ' . $this->daysIncrement . ' days,  expected less.' .
                    ' Process continue to execute but please review the iterations.');
            }

            // Copy rows from users_progress_lastWeek:
            $moUsersProgress = new AbaUsersProgressByWeek();
            $totalCopiedSuccess = $moUsersProgress->pickLastRows($this->batchRows);
            if (!$totalCopiedSuccess || $totalCopiedSuccess == 0) {
                $this->processEndsWithError('No rows were copied to ' . $moUsersProgress->tableName() .
                    ' from temporary table ' . $moLastRow->tableName());
                return false;
            }


            // Delete rows just copied if confirmed:
            if ($totalCopiedSuccess > 0) {
                $deletedRows = $moLastRow->deleteLastRows($totalCopiedSuccess);
                if (!$deletedRows) {
                    $this->processEndsWithError('Failed when deleting rows from original(temporary) table.' .
                        'No rows were deleted. Please review the content of both tables.');
                    return false;
                } elseif ($deletedRows !== $totalCopiedSuccess) {
                    $this->processEndsWithError(" $deletedRows deleted rows OUT OF $totalCopiedSuccess " .
                        " from original(temporary) table. Al lof them should have been removed from the table.");
                    return false;
                }
            }


            // If copied and deleted are less than expected by  _batchRows then we consider the process is finished:
            if ($totalToUpdate > $totalCopiedSuccess) {
                $moLastRow = new AbaUsersProgressLastweek();
                if (!$moLastRow->getLastRow()) {
                    // TRUNCATE,
                    $moLastRow->fullTruncate();
                    // UPDATE config Key NEXT_ITERATION_SHOT_PROGRESS for the next iteration($_daysIncrement days).
                    $dateNext = HeDate::getDateAdd($dateNext, $this->daysIncrement, 0);
                    Yii::app()->config->set('NEXT_ITERATION_SHOT_PROGRESS', $dateNext . ',0,5000');
                } else {
                    $this->processEndsWithError('Still some rows appear in ' . $moLastRow->tableName() .
                        ', some issue is going on, it should be empty, because last iteration from this ' .
                        'process copied less than expected.');
                    return false;
                }

            }

            $bodyDesc = json_encode(array(
                "body" => 'MoveUsersProgressToTotal has found rows in progress during last week and now ' .
                  ' you can download all from intranet.abaenglish.com.',
                "listTable" => null
              ));

            HeLogger::sendLog(
              HeLogger::PREFIX_NOTIF_L . ', MoveUsersProgressToTotal has finished with success. Please download file.',
              HeLogger::SELLIGENT, HeLogger::CRITICAL,
              $bodyDesc);
            return true;
            /* ---------------------------------------------------------------------------------------*/
            /* ---------------------------------------------------------------------------------------*/

        } // try
        catch (Exception $e) {
            $this->errorMessage = "Uncontrolled error on MoveUsersProgressToTotalCommand: " . $e->getMessage();
            throw new Exception($this->errorMessage);
        }
    }

    /**
     * @param string $actionDetails
     *
     * @return bool
     */
    private function processEndsWithError($actionDetails)
    {
        HeLogger::sendLog(
          HeLogger::PREFIX_ERR_UNKNOWN_H .
            " Cron MoveUsersProgressToTotal was executed and finished but processed some errors.",
            HeLogger::IT_BUGS,
            HeLogger::CRITICAL,
            $actionDetails
        );
        echo " \n ".date("Y-m-d H:i:s", time()) . " MoveUsersProgressToTotal finished with ERRORS : " . $actionDetails;
        echo " \n -----------------------------------------------------------------------------------------------";
        return true;
    }

    /**
     * @param $email
     * @param $actionDetails
     *
     * @return bool
     */
    private function processRowWithError($email, $actionDetails)
    {
        HeLogger::sendLog(
            HeLogger::PREFIX_ERR_UNKNOWN_L .
            " Cron MoveUsersProgressToTotal execution on user " . $email . " has raised an error",
            HeLogger::IT_BUGS,
            HeLogger::CRITICAL,
            $actionDetails
        );
        echo " \n " . $actionDetails;
        return true;
    }
}
