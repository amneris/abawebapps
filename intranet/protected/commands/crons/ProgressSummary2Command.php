<?php

class ProgressSummary2Command extends AbaConsoleCommand
{

    public function actionSynchronize($lastUpdate = '')
    {
        $date = date("Y-m-d H:i:s");
        echo "{$date}: Start action Synchronize\n";

        $now = date("Y-m-d H:i:s");
        if (empty($lastUpdate)) {
            $lastUpdate = Yii::app()->config->get("SUMMARY_LAST_UPDATE");
        }
        if (strtotime($lastUpdate) <= 0) {
            throw new Exception('Error: Fecha incorrecta.');
        }
        $lastUpdate = date("Y-m-d H:i:s", strtotime($lastUpdate));

        //#ZOR-524
        $CmAbaFollowUpVersion = new CmAbaFollowUpVersion();
        $idVersion = $CmAbaFollowUpVersion->getMaxVersion();

        $connection = Yii::app()->db;
        $transaction = $connection->beginTransaction();
        try {

            $index = 0;

            //#ZOR-524
            while ($index <= $idVersion) {
                $sql = "insert into aba_b2c_summary.followup4_summary (progressVersion,idFollowup,themeid,userid,lastchange,all_por,all_err,all_ans,all_time,sit_por,stu_por,dic_por,dic_ans,dic_err,rol_por,gra_por,gra_ans,gra_err,wri_por,wri_ans,wri_err,new_por,spe_por,spe_ans,time_aux,gra_vid,rol_on,exercises,eva_por)
                select {$index}, f.* 
                from aba_b2c.followup4_{$index} f, aba_b2c.user u
                where f.lastchange >= '{$lastUpdate}'
                  and f.userid = u.id
                ON DUPLICATE KEY UPDATE
                  lastchange = f.lastchange, 
                  all_por = f.all_por, 
                  all_err = f.all_err, 
                  all_ans = f.all_ans, 
                  all_time = f.all_time, 
                  sit_por = f.sit_por, 
                  stu_por = f.stu_por,
                  dic_por = f.dic_por,
                  dic_ans = f.dic_ans,
                  dic_err = f.dic_err,
                  rol_por = f.rol_por,
                  gra_por = f.gra_por,
                  gra_ans = f.gra_ans,
                  gra_err = f.gra_err,
                  wri_por = f.wri_por,
                  wri_ans = f.wri_ans,
                  wri_err = f.wri_err,
                  new_por = f.new_por,
                  spe_por = f.spe_por,
                  spe_ans = f.spe_ans,
                  time_aux = f.time_aux,
                  gra_vid = f.gra_vid,
                  rol_on = f.rol_on,
                  exercises = f.exercises,
                  eva_por = f.eva_por ";
                if (empty($index)) {
                    $sql = str_replace('aba_b2c.followup4_0', 'aba_b2c.followup4', $sql);
                }
                $command = $connection->createCommand($sql);
                $command->query();

                $index++;
            }

            $transaction->commit();
            Yii::app()->config->set("SUMMARY_LAST_UPDATE", $now);
        } catch (Exception $e) {
            throw  new Exception("Error: " . $e->getMessage());
            $transaction->rollback();
        }

        echo date("Y-m-d H:i:s") . ": End.\n";
    }

}