<?php
/**
 * Export user progress to send to selliegent.
 * Created by JetBrains PhpStorm.
 * User: Luis
 * Date: 13/02/2014
 * Time: 12:47
 */

class UserProgressExportCommand extends AbaConsoleCommand
{
    public function getHelp()
    {
        return parent::getHelp()." \n ";
    }

    public function run($args)
    {
        $model = new B2cFollowup();
        $dataReader = $model->searchExportCron();
        $data = array();
        $xls = new JPhpCsv('UTF-8', false, 'Users');
        $flagFirst=true;

        while(($row = $dataReader->read())!==false)
        {
            if($flagFirst)
            {
                $flagFirst= false;
                $theRow=array();
                foreach ($row as $key =>$value)
                {
                    if($key!=='tmpCurrentWatchedVideoClass')
                    {
                        array_push($theRow, $key);
                    }
                }
                array_push($data, $theRow);
            }
            $theRow=array();
            foreach ($row as $key =>$value)
            {
                if($key!=='tmpCurrentWatchedVideoClass')
                {
                    array_push($theRow, $value);
                }
            }
            array_push($data, $theRow);
        }
        if($flagFirst==true)
        {
            $theRow=array();
            array_push($theRow, "DATA NOT FOUND (Data not exported)");
            array_push($data, $theRow);
        }
        $xls->addArray($data);
        $xls->generateFileTXT("data_uso");
        return true;

    }
}