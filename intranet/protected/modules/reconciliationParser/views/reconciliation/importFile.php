<?php
/* @var $this ReconciliationController */
/* @var $frmImport ImportReconciliationForm */
/* @var $fileLink string */
/* @var $isSuccess bool */


/*$this->breadcrumbs=array(
    'Reconciliation parser'=>array('index'),
    'Load reconciliation file from payment gateway supplier',
);*/
?>


<div class="form" id="divImportReconciliation">
    <h3>    <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/reconciliation.png')."&nbsp;"; ?>
            RECONCILIATION: Load CSV from the gateway payment supplier
    </h3>

<p>This page should be used to upload the file our payment gateway partners delivers to us.
    The objective is to match transactions on their side and ours.
    The process of reconciliation will be as follows:</p>
    <ol>
        <li>The uploaded file should have the columns and values format as expected.
            When a line is inconsistent, this will be ignored.</li>
        <li>Then the Intranet will search in our payments table by our identifier based on the CSV file: -paySuppOrderId-.</li>
        <li>If found, then the process of reconciliation will check the fields: status, amount price,
            date of transaction, currency and paySuppExtUnid</li>
        <li>If everything matches, MATCHED will be stamped in the CSV returned file.</li>
        <li>If anything will not match then INCONSISTENT will be stamped.</li>
        <li>If not found, then NOT FOUND will be stamped.</li>
    </ol>


<?php
$urlAction = $this->createURL('/reconciliationParser/reconciliation/importfile');
$frmReconciliationImport = $this->beginWidget('CActiveForm', array(
    'id' => 'frmReconciliationImport',
    'action' => $urlAction,
    'htmlOptions'=>array('enctype' => 'multipart/form-data', 'name'=>'frmReconciliationImport'),
    'enableClientValidation' => false,
    'clientOptions' => array(
        'validateOnSubmit' => false,
    ),
));
?>

<p class="note">Fields with <span class="required">*</span> are required.</p>

<?php
echo $frmReconciliationImport->errorSummary($frmImport);
 if(Yii::app()->user->hasFlash('errorCheckBox')){ ?>

    <div class="flash-error">
        <?php echo Yii::app()->user->getFlash('errorCheckBox'); ?>
    </div>

<?php } ?>

<table id="myFormReconciliation">
    <tr>
        <td><?php echo $frmReconciliationImport->labelEx($frmImport,'paySupplierExtId'); ?></td>
        <td>
            <?php
            echo $frmReconciliationImport->DropDownList($frmImport,'paySupplierExtId', HeList::supplierExtGatewayList());
            echo $frmReconciliationImport->error($frmImport,'paySupplierExtId');
            ?>
        </td>
    </tr>

    <tr>
        <td><?php echo $frmReconciliationImport->labelEx($frmImport,'dateOperation');?></td>
        <td>
            <?php
            $frmReconciliationImport->widget('zii.widgets.jui.CJuiDatePicker',array(
                'model'=>$frmImport,
                'attribute'=>'dateOperation',
                // Additional javascript options for the date picker plugin
                'options'=>array(
                    'showAnim'=>'fold',
                    'dateFormat'=>'dd-mm-yy',
                ),
                'htmlOptions'=>array('style'=>'width:70px;', 'id'=>'dateOperation', 'value'=>$frmImport->dateOperation),
            ));
            ?>
        </td>
    </tr>

    <tr>
        <td><?php echo $frmReconciliationImport->labelEx($frmImport,'fileName');?></td>
        <td>
            <?php echo $frmReconciliationImport->fileField($frmImport,'fileName'); ?>
            <?php echo $frmReconciliationImport->error($frmImport,'fileName'); ?>

        </td>
    </tr>
    <?php if(trim($fileLink)!==''){ ?>
        <tr>
            <td>Click here to download the processed file:</td>
            <td>
                <?php $urlDownload = $this->createURL('/reconciliationParser/reconciliation/downloadfile/fileName/'.$fileLink); ?>
                <a name="downloadReconcFile" id="downloadReconcFile" href="<?php echo $urlDownload; ?>">
                    <img src="/images/csvFile.jpg" >
                </a>
                <?php if($isSuccess){ ?>
                    <div class="greenOkMessage">
                    Process has been successful, everything was matched.
                    </div>
                <?php }
                else{ ?>
                    <div class="errorMessage">
                    Process has finished with some inconsistencies.<br/>
                    Review returned file and once you would have found the problems update referenced payments in the Intranet.<br/>
                    Remember you have <?php echo Yii::app()->config->get("PAY_ABA_RECONCILIATION_DAYS"); ?> days to manually reconciliate this inconsistent payments.
                    </div>
                <?php } ?>
            </td>
        </tr>
    <?php } ?>
</table>

<div class="row buttons" align="center">
    <?php echo CHtml::submitButton('Send and process file', array("id"=>"btnSubmit")); ?>
    <div id="IdWaitingSendFile" class="loadingImportFile" style="display: none;">
        <span class="loadingImportFileInterior ">
            <?php
            $imagePath = "/images/loading.gif";
            echo CHtml::image($imagePath, "Loading",  array ( "id"=>"loadingGif") );
            ?>&nbsp;
        </span>
    </div>
</div>

<?php $this->endWidget(); ?>

</div>
