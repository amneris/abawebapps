<?php
/* @var $this LogReconciliationController */
/* @var $moLogReconc LogReconciliation */

$this->breadcrumbs=array(
	'Log Reconciliations'=>array('admin'),
);

$this->menu=array(
	array('label'=>'List LogReconciliation', 'url'=>array('index')),
	array('label'=>'Create LogReconciliation', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('log-reconciliation-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3><?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/reconciliation_log.png')."&nbsp;"; ?>
    Log of all files from gateway platforms
</h3>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search and export to Excel','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'moLogReconc'=>$moLogReconc,
)); ?>
</div><!-- search-form -->

<?php $this->widget('application.components.widgets.CGridViewABA', array(
	'id'=>'log-reconciliation-grid',
	'dataProvider'=>$moLogReconc->search(),
	'filter'=>$moLogReconc,
	'columns'=>array(
        'paySuppExtId'=> array( 'name'=>'paySuppExtId',
                                    'type'=>'supplierExtGatewayList',
                                    'filter'=>HeList::supplierExtGatewayList(),),
		'filePath',
		'lineNode',
		'dateOperation',
		'idPayment',
		'refPaySuppOrderId',
		'refPaySuppStatus'=> array( 'name'=>'refPaySuppStatus',
                                    'type'=>'statusDescriptionFull',
                                    'filter'=>HeList::statusDescriptionFull(),),
		'refAmountPrice'=> array( 'name'=>'refAmountPrice',
                                    'type'=>'RAW',
                                    'filter'=>CDataColumnABA::NO_FILTER),
		'refDateEndTransaction',
		'refCurrencyTrans',
		'refPaySuppExtUniId',
		'success'=> array( 'name'=>'success',
                        'type'=>'YesNoBoolList',
                        'filter'=>HeList::getYesNoAliasBool() ),
		'matchType'
	),
)); ?>
