<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 06/10/14
 * Time: 15:39
 */


/**

 */
Yii::import('zii.widgets.grid.CGridView');

//Yii::import('zii.widgets.CBaseListView');
//Yii::import('zii.widgets.grid.CDataColumn');
Yii::import('application.components.widgets.CDataColumnABA');
//Yii::import('zii.widgets.grid.CLinkColumn');
Yii::import('application.components.widgets.CButtonColumnABA');
//Yii::import('zii.widgets.grid.CCheckBoxColumn');

class CGridViewABA extends CGridView
{

    private $_formatter;

    public $extraButtons= array();
    public $extraInfo= array();
    /**
     * @var string a PHP representation to identify the rows of a view
     *
     */
    public $rowIdExpression;

    /**
     * Creates column objects and initializes them.
     */
    protected function initColumns()
    {
        if($this->columns===array())
        {
            if($this->dataProvider instanceof CActiveDataProvider)
                $this->columns=$this->dataProvider->model->attributeNames();
            else if($this->dataProvider instanceof IDataProvider)
            {
                // use the keys of the first row of data as the default columns
                $data=$this->dataProvider->getData();
                if(isset($data[0]) && is_array($data[0]))
                    $this->columns=array_keys($data[0]);
            }
        }
        $id=$this->getId();
        foreach($this->columns as $i=>$column)
        {
            if(is_string($column))
                $column=$this->createDataColumn($column);
            else
            {
                if(!isset($column['class']))
                    $column['class']='CDataColumnABA';
                $column=Yii::createComponent($column, $this);
            }
            if(!$column->visible)
            {
                unset($this->columns[$i]);
                continue;
            }
            if($column->id===null)
                $column->id=$id.'_c'.$i;
            $this->columns[$i]=$column;
        }

        foreach($this->columns as $column)
            $column->init($this->extraButtons,/*, $this->extraButton1, $this->extraButton2, $this->extraButton3,*/ $this->extraInfo);
    }


    /**
     * Creates a {@link CDataColumn} based on a shortcut column specification string.
     * @param string $text
     * @return CDataColumn|CDataColumnABA
     * @throws CException
     */
    protected function createDataColumn($text)
    {
        if(!preg_match('/^([\w\.]+)(:(\w*))?(:(.*))?$/',$text,$matches))
            throw new CException(Yii::t('zii','The column must be specified in the format of "Name:Type:Label", where "Type" and "Label" are optional.'));
        $column=new CDataColumnABA($this);
        $column->name=$matches[1];
        if(isset($matches[3]) && $matches[3]!=='')
            $column->type=$matches[3];
        if(isset($matches[5]))
            $column->header=$matches[5];
        return $column;
    }

    /**
     * Renders a table body row.
     * @param integer $row the row number (zero-based).
     */
    public function renderTableRow($row)
    {
        $idRow = null;
        if($this->rowIdExpression!==null){
            $data=$this->dataProvider->data[$row];
            $idRow=$this->evaluateExpression($this->rowIdExpression,array('row'=>$row,'data'=>$data));
        }

        if($this->rowCssClassExpression!==null)
        {
            $data=$this->dataProvider->data[$row];
            $class=$this->evaluateExpression($this->rowCssClassExpression,array('row'=>$row,'data'=>$data));
        }
        else if(is_array($this->rowCssClass) && ($n=count($this->rowCssClass))>0)
            $class=$this->rowCssClass[$row%$n];
        else
            $class='';

        $txtIdRow = '';
        if ( !empty($idRow) ){
            $txtIdRow = 'id="'.$idRow.'"';
        }
        echo empty($class) ? '<tr>' : '<tr '.$txtIdRow.' class="'.$class.'">';
        foreach($this->columns as $column)
            $column->renderDataCell($row);
        echo "</tr>\n";
    }

    /**
     * @return CFormatter the formatter instance. Defaults to the 'format' application component.
     */
    public function getFormatter()
    {
        if ($this->_formatter === null) {
            $this->_formatter = Yii::app()->format;
            if ($this->_formatter instanceof AbaFormatter) {
                $this->_formatter->setRenders(HeList::getRenderNames());
            }
        }
        return $this->_formatter;
    }

} 