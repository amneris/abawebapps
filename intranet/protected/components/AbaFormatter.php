<?php
class AbaFormatter extends CFormatter
{
    /**
     * @var array the format used to format a number with PHP number_format() function.
     * Three elements may be specified: "decimals", "decimalSeparator" and 
     * "thousandSeparator". They correspond to the number of digits after 
     * the decimal point, the character displayed as the decimal point,
     * and the thousands separator character.
     * new: override default value: 2 decimals, a comma (,) before the decimals 
     * and no separator between groups of thousands
    */
    public $numberFormat=array('decimals'=>2, 'decimalSeparator'=>',', 'thousandSeparator'=>'');
	public $renders=array();
	public function setRenders ($inRenders)
	{
		$this->renders=$inRenders; //set the predefined translation formats to render
	}
	public function getDBValue($table,$keyfield, $field, $value)
	{
		$connection = Yii::app()->db;
		$sql= "SELECT $field, count(*) as num FROM $table WHERE $keyfield='$value'";
		$command = $connection->createCommand($sql);		
        $dataReader=$command->query();
		if(($row = $dataReader->read())!==false)
			return $row["$field"];
	}
	public function format($value,$type,$data=array()) 
	{
        if($type==='RAW')
        {
            return $value;
        }

		if($type=='AES')
		{
            return AbaUserCreditForms::AES_decode($value);
		}

		$prefix ="";
		$postfix ="";
		if(substr($type,0,1)=="*")
		{
			$theVar=substr($type,1);
			$theVar=HeString::replaceText($theVar,"#value#",$value);
			return "<a href='$theVar' target='_blank'>$value</a>";
		}
		if(substr($type,0,1)=="?")
		{
			/* sample use 
			  'email'=>array(  
			  'header'=>'Teacher Id',
              'name'=>'email',
              'type'=>'?index.php?r=abaMessages/detail&id=*teacherid*&begin='.$model->startDate.'&end='.$model->endDate ,
			),*/
			$theVar=substr($type,1);
			$theVar=HeString::replaceText($theVar,"#value#",$value);
			foreach($data as $key=>$val)
			{
				$theVar=HeString::replaceText($theVar,"*".$key."*",$val);
			}
			return "<a href='$theVar'>$value</a>";
		}
        if(substr($type,0,1)=='[')
        {
            /*sample use
                'cardNumber'=>array(
                'name'=>'cardNumber',
                'type'=>'{HeSql::tooltipAES("*id*","cardNumber");',
          ),*/
            $theVar=substr($type,1);
            $theArray=explode("|",$theVar);
        /*    $theTest=$theArray[0];
            foreach($data as $key=>$val)
                $theTest=HeString::replaceText($theTest,"*".$key."*",$val);
            $theResTest = eval('return '.$theTest.';');*/

            $theFn=$theArray[1];
            foreach($data as $key=>$val)
                $theFn=HeString::replaceText($theFn,"*".$key."*",$val);
            $theVar=$theArray[0];
            foreach($data as $key=>$val)
                $theVar=HeString::replaceText($theVar,"*".$key."*",$val);
            $theVar = eval('return '.$theVar.';');
            if($theVar=="")
                    return "";
            return "<a  href='$theFn'>".$theVar."</a>";
        }
		if(substr($type,0,1)=='{')
		{
			/*sample use
				'cardNumber'=>array(              
				'name'=>'cardNumber',
				'type'=>'{HeSql::tooltipAES("*id*","cardNumber");',
          ),*/
			$theText=substr($type,1);
			foreach($data as $key=>$val)
				$theText=HeString::replaceText($theText,"*".$key."*",$val);	
			$theText = eval('return '.$theText.';');	
			return $theText;	
		}
		if(substr($type,0,1)=='|')
		{
			/*sample use
			'id'=>	array(              
            'name'=>'id',
            'type'=>"|HeString::cropStr('*id*', 4)|HeString::tootip('*id*')",
          ),*/
			$theVar=substr($type,1);
			$theArray=explode("|",$theVar);
			$theText=$theArray[0];
			foreach($data as $key=>$val)
				$theText=HeString::replaceText($theText,"*".$key."*",$val);	
			$theText = eval('return '.$theText.';');	
			$theVar=$theArray[1];
			foreach($data as $key=>$val)
				$theVar=HeString::replaceText($theVar,"*".$key."*",$val);
			$theVar = eval('return '.$theVar.';');	
			return "<a class='tooltip' href='#'>$theText<span>".$theVar."</span></a>";	
		}
		if(substr($type,0,1)=='@')
		{
			/*sample use
			'Free'=>array(  
			  'header'=>'DEL'	,
              'name'=>'email',
              'type'=>'@DEL@index.php?r=abaMessages/detail&id=*teacherid*&begin='.$model->startDate.'&end='.$model->endDate ,
			),*/
			$theVar=substr($type,1);
			$theArray=explode("@",$theVar);
			$theText=$theArray[0];
			foreach($data as $key=>$val)
				$theText=HeString::replaceText($theText,"*".$key."*",$val);
			$theVar=$theArray[1];
			foreach($data as $key=>$val)
				$theVar=HeString::replaceText($theVar,"*".$key."*",urlencode($val));
			return "<a href='$theVar'>".$theText."</a>";
		}
		if(substr($type,0,1)=="#")
		{
			$theArray=explode("#",$type);
			return $this->getDBValue($theArray[1], $theArray[2], $theArray[3], $value)." (".$value.")";
		}
		else
		if(substr($type,-1)=="+")
		{
			$postfix="($value)";
			$type=substr($type,0,-1);
		}
		else
		if(substr($type,0,1)=="+")
		{
			$prefix="($value)";
			$type=substr($type,1);
		}
		$theRender=HeList::findByKey($this->renders, $type, 0);
		if($theRender!= 0)
		{

			$theReturn = HeList::findByKey($theRender, $value, $value);
			if($theReturn!="")
				return $prefix.$theReturn.$postfix;
		}
		
		if(is_numeric($value) && is_float($value+0))
		{
			if(((float)$value)==0)
			{
				$value="0";
				return CFormatter::format($value, $type);
			}
            elseif((substr_count($value,'e') >=1) || (substr_count($value,'E') >=1))
            {
                return CFormatter::format($value, $type);
            }
			else
            {
                return $this->formatNumber((float)$value);
                //	return CFormatter::format($value, $type);
            }
		}	
		else	
			return CFormatter::format($value, $type);
	}
    /**
     * Formats the value as a number using PHP number_format() function.
     * new: if the given $value is null/empty, return null/empty string
     * @param mixed $value the value to be formatted
     * @return string the formatted result
     * @see numberFormat
     */
    public function formatNumber($value) {
        if($value === null) return null;    // new
        if($value === '') return '';        // new
        return number_format($value, $this->numberFormat['decimals'], $this->numberFormat['decimalSeparator'], $this->numberFormat['thousandSeparator']);
        }
 
    /*
     * new function unformatNumber():
     * turns the given formatted number (string) into a float
     * @param string $formatted_number A formatted number 
     * (usually formatted with the formatNumber() function)
     * @return float the 'unformatted' number
     */
    public function unformatNumber($formatted_number) {
        if($formatted_number === null) return null;
        if($formatted_number === '') return '';
        if(is_float($formatted_number)) return $formatted_number; // only 'unformat' if parameter is not float already
 
        $value = str_replace($this->numberFormat['thousandSeparator'], '', $formatted_number);
        $value = str_replace($this->numberFormat['decimalSeparator'], '.', $value);
        return (float) $value;
    }
}
?>