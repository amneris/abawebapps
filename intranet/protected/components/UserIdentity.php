<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */

	public function authenticate()
	{
		$user = new AbaUserBackoffice();
		$users=array( //possibles rols
			// username => password
			'teacher'=>'abateacher',
			'support'=>'abasupport',
			'root'=>'abaroot',
            'marketing'=>'abamarketing',
		);


		$this->errorCode = "";
		if( !$user->getUserByEmail(/*array('email'=>*/$this->username/*)*/, $this->password, true))
        {
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        }
		else
		{
            Yii::app()->request->cookies['ABA_IdUserIntranet'] = new CHttpCookie('ABA_IdUserIntranet', $user-> getUserId());
			switch($user->getRole())
			{
                case AbaUserBackoffice::MARKETING:
					$this->username="marketing";
					$this->password="abamarketing";

					break;
				case AbaUserBackoffice::TEACHER:
					$this->username="teacher";
					$this->password="abateacher";
					break;
				case AbaUserBackoffice::ROOT:
					$this->username="root";
					$this->password="abaroot";
					break;
				case AbaUserBackoffice::SUPPORT;
					$this->username="support";
					$this->password="abasupport";
					break;
			}
			if(!isset($users[$this->username]))
				$this->errorCode=self::ERROR_USERNAME_INVALID;
			else if($users[$this->username]!==$this->password)
				$this->errorCode=self::ERROR_PASSWORD_INVALID;
			else
				$this->errorCode=self::ERROR_NONE;
		}
		return !$this->errorCode;
	}
}