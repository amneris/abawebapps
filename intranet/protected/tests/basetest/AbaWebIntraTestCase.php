<?php

/**
 * Change the following URL based on your server configuration
 * Make sure the URL ends with a slash so that we can use relative URLs in test cases
 */
define('TEST_BASE_URL', 'http://intranet.aba.local/index-test.php');
/**
 * The base class for functional test cases.
 * In this class, we set the base URL for the test application.
 * We also provide some common methods to be used by concrete test classes.
 */
class AbaWebIntraTestCase extends CWebTestCase
{
    protected $sendEmailOnFailure = false;
    protected $captureScreenshotOnFailure = false;
    protected $screenshotPath = '/var/www/abawebapps/intranet/protected/tests/freport/screenshots';
    protected $screenshotUrl = 'http://intranet.aba.local/screenshots';

    /**
     * Sets up before each test method runs.
     * This mainly sets the base URL for the test application.
     */
    public function setUp()
    {
        parent::setUp();
        $this->setBrowserUrl(TEST_BASE_URL);
    }
}
