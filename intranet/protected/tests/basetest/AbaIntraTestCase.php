<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abaenglish
 * Date: 9/04/13
 * Time: 13:46
 * To change this template use File | Settings | File Templates.
 */
class AbaIntraTestCase extends CTestCase
{

    public function setUp()
    {
        parent::setUp();
    }


    public function tearDown()
    {
        Yii::app()->user->setId('');

        parent::tearDown();
    }



    /** Data provider of sample cards.
     *
     * @return array
     */
    public function dataCardsSamples()
    {
        return DataCollections::$aCreditForms;
    }

    /** Data Provider of show case users.
     *
     * @return array
     */
    public function dataUsersCase()
    {
        return DataCollections::$aUsersShowCase;
    }
}
