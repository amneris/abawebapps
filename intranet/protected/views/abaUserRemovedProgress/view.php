<?php
/* @var $this AbaUserRemovedProgressController */

$this->breadcrumbs = array(
  'User progress' => array('admin'),
  'Removed progress',
);

$this->menu = array();
?>

    <h3>Remove progress of: <?php echo " &nbsp;&nbsp; " . $user->email . " &nbsp;&nbsp; " . $user->id; ?> &nbsp;&nbsp;
        <a class="queueprogressstatuscontainer"
           style="float: none; text-decoration: none; font-size: 10px; font-style: normal; " target="_blank"
           href="<?php echo HeList::getExternalUrl($model); ?>">AUTOLOGIN
        </a>
    </h3>

<?php if (isset($errMsg) AND trim($errMsg) <> ''): ?>
    <div class="form">
        <div class="errorSummary"><p>Please fix the following input errors:</p>
            <ul>
                <li><?php echo $errMsg; ?></li>
            </ul>
        </div>
    </div>
<?php endif; ?>

    <div class="view">

        <b/><?php echo CHtml::encode($model->getAttributeLabel('removedProgressId')); ?>:</b>
        <?php echo CHtml::encode($model->removedProgressId); ?>
        <br/><br/>
        <b/><?php echo CHtml::encode($model->getAttributeLabel('units')); ?>:</b>
        <?php echo CHtml::encode($model->units); ?>
        <br/><br/>
        <b/><?php echo CHtml::encode($model->getAttributeLabel('dateAdd')); ?>:</b>
        <?php echo CHtml::encode($model->dateAdd); ?>
        <br/>

    </div>

<?php $this->widget(
  'application.components.widgets.CGridViewABA',
  array(
    'id' => 'aba-userprogressremoved-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'emptyText' => '<b>No results found. Type in any text in any column to execute a search.</b>',
    'pager' => array(
      'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
      'prevPageLabel' => '<',
      'nextPageLabel' => '>',
      'firstPageLabel' => '<<',
      'lastPageLabel' => '>>',
    ),
    'dataProvider' => $model->searchRemovedProgress(array(
      'userId' => $user->id,
      'removedProgressId' => $model->removedProgressId
    )),
    'columns' => array(
      'removedProgressId' => array(
        'header' => 'Id',
        'name' => 'removedProgressId',
      ),
      'userId' => array(
        'header' => 'User ID',
        'name' => 'userId',
      ),
      'units' => array(
        'header' => 'Unit(s)',
        'name' => 'units',
      ),
      'dateAdd' => array(
        'header' => 'Created',
        'name' => 'dateAdd',
      ),
    )
  )
);




