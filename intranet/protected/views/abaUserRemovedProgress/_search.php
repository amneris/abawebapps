<?php
/* @var $this AbaUserRemovedProgressController */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
      'CActiveForm',
      array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
      )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'removedProgressId'); ?>
        <?php echo $form->textField($model, 'removedProgressId', array('size' => 25, 'maxlength' => 20)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'userId'); ?>
        <?php echo $form->textField($model, 'userId', array('size' => 25, 'userId' => 20)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'units'); ?>
        <?php echo $form->textField($model, 'units', array('size' => 25, 'maxlength' => 1000)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dateAdd');
        echo $form->textField($model, 'dateAdd');
        echo CHtml::image("images/calendar.jpg", "calendar1", array("id" => "c_buttonDateAdd", "class" => "pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
          array(
            'inputField' => 'LogUserProgressRemoved_dateAdd',
            'button' => 'c_buttonDateAdd',
            'ifFormat' => '%Y-%m-%d 00:00:00',
          ));
        ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->