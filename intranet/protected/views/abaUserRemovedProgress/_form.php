<?php
/* @var $this AbaUserRemovedProgressController */
?>

<div class="form">

    <?php $form = $this->beginWidget(
      'CActiveForm',
      array(
        'id' => 'aba-userprogressremoved-form',
        'enableAjaxValidation' => false,
      )
    );
    ?>

    <?php echo $form->errorSummary($model); ?>

    <table id="myForm" style="background-color: #ebebeb;">
        <tr>
            <td style="vertical-align: top; text-align: center;" colspan="2">
                <br/><br/>
                <a target="_blank" href="javascript: $('#aba-userprogressremoved-form').submit();" class="btnCancel">
                    REMOVE PROGRESS
                </a>

                <p class="cancel">* Remove progress from all units</p>
                <p class="cancel" style="font-weight: bold;">THE PROCESS COULD TAKE A FEW SECONDS</p>
            </td>
        </tr>
    </table>

    <div class="row buttons" align="center">
        <?php echo CHtml::hiddenField("LogUserProgressRemoved[userId]", $user->id,
          array("id" => "LogUserProgressRemoved_userId")); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
