<?php
/* @var $this AbaUserRemovedProgressController */

$this->breadcrumbs = array(
  'Removed user progress',
);

$this->menu = array();

Yii::app()->clientScript->registerScript(
  'search',
  "
   $('.search-button').click(function(){
     $('.search-form').toggle();
     return false;
   });
   $('.search-form form').submit(function(){
     $.fn.yiiGridView.update('aba-userprogressremoved-grid', {
       data: $(this).serialize()
     });
     return false;
   });
   "
);
?>

<h3>Removed user progress</h3>

<br>
<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial(
      '_search',
      array(
        'model' => $model,
      )
    ); ?>
</div><!-- search-form -->

<?php $this->widget(
  'application.components.widgets.CGridViewABA',
  array(
    'id' => 'aba-userprogressremoved-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager' => array(
      'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
      'prevPageLabel' => '<',
      'nextPageLabel' => '>',
      'firstPageLabel' => '<<',
      'lastPageLabel' => '>>',
    ),
    'extraInfo' => array('view'),
    'dataProvider' => $model->search(),
    'filter' => $model,
    'filterSelector' => '{filter}',
    'columns' => array(
      'removedProgressId',
      'userId',
      'units',
      'dateAdd',
      array(
        'class' => 'CLinkColumn',
        'label' => 'Open',
        'urlExpression' => 'HeList::getExternalUrl($data)',
        'header' => 'Autologin',
        'linkHtmlOptions' => array('target' => '_blank', 'class' => 'queueprogressstatuscontainer'),
      ),
      array(
        'class' => 'CButtonColumnABA',
        'header' => 'Options',
      ),
    ),
  )
); ?>
