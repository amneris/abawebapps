<?php
/* @var $this AbaUserRemovedProgressController */

$this->breadcrumbs = array(
  'User progress' => array('admin'),
  $model->removedProgressId => array('view', 'id' => $model->removedProgressId),
  'Update progress',
);

$this->menu = array();
?>

    <h3>Edit progress of: <?php echo " &nbsp;&nbsp; " . $user->email . " &nbsp;&nbsp; " . $user->id; ?></h3>

<?php echo $this->renderPartial('_form', array('model' => $model, 'user' => $user)); ?>