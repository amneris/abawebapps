<?php
/* @var $this AbaUserRemovedProgressController */

$this->breadcrumbs = array(
  'Removed user progress',
);

$this->menu = array();
?>

<h3>Removed user progress</h3>

<?php $this->widget(
  'zii.widgets.CListView',
  array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
  )
); ?>
