<?php
/* @var $this AbaUserRemovedProgressController */
?>

<div class="view">

    <b/><?php echo CHtml::encode($data->getAttributeLabel('removedProgressId')); ?>:</b>
    <?php echo CHtml::encode($data->removedProgressId); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('userId')); ?>:</b>
    <?php echo CHtml::encode($data->userId); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('units')); ?>:</b>
    <?php echo CHtml::encode($data->units); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('dateAdd')); ?>:</b>
    <?php echo CHtml::encode($data->dateAdd); ?>
    <br/>

</div>