<?php
/* @var $this AbaUserRemovedProgressController */

$this->breadcrumbs = array(
  'User progress' => array('admin'),
  'Remove progress',
);

$this->menu = array();
?>

    <h3>Remove progress of: <?php echo " &nbsp;&nbsp; " . $user->email . " &nbsp;&nbsp; " . $user->id; ?></h3>

<?php echo $this->renderPartial('_form', array('model' => $model, 'user' => $user)); ?>

<?php $this->widget(
  'application.components.widgets.CGridViewABA',
  array(
    'id' => 'aba-userremovedprogress-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'emptyText' => '<b>No results found. Type in any text in any column to execute a search.</b>',
    'pager' => array(
      'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
      'prevPageLabel' => '<',
      'nextPageLabel' => '>',
      'firstPageLabel' => '<<',
      'lastPageLabel' => '>>',
    ),
    'dataProvider' => $model->searchRemovedProgress(array('userId' => $user->id)),
    'columns' => array(
      'removedProgressId' => array(
        'header' => 'ID Removed progress',
        'name' => 'removedProgressId',
      ),
      'units' => array(
        'header' => 'Unit(s)',
        'name' => 'units',
      ),
      'dateAdd' => array(
        'header' => 'Executed',
        'name' => 'dateAdd',
      ),
    ),
  )
);

