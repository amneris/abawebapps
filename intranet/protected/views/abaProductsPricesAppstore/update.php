<?php
/* @var $this AbaProductsPricesAppstoreController */
/* @var $model ProductsPricesAppstore */

$this->breadcrumbs=array(
    'ProductsAppStore' => array('admin'),
    '[Current Product]' => array('view', 'id' => $model->idProductTier),
    'Update',
);
?>
<?php if(Yii::app()->user->hasFlash('delError')): ?>
    <?php echo "<br>"; ?>
    <div class="flash-error">
        <?php echo Yii::app()->user->getFlash('delError'); ?>
    </div>

<?php endif; ?>

<h3>Edit product: <?php echo $model->idProductApp; ?></h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
