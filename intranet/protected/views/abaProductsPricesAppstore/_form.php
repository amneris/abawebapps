<?php
/* @var $this AbaProductsPricesAppstoreController */
/* @var $model ProductsPricesAppstore */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'aba-products-prices-form',
	'enableAjaxValidation' => false,
)); ?>

<?php
$modelCountry = new AbaCountry;
$countryList =  $modelCountry->getFullList();

HeMixed::arrayUnshiftAssoc($countryList, '', '');

$options = array(1 => 'Yes', 0 => 'No');
?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <table id="myForm">

        <?php if(is_numeric($model->idProductTier)): ?>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'idProductTier'); ?>
            </td>
            <td>
                <?php echo $model->idProductTier; ?>
            </td>
        </tr>
        <?php endif; ?>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'idProductApp'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'idProductApp',array('size'=>20, 'maxlength'=>20)); ?>
                <?php echo $form->error($model, 'idProductApp'); ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'idPeriodPay'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'idPeriodPay',array('size'=>5, 'maxlength'=>5)); ?>
                <?php echo $form->error($model ,'idPeriodPay'); ?>
            </td>
        </tr>
<!---->
<!--        <tr>-->
<!--            <td>-->
<!--                --><?php //echo $form->labelEx($model, 'currencyAppStore'); ?>
<!--            </td>-->
<!--            <td>-->
<!--                --><?php //echo $form->textField($model, 'currencyAppStore',array('size'=>5, 'maxlength'=>5)); ?>
<!--                --><?php //echo $form->error($model ,'currencyAppStore'); ?>
<!--            </td>-->
<!--        </tr>-->

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'tier'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'tier',array('size'=>5, 'maxlength'=>5)); ?>
                <?php echo $form->error($model ,'tier'); ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'priceAppStore'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'priceAppStore',array('size'=>7, 'maxlength'=>7)); ?>
                <?php echo $form->error($model ,'priceAppStore'); ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'isBestOffer'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'isBestOffer', $options); ?>
                <?php echo $form->error($model, 'isBestOffer'); ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'idCountry'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'idCountry', $countryList); ?>
                <?php echo $form->error($model, 'idCountry'); ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'isDefault'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'isDefault', $options); ?>
                <?php echo $form->error($model, 'isDefault'); ?>
            </td>
        </tr>

    </table>

	<div class="row buttons" align="center">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->