<?php
/* @var $this AbaProductsPricesAppstoreController */
/* @var $model ProductsPricesAppstore */

$this->breadcrumbs = array(
  'Product List',
);

$this->menu = array(
  array('label' => 'List ProductsPricesAppstore', 'url' => array('index')),
  array('label' => 'Create ProductsPricesAppstore', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('aba-products-prices-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>AppStore Product list & prices</h3>

<?php echo CHtml::link('Create new appstore product', array('AbaProductsPricesAppstore/create')); ?><br/>
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>

<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
      'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('application.components.widgets.CGridViewABA', array(
  'id' => 'aba-products-prices-grid',
  'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
  'pager' => array(
    'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
    'prevPageLabel' => '<',
    'nextPageLabel' => '>',
    'firstPageLabel' => '<<',
    'lastPageLabel' => '>>',
  ),
  'extraInfo' => array('view', 'update', 'delete'),
  'dataProvider' => $model->search(),
  'filter' => $model,
  'filterSelector' => '{filter}',
  'columns' => array(
    'idProductTier',
    'idProductApp',
    'idPeriodPay',
    'tier',
    'priceAppStore' =>
      array(
        'name' => 'priceAppStore',
        'type' => '{number_format("*priceAppStore*", 2);'
      ),
    'isBestOffer' =>
      array(
        'name' => 'isBestOffer',
        'type' => 'booleanList',
        'filter' => HeList::booleanList()
      ),
    'idCountry' =>
      array(
        'name' => 'idCountry',
        'type' => 'countryList+',
        'filter' => HeList::getCountryList(),
      ),
    'isDefault' =>
      array(
        'name' => 'isDefault',
        'type' => 'booleanList',
        'filter' => HeList::booleanList()
      ),
    array(
      'class' => 'CButtonColumnABA',
      'htmlOptions' => array('style' => 'width: 110px;'),
      'header' => 'Options',
    ),
  ),
)); ?>
