<?php
/* @var $this AbaProductsPricesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Products Prices',
);

$this->menu=array(
	array('label'=>'Create AbaProductsPrices', 'url'=>array('create')),
	array('label'=>'Manage AbaProductsPrices', 'url'=>array('admin')),
);
?>

<h3>Products prices</h3>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
