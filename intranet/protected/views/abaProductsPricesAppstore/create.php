<?php
/* @var $this AbaProductsPricesAppstoreController */
/* @var $model ProductsPricesAppstore */

$this->breadcrumbs = array(
    'ProductsAppStore' => array('admin'),
	'Create',
);

$this->menu = array(
	array('label' => 'List AbaProductsPricesAppstore', 'url' => array('index')),
	array('label' => 'Manage ProductsPricesAppstore', 'url' => array('admin')),
);
?>

<h3>Create Appstore Product</h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
