<?php
/* @var $this AbaProductsPricesAppstoreController */
/* @var $model ProductsPricesAppstore */

$this->breadcrumbs=array(
    'ProductsAppStore' => array('admin'),
    $model->idProductApp,
);

$this->menu=array(
    array('label' => 'Update AbaUser', 'url' => array('update', 'id' => $model->idProductTier)),
    array('label' => 'Manage AbaUser', 'url' => array('admin')),
);

?>
<h3>View product: <?php echo $model->idProductApp; ?></h3>

<?php $this->widget('application.components.widgets.CDetailViewABA', array(
    'data'=>$model,
    'attributes'=>array(
        'idProductTier',
        'idProductApp',
        'idPeriodPay',
        'tier',
        'priceAppStore',
        'isBestOffer' =>
            array(
                'name' =>   'isBestOffer',
                'type' =>   'booleanList',
                'filter' => HeList::booleanList()
            ),
        'idCountry' =>
            array(
                'name' =>   'idCountry',
                'type' =>   'countryList+',
                'filter' => HeList::getCountryList(),
            ),
        'isDefault' =>
            array(
                'name' =>   'isDefault',
                'type' =>   'booleanList',
                'filter' => HeList::booleanList()
            ),
    ),
)); ?>
