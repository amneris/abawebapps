<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 7/12/12
 * Time: 14:01
 * To change this template use File | Settings | File Templates.
 */

/*
 * STYLES HAS TO BE INLINE INSERTED. NO HEADERS NOR IMPORTS!!!!!!!
 * */
?>
<!-- /*
 * STYLES HAS TO BE INLINE INSERTED. NO HEADERS NOR IMPORTS!!!!!!!
 * */ -->
<div style="clear: both;"></div>
<div style=" width: 921px;border-style: solid;border-width: 1px;border-color: #e1dfdc;background-color: #006400;color: #ffffff;padding-left: 20px;">
    <div style="margin-top: 28px;">
        <span class="normal">
                AbaEnglish
        </span>
    </div>
    <div style="clear: both;"></div>
    <br/><br/><br/>
    <div>
        <div style="font-weight: bold; font-size: 16px;">
            Enviado email desde la Intranet de ABA<br/>
        </div>
    </div>

    <br/><br/>
    <!-- /*
 * STYLES HAS TO BE INLINE INSERTED. NO HEADERS NOR IMPORTS!!!!!!!
 * */ -->
    <div style="clear: both;"></div>
    <div style="margin: 8px;border-style: solid 1px #e1dfdc;">
        <div>
            INFO SUMMARY:
        </div>
        <div>
            <?php echo $subject; ?>
        </div>

        <br/>
        <div style="clear: both;"></div>

        <div>
            DETAILS:
        </div>
        <div>
            <?php echo $body; ?>
        </div>

        <br/>
        <div style="clear: both;"></div>

        <div>
            BACKTRACE:
        </div>
        <div style="border-style: 2px dashed #666666; font-style: italic;">
            <?php //(debug_print_backtrace()); ?>
        </div>
    </div>
</div>
<!-- /*
* STYLES HAS TO BE INLINE INSERTED. NO HEADERS NOR IMPORTS!!!!!!!
* */ -->