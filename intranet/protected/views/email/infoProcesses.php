<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 7/12/12
 * Time: 14:01
 * To change this template use File | Settings | File Templates.
 */

/*
 * STYLES HAS TO BE INLINE INSERTED. NO HEADERS NOR IMPORTS!!!!!!!
 * */
?>
<!-- /*
 * STYLES HAS TO BE INLINE INSERTED. NO HEADERS NOR IMPORTS!!!!!!!
 * */ -->
<div style="clear: both;"></div>
<div style=" width: 90%;border-style: solid;border-width: 1px;border-color: #e1dfdc;background-color: #3b86a0;color: #fffafa;padding-left: 20px;">
    <div style="margin-top: 28px;">
        <span class="normal">
                AbaEnglish
        </span>
    </div>
    <div style="clear: both;"></div>
    <br/><br/><br/>
    <div>
        <div style="font-weight: bold; font-size: 16px;">
            Enviado email desde la Intranet de ABA<br/>
        </div>
    </div>

    <br/><br/>
    <!-- /*
 * STYLES HAS TO BE INLINE INSERTED. NO HEADERS NOR IMPORTS!!!!!!!
 * */ -->
    <div style="clear: both;"></div>
    <div style="margin: 8px;border-style: solid 1px #e1dfdc;">
        <div>
            INFO SUMMARY:
        </div>
        <div>
            <?php echo $subject; ?>
        </div>

        <br/>
        <div style="clear: both;"></div>

        <div>
            DETAILS:
        </div>
        <div>
            <?php if(!is_array($body))
            {
                echo $body;
            }
            else
            {
            ?><table style="border-style: solid 1px ; color: whitesmoke;"><?php
                        $i=0;
                        foreach($body as $aRows)
                        {
                            $i++;
                            if($i==1){
                                ?><tr><?php
                                    foreach($aRows as $name=>$value)
                                    {
                                        ?><th style="width: 150px;"><?php

                                        echo '<span style="font-style:bold;">'.$name.'</span>';

                                        ?></th><?php
                                    }
                                    ?></tr><?php
                            }

                            ?><tr><?php
                            foreach($aRows as $name=>$value)
                            {
                            ?><td style="width: 150px;border-style: solid 1px ; color: whitesmoke; ">
                                <span style="color:lightcyan;">
                                <?php echo ''.$value; ?>
                                </span>
                              </td>
                            <?php } ?>
                            </tr><?php
                        }
             ?></table>
       <?php } ?>
        </div>

        <br/>
        <div style="clear: both;"></div>
    </div>
</div>
<!-- /*
* STYLES HAS TO BE INLINE INSERTED. NO HEADERS NOR IMPORTS!!!!!!!
* */ -->