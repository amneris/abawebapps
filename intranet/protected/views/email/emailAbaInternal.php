<?php
/* @var $body string|array */
/* @var $subject string */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html">
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <title>Email</title>



    </head>
    <body class="body" style="padding:0 !important; margin:0 !important; display:block !important; background:#f0f0f0; -webkit-text-size-adjust: none">
    <style type="text/css" media="screen">
            /* Linked Styles */
        body { padding:0 !important; margin:0 !important; display:block !important; background:#f0f0f0; -webkit-text-size-adjust: none }
        a { color:#007ee1; text-decoration:underline }
    </style>
    <style type="text/css"  media="screen">
        div.cmp_subheader{
            width: 100%;
            height: 90px;
        }

        #my_account_nav   li  a{
            color: #44545f;
            text-decoration: none;
            display: block;
        }
        #my_account_nav   li  a:hover {
            text-decoration: underline;
        }

    </style>
    <table width="1000px" border="0" cellspacing="0" cellpadding="0" bgcolor="#f0f0f0"">
    <tr><td>
            <?php
            $imagePath = "";
            ?>
           <!-- <a href="https://campus.abaenglish.com">
                <img src="<?php /*echo $base64; */?>" style="float: left !important;position: absolute;
                                                                    top: 10px;left: 25px;">
            </a>-->
            <div class="cmp_subheader" style="width: 1000px;">
                <ul id="my_account_nav" style="float:right;list-style: disc; color: #19BFF3;
                                     background:#f0f0f0; cursor: pointer; font-family: 'museo_slab_700regular';
                                     font-size: 14px; line-height: 50px; list-style: none; width: 1000px; text-align: center;">
                    <li style="font-family: 'proxima_nova_rgregular'; font-size: 13px; float: left;
                color: #44545f; list-style: none; padding: 8px; padding-right: 30px; border-bottom: 1px solid #DDE0E2; width: 200px;">
                        <span id="linkIntranet">
                            <?php echo CHtml::link("Intranet", "http://intranet.abaenglish.com"); ?>
                        </span>
                    </li>
                    <li style="font-family: 'proxima_nova_rgregular'; font-size: 13px; float: left;
                color: #44545f; list-style: none; padding: 8px; padding-right: 30px; border-bottom: 1px solid #DDE0E2; width: 200px;">
                        <span id="linkCampus">
                            <?php echo CHtml::link("Campus", "http://campus.abaenglish.com"); ?>
                        </span>
                    </li>
                    <li style="font-family: 'proxima_nova_rgregular'; font-size: 13px; float: left;
                color: #44545f; list-style: none; padding: 8px; padding-right: 30px; border-bottom: 1px solid #DDE0E2; width: 200px;">
                        <span id="linkWeb">
                            <?php echo CHtml::link("Web", "http://www.abaenglish.com"); ?>
                        </span>
                    </li>
                </ul>
            </div>
        </td></tr>
        <tr><td>
            <div style="background:#f0f0f0; height: 80px; width: 1000px; font-family: 'MuseoSlab500'; font-size: 16px; color: #39444c; text-align: center; float: left;">
                <?php echo "Warning e-mail, auto-generated and sent from Campus or Intranet." ?>
            </div>
        </td></tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f0f0f0" style="float: left !important;">
            <tr>

                    <td align="center" valign="top">
                            <div style="font-size:0pt; line-height:0pt; height:9px; background:#55464b; "><img src="images/empty.gif" width="1" height="9" style="height:9px" alt="" /></div>

                            <div style="font-size:0pt; line-height:0pt; height:1px; background:#ffffff; "><img src="images/empty.gif" width="1" height="1" style="height:1px" alt="" /></div>

                            <table width="1000" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                            <td>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>

                                                                    <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" valign="top">
                                                                        <div style="font-size:0pt; line-height:0pt; height:26px">
                                                                            <img src="images/empty.gif" width="1" height="26" style="height:26px" alt="" />
                                                                        </div>
                                                                    </td>
                                                                    <td class="web" style="color:#444444; font-family:Arial; font-size:10px; line-height:30px; text-align:left" valign="top">
                                                                        You’re receiving this newsletter because you are part of the mailing list linked to FINANCE, IT or SUPPORT.
                                                                        Ask IT for doubts about this notification.
                                                                    </td>
                                                            </tr>

                                                    </table>
                                                    <div style="font-size:0pt; line-height:0pt; height:13px"><img src="images/empty.gif" width="1" height="13" style="height:13px" alt="" /></div>

                                            </td>
                                    </tr>
                                    <tr>
                                            <td class="img" style="font-size:0pt; line-height:0pt; text-align:left">&nbsp;</td>
                                    </tr>
                                    <tr>

                                            <td class="text" style="color:#444444; font-family:Arial; font-size:12px; line-height:20px; text-align:left">
                                                    <div class="h2" style="color:#212727; font-family:Arial; font-size:19px; line-height:23px; text-align:left; font-weight:bold">
                                                        <?php echo $subject; ?>
                                                    </div>
                                                    <div style="font-size:0pt; line-height:0pt; height:10px"><img src="images/empty.gif" width="1" height="10" style="height:10px" alt="" /></div>

                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                    <td class="img" style="font-size:0pt; line-height:0pt; text-align:left">
                                                                        <!--<img src="images/separator.jpg" width="795" height="1" alt="" border="0" />-->
                                                                    </td>
                                                            </tr>
                                                    </table>

                                                    <div style="font-size:0pt; line-height:0pt; height:15px"><img src="images/empty.gif" width="1" height="15" style="height:15px" alt="" /></div>

                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                    <td class="text" style="color:#444444; font-family:Arial; font-size:12px; line-height:20px; text-align:left">
                                                                        <!-- Body principal -->
                                                                         <div>
                                                                            <?php
                                                                            $bodyDesc = $body;
                                                                            $listTable = null;
                                                                            if(is_array($body) && array_key_exists("body", $body)){
                                                                                $listTable = $body["listTable"];
                                                                                $bodyDesc = $body["body"];
                                                                            }elseif(is_array($body)){
                                                                                $listTable = $body;
                                                                            }

                                                                            if(!is_array($bodyDesc))
                                                                            {
                                                                                echo $bodyDesc;
                                                                            }
                                                                            if(!empty($listTable))
                                                                            {
                                                                                ?><table cellpadding="4px"><?php
                                                                                $i=0;
                                                                                foreach($listTable as $aRows)
                                                                                {
                                                                                    $i++;
                                                                                    if($i==1){
                                                                                        ?><tr style="background-color:#55464b;"><?php
                                                                                        foreach($aRows as $name=>$value)
                                                                                        {
                                                                                            ?><td style="font-weight: bold;background-color:#55464b;"><?php

                                                                                            echo '<span style="font-style:bold;color:white;">'.$name.'</span>';

                                                                                            ?></td><?php
                                                                                        }
                                                                                        ?></tr><tr><?php
                                                                                        foreach($aRows as $value)
                                                                                        {
                                                                                            ?>
                                                                                            <td style="border:1px #d3d3d3 solid;">
                                                                                                <?php  echo '<span style="color: #212121;">'.$value.'</span>'; ?>
                                                                                            </td>
                                                                                        <?php } ?>
                                                                                        </tr><?php
                                                                                    }else{

                                                                                    ?><tr><?php
                                                                                    foreach($aRows as $value)
                                                                                    {
                                                                                        ?>
                                                                                        <td style="border:1px #d3d3d3 solid;">
                                                                                            <?php  echo '<span style="color: #212121;">'.$value.'</span>'; ?>
                                                                                        </td>
                                                                                    <?php } ?>
                                                                                    </tr><?php }
                                                                                }
                                                                                ?></table>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </td>
                                                                    <td class="img-right" style="font-size:0pt; line-height:0pt; text-align:right" width="95"><img src="images/img_1.jpg" width="75" height="92" alt="" border="0" /></td>
                                                            </tr>
                                                    </table>
                                                    <div style="font-size:0pt; line-height:0pt; height:5px"><img src="images/empty.gif" width="1" height="5" style="height:5px" alt="" /></div>

                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                    <td class="img" style="font-size:0pt; line-height:0pt; text-align:left"><a href="#" target="_blank"><img src="images/read_more.jpg" width="104" height="29" alt="" border="0" /></a></td>
                                                            </tr>
                                                    </table>
                                                    <div style="font-size:0pt; line-height:0pt; height:50px"><img src="images/empty.gif" width="1" height="50" style="height:50px" alt="" /></div>

                                                    <div style="font-size:0pt; line-height:0pt; height:15px"><img src="images/empty.gif" width="1" height="15" style="height:15px" alt="" /></div>

                                            </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="481" valign="top" class="text"
                                                        style="color:#444444; font-family:Arial; font-size:12px; line-height:20px; text-align:left">
                                                        <div class="h2"
                                                             style="color:#212727; font-family:Arial; font-size:19px; line-height:23px; text-align:left; font-weight:bold">
                                                            Source details from this e-mail.
                                                        </div>

                                                        <div style="font-size:0pt; line-height:0pt; height:10px"><img
                                                                src="images/empty.gif" width="1" height="10"
                                                                style="height:10px" alt=""/></div>

                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="img"
                                                                    style="font-size:0pt; line-height:0pt; text-align:left">
                                                                    <img src="images/separator_wide.jpg" width="481"
                                                                         height="1" alt="" border="0"/></td>
                                                            </tr>
                                                        </table>
                                                        <!-- Triggers and source of the e-mail. -->
                                                        <table width="100%" border="0" cellspacing="0"
                                                               cellpadding="0">
                                                            <tr>
                                                                <td valign="top" width="150px" style="vertical-align:top;" >
                                                                    Trigger URL / Process / Cron:
                                                                </td>
                                                                <td class="list" style="color:#444444; font-family:Arial; font-size:12px; line-height:16px; text-align:left" valign="top">
                                                                    <a href="http://campus.abaenglish.com" target="_blank" class="link" style="color:#007ee1; text-decoration:underline">
                                                                    <?php if( array_key_exists("HTTP_HOST", $_SERVER) )
                                                                    {
                                                                        echo $_SERVER["HTTP_HOST"]."".$_SERVER["REQUEST_URI"];
                                                                    }
                                                                    ?>
                                                                    <br />
                                                                    <?php if(array_key_exists("HTTP_REFERER", $_SERVER))
                                                                    {
                                                                        echo $_SERVER["HTTP_REFERER"];
                                                                    }
                                                                    ?>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td  valign="top" width="150px" style="vertical-align:top;" >
                                                                    Execution path or debug backtrace:
                                                                    <div style="font-size:0pt; line-height:0pt; height:6px">
                                                                        <img src="images/empty.gif" width="1" height="6" style="height:6px" alt=""/>
                                                                    </div>
                                                                    <img src="images/bullet.jpg" width="3" height="3" alt="" border="0"/></td>
                                                                <td class="list" style="color:#444444; font-family:Arial; font-size:12px; line-height:16px; text-align:left" valign="top">
                                                                    <?php
                                                                    $aScriptSteps = debug_backtrace();
                                                                    foreach($aScriptSteps as $stepFunction) {
                                                                        if(array_key_exists("file",$stepFunction)){
                                                                            echo htmlentities( $stepFunction["file"] );
                                                                            echo ', ';
                                                                            echo htmlentities( $stepFunction["function"] );
                                                                            echo ', ';
                                                                            echo htmlentities( $stepFunction["line"] );
                                                                            echo '<br/>';
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" width="150px" style="vertical-align:top;" >
                                                                   Time execution:
                                                                </td>
                                                                <td class="list" style="color:#444444; font-family:Arial; font-size:12px; line-height:16px; text-align:left" valign="top">
                                                                    <?php echo HeDate::todaySQL(true); ?>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div style="font-size:0pt; line-height:0pt; height:15px"><img src="images/empty.gif" width="1" height="15" style="height:15px" alt=""/></div>
                                        </td>
                                    </tr>

                                    <tr>
                                            <td>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                    <td class="img" style="font-size:0pt; line-height:0pt; text-align:left"><img src="images/box_top.jpg" width="795" height="4" alt="" border="0" /></td>
                                                            </tr>
                                                            <tr>
                                                                    <td>
                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                                                    <tr>
                                                                                            <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="1" bgcolor="#e2e2e2"></td>
                                                                                            <td bgcolor="#fdfdfd"></td>
                                                                                            <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="1" bgcolor="#e2e2e2"></td>
                                                                                    </tr>
                                                                            </table>

                                                                    </td>
                                                            </tr>
                                                            <tr>
                                                                    <td>
                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#e2e2e2">
                                                                                    <tr>
                                                                                            <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="1" bgcolor="#dadada"></td>
                                                                                            <td>
                                                                                                    <div style="font-size:0pt; line-height:0pt; height:1px; background:#aeaeae; "><img src="images/empty.gif" width="1" height="1" style="height:1px" alt="" /></div>

                                                                                                    <div style="font-size:0pt; line-height:0pt; height:1px; background:#c7c7c7; "><img src="images/empty.gif" width="1" height="1" style="height:1px" alt="" /></div>

                                                                                                    <div style="font-size:0pt; line-height:0pt; height:1px; background:#dddddd; "><img src="images/empty.gif" width="1" height="1" style="height:1px" alt="" /></div>

                                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="10">
                                                                                                            <tr>
                                                                                                                    <td>
                                                                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                                    <tr>

                                                                                                                                            <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="10"></td>
                                                                                                                                            <td class="footer" style="color:#878787; font-family:Arial; font-size:9px; line-height:17px; text-align:left">
                                                                                                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pulvinar, eros tristique hendrerit tempor, ipsum mauris tristique metus,in luctus enim massa at diam. Sed eu augue eros. Duis sed velit ac risus mollis molestie. Design by Aba English</a>
                                                                                                                                            </td>
                                                                                                                                            <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="10"></td>
                                                                                                                                    </tr>
                                                                                                                            </table>
                                                                                                                    </td>
                                                                                                            </tr>
                                                                                                    </table>

                                                                                            </td>
                                                                                            <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="1" bgcolor="#dadada"></td>
                                                                                    </tr>
                                                                            </table>
                                                                    </td>
                                                            </tr>
                                                            <tr>
                                                                    <td class="img" style="font-size:0pt; line-height:0pt; text-align:left"><img src="images/box_bottom.jpg" width="795" height="4" alt="" border="0" /></td>
                                                            </tr>

                                                    </table>
                                            </td>
                                    </tr>
                            </table>
                            <div style="font-size:0pt; line-height:0pt; height:37px"><img src="images/empty.gif" width="1" height="37" style="height:37px" alt="" /></div>

                    </td>
            </tr>
    </table>

    </body>
    </html>