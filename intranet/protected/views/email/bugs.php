<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 7/12/12
 * Time: 14:01
 * To change this template use File | Settings | File Templates.
 */

/*
 * STYLES HAS TO BE INLINE INSERTED. NO HEADERS NOR IMPORTS!!!!!!!
 * */
?>
<!-- /*
 * STYLES HAS TO BE INLINE INSERTED. NO HEADERS NOR IMPORTS!!!!!!!
 * */ -->
<div style="clear: both;"></div>
<div style=" width: 921px;border-style: solid;border-width: 1px;border-color: #e1dfdc;background-color: #fff;padding-left: 20px;">
    <div style="margin-top: 28px;">
        <span class="normal">
                AbaEnglish
        </span>
    </div>
    <div style="clear: both;"></div>
    <br/><br/><br/>
    <div>
        <div style="font-weight: bold; font-size: 16px;">
            Enviado email desde la Intranet de ABA debido a una incidencia de detección automática.<br/>
            Please review the details.
        </div>
    </div>

    <br/><br/>
    <!-- /*
 * STYLES HAS TO BE INLINE INSERTED. NO HEADERS NOR IMPORTS!!!!!!!
 * */ -->
    <div style="clear: both;"></div>
    <div style="margin: 8px;border-style: solid 1px #e1dfdc;background-color: #ECFBD4;">
        <div>
            URL SOURCE / REFERRER:
        </div>
        <div>
            <?php if( array_key_exists("HTTP_HOST", $_SERVER) )
            {
               echo $_SERVER["HTTP_HOST"]."".$_SERVER["REQUEST_URI"];
            }
            ?>
            <br />
            <?php if(array_key_exists("HTTP_REFERER", $_SERVER))
            {
                echo $_SERVER["HTTP_REFERER"];
            }
            ?>
        </div>

        <br/>
        <div>
            BUG/WARNING CODE:
        </div>
        <div>
            <?php echo $subject; ?>
        </div>

        <br/>
        <div style="clear: both;"></div>

        <div>
            DETAILS:
        </div>
        <div>
            <?php echo $body; ?>
        </div>

        <br/>
        <div style="clear: both;"></div>

        <div>
            BACKTRACE:
        </div>
        <div style="border-style: 2px dashed #666666; font-style: italic;">
            <?php //(debug_print_backtrace()); ?>
        </div>
    </div>
</div>
<!-- /*
* STYLES HAS TO BE INLINE INSERTED. NO HEADERS NOR IMPORTS!!!!!!!
* */ -->