<?php
/* @var $this AbaTaxRatesController */
/* @var $model AbaTaxRates */

$this->breadcrumbs=array(
	'Tax Rates'=>array('admin'),
	$model->idTaxRate=>array('view','id'=>$model->idTaxRate),
	'Update',
);

$this->menu=array(
	array('label'=>'List TaxRates', 'url'=>array('index')),
	array('label'=>'Create TaxRates', 'url'=>array('create')),
	array('label'=>'View TaxRates', 'url'=>array('view', 'id'=>$model->idTaxRate)),
	array('label'=>'Manage TaxRates', 'url'=>array('admin')),
);
?>

<h1>Update TaxRates <?php echo $model->idTaxRate; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>