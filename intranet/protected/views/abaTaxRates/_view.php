<?php
/* @var $this AbaTaxRatesController */
/* @var $data AbaTaxRates */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idTaxRate')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idTaxRate), array('view', 'id'=>$data->idTaxRate)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCountry')); ?>:</b>
	<?php echo CHtml::encode($data->idCountry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('taxRateValue')); ?>:</b>
	<?php echo CHtml::encode($data->taxRateValue); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dateStartRate')); ?>:</b>
	<?php echo CHtml::encode($data->dateStartRate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dateEndRate')); ?>:</b>
	<?php echo CHtml::encode($data->dateEndRate); ?>
	<br />


</div>