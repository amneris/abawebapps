<?php
/* @var $this AbaTaxRatesController */
/* @var $model AbaTaxRates */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idTaxRate'); ?>
		<?php echo $form->textField($model,'idTaxRate',array('size'=>10,'maxlength'=>10)); ?>
	</div>

    <div class="row">
        <?php echo $form->label($model,'idCountry'); ?>
        <?php echo $form->DropDownList($model,'idCountry',HeList::getUeCountryList(), array('empty'=>'--Any Country--')); ?>
    </div>

	<div class="row">
		<?php echo $form->label($model,'taxRateValue'); ?>
		<?php echo $form->textField($model,'taxRateValue',array('size'=>6,'maxlength'=>6)); ?>
	</div>

    <div class="row">
        <?php echo $form->label($model,'dateStartRate');
        echo $form->textField($model,'dateStartRate');
        echo CHtml::image("images/calendar.jpg","calendar1", array("id"=>"c_buttonStart","class"=>"pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
            array(
                'inputField'=>'AbaTaxRates_dateStartRate',
                'button'=>'c_buttonStart',
                'ifFormat'=>'%Y-%m-%d 00:00:00',
            ));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'dateEndRate');
        echo $form->textField($model,'dateEndRate');
        echo CHtml::image("images/calendar.jpg","calendar2", array("id"=>"c_buttonEnd","class"=>"pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
            array(
                'inputField'=>'AbaTaxRates_dateEndRate',
                'button'=>'c_buttonEnd',
                'ifFormat'=>'%Y-%m-%d 00:00:00',
            ));
        ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->