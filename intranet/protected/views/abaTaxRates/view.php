<?php
/* @var $this AbaTaxRatesController */
/* @var $model AbaTaxRates */

$this->breadcrumbs=array(
	'Tax Rates'=>array('admin'),
	$model->idTaxRate,
);

$this->menu=array(
	array('label'=>'List TaxRates', 'url'=>array('index')),
	array('label'=>'Create TaxRates', 'url'=>array('create')),
	array('label'=>'Update TaxRates', 'url'=>array('update', 'id'=>$model->idTaxRate)),
	array('label'=>'Delete TaxRates', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idTaxRate),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TaxRates', 'url'=>array('admin')),
);
?>

<h1>View TaxRates #<?php echo $model->idTaxRate; ?></h1>

<?php $this->widget('application.components.widgets.CDetailViewABA', array(
	'data'=>$model,
	'attributes'=>array(
		'idTaxRate',
        'idCountry'=>
            array(
                'name'=>'idCountry',
                'type'=>'countryUeList',
            ),
		'taxRateValue',
		'dateStartRate',
		'dateEndRate',
	),
)); ?>
