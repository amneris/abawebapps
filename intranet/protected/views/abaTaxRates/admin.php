<?php
/* @var $this AbaTaxRatesController */
/* @var $model AbaTaxRates */

//$this->breadcrumbs=array('Tax Rates'=>array('admin'),'Manage',);
$this->breadcrumbs=array('Tax Rates');

//$this->menu=array(
//	array('label'=>'List TaxRates', 'url'=>array('index')),
//	array('label'=>'Create TaxRates', 'url'=>array('create')),
//);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tax-rates-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Tax Rates</h3>

<?php echo CHtml::link('Create a new tax rate',array('abaTaxRates/create')); echo "<br>"; ?>
<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('application.components.widgets.CGridViewABA', array(
	'id'=>'tax-rates-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
        'prevPageLabel'  => '<',
        'nextPageLabel'  => '>',
        'firstPageLabel' => '<<',
        'lastPageLabel'  => '>>',),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
//    'filterSelector'=>'{filter}',
	'columns'=>array(
		'idTaxRate',
        'idCountry'=>
            array( 'name'=>'idCountry',
                'type'=>'countryUeList',
                'filter'=>HeList::getUeCountryList(),
                'htmlOptions'=>array('width'=>"120px")
            ),
		'taxRateValue',
		'dateStartRate',
		'dateEndRate',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
