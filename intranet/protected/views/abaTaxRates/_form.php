<?php
/* @var $this AbaTaxRatesController */
/* @var $model AbaTaxRates */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tax-rates-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'idCountry'); ?>
        <?php echo $form->DropDownList($model, 'idCountry', HeList::getUeCountryList(), array('empty' => '--Any Country--')); ?>
		<?php echo $form->error($model,'idCountry'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'taxRateValue'); ?>
		<?php echo $form->textField($model,'taxRateValue',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'taxRateValue'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dateStartRate'); ?>
		<?php echo $form->textField($model,'dateStartRate'); ?>
        <?php
        echo CHtml::image("images/calendar.jpg","calendar1", array("id"=>"c_buttonStart","class"=>"pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
            array(
                'inputField'=>'AbaTaxRates_dateStartRate',
                'button'=>'c_buttonStart',
                'ifFormat'=>'%Y-%m-%d 00:00:00',
            ));
        ?>
        <?php echo $form->error($model,'dateStartRate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dateEndRate'); ?>
		<?php echo $form->textField($model,'dateEndRate'); ?>
        <?php
        echo CHtml::image("images/calendar.jpg","calendar2", array("id"=>"c_buttonEnd","class"=>"pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
            array(
                'inputField'=>'AbaTaxRates_dateEndRate',
                'button'=>'c_buttonEnd',
                'ifFormat'=>'%Y-%m-%d 00:00:00',
            ));
        ?>
        <?php echo $form->error($model,'dateEndRate'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->