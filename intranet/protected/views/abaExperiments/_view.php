<?php
/* @var $this AbaExperimentsController */
/* @var $model AbaExperiments */
?>

<div class="view">

    <b/><?php echo CHtml::encode($data->getAttributeLabel('experimentId')); ?>:</b>
    <?php echo CHtml::encode($data->experimentId); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('counterUsers')); ?>:</b>
    <?php echo CHtml::encode($data->counterUsers); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('limitUsers')); ?>:</b>
    <?php echo CHtml::encode($data->limitUsers); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('experimentIdentifier')); ?>:</b>
    <?php echo CHtml::encode($data->experimentIdentifier); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('experimentDescription')); ?>:</b>
    <?php echo CHtml::encode($data->experimentDescription); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('userCategoryId')); ?>:</b>
    <?php echo CHtml::encode($data->userCategoryId); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('experimentTypeId')); ?>:</b>
    <?php echo CHtml::encode($data->experimentTypeId); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('randomMode')); ?>:</b>
    <?php echo CHtml::encode($data->randomMode); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('availableModeId')); ?>:</b>
    <?php echo CHtml::encode($data->availableModeId); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('dateStart')); ?>:</b>
    <?php echo CHtml::encode($data->dateStart); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('dateEnd')); ?>:</b>
    <?php echo CHtml::encode($data->dateEnd); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('languages')); ?>:</b>
    <?php echo CHtml::encode($data->languages); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('experimentStatus')); ?>:</b>
    <?php echo CHtml::encode($data->experimentStatus); ?>
    <br/>

</div>