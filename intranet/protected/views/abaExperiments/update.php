<?php
/* @var $this AbaExperimentsController */
/* @var $model AbaExperiments */

$this->breadcrumbs = array(
  'Aba Experiments' => array('admin'),
  $model->experimentId => array('view', 'id' => $model->experimentId),
  'Update',
);

$this->menu = array(
  array('label' => 'List Experiments', 'url' => array('index')),
  array('label' => 'Create Experiment', 'url' => array('create')),
  array('label' => 'View Experiment', 'url' => array('view', 'id' => $model->experimentId)),
  array('label' => 'Manage Experiments', 'url' => array('admin')),
);
?>

    <h3>Update Experiment: <?php echo $model->experimentId; ?></h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>