<?php
/* @var $this AbaExperimentsController */
/* @var $model AbaExperiments */

$this->breadcrumbs = array(
  'Experiments' => array('admin'),
  $model->experimentId,
);

$this->menu = array(
  array('label' => 'List Experiments', 'url' => array('index')),
  array('label' => 'Create Experiment', 'url' => array('create')),
  array('label' => 'Update Experiment', 'url' => array('update', 'id' => $model->experimentId)),
  array(
    'label' => 'Delete Experiment',
    'url' => '#',
    'linkOptions' => array(
      'submit' => array('delete', 'id' => $model->experimentId),
      'confirm' => 'Are you sure you want to delete this item?'
    )
  ),
  array('label' => 'Manage Experiments', 'url' => array('admin')),
);
?>

<h3>View Experiments <b><?php echo $model->experimentId . ": " . $model->experimentDescription; ?></b></h3>

<?php $this->widget(
  'application.components.widgets.CDetailViewABA',
  array(
    'data' => $model,
    'attributes' => array(
      'experimentId',
      'userCategoryId' =>
        array(
          'name' => 'userCategoryId',
          'type' => 'experimentsCategoriesList',
          'filter' => HeList::experimentsCategoriesList()
        ),
      'experimentTypeId' =>
        array(
          'name' => 'experimentTypeId',
          'type' => 'experimentsTypesList',
          'filter' => HeList::experimentsTypesList()
        ),
      'experimentIdentifier',
      'experimentDescription',
      'counterUsers',
      'limitUsers',
      'randomMode' =>
        array(
          'name' => 'randomMode',
          'type' => 'experimentsRandomModeList',
          'filter' => HeList::experimentsRandomModeList()
        ),
      'availableModeId' =>
        array(
          'name' => 'availableModeId',
          'type' => 'experimentsAvailableModeList',
          'filter' => HeList::experimentsAvailableModeList()
        ),
      'dateStart',
      'dateEnd',
      'languages',
      'experimentStatus' =>
        array(
          'name' => 'experimentStatus',
          'type' => 'experimentsStatusList',
          'filter' => HeList::experimentsStatusList()
        ),
    ),
  )
); ?>
