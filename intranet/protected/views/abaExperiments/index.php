<?php
/* @var $this AbaExperimentsController */
/* @var $model AbaExperiments */

$this->breadcrumbs = array(
  'Aba Experiments',
);

$this->menu = array(
  array('label' => 'Create Experiments', 'url' => array('create')),
  array('label' => 'Manage Experiments', 'url' => array('admin')),
);
?>

<h3>Experiments</h3>

<?php $this->widget(
  'zii.widgets.CListView',
  array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
  )
); ?>
