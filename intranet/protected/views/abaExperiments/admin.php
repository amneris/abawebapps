<?php
/* @var $this AbaExperimentsController */
/* @var $model AbaExperiments */

$this->breadcrumbs = array(
  'Experiments',
);

$this->menu = array(
  array('label' => 'Create Experiment', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript(
  'search',
  "
   $('.search-button').click(function(){
     $('.search-form').toggle();
     return false;
   });
   $('.search-form form').submit(function(){
     $.fn.yiiGridView.update('aba-experiments-grid', {
       data: $(this).serialize()
     });
     return false;
   });
   "
);
?>

<h3>Experiments</h3>

<?php echo CHtml::link('Create new experiment', array('abaExperiments/create')); ?>
<br>
<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial(
      '_search',
      array(
        'model' => $model,
      )
    ); ?>
</div><!-- search-form -->

<?php $this->widget(
  'application.components.widgets.CGridViewABA',
  array(
    'id' => 'aba-experiments-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager' => array(
      'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
      'prevPageLabel' => '<',
      'nextPageLabel' => '>',
      'firstPageLabel' => '<<',
      'lastPageLabel' => '>>',
    ),
    'extraInfo' => array('view', 'update'),
    'dataProvider' => $model->search(),
    'filter' => $model,
    'filterSelector' => '{filter}',
    'columns' => array(
      'experimentId',
      'userCategoryId' => array(
        'name' => 'userCategoryId',
        'type' => 'experimentsCategoriesList',
        'filter' => HeList::experimentsCategoriesList()
      ),
      'experimentTypeId' => array(
        'name' => 'experimentTypeId',
        'type' => 'experimentsTypesList',
        'filter' => HeList::experimentsTypesList()
      ),
      'experimentIdentifier',
      'experimentDescription',
      'counterUsers',
      'limitUsers',
      'randomMode' => array(
        'name' => 'randomMode',
        'type' => 'experimentsRandomModeList',
        'filter' => HeList::experimentsRandomModeList()
      ),
      'availableModeId' => array(
        'name' => 'availableModeId',
        'type' => 'experimentsAvailableModeList',
        'filter' => HeList::experimentsAvailableModeList()
      ),
      'dateStart',
      'dateEnd',
      'languages',
      'experimentStatus' => array(
        'name' => 'experimentStatus',
        'type' => 'experimentsStatusList',
        'filter' => HeList::experimentsStatusList()
      ),
      array(
        'class' => 'CButtonColumnABA',
        'header' => 'Options',
      ),
    ),
  )
); ?>
