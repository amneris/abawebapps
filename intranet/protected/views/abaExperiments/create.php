<?php
/* @var $this AbaExperimentsController */
/* @var $model AbaExperiments */

$this->breadcrumbs = array(
  'Experiments' => array('admin'),
  'Create',
);

$this->menu = array(
  array('label' => 'Manage Experiments', 'url' => array('admin')),
);
?>

    <h3>Experiments</h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>