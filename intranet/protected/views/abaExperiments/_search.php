<?php
/* @var $this AbaExperimentsController */
/* @var $model AbaExperiments */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
      'CActiveForm',
      array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
      )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'experimentId'); ?>
        <?php echo $form->textField($model, 'experimentId', array('size' => 5, 'maxlength' => 20)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'userCategoryId'); ?>
        <?php echo $form->DropDownList($model, 'userCategoryId', HeList::experimentsCategoriesList(), array('empty' => '--')); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'experimentTypeId'); ?>
        <?php echo $form->DropDownList($model, 'experimentTypeId', HeList::experimentsTypesList(), array('empty' => '--')); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'experimentIdentifier'); ?>
        <?php echo $form->textField($model, 'experimentIdentifier', array('size' => 30, 'maxlength' => 32)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'experimentDescription'); ?>
        <?php echo $form->textField($model, 'experimentDescription', array('size' => 50, 'maxlength' => 255)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'counterUsers'); ?>
        <?php echo $form->textField($model, 'counterUsers', array('size' => 15, 'maxlength' => 32)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'limitUsers'); ?>
        <?php echo $form->textField($model, 'limitUsers', array('size' => 15, 'maxlength' => 32)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'randomMode'); ?>
        <?php echo $form->DropDownList($model, 'randomMode', HeList::experimentsRandomModeList(), array('empty' => '--')); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'availableModeId'); ?>
        <?php echo $form->DropDownList($model, 'availableModeId', HeList::experimentsAvailableModeList(), array('empty' => '--')); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'dateStart'); ?>
        <?php echo $form->textField($model, 'dateStart', array('size' => 15, 'maxlength' => 19)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'dateEnd'); ?>
        <?php echo $form->textField($model, 'dateEnd', array('size' => 15, 'maxlength' => 19)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'languages'); ?>
        <?php echo $form->textField($model, 'languages', array('size' => 50, 'maxlength' => 225)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'experimentStatus'); ?>
        <?php echo $form->DropDownList($model, 'experimentStatus', HeList::experimentsStatusList(), array('empty' => '--')); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->