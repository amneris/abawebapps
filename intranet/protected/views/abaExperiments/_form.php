<?php
/* @var $this AbaExperimentsController */
/* @var $model AbaExperiments */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget(
      'CActiveForm',
      array(
        'id' => 'aba-experiments-form',
        'enableAjaxValidation' => false,
      )
    );

    $options = HeList::booleanList();
    $optionsStatus = HeList::experimentsStatusList();
    $optionsRandom = HeList::experimentsRandomModeList();
    $optionsCategory = HeList::experimentsCategoriesList();
    $optionsType = HeList::experimentsTypesList();
    $optionsAvailable = HeList::experimentsAvailableModeList();

    $cssPrefix = (isset($wAction) AND $wAction == 'update' ? 'Cm' : '');
?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <table id="myForm">
        <?php if (is_numeric($model->experimentId)): ?>
            <tr>
                <td>
                    <?php echo $form->labelEx($model, 'experimentId'); ?>
                </td>
                <td>
                    <?php echo $model->experimentId; ?>
                    <?php echo $form->error($model, 'experimentId'); ?>
                </td>
            </tr>
        <?php endif; ?>
        <?php if (is_numeric($model->experimentId)): ?>
            <tr>
                <td>
                    <?php echo $form->labelEx($model, 'counterUsers'); ?>
                </td>
                <td>
                    <?php echo $model->counterUsers; ?>
                    <?php echo $form->error($model, 'counterUsers'); ?>
                </td>
            </tr>
        <?php else: ?>
            <tr>
                <td>
                    <?php echo $form->labelEx($model, 'counterUsers'); ?>
                </td>
                <td>
                    <?php echo $form->textField($model, 'counterUsers', array('size' => 15, 'maxlength' => 32)); ?>
                    <?php echo $form->error($model, 'counterUsers'); ?>
                </td>
            </tr>
        <?php endif; ?>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'limitUsers'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'limitUsers', array('size' => 15, 'maxlength' => 32)); ?>
                <?php echo $form->error($model, 'limitUsers'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'experimentIdentifier'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'experimentIdentifier', array('size' => 30, 'maxlength' => 32)); ?>
                <?php echo $form->error($model, 'experimentIdentifier'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'experimentDescription'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'experimentDescription',
                  array('size' => 50, 'maxlength' => 150)); ?>
                <?php echo $form->error($model, 'experimentDescription'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'userCategoryId'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'userCategoryId', $optionsCategory); ?>
                <?php echo $form->error($model, 'userCategoryId'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'experimentTypeId'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'experimentTypeId', $optionsType); ?>
                <?php echo $form->error($model, 'experimentTypeId'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'randomMode'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'randomMode', $optionsRandom); ?>
                <?php echo $form->error($model, 'randomMode'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'availableModeId'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'availableModeId', $optionsAvailable); ?>
                <?php echo $form->error($model, 'availableModeId'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'dateStart'); ?>
            </td>
            <td>
                <?php
                echo $form->textField($model, 'dateStart', array('size' => 22, 'maxlength' => 19));
                echo CHtml::image(
                  "images/calendar.jpg",
                  "calendar0",
                  array("id" => "c_dateStart", "class" => "pointer")
                );
                $this->widget(
                  'application.extensions.calendar.SCalendar',
                  array(
                    'inputField' => $cssPrefix . 'AbaExperiments_dateStart',
                    'button' => 'c_dateStart',
                    'ifFormat' => '%Y-%m-%d %H:%M:%S',
                  )
                );
                ?>
                <?php echo $form->error($model, 'dateStart'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'dateEnd'); ?>
            </td>
            <td>
                <?php
                echo $form->textField($model, 'dateEnd', array('size' => 22, 'maxlength' => 19));
                echo CHtml::image(
                  "images/calendar.jpg",
                  "calendar1",
                  array("id" => "c_dateEnd", "class" => "pointer")
                );
                $this->widget(
                  'application.extensions.calendar.SCalendar',
                  array(
                    'inputField' => $cssPrefix . 'AbaExperiments_dateEnd',
                    'button' => 'c_dateEnd',
                    'ifFormat' => '%Y-%m-%d %H:%M:%S',
                  )
                );
                ?>
                <?php echo $form->error($model, 'dateEnd'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'languages'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'languages',
                  array('size' => 50, 'maxlength' => 150)); ?>
                <?php echo $form->error($model, 'languages'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'experimentStatus'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'experimentStatus', $optionsStatus); ?>
                <?php echo $form->error($model, 'experimentStatus'); ?>
            </td>
        </tr>
    </table>
    <div class="row buttons" align="center">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->