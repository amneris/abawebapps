<?php
/* @var $this AbaPartnerchannelsController */
/* @var $model AbaPartnerchannels */
?>

<div class="view">

    <b/><?php echo CHtml::encode($data->getAttributeLabel('idChannel')); ?>:</b>
    <?php echo CHtml::encode($data->idChannel); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('nameChannel')); ?>:</b>
    <?php echo CHtml::encode($data->nameChannel); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('dateAdd')); ?>:</b>
    <?php echo CHtml::encode($data->dateAdd); ?>
    <br/>

</div>