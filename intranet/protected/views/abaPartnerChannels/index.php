<?php
/* @var $this AbaPartnerchannelsController */
/* @var $model AbaPartnerchannels */

$this->breadcrumbs = array(
  'Aba Partner channels',
);

$this->menu = array(
  array('label' => 'Create Partner channels', 'url' => array('create')),
  array('label' => 'Manage Partner channels', 'url' => array('admin')),
);
?>

<h3>Partner channels</h3>

<?php $this->widget(
  'zii.widgets.CListView',
  array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
  )
); ?>
