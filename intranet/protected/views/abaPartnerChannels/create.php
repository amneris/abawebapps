<?php
/* @var $this AbaPartnerchannelsController */
/* @var $model AbaPartnerchannels */

$this->breadcrumbs = array(
  'Partner channels' => array('admin'),
  'Create',
);

$this->menu = array(
  array('label' => 'Manage Partner channel', 'url' => array('admin')),
);
?>

    <h3>Partner channels</h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>