<?php
/* @var $this AbaPartnerchannelsController */
/* @var $model AbaPartnerchannels */

$this->breadcrumbs = array(
  'Partner channels' => array('admin'),
  $model->idChannel,
);

$this->menu = array(
  array('label' => 'List Partner channels', 'url' => array('index')),
  array('label' => 'Create Partner channel', 'url' => array('create')),
  array('label' => 'Update Partner channel', 'url' => array('update', 'id' => $model->idChannel)),
  array(
    'label' => 'Delete Partner channel',
    'url' => '#',
    'linkOptions' => array(
      'submit' => array('delete', 'id' => $model->idChannel),
      'confirm' => 'Are you sure you want to delete this item?'
    )
  ),
  array('label' => 'Manage Partner channels', 'url' => array('admin')),
);
?>

<h3>View Partner channels <b><?php echo $model->idChannel . ": " . $model->nameChannel; ?></b></h3>

<?php $this->widget(
  'application.components.widgets.CDetailViewABA',
  array(
    'data' => $model,
    'attributes' => array(
      'idChannel',
      'nameChannel',
      'dateAdd',
    ),
  )
); ?>
