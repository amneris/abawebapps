<?php
/* @var $this AbaPartnerchannelsController */
/* @var $model AbaPartnerchannels */

$this->breadcrumbs = array(
  'Aba Partner channels' => array('admin'),
  $model->idChannel => array('view', 'id' => $model->idChannel),
  'Update',
);

$this->menu = array(
  array('label' => 'List Partner channels', 'url' => array('index')),
  array('label' => 'Create Partner channel', 'url' => array('create')),
  array('label' => 'View Partner channels', 'url' => array('view', 'id' => $model->idChannel)),
  array('label' => 'Manage Partner channels', 'url' => array('admin')),
);
?>

    <h3>Update Partner channel: <?php echo $model->idChannel; ?></h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>