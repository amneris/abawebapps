<?php
/* @var $this AbaProductsPromosController */
/* @var $model AbaProductsPromos */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aba-products-promos-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <table id="myForm">


        <tr>
            <td>
		<?php echo $form->labelEx($model,'idPromoCode'); ?>
            </td>
            <td>
		<?php echo $form->textField($model,'idPromoCode',array('size'=>20,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'idPromoCode'); ?>
            </td>
        </tr>

<tr>
    <td>
		<?php echo $form->labelEx($model,'idProduct'); ?>
    </td>
    <td>
        <?php echo $form->dropDownList($model,'idProduct', $productList, array('empty'=>'')); ?>
		<?php echo $form->error($model,'idProduct'); ?>
    </td>
</tr>

<tr>
    <td>
        <?php echo $form->labelEx($model,'idPeriodPay'); ?>
    </td>
    <td>
        <?php echo $form->dropDownList($model,'idPeriodPay', $periodList, array('empty'=>'')); ?>
        <?php echo $form->error($model,'idPeriodPay'); ?>

    </td>
</tr>
<tr>
    <td>
		<?php echo $form->labelEx($model,'descriptionText'); ?>
    </td>
    <td>
		<?php echo $form->textField($model,'descriptionText',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'descriptionText'); ?>
    </td>
</tr>

<tr>
    <td>
		<?php echo $form->labelEx($model,'type'); ?>
    </td>
    <td>
        <?php
        echo $form->dropDownList($model, 'type', CHtml::listData(AbaProductsPromos::getTypes(), 'id', 'title'));
        ?>
		<?php echo $form->error($model,'type'); ?>
    </td>
</tr>

<tr>
    <td>
		<?php echo $form->labelEx($model,'value'); ?>
    </td>
    <td>
		<?php echo $form->textField($model,'value',array('size'=>20,'maxlength'=>18)); ?>
		<?php echo $form->error($model,'value'); ?>
    </td>
</tr>

<tr>
    <td>
		<?php echo $form->labelEx($model,'dateStart'); ?>
    </td>
    <td>
        <?php
        echo $form->textField($model,'dateStart',array('size'=>7,'maxlength'=>18));
        echo $form->error($model,'dateStart');
        echo CHtml::image("images/calendar.jpg","calendar0", array("id"=>"c_dateStart","class"=>"pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
            array(
                'inputField'=>'AbaProductsPromos_dateStart',
                'button'=>'c_dateStart',
                'ifFormat'=>'%Y-%m-%d',
            ));
        ?>
		<?php echo $form->error($model,'dateStart'); ?>
    </td>
</tr>

<tr>
    <td>
		<?php echo $form->labelEx($model,'dateEnd'); ?>
    </td>
    <td>
        <?php
        echo $form->textField($model,'dateEnd',array('size'=>7,'maxlength'=>18));
        echo $form->error($model,'dateEnd');
        echo CHtml::image("images/calendar.jpg","calendar0", array("id"=>"c_dateEnd","class"=>"pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
            array(
                'inputField'=>'AbaProductsPromos_dateEnd',
                'button'=>'c_dateEnd',
                'ifFormat'=>'%Y-%m-%d',
            ));
        ?>
		<?php echo $form->error($model,'dateEnd'); ?>
    </td>
</tr>

<tr>
    <td>
		<?php echo $form->labelEx($model,'enabled'); ?>
    </td>
    <td>
        <?php
        $options = array(1=>'Yes', 0=>'No');
        echo $form->dropDownList($model, 'enabled', $options);
        ?>
        <?php echo $form->error($model,'enabled'); ?>
    </td>
</tr>

<tr>
    <td>
		<?php echo $form->labelEx($model,'visibleWeb'); ?>
    </td>
    <td>
        <?php
        $options = array(1=>'Yes', 0=>'No');
        echo $form->dropDownList($model, 'visibleWeb', $options);
        ?>
		<?php echo $form->error($model,'visibleWeb'); ?>
    </td>
</tr>

<tr>
    <td>
        <?php echo $form->labelEx($model,'showValidationType'); ?>
    </td>
    <td>
        <?php echo $form->dropDownList($model, 'showValidationType', CHtml::listData(AbaProductsPromos::getValidationTypes(), 'id', 'title')); ?>
        <?php echo $form->error($model,'showValidationType'); ?>
    </td>
</tr>
<tr>
    <td>
        <?php echo $form->labelEx($model,'idDescription'); ?>
    </td>
    <td>
        <?php echo $form->dropDownList($model, 'idDescription', $stDescriptionTypes, array('empty' => '')); ?>
        <?php echo $form->error($model,'idDescription'); ?>
    </td>
</tr>
<tr>
    <td>
        <?php echo $form->labelEx($model,'idPromoType'); ?>
    </td>
    <td>
        <?php echo $form->dropDownList($model, 'idPromoType', CHtml::listData(AbaProductsPromos::getPromoTypes(), 'id', 'title')); ?>
        <?php echo $form->error($model,'idPromoType'); ?>
    </td>
</tr>
    </table>
	<div class="row buttons" align="center">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->