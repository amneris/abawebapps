<?php
/* @var $this AbaProductsPromosController */
/* @var $data AbaProductsPromos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPromoCode')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idPromoCode), array('view', 'id'=>$data->idPromoCode)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idProduct')); ?>:</b>
	<?php echo CHtml::encode($data->idProduct); ?>
	<br />

	<b><?php /*echo CHtml::encode($data->getAttributeLabel('idPartner'));*/ ?>:</b>
	<?php /*echo CHtml::encode($data->idPartner);*/ ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descriptionText')); ?>:</b>
	<?php echo CHtml::encode($data->descriptionText); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('value')); ?>:</b>
	<?php echo CHtml::encode($data->value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dateStart')); ?>:</b>
	<?php echo CHtml::encode($data->dateStart); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('dateEnd')); ?>:</b>
	<?php echo CHtml::encode($data->dateEnd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('enabled')); ?>:</b>
	<?php echo CHtml::encode($data->enabled); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('visibleWeb')); ?>:</b>
	<?php echo CHtml::encode($data->visibleWeb); ?>
	<br />

	*/ ?>

</div>