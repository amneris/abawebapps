<?php
/* @var $this AbaProductsPromosController */
/* @var $model AbaProductsPromos */

$this->breadcrumbs = array(
    'Promotion Codes' => array('admin'),
    $model->idPromoCode,
);

$this->menu = array(
    array('label' => 'List AbaProductsPromos', 'url' => array('index')),
    array('label' => 'Create AbaProductsPromos', 'url' => array('create')),
    array('label' => 'Update AbaProductsPromos', 'url' => array('update', 'id' => $model->idPromoCode)),
    array(
        'label' => 'Delete AbaProductsPromos',
        'url' => '#',
        'linkOptions' => array(
            'submit' => array('delete', 'id' => $model->idPromoCode),
            'confirm' => 'Are you sure you want to delete this item?'
        )
    ),
    array('label' => 'Manage AbaProductsPromos', 'url' => array('admin')),
);
?>

<h3>View promotion code <b><?php echo $model->idPromoCode; ?></b></h3>

<?php $this->widget(
    'application.components.widgets.CDetailViewABA',
    array(
        'data' => $model,
        'attributes' => array(
            'idPromoCode',
            'idProduct',
            'idPeriodPay',
            'descriptionText',
            'type',
            'value',
            'dateStart',
            'dateEnd',
            'enabled',
            'visibleWeb',
            'showValidationType',
            'idDescription' => array(
                'name' => 'idDescription',
                'type' => 'promosDescriptionList',
                'filter' => HeList::getPromosDescriptionList()
            ),
            'idPromoType' => array(
                'name' => 'idPromoType',
                'type' => 'getPromoTypes',
                'filter' => HeList::getPromoTypes()
            ),
        ),
    )
); ?>

