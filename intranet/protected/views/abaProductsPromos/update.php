<?php
/* @var $this AbaProductsPromosController */
/* @var $model AbaProductsPromos */

$this->breadcrumbs=array(
	'Aba Products Promoses'=>array('admin'),
	$model->idPromoCode=>array('view','id'=>$model->idPromoCode),
	'Update',
);

$this->menu=array(
	array('label'=>'List AbaProductsPromos', 'url'=>array('index')),
	array('label'=>'Create AbaProductsPromos', 'url'=>array('create')),
	array('label'=>'View AbaProductsPromos', 'url'=>array('view', 'id'=>$model->idPromoCode)),
	array('label'=>'Manage AbaProductsPromos', 'url'=>array('admin')),
);
?>

<h3>Update Promotion Code: <?php echo $model->idPromoCode; ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'periodList'=>$periodList, 'productList'=>$productList, 'stDescriptionTypes' => $stDescriptionTypes)); ?>