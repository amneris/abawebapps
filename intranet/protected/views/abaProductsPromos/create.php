<?php
/* @var $this AbaProductsPromosController */
/* @var $model AbaProductsPromos */

$this->breadcrumbs=array(
	'Products Promotions'=>array('admin'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List AbaProductsPromos', 'url'=>array('index')),
	array('label'=>'Manage Products Promotions', 'url'=>array('admin')),
);
?>

<h3>Promotion Codes</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'periodList'=>$periodList, 'productList'=>$productList, 'stDescriptionTypes' => $stDescriptionTypes)); ?>