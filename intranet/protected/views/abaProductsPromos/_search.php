<?php
/* @var $this AbaProductsPromosController */
/* @var $model AbaProductsPromos */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idPromoCode'); ?>
		<?php echo $form->textField($model,'idPromoCode',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idProduct'); ?>
		<?php echo $form->textField($model,'idProduct',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php /* echo $form->label($model,'idPartner'); 
		 	 echo $form->DropDownList($model,'idPartner',HeList::getPartnerList(), array('empty'=>'--Any Partner--')); */
		 ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'descriptionText'); ?>
		<?php echo $form->textField($model,'descriptionText',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'type'); 
			echo $form->DropDownList($model,'type',HeList::typePromoList(), array('empty'=>'--Any Type--'));
		?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'value'); ?>
		<?php echo $form->textField($model,'value',array('size'=>18,'maxlength'=>18)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dateStart'); 
		 echo $form->textField($model,'dateStart');
		 echo CHtml::image("images/calendar.jpg","calendar1", array("id"=>"c_buttonStart","class"=>"pointer")); 
			$this->widget('application.extensions.calendar.SCalendar',
				array(
				'inputField'=>'AbaProductsPromos_dateStart',
				'button'=>'c_buttonStart',
				'ifFormat'=>'%Y-%m-%d 00:00:00',
			));		 
		 ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dateEnd'); 
			echo $form->textField($model,'dateEnd'); 
			echo CHtml::image("images/calendar.jpg","calendar2", array("id"=>"c_buttonEnd","class"=>"pointer")); 
			$this->widget('application.extensions.calendar.SCalendar',
				array(
				'inputField'=>'AbaProductsPromos_dateEnd',
				'button'=>'c_buttonEnd',
				'ifFormat'=>'%Y-%m-%d 00:00:00',
			));	
		?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'enabled'); 
		echo $form->DropDownList($model,'enabled',HeList::booleanList(), array('empty'=>'--Any--'));
//		 echo $form->DropDownList($model,'type',HeList::typePromoList(), array('empty'=>'--Any Type--'));

		//echo $form->textField($model,'enabled'); 
	?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'visibleWeb'); 
		 echo $form->DropDownList($model,'visibleWeb',HeList::booleanList(), array('empty'=>'--Any--'));
		 ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->