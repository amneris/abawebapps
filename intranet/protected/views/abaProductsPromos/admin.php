<?php
/* @var $this AbaProductsPromosController */
/* @var $model AbaProductsPromos */

$this->breadcrumbs = array(
    'Promotion Codes',
);

$this->menu = array(
//	array('label'=>'List AbaProductsPromos', 'url'=>array('index')),
    array('label' => 'Create Products Promotions', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript(
    'search',
    "
   $('.search-button').click(function(){
     $('.search-form').toggle();
     return false;
   });
   $('.search-form form').submit(function(){
     $.fn.yiiGridView.update('aba-products-promos-grid', {
       data: $(this).serialize()
     });
     return false;
   });
   "
);
?>

<h3>Promotion codes</h3>

<?php echo CHtml::link('Create new promotion code', array('AbaProductsPromos/create')); ?>
<br>
<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial(
        '_search',
        array(
            'model' => $model,
        )
    ); ?>
</div><!-- search-form -->

<?php $this->widget(
    'application.components.widgets.CGridViewABA',
    array(
        'id' => 'aba-products-promos-grid',
        'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
        'pager' => array(
            'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
            'prevPageLabel' => '<',
            'nextPageLabel' => '>',
            'firstPageLabel' => '<<',
            'lastPageLabel' => '>>',
        ),
        'extraInfo' => array('view', 'update', 'delete'),
        'dataProvider' => $model->search(),
        'filter' => $model,
        'filterSelector' => '{filter}',
        'columns' => array(
            'idPromoCode',
            'descriptionText',
            array(
                'name' => 'type',
                'header' => "Type",
                'value' => 'AbaProductsPromos::getType($data->type)',
                'filter' => CHtml::listData(AbaProductsPromos::getTypes(), 'id', 'title'),
            ),
            'value',
            'dateStart',
            'dateEnd',
            'enabled' => array(
                'name' => 'enabled',
                'type' => 'booleanList',
                'filter' => HeList::booleanList()
            ),
            'visibleWeb' => array(
                'name' => 'visibleWeb',
                'type' => 'booleanList',
                'filter' => HeList::booleanList()
            ),
            array(
                'name' => 'showValidationType',
                'header' => "Show validation type",
                'value' => 'AbaProductsPromos::getValidationType($data->showValidationType)',
                'filter' => CHtml::listData(AbaProductsPromos::getValidationTypes(), 'id', 'title'),
            ),
            array(
                'name' => 'idDescription',
                'header' => "Description",
                'filter' => CHtml::listData(HeList::getPromosDescriptionList(true), 'id', 'title'),
            ),
            array(
                'name' => 'idPromoType',
                'header' => "Promo type",
                'value' => 'AbaProductsPromos::getPromoType($data->idPromoType)',
                'filter' => CHtml::listData(AbaProductsPromos::getPromoTypes(), 'id', 'title'),
            ),
            array(
                'class' => 'CButtonColumnABA',
                'header' => 'Options',
            ),
        ),
    )
); ?>
