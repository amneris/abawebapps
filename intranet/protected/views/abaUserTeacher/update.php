<?php
/* @var $this AbaUserTeacherController */
/* @var $model AbaUserTeacher */

$this->breadcrumbs=array(
	'Aba User Teachers'=>array('index'),
	$model->userid=>array('view','id'=>$model->userid),
	'Update',
);

$this->menu=array(
	array('label'=>'List AbaUserTeacher', 'url'=>array('index')),
	array('label'=>'Create AbaUserTeacher', 'url'=>array('create')),
	array('label'=>'View AbaUserTeacher', 'url'=>array('view', 'id'=>$model->userid)),
	array('label'=>'Manage AbaUserTeacher', 'url'=>array('admin')),
);
?>

<h1>Update AbaUserTeacher <?php echo $model->userid; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>