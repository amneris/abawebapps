<?php
/* @var $this AbaUserTeacherController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Aba User Teachers',
);

$this->menu=array(
	array('label'=>'Create AbaUserTeacher', 'url'=>array('create')),
	array('label'=>'Manage AbaUserTeacher', 'url'=>array('admin')),
);
?>

<h1>Aba User Teachers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
