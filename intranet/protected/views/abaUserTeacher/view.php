<?php
/* @var $this AbaUserTeacherController */
/* @var $model AbaUserTeacher */

$this->breadcrumbs=array(
	'Aba User Teachers'=>array('index'),
	$model->userid,
);

$this->menu=array(
	array('label'=>'List AbaUserTeacher', 'url'=>array('index')),
	array('label'=>'Create AbaUserTeacher', 'url'=>array('create')),
	array('label'=>'Update AbaUserTeacher', 'url'=>array('update', 'id'=>$model->userid)),
	array('label'=>'Delete AbaUserTeacher', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->userid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AbaUserTeacher', 'url'=>array('admin')),
);
?>

<h1>View AbaUserTeacher #<?php echo $model->userid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'userid',
		'nationality',
		'language',
		'photo',
	),
)); ?>
