<?php
/* @var $this AbaUserTeacherController */
/* @var $model AbaUserTeacher */

$this->breadcrumbs=array(
	'Aba User Teachers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AbaUserTeacher', 'url'=>array('index')),
	array('label'=>'Manage AbaUserTeacher', 'url'=>array('admin')),
);
?>

<h1>Create AbaUserTeacher</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>