<?php /* @var $this Controller */

$fraceCelebre = array(
  1 => "Great spirits have always encountered violent opposition from mediocre minds.",
  2 => "I have never met a man so ignorant that I couldn't learn something from him.",
  3 => "Do what you can, with what you have, where you are.",
  4 => "Stay hungry stay foolish.",
  5 => "I have found the paradox, that if you love until it hurts, there can be no more hurt, only more love.",
  6 => "Every saint has a past and every sinner has a future.",
  7 => "Happiness is not something ready made. It comes from your own actions.",
  8 => "The mind is everything. What you think you become.",
  9 => "Your conscience is the measure of the honesty of your selfishness. Listen to it carefully.",
  10 => "Everything that irritates us about others can lead us to an understanding of ourselves.",
  11 => "Nothing is impossible, the word itself says 'I'm possible'",
  12 => "I can't change the direction of the wind, but I can adjust my sails to always reach my destination.",
  13 => "Experience is simply the name we give our mistakes."
);

$autor = array(
  1 => "Albert Einstein",
  2 => "Galileo Galilei",
  3 => "Theodore Roosevelt",
  4 => "Steve Jobs",
  5 => "Mother Teresa",
  6 => "Oscar Wilde",
  7 => "Dalai Lama",
  8 => "Buddha",
  9 => "Richard Bach",
  10 => "Carl Jung",
  11 => "Audrey Hepburn",
  12 => "Jimmy Dean ",
  13 => "Oscar Wilde"
);


$arMenus = array(
  array(
    'label' => 'Users',
    'roles' => array('root', 'support', 'marketing', 'teacher'),
    'subMenus' => array(

      array(
        'label' => 'Users',
        'roles' => array('root', 'support', 'marketing', 'teacher'),
        'subMenus' => '/abaUser/admin',
      ),
      array(
        'label' => 'Bank Details',
        'roles' => array('root', 'support'),
        'subMenus' => '/abaUserCreditForms/admin',
      ),
      array(
        'label' => 'Level Certificates',
        'roles' => array('root', 'support', 'marketing'),
        'subMenus' => '/abaUser/expresscertificates',
      ),
    )
  ),
  array(
    'label' => 'Payments',
    'roles' => array('root', 'support', 'marketing'),
    'subMenus' => array(

      array(
        'label' => 'Paylines',
        'roles' => array('root', 'support', 'marketing'),
        'subMenus' => '/abaPayments/admin',
      ),
      array(
        'label' => 'Paylines log',
        'roles' => array('root', 'support', 'marketing'),
        'subMenus' => '/abaPaymentsControlCheck/admin',
      ),
      array(
        'label' => 'Boletos',
        'roles' => array('root', 'support'),
        'subMenus' => '/abaPaymentsBoleto/admin',
      ),
      array(
        'label' => 'Reconciliation',
        'roles' => array('root', 'support'),
        'subMenus' => '/reconciliationParser/reconciliation/importfile',
      ),
      array(
        'label' => 'Logs files reconciliation',
        'roles' => array('root', 'support'),
        'subMenus' => '/reconciliationParser/LogReconciliation/admin',
      ),
      array(
        'label' => 'Tax rates',
        'roles' => array('root', 'support'),
        'subMenus' => '/abaTaxRates/admin',
      ),
      array(
        'label' => 'Tax commissions',
        'roles' => array('root', 'support'),
        'subMenus' => '/abaTaxCommissions/admin',
      ),
      array(
        'label' => 'Adyen Frauds',
        'roles' => array('root', 'support'),
        'subMenus' => '/abaAdyenFrauds/admin',
      ),
      array(
        'label' => 'Check CC Number',
        'roles' => array('root', 'support'),
        'subMenus' => '/abaPayments/validatecreditcard',
      ),

    )
  ),
  array(
    'label' => 'Teachers',
    'roles' => array('root', 'marketing', 'teacher'),
    'subMenus' => array(

      array(
        'label' => 'Messages',
        'roles' => array('root', 'marketing', 'teacher'),
        'subMenus' => '/abaQuestionsAndAnswers/admin',
      ),
      array(
        'label' => 'Sent & Answered',
        'roles' => array('root', 'marketing', 'teacher'),
        'subMenus' => '/abaQuestionsAndAnswers/getMessageSentAndAnswered',
      ),
      array(
        'label' => 'Total Sent & Answered',
        'roles' => array('root', 'marketing', 'teacher'),
        'subMenus' => '/abaQuestionsAndAnswers/getTotalMessageSentAndAnswered',
      ),

    )
  ),
  array(
    'label' => 'Flash Sales',
    'roles' => array('root', 'support', 'marketing'),
    'subMenus' => array(

      array(
        'label' => 'Coupons',
        'roles' => array('root', 'support', 'marketing'),
        'subMenus' => '/AbaVentasFlash/index',
      ),
      array(
        'label' => 'Grouper Sales',
        'roles' => array('root', 'marketing'),
        'subMenus' => '/AbaVentasFlash/salesForAgrupador',
      ),
      array(
        'label' => 'Groupers',
        'roles' => array('root'),
        'subMenus' => '/AbaAgrupadores/index',
      ),
      array(
        'label' => 'Load coupons from a file',
        'roles' => array('root', 'marketing'),
        'subMenus' => '/AbaVentasFlash/createFromFile',
      ),

    )
  ),
  array(
    'label' => 'Course',
    'roles' => array('root', 'support', 'marketing', 'teacher'),
    'subMenus' => array(

      array(
        'label' => 'Units Followup',
        'roles' => array('root'),
        'subMenus' => '/abaFollowup4/admin',
      ),
      array(
        'label' => 'Lines Exercises',
        'roles' => array('root'),
        'subMenus' => '/abaFollowupHtml4/admin',
      ),
      array(
        'label' => 'B2B Study Report',
        'roles' => array('root', 'support', 'marketing', 'teacher'),
        'subMenus' => '/b2bFollowup/admin',
      ),
      array(
        'label' => 'B2C Study Report',
        'roles' => array('root', 'support', 'marketing', 'teacher'),
        'subMenus' => '/b2cFollowup/admin',
      ),

    )
  ),
  array(
    'label' => 'Products',
    'roles' => array('root', 'support', 'marketing'),
    'subMenus' => array(
      array(
        'label' => 'Product List & Prices',
        'roles' => array('root', 'support', 'marketing'),
        'subMenus' => '/abaProductsPrices/admin',
      ),
      array(
        'label' => 'AppStore Product List & Prices',
        'roles' => array('root', 'support', 'marketing'),
        'subMenus' => '/abaProductsPricesAppstore/admin',
      ),
      array(
        'label' => 'Adyen Prices & Methods',
        'roles' => array('root', 'support', 'marketing'),
        'subMenus' => array(
          array(
            'label' => 'Adyen Hpp Product List & Prices',
            'roles' => array('root', 'support', 'marketing'),
            'subMenus' => '/abaProductsPricesAdyenHpp/admin',
          ),
          array(
            'label' => 'Adyen Hpp Payment Methods',
            'roles' => array('root', 'support', 'marketing'),
            'subMenus' => '/AbaPaymentMethodsAdyenHpp/admin',
          ),
        )
      ),
      array(
        'label' => 'Products Periodicity',
        'roles' => array('root', 'support', 'marketing'),
        'subMenus' => '/abaProdPeriodicity/admin',
      ),
    )
  ),
  array(
    'id' => 'B2B',
    'label' => 'B2B',
    'roles' => array('root', 'support', 'marketing'),
    'subMenus' => array(

      array(
        'label' => 'B2B Channels List',
        'roles' => array('root', 'support', 'marketing'),
        'subMenus' => '/abaChannelList/admin',
      ),
      array(
        'label' => 'Enterprise Channels',
        'roles' => array('root', 'support', 'marketing'),
        'subMenus' => '/abaB2bEnterprise/admin',
      ),
      array(
        'id' => 'B2B_GeneralOptions_Students',
        'label' => 'Students',
        'roles' => array('root', 'support', 'marketing'),
        'subMenus' => '/extranet/Userlist',
      ),
    ),
  ),
  array(
    'label' => 'Other Tools',
    'roles' => array('root', 'support', 'marketing', 'teacher'),
    'subMenus' => array(
      array(
        'label' => 'Promotions',
        'roles' => array('root', 'marketing'),
        'subMenus' => array(
          array(
            'label' => 'Promotion Codes',
            'roles' => array('root', 'marketing'),
            'subMenus' => '/abaProductsPromos/admin',
          ),
          array(
            'label' => 'Promotion Descriptions',
            'roles' => array('root', 'marketing'),
            'subMenus' => '/abaProductsPromosDescription/admin',
          ),
        )
      ),
      array(
        'label' => 'Experiments',
        'roles' => array('root'),
        'subMenus' => array(
          array(
            'label' => 'Experiments',
            'roles' => array('root'),
            'subMenus' => '/abaExperiments/admin',
          ),
          array(
            'label' => 'Experiments Variations',
            'roles' => array('root'),
            'subMenus' => '/abaExperimentsVariations/admin',
          ),
          array(
            'label' => 'Scenarios Payments Prices',
            'roles' => array('root'),
            'subMenus' => '/AbaExperimentsVariationsAttributes/admin',
          ),
          array(
            'label' => 'Experiments Types',
            'roles' => array('root'),
            'subMenus' => '/abaExperimentsTypes/admin',
          ),
          array(
            'label' => 'Experiments User Categories',
            'roles' => array('root'),
            'subMenus' => '/abaExperimentsUserCategories/admin',
          ),
          array(
            'label' => 'Experiments availability modes',
            'roles' => array('root'),
            'subMenus' => '/abaExperimentsAvailabilityModes/admin',
          ),
          array(
            'label' => 'Users Experiments Variations',
            'roles' => array('root'),
            'subMenus' => '/abaUserExperimentsVariations/admin',
          ),
        )
      ),

      array(
        'label' => 'Partners',
        'roles' => array('root', 'marketing'),
        'subMenus' => array(
          array(
            'label' => 'Partners List',
            'roles' => array('root', 'marketing'),
            'subMenus' => '/abaPartnersList/admin',
          ),
          array(
            'label' => 'Partners Groups',
            'roles' => array('root', 'marketing'),
            'subMenus' => '/abaPartnerGroups/admin',
          ),
          array(
            'label' => 'Partners Categories',
            'roles' => array('root', 'marketing'),
            'subMenus' => '/abaPartnerCategories/admin',
          ),
          array(
            'label' => 'Partners Business Areas',
            'roles' => array('root', 'marketing'),
            'subMenus' => '/abaPartnerBusinessareas/admin',
          ),
          array(
            'label' => 'Partners Types',
            'roles' => array('root', 'marketing'),
            'subMenus' => '/abaPartnerTypes/admin',
          ),
          array(
            'label' => 'Partners Channels',
            'roles' => array('root', 'marketing'),
            'subMenus' => '/abaPartnerChannels/admin',
          ),
        )
      ),

      array(
        'label' => 'Sources List',
        'roles' => array('root', 'marketing'),
        'subMenus' => '/sourcesList/admin',
      ),
      array(
        'label' => 'Backoffice Users',
        'roles' => array('root'),
        'subMenus' => '/abaUserBackoffice/admin',
      ),
      array(
        'label' => 'Change my password',
        'roles' => array('root', 'support', 'marketing', 'teacher'),
        'subMenus' => '/abaUserBackoffice/updateNormalUser&id=' . Yii::app()->user->idUser,
      ),
      array(
        'label' => 'Enable/Disable Configs',
        'roles' => array('root', 'support', 'marketing'),
        'subMenus' => '/config/updateConfig',
      ),
      array(
        'label' => 'Blogs Newsletter',
        'roles' => array('root', 'support', 'marketing'),
        'subMenus' => '/blognewsletter/index',
      ),
      array(
        'label' => 'Users progress problems queues',
        'roles' => array('root', 'support', 'marketing'),
        'subMenus' => '/abaUserProgressQueues/admin',
      ),
      array(
        'label' => 'Removed progress log',
        'roles' => array('root', 'support', 'marketing'),
        'subMenus' => '/abaUserRemovedProgress/admin',
      ),
    )
  ),
  array(
    'label' => 'KPI Performance',
    'roles' => array('root'),
    'subMenus' => array(
      array(
        'label' => 'World Map Aba English',
        'roles' => array('root'),
        'subMenus' => '/abaBi/GeoChartUsers',
      ),
      array(
        'label' => 'Q. Payments by Supplier',
        'roles' => array('root'),
        'subMenus' => '/abaBi/LChartPayBySupplier',
      ),
      array(
        'label' => 'Percentage of <br>successful payments',
        'roles' => array('root'),
        'subMenus' => '/abaBi/AChartPayAttempts',
      ),
      array(
        'label' => 'Webservices errors log',
        'roles' => array('root'),
        'subMenus' => '/abaBi/WsErrorsLog',
      ),
      array(
        'label' => 'Adyen notifications',
        'roles' => array('root'),
        'subMenus' => '/abaAdyenNotifications/admin',
      ),
    )
  ),
);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>

    <!-- blueprint CSS framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css"
          media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css"
          media="print"/>
    <link rel="shortcut icon" href="/images/favicon.ico">
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css"
          media="screen, projection"/>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/menu.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/menu2.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/toggle-switch.css"/>
    <script src="//code.jquery.com/jquery-1.10.1.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/abaEvents.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/abaUtils.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.blockUI.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/menuScript.js"></script>

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/konami.js"></script>

    <!-- Hear me Roar -->
    <script type="application/javascript">
      function bybye() {
        $('#santa').hide();
      }
      $( document ).ready(function() {
        $('#dino').click(function(){
            $('#dino').hide();
            $('#dino').css("left", 0);
        });
      });

      var easter_egg = new Konami();
      easter_egg.code = function() {
        $('#logoABA').attr("src","/images/lannister.jpg");
        $('#dino').show();
        $("#dino").animate({left: $(window).width()-200}, 1500, "swing", bybye);

        //alert('Hear me roar');

      }

      easter_egg.load();
    </script>
    <!-- Single library added to make snow effect on christmas. -->
    <?php
    $nadal = false;
    $isNadalM = date('m');
    $isNadalD = date('d');

    if ($isNadalM == 12) {
        $nadal = true;
    }
    if (($isNadalM == 1) && ($isNadalD <= 6)) {
        $nadal = true;
    }

    if ($nadal) {
        ?>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/snowstorm.js"></script>
        <script type="text/javascript">
            snowStorm.snowColor = '#ffffff'; // blue-ish snow!?
            snowStorm.flakesMaxActive = 50;  // show more snow on screen at once
            snowStorm.useTwinkleEffect = true; // let the snow flicker in and out of view
            snowStorm.animationInterval = 15;
            snowStorm.followMouse = false;
            snowStorm.freezeOnBlur = true;
            snowStorm.vMaxX = 3;                 // Maximum X velocity range for snow
            snowStorm.vMaxY = 2;
        </script>
        <?php
    }
    ?>


    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>


<script type="text/javascript">
    jQuery(window).load(function () {

        $("#nav > li > a").click(function (e) { // binding onclick
            if ($(this).parent().hasClass('selected')) {
                $("#nav .selected div div").slideUp(25); // hiding popups
                $("#nav .selected").removeClass("selected");
            } else {
                $("#nav .selected div div").slideUp(25); // hiding popups
                $("#nav .selected").removeClass("selected");

                if ($(this).next(".subs").length) {
                    $(this).parent().addClass("selected"); // display popup
                    $(this).next(".subs").children().slideDown(100);
                }
            }
            e.stopPropagation();
        });

        $("body").click(function () { // binding onclick to body
            $("#nav .selected div div").slideUp(50); // hiding popups
            $("#nav .selected").removeClass("selected");
        });

    });

    function hideSanta() {
        $('#santa').hide();
    }

</script>

<div class="container" id="page">

    <div id="header">
        <table border="0" id="logoTable">
            <tr>
                <td width="120px">
                    <?php
                    $image = CHtml::image("/images/logo-aba-english.png", '', array('width' => '120', 'id' => 'logoABA'));
                    echo CHtml::link($image, array('/site/index'));
                    ?>
                </td>
                <td style="text-align: left;">
                    <span style="font-size: 27px; font-weight: bold">Intranet Abaenglish</span><br>
                    <?php
                    $random = rand(1, sizeof($fraceCelebre));
                    echo "<span style=\"font-size: 12px\">" . $fraceCelebre[$random] . "</span><br>";
                    echo "<span style=\"font-size: 10px\"><i>-" . $autor[$random] . "</i></span><br>";
                    ?>
                </td>
                <td style="text-align: right;">
                    <?php
                    if (Yii::app()->user->isGuest) {
                        //$image = CHtml::image(Yii::app()->request->baseUrl . '/images/login.png');
                        //echo CHtml::link($image, array('/site/logout'))."<br>";
                        //echo CHtml::link('Login', array('/site/login'));
                    } else {
                        $today = date('N');
                        if ($today == 5) {
                            $image = CHtml::image(Yii::app()->request->baseUrl . '/images/beer.png');
                        } else {
                            $image = CHtml::image(Yii::app()->request->baseUrl . '/images/logout.png');
                        }
                        echo CHtml::link($image, array('/site/logout')) . "<br>";
                        echo "<i>Login as: <b>" . Yii::app()->user->name . "</b></i><br>";


                    }
                    ?>

                </td>
            </tr>
        </table>

    </div>

    <!-- header -->
    <div id='cssmenu'>
        <ul style="z-index: 1000;">
            <?php $userRole = Yii::app()->user->getId(); ?>
            <?php $idCount = 0; ?>
            <?php foreach ($arMenus as $menu) { ?>
                <?php $idBtn = (isset($menu['id'])) ? 'idBtn_' . $menu['id'] : 'idBtn_' . $idCount++; ?>
                <?php if (array_search($userRole, $menu['roles']) === false) {
                    continue;
                } ?>
                <li>
                    <?php if (is_string($menu['subMenus'])) { ?>
                        <?php echo CHtml::link($menu['label'], array($menu['subMenus']), array('id' => $idBtn)); ?>
                    <?php } else {
                        if (is_array($menu['subMenus'])) { ?>
                            <a href='#' id="<?php echo $idBtn; ?>"><?php echo $menu['label']; ?></a>
                            <ul>
                                <?php foreach ($menu['subMenus'] as $submenu) { ?>
                                    <?php $idBtn = (isset($submenu['id'])) ? 'idBtn_' . $submenu['id'] : 'idBtn_' . $idCount++; ?>
                                    <?php if (array_search($userRole, $submenu['roles']) === false) {
                                        continue;
                                    } ?>
                                    <li>
                                        <?php if (is_string($submenu['subMenus'])) { ?>
                                            <?php
                                            $linkOptions = array();
                                            if (isset($submenu['external_link']) && $submenu['external_link']) {
                                                $linkOptions['href'] = $submenu['subMenus'];
                                                $link = '';
                                            } else {
                                                $link = array($submenu['subMenus']);
                                            }
                                            if (isset($submenu['target'])) {
                                                $linkOptions['target'] = $submenu['target'];
                                            }
                                            $linkOptions['id'] = $idBtn;
                                            ?>
                                            <?php echo CHtml::link($submenu['label'], $link, $linkOptions); ?>
                                        <?php } else {
                                            if (is_array($submenu['subMenus'])) { ?>
                                                <a href='#'
                                                   id="<?php echo $idBtn; ?>"><?php echo $submenu['label']; ?></a>
                                                <ul>
                                                    <?php foreach ($submenu['subMenus'] as $subsubmenu) { ?>
                                                    <?php $idBtn = (isset($subsubmenu['id'])) ? 'idBtn_' . $subsubmenu['id'] : 'idBtn_' . $idCount++; ?>
                                                    <?php $menuOptions = array(); ?>
                                                    <?php if (array_search($userRole, $subsubmenu['roles']) === false) {
                                                        continue;
                                                    } ?>
                                                    <li><?php echo CHtml::link($subsubmenu['label'],
                                                          array($subsubmenu['subMenus']), array('id' => $idBtn)); ?>
                                                        <?php } ?>
                                                </ul>
                                            <?php }
                                        } ?>
                                    </li>
                                <?php } ?>
                            </ul>

                        <?php }
                    } ?>
                </li>
            <?php } ?>
        </ul>
    </div>

    <?php if (isset($this->breadcrumbs)): ?>
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
          'links' => $this->breadcrumbs,
        )); ?><!-- breadcrumbs -->
    <?php endif ?>

    <?php echo $content; ?>

    <div class="clear"></div>

    <div id="footer">
        Copyright &copy; <?php echo date('Y'); ?> by Abaenglish.<br>
        All Rights Reserved.<br><br>
    </div>
    <!-- footer -->

</div>
<!-- page -->
<?php
if ($nadal) {
    ?>
    <img id="santa" onclick="hideSanta();" src="/images/santa-gift-icon.png" width="100px"
         style="position:fixed; right:0; bottom:0; margin:0; padding:0;"/>
    <?php
}
?>
<img id="dino" onclick="hideSanta();" src="/images/dino.gif"
     style="position:fixed; left:0; bottom:0; margin:0; padding:0; display: none;"/>

</body>
</html>
