<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<title>Stats</title>
    <style type="text/css">
        body
        {
            margin: 0;
            padding: 0;
            color: #555;
            font: normal 10pt Arial,Helvetica,sans-serif;
            /* background: #666666; */
        }

        #page
        {
            margin-top: 20px;
            margin-bottom: 20px;
            background: white;
            border: 0px solid #CCCCCC;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            -webkit-box-shadow: 0px 0px 12px rgba(0, 0, 0, 0.9);
            -moz-box-shadow: 0px 0px 12px rgba(0, 0, 0, 0.9);
            box-shadow: 0px 0px 12px rgba(0, 0, 0, 0.9);
        }

        #content
        {
            padding: 20px;
        }
        .container {width:95%;margin:0 auto;}
        #content
        {
            padding: 20px;
        }
        #salesTable
        {
            width:95%;margin:0 auto;
            background-color: #fcfcfc;
            padding: 10px;
            -moz-border-radius: 10px 10px 10px 10px;
            -webkit-border-radius: 10px 10px 10px 10px;
            border-radius: 10px;
            -webkit-box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.1);
            box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.1);
        }
        #salesTable tr td
        {
            border-bottom: 1px solid #CCCCCC;
            border-left: 1px solid #CCCCCC;
            border-radius: 5px;
        }


    </style>
</head>
<body>

<div class="container" id="page">


	<?php echo $content; ?>

</div><!-- page -->

</body>
</html>
