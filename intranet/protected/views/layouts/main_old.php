<?php /* @var $this Controller */ 

$fraceCelebre = array(  1=>"Great spirits have always encountered violent opposition from mediocre minds.", 
                        2=>"I have never met a man so ignorant that I couldn't learn something from him.",
                        3=>"Do what you can, with what you have, where you are.",
                        4=>"Stay hungry stay foolish.",
                        5=>"I have found the paradox, that if you love until it hurts, there can be no more hurt, only more love.",
                        6=>"Every saint has a past and every sinner has a future.",
                        7=>"Happiness is not something ready made. It comes from your own actions.",
                        8=>"The mind is everything. What you think you become.",
                        9=>"Your conscience is the measure of the honesty of your selfishness. Listen to it carefully.",
                        10=>"Everything that irritates us about others can lead us to an understanding of ourselves.",
                        11=>"Nothing is impossible, the word itself says 'I'm possible'",
                        12=>"I can't change the direction of the wind, but I can adjust my sails to always reach my destination.",
                        13=>"Experience is simply the name we give our mistakes.");

$autor = array( 1=>"Albert Einstein",
                2=>"Galileo Galilei",
                3=>"Theodore Roosevelt",
                4=>"Steve Jobs",
                5=>"Mother Teresa",
                6=>"Oscar Wilde",
                7=>"Dalai Lama",
                8=>"Buddha",
                9=>"Richard Bach",
                10=>"Carl Jung",
                11=>"Audrey Hepburn",
                12=>"Jimmy Dean ",
                13=>"Oscar Wilde");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
        <link rel="shortcut icon" href="/images/favicon.ico">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/menu.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/toggle-switch.css" />
    <script src="//code.jquery.com/jquery-1.10.1.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/abaEvents.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/abaUtils.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.blockUI.js"></script>

    <!-- Single library added to make snow effect on christmas. -->
    <?php
        $nadal=false;
        $isNadalM = date('m');
        $isNadalD = date('d');

        if($isNadalM == 12)
            $nadal=true;
        if(($isNadalM == 1)&&($isNadalD<=6))
            $nadal=true;

        if($nadal)
        {
            ?>
            <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/snowstorm.js"></script>
            <script type="text/javascript">
                snowStorm.snowColor = '#ffffff'; // blue-ish snow!?
                snowStorm.flakesMaxActive = 50;  // show more snow on screen at once
                snowStorm.useTwinkleEffect = true; // let the snow flicker in and out of view
                snowStorm.animationInterval = 15;
                snowStorm.followMouse = false;
                snowStorm.freezeOnBlur = true;
                snowStorm.vMaxX = 3;                 // Maximum X velocity range for snow
                snowStorm.vMaxY = 2;
            </script>
        <?php
        }
    ?>


	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>


<script type="text/javascript">
    jQuery(window).load(function() {

        $("#nav > li > a").click(function (e) { // binding onclick
            if ($(this).parent().hasClass('selected')) {
                $("#nav .selected div div").slideUp(25); // hiding popups
                $("#nav .selected").removeClass("selected");
            } else {
                $("#nav .selected div div").slideUp(25); // hiding popups
                $("#nav .selected").removeClass("selected");

                if ($(this).next(".subs").length) {
                    $(this).parent().addClass("selected"); // display popup
                    $(this).next(".subs").children().slideDown(100);
                }
            }
            e.stopPropagation();
        });

        $("body").click(function () { // binding onclick to body
            $("#nav .selected div div").slideUp(50); // hiding popups
            $("#nav .selected").removeClass("selected");
        });

    });

    function hideSanta()
    {
        $('#santa').hide();
    }

</script>

<div class="container" id="page">

	<div id="header">
            <table border="0" id="logoTable">
                <tr>
                    <td width="120px">
                        <?php
                            $image = CHtml::image("/images/logo-aba-english.png",'', array('width'=>'120'));
                            echo CHtml::link($image, array('/site/index'));
                        ?>
                    </td>
                    <td style= "text-align: left;" >
                        <span style="font-size: 27px; font-weight: bold">Intranet Abaenglish</span><br>
                                <?php
                                    $random = rand(1, sizeof($fraceCelebre)); 
                                    echo "<span style=\"font-size: 12px\">".$fraceCelebre[$random]."</span><br>";
                                    echo "<span style=\"font-size: 10px\"><i>-".$autor[$random]."</i></span><br>";
                                ?>
                    </td>
                    <td style= "text-align: right;" >
                        <?php
                            if(Yii::app()->user->isGuest)
                            { 
                                //$image = CHtml::image(Yii::app()->request->baseUrl . '/images/login.png');
                                //echo CHtml::link($image, array('/site/logout'))."<br>";    
                                //echo CHtml::link('Login', array('/site/login'));
                            } 
                            else 
                            {
                                $today = date('N');
                                if($today==5)
                                {
                                    $image = CHtml::image(Yii::app()->request->baseUrl . '/images/beer.png');
                                }
                                else
                                {
                                    $image = CHtml::image(Yii::app()->request->baseUrl . '/images/logout.png');
                                }
                                echo CHtml::link($image, array('/site/logout'))."<br>";
                                echo "<i>Login as: <b>".Yii::app()->user->name."</b></i><br>";
                                
                                
                            } 
                        ?>

                    </td>
                </tr>
            </table>

	</div><!-- header -->
    
    <?php

    switch (Yii::app()->user->getId())
    {
    case 'root':
        //begin menu.
        echo "<div class=\"menu\"><span><ul id=\"nav\">";

        echo "<li>";
        echo "<a href=\"#\">Users</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Users', array('/abaUser/admin'))."</li>";
        echo "<li>".CHtml::link('Bank Details', array('/abaUserCreditForms/admin'))."</li>";
        echo "<li>".CHtml::link('Level Certificates', array('/abaUser/expresscertificates'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Payments</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Paylines', array('/abaPayments/admin'))."</li>";
        echo "<li>".CHtml::link('Paylines log', array('/abaPaymentsControlCheck/admin'))."</li>";
        echo "<li>".CHtml::link('Boletos', array('/abaPaymentsBoleto/admin'))."</li>";
        echo "<li>".CHtml::link('Reconciliation ', array('/reconciliationParser/reconciliation/importfile'))."</li>";
        echo "<li>".CHtml::link('Logs files reconciliation', array('/reconciliationParser/LogReconciliation/admin'))."</li>";
        echo "<li>".CHtml::link('Tax rates', array('/abaTaxRates/admin'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Statistics</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Sales Dashboard', array('/abaStatistics/totalSalesDashboard'))."</li>";
        echo "<li>".CHtml::link('Daily & Weekly Sales', array('/abaStatistics/totalSalesDashboardMobil'))."</li>";
        echo "<li>".CHtml::link('Sales by Country', array('/abaStatistics/totalSalesDashboardByCountry'))."</li>";
        echo "<li>".CHtml::link('Sales by Partner', array('/abaStatistics/totalSalesDashboardByPartner'))."</li>";
        echo "<li>".CHtml::link('Registration Dashboard', array('/abaStatistics/registrationDashboard'))."</li>";
        echo "</ul></li></ul></div></div>";

        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Teachers</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Messages', array('/abaQuestionsAndAnswers/admin'))."</li>";
        echo "<li>".CHtml::link('Sent & Answered', array('/abaQuestionsAndAnswers/getMessageSentAndAnswered'))."</li>";
        echo "<li>".CHtml::link('Total Sent & Answered', array('/abaQuestionsAndAnswers/getTotalMessageSentAndAnswered'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Flash Sales</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Coupons', array('/AbaVentasFlash/index'))."</li>";
        echo "<li>".CHtml::link('Grouper Sales', array('/AbaVentasFlash/salesForAgrupador'))."</li>";
        echo "<li>".CHtml::link('Groupers', array('/AbaAgrupadores/index'))."</li>";
        echo "<li>".CHtml::link('Load coupons from a file', array('/AbaVentasFlash/createFromFile'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Course</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Units Followup', array('/abaFollowup4/admin'))."</li>";
        echo "<li>".CHtml::link('Lines Exercises', array('/abaFollowupHtml4/admin'))."</li>";
        echo "<li>".CHtml::link('B2B Study Report', array('/b2bFollowup/admin'))."</li>";
        echo "<li>".CHtml::link('B2C Study Report', array('/b2cFollowup/admin'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Products</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Product List & Prices', array('/abaProductsPrices/admin'))."</li>";
        echo "<li>".CHtml::link('Products Periodicity', array('/abaProdPeriodicity/admin'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Other Tools</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Promotion Codes', array('/abaProductsPromos/admin'))."</li>";
        echo "<li>".CHtml::link('Partner List', array('/abaPartnersList/admin'))."</li>";
        echo "<li>".CHtml::link('Sources List', array('/sourcesList/admin'))."</li>";
        echo "<li>".CHtml::link('Backoffice Users', array('/abaUserBackoffice/admin'))."</li>";
        echo "<li>".CHtml::link('Change my password', array('/abaUserBackoffice/updateNormalUser&id='.Yii::app()->user->idUser))."</li>";
        echo "<li>".CHtml::link('Enable/Disable Configs', array('/config/updateConfig'))."</li>";
        echo "<li>".CHtml::link('Blogs Newsletter', array('/blognewsletter/index'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Extranet</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<span style='color: #ffffff; text-align: center; font-weight: bolder;'>General options</span>";
        echo "<li>".CHtml::link('B2B Channels List', array('/abaChannelList/admin'))."</li>";
        echo "<li>".CHtml::link('Enterprise Channels', array('/abaB2bEnterprise/admin'))."</li>";
        echo "<li>&nbsp;</li>";
        echo "<span style='color: #ffffff; text-align: center; font-weight: bolder;'>Sales</span>";
        echo "<li>".CHtml::link('Sales Dashboard', array('/abaStatisticsB2b/totalSalesDashboard'))."</li>";
        echo "<li>".CHtml::link('Daily & Weekly Sales', array('/abaStatisticsB2b/totalSalesDashboardMobil'))."</li>";
        echo "<li>".CHtml::link('Sales by Country', array('/abaStatisticsB2b/totalSalesDashboardByCountry'))."</li>";
        echo "<li>".CHtml::link('Sales by Partner', array('/abaStatisticsB2b/totalSalesDashboardByPartner'))."</li>";
        echo "<li>".CHtml::link('Registration Dashboard', array('/abaStatisticsB2b/registrationDashboard'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">KPI Performance</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('World Map Aba English', array('/abaBi/GeoChartUsers'))."</li>";
        echo "<li>".CHtml::link('Q. Payments by Supplier', array('/abaBi/LChartPayBySupplier'))."</li>";
        echo "<li>".CHtml::link('Percentage of <br>successful payments', array('/abaBi/AChartPayAttempts'))."</li>";
        echo "<li>".CHtml::link('Webservices errors log', array('/abaBi/WsErrorsLog'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        //end menu.
        echo "</ul></span></div>";
        break;
        
    case 'support':
        //begin menu.
        echo "<div class=\"menu\"><span><ul id=\"nav\">";

        echo "<li>";
        echo "<a href=\"#\">Users</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Users', array('/abaUser/admin'))."</li>";
        echo "<li>".CHtml::link('Bank Details', array('/abaUserCreditForms/admin'))."</li>";
        echo "<li>".CHtml::link('Level Certificates', array('/abaUser/expresscertificates'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Payments</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Paylines', array('/abaPayments/admin'))."</li>";
        echo "<li>".CHtml::link('Paylines log', array('/abaPaymentsControlCheck/admin'))."</li>";
        echo "<li>".CHtml::link('Boletos', array('/abaPaymentsBoleto/admin'))."</li>";
        echo "<li>".CHtml::link('Reconciliation ', array('/reconciliationParser/reconciliation/importfile'))."</li>";
        echo "<li>".CHtml::link('Logs files reconciliation', array('/reconciliationParser/LogReconciliation/admin'))."</li>";
        echo "<li>".CHtml::link('Tax rates', array('/abaTaxRates/admin'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Statistics</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Sales Dashboard', array('/abaStatistics/totalSalesDashboard'))."</li>";
        echo "<li>".CHtml::link('Daily & Weekly Sales', array('/abaStatistics/totalSalesDashboardMobil'))."</li>";
        echo "<li>".CHtml::link('Sales by Country', array('/abaStatistics/totalSalesDashboardByCountry'))."</li>";
        echo "<li>".CHtml::link('Sales by Partner', array('/abaStatistics/totalSalesDashboardByPartner'))."</li>";
        echo "<li>".CHtml::link('Registration Dashboard', array('/abaStatistics/registrationDashboard'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        /*echo "<li>";
        echo "<a href=\"#\">Teachers</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Messages', array('/abaQuestionsAndAnswers/admin'))."</li>";
        echo "<li>".CHtml::link('Sent & Answered', array('/abaQuestionsAndAnswers/getMessageSentAndAnswered'))."</li>";
        echo "<li>".CHtml::link('Total Sent & Answered', array('/abaQuestionsAndAnswers/getTotalMessageSentAndAnswered'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";*/

        echo "<li>";
        echo "<a href=\"#\">Flash Sales</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Coupons', array('/AbaVentasFlash/index'))."</li>";
       /* echo "<li>".CHtml::link('Grouper Sales', array('/AbaVentasFlash/salesForAgrupador'))."</li>";
        echo "<li>".CHtml::link('Groupers', array('/AbaAgrupadores/index'))."</li>";
        echo "<li>".CHtml::link('Load coupons from a file', array('/AbaVentasFlash/createFromFile'))."</li>";*/
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Course</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        //echo "<li>".CHtml::link('Units Followup', array('/abaFollowup4/admin'))."</li>";
        //echo "<li>".CHtml::link('Lines Exercises', array('/abaFollowupHtml4/admin'))."</li>";
        echo "<li>".CHtml::link('B2B Study Report', array('/b2bFollowup/admin'))."</li>";
        echo "<li>".CHtml::link('B2C Study Report', array('/b2cFollowup/admin'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Products</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Product List & Prices', array('/abaProductsPrices/admin'))."</li>";
        echo "<li>".CHtml::link('Products Periodicity', array('/abaProdPeriodicity/admin'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Other Tools</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        /* echo "<li>".CHtml::link('Promotion Codes', array('/abaProductsPromos/admin'))."</li>";
         echo "<li>".CHtml::link('Partner List', array('/abaPartnersList/admin'))."</li>";
         echo "<li>".CHtml::link('B2B Channels List', array('/abaChannelList/admin'))."</li>";
         echo "<li>".CHtml::link('Sources List', array('/sourcesList/admin'))."</li>";
         echo "<li>".CHtml::link('Backoffice Users', array('/abaUserBackoffice/admin'))."</li>";*/
        echo "<li>".CHtml::link('Change my password', array('/abaUserBackoffice/updateNormalUser&id='.Yii::app()->user->idUser))."</li>";
        echo "<li>".CHtml::link('Enable/Disable Configs', array('/config/updateConfig'))."</li>";
        echo "<li>".CHtml::link('Blogs Newsletter', array('/blognewsletter/index'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Extranet</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<span style='color: #ffffff; text-align: center; font-weight: bolder;'>General options</span>";
        echo "<li>".CHtml::link('B2B Channels List', array('/abaChannelList/admin'))."</li>";
        echo "<li>".CHtml::link('Enterprise Channels', array('/abaB2bEnterprise/admin'))."</li>";
        echo "<li>&nbsp;</li>";
        echo "<span style='color: #ffffff; text-align: center; font-weight: bolder;'>Sales</span>";
        echo "<li>".CHtml::link('Sales Dashboard', array('/abaStatisticsB2b/totalSalesDashboard'))."</li>";
        echo "<li>".CHtml::link('Daily & Weekly Sales', array('/abaStatisticsB2b/totalSalesDashboardMobil'))."</li>";
        echo "<li>".CHtml::link('Sales by Country', array('/abaStatisticsB2b/totalSalesDashboardByCountry'))."</li>";
        echo "<li>".CHtml::link('Sales by Partner', array('/abaStatisticsB2b/totalSalesDashboardByPartner'))."</li>";
        echo "<li>".CHtml::link('Registration Dashboard', array('/abaStatisticsB2b/registrationDashboard'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

       /* echo "<li>";
        echo "<a href=\"#\">KPI Payments</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('World Map Aba English', array('/abaBi/GeoChartUsers'))."</li>";
        echo "<li>".CHtml::link('Q. Payments by method', array('/abaBi/LChartPayBySupplier'))."</li>";
        echo "<li>".CHtml::link('Percentage of <br>successful payments', array('/abaBi/AChartPayAttempts'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";*/

        //end menu.
        echo "</ul></span></div>";
        break;
        
    case 'marketing':
        //begin menu.
        echo "<div class=\"menu\"><span><ul id=\"nav\">";

        echo "<li>";
        echo "<a href=\"#\">Users</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Users', array('/abaUser/admin'))."</li>";
        /*echo "<li>".CHtml::link('Bank Details', array('/abaUserCreditForms/admin'))."</li>";*/
        echo "<li>".CHtml::link('Level Certificates', array('/abaUser/expresscertificates'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Payments</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Paylines', array('/abaPayments/admin'))."</li>";
        echo "<li>".CHtml::link('Paylines log', array('/abaPaymentsControlCheck/admin'))."</li>";
        /*echo "<li>".CHtml::link('Boletos', array('/abaPaymentsBoleto/admin'))."</li>";*/
       /* echo "<li>".CHtml::link('Reconciliation ', array('/reconciliationParser/reconciliation/importfile'))."</li>";
        echo "<li>".CHtml::link('Logs files reconciliation', array('/reconciliationParser/LogReconciliation/admin'))."</li>";*/
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Statistics</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Sales Dashboard', array('/abaStatistics/totalSalesDashboard'))."</li>";
        echo "<li>".CHtml::link('Daily & Weekly Sales', array('/abaStatistics/totalSalesDashboardMobil'))."</li>";
        echo "<li>".CHtml::link('Sales by Country', array('/abaStatistics/totalSalesDashboardByCountry'))."</li>";
        echo "<li>".CHtml::link('Sales by Partner', array('/abaStatistics/totalSalesDashboardByPartner'))."</li>";
        echo "<li>".CHtml::link('Registration Dashboard', array('/abaStatistics/registrationDashboard'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Teachers</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Messages', array('/abaQuestionsAndAnswers/admin'))."</li>";
        echo "<li>".CHtml::link('Sent & Answered', array('/abaQuestionsAndAnswers/getMessageSentAndAnswered'))."</li>";
        echo "<li>".CHtml::link('Total Sent & Answered', array('/abaQuestionsAndAnswers/getTotalMessageSentAndAnswered'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Flash Sales</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Coupons', array('/AbaVentasFlash/index'))."</li>";
        echo "<li>".CHtml::link('Grouper Sales', array('/AbaVentasFlash/salesForAgrupador'))."</li>";
        /*echo "<li>".CHtml::link('Groupers', array('/AbaAgrupadores/index'))."</li>";*/
        echo "<li>".CHtml::link('Load coupons from a file', array('/AbaVentasFlash/createFromFile'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Course</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
       /* echo "<li>".CHtml::link('Units Followup', array('/abaFollowup4/admin'))."</li>";
        echo "<li>".CHtml::link('Lines Exercises', array('/abaFollowupHtml4/admin'))."</li>";*/
        echo "<li>".CHtml::link('B2B Study Report', array('/b2bFollowup/admin'))."</li>";
        echo "<li>".CHtml::link('B2C Study Report', array('/b2cFollowup/admin'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Products</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Product List & Prices', array('/abaProductsPrices/admin'))."</li>";
        echo "<li>".CHtml::link('Products Periodicity', array('/abaProdPeriodicity/admin'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Other Tools</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Promotion Codes', array('/abaProductsPromos/admin'))."</li>";
        echo "<li>".CHtml::link('Partner List', array('/abaPartnersList/admin'))."</li>";
        echo "<li>".CHtml::link('Sources List', array('/sourcesList/admin'))."</li>";
        /*echo "<li>".CHtml::link('Backoffice Users', array('/abaUserBackoffice/admin'))."</li>";*/
        echo "<li>".CHtml::link('Change my password', array('/abaUserBackoffice/updateNormalUser&id='.Yii::app()->user->idUser))."</li>";
        echo "<li>".CHtml::link('Enable/Disable Configs', array('/config/updateConfig'))."</li>";
        echo "<li>".CHtml::link('Blogs Newsletter', array('/blognewsletter/index'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        echo "<li>";
        echo "<a href=\"#\">Extranet</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<span style='color: #ffffff; text-align: center; font-weight: bolder;'>General options</span>";
        echo "<li>".CHtml::link('B2B Channels List', array('/abaChannelList/admin'))."</li>";
        echo "<li>".CHtml::link('Enterprise Channels', array('/abaB2bEnterprise/admin'))."</li>";
        echo "<li>&nbsp;</li>";
        echo "<span style='color: #ffffff; text-align: center; font-weight: bolder;'>Sales</span>";
        echo "<li>".CHtml::link('Sales Dashboard', array('/abaStatisticsB2b/totalSalesDashboard'))."</li>";
        echo "<li>".CHtml::link('Daily & Weekly Sales', array('/abaStatisticsB2b/totalSalesDashboardMobil'))."</li>";
        echo "<li>".CHtml::link('Sales by Country', array('/abaStatisticsB2b/totalSalesDashboardByCountry'))."</li>";
        echo "<li>".CHtml::link('Sales by Partner', array('/abaStatisticsB2b/totalSalesDashboardByPartner'))."</li>";
        echo "<li>".CHtml::link('Registration Dashboard', array('/abaStatisticsB2b/registrationDashboard'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        /*echo "<li>";
        echo "<a href=\"#\">KPI Payments</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('World Map Aba English', array('/abaBi/GeoChartUsers'))."</li>";
        echo "<li>".CHtml::link('Q. Payments by method', array('/abaBi/LChartPayBySupplier'))."</li>";
        echo "<li>".CHtml::link('Percentage of <br>successful payments', array('/abaBi/AChartPayAttempts'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";*/

        //end menu.
        echo "</ul></span></div>";
        break;
        
    case 'teacher':
        //begin menu.
        echo "<div class=\"menu\"><span><ul id=\"nav\">";

        echo "<li>";
        echo "<a href=\"#\">Users</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Users', array('/abaUser/admin'))."</li>";
        /*echo "<li>".CHtml::link('Bank Details', array('/abaUserCreditForms/admin'))."</li>";
        echo "<li>".CHtml::link('Level Certificates', array('/abaUser/expresscertificates'))."</li>";*/
        echo "</ul></li></ul></div></div>";
        echo "</li>";

       /* echo "<li>";
        echo "<a href=\"#\">Payments</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Paylines', array('/abaPayments/admin'))."</li>";
        echo "<li>".CHtml::link('Paylines log', array('/abaPaymentsControlCheck/admin'))."</li>";
        echo "<li>".CHtml::link('Boletos', array('/abaPaymentsBoleto/admin'))."</li>";
        echo "<li>".CHtml::link('Reconciliation ', array('/reconciliationParser/reconciliation/importfile'))."</li>";
        echo "<li>".CHtml::link('Logs files reconciliation', array('/reconciliationParser/LogReconciliation/admin'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";*/

        /*echo "<li>";
        echo "<a href=\"#\">Statistics</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Sales Dashboard', array('/abaStatistics/totalSalesDashboard'))."</li>";
        echo "<li>".CHtml::link('Daily & Weekly Sales', array('/abaStatistics/totalSalesDashboardMobil'))."</li>";
        echo "<li>".CHtml::link('Sales by Country', array('/abaStatistics/totalSalesDashboardByCountry'))."</li>";
        echo "<li>".CHtml::link('Sales by Partner', array('/abaStatistics/totalSalesDashboardByPartner'))."</li>";
        echo "<li>".CHtml::link('Registration Dashboard', array('/abaStatistics/registrationDashboard'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";*/

        echo "<li>";
        echo "<a href=\"#\">Teachers</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Messages', array('/abaQuestionsAndAnswers/admin'))."</li>";
        echo "<li>".CHtml::link('Sent & Answered', array('/abaQuestionsAndAnswers/getMessageSentAndAnswered'))."</li>";
        echo "<li>".CHtml::link('Total Sent & Answered', array('/abaQuestionsAndAnswers/getTotalMessageSentAndAnswered'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        /*echo "<li>";
        echo "<a href=\"#\">Flash Sales</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Coupons', array('/AbaVentasFlash/index'))."</li>";
        echo "<li>".CHtml::link('Grouper Sales', array('/AbaVentasFlash/salesForAgrupador'))."</li>";
        echo "<li>".CHtml::link('Groupers', array('/AbaAgrupadores/index'))."</li>";
        echo "<li>".CHtml::link('Load coupons from a file', array('/AbaVentasFlash/createFromFile'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";*/

        echo "<li>";
        echo "<a href=\"#\">Course</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        /* echo "<li>".CHtml::link('Units Followup', array('/abaFollowup4/admin'))."</li>";
        echo "<li>".CHtml::link('Lines Exercises', array('/abaFollowupHtml4/admin'))."</li>";*/
        echo "<li>".CHtml::link('B2B Study Report', array('/b2bFollowup/admin'))."</li>";
        echo "<li>".CHtml::link('B2C Study Report', array('/b2cFollowup/admin'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        /* echo "<li>";
        echo "<a href=\"#\">Products</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('Product List & Prices', array('/abaProductsPrices/admin'))."</li>";
        echo "<li>".CHtml::link('Products Periodicity', array('/abaProdPeriodicity/admin'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";*/

        echo "<li>";
        echo "<a href=\"#\">Other Tools</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        /*echo "<li>".CHtml::link('Promotion Codes', array('/abaProductsPromos/admin'))."</li>";
        echo "<li>".CHtml::link('Partner List', array('/abaPartnersList/admin'))."</li>";
        echo "<li>".CHtml::link('Sources List', array('/sourcesList/admin'))."</li>";
        echo "<li>".CHtml::link('Backoffice Users', array('/abaUserBackoffice/admin'))."</li>";*/
        echo "<li>".CHtml::link('Change my password', array('/abaUserBackoffice/updateNormalUser&id='.Yii::app()->user->idUser))."</li>";
        /*echo "<li>".CHtml::link('Enable/Disable Configs', array('/config/updateConfig'))."</li>";*/
        echo "</ul></li></ul></div></div>";
        echo "</li>";

        /*echo "<li>";
        echo "<a href=\"#\">Extranet</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<span style='color: #ffffff; text-align: center; font-weight: bolder;'>General options</span>";
        echo "<li>".CHtml::link('B2B Channels List', array('/abaChannelList/admin'))."</li>";
        echo "<li>".CHtml::link('Enterprise Channels', array('/abaB2bEnterprise/admin'))."</li>";
        echo "<li>&nbsp;</li>";
        echo "<span style='color: #ffffff; text-align: center; font-weight: bolder;'>Sales</span>";
        echo "<li>".CHtml::link('Sales Dashboard', array('/abaStatisticsB2b/totalSalesDashboard'))."</li>";
        echo "<li>".CHtml::link('Daily & Weekly Sales', array('/abaStatisticsB2b/totalSalesDashboardMobil'))."</li>";
        echo "<li>".CHtml::link('Sales by Country', array('/abaStatisticsB2b/totalSalesDashboardByCountry'))."</li>";
        echo "<li>".CHtml::link('Sales by Partner', array('/abaStatisticsB2b/totalSalesDashboardByPartner'))."</li>";
        echo "<li>".CHtml::link('Registration Dashboard', array('/abaStatisticsB2b/registrationDashboard'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";*/

        /*echo "<li>";
        echo "<a href=\"#\">KPI Payments</a>";
        echo "<div class=\"subs\"><div><ul><li><ul>";
        echo "<li>".CHtml::link('World Map Aba English', array('/abaBi/GeoChartUsers'))."</li>";
        echo "<li>".CHtml::link('Q. Payments by method', array('/abaBi/LChartPayBySupplier'))."</li>";
        echo "<li>".CHtml::link('Percentage of <br>successful payments', array('/abaBi/AChartPayAttempts'))."</li>";
        echo "</ul></li></ul></div></div>";
        echo "</li>";*/

        //end menu.
        echo "</ul></span></div>";
        break;
    }
    
    ?>

	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by Abaenglish.<br>
		All Rights Reserved.<br><br>
	</div><!-- footer -->

</div><!-- page -->
<?php
if($nadal) {
    ?>
    <img id="santa" onclick="hideSanta();" src="/images/santa-gift-icon.png" width="100px"
         style="position:fixed; right:0; bottom:0; margin:0; padding:0;"/>
<?php
}
?>

</body>
</html>
