<?php
/* @var $this AbaUserHistoryStateController */
/* @var $model AbaUserHistoryState */

$this->breadcrumbs=array(
	'Aba User History States'=>array('admin'),
	$model->userId,
);

$this->menu=array(
	array('label'=>'List AbaUserHistoryState', 'url'=>array('index')),
	array('label'=>'Create AbaUserHistoryState', 'url'=>array('create')),
	array('label'=>'Update AbaUserHistoryState', 'url'=>array('update', 'id'=>$model->userId)),
	array('label'=>'Delete AbaUserHistoryState', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->userId),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AbaUserHistoryState', 'url'=>array('admin')),
);
?>

<h1>View AbaUserHistoryState #<?php echo $model->userId; ?></h1>

<?php $this->widget('application.components.widgets.CDetailViewABA', array(
	'data'=>$model,
	'attributes'=>array(
		'userId',
		'userType'=>
		array( 
			'name'=>'userType',
            'type'=>'userTypeDescription',
          ),
		'date',
		'idPartner'=>array( 
			'name'=>'idPartner',
            'type'=>'partnerList',
          ),
	),
)); ?>
