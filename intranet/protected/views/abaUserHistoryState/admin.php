<?php
/* @var $this AbaUserHistoryStateController */
/* @var $model AbaUserHistoryState */

$this->breadcrumbs=array(
	'Aba User History States'=>array('admin'),
	'Manage',
);

$this->menu=array(
//	array('label'=>'List AbaUserHistoryState', 'url'=>array('index')),
	array('label'=>'Create AbaUserHistoryState', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('aba-user-history-state-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Aba User History States</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>



<?php $this->widget('application.components.widgets.CGridViewABA', array(
	'id'=>'aba-user-history-state-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'extraInfo'=>array('view','update'),
	'extraButtons'=>array(
		'extraButton1'=> array(
	//	'iconExtraLink'=>'user.png',
		'textExtraLink'=>'User',
		'classExtraLink'=>'payments',
		'urlExtraLink'=>'"abaUser/admin_id",array("id"=>$data->userId)'),
	),
	'columns'=>array(
		//'userId',
		'userEmail'=>
		array(               
			'name'=>'userId',
            'type'=>'#user#id#email',
          ),
		'userType'=>
		array(               // related city displayed as a link
            'name'=>'userType',
            'type'=>'userTypeDescription',
    		'filter'=>HeList::userTypeDescription(),
          ),
		'date',
		array(
			'class'=>'CButtonColumnABA',
		),
	),
)); ?>
