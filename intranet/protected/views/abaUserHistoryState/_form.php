<?php
/* @var $this AbaUserHistoryStateController */
/* @var $model AbaUserHistoryState */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aba-user-history-state-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'userId'); ?>
		<?php echo $form->textField($model,'userId'); ?>
		<?php echo $form->error($model,'userId'); ?>
	</div>

	<div class="row">
		<?php 	
			$datos = $extrainfo['userTypeList'];
			echo $form->labelEx($model,'userType'); 
			echo $form->DropDownList($model,'userType',$datos);
		//	echo $form->textField($model,'userType',array('size'=>2,'maxlength'=>2)); 
			echo $form->error($model,'userType'); 
		?>
	</div>

	<div class="row">
	<?php 	
			echo $form->labelEx($model,'date'); 
			echo $form->textField($model,'date'); 
			echo $form->error($model,'date'); 
			echo CHtml::image("images/calendar.jpg","calendar2", array("id"=>"c_buttonEnd","class"=>"pointer")); 
			$this->widget('application.extensions.calendar.SCalendar',
				array(
				'inputField'=>'AbaUserHistoryState_date',
				'button'=>'c_buttonEnd',
				'ifFormat'=>'%Y-%m-%d 00:00:00',
			));
		?>
	
	
		
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->