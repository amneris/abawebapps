<?php
/* @var $this AbaUserHistoryStateController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Aba User History States',
);

$this->menu=array(
	array('label'=>'Create AbaUserHistoryState', 'url'=>array('create')),
	array('label'=>'Manage AbaUserHistoryState', 'url'=>array('admin')),
);
?>

<h1>Aba User History States</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
