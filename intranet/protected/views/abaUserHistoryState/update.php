<?php
/* @var $this AbaUserHistoryStateController */
/* @var $model AbaUserHistoryState */

$this->breadcrumbs=array(
	'Aba User History States'=>array('admin'),
	$model->userId=>array('view','id'=>$model->userId),
	'Update',
);

$this->menu=array(
	array('label'=>'List AbaUserHistoryState', 'url'=>array('index')),
	array('label'=>'Create AbaUserHistoryState', 'url'=>array('create')),
	array('label'=>'View AbaUserHistoryState', 'url'=>array('view', 'id'=>$model->userId)),
	array('label'=>'Manage AbaUserHistoryState', 'url'=>array('admin')),
);
?>

<h1>Update AbaUserHistoryState <?php echo $model->userId; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'extrainfo'=>$extrainfo)); ?>