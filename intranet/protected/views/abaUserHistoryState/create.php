<?php
/* @var $this AbaUserHistoryStateController */
/* @var $model AbaUserHistoryState */

$this->breadcrumbs=array(
	'Aba User History States'=>array('admin'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List AbaUserHistoryState', 'url'=>array('index')),
	array('label'=>'Manage AbaUserHistoryState', 'url'=>array('admin')),
);
?>

<h1>Create AbaUserHistoryState</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>