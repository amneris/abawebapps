<?php
/* @var $this AbaUserHistoryStateController */
/* @var $data AbaUserHistoryState */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('userId')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->userId), array('view', 'id'=>$data->userId)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userType')); ?>:</b>
	<?php echo CHtml::encode($data->userType); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />
	<b><?php echo CHtml::encode($data->getAttributeLabel('idPartner')); ?>:</b>
	<?php echo CHtml::encode($data->idPartner); ?>
	<br />

</div>