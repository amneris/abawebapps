<?php
/* @var $this AbaProductsPromosDescriptionController */
/* @var $data AbaProductsPromosDescription */
?>

<div class="view">

	<b /><?php echo CHtml::encode($data->getAttributeLabel('Description')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Description), array('view', 'id'=>$data->Description)); ?>
	<br />

	<b /><?php echo CHtml::encode($data->getAttributeLabel('translationKey')); ?>:</b>
	<?php echo CHtml::encode($data->translationKey); ?>
	<br />

	<b /><?php echo CHtml::encode($data->getAttributeLabel('descriptionText')); ?>:</b>
	<?php echo CHtml::encode($data->descriptionText); ?>
	<br />

	<b /><?php echo CHtml::encode($data->getAttributeLabel('createDate')); ?>:</b>
	<?php echo CHtml::encode($data->createDate); ?>
	<br />

</div>