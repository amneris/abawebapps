<?php
/* @var $this AbaProductsPromosDescriptionController */
/* @var $model AbaProductsPromosDescription */

$this->breadcrumbs = array(
    'Promotion Codes Description' => array('admin'),
    $model->idDescription,
);

$this->menu = array(
    array('label' => 'List AbaProductsPromosDescription', 'url' => array('index')),
    array('label' => 'Create AbaProductsPromosDescription', 'url' => array('create')),
    array('label' => 'Update AbaProductsPromosDescription', 'url' => array('update', 'id' => $model->idDescription)),
    array(
        'label' => 'Delete AbaProductsPromosDescription',
        'url' => '#',
        'linkOptions' => array(
            'submit' => array('delete', 'id' => $model->idDescription),
            'confirm' => 'Are you sure you want to delete this item?'
        )
    ),
    array('label' => 'Manage AbaProductsPromosDescription', 'url' => array('admin')),
);
?>

<h3>View promotion description <b><?php echo $model->translationKey; ?></b></h3>

<?php $this->widget(
    'application.components.widgets.CDetailViewABA',
    array(
        'data' => $model,
        'attributes' => array(
            'idDescription',
            'translationKey',
            'descriptionText',
            'createDate',
        ),
    )
); ?>
