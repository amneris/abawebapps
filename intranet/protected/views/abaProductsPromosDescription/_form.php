<?php
/* @var $this AbaProductsPromosDescriptionController */
/* @var $model AbaProductsPromosDescription */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'id' => 'aba-products-promos-description-form',
            'enableAjaxValidation' => false,
        )
    );
?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <table id="myForm">
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'translationKey'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'translationKey', array('size' => 20, 'maxlength' => 50)); ?>
                <?php echo $form->error($model, 'translationKey'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'descriptionText'); ?>
            </td>
            <td>
                <?php echo $form->textArea($model, 'descriptionText', array('rows'=>6, 'cols'=>50)); ?>
                <?php echo $form->error($model, 'descriptionText'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'createDate'); ?>
            </td>
            <td>
                <?php
                echo $form->textField($model, 'createDate', array('size' => 15, 'maxlength' => 18));
                echo $form->error($model, 'createDate');
                echo CHtml::image(
                    "images/calendar.jpg",
                    "calendar0",
                    array("id" => "c_createDate", "class" => "pointer")
                );
                $this->widget(
                    'application.extensions.calendar.SCalendar',
                    array(
                        'inputField' => 'AbaProductsPromosDescription_createDate',
                        'button' => 'c_createDate',
                        'ifFormat' => '%Y-%m-%d %H:%M:%S',
                    )
                );
                ?>
                <?php echo $form->error($model, 'createDate'); ?>
            </td>
        </tr>
    </table>
    <div class="row buttons" align="center">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->