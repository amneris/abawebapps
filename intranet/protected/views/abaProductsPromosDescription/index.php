<?php
/* @var $this AbaProductsPromosDescriptionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Aba Products Promoses',
);

$this->menu=array(
	array('label'=>'Create AbaProductsPromos', 'url'=>array('create')),
	array('label'=>'Manage AbaProductsPromos', 'url'=>array('admin')),
);
?>

<h3>Promotion Codes Descriptions</h3>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
