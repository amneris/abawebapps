<?php
/* @var $this AbaProductsPromosDescriptionController */
/* @var $model AbaProductsPromosDescription */

$this->breadcrumbs=array(
	'Products Promotions Descriptions'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Products Promotions Descriptions', 'url'=>array('admin')),
);
?>

<h3>Promotion Codes Description</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>