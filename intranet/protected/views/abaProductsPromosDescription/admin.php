<?php
/* @var $this AbaProductsPromosDescriptionController */
/* @var $model AbaProductsPromosDescription */

$this->breadcrumbs = array(
    'Promotion Descriptions',
);

$this->menu = array(
    array('label' => 'Create Products Promotions Description', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript(
    'search',
    "
   $('.search-button').click(function(){
     $('.search-form').toggle();
     return false;
   });
   $('.search-form form').submit(function(){
     $.fn.yiiGridView.update('aba-products-promos-description-grid', {
       data: $(this).serialize()
     });
     return false;
   });
   "
);
?>

<h3>Promotion Descriptions</h3>

<?php echo CHtml::link('Create new promotion description', array('AbaProductsPromosDescription/create')); ?>
<br>
<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial(
        '_search',
        array(
            'model' => $model,
        )
    ); ?>
</div><!-- search-form -->

<?php $this->widget(
    'application.components.widgets.CGridViewABA',
    array(
        'id' => 'aba-products-promos-description-grid',
        'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
        'pager' => array(
            'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
            'prevPageLabel' => '<',
            'nextPageLabel' => '>',
            'firstPageLabel' => '<<',
            'lastPageLabel' => '>>',
        ),
        'extraInfo' => array('view', 'update', 'delete'),
        'dataProvider' => $model->search(),
        'filter' => $model,
        'filterSelector' => '{filter}',
        'columns' => array(
            'idDescription',
            'translationKey',
            'descriptionText',
            'createDate',
            array(
                'class' => 'CButtonColumnABA',
                'header' => 'Options',
            ),
        ),
    )
); ?>
