<?php
/* @var $this AbaProductsPromosDescriptionController */
/* @var $model AbaProductsPromosDescription */

$this->breadcrumbs=array(
	'Aba Products Promoses'=>array('admin'),
	$model->idDescription=>array('view','id'=>$model->idDescription),
	'Update',
);

$this->menu=array(
	array('label'=>'List AbaProductsPromosDescription', 'url'=>array('index')),
	array('label'=>'Create AbaProductsPromosDescription', 'url'=>array('create')),
	array('label'=>'View AbaProductsPromosDescription', 'url'=>array('view', 'id'=>$model->idDescription)),
	array('label'=>'Manage AbaProductsPromosDescription', 'url'=>array('admin')),
);
?>

<h3>Update Promotion Description: <?php echo $model->idDescription; ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>