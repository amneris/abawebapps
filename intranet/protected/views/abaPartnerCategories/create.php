<?php
/* @var $this AbaPartnercategoriesController */
/* @var $model AbaPartnercategories */

$this->breadcrumbs = array(
  'Partnercategories' => array('admin'),
  'Create',
);

$this->menu = array(
  array('label' => 'Manage Partnercategory', 'url' => array('admin')),
);
?>

    <h3>Partnercategories</h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>