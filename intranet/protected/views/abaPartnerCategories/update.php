<?php
/* @var $this AbaPartnercategoriesController */
/* @var $model AbaPartnercategories */

$this->breadcrumbs = array(
  'Aba Partnercategories' => array('admin'),
  $model->idCategory => array('view', 'id' => $model->idCategory),
  'Update',
);

$this->menu = array(
  array('label' => 'List Partnercategories', 'url' => array('index')),
  array('label' => 'Create Partnercategory', 'url' => array('create')),
  array('label' => 'View Partnercategories', 'url' => array('view', 'id' => $model->idCategory)),
  array('label' => 'Manage Partnercategories', 'url' => array('admin')),
);
?>

    <h3>Update Partnercategory: <?php echo $model->idCategory; ?></h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>