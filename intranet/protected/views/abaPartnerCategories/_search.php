<?php
/* @var $this AbaPartnercategoriesController */
/* @var $model AbaPartnercategories */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
      'CActiveForm',
      array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
      )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'idCategory'); ?>
        <?php echo $form->textField($model, 'idCategory', array('size' => 5, 'maxlength' => 20)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'nameCategory'); ?>
        <?php echo $form->textField($model, 'nameCategory', array('size' => 30, 'maxlength' => 45)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'dateAdd'); ?>
        <?php echo $form->textField($model, 'dateAdd', array('size' => 15, 'maxlength' => 19)); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->