<?php
/* @var $this AbaPartnercategoriesController */
/* @var $model AbaPartnercategories */

$this->breadcrumbs = array(
  'Partnercategories' => array('admin'),
  $model->idCategory,
);

$this->menu = array(
  array('label' => 'List Partnercategories', 'url' => array('index')),
  array('label' => 'Create Partner category', 'url' => array('create')),
  array('label' => 'Update Partner category', 'url' => array('update', 'id' => $model->idCategory)),
  array(
    'label' => 'Delete Partnercategory',
    'url' => '#',
    'linkOptions' => array(
      'submit' => array('delete', 'id' => $model->idCategory),
      'confirm' => 'Are you sure you want to delete this item?'
    )
  ),
  array('label' => 'Manage Partnercategories', 'url' => array('admin')),
);
?>

<h3>View Partnercategories <b><?php echo $model->idCategory . ": " . $model->nameCategory; ?></b></h3>

<?php $this->widget(
  'application.components.widgets.CDetailViewABA',
  array(
    'data' => $model,
    'attributes' => array(
      'idCategory',
      'nameCategory',
      'dateAdd',
    ),
  )
); ?>
