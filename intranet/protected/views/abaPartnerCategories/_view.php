<?php
/* @var $this AbaPartnercategoriesController */
/* @var $model AbaPartnercategories */
?>

<div class="view">

    <b/><?php echo CHtml::encode($data->getAttributeLabel('idCategory')); ?>:</b>
    <?php echo CHtml::encode($data->idCategory); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('nameCategory')); ?>:</b>
    <?php echo CHtml::encode($data->nameCategory); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('dateAdd')); ?>:</b>
    <?php echo CHtml::encode($data->dateAdd); ?>
    <br/>

</div>