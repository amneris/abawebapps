<?php
/* @var $this AbaPartnercategoriesController */
/* @var $model AbaPartnercategories */

$this->breadcrumbs = array(
  'Categories',
);

$this->menu = array(
  array('label' => 'Create Category', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript(
  'search',
  "
   $('.search-button').click(function(){
     $('.search-form').toggle();
     return false;
   });
   $('.search-form form').submit(function(){
     $.fn.yiiGridView.update('aba-partnercategories-grid', {
       data: $(this).serialize()
     });
     return false;
   });
   "
);
?>

<h3>Partners categories</h3>

<?php echo CHtml::link('Create new category', array('AbaPartnerCategories/create')); ?>
<br>
<div class="search-form" style="display:none">
    <?php $this->renderPartial(
      '_search',
      array(
        'model' => $model,
      )
    ); ?>
</div><!-- search-form -->

<?php $this->widget(
  'application.components.widgets.CGridViewABA',
  array(
    'id' => 'aba-partnercategories-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager' => array(
      'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
      'prevPageLabel' => '<',
      'nextPageLabel' => '>',
      'firstPageLabel' => '<<',
      'lastPageLabel' => '>>',
    ),
    'extraInfo' => array('view', 'update'),
    'dataProvider' => $model->search(),
    'filter' => $model,
    'filterSelector' => '{filter}',
    'columns' => array(
      'idCategory',
      'nameCategory',
      'dateAdd',
      array(
        'class' => 'CButtonColumnABA',
        'header' => 'Options',
      ),
    ),
  )
); ?>
