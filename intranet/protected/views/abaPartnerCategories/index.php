<?php
/* @var $this AbaPartnercategoriesController */
/* @var $model AbaPartnercategories */

$this->breadcrumbs = array(
  'Aba Partnercategories',
);

$this->menu = array(
  array('label' => 'Create Partnercategories', 'url' => array('create')),
  array('label' => 'Manage Partnercategories', 'url' => array('admin')),
);
?>

<h3>Partnercategories</h3>

<?php $this->widget(
  'zii.widgets.CListView',
  array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
  )
); ?>
