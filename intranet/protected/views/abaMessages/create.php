<?php
/* @var $this AbaMessagesController */
/* @var $model AbaMessages */

$this->breadcrumbs=array(
	'Aba Messages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AbaMessages', 'url'=>array('index')),
	array('label'=>'Manage AbaMessages', 'url'=>array('admin')),
);
?>

<h3>Create AbaMessages</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>