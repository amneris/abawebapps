<?php
/* @var $this AbaMessagesController */
/* @var $model AbaMessages */

$this->breadcrumbs=array(
	'Aba Messages'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AbaMessages', 'url'=>array('index')),
	array('label'=>'Create AbaMessages', 'url'=>array('create')),
	array('label'=>'Update AbaMessages', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AbaMessages', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AbaMessages', 'url'=>array('admin')),
);
?>

<h3>View AbaMessages #<?php echo $model->id; ?></h3>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'sender_id',
		'receiver_id',
		'subject',
		'body',
		'is_read',
		'deleted_by',
		'created_at',
	),
)); ?>
