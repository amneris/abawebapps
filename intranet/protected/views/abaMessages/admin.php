<?php
/* @var $this AbaMessagesController */
/* @var $model AbaMessages */

$this->breadcrumbs=array(
	'Messages',
); 

$this->menu=array(
	array('label'=>'List AbaMessages', 'url'=>array('index')),
	array('label'=>'Create AbaMessages', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('aba-messages-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Messages</h3>

<?php  $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
<br><br><br>
<b>Period from:</b> <?php  echo ($model->startDate." <b>to:</b> ".$model->endDate); ?>
<?php 

 $this->widget('application.components.widgets.CGridViewStaticModelABA', array(
	'id'=>'aba-messages-grid',
     'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
		'email'=>array(  
			  'header'=>'Teacher Id'	,
              'name'=>'email',
              'type'=>'?index.php?r=abaMessages/detail&id=*teacherid*&begin='.$model->startDate.'&end='.$model->endDate ,
          ),
		'total_users'=>array(  
				  'header'=>'Total premium users',
				  'name'=>'total_users'
				  ), 
		'total_questions'=>array(  
				  'header'=>'Total questions',
				  'name'=>'total_questions'), 
		'total_answers'=>array(  
				  'header'=>'Total answers',
				  'name'=>'total_answers'
				  ) , 
		'total_noanswers'=>array(  
				  'header'=>'Total NO answers',
				  'name'=>'total_noanswers'
				  ) ,
		'period_questions'=>array(  
				  'header'=>'<span style="color:red">Period questions</span>',
				  'name'=>'period_questions'
				  ), 
		'period_answers'=>array(  
				  'header'=>'<span style="color:red">Period answers</span>',
				  'name'=>'period_answers'
				  ),
		'period_noanswers'=>array(  
				  'header'=>'Period NO answers',
				  'name'=>'period_noanswers'
				  ),
		'noperiod_answers'=>array(  
				  'header'=>'NO period answers',
				  'name'=>'noperiod_answers'
				  ),
		),
)); ?>
