<?php
/* @var $this AbaMessagesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Aba Messages',
);

$this->menu=array(
	array('label'=>'Create AbaMessages', 'url'=>array('create')),
	array('label'=>'Manage AbaMessages', 'url'=>array('admin')),
);
?>

<h3>Aba Messages</h3>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
