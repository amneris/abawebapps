<?php
/* @var $this AbaMessagesController */
/* @var $model AbaMessages */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <table id="myForm">
        
        <tr>
            <td width="100px">
                <?php echo $form->label($model,'startDate'); ?>
            </td>
            <td>
                <?php 
                    echo $form->textField($model,'startDate' ); 
                    echo CHtml::image("images/calendar.jpg","calendar", array("id"=>"c_button","class"=>"pointer")); 
                    $this->widget('application.extensions.calendar.SCalendar',
                    array(
                    'inputField'=>'AbaTeachersMessages_startDate',
                    'button'=>'c_button',
                    'ifFormat'=>'%Y-%m-%d 00:00:00',
                ));
                ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <?php echo $form->label($model,'endDate'); ?>
            </td>
            <td>
                 <?php
                 
                    echo $form->textField($model,'endDate'); 
                    echo CHtml::image("images/calendar.jpg","calendar2", array("id"=>"c_button2","class"=>"pointer")); 
                    $this->widget('application.extensions.calendar.SCalendar',
                    array(
                    'inputField'=>'AbaTeachersMessages_endDate',
                    'button'=>'c_button2',
                    'ifFormat'=>'%Y-%m-%d 00:00:00',
                        )); 
                     ?>
            </td>
        </tr>
        
    </table>

	<div class="row buttons" align="center">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>
