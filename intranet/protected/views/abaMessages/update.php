<?php
/* @var $this AbaMessagesController */
/* @var $model AbaMessages */

$this->breadcrumbs=array(
	'Aba Messages'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AbaMessages', 'url'=>array('index')),
	array('label'=>'Create AbaMessages', 'url'=>array('create')),
	array('label'=>'View AbaMessages', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AbaMessages', 'url'=>array('admin')),
);
?>

<h3>Update AbaMessages <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>