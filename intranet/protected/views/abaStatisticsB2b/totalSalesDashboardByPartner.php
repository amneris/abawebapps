<?php
/* @var $this AbaPaymentsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Sales by partner B2B',
);
?>
<h3>Sales by partner B2B</h3>

<?php

$dollar = new AbaCurrency();
$dollar->getCurrencyById('USD');
$dollarToday = $dollar->outEur;

$real = new AbaCurrency();
$real->getCurrencyById('BRL');
$realToday = $real->outEur;

$peso = new AbaCurrency();
$peso->getCurrencyById('MXN');
$pesoToday = $peso->outEur;

//function just to make syntax more easy
function toMoney($number)
{
    $money = number_format($number,2,',','.');
    return $money;
}

?>

<div style="text-align: center; font-size: 15px">
    Sales and renovations for period from <b><?php echo $startDate ?></b> to <b><?php echo $endDate ?></b><br><br>
    <?php
        echo CHtml::form(Yii::app()->createUrl('abaStatisticsB2b/totalSalesDashboardByPartner'), 'post', array('id'=>'form', 'class'=>'myForm'));
        echo "range from ";
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'startDate',
            'value' => $startDate,
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth'=>true,
                'changeYear'=>true,
            ),
                'htmlOptions' => array(
                'size' => '10',
                'maxlength' => '10',
            ),
        ));
        echo " to ";
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'endDate',
            'value' => $endDate,
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth'=>true,
                'changeYear'=>true,
            ),
            'htmlOptions' => array(
                'size' => '10',
                'maxlength' => '10',
            ),
        ));
        echo " <i>and check this</i> ";
        if($entryDate)
            echo "<input type='checkbox' name='entryDate' value='true' checked/>";
        else
            echo "<input type='checkbox' name='entryDate' value='false'/>";
        echo " <i>if you like to search by <b>\"entry date\"</b> instead <b>\"end date transaction\"</b>)</i> ";

        echo CHtml::button("Search",array('onclick' => 'this.form.submit();','title'=>"Search",'id'=>'searchButton'));
        echo CHtml::endForm();
    ?>

</div>
<br>
<div style="width: 100%; text-align: center;"><button id="showVatBut" onclick="showVat();">Show VAT</button></div>
<br>
<table id="salesTable">
    <tr>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            Partner
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            Partner Group
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            Total Sales
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            Amount Sales
        </td>
        <td class="withVat" style="background-color: #9ebacf; color: #ffffff; font-weight: bold; text-align: center; width: 200px; display: none;">
            Sales with VAT
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            Total Renovation
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            Amount Renovation
        </td>
        <td class="withVat" style="background-color: #9ebacf; color: #ffffff; font-weight: bold; text-align: center; width: 200px; display: none;">
            Renovation with VAT
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            Total Transactions
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            Total Amount
        </td>
        <td class="withVat" style="background-color: #9ebacf; color: #ffffff; font-weight: bold; text-align: center; width: 200px; display: none;">
            Total Amount with VAT
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            %
        </td>
    </tr>
<?php
$totalCounts = array('totalSales'=>0, 'amountSales'=>0, 'amountSalesnoVat'=>0, 'totalRenovations'=>0, 'amountRenovation'=>0, 'amountRenovationnoVat'=>0, 'totalMovements'=>0, 'totalAmount'=>0, 'totalAmountnoVat'=>0);
$valuesToChart = array();
$valuesToChart[0] = array('Country', 'Transactions');

foreach($totalByPartner as $value)
{
    $totalCounts['totalSales'] = $totalCounts['totalSales'] + $value['saleQuantity'];
    $totalCounts['amountSales'] = $totalCounts['amountSales'] + $value['saleAmount'];
    $totalCounts['amountSalesnoVat'] = $totalCounts['amountSalesnoVat'] + $value['saleAmountnoVat'];
    $totalCounts['totalRenovations'] = $totalCounts['totalRenovations'] + $value['renovationQuantity'];
    $totalCounts['amountRenovation'] = $totalCounts['amountRenovation'] + $value['renovationAmount'];
    $totalCounts['amountRenovationnoVat'] = $totalCounts['amountRenovationnoVat'] + $value['renovationAmountnoVat'];
    $totalCounts['totalMovements'] = $totalCounts['totalMovements'] + $value['totalMovement'];
    $totalCounts['totalAmount'] = $totalCounts['totalAmount'] + $value['totalAmount'];
    $totalCounts['totalAmountnoVat'] = $totalCounts['totalAmountnoVat'] + $value['totalAmountnoVat'];
}

$x=1;
foreach($totalByPartner as $value)
{

    echo "<tr>";

    echo "<td>";
    echo "<span style='font-weight: bold; font-size: 14px; margin-left: 10px'>";
    echo ucfirst($value['name']);
    echo "</span>";
    echo "</td>";

    echo "<td>";
    echo ucfirst($value['nameGroup']);
    echo "</td>";

    echo "<td style='text-align: right'>";
    echo $value['saleQuantity'];
    echo "</td>";

    echo "<td style='text-align: right'>";
    echo toMoney($value['saleAmountnoVat']);
    echo "€";
    echo "</td>";

    echo "<td class='withVat' style='text-align: right; display: none;'>";
    echo toMoney($value['saleAmount']);
    echo "€";
    echo "</td>";

    echo "<td style='text-align: right'>";
    echo $value['renovationQuantity'];
    echo "</td>";

    echo "<td style='text-align: right'>";
    echo toMoney($value['renovationAmountnoVat']);
    echo "€";
    echo "</td>";

    echo "<td class='withVat' style='text-align: right; display: none;'>";
    echo toMoney($value['renovationAmount']);
    echo "€";
    echo "</td>";

    echo "<td style='text-align: right'>";
    echo $value['totalMovement'];
    echo "</td>";

    echo "<td style='text-align: right;background-color: greenyellow; font-weight: bold; font-size: 15px'>";
    echo toMoney($value['totalAmountnoVat']);
    echo "€";
    echo "</td>";

    echo "<td class='withVat' style='background-color: #d2ff8a; text-align: right; font-weight: bold; font-size: 15px; display: none;'>";
    echo toMoney($value['totalAmount']);
    echo "€";
    echo "</td>";

    echo "<td style='text-align: center'>";
    echo number_format(($value['totalAmount'] * 100) / $totalCounts['totalAmount'],2)."%";
    echo "</td>";

    echo "</tr>";

    $countryValue = array($value['name'], $value['saleQuantity'] + $value['renovationQuantity']);
    $valuesToChart[$x] = $countryValue;
    $x++;
}
?>
    <tr>
        <td style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px" colspan="2">
            Total
        </td>
        <td style="background-color: green; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo $totalCounts['totalSales'] ?>
        </td>
        <td style="background-color: green; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo toMoney($totalCounts['amountSalesnoVat']) ?>€
        </td>
        <td class="withVat" style="background-color: #65af66; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff; display: none;">
            <?php echo toMoney($totalCounts['amountSales']) ?>€
        </td>
        <td style="background-color: green; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo $totalCounts['totalRenovations'] ?>
        </td>
        <td style="background-color: green; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo toMoney($totalCounts['amountRenovationnoVat']) ?>€
        </td>
        <td class="withVat" style="background-color: #65af66; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff; display: none;">
            <?php echo toMoney($totalCounts['amountRenovation']) ?>€
        </td>
        <td style="background-color: green; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo $totalCounts['totalMovements'] ?>
        </td>
        <td style="background-color: green; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo toMoney($totalCounts['totalAmountnoVat']) ?>€
        </td>
        <td class="withVat" style="background-color: #65af66; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff; display: none;">
            <?php echo toMoney($totalCounts['totalAmount']) ?>€
        </td>
    </tr>
</table>

<div style="text-align: right">
    <?php
        echo "<span style=\"font-size:11px;\">* 1$ today is about ".$dollarToday."€</span><br />";
        echo "<span style=\"font-size:11px;\">* 1<span style='font-size:8px'>BRL</span> today is about ".$realToday."€</span><br />";
        echo "<span style=\"font-size:11px;\">* 1<span style='font-size:8px'>MXN</span> today is about ".$realToday."€</span>";
    ?>
</div>

<script>
    function showVat()
    {
        $('.withVat').toggle();
        if($('#showVatBut').text()=='Show VAT')
        {
            $('#showVatBut').text('Hidde Vat');
        }
        else
        {
            $('#showVatBut').text('Show VAT');
        }
    }
</script>