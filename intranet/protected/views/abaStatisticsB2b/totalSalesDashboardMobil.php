<?php
/* @var $this AbaPaymentsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Daily & Weekly sales B2B',
);
?>

<h3>ABA English Daily & Weekly sales B2B</h3>
<br>
<div style="width: 100%; text-align: center;"><button id="showVatBut" onclick="showVat();">Show VAT</button></div>
<br>
<?php

$dollar = new AbaCurrency();
$dollar->getCurrencyById('USD');
$dollarToday = $dollar->outEur;

$real = new AbaCurrency();
$real->getCurrencyById('BRL');
$realToday = $real->outEur;

$peso = new AbaCurrency();
$peso->getCurrencyById('MXN');
$pesoToday = $peso->outEur;

//function just to make syntax more easy
function toMoney($number)
{
    $money = number_format($number,2,',','.');
    return $money;
}
?>

<table id="salesTable">
    <tr>
        <td style="border: 0px; text-align: center; font-size: 15px; font-weight: bold; padding-bottom: 10px" colspan="4">
                Daily sales for today (<?php echo $endDate ?>)<br>
        </td>
    </tr>

    <tr>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            Type of Sale
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Average Ticket
        </td>
        <td class="withVat" style="background-color: #9ebacf; color: #ffffff; font-weight: bold; text-align: center; width: 200px; display: none;">
            Average with VAT
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Quantity
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Amount
        </td>
        <td class="withVat" style="background-color: #9ebacf; color: #ffffff; font-weight: bold; text-align: center; width: 200px; display: none;">
            Amount with VAT
        </td>
    </tr>

    <tr>
        <td>
            New sale
        </td>
        <td style="text-align: center">
            <?php
                if(($totalDaily['countEUR'] + $totalDaily['countUSD'] + $totalDaily['countBRL'] + $totalDaily['countMXN']) > 0)
                    echo toMoney(($totalDaily['totalEURnoVat'] + ($totalDaily['totalUSDnoVat'] * $dollarToday) + ($totalDaily['totalBRLnoVat'] * $realToday) + ($totalDaily['totalMXNnoVat'] * $pesoToday)) / ($totalDaily['countEUR'] + $totalDaily['countUSD'] + $totalDaily['countBRL'] + $totalDaily['countMXN']));
            ?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <?php
            if(($totalDaily['countEUR'] + $totalDaily['countUSD'] + $totalDaily['countBRL'] + $totalDaily['countMXN']) > 0)
                echo toMoney(($totalDaily['totalEUR'] + ($totalDaily['totalUSD'] * $dollarToday) + ($totalDaily['totalBRL'] * $realToday) + ($totalDaily['totalMXN'] * $pesoToday)) / ($totalDaily['countEUR'] + $totalDaily['countUSD'] + $totalDaily['countBRL'] + $totalDaily['countMXN']));
            ?>€
        </td>
        <td style="text-align: right;">
            <?php echo $totalDaily['countEUR'] + $totalDaily['countUSD'] + $totalDaily['countBRL'] + $totalDaily['countMXN']?>
        </td>
        <td style="text-align: right;">
            <?php echo toMoney($totalDaily['totalEURnoVat'] + ($totalDaily['totalUSDnoVat'] * $dollarToday) + ($totalDaily['totalBRLnoVat'] * $realToday) + ($totalDaily['totalMXNnoVat'] * $pesoToday)) . "€" ?>
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <?php echo toMoney($totalDaily['totalEUR'] + ($totalDaily['totalUSD'] * $dollarToday) + ($totalDaily['totalBRL'] * $realToday) + ($totalDaily['totalMXN'] * $pesoToday)) . "€" ?>
        </td>
    </tr>

    <tr>
        <td>
            Renovation
        </td>
        <td style="text-align: center">
            <?php
            if(($totalDaily['countRenovationEUR'] + $totalDaily['countRenovationUSD'] + $totalDaily['countRenovationBRL'] + $totalDaily['countRenovationMXN']) > 0)
                echo toMoney(($totalDaily['totalRenovationEURnoVat'] + ($totalDaily['totalRenovationUSDnoVat'] * $dollarToday) + ($totalDaily['totalRenovationBRLnoVat'] * $realToday) + ($totalDaily['totalRenovationMXNnoVat'] * $pesoToday)) / ($totalDaily['countRenovationEUR'] + $totalDaily['countRenovationUSD'] + $totalDaily['countRenovationBRL'] + $totalDaily['countRenovationMXN']));
            ?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <?php
            if(($totalDaily['countRenovationEUR'] + $totalDaily['countRenovationUSD'] + $totalDaily['countRenovationBRL'] + $totalDaily['countRenovationMXN']) > 0)
                echo toMoney(($totalDaily['totalRenovationEUR'] + ($totalDaily['totalRenovationUSD'] * $dollarToday) + ($totalDaily['totalRenovationBRL'] * $realToday) + ($totalDaily['totalRenovationMXN'] * $pesoToday)) / ($totalDaily['countRenovationEUR'] + $totalDaily['countRenovationUSD'] + $totalDaily['countRenovationBRL'] + $totalDaily['countRenovationMXN']));
            ?>€
        </td>
        <td style="text-align: right;">
            <?php echo $totalDaily['countRenovationEUR'] + $totalDaily['countRenovationUSD'] + $totalDaily['countRenovationBRL'] + $totalDaily['countRenovationMXN']?>
        </td>
        <td style="text-align: right;">
            <?php echo toMoney($totalDaily['totalRenovationEURnoVat'] + ($totalDaily['totalRenovationUSDnoVat'] * $dollarToday) + ($totalDaily['totalRenovationBRLnoVat'] * $realToday) + ($totalDaily['totalRenovationMXNnoVat'] * $pesoToday)) . "€" ?>
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <?php echo toMoney($totalDaily['totalRenovationEUR'] + ($totalDaily['totalRenovationUSD'] * $dollarToday) + ($totalDaily['totalRenovationBRL'] * $realToday) + ($totalDaily['totalRenovationMXN'] * $pesoToday)) . "€" ?>
        </td>
    </tr>

    <tr>
        <td style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px">
            Total
        </td>
        <td style="background-color: greenyellow; text-align: center; font-weight: bold; font-size: 15px">
            <?php
                if($totalDaily['countRenovationUSD'] + $totalDaily['countRenovationBRL'] + $totalDaily['countRenovationMXN'] + $totalDaily['countRenovationEUR'] + $totalDaily['countUSD'] + $totalDaily['countBRL'] + $totalDaily['countMXN'] + $totalDaily['countEUR'] > 0)
                    echo toMoney((($totalDaily['totalUSDnoVat'] * $dollarToday) + ($totalDaily['totalRenovationUSDnoVat'] * $dollarToday) + ($totalDaily['totalBRLnoVat'] * $realToday) + ($totalDaily['totalRenovationBRLnoVat'] * $realToday) + ($totalDaily['totalMXNnoVat'] * $pesoToday) + ($totalDaily['totalRenovationMXNnoVat'] * $pesoToday) + $totalDaily['totalEURnoVat'] + $totalDaily['totalRenovationEURnoVat']) / ($totalDaily['countRenovationUSD'] + $totalDaily['countRenovationBRL'] + $totalDaily['countRenovationMXN'] + $totalDaily['countRenovationEUR'] + $totalDaily['countUSD'] + $totalDaily['countBRL'] + $totalDaily['countMXN'] + $totalDaily['countEUR']));
            ?>€
        </td>
        <td class="withVat" style="text-align: right; display: none; background-color: #D2FF8A; font-weight: bold; font-size: 15px;">
            <?php
            if($totalDaily['countRenovationUSD'] + $totalDaily['countRenovationBRL'] + $totalDaily['countRenovationMXN'] + $totalDaily['countRenovationEUR'] + $totalDaily['countUSD'] + $totalDaily['countBRL'] + $totalDaily['countMXN'] + $totalDaily['countEUR'] > 0)
                echo toMoney((($totalDaily['totalUSD'] * $dollarToday) + ($totalDaily['totalRenovationUSD'] * $dollarToday) + ($totalDaily['totalBRL'] * $realToday) + ($totalDaily['totalRenovationBRL'] * $realToday) + ($totalDaily['totalMXN'] * $pesoToday) + ($totalDaily['totalRenovationMXN'] * $pesoToday) + $totalDaily['totalEUR'] + $totalDaily['totalRenovationEUR']) / ($totalDaily['countRenovationUSD'] + $totalDaily['countRenovationBRL'] + $totalDaily['countRenovationMXN'] + $totalDaily['countRenovationEUR'] + $totalDaily['countUSD'] + $totalDaily['countBRL'] + $totalDaily['countMXN'] + $totalDaily['countEUR']));
            ?>€
        </td>
        <td style="background-color: greenyellow; text-align: right; font-weight: bold; font-size: 15px">
            <?php echo $totalDaily['countRenovationUSD'] + $totalDaily['countRenovationBRL'] + $totalDaily['countRenovationMXN'] + $totalDaily['countRenovationEUR'] + $totalDaily['countUSD'] + $totalDaily['countBRL'] + $totalDaily['countMXN'] + $totalDaily['countEUR']?>
        </td>
        <td style="background-color: greenyellow; text-align: right; font-weight: bold; font-size: 15px">
            <?php echo toMoney($totalDaily['totalUSDnoVat'] * $dollarToday + $totalDaily['totalRenovationUSDnoVat'] * $dollarToday + $totalDaily['totalBRLnoVat'] * $realToday + $totalDaily['totalRenovationBRLnoVat'] * $realToday + $totalDaily['totalMXNnoVat'] * $pesoToday + $totalDaily['totalRenovationMXNnoVat'] * $pesoToday + $totalDaily['totalEURnoVat'] + $totalDaily['totalRenovationEURnoVat']) ?>€
        </td>
        <td class="withVat" style="text-align: right; display: none; background-color: #D2FF8A; font-weight: bold; font-size: 15px;">
            <?php echo toMoney($totalDaily['totalUSD'] * $dollarToday + $totalDaily['totalRenovationUSD'] * $dollarToday + $totalDaily['totalBRL'] * $realToday + $totalDaily['totalRenovationBRL'] * $realToday + $totalDaily['totalMXN'] * $pesoToday + $totalDaily['totalRenovationMXN'] * $pesoToday + $totalDaily['totalEUR'] + $totalDaily['totalRenovationEUR']) ?>€
        </td>
    </tr>

    <tr>
        <td class="applyingRefunds" style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px" colspan="2">
            Applying refunds
        </td>
        <td style="background-color: #ff0000; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo $totalDaily['contRefundUSD'] + $totalDaily['contRefundBRL'] + $totalDaily['contRefundMXN'] + $totalDaily['contRefundEUR']; ?>
        </td>

        <td style="background-color: #ff0000; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo toMoney(($totalDaily['totalRefundUSDnoVat'] * $dollarToday) + ($totalDaily['totalRefundBRLnoVat'] * $realToday) + ($totalDaily['totalRefundMXNnoVat'] * $pesoToday) + $totalDaily['totalRefundEURnoVat']) . "€"; ?>
        </td>
        <td class="withVat" style="background-color: #ff8482; text-align: right; font-weight: bold; font-size: 15px; display: none; color: #ffffff;">
            <?php echo toMoney(($totalDaily['totalRefundUSD'] * $dollarToday) + ($totalDaily['totalRefundBRL'] * $realToday) + ($totalDaily['totalRefundMXN'] * $pesoToday) + $totalDaily['totalRefundEUR']) . "€"; ?>
        </td>
    </tr>

    <tr>
        <td class="totalNet" style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px" colspan="3">
            Total Net
        </td>
        <td style="background-color: green; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo toMoney((($totalDaily['totalUSDnoVat'] * $dollarToday) + ($totalDaily['totalRenovationUSDnoVat'] * $dollarToday) + ($totalDaily['totalBRLnoVat'] * $realToday) + ($totalDaily['totalRenovationBRLnoVat'] * $realToday) + ($totalDaily['totalMXNnoVat'] * $pesoToday) + ($totalDaily['totalRenovationMXNnoVat'] * $pesoToday) + $totalDaily['totalEURnoVat'] + $totalDaily['totalRenovationEURnoVat']) - (($totalDaily['totalRefundUSDnoVat'] * $dollarToday) + ($totalDaily['totalRefundBRLnoVat'] * $realToday) + ($totalDaily['totalRefundMXNnoVat'] * $pesoToday) + $totalDaily['totalRefundEURnoVat'])); ?>€
        </td>
        <td class="withVat" style="background-color: #65af66; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff; display: none;">
            <?php echo toMoney((($totalDaily['totalUSD'] * $dollarToday) + ($totalDaily['totalRenovationUSD'] * $dollarToday) + ($totalDaily['totalBRL'] * $realToday) + ($totalDaily['totalRenovationBRL'] * $realToday) + ($totalDaily['totalMXN'] * $pesoToday) + ($totalDaily['totalRenovationMXN'] * $pesoToday) + $totalDaily['totalEUR'] + $totalDaily['totalRenovationEUR']) - (($totalDaily['totalRefundUSD'] * $dollarToday) + ($totalDaily['totalRefundBRL'] * $realToday) + ($totalDaily['totalRefundMXN'] * $pesoToday) + $totalDaily['totalRefundEUR'])); ?>€
        </td>
    </tr>

</table>

<br /><br />

<table id="salesTable">

    <tr>
        <td style="border: 0px; text-align: center; font-size: 15px; font-weight: bold; padding-bottom: 10px" colspan="4">
            Weekly sales for range <?php echo $weekBefore ?> to <?php echo $endDate; ?><br>
            <span style="font-size: 12px">
                <?php

                switch($dw)
                {
                    case 1:
                        echo "<span style=\"color:green\">[Mon]</span> Tue Wed Thu Fri Sat Sun";
                        break;
                    case 2:
                        echo "<span style=\"color:green\">[Mon Tue]</span> Wed Thu Fri Sat Sun";
                        break;
                    case 3:
                        echo "<span style=\"color:green\">[Mon Tue Wed]</span> Thu Fri Sat Sun";
                        break;
                    case 4:
                        echo "<span style=\"color:green\">[Mon Tue Wed Thu]</span> Fri Sat Sun";
                        break;
                    case 5:
                        echo "<span style=\"color:green\">[Mon Tue Wed Thu Fri]</span> Sat Sun";
                        break;
                    case 6:
                        echo "<span style=\"color:green\">[Mon Tue Wed Thu Fri Sat]</span> Sun";
                        break;
                    case 7:
                        echo "<span style=\"color:green\">[Mon Tue Wed Thu Fri Sat Sun]</span>";
                        break;
                }
                ?>
            </span>
        </td>
    </tr>

    <tr>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            Type of Sale
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Average Ticket
        </td>
        <td class="withVat" style="background-color: #9ebacf; color: #ffffff; font-weight: bold; text-align: center; width: 200px; display: none;">
            Amount with VAT
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Quantity
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Amount
        </td>
        <td class="withVat" style="background-color: #9ebacf; color: #ffffff; font-weight: bold; text-align: center; width: 200px; display: none;">
            Amount with  VAT
        </td>
    </tr>

    <tr>
        <td>
            New sale
        </td>
        <td style="text-align: center">
            <?php
            if(($totalWeekly['countEUR'] + $totalWeekly['countBRL'] + $totalWeekly['countMXN'] + $totalWeekly['countUSD'])>0)
                echo toMoney(($totalWeekly['totalEURnoVat'] + ($totalWeekly['totalUSDnoVat'] * $dollarToday) + ($totalWeekly['totalBRLnoVat'] * $realToday) + ($totalWeekly['totalMXNnoVat'] * $pesoToday)) / ($totalWeekly['countEUR'] + $totalWeekly['countUSD'] + $totalWeekly['countBRL'] + $totalWeekly['countMXN']));
            ?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <?php
            if(($totalWeekly['countEUR'] + $totalWeekly['countBRL'] + $totalWeekly['countMXN'] + $totalWeekly['countUSD'])>0)
                echo toMoney(($totalWeekly['totalEUR'] + ($totalWeekly['totalUSD'] * $dollarToday) + ($totalWeekly['totalBRL'] * $realToday) + ($totalWeekly['totalMXN'] * $pesoToday)) / ($totalWeekly['countEUR'] + $totalWeekly['countUSD'] + $totalWeekly['countBRL'] + $totalWeekly['countMXN']));
            ?>€
        </td>
        <td style="text-align: right;">
            <?php echo $totalWeekly['countEUR'] + $totalWeekly['countUSD'] + $totalWeekly['countBRL'] + $totalWeekly['countMXN']; ?>
        </td>
        <td style="text-align: right;">
            <?php echo toMoney($totalWeekly['totalEURnoVat'] + ($totalWeekly['totalUSDnoVat'] * $dollarToday) + ($totalWeekly['totalBRLnoVat'] * $realToday) + ($totalWeekly['totalMXNnoVat'] * $pesoToday)) . "€"; ?>
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <?php echo toMoney($totalWeekly['totalEUR'] + ($totalWeekly['totalUSD'] * $dollarToday) + ($totalWeekly['totalBRL'] * $realToday) + ($totalWeekly['totalMXN'] * $pesoToday)) . "€"; ?>
        </td>
    </tr>

    <tr>
        <td>
            Renovation
        </td>
        <td style="text-align: center">
            <?php
            if(($totalWeekly['countRenovationEUR'] + $totalWeekly['countRenovationUSD'] + $totalWeekly['countRenovationBRL'] + $totalWeekly['countRenovationMXN']) > 0)
                echo toMoney(($totalWeekly['totalRenovationEURnoVat'] + ($totalWeekly['totalRenovationUSDnoVat'] * $dollarToday) + ($totalWeekly['totalRenovationBRLnoVat'] * $realToday) + ($totalWeekly['totalRenovationMXNnoVat'] * $pesoToday)) / ($totalWeekly['countRenovationEUR'] + $totalWeekly['countRenovationUSD'] + $totalWeekly['countRenovationBRL'] + $totalWeekly['countRenovationMXN']));
            ?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <?php
            if(($totalWeekly['countRenovationEUR'] + $totalWeekly['countRenovationUSD'] + $totalWeekly['countRenovationBRL'] + $totalWeekly['countRenovationMXN']) > 0)
                echo toMoney(($totalWeekly['totalRenovationEUR'] + ($totalWeekly['totalRenovationUSD'] * $dollarToday) + ($totalWeekly['totalRenovationBRL'] * $realToday) + ($totalWeekly['totalRenovationMXN'] * $pesoToday)) / ($totalWeekly['countRenovationEUR'] + $totalWeekly['countRenovationUSD'] + $totalWeekly['countRenovationBRL'] + $totalWeekly['countRenovationMXN']));
            ?>€
        </td>
        <td style="text-align: right;">
            <?php echo $totalWeekly['countRenovationEUR'] + $totalWeekly['countRenovationUSD'] + $totalWeekly['countRenovationBRL'] + $totalWeekly['countRenovationMXN']; ?>
        </td>
        <td style="text-align: right;">
            <?php echo toMoney($totalWeekly['totalRenovationEURnoVat'] + ($totalWeekly['totalRenovationUSDnoVat'] * $dollarToday) + ($totalWeekly['totalRenovationBRLnoVat'] * $realToday) + ($totalWeekly['totalRenovationMXNnoVat'] * $pesoToday)) . "€"; ?>
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <?php echo toMoney($totalWeekly['totalRenovationEUR'] + ($totalWeekly['totalRenovationUSD'] * $dollarToday) + ($totalWeekly['totalRenovationBRL'] * $realToday) + ($totalWeekly['totalRenovationMXN'] * $pesoToday)) . "€"; ?>
        </td>
    </tr>

    <tr>
        <td style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px">
            Total
        </td>
        <td style="background-color: greenyellow; text-align: center; font-weight: bold; font-size: 15px">
            <?php
            if($totalWeekly['countRenovationUSD'] + $totalWeekly['countRenovationBRL'] + $totalWeekly['countRenovationMXN'] + $totalWeekly['countRenovationEUR'] + $totalWeekly['countUSD'] + $totalWeekly['countBRL'] + $totalWeekly['countMXN'] + $totalWeekly['countEUR'] > 0)
                echo toMoney((($totalWeekly['totalUSDnoVat'] * $dollarToday) + ($totalWeekly['totalRenovationUSDnoVat'] * $dollarToday) + ($totalWeekly['totalBRLnoVat'] * $realToday) + ($totalWeekly['totalRenovationBRLnoVat'] * $realToday) + ($totalWeekly['totalMXNnoVat'] * $pesoToday) + ($totalWeekly['totalRenovationMXNnoVat'] * $pesoToday) + $totalWeekly['totalEURnoVat'] + $totalWeekly['totalRenovationEURnoVat']) / ($totalWeekly['countRenovationUSD'] + $totalWeekly['countRenovationBRL'] + $totalWeekly['countRenovationMXN'] + $totalWeekly['countRenovationEUR'] + $totalWeekly['countUSD'] + $totalWeekly['countBRL'] + $totalWeekly['countMXN'] + $totalWeekly['countEUR']));
            ?>€
        </td>
        <td class="withVat" style="text-align: right; display: none; background-color: #D2FF8A; font-weight: bold; font-size: 15px;">
            <?php
            if($totalWeekly['countRenovationUSD'] + $totalWeekly['countRenovationBRL'] + $totalWeekly['countRenovationMXN'] + $totalWeekly['countRenovationEUR'] + $totalWeekly['countUSD'] + $totalWeekly['countBRL'] + $totalWeekly['countMXN'] + $totalWeekly['countEUR'] > 0)
                echo toMoney((($totalWeekly['totalUSD'] * $dollarToday) + ($totalWeekly['totalRenovationUSD'] * $dollarToday) + ($totalWeekly['totalBRL'] * $realToday) + ($totalWeekly['totalRenovationBRL'] * $realToday) + ($totalWeekly['totalMXN'] * $pesoToday) + ($totalWeekly['totalRenovationMXN'] * $pesoToday) + $totalWeekly['totalEUR'] + $totalWeekly['totalRenovationEUR']) / ($totalWeekly['countRenovationUSD'] + $totalWeekly['countRenovationBRL'] + $totalWeekly['countRenovationMXN'] + $totalWeekly['countRenovationEUR'] + $totalWeekly['countUSD'] + $totalWeekly['countBRL'] + $totalWeekly['countMXN'] + $totalWeekly['countEUR']));
            ?>€
        </td>
        <td style="background-color: greenyellow; text-align: right; font-weight: bold; font-size: 15px">
            <?php echo $totalWeekly['countRenovationUSD'] + $totalWeekly['countRenovationBRL'] + $totalWeekly['countRenovationMXN'] + $totalWeekly['countRenovationEUR'] + $totalWeekly['countUSD'] + $totalWeekly['countBRL'] + $totalWeekly['countMXN'] + $totalWeekly['countEUR']?>
        </td>
        <td style="background-color: greenyellow; text-align: right; font-weight: bold; font-size: 15px">
            <?php echo toMoney(($totalWeekly['totalUSDnoVat'] * $dollarToday) + ($totalWeekly['totalRenovationUSDnoVat'] * $dollarToday) + ($totalWeekly['totalBRLnoVat'] * $realToday) + ($totalWeekly['totalRenovationBRLnoVat'] * $realToday) + ($totalWeekly['totalMXNnoVat'] * $pesoToday) + ($totalWeekly['totalRenovationMXNnoVat'] * $pesoToday) + $totalWeekly['totalEURnoVat'] + $totalWeekly['totalRenovationEURnoVat']); ?>€
        </td>
        <td class="withVat" style="text-align: right; display: none; background-color: #D2FF8A; font-weight: bold; font-size: 15px;">
            <?php echo toMoney(($totalWeekly['totalUSD'] * $dollarToday) + ($totalWeekly['totalRenovationUSD'] * $dollarToday) + ($totalWeekly['totalBRL'] * $realToday) + ($totalWeekly['totalRenovationBRL'] * $realToday) + ($totalWeekly['totalMXN'] * $pesoToday) + ($totalWeekly['totalRenovationMXN'] * $pesoToday) + $totalWeekly['totalEUR'] + $totalWeekly['totalRenovationEUR']); ?>€
        </td>
    </tr>

    <tr>
        <td class="applyingRefunds" style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px" colspan="2">
            Applying refunds
        </td>
        <td style="background-color: #ff0000; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo $totalWeekly['contRefundUSD'] + $totalWeekly['contRefundBRL'] + $totalWeekly['contRefundMXN'] + $totalWeekly['contRefundEUR']; ?>
        </td>
        <td style="background-color: #ff0000; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo toMoney(($totalWeekly['totalRefundUSDnoVat'] * $dollarToday) + ($totalWeekly['totalRefundBRLnoVat'] * $realToday) + ($totalWeekly['totalRefundMXNnoVat'] * $pesoToday) + $totalWeekly['totalRefundEURnoVat']) . "€"; ?>
        </td>
        <td class="withVat" style="background-color: #ff8482; text-align: right; font-weight: bold; font-size: 15px; display: none; color: #ffffff;">
            <?php echo toMoney(($totalWeekly['totalRefundUSD'] * $dollarToday) + ($totalWeekly['totalRefundBRL'] * $realToday) + ($totalWeekly['totalRefundMXN'] * $pesoToday) + $totalWeekly['totalRefundEUR']) . "€"; ?>
        </td>
    </tr>

    <tr>
        <td class="totalNet" style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px" colspan="3">
            Total Net
        </td>
        <td style="background-color: green; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo toMoney((($totalWeekly['totalUSDnoVat'] * $dollarToday) + ($totalWeekly['totalRenovationUSDnoVat'] * $dollarToday) + ($totalWeekly['totalBRLnoVat'] * $realToday) + ($totalWeekly['totalRenovationBRLnoVat'] * $realToday) + ($totalWeekly['totalMXNnoVat'] * $pesoToday) + ($totalWeekly['totalRenovationMXNnoVat'] * $pesoToday) + $totalWeekly['totalEURnoVat'] + $totalWeekly['totalRenovationEURnoVat']) - (($totalWeekly['totalRefundUSDnoVat'] * $dollarToday) + $totalWeekly['totalRefundEURnoVat'] + ($totalWeekly['totalRefundBRLnoVat'] * $realToday) + ($totalWeekly['totalRefundMXNnoVat'] * $pesoToday))); ?>€
        </td>
        <td class="withVat" style="background-color: #65af66; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff; display: none;">
            <?php echo toMoney((($totalWeekly['totalUSD'] * $dollarToday) + ($totalWeekly['totalRenovationUSD'] * $dollarToday) + ($totalWeekly['totalBRL'] * $realToday) + ($totalWeekly['totalRenovationBRL'] * $realToday) + ($totalWeekly['totalMXN'] * $pesoToday) + ($totalWeekly['totalRenovationMXN'] * $pesoToday) + $totalWeekly['totalEUR'] + $totalWeekly['totalRenovationEUR']) - (($totalWeekly['totalRefundUSD'] * $dollarToday) + $totalWeekly['totalRefundEUR'] + ($totalWeekly['totalRefundBRL'] * $realToday) + ($totalWeekly['totalRefundMXN'] * $pesoToday))); ?>€
        </td>
    </tr>

</table>

<div style="text-align: right;">
    <?php
        echo "<span style=\"font-size:11px;\">* 1$ today is about ".$dollarToday."€</span><br />";
        echo "<span style=\"font-size:11px;\">* 1<span style='font-size:8px'>BRL</span> today is about ".$realToday."€</span><br />";
        echo "<span style=\"font-size:11px;\">* 1<span style='font-size:8px'>MXN</span> today is about ".$pesoToday."€</span>";
    ?>
</div>

<script>
    function showVat()
    {
        $('.withVat').toggle();
        if($('#showVatBut').text()=='Show VAT')
        {
            $('#showVatBut').text('Hidde Vat');
            $('.applyingRefunds').attr('colspan',3);
            $('.totalNet').attr('colspan',4);
        }
        else
        {
            $('#showVatBut').text('Show VAT');
            $('.applyingRefunds').attr('colspan',2);
            $('.totalNet').attr('colspan',3);
        }
    }


</script>
