<?php
/* @var $this AbaUserProductsStampController */
/* @var $model AbaUserProductsStamp */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aba-user-products-stamp-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'userId'); ?>
		<?php echo $form->textField($model,'userId'); ?>
		<?php echo $form->error($model,'userId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idProduct'); ?>
		<?php echo $form->textField($model,'idProduct',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'idProduct'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idCountry'); ?>
		<?php echo $form->textField($model,'idCountry',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'idCountry'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idPeriodPay'); ?>
		<?php echo $form->textField($model,'idPeriodPay',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'idPeriodPay'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'priceOfficialCry'); ?>
		<?php echo $form->textField($model,'priceOfficialCry',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'priceOfficialCry'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idCurrency'); ?>
		<?php echo $form->textField($model,'idCurrency',array('size'=>3,'maxlength'=>3)); ?>
		<?php echo $form->error($model,'idCurrency'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dateCreation'); ?>
		<?php echo $form->textField($model,'dateCreation'); ?>
		<?php echo $form->error($model,'dateCreation'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idPromoCode'); ?>
		<?php echo $form->textField($model,'idPromoCode',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'idPromoCode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'upToDatePaid'); ?>
		<?php echo $form->textField($model,'upToDatePaid'); ?>
		<?php echo $form->error($model,'upToDatePaid'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->