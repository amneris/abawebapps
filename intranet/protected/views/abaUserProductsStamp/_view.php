<?php
/* @var $this AbaUserProductsStampController */
/* @var $data AbaUserProductsStamp */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userId')); ?>:</b>
	<?php echo CHtml::encode($data->userId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idProduct')); ?>:</b>
	<?php echo CHtml::encode($data->idProduct); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCountry')); ?>:</b>
	<?php echo CHtml::encode($data->idCountry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPeriodPay')); ?>:</b>
	<?php echo CHtml::encode($data->idPeriodPay); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('priceOfficialCry')); ?>:</b>
	<?php echo CHtml::encode($data->priceOfficialCry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCurrency')); ?>:</b>
	<?php echo CHtml::encode($data->idCurrency); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('dateCreation')); ?>:</b>
	<?php echo CHtml::encode($data->dateCreation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPromoCode')); ?>:</b>
	<?php echo CHtml::encode($data->idPromoCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('upToDatePaid')); ?>:</b>
	<?php echo CHtml::encode($data->upToDatePaid); ?>
	<br />

	*/ ?>

</div>