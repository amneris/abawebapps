<?php
/* @var $this AbaUserProductsStampController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Aba User Products Stamps',
);

$this->menu=array(
	array('label'=>'Create AbaUserProductsStamp', 'url'=>array('create')),
	array('label'=>'Manage AbaUserProductsStamp', 'url'=>array('admin')),
);
?>

<h1>Aba User Products Stamps</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
