<?php
/* @var $this AbaUserProductsStampController */
/* @var $model AbaUserProductsStamp */

$this->breadcrumbs=array(
	'Aba User Products Stamps'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AbaUserProductsStamp', 'url'=>array('index')),
	array('label'=>'Create AbaUserProductsStamp', 'url'=>array('create')),
	array('label'=>'View AbaUserProductsStamp', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AbaUserProductsStamp', 'url'=>array('admin')),
);
?>

<h1>Update AbaUserProductsStamp <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>