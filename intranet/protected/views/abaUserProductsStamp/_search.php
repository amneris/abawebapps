<?php
/* @var $this AbaUserProductsStampController */
/* @var $model AbaUserProductsStamp */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'userId'); ?>
		<?php echo $form->textField($model,'userId'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idProduct'); ?>
		<?php echo $form->textField($model,'idProduct',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idCountry'); ?>
		<?php echo $form->textField($model,'idCountry',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idPeriodPay'); ?>
		<?php echo $form->textField($model,'idPeriodPay',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'priceOfficialCry'); ?>
		<?php echo $form->textField($model,'priceOfficialCry',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idCurrency'); ?>
		<?php echo $form->textField($model,'idCurrency',array('size'=>3,'maxlength'=>3)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dateCreation'); ?>
		<?php echo $form->textField($model,'dateCreation'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idPromoCode'); ?>
		<?php echo $form->textField($model,'idPromoCode',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'upToDatePaid'); ?>
		<?php echo $form->textField($model,'upToDatePaid'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->