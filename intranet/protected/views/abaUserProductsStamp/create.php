<?php
/* @var $this AbaUserProductsStampController */
/* @var $model AbaUserProductsStamp */

$this->breadcrumbs=array(
	'Aba User Products Stamps'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AbaUserProductsStamp', 'url'=>array('index')),
	array('label'=>'Manage AbaUserProductsStamp', 'url'=>array('admin')),
);
?>

<h1>Create AbaUserProductsStamp</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>