<?php
/* @var $this AbaUserProductsStampController */
/* @var $model AbaUserProductsStamp */

$this->breadcrumbs=array(
	'Aba User Products Stamps'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AbaUserProductsStamp', 'url'=>array('index')),
	array('label'=>'Create AbaUserProductsStamp', 'url'=>array('create')),
	array('label'=>'Update AbaUserProductsStamp', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AbaUserProductsStamp', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AbaUserProductsStamp', 'url'=>array('admin')),
);
?>

<h1>View AbaUserProductsStamp #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'userId',
		'idProduct',
		'idCountry',
		'idPeriodPay',
		'priceOfficialCry',
		'idCurrency',
		'dateCreation',
		'idPromoCode',
		'upToDatePaid',
	),
)); ?>
