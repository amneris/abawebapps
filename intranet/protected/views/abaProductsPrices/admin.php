<?php
/* @var $this AbaProductsPricesController */
/* @var $model AbaProductsPrices */

$this->breadcrumbs=array(
	'Product List',
);

$this->menu=array(
	array('label'=>'List AbaProductsPrices', 'url'=>array('index')),
	array('label'=>'Create AbaProductsPrices', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('aba-products-prices-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Product list & prices</h3>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('application.components.widgets.CGridViewABA', array(
	'id'=>'aba-products-prices-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
                    'prevPageLabel'  => '<',
                    'nextPageLabel'  => '>',
                    'firstPageLabel' => '<<',
                    'lastPageLabel'  => '>>',),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'filterSelector'=>'{filter}',
	'columns'=>array(
		'idProduct',
		'idCountry'=>
		array(               // related city displayed as a link
              'name'=>'idCountry',
              'type'=>'countryList+',
			  'filter'=>HeList::getCountryList(),
          ),
		'descriptionText',		
		'idPeriodPay',
		'priceOfficialCry',
		'enabled'=>
		array( 
			'name'=>'enabled',
            'type'=>'booleanList',
			'filter'=>HeList::booleanList()
          ),
		//'modified',
		//'userType',
		//'hierarchy',
        array(
            'name'=>'visibleWeb',
            'type'=>'booleanList',
            'filter'=>HeList::booleanList()
        ),
        /*
		array(
			'class'=>'CButtonColumnABA',
            'header'=>'Options',
		),
        */
	),
)); ?>
