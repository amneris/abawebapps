<?php
/* @var $this AbaProductsPricesController */
/* @var $model AbaProductsPrices */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aba-products-prices-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <table id="myForm">
        
        <tr>
            <td>
                <?php echo $form->labelEx($model,'idProduct'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'idProduct',array('size'=>15,'maxlength'=>15)); ?>
                <?php echo $form->error($model,'idProduct'); ?>
            </td>
        </tr>
	

	<tr>
            <td>
		<?php echo $form->labelEx($model,'idCountry'); ?>
                 </td>
            <td>
		<?php echo $form->textField($model,'idCountry',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'idCountry'); ?>
	 </td>
        </tr>

	<tr>
            <td>
		<?php echo $form->labelEx($model,'idPeriodPay'); ?>
                 </td>
            <td>
		<?php echo $form->textField($model,'idPeriodPay',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'idPeriodPay'); ?>
	 </td>
        </tr>

	<tr>
            <td>
		<?php echo $form->labelEx($model,'priceOfficialCry'); ?>
                 </td>
            <td>
		<?php echo $form->textField($model,'priceOfficialCry',array('size'=>18,'maxlength'=>18)); ?>
		<?php echo $form->error($model,'priceOfficialCry'); ?>
	 </td>
        </tr>

	<tr>
            <td>
		<?php echo $form->labelEx($model,'enabled'); ?>
                 </td>
            <td>
		<?php echo $form->textField($model,'enabled'); ?>
		<?php echo $form->error($model,'enabled'); ?>
	 </td>
        </tr>

	<tr>
            <td>
		<?php echo $form->labelEx($model,'modified'); ?>
                 </td>
            <td>
		<?php echo $form->textField($model,'modified'); ?>
		<?php echo $form->error($model,'modified'); ?>
	 </td>
        </tr>

	<tr>
            <td>
		<?php echo $form->labelEx($model,'descriptionText'); ?>
                 </td>
            <td>
		<?php echo $form->textField($model,'descriptionText',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'descriptionText'); ?>
	 </td>
        </tr>

	<tr>
            <td>
		<?php echo $form->labelEx($model,'userType'); ?>
                 </td>
            <td>
		<?php echo $form->textField($model,'userType',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'userType'); ?>
	 </td>
        </tr>

	<tr>
            <td>
		<?php echo $form->labelEx($model,'hierarchy'); ?>
                 </td>
            <td>
		<?php echo $form->textField($model,'hierarchy',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'hierarchy'); ?>
	 </td>
        </tr>

	<tr>
            <td>
		<?php echo $form->labelEx($model,'visibleWeb'); ?>
                 </td>
            <td>
		<?php echo $form->textField($model,'visibleWeb'); ?>
		<?php echo $form->error($model,'visibleWeb'); ?>
	 </td>
        </tr>
    
    </table>

	<div class="row buttons" align="center">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->