<?php
/* @var $this AbaProductsPricesController */
/* @var $model AbaProductsPrices */

$this->breadcrumbs=array(
	'Product List'=>array('admin'),
	$model->descriptionText=>array('view','id'=>$model->idProduct),
	'Update',
);

$this->menu=array(
	array('label'=>'List AbaProductsPrices', 'url'=>array('index')),
	array('label'=>'Create AbaProductsPrices', 'url'=>array('create')),
	array('label'=>'View AbaProductsPrices', 'url'=>array('view', 'id'=>$model->idProduct)),
	array('label'=>'Manage AbaProductsPrices', 'url'=>array('admin')),
);
?>

<h3>Update product <b><?php echo $model->descriptionText ?></b></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>