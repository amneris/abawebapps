<?php
/* @var $this AbaProductsPricesController */
/* @var $model AbaProductsPrices */

$this->breadcrumbs=array(
	'Product List'=>array('admin'),
	$model->descriptionText,
);

$this->menu=array(
	array('label'=>'List AbaProductsPrices', 'url'=>array('index')),
	array('label'=>'Create AbaProductsPrices', 'url'=>array('create')),
	array('label'=>'Update AbaProductsPrices', 'url'=>array('update', 'id'=>$model->idProduct)),
	array('label'=>'Delete AbaProductsPrices', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idProduct),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AbaProductsPrices', 'url'=>array('admin')),
);
?>

<h3>Detail for product <b><?php echo $model->descriptionText ?></b></h3>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idProduct',
		'idCountry',
		'idPeriodPay',
		'priceOfficialCry',
		'enabled',
		'modified',
		'descriptionText',
		'userType',
		'hierarchy',
		'visibleWeb',
	),
)); ?>
