<?php
/* @var $this AbaProductsPricesController */
/* @var $model AbaProductsPrices */

$this->breadcrumbs=array(
	'Products Prices'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AbaProductsPrices', 'url'=>array('index')),
	array('label'=>'Manage AbaProductsPrices', 'url'=>array('admin')),
);
?>

<h3>Create AbaProductsPrices</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>