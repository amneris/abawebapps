<?php
/* @var $this AbaUserBackofficeController */
/* @var $model AbaUserBackoffice */

$this->breadcrumbs=array(
	'Backoffice Users'=>array('admin'),
	$model->email=>array('view','id'=>$model->id),
	'Update',
);
?>

<h3>Update backoffice user <b><?php echo $model->email; ?></b></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>