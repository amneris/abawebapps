<?php
/* @var $this AbaUserBackofficeController */
/* @var $model AbaUserBackoffice */

$this->breadcrumbs=array(
	'Backoffice Users'=>array('admin'),
	$model->email,
);

?>

<h3>View backoffice user <b><?php echo $model->email; ?></b></h3>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'email',
		'password',
		'role',
	),
)); ?>
