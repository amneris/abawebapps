<?php
/* @var $this AbaUserBackofficeController */
/* @var $model AbaUserBackoffice */

$this->breadcrumbs=array(
	'Backoffice Users'
);
?>

<h3>Backoffice users</h3>

<?php echo CHtml::link('Create new backoffice user',array('abaUserBackoffice/create')); ?>
<br>

<?php 

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'aba-user-grid',
	'dataProvider'=>$model->search(),
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
                    'prevPageLabel'  => '<',
                    'nextPageLabel'  => '>',
                    'firstPageLabel' => '<<',
                    'lastPageLabel'  => '>>',),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'email',
		'password',
        array(
                    'name' => 'role',
                    'header' => "Role",
                    'value'=>'AbaUserBackoffice::getUserType($data->role)',
                    'filter' => CHtml::listData(AbaUserBackoffice::getUserTypes(), 'id', 'title'),
                    
              ),
		array(
			'class'=>'CButtonColumn',
            'header'=>'Options',
		),
	),
)); 
?>
