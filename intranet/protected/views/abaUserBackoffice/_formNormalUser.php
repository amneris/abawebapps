<?php
/* @var $this AbaUserBackofficeController */
/* @var $model AbaUserBackoffice */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aba-user-backoffice-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
    
    <table id="myForm">
        
        <tr>
            <td>
                <?php echo $form->labelEx($model,'email'); ?>
            </td>
            <td>
                <?php echo $model->email ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <?php echo $form->labelEx($model,'password'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'password',array('size'=>50,'maxlength'=>50)); ?>
                <?php echo $form->error($model,'password'); ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <?php echo $form->labelEx($model,'role'); ?>
            </td>
            <td>
                <?php
                $options = array(1=>'Root', 2=>'Support', 3=>'Teacher', 4=>'Marketing');
                echo $options[$model->role];
                ?>
            </td>
        </tr>
        
    </table>

	<div class="row buttons" align="center">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->