<?php
/* @var $this AbaUserBackofficeController */
/* @var $model AbaUserBackoffice */

$this->breadcrumbs=array(
	'Update password',
);
?>

<h3>Update backoffice password for user <b><?php echo $model->email; ?></b></h3>

<?php if(Yii::app()->user->hasFlash('passwordUpdated')): ?>
        <div class="flash-success">
            <?php echo Yii::app()->user->getFlash('passwordUpdated'); ?>
        </div>

<?php endif; ?>

<?php echo $this->renderPartial('_formNormalUser', array('model'=>$model)); ?>