<?php
/* @var $this AbaUserBackofficeController */
/* @var $model AbaUserBackoffice */

$this->breadcrumbs=array(
	'Backoffice Users'=>array('admin'),
	'Create',
);

?>

<h3>Create backoffice user</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>