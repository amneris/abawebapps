<?php
/* @var $this AbaUserBackofficeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Backoffice Users',
);

?>

<h1>Backoffice users</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
