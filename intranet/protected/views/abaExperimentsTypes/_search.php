<?php
/* @var $this AbaExperimentsTypesController */
/* @var $model AbaExperimentsTypes */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
      'CActiveForm',
      array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
      )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'experimentTypeId'); ?>
        <?php echo $form->textField($model, 'experimentTypeId', array('size' => 5, 'maxlength' => 20)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'experimentTypeIdentifier'); ?>
        <?php echo $form->textField($model, 'experimentTypeIdentifier', array('size' => 30, 'maxlength' => 32)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'experimentTypeDescription'); ?>
        <?php echo $form->textField($model, 'experimentTypeDescription', array('size' => 50, 'maxlength' => 255)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'dateAdd'); ?>
        <?php echo $form->textField($model, 'dateAdd', array('size' => 15, 'maxlength' => 19)); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->