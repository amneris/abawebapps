<?php
/* @var $this AbaExperimentsTypesController */
/* @var $model AbaExperimentsTypes */

$this->breadcrumbs = array(
  'Experiments Types' => array('admin'),
  $model->experimentTypeId,
);

$this->menu = array(
  array('label' => 'List Experiments Types', 'url' => array('index')),
  array('label' => 'Create Experiment Type', 'url' => array('create')),
  array('label' => 'Update Experiment Type', 'url' => array('update', 'id' => $model->experimentTypeId)),
  array(
    'label' => 'Delete Experiment Type',
    'url' => '#',
    'linkOptions' => array(
      'submit' => array('delete', 'id' => $model->experimentTypeId),
      'confirm' => 'Are you sure you want to delete this item?'
    )
  ),
  array('label' => 'Manage Experiments Types', 'url' => array('admin')),
);
?>

<h3>View Experiments Types <b><?php echo $model->experimentTypeId . ": " . $model->experimentTypeIdentifier; ?></b></h3>

<?php $this->widget(
  'application.components.widgets.CDetailViewABA',
  array(
    'data' => $model,
    'attributes' => array(
      'experimentTypeId',
      'experimentTypeIdentifier',
      'experimentTypeDescription',
      'dateAdd',
    ),
  )
); ?>
