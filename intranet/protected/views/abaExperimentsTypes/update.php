<?php
/* @var $this AbaExperimentsTypesController */
/* @var $model AbaExperimentsTypes */

$this->breadcrumbs = array(
  'Aba Experiments Types' => array('admin'),
  $model->experimentTypeId => array('view', 'id' => $model->experimentTypeId),
  'Update',
);

$this->menu = array(
  array('label' => 'List Experiments Types', 'url' => array('index')),
  array('label' => 'Create Experiment Type', 'url' => array('create')),
  array('label' => 'View Experiment Type', 'url' => array('view', 'id' => $model->experimentTypeId)),
  array('label' => 'Manage Experiments Types', 'url' => array('admin')),
);
?>

    <h3>Update Experiment Type: <?php echo $model->experimentTypeId; ?></h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>