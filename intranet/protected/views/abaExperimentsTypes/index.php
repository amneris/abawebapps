<?php
/* @var $this AbaExperimentsTypesController */
/* @var $model AbaExperimentsTypes */

$this->breadcrumbs = array(
  'Aba Experiments Types',
);

$this->menu = array(
  array('label' => 'Create Experiments Types', 'url' => array('create')),
  array('label' => 'Manage Experiments Types', 'url' => array('admin')),
);
?>

<h3>Experiments Types</h3>

<?php $this->widget(
  'zii.widgets.CListView',
  array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
  )
); ?>
