<?php
/* @var $this AbaExperimentsTypesController */
/* @var $model AbaExperimentsTypes */
?>

<div class="view">

    <b/><?php echo CHtml::encode($data->getAttributeLabel('experimentTypeId')); ?>:</b>
    <?php echo CHtml::encode($data->experimentTypeId); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('experimentTypeIdentifier')); ?>:</b>
    <?php echo CHtml::encode($data->experimentTypeIdentifier); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('experimentTypeIdentifier')); ?>:</b>
    <?php echo CHtml::encode($data->experimentTypeIdentifier); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('experimentTypeDescription')); ?>:</b>
    <?php echo CHtml::encode($data->experimentTypeDescription); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('dateAdd')); ?>:</b>
    <?php echo CHtml::encode($data->dateAdd); ?>
    <br/>

</div>