<?php
/* @var $this AbaExperimentsTypesController */
/* @var $model AbaExperimentsTypes */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget(
      'CActiveForm',
      array(
        'id' => 'aba-experiments-types-form',
        'enableAjaxValidation' => false,
      )
    );

    $cssPrefix = (isset($wAction) AND $wAction == 'update' ? 'Cm' : '');

    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <table id="myForm">
        <?php if (is_numeric($model->experimentTypeId)): ?>
            <tr>
                <td>
                    <?php echo $form->labelEx($model, 'experimentTypeId'); ?>
                </td>
                <td>
                    <?php echo $model->experimentTypeId; ?>
                    <?php echo $form->error($model, 'experimentTypeId'); ?>
                </td>
            </tr>
        <?php endif; ?>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'experimentTypeIdentifier'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'experimentTypeIdentifier',
                  array('size' => 30, 'maxlength' => 32)); ?>
                <?php echo $form->error($model, 'experimentTypeIdentifier'); ?>
            </td>
        </tr>        <tr>
            <td>
                <?php echo $form->labelEx($model, 'experimentTypeDescription'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'experimentTypeDescription',
                  array('size' => 50, 'maxlength' => 150)); ?>
                <?php echo $form->error($model, 'experimentTypeDescription'); ?>
            </td>
        </tr>
        <?php if (is_numeric($model->experimentTypeId)): ?>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'dateAdd'); ?>
            </td>
            <td>
                <?php
                echo $form->textField($model, 'dateAdd', array('size' => 15, 'maxlength' => 19));
                echo CHtml::image(
                  "images/calendar.jpg",
                  "calendar0",
                  array("id" => "c_dateAdd", "class" => "pointer")
                );
                $this->widget(
                  'application.extensions.calendar.SCalendar',
                  array(
                    'inputField' => $cssPrefix . 'AbaExperimentsTypes_dateAdd',
                    'button' => 'c_dateAdd',
                    'ifFormat' => '%Y-%m-%d %H:%M:%S',
                  )
                );
                ?>
                <?php echo $form->error($model, 'dateAdd'); ?>
            </td>
        </tr>
        <?php endif; ?>
    </table>
    <div class="row buttons" align="center">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->