<?php
/* @var $this AbaExperimentsTypesController */
/* @var $model AbaExperimentsTypes */

$this->breadcrumbs = array(
  'Experiments Types' => array('admin'),
  'Create',
);

$this->menu = array(
  array('label' => 'Manage Experiments Types', 'url' => array('admin')),
);
?>

    <h3>Experiments Types</h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>