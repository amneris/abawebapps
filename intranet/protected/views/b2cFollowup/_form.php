<?php
/* @var $this B2cFollowupController */
/* @var $model B2cFollowup */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'b2c-followup-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'themeid'); ?>
		<?php echo $form->textField($model,'themeid',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'themeid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'userid'); ?>
		<?php echo $form->textField($model,'userid',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'userid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lastchange'); ?>
		<?php echo $form->textField($model,'lastchange'); ?>
		<?php echo $form->error($model,'lastchange'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'all_por'); ?>
		<?php echo $form->textField($model,'all_por',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'all_por'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'all_err'); ?>
		<?php echo $form->textField($model,'all_err',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'all_err'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'all_ans'); ?>
		<?php echo $form->textField($model,'all_ans',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'all_ans'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'all_time'); ?>
		<?php echo $form->textField($model,'all_time',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'all_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sit_por'); ?>
		<?php echo $form->textField($model,'sit_por',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'sit_por'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stu_por'); ?>
		<?php echo $form->textField($model,'stu_por',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'stu_por'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dic_por'); ?>
		<?php echo $form->textField($model,'dic_por',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'dic_por'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dic_ans'); ?>
		<?php echo $form->textField($model,'dic_ans',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'dic_ans'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dic_err'); ?>
		<?php echo $form->textField($model,'dic_err',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'dic_err'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rol_por'); ?>
		<?php echo $form->textField($model,'rol_por',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'rol_por'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gra_por'); ?>
		<?php echo $form->textField($model,'gra_por',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'gra_por'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gra_ans'); ?>
		<?php echo $form->textField($model,'gra_ans',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'gra_ans'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gra_err'); ?>
		<?php echo $form->textField($model,'gra_err',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'gra_err'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wri_por'); ?>
		<?php echo $form->textField($model,'wri_por',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'wri_por'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wri_ans'); ?>
		<?php echo $form->textField($model,'wri_ans',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'wri_ans'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wri_err'); ?>
		<?php echo $form->textField($model,'wri_err',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'wri_err'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'new_por'); ?>
		<?php echo $form->textField($model,'new_por',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'new_por'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'spe_por'); ?>
		<?php echo $form->textField($model,'spe_por',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'spe_por'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'spe_ans'); ?>
		<?php echo $form->textField($model,'spe_ans',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'spe_ans'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'time_aux'); ?>
		<?php echo $form->textField($model,'time_aux',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'time_aux'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gra_vid'); ?>
		<?php echo $form->textField($model,'gra_vid',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'gra_vid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rol_on'); ?>
		<?php echo $form->textField($model,'rol_on',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'rol_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'exercises'); ?>
		<?php echo $form->textField($model,'exercises',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'exercises'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'eva_por'); ?>
		<?php echo $form->textField($model,'eva_por',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'eva_por'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->