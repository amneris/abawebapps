<?php
/* @var $this B2cFollowupController */
/* @var $model b2cFollowup */
/* @var $totalsPercent Array() */

$this->breadcrumbs=array(
    'B2C Study Report'=>array('admin&level='.$level.'&criterio='.$criterio.'&defaultPartner='.$defaultPartner.'&defaultPartnerGroup='.$defaultPartnerGroup),
    $model->email,
);

$progressBarStyle="width:100%; height:12px; float:left; background:#FFFFFF";
?>

<h3>B2C Study Report</h3>

<table id="myForm">

<tr>
    <td colspan="2" style="text-align: center; padding: 20px">
        <span style="font-size: 15px; font-weight: bold"><?php echo $model->getAttributeLabel('themeid')." "; echo $model->themeid; ?></span><br>
        <?php echo '<b style="font-size: 10px"> ('.$model->getAttributeLabel('idFollowup').'</b>' ?> <?php echo '<span style="font-size: 10px">'.$model->idFollowup.')</span>'; ?>
    </td>
</tr>

<tr>
    <td style="width: 120px">
        <?php echo '<b>'.$model->getAttributeLabel('name').'</b>' ?>
    </td>
    <td>
        <?php echo ucfirst(strtolower($model->name)).', '. ucfirst(strtolower($model->surnames)); ?>
        <?php echo '<br><b style="font-size: 10px"> ('.$model->getAttributeLabel('userid').'</b>' ?> <?php echo '<span style="font-size: 10px">'.$model->userid.')</span>'; ?>
    </td>
</tr>

<tr>
    <td>
        <?php echo '<b>'.$model->getAttributeLabel('email').'</b>' ?>
    </td>
    <td>
        <?php echo $model->email; ?>
    </td>
</tr>

<?php
//don't show telephone if it´s null
if($model->telephone)
{
    echo '<tr><td><b>';
    echo $model->getAttributeLabel('telephone');
    echo '</b></td><td>';
    echo $model->telephone;
    echo '</td></tr>';
}
?>

<tr>
    <td>
        <?php echo '<b>'.$model->getAttributeLabel('entryDate').'</b>' ?>
    </td>
    <td>
        <?php echo $model->entryDate; ?>
    </td>
</tr>

<tr>
    <td>
        <?php echo '<b>'.$model->getAttributeLabel('expirationDate').'</b>' ?>
    </td>
    <td>
        <?php echo $model->expirationDate; ?>
    </td>
</tr>

<tr>
    <td>
        <?php echo '<b>'.$model->getAttributeLabel('lastchange').'</b>' ?>
    </td>
    <td>
        <?php echo $model->lastchange; ?>
    </td>
</tr>
<tr>
    <td>
        <?php echo '<b>'.$model->getAttributeLabel('userType').'</b>' ?>
    </td>
    <td>
        <?php echo $model->userType; ?>
    </td>
</tr>
<tr>
    <td colspan="2" style="text-align: center; padding: 20px">
        <span style="font-size: 15px; font-weight: bold">Resumen</span>
    </td>
</tr>

<tr>
    <td style="padding-top: 15px; padding-bottom: 15px">
        <?php echo '<b>'.$model->getAttributeLabel('sit_por').$totalsPercent['sitPercent'].'</b>' ?>
    </td>
    <td>
        <?php
        $this->createWidget("zii.widgets.jui.CJuiProgressBar",array(
            "value"=>intval($totalsPercent['sitPercent']),
            "htmlOptions"=>array(
                "style"=>$progressBarStyle,
            ),
        ))->run()
        ?>
    </td>
</tr>

<tr>
    <td style="padding-top: 15px; padding-bottom: 15px">
        <?php echo '<b>'.$model->getAttributeLabel('stu_por').$totalsPercent['stuPercent'].'</b>' ?>
    </td>
    <td>
        <?php
        $this->createWidget("zii.widgets.jui.CJuiProgressBar",array(
            "value"=>intval($totalsPercent['stuPercent']),
            "htmlOptions"=>array(
                "style"=>$progressBarStyle,
            ),
        ))->run()
        ?>
    </td>
</tr>

<tr>
    <td style="padding-top: 15px; padding-bottom: 15px">
        <?php echo '<b>'.$model->getAttributeLabel('dic_por').$totalsPercent['dicPercent'].'</b>' ?>
    </td>
    <td>
        <?php
        $this->createWidget("zii.widgets.jui.CJuiProgressBar",array(
            "value"=>intval($totalsPercent['dicPercent']),
            "htmlOptions"=>array(
                "style"=>$progressBarStyle,
            ),
        ))->run()
        ?>
        <br>
        <?php echo '<b style="color:red">'.$model->getAttributeLabel('dic_err').'</b>: '.$model->dic_err?>
        <?php echo '<b style="color:green">'.$model->getAttributeLabel('dic_ans').'</b>: '.$model->dic_ans?>
    </td>
</tr>

<tr>
    <td style="padding-top: 15px; padding-bottom: 15px">
        <?php echo '<b>'.$model->getAttributeLabel('rol_por').$totalsPercent['rolPercent'].'</b>' ?>
    </td>
    <td>
        <?php
        $this->createWidget("zii.widgets.jui.CJuiProgressBar",array(
            "value"=>intval($totalsPercent['rolPercent']),
            "htmlOptions"=>array(
                "style"=>$progressBarStyle,
            ),
        ))->run()
        ?>
    </td>
</tr>

<tr>
    <td style="padding-top: 15px; padding-bottom: 15px">
        <?php echo '<b>'.$model->getAttributeLabel('gra_vid').$totalsPercent['gra_vid'].'</b>' ?>
    </td>
    <td>
        <?php
        $this->createWidget("zii.widgets.jui.CJuiProgressBar",array(
            "value"=>intval($totalsPercent['gra_vid']),
            "htmlOptions"=>array(
                "style"=>$progressBarStyle,
            ),
        ))->run()
        ?>
        <br>
        <?php echo '<b style="color:red">'.$model->getAttributeLabel('gra_err').'</b>: '.$model->gra_err?>
        <?php echo '<b style="color:green">'.$model->getAttributeLabel('gra_ans').'</b>: '.$model->gra_ans?>
    </td>
</tr>

<tr>
    <td style="padding-top: 15px; padding-bottom: 15px">
        <?php echo '<b>'.$model->getAttributeLabel('wri_por').$totalsPercent['wriPercent'].'</b>' ?>
    </td>
    <td>
        <?php
        $this->createWidget("zii.widgets.jui.CJuiProgressBar",array(
            "value"=>intval($totalsPercent['wriPercent']),
            "htmlOptions"=>array(
                "style"=>$progressBarStyle,
            ),
        ))->run()
        ?>
        <br>
        <?php echo '<b style="color:red">'.$model->getAttributeLabel('wri_err').'</b>: '.$model->wri_err?>
        <?php echo '<b style="color:green">'.$model->getAttributeLabel('wri_ans').'</b>: '.$model->wri_ans?>
    </td>
</tr>

<tr>
    <td style="padding-top: 15px; padding-bottom: 15px">
        <?php echo '<b>'.$model->getAttributeLabel('new_por').$totalsPercent['newPercent'].'</b>' ?>
    </td>
    <td>
        <?php
        $this->createWidget("zii.widgets.jui.CJuiProgressBar",array(
            "value"=>intval($totalsPercent['newPercent']),
            "htmlOptions"=>array(
                "style"=>$progressBarStyle,
            ),
        ))->run()
        ?>
    </td>
</tr>

<tr>
    <td style="padding-top: 15px; padding-bottom: 15px">
        <?php echo '<b>'.$model->getAttributeLabel('eva_por').'</b>' ?>
    </td>
    <td>
        <?php

        for($i=1;$i<=$model->eva_por;$i++)
            echo CHtml::image(Yii::app()->request->baseUrl . '/images/star.png');

        for($i;$i<=10;$i++)
            echo CHtml::image(Yii::app()->request->baseUrl . '/images/starB&W.png');

        echo "<br><b style=\"font-size: 10px\">(score: ".$totalsPercent['evaPercent']."/10)</b>";
        ?>

    </td>
</tr>

<tr>
    <td style="padding: 30px">
        <?php echo '<b>'.$model->getAttributeLabel('all_por').$totalsPercent['total'].'</b>' ?>
    </td>
    <td>
        <?php
        $this->createWidget("zii.widgets.jui.CJuiProgressBar",array(
            "value"=>intval($totalsPercent['total']),
            'cssFile' => 'themeRed.css',
            'theme'=>'red',
            "htmlOptions"=>array(
                "style"=>$progressBarStyle,
            ),
        ))->run()
        ?>
        <br>
        <?php echo '<b style="color:red">'.$model->getAttributeLabel('all_err').'</b>: '.$model->all_err?>
        <?php echo '<b style="color:green">'.$model->getAttributeLabel('all_ans').'</b>: '.$model->all_ans?>
        <?php echo '<b style="color:blue">'.$model->getAttributeLabel('all_time').'</b>: '.$model->all_time?>
    </td>
</tr>

</table>