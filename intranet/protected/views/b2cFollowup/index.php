<?php
/* @var $this B2cFollowupController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'B2c Followups',
);

$this->menu=array(
	array('label'=>'Create B2cFollowup', 'url'=>array('create')),
	array('label'=>'Manage B2cFollowup', 'url'=>array('admin')),
);
?>

<h1>B2c Followups</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
