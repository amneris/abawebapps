<?php
/* @var $this B2cFollowupController */
/* @var $model B2cFollowup */

$this->breadcrumbs=array(
    'B2C Study Report',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('b2c-followup-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<h3>B2C Study Report</h3>

<?php
echo CHtml::form(Yii::app()->createUrl('b2cFollowup/admin'), 'get');
echo "Generate report for level ";
echo CHtml::dropDownList('level', $level, $levelList);
echo " between dates ";
$this->widget('zii.widgets.jui.CJuiDatePicker', array(
    'name' => 'startDate',
    'value' => $startDate,
    'options' => array(
        'dateFormat' => 'yy-mm-dd',
        'changeMonth'=>true,
        'changeYear'=>true,
    ),
    'htmlOptions' => array(
        'size' => '10',
        'maxlength' => '10',
    ),
));
echo " and ";
$this->widget('zii.widgets.jui.CJuiDatePicker', array(
    'name' => 'endDate',
    'value' => $endDate,
    'options' => array(
        'dateFormat' => 'yy-mm-dd',
        'changeMonth'=>true,
        'changeYear'=>true,
    ),
    'htmlOptions' => array(
        'size' => '10',
        'maxlength' => '10',
    ),
));
echo " <i>(check this</i> ";

if($entryDate)
    echo "<input type='checkbox' name='entryDate' value='true' checked/>";
else
    echo "<input type='checkbox' name='entryDate' value='false'/>";
echo " <i>if you like to search by <b>\"entry date\"</b> instead <b>\"expiration date\"</b>)</i> ";

echo "<br><br>";
echo "Select a partner group ";
echo CHtml::dropDownList('defaultPartnerGroup', $defaultPartnerGroup, $dropDownProviderGroup,
    array(
        'ajax' => array(
        'type'=>'POST',
        'url'=>CController::createUrl('B2cFollowup/getGroupPartnerList'),
        'update'=>'#defaultPartner',
        )
    ));

echo " , an specific partner maybe ";
echo CHtml::dropDownList('defaultPartner', $defaultPartner, $dropDownProvider);
echo " , filter by email ";
echo CHtml::textField('criterio', $criterio, array('size'=>'40px','placeholder'=>'emailexample@abaenglish.com'));
echo CHtml::submitButton('Submit',array('onchange'=>'this.form.submit();'));
echo CHtml::endForm();
echo "<br>";
echo CHtml::link('Export units to CSV',array('B2cFollowup/export&level='.$level.'&criterio='.$criterio.'&defaultPartnerGroup='.$defaultPartnerGroup.'&defaultPartner='.$defaultPartner.'&startDate='.$startDate.'&endDate='.$endDate.'&entryDate='.$entryDate));
echo "<br>";
?>
<?php $this->widget('application.components.widgets.CGridViewABA', array(
    'id'=>'b2c-followup-grid',
    'selectableRows' => 1,
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
        'prevPageLabel'  => '<',
        'nextPageLabel'  => '>',
        'firstPageLabel' => '<<',
        'lastPageLabel'  => '>>',),
    'dataProvider'=>$model->findByLevel($level, $pages, $criterio, $defaultPartner, $defaultPartnerGroup, $startDate, $endDate, $entryDate),
    'columns'=>array(

        'name'=>array(
            'header'=>$model->getAttributeLabel('name'),
            'name'=>'name',
        ),
        'surnames'=>array(
            'header'=>$model->getAttributeLabel('surnames'),
            'name'=>'surnames',
        ),
        'email'=>array(
            'header'=>$model->getAttributeLabel('email'),
            'name'=>'email',
        ),
        //'telephone',
        'userType'=>array(
            'header'=>$model->getAttributeLabel('userType'),
            'name'=>'userType',
        ),
        //'currentLevel',
        //'entryDate',
        //'expirationDate',
        //'idFollowup',
        'themeid'=>array(
            'header'=>$model->getAttributeLabel('themeid'),
            'name'=>'themeid',
        ),
        //'userid',
        'lastchange'=>array(
            'header'=>$model->getAttributeLabel('lastchange'),
            'name'=>'lastchange',
        ),
        'all_por'=>array(
            'header'=>$model->getAttributeLabel('all_por'),
            'name'=>'all_por',
        ),
        //'all_err',
        //'all_ans',
        'all_time'=>array(
            'header'=>$model->getAttributeLabel('all_time'),
            'name'=>'all_time',
        ),
        'sit_por'=>array(
            'header'=>$model->getAttributeLabel('sit_por'),
            'name'=>'sit_por',
        ),
        'stu_por'=>array(
            'header'=>$model->getAttributeLabel('stu_por'),
            'name'=>'stu_por',
        ),
        'dic_por'=>array(
            'header'=>$model->getAttributeLabel('dic_por'),
            'name'=>'dic_por',
        ),
        //'dic_ans',
        //'dic_err',
        'rol_por'=>array(
            'header'=>$model->getAttributeLabel('rol_por'),
            'name'=>'rol_por',
        ),
        'gra_por'=>array(
            'header'=>$model->getAttributeLabel('gra_por'),
            'name'=>'gra_por',
        ),
        //'gra_ans',
        //'gra_err',
        'wri_por'=>array(
            'header'=>$model->getAttributeLabel('wri_por'),
            'name'=>'wri_por',
        ),
        //'wri_ans',
        //'wri_err',
        'new_por'=>array(
            'header'=>$model->getAttributeLabel('new_por'),
            'name'=>'new_por',
        ),
        'spe_por'=>array(
            'header'=>$model->getAttributeLabel('spe_por'),
            'name'=>'spe_por',
        ),
        //'spe_ans',
        //'time_aux',
        //'gra_vid',
        //'rol_on',
        //'exercises',
        //'eva_por',
        'View'=>array(
            'header'=>'View',
            'name'=>'idFollowup',
            'type'=>'@View@index.php?r=b2cFollowup/view&id=*idFollowup*&level='.$level.'&criterio='.urlencode($criterio).'&defaultPartner='.$defaultPartner.
                '&defaultPartnerGroup='.$defaultPartnerGroup,
        ),

    ),
)); ?>
