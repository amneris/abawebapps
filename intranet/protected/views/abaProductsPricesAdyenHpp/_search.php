<?php
/* @var $this AbaProductsPricesAdyenHppController */
/* @var $model ProductsPricesAdyenHpp */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget('CActiveForm', array(
      'action' => Yii::app()->createUrl($this->route),
      'method' => 'get',
    )); ?>

    <div class="row">
        <?php echo $form->label($model, 'idProductPrice'); ?>
        <?php echo $form->textField($model, 'idProductPrice', array('size' => 15, 'maxlength' => 15)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'idProduct'); ?>
        <?php echo $form->textField($model, 'idProduct', array('size' => 15, 'maxlength' => 15)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'idPaymentMethod'); ?>
        <?php echo $form->textField($model, 'idPaymentMethod', array('size' => 4, 'maxlength' => 4)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'currencyAdyenHpp'); ?>
        <?php echo $form->textField($model, 'currencyAdyenHpp', array('size' => 18, 'maxlength' => 18)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'priceAdyenHpp'); ?>
        <?php echo $form->textField($model, 'priceAdyenHpp', array('size' => 18, 'maxlength' => 18)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'enabled'); ?>
        <?php echo $form->textField($model, 'enabled', array('size' => 4, 'maxlength' => 4)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'modified'); ?>
        <?php echo $form->textField($model, 'modified', array('size' => 18, 'maxlength' => 18)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'priceAdyenHppExtranet'); ?>
        <?php echo $form->textField($model, 'priceAdyenHppExtranet', array('size' => 18, 'maxlength' => 18)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'enabledExtranet'); ?>
        <?php echo $form->textField($model, 'enabledExtranet', array('size' => 18, 'maxlength' => 18)); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->