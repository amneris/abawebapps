<?php
/* @var $this AbaProductsPricesAdyenHppController */
/* @var $model ProductsPricesAdyenHpp */

$this->breadcrumbs = array(
  'Adyen Products Prices',
);

$this->menu = array(
  array('label' => 'List Adyen Hpp Products Prices', 'url' => array('index')),
  array('label' => 'Create Adyen Hpp Products Prices', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('aba-products-prices-adyen-hpp-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Adyen Hpp Product list & prices</h3>

<?php echo CHtml::link('Create new adyen hpp product price', array('AbaProductsPricesAdyenHpp/create')); ?><br/>
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>

<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
      'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('application.components.widgets.CGridViewABA', array(
  'id' => 'aba-products-prices-adyen-hpp-grid',
  'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
  'pager' => array(
    'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
    'prevPageLabel' => '<',
    'nextPageLabel' => '>',
    'firstPageLabel' => '<<',
    'lastPageLabel' => '>>',
  ),
  'extraInfo' => array('view', 'update'),
  'dataProvider' => $model->search(),
  'filter' => $model,
  'filterSelector' => '{filter}',
  'columns' => array(
    'idProductPrice',
    'idProduct',
    'idPaymentMethod' => array(
      'name' => 'idPaymentMethod',
      'type' => 'adyenHppPaymentsMethodList',
      'filter' => HeList::adyenHppPaymentsMethodList()
    ),
    'currencyAdyenHpp',
    'priceAdyenHpp' =>
      array(
        'name' => 'priceAdyenHpp',
        'type' => '{number_format("*priceAdyenHpp*", 2);'
      ),
    'enabled' =>
      array(
        'name' => 'enabled',
        'type' => 'booleanList',
        'filter' => HeList::booleanList()
      ),
    'modified',
    'priceAdyenHppExtranet' =>
      array(
        'name' => 'priceAdyenHppExtranet',
        'type' => '{number_format("*priceAdyenHppExtranet*", 2);'
      ),
    'enabledExtranet' =>
      array(
        'name' => 'enabledExtranet',
        'type' => 'booleanList',
        'filter' => HeList::booleanList()
      ),
    array(
      'class' => 'CButtonColumnABA',
      'htmlOptions' => array('style' => 'width: 110px;'),
      'header' => 'Options',
    ),
  ),
)); ?>
