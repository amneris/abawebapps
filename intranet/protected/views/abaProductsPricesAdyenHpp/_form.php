<?php
/* @var $this AbaProductsPricesAdyenHppController */
/* @var $model ProductsPricesAdyenHpp */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'aba-products-prices-adyen-hpp-form',
	'enableAjaxValidation' => false,
)); ?>

<?php
$options = HeList::getYesNoAliasBool();
$optionsMethodsList = HeList::adyenHppPaymentsMethodList();
?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <table id="myForm">

        <?php if (is_numeric($model->idProductPrice)): ?>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'idProductPrice'); ?>
            </td>
            <td>
                <?php echo CHtml::encode($model->idProductPrice); ?>
                <?php echo $form->error($model ,'idProductPrice'); ?>
            </td>
        </tr>
        <?php endif; ?>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'idProduct'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'idProduct',array('size' => 20, 'maxlength' => 45)); ?>
                <?php echo $form->error($model ,'idProduct'); ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'idPaymentMethod'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'idPaymentMethod', $optionsMethodsList); ?>
                <?php echo $form->error($model, 'idPaymentMethod'); ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'currencyAdyenHpp'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'currencyAdyenHpp',array('size'=>5, 'maxlength'=>5)); ?>
                <?php echo $form->error($model ,'currencyAdyenHpp'); ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'priceAdyenHpp'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'priceAdyenHpp',array('size'=>10, 'maxlength'=>25)); ?>
                <?php echo $form->error($model ,'priceAdyenHpp'); ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'enabled'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'enabled', $options); ?>
                <?php echo $form->error($model, 'enabled'); ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'modified'); ?>
            </td>
            <td>
                <?php
                echo $form->textField($model, 'modified', array('size' => 22, 'maxlength' => 19));
                echo CHtml::image(
                  "images/calendar.jpg",
                  "calendar0",
                  array("id" => "c_modified", "class" => "pointer")
                );
                $this->widget(
                  'application.extensions.calendar.SCalendar',
                  array(
                    'inputField' => 'ProductsPricesAdyenHpp_modified',
                    'button' => 'c_modified',
                    'ifFormat' => '%Y-%m-%d %H:%M:%S',
                  )
                );
                ?>
                <?php echo $form->error($model, 'modified'); ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'priceAdyenHppExtranet'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'priceAdyenHppExtranet',array('size'=>10, 'maxlength'=>25)); ?>
                <?php echo $form->error($model ,'priceAdyenHppExtranet'); ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'enabledExtranet'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'enabledExtranet', $options); ?>
                <?php echo $form->error($model, 'enabledExtranet'); ?>
            </td>
        </tr>

    </table>

	<div class="row buttons" align="center">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->