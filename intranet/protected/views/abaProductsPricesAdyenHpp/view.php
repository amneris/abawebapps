<?php
/* @var $this AbaProductsPricesAdyenHppController */
/* @var $model ProductsPricesAdyenHpp */

$this->breadcrumbs = array(
  'Adyen Hpp Product' => array('admin'),
  $model->idProductPrice,
);

$this->menu = array(
  array('label' => 'Update Adyen Hpp Product', 'url' => array('update', 'id' => $model->idProductPrice)),
  array('label' => 'Manage Adyen Hpp Product', 'url' => array('admin')),
);

?>
<h3>View product: <?php echo $model->idProduct; ?></h3>

<?php $this->widget('application.components.widgets.CDetailViewABA', array(
  'data' => $model,
  'attributes' => array(
    'idProductPrice',
    'idProduct',
    'idPaymentMethod' =>
      array(
        'name' => 'idPaymentMethod',
        'type' => 'adyenHppPaymentsMethodList',
        'filter' => HeList::adyenHppPaymentsMethodList()
      ),
    'currencyAdyenHpp',
    'priceAdyenHpp',
    'enabled' =>
      array(
        'name' => 'enabled',
        'type' => 'booleanList',
        'filter' => HeList::booleanList()
      ),
    'modified',
    'priceAdyenHppExtranet',
    'enabledExtranet' =>
      array(
        'name' => 'enabledExtranet',
        'type' => 'booleanList',
        'filter' => HeList::booleanList()
      ),
  ),
)); ?>
