<?php
/* @var $this AbaProductsPricesAdyenHppController */
/* @var $model ProductsPricesAdyenHpp */

$this->breadcrumbs = array(
  'Adyen Hpp Product' => array('admin'),
  '[Current Product]' => array('view', 'id' => $model->idProductPrice),
  'Update',
);
?>
<?php if (Yii::app()->user->hasFlash('delError')): ?>
    <?php echo "<br>"; ?>
    <div class="flash-error">
        <?php echo Yii::app()->user->getFlash('delError'); ?>
    </div>

<?php endif; ?>

<h3>Edit product: <?php echo $model->idProduct; ?></h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
