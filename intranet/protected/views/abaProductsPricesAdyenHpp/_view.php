<?php
/* @var $this AbaProductsPricesAdyenHppController */
/* @var $data ProductsPricesAdyenHpp */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('idProductPrice')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->idProductPrice), array('view', 'id' => $data->idProductPrice)); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('idProduct')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->idProduct), array('view', 'id' => $data->idProduct)); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('idPaymentMethod')); ?>:</b>
    <?php echo CHtml::encode($data->idPaymentMethod); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('currencyAdyenHpp')); ?>:</b>
    <?php echo CHtml::encode($data->currencyAdyenHpp); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('priceAdyenHpp')); ?>:</b>
    <?php echo CHtml::encode($data->priceAdyenHpp); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('enabled')); ?>:</b>
    <?php echo CHtml::encode($data->enabled); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('modified')); ?>:</b>
    <?php echo CHtml::encode($data->modified); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('priceAdyenHppExtranet')); ?>:</b>
    <?php echo CHtml::encode($data->priceAdyenHppExtranet); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('enabledExtranet')); ?>:</b>
    <?php echo CHtml::encode($data->enabledExtranet); ?>
    <br/>

</div>