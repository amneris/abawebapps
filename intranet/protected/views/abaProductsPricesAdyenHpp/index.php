<?php
/* @var $this AbaProductsPricesAdyenHppController */
/* @var $dataProvider ProductsPricesAdyenHpp */

$this->breadcrumbs=array(
	'Adyen products prices',
);

$this->menu=array(
	array('label'=>'Create Adyen Hpp Products Prices', 'url'=>array('create')),
	array('label'=>'Manage Adyen Hpp Products Prices', 'url'=>array('admin')),
);
?>

<h3>Adyen products prices</h3>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
