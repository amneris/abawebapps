<?php
/* @var $this AbaProductsPricesAdyenHppController */
/* @var $model ProductsPricesAdyenHpp */

$this->breadcrumbs = array(
  'Adyen Products Prices' => array('admin'),
  'Create',
);

$this->menu = array(
  array('label' => 'List Adyen Hpp Products Prices', 'url' => array('index')),
  array('label' => 'Manage Adyen Hpp Products Prices', 'url' => array('admin')),
);
?>

<h3>Create Adyen Hpp Product</h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
