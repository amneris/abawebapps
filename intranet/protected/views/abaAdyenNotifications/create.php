<?php
/* @var $this AbaAdyenNotificationsController */
/* @var $model AbaPaymentsAdyenNotifications */

$this->breadcrumbs = array(
  'Notifications' => array('admin'),
  'Create',
);

$this->menu = array(
  array('label' => 'Manage Notifications', 'url' => array('admin')),
);
?>

<h3>Notifications</h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>