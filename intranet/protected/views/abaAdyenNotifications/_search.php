<?php
/* @var $this AbaAdyenNotificationsController */
/* @var $model AbaPaymentsAdyenNotifications */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
      'CActiveForm',
      array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
      )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'id'); ?>
        <?php echo $form->textField($model, 'id', array('size' => 5)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'amountCurrency'); ?>
        <?php echo $form->textField($model, 'amountCurrency', array('size' => 5)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'amountValue'); ?>
        <?php echo $form->textField($model, 'amountValue', array('size' => 5)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'eventCode'); ?>
        <?php echo $form->textField($model, 'eventCode', array('size' => 5)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'merchantReference'); ?>
        <?php echo $form->textField($model, 'merchantReference', array('size' => 25)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'originalReference'); ?>
        <?php echo $form->textField($model, 'originalReference', array('size' => 25)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'pspReference'); ?>
        <?php echo $form->textField($model, 'pspReference', array('size' => 25)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'success'); ?>
        <?php echo $form->DropDownList($model, 'success', HeList::booleanList(), array('empty' => '--')); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'paymentMethod'); ?>
        <?php echo $form->textField($model, 'paymentMethod', array('size' => 25)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'operations'); ?>
        <?php echo $form->textField($model, 'operations', array('size' => 25)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'dateAdd'); ?>
        <?php echo $form->textField($model, 'dateAdd', array('size' => 15, 'maxlength' => 19)); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->