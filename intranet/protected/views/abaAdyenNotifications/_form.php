<?php
/* @var $this AbaAdyenNotificationsController */
/* @var $model AbaPaymentsAdyenNotifications */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget(
      'CActiveForm',
      array(
        'id' => 'aba-adyen-notifications-form',
        'enableAjaxValidation' => false,
      )
    );

    $options = HeList::booleanList();

    $cssPrefix = (isset($wAction) AND $wAction == 'update' ? 'Cm' : '');
?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <table id="myForm">
        <?php if (is_numeric($model->id)): ?>
            <tr>
                <td>
                    <?php echo $form->labelEx($model, 'id'); ?>
                </td>
                <td>
                    <?php echo $model->id; ?>
                    <?php echo $form->error($model, 'id'); ?>
                </td>
            </tr>
        <?php endif; ?>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'live'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'live', $options); ?>
                <?php echo $form->error($model, 'live'); ?>
            </td>
        </tr>
        <tr><td><?php echo $form->labelEx($model, 'amountCurrency'); ?></td>
            <td><?php echo $form->textField($model, 'amountCurrency', array('size' => 30, 'maxlength' => 32)); ?>
                <?php echo $form->error($model, 'amountCurrency'); ?></td></tr>
        <tr><td><?php echo $form->labelEx($model, 'amountValue'); ?></td>
            <td><?php echo $form->textField($model, 'amountValue', array('size' => 30, 'maxlength' => 32)); ?>
                <?php echo $form->error($model, 'amountValue'); ?></td></tr>
        <tr><td><?php echo $form->labelEx($model, 'eventCode'); ?></td>
            <td><?php echo $form->textField($model, 'eventCode', array('size' => 30, 'maxlength' => 32)); ?>
                <?php echo $form->error($model, 'eventCode'); ?></td></tr>
        <tr><td><?php echo $form->labelEx($model, 'eventDate'); ?></td>
            <td><?php echo $form->textField($model, 'eventDate', array('size' => 30, 'maxlength' => 32)); ?>
                <?php echo $form->error($model, 'eventDate'); ?></td></tr>
        <tr><td><?php echo $form->labelEx($model, 'merchantAccountCode'); ?></td>
            <td><?php echo $form->textField($model, 'merchantAccountCode', array('size' => 30, 'maxlength' => 32)); ?>
                <?php echo $form->error($model, 'merchantAccountCode'); ?></td></tr>
        <tr><td><?php echo $form->labelEx($model, 'merchantReference'); ?></td>
            <td><?php echo $form->textField($model, 'merchantReference', array('size' => 30, 'maxlength' => 32)); ?>
                <?php echo $form->error($model, 'merchantReference'); ?></td></tr>
        <tr><td><?php echo $form->labelEx($model, 'originalReference'); ?></td>
            <td><?php echo $form->textField($model, 'originalReference', array('size' => 30, 'maxlength' => 32)); ?>
                <?php echo $form->error($model, 'originalReference'); ?></td></tr>
        <tr><td><?php echo $form->labelEx($model, 'pspReference'); ?></td>
            <td><?php echo $form->textField($model, 'pspReference', array('size' => 30, 'maxlength' => 32)); ?>
                <?php echo $form->error($model, 'pspReference'); ?></td></tr>
        <tr><td><?php echo $form->labelEx($model, 'reason'); ?></td>
            <td><?php echo $form->textField($model, 'reason', array('size' => 30, 'maxlength' => 32)); ?>
                <?php echo $form->error($model, 'reason'); ?></td></tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'success'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'success', $options); ?>
                <?php echo $form->error($model, 'success'); ?>
            </td>
        </tr>
        <tr><td><?php echo $form->labelEx($model, 'paymentMethod'); ?></td>
            <td><?php echo $form->textField($model, 'paymentMethod', array('size' => 30, 'maxlength' => 32)); ?>
                <?php echo $form->error($model, 'paymentMethod'); ?></td></tr>
        <tr><td><?php echo $form->labelEx($model, 'operations'); ?></td>
            <td><?php echo $form->textField($model, 'operations', array('size' => 30, 'maxlength' => 32)); ?>
                <?php echo $form->error($model, 'operations'); ?></td></tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'additionalData'); ?>
            </td>
            <td>
                <?php echo $form->textArea($model, 'additionalData', array('rows'=>3, 'cols'=>50)); ?>
                <?php echo $form->error($model, 'additionalData'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'notification'); ?>
            </td>
            <td>
                <?php echo $form->textArea($model, 'notification', array('rows'=>6, 'cols'=>50)); ?>
                <?php echo $form->error($model, 'notification'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'dateAdd'); ?>
            </td>
            <td>
                <?php
                echo $form->textField($model, 'dateAdd', array('size' => 22, 'maxlength' => 19));
                echo CHtml::image(
                  "images/calendar.jpg",
                  "calendar0",
                  array("id" => "c_dateAdd", "class" => "pointer")
                );
                $this->widget(
                  'application.extensions.calendar.SCalendar',
                  array(
                    'inputField' => $cssPrefix . 'AbaPaymentsAdyenNotifications_dateAdd',
                    'button' => 'c_dateAdd',
                    'ifFormat' => '%Y-%m-%d %H:%M:%S',
                  )
                );
                ?>
                <?php echo $form->error($model, 'dateAdd'); ?>
            </td>
        </tr>

    </table>
    <div class="row buttons" align="center">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->