<?php
/* @var $this AbaAdyenNotificationsController */
/* @var $model AbaPaymentsAdyenNotifications */

$this->breadcrumbs = array(
  'Notifications',
);

$this->menu = array(
//  array('label' => 'Create Experiment', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript(
  'search',
  "
   $('.search-button').click(function(){
     $('.search-form').toggle();
     return false;
   });
   $('.search-form form').submit(function(){
     $.fn.yiiGridView.update('aba-adyen-notifications-grid', {
       data: $(this).serialize()
     });
     return false;
   });
   "
);
?>

<h3>Notifications</h3>

<br>
<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none;">
    <?php $this->renderPartial(
      '_search',
      array(
        'model' => $model,
      )
    ); ?>
</div><!-- search-form -->

<?php $this->widget(
  'application.components.widgets.CGridViewABA',
  array(
    'id' => 'aba-adyen-notifications-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager' => array(
      'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
      'prevPageLabel' => '<',
      'nextPageLabel' => '>',
      'firstPageLabel' => '<<',
      'lastPageLabel' => '>>',
    ),
    'extraInfo' => array('view'),
    'dataProvider' => $model->search(),
    'filter' => $model,
    'filterSelector' => '{filter}',
    'columns' => array(
      'id',
      'amountCurrency',
      'amountValue' =>
        array(
          'name' => 'amountValue',
          'type' => '{number_format("*amountValue*", 2);'
        ),
      'eventCode',
      'merchantReference',
      'originalReference',
      'pspReference' =>
        array(
          'name' => 'pspReference',
          'type' => '{HeQuery::formatField("*pspReference*", 45);'
        ),
      'reason' =>
        array(
          'name' => 'reason',
          'type' => '{HeQuery::formatField("*reason*", 45, true, "*eventCode*");'
        ),
      'success' => array(
        'name' => 'success',
        'type' => 'booleanList',
        'filter' => HeList::booleanList()
      ),
      'paymentMethod',
      'operations',
      'dateAdd',
      array(
        'class' => 'CButtonColumnABA',
        'header' => 'Options',
      ),
    ),
  )
); ?>
