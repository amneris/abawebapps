<?php
/* @var $this AbaAdyenNotificationsController */
/* @var $model AbaPaymentsAdyenNotifications */

$this->breadcrumbs = array(
  'Notifications' => array('admin'),
  $model->id,
);

$this->menu = array(
  array('label' => 'List Notifications', 'url' => array('index')),
//  array('label' => 'Create Experiment', 'url' => array('create')),
//  array('label' => 'Update Experiment', 'url' => array('update', 'id' => $model->id)),
  array(
//    'label' => 'Delete Experiment',
//    'url' => '#',
//    'linkOptions' => array(
//      'submit' => array('delete', 'id' => $model->id),
//      'confirm' => 'Are you sure you want to delete this item?'
//    )
  ),
  array('label' => 'Manage Notifications', 'url' => array('admin')),
);
?>

<h3>View Notification <b><?php echo $model->id . ": " . $model->eventCode; ?></b></h3>

<?php $this->widget(
  'application.components.widgets.CDetailViewABA',
  array(
    'data' => $model,
    'attributes' => array(
      'id',
      'live' => array(
        'name' => 'live',
        'type' => 'booleanList',
        'filter' => HeList::booleanList()
      ),
      'amountCurrency',
      'amountValue',
      'eventCode',
      'eventDate',
      'merchantAccountCode',
      'merchantReference',
      'originalReference',
      'pspReference',
      'reason' =>
        array(
          'name' => 'reason',
          'type' => '{HeQuery::formatField("*reason*", 5000, true, "*eventCode*");'
        ),
      'success' => array(
        'name' => 'success',
        'type' => 'booleanList',
        'filter' => HeList::booleanList()
      ),
      'paymentMethod',
      'operations',
      'additionalData',
      'notification',
      'dateAdd',
    ),
  )
); ?>

