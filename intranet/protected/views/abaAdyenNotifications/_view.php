<?php
/* @var $this AbaAdyenNotificationsController */
/* @var $model AbaPaymentsAdyenNotifications */
?>

<div class="view">

    <b/><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
    <?php echo CHtml::encode($data->id); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('live')); ?>:</b>
    <?php echo CHtml::encode($data->live); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('amountCurrency')); ?>:</b>
    <?php echo CHtml::encode($data->amountCurrency); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('amountValue')); ?>:</b>
    <?php echo CHtml::encode($data->amountValue); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('eventCode')); ?>:</b>
    <?php echo CHtml::encode($data->eventCode); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('eventDate')); ?>:</b>
    <?php echo CHtml::encode($data->eventDate); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('merchantAccountCode')); ?>:</b>
    <?php echo CHtml::encode($data->merchantAccountCode); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('merchantReference')); ?>:</b>
    <?php echo CHtml::encode($data->merchantReference); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('originalReference')); ?>:</b>
    <?php echo CHtml::encode($data->originalReference); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('pspReference')); ?>:</b>
    <?php echo CHtml::encode($data->pspReference); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('reason')); ?>:</b>
    <?php echo CHtml::encode($data->reason); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('success')); ?>:</b>
    <?php echo CHtml::encode($data->success); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('paymentMethod')); ?>:</b>
    <?php echo CHtml::encode($data->paymentMethod); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('counterUsers')); ?>:</b>
    <?php echo CHtml::encode($data->counterUsers); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('counterUsers')); ?>:</b>
    <?php echo CHtml::encode($data->counterUsers); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('counterUsers')); ?>:</b>
    <?php echo CHtml::encode($data->counterUsers); ?>
    <br/>

</div>