<?php
/* @var $this AbaAdyenNotificationsController */
/* @var $model AbaPaymentsAdyenNotifications */

$this->breadcrumbs = array(
  'Notifications',
);

$this->menu = array(
//  array('label' => 'Create Notifications', 'url' => array('create')),
//  array('label' => 'Manage Notifications', 'url' => array('admin')),
);
?>

<h3>Notifications</h3>

<?php $this->widget(
  'zii.widgets.CListView',
  array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
  )
); ?>
