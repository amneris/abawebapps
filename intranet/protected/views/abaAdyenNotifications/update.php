<?php
/* @var $this AbaAdyenNotificationsController */
/* @var $model AbaPaymentsAdyenNotifications */

$this->breadcrumbs = array(
  'Notifications' => array('admin'),
  $model->id => array('view', 'id' => $model->id),
  'Update',
);

$this->menu = array(
//  array('label' => 'List Notifications', 'url' => array('index')),
//  array('label' => 'Create Experiment', 'url' => array('create')),
//  array('label' => 'View Experiment', 'url' => array('view', 'id' => $model->id)),
//  array('label' => 'Manage Notifications', 'url' => array('admin')),
);
?>

<h3>Update Notification: <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>