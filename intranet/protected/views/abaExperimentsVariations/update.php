<?php
/* @var $this AbaExperimentsVariationsController */
/* @var $model AbaExperimentsVariations */

$this->breadcrumbs = array(
  'Experiments variations' => array('admin'),
  $model->experimentVariationId => array('view', 'id' => $model->experimentVariationId),
  'Update',
);

$this->menu = array(
  array('label' => 'List Experiments variations', 'url' => array('index')),
  array('label' => 'Create experiments variation', 'url' => array('create')),
  array('label' => 'View experiments variation', 'url' => array('view', 'id' => $model->experimentVariationId)),
  array('label' => 'Manage Experiments variations', 'url' => array('admin')),
);
?>

    <h3>Update experiments variations: <?php echo $model->experimentVariationId; ?></h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>