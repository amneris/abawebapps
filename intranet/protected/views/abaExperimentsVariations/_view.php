<?php
/* @var $this AbaExperimentsVariationsController */
/* @var $model AbaExperimentsVariations */
?>

<div class="view">

    <b/><?php echo CHtml::encode($data->getAttributeLabel('experimentVariationId')); ?>:</b>
    <?php echo CHtml::encode($data->experimentVariationId); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('experimentId')); ?>:</b>
    <?php echo CHtml::encode($data->experimentId); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('experimentVariationIdentifier')); ?>:</b>
    <?php echo CHtml::encode($data->experimentVariationIdentifier); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('experimentVariationDescription')); ?>:</b>
    <?php echo CHtml::encode($data->experimentVariationDescription); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('counterUsers')); ?>:</b>
    <?php echo CHtml::encode($data->counterUsers); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('limitUsers')); ?>:</b>
    <?php echo CHtml::encode($data->limitUsers); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('isDefault')); ?>:</b>
    <?php echo CHtml::encode($data->isDefault); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('experimentVariationStatus')); ?>:</b>
    <?php echo CHtml::encode($data->experimentVariationStatus); ?>
    <br/>

</div>