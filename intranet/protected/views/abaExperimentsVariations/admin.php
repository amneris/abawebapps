<?php
/* @var $this AbaExperimentsVariationsController */
/* @var $model AbaExperimentsVariations */

$this->breadcrumbs = array(
  'Experiments variations',
);

$this->menu = array(
  array('label' => 'Create experiments variations', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript(
  'search',
  "
   $('.search-button').click(function(){
     $('.search-form').toggle();
     return false;
   });
   $('.search-form form').submit(function(){
     $.fn.yiiGridView.update('aba-experiments-variations-grid', {
       data: $(this).serialize()
     });
     return false;
   });
   "
);
?>

<h3>Experiments variations</h3>

<?php echo CHtml::link('Create new experiments variation', array('AbaExperimentsVariations/create')); ?>
<br>
<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial(
      '_search',
      array(
        'model' => $model,
      )
    ); ?>
</div><!-- search-form -->

<?php $this->widget(
  'application.components.widgets.CGridViewABA',
  array(
    'id' => 'aba-experiments-variations-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager' => array(
      'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
      'prevPageLabel' => '<',
      'nextPageLabel' => '>',
      'firstPageLabel' => '<<',
      'lastPageLabel' => '>>',
    ),
    'extraInfo' => array('view', 'update'),
    'dataProvider' => $model->search(),
    'filter' => $model,
    'filterSelector' => '{filter}',
    'columns' => array(
      'experimentVariationId',
      'experimentId' => array(
        'name' => 'experimentId',
        'type' => 'experimentsList',
        'filter' => CHtml::listData(HeList::getExperimentsList(true), 'id', 'title'),
      ),
      'experimentVariationIdentifier',
      'experimentVariationDescription',
      'counterUsers',
      'limitUsers',
      'isDefault',
      'experimentVariationStatus' => array(
        'name' => 'experimentVariationStatus',
        'type' => 'statusExperimentsVariations',
        'filter' => HeList::statusExperimentsVariations()
      ),
      array(
        'class' => 'CButtonColumnABA',
        'header' => 'Options',
      ),
    ),
  )
); ?>
