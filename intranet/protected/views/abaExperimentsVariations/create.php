<?php
/* @var $this AbaExperimentsVariationsController */
/* @var $model AbaExperimentsVariations */

$this->breadcrumbs = array(
  'Experiments variations' => array('admin'),
  'Create',
);

$this->menu = array(
  array('label' => 'Manage experiments variations', 'url' => array('admin')),
);
?>

    <h3>Experiments variations</h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>