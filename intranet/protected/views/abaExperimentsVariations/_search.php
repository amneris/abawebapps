<?php
/* @var $this AbaExperimentsVariationsController */
/* @var $model AbaExperimentsVariations */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'experimentVariationId'); ?>
        <?php echo $form->textField($model, 'experimentVariationId', array('size' => 5, 'maxlength' => 20)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'experimentId'); ?>
        <?php echo $form->DropDownList($model, 'experimentId', HeList::getExperimentsList(), array('empty' => '--')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'experimentVariationIdentifier'); ?>
        <?php echo $form->textField($model, 'experimentVariationIdentifier', array('size' => 25, 'maxlength' => 32)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'experimentVariationDescription'); ?>
        <?php echo $form->textField($model, 'experimentVariationDescription', array('size' => 50, 'maxlength' => 20)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'counterUsers'); ?>
        <?php echo $form->textField($model, 'counterUsers', array('size' => 5, 'maxlength' => 50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'limitUsers'); ?>
        <?php echo $form->textField($model, 'limitUsers', array('size' => 5, 'maxlength' => 50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'isDefault'); ?>
        <?php echo $form->DropDownList($model, 'isDefault', HeList::booleanList(), array('empty' => '--')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'experimentVariationStatus'); ?>
        <?php echo $form->DropDownList($model, 'experimentVariationStatus', HeList::booleanList(), array('empty' => '--')); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->