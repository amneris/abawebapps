<?php
/* @var $this AbaExperimentsVariationsController */
/* @var $model AbaExperimentsVariations */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget(
      'CActiveForm',
      array(
        'id' => 'aba-experiments-variations-form',
        'enableAjaxValidation' => false,
      )
    );

    $options = HeList::booleanList();
    $optionsStatus = HeList::statusExperimentsVariations();
    $optionsTypes = HeList::getExperimentsList();

    $cssPrefix = (isset($wAction) AND $wAction == 'update' ? 'Cm' : '');

    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <table id="myForm">
        <?php if (is_numeric($model->experimentVariationId)): ?>
            <tr>
                <td>
                    <?php echo $form->labelEx($model, 'experimentVariationId'); ?>
                </td>
                <td>
                    <?php echo $model->experimentVariationId; ?>
                    <?php echo $form->error($model, 'experimentVariationId'); ?>
                </td>
            </tr>
        <?php endif; ?>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'experimentId'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'experimentId', $optionsTypes); ?>
                <?php echo $form->error($model, 'experimentId'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'experimentVariationIdentifier'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'experimentVariationIdentifier',
                  array('size' => 30, 'maxlength' => 32)); ?>
                <?php echo $form->error($model, 'experimentVariationIdentifier'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'experimentVariationDescription'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'experimentVariationDescription',
                  array('size' => 50, 'maxlength' => 150)); ?>
                <?php echo $form->error($model, 'experimentVariationDescription'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'counterUsers'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'counterUsers', array('size' => 5, 'maxlength' => 50)); ?>
                <?php echo $form->error($model, 'counterUsers'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'limitUsers'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'limitUsers', array('size' => 5, 'maxlength' => 50)); ?>
                <?php echo $form->error($model, 'limitUsers'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'isDefault'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'isDefault', $options); ?>
                <?php echo $form->error($model, 'isDefault'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'experimentVariationStatus'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'experimentVariationStatus', $optionsStatus); ?>
                <?php echo $form->error($model, 'experimentVariationStatus'); ?>
            </td>
        </tr>
    </table>
    <div class="row buttons" align="center">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->