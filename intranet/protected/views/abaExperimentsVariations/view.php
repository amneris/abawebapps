<?php
/* @var $this AbaExperimentsVariationsController */
/* @var $model AbaExperimentsVariations */

$this->breadcrumbs = array(
    'Experiments variations' => array('admin'),
    $model->experimentVariationId,
);

$this->menu = array(
    array('label' => 'List experiments variations', 'url' => array('index')),
    array('label' => 'Create experiments variation', 'url' => array('create')),
    array('label' => 'Update experiments variation', 'url' => array('update', 'id' => $model->experimentVariationId)),
    array(
        'label' => 'Delete experiments variation',
        'url' => '#',
        'linkOptions' => array(
            'submit' => array('delete', 'id' => $model->experimentVariationId),
            'confirm' => 'Are you sure you want to delete this item?'
        )
    ),
    array('label' => 'Manage experiments variation', 'url' => array('admin')),
);
?>

<h3>View experiments variations <b><?php echo $model->experimentVariationId . ": " . $model->experimentVariationDescription; ?></b></h3>

<?php $this->widget(
    'application.components.widgets.CDetailViewABA',
    array(
        'data' => $model,
        'attributes' => array(
            'experimentVariationId',
            'experimentId' =>
              array(
                  'name' =>   'experimentId',
                  'type' =>   'experimentsList',
                  'filter' => HeList::getExperimentsList()
              ),
            'experimentVariationIdentifier',
            'experimentVariationDescription',
            'counterUsers',
            'limitUsers',
            'isDefault' =>
              array(
                  'name' =>   'isDefault',
                  'type' =>   'booleanList',
                  'filter' => HeList::booleanList()
              ),
            'experimentVariationStatus' =>
              array(
                  'name' =>   'experimentVariationStatus',
                  'type' =>   'statusExperimentsVariations',
                  'filter' => HeList::statusExperimentsVariations()
              ),
        ),
    )
); ?>
