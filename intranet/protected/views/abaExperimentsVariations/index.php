<?php
/* @var $this AbaExperimentsVariationsController */
/* @var $model AbaExperimentsVariations */

$this->breadcrumbs = array(
    'Experiments variations',
);

$this->menu = array(
    array('label' => 'Create Experiments variation', 'url' => array('create')),
    array('label' => 'Manage Experiments variations', 'url' => array('admin')),
);
?>

<h3>Experiments variations</h3>

<?php $this->widget(
    'zii.widgets.CListView',
    array(
        'dataProvider' => $dataProvider,
        'itemView' => '_view',
    )
); ?>
