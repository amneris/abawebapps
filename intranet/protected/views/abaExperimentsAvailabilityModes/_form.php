<?php
/* @var $this AbaExperimentsAvailabilityModesController */
/* @var $model AbaExperimentsAvailabilityModes */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget(
      'CActiveForm',
      array(
        'id' => 'aba-experiments-availability-modes-form',
        'enableAjaxValidation' => false,
      )
    );

    $cssPrefix = (isset($wAction) AND $wAction == 'update' ? 'Cm' : '');

    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <table id="myForm">
        <?php if (is_numeric($model->availableModeId)): ?>
            <tr>
                <td>
                    <?php echo $form->labelEx($model, 'availableModeId'); ?>
                </td>
                <td>
                    <?php echo $model->availableModeId; ?>
                    <?php echo $form->error($model, 'availableModeId'); ?>
                </td>
            </tr>
        <?php endif; ?>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'availableModeDescription'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'availableModeDescription',
                  array('size' => 50, 'maxlength' => 150)); ?>
                <?php echo $form->error($model, 'availableModeDescription'); ?>
            </td>
        </tr>
        <?php if (is_numeric($model->availableModeId)): ?>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'dateAdd'); ?>
            </td>
            <td>
                <?php
                echo $form->textField($model, 'dateAdd', array('size' => 15, 'maxlength' => 19));
                echo CHtml::image(
                  "images/calendar.jpg",
                  "calendar0",
                  array("id" => "c_dateAdd", "class" => "pointer")
                );
                $this->widget(
                  'application.extensions.calendar.SCalendar',
                  array(
                    'inputField' => $cssPrefix . 'AbaExperimentsAvailabilityModes_dateAdd',
                    'button' => 'c_dateAdd',
                    'ifFormat' => '%Y-%m-%d %H:%M:%S',
                  )
                );
                ?>
                <?php echo $form->error($model, 'dateAdd'); ?>
            </td>
        </tr>
        <?php endif; ?>
    </table>
    <div class="row buttons" align="center">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->