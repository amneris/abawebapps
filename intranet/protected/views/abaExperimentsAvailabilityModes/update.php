<?php
/* @var $this AbaExperimentsAvailabilityModesController */
/* @var $model AbaExperimentsAvailabilityModes */

$this->breadcrumbs = array(
  'Aba Experiments availability modes' => array('admin'),
  $model->availableModeId => array('view', 'id' => $model->availableModeId),
  'Update',
);

$this->menu = array(
  array('label' => 'List Experiments availability modes', 'url' => array('index')),
  array('label' => 'Create Experiment availability mode', 'url' => array('create')),
  array('label' => 'View Experiment availability mode', 'url' => array('view', 'id' => $model->availableModeId)),
  array('label' => 'Manage Experiments availability modes', 'url' => array('admin')),
);
?>

    <h3>Update Experiment availability mode: <?php echo $model->availableModeId; ?></h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>