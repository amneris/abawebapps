<?php
/* @var $this AbaExperimentsAvailabilityModesController */
/* @var $model AbaExperimentsAvailabilityModes */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
      'CActiveForm',
      array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
      )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'availableModeId'); ?>
        <?php echo $form->textField($model, 'availableModeId', array('size' => 5, 'maxlength' => 20)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'availableModeDescription'); ?>
        <?php echo $form->textField($model, 'availableModeDescription', array('size' => 50, 'maxlength' => 255)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'dateAdd'); ?>
        <?php echo $form->textField($model, 'dateAdd', array('size' => 15, 'maxlength' => 19)); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->