<?php
/* @var $this AbaExperimentsAvailabilityModesController */
/* @var $model AbaExperimentsAvailabilityModes */

$this->breadcrumbs = array(
  'Experiments availability modes' => array('admin'),
  $model->availableModeId,
);

$this->menu = array(
  array('label' => 'List Experiments availability modes', 'url' => array('index')),
  array('label' => 'Create Experiment availability mode', 'url' => array('create')),
  array('label' => 'Update Experiment availability mode', 'url' => array('update', 'id' => $model->availableModeId)),
  array(
    'label' => 'Delete Experiment availability mode',
    'url' => '#',
    'linkOptions' => array(
      'submit' => array('delete', 'id' => $model->availableModeId),
      'confirm' => 'Are you sure you want to delete this item?'
    )
  ),
  array('label' => 'Manage Experiments availability modes', 'url' => array('admin')),
);
?>

<h3>View Experiments availability modes <b><?php echo $model->availableModeId . ": " . $model->availableModeDescription; ?></b></h3>

<?php $this->widget(
  'application.components.widgets.CDetailViewABA',
  array(
    'data' => $model,
    'attributes' => array(
      'availableModeId',
      'availableModeDescription',
      'dateAdd',
    ),
  )
); ?>
