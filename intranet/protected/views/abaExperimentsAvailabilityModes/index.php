<?php
/* @var $this AbaExperimentsAvailabilityModesController */
/* @var $model AbaExperimentsAvailabilityModes */

$this->breadcrumbs = array(
  'Aba Experiments availability modes',
);

$this->menu = array(
  array('label' => 'Create Experiments availability modes', 'url' => array('create')),
  array('label' => 'Manage Experiments availability modes', 'url' => array('admin')),
);
?>

<h3>Experiments availability modes</h3>

<?php $this->widget(
  'zii.widgets.CListView',
  array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
  )
); ?>
