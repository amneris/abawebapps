<?php
/* @var $this AbaExperimentsAvailabilityModesController */
/* @var $model AbaExperimentsAvailabilityModes */
?>

<div class="view">

    <b/><?php echo CHtml::encode($data->getAttributeLabel('availableModeId')); ?>:</b>
    <?php echo CHtml::encode($data->availableModeId); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('availableModeDescription')); ?>:</b>
    <?php echo CHtml::encode($data->availableModeDescription); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('dateAdd')); ?>:</b>
    <?php echo CHtml::encode($data->dateAdd); ?>
    <br/>

</div>