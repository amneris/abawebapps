<?php
/* @var $this AbaExperimentsAvailabilityModesController */
/* @var $model AbaExperimentsAvailabilityModes */

$this->breadcrumbs = array(
  'Experiments availability modes' => array('admin'),
  'Create',
);

$this->menu = array(
  array('label' => 'Manage Experiments availability modes', 'url' => array('admin')),
);
?>

    <h3>Experiments availability modes</h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>