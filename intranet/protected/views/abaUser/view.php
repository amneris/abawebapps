<?php
/* @var $this AbaUserController */
/* @var $model AbaUser */

$this->breadcrumbs = array(
    'Users' => array('admin'),
    '[Current User]' => array('admin_id', 'id' => $model->id),
    $model->email,
);

$this->menu = array(
    array('label' => 'Update AbaUser', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Manage AbaUser', 'url' => array('admin')),
);

?>
<script type="text/javascript">
    function unsubscribeUser(email) {
        var date = new Date();
        var year = date.getFullYear().toString();
        var month = date.getMonth().toString();
        var day = date.getDay().toString();
        var parsedDate = year + month + day;

        if (confirm('This action will delete this email from our system. Which means that this account will no longer ' +
          ' be able to logon in the Campus, no newsletters will be sent to him, etc. Even though the history of ' +
          'this account will be kept under email ' + email + '_' + parsedDate + '. Do you want to confirm?')) {
            document.location.href = '<?php echo Yii::app()->createUrl("/AbaUser/unsubscribe",array("userId"=>$model->id, "confirm"=>1));?>';
        }
    }
</script>
<h3>View user: <?php echo $model->email; ?></h3>
<?php if ($showUnsubscribe) { ?>
    <div class="row buttons" align="center">
        <?php echo CHtml::button('UNSUBSCRIBE', array('onclick' => "unsubscribeUser('" . $model->email . "');")); ?>
    </div>
<?php
} else {
    //echo " Nothing";
} ?>

<?php $this->widget(
    'application.components.widgets.CDetailViewABA',
    array(
        'data' => $model,
        'attributes' => array(
            'id',
            'name',
            'surnames',
            'email',
            'password',
            'releaseCampus',
            'keyExternalLogin' => array(
                'name' => 'keyExternalLogin',
                'type' => '*https://campus' . Yii::app()->config->get("COMMON_DOMAIN") . '/?autol=#value#',
            ),
            //	'passwordFlashMedia',
            'city',
            'countryId' =>
              array(
                  'name' => 'countryId',
                  'type' => 'countryList',
              ),
            'countryIdCustom' =>
              array(
                  'name' => 'countryIdCustom',
                  'type' => 'countryList',
              ),
            'birthDate',
            'telephone',
            'langEnv',
            'langCourse',
            'userType' => array(
                'name' => 'userType',
                'type' => 'userTypeDescription',
            ),
            'currentLevel' => array(
                'name' => 'currentLevel',
                'type' => 'levelDescription',
            ),
            'entryDate',
            'expirationDate',
            'teacherId' => array(
                'name' => 'teacherId',
                'type' => 'teacherList',
            ),
            'idPartnerSource' => array(
                'name' => 'idPartnerSource',
                'type' => 'partnerList',
            ),
            'idPartnerFirstPay' => array(
                'name' => 'idPartnerFirstPay',
                'type' => 'partnerList',
            ),
            'idPartnerCurrent' => array(
                'name' => 'idPartnerCurrent',
                'type' => 'partnerList',
            ),
            'firstLoginDone',
            'savedUserObjective',
            'releaseCampus',
            'idPartnerSource',
            'keyExternalLogin',
            'gender' => array(
                'name' => 'gender',
                'type' => 'genderList',
            ),
            'cancelReason',
            'registerUserSource',
            'hasWorkedCourse',
            'dateLastSentSelligent',
            'lastActionFixSql',
            'followUpAtFirstPay',
            'watchedVideosAtFirstPay',
            'idSourceList' => array(
                'name' => 'idSourceList',
                'type' => 'sourceList',
            ),
            'deviceTypeSource'

        ),
    )
); ?>
