<?php
/* @var $this AbaUserController */
/* @var $model AbaUser */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aba-export-user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
    
    <table id="myForm">
        
        <tr>
            <td>
                <label for="AbaUser_startDate" class="required">Start Period (Entry date)</label>
            </td>
            <td>
                <input name="AbaUser[startDate]" id="AbaUser_startDate" type="text" value="<?php echo HeDate::yesterday(true);?>" />
                <?php 
                    echo CHtml::image("images/calendar.jpg","calendar", array("id"=>"c_button","class"=>"pointer")); 
                    $this->widget('application.extensions.calendar.SCalendar',
                        array(
                        'inputField'=>'AbaUser_startDate',
                        'button'=>'c_button',
                        'ifFormat'=>'%Y-%m-%d 00:00:00',
                    ));
                ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <label for="AbaUser_endDate" class="required">End Period (Entry date)</label>
            </td>
            <td>
                <input name="AbaUser[endDate]" id="AbaUser_endDate" type="text" value="<?php echo HeDate::today(true);?>" />
                <?php 
                    echo CHtml::image("images/calendar.jpg","calendar", array("id"=>"c_buttonEnd","class"=>"pointer")); 
                    $this->widget('application.extensions.calendar.SCalendar',
                        array(
                        'inputField'=>'AbaUser_endDate',
                        'button'=>'c_buttonEnd',
                        'ifFormat'=>'%Y-%m-%d 00:00:00',
                    ));
                ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <?php 
                    $datos = $extrainfo['userTypeDescription'];
                    echo $form->labelEx($model,'userType'); 
                ?>
            </td>
            <td>
                <?php
                    echo $form->DropDownList($model,'userType',$datos, array('multiple' => 'multiple'));
                    echo $form->error($model,'userType');
                ?>
            </td>
        </tr>

    </table>
	<div class="row buttons" align="center">
		<?php echo CHtml::submitButton('Create xls'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->