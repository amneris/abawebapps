<?php
/**
 * ${NAME}
 * Author: mgadegaard
 * Date: 08/04/2015
 * Time: 16:09
 * © ABA English 2015
 */
?>
<?php
/* @var $this AbaUserController */
/* @var $pendingPayment AbaPayments */
/* @var $refundPayment AbaPayments */
/* @var $enableCancel bool */
/* @var $enableRefund bool */
?>
<?php if($enableCancel) { ?>
    <div style="width: 49%; float: left;">
        <h4>Pending Payment details:</h4>
        <?php $this->widget('application.components.widgets.CDetailViewABA', array(
            'data' => $pendingPayment,
            'attributes' => array(
                'id',
                'userId',
                'idProduct',
                'idCountry' =>
                    array(
                        'name' => 'idCountry',
                        'type' => 'countryList',
                    ),
                'idPeriodPay',
                'idUserCreditForm',
                'paySuppExtId' =>
                    array(
                        'name' => 'paySuppExtId',
                        'type' => 'supplierExtGatewayList',
                    ),
                'paySuppOrderId',
                'paySuppExtProfId',
                'status' =>
                    array(
                        'name' => 'status',
                        'type' => 'statusDescription',
                    ),
                'dateStartTransaction',
                'dateEndTransaction',
                'dateToPay',
                'xRateToEUR',
                'xRateToUSD',
                'currencyTrans',
                'foreignCurrencyTrans',
                'idPromoCode',
                'amountOriginal',
                'amountDiscount',
                'amountPrice',
                'idPayControlCheck',
                'idPartner' => array(
                    'name' => 'idPartner',
                    'type' => 'partnerList',
                ),
                'lastAction',
                'isRecurring' => array(
                    'name' => 'isRecurring',
                    'type' => "recurringList",
                ),
                'isExtend' => array(
                    'name' => 'isExtend',
                    'type' => "recurringList",
                ),
                'paySuppExtUniId',
                'paySuppLinkStatus' => array(
                    'name' => 'paySuppLinkStatus',
                    'type' => 'reconciliationStatusList',
                    'nullDisplay' => 'NOT MATCHED*',
                ),
                'isPeriodPayChange',
                'cancelOrigin' => array(
                    'name' => 'cancelOrigin',
                    'type' => 'cancelOriginList',
                    'nullDisplay' => '',
                ),
            ),
        )); ?>
    </div>
<?php } ?>
<?php if($enableRefund) { ?>
    <div style="width: 49%; margin-left: 50%">
        <h4>Details of Payment eligible for Refund:</h4>
        <?php $this->widget('application.components.widgets.CDetailViewABA', array(
            'data' => $refundPayment,
            'attributes' => array(
                'id',
                'userId',
                'idProduct',
                'idCountry' =>
                    array(
                        'name' => 'idCountry',
                        'type' => 'countryList',
                    ),
                'idPeriodPay',
                'idUserCreditForm',
                'paySuppExtId' =>
                    array(
                        'name' => 'paySuppExtId',
                        'type' => 'supplierExtGatewayList',
                    ),
                'paySuppOrderId',
                'paySuppExtProfId',
                'status' =>
                    array(
                        'name' => 'status',
                        'type' => 'statusDescription',
                    ),
                'dateStartTransaction',
                'dateEndTransaction',
                'dateToPay',
                'xRateToEUR',
                'xRateToUSD',
                'currencyTrans',
                'foreignCurrencyTrans',
                'idPromoCode',
                'amountOriginal',
                'amountDiscount',
                'amountPrice',
                'idPayControlCheck',
                'idPartner' => array(
                    'name' => 'idPartner',
                    'type' => 'partnerList',
                ),
                'lastAction',
                'isRecurring' => array(
                    'name' => 'isRecurring',
                    'type' => "recurringList",
                ),
                'isExtend' => array(
                    'name' => 'isExtend',
                    'type' => "recurringList",
                ),
                'paySuppExtUniId',
                'paySuppLinkStatus' => array(
                    'name' => 'paySuppLinkStatus',
                    'type' => 'reconciliationStatusList',
                    'nullDisplay' => 'NOT MATCHED*',
                ),
                'isPeriodPayChange',
                'cancelOrigin' => array(
                    'name' => 'cancelOrigin',
                    'type' => 'cancelOriginList',
                    'nullDisplay' => '',
                ),
            ),
        )); ?>
    </div>
<?php } ?>