<?php
/**
 * Author: mgadegaard
 * Date: 07/04/2015
 * Time: 16:12
 * © ABA English 2015
 */

/* @var $this AbaUserController */
/* @var $user CmAbaUser */
/* @var $pendingPayment AbaPayments */
/* @var $refundPayment AbaPayments */
/* @var $enableCancelRefund bool */
/* @var $enableCancel bool */
/* @var $enableRefund bool */

?>
<div class="form">
    <script language="JavaScript">
        function sendCancelRefundForm() {
            if ($("#AbaPayments_dateEndTransaction").value == '0000-00-00 00:00:00') {
                return alert('Field \"End Subscription by:\" should have a valid value');
            } else if ($("#refundPayment_dateEndTransaction").value == '0000-00-00 00:00:00') {
                return alert('Field \"Date To Pay:\" should have a valid value');
            } else {
                $("#aba-cancel-refund-form").submit();
                return true;
            }
            return false;
        }
    </script>
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'aba-cancel-refund-form',
        'enableAjaxValidation' => false,
    )); ?>
    <?php echo $form->errorSummary($user, 'ATTENTION'); ?>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <div class="row">
        <!-- -------------------- CANCEL START -------------------- -->
        <?php if($enableCancel) { ?>
            <div style="float: left; width: 49%;">
                <!--?php if(HePayments::isCreditCard($pendingPayment) && HePayments::isCandidateForRefund($refundPayment)) {?-->

                <h3>Cancel Field</h3>
                <label for="AbaPayments_dateEndTransaction" class="required">End Subscription by: <span
                        class="required">*</span></label> <?php
                if($pendingPayment->dateEndTransaction == '0000-00-00 00:00:00') {
                    $pendingPayment->dateEndTransaction = HeDate::todaySQL(true);
                }
                echo $form->textField($pendingPayment, 'dateEndTransaction');
                echo CHtml::image("images/calendar.jpg", "calendar", array("id" => "c_button", "class" => "pointer"));
                $this->widget('application.extensions.calendar.SCalendar',
                    array(
                        'inputField' => 'AbaPayments_dateEndTransaction',
                        'button' => 'c_button',
                        'ifFormat' => '%Y-%m-%d',
                    ));
                echo $form->error($pendingPayment, 'dateEndTransaction');

                ?>
            </div>
        <?php } ?>
        <!-- -------------------- CANCEL END -------------------- -->
    </div>
    <!-- -------------------- REFUND START -------------------- -->
    <?php if($enableRefund) { ?>
        <div style="width: 49%; margin-left: 50%;">
            <h3>Refund Fields</h3>

            <div class="row">
                <?php echo $form->labelEx($refundPayment, 'paySuppOrderId Original');
                echo CHtml::textField("readPaySuppOrderId", $refundPayment->paySuppOrderId, array('size' => 60, 'disabled' => 'disabled', 'class' => 'readOnly'));
                ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($refundPayment, 'paySuppOrderId'); ?>
                (Identifier aimed to find this payment in the remote gateway.
                For payments made through AllPago this should be be different from the original. For payments made
                through La Caixa it should be the same, otherwise it will be
                rejected.)
                </br><?php echo $form->textField($refundPayment, 'paySuppOrderId', array('size' => 60, 'maxlength' => 60, 'name' => 'refundPayment[paySuppOrderId]', 'id' => 'refundPayment_paySuppOrderId')); ?>
                <?php echo $form->error($refundPayment, 'paySuppOrderId'); ?>
            </div>
            <div class="row">
                <?php
                $refundPayment->amountPrice = sprintf('%0.2f', $refundPayment->amountPrice);
                echo $form->labelEx($refundPayment, 'Amount to refund');
                echo $form->textField($refundPayment, 'amountPrice', array('size' => 18, 'maxlength' => 18, 'name' => 'refundPayment[amountPrice]', 'id' => 'refundPayment_amountPrice'));
                echo $form->error($refundPayment, 'amountPrice');
                switch($refundPayment->paySuppExtId) {
                    case PAY_SUPPLIER_ALLPAGO_BR:
                    case PAY_SUPPLIER_ALLPAGO_MX:
                        echo '&nbsp;' . CHtml::checkBox("sendToPayGateway", true, array('style' => 'width: 20px; height: 20px;'));
                        echo 'If this checkbox is marked then the REFUND will be sent automatically to ALLPAGO (Only it should be disabled in case of REJECTIONS FROM THE BANK, not via ALLPAGO).';
                        break;
                    case PAY_SUPPLIER_CAIXA:
                        echo '&nbsp;' . CHtml::checkBox("sendToPayGateway", true, array('style' => 'width: 20px; height: 20px;'));
                        echo 'If this checkbox is marked then the REFUND will be sent automatically to LA CAIXA (You only need to disable this in case of REJECTIONS FROM THE BANK, not via SERMEPA).';
                        break;
                    case PAY_SUPPLIER_ADYEN:
                    case PAY_SUPPLIER_ADYEN_HPP:
                        echo '&nbsp;' . CHtml::checkBox("sendToPayGateway", true, array('style' => 'width: 20px; height: 20px;'));
                        echo 'If this checkbox is marked then the REFUND will be sent automatically to ADYEN.';
                        break;
                    default:
                        // continue, nothing to print out.
                        break;
                }
                ?>
            </div>
            <div class="row">
                <?php
                echo $form->labelEx($refundPayment, 'dateToPay');
                echo $form->textField($refundPayment, 'dateToPay', array('name' => 'refundPayment[dateToPay]', 'id' => 'refundPayment_dateToPay'));
                echo CHtml::image("images/calendar.jpg", "calendar", array("id" => "c_button", "class" => "pointer"));
                $this->widget('application.extensions.calendar.SCalendar',
                    array(
                        'inputField' => 'refundPayment_dateToPay',
                        'button' => 'c_button',
                        'ifFormat' => '%Y-%m-%d',
                    ));
                echo $form->error($refundPayment, 'dateToPay');
                /*  } */ ?>
            </div>
        </div>
    <?php } else { ?>
        <div style="width: 49%; margin-left: 50%;">
            <br /> <br /> <br /> <br /> <br />
            </div>
            <?php } ?>
    <!-- -------------------- REFUND END -------------------- -->
    <?php
    if($enableCancel) {
        echo $form->hiddenField($pendingPayment, 'amountPrice');
        echo $form->hiddenField($pendingPayment, 'id');
        echo $form->hiddenField($pendingPayment, 'userId');
        echo $form->hiddenField($pendingPayment, 'idProduct');
        echo $form->hiddenField($pendingPayment, 'idCountry');
        echo $form->hiddenField($pendingPayment, 'idPeriodPay');
        echo $form->hiddenField($pendingPayment, 'idUserCreditForm');
        echo $form->hiddenField($pendingPayment, 'paySuppExtId');
        echo $form->hiddenField($pendingPayment, 'paySuppOrderId');
        echo $form->hiddenField($pendingPayment, 'status');
        echo $form->hiddenField($pendingPayment, 'dateToPay');
        echo $form->hiddenField($pendingPayment, 'dateStartTransaction');
        echo $form->hiddenField($pendingPayment, 'xRateToEUR');
        echo $form->hiddenField($pendingPayment, 'xRateToUSD');
        echo $form->hiddenField($pendingPayment, 'currencyTrans');
        echo $form->hiddenField($pendingPayment, 'foreignCurrencyTrans');
        echo $form->hiddenField($pendingPayment, 'idPromoCode');
        echo $form->hiddenField($pendingPayment, 'amountOriginal');
        echo $form->hiddenField($pendingPayment, 'amountDiscount');
        echo $form->hiddenField($pendingPayment, 'idPayControlCheck');
        echo $form->hiddenField($pendingPayment, 'idPartner');
        echo $form->hiddenField($pendingPayment, 'taxRateValue');
        echo $form->hiddenField($pendingPayment, 'amountTax');
        echo $form->hiddenField($pendingPayment, 'amountPriceWithoutTax');
    }
    if($enableRefund) {
        echo $form->hiddenField($refundPayment, 'id', array('name' => 'refundPayment[id]', 'id' => 'refundPayment_id'));
        echo $form->hiddenField($refundPayment, 'userId', array('name' => 'refundPayment[userId]', 'id' => 'refundPayment_userId'));
        echo $form->hiddenField($refundPayment, 'idProduct', array('name' => 'refundPayment[idProduct]', 'id' => 'refundPayment_idProduct'));
        echo $form->hiddenField($refundPayment, 'idCountry', array('name' => 'refundPayment[idCountry]', 'id' => 'refundPayment_idCountry'));
        echo $form->hiddenField($refundPayment, 'idPeriodPay', array('name' => 'refundPayment[idPeriodPay]', 'id' => 'refundPayment_idPeriodPay'));
        echo $form->hiddenField($refundPayment, 'idUserCreditForm', array('name' => 'refundPayment[idUserCreditForm]', 'id' => 'refundPayment_idUserCreditForm'));
        echo $form->hiddenField($refundPayment, 'paySuppExtId', array('name' => 'refundPayment[paySuppExtId]', 'id' => 'refundPayment_paySuppExtId'));
        echo $form->hiddenField($refundPayment, 'status', array('name' => 'refundPayment[status]', 'id' => 'refundPayment_status'));
        echo $form->hiddenField($refundPayment, 'dateEndTransaction', array('name' => 'refundPayment[dateEndTransaction]', 'id' => 'refundPayment_dateEndTransaction'));
        echo $form->hiddenField($refundPayment, 'dateStartTransaction', array('name' => 'refundPayment[dateStartTransaction]', 'id' => 'refundPayment_dateStartTransaction'));
        echo $form->hiddenField($refundPayment, 'xRateToEUR', array('name' => 'refundPayment[xRateToEUR]', 'id' => 'refundPayment_xRateToEUR'));
        echo $form->hiddenField($refundPayment, 'xRateToUSD', array('name' => 'refundPayment[xRateToUSD]', 'id' => 'refundPayment_xRateToUSD'));
        echo $form->hiddenField($refundPayment, 'currencyTrans', array('name' => 'refundPayment[currencyTrans]', 'id' => 'refundPayment_currencyTrans'));
        echo $form->hiddenField($refundPayment, 'foreignCurrencyTrans', array('name' => 'refundPayment[foreignCurrencyTrans]', 'id' => 'refundPayment_foreignCurrencyTrans'));
        echo $form->hiddenField($refundPayment, 'idPromoCode', array('name' => 'refundPayment[idPromoCode]', 'id' => 'refundPayment_idPromoCode'));
        echo $form->hiddenField($refundPayment, 'amountOriginal', array('name' => 'refundPayment[amountOriginal]', 'id' => 'refundPayment_amountOriginal'));
        echo $form->hiddenField($refundPayment, 'amountDiscount', array('name' => 'refundPayment[amountDiscount]', 'id' => 'refundPayment_amountDiscount'));
        echo $form->hiddenField($refundPayment, 'idPayControlCheck', array('name' => 'refundPayment[idPayControlCheck]', 'id' => 'refundPayment_idPayControlCheck'));
        echo $form->hiddenField($refundPayment, 'idPartner', array('name' => 'refundPayment[idPartner]', 'id' => 'refundPayment_idPartner'));
        echo $form->hiddenField($refundPayment, 'taxRateValue', array('name' => 'refundPayment[taxRateValue]', 'id' => 'refundPayment_taxRateValue'));
        echo $form->hiddenField($refundPayment, 'amountTax', array('name' => 'refundPayment[amountTax]', 'id' => 'refundPayment_amountTax'));
        echo $form->hiddenField($refundPayment, 'amountPriceWithoutTax', array('name' => 'refundPayment[amountPriceWithoutTax]', 'id' => 'refundPayment_amountPriceWithoutTax'));
    }
    ?>
    <br/>

    <div class="row buttons">
        <?php
        if($enableCancel && !$enableCancelRefund) {
            echo '<div>' . CHtml::button('SUBMIT CANCEL', array('onclick' => 'sendCancelRefundForm()')) . '</div>';
        }
        if($enableCancelRefund) {
            echo '<div style="width:100%; text-align: center;">' . CHtml::button('SUBMIT CANCEL & REFUND', array('onclick' => 'sendCancelRefundForm()')) . '</div>';
        }
        if($enableRefund && !$enableCancelRefund) {
            echo '<div style="margin-left: 50%">' . CHtml::button('SUBMIT REFUND', array('onclick' => 'sendCancelRefundForm()')) . '</div>';
        }
        ?>
    </div>
    <?php $this->endWidget(); ?>
</div>