<?php
/* @var $this AbaUserController */
/* @var $model AbaUser */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <div class="row">
        <?php echo $form->label($model,'id'); ?>
        <?php echo $form->textField($model,'id'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'name'); ?>
        <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>200)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'surnames'); ?>
        <?php echo $form->textField($model,'surnames',array('size'=>60,'maxlength'=>200)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'email'); ?>
        <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>200)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'city'); ?>
        <?php echo $form->textField($model,'city',array('size'=>60,'maxlength'=>200)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'countryId');
        echo $form->DropDownList($model,'countryId',HeList::getCountryList(), array('empty'=>'--Any Country--'));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'countryIdCustom');
        echo $form->DropDownList($model,'countryIdCustom',HeList::getCountryList(), array('empty'=>'--Any Country--'));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'birthDate'); ?>
        <?php echo $form->textField($model,'birthDate'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'telephone'); ?>
        <?php echo $form->textField($model,'telephone',array('size'=>20,'maxlength'=>20)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'gender'); ?>
        <?php echo $form->textField($model,'gender',array('size'=>20,'maxlength'=>20)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'langEnv'); ?>
        <?php echo $form->textField($model,'langEnv',array('size'=>5,'maxlength'=>5)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'langCourse'); ?>
        <?php echo $form->textField($model,'langCourse',array('size'=>5,'maxlength'=>5)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'userType');
        echo $form->DropDownList($model,'userType',HeList::userTypeDescription(), array('empty'=>'--Any Type--'));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'currentLevel');
        echo $form->DropDownList($model,'currentLevel',HeList::levelsDescription(), array('empty'=>'--Any Level--'));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'entryDate');
        echo $form->textField($model,'entryDate');
        echo CHtml::image("images/calendar.jpg","calendar1", array("id"=>"c_buttonStart","class"=>"pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
            array(
                'inputField'=>'AbaUser_entryDate',
                'button'=>'c_buttonStart',
                'ifFormat'=>'%Y-%m-%d 00:00:00',
            ));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'expirationDate');
        echo $form->textField($model,'expirationDate');
        echo CHtml::image("images/calendar.jpg","calendar2", array("id"=>"c_buttonEnd","class"=>"pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
            array(
                'inputField'=>'AbaUser_expirationDate',
                'button'=>'c_buttonEnd',
                'ifFormat'=>'%Y-%m-%d 00:00:00',
            ));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'teacherId');
        echo $form->DropDownList($model,'teacherId',HeList::getTeacherList(), array('empty'=>'--Any Teacher--'));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'idPartnerSource');
        $dataPartnerList = HeList::getPartnerListWithoutFlashSales();
        echo $form->DropDownList($model,'idPartnerSource',$dataPartnerList, array('empty'=>'--Any Partner--'));
        //		 echo $form->textField($model,'idPartnerSource',array('size'=>3,'maxlength'=>3));
        ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'idPartnerFirstPay');
        echo $form->DropDownList($model,'idPartnerFirstPay',$dataPartnerList, array('empty'=>'--Any Partner--'));
        ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'idPartnerCurrent');
        echo $form->DropDownList($model,'idPartnerCurrent',$dataPartnerList, array('empty'=>'--Any Partner--'));
        ?>
    </div>

    <div class="row">
        <?php $options = array(1=>'Yes', 0=>'No', ''=>'Any'); ?>
        <?php echo $form->label($model,'firstLoginDone'); ?>
        <?php echo $form->DropDownList($model,'firstLoginDone',$options); ?>
    </div>

    <div class="row">
        <?php $options = array(1=>'Yes', 0=>'No', ''=>'Any'); ?>
        <?php echo $form->label($model,'savedUserObjective'); ?>
        <?php echo $form->DropDownList($model,'savedUserObjective',$options); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'releaseCampus'); ?>
        <?php echo $form->textField($model,'releaseCampus',array('size'=>10,'maxlength'=>200)); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'keyExternalLogin'); ?>
        <?php echo $form->textField($model,'keyExternalLogin',array('size'=>60,'maxlength'=>200)); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'cancelReason'); ?>
        <?php echo $form->textField($model,'cancelReason',array('size'=>10,'maxlength'=>10)); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'registerUserSource'); ?>
        <?php echo $form->textField($model,'registerUserSource',array('size'=>10,'maxlength'=>200)); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'hasWorkedCourse'); ?>
        <?php echo $form->textField($model,'hasWorkedCourse',array('size'=>10,'maxlength'=>200)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'dateLastSentSelligent');
        echo $form->textField($model,'dateLastSentSelligent');
        echo CHtml::image("images/calendar.jpg","calendar2", array("id"=>"c_buttonSelligent","class"=>"pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
            array(
                'inputField'=>'AbaUser_dateLastSentSelligent',
                'button'=>'c_buttonSelligent',
                'ifFormat'=>'%Y-%m-%d 00:00:00',
            )); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'lastActionFixSql'); ?>
        <?php echo $form->textField($model,'lastActionFixSql',array('size'=>60, 'maxlength'=>200)); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'followUpAtFirstPay'); ?>
        <?php echo $form->textField($model,'followUpAtFirstPay',array('size'=>60, 'maxlength'=>200)); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'watchedVideosAtFirstPay'); ?>
        <?php echo $form->textField($model,'watchedVideosAtFirstPay',array('size'=>30, 'maxlength'=>4)); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->