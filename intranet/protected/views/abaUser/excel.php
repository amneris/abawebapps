<?php
/* @var $this AbaUserController */
/* @var $model AbaUser */

$this->breadcrumbs=array(
	'Users'=>array('admin'),
	'Excel export',
);
/*
$this->menu=array(
	array('label'=>'List Users', 'url'=>array('index')),
	array('label'=>'Manage AbaUsers', 'url'=>array('admin')),
);*/
?>

<h3>Export users to excel</h3>

<?php echo $this->renderPartial('_formexcel', array('model'=>$model, 'extrainfo'=>$extrainfo)); ?>