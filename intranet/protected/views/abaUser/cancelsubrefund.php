<?php
/**
 * ${NAME}
 * Author: mgadegaard
 * Date: 02/04/2015
 * Time: 17:43
 * © ABA English 2015
 */
/* @var $this AbaUserController */
/* @var $user CmAbaUser */
/* @var $pendingPayment AbaPayments */
/* @var $refundPayment AbaPayments */
/* @var $enableCancelRefund bool */
/* @var $enableCancel bool */
/* @var $enableRefund bool */

$this->breadcrumbs=array(
    'Users'=>array('admin'),
    '[Current User]'=>array('admin_id','id'=>$user->id),
    $user->email,
);
?>
<h3>Cancel User Subscription, Pending Payment and Refund amount to User </h3>
<?php
$this->renderPartial('_formcancelrefund', array('user'=>$user,
    'pendingPayment'=>$pendingPayment,
    'refundPayment' => $refundPayment,
    'enableCancelRefund' => $enableCancelRefund,
    'enableCancel' => $enableCancel,
    'enableRefund' => $enableRefund));
$this->renderPartial('_fulldetails', array("pendingPayment"=>$pendingPayment,
    'refundPayment' => $refundPayment,
    'enableCancel' => $enableCancel,
    'enableRefund' => $enableRefund));
?>