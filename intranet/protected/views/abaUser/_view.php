<?php
/* @var $this AbaUserController */
/* @var $data AbaUser */
?>

<div class="view">
	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('surnames')); ?>:</b>
	<?php echo CHtml::encode($data->surnames); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	<?php echo CHtml::encode($data->password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('passwordFlashMedia')); ?>:</b>
	<?php echo CHtml::encode($data->passwordFlashMedia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('complete')); ?>:</b>
	<?php echo CHtml::encode($data->complete); ?>
	<br />

</div>