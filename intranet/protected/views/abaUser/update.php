<?php
/* @var $this AbaUserController */
/* @var $model AbaUser */
/* @var $moLastPayment AbaPayments */

$this->breadcrumbs=array(
	'Users'=>array('admin'),
	'[Current User]'=>array('admin_id','id'=>$model->id),
	'View'=>array('view','id'=>$model->id),
	'Update',
);
?>

<?php if(Yii::app()->user->hasFlash('delError')): ?>
    <?php echo "<br>"; ?>
    <div class="flash-error">
        <?php echo Yii::app()->user->getFlash('delError'); ?>
    </div>

<?php endif; ?>


<h3>Update user: <?php echo $model->email; ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'extrainfo'=>$extrainfo, 'moLastPayment'=>$moLastPayment)); ?>