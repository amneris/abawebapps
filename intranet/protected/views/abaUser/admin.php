<?php
/* @var $this AbaUserController */
/* @var $model AbaUser */
/* @var $dRowsUser CDataProvider */

$this->breadcrumbs = array(
  'Users',
);

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('.search-form form').submit(function(){
        $.fn.yiiGridView.update('aba-user-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
if (isset($_GET['AbaUser']) || isset($_GET["id"])) {
    $dRowsUser = $model->search();
} else {
    $dRowsUser = $model->emptySearch();
}
?>

<h3>Users</h3>
<?php

echo CHtml::link('Export to excel', array('abaUser/export'));
echo "<br>";
echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>

<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
      'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('application.components.widgets.CGridViewABA', array(
  'id' => 'aba-user-grid',
  'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
  'extraInfo' => array('view', 'update'),
  'emptyText' => '<b>No results found. Type in any text in any column to execute a search.</b>',
  'pager' => array(
    'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
    'prevPageLabel' => '<',
    'nextPageLabel' => '>',
    'firstPageLabel' => '<<',
    'lastPageLabel' => '>>',
  ),
  'extraButtons' => array(
    'extraButton1' => array(
      'textExtraLink' => '</br>Payments',
      'classExtraLink' => 'user',
      'urlExtraLink' => '"abaPayments/admin_id",array("id"=>$data->id)'
    ),
    'extraButton2' => array(
      'textExtraLink' => '</br>Cancel_recurring',
      'classExtraLink' => 'user',
      // 'fnValidate' => 'HeQuery::checkPaymentCanCancelRecurring($data->id)',
      'urlExtraLink' => '"abaPayments/cancel",array("userId"=>$data->id)'
    ),
    'extraButton3' => array(
      'textExtraLink' => '</br>Credit_Card',
      'classExtraLink' => 'user',
      'urlExtraLink' => '"abaUserCreditForms/admin_id", array("id"=>$data->id)'
    ),
    'extraButton4' => array(
      'textExtraLink' => '</br>Unsubscribe',
      'classExtraLink' => 'user',
      'urlExtraLink' => '"abaUser/unsubscribe", array("userId"=>$data->id, "confirm"=>0)',
    ),
    'extraButton5' => array(
      'textExtraLink' => '</br>Followup',
      'classExtraLink' => 'user',
      'urlExtraLink' => '"abaFollowup4/admin",array("userid"=>$data->id)'
    ),
    'extraButton6' => array(
      'textExtraLink' => '</br>Cancel & Refund',
      'classExtraLink' => 'user',
      'urlExtraLink' => '"abaUser/cancelsubrefund",array("idUser"=>$data->id)'
    ),
    'extraButton7' => array(
      'textExtraLink' => '</br>Progress problems',
      'classExtraLink' => 'user',
      'urlExtraLink' => '"AbaUserProgressQueues/create",array("userId"=>$data->id)'
    ),
    'extraButton8' => array(
      'textExtraLink' => '</br>Remove Progress',
      'classExtraLink' => 'user',
      'urlExtraLink' => '"AbaUserRemovedProgress/create",array("userId"=>$data->id)'
    ),
  ),
  'dataProvider' => $dRowsUser,
  'filter' => $model,
//    'filterSelector'=>'{filter}',
  'columns' => array(
    'id' => array(
      'name' => 'id',
      'headerHtmlOptions' => array('style' => 'width: 50px;'),
    ),
    'name',
    'surnames',
    'email' => array(
      'name' => 'email',
      'htmlOptions' => array('style' => 'font-weight: bold;'),
    ),
    'langEnv' => array(
      'name' => 'langEnv',
      'headerHtmlOptions' => array('style' => 'width: 50px;'),
    ),
    'langCourse' => array(
      'name' => 'langCourse',
      'headerHtmlOptions' => array('style' => 'width: 50px;'),
    ),
    'userType' =>
      array(
        'name' => 'userType',
        'type' => 'userTypeDescription',
        'htmlOptions' => array('style' => 'font-weight: bold;'),
        'filter' => HeList::userTypeDescription(),
      ),
    'expirationDate' => array(
      'name' => 'expirationDate',
      'htmlOptions' => array('style' => 'text-align: center;'),
    ),
    'teacherId' =>
      array(
        'name' => 'teacherId',
        'type' => 'teacherList',
        'filter' => HeList::getTeacherList(),
      ),
    array(
      'name' => 'idPartnerSource',
      'type' => 'partnerList',
      'filter' => HeList::getPartnerList(),
    ),
    array(
      'class' => 'CButtonColumnABA',
      'htmlOptions' => array('style' => 'width: 110px;'),
      'header' => 'Options',
    ),
  ),
)); ?>
