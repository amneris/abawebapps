<?php
/* @var $this AbaUserController */
/* @var $model AbaUser */
/* @var $form CActiveForm */
/* @var $moLastPayment AbaPayments */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
      'id' => 'aba-user-form',
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>
    <table id="myForm">
        <tr>
            <td style="width:300px">
                <?php echo $form->labelEx($model, 'name'); ?>
            </td>
            <td style="width:400px;">
                <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 200)); ?>
                <?php echo $form->error($model, 'name'); ?>
            </td>
            <td style="width:*;">
                &nbsp;&nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'surnames'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'surnames', array('size' => 60, 'maxlength' => 200)); ?>
                <?php echo $form->error($model, 'surnames'); ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'email'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'email',
                  array('disabled' => 'true', 'size' => 60, 'maxlength' => 200)); ?>
                <?php echo $form->error($model, 'email'); ?>
            </td>
            <td>
                <?php if ($model->userType > FREE && intval($moLastPayment->paySuppExtId) !== PAY_SUPPLIER_PAYPAL) { ?>
                    <input name="btnUserEmailField" id="btnUserEmailField" type="button"
                           onclick="enableEmailField(this);" value="Enable/disable e-mail field">

                    <span id="helpEmailEdit" style="font-size: 11px; color: red; display: none;">
                        <br/>
                        Warning:
                        <br/>
                        It is NOT recommended to do this action for PREMIUM users with already recorded AUDIOS in his course. If user insists
                        on changing his email we should make him clear that he will lose all his recorded AUDIOS.
                    </span>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                $datos = $extrainfo['userTypeList'];
                echo $form->labelEx($model, 'userType');
                ?>
            </td>
            <td>
                <?php
                $htmlOptionsUType = array();
                if ($model->userType <= FREE) {
                    $htmlOptionsUType = array("disabled" => "true");
                }
                echo $form->DropDownList($model, 'userType', HeList::userTypeForEdition($model->userType),
                  $htmlOptionsUType);
                ?>
            </td>
            <td><?php echo $form->error($model, 'userType'); ?>
                <span style="font-size: 11px; color: red">If you change the user type of the user,
                    please bear in mind that you have to respect the consistency rules:<br/>
                        FREE has NO SUCCESS payments active. / PREMIUM must have an active payment . </span>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'password'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'password', array('size' => 40, 'maxlength' => 50)); ?>
            </td>
            <td>
                <span style="font-size: 11px; color: red">If you change the password content, this is used to create the new password encoded.</span>
                <?php echo $form->error($model, 'password'); ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'city'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'city', array('size' => 60, 'maxlength' => 200)); ?>
            </td>
            <td>
                <?php echo $form->error($model, 'city'); ?>&nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php
                $datos = $extrainfo['countryDescription'];
                echo $form->labelEx($model, 'countryId');
                ?>
            </td>
            <td style="display: inline;">
                <?php if ($model->userType == FREE): ?>
                <?php
                echo $form->DropDownList($model, 'countryId', $datos, array('disabled' => 'true'));
                echo $form->error($model, 'countryId');
                ?>
            </td>
            <td>
                <input name="btnUserEmailField" id="btnUserEmailField" type="button" onclick="enableCountryField(this);"
                       value="Enable/disable Country">
            </td>
            <?php else: ?>
                <?php
                    echo $form->DropDownList($model, 'countryId', $datos, array('disabled'=>'true'));
                    echo $form->error($model, 'countryId');
                ?>
                </td>
                <td>&nbsp;</td>
            <?php endif; ?>

            <!---->
            <!--            <td>-->
            <!--                --><?php //if($model->userType == FREE): ?>
            <?php
            //                echo $form->DropDownList($model, 'countryId', $datos, array());
            //                echo $form->error($model, 'countryId');
            //                ?>
            <!--            </td>-->
            <!--            <td>-->
            <!--                <input name="btnUserEmailField" id="btnUserEmailField" type="button" onclick="enableCountryField(this);" value="Enable/disable Country">-->
            <!--            </td>-->
            <!--        --><?php //else: ?>
            <?php
            //            echo $form->DropDownList($model, 'countryId', $datos, array('disabled'=>'true'));
            //            echo $form->error($model, 'countryId');
            //            ?>
            <!--            </td>-->
            <!--            <td>&nbsp;</td>-->
            <!--        --><?php //endif; ?>


        </tr>

        <tr>
            <td>
                <?php
                $datos = $extrainfo['countryDescription'];
                echo $form->labelEx($model, 'countryIdCustom');
                ?>
            </td>
            <td>
                <?php
                echo $form->DropDownList($model, 'countryIdCustom', $datos);
                echo $form->error($model, 'countryIdCustom');
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'birthDate'); ?>
            </td>
            <td>
                <?php
                echo $form->textField($model, 'birthDate');
                echo $form->error($model, 'birthDate');
                echo CHtml::image("images/calendar.jpg", "calendar0",
                  array("id" => "c_buttonBirthDate", "class" => "pointer"));
                $this->widget('application.extensions.calendar.SCalendar',
                  array(
                    'inputField' => 'AbaUser_birthDate',
                    'button' => 'c_buttonBirthDate',
                    'ifFormat' => '%Y-%m-%d 00:00:00',
                  ));
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php $options = HeList::getGenderList(); ?>
                <?php echo $form->labelEx($model, 'gender'); ?>
            </td>
            <td>
                <?php
                echo $form->DropDownList($model, 'gender', $options);
                echo $form->error($model, 'gender');
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'telephone'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'telephone', array('size' => 20, 'maxlength' => 20)); ?>
                <?php echo $form->error($model, 'telephone'); ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php
                $datos = $extrainfo['languageEnvDescription'];
                echo $form->labelEx($model, 'langEnv');
                ?>
            </td>
            <td>
                <?php
                echo $form->DropDownList($model, 'langEnv', $datos);
                echo $form->error($model, 'langEnv');
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php
                $datos = $extrainfo['languageDescription'];
                echo $form->labelEx($model, 'langCourse');
                ?>
            </td>
            <td>
                <?php
                echo $form->DropDownList($model, 'langCourse', $datos, array('disabled' => 'true'));
                echo $form->error($model, 'langCourse');
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <?php
                $datos = $extrainfo['levelDescription'];
                echo $form->labelEx($model, 'currentLevel');
                ?>
            </td>
            <td>
                <?php
                echo $form->DropDownList($model, 'currentLevel', $datos);
                echo $form->error($model, 'currentLevel');
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php
                echo $form->labelEx($model, 'entryDate');
                ?>
            </td>
            <td>
                <?php

                echo $form->textField($model, 'entryDate', array('disabled' => 'true', 'size' => 20));
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <?php
                echo $form->labelEx($model, 'expirationDate');
                ?>
            </td>
            <td>
                <?php
                echo $form->textField($model, 'expirationDate', array("onchange" => "displayHelpExpirationDate();"));
                echo $form->error($model, 'expirationDate');
                echo CHtml::image("images/calendar.jpg", "calendar2",
                  array("id" => "c_buttonEnd", "class" => "pointer"));
                $this->widget('application.extensions.calendar.SCalendar',
                  array(
                    'inputField' => 'AbaUser_expirationDate',
                    'button' => 'c_buttonEnd',
                    'ifFormat' => '%Y-%m-%d 00:00:00',
                  ));
                ?>
            </td>
            <td>
                     <span id="helpExpirationDateEdit" style="font-size: 11px; color: red; display: none;">
                        <br/>
                        Warning:
                        <br/>a) In general the expiration date should match with the DURATION of the last SUCCESS payment for this user.
                        <br/>b) In case of a CANCELLATION and a REFUND of payments, if you want to force this user to become FREE, please just set the date to today.
                         An automatic process will expire the user in the next days.
                        <br/>c) If you want to let the user to be PREMIUM a few more days for free, we recommend give him a ABA English Friends User Coupon.
                    </span>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php
                $datos = $extrainfo['teacherList'];
                echo $form->labelEx($model, 'teacherId');
                ?>
            </td>
            <td>
                <?php
                echo $form->DropDownList($model, 'teacherId', $datos, array('empty' => '--No teacher assigned--'));
                echo $form->error($model, 'teacherId');
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php
                $datos = $extrainfo['partnerList'];
                echo $form->labelEx($model, 'idPartnerSource');
                ?>
            </td>
            <td>
                <?php
                echo $form->DropDownList($model, 'idPartnerSource', $datos,
                  array('disabled' => 'true', 'empty' => '--Any Partner--'));
                echo $form->error($model, 'idPartnerSource');
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php
                $datos = $extrainfo['partnerList'];
                echo $form->labelEx($model, 'idPartnerFirstPay');
                ?>
            </td>
            <td>
                <?php
                echo $form->DropDownList($model, 'idPartnerFirstPay', $datos,
                  array('disabled' => 'true', 'empty' => '--Any Partner--'));
                echo $form->error($model, 'idPartnerFirstPay');
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php
                $datos = $extrainfo['partnerList'];
                echo $form->labelEx($model, 'idPartnerCurrent');
                ?>
            </td>
            <td>
                <?php
                echo $form->DropDownList($model, 'idPartnerCurrent', $datos,
                  array('disabled' => 'true', 'empty' => '--Any Partner--'));
                echo $form->error($model, 'idPartnerCurrent');
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php $options = array(1 => 'Yes', 0 => 'No'); ?>
                <?php echo $form->labelEx($model, 'firstLoginDone'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'firstLoginDone', $options, array('disabled' => 'true')); ?>
                <?php echo $form->error($model, 'firstLoginDone'); ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php $options = array(1 => 'Yes', 0 => 'No'); ?>
                <?php echo $form->labelEx($model, 'savedUserObjective'); ?>
            </td>
            <td>
                <?php
                echo $form->DropDownList($model, 'savedUserObjective', $options);
                echo $form->error($model, 'savedUserObjective');
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php
                echo $form->labelEx($model, 'releaseCampus');
                ?>
            </td>
            <td>
                <?php
                echo $form->textField($model, 'releaseCampus',
                  array('disabled' => 'true', 'size' => 3, 'maxlength' => 3));
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php
                echo $form->labelEx($model, 'keyExternalLogin');
                ?>
            </td>
            <td>
                <?php
                echo $form->textField($model, 'keyExternalLogin', array('size' => 60, 'maxlength' => 200));
                echo $form->error($model, 'keyExternalLogin');
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php
                echo $form->labelEx($model, 'cancelReason');
                ?>
            </td>
            <td>
                <?php
                echo $form->DropDownList($model, 'cancelReason', HeList::cancelReasonDescription(),
                  array('disabled' => 'true'));
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php
                echo $form->labelEx($model, 'registerUserSource');
                ?>
            </td>
            <td>
                <?php
                echo $form->DropDownList($model, 'registerUserSource', HeList::registrationSourceDescription(),
                  array('disabled' => 'true'));
                echo $form->error($model, 'registerUserSource');
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php
                echo $form->labelEx($model, 'hasWorkedCourse');
                ?>
            </td>
            <td>
                <?php
                $options = array(1 => 'Yes', 0 => 'No');
                echo $form->DropDownList($model, 'hasWorkedCourse', $options, array('disabled' => 'true'));
                echo $form->error($model, 'hasWorkedCourse');
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php
                echo $form->labelEx($model, 'dateLastSentSelligent');
                ?>
            </td>
            <td>
                <?php
                echo $form->textField($model, 'dateLastSentSelligent', array('disabled' => 'true', 'size' => 20));
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php
                echo $form->labelEx($model, 'lastActionFixSql');
                ?>
            </td>
            <td>
                <?php
                echo $form->textField($model, 'lastActionFixSql',
                  array('disabled' => 'true', 'size' => 60, 'maxlength' => 200));
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php
                echo $form->labelEx($model, 'followUpAtFirstPay');
                ?>
            </td>
            <td>
                <?php
                echo $form->textField($model, 'followUpAtFirstPay',
                  array('disabled' => 'true', 'size' => 10, 'maxlength' => 4));
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php
                echo $form->labelEx($model, 'watchedVideosAtFirstPay');
                ?>
            </td>
            <td>
                <?php
                echo $form->textField($model, 'watchedVideosAtFirstPay',
                  array('disabled' => 'true', 'size' => 5, 'maxlength' => 3));
                ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'deviceTypeSource'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'deviceTypeSource',
                  array('disabled' => 'true', 'size' => 1, 'maxlength' => 1)); ?>
                <?php echo $form->error($model, 'deviceTypeSource'); ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

    </table>
    <!-- </div>-->

    <div class="row buttons" align="center">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->