<?php
/* @var $this AbaUserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Users',
);

$this->menu=array(
	array('label'=>'Create AbaUser', 'url'=>array('create')),
	array('label'=>'Manage AbaUser', 'url'=>array('admin')),
	array('label'=>'Payments AbaUser', 'url'=>array('abaPayments/index')),
);
?>

<h3>Aba Users</h3>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
