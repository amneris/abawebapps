<?php
/* @var $this AbaUserController */
/* @var $model AbaUser */

$this->breadcrumbs=array(
	'Users'=>array('admin'),
	'Create',
);
/*
$this->menu=array(
//	array('label'=>'List AbaUser', 'url'=>array('index')),
	array('label'=>'Manage AbaUsers', 'url'=>array('admin')),
);*/
?>

<h3>Create user</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'extrainfo'=>$extrainfo)); ?>