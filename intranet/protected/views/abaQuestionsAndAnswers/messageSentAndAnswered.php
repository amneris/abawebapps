<?php
/* @var $this AbaQuestionsAndAnswersController */
/* @var $model AbaQuestionsAndAnswers */

$this->breadcrumbs=array(
    'Messages Sent and Answered',
);

?>

<h3>Messages Sent and Answered</h3>

<br><br>
<div style="text-align: center; font-size: 15px">
    Messages sent and answered for period from <b><?php echo $startDate ?></b> to <b><?php echo $endDate ?> </b><br><br>
    <?php
    echo CHtml::form(Yii::app()->createUrl('abaQuestionsAndAnswers/getMessageSentAndAnswered'), 'post', array('id'=>'form', 'class'=>'myForm'));
    echo "range from ";
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'name' => 'startDate',
        'value' => $startDate,
        'options' => array(
            'dateFormat' => 'yy-mm-dd',
            'changeMonth'=>true,
            'changeYear'=>true,
            'minDate'=>'2014-10-01', // -30
        ),
        'htmlOptions' => array(
            'size' => '10',
            'maxlength' => '10',
        ),
    ));
    echo " to ";
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'name' => 'endDate',
        'value' => $endDate,
        'options' => array(
            'dateFormat' => 'yy-mm-dd',
            'changeMonth'=>true,
            'changeYear'=>true,
        ),
        'htmlOptions' => array(
            'size' => '10',
            'maxlength' => '10',
        ),
    ));

    echo CHtml::button("Search",array('onclick' => 'this.form.submit();','title'=>"Search",'id'=>'searchButton'));
    echo CHtml::endForm();
    ?>
</div>
<br><br>

<table id="salesTable">
    <tr>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            Name
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            Email
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            Teacher ID
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Questions made
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Answers made
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Waiting response
        </td>
    </tr>

<?php

    $auxTotal = array('questions'=>0, 'answers'=>0, 'waiting'=>0);

    foreach($total as $value)
    {
    echo "<tr>";

        echo "<td style='text-align: left'>";
        echo $value['surnames'].", ".$value['name'];
        echo "</td>";

        echo "<td style='text-align: left'>";
        echo $value['email'];
        echo "</td>";

        echo "<td style='text-align: center; width: 70px'>";
        echo $value['teacherid'];
        echo "</td>";

        echo "<td style='text-align: right; background-color: #CEE3F6'>";
        echo $value['period_questions'];
        $auxTotal['questions'] += $value['period_questions'];
        echo "</td>";

        echo "<td style='text-align: right; background-color: #D8F6CE''>";
        echo $value['period_answers'];
        $auxTotal['answers'] += $value['period_answers'];
        echo "</td>";

        echo "<td style='text-align: right; background-color: #F6D8CE''>";
        echo $value['period_questions']-$value['period_answers'];
        $auxTotal['waiting'] += $value['period_questions']-$value['period_answers'];
        echo "</td>";

    echo "</tr>";
    }
?>
    <tr>
        <td style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px" colspan="3">
            Total
        </td>
        <td style="background-color: #0000ff; text-align: right; font-weight: bold; font-size: 15px; color:#ffffff">
            <?php echo $auxTotal['questions']; ?>
        </td>
        <td style="background-color: green; text-align: right; font-weight: bold; font-size: 15px; color:#ffffff"">
        <?php echo $auxTotal['answers']; ?>
        </td>
        <td style="background-color: red; text-align: right; font-weight: bold; font-size: 15px; color:#ffffff"">
        <?php echo $auxTotal['waiting']; ?>
        </td>
    </tr>

</table>


