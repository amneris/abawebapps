<?php
/* @var $this AbaQuestionsAndAnswersController */
/* @var $model AbaQuestionsAndAnswers */
/* @var $senderInfo AbaQuestionsAndAnswers */
/* @var $receiverInfo AbaQuestionsAndAnswers */

$this->breadcrumbs=array(
	'Messages'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AbaQuestionsAndAnswers', 'url'=>array('index')),
	array('label'=>'Create AbaQuestionsAndAnswers', 'url'=>array('create')),
	array('label'=>'Update AbaQuestionsAndAnswers', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AbaQuestionsAndAnswers', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AbaQuestionsAndAnswers', 'url'=>array('admin')),
);
?>

<h3>Message #<?php echo $model->id; ?></h3>
<?php



?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'sender_id',
        'senderInfo',
		'receiver_id',
        'receiverInfo',
		'subject',
		'body',
		'is_read',
		'deleted_by',
		'created_at',
	),
)); ?>
