<?php
/* @var $this AbaQuestionsAndAnswersController */
/* @var $model AbaQuestionsAndAnswers */

$this->breadcrumbs=array(
	'Aba Questions And Answers'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AbaQuestionsAndAnswers', 'url'=>array('index')),
	array('label'=>'Create AbaQuestionsAndAnswers', 'url'=>array('create')),
	array('label'=>'View AbaQuestionsAndAnswers', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AbaQuestionsAndAnswers', 'url'=>array('admin')),
);
?>

<h1>Update AbaQuestionsAndAnswers <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>