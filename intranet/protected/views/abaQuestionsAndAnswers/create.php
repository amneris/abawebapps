<?php
/* @var $this AbaQuestionsAndAnswersController */
/* @var $model AbaQuestionsAndAnswers */

$this->breadcrumbs=array(
	'Aba Questions And Answers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AbaQuestionsAndAnswers', 'url'=>array('index')),
	array('label'=>'Manage AbaQuestionsAndAnswers', 'url'=>array('admin')),
);
?>

<h1>Create AbaQuestionsAndAnswers</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>