<?php
/* @var $this AbaQuestionsAndAnswersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Aba Questions And Answers',
);

$this->menu=array(
	array('label'=>'Create AbaQuestionsAndAnswers', 'url'=>array('create')),
	array('label'=>'Manage AbaQuestionsAndAnswers', 'url'=>array('admin')),
);
?>

<h1>Aba Questions And Answers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
