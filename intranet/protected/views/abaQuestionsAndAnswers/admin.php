<?php
/* @var $this AbaQuestionsAndAnswersController */
/* @var $model AbaQuestionsAndAnswers */

$this->breadcrumbs=array(
	'Messages',
);

$this->menu=array(
	array('label'=>'List AbaQuestionsAndAnswers', 'url'=>array('index')),
	array('label'=>'Create AbaQuestionsAndAnswers', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('aba-questions-and-answers-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Messages</h3>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php
    $this->widget('application.components.widgets.CGridViewABA', array(
	'id'=>'aba-questions-and-answers-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
        'prevPageLabel'  => '<',
        'nextPageLabel'  => '>',
        'firstPageLabel' => '<<',
        'lastPageLabel'  => '>>',),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'filterSelector'=>'{filter}',
    'extraInfo'=>array('view'),
	'columns'=>array(
		//'id',
		'sender_id',
		'receiver_id',
		//'subject',
        'subject' => array(
            'name'=>'subject',
            'type'=>'raw',

        ),
        'body' => array(
            'name'=>'body',
            'value' => 'substr($data->body, 0, 170);',
            //'type'=>'raw',
            //'headerHtmlOptions'=>array('style'=>'padding: 0 50px;'),
//            'filter'=>false
        ),		//'is_read',
		//'deleted_by',
		//'created_at',
        array(
            'class'=>'CButtonColumnABA',
            'header'=>'Options',
        ),
	),
)); ?>
