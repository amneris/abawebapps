<?php
/* @var $this AbaQuestionsAndAnswersController */
/* @var $model AbaQuestionsAndAnswers */

$this->breadcrumbs=array(
    'Total Messages Sent and Answered',
);

?>

<h3>Total Messages Sent and Answered</h3>
<br><br>

<table id="salesTable">
    <tr>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            Name
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            Email
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            Teacher ID
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Total premium users
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Total questions made
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Total answers made
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Total waiting response
        </td>
    </tr>

    <?php
    $auxTotal = array('users'=>0, 'questions'=>0, 'answers'=>0, 'waiting'=>0);


    foreach($total as $value)
    {
        echo "<tr>";

        echo "<td style='text-align: left'>";
        echo $value['surnames'].", ".$value['name'];
        echo "</td>";

        echo "<td style='text-align: left'>";
        echo $value['email'];
        echo "</td>";

        echo "<td style='text-align: center; width: 70px'>";
        echo $value['teacherid'];
        echo "</td>";

        echo "<td style='text-align: right;'>";
        echo $value['total_users'];
        $auxTotal['users'] += $value['total_users'];
        echo "</td>";

        echo "<td style='text-align: right; background-color: #CEE3F6'>";
        echo $value['total_questions'];
        $auxTotal['questions'] += $value['total_questions'];
        echo "</td>";

        echo "<td style='text-align: right; background-color: #D8F6CE''>";
        echo $value['total_answers'];
        $auxTotal['answers'] += $value['total_answers'];
        echo "</td>";

        echo "<td style='text-align: right; background-color: #F6D8CE''>";
        echo $value['total_questions']-$value['total_answers'];
        $auxTotal['waiting'] += $value['total_questions']-$value['total_answers'];
        echo "</td>";

        echo "</tr>";
    }
    ?>

    <tr>
        <td style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px" colspan="3">
            Total
        </td>
        <td style="text-align: right; font-weight: bold; font-size: 15px">
            <?php echo $auxTotal['users']; ?>
        </td>
        <td style="background-color: #0000ff; text-align: right; font-weight: bold; font-size: 15px; color:#ffffff">
            <?php echo $auxTotal['questions']; ?>
        </td>
        <td style="background-color: green; text-align: right; font-weight: bold; font-size: 15px; color:#ffffff"">
            <?php echo $auxTotal['answers']; ?>
        </td>
        <td style="background-color: red; text-align: right; font-weight: bold; font-size: 15px; color:#ffffff"">
            <?php echo $auxTotal['waiting']; ?>
        </td>
    </tr>

</table>


