<?php
/* @var $this AbaProductsListController */
/* @var $model AbaProductsList */

$this->breadcrumbs=array(
	'Aba Products Lists'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AbaProductsList', 'url'=>array('index')),
	array('label'=>'Manage AbaProductsList', 'url'=>array('admin')),
);
?>

<h1>Create AbaProductsList</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>