<?php
/* @var $this AbaProductsListController */
/* @var $model AbaProductsList */

$this->breadcrumbs=array(
	'Products Lists',
);

$this->menu=array(
//	array('label'=>'List AbaProductsList', 'url'=>array('index')),
	array('label'=>'Create AbaProductsList', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('aba-products-list-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Products Lists</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('application.components.widgets.CGridViewABA', array(
	'id'=>'aba-products-list-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'filterSelector'=>'{filter}',
	'columns'=>array(
		'id',
		'descriptionText',
		'userType'=>
		array( 
			'name'=>'userType',
            'type'=>'userTypeDescription',
          ),
		'hierarchy',
		'visibleWeb'=>
		array( 
			'name'=>'visibleWeb',
            'type'=>'booleanList',
          ),
		array(
			'class'=>'CButtonColumnABA',
		),
	),
)); ?>
