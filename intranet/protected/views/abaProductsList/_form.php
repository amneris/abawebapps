<?php
/* @var $this AbaProductsListController */
/* @var $model AbaProductsList */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aba-products-list-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descriptionText'); ?>
		<?php echo $form->textField($model,'descriptionText',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'descriptionText'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'userType'); ?>
		<?php echo $form->textField($model,'userType',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'userType'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hierarchy'); ?>
		<?php echo $form->textField($model,'hierarchy',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'hierarchy'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'visibleWeb'); ?>
		<?php echo $form->textField($model,'visibleWeb'); ?>
		<?php echo $form->error($model,'visibleWeb'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->