<?php
/* @var $this AbaProductsListController */
/* @var $data AbaProductsList */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descriptionText')); ?>:</b>
	<?php echo CHtml::encode($data->descriptionText); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userType')); ?>:</b>
	<?php echo CHtml::encode($data->userType); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hierarchy')); ?>:</b>
	<?php echo CHtml::encode($data->hierarchy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('visibleWeb')); ?>:</b>
	<?php echo CHtml::encode($data->visibleWeb); ?>
	<br />


</div>