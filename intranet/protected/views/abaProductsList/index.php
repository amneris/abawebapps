<?php
/* @var $this AbaProductsListController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Aba Products Lists',
);

$this->menu=array(
	array('label'=>'Create AbaProductsList', 'url'=>array('create')),
	array('label'=>'Manage AbaProductsList', 'url'=>array('admin')),
);
?>

<h1>Aba Products Lists</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
