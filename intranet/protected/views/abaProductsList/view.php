<?php
/* @var $this AbaProductsListController */
/* @var $model AbaProductsList */

$this->breadcrumbs=array(
	'Aba Products Lists'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AbaProductsList', 'url'=>array('index')),
	array('label'=>'Create AbaProductsList', 'url'=>array('create')),
	array('label'=>'Update AbaProductsList', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AbaProductsList', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AbaProductsList', 'url'=>array('admin')),
);
?>

<h1>View AbaProductsList #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'descriptionText',
		'userType',
		'hierarchy',
		'visibleWeb',
	),
)); ?>
