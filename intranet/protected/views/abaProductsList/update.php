<?php
/* @var $this AbaProductsListController */
/* @var $model AbaProductsList */

$this->breadcrumbs=array(
	'Aba Products Lists'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AbaProductsList', 'url'=>array('index')),
	array('label'=>'Create AbaProductsList', 'url'=>array('create')),
	array('label'=>'View AbaProductsList', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AbaProductsList', 'url'=>array('admin')),
);
?>

<h1>Update AbaProductsList <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>