<?php
/* @var $this AbaPartnersListController */
/* @var $model AbaPartnersList */

$this->breadcrumbs=array(
    'Enterprise Channels',
);

?>

<h3>Enterprise Channels</h3>

<?php
echo CHtml::form(Yii::app()->createUrl('AbaB2bEnterprise/admin'), 'get');
echo "Filter by channel ";
echo CHtml::dropDownList('idPartnerEnterprise', $channel, $channelList, array('onchange' => 'this.form.submit();'));
/*echo " , company ";
echo CHtml::dropDownList('company', $company, $list, array('onchange' => 'this.form.submit();'));
echo " , filter by by email ";
echo CHtml::textField('criterio', $criterio, array('size'=>'40px','placeholder'=>'emailexample@abaenglish.com','onchange' => 'this.form.submit();'));*/
echo CHtml::endForm();
echo "<br>";
echo CHtml::link('Export results',array('AbaB2bEnterprise/export&channel='.$channel));
echo "<br>";
?>

<?php $this->widget('application.components.widgets.CGridViewABA', array(
    'id'=>'aba-enterprise-channels-list-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
        'prevPageLabel'  => '<',
        'nextPageLabel'  => '>',
        'firstPageLabel' => '<<',
        'lastPageLabel'  => '>>',),
    'extraInfo'=>array('view'),
    'dataProvider'=>$model->getEnterprises($pages, $channel),
    'columns'=>array(
        'id',
        'Enterprise',
        'Contact',
        'Country',
        'Partner',
        'Channel',
        'created',
    ),
)); ?>
