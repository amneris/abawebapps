<?php
/* @var $this AbaExternalSalesController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php

//$dollarToday =  0;
//$realToday =    0;
//$pesoToday =    0;
//
//$UsdEurJson = file_get_contents('http://rate-exchange.appspot.com/currency?from=USD&to=EUR');
//$UsdEurJson = json_decode(trim($UsdEurJson), true);
//
//if(isset($UsdEurJson['rate']) AND is_numeric($UsdEurJson['rate'])) {
//    $dollarToday = floatval($UsdEurJson['rate']);
//}
//else {
//    $dollarToday = file_get_contents('http://currencies.apps.grandtrunk.net/getlatest/USD/EUR');
//}
//
//$BrlEurJson = file_get_contents('http://rate-exchange.appspot.com/currency?from=BRL&to=EUR');
//$BrlEurJson = json_decode(trim($BrlEurJson), true);
//
//if(isset($BrlEurJson['rate']) AND is_numeric($BrlEurJson['rate'])) {
//    $realToday = floatval($BrlEurJson['rate']);
//}
//else {
//    $realToday = file_get_contents('http://currencies.apps.grandtrunk.net/getlatest/BRL/EUR');
//}
//
//$MxnEurJson = file_get_contents('http://rate-exchange.appspot.com/currency?from=MXN&to=EUR');
//$MxnEurJson = json_decode(trim($MxnEurJson), true);
//
//if(isset($MxnEurJson['rate']) AND is_numeric($MxnEurJson['rate'])) {
//    $pesoToday = floatval($MxnEurJson['rate']);
//}
//else {
//    $realToday = file_get_contents('http://currencies.apps.grandtrunk.net/getlatest/MXN/EUR');
//}

$dollar = new AbaCurrency();
$dollar->getCurrencyById('USD');
$dollarToday = $dollar->outEur;

$real = new AbaCurrency();
$real->getCurrencyById('BRL');
$realToday = $real->outEur;

$peso = new AbaCurrency();
$peso->getCurrencyById('MXN');
$pesoToday = $peso->outEur;

//function just to make syntax more easy
function toMoney($number)
{
    $money = number_format($number,2,',','.');
    return $money;
}

?>


<table id="salesTable">
    <tr>
        <td style="border: 0px; text-align: center; font-size: 15px; font-weight: bold; padding-bottom: 10px" colspan="4">
                Daily (<?php echo $endDate ?>)<br>
        </td>
    </tr>


    <tr>
        <td>
            New
        </td>
        <td style="text-align: center">
            <?php
            if(($totalDaily['countEUR'] + $totalDaily['countUSD'] + $totalDaily['countBRL'] + $totalDaily['countMXN'])>0)
                echo toMoney(($totalDaily['totalEUR'] + ($totalDaily['totalUSD'] * $dollarToday) + ($totalDaily['totalBRL'] * $realToday) + ($totalDaily['totalMXN'] * $pesoToday)) / ($totalDaily['countEUR'] + $totalDaily['countUSD'] + $totalDaily['countBRL'] + $totalDaily['countMXN']));
            ?>€
        </td>
        <td style="text-align: right;">
            <?php echo $totalDaily['countEUR'] + $totalDaily['countUSD'] + $totalDaily['countBRL'] + $totalDaily['countMXN']; ?>
        </td>
        <td style="text-align: right;">
            <?php echo toMoney($totalDaily['totalEUR'] + ($totalDaily['totalUSD'] * $dollarToday) + ($totalDaily['totalBRL'] * $realToday) + ($totalDaily['totalMXN'] * $pesoToday)) . "€"; ?>
        </td>
    </tr>

    <tr>
        <td>
            Renovation
        </td>
        <td style="text-align: center">
            <?php
            if(($totalDaily['countRenovationEUR'] + $totalDaily['countRenovationUSD'] + $totalDaily['countRenovationBRL'] + $totalDaily['countRenovationMXN']) > 0)
                echo toMoney(($totalDaily['totalRenovationEUR'] + ($totalDaily['totalRenovationUSD'] * $dollarToday) + ($totalDaily['totalRenovationBRL'] * $realToday) + ($totalDaily['totalRenovationMXN'] * $pesoToday)) / ($totalDaily['countRenovationEUR'] + $totalDaily['countRenovationUSD'] + $totalDaily['countRenovationBRL'] + $totalDaily['countRenovationMXN']));
            ?>€
        </td>
        <td style="text-align: right;">
            <?php echo $totalDaily['countRenovationEUR'] + $totalDaily['countRenovationUSD'] + $totalDaily['countRenovationBRL'] + $totalDaily['countRenovationMXN']; ?>
        </td>
        <td style="text-align: right;">
            <?php echo toMoney($totalDaily['totalRenovationEUR'] + ($totalDaily['totalRenovationUSD'] * $dollarToday) + ($totalDaily['totalRenovationBRL'] * $realToday) + ($totalDaily['totalRenovationMXN'] * $pesoToday)) . "€"; ?>
        </td>
    </tr>

    <tr>
        <td style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px">
            Total
        </td>
        <td style="background-color: greenyellow; text-align: center; font-weight: bold; font-size: 15px">
            <?php
            if($totalDaily['countRenovationUSD'] + $totalDaily['countRenovationBRL'] + $totalDaily['countRenovationMXN'] + $totalDaily['countRenovationEUR'] + $totalDaily['countUSD'] + $totalDaily['countBRL'] + $totalDaily['countMXN'] + $totalDaily['countEUR'] > 0)
                echo toMoney((($totalDaily['totalUSD'] * $dollarToday) + ($totalDaily['totalRenovationUSD'] * $dollarToday)
                        + ($totalDaily['totalBRL'] * $realToday) + ($totalDaily['totalRenovationBRL'] * $realToday)
                        + ($totalDaily['totalMXN'] * $pesoToday) + ($totalDaily['totalRenovationMXN'] * $pesoToday)
                        + $totalDaily['totalEUR'] + $totalDaily['totalRenovationEUR']) / ($totalDaily['countRenovationUSD'] + $totalDaily['countRenovationBRL'] + $totalDaily['countRenovationMXN'] + $totalDaily['countRenovationEUR'] + $totalDaily['countUSD'] + $totalDaily['countBRL'] + $totalDaily['countMXN'] + $totalDaily['countEUR']));
            ?>€
        </td>
        <td style="background-color: greenyellow; text-align: right; font-weight: bold; font-size: 15px">
            <?php echo $totalDaily['countRenovationUSD'] + $totalDaily['countRenovationBRL'] + $totalDaily['countRenovationMXN'] + $totalDaily['countRenovationEUR'] + $totalDaily['countUSD'] + $totalDaily['countBRL'] + $totalDaily['countMXN'] + $totalDaily['countEUR']?>
        </td>
        <td style="background-color: greenyellow; text-align: right; font-weight: bold; font-size: 15px">
            <?php echo toMoney($totalDaily['totalUSD'] * $dollarToday + $totalDaily['totalRenovationUSD'] * $dollarToday +
                $totalDaily['totalBRL'] * $realToday + $totalDaily['totalRenovationBRL'] * $realToday +
                $totalDaily['totalMXN'] * $pesoToday + $totalDaily['totalRenovationMXN'] * $pesoToday +
                $totalDaily['totalEUR'] + $totalDaily['totalRenovationEUR']) ?>€
        </td>
    </tr>

    <tr>
        <td style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px" colspan="2">
            Refunds
        </td>
        <td style="background-color: #ff0000; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo $totalDaily['contRefundUSD'] + $totalDaily['contRefundBRL'] + $totalDaily['contRefundMXN'] + $totalDaily['contRefundEUR']; ?>
        </td>
        <td style="background-color: #ff0000; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo toMoney(($totalDaily['totalRefundUSD'] * $dollarToday) + ($totalDaily['totalRefundBRL'] * $realToday) + ($totalDaily['totalRefundMXN'] * $pesoToday) + $totalDaily['totalRefundEUR'])."€"; ?>
        </td>
    </tr>

    <tr>
        <td style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px" colspan="3">
            Total
        </td>
        <td style="background-color: green; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo toMoney((($totalDaily['totalUSD'] * $dollarToday) + ($totalDaily['totalRenovationUSD'] * $dollarToday) +
                    ($totalDaily['totalBRL'] * $realToday) + ($totalDaily['totalRenovationBRL'] * $realToday) +
                    ($totalDaily['totalMXN'] * $pesoToday) + ($totalDaily['totalRenovationMXN'] * $pesoToday) +
                    $totalDaily['totalEUR'] + $totalDaily['totalRenovationEUR']) - (($totalDaily['totalRefundUSD'] * $dollarToday) + ($totalDaily['totalRefundBRL'] * $realToday) + ($totalDaily['totalRefundMXN'] * $pesoToday) + $totalDaily['totalRefundEUR'])); ?>€
        </td>
    </tr>

    <tr>
        <td style="border: 0px; text-align: center; font-size: 15px; font-weight: bold; padding-bottom: 10px" colspan="4">
        </td>
    </tr>

    <tr>
        <td style="border: 0px; text-align: center; font-size: 15px; font-weight: bold; padding-bottom: 10px" colspan="4">
            Weekly   <?php echo $weekBefore ?> to <?php echo $endDate; ?><br>
        </td>
    </tr>

    <tr>
        <td>
            New
        </td>
        <td style="text-align: center">
            <?php
            if(($totalWeekly['countEUR'] + $totalWeekly['countBRL'] + $totalWeekly['countMXN'] + $totalWeekly['countUSD'])>0)
                echo toMoney(($totalWeekly['totalEUR'] + ($totalWeekly['totalUSD'] * $dollarToday) + ($totalWeekly['totalBRL'] * $realToday) + ($totalWeekly['totalMXN'] * $pesoToday)) / ($totalWeekly['countEUR'] + $totalWeekly['countUSD'] + $totalWeekly['countBRL'] + $totalWeekly['countMXN']));
            ?>€
        </td>
        <td style="text-align: right;">
            <?php echo $totalWeekly['countEUR'] + $totalWeekly['countUSD'] + $totalWeekly['countBRL'] + $totalWeekly['countMXN']; ?>
        </td>
        <td style="text-align: right;">
            <?php echo toMoney($totalWeekly['totalEUR'] + ($totalWeekly['totalUSD'] * $dollarToday) + ($totalWeekly['totalBRL'] * $realToday) + ($totalWeekly['totalMXN'] * $pesoToday))."€" ?>
        </td>
    </tr>

    <tr>
        <td>
            Renovation
        </td>
        <td style="text-align: center">
            <?php
            if(($totalWeekly['countRenovationEUR'] + $totalWeekly['countRenovationUSD'] + $totalWeekly['countRenovationBRL'] + $totalWeekly['countRenovationMXN'])>0)
                echo toMoney(($totalWeekly['totalRenovationEUR'] + ($totalWeekly['totalRenovationUSD'] * $dollarToday) + ($totalWeekly['totalRenovationBRL'] * $realToday) + ($totalWeekly['totalRenovationMXN'] * $pesoToday)) / ($totalWeekly['countRenovationEUR'] + $totalWeekly['countRenovationUSD'] + $totalWeekly['countRenovationBRL'] + $totalWeekly['countRenovationMXN']));
            ?>€
        </td>
        <td style="text-align: right;">
            <?php echo $totalWeekly['countRenovationEUR'] + $totalWeekly['countRenovationUSD'] + $totalWeekly['countRenovationBRL'] + $totalWeekly['countRenovationMXN']; ?>
        </td>
        <td style="text-align: right;">
            <?php echo toMoney($totalWeekly['totalRenovationEUR'] + ($totalWeekly['totalRenovationUSD'] * $dollarToday) + ($totalWeekly['totalRenovationBRL'] * $realToday) + ($totalWeekly['totalRenovationMXN'] * $pesoToday))."€" ?>
        </td>
    </tr>

    <tr>
        <td style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px">
            Total
        </td>
        <td style="background-color: greenyellow; text-align: center; font-weight: bold; font-size: 15px">
            <?php
            if($totalWeekly['countRenovationUSD'] + $totalWeekly['countRenovationBRL'] + $totalWeekly['countRenovationMXN'] + $totalWeekly['countRenovationEUR'] + $totalWeekly['countUSD'] + $totalWeekly['countBRL'] + $totalWeekly['countMXN'] + $totalWeekly['countEUR'] > 0)
                echo toMoney((($totalWeekly['totalUSD'] * $dollarToday) + ($totalWeekly['totalRenovationUSD'] * $dollarToday) +
                        ($totalWeekly['totalBRL'] * $realToday) + ($totalWeekly['totalRenovationBRL'] * $realToday) +
                        ($totalWeekly['totalMXN'] * $pesoToday) + ($totalWeekly['totalRenovationMXN'] * $pesoToday) +
                        $totalWeekly['totalEUR'] + $totalWeekly['totalRenovationEUR']) / ($totalWeekly['countRenovationUSD'] + $totalWeekly['countRenovationBRL'] + $totalWeekly['countRenovationMXN'] + $totalWeekly['countRenovationEUR'] + $totalWeekly['countUSD'] + $totalWeekly['countBRL'] + $totalWeekly['countMXN'] + $totalWeekly['countEUR']));
            ?>€
        </td>
        <td style="background-color: greenyellow; text-align: right; font-weight: bold; font-size: 15px">
            <?php echo $totalWeekly['countRenovationUSD'] + $totalWeekly['countRenovationBRL'] + $totalWeekly['countRenovationMXN'] + $totalWeekly['countRenovationEUR'] + $totalWeekly['countUSD'] + $totalWeekly['countBRL'] + $totalWeekly['countMXN'] + $totalWeekly['countEUR']?>
        </td>
        <td style="background-color: greenyellow; text-align: right; font-weight: bold; font-size: 15px">
            <?php echo toMoney(($totalWeekly['totalUSD'] * $dollarToday) + ($totalWeekly['totalRenovationUSD'] * $dollarToday) +
                ($totalWeekly['totalBRL'] * $realToday) + ($totalWeekly['totalRenovationBRL'] * $realToday) +
                ($totalWeekly['totalMXN'] * $pesoToday) + ($totalWeekly['totalRenovationMXN'] * $pesoToday) +
                $totalWeekly['totalEUR'] + $totalWeekly['totalRenovationEUR']) ?>€
        </td>
    </tr>

    <tr>
        <td style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px" colspan="2">
            Refunds
        </td>
        <td style="background-color: #ff0000; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo $totalWeekly['contRefundUSD'] + $totalWeekly['contRefundBRL'] + $totalWeekly['contRefundMXN'] + $totalWeekly['contRefundEUR']; ?>
        </td>
        <td style="background-color: #ff0000; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo toMoney(($totalWeekly['totalRefundUSD'] * $dollarToday) + ($totalWeekly['totalRefundBRL'] * $realToday) + ($totalWeekly['totalRefundMXN'] * $pesoToday) + $totalWeekly['totalRefundEUR'])."€"; ?>
        </td>
    </tr>

    <tr>
        <td style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px" colspan="3">
            Total
        </td>
        <td style="background-color: green; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo toMoney((($totalWeekly['totalUSD'] * $dollarToday) + ($totalWeekly['totalRenovationUSD'] * $dollarToday) +
                    ($totalWeekly['totalBRL'] * $realToday) + ($totalWeekly['totalRenovationBRL'] * $realToday) +
                    ($totalWeekly['totalMXN'] * $pesoToday) + ($totalWeekly['totalRenovationMXN'] * $pesoToday) +
                    $totalWeekly['totalEUR'] + $totalWeekly['totalRenovationEUR']) - (($totalWeekly['totalRefundUSD'] * $dollarToday) + $totalWeekly['totalRefundEUR'])); ?>€
        </td>
    </tr>

</table>
