<?php
/* @var $this AbaPartnertypesController */
/* @var $model AbaPartnertypes */

$this->breadcrumbs = array(
  'Partner types' => array('admin'),
  $model->idType,
);

$this->menu = array(
  array('label' => 'List Partner types', 'url' => array('index')),
  array('label' => 'Create Partner type', 'url' => array('create')),
  array('label' => 'Update Partner type', 'url' => array('update', 'id' => $model->idType)),
  array(
    'label' => 'Delete Partner type',
    'url' => '#',
    'linkOptions' => array(
      'submit' => array('delete', 'id' => $model->idType),
      'confirm' => 'Are you sure you want to delete this item?'
    )
  ),
  array('label' => 'Manage Partner types', 'url' => array('admin')),
);
?>

<h3>View Partner types <b><?php echo $model->idType . ": " . $model->nameType; ?></b></h3>

<?php $this->widget(
  'application.components.widgets.CDetailViewABA',
  array(
    'data' => $model,
    'attributes' => array(
      'idType',
      'nameType',
      'dateAdd',
    ),
  )
); ?>
