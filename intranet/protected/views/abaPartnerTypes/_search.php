<?php
/* @var $this AbaPartnertypesController */
/* @var $model AbaPartnertypes */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
      'CActiveForm',
      array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
      )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'idType'); ?>
        <?php echo $form->textField($model, 'idType', array('size' => 5, 'maxlength' => 20)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'nameType'); ?>
        <?php echo $form->textField($model, 'nameType', array('size' => 30, 'maxlength' => 45)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'dateAdd'); ?>
        <?php echo $form->textField($model, 'dateAdd', array('size' => 15, 'maxlength' => 19)); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->