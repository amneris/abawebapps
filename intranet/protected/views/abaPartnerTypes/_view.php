<?php
/* @var $this AbaPartnertypesController */
/* @var $model AbaPartnertypes */
?>

<div class="view">

    <b/><?php echo CHtml::encode($data->getAttributeLabel('idType')); ?>:</b>
    <?php echo CHtml::encode($data->idType); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('nameType')); ?>:</b>
    <?php echo CHtml::encode($data->nameType); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('dateAdd')); ?>:</b>
    <?php echo CHtml::encode($data->dateAdd); ?>
    <br/>

</div>