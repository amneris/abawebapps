<?php
/* @var $this AbaPartnertypesController */
/* @var $model AbaPartnertypes */

$this->breadcrumbs = array(
  'Aba Partner types',
);

$this->menu = array(
  array('label' => 'Create Partner types', 'url' => array('create')),
  array('label' => 'Manage Partner types', 'url' => array('admin')),
);
?>

<h3>Partner types</h3>

<?php $this->widget(
  'zii.widgets.CListView',
  array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
  )
); ?>
