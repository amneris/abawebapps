<?php
/* @var $this AbaPartnertypesController */
/* @var $model AbaPartnertypes */

$this->breadcrumbs = array(
  'Aba Partner types' => array('admin'),
  $model->idType => array('view', 'id' => $model->idType),
  'Update',
);

$this->menu = array(
  array('label' => 'List Partner types', 'url' => array('index')),
  array('label' => 'Create Partner type', 'url' => array('create')),
  array('label' => 'View Partner types', 'url' => array('view', 'id' => $model->idType)),
  array('label' => 'Manage Partner types', 'url' => array('admin')),
);
?>

    <h3>Update Partner type: <?php echo $model->idType; ?></h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>