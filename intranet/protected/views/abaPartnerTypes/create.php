<?php
/* @var $this AbaPartnertypesController */
/* @var $model AbaPartnertypes */

$this->breadcrumbs = array(
  'Partner types' => array('admin'),
  'Create',
);

$this->menu = array(
  array('label' => 'Manage Partner type', 'url' => array('admin')),
);
?>

    <h3>Partner types</h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>