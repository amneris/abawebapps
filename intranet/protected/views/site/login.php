<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>
<h3>Login</h3>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <table id="myForm">
        <tr>
            <td>
                <?php echo $form->labelEx($model,'username'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'username'); ?>
                <?php echo $form->error($model,'username'); ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model,'password'); ?>
            </td>
            <td>
                <?php echo $form->passwordField($model,'password'); ?>
                <?php echo $form->error($model,'password'); ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->label($model,'rememberMe'); ?>
            </td>
            <td>
                <?php echo $form->checkBox($model,'rememberMe'); ?>
                <?php echo $form->error($model,'rememberMe'); ?>
            </td>
        </tr>
    </table>

	<div class="row buttons" align="center">
		<?php echo CHtml::submitButton('Login', array('id' => 'btnLogin') ); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
