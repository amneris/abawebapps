<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name;

//set de number of elements for each section.
$numElements = 7;
$porcent= 100 / $numElements;

?>
<!-- version '[2013-03-20]' -->
<!-- UserRole <?php echo(Yii::app()->user->getId()) ?> -->

<!--<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1> -->
<?php

//Gestion de usuarios
if(AbaUserBackoffice::isValid(array('marketing', 'root', 'support','teacher')))
{
    $cont=0;
    echo "<div id=\"tabsStyle\">";
    echo "<span id=\"tabsTitleStyle\">User Management</span>";
    echo "</div>";

    echo "<table id=\"pastilla\">";
    echo "<tr>";

    if(AbaUserBackoffice::isValid(array('marketing', 'root', 'support','teacher')))
    {

        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/users.png');
        echo "<center>".CHtml::link($image, array('abaUser/admin'))."</br>";
        echo CHtml::link('Users',array('abaUser/admin'))."</center>";

        echo "</td>";
    }
    else
    {
        $cont++;
    }

    if(AbaUserBackoffice::isValid(array('root', 'support')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/datos_bancarios.png');
        echo "<center>".CHtml::link($image, array('abaUserCreditForms/admin'))."<br>";
        echo CHtml::link('Bank Details',array('abaUserCreditForms/admin'))."</center>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }
    
    if(AbaUserBackoffice::isValid(array('root', 'support','marketing')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/certificado.png');
        echo "<center>".CHtml::link($image, array('abaUser/expresscertificates'))."<br>";
        echo CHtml::link('Level Certificates', array('abaUser/expresscertificates'))."</center>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }
    
    for($i=$cont; $i<=$numElements; $i++)
    {
        echo "<td width=\"".$porcent."%\">";
        echo "</td>";
    }
    
    echo "</tr>";
    echo "</table>";
}
//fin Gestion de usuarios

//Gestion de pagos
if(AbaUserBackoffice::isValid(array('root', 'support','marketing')))
{
    $cont=0;
    echo "<div id=\"tabsStyle\">";
    echo "<span id=\"tabsTitleStyle\">Payment Management</span>";
    echo "</div>";

    echo "<table id=\"pastilla\">";
    echo "<tr>";

    if(AbaUserBackoffice::isValid(array('root', 'support','marketing')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/lineas_pago.png');
        echo "<center>".CHtml::link($image, array('abaPayments/admin'))."<br>";
        echo CHtml::link('Paylines',array('abaPayments/admin'))."<br>";
        echo "</td>";
    }
    else
    {
       $cont++;
    }

    if(AbaUserBackoffice::isValid(array('root', 'support','marketing')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/paylineLogs.png');
        echo "<center>".CHtml::link($image, array('abaPaymentsControlCheck/admin'))."<br>";
        echo CHtml::link('Paylines Log',array('abaPaymentsControlCheck/admin'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }

    if(AbaUserBackoffice::isValid(array('root', 'support')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/boletos.png');
        echo "<center>".CHtml::link($image, array('abaPaymentsBoleto/admin'))."<br>";
        echo CHtml::link('Boletos',array('abaPaymentsBoleto/admin'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }

    if(AbaUserBackoffice::isValid(array('root', 'support')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/reconciliation.png');
        echo "<center>".CHtml::link($image, array('/reconciliationParser/reconciliation/importfile'))."<br>";
        echo CHtml::link('Import reconciliation files',  array('/reconciliationParser/reconciliation/importfile'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }

    if(AbaUserBackoffice::isValid(array('root', 'support')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/reconciliation_log.png');
        echo "<center>".CHtml::link($image, array('/reconciliationParser/LogReconciliation/admin'))."<br>";
        echo CHtml::link('Reconciliation files log',  array('/reconciliationParser/logreconciliation/admin'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }

    for($i=$cont; $i<=$numElements; $i++)
    {
        echo "<td width=\"".$porcent."%\">";
        echo "</td>";
    }

    echo "</tr>";
    echo "</table>";
}
//fin Gestion de pagos

//Gestion de estadisticas
if(AbaUserBackoffice::isValid(array('root', 'support','marketing')))
{
    $cont=0;
    echo "<div id=\"tabsStyle\">";
    echo "<span id=\"tabsTitleStyle\">Statistics Management</span>";
    echo "</div>";

    echo "<table id=\"pastilla\">";
    echo "<tr>";

    if(AbaUserBackoffice::isValid(array('root', 'support','marketing')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/saleDashboard.png');
        echo "<center>".CHtml::link($image, '', array('href' => 'http://bi.aba.land/report.php?r=15&m=4&h=3a57e29f2997ad29241510a86ae90cabb5327666&i=1', 'target' => '_blank') )."<br>";
        echo CHtml::link('Sales Dashboard','', array('href' => 'http://bi.aba.land/report.php?r=15&m=4&h=3a57e29f2997ad29241510a86ae90cabb5327666&i=1', 'target' => '_blank') )."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }

    if(AbaUserBackoffice::isValid(array('root', 'support','marketing')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/saleDashboardMobil.png');
        echo "<center>".CHtml::link($image, array('abaStatistics/totalSalesDashboardMobil'))."<br>";
        echo CHtml::link('Daily & Weekly Sales',array('abaStatistics/totalSalesDashboardMobil'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }

    if(AbaUserBackoffice::isValid(array('root', 'support','marketing')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/salesByCountry.png');
        echo "<center>".CHtml::link($image, array('abaStatistics/totalSalesDashboardByCountry'))."<br>";
        echo CHtml::link('Sales by Country',array('abaStatistics/totalSalesDashboardByCountry'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }

    if(AbaUserBackoffice::isValid(array('root', 'support','marketing')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/salesByPartner.png');
        echo "<center>".CHtml::link($image, array('abaStatistics/totalSalesDashboardByPartner'))."<br>";
        echo CHtml::link('Sales by Partner',array('abaStatistics/totalSalesDashboardByPartner'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }

    if(AbaUserBackoffice::isValid(array('root', 'support','marketing')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/registrationDashboard.png');
        echo "<center>".CHtml::link($image, 'http://bi.aba.land/report.php?r=35&m=4&h=d37a5b6520d787d26e2bd3b4ac941bd090cdd435&i=1', array('target'=>'_blank'))."<br>";
        echo CHtml::link('Registration Dashboard','http://bi.aba.land/report.php?r=35&m=4&h=d37a5b6520d787d26e2bd3b4ac941bd090cdd435&i=1', array('target'=>'_blank'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }

    for($i=$cont; $i<=$numElements; $i++)
    {
        echo "<td width=\"".$porcent."%\">";
        echo "</td>";
    }

    echo "</tr>";
    echo "</table>";
}
//fin Gestion de Stadisticas

//Gestion de teachers
if(AbaUserBackoffice::isValid(array('root', 'teacher','marketing')))
{
    $cont=0;
    echo "<div id=\"tabsStyle\">";
    echo "<span id=\"tabsTitleStyle\">Teachers Management</span>";
    echo "</div>";
    echo "<table id=\"pastilla\">";
    echo "<tr>";
    
    if(AbaUserBackoffice::isValid(array('root', 'teacher', 'marketing')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/mensaje.png');
        echo "<center>".CHtml::link($image, array('abaQuestionsAndAnswers/admin'))."<br>";
        echo CHtml::link('Messages',array('abaQuestionsAndAnswers/admin'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }

    if(AbaUserBackoffice::isValid(array('root', 'teacher', 'marketing')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/sentAndAnswered.png');
        echo "<center>".CHtml::link($image, array('abaQuestionsAndAnswers/getMessageSentAndAnswered'))."<br>";
        echo CHtml::link('Sent & Answered',array('abaQuestionsAndAnswers/getMessageSentAndAnswered'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }

    if(AbaUserBackoffice::isValid(array('root', 'teacher', 'marketing')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/totalSentAndAnswered.png');
        echo "<center>".CHtml::link($image, array('abaQuestionsAndAnswers/getTotalMessageSentAndAnswered'))."<br>";
        echo CHtml::link('Total Sent & Answered',array('abaQuestionsAndAnswers/getTotalMessageSentAndAnswered'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }
    
    for($i=$cont; $i<=$numElements; $i++)
    {
        echo "<td width=\"".$porcent."%\">";
        echo "</td>";
    }
    
    echo "</tr>";
    echo "</table>";
}
//fin gestion de teachers

//Gestion de ventas flash
if(AbaUserBackoffice::isValid(array('marketing', 'root', 'support')))
{
    $cont=0;
    echo "<div id=\"tabsStyle\">";
    echo "<span id=\"tabsTitleStyle\">Flash Sales Management</span>";
    echo "</div>";
    echo "<table id=\"pastilla\">";
    echo "<tr>";
    
    if(AbaUserBackoffice::isValid(array('marketing', 'root', 'support')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/cupon.png');
        echo "<center>".CHtml::link($image, array('AbaVentasFlash/index'))."<br>";
        echo CHtml::link('Coupons',array('AbaVentasFlash/index'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }
    
    if(AbaUserBackoffice::isValid(array('marketing', 'root')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/agrupador_sales.png');
        echo "<center>".CHtml::link($image, array('AbaVentasFlash/salesForAgrupador'))."<br>";
        echo CHtml::link('Grouper Sales',array('AbaVentasFlash/salesForAgrupador'))."<br>";
        echo "</td>";
    }
    else
    {
       $cont++;
    }
    
   if(AbaUserBackoffice::isValid(array('root')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/agrupadores.png');
        echo "<center>".CHtml::link($image, array('AbaAgrupadores/index'))."<br>";
        echo CHtml::link('Groupers',array('AbaAgrupadores/index'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }
    
    if(AbaUserBackoffice::isValid(array('marketing','root')))
    {
       echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/upload_from_file.png');
        echo "<center>".CHtml::link($image, array('AbaVentasFlash/createFromFile'))."<br>";
        echo CHtml::link('Load coupons from a file',array('AbaVentasFlash/createFromFile'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }
    
    for($i=$cont; $i<=$numElements; $i++)
    {
       echo "<td width=\"".$porcent."%\">";
        echo "</td>";
    }
    
    echo "</tr>";
    echo "</table>";
    
}  
//fin Gestion de ventas flash

//gestion de curso
if(AbaUserBackoffice::isValid(array('root','teacher','marketing')))
{
    $cont=0;
    echo "<div id=\"tabsStyle\">";
    echo "<span id=\"tabsTitleStyle\">Course Management</span>";
    echo "</div>";
    echo "<table id=\"pastilla\">";
    echo "<tr>";
    
    if(AbaUserBackoffice::isValid(array('root')))
    {
       echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/Followup.png');
        echo "<center>".CHtml::link($image, array('abaFollowup4/admin'))."<br>";
       echo CHtml::link('Units Followup',array('abaFollowup4/admin'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }
    
    if(AbaUserBackoffice::isValid(array('root')))
    {
       echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/FollowupHtml4.png');
        echo "<center>".CHtml::link($image, array('abaFollowupHtml4/admin'))."<br>";
        echo CHtml::link('Lines Exercises',array('abaFollowupHtml4/admin'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }

    if(AbaUserBackoffice::isValid(array('root','marketing','teacher','marketing')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/b2bStudyReport.png');
        echo "<center>".CHtml::link($image, array('b2bFollowup/admin'))."<br>";
        echo CHtml::link('B2B Study Report', array('b2bFollowup/admin'))."</center>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }

    if(AbaUserBackoffice::isValid(array('root','marketing','teacher','marketing')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/b2cStudyReport.png');
        echo "<center>".CHtml::link($image, array('b2cFollowup/admin'))."<br>";
        echo CHtml::link('B2C Study Report', array('b2cFollowup/admin'))."</center>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }
    
    for($i=$cont; $i<=$numElements; $i++)
    {
       echo "<td width=\"".$porcent."%\">";
        echo "</td>";
    }
    
    echo "</tr>";
    echo "</table>";
    
}
//fin Gestion de curso

//Gestion de productos
if(AbaUserBackoffice::isValid(array('root','marketing','support')))
{    
    $cont=0;
    echo "<div id=\"tabsStyle\">";
    echo "<span id=\"tabsTitleStyle\">Product Management</span>";
    echo "</div>";
    echo "<table id=\"pastilla\">";
    echo "<tr>";
    
    if(AbaUserBackoffice::isValid(array('root','marketing','support')))
    {
       echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/product.png');
        echo "<center>".CHtml::link($image, array('abaProductsPrices/admin'))."<br>";
        echo CHtml::link('Product List & Prices',array('abaProductsPrices/admin'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }
    
    if(AbaUserBackoffice::isValid(array('root','marketing','support')))
    {
       echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/products_periodicity.png');
        echo "<center>".CHtml::link($image, array('abaProdPeriodicity/admin'))."<br>";
        echo CHtml::link('Products Periodicity',array('abaProdPeriodicity/admin'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }
    
    for($i=$cont; $i<=$numElements; $i++)
    {
       echo "<td width=\"".$porcent."%\">";
        echo "</td>";
    }
    
    echo "</tr>";
    echo "</table>";
}
//fin Gestion de productos

//Other tools
if(AbaUserBackoffice::isValid(array('marketing', 'root', 'support', 'teacher')))
{  
    $cont=0;
    echo "<div id=\"tabsStyle\">";
    echo "<span id=\"tabsTitleStyle\">Other Tools</span>";
    echo "</div>";
    echo "<table id=\"pastilla\">";
    echo "<tr>";
    
    if(AbaUserBackoffice::isValid(array('marketing', 'root')))
    { 
       echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/codigo_promocion.png');
        echo "<center>".CHtml::link($image, array('abaProductsPromos/admin'))."<br>";
        echo CHtml::link('Promotion Codes',array('abaProductsPromos/admin'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }
    
    if(AbaUserBackoffice::isValid(array('marketing', 'root')))
    { 
       echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/partner_list.png');
        echo "<center>".CHtml::link($image, array('abaPartnersList/admin'))."<br>";
        echo CHtml::link('Partner List',array('abaPartnersList/admin'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }

    if(AbaUserBackoffice::isValid(array('marketing', 'root')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/sources_list.png');
        echo "<center>".CHtml::link($image, array('sourcesList/admin'))."<br>";
        echo CHtml::link('Sources List',array('sourcesList/admin'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }
    
    if(AbaUserBackoffice::isValid(array('root')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/backoffice.png');
        echo "<center>".CHtml::link($image, array('abaUserBackoffice/admin'))."<br>";
        echo CHtml::link('Backoffice Users', array('abaUserBackoffice/admin'))."</center>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }
    
    if(AbaUserBackoffice::isValid(array('support','marketing','teacher','root')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/password.png');
        echo "<center>".CHtml::link($image, array('abaUserBackoffice/updateNormalUser&id='.Yii::app()->user->idUser))."<br>";
        echo CHtml::link('Change my password', array('abaUserBackoffice/updateNormalUser&id='.Yii::app()->user->idUser))."</center>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }

    if(AbaUserBackoffice::isValid(array('support','root','marketing')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/configs.png');
        echo "<center>".CHtml::link($image, array('config/updateConfig'))."<br>";
        echo CHtml::link('Enable/Disable Configs', array('config/updateConfig'))."</center>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }
    
    for($i=$cont; $i<=$numElements; $i++)
    {
       echo "<td width=\"".$porcent."%\">";
        echo "</td>";
    }
    
    echo "</tr>";
    echo "</table>";
}
//fin Other tools

if(AbaUserBackoffice::isValid(array('marketing', 'support', 'root'))) {
    $cont = 0;
    echo "<div id=\"tabsStyle\">";
    echo "<span id=\"tabsTitleStyle\">Extranet</span>";
    echo "</div>";
    echo "<table id=\"pastilla\">";
    echo "<tr>";

    if (AbaUserBackoffice::isValid(array('marketing', 'root'))) {
        echo "<td width=\"" . $porcent . "%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/B2bPartnerList.png');
        echo "<center>" . CHtml::link($image, array('abaChannelList/admin')) . "<br>";
        echo CHtml::link('B2B Channels List', array('abaChannelList/admin')) . "<br>";
        echo "</td>";
    } else {
        $cont++;
    }

    if (AbaUserBackoffice::isValid(array('marketing', 'root'))) {
        echo "<td width=\"" . $porcent . "%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/enterpriseChannels.png');
        echo "<center>" . CHtml::link($image, array('abaB2bEnterprise/admin')) . "<br>";
        echo CHtml::link('Enterprise Channels', array('abaB2bEnterprise/admin')) . "<br>";
        echo "</td>";
    } else
    {
        $cont++;
    }

    if(AbaUserBackoffice::isValid(array('root', 'support','marketing')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/saleDashboard.png');
        echo "<center>".CHtml::link($image, array('abaStatisticsB2b/totalSalesDashboard'))."<br>";
        echo CHtml::link('Sales Dashboard',array('abaStatisticsB2b/totalSalesDashboard'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }

    if(AbaUserBackoffice::isValid(array('root', 'support','marketing')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/saleDashboardMobil.png');
        echo "<center>".CHtml::link($image, array('abaStatisticsB2b/totalSalesDashboardMobil'))."<br>";
        echo CHtml::link('Daily & Weekly Sales',array('abaStatisticsB2b/totalSalesDashboardMobil'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }

    if(AbaUserBackoffice::isValid(array('root', 'support','marketing')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/salesByCountry.png');
        echo "<center>".CHtml::link($image, array('abaStatisticsB2b/totalSalesDashboardByCountry'))."<br>";
        echo CHtml::link('Sales by Country',array('abaStatisticsB2b/totalSalesDashboardByCountry'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }

    if(AbaUserBackoffice::isValid(array('root', 'support','marketing')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/salesByPartner.png');
        echo "<center>".CHtml::link($image, array('abaStatisticsB2b/totalSalesDashboardByPartner'))."<br>";
        echo CHtml::link('Sales by Partner',array('abaStatisticsB2b/totalSalesDashboardByPartner'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }

    if(AbaUserBackoffice::isValid(array('root', 'support','marketing')))
    {
        echo "<td width=\"".$porcent."%\">";
        $image = CHtml::image(Yii::app()->request->baseUrl . '/images/registrationDashboard.png');
        echo "<center>".CHtml::link($image, array('abaStatisticsB2b/registrationDashboard'))."<br>";
        echo CHtml::link('Registration Dashboard',array('abaStatisticsB2b/registrationDashboard'))."<br>";
        echo "</td>";
    }
    else
    {
        $cont++;
    }

    for($i=$cont; $i<=$numElements; $i++)
    {
        echo "<td width=\"".$porcent."%\">";
        echo "</td>";
    }

    echo "</tr>";
    echo "</table>";
}

?>