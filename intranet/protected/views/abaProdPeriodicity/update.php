<?php
/* @var $this AbaProdPeriodicityController */
/* @var $model AbaProdPeriodicity */

$this->breadcrumbs=array(
	'Products Periodicity'=>array('admin'),
	$model->descriptionText=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AbaProdPeriodicity', 'url'=>array('index')),
	array('label'=>'Create AbaProdPeriodicity', 'url'=>array('create')),
	array('label'=>'View AbaProdPeriodicity', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AbaProdPeriodicity', 'url'=>array('admin')),
);
?>

<h3>Update products periodicity for <b><?php echo $model->descriptionText; ?></b></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>