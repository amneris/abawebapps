<?php
/* @var $this AbaProdPeriodicityController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Products Periodicity',
);

$this->menu=array(
	array('label'=>'Create AbaProdPeriodicity', 'url'=>array('create')),
	array('label'=>'Manage AbaProdPeriodicity', 'url'=>array('admin')),
);
?>

<h3>Aba Prod Periodicities</h3>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
