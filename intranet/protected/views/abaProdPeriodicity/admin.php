<?php
/* @var $this AbaProdPeriodicityController */
/* @var $model AbaProdPeriodicity */

$this->breadcrumbs=array(
	'Products Periodicity',
);

$this->menu=array(
	array('label'=>'List AbaProdPeriodicity', 'url'=>array('index')),
	array('label'=>'Create AbaProdPeriodicity', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('aba-prod-periodicity-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Products periodicity</h3>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'aba-prod-periodicity-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
                    'prevPageLabel'  => '<',
                    'nextPageLabel'  => '>',
                    'firstPageLabel' => '<<',
                    'lastPageLabel'  => '>>',),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'days',
		'months',
		'descriptionText',
        /*
		array(
			'class'=>'CButtonColumn',
            'header'=>'Options',
		),*/
	),
)); ?>
