<?php
/* @var $this AbaProdPeriodicityController */
/* @var $model AbaProdPeriodicity */

$this->breadcrumbs=array(
	'Products Periodicity'=>array('admin'),
	$model->descriptionText,
);

$this->menu=array(
	array('label'=>'List AbaProdPeriodicity', 'url'=>array('index')),
	array('label'=>'Create AbaProdPeriodicity', 'url'=>array('create')),
	array('label'=>'Update AbaProdPeriodicity', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AbaProdPeriodicity', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AbaProdPeriodicity', 'url'=>array('admin')),
);
?>

<h3>View products periodicity for <b><?php echo $model->descriptionText; ?></b></h3>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'days',
		'months',
		'descriptionText',
	),
)); ?>
