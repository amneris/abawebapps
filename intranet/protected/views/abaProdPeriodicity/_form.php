<?php
/* @var $this AbaProdPeriodicityController */
/* @var $model AbaProdPeriodicity */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aba-prod-periodicity-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <table id="myForm">
        <tr>
            <td>
                <?php echo $form->labelEx($model,'days'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'days',array('size'=>4,'maxlength'=>4)); ?>
                <?php echo $form->error($model,'days'); ?>
            </td>
        </tr>
        
         <tr>
            <td>
                <?php echo $form->labelEx($model,'months'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'months',array('size'=>2,'maxlength'=>2)); ?>
                <?php echo $form->error($model,'months'); ?>
            </td>
        </tr>
        
         <tr>
            <td>
                <?php echo $form->labelEx($model,'descriptionText'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'descriptionText',array('size'=>20,'maxlength'=>20)); ?>
                <?php echo $form->error($model,'descriptionText'); ?>
            </td>
        </tr>
        
    </table>

	<div class="row buttons" align="center">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->