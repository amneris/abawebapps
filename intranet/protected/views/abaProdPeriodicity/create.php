<?php
/* @var $this AbaProdPeriodicityController */
/* @var $model AbaProdPeriodicity */

$this->breadcrumbs=array(
	'Products Periodicity'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AbaProdPeriodicity', 'url'=>array('index')),
	array('label'=>'Manage AbaProdPeriodicity', 'url'=>array('admin')),
);
?>

<h3>Create AbaProdPeriodicity</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>