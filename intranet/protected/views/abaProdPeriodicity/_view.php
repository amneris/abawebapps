<?php
/* @var $this AbaProdPeriodicityController */
/* @var $data AbaProdPeriodicity */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('days')); ?>:</b>
	<?php echo CHtml::encode($data->days); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('months')); ?>:</b>
	<?php echo CHtml::encode($data->months); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descriptionText')); ?>:</b>
	<?php echo CHtml::encode($data->descriptionText); ?>
	<br />


</div>