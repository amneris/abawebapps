<?php
/* @var $this AbaFollowup4Controller */
/* @var $model AbaFollowup4 */

$this->breadcrumbs=array(
	'Aba Followup4s'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AbaFollowup4', 'url'=>array('index')),
	array('label'=>'Manage AbaFollowup4', 'url'=>array('admin')),
);
?>

<h3>Create AbaFollowup4</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>