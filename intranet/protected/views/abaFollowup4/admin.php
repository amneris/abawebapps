<?php
/* @var $this AbaFollowup4Controller */
/* @var $model AbaFollowup4 */

$this->breadcrumbs=array(
	'Units Followups',
	
);

$this->menu=array(
	array('label'=>'List AbaFollowup4', 'url'=>array('index')),
	array('label'=>'Create AbaFollowup4', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('aba-followup4-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Units followups</h3>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'aba-followup4-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
                    'prevPageLabel'  => '<',
                    'nextPageLabel'  => '>',
                    'firstPageLabel' => '<<',
                    'lastPageLabel'  => '>>',),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'themeid',
		'userid',
		'lastchange',
		'all_por',
		'all_err',
		/*
		'all_ans',
		'all_time',
		'sit_por',
		'stu_por',
		'dic_por',
		'dic_ans',
		'dic_err',
		'rol_por',
		'gra_por',
		'gra_ans',
		'gra_err',
		'wri_por',
		'wri_ans',
		'wri_err',
		'new_por',
		'spe_por',
		'spe_ans',
		'time_aux',
		'gra_vid',
		'rol_on',
		'exercises',
		'eva_por',
		*/
		array(
			'class'=>'CButtonColumn',
            'header'=>'Options'
		),
	),
)); ?>
