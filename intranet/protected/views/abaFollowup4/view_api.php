<?php
/* @var $this AbaFollowup4Controller */
/* @var $model AbaFollowup4 */

$this->breadcrumbs=array(
	'Units Followup'=>array('admin'),
    'Unit '.$model->themeid,
);

$this->menu=array(
	array('label'=>'List AbaFollowup4', 'url'=>array('index')),
	array('label'=>'Create AbaFollowup4', 'url'=>array('create')),
	array('label'=>'Update AbaFollowup4', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AbaFollowup4', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AbaFollowup4', 'url'=>array('admin')),
);
?>

<h3>View followup for unit <b><?php echo $model->themeid; ?></b> for user id <b><?php echo $model->userid; ?></b></h3>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'progressVersion',
		'idFollowup',
		'themeid',
		'userid',
		'lastchange',
		'all_por',
		'all_err',
		'all_ans',
		'all_time',
		'sit_por',
		'stu_por',
		'dic_por',
		'dic_ans',
		'dic_err',
		'rol_por',
		'gra_por',
		'gra_ans',
		'gra_err',
		'wri_por',
		'wri_ans',
		'wri_err',
		'new_por',
		'spe_por',
		'spe_ans',
		'time_aux',
		'gra_vid',
		'rol_on',
		'exercises',
		'eva_por',
	),
)); ?>
