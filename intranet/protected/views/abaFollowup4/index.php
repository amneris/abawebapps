<?php
/* @var $this AbaFollowup4Controller */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Aba Followup4s',
);

$this->menu=array(
	array('label'=>'Create AbaFollowup4', 'url'=>array('create')),
	array('label'=>'Manage AbaFollowup4', 'url'=>array('admin')),
);
?>

<h3>Aba Followup4s</h3>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
