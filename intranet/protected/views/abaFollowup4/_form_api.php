<?php
/* @var $this AbaFollowup4Controller */
/* @var $model AbaFollowup4 */
/* @var $form CActiveForm */
?>
<script type="text/javascript">
    function set100Percentage()
    {
        document.getElementById('allPor').value = 100;
        document.getElementById('allTime').value = 145;
        document.getElementById('sitPor').value = 100;
        document.getElementById('stuPor').value = 100;
        document.getElementById('dicPor').value = 100;
        document.getElementById('rolPor').value = 100;
        document.getElementById('graPor').value = 100;
        document.getElementById('wriPor').value = 100;
        document.getElementById('newPor').value = 100;
        document.getElementById('spePor').value = 100;
        document.getElementById('graVid').value = 100;
    }

</script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aba-followup4-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <table id="myForm">

        <tr>
            <td>
                <?php echo $form->labelEx($model,'progressVersion'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'progressVersion',array('size'=>4,'maxlength'=>4)); ?>
                <?php echo $form->error($model,'progressVersion'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'idFollowup'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'idFollowup',array('size'=>4)); ?>
                <?php echo $form->error($model,'idFollowup'); ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <?php echo $form->labelEx($model,'themeid'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'themeid',array('size'=>4,'maxlength'=>4,'disabled'=>'true')); ?>
                <?php echo $form->error($model,'themeid'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'userid'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'userid',array('size'=>4,'maxlength'=>4,'disabled'=>'true')); ?>
                <?php echo $form->error($model,'userid'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'lastchange'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'lastchange'); ?>
                <?php echo $form->error($model,'lastchange'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'all_por'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'all_por',array('size'=>4,'maxlength'=>4,'id'=>'allPor')); ?>
                <?php echo $form->error($model,'all_por'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'all_err'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'all_err',array('size'=>4,'maxlength'=>4)); ?>
                <?php echo $form->error($model,'all_err'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'all_ans'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'all_ans',array('size'=>4,'maxlength'=>4)); ?>
                <?php echo $form->error($model,'all_ans'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'all_time'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'all_time',array('size'=>4,'maxlength'=>4,'id'=>'allTime')); ?>
                <?php echo $form->error($model,'all_time'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'sit_por'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'sit_por',array('size'=>4,'maxlength'=>4,'id'=>'sitPor')); ?>
                <?php echo $form->error($model,'sit_por'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'stu_por'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'stu_por',array('size'=>4,'maxlength'=>4,'id'=>'stuPor')); ?>
                <?php echo $form->error($model,'stu_por'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'dic_por'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'dic_por',array('size'=>4,'maxlength'=>4,'id'=>'dicPor')); ?>
                <?php echo $form->error($model,'dic_por'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'dic_ans'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'dic_ans',array('size'=>4,'maxlength'=>4)); ?>
                <?php echo $form->error($model,'dic_ans'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'dic_err'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'dic_err',array('size'=>4,'maxlength'=>4)); ?>
                <?php echo $form->error($model,'dic_err'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'rol_por'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'rol_por',array('size'=>4,'maxlength'=>4,'id'=>'rolPor')); ?>
                <?php echo $form->error($model,'rol_por'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'gra_por'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'gra_por',array('size'=>4,'maxlength'=>4,'id'=>'graPor')); ?>
                <?php echo $form->error($model,'gra_por'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'gra_ans'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'gra_ans',array('size'=>4,'maxlength'=>4)); ?>
                <?php echo $form->error($model,'gra_ans'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'gra_err'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'gra_err',array('size'=>4,'maxlength'=>4)); ?>
                <?php echo $form->error($model,'gra_err'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'wri_por'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'wri_por',array('size'=>4,'maxlength'=>4, 'id'=>'wriPor')); ?>
                <?php echo $form->error($model,'wri_por'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'wri_ans'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'wri_ans',array('size'=>4,'maxlength'=>4)); ?>
                <?php echo $form->error($model,'wri_ans'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'wri_err'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'wri_err',array('size'=>4,'maxlength'=>4)); ?>
                <?php echo $form->error($model,'wri_err'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'new_por'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'new_por',array('size'=>4,'maxlength'=>4,'id'=>'newPor')); ?>
                <?php echo $form->error($model,'new_por'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'spe_por'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'spe_por',array('size'=>4,'maxlength'=>4,'id'=>'spePor')); ?>
                <?php echo $form->error($model,'spe_por'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'spe_ans'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'spe_ans',array('size'=>4,'maxlength'=>4)); ?>
                <?php echo $form->error($model,'spe_ans'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'time_aux'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'time_aux',array('size'=>4,'maxlength'=>4)); ?>
                <?php echo $form->error($model,'time_aux'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'gra_vid'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'gra_vid',array('size'=>1,'maxlength'=>3,'id'=>'graVid')); ?>
                <?php echo $form->error($model,'gra_vid'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'rol_on'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'rol_on',array('size'=>1,'maxlength'=>1)); ?>
                <?php echo $form->error($model,'rol_on'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'exercises'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'exercises',array('size'=>25,'maxlength'=>25)); ?>
                <?php echo $form->error($model,'exercises'); ?>
            </td>
        </tr>

        <tr>
            <td>
		        <?php echo $form->labelEx($model,'eva_por'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'eva_por',array('size'=>4,'maxlength'=>4)); ?>
                <?php echo $form->error($model,'eva_por'); ?>
            </td>
        </tr>
</table>
<div class="row buttons" align="center">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    <?php
    if(AbaUserBackoffice::isValid(array('root')))
    {
        echo CHtml::button('Set all to 100%',array('onclick'=>'js:set100Percentage();'));
    }
    ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->