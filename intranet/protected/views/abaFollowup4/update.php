<?php
/* @var $this AbaFollowup4Controller */
/* @var $model AbaFollowup4 */

$this->breadcrumbs=array(
	'Units Followup'=>array('admin'),
    'Unit '.$model->themeid=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AbaFollowup4', 'url'=>array('index')),
	array('label'=>'Create AbaFollowup4', 'url'=>array('create')),
	array('label'=>'View AbaFollowup4', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AbaFollowup4', 'url'=>array('admin')),
);
?>

<h3>Update followup for unit <b><?php echo $model->themeid; ?></b> for user id <b><?php echo $model->userid; ?></b></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>