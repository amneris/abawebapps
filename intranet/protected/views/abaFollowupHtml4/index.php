<?php
/* @var $this AbaFollowupHtml4Controller */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Aba Followup Html4s',
);

$this->menu=array(
	array('label'=>'Create AbaFollowupHtml4', 'url'=>array('create')),
	array('label'=>'Manage AbaFollowupHtml4', 'url'=>array('admin')),
);
?>

<h3>Aba Followup Html4s</h3>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
