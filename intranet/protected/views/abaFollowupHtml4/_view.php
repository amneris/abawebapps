<?php
/* @var $this AbaFollowupHtml4Controller */
/* @var $data AbaFollowupHtml4 */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userid')); ?>:</b>
	<?php echo CHtml::encode($data->userid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('themeid')); ?>:</b>
	<?php echo CHtml::encode($data->themeid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('page')); ?>:</b>
	<?php echo CHtml::encode($data->page); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('section')); ?>:</b>
	<?php echo CHtml::encode($data->section); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('action')); ?>:</b>
	<?php echo CHtml::encode($data->action); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('audio')); ?>:</b>
	<?php echo CHtml::encode($data->audio); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('bien')); ?>:</b>
	<?php echo CHtml::encode($data->bien); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mal')); ?>:</b>
	<?php echo CHtml::encode($data->mal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastchange')); ?>:</b>
	<?php echo CHtml::encode($data->lastchange); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wtext')); ?>:</b>
	<?php echo CHtml::encode($data->wtext); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('difficulty')); ?>:</b>
	<?php echo CHtml::encode($data->difficulty); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('time_aux')); ?>:</b>
	<?php echo CHtml::encode($data->time_aux); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contador')); ?>:</b>
	<?php echo CHtml::encode($data->contador); ?>
	<br />

	*/ ?>

</div>