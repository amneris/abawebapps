<?php
/* @var $this AbaFollowupHtml4Controller */
/* @var $model AbaFollowupHtml4 */

$this->breadcrumbs=array(
	'Lines Exercises'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AbaFollowupHtml4', 'url'=>array('index')),
	array('label'=>'Create AbaFollowupHtml4', 'url'=>array('create')),
	array('label'=>'Update AbaFollowupHtml4', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AbaFollowupHtml4', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AbaFollowupHtml4', 'url'=>array('admin')),
);
?>

<h3>View lines exercises: <?php echo $model->id; ?></h3>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'userid',
		'themeid',
		'page',
		'section',
		'action',
		'audio',
		'bien',
		'mal',
		'lastchange',
		'wtext',
		'difficulty',
		'time_aux',
		'contador',
	),
)); ?>
