<?php
/* @var $this AbaFollowupHtml4Controller */
/* @var $model AbaFollowupHtml4 */

$this->breadcrumbs=array(
	'Aba Followup Html4s'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AbaFollowupHtml4', 'url'=>array('index')),
	array('label'=>'Manage AbaFollowupHtml4', 'url'=>array('admin')),
);
?>

<h3>Create AbaFollowupHtml4</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>