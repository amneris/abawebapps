<?php
/* @var $this AbaFollowupHtml4Controller */
/* @var $model AbaFollowupHtml4 */

$this->breadcrumbs=array(
	'Lines exercises'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AbaFollowupHtml4', 'url'=>array('index')),
	array('label'=>'Create AbaFollowupHtml4', 'url'=>array('create')),
	array('label'=>'View AbaFollowupHtml4', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AbaFollowupHtml4', 'url'=>array('admin')),
);
?>

<h3>Update lines exercises: <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>