<?php
/* @var $this AbaFollowupHtml4Controller */
/* @var $model AbaFollowupHtml4 */

$this->breadcrumbs=array(
	'Lines Exercises',
	
);

$this->menu=array(
	array('label'=>'List AbaFollowupHtml4', 'url'=>array('index')),
	array('label'=>'Create AbaFollowupHtml4', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('aba-followup-html4-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Lines exercises</h3>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'aba-followup-html4-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
                    'prevPageLabel'  => '<',
                    'nextPageLabel'  => '>',
                    'firstPageLabel' => '<<',
                    'lastPageLabel'  => '>>',),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'userid',
		'themeid',
		'page',
		'section',
		'action',
		/*
		'audio',
		'bien',
		'mal',
		'lastchange',
		'wtext',
		'difficulty',
		'time_aux',
		'contador',
		*/
		array(
			'class'=>'CButtonColumn',
            'header'=>'Options'
		),
	),
)); ?>
