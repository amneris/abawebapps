<?php
/* @var $this AbaFollowupHtml4Controller */
/* @var $model AbaFollowupHtml4 */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aba-followup-html4-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <table id="myForm">
        
        <tr>
            <td>
                <?php echo $form->labelEx($model,'userid'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'userid',array('size'=>10,'maxlength'=>10)); ?>
                <?php echo $form->error($model,'userid'); ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <?php echo $form->labelEx($model,'themeid'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'themeid',array('size'=>10,'maxlength'=>10)); ?>
                <?php echo $form->error($model,'themeid'); ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <?php echo $form->labelEx($model,'page'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'page',array('size'=>10,'maxlength'=>10)); ?>
                <?php echo $form->error($model,'page'); ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <?php echo $form->labelEx($model,'section'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'section',array('size'=>20,'maxlength'=>20)); ?>
                <?php echo $form->error($model,'section'); ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <?php echo $form->labelEx($model,'action'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'action',array('size'=>20,'maxlength'=>20)); ?>
                <?php echo $form->error($model,'action'); ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <?php echo $form->labelEx($model,'audio'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'audio',array('size'=>20,'maxlength'=>20)); ?>
                <?php echo $form->error($model,'audio'); ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <?php echo $form->labelEx($model,'bien'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'bien'); ?>
                <?php echo $form->error($model,'bien'); ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <?php echo $form->labelEx($model,'mal'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'mal',array('size'=>10,'maxlength'=>10)); ?>
                <?php echo $form->error($model,'mal'); ?>
            </td>
        </tr>
        
         <tr>
            <td>
                <?php echo $form->labelEx($model,'lastchange'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'lastchange'); ?>
                <?php echo $form->error($model,'lastchange'); ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <?php echo $form->labelEx($model,'wtext'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'wtext',array('size'=>60,'maxlength'=>100)); ?>
                <?php echo $form->error($model,'wtext'); ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <?php echo $form->labelEx($model,'difficulty'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'difficulty',array('size'=>10,'maxlength'=>10)); ?>
                <?php echo $form->error($model,'difficulty'); ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <?php echo $form->labelEx($model,'time_aux'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'time_aux',array('size'=>4,'maxlength'=>4)); ?>
                <?php echo $form->error($model,'time_aux'); ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <?php echo $form->labelEx($model,'contador'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'contador',array('size'=>10,'maxlength'=>10)); ?>
                <?php echo $form->error($model,'contador'); ?>
            </td>
        </tr>
        
    </table>

	<div class="row buttons" align="center">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->