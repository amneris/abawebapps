<?php
/*
 * @var Array $aCountriesPremium
 */

$this->breadcrumbs=array(
	'Users',
);

//Yii::app()->clientScript->registerScript('googlemaps', "https://www.google.com/jsapi");
?>
<h3>Premium users in the World Map</h3>

<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<script type='text/javascript'>
    google.load('visualization', '1', {'packages': ['geochart']});
    google.setOnLoadCallback(drawRegionsMap);

    function drawRegionsMap() {
        var data = google.visualization.arrayToDataTable(<?php echo json_encode($aCountriesPremium); ?>);

        var options = {enable: true, zoomFactor: 15, keepAspectRatio: false};

        var chart = new google.visualization.GeoChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    };
</script>

<div id="chart_div" style="width: 100%; height: 700px;"></div>

