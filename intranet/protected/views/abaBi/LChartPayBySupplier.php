<?php
/*
 * @var Array $aPaymentsBySupplier, array('Payment Date', 'Paypal', 'Caixa', 'AllPago-Br', 'AllPago-Mx', 'AllPago-Boleto', 'Oxo', 'AppStore', 'Adyen', 'Adyen-Hpp', 'Android-PlayStore', 'Zuora');
 * @var Payments $moPayments
 * @var string $lChartSinceDate
 * @var integer $lChartIdCountry
 */
$this->breadcrumbs = array(
  'Users',
);
?>

<h3>Evolution of payments by payment supplier</h3>

<?php
/* @var CActiveForm $form */
$form = $this->beginWidget('CActiveForm', array(
  'action' => Yii::app()->createUrl($this->route),
  'method' => 'get',
)); ?>
<table id="myForm">

    <tr>
        <td width="250px">
            Since date:
        </td>
        <td>
            <?php
            echo CHtml::textField("lChartSinceDate", $lChartSinceDate, array('style' => 'width:70px;'));
            echo CHtml::image("images/calendar.jpg", "calendar", array("id" => "c_button", "class" => "pointer"));
            $this->widget('application.extensions.calendar.SCalendar',
              array(
                'inputField' => 'lChartSinceDate',
                'button' => 'c_button',
                'ifFormat' => '%Y-%m-%d',
              ));
            ?>
        </td>
    </tr>
    <tr>
        <td width="250px">
            Filter by country:
        </td>
        <td>
            <?php echo cHtml::dropDownList("lChartIdCountry", $lChartIdCountry, HeList::getCountryList(),
              array('empty' => '--Any Country--')); ?>
        </td>
    </tr>
</table>
<div class="row buttons" align="center">
    <?php echo CHtml::submitButton('Search'); ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages: ["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
        try {
            var data = google.visualization.arrayToDataTable(<?php echo json_encode($aPaymentsBySupplier); ?>);

            var options = {
                title: 'Payments by suppliers',
                colors: ['yellow', 'blue', 'green', 'red', 'purple', 'coral', 'lime', 'maroon', 'brown', 'orange', 'cyan']
            };

            var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
            chart.draw(data, options);
        }
        catch (err) {
            alert(err.message);
        }
    }
</script>
<!--silver, orange-->

<div id="chart_div" style="width: 100%; height: 800px;"></div>
