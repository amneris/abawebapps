
<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
//        'id'=>'LogWebService',
//        'enableAjaxValidation'=>false,
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
    )); ?>

    <div class="row">
        <?php echo $form->label($model,'startDate');
        echo $form->textField($model,'startDate');
        echo CHtml::image("images/calendar.jpg","calendar1", array("id"=>"c_startDate","class"=>"pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
            array(
                'inputField'=>'LogWebService_startDate',
                'button'=>'c_startDate',
                'ifFormat'=>'%Y-%m-%d 00:00:00',
            ));
//         'ifFormat'=>'%Y-%m-%d %H:%M',
        ?>
        <?php echo $form->error($model,'dateStart'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'endDate');
        echo $form->textField($model,'endDate');
        echo CHtml::image("images/calendar.jpg","calendar2", array("id"=>"c_endDate","class"=>"pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
            array(
                'inputField'=>'LogWebService_endDate',
                'button'=>'c_endDate',
                'ifFormat'=>'%Y-%m-%d 23:59:59',
            ));
        ?>
        <?php echo $form->error($model,'endDate'); ?>
    </div>

    <div class="row">
        <?php
        echo $form->label($model,'successResponse');
        echo $form->DropDownList($model,'successResponse',HeList::booleanList(), array('empty'=>'--All--')); // Any
        // getSuccess
        ?>
        <?php echo $form->error($model,'successResponse'); ?>
    </div>
    <div class="row">
        <?php
        echo $form->label($model,'nameService');
        echo $form->DropDownList($model,'nameService', CHtml::listData(LogWebService::getWsNames(), 'id', 'title'), array('empty'=>'--Ws name--')); // Any
        ?>
        <?php echo $form->error($model,'nameService'); ?>
    </div>
    <div class="row">
        <?php
        echo $form->label($model,'errorCode');
        echo $form->DropDownList($model,'errorCode', CHtml::listData(LogWebService::getWsErrorCodes(), 'id', 'title'), array('empty'=>'--Ws errors codes--')); // Any
        ?>
        <?php echo $form->error($model,'errorCode'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->
