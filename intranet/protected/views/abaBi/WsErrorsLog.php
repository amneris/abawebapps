<?php

$this->breadcrumbs=array(
    'Webservices log',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('aba-ws-logs-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Webservices errors log</h3>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>

<div class="search-form" style="display: none;">
    <?php $this->renderPartial('_search',array(
        'model' =>  $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('application.components.widgets.CGridViewABA', array(
    'id'=>'aba-ws-logs-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager'=>array(
        'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
        'prevPageLabel'  => '<',
        'nextPageLabel'  => '>',
        'firstPageLabel' => '<<',
        'lastPageLabel'  => '>>',
    ),
    'extraInfo' => array('view','update','delete'),
    'dataProvider' => $model->search(),
    'filter' => $model,
    'filterSelector'=>'{filter}',
    'columns'=>array(
        'nameService' => array(
            'name' => 'nameService',
            'header' => "Service name",
            'value' => 'LogWebService::getWsName($data->nameService)',
            'filter' => CHtml::listData(LogWebService::getWsNames(), 'id', 'title'),
        ),
        'errorCode' => array(
            'name' => 'errorCode',
            'header' => "Error",
            'value'=> 'LogWebService::getWsErrorCode($data->errorCode)',
            'filter' => CHtml::listData(LogWebService::getWsErrorCodes(), 'id', 'title'),
        ),
        'successResponse' => array
        (
            'name' => 'successResponse',
            'header' => "Success",
            'type' => 'booleanList',
            'filter' => HeList::booleanList()
        ),
        'nSuccess' => array
        (
            'name' => 'nSuccess',
            'header' => "Num. success",
            'value'=> '$data->nSuccess',
        ),

        'startDate' => array
        (
            'name' => 'startDate',
            'header' => "First success",
            'value'=> '$data->startDate',
        ),
        'endDate' => array
        (
            'name' => 'endDate',
            'header' => "Last success",
            'value'=> '$data->endDate',
        ),
    ),
)); ?>
