<?php
/*
 * @var Array $aPaymentsAttempts, array('Attempt Date',  'PKI 1', 'KPI 2');
  * @var string $aChartSinceDate
 * @var string $aChartIdCountry
 * @var string $aChartPaySupExtId
 */
$this->breadcrumbs=array(
	'Users',
);

?>
<h3>Payment successful percentage based on attempts</h3>

<?php
/* @var CActiveForm $form*/
$form=$this->beginWidget('CActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>
<table id="myForm">

    <tr>
        <td width="250px">
            Since date:
        </td>
        <td>
            <?php
            echo CHtml::textField("aChartSinceDate",$aChartSinceDate,array('style'=>'width:70px;'));
            echo CHtml::image("images/calendar.jpg","calendar", array("id"=>"c_button","class"=>"pointer"));
            $this->widget('application.extensions.calendar.SCalendar',
                array(
                    'inputField'=>'aChartSinceDate',
                    'button'=>'c_button',
                    'ifFormat'=>'%Y-%m-%d',
                ));
            ?>
        </td>
    </tr>
    <tr>
        <td width="250px">
            Filter by country:
        </td>
        <td>
            <?php echo cHtml::dropDownList("aChartIdCountry", $aChartIdCountry, HeList::getCountryList(),array('empty'=>'--Any Country--') ); ?>
        </td>
    </tr>
    <tr>
        <td width="250px">
            Filter by method of payment:
        </td>
        <td>
            <?php echo cHtml::dropDownList("aChartPaySupExtId", $aChartPaySupExtId, HeList::supplierExtGatewayList(),array('empty'=>'--ALL--') ); ?>
        </td>
    </tr>
</table>
<div class="row buttons" align="center">
    <?php echo CHtml::submitButton('Apply filter'); ?>
</div>

<?php $this->endWidget(); ?>


<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable(<?php echo json_encode($aPaymentsAttempts); ?>);
        var options = {
            title: 'Successful attempts percentage',
            hAxis: {title: 'Date by days',  titleTextStyle: {color: '#333'}},
            vAxis: {minValue: 0, title: 'Percentage 100%',  titleTextStyle: {color: '#333'}},
            colors:['#0040FF','#086A87'],
            areaOpacity:[0.5]
        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }
</script>

<div id="chart_div" style="width: 100%; height: 800px;"></div>
