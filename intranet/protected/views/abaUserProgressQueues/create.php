<?php
/* @var $this AbaUserProgressQueuesController */
/* @var $model AbaUserProgressQueues */
/* @var $modelDetails AbaUserProgressQueuesDetails */

$this->breadcrumbs = array(
  'User progress problems queue' => array('admin'),
  'Create',
);

$this->menu = array();
?>

    <h3>Fill progress of: <?php echo " &nbsp;&nbsp; " . $user->email . " &nbsp;&nbsp; " . $user->id; ?></h3>

<?php echo $this->renderPartial('_form', array('model' => $model, 'user' => $user)); ?>

<?php $this->widget(
  'application.components.widgets.CGridViewABA',
  array(
    'id' => 'aba-userprogressqueues-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'emptyText' => '<b>No results found. Type in any text in any column to execute a search.</b>',
    'pager' => array(
      'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
      'prevPageLabel' => '<',
      'nextPageLabel' => '>',
      'firstPageLabel' => '<<',
      'lastPageLabel' => '>>',
    ),
    'dataProvider' => $modelDetails->searchQueueDetails(1, array('userId' => $user->id)),
    'columns' => array(
      'unitId' => array(
        'header' => 'Unit number',
        'name' => 'unitId',
      ),
      'status' => array(
        'name' => 'status',
        'header' => 'Status',
        'type' => 'userProgressQueuesDetailsListFormat',
      ),
      'dateStart' => array(
        'header' => 'Date Start',
        'name' => 'dateStart',
      ),
      'dateEnd' => array(
        'header' => 'Date End',
        'name' => 'dateEnd',
      ),
      'dateAdd' => array(
        'header' => 'Created',
        'name' => 'dateAdd',
      ),
    ),
  )
);




