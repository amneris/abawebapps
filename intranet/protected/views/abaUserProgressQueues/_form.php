<?php
/* @var $this AbaUserProgressQueuesController */
/* @var $model AbaUserProgressQueues */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget(
      'CActiveForm',
      array(
        'id' => 'aba-userprogressqueues-form',
        'enableAjaxValidation' => false,
      )
    );

    $cssPrefix = (isset($wAction) AND $wAction == 'update' ? 'Cm' : '');

    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <table id="myForm" style="background-color: #ebebeb;">
        <?php if (is_numeric($model->queueId)): ?>
            <tr>
                <td>
                    <?php echo $form->labelEx($model, 'queueId'); ?>
                </td>
                <td>
                    <?php echo $model->queueId; ?>
                    <?php echo $form->error($model, 'queueId'); ?>
                </td>
            </tr>
        <?php endif; ?>
        <tr>
            <td style="vertical-align: top; padding-top: 9px; font-size: 1.4em; ">

            </td>
            <td style="vertical-align: top;">

                <div style="clear: both;">
                    <?php $checked = true; ?>
                    <?php if (is_numeric($model->queueId)): ?>
                        <?php $checked = ($model->evaluation == 1 ? true : false); ?>
                        <?php echo $form->hiddenField($model, 'userId'); ?>
                    <?php endif; ?>
                    <?php
                    echo '&nbsp;'.CHtml::checkBox( "AbaUserProgressQueues[evaluation]", $checked, array('style'=>'width: 15px; height: 15px;') );
                    ?> Fill evaluation - force <span style="font-weight: bold;">100%</span> of progress
                </div>

            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; padding-top: 9px; font-size: 1.4em; ">
                <?php echo $form->labelEx($model, 'units'); ?>
            </td>
            <td style="vertical-align: top;">
                <?php echo $form->textField($model, 'units',
                  array(
                    'size' => 50,
                    'maxlength' => 1000,
                    "style" => "height: 25px; border-radius: 10px; padding: 3px; border: 2px solid #C00;"
                  )); ?>

                <a target="_blank" href="javascript: $('#aba-userprogressqueues-form').submit();" class="btnCancel">
                    <?php if ($model->isNewRecord): ?>
                        ADD PROGRESS
                    <?php else: ?>
                        SAVE PROGRESS
                    <?php endif; ?>
                </a>
                <?php echo $form->error($model, 'units'); ?>
                <p class="cancel">* Add the units number followed by commas</p>
            </td>
        </tr>
        <?php if (is_numeric($model->queueId)): ?>
            <tr>
                <td>
                    <?php echo $form->labelEx($model, 'dateAdd'); ?>
                </td>
                <td>
                    <?php echo $model->dateAdd; ?>
                    <?php echo $form->error($model, 'dateAdd'); ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo $form->labelEx($model, 'dateStart'); ?>
                </td>
                <td>
                    <?php echo $model->dateStart; ?>
                    <?php echo $form->error($model, 'dateStart'); ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo $form->labelEx($model, 'dateEnd'); ?>
                </td>
                <td>
                    <?php echo $model->dateEnd; ?>
                    <?php echo $form->error($model, 'dateEnd'); ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo $form->labelEx($model, 'status'); ?>
                </td>
                <td>
                    <?php $sUserProgressStatus = HeList::userProgressQueuesList(); ?>
                    <?php echo(isset($sUserProgressStatus[$model->status]) ? $sUserProgressStatus[$model->status] : ''); ?>
                </td>
            </tr>
        <?php endif; ?>
    </table>

    <div class="row buttons" align="center">

        <?php if (is_numeric($model->queueId)): ?>
            <?php echo $form->hiddenField($model, 'userId'); ?>
        <?php else: ?>
            <?php echo CHtml::hiddenField( "AbaUserProgressQueues[userId]", $user->id, array("id" => "AbaUserProgressQueues_userId")); ?>
        <?php endif; ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->