<?php
/* @var $this AbaUserProgressQueuesController */
/* @var $model AbaUserProgressQueues */
/* @var $form CActiveForm */
?>

<?php

$bIsOs = true;

switch($model->status) {
    case AbaUserProgressQueues::QUEUE_STATUS_PENDING:
    case AbaUserProgressQueues::QUEUE_STATUS_INPROCESS:
        $bIsOs = false;
        break;
}
?>
<div class="form">

    <?php $form = $this->beginWidget(
      'CActiveForm',
      array(
        'id' => 'aba-userprogressqueues-form',
        'enableAjaxValidation' => false,
        'action' => Yii::app()->createUrl('AbaUserProgressQueues/create&userId=' . $user->id),
      )
    );
    $cssPrefix = (isset($wAction) AND $wAction == 'update' ? 'Cm' : '');
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <table id="myForm" style="background-color: #ebebeb;">

        <tr>
            <td style="vertical-align: top; padding-top: 9px; font-size: 1.4em; ">

            </td>
            <td style="vertical-align: top;">

                <div style="clear: both;">
                    <?php $checked = false; ?>
                    <?php if (is_numeric($model->queueId)): ?>
                      <?php $checked = ($model->evaluation == 1 ? true : false); ?>
                        <?php echo $form->hiddenField($model, 'userId'); ?>
                    <?php endif; ?>
                    <?php
                    echo '&nbsp;'.CHtml::checkBox( "AbaUserProgressQueues[evaluation]", $checked, array('style'=>'width: 15px; height: 15px;', 'disabled' => 'disabled') );
                    ?> Fill evaluation - force 100% of progress
                </div>

            </td>
        </tr>

        <tr>
            <td style="vertical-align: top; padding-top: 9px; font-size: 1.4em; ">
                <?php echo $form->labelEx($model, 'units'); ?>
            </td>
            <td style="vertical-align: top;">

<?php if($bIsOs): ?>
    <?php echo $form->textField($model, 'units',
      array(
        'size' => 50,
        'maxlength' => 1000,
        "style" => "height: 25px; border-radius: 10px; padding: 3px; border: 2px solid #c00;",
        "disabled" => "disabled", "class" => "readOnly"
      ));
    ?>
<?php else: ?>
    <?php echo $form->textField($model, 'units',
      array(
        'size' => 50,
        'maxlength' => 1000,
        "style" => "height: 25px; border-radius: 10px; padding: 3px; border: 2px solid #aaa; color: #aaa",
        "disabled" => "disabled", "class" => "readOnly"
      ));
    ?>
<?php endif; ?>
                <?php echo $form->error($model, 'units'); ?>

                <a target="_blank" href="javascript: location.href=location.href;" class="btnCancel" style="background-color:#FE642E;">
                    REFRESH
                </a>
<?php
    switch($model->status) {
        case AbaUserProgressQueues::QUEUE_STATUS_PENDING:
?>
                    <p class="cancel">* Pending to be processed</p>
<?php
            break;
        case AbaUserProgressQueues::QUEUE_STATUS_INPROCESS:
?>
                    <p class="cancel">* Wait while data just processed</p>
<?php
            break;
        case AbaUserProgressQueues::QUEUE_STATUS_FAIL:
?>
                    <p class="cancel">* Process failed</p>
<?php
            break;
        case AbaUserProgressQueues::QUEUE_STATUS_PROCESSED:
?>
                    <p class="cancel">* Processed</p>
<?php
            break;
        case AbaUserProgressQueues::QUEUE_STATUS_DELETED:
?>
                    <p class="cancel">* Deleted</p>
<?php
            break;
    }
?>
            </td>
        </tr>
    </table>

    <div class="row buttons" align="center">

        <?php if (is_numeric($model->queueId)): ?>
            <?php echo $form->hiddenField($model, 'userId'); ?>
        <?php else: ?>
            <?php echo CHtml::hiddenField( "AbaUserProgressQueues[userId]", $user->id, array("id" => "AbaUserProgressQueues_userId")); ?>
        <?php endif; ?>

    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->