<?php
/* @var $this AbaUserProgressQueuesController */
/* @var $model AbaUserProgressQueues */

$this->breadcrumbs = array(
  'Users progress problems queues',
);

$this->menu = array();

Yii::app()->clientScript->registerScript(
  'search',
  "
   $('.search-button').click(function(){
     $('.search-form').toggle();
     return false;
   });
   $('.search-form form').submit(function(){
     $.fn.yiiGridView.update('aba-userprogressqueues-grid', {
       data: $(this).serialize()
     });
     return false;
   });
   "
);
?>

<h3>Users progress problems queues</h3>

<br>
<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial(
      '_search',
      array(
        'model' => $model,
      )
    ); ?>
</div><!-- search-form -->

<?php $this->widget(
  'application.components.widgets.CGridViewABA',
  array(
    'id' => 'aba-userprogressqueues-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager' => array(
      'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
      'prevPageLabel' => '<',
      'nextPageLabel' => '>',
      'firstPageLabel' => '<<',
      'lastPageLabel' => '>>',
    ),
    'extraInfo' => array('view'),
    'dataProvider' => $model->search(),
    'filter' => $model,
    'filterSelector' => '{filter}',
    'columns' => array(
      'queueId',
      'userId',
      'units',
      'evaluation' =>
        array(
          'name' => 'evaluation',
          'type' => 'YesNoBoolList',
          'filter' => HeList::getYesNoAliasBool(),
          'htmlOptions' => array('width' => "120px")
        ),
      'dateAdd',
      'dateStart',
      'dateEnd',
      'status' => array(
        'name' => 'status',
        'type' => 'userProgressQueuesListFormat',
        'filter' => HeList::userProgressQueuesList()
      ),
      array(
        'class' => 'CLinkColumn',
        'label' => 'Open',
        'urlExpression' => 'HeList::getExternalUrl($data)',
        'header' => 'Autologin',
        'linkHtmlOptions' => array('target' => '_blank', 'class' => 'queueprogressstatuscontainer'),
      ),
      array(
        'class' => 'CButtonColumnABA',
        'header' => 'Options',
      ),
    ),
  )
); ?>
