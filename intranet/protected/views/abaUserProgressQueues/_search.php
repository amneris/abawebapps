<?php
/* @var $this AbaUserProgressQueuesController */
/* @var $model AbaUserProgressQueues */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'queueId'); ?>
        <?php echo $form->textField($model, 'queueId', array('size' => 25, 'maxlength' => 20)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'userId'); ?>
        <?php echo $form->textField($model, 'userId', array('size' => 25, 'userId' => 20)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'units'); ?>
        <?php echo $form->textField($model, 'units', array('size' => 25, 'maxlength' => 1000)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'dateAdd');
        echo $form->textField($model,'dateAdd');
        echo CHtml::image("images/calendar.jpg","calendar1", array("id"=>"c_buttonDateAdd","class"=>"pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
          array(
            'inputField'=>'AbaUserProgressQueues_dateAdd',
            'button'=>'c_buttonDateAdd',
            'ifFormat'=>'%Y-%m-%d 00:00:00',
          ));
        ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'dateStart');
        echo $form->textField($model,'dateStart');
        echo CHtml::image("images/calendar.jpg","calendar2", array("id"=>"c_buttonDateStart","class"=>"pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
          array(
            'inputField'=>'AbaUserProgressQueues_dateStart',
            'button'=>'c_buttonDateStart',
            'ifFormat'=>'%Y-%m-%d 00:00:00',
          ));
        ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'dateEnd');
        echo $form->textField($model,'dateEnd');
        echo CHtml::image("images/calendar.jpg","calendar3", array("id"=>"c_buttonDateEnd","class"=>"pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
          array(
            'inputField'=>'AbaUserProgressQueues_dateEnd',
            'button'=>'c_buttonDateEnd',
            'ifFormat'=>'%Y-%m-%d 00:00:00',
          ));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'status'); ?>
        <?php echo $form->DropDownList($model, 'status', HeList::userProgressQueuesList(), array('empty' => '--')); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->