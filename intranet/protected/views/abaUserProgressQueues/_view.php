<?php
/* @var $this AbaUserProgressQueuesController */
/* @var $model AbaUserProgressQueues */
?>

<div class="view">

    <b/><?php echo CHtml::encode($data->getAttributeLabel('queueId')); ?>:</b>
    <?php echo CHtml::encode($data->queueId); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('userId')); ?>:</b>
    <?php echo CHtml::encode($data->userId); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('units')); ?>:</b>
    <?php echo CHtml::encode($data->units); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('dateAdd')); ?>:</b>
    <?php echo CHtml::encode($data->dateAdd); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('dateStart')); ?>:</b>
    <?php echo CHtml::encode($data->dateStart); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('dateEnd')); ?>:</b>
    <?php echo CHtml::encode($data->dateEnd); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
    <?php echo CHtml::encode($data->status); ?>
    <br/>

</div>