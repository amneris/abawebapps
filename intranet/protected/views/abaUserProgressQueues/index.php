<?php
/* @var $this AbaUserProgressQueuesController */
/* @var $model AbaUserProgressQueues */

$this->breadcrumbs = array(
    'Users progress problems queues',
);

$this->menu = array();
?>

<h3>User progress queue</h3>

<?php $this->widget(
    'zii.widgets.CListView',
    array(
        'dataProvider' => $dataProvider,
        'itemView' => '_view',
    )
); ?>
