<?php
/* @var $this AbaUserProgressQueuesController */
/* @var $model AbaUserProgressQueues */

$this->breadcrumbs = array(
  'Users progress problems queues' => array('admin'),
  $model->queueId => array('view', 'id' => $model->queueId),
  'Update',
);

$this->menu = array();
?>

    <h3>Edit progress of: <?php echo " &nbsp;&nbsp; " . $user->email . " &nbsp;&nbsp; " . $user->id; ?></h3>

<?php echo $this->renderPartial('_form', array('model' => $model, 'user' => $user)); ?>