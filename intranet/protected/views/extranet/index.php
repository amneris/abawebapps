<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name;

$this->breadcrumbs=array(
  'Extranet',
  'User',
);

Yii::app()->clientScript->registerScript('search', "
    function reinstallButtons() {
      $('select[name=\"commonNamespace_models_intranet_ExtranetUserListForm[isdeleted]\"]').change( function() {
        return refreshList();
      });
    }
    reinstallButtons();
");

?>

  <style>
    #newPublicationButton {
      -webkit-border-radius: 2em;
      -moz-border-radius: 2em;
      border-radius: 2em;

      border: 1px solid #9AAFE5;
      font-weight: bold;
      padding: 0.2em 1em;
      text-decoration: none;

      background: none repeat scroll 0% 0% #0297D1;
      color: #FFF;
      margin: 0.5em;

      cursor: pointer;

      width: auto;
      float: right;
    }
  </style>

<!-- FancyBox -->
<link rel="stylesheet" href="/js/fancyapps-fancyBox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="/js/fancyapps-fancyBox/jquery.fancybox.pack.js?v=2.1.5"></script>

<script src="/js/aba/core.js"></script>

<?php $this->widget('application.components.widgets.CGridViewABA', array(
  'id'=>'aba-user-grid',
  'extraInfo'=>array('update', 'view'),
  'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
  'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
    'prevPageLabel'  => '<',
    'nextPageLabel'  => '>',
    'firstPageLabel' => '<<',
    'lastPageLabel'  => '>>',),
  'extraButtons'=>array(

  ), //end ExtraButtons
  'ajaxType' => 'post',
  'afterAjaxUpdate' => 'reinstallButtons',

  'dataProvider'=>$dataProvider,
  'filter'=>$filtersForm,
  'filterSelector'=>'{filter}',
  'columns'=>array
  (
    array(
      'name' => 'name',
      'header' => Yii::t( 'app', 'Name' ),
      'value' => '$data["name"]',
    ),

    array(
      'name' => 'surname',
      'header' => Yii::t( 'app', 'Surname' ),
      'value' => '$data["surname"]',
    ),

    array(
      'name' => 'email',
      'header' => Yii::t( 'app', 'Email' ),
      'value' => '$data["email"]',
    ),

    array(
      'name' => 'idEnterprise',
      'header' => Yii::t( 'app', 'Enterprise' ),
      'value' => 'Enterprise::model()->findByPk( $data["idEnterprise"] )->name',
    ),

    array(
      'name' => 'isdeleted',
      'header' => Yii::t( 'app', 'isdeleted' ),
      'value' => '($data["isdeleted"] == 1)? Yii::t(\'app\', \'Deleted\')." (".date("Y-m-d", strtotime($data["deleted"])).")" : Yii::t(\'app\', \'Alive\')',
      'filter' => array( 1 => Yii::t('app', 'Deleted'), '0' => Yii::t('app', 'Alive') ),
    ),

    array
    (
      'header'=>'Options <div class="filters">',
      'class'=>'CButtonColumn',
      'template'=> '{unregister}', //'{edit}',

      'buttons'=>array
      (
        'unregister' => array
        (
          'label'=> Yii::t( 'app', 'Unregister' ),
          'imageUrl'=>Yii::app()->baseUrl.'/images/unregister_24.png',
//          'url'=>'"#".Yii::app()->createUrl("/extranet/", array("id"=>$data["id"]))',
          'url'=>'"#id=".$data["id"]',
          'click'=>'function(){ Unregister( this ); return false; }',
          'visible' => '( $data["idStudentCampus"] > 0 )',
        ),
//        'delete' => array
//        (
//          'label'=>'[-]',
//          'url'=>'"#"',
//          'visible'=>'$data["email"] == ""',
//          'click'=>'function(){alert("Going down!"); }',
//        ),
      ),

    )

  ),
));

?>

<script>

  function refreshList() {
    var id='aba-user-grid';
    var inputSelector='#'+id+' .filters input, '+'#'+id+' .filters select';
    var data=$.param($(inputSelector));
    $.fn.yiiGridView.update(id, {data: data});
    return false;
  }

  function Unregister( obj ) {
    if ( !confirm(<?php echo CJavaScript::encode( Yii::t('app', 'Esta seguro de que desea eliminar las licencias de B2B activadas para este usuario ?') ); ?>) ) return;

    var url = <?php echo CJavaScript::encode( Yii::app()->createUrl("/extranet/unregister") ); ?>;
    var parameters = {
      idStudent: abaCore.getParameterByName('id', obj.href.replace('#', '&') ),
    };

    var successF = function( data ) {
      if (data.ok ) {
        console.log(data.okMsg);
        refreshList();
      } else {
        console.log(data.errorMsg);
      }
    }

    abaCore.launchAjax( url, parameters, successF );
  }

</script>