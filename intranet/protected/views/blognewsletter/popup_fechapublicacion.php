<?php
/* @var $this SiteController */

?>
<div style="border: 1px solid #0297D1; width: 100%; height: 100%; color: #000; padding: 2px;">
  <label for="publicationDatePicker"><?php echo Yii::t('app', 'Seleccionar fecha publicación:'); ?></label>
  <div style="text-align: center;">
  <?php
    $startDate = date("Y-m-d");

    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
      'id' => 'publicationDatePicker',
      'name' => 'startDate',
      'value' => $startDate,
      'language'=> Yii::app()->language,
      'options' => array(
        'dateFormat' => 'yy-mm-dd',
        'changeMonth'=>true,
        'changeYear'=>true,
        'minDate' => $startDate,
      ),
      'htmlOptions' => array(
        'size' => '10',
        'maxlength' => '10',
        'style' => 'readonly'
      ),
    ));

  ?>
  </div>

  <div id="divButtonZone">
    <div id="cancelButton" class="btn"><?php echo Yii::t('app', 'Cancel'); ?></div>
    <div id="okButton" class="btn"><?php echo Yii::t('app', 'Send'); ?></div>
  </div>
</div>
<script>
  $(document).ready( function() {
    $( "#publicationDatePicker" ).datepicker();
    $( "#publicationDatePicker" ).datepicker('option', 'dateFormat', 'yy-mm-dd');
    $( "#publicationDatePicker" ).datepicker('option', 'minDate', '<?php echo $startDate; ?>');
    $( "#publicationDatePicker" ).change( function() {
      var url = '<?php echo Yii::app()->createUrl('blognewsletter/verifyDate');?>';

      var parameters = {
        date: $(this).val()
      };

      var successF = function(data) {
        if ( data.ok ) {

        } else {
          alert(data.errorMsg);
          $('#publicationDatePicker').val('');
        }
      };

      var errorF = function(data) {
        console.log('error');
      };

      abaCore.launchAjax( url, parameters, successF, errorF );
    });

    $('#cancelButton').click( function() {
      $('.fancybox-close').trigger('click');
    });

    $('#okButton').click( function() {
      if ( $('#publicationDatePicker').val() == '' ) return;

      var url = '<?php echo Yii::app()->createUrl('blognewsletter/publish');?>';

      var parameters = {
        date: $('#publicationDatePicker').val()
      };

      var successF = function(data) {
        if ( data.ok ) {
          refreshList();
          $('.fancybox-close').trigger('click');
        } else {
          alert(data.errorMsg);
        }
      };

      var errorF = function(data) {
        console.log('error');
      };

      abaCore.launchAjax( url, parameters, successF, errorF );
    });

  });
</script>