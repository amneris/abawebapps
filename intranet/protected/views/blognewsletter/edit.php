<?php
/* @var $this SiteController */
$this->pageTitle = Yii::app()->name;

$this->breadcrumbs = array(
  'Newsletter' => '/index.php?r=blognewsletter/index',
  'Edit',
);

?>

<!-- https://github.com/stephband/jquery.event.move -->
<script src="/js/jquery.event.move.js"></script>
<!-- https://github.com/stephband/jquery.event.swipe -->
<script src="/js/jquery.event.swipe.js"></script>
<!-- http://bxslider.com/ -->
<script src="/js/bxslider/jquery.bxslider.min.js"></script>
<link href="/js/bxslider/jquery.bxslider.css" rel="stylesheet"/>

<script src="/js/jquery.elastic.source.js"></script>

<!-- FancyBox -->
<link rel="stylesheet" href="/js/fancyapps-fancyBox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen"/>
<script type="text/javascript" src="/js/fancyapps-fancyBox/jquery.fancybox.pack.js?v=2.1.5"></script>

<script src="/js/aba/core.js"></script>
<link href="/css/sections/blognewsletter/edit.css" rel="stylesheet" media="screen">

<script>
    var arPost = {
        '0': '0',
        <?php $contador = 1; ?>
        <?php foreach( $mlPost as $mPost ) { ?>
        '<?php echo $contador++; ?>': <?php echo $mPost->id ?>,
        <?php } ?>
    };
</script>

<div class="sectionBody">

    <div class="sectionMainBody">

        <div>
            <label for="inputSubject"><?php echo Yii::t('app', 'Subject:'); ?></label>
            <?php $textSubject = ($mPublish->textSubject) ? $mPublish->textSubject : 'Lorem ipsum dolor sit amet'; ?>
            <input class="inputText inputTextHeader" type="text" id="inputSubject" value="<?php echo $textSubject; ?>"
                   maxlength="60">
        </div>

        <div id="divImgCapcalera">
            <div><img src="/images/tmp/capcalera.png"></div>
        </div>

        <div id="divTextareaHeader">
            <h4><?php echo Yii::t('app', 'Hello !'); ?></h4>
            <?php $textHeader = ($mPublish->textHeader) ? $mPublish->textHeader : 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,'; ?>
            <textarea id="textareaHeader" class="inputTextarea" maxlength="1000"><?php echo $textHeader; ?></textarea>
        </div>

        <div class="divTweetBackground">
            <?php $textTweet = ($mPublish->textTweet) ? $mPublish->textTweet : 'Lorem ipsum dolor sit amet'; ?>
            <input class="inputText inputTextTweet" type="text" id="inputTweet" value="<?php echo $textTweet; ?>"
                   maxlength="80">
        </div>

        <div>
            <label for="inputLinkTweet"><?php echo Yii::t('app', 'Link Tweet:'); ?></label>
            <?php $linkTweet = ($mPublish->linkTweet) ? $mPublish->linkTweet : 'Lorem ipsum dolor sit amet:  http://goo.gl/Ov3oZ3 #grammar'; ?>
            <input class="inputText" type="text" id="inputLinkTweet" value="<?php echo $linkTweet; ?>" maxlength="60">
        </div>

        <div class="sliderZone">

            <?php for ($contadorSlider = 1; $contadorSlider < 6; $contadorSlider++) { ?>
                <?php
                $indexIdPost = 'idPost' . $contadorSlider;
                $indexDescPost = 'descriptionPost' . $contadorSlider;
                $indexLinkPost = 'linkPost' . $contadorSlider;
                $$indexIdPost = 0;
                ?>
                <textarea style="display:none;"
                          id="idPost_<?php echo $contadorSlider; ?>"><?php echo $mPublish->$indexIdPost; ?></textarea>
                <textarea style="display:none;"
                          id="descPost_<?php echo $contadorSlider; ?>"><?php echo $mPublish->$indexDescPost; ?></textarea>
                <textarea style="display:none;"
                          id="linkPost_<?php echo $contadorSlider; ?>"><?php echo $mPublish->$indexLinkPost; ?></textarea>
                <div>
                    <ul id="bxslider<?php echo $contadorSlider; ?>" class="bxslider">
                        <li>
                            <p><?php echo Yii::t('app', 'Nothing to send'); ?></p>
                        </li>

                        <?php $contadorPost = 1; ?>
                        <?php foreach ($mlPost as $mPost) { ?>
                            <?php if ($mPublish->$indexIdPost == $mPost->id) {
                                $$indexIdPost = $contadorPost;
                                $descPost = $mPublish->$indexDescPost;
                                $linkPost = $mPublish->$indexLinkPost;
                            } else {
                                $descPost = $mPost->description;
                                $linkPost = Yii::t('app', 'read more') . ' >';
                            }
                            ?>
                            <li>
                                <div class="slide-body" data-group="slide">
                                    <div class="post">
                                        <img src="<?php echo utf8_decode($mPost->urlImage); ?>" class="imgPost">
                                        <h4><?php echo $mPost->title; ?></h4>

                                        <p
                                          id="pDescriptionPost_<?php echo $contadorSlider; ?>_<?php echo $mPost->id; ?>"
                                          class="pDescriptionPost"
                                          data-idSlider="<?php echo $contadorSlider; ?>"><?php echo $descPost; ?></p>

                                        <p>
                                            <input class="inputText inputTextLink"
                                                   data-numSlider="<?php echo $contadorSlider; ?>" type="text"
                                                   id="linkPostInner_<?php echo $contadorSlider; ?>_<?php echo $contadorPost; ?>"
                                                   value="<?php echo $linkPost; ?>" maxlength="15">
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <?php $contadorPost++; ?>
                        <?php } ?>
                    </ul>

                    <div class="divPostEditDescription">
                        <textarea class="inputTextarea inputTextAreaEditPost"
                                  data-idslider="<?php echo $contadorSlider; ?>"
                                  id="descPostInner_<?php echo $contadorSlider; ?>" maxlength="120"></textarea>
                    </div>

                </div>
            <?php } ?>

        </div>

        <div id="divButtonZone">

            <div id="saveButton"><?php echo Yii::t('app', 'Save'); ?></div>
            <div id="finishButton"><?php echo Yii::t('app', 'Finish'); ?></div>

            <div id="backButton"><?php echo Yii::t('app', 'Back'); ?></div>

        </div>

    </div>

</div>

<div id="divHiddenPopup" style="display: none; text-align: center;">
    <table height="100%" width='100%'>
        <tr>
            <td>
                <div style="line-height: 6em;text-align: center;"><img src="/images/loading2.gif"></div>
                <div
                  style="text-align: center; font-weight: bold; font-size: 1.3em; line-height: 3em;"><?php echo Yii::t('app',
                      'Loading'); ?> ...
                </div>
                <div style="text-align: center; font-size: 1em;"><?php echo Yii::t('app',
                      'Please, wait some seconds.'); ?></div>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
</div>

<script>

    $(document).ready(function () {
        resizeSectionBody();

        $(window).resize(function () {
            resizeSectionBody();
        });

        $('#textareaHeader').elastic();
        $('.inputTextAreaEditPost').elastic().keyup(function () {
            var idSlider = $(this).data('idslider');
            var idPost = $('#idPost_' + idSlider).val();
            $('#pDescriptionPost_' + idSlider + '_' + idPost).html($(this).val());
            $('#descPost_' + idSlider).html($(this).val());
        }).blur(function () {
            $('.inputTextAreaEditPost').hide();
        });

        $('.pDescriptionPost').dblclick(function () {
            var idSlider = $(this).data('idslider');
            $('.inputTextAreaEditPost').hide();
            $('#descPostInner_' + idSlider).html($('#descPost_' + idSlider).html());
            $('#descPostInner_' + idSlider).show();
        });

        $('.inputTextLink').change(function () {
            $('#linkPost_' + $(this).data('numslider')).val($(this).val());
        });

        <?php for( $i=1; $i<6; $i++ ) { ?>
        $('#bxslider<?php echo $i; ?>').bxSlider({
            mode: 'fade',
            onSlideAfter: function ($slideElement, oldIndex, newIndex) {
                $('#idPost_<?php echo $i; ?>').html(arPost[newIndex]);

                var descValue = 'pDescriptionPost_<?php echo $i; ?>_' + arPost[newIndex];
                $('#descPost_<?php echo $i; ?>').html($('#' + descValue).html());

                var linkValue = 'linkPostInner_<?php echo $i; ?>_' + newIndex;
                $('#linkPost_<?php echo $i; ?>').html($('#' + linkValue).val());
            },
        }).goToSlide(<?php $indexIdPost = 'idPost'.$i; echo $$indexIdPost; ?>);
        <?php } ?>

        $('#backButton').click(function () {
            var pageURL = '/index.php?r=blognewsletter/index';

            var parameters = {};

            abaCore.changePage(pageURL, parameters);
        });

        $('#saveButton,#finishButton').click(function () {
            var url = '/index.php?r=blognewsletter/save&id=<?php echo $mPublish->id; ?>';

            var parameters = {
                buttonType: $(this).attr('id'),
                idPublication: <?php echo $mPublish->id; ?>,
                idBlog: <?php echo $mPublish->idBlog; ?>,
                idPost1: $('#idPost_1').html(),
                descPost1: $('#descPost_1').html(),
                linkPost1: $('#linkPost_1').html(),
                idPost2: $('#idPost_2').html(),
                descPost2: $('#descPost_2').html(),
                linkPost2: $('#linkPost_2').html(),
                idPost3: $('#idPost_3').html(),
                descPost3: $('#descPost_3').html(),
                linkPost3: $('#linkPost_3').html(),
                idPost4: $('#idPost_4').html(),
                descPost4: $('#descPost_4').html(),
                linkPost4: $('#linkPost_4').html(),
                idPost5: $('#idPost_5').html(),
                descPost5: $('#descPost_5').html(),
                linkPost5: $('#linkPost_5').html(),
                inputSubject: $('#inputSubject').val(),
                textareaHeader: $('#textareaHeader').val(),
                inputTweet: $('#inputTweet').val(),
                linkTweet: $('#inputLinkTweet').val(),
            };

            var successF = function (data) {
                if (data.ok) {
                    var pageURL = '/index.php?r=blognewsletter/index';
                    var parameters = {};

                    abaCore.changePage(pageURL, parameters);
                } else {
                    abaCore.showDialog('<html><body><table width="100%" height="88%"><tr><td width="100%" style="text-align: center;"><img src="/images/redalert2.png" align="bottom">&nbsp; ' + data.errorMsg + '</td></tr></table></body></html>', true, '25%', '25%');
                }
            };

            var errorF = function (data) {
                console.log('error');
            };

            abaCore.launchAjax(url, parameters, successF, errorF);
        });

    });

    function resizeSectionBody() {
        return;
        var header_h = $('#header').outerHeight();
        var menu_h = $('.menu').outerHeight();
        var breadcrumbs_h = $('.breadcrumbs').outerHeight();
        var footer_h = $('#footer').outerHeight();
        var windowBody_h = $(window).outerHeight();
        var contentHeight = $('#content').outerHeight();
        var sectionHeight = $('.sectionBody').outerHeight();

        $('#content').height(windowBody_h - header_h - menu_h - breadcrumbs_h - footer_h - 100);
    }

</script>
