<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name;

$this->breadcrumbs=array(
  'Newsletter',
);

Yii::app()->clientScript->registerScript('search', "

    function reinstallButtons() {
      $('select[name=\"BlogNewsletterPublication[idBlog]\"]').change( function() {
        return refreshList();
      });
      $('select[name=\"BlogNewsletterPublication[status]\"]').change( function() {
        return refreshList();
      });
      $('select[name=\"BlogNewsletterPublication[dateToSend]\"]').change( function() {
        return refreshList();
      });
    }
    reinstallButtons();
");

?>
<link rel="stylesheet" href="/css/sections/blognewsletter/main.css" type="text/css" media="screen" />
<!-- FancyBox -->
<link rel="stylesheet" href="/js/fancyapps-fancyBox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="/js/fancyapps-fancyBox/jquery.fancybox.pack.js?v=2.1.5"></script>

<script src="/js/aba/core.js"></script>

  <style>
    #newPublicationButton {
      -webkit-border-radius: 2em;
      -moz-border-radius: 2em;
      border-radius: 2em;

      border: 1px solid #9AAFE5;
      font-weight: bold;
      padding: 0.2em 1em;
      text-decoration: none;

      background: none repeat scroll 0% 0% #0297D1;
      color: #FFF;
      margin: 0.5em;

      cursor: pointer;

      width: auto;
      float: right;
    }
  </style>


<?php $this->widget('application.components.widgets.CGridViewABA', array(
  'id'=>'aba-newsletter-grid',
  'extraInfo'=>array('update', 'view'),
  'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
  'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
    'prevPageLabel'  => '<',
    'nextPageLabel'  => '>',
    'firstPageLabel' => '<<',
    'lastPageLabel'  => '>>',),
  'extraButtons'=>array(

  ), //end ExtraButtons
  'ajaxType' => 'post',
  'afterAjaxUpdate' => 'reinstallButtons',

  'dataProvider'=>$mPublish->searchIndex(),
  'filter'=>$mPublish,
//  'filterSelector'=>'{filter}',
  'columns'=>array
  (

    array(
      'name' => 'idBlog',
      'header' => Yii::t( 'app', 'Blog' ),
      'value' => '$data->blogName( $data->idBlog )',
      'filter' => $mPublish->blogNames(),
    ),

    array(
      'name' => 'status',
      'header' => Yii::t( 'app', 'Status' ),
      'value' => '$data->blogStatus( $data->status )',
      'filter' => $mPublish->blogStatusFilter(),
    ),

    array(
      'name' => 'dateToSend',
      'header' => Yii::t( 'app', 'Fecha' ),
      'value' => '$data->blogFecha( $data->dateToSend )',
      'filter' => $mPublish->blogFechaFilter(),
    ),

    array(
      'header'=>'Options',
      'class'=>'CButtonColumnABA',
    ),

  ),
));

?>
<div style="clear: both;"></div>
<div>
  <div id="newPublicationButton"><?php echo Yii::t('app', 'Nueva publicación'); ?></div>
</div>
<div style="clear: both;"></div>

<script>
  $(document).ready( function() {
    $('#newPublicationButton').click( function() {
      abaCore.showDialog( <?php echo CJavaScript::encode( $this->renderPartial('popup_fechapublicacion', array(), true) ); ?>, true, '250px', '100px' );
    });
  });

  function refreshList() {
    var id='aba-newsletter-grid';
    var inputSelector='#'+id+' .filters input, '+'#'+id+' .filters select';
    var data=$.param($(inputSelector));
    $.fn.yiiGridView.update(id, {data: data});
    return false;
  }

</script>
