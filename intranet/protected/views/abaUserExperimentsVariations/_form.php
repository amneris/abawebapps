<?php
/* @var $this AbaUserExperimentsVariationsController */
/* @var $model AbaUserExperimentsVariations */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'id' => 'aba-user-experiments-variations-form',
            'enableAjaxValidation' => false,
        )
    );

    $options = HeList::statusUserExperimentsVariations();

    $modelExperimentsVariations = new AbaExperimentsVariations();
    $optionsProductGroup = $modelExperimentsVariations->getFullList(true);

    $cssPrefix = (isset($wAction) AND $wAction == 'update' ? 'Cm' : '');

    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <table id="myForm">
        <?php if(is_numeric($model->userExperimentVariationId)): ?>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'userExperimentVariationId'); ?>
            </td>
            <td>
                <?php echo $model->userExperimentVariationId; ?>
                <?php echo $form->error($model, 'userExperimentVariationId'); ?>
            </td>
        </tr>
        <?php endif; ?>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'userId'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'userId', array('size' => 50, 'maxlength' => 150)); ?>
                <?php echo $form->error($model, 'userId'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'experimentVariationId'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'experimentVariationId', $optionsProductGroup, array()); ?>
                <?php echo $form->error($model, 'experimentVariationId'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'status'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'status', $options); ?>
                <?php echo $form->error($model, 'status'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'dateAdd'); ?>
            </td>
            <td>
                <?php
                echo $form->textField($model, 'dateAdd', array('size' => 15, 'maxlength' => 18));
                echo $form->error($model, 'dateAdd');
                echo CHtml::image(
                    "images/calendar.jpg",
                    "calendar0",
                    array("id" => "c_dateAdd", "class" => "pointer")
                );
                $this->widget(
                    'application.extensions.calendar.SCalendar',
                    array(
                        'inputField' => $cssPrefix . 'AbaUserExperimentsVariations_dateAdd',
                        'button' => 'c_dateAdd',
                        'ifFormat' => '%Y-%m-%d %H:%M:%S',
                    )
                );
                ?>
                <?php echo $form->error($model, 'dateAdd'); ?>
            </td>
        </tr>
    </table>
    <div class="row buttons" align="center">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->