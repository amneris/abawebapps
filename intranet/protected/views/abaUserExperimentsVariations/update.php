<?php
/* @var $this AbaUserExperimentsVariationsController */
/* @var $model AbaUserExperimentsVariations */

$this->breadcrumbs = array(
    'Aba user experiments variations' => array('admin'),
    $model->userExperimentVariationId => array('view', 'id' => $model->userExperimentVariationId),
    'Update',
);

$this->menu = array(
    array('label' => 'List User experiments variations', 'url' => array('index')),
    array('label' => 'View User experiments variations', 'url' => array('view', 'id' => $model->userExperimentVariationId)),
    array('label' => 'Manage User experiments variations', 'url' => array('admin')),
);
?>

<h3>Update user experiments variation: <?php echo $model->userExperimentVariationId; ?></h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>