<?php
/* @var $this AbaUserExperimentsVariationsController */
/* @var $model AbaUserExperimentsVariations */

$this->breadcrumbs = array(
    'Aba User experiments variations',
);

$this->menu = array(
    array('label' => 'Manage user experiments variations', 'url' => array('admin')),
);
?>

<h3>User experiments variations</h3>

<?php $this->widget(
    'zii.widgets.CListView',
    array(
        'dataProvider' => $dataProvider,
        'itemView' => '_view',
    )
); ?>
