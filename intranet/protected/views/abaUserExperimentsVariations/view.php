<?php
/* @var $this AbaUserExperimentsVariationsController */
/* @var $model AbaUserExperimentsVariations */

$this->breadcrumbs = array(
    'User experiments variations' => array('admin'),
    $model->userExperimentVariationId,
);

$this->menu = array(
    array('label' => 'List User experiments variations', 'url' => array('index')),
    array('label' => 'Update User experiments variations', 'url' => array('update', 'id' => $model->userExperimentVariationId)),
    array('label' => 'Manage User experiments variations', 'url' => array('admin')),
);
?>

<h3>View user experiments variations <b><?php echo $model->userExperimentVariationId; ?></b></h3>

<?php $this->widget(
    'application.components.widgets.CDetailViewABA',
    array(
        'data' => $model,
        'attributes' => array(
            'userExperimentVariationId',
            'userId',
            'experimentVariationId' =>
              array(
                  'name' => 'experimentVariationId',
                  'type' => 'experimentsVariationsList',
                  'filter' => HeList::getExperimentsVariationsList()
              ),
            'status' =>
              array(
                  'name' => 'status',
                  'type' => 'userExperimentsVariations',
                  'filter' => HeList::statusUserExperimentsVariations()
              ),
            'dateAdd',
        ),
    )
); ?>
