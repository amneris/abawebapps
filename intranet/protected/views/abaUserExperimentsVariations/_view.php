<?php
/* @var $this AbaUserExperimentsVariationsController */
/* @var $model AbaUserExperimentsVariations */
?>

<div class="view">

    <b/><?php echo CHtml::encode($data->getAttributeLabel('userExperimentVariationId')); ?>:</b>
    <?php echo CHtml::encode($data->userExperimentVariationId); ?>
    <br/>
    <b/><?php echo CHtml::encode($data->getAttributeLabel('userId')); ?>:</b>
    <?php echo CHtml::encode($data->userId); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('experimentVariationId')); ?>:</b>
    <?php echo CHtml::encode($data->experimentVariationId); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
    <?php echo CHtml::encode($data->status); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('dateAdd')); ?>:</b>
    <?php echo CHtml::encode($data->dateAdd); ?>
    <br/>

</div>