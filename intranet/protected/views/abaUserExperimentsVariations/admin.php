<?php
/* @var $this AbaUserExperimentsVariationsController */
/* @var $model AbaUserExperimentsVariations */

$this->breadcrumbs = array(
    'User experiments variations ',
);

Yii::app()->clientScript->registerScript(
    'search',
    "
   $('.search-button').click(function(){
     $('.search-form').toggle();
     return false;
   });
   $('.search-form form').submit(function(){
     $.fn.yiiGridView.update('aba-user-experiments-variations-grid', {
       data: $(this).serialize()
     });
     return false;
   });
   "
);
?>

<h3>User experiments variations </h3>

<br>
<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial(
        '_search',
        array(
            'model' => $model,
        )
    ); ?>
</div><!-- search-form -->

<?php $this->widget(
    'application.components.widgets.CGridViewABA',
    array(
        'id' => 'aba-user-experiments-variations-grid',
        'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
        'pager' => array(
            'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
            'prevPageLabel' => '<',
            'nextPageLabel' => '>',
            'firstPageLabel' => '<<',
            'lastPageLabel' => '>>',
        ),
        'dataProvider' => $model->search(),
        'filter' => $model,
        'filterSelector' => '{filter}',
        'columns' => array(
            'userExperimentVariationId',
            'userId',
            'experimentVariationId' => array(
                'name' => 'experimentVariationId',
                'type' => 'experimentsVariationsList',
                'filter' => CHtml::listData(HeList::getExperimentsVariationsList(true), 'id', 'title'),
            ),
            'status' => array(
              'name' => 'status',
              'type' => 'userExperimentsVariations',
              'filter' => HeList::statusUserExperimentsVariations()
            ),
            'dateAdd',
            array(
                'class' => 'CButtonColumnABA',
                'header' => 'Options',
            ),
        ),
        'extraInfo' => array('view'),
        'extraButtons' => array(
            'extraButton1' => array(
                'textExtraLink' => 'User',
                'classExtraLink' => 'payments',
                'urlExtraLink' => '"abaUser/admin_id",array("id"=>$data->userId)'
            ),
        ),
    )
); ?>
