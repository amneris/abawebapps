<?php
/* @var $this AbaUserExperimentsVariationsController */
/* @var $model AbaUserExperimentsVariations */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
      'CActiveForm',
      array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
      )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'userExperimentVariationId'); ?>
        <?php echo $form->textField($model, 'userExperimentVariationId', array('size' => 5, 'maxlength' => 20)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'userId'); ?>
        <?php echo $form->textField($model, 'userId', array('size' => 5, 'maxlength' => 20)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'experimentVariationId'); ?>
        <?php echo $form->textField($model, 'experimentVariationId', array('size' => 5, 'maxlength' => 20)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'status'); ?>
        <?php echo $form->DropDownList($model, 'status', HeList::statusUserExperimentsVariations(), array('empty' => '--')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dateAdd');
        echo $form->textField($model, 'dateAdd');
        echo CHtml::image("images/calendar.jpg", "calendar1", array("id" => "c_buttonStart", "class" => "pointer"));
        $this->widget(
          'application.extensions.calendar.SCalendar',
          array(
            'inputField' => 'AbaUserExperimentsVariations_dateAdd',
            'button' => 'c_buttonStart',
            'ifFormat' => '%Y-%m-%d 00:00:00',
          )
        );
        ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->