<?php
/* @var $this AbaUserExperimentsVariationsController */
/* @var $model AbaUserExperimentsVariations */

$this->breadcrumbs = array(
    'User experiments variations' => array('admin'),
    'Create',
);

$this->menu = array(
    array('label' => 'Manage user experiments variations', 'url' => array('admin')),
);
?>

    <h3>User experiments variations</h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>