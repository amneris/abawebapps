<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs=array(
	'Configs',
);
?>
<h3>Configs</h3>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'config-grid',
    'selectableRows' => 1,
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
        'prevPageLabel'  => '<',
        'nextPageLabel'  => '>',
        'firstPageLabel' => '<<',
        'lastPageLabel'  => '>>',),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'key',
		'value',
		'description',
		'dateUpdate',
		array(
			'class'=>'CButtonColumn',
            'template'=>'{view}{update}',
		),
	),
)); ?>
