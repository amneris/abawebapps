<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs=array(
	'Chat Status',
);

?>

<h3>Chat Status</h3>

<?php
if($model->value == 1)
    echo "The chat is <b><span style=\"color:green;\">enabled</span></b> since last change on: <b>".$model->dateUpdate."</b>";
else
    echo "The chat is <b><span style=\"color:red;\">disabled</span></b> since last change on: <b>".$model->dateUpdate."</b>";
?>

