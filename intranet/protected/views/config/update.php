<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs=array(
	'Configs'=>array('admin'),
	$model->key=>array('view','id'=>$model->key),
	'Update',
);

?>

<h3>Update config <?php echo $model->key; ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>