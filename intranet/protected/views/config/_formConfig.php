<?php
/* @var $this ConfigController */
/* @var $model Config */
/* @var $form CActiveForm */


$userRole = Yii::app()->user->getId();
$isDisabled = ($userRole != 'root') ? 'disabled' : '';
$eventClick = ( empty($isDisabled) ) ? 'onClick' : 'noClick';

?>
<script type="text/javascript">

    // unblock when ajax activity stops
    $(document).ajaxStop($.unblockUI);

    function blockPage() {
        $.blockUI({
            message: 'Just a moment, applying settings...',
            css: {
                backgroundColor: 'rgba(44, 44, 44, 0.6)',
                color: '#fff',
                cursor: 'wait',
                width: '30%',
                border: '1px solid #aaa',
                padding: 10
            }
        });
    }

    $(document).ready(function () {
          $('#view').change(function () {
              blockPage();
              if ($('#view').is(":checked"))
                  $('#valor').val(1);
              else
                  $('#valor').val(0);
          });
          $('#viewPREMIUM').change(function () {
              blockPage();
              if ($('#viewPREMIUM').is(":checked"))
                  $('#valorPREMIUM').val(1);
              else
                  $('#valorPREMIUM').val(0);
          });
          $('#viewEN').change(function () {
              blockPage();
              if ($('#viewEN').is(":checked"))
                  $('#valorEN').val(1);
              else
                  $('#valorEN').val(0);
          });
          $('#viewES').change(function () {
              blockPage();
              if ($('#viewES').is(":checked"))
                  $('#valorES').val(1);
              else
                  $('#valorES').val(0);
          });
          $('#viewIT').change(function () {
              blockPage();
              if ($('#viewIT').is(":checked"))
                  $('#valorIT').val(1);
              else
                  $('#valorIT').val(0);
          });
          $('#viewDE').change(function () {
              blockPage();
              if ($('#viewDE').is(":checked"))
                  $('#valorDE').val(1);
              else
                  $('#valorDE').val(0);
          });
          $('#viewFR').change(function () {
              blockPage();
              if ($('#viewFR').is(":checked"))
                  $('#valorFR').val(1);
              else
                  $('#valorFR').val(0);
          });
          $('#viewPT').change(function () {
              blockPage();
              if ($('#viewPT').is(":checked"))
                  $('#valorPT').val(1);
              else
                  $('#valorPT').val(0);
          });
        $('#viewHELPALL').change(function () {
            blockPage();
            if ($('#viewHELPALL').is(":checked"))
                $('#valorHELPALL').val(1);
            else
                $('#valorHELPALL').val(0);
        });
        $('#viewHELPABA').change(function () {
            blockPage();
            if ($('#viewHELPALL').is(":checked"))
                $('#valorHELPABA').val(1);
            else
                $('#valorHELPABA').val(0);
        });
      }
    );

</script>

<?php if ( empty($isDisabled) ) { ?>
  <div style="color:red; text-align: center; font-size: 1.2em;"> Recuerda <a href="http://myabamemcache.aba.land/" target="_blank">hacer un flush</a> despues de cualquier cambio. </div>
<?php } else { ?>
  <div style="color:red; text-align: center; font-size: 1.2em;"> This section only available for admin users. </div>
<?php } ?>

<div class="form">
    <table id="myForm">

        <tr>
            <td colspan="3" style="padding-top: 25px; padding-bottom: 10px; text-align: left">
                <span style="font-weight: bold; font-size: 14px;">Chat settings</span>
            </td>
        </tr>
        <tr>
            <?php $formChat = $this->beginWidget('CActiveForm', array(
              'id' => 'chat-form',
              'enableAjaxValidation' => false,
            )); ?>
            <?php echo $formChat->errorSummary($modelChat); ?>
            <?php echo $formChat->hiddenField($modelChat, 'key', array('size' => 70, 'maxlength' => 100)); ?>
            <?php echo $formChat->hiddenField($modelChat, 'description', array('size' => 15)); ?>
            <?php echo $formChat->hiddenField($modelChat, 'value',
              array('id' => 'valor', 'rows' => 6, 'cols' => 60)); ?>
            <?php echo $formChat->hiddenField($modelChat, 'dateUpdate', array('size' => 15)); ?>
            <td width="140px" style="padding-right:0; margin-right:0;">
                Chat
            </td>
            <td width="75px" style="padding-left:0;  margin-left:0;">
                <label class="checkbox toggle candy" <?= $eventClick ?>="$('#chat-form').submit()" style="width: 75px">
                    <?php
                    if ($modelChat->value == 0) {
                        echo "<input id=\"view\" type=\"checkbox\" name=\"view\" ".$isDisabled.">";
                    } else {
                        echo "<input id=\"view\" type=\"checkbox\" checked ".$isDisabled.">";
                    }
                    ?>
                    <p>
                        <span>On</span>
                        <span>Off</span>
                    </p>
                    <a class="slide-button"></a>
                </label>
            </td>
            <td>
                <span class="lastUpdateLabel"> last update was <?php echo $modelChat->dateUpdate ?></span>
            </td>
            <?php $this->endWidget(); ?>
        </tr>

        <tr>
            <?php $formZenddesk = $this->beginWidget('CActiveForm', array(
              'id' => 'chat-zenddesk-form',
              'enableAjaxValidation' => false,
            )); ?>
            <?php echo $formZenddesk->errorSummary($modelZenddesk); ?>
            <?php echo $formZenddesk->hiddenField($modelZenddesk, 'key', array('size' => 70, 'maxlength' => 100)); ?>
            <?php echo $formZenddesk->hiddenField($modelZenddesk, 'description', array('size' => 15)); ?>
            <?php echo $formZenddesk->hiddenField($modelZenddesk, 'dateUpdate', array('size' => 15)); ?>
            <td width="140px" style="padding-right:0; margin-right:0;">
                <?php echo $formZenddesk->textField($modelZenddesk, 'value',
                  array('size' => 15, 'maxlength' => 200, 'disabled' => $isDisabled)); ?>
            </td>
            <td width="75px" style="padding-left:0;  margin-left:0;">
                <?php if ( empty($isDisabled) ) { ?>
                <input type="button" value="Save" onclick="$('#chat-zenddesk-form').submit();"/>
                <?php } ?>
            </td>
            <td>
                <span class="lastUpdateLabel"> last update was <?php echo $modelZenddesk->dateUpdate; ?></span>
            </td>
            <?php $this->endWidget(); ?>
        </tr>

        <tr>
            <td colspan="3" style="padding-top: 25px; padding-bottom: 10px; text-align: left">
                <span style="font-weight: bold; font-size: 14px;">Zendesk Help Center</span>
            </td>
        </tr>

        <tr>
            <?php $formZenddeskHC = $this->beginWidget('CActiveForm', array(
              'id' => 'ZendeskHC-form',
              'enableAjaxValidation' => false,
            )); ?>
            <?php echo $formZenddeskHC->errorSummary($modelZenddeskHelpCenter); ?>
            <?php echo $formZenddeskHC->hiddenField($modelZenddeskHelpCenter, 'key', array('size' => 70, 'maxlength' => 100)); ?>
            <?php echo $formZenddeskHC->hiddenField($modelZenddeskHelpCenter, 'description', array('size' => 15)); ?>
            <?php echo $formZenddeskHC->hiddenField($modelZenddeskHelpCenter, 'value',
              array('id' => 'valorHELPALL', 'rows' => 6, 'cols' => 60)); ?>
            <?php echo $formZenddeskHC->hiddenField($modelZenddeskHelpCenter, 'dateUpdate', array('size' => 15)); ?>
            <td width="140px" style="padding-right:0; margin-right:0;">
                Zendesk Help Center for all.
            </td>
            <td width="75px" style="padding-left:0;  margin-left:0;">
                <label class="checkbox toggle candy" <?= $eventClick ?>="$('#ZendeskHC-form').submit()" style="width: 75px">
                    <?php
                    if ($modelZenddeskHelpCenter->value == 0) {
                        echo "<input id=\"viewHELPALL\" type=\"checkbox\" name=\"viewHELPALL\" ".$isDisabled.">";
                    } else {
                        echo "<input id=\"viewHELPALL\" type=\"checkbox\" name=\"viewHELPALL\" checked ".$isDisabled.">";
                    }
                    ?>
                    <p>
                        <span>On</span>
                        <span>Off</span>
                    </p>
                    <a class="slide-button"></a>
                </label>
            </td>
            <td>
                <span class="lastUpdateLabel"> last update was <?php echo $modelZenddeskHelpCenter->dateUpdate ?></span>
            </td>
            <?php $this->endWidget(); ?>
        </tr>

        <tr>
            <?php $formZenddeskHCABA = $this->beginWidget('CActiveForm', array(
              'id' => 'ZendeskHCABA-form',
              'enableAjaxValidation' => false,
            )); ?>
            <?php echo $formZenddeskHCABA->errorSummary($modelZenddeskHelpCenterABA); ?>
            <?php echo $formZenddeskHCABA->hiddenField($modelZenddeskHelpCenterABA, 'key', array('size' => 70, 'maxlength' => 100)); ?>
            <?php echo $formZenddeskHCABA->hiddenField($modelZenddeskHelpCenterABA, 'description', array('size' => 15)); ?>
            <?php echo $formZenddeskHCABA->hiddenField($modelZenddeskHelpCenterABA, 'value',
              array('id' => 'valorHELPABA', 'rows' => 6, 'cols' => 60)); ?>
            <?php echo $formZenddeskHCABA->hiddenField($modelZenddeskHelpCenterABA, 'dateUpdate', array('size' => 15)); ?>
            <td width="340px" style="padding-right:0; margin-right:0;">
                Zendesk Help Center only for ABA Users
            </td>
            <td width="75px" style="padding-left:0;  margin-left:0;">
                <label class="checkbox toggle candy" <?= $eventClick ?>="$('#ZendeskHCABA-form').submit()" style="width: 75px">
                    <?php
                    if ($modelZenddeskHelpCenterABA->value == 0) {
                        echo "<input id=\"viewHELPABA\" type=\"checkbox\" name=\"viewHELPABA\" ".$isDisabled.">";
                    } else {
                        echo "<input id=\"viewHELPABA\" type=\"checkbox\" name=\"viewHELPABA\" checked ".$isDisabled.">";
                    }
                    ?>
                    <p>
                        <span>On</span>
                        <span>Off</span>
                    </p>
                    <a class="slide-button"></a>
                </label>
            </td>
            <td>
                <span class="lastUpdateLabel"> last update was <?php echo $modelZenddeskHelpCenterABA->dateUpdate ?></span>
            </td>
            <?php $this->endWidget(); ?>
        </tr>

        <tr>
            <td colspan="3" style="padding-top: 25px; padding-bottom: 10px; text-align: left">
                <span style="font-weight: bold; font-size: 14px;">Costumer support banner settings</span>
            </td>
        </tr>

        <tr>
            <?php $formPREMIUM = $this->beginWidget('CActiveForm', array(
              'id' => 'costumerSupportPREMIUM-form',
              'enableAjaxValidation' => false,
            )); ?>
            <?php echo $formPREMIUM->errorSummary($modelCostumerSupportPREMIUM); ?>
            <?php echo $formPREMIUM->hiddenField($modelCostumerSupportPREMIUM, 'key',
              array('size' => 70, 'maxlength' => 100)); ?>
            <?php echo $formPREMIUM->hiddenField($modelCostumerSupportPREMIUM, 'description', array('size' => 15)); ?>
            <?php echo $formPREMIUM->hiddenField($modelCostumerSupportPREMIUM, 'value',
              array('id' => 'valorPREMIUM', 'rows' => 6, 'cols' => 60)); ?>
            <?php echo $formPREMIUM->hiddenField($modelCostumerSupportPREMIUM, 'dateUpdate', array('size' => 15)); ?>
            <td width="140px" style="padding-right:0; margin-right:0;">
                for Premium Users
            </td>
            <td width="75px" style="padding-left:0;  margin-left:0;">
                <label class="checkbox toggle candy" <?= $eventClick ?>="$('#costumerSupportPREMIUM-form').submit()"
                       style="width: 75px">
                    <?php
                    if ($modelCostumerSupportPREMIUM->value == 0) {
                        echo "<input id=\"viewPREMIUM\" type=\"checkbox\" name=\"viewPREMIUM\" ".$isDisabled.">";
                    } else {
                        echo "<input id=\"viewPREMIUM\" type=\"checkbox\" checked ".$isDisabled.">";
                    }
                    ?>
                    <p>
                        <span>On</span>
                        <span>Off</span>
                    </p>
                    <a class="slide-button"></a>
                </label>
            </td>
            <td>
                <span
                  class="lastUpdateLabel">last update was <?php echo $modelCostumerSupportPREMIUM->dateUpdate ?></span>
            </td>
            <?php $this->endWidget(); ?>
        </tr>

        <tr>
            <?php $formEN = $this->beginWidget('CActiveForm', array(
              'id' => 'costumerSupportEN-form',
              'enableAjaxValidation' => false,
            )); ?>
            <?php echo $formEN->errorSummary($modelCostumerSupportEN); ?>
            <?php echo $formEN->hiddenField($modelCostumerSupportEN, 'key', array('size' => 70, 'maxlength' => 100)); ?>
            <?php echo $formEN->hiddenField($modelCostumerSupportEN, 'description', array('size' => 15)); ?>
            <?php echo $formEN->hiddenField($modelCostumerSupportEN, 'value',
              array('id' => 'valorEN', 'rows' => 6, 'cols' => 60)); ?>
            <?php echo $formEN->hiddenField($modelCostumerSupportEN, 'dateUpdate', array('size' => 15)); ?>
            <td width="140px" style="padding-right:0; margin-right:0;">
                for EN
            </td>
            <td width="75px" style="padding-left:0;  margin-left:0;">
                <label class="checkbox toggle candy" <?= $eventClick ?>="$('#costumerSupportEN-form').submit()"
                       style="width: 75px">
                    <?php
                    if ($modelCostumerSupportEN->value == 0) {
                        echo "<input id=\"viewEN\" type=\"checkbox\" name=\"viewEN\" ".$isDisabled.">";
                    } else {
                        echo "<input id=\"viewEN\" type=\"checkbox\" checked ".$isDisabled.">";
                    }
                    ?>
                    <p>
                        <span>On</span>
                        <span>Off</span>
                    </p>
                    <a class="slide-button"></a>
                </label>
            </td>
            <td>
                <span class="lastUpdateLabel">last update was <?php echo $modelCostumerSupportEN->dateUpdate ?></span>
            </td>
            <?php $this->endWidget(); ?>
        </tr>

        <tr>
            <?php $formES = $this->beginWidget('CActiveForm', array(
              'id' => 'costumerSupportES-form',
              'enableAjaxValidation' => false,
            )); ?>
            <?php echo $formES->errorSummary($modelCostumerSupportES); ?>
            <?php echo $formES->hiddenField($modelCostumerSupportES, 'key', array('size' => 70, 'maxlength' => 100)); ?>
            <?php echo $formES->hiddenField($modelCostumerSupportES, 'description', array('size' => 15)); ?>
            <?php echo $formES->hiddenField($modelCostumerSupportES, 'value',
              array('id' => 'valorES', 'rows' => 6, 'cols' => 60)); ?>
            <?php echo $formES->hiddenField($modelCostumerSupportES, 'dateUpdate', array('size' => 15)); ?>
            <td width="140px" style="padding-right:0; margin-right:0;">
                for ES
            </td>
            <td width="75px" style="padding-left:0;  margin-left:0;">
                <label class="checkbox toggle candy" <?= $eventClick ?>="$('#costumerSupportES-form').submit()"
                       style="width: 75px">
                    <?php
                    if ($modelCostumerSupportES->value == 0) {
                        echo "<input id=\"viewES\" type=\"checkbox\" name=\"viewES\" ".$isDisabled.">";
                    } else {
                        echo "<input id=\"viewES\" type=\"checkbox\" checked ".$isDisabled.">";
                    }
                    ?>
                    <p>
                        <span>On</span>
                        <span>Off</span>
                    </p>
                    <a class="slide-button"></a>
                </label>
            </td>
            <td>
                <span class="lastUpdateLabel">last update was <?php echo $modelCostumerSupportES->dateUpdate ?></span>
            </td>
            <?php $this->endWidget(); ?>
        </tr>

        <tr>
            <?php $formIT = $this->beginWidget('CActiveForm', array(
              'id' => 'costumerSupportIT-form',
              'enableAjaxValidation' => false,
            )); ?>
            <?php echo $formIT->errorSummary($modelCostumerSupportIT); ?>
            <?php echo $formIT->hiddenField($modelCostumerSupportIT, 'key', array('size' => 70, 'maxlength' => 100)); ?>
            <?php echo $formIT->hiddenField($modelCostumerSupportIT, 'description', array('size' => 15)); ?>
            <?php echo $formIT->hiddenField($modelCostumerSupportIT, 'value',
              array('id' => 'valorIT', 'rows' => 6, 'cols' => 60)); ?>
            <?php echo $formIT->hiddenField($modelCostumerSupportIT, 'dateUpdate', array('size' => 15)); ?>
            <td width="140px" style="padding-right:0; margin-right:0;">
                for IT
            </td>
            <td width="75px" style="padding-left:0;  margin-left:0;">
                <label class="checkbox toggle candy" <?= $eventClick ?>="$('#costumerSupportIT-form').submit()"
                       style="width: 75px">
                    <?php
                    if ($modelCostumerSupportIT->value == 0) {
                        echo "<input id=\"viewIT\" type=\"checkbox\" name=\"viewIT\" ".$isDisabled.">";
                    } else {
                        echo "<input id=\"viewIT\" type=\"checkbox\" checked ".$isDisabled.">";
                    }
                    ?>
                    <p>
                        <span>On</span>
                        <span>Off</span>
                    </p>
                    <a class="slide-button"></a>
                </label>
            </td>
            <td>
                <span class="lastUpdateLabel">last update was <?php echo $modelCostumerSupportIT->dateUpdate ?></span>
            </td>
            <?php $this->endWidget(); ?>
        </tr>

        <tr>
            <?php $formDE = $this->beginWidget('CActiveForm', array(
              'id' => 'costumerSupportDE-form',
              'enableAjaxValidation' => false,
            )); ?>
            <?php echo $formDE->errorSummary($modelCostumerSupportDE); ?>
            <?php echo $formDE->hiddenField($modelCostumerSupportDE, 'key', array('size' => 70, 'maxlength' => 100)); ?>
            <?php echo $formDE->hiddenField($modelCostumerSupportDE, 'description', array('size' => 15)); ?>
            <?php echo $formDE->hiddenField($modelCostumerSupportDE, 'value',
              array('id' => 'valorDE', 'rows' => 6, 'cols' => 60)); ?>
            <?php echo $formDE->hiddenField($modelCostumerSupportDE, 'dateUpdate', array('size' => 15)); ?>
            <td width="140px" style="padding-right:0; margin-right:0;">
                for DE
            </td>
            <td width="75px" style="padding-left:0;  margin-left:0;">
                <label class="checkbox toggle candy" <?= $eventClick ?>="$('#costumerSupportDE-form').submit()"
                       style="width: 75px">
                    <?php
                    if ($modelCostumerSupportDE->value == 0) {
                        echo "<input id=\"viewDE\" type=\"checkbox\" name=\"viewDE\" ".$isDisabled.">";
                    } else {
                        echo "<input id=\"viewDE\" type=\"checkbox\" checked ".$isDisabled.">";
                    }
                    ?>
                    <p>
                        <span>On</span>
                        <span>Off</span>
                    </p>
                    <a class="slide-button"></a>
                </label>
            </td>
            <td>
                <span class="lastUpdateLabel">last update was <?php echo $modelCostumerSupportDE->dateUpdate ?></span>
            </td>
            <?php $this->endWidget(); ?>
        </tr>

        <tr>
            <?php $formFR = $this->beginWidget('CActiveForm', array(
              'id' => 'costumerSupportFR-form',
              'enableAjaxValidation' => false,
            )); ?>
            <?php echo $formFR->errorSummary($modelCostumerSupportFR); ?>
            <?php echo $formFR->hiddenField($modelCostumerSupportFR, 'key', array('size' => 70, 'maxlength' => 100)); ?>
            <?php echo $formFR->hiddenField($modelCostumerSupportFR, 'description', array('size' => 15)); ?>
            <?php echo $formFR->hiddenField($modelCostumerSupportFR, 'value',
              array('id' => 'valorFR', 'rows' => 6, 'cols' => 60)); ?>
            <?php echo $formFR->hiddenField($modelCostumerSupportFR, 'dateUpdate', array('size' => 15)); ?>
            <td width="140px" style="padding-right:0; margin-right:0;">
                for FR
            </td>
            <td width="75px" style="padding-left:0;  margin-left:0;">
                <label class="checkbox toggle candy" <?= $eventClick ?>="$('#costumerSupportFR-form').submit()"
                       style="width: 75px">
                    <?php
                    if ($modelCostumerSupportFR->value == 0) {
                        echo "<input id=\"viewFR\" type=\"checkbox\" name=\"viewFR\" ".$isDisabled.">";
                    } else {
                        echo "<input id=\"viewFR\" type=\"checkbox\" checked ".$isDisabled.">";
                    }
                    ?>
                    <p>
                        <span>On</span>
                        <span>Off</span>
                    </p>
                    <a class="slide-button"></a>
                </label>
            </td>
            <td>
                <span class="lastUpdateLabel">last update was <?php echo $modelCostumerSupportFR->dateUpdate ?></span>
            </td>
            <?php $this->endWidget(); ?>
        </tr>

        <tr>
            <?php $formPT = $this->beginWidget('CActiveForm', array(
              'id' => 'costumerSupportPT-form',
              'enableAjaxValidation' => false,
            )); ?>
            <?php echo $formPT->errorSummary($modelCostumerSupportPT); ?>
            <?php echo $formPT->hiddenField($modelCostumerSupportPT, 'key', array('size' => 70, 'maxlength' => 100)); ?>
            <?php echo $formPT->hiddenField($modelCostumerSupportPT, 'description', array('size' => 15)); ?>
            <?php echo $formPT->hiddenField($modelCostumerSupportPT, 'value',
              array('id' => 'valorPT', 'rows' => 6, 'cols' => 60)); ?>
            <?php echo $formPT->hiddenField($modelCostumerSupportPT, 'dateUpdate', array('size' => 15)); ?>
            <td width="140px" style="padding-right:0; margin-right:0;">
                for PT
            </td>
            <td width="75px" style="padding-left:0;  margin-left:0;">
                <label class="checkbox toggle candy" <?= $eventClick ?>="$('#costumerSupportPT-form').submit()"
                       style="width: 75px">
                    <?php
                    if ($modelCostumerSupportPT->value == 0) {
                        echo "<input id=\"viewPT\" type=\"checkbox\" name=\"viewPT\" ".$isDisabled.">";
                    } else {
                        echo "<input id=\"viewPT\" type=\"checkbox\" checked ".$isDisabled.">";
                    }
                    ?>
                    <p>
                        <span>On</span>
                        <span>Off</span>
                    </p>
                    <a class="slide-button"></a>
                </label>
            </td>
            <td>
                <span class="lastUpdateLabel">last update was <?php echo $modelCostumerSupportPT->dateUpdate ?></span>
            </td>
            <?php $this->endWidget(); ?>
        </tr>


    </table>

</div><!-- form -->