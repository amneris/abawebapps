<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs = array(
  'Enable/Disable Configs',
);

?>

    <h3>Enable/Disable Configs</h3>

<?php if (Yii::app()->user->hasFlash('delSucceded')): ?>
    <?php echo "<br>"; ?>
    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('delSucceded'); ?>
    </div>

<?php endif; ?>
<?php if (Yii::app()->user->hasFlash('delError')): ?>
    <?php echo "<br>"; ?>
    <div class="flash-error">
        <?php echo Yii::app()->user->getFlash('delError'); ?>
    </div>

<?php endif; ?>

<?php echo $this->renderPartial('_formConfig', array(
  'modelChat' => $modelChat,
  'modelZenddesk' => $modelZenddesk,
  'modelZenddeskHelpCenter' => $modelZenddeskHelpCenter,
  'modelZenddeskHelpCenterABA' => $modelZenddeskHelpCenterABA,
  'modelCostumerSupportPREMIUM' => $modelCostumerSupportPREMIUM,
  'modelCostumerSupportEN' => $modelCostumerSupportEN,
  'modelCostumerSupportES' => $modelCostumerSupportES,
  'modelCostumerSupportIT' => $modelCostumerSupportIT,
  'modelCostumerSupportDE' => $modelCostumerSupportDE,
  'modelCostumerSupportFR' => $modelCostumerSupportFR,
  'modelCostumerSupportPT' => $modelCostumerSupportPT,
));
?>