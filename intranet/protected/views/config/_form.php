<?php
/* @var $this ConfigController */
/* @var $model Config */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'config-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <table id="myForm">
        <tr>
            <td>
                <?php echo $form->labelEx($model,'key'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'key',array('disabled'=>'true','size'=>70,'maxlength'=>100)); ?>
                <?php echo $form->error($model,'key'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'value'); ?>
            </td>
            <td>
                <?php echo $form->textArea($model,'value',array('rows'=>6, 'cols'=>60)); ?>
                <?php echo $form->error($model,'value'); ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model,'description'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'description',array('size'=>70,'maxlength'=>250)); ?>
                <?php echo $form->error($model,'description'); ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model,'dateUpdate'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'dateUpdate', array('disabled'=>'true', 'size'=>15)); ?>
                <?php echo $form->error($model,'dateUpdate'); ?>
            </td>
        </tr>

    </table>

	<div class="row buttons" align="center">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->