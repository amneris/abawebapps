<?php
/* @var $this AbaPaymentsControlCheckController */
/* @var $data AbaPaymentsControlCheck */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userId')); ?>:</b>
	<?php echo CHtml::encode($data->userId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idProduct')); ?>:</b>
	<?php echo CHtml::encode($data->idProduct); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCountry')); ?>:</b>
	<?php echo CHtml::encode($data->idCountry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPeriodPay')); ?>:</b>
	<?php echo CHtml::encode($data->idPeriodPay); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paySuppExtId')); ?>:</b>
	<?php echo CHtml::encode($data->paySuppExtId); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('paySuppOrderId')); ?>:</b>
	<?php echo CHtml::encode($data->paySuppOrderId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dateStartTransaction')); ?>:</b>
	<?php echo CHtml::encode($data->dateStartTransaction); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dateEndTransaction')); ?>:</b>
	<?php echo CHtml::encode($data->dateEndTransaction); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dateToPay')); ?>:</b>
	<?php echo CHtml::encode($data->dateToPay); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xRateToEUR')); ?>:</b>
	<?php echo CHtml::encode($data->xRateToEUR); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xRateToUSD')); ?>:</b>
	<?php echo CHtml::encode($data->xRateToUSD); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('currencyTrans')); ?>:</b>
	<?php echo CHtml::encode($data->currencyTrans); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('foreignCurrencyTrans')); ?>:</b>
	<?php echo CHtml::encode($data->foreignCurrencyTrans); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPromoCode')); ?>:</b>
	<?php echo CHtml::encode($data->idPromoCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('amountOriginal')); ?>:</b>
	<?php echo CHtml::encode($data->amountOriginal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('amountDiscount')); ?>:</b>
	<?php echo CHtml::encode($data->amountDiscount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('amountPrice')); ?>:</b>
	<?php echo CHtml::encode($data->amountPrice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kind')); ?>:</b>
	<?php echo CHtml::encode($data->kind); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cardNumber')); ?>:</b>
	<?php echo CHtml::encode($data->cardNumber); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cardYear')); ?>:</b>
	<?php echo CHtml::encode($data->cardYear); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cardMonth')); ?>:</b>
	<?php echo CHtml::encode($data->cardMonth); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cardCvc')); ?>:</b>
	<?php echo CHtml::encode($data->cardCvc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cardName')); ?>:</b>
	<?php echo CHtml::encode($data->cardName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tokenPayPal')); ?>:</b>
	<?php echo CHtml::encode($data->tokenPayPal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payerPayPalId')); ?>:</b>
	<?php echo CHtml::encode($data->payerPayPalId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastAction')); ?>:</b>
	<?php echo CHtml::encode($data->lastAction); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('attempts')); ?>:</b>
	<?php echo CHtml::encode($data->attempts); ?>
	<br />

	*/ ?>

</div>