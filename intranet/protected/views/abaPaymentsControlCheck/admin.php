<?php
/* @var $this AbaPaymentsControlCheckController */
/* @var AbaPaymentsControlCheck $model */
$cs = Yii::app()->getClientScript();
$cs->registerScript(
    'my-changesCheckReview-1',
    'function  changesCheckReview(inId, inCurrent)
    {
        var newVal = inCurrent=="1"?"0":"1";
        var element=document.getElementById("advSearchForm");
        var field= document.getElementById("AbaPaymentsControlCheck_reviewedValue");
        field.value=newVal;
        var field= document.getElementById("AbaPaymentsControlCheck_idReviewed");
        field.value=inId;
        element.submit();
    }

    function sendCheckedReviewed()
    {
        var element=document.getElementById("advSearchForm");
        element.submit();
    }

    function  markCheckedReviewed( objCheck, inId )
    {
        //alert(objCheck.checked + ", name= " + objCheck.name);
        var newVal = "1";
        var element=document.getElementById("advSearchForm");
        var field= document.getElementById("AbaPaymentsControlCheck_reviewedValue");
        field.value=newVal;
        var field= document.getElementById("AbaPaymentsControlCheck_idReviewed");
        if(objCheck.checked){
            if(field.value==""){
                field.value = inId;
            }else{
                field.value = field.value + "," + inId;
            }
        } else{
            field.value = field.value.replace( ","+inId, "");
            field.value = field.value.replace( inId+",", "");
            field.value = field.value.replace( inId, "");
        }
    }
    ',
    CClientScript::POS_END
);
$this->breadcrumbs=array(
	'Paylines log',
);
?>

<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('aba-payments-control-check-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Paylines log (attempts)</h3>
<h5>It displays all list of payments attempts that have not finished successfully.
    <br/>It will never show attempts from users that currently are PREMIUM, even though they recently have payments attempts.
    <br/>This tool is to be used to manage payments attempts of FREE users that were note able to pay in our Campus through PAYPAL or credit card.
</h5>
<?php

  $this->renderPartial('_searchDate',array(
    'model'=>$model, 'startDate'=> $startDate, 'endDate'=>$endDate, 'showReviewed'=>$showReviewed
));
?>

<br>
<div style="text-align: center;"><b>Period from:</b> <?php  echo ($model->startDate." <b>to:</b> ".$model->endDate); ?>
</div>
<br><br>
<?php
//array('AbaPaymentsControlCheck/export/?startDate='.$startDate.'&endDate='.$endDate.'&showReviewed='.$showReviewed)
echo CHtml::link('Export to excel',$this->createUrl("AbaPaymentsControlCheck/export",
                                                    array('startDate'=>$startDate, 'endDate'=>$endDate,'showReviewed'=>$showReviewed)));
echo "<br>";
echo CHtml::link('Advanced Search','#',array('class'=>'search-button'));
?>

<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search',array(
        'model'=>$model, 'startDate'=> $startDate, 'endDate'=>$endDate, 'showReviewed'=>$showReviewed
    )); ?>
</div>


<!-- search-form -->
<div class="row buttons" style="text-align: right;">
    <?php echo CHtml::button('Mark selected as reviewed',array('onclick'=>'sendCheckedReviewed();')); ?>
</div>

<?php $this->widget('application.components.widgets.CGridViewABA', array(
	'id'=>'aba-payments-control-check-grid',
	'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
	'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
                    'prevPageLabel'  => '<',
                    'nextPageLabel'  => '>',
                    'firstPageLabel' => '<<',
                    'lastPageLabel'  => '>>',),
	'dataProvider'=>$model->searchByDate($startDate, $endDate, $showReviewed),
	'extraInfo'=>array('view'),
	'extraButtons'=>array(
		'extraButton1'=> array(
		'textExtraLink'=>'User',
		'classExtraLink'=>'paymentsControlCheck',
		'urlExtraLink'=>'"abaUser/admin_id",array("id"=>$data->userId)'),
		
	),	
	'columns'=>array(
		'userEmail'=>
		array(               
			'name'=>'userId',
		    'type'=>'|"*userId*"|"<strong>".HeSql::getUser("*userId*")."</strong>"',
            ),
		'id',
        'userType'=>
            array(
                'name'=>'userType',
                'type'=>'userTypeDescription',
                'htmlOptions'=>array('style'=>'font-weight: bold;'),
                'filter'=>HeList::userTypeDescription(),
            ),
		'status'=>
            array(
                'name'=>'status',
                'type'=>'statusDescriptionFull',
                'filter'=>HeList::statusDescriptionFull(),
                ),
		'dateStartTransaction'=>array(              
            'name'=>'dateStartTransaction',
            'type'=>'|HeString::cropStr("*dateStartTransaction*", 13)|HeString::tooltip("*dateStartTransaction*");',
            ),
		'dateEndTransaction'=>array(              
            'name'=>'dateEndTransaction',
            'type'=>'|HeString::cropStr("*dateEndTransaction*", 13)|HeString::tooltip("*dateEndTransaction*");',
            ),
		'currencyTrans',
		'amountOriginal',
		'amountPrice',
        'paySuppExtId'=>
        array( 'name'=>'paySuppExtId',
            'type'=>'supplierExtGatewayList',
            'filter'=>HeList::supplierExtGatewayList(),),
		'kind'=>array(
            'name'=>'kind',
            'type'=>'|HeString::showIdStr(*kind*)|HeString::showKindTooltip(*kind*);',
        ),
		'cardNumber'=>array(              
            'name'=>'cardNumber',
	        'type'=>'|HeString::cropStr("[###]", 8)|HeSql::tooltipAES("*id*","cardNumber");',
          ),
		'cardYear'=>array(                
            'name'=>'cardYear',
            'type'=>'|HeString::cropStr("[###]", 8)|HeSql::tooltipAES("*id*","cardYear");',
          ),
		'cardMonth'=>array(              
            'name'=>'cardMonth',
            'type'=>'|HeString::cropStr("[###]", 8)|HeSql::tooltipAES("*id*","cardMonth");',
          ),
		'cardCvc'=>array(              
            'name'=>'cardCvc',
            'type'=>'|HeString::cropStr("[###]", 8)|HeSql::tooltipAES("*id*","cardCvc");',
          ),
		'cardName'=>array(              
            'name'=>'cardName',
            'type'=>'|HeString::cropStr("*cardName*", 10)|HeString::tooltip("*cardName*");',
          ),
        'reviewed'=>array(
            'name'=>'reviewed',
            'type'=>'[HeString::isReviewed("*id*", "*reviewed*")|javascript:changesCheckReview("*id*", "*reviewed*");',
          ),
        'select'=>array(
            'name'=>'Select',
            'value'=>'CHtml::checkBox("idAuxReviewed[]",null,array( "value"=> $data->id,
                                                                    "id"=> "idAuxReviewed_".$data->id,
                                                                    "onclick"=> "markCheckedReviewed(this,\'".$data->id."\')"))',
            'type'=>'raw',
            'htmlOptions'=>array('width'=>5),
        ),
		'attempts',
		array(
			'class'=>'CButtonColumnABA',
		),
	),
)); ?>
