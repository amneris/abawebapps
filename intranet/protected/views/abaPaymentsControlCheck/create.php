<?php
/* @var $this AbaPaymentsControlCheckController */
/* @var $model AbaPaymentsControlCheck */

$this->breadcrumbs=array(
	'Aba Payments Control Checks'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AbaPaymentsControlCheck', 'url'=>array('index')),
	array('label'=>'Manage AbaPaymentsControlCheck', 'url'=>array('admin')),
);
?>

<h1>Create AbaPaymentsControlCheck</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>