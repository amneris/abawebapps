<?php
/* @var $this AbaUserController */
/* @var $model AbaPaymentsControlCheck */
/* @var string $startDate */
/* @var string $endDate */
/* @var integer $showReviewed */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aba-export-payline-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
    
    <table id="myForm">
        
        <tr>
            <td>
                <label for="AbaPaymentsControlCheck_startDate" class="required">Begin period (by date start transaction)</label>
            </td>
            <td>
                <input name="AbaPaymentsControlCheck[startDate]" id="AbaPaymentsControlCheck_startDate" type="text"
                       value="<?php echo ($startDate!=='')? $startDate: HeDate::yesterday(true);?>" />
                <?php 
                    echo CHtml::image("images/calendar.jpg","calendar", array("id"=>"c_button","class"=>"pointer")); 
                    $this->widget('application.extensions.calendar.SCalendar',
                        array(
                        'inputField'=>'AbaPaymentsControlCheck_startDate',
                        'button'=>'c_button',
                        'ifFormat'=>'%Y-%m-%d 00:00:00',
                    ));
                ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <label for="AbaPaymentsControlCheck_endDate" class="required">End period (by date start transaction)</label>
            </td>
            <td>
                <input name="AbaPaymentsControlCheck[endDate]" id="AbaPaymentsControlCheck_endDate" type="text"
                       value="<?php echo ($endDate!=='')? $endDate: HeDate::yesterday(true);?>" />
                <?php 
                    echo CHtml::image("images/calendar.jpg","calendar", array("id"=>"c_buttonEnd","class"=>"pointer")); 
                    $this->widget('application.extensions.calendar.SCalendar',
                        array(
                        'inputField'=>'AbaPaymentsControlCheck_endDate',
                        'button'=>'c_buttonEnd',
                        'ifFormat'=>'%Y-%m-%d 00:00:00',
                    ));
                ?>
            </td>
        </tr>

        <tr>
            <td>
                <label for="AbaPaymentsControlCheck_showReviewed" class="required">Show reviewed and not reviewed</label>
            </td>
            <td>
                <input name="AbaPaymentsControlCheck[showReviewed]" id="AbaPaymentsControlCheck_showReviewed" type="checkbox"
                       value="1" <?php echo ($showReviewed=='1')? "CHECKED":'';?> />
            </td>
        </tr>

    </table>
	<div class="row buttons" align="center">
		<?php echo CHtml::submitButton('Create and download xls'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->