
<?php $form=$this->beginWidget('CActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

<table id="myForm">

    <tr>
        <td width="250px">
            <?php echo $form->label($model,'startDate'); ?>
        </td>
        <td>
            <?php
            echo $form->textField($model,'startDate',array('style'=>'width:70px;'));
            echo CHtml::image("images/calendar.jpg","calendar", array("id"=>"c_button","class"=>"pointer"));
            $this->widget('application.extensions.calendar.SCalendar',
                array(
                    'inputField'=>'AbaPaymentsControlCheck_startDate',
                    'button'=>'c_button',
                    'ifFormat'=>'%Y-%m-%d',
                ));
            ?>
        </td>
    </tr>

    <tr>
        <td>
            <?php echo $form->label($model,'endDate'); ?>
        </td>
        <td>
            <?php

            echo $form->textField($model,'endDate',array('style'=>'width:70px;'));
            echo CHtml::image("images/calendar.jpg","calendar2", array("id"=>"c_button2","class"=>"pointer"));
            $this->widget('application.extensions.calendar.SCalendar',
                array(
                    'inputField'=>'AbaPaymentsControlCheck_endDate',
                    'button'=>'c_button2',
                    'ifFormat'=>'%Y-%m-%d',
                ));
            ?>
        </td>
    </tr>


    <tr>
        <td>

        </td>
        <td>
            <?php
                echo $form->checkBox($model,'showReviewed');
                echo 'Show reviewed and not reviewed';
            ?>
        </td>
    </tr>

</table>
<?php
echo $form->hiddenField($model,'id');
//echo $form->hiddenField($model,'idUserProdStamp');
echo $form->hiddenField($model,'userId');
echo $form->hiddenField($model,'idProduct');
echo $form->hiddenField($model,'idCountry');
echo $form->hiddenField($model,'idPeriodPay');
echo $form->hiddenField($model,'paySuppExtId');

echo $form->hiddenField($model,'status');
echo $form->hiddenField($model,'dateEndTransaction');
echo $form->hiddenField($model,'dateStartTransaction');
echo $form->hiddenField($model,'xRateToEUR');
echo $form->hiddenField($model,'xRateToUSD');
echo $form->hiddenField($model,'currencyTrans');
echo $form->hiddenField($model,'foreignCurrencyTrans');
echo $form->hiddenField($model,'idPromoCode');
echo $form->hiddenField($model,'amountOriginal');
echo $form->hiddenField($model,'amountDiscount');

?>
<div class="row buttons" align="center">
    <?php echo CHtml::submitButton('Search'); ?>
</div>

<?php $this->endWidget(); ?>
