<?php
/* @var $this AbaPaymentsControlCheckController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Aba Payments Control Checks',
);

$this->menu=array(
	array('label'=>'Create AbaPaymentsControlCheck', 'url'=>array('create')),
	array('label'=>'Manage AbaPaymentsControlCheck', 'url'=>array('admin')),
);
?>

<h1>Aba Payments Control Checks</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
