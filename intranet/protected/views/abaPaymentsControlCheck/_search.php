<?php
/* @var $this AbaPaymentsControlCheckController */
/* @var $model AbaPaymentsControlCheck */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
    'id'=>'advSearchForm'
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php /* echo $form->label($model,'idUserProdStamp'); 
		echo $form->textField($model,'idUserProdStamp',array('size'=>10,'maxlength'=>10));*/ ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'userId'); ?>
		<?php echo $form->textField($model,'userId',array('size'=>15,'maxlength'=>64)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idProduct'); ?>
		<?php echo $form->textField($model,'idProduct',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idCountry');
		 echo $form->DropDownList($model,'idCountry',HeList::getCountryList(), array('empty'=>'--Any Country--'));
		?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idPeriodPay'); ?>
		<?php echo $form->textField($model,'idPeriodPay',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'paySuppExtId');
        echo $form->DropDownList($model,'paySuppExtId',HeList::supplierExtGatewayList(), array('empty'=>'-Any Gateway-'));  ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'paySuppOrderId'); ?>
		<?php echo $form->textField($model,'paySuppOrderId',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); 
		 echo $form->DropDownList($model,'status',HeList::statusDescriptionFull(), array('empty'=>'--Any Status--'));
		 ?>
	</div>


	<div class="row">
		<?php echo $form->label($model,'dateEndTransaction'); 
		echo $form->textField($model,'dateEndTransaction'); 
		echo CHtml::image("images/calendar.jpg","calendar2", array("id"=>"c_buttonEnd","class"=>"pointer")); 
			$this->widget('application.extensions.calendar.SCalendar',
				array(
				'inputField'=>'AbaPaymentsControlCheck_dateEndTransaction',
				'button'=>'c_buttonEnd',
				'ifFormat'=>'%Y-%m-%d 00:00:00',
			));
		?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dateToPay'); 
		echo $form->textField($model,'dateToPay'); 
		echo CHtml::image("images/calendar.jpg","calendar3", array("id"=>"c_buttonToPay","class"=>"pointer")); 
			$this->widget('application.extensions.calendar.SCalendar',
				array(
				'inputField'=>'AbaPaymentsControlCheck_dateToPay',
				'button'=>'c_buttonToPay',
				'ifFormat'=>'%Y-%m-%d 00:00:00',
			));
		?>
	</div>


	<div class="row">
		<?php /*echo $form->label($model,'xRateToEUR'); 
		 echo $form->textField($model,'xRateToEUR',array('size'=>18,'maxlength'=>18));*/ ?>
	</div>

	<div class="row">
		<?php /*echo $form->label($model,'xRateToUSD');
		 echo $form->textField($model,'xRateToUSD',array('size'=>18,'maxlength'=>18));*/ ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'currencyTrans'); ?>
		<?php echo $form->textField($model,'currencyTrans',array('size'=>3,'maxlength'=>3)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'foreignCurrencyTrans'); ?>
		<?php echo $form->textField($model,'foreignCurrencyTrans',array('size'=>3,'maxlength'=>3)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idPromoCode'); ?>
		<?php echo $form->textField($model,'idPromoCode',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'amountOriginal'); ?>
		<?php echo $form->textField($model,'amountOriginal',array('size'=>18,'maxlength'=>18)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'amountDiscount'); ?>
		<?php echo $form->textField($model,'amountDiscount',array('size'=>18,'maxlength'=>18)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'amountPrice'); ?>
		<?php echo $form->textField($model,'amountPrice',array('size'=>18,'maxlength'=>18)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kind');
        echo $form->DropDownList($model,'kind', HeList::creditCardDescription(), array('empty'=>'--Any card--')); ?>
	</div>

	<div class="row">
		<?php /*echo $form->label($model,'cardNumber'); 
		 echo $form->textField($model,'cardNumber');*/ ?>
	</div>

	<div class="row">
		<?php /*echo $form->label($model,'cardYear'); 
		 echo $form->textField($model,'cardYear');*/ ?>
	</div>

	<div class="row">
		<?php /*echo $form->label($model,'cardMonth'); 
		 echo $form->textField($model,'cardMonth');*/ ?>
	</div>

	<div class="row">
		<?php /* echo $form->label($model,'cardCvc'); 
		 echo $form->textField($model,'cardCvc');*/ ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cardName'); ?>
		<?php echo $form->textField($model,'cardName',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tokenPayPal'); ?>
		<?php echo $form->textField($model,'tokenPayPal',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'payerPayPalId'); ?>
		<?php echo $form->textField($model,'payerPayPalId',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lastAction'); ?>
		<?php echo $form->textArea($model,'lastAction',array('rows'=>6, 'cols'=>50)); ?>
	</div>

    <?php
    echo $form->hiddenField($model,'endDate');
    echo $form->hiddenField($model,'startDate');
//    echo $form->hiddenField($model,'showReviewed');
    echo $form->hiddenField($model,'idReviewed');
    echo $form->hiddenField($model,'reviewedValue');
    ?>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->