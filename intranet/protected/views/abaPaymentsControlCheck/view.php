<?php
/* @var $this AbaPaymentsControlCheckController */
/* @var $model AbaPaymentsControlCheck */

$this->breadcrumbs=array(
	'Payments Control Checks'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AbaPaymentsControlCheck', 'url'=>array('index')),
	array('label'=>'Create AbaPaymentsControlCheck', 'url'=>array('create')),
	array('label'=>'Update AbaPaymentsControlCheck', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AbaPaymentsControlCheck', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AbaPaymentsControlCheck', 'url'=>array('admin')),
);
?>

<h1>View AbaPaymentsControlCheck #<?php echo $model->id; ?></h1>

<?php $this->widget('application.components.widgets.CDetailViewABA', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'userId',
		'idProduct',
		'idCountry'=>
		array(   
              'name'=>'idCountry',
              'type'=>'countryList',
          ),
		'idPeriodPay',
		'paySuppExtId',
		'paySuppOrderId',
		'status'=>
		array(               // related city displayed as a link
            'name'=>'status',
            'type'=>'statusDescriptionFull',
          ),
		'dateStartTransaction',
		'dateEndTransaction',
		'dateToPay',
		'xRateToEUR',
		'xRateToUSD',
		'currencyTrans',
		'foreignCurrencyTrans',
		'idPromoCode',
		'amountOriginal',
		'amountDiscount',
		'amountPrice',
		'kind',
		
		/*
		'cardNumber',
		'cardYear',
		'cardMonth',
		'cardCvc',*/
		
		'cardNumber'=>array(              
            'name'=>'cardNumber',
      //      'type'=>'|HeString::cropStr("[###]", 8)|HeSql::tooltipAES("*id*","cardNumber");',
	        'type'=>'{HeSql::tooltipAES("*id*","cardNumber");',
          ),
		 'cardNumber2'=>array(              
            'name'=>'cardNumber',
			'label'=>'Bank',
	        'type'=>'{HeSql::getBank("*cardNumber*");',
          ), 
		'cardYear'=>array(                
            'name'=>'cardYear',
            'type'=>'{HeSql::tooltipAES("*id*","cardYear");',
          ),
		'cardMonth'=>array(              
            'name'=>'cardMonth',
            'type'=>'{HeSql::tooltipAES("*id*","cardMonth");',
          ),
		'cardCvc'=>array(              
            'name'=>'cardCvc',
            'type'=>'{HeSql::tooltipAES("*id*","cardCvc");',
          ),
		
		'cardName',
		'tokenPayPal',
		'payerPayPalId',
		'lastAction',
		'attempts',
        'reviewed'=>array(               // related city displayed as a link
            'name'=>'reviewed',
            'type'=>'booleanList',
        ),
	),
)); ?>
