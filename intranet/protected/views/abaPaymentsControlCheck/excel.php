<?php
/* @var $this AbaUserController */
/* @var $model AbaPaymentsControlCheck */
/* @var string $startDate */
/* @var string $endDate */
/* @var integer $showReviewed */


$this->breadcrumbs=array(
	'Paylines log'=>array('admin'),
	'Excel export',
);

?>

<h3>Export paylines logs to excel</h3>
<h5>
    It will export all list of payments attempts that have not finished successfully.
    <br/>It will never export attempts from users that currently are PREMIUM, even though they recently have payments attempts. They are considered useless.
    <br/>This tool is to be used to manage payments attempts of FREE users that were note able to pay in our Campus through PAYPAL or credit card.
</h5>

<?php echo $this->renderPartial('_formexcel', array('model'=>$model,
                                    'startDate'=>$startDate,
                                    'endDate'=>$endDate,
                                    'showReviewed'=>$showReviewed,)); ?>