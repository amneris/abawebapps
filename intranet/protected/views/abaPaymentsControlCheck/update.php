<?php
/* @var $this AbaPaymentsControlCheckController */
/* @var $model AbaPaymentsControlCheck */

$this->breadcrumbs=array(
	'Aba Payments Control Checks'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AbaPaymentsControlCheck', 'url'=>array('index')),
	array('label'=>'Create AbaPaymentsControlCheck', 'url'=>array('create')),
	array('label'=>'View AbaPaymentsControlCheck', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AbaPaymentsControlCheck', 'url'=>array('admin')),
);
?>

<h1>Update AbaPaymentsControlCheck <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>