<?php
/* @var $this AbaPaymentMethodsAdyenHppController */
/* @var $model AbaAdyenHppPaymentMethods */

$this->breadcrumbs = array(
  'Adyen Hpp Payment Method' => array('admin'),
  '[Current Method]' => array('view', 'id' => $model->idPaymentMethod),
  'Update',
);
?>
<?php if (Yii::app()->user->hasFlash('delError')): ?>
    <?php echo "<br>"; ?>
    <div class="flash-error">
        <?php echo Yii::app()->user->getFlash('delError'); ?>
    </div>

<?php endif; ?>

<h3>Edit method: <?php echo $model->paymentMethod; ?></h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
