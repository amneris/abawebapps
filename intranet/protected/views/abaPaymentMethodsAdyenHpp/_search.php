<?php
/* @var $this AbaPaymentMethodsAdyenHppController */
/* @var $model AbaAdyenHppPaymentMethods */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget('CActiveForm', array(
      'action' => Yii::app()->createUrl($this->route),
      'method' => 'get',
    )); ?>

    <div class="row">
        <?php echo $form->label($model, 'idPaymentMethod'); ?>
        <?php echo $form->textField($model, 'idPaymentMethod', array('size' => 15, 'maxlength' => 15)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'paymentMethod'); ?>
        <?php echo $form->textField($model, 'paymentMethod', array('size' => 15, 'maxlength' => 15)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'enabled'); ?>
        <?php echo $form->textField($model, 'enabled', array('size' => 4, 'maxlength' => 4)); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->