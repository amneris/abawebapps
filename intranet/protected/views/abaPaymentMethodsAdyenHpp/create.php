<?php
/* @var $this AbaPaymentMethodsAdyenHppController */
/* @var $model AbaAdyenHppPaymentMethods */

$this->breadcrumbs = array(
  'Adyen Hpp Payment Methods' => array('admin'),
  'Create',
);

$this->menu = array(
  array('label' => 'List Adyen Hpp Payment Methods', 'url' => array('index')),
  array('label' => 'Manage Adyen Hpp Payment Methods', 'url' => array('admin')),
);
?>

<h3>Create Adyen Hpp Payment Method</h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
