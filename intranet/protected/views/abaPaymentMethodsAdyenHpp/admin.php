<?php
/* @var $this AbaPaymentMethodsAdyenHppController */
/* @var $model AbaAdyenHppPaymentMethods */

$this->breadcrumbs = array(
  'Adyen Payment Methods',
);

$this->menu = array(
  array('label' => 'List Adyen Hpp Payment Methods', 'url' => array('index')),
  array('label' => 'Create Adyen Hpp Payment Methods', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('aba-adyen-payment-methods-hpp-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Adyen Hpp Payment Methods</h3>

<?php echo CHtml::link('Create new adyen hpp product price', array('AbaPaymentMethodsAdyenHpp/create')); ?><br/>

<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
      'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('application.components.widgets.CGridViewABA', array(
  'id' => 'aba-adyen-payment-methods-hpp-grid',
  'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
  'pager' => array(
    'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
    'prevPageLabel' => '<',
    'nextPageLabel' => '>',
    'firstPageLabel' => '<<',
    'lastPageLabel' => '>>',
  ),
  'extraInfo' => array('view', 'update', 'delete'),
  'dataProvider' => $model->search(),
  'filter' => $model,
  'filterSelector' => '{filter}',
  'columns' => array(
    'idPaymentMethod',
    'paymentMethod',
    'enabled' =>
      array(
        'name' => 'enabled',
        'type' => 'booleanList',
        'filter' => HeList::booleanList()
      ),
    array(
      'class' => 'CButtonColumnABA',
      'htmlOptions' => array('style' => 'width: 110px;'),
      'header' => 'Options',
    ),
  ),
)); ?>
