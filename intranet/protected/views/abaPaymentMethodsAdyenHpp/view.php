<?php
/* @var $this AbaPaymentMethodsAdyenHppController */
/* @var $model AbaAdyenHppPaymentMethods */

$this->breadcrumbs = array(
  'Adyen Hpp Payment Methods' => array('admin'),
  $model->idPaymentMethod,
);

$this->menu = array(
  array('label' => 'Update Adyen Hpp Payment Method', 'url' => array('update', 'id' => $model->idPaymentMethod)),
  array('label' => 'Manage Adyen Hpp Payment Method', 'url' => array('admin')),
);

?>
<h3>View method: <?php echo $model->paymentMethod; ?></h3>

<?php $this->widget('application.components.widgets.CDetailViewABA', array(
  'data' => $model,
  'attributes' => array(
    'idPaymentMethod',
    'paymentMethod',
    'enabled' =>
      array(
        'name' => 'enabled',
        'type' => 'booleanList',
        'filter' => HeList::booleanList()
      ),
  ),
)); ?>
