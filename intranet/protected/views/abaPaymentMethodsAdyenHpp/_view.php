<?php
/* @var $this AbaPaymentMethodsAdyenHppController */
/* @var $data AbaAdyenHppPaymentMethods */
?>

<div class="view">
    <b><?php echo CHtml::encode($data->getAttributeLabel('idPaymentMethod')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->idPaymentMethod), array('view', 'id' => $data->idPaymentMethod)); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('paymentMethod')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->paymentMethod), array('view', 'id' => $data->paymentMethod)); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('enabled')); ?>:</b>
    <?php echo CHtml::encode($data->enabled); ?>
    <br/>
</div>