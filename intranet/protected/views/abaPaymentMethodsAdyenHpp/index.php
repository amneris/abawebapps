<?php
/* @var $this AbaPaymentMethodsAdyenHppController */
/* @var $dataProvider AbaAdyenHppPaymentMethods */

$this->breadcrumbs=array(
	'Adyen Hpp Payment Methods',
);

$this->menu=array(
	array('label'=>'Create Adyen Hpp Payment Methods', 'url'=>array('create')),
	array('label'=>'Manage Adyen Hpp Payment Methods', 'url'=>array('admin')),
);
?>

<h3>Adyen Hpp Payment Methods</h3>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
