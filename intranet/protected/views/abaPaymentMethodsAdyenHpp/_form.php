<?php
/* @var $this AbaPaymentMethodsAdyenHppController */
/* @var $model AbaAdyenHppPaymentMethods */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'aba-adyen-hpp-payment-methods-form',
	'enableAjaxValidation' => false,
)); ?>

<?php
$options = HeList::getYesNoAliasBool();
?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <table id="myForm">

        <?php if (is_numeric($model->idPaymentMethod)): ?>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'idPaymentMethod'); ?>
            </td>
            <td>
                <?php echo CHtml::encode($model->idPaymentMethod); ?>
                <?php echo $form->error($model ,'idPaymentMethod'); ?>
            </td>
        </tr>
        <?php endif; ?>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'paymentMethod'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'paymentMethod',array('size' => 20, 'maxlength' => 45)); ?>
                <?php echo $form->error($model ,'paymentMethod'); ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model, 'enabled'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'enabled', $options); ?>
                <?php echo $form->error($model, 'enabled'); ?>
            </td>
        </tr>
    </table>

	<div class="row buttons" align="center">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->