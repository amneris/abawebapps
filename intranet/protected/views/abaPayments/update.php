<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */

$this->breadcrumbs=array(
	'Aba Payments'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
//	array('label'=>'List AbaPayments', 'url'=>array('index')),
//	array('label'=>'Create AbaPayments', 'url'=>array('create')),
//	array('label'=>'View AbaPayments', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AbaPayments', 'url'=>array('admin')),
);
?>

<h3>Update AbaPayments <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>