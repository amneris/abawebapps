<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */

$this->renderPartial("_formheader", array(
  "model" => $model,
  "actionTitle" => "View full details"
));

$this->renderPartial("_fulldetails", array(
    "model" => $model,
    "modelNotifications" => (isset($modelNotifications) ? $modelNotifications : false)
  )
);
