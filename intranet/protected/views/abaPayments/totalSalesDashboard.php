<?php
/* @var $this AbaPaymentsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Sales dashboard',
);
?>

<h3>Sales dashboard</h3>

<?php /* echo CHtml::link('Sales dashboard by country',array('AbaPayments/totalSalesDashboardByCountry')); */ ?>
<?php
$dollarToday = file_get_contents('http://currencies.apps.grandtrunk.net/getlatest/USD/EUR');

//function just to make syntax more easy
function toMoney($number)
{
    $money = number_format($number,2,',','.');
    return $money;
}

?>

<div style="text-align: center; font-size: 15px">
    Sales and renovations for period from <b><?php echo $startDate ?></b> to <b><?php echo $endDate ?></b><br><br>
    <?php
        echo CHtml::form(Yii::app()->createUrl('AbaPayments/totalSalesDashboard'), 'post', array('id'=>'form', 'class'=>'myForm'));
        echo "range from ";
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'startDate',
            'value' => $startDate,
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth'=>true,
                'changeYear'=>true,
            ),
                'htmlOptions' => array(
                'size' => '10',
                'maxlength' => '10',
            ),
        ));
        echo " to ";
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'endDate',
            'value' => $endDate,
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth'=>true,
                'changeYear'=>true,
            ),
            'htmlOptions' => array(
                'size' => '10',
                'maxlength' => '10',
            ),
        ));

        echo CHtml::button("Search",array('onclick' => 'this.form.submit();','title'=>"Search",'id'=>'searchButton'));
        echo CHtml::endForm();
    ?>
</div>
<br><br>

<table id="salesTable">
    <tr>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            Type of Sale
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            Currency
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Average Ticket
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Quantity
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Amount
        </td>
    </tr>

    <tr>
        <td>
            New sale
        </td>
        <td style="text-align: center">
            US Dollar
        </td>
        <td style="text-align: center">
            <? echo toMoney(($total['totalUSD']* $dollarToday) /$total['countUSD'])?>€
        </td>
        <td style="text-align: right;">
            <? echo $total['countUSD'] ?>
        </td>
        <td style="text-align: right;">
            <span style="font-size: 11px">*(<? echo toMoney($total['totalUSD'])."$)"; ?></span>
            <? echo toMoney($total['totalUSD']* $dollarToday)?>€
        </td>
    </tr>

    <tr>
        <td>
            Renovation
        </td>
        <td style="text-align: center">
            US Dollar
        </td>
        <td style="text-align: center">
            <? echo toMoney(($total['totalRenovationUSD'] * $dollarToday) / $total['countRenovationUSD'])?>€
        </td>
        <td style="text-align: right;">
            <? echo $total['countRenovationUSD'] ?>
        </td>
        <td style="text-align: right;">
            <span style="font-size: 11px">*(<? echo toMoney($total['totalRenovationUSD'])."$)" ?></span>
            <? echo toMoney($total['totalRenovationUSD'] * $dollarToday)?>€
        </td>
    </tr>

    <tr>
        <td>
            New sale
        </td>
        <td style="text-align: center">
            Euro
        </td>
        <td style="text-align: center">
            <? echo toMoney($total['totalEUR'] / $total['countEUR'])."€" ?>
        </td>
        <td style="text-align: right;">
            <? echo $total['countEUR'] ?>
        </td>
        <td style="text-align: right;">
            <? echo toMoney($total['totalEUR'])."€" ?>
        </td>
    </tr>

    <tr>
        <td>
            Renovation
        </td>
        <td style="text-align: center">
            Euro
        </td>
        <td style="text-align: center">
            <? echo toMoney($total['totalRenovationEUR'] / $total['countRenovationEUR'])."€" ?>
        </td>
        <td style="text-align: right;">
            <? echo $total['countRenovationEUR'] ?>
        </td>
        <td style="text-align: right;">
            <? echo toMoney($total['totalRenovationEUR'])."€" ?>
        </td>
    </tr>

    <tr>
        <td style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px" colspan="2">
            Total
        </td>
        <td style="background-color: greenyellow; text-align: center; font-weight: bold; font-size: 15px">
            <? echo toMoney(($total['totalUSD'] * $dollarToday + $total['totalRenovationUSD'] * $dollarToday + $total['totalEUR'] + $total['totalRenovationEUR']) / ($total['countRenovationUSD'] + $total['countRenovationEUR'] + $total['countUSD'] + $total['countEUR'])) ?>€
        </td>
        <td style="background-color: greenyellow; text-align: right; font-weight: bold; font-size: 15px">
            <? echo $total['countRenovationUSD'] + $total['countRenovationEUR'] + $total['countUSD'] + $total['countEUR']?>
        </td>
        <td style="background-color: greenyellow; text-align: right; font-weight: bold; font-size: 15px">
            <? echo toMoney($total['totalUSD'] * $dollarToday + $total['totalRenovationUSD'] * $dollarToday + $total['totalEUR'] + $total['totalRenovationEUR']) ?>€
        </td>
    </tr>
</table>

<table id="salesTable">

    <tr>
        <td rowspan="5" style="width: 400px; border: 0">
            <?php

            $this->widget('ext.Hzl.google.HzlVisualizationChart', array('visualization' => 'PieChart',
                'data' => array(
                    array('Concept', 'Amount'),
                    array('New sales '.toMoney($total['totalUSD'] * $dollarToday + $total['totalEUR']).'€',$total['countUSD']+$total['countEUR']),
                    array('Renovation '.toMoney($total['totalRenovationUSD'] * $dollarToday + $total['totalRenovationEUR']).'€', $total['countRenovationUSD']+$total['countRenovationEUR']),
                    array('Refund '.toMoney($total['totalRefundEUR'] + $total['totalRefundUSD'] * $dollarToday).'€',$total['contRefundUSD']+ $total['contRefundEUR']),
                    array('Failed '.toMoney($total['totalFailedUSD'] * $dollarToday + $total['totalFailedEUR']).'€', $total['countFailedUSD'] + $total['countFailedEUR']),
                    array('Canceled '.toMoney($total['totalCanceledUSD'] * $dollarToday + $total['totalCanceledEUR']).'€', $total['countCanceledUSD'] + $total['countCanceledEUR']),

                ),
                'options' => array(
                    //'title' => 'New sales, renovations & refunds',
                    'width'=>400,
                    'height'=>180,
                    //'legend'=>'none',
                    'backgroundColor'=>'#fcfcfc',
                    'colors'=> array('#3366cc', '#109618', '#a60000', '#ff9900', '#dc3912'),
                    'chartArea'=>array('width'=>'100%', 'height'=>'90%'),
                    'legend'=> array('position'=> 'right'),
                    //'is3D'=>true,
                    'fontSize'=>11,
                )
            ));
            ?>
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Type of Refund
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Quantity
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Amount
        </td>
    </tr>

    <tr>
        <td>
            Refund in US Dollar
        </td>
        <td style="text-align: right;">
            <? echo $total['contRefundUSD'] ?>
        </td>
        <td style="text-align: right;">
            <span style="font-size: 11px">*(<? echo toMoney($total['totalRefundUSD'])."$)"  ?></span>
            <? echo toMoney($total['totalRefundUSD'] * $dollarToday)?>€
        </td>
    </tr>


    <tr>
        <td>
            Refund in Euro
        </td>
        <td style="text-align: right;">
            <? echo $total['contRefundEUR'] ?>
        </td>
        <td style="text-align: right;">
            <? echo toMoney($total['totalRefundEUR'])."€" ?>
        </td>
    </tr>

    <tr>
        <td style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px">
            Total Refund
        </td>
        <td style="background-color: #ff0000; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <? echo $total['contRefundUSD']+ $total['contRefundEUR'] ?>
        </td>
        <td style="background-color: #ff0000; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <? echo toMoney($total['totalRefundEUR'] + $total['totalRefundUSD'] * $dollarToday)."€" ?>
        </td>
    </tr>

    <tr>
        <td style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px" colspan="2">
            Total Net
        </td>

        <td style="background-color: green; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <? echo toMoney($total['totalUSD'] * $dollarToday + $total['totalRenovationUSD'] * $dollarToday + $total['totalEUR'] + $total['totalRenovationEUR'] - ($total['totalRefundEUR'] + $total['totalRefundUSD'] * $dollarToday)) ?>€
        </td>

    </tr>

</table>

<div style="text-align: right">
    <?php
        echo "<span style=\"font-size:10px;\">* 1$ today is about ".$dollarToday."€</span>";
    ?>
</div>







