<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */
/* @var $form CActiveForm */
?>
<?php
$isValidPaymentForCancel = true;
if ($model->paySuppExtId == PAY_SUPPLIER_Z OR $model->paySuppExtId == PAY_SUPPLIER_PAYPAL OR $model->paySuppExtId == PAY_SUPPLIER_APP_STORE OR $model->paySuppExtId == PAY_SUPPLIER_ANDROID_PLAY_STORE) {
    $isValidPaymentForCancel = false;
}
?>
<div class="form">
    <script language="JavaScript">
        function sendCancelForm() {
            if ($("#AbaPayments_dateEndTransaction").value == '0000-00-00 00:00:00') {
                return alert('Field data end transaction should have a valid value');
            } else {

                if ($("#AbaPayments_paySuppExtId").val() != '<?php echo PAY_SUPPLIER_PAYPAL; ?>' ||
                  confirm("Are you sure you want to cancel this Paypal recurring profile? " +
                    "In that case please review in www.paypal.com that this recurring profile is also cancelled, otherwise Paypal " +
                    "will charge the user even it is cancelled in our system.")) {
                    $("#aba-cancel-form").submit();
                    return true;
                }
            }

            return false;
        }
    </script>
    <?php $form = $this->beginWidget('CActiveForm', array(
      'id' => 'aba-cancel-form',
      'enableAjaxValidation' => false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php if($model->paySuppExtId == PAY_SUPPLIER_PAYPAL) {  $idRecurring = $model->paySuppExtProfId; ?>
        <div class="row errorRow">
            <a target='_blank' href='https://www.paypal.com/es/cgi-bin/webscr?cmd=_profile-recurring-payments&encrypted_profile_id=<?php echo $idRecurring ?>'
               class="btnCancel">
                PAYPAL CANCELLATION RENEWAL
            </a>
            <p class="cancel">*In order to cancel the renewal Paypal login is required</p>
        </div>
    <?php } ?>


    <?php if (Yii::app()->user->hasFlash('mustHaveAction')) { ?>

        <div class="flash-error">
            <?php echo Yii::app()->user->getFlash('mustHaveAction'); ?>
        </div>

    <?php } ?>

    <div class="row">
        <?php
        if ($isValidPaymentForCancel) {
            $datos = HeList::actionsPayments();
            echo $form->labelEx($model, 'action');
            echo $form->DropDownList($model, 'action', $datos, array('empty' => 'Select option...'));
            echo $form->error($model, 'action');
        }
        ?>

    </div>
    <div class="row">

        <?php
        if ($isValidPaymentForCancel) {
            echo $form->labelEx($model, 'dateEndTransaction');
            if ($model->dateEndTransaction == '0000-00-00 00:00:00') {
                $model->dateEndTransaction = HeDate::todaySQL(true);
            }
            echo $form->textField($model, 'dateEndTransaction');
            echo CHtml::image("images/calendar.jpg", "calendar", array("id" => "c_button", "class" => "pointer"));
            $this->widget('application.extensions.calendar.SCalendar',
              array(
                'inputField' => 'AbaPayments_dateEndTransaction',
                'button' => 'c_button',
                'ifFormat' => '%Y-%m-%d',
              ));
            echo $form->error($model, 'dateEndTransaction');
        }
        ?>
    </div>
    <?php
    echo $form->hiddenField($model, 'amountPrice');
    echo $form->hiddenField($model, 'id');
    echo $form->hiddenField($model, 'userId');
    echo $form->hiddenField($model, 'idProduct');
    echo $form->hiddenField($model, 'idCountry');
    echo $form->hiddenField($model, 'idPeriodPay');
    echo $form->hiddenField($model, 'idUserCreditForm');
    echo $form->hiddenField($model, 'paySuppExtId');
    echo $form->hiddenField($model, 'paySuppOrderId');

    echo $form->hiddenField($model, 'status');
    echo $form->hiddenField($model, 'dateToPay');
    echo $form->hiddenField($model, 'dateStartTransaction');
    echo $form->hiddenField($model, 'xRateToEUR');
    echo $form->hiddenField($model, 'xRateToUSD');
    echo $form->hiddenField($model, 'currencyTrans');
    echo $form->hiddenField($model, 'foreignCurrencyTrans');
    echo $form->hiddenField($model, 'idPromoCode');
    echo $form->hiddenField($model, 'amountOriginal');
    echo $form->hiddenField($model, 'amountDiscount');
    echo $form->hiddenField($model, 'idPayControlCheck');
    echo $form->hiddenField($model, 'idPartner');

    echo $form->hiddenField($model, 'taxRateValue');
    echo $form->hiddenField($model, 'amountTax');
    echo $form->hiddenField($model, 'amountPriceWithoutTax');
    ?>

    <div class="row buttons">
        <?php

        switch ($model->paySuppExtId) {
            case PAY_SUPPLIER_ADYEN:
            case PAY_SUPPLIER_ADYEN_HPP:
                echo '&nbsp;' . CHtml::checkBox("sendToCancelGateway", true,
                    array('style' => 'width: 20px; height: 20px;'));
                echo 'If this checkbox is marked then the CANCEL will be sent automatically to ADYEN. <br />';
                break;
            default:
                break;
        }

        if ($isValidPaymentForCancel) {
            echo CHtml::button('SAVE', array('onclick' => 'sendCancelForm()'));
        }

        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->