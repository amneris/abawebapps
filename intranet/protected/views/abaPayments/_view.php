<?php
/* @var $this AbaPaymentsController */
/* @var $data AbaPayments */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php /*echo CHtml::encode($data->getAttributeLabel('idUserProdStamp'));*/ ?>:</b>
	<?php /*echo CHtml::encode($data->idUserProdStamp);*/ ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userId')); ?>:</b>
	<?php echo CHtml::encode($data->userId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idProduct')); ?>:</b>
	<?php echo CHtml::encode($data->idProduct); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCountry')); ?>:</b>
	<?php echo CHtml::encode($data->idCountry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPeriodPay')); ?>:</b>
	<?php echo CHtml::encode($data->idPeriodPay); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idUserCreditForm')); ?>:</b>
	<?php echo CHtml::encode($data->idUserCreditForm); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('paySuppExtId')); ?>:</b>
	<?php echo CHtml::encode($data->paySuppExtId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paySuppOrderId')); ?>:</b>
	<?php echo CHtml::encode($data->paySuppOrderId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dateStartTransaction')); ?>:</b>
	<?php echo CHtml::encode($data->dateStartTransaction); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dateEndTransaction')); ?>:</b>
	<?php echo CHtml::encode($data->dateEndTransaction); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dateToPay')); ?>:</b>
	<?php echo CHtml::encode($data->dateToPay); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xRateToEUR')); ?>:</b>
	<?php echo CHtml::encode($data->xRateToEUR); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xRateToUSD')); ?>:</b>
	<?php echo CHtml::encode($data->xRateToUSD); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('currencyTrans')); ?>:</b>
	<?php echo CHtml::encode($data->currencyTrans); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('foreignCurrencyTrans')); ?>:</b>
	<?php echo CHtml::encode($data->foreignCurrencyTrans); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPromoCode')); ?>:</b>
	<?php echo CHtml::encode($data->idPromoCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('amountOriginal')); ?>:</b>
	<?php echo CHtml::encode($data->amountOriginal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('amountDiscount')); ?>:</b>
	<?php echo CHtml::encode($data->amountDiscount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('amountPrice')); ?>:</b>
	<?php echo CHtml::encode($data->amountPrice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPayControlCheck')); ?>:</b>
	<?php echo CHtml::encode($data->idPayControlCheck); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPartner')); ?>:</b>
	<?php echo CHtml::encode($data->idPartner); ?>
	<br />

	*/ ?>

</div>