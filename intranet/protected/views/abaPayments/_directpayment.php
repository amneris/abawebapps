<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aba-refund-form',
	'enableAjaxValidation'=>false,
));
$aMethods = HeList::supplierExtGatewayList();
if ($model->paySuppExtId==PAY_SUPPLIER_CAIXA){
?>


<p class="highlight">ATTENTION: This transaction will be linked to <?php echo $aMethods[$model->paySuppExtId]; ?> PLATFORM, which means it will execute a REAL REFUND to the USER.
    There is no need you access the <?php echo $aMethods[$model->paySuppExtId]; ?> platform (web site) to execute this REFUND.
    <?php if($model->paySuppExtId == PAY_SUPPLIER_ADYEN OR $model->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP): ?>
        <br /><br /><strong style="color: #cc0000;">* The system is going to communicate with the payment gateway. The Payments will not be created in AbA systems until the gateway returns a positive response.
        <br />A button to check for this will be available after submitting. When Adyen returns a positive response the refund will be created in our system.
        <br />We recommend that you retry after 3-4 minutes.</strong>
    <?php endif; ?>
</p>

<?php
} else  {
    ?>
    <p class="highlight">This transaction is linked to <?php echo $aMethods[$model->paySuppExtId]; ?>.
        Remember you should have done previously the transaction in the gateway payment platform</p>
<?php
}
?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php 
			$model->amountPrice=sprintf('%0.2f', $model->amountPrice);
			echo $form->labelEx($model,'amountPrice'); 
			echo $form->textField($model,'amountPrice',array('size'=>18,'maxlength'=>18)); 
			echo $form->error($model,'amountPrice');
    ?>
	</div>
	<div class="row">
		<?php 
			echo $form->labelEx($model,'dateToPay'); 
			echo $form->textField($model,'dateToPay'); 
			echo CHtml::image("images/calendar.jpg","calendar", array("id"=>"c_button","class"=>"pointer")); 
			$this->widget('application.extensions.calendar.SCalendar',
			array(
			'inputField'=>'AbaPayments_dateToPay',
			'button'=>'c_button',
			'ifFormat'=>'%Y-%m-%d',
			));
			echo $form->error($model,'dateToPay'); ?>
	</div>
	<?php 
		echo $form->hiddenField($model,'id'); 
		//echo $form->hiddenField($model,'idUserProdStamp'); 
		echo $form->hiddenField($model,'userId'); 
		echo $form->hiddenField($model,'idProduct'); 
		echo $form->hiddenField($model,'idCountry');
		echo $form->hiddenField($model,'idPeriodPay');
		echo $form->hiddenField($model,'idUserCreditForm'); 
		echo $form->hiddenField($model,'paySuppExtId'); 
	
		echo $form->hiddenField($model,'status'); 
		echo $form->hiddenField($model,'dateEndTransaction'); 
		echo $form->hiddenField($model,'dateStartTransaction'); 
		echo $form->hiddenField($model,'xRateToEUR'); 
		echo $form->hiddenField($model,'xRateToUSD'); 
		echo $form->hiddenField($model,'currencyTrans'); 
		echo $form->hiddenField($model,'foreignCurrencyTrans'); 
		echo $form->hiddenField($model,'idPromoCode'); 
		echo $form->hiddenField($model,'amountOriginal'); 
		echo $form->hiddenField($model,'amountDiscount'); 
		echo $form->hiddenField($model,'idPayControlCheck'); 
		echo $form->hiddenField($model,'idPartner');

		echo $form->hiddenField($model,'taxRateValue');
		echo $form->hiddenField($model,'amountTax');
		echo $form->hiddenField($model,'amountPriceWithoutTax');
	?>
	<div class="row buttons">
		<?php if ($model->paySuppExtId == PAY_SUPPLIER_CAIXA){
            echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save');
        } ?>
        <?php echo CHtml::button('Cancel',
                    array('onClick'=>'window.location="'.Yii::app()->createUrl('abaPayments/admin_id',array('id'=>$model->userId)).'";')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->