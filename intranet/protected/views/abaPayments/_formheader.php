<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */
/* @var $actionTitle string */

$this->breadcrumbs = array(
  'User' => array('abaUser/admin_id', 'id' => (isset($model->userId) ? $model->userId : $userId)),
  'Paylines from user ' . (isset($model->userId) ? $model->userId : $userId) => array(
    'abaPayments/admin_id',
    'id' => (isset($model->userId) ? $model->userId : $userId)
  ),
  $actionTitle,
);
?>

<h3>
    <?php if (isset($model->id)): ?>
        PAYMENT WITH IDENTIFIER <?php echo (isset($model->id) ? $model->id : '') . " : " . strtoupper($actionTitle); ?>
    <?php else: ?>
        PENDING PAYMENT DOES NOT EXIST FOR THIS USER
    <?php endif; ?>
</h3>
