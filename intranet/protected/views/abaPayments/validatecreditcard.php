<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaValidateCreditCardForm */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
      'id' => 'aba-validatecreditcard-form',
      'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php if($model->cardNumberType != ''): ?>
      <div style="border: 2px solid #298A08; padding: 10px; margin: 0 0 20px 0; background: #E0F8E0; font-size: 18px; border-radius: 10px; width: 300px;">
      <?php echo $model->cardNumberType; ?>
      </div>
    <?php endif; ?>

    <div class="row">
        <?php
        echo $form->labelEx($model, 'cardNumber');
        echo $form->textField($model, 'cardNumber', array('size' => 50, 'maxlength' => 30));
        echo $form->error($model, 'cardNumber');
        ?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton('Validate'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->