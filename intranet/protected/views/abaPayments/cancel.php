<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */

$this->renderPartial("_formheader",array(
    "model"=>$model,
    "actionTitle"=>"Mark payment as a CANCEL or FAILURE", 'userId' => $userId));

if(isset($model->id)) {
		$this->renderPartial('_formcancel', array('model'=>$model));
		$this->renderPartial( '_fulldetails', array("model"=>$model));
}


