<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */

$this->breadcrumbs=array(
	'Payments'=>array('admin'),
	'Excel with taxes export',
);

$this->menu=array(
	array('label'=>'List AbaPayments', 'url'=>array('index')),
	array('label'=>'Manage AbaPayments', 'url'=>array('admin')),
);
?>

<h3>Export invoices to excel</h3>

<?php echo $this->renderPartial('_formexcelinvoices', array('model'=>$model, 'extrainfo'=>$extrainfo)); ?>