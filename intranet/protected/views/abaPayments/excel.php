<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */

$this->breadcrumbs=array(
	'Payments'=>array('admin'),
	'Excel export',
);

$this->menu=array(
	array('label'=>'List AbaPayments', 'url'=>array('index')),
	array('label'=>'Manage AbaPayments', 'url'=>array('admin')),
);
?>

<h3>Export payments to excel</h3>

<?php echo $this->renderPartial('_formexcel', array('model'=>$model, 'extrainfo'=>$extrainfo)); ?>