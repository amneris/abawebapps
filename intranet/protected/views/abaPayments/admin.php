<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */
/* @var $email string */

$this->breadcrumbs = array('Paylines search'/*=>array('/AbaPayments/admin')*/);

Yii::app()->clientScript->registerScript(
  'search',
  "
   $('.search-button').click(function(){
     $('.search-form').toggle();
     return false;
   });
   $('.search-form form').submit(function(){
     $.fn.yiiGridView.update('aba-payments-grid', {
       data: $(this).serialize()
     });
     return false;
   });
   "
);
?>

<?php if (isset($msgAdyenHpp) AND trim($msgAdyenHpp) != ''): ?>
    <p class="highlight" style="padding: 10px;">ATTENTION: <strong style="color: #cc0000;">* Ya existe un intento de
            REFUND: <?php echo $msgAdyenHpp; ?></strong>
    </p>
<?php endif; ?>

<h3>Paylines
    <?php if (isset($email)) {
        echo ' of user: ' . $email;
    } ?>
</h3>

<?php
echo CHtml::link('Export to excel', array('abaPayments/export'));
echo "<br>";
echo CHtml::link('Export invoices', array('abaPayments/exportinvoices'));
echo "<br>";
echo CHtml::link('Advanced Search', '#', array('class' => 'search-button'));
?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
      'model' => $model,
    )); ?>
</div><!-- search-form -->
<?php $this->widget('application.components.widgets.CGridViewABA', array(
  'id' => 'aba-payments-grid',
  'rowIdExpression' => '"trPay-{$data->id}-{$data->idPayControlCheck}"',
  'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
  'pager' => array(
    'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
    'prevPageLabel' => '<',
    'nextPageLabel' => '>',
    'firstPageLabel' => '<<',
    'lastPageLabel' => '>>',
  ),
  'extraInfo' => array('view'),
  'extraButtons' => array(
    'extraButton1' => array(
        //	'iconExtraLink'=>'user.png',
      'textExtraLink' => 'User',
      'classExtraLink' => 'payments',
      'urlExtraLink' => '"abaUser/admin_id",array("id"=>$data->userId)'
    ),
    'extraButton2' => array(
      'textExtraLink' => 'Credit_Card',
      'classExtraLink' => 'user',
      'fnValidate' => 'HeQuery::checkPaymentHasCreditCard($data->userId, $data->id)',
      'urlExtraLink' => '"abaUserCreditForms/admin_id",array("id"=>$data->userId)'
    ),
    'extraButton3' => array(
      'textExtraLink' => 'Refund',
      'classExtraLink' => 'user',
      'fnValidate' => 'HeQuery::testRegisterSuccess($data->userId, $data->id)',
      'urlExtraLink' => '"abaPayments/refund",array("id"=>$data->userId, "payment"=>$data->id)'
    ),
    'extraButton4' => array(
      'textExtraLink' => 'Reconcile',
      'classExtraLink' => 'user',
      'fnValidate' => 'HeQuery::checkPaymentCanReconcile($data->userId, $data->id)',
      'urlExtraLink' => '"abaPayments/markReconcile",array("userId"=>$data->userId, "idPayment"=>$data->id)'
    ),
    'extraButton5' => array(
      'textExtraLink' => 'Change_Card',
      'classExtraLink' => 'user',
      'fnValidate' => 'HeQuery::checkPaymentCanChangeCredit($data->userId, $data->id)',
      'urlExtraLink' => '"abaUserCreditForms/create", array("idPayPending"=>$data->id)'
    ),
    'extraButton6' => array(
      'textExtraLink' => 'Direct Payment',
      'classExtraLink' => 'user',
      'fnValidate' => 'HeQuery::checkForDirectPayment($data->paySuppExtId, $data->status)',
      'urlExtraLink' => '"abaPayments/directPayment",array("userId"=>$data->userId, "idPayment"=>$data->id)'
    ),
  ),
  'dataProvider' => $model->search(),
  'filter' => $model,
  'filterSelector' => '{filter}',
  'columns' => array(
    'userId' =>
      array('name' => 'userId'),
    'id' => array(
      'name' => 'id',
      'htmlOptions' => array('width' => '100px'),
    ),
    'idProduct',
    'idCountry' =>
      array(
        'name' => 'idCountry',
        'type' => 'countryList',
        'filter' => HeList::getCountryList(),
        'htmlOptions' => array('width' => "120px")
      ),
    'paySuppExtId' =>
      array(
        'name' => 'paySuppExtId',
        'type' => 'supplierExtGatewayList',
        'filter' => HeList::supplierExtGatewayList(),
      ),
    'status' =>
      array(
        'name' => 'status',
        'type' => 'statusDescription',
        'filter' => HeList::statusDescription(),
      ),
    'dateStartTransaction',
    'dateEndTransaction',
    'dateToPay',
    'currencyTrans',
    'amountOriginal' =>
      array(
        'name' => 'amountOriginal',
        'type' => '{number_format("*amountOriginal*", 2);'
      ),
    'amountPrice' =>
      array(
        'name' => 'amountPrice',
        'type' => '{number_format("*amountPrice*", 2);'
      ),
    'idUserCreditForm' =>
      array('name' => 'idUserCreditForm', 'header' => 'Subscription Id'),

//    'paySuppExtProfId' => array(
//      'name' => 'paySuppExtProfId',
////      'type' => 'html',
//      'value' => 'HeList::getReference($data)',),
//
    'isRecurring' => array(
      'name' => 'isRecurring',
      'type' => 'html',
      'filter' => array('1' => 'Yes', '0' => 'No'),
      'value' => 'CHtml::tag("img", array(
          "title" => "Is a recurring, initial, extended or direct payment",
          "style" => HeList::getPaymentIconCss($data),
          "src"=> HeList::getPaymentIcon($data),
          )
      )',
      'htmlOptions' => array('width' => "45px")
    ),
    array(
      'class' => 'CButtonColumnABA',
      'header' => 'Options'
    ),
  ),
)); ?>
