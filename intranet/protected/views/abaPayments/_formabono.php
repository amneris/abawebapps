<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
      'id' => 'aba-refund-form',
      'enableAjaxValidation' => false,
    ));
    $aMethods = HeList::supplierExtGatewayList();
    if ($model->paySuppExtId != PAY_SUPPLIER_GROUPON &&
      $model->paySuppExtId != PAY_SUPPLIER_PAYPAL &&
      $model->paySuppExtId != PAY_SUPPLIER_B2B &&
      $model->paySuppExtId != PAY_SUPPLIER_Z
    ) {
        ?>

        <?php if (HeQuery::isParcialSuccess($model->id)): ?>
            <?php if ($model->paySuppExtId == PAY_SUPPLIER_ADYEN OR $model->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP): ?>
                <p class="highlight">ATTENTION: This transaction will be linked
                    to <?php echo $aMethods[$model->paySuppExtId]; ?> PLATFORM, which means it will execute a REAL
                    REFUND to the USER.
                    There is no need you access the <?php echo $aMethods[$model->paySuppExtId]; ?> platform (web site)
                    to execute this REFUND.
                    <br/><br/><strong style="color: #cc0000;">* The system is going to communicate with the payment
                        gateway. The Payments will not be created in AbA systems until the gateway returns a positive
                        response.
                        <br/>A button to check for this will be available after submitting. When Adyen returns a
                        positive response the refund will be created in our system.
                        <br/>We recommend that you retry after 3-4 minutes.</strong>
                </p>
            <?php else: ?>
                <div class="flash-error">ATTENTION: This transaction won't be linked
                    to <?php echo $aMethods[$model->paySuppExtId]; ?> PLATFORM, which means it won't execute a REAL
                    REFUND to the USER.
                    There is no need you access the <?php echo $aMethods[$model->paySuppExtId]; ?> platform (web site)
                    to execute this REFUND.
                </div>
            <?php endif; ?>
        <?php else: ?>
            <p class="highlight">ATTENTION: This transaction will be linked
                to <?php echo $aMethods[$model->paySuppExtId]; ?> PLATFORM, which means it will execute a REAL REFUND to
                the USER.
                There is no need you access the <?php echo $aMethods[$model->paySuppExtId]; ?> platform (web site) to
                execute this REFUND.
                <?php if ($model->paySuppExtId == PAY_SUPPLIER_ADYEN OR $model->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP): ?>
                    <br/><br/><strong style="color: #cc0000;">* The system is going to communicate with the payment
                        gateway. The Payments will not be created in AbA systems until the gateway returns a positive
                        response.
                        <br/>A button to check for this will be available after submitting. When Adyen returns a
                        positive response the refund will be created in our system.
                        <br/>We recommend that you retry after 3-4 minutes.</strong>
                <?php endif; ?>
            </p>
        <?php endif; ?>

        <?php
    } else {
        if ($model->paySuppExtId == PAY_SUPPLIER_GROUPON ||
          $model->paySuppExtId == PAY_SUPPLIER_B2B
        ) {
            ?>
            <p class="highlight">This transaction is linked to <?php echo $aMethods[$model->paySuppExtId]; ?> therefore
                the option to make a refund is disabled because we are not able to return any money.</p>
            <?php
        } else {
            ?>
            <p class="highlight">This transaction is linked to <?php echo $aMethods[$model->paySuppExtId]; ?>.
                Remember you should have done previously the transaction in the gateway payment platform</p>
            <?php
        }
    }
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <div class="container_refund">
        <?php if ($model->paySuppExtId == PAY_SUPPLIER_PAYPAL) { ?>
            <div class="row">
                <label class="refund">Transaction ID</label>
                <?php
                if (isset($model->paySuppExtUniId) && $model->paySuppExtUniId != "") {
                    echo $model->paySuppExtUniId;
                } else {
                    echo "NOT TRANSACTION ID";
                }
                ?>
            </div>
            <div class="row">
                <label class="refund">Refund Paypal</label>
                <a class="btnRefund" target='_blank' href="https://history.paypal.com/es/cgi-bin/webscr?cmd=_history">Search
                    Paypal Transaction</a>

                <p class="refund">*In order to cancel the renewal Paypal login is required</p>
            </div>
        <?php } ?>

        <?php echo $form->errorSummary($model); ?>
        <div class="row">
            <?php
            if ($model->paySuppExtId == PAY_SUPPLIER_PAYPAL) {
                echo $form->hiddenField($model, 'paySuppOrderId');
            } else {
                echo $form->labelEx($model, 'paySuppOrderId Original');
                echo CHtml::textField("readPaySuppOrderId", $model->paySuppOrderId,
                  array('size' => 60, 'disabled' => 'disabled', 'class' => 'readOnly'));
            }
            ?>
        </div>
        <div class="row">
            <?php
            if($model->paySuppExtId == PAY_SUPPLIER_PAYPAL){
                echo $form->hiddenField($model,'paySuppOrderId');
            } else {
                echo $form->labelEx($model,'paySuppOrderId'); ?>
                ( Identifier aimed to find this payment in the remote gateway.
                In AllPago should be different from the original one. In La Caixa should be the same, otherwise it will be rejected.)
                </br>
                <?php echo $form->textField($model,'paySuppOrderId', array('size'=>60,'maxlength'=>60)); ?>
                <?php echo $form->error($model,'paySuppOrderId'); ?>
        <?php } ?>
        </div>
        <div class="row">
            <?php
            $model->amountPrice = sprintf('%0.2f', $model->amountPrice);
            echo $form->labelEx($model, 'amountPrice');
            echo $form->textField($model, 'amountPrice', array('size' => 18, 'maxlength' => 18));
            echo $form->error($model, 'amountPrice');
            switch ($model->paySuppExtId) {
                case PAY_SUPPLIER_ALLPAGO_BR:
                case PAY_SUPPLIER_ALLPAGO_MX:
                    echo '&nbsp;' . CHtml::checkBox("sendToPayGateway", true,
                        array('style' => 'width: 20px; height: 20px;'));
                    echo 'If this checkbox is marked then the REFUND will be sent automatically to ALLPAGO (Only it should be disabled in case of REJECTIONS FROM THE BANK, not via ALLPAGO).';
                    break;
                case PAY_SUPPLIER_CAIXA:
                    echo '&nbsp;' . CHtml::checkBox("sendToPayGateway", true,
                        array('style' => 'width: 20px; height: 20px;'));
                    echo 'If this checkbox is marked then the REFUND will be sent automatically to LA CAIXA (Only it should be disabled in case of REJECTIONS FROM THE BANK, not via SERMEPA).';
                    break;
                case PAY_SUPPLIER_PAYPAL:
//                case PAY_SUPPLIER_Z:
                    echo '&nbsp;' . CHtml::checkBox("sendToPayGateway", false,
                        array('disabled' => 'disabled', 'class' => 'readOnly'));
                    echo 'If this checkbox is marked then the REFUND will be sent automatically to PAYPAL.';
                    break;
                case PAY_SUPPLIER_ADYEN:
                case PAY_SUPPLIER_ADYEN_HPP:
                    echo '&nbsp;' . CHtml::checkBox("sendToPayGateway", true,
                        array('style' => 'width: 20px; height: 20px;'));
                    echo 'If this checkbox is marked then the REFUND will be sent automatically to ADYEN.';
                    break;
                default:
                    // continue, nothing to print out.
                    break;
            }
            ?>
        </div>
        <div class="row">
            <?php
            echo $form->labelEx($model, 'dateToPay');
            echo $form->textField($model, 'dateToPay');
            echo CHtml::image("images/calendar.jpg", "calendar", array("id" => "c_button", "class" => "pointer"));
            $this->widget('application.extensions.calendar.SCalendar',
              array(
                'inputField' => 'AbaPayments_dateToPay',
                'button' => 'c_button',
                'ifFormat' => '%Y-%m-%d',
              ));
            echo $form->error($model, 'dateToPay'); ?>
        </div>
        <?php
        echo $form->hiddenField($model, 'id');
        echo $form->hiddenField($model, 'userId');
        echo $form->hiddenField($model, 'idProduct');
        echo $form->hiddenField($model, 'idCountry');
        echo $form->hiddenField($model, 'idPeriodPay');
        echo $form->hiddenField($model, 'idUserCreditForm');
        echo $form->hiddenField($model, 'paySuppExtId');

        echo $form->hiddenField($model, 'status');
        echo $form->hiddenField($model, 'dateEndTransaction');
        echo $form->hiddenField($model, 'dateStartTransaction');
        echo $form->hiddenField($model, 'xRateToEUR');
        echo $form->hiddenField($model, 'xRateToUSD');
        echo $form->hiddenField($model, 'currencyTrans');
        echo $form->hiddenField($model, 'foreignCurrencyTrans');
        echo $form->hiddenField($model, 'idPromoCode');
        echo $form->hiddenField($model, 'amountOriginal');
        echo $form->hiddenField($model, 'amountDiscount');
        echo $form->hiddenField($model, 'idPayControlCheck');
        echo $form->hiddenField($model, 'idPartner');

        echo $form->hiddenField($model, 'taxRateValue');
        echo $form->hiddenField($model, 'amountTax');
        echo $form->hiddenField($model, 'amountPriceWithoutTax');
        ?>
        <div class="row buttons">
            <?php if ($model->paySuppExtId != PAY_SUPPLIER_GROUPON && $model->paySuppExtId != PAY_SUPPLIER_B2B) {
                echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save');
            } ?>
            <?php echo CHtml::button('Cancel',
              array(
                'onClick' => 'window.location="' . Yii::app()->createUrl('abaPayments/admin_id',
                    array('id' => $model->userId)) . '";'
              )); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->