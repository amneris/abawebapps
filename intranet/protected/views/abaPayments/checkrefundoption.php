<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */

$this->renderPartial("_formheader",array( "model" => $model, "actionTitle" => "Refund payment" ));

$optionsRender = array("model" => $model, "adyenSuccessRefund" => $adyenSuccessRefund);
if(isset($success)) {
    $optionsRender["success"] = $success;
}
if(isset($msgRefund)) {
    $optionsRender["msgRefund"] = $msgRefund;
}

$this->renderPartial( "_checkrefund", $optionsRender );
$this->renderPartial( "_fulldetails", array("model" => $model) );
