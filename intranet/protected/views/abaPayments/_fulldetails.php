<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */
?>
<h4>Full details:</h4>
<?php $this->widget('application.components.widgets.CDetailViewABA', array(
    'data'=>$model,
    'attributes'=>array(
        'id',
        'userId',
        'idProduct',
        'idCountry'=>
        array(
            'name'=>'idCountry',
            'type'=>'countryList',
        ),
        'idPeriodPay',
        'idUserCreditForm',
        'paySuppExtId'=>
        array(
            'name'=>'paySuppExtId',
            'type'=>'supplierExtGatewayList',
        ),
        'paySuppOrderId',
        'paySuppExtProfId',
        'status'=>
        array(
            'name'=>'status',
            'type'=>'statusDescription',
        ),
        'dateStartTransaction',
        'dateEndTransaction',
        'dateToPay',
        'xRateToEUR',
        'xRateToUSD',
        'currencyTrans',
        'foreignCurrencyTrans',
        'idPromoCode',
        'amountOriginal',
        'amountDiscount',
        'amountPrice',
        'idPayControlCheck',
        'idPartner'=> array(
            'name'=>'idPartner',
            'type'=>'partnerList',
        ),
        'lastAction',
        'isRecurring' => array(
            'name'=>'isRecurring',
            'type'=>"recurringList",
        ),
        'isExtend' => array(
            'name'=>'isExtend',
            'type'=>"recurringList",
        ),
        'paySuppExtUniId',
        'paySuppLinkStatus' => array(
            'name'=>'paySuppLinkStatus',
            'type'=>'reconciliationStatusList',
            'nullDisplay'=>'NOT MATCHED*',
        ),
        'isPeriodPayChange',
        'cancelOrigin' => array(
                'name' => 'cancelOrigin',
                'type' =>'cancelOriginList',
                'nullDisplay' => '',
        ),
    ),
));
?>

<?php if(isset($modelNotifications) AND $modelNotifications): ?>
    <br />
    <div style="border: 2px solid #298A08; padding: 10px; margin: 0 0 20px 0; background: #E0F8E0; font-size: 18px; border-radius: 10px; width: 300px;">
        Payment Method: <?php echo strtoupper(trim($modelNotifications->paymentMethod)); ?>
    </div>
<?php endif; ?>
