<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget('CActiveForm', array(
      'action' => Yii::app()->createUrl($this->route),
      'method' => 'get',
    )); ?>

    <div class="row">
        <?php echo $form->label($model, 'id'); ?>
        <?php echo $form->textField($model, 'id', array('size' => 15, 'maxlength' => 15)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'userId'); ?>
        <?php echo $form->textField($model, 'userId', array('size' => 4, 'maxlength' => 4)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'idProduct'); ?>
        <?php echo $form->textField($model, 'idProduct', array('size' => 15, 'maxlength' => 15)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'idCountry');
        echo $form->DropDownList($model, 'idCountry', HeList::getCountryList(), array('empty' => '--Any Country--'));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'idPeriodPay'); ?>
        <?php echo $form->textField($model, 'idPeriodPay', array('size' => 5, 'maxlength' => 5)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'idUserCreditForm'); ?>
        <?php echo $form->textField($model, 'idUserCreditForm', array('size' => 10, 'maxlength' => 10)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'paySuppExtId'); ?>
        <?php echo $form->textField($model, 'paySuppExtId', array('size' => 2, 'maxlength' => 2)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'paySuppOrderId'); ?>
        <?php echo $form->textField($model, 'paySuppOrderId', array('size' => 15, 'maxlength' => 15)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'status');
        echo $form->DropDownList($model, 'status', HeList::statusDescription(), array('empty' => '--Any Status--'));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dateStartTransaction');
        echo $form->textField($model, 'dateStartTransaction');
        echo CHtml::image("images/calendar.jpg", "calendar1", array("id" => "c_buttonStart", "class" => "pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
          array(
            'inputField' => 'AbaPayments_dateStartTransaction',
            'button' => 'c_buttonStart',
            'ifFormat' => '%Y-%m-%d 00:00:00',
          ));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dateEndTransaction');
        echo $form->textField($model, 'dateEndTransaction');
        echo CHtml::image("images/calendar.jpg", "calendar2", array("id" => "c_buttonEnd", "class" => "pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
          array(
            'inputField' => 'AbaPayments_dateEndTransaction',
            'button' => 'c_buttonEnd',
            'ifFormat' => '%Y-%m-%d 00:00:00',
          ));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dateToPay');
        echo $form->textField($model, 'dateToPay');
        echo CHtml::image("images/calendar.jpg", "calendar3", array("id" => "c_buttonToPay", "class" => "pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
          array(
            'inputField' => 'AbaPayments_dateToPay',
            'button' => 'c_buttonToPay',
            'ifFormat' => '%Y-%m-%d 00:00:00',
          ));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'xRateToEUR'); ?>
        <?php echo $form->textField($model, 'xRateToEUR', array('size' => 18, 'maxlength' => 18)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'xRateToUSD'); ?>
        <?php echo $form->textField($model, 'xRateToUSD', array('size' => 18, 'maxlength' => 18)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'currencyTrans'); ?>
        <?php echo $form->textField($model, 'currencyTrans', array('size' => 3, 'maxlength' => 3)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'foreignCurrencyTrans'); ?>
        <?php echo $form->textField($model, 'foreignCurrencyTrans', array('size' => 3, 'maxlength' => 3)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'idPromoCode'); ?>
        <?php echo $form->textField($model, 'idPromoCode', array('size' => 50, 'maxlength' => 50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'amountOriginal'); ?>
        <?php echo $form->textField($model, 'amountOriginal', array('size' => 18, 'maxlength' => 18)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'amountDiscount'); ?>
        <?php echo $form->textField($model, 'amountDiscount', array('size' => 18, 'maxlength' => 18)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'amountPrice'); ?>
        <?php echo $form->textField($model, 'amountPrice', array('size' => 18, 'maxlength' => 18)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'idPayControlCheck'); ?>
        <?php echo $form->textField($model, 'idPayControlCheck', array('size' => 15, 'maxlength' => 15)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'idPartner');
        echo $form->DropDownList($model, 'idPartner', HeList::getPartnerListWithoutFlashSales(),
          array('empty' => '--Any Partner--'));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'paySuppLinkStatus');
        echo $form->DropDownList($model, 'paySuppLinkStatus', HeList::reconciliationStatusList(),
          array('empty' => '--Select status--'));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'paySuppExtProfId'); ?>
        <?php echo $form->textField($model, 'paySuppExtProfId', array('size' => 50, 'maxlength' => 75)); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->