<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */

$aPayMethods = HeList::supplierExtGatewayList();
$strPaySuppExtId = $aPayMethods[$model->paySuppExtId];


$this->renderPartial("_formheader",array(
                                    "model"=>$model,
                                    "actionTitle"=>"Mark payment from ".$strPaySuppExtId." as reconciled"));

$this->renderPartial('_formreconcile', array('model'=>$model));

$this->renderPartial( "_fulldetails", array("model"=>$model) );
