<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */

$this->breadcrumbs=array(
	'Aba Payments'=>array('admin'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List AbaPayments', 'url'=>array('index')),
	array('label'=>'Manage AbaPayments', 'url'=>array('admin')),
);
?>

<h3>Create AbaPayments</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>