<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aba-exportinvoices-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required. </p>

	<?php echo $form->errorSummary($model); ?>
    
    <table id="myForm">
        
        <tr>
            <td>
                <?php 
                    $datos = $extrainfo['statusDescription'];
                    echo $form->labelEx($model,'status'); 
                ?>
            </td>
            <td>
                <?php
                    echo $form->DropDownList($model,'status',$datos, array('multiple' => 'multiple'));
                    echo $form->error($model,'status');
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <label for="AbaPayments_startDate" class="required">Start Period</label>
            </td>
            <td>
                <input name="AbaPayments[startDate]" id="AbaPayments_startDate" type="text" value="<?php echo HeDate::today();?>" />
                <?php 
                    echo CHtml::image("images/calendar.jpg","calendar", array("id"=>"c_button","class"=>"pointer")); 
                    $this->widget('application.extensions.calendar.SCalendar',
                        array(
                            'inputField'=>'AbaPayments_startDate',
                            'button'=>'c_button',
                            'ifFormat'=>'%Y-%m-%d',
                            ));
//                'ifFormat'=>'%Y-%m-%d 00:00:00',
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <label for="AbaPayments_endDate" class="required">End Period</label>
            </td>
            <td>
                <input name="AbaPayments[endDate]" id="AbaPayments_endDate" type="text" value="<?php echo HeDate::todayComplete();?>" />
                <?php 
                    echo CHtml::image("images/calendar.jpg","calendar", array("id"=>"c_buttonEnd","class"=>"pointer")); 
                    $this->widget('application.extensions.calendar.SCalendar',
                        array(
                        'inputField'=>'AbaPayments_endDate',
                        'button'=>'c_buttonEnd',
                        'ifFormat'=>'%Y-%m-%d',
                    ));
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php 
                    $datos = $extrainfo['invoicesPartnerGroupList'];
                ?>
                <label for="AbaPayments_idPartnerGroup">PartnerGroup</label>
            </td>
            <td>
                <?php 		
                    echo $form->DropDownList($model, 'idPartnerGroup', $datos, array('multiple' => 'multiple'));
                ?>
            </td>
        </tr>
    </table>

	<div class="row buttons" align="center">
		<?php echo CHtml::submitButton('Create xls'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->