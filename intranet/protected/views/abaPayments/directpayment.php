<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */

$this->renderPartial("_formheader", array(
  "model" => $model,
  "actionTitle" => "Direct payment"
));

$this->renderPartial('_directpayment', array('model' => $model));

$this->renderPartial("_fulldetails", array("model" => $model));
