<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */

$this->renderPartial("_formheader", array(
  "model" => $model,
  "actionTitle" => "Refund payment"
));

if (
  $model->paySuppExtId == PAY_SUPPLIER_ANDROID_PLAY_STORE
  OR $model->paySuppExtId == PAY_SUPPLIER_Z
) {

    $this->renderPartial('_msgcantrefund', array('model' => $model));
} else {

    $this->renderPartial('_formabono', array('model' => $model));

    $this->renderPartial("_fulldetails", array("model" => $model));

}
