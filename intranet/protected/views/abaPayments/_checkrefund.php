<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */
/* @var $adyenSuccessRefund AbaPaymentsAdyenPendingRefunds */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form = $this->beginWidget('CActiveForm', array('id'=>'aba-adyen-refund-form', 'enableAjaxValidation' => false));

if ($model->paySuppExtId == PAY_SUPPLIER_ADYEN) {
?>

<?php //if(HeQuery::isParcialSuccess($model->id)): ?>
    <p class="highlight">ATTENTION: We are checking our systems whether we have received a notification from Adyen, when we receive an affirmative notification the REFUND will be created.
        <?php if(isset($msgRefund)): ?>
            <br /><strong style="color: #cc0000;">ERROR: <?php echo $msgRefund; ?></strong>
        <?php endif; ?>
        <br /><?php if(isset($success)): ?>
            <br /><strong>! The notification still hasn't arrived. Please try again later !</strong>
        <?php endif; ?> We recommend that you retry after 3-4 minutes.
    </p>
<?php //endif; ?>
<?php
}
?>
<?php
        echo CHtml::hiddenField("readPaySuppOrderId", $model->paySuppOrderId);

        echo $form->hiddenField($model, 'paySuppOrderId');
        echo $form->hiddenField($model, 'amountPrice');
        echo $form->hiddenField($model, 'dateToPay');

        echo $form->hiddenField($model, 'id');
        echo $form->hiddenField($model, 'userId');
        echo $form->hiddenField($model, 'idProduct');
        echo $form->hiddenField($model, 'idCountry');
        echo $form->hiddenField($model, 'idPeriodPay');
        echo $form->hiddenField($model, 'idUserCreditForm');
        echo $form->hiddenField($model, 'paySuppExtId');
        echo $form->hiddenField($model, 'status');
        echo $form->hiddenField($model, 'dateEndTransaction');
        echo $form->hiddenField($model, 'dateStartTransaction');
        echo $form->hiddenField($model, 'xRateToEUR');
        echo $form->hiddenField($model, 'xRateToUSD');
        echo $form->hiddenField($model, 'currencyTrans');
        echo $form->hiddenField($model, 'foreignCurrencyTrans');
        echo $form->hiddenField($model, 'idPromoCode');
        echo $form->hiddenField($model, 'amountOriginal');
        echo $form->hiddenField($model, 'amountDiscount');
        echo $form->hiddenField($model, 'idPayControlCheck');
        echo $form->hiddenField($model, 'idPartner');
        echo $form->hiddenField($model, 'taxRateValue');
        echo $form->hiddenField($model, 'amountTax');
        echo $form->hiddenField($model, 'amountPriceWithoutTax');

		echo $form->hiddenField($adyenSuccessRefund, 'id');
		echo $form->hiddenField($adyenSuccessRefund, 'idPayment');
		echo $form->hiddenField($adyenSuccessRefund, 'pspReference');
		echo $form->hiddenField($adyenSuccessRefund, 'merchantReference');
		echo $form->hiddenField($adyenSuccessRefund, 'status');
	?>
	<div class="row buttons">
<!--		--><?php //echo CHtml::submitButton($model->isNewRecord ? 'Check & Create Refund' : 'Save'); ?>
        <?php echo CHtml::submitButton('Check & Create Refund'); ?>
<!--        --><?php //echo CHtml::button('Cancel',
//                    array('onClick'=>'window.location="'.Yii::app()->createUrl('abaPayments/admin_id',array('id'=>$model->userId)).'";')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->