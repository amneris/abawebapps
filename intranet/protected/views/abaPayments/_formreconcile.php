<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */
/* @var $form CActiveForm */
?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aba-reconcile-form',
	'enableAjaxValidation'=>false,
));
echo $form->hiddenField($model,'id');
echo $form->hiddenField($model,'userId');
echo $form->hiddenField($model,'paySuppExtId');
echo $form->hiddenField($model,'status');
echo $form->hiddenField($model,'dateEndTransaction');
echo $form->hiddenField($model,'dateStartTransaction');
?>
	<div class="note">Fields with <span class="required">*</span> are required.</div>
	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<?php
            echo $form->labelEx($model,'paySuppOrderId');
            echo $form->textField($model,'paySuppOrderId', array('readonly'=>"readonly", 'size'=>15,'maxlength'=>15));
            echo $form->error($model,'paySuppOrderId');
        ?>
	</div>
	<div class="row">
		<?php
			echo $form->labelEx($model, 'paySuppLinkStatus');
			echo $form->dropDownList($model, 'paySuppLinkStatus', HeList::reconciliationStatusList());
			echo $form->error($model,'paySuppLinkStatus');
        ?>
	</div>
	<div class="row">
		<?php 
			echo $form->labelEx($model,'paySuppExtUniId');
			echo $form->textField($model,'paySuppExtUniId', array('size'=>30,'maxlength'=>55));
			echo $form->error($model,'paySuppExtUniId');
        ?>
	</div>
        <?php
            echo $form->hiddenField($model,'id');
            echo $form->hiddenField($model,'userId');
        ?>
	<div class="row buttons">
		<?php
            echo CHtml::submitButton('Save');
            echo "&nbsp;&nbsp;";
            echo CHtml::button('Cancel',array("id"=>'idCancel',
                                              "onclick"=>'document.location.href="'.Yii::app()->createUrl('abaPayments/admin_id',array('id'=>$model->userId)).'"'));
        ?>
	</div>
<?php $this->endWidget(); ?>
<!-- form -->
</div>