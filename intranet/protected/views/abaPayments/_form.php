<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aba-payments-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>

	<div class="row">
		<?php/* echo $form->labelEx($model,'idUserProdStamp'); 
		 echo $form->textField($model,'idUserProdStamp',array('size'=>10,'maxlength'=>10));
		 echo $form->error($model,'idUserProdStamp');*/ ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'userId'); ?>
		<?php echo $form->textField($model,'userId',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'userId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idProduct'); ?>
		<?php echo $form->textField($model,'idProduct',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'idProduct'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idCountry'); ?>
		<?php echo $form->textField($model,'idCountry',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'idCountry'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idPeriodPay'); ?>
		<?php echo $form->textField($model,'idPeriodPay',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'idPeriodPay'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idUserCreditForm'); ?>
		<?php echo $form->textField($model,'idUserCreditForm',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'idUserCreditForm'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'paySuppExtId'); ?>
		<?php echo $form->textField($model,'paySuppExtId',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'paySuppExtId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'paySuppOrderId'); ?>
		<?php echo $form->textField($model,'paySuppOrderId',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'paySuppOrderId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dateStartTransaction'); ?>
		<?php echo $form->textField($model,'dateStartTransaction'); ?>
		<?php echo $form->error($model,'dateStartTransaction'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dateEndTransaction'); ?>
		<?php echo $form->textField($model,'dateEndTransaction'); ?>
		<?php echo $form->error($model,'dateEndTransaction'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dateToPay'); ?>
		<?php echo $form->textField($model,'dateToPay'); ?>
		<?php echo $form->error($model,'dateToPay'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xRateToEUR'); ?>
		<?php echo $form->textField($model,'xRateToEUR',array('size'=>18,'maxlength'=>18)); ?>
		<?php echo $form->error($model,'xRateToEUR'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xRateToUSD'); ?>
		<?php echo $form->textField($model,'xRateToUSD',array('size'=>18,'maxlength'=>18)); ?>
		<?php echo $form->error($model,'xRateToUSD'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'currencyTrans'); ?>
		<?php echo $form->textField($model,'currencyTrans',array('size'=>3,'maxlength'=>3)); ?>
		<?php echo $form->error($model,'currencyTrans'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'foreignCurrencyTrans'); ?>
		<?php echo $form->textField($model,'foreignCurrencyTrans',array('size'=>3,'maxlength'=>3)); ?>
		<?php echo $form->error($model,'foreignCurrencyTrans'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idPromoCode'); ?>
		<?php echo $form->textField($model,'idPromoCode',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'idPromoCode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'amountOriginal'); ?>
		<?php echo $form->textField($model,'amountOriginal',array('size'=>18,'maxlength'=>18)); ?>
		<?php echo $form->error($model,'amountOriginal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'amountDiscount'); ?>
		<?php echo $form->textField($model,'amountDiscount',array('size'=>18,'maxlength'=>18)); ?>
		<?php echo $form->error($model,'amountDiscount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'amountPrice'); ?>
		<?php echo $form->textField($model,'amountPrice',array('size'=>18,'maxlength'=>18)); ?>
		<?php echo $form->error($model,'amountPrice'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idPayControlCheck'); ?>
		<?php echo $form->textField($model,'idPayControlCheck',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'idPayControlCheck'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idPartner'); ?>
		<?php echo $form->textField($model,'idPartner',array('size'=>3,'maxlength'=>3)); ?>
		<?php echo $form->error($model,'idPartner'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->