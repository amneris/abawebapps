<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aba-export-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required. </p>

	<?php echo $form->errorSummary($model); ?>
    
    <table id="myForm">
        
        <tr>
            <td>
                <?php 
                    $datos = $extrainfo['statusDescription'];
                    echo $form->labelEx($model,'status'); 
                ?>
            </td>
            <td>
                <?php
                    echo $form->DropDownList($model,'status',$datos, array('multiple' => 'multiple'));
                    echo $form->error($model,'status');
                ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <label for="AbaPayments_startDate" class="required">Start Period</label>
            </td>
            <td>
                <input name="AbaPayments[startDate]" id="AbaPayments_startDate" type="text" value="<?php echo HeDate::today(true);?>" />
                <?php 
                    echo CHtml::image("images/calendar.jpg","calendar", array("id"=>"c_button","class"=>"pointer")); 
                    $this->widget('application.extensions.calendar.SCalendar',
                        array(
                            'inputField'=>'AbaPayments_startDate',
                            'button'=>'c_button',
                            'ifFormat'=>'%Y-%m-%d 00:00:00',
                            ));
                ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <label for="AbaPayments_endDate" class="required">End Period</label>
            </td>
            <td>
                <input name="AbaPayments[endDate]" id="AbaPayments_endDate" type="text" value="<?php echo HeDate::todayComplete(true);?>" />
                <?php 
                    echo CHtml::image("images/calendar.jpg","calendar", array("id"=>"c_buttonEnd","class"=>"pointer")); 
                    $this->widget('application.extensions.calendar.SCalendar',
                        array(
                        'inputField'=>'AbaPayments_endDate',
                        'button'=>'c_buttonEnd',
                        'ifFormat'=>'%Y-%m-%d 23:59:59',
                    ));

                echo " <i>and check this</i> ";
                echo $form->checkBox($model,'endDateTransaction');
                echo " <i>if you like to search by \"end date transaction\".</i> ";
                ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <?php 
                    $datos = $extrainfo['partnerGroupList'];
                ?>
                <label for="AbaPayments_idPartnerGroup">Id PartnerGroup</label>		
            </td>
            <td>
                <?php 		
                    echo $form->DropDownList($model,'idPartnerGroup',$datos, array('multiple' => 'multiple'));
                ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <?php 
                    $datos = $extrainfo['partnerList'];
                    echo $form->labelEx($model,'idPartner'); 
                ?>
            </td>
            <td>
                <?php
                    echo $form->DropDownList($model,'idPartner',$datos, array('multiple' => 'multiple'));
                    echo $form->error($model,'idPartner');
                ?>
            </td>
        </tr>

        <tr>
            <td>
                <label for="includeCampaign" class="required">Include Campaing</label>
            </td>
            <td>
                <?php
                    echo '&nbsp;' . CHtml::checkBox( "includeCampaign", false, array());
                ?>
            </td>
        </tr>

    </table>
    
	<div class="row buttons" align="center">
		<?php echo CHtml::submitButton('Create xls'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->