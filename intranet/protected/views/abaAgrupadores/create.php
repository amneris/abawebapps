<?php
/* @var $this AbaUserController */
/* @var $model AbaUser */

$this->breadcrumbs=array(
    'Groupers'=>array('index'),
	'Create',
);
/*
$this->menu=array(
//	array('label'=>'List AbaUser', 'url'=>array('index')),
	array('label'=>'Manage AbaUsers', 'url'=>array('admin')),
);*/
?>

<h3>Create new grouper</h3>
<p><i><b>Note:</b> When creating a new grouper, it will create all the tables for each product.<br>
If a grouper for example, has no monthly product, indicate with "no" in the appropriate box and the table will not be created.</i></p>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>