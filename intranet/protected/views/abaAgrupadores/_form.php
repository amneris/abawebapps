<?php
/* @var $this AbaAgrupadorController */
/* @var $model AbaAgrupador */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aba-agrupador-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

        <table id="myForm">
            <tr>
                <td><?php echo $form->labelEx($model,'agrupadorName'); ?></td>
                <td><?php echo $form->textField($model,'agrupadorName',array('size'=>60,'maxlength'=>100)); ?>
                <?php echo $form->error($model,'agrupadorName'); ?></td>
            </tr>
            <tr>
                <td><?php echo $form->labelEx($model,'tableName'); ?></td>
                <td><?php echo $form->textField($model,'tableName',array('size'=>60,'maxlength'=>100)); ?>
                    <?php echo $form->error($model,'tableName'); ?></td>
            </tr>          
            <tr>
                <td><?php echo $form->labelEx($model,'oneOrTwoCodes'); ?></td>
                <td><?php echo $form->textField($model,'oneOrTwoCodes',array('size'=>2,'maxlength'=>4)); ?>
                    <?php echo $form->error($model,'oneOrTwoCodes'); ?></td>
            </tr>
            <tr>
                <td><?php echo $form->labelEx($model,'totalDeals'); ?></td>
                <td><?php echo $form->textField($model,'totalDeals',array('size'=>2,'maxlength'=>10)); ?>
                <?php echo $form->error($model,'totalDeals'); ?></td>
            </tr>

            <tr>
                <td><?php echo $form->labelEx($model,'url'); ?></td>
                <td><?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>100)); ?>
                    <?php echo $form->error($model,'url'); ?></td>
            </tr>
          
             <tr>
                <td>
                    <?php echo $form->labelEx($model,'prefijoMensual'); ?>
                </td>
                <td>
                    <?php echo $form->textField($model,'prefijoMensual',array('size'=>2,'maxlength'=>10)); ?>
                    <?php echo $form->error($model,'prefijoMensual'); ?> 
                </td>
            </tr>

            <tr>
                <td>
                    <?php echo $form->labelEx($model,'prefijoBimensual'); ?>
                </td>
                <td>
                    <?php echo $form->textField($model,'prefijoBimensual',array('size'=>2,'maxlength'=>10)); ?>
                    <?php echo $form->error($model,'prefijoBimensual'); ?>
                </td>
            </tr>
            
            <tr>
                <td>
                    <?php echo $form->labelEx($model,'prefijoTrimestral'); ?>
                </td>
                <td>
                    <?php echo $form->textField($model,'prefijoTrimestral',array('size'=>2,'maxlength'=>10)); ?>
                    <?php echo $form->error($model,'prefijoTrimestral'); ?> 
                </td>
            </tr>
            
            <tr>
                <td>
                    <?php echo $form->labelEx($model,'prefijoSemestral'); ?>
                </td>
                <td>
                    <?php echo $form->textField($model,'prefijoSemestral',array('size'=>2,'maxlength'=>10)); ?>
                    <?php echo $form->error($model,'prefijoSemestral'); ?> 
                </td>
            </tr>
            
            <tr>
                <td>
                    <?php echo $form->labelEx($model,'prefijoAnual'); ?>
                </td>
                <td>
                    <?php echo $form->textField($model,'prefijoAnual',array('size'=>2,'maxlength'=>10)); ?>
                    <?php echo $form->error($model,'prefijoAnual'); ?> 
                </td>
            </tr>
            
            <tr>
                <td>
                    <?php echo $form->labelEx($model,'prefijo18Meses'); ?>
                </td>
                <td>
                    <?php echo $form->textField($model,'prefijo18Meses',array('size'=>2,'maxlength'=>10)); ?>
                    <?php echo $form->error($model,'prefijo18Meses'); ?> 
                </td>
            </tr>
            
            <tr>
                <td>
                    <?php echo $form->labelEx($model,'prefijo24Meses'); ?>
                </td>
                <td>
                    <?php echo $form->textField($model,'prefijo24Meses',array('size'=>2,'maxlength'=>10)); ?>
                    <?php echo $form->error($model,'prefijo24Meses'); ?> 
                </td>
            </tr>
            
        </table>
        
	<div class="row buttons" align="center">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->