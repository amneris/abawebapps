<?php
/* @var $this AbaAgrupadoresController */

$this->breadcrumbs=array(
	'Groupers'=>array('index'),
	$model->agrupadorName,
);

?>
<h3>Details for grouper <b><?php echo $model->agrupadorName; ?></b></h3>

<?php $this->widget('application.components.widgets.CDetailViewABA', array(
	'data'=>$model,
	'attributes'=>array(
		'idventasFlashScript',
        'agrupadorName',
		'tableName',
        array(
            'label'=> $model->getAttributeLabel('url'),
            'type'=>'raw',
            'value'=>'<a href='.$model->url.' target="_blank">'.$model->url.'</a>',
        ),
		'oneOrTwoCodes',
        'totalDeals',
		'prefijoMensual',
        'prefijoBimensual',
		'prefijoTrimestral',
		'prefijoSemestral',
		'prefijoAnual',
		'prefijo18Meses',
		'prefijo24Meses',
		
	),
)); ?>