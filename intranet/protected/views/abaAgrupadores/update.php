<?php
/* @var $this AbaUserController */
/* @var $model AbaUser */


$this->breadcrumbs=array(
	'Groupers'=>array('index'),
	'Update',
);

?>

<h3>Update grouper <b><?php echo $model->agrupadorName; ?></b></h3>
<p><i><b>Note:</b>If the table name or the prefix of some of the products changes, it will change the data base names also.</i></p>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>