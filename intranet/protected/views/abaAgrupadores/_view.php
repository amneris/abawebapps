<?php
/* @var $this AbaAgrupadoresController */
/* @var $data AbaAgrupadores */
?>

<div class="view">
   
        <span class="agrupdaroTitle"><b><?php echo CHtml::encode($data->agrupadorName); ?></b></span>
	<br><br>
        
	<b><?php echo CHtml::encode($data->getAttributeLabel('tableName')); ?>:</b>
	<?php echo CHtml::encode($data->tableName); ?>
	<br />
        
        <b><?php echo CHtml::encode($data->getAttributeLabel('oneOrTwoCodes')); ?>:</b>
	<?php echo CHtml::encode($data->oneOrTwoCodes); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('url')); ?>:</b>
    <a href="<?php echo CHtml::encode($data->url); ?>"><?php echo CHtml::encode($data->url); ?></a>
    <br />
        
        <b><?php echo CHtml::encode($data->getAttributeLabel('prefijoMensual')); ?>:</b>
	<?php echo CHtml::encode($data->prefijoMensual); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('prefijoBimensual')); ?>:</b>
    <?php echo CHtml::encode($data->prefijoBimensual); ?>
    <br />
	
        <b><?php echo CHtml::encode($data->getAttributeLabel('prefijoTrimestral')); ?>:</b>
	<?php echo CHtml::encode($data->prefijoTrimestral); ?>
	<br />
        
        <b><?php echo CHtml::encode($data->getAttributeLabel('prefijoSemestral')); ?>:</b>
	<?php echo CHtml::encode($data->prefijoSemestral); ?>
	<br />
        
        <b><?php echo CHtml::encode($data->getAttributeLabel('prefijoAnual')); ?>:</b>
	<?php echo CHtml::encode($data->prefijoAnual); ?>
	<br />
        
        <b><?php echo CHtml::encode($data->getAttributeLabel('prefijo18Meses')); ?>:</b>
	<?php echo CHtml::encode($data->prefijo18Meses); ?>
	<br />
        
        <b><?php echo CHtml::encode($data->getAttributeLabel('prefijo24Meses')); ?>:</b>
	<?php echo CHtml::encode($data->prefijo24Meses); ?>
	<br />

</div>