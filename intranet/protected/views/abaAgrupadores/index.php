<?php
/* @var $this AbaAgrupadoresController */

$this->breadcrumbs=array(
	'Groupers',
);

?>
<h3>Groupers</h3>

<?php echo CHtml::link('Create new grouper',array('AbaAgrupadores/create'),array('title'=>'Crea un nuevo agrupador con sus respectivas tablas en base de datos.'));

echo "<br>";

?>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/

$this->widget('application.components.widgets.CGridViewABA', array(
	'id'=>'aba-agrupadores-grid',
	'extraInfo'=>array('update', 'view'),
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
                    'prevPageLabel'  => '<',
                    'nextPageLabel'  => '>',
                    'firstPageLabel' => '<<',
                    'lastPageLabel'  => '>>',),
	'extraButtons'=>array(
		
	), //end ExtraButtons

	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'filterSelector'=>'{filter}',
	'columns'=>array
    (
		//'idventasFlashScript',
        'agrupadorName',
		'tableName',
		'oneOrTwoCodes',
        'totalDeals',
		'prefijoMensual',
        'prefijoBimensual',
		'prefijoTrimestral',
		'prefijoSemestral',
		'prefijoAnual',
		'prefijo18Meses',
		'prefijo24Meses',
        array(
            'header'=>'Options',
            'class'=>'CButtonColumnABA',
		),
        array(
            'class'=>'CLinkColumn',
            'label'=>'Visit',
            'urlExpression'=>'$data->url',
            'header'=>'Landing',
            'linkHtmlOptions'=>array('target'=>'_blank',),
        ),
    ),
));


?>
