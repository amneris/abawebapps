<?php
/* @var $this AbaUserObjectiveController */
/* @var $model AbaUserObjective */

$this->breadcrumbs=array(
	'Aba User Objectives'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AbaUserObjective', 'url'=>array('index')),
	array('label'=>'Manage AbaUserObjective', 'url'=>array('admin')),
);
?>

<h1>Create AbaUserObjective</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>