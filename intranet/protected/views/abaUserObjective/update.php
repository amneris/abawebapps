<?php
/* @var $this AbaUserObjectiveController */
/* @var $model AbaUserObjective */

$this->breadcrumbs=array(
	'Aba User Objectives'=>array('index'),
	$model->userId=>array('view','id'=>$model->userId),
	'Update',
);

$this->menu=array(
	array('label'=>'List AbaUserObjective', 'url'=>array('index')),
	array('label'=>'Create AbaUserObjective', 'url'=>array('create')),
	array('label'=>'View AbaUserObjective', 'url'=>array('view', 'id'=>$model->userId)),
	array('label'=>'Manage AbaUserObjective', 'url'=>array('admin')),
);
?>

<h1>Update AbaUserObjective <?php echo $model->userId; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>