<?php
/* @var $this AbaUserObjectiveController */
/* @var $model AbaUserObjective */

$this->breadcrumbs=array(
	'Aba User Objectives'=>array('index'),
	$model->userId,
);

$this->menu=array(
	array('label'=>'List AbaUserObjective', 'url'=>array('index')),
	array('label'=>'Create AbaUserObjective', 'url'=>array('create')),
	array('label'=>'Update AbaUserObjective', 'url'=>array('update', 'id'=>$model->userId)),
	array('label'=>'Delete AbaUserObjective', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->userId),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AbaUserObjective', 'url'=>array('admin')),
);
?>

<h1>View AbaUserObjective #<?php echo $model->userId; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'userId',
		'objectiveId',
		'value',
	),
)); ?>
