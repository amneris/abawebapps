<?php
/* @var $this AbaUserObjectiveController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Aba User Objectives',
);

$this->menu=array(
	array('label'=>'Create AbaUserObjective', 'url'=>array('create')),
	array('label'=>'Manage AbaUserObjective', 'url'=>array('admin')),
);
?>

<h1>Aba User Objectives</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
