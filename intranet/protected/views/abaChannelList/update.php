<?php
/* @var $this AbaChannelListController */
/* @var $model AbaChannelList */

$this->breadcrumbs=array(
	'Aba Channel Lists'=>array('index'),
	$model->name=>array('view','id'=>$model->idPartner),
	'Update',
);

$this->menu=array(
	array('label'=>'List AbaChannelList', 'url'=>array('index')),
	array('label'=>'Create AbaChannelList', 'url'=>array('create')),
	array('label'=>'View AbaChannelList', 'url'=>array('view', 'id'=>$model->idPartner)),
	array('label'=>'Manage AbaChannelList', 'url'=>array('admin')),
);
?>

<h1>Update AbaChannelList <?php echo $model->idPartner; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>