<?php
/* @var $this AbaChannelListController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Aba Channel Lists',
);

$this->menu=array(
	array('label'=>'Create AbaChannelList', 'url'=>array('create')),
	array('label'=>'Manage AbaChannelList', 'url'=>array('admin')),
);
?>

<h1>Aba Channel Lists</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
