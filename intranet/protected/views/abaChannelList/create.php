<?php
/* @var $this AbaChannelListController */
/* @var $model AbaChannelList */

$this->breadcrumbs=array(
    'B2B Channel Lists'=>array('admin'),
    'Create',
);

$this->menu=array(
	array('label'=>'List AbaChannelList', 'url'=>array('index')),
	array('label'=>'Manage AbaChannelList', 'url'=>array('admin')),
);
?>

<h3>Create channel</h3>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>