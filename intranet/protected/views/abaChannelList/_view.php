<?php
/* @var $this AbaChannelListController */
/* @var $data AbaChannelList */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPartner')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idPartner), array('view', 'id'=>$data->idPartner)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPartnerGroup')); ?>:</b>
	<?php echo CHtml::encode($data->idPartnerGroup); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nameGroup')); ?>:</b>
	<?php echo CHtml::encode($data->nameGroup); ?>
	<br />


</div>