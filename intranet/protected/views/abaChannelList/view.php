<?php
/* @var $this AbaChannelListController */
/* @var $model AbaChannelList */

$this->breadcrumbs=array(
    'B2B Channel List'=>array('admin'),
    $model->name,
);

$this->menu=array(
	array('label'=>'List AbaChannelList', 'url'=>array('index')),
	array('label'=>'Create AbaChannelList', 'url'=>array('create')),
	array('label'=>'Update AbaChannelList', 'url'=>array('update', 'id'=>$model->idPartner)),
	array('label'=>'Delete AbaChannelList', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idPartner),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AbaChannelList', 'url'=>array('admin')),
);
?>

<h3>View channel <?php echo $model->name; ?></h3>

<?php $this->widget('application.components.widgets.CDetailViewABA', array(
	'data'=>$model,
	'attributes'=>array(
		'idPartner',
		'name',
        'idPartnerGroup'=>
            array(               // related city displayed as a link
                'name'=>'idPartnerGroup',
                'type'=>'channelGroupList+',
            ),
	),
)); ?>
