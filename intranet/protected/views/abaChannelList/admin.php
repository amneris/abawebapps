<?php
/* @var $this AbaChannelListController */
/* @var $model AbaChannelList */

$this->breadcrumbs=array(
    'B2B Channel List',
);

$this->menu=array(
	array('label'=>'List AbaChannelList', 'url'=>array('index')),
	array('label'=>'Create AbaChannelList', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#aba-channel-list-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>B2B Channel list</h3>

<?php echo CHtml::link('Create new channel',array('abaChannelList/create')); ?>

<?php $this->widget('application.components.widgets.CGridViewABA', array(
    'id'=>'aba-channel-list-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
        'prevPageLabel'  => '<',
        'nextPageLabel'  => '>',
        'firstPageLabel' => '<<',
        'lastPageLabel'  => '>>',),
    'extraInfo'=>array('view'),
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'filterSelector'=>'{filter}',
    'columns'=>array(
        'idPartner',
        'name',
        'idPartnerGroup'=>
            array(               // related city displayed as a link
                'name'=>'idPartnerGroup',
                'type'=>'channelGroupList',
                'filter'=>HeList::channelGroupList(),
            ),
        array(
            'class'=>'CButtonColumnABA',
            'header'=>'Options',
        ),
    ),
)); ?>