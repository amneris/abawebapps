<?php
/* @var $this AbaPartnersListController */
/* @var $model AbaPartnersList */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'aba-channel-list-form',
        'enableAjaxValidation'=>false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <table id="myForm">
        <tr>
            <td>
                <?php echo $form->labelEx($model,'name'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'name',array('size'=>40,'maxlength'=>60)); ?>
                <?php echo $form->error($model,'name'); ?>
            </td>
        </tr>

        <tr>
            <td><?php echo $form->labelEx($model,'idPartnerGroup'); ?></td>
            <td>
                <?php
                $options = HeList::channelGroupList();
                echo $form->dropDownList($model, 'idPartnerGroup', $options);
                ?>
            </td>
        </tr>

    </table>

    <div class="row buttons" align="center">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->