<?php
/* @var $this SourcesListController */
/* @var $model SourcesList */

$this->breadcrumbs=array(
	'Sources List'=>array('admin'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List SourcesList', 'url'=>array('index')),
	array('label'=>'Create SourcesList', 'url'=>array('create')),
	array('label'=>'Update SourcesList', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SourcesList', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SourcesList', 'url'=>array('admin')),
);
?>

<h3>View sources <b><?php echo $model->id; ?></b></h3>

<?php $this->widget('application.components.widgets.CDetailViewABA', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'description',
	),
)); ?>
