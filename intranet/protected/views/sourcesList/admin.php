<?php
/* @var $this SourcesListController */
/* @var $model SourcesList */

$this->breadcrumbs=array(
	'Sources List',
);

$this->menu=array(
	array('label'=>'Create SourcesList', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('sources-list-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Sources List</h3>

<?php echo CHtml::link('Create new source',array('sourcesList/create')); ?>

<?php $this->widget('application.components.widgets.CGridViewABA', array(
	'id'=>'sources-list-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
        'prevPageLabel'  => '<',
        'nextPageLabel'  => '>',
        'firstPageLabel' => '<<',
        'lastPageLabel'  => '>>',),
    'extraInfo'=>array('view'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'description',
        array(
            'class'=>'CButtonColumnABA',
            'header'=>'Options',
        ),
	),
)); ?>
