<?php
/* @var $this SourcesListController */
/* @var $model SourcesList */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sources-list-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <table id="myForm">
        <tr>
            <td>
                <?php echo $form->labelEx($model,'name'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>60)); ?>
                <?php echo $form->error($model,'name'); ?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo $form->labelEx($model,'description'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>250)); ?>
                <?php echo $form->error($model,'description'); ?>
            </td>
        </tr>

    </table>

    <div class="row buttons" align="center">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->