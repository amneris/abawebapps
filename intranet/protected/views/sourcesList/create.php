<?php
/* @var $this SourcesListController */
/* @var $model SourcesList */

$this->breadcrumbs=array(
	'Sources List'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SourcesList', 'url'=>array('index')),
	array('label'=>'Manage SourcesList', 'url'=>array('admin')),
);
?>

<h1>Create source</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>