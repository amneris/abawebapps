<?php
/* @var $this SourcesListController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Sources Lists',
);

$this->menu=array(
	array('label'=>'Create SourcesList', 'url'=>array('create')),
	array('label'=>'Manage SourcesList', 'url'=>array('admin')),
);
?>

<h1>Sources Lists</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
