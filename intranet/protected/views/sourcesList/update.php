<?php
/* @var $this SourcesListController */
/* @var $model SourcesList */

$this->breadcrumbs=array(
	'Sources Lists'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SourcesList', 'url'=>array('index')),
	array('label'=>'Create SourcesList', 'url'=>array('create')),
	array('label'=>'View SourcesList', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SourcesList', 'url'=>array('admin')),
);
?>

<h1>Update SourcesList <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>