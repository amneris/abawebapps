<?php
/* @var $this AbaExperimentsUserCategoriesController */
/* @var $model AbaExperimentsUserCategories */

$this->breadcrumbs = array(
  'Experiments',
);

$this->menu = array(
  array('label' => 'Create Experiment', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript(
  'search',
  "
   $('.search-button').click(function(){
     $('.search-form').toggle();
     return false;
   });
   $('.search-form form').submit(function(){
     $.fn.yiiGridView.update('aba-experiments-user-categories-grid', {
       data: $(this).serialize()
     });
     return false;
   });
   "
);
?>

<h3>Experiments user categories</h3>

<?php echo CHtml::link('Create new user category', array('AbaExperimentsUserCategories/create')); ?>
<br>
<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial(
      '_search',
      array(
        'model' => $model,
      )
    ); ?>
</div><!-- search-form -->

<?php $this->widget(
  'application.components.widgets.CGridViewABA',
  array(
    'id' => 'aba-experiments-user-categories-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager' => array(
      'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
      'prevPageLabel' => '<',
      'nextPageLabel' => '>',
      'firstPageLabel' => '<<',
      'lastPageLabel' => '>>',
    ),
    'extraInfo' => array('view', 'update'),
    'dataProvider' => $model->search(),
    'filter' => $model,
    'filterSelector' => '{filter}',
    'columns' => array(
      'userCategoryId',
      'userCategoryDescription',
      'dateAdd',
      array(
        'class' => 'CButtonColumnABA',
        'header' => 'Options',
      ),
    ),
  )
); ?>
