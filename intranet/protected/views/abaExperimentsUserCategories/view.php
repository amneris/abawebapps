<?php
/* @var $this AbaExperimentsUserCategoriesController */
/* @var $model AbaExperimentsUserCategories */

$this->breadcrumbs = array(
  'Experiments User Categories' => array('admin'),
  $model->userCategoryId,
);

$this->menu = array(
  array('label' => 'List Experiments User Categories', 'url' => array('index')),
  array('label' => 'Create Experiment User Category', 'url' => array('create')),
  array('label' => 'Update Experiment User Category', 'url' => array('update', 'id' => $model->userCategoryId)),
  array(
    'label' => 'Delete Experiment User Category',
    'url' => '#',
    'linkOptions' => array(
      'submit' => array('delete', 'id' => $model->userCategoryId),
      'confirm' => 'Are you sure you want to delete this item?'
    )
  ),
  array('label' => 'Manage Experiments User Categories', 'url' => array('admin')),
);
?>

<h3>View Experiments User Categories <b><?php echo $model->userCategoryId . ": " . $model->userCategoryDescription; ?></b></h3>

<?php $this->widget(
  'application.components.widgets.CDetailViewABA',
  array(
    'data' => $model,
    'attributes' => array(
      'userCategoryId',
      'userCategoryDescription',
      'dateAdd',
    ),
  )
); ?>
