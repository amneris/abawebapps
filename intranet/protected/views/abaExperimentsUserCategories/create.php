<?php
/* @var $this AbaExperimentsUserCategoriesController */
/* @var $model AbaExperimentsUserCategories */

$this->breadcrumbs = array(
  'Experiments User Categories' => array('admin'),
  'Create',
);

$this->menu = array(
  array('label' => 'Manage Experiments User Categories', 'url' => array('admin')),
);
?>

    <h3>Experiments User Categories</h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>