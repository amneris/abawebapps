<?php
/* @var $this AbaExperimentsUserCategoriesController */
/* @var $model AbaExperimentsUserCategories */

$this->breadcrumbs = array(
  'Aba Experiments User Categories' => array('admin'),
  $model->userCategoryId => array('view', 'id' => $model->userCategoryId),
  'Update',
);

$this->menu = array(
  array('label' => 'List Experiments User Categories', 'url' => array('index')),
  array('label' => 'Create Experiment User Category', 'url' => array('create')),
  array('label' => 'View Experiment User Category', 'url' => array('view', 'id' => $model->userCategoryId)),
  array('label' => 'Manage Experiments User Categories', 'url' => array('admin')),
);
?>

    <h3>Update Experiment User Category: <?php echo $model->userCategoryId; ?></h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>