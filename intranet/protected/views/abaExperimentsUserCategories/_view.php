<?php
/* @var $this AbaExperimentsUserCategoriesController */
/* @var $model AbaExperimentsUserCategories */
?>

<div class="view">

    <b/><?php echo CHtml::encode($data->getAttributeLabel('userCategoryId')); ?>:</b>
    <?php echo CHtml::encode($data->userCategoryId); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('userCategoryDescription')); ?>:</b>
    <?php echo CHtml::encode($data->userCategoryDescription); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('dateAdd')); ?>:</b>
    <?php echo CHtml::encode($data->dateAdd); ?>
    <br/>

</div>