<?php
/* @var $this AbaExperimentsUserCategoriesController */
/* @var $model AbaExperimentsUserCategories */

$this->breadcrumbs = array(
  'Aba Experiments User Categories',
);

$this->menu = array(
  array('label' => 'Create Experiments User Categories', 'url' => array('create')),
  array('label' => 'Manage Experiments User Categories', 'url' => array('admin')),
);
?>

<h3>Experiments User Categories</h3>

<?php $this->widget(
  'zii.widgets.CListView',
  array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
  )
); ?>
