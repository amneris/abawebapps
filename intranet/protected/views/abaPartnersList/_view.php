<?php
/* @var $this AbaPartnersListController */
/* @var $data AbaPartnersList */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPartner')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idPartner), array('view', 'id'=>$data->idPartner)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />


</div>