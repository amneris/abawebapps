<?php
/* @var $this AbaPartnersListController */
/* @var $model AbaPartnersList */

$this->breadcrumbs = array(
  'Aba Partners Lists' => array('admin'),
  $model->name => array('view', 'id' => $model->idPartner),
  'Update',
);

$this->menu = array(
  array('label' => 'List AbaPartnersList', 'url' => array('index')),
  array('label' => 'Create AbaPartnersList', 'url' => array('create')),
  array('label' => 'View AbaPartnersList', 'url' => array('view', 'id' => $model->idPartner)),
  array('label' => 'Manage AbaPartnersList', 'url' => array('admin')),
);
?>

    <h3>Update Partner<?php echo $model->name; ?></h3>

<?php echo $this->renderPartial('_form',
  array(
    'model' => $model,
    'idPartnerPlaceholder' => 'auto',
    'bManuallyEditPartnerGroup' => (isset($bManuallyEditPartnerGroup) ? $bManuallyEditPartnerGroup : false),
    'formAction' => 'update'
  )); ?>