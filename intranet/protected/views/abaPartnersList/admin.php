<?php
/* @var $this AbaPartnersListController */
/* @var $model AbaPartnersList */

$this->breadcrumbs = array(
  'Partner List',
);

$this->menu = array(
  array('label' => 'Create AbaPartnersList', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('aba-partners-list-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Partner list</h3>

<?php echo CHtml::link('Create new partner', array('abaPartnersList/create')); ?>

<?php $this->widget('application.components.widgets.CGridViewABA', array(
  'id' => 'aba-partners-list-grid',
  'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
  'pager' => array(
    'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
    'prevPageLabel' => '<',
    'nextPageLabel' => '>',
    'firstPageLabel' => '<<',
    'lastPageLabel' => '>>',
  ),
  'extraInfo' => array('view', 'update'),
  'dataProvider' => $model->search(),
  'filter' => $model,
  'filterSelector' => '{filter}',
  'columns' => array(
    'idPartner',
    'name',
    'idPartnerGroup' =>
      array(               // related city displayed as a link
        'name' => 'idPartnerGroup',
        'type' => 'partnerGroupList',
        'filter' => HeList::partnerGroupList(),
      ),

    'idCategory' =>
      array(
        'name' => 'idCategory',
        'type' => 'partnerCategoryList',
        'filter' => HeList::partnerCategoryList(),
      ),
    'idBusinessArea' =>
      array(
        'name' => 'idBusinessArea',
        'type' => 'partnerBusinessAreaList',
        'filter' => HeList::partnerBusinessAreaList(),
      ),
    'idType' =>
      array(
        'name' => 'idType',
        'type' => 'partnerTypeList',
        'filter' => HeList::partnerTypeList(),
      ),
    'idGroup' =>
      array(
        'name' => 'idGroup',
        'type' => 'partnerNewGroupList',
        'filter' => HeList::partnerNewGroupList(),
      ),
    'idChannel' =>
      array(
        'name' => 'idChannel',
        'type' => 'partnerChannelList',
        'filter' => HeList::partnerChannelList(),
      ),
    'idCountry' =>
      array(
        'name' => 'idCountry',
        'type' => 'partnerCountryList',
        'filter' => HeList::partnerCountryList(),
      ),
    array(
      'class' => 'CButtonColumnABA',
      'header' => 'Options',
    ),
  ),
)); ?>
