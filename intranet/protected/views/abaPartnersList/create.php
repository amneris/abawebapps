<?php
/* @var $this AbaPartnersListController */
/* @var $model AbaPartnersList */
/* @var $validation string */

$this->breadcrumbs = array(
  'Partners Lists' => array('admin'),
  'Create',
);

$this->menu = array(
  array('label' => 'List AbaPartnersList', 'url' => array('index')),
  array('label' => 'Manage AbaPartnersList', 'url' => array('admin')),
);
?>

    <h3>Create partner</h3>

<?php echo $this->renderPartial('_form',
  array(
    'model' => $model,
    'idPartnerPlaceholder' => $idPartnerPlaceholder,
    'formAction' => 'create',
    'bManuallyEditIdPartner' => $bManuallyEditIdPartner,
    'bManuallyEditPartnerGroup' => $bManuallyEditPartnerGroup
  )); ?>