<?php
/* @var $this AbaPartnersListController */
/* @var $model AbaPartnersList */

$this->breadcrumbs = array(
  'Partner List' => array('admin'),
  $model->name,
);

$this->menu = array(
  array('label' => 'List AbaPartnersList', 'url' => array('index')),
  array('label' => 'Create AbaPartnersList', 'url' => array('create')),
  array('label' => 'Update AbaPartnersList', 'url' => array('update', 'id' => $model->idPartner)),
  array(
    'label' => 'Delete AbaPartnersList',
    'url' => '#',
    'linkOptions' => array(
      'submit' => array('delete', 'id' => $model->idPartner),
      'confirm' => 'Are you sure you want to delete this item?'
    )
  ),
  array('label' => 'Manage AbaPartnersList', 'url' => array('admin')),
);
?>

<h3>View partner <b><?php echo $model->name; ?></b></h3>

<?php $this->widget('application.components.widgets.CDetailViewABA', array(
  'data' => $model,
  'attributes' => array(
    'idPartner',
    'name',
    'idPartnerGroup' =>
      array(               // related city displayed as a link
        'name' => 'idPartnerGroup',
        'type' => 'partnerGroupList+',
      ),
    'idCategory' =>
      array(
        'name' => 'idCategory',
        'type' => 'partnerCategoryList',
        'filter' => HeList::partnerCategoryList(),
      ),
    'idBusinessArea' =>
      array(
        'name' => 'idBusinessArea',
        'type' => 'partnerBusinessAreaList',
        'filter' => HeList::partnerBusinessAreaList(),
      ),
    'idType' =>
      array(
        'name' => 'idType',
        'type' => 'partnerTypeList',
        'filter' => HeList::partnerTypeList(),
      ),
    'idGroup' =>
      array(
        'name' => 'idGroup',
        'type' => 'partnerNewGroupList',
        'filter' => HeList::partnerNewGroupList(),
      ),
    'idChannel' =>
      array(
        'name' => 'idChannel',
        'type' => 'partnerChannelList',
        'filter' => HeList::partnerChannelList(),
      ),
    'idCountry' =>
      array(
        'name' => 'idCountry',
        'type' => 'partnerCountryList',
        'filter' => HeList::partnerCountryList(),
      ),
  ),
)); ?>
