<?php
/* @var $this AbaPartnersListController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Aba Partners Lists',
);

$this->menu=array(
	array('label'=>'Create AbaPartnersList', 'url'=>array('create')),
	array('label'=>'Manage AbaPartnersList', 'url'=>array('admin')),
);
?>

<h3>Partner List</h3>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
