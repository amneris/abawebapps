<?php
/* @var $this AbaPartnersListController */
/* @var $model AbaPartnersList */
/* @var $form CActiveForm */
?>

<?php
if (!isset($idPartnerPlaceholder)) {
    $idPartnerPlaceholder = 'auto';
}
if (!isset($formAction)) {
    $formAction = 'create';
}
if (!isset($bManuallyEditIdPartner)) {
    $bManuallyEditIdPartner = false;
}
if (!isset($bManuallyEditPartnerGroup)) {
    $bManuallyEditPartnerGroup = false;
}
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
      'id' => 'aba-partners-list-form',
      'enableAjaxValidation' => false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php
    $optionsIdCategory = HeList::partnerCategoryList();
    $optionsIdBusinessArea = HeList::partnerBusinessAreaList();
    $optionsIdType = HeList::partnerTypeList();
    $optionsIdGroup = HeList::partnerNewGroupList();
    $optionsIdChannel = HeList::partnerChannelList();
    $optionsIdCountry = HeList::partnerCountryList();
    ?>
    <style>
        #AbaPartnersList_idPartner:focus::-webkit-input-placeholder {
            color: transparent;
        }

        #AbaPartnersList_idPartner:focus:-moz-placeholder {
            color: transparent;
        }
    </style>
    <script>
        function changeIdPartnerEditStatus(chkBox, inputName) {
            if ($(chkBox).is(':checked')) {
                $('#' + inputName).removeAttr('disabled').focus();
            }
            else {
                $('#' + inputName).attr('disabled', 'disabled');
            }
        }
    </script>
    <?php echo $form->errorSummary($model); ?>

    <table id="myForm">
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'idPartner'); ?>
            </td>
            <td>
                <?php if ($formAction == 'create'): ?>
                    <?php
                    $idPartnerParams = array(
                      'size' => 20,
                      'maxlength' => 20,
                      'style' => 'witdh:125px;',
                      'placeholder' => $idPartnerPlaceholder
                    );
                    if (!$bManuallyEditIdPartner) {
                        $idPartnerParams['disabled'] = 'disabled';
                    }
                    $partnerGroupParams = array('style' => 'witdh:125px;');
                    if (!$bManuallyEditPartnerGroup) {
                        $partnerGroupParams['disabled'] = 'disabled';
                    }
                    ?>
                    <?php echo $form->textField($model, 'idPartner', $idPartnerParams); ?>
                    <?php
                    echo '&nbsp;' . CHtml::checkBox("idPartnerEdit", $bManuallyEditIdPartner,
                        array('onclick' => 'javascript:changeIdPartnerEditStatus(this, "AbaPartnersList_idPartner");'));
                    echo '&nbsp;Force ID (Do not use this feature, unless you know what you are doing)';
                    ?><br/>
                    <?php echo $form->error($model, 'idPartner'); ?>
                <?php else: ?>
                    <?php echo $form->hiddenField($model, 'idPartner'); ?>
                    <?php echo $model->idPartner; ?>
                <?php endif; ?>
            </td>
        </tr>
        <?php
        $partnerGroupParams = array('style' => 'witdh:125px;');
        if (!$bManuallyEditPartnerGroup) {
            $partnerGroupParams['disabled'] = 'disabled';
        }
        ?>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'name'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'name', array('size' => 40, 'maxlength' => 60)); ?>
                <?php echo $form->error($model, 'name'); ?>
            </td>
        </tr>
        <tr>
            <td><?php echo $form->labelEx($model, 'idPartnerGroup'); ?></td>
            <td>
                <?php
                $options = HeList::partnerGroupList();
                echo $form->dropDownList($model, 'idPartnerGroup', $options, $partnerGroupParams);
                ?>
                <?php
                echo '&nbsp;' . CHtml::checkBox("idPartnerGroupEdit", $bManuallyEditPartnerGroup,
                    array('onclick' => 'javascript:changeIdPartnerEditStatus(this, "AbaPartnersList_idPartnerGroup");'));
                echo '&nbsp;Change Group (Do not use this feature, unless you know what you are doing)';
                ?>
                <?php echo $form->error($model, 'idPartnerGroup'); ?>
                <br/>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'idCategory'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'idCategory', $optionsIdCategory); ?>
                <?php echo $form->error($model, 'idCategory'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'idBusinessArea'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'idBusinessArea', $optionsIdBusinessArea); ?>
                <?php echo $form->error($model, 'idBusinessArea'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'idType'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'idType', $optionsIdType); ?>
                <?php echo $form->error($model, 'idType'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'idGroup'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'idGroup', $optionsIdGroup); ?>
                <?php echo $form->error($model, 'idGroup'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'idChannel'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'idChannel', $optionsIdChannel); ?>
                <?php echo $form->error($model, 'idChannel'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'idCountry'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'idCountry', $optionsIdCountry); ?>
                <?php echo $form->error($model, 'idCountry'); ?>
            </td>
        </tr>
    </table>

    <div class="row buttons" align="center">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->