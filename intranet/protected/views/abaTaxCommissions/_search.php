<?php
/* @var $this AbaTaxCommissionsController */
/* @var $model AbaTaxCommissions */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget('CActiveForm', array(
      'action' => Yii::app()->createUrl($this->route),
      'method' => 'get',
    )); ?>

    <div class="row">
        <?php echo $form->label($model, 'idCommission'); ?>
        <?php echo $form->textField($model, 'idCommission', array('size' => 10, 'maxlength' => 10)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'paySuppExtId'); ?>
        <?php echo $form->DropDownList($model, 'paySuppExtId', HeList::supplierExtGatewayList(),
          array('empty' => '--Any supplier--')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'idCountry'); ?>
        <?php echo $form->DropDownList($model, 'idCountry', HeList::getUeCountryList(),
          array('empty' => '--Any Country--')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'taxCommissionValue'); ?>
        <?php echo $form->textField($model, 'taxCommissionValue', array('size' => 6, 'maxlength' => 6)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dateStartCommission');
        echo $form->textField($model, 'dateStartCommission');
        echo CHtml::image("images/calendar.jpg", "calendar1", array("id" => "c_buttonStart", "class" => "pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
          array(
            'inputField' => 'AbaTaxCommissions_dateStartCommission',
            'button' => 'c_buttonStart',
            'ifFormat' => '%Y-%m-%d 00:00:00',
          ));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dateEndCommission');
        echo $form->textField($model, 'dateEndCommission');
        echo CHtml::image("images/calendar.jpg", "calendar2", array("id" => "c_buttonEnd", "class" => "pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
          array(
            'inputField' => 'AbaTaxCommissions_dateEndCommission',
            'button' => 'c_buttonEnd',
            'ifFormat' => '%Y-%m-%d 00:00:00',
          ));
        ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->