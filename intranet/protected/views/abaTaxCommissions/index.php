<?php
/* @var $this AbaTaxCommissionsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tax Commissions',
);

$this->menu=array(
	array('label'=>'Create Tax Commissions', 'url'=>array('create')),
	array('label'=>'Manage Tax Commissions', 'url'=>array('admin')),
);
?>

<h1>Tax Commissions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
