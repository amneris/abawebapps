<?php
/* @var $this AbaTaxCommissionsController */
/* @var $model AbaTaxCommissions */

$this->breadcrumbs = array(
  'Tax Commissions' => array('admin'),
  $model->idCommission,
);

$this->menu = array(
  array('label' => 'List Tax Commissions', 'url' => array('index')),
  array('label' => 'Create Tax Commissions', 'url' => array('create')),
  array('label' => 'Update Tax Commissions', 'url' => array('update', 'id' => $model->idCommission)),
  array(
    'label' => 'Delete Tax Commissions',
    'url' => '#',
    'linkOptions' => array(
      'submit' => array('delete', 'id' => $model->idCommission),
      'confirm' => 'Are you sure you want to delete this item?'
    )
  ),
  array('label' => 'Manage Tax Commissions', 'url' => array('admin')),
);
?>

<h1>View Tax Commissions #<?php echo $model->idCommission; ?></h1>

<?php $this->widget('application.components.widgets.CDetailViewABA', array(
  'data' => $model,
  'attributes' => array(
    'idCommission',
    'paySuppExtId' =>
      array(
        'name' => 'paySuppExtId',
        'type' => 'supplierExtGatewayList',
      ),
    'idCountry' =>
      array(
        'name' => 'idCountry',
        'type' => 'countryUeList',
      ),
    'taxCommissionValue',
    'dateStartCommission',
    'dateEndCommission',
  ),
)); ?>
