<?php
/* @var $this AbaTaxCommissionsController */
/* @var $model AbaTaxCommissions */

$this->breadcrumbs=array(
	'Tax Commissions'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Tax Commissions', 'url'=>array('index')),
	array('label'=>'Manage Tax Commissions', 'url'=>array('admin')),
);
?>

<h1>Create Tax Commissions</h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>