<?php
/* @var $this AbaTaxCommissionsController */
/* @var $model AbaTaxCommissions */

$this->breadcrumbs = array('Tax Commissions');

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tax-commissions-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
//");
?>

<h3>Tax Commissions</h3>

<?php echo CHtml::link('Create a new tax commission', array('abaTaxCommissions/create'));
echo "<br>"; ?>
<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>

<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
      'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('application.components.widgets.CGridViewABA', array(
  'id' => 'tax-commissions-grid',
  'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
  'pager' => array(
    'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
    'prevPageLabel' => '<',
    'nextPageLabel' => '>',
    'firstPageLabel' => '<<',
    'lastPageLabel' => '>>',
  ),
  'dataProvider' => $model->search(),
  'filter' => $model,
  'columns' => array(
    'idCommission',
    'paySuppExtId' =>
      array(
        'name' => 'paySuppExtId',
        'type' => 'supplierExtGatewayList',
        'filter' => HeList::supplierExtGatewayList(),
        'htmlOptions' => array('width' => "120px")
      ),
    'idCountry' =>
      array(
        'name' => 'idCountry',
        'type' => 'countryUeList',
        'filter' => HeList::getUeCountryList(),
        'htmlOptions' => array('width' => "120px")
      ),
    'taxCommissionValue',
    'dateStartCommission',
    'dateEndCommission',
    array(
      'class' => 'CButtonColumn',
    ),
  ),
)); ?>
