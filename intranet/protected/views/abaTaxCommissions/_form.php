<?php
/* @var $this AbaTaxCommissionsController */
/* @var $model AbaTaxCommissions */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tax-commissions-form',
	// Please note: When you enable ajax validation, make sure the correspondingº
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'idCountry'); ?>
        <?php echo $form->DropDownList($model, 'idCountry', HeList::getUeCountryList(), array('empty' => '--Any Country--')); ?>
		<?php echo $form->error($model,'idCountry'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'paySuppExtId'); ?>
        <?php echo $form->DropDownList($model, 'paySuppExtId', HeList::supplierExtGatewayList(), array('empty' => '--Select supplier--')); ?>
		<?php echo $form->error($model,'paySuppExtId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'taxCommissionValue'); ?>
		<?php echo $form->textField($model,'taxCommissionValue',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'taxCommissionValue'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dateStartCommission'); ?>
		<?php echo $form->textField($model,'dateStartCommission'); ?>
        <?php
        echo CHtml::image("images/calendar.jpg","calendar1", array("id"=>"c_buttonStart","class"=>"pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
            array(
                'inputField'=>'AbaTaxCommissions_dateStartCommission',
                'button'=>'c_buttonStart',
                'ifFormat'=>'%Y-%m-%d 00:00:00',
            ));
        ?>
        <?php echo $form->error($model,'dateStartCommission'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dateEndCommission'); ?>
		<?php echo $form->textField($model,'dateEndCommission'); ?>
        <?php
        echo CHtml::image("images/calendar.jpg","calendar2", array("id"=>"c_buttonEnd","class"=>"pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
            array(
                'inputField'=>'AbaTaxCommissions_dateEndCommission',
                'button'=>'c_buttonEnd',
                'ifFormat'=>'%Y-%m-%d 00:00:00',
            ));
        ?>
        <?php echo $form->error($model,'dateEndCommission'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->