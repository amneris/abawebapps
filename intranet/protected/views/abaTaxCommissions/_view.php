<?php
/* @var $this AbaTaxCommissionsController */
/* @var $data AbaTaxCommissions */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('idCommission')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->idCommission), array('view', 'id' => $data->idCommission)); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('paySuppExtId')); ?>:</b>
    <?php echo CHtml::encode($data->paySuppExtId); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('idCountry')); ?>:</b>
    <?php echo CHtml::encode($data->idCountry); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('taxCommissionValue')); ?>:</b>
    <?php echo CHtml::encode($data->taxCommissionValue); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('dateStartCommission')); ?>:</b>
    <?php echo CHtml::encode($data->dateStartCommission); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('dateEndCommission')); ?>:</b>
    <?php echo CHtml::encode($data->dateEndCommission); ?>
    <br/>

</div>