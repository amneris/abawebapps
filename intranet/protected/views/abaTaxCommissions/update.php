<?php
/* @var $this AbaTaxCommissionsController */
/* @var $model AbaTaxCommissions */

$this->breadcrumbs=array(
	'Tax Commissions'=>array('admin'),
	$model->idCommission=>array('view','id'=>$model->idCommission),
	'Update',
);

$this->menu=array(
	array('label'=>'List Tax Commissions', 'url'=>array('index')),
	array('label'=>'Create Tax Commissions', 'url'=>array('create')),
	array('label'=>'View Tax Commissions', 'url'=>array('view', 'id'=>$model->idCommission)),
	array('label'=>'Manage Tax Commissions', 'url'=>array('admin')),
);
?>

<h1>Update Tax Commissions <?php echo $model->idCommission; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>