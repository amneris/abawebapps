<?php
/* @var $this AbaPartnerBusinessareasController */
/* @var $model AbaPartnerbusinessareas */

$this->breadcrumbs = array(
  'Business areas' => array('admin'),
  $model->idBusinessArea,
);

$this->menu = array(
  array('label' => 'List Business areas', 'url' => array('index')),
  array('label' => 'Create Business area', 'url' => array('create')),
  array('label' => 'Update Business area', 'url' => array('update', 'id' => $model->idBusinessArea)),
  array(
    'label' => 'Delete Business area',
    'url' => '#',
    'linkOptions' => array(
      'submit' => array('delete', 'id' => $model->idBusinessArea),
      'confirm' => 'Are you sure you want to delete this item?'
    )
  ),
  array('label' => 'Manage Business areas', 'url' => array('admin')),
);
?>

<h3>View Business areas <b><?php echo $model->idBusinessArea . ": " . $model->nameBusinessArea; ?></b></h3>

<?php $this->widget(
  'application.components.widgets.CDetailViewABA',
  array(
    'data' => $model,
    'attributes' => array(
      'idBusinessArea',
      'nameBusinessArea',
      'dateAdd',
    ),
  )
); ?>
