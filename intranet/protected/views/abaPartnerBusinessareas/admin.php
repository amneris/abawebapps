<?php
/* @var $this AbaPartnerBusinessareasController */
/* @var $model AbaPartnerbusinessareas */

$this->breadcrumbs = array(
  'Business areas',
);

$this->menu = array(
  array('label' => 'Create Business area', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript(
  'search',
  "
   $('.search-button').click(function(){
     $('.search-form').toggle();
     return false;
   });
   $('.search-form form').submit(function(){
     $.fn.yiiGridView.update('aba-partnerbusinessareas-grid', {
       data: $(this).serialize()
     });
     return false;
   });
   "
);
?>

<h3>Partners Business areas</h3>

<?php echo CHtml::link('Create new Business area', array('AbaPartnerBusinessareas/create')); ?>
<br>
<div class="search-form" style="display:none">
    <?php $this->renderPartial(
      '_search',
      array(
        'model' => $model,
      )
    ); ?>
</div><!-- search-form -->

<?php $this->widget(
  'application.components.widgets.CGridViewABA',
  array(
    'id' => 'aba-partnerbusinessareas-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager' => array(
      'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
      'prevPageLabel' => '<',
      'nextPageLabel' => '>',
      'firstPageLabel' => '<<',
      'lastPageLabel' => '>>',
    ),
    'extraInfo' => array('view', 'update'),
    'dataProvider' => $model->search(),
    'filter' => $model,
    'filterSelector' => '{filter}',
    'columns' => array(
      'idBusinessArea',
      'nameBusinessArea',
      'dateAdd',
      array(
        'class' => 'CButtonColumnABA',
        'header' => 'Options',
      ),
    ),
  )
); ?>
