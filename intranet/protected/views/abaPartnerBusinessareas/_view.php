<?php
/* @var $this AbaPartnerBusinessareasController */
/* @var $model AbaPartnerbusinessareas */
?>

<div class="view">

    <b/><?php echo CHtml::encode($data->getAttributeLabel('idBusinessArea')); ?>:</b>
    <?php echo CHtml::encode($data->idBusinessArea); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('nameBusinessArea')); ?>:</b>
    <?php echo CHtml::encode($data->nameBusinessArea); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('dateAdd')); ?>:</b>
    <?php echo CHtml::encode($data->dateAdd); ?>
    <br/>

</div>