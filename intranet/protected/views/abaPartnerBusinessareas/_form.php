<?php
/* @var $this AbaPartnerBusinessareasController */
/* @var $model AbaPartnerbusinessareas */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget(
      'CActiveForm',
      array(
        'id' => 'aba-partnerbusinessareas-form',
        'enableAjaxValidation' => false,
      )
    );

    $cssPrefix = (isset($wAction) AND $wAction == 'update' ? 'Cm' : '');
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <table id="myForm">
        <?php if (is_numeric($model->idBusinessArea)): ?>
            <tr>
                <td>
                    <?php echo $form->labelEx($model, 'idBusinessArea'); ?>
                </td>
                <td>
                    <?php echo $model->idBusinessArea; ?>
                    <?php echo $form->error($model, 'idBusinessArea'); ?>
                </td>
            </tr>
        <?php endif; ?>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'nameBusinessArea'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'nameBusinessArea',
                  array('size' => 30, 'maxlength' => 45)); ?>
                <?php echo $form->error($model, 'nameBusinessArea'); ?>
            </td>
        </tr>
        <?php if (is_numeric($model->idBusinessArea)): ?>
            <tr>
                <td>
                    <?php echo $form->labelEx($model, 'dateAdd'); ?>
                </td>
                <td>
                    <?php echo $model->dateAdd; ?>
                </td>
            </tr>
        <?php endif; ?>
    </table>
    <div class="row buttons" align="center">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->