<?php
/* @var $this AbaPartnerBusinessareasController */
/* @var $model AbaPartnerbusinessareas */

$this->breadcrumbs = array(
  'Business areas' => array('admin'),
  'Create',
);

$this->menu = array(
  array('label' => 'Manage Business area', 'url' => array('admin')),
);
?>

    <h3>Business areas</h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>