<?php
/* @var $this AbaPartnerBusinessareasController */
/* @var $model AbaPartnerbusinessareas */

$this->breadcrumbs = array(
  'Aba Business areas' => array('admin'),
  $model->idBusinessArea => array('view', 'id' => $model->idBusinessArea),
  'Update',
);

$this->menu = array(
  array('label' => 'List Business areas', 'url' => array('index')),
  array('label' => 'Create Business area', 'url' => array('create')),
  array('label' => 'View Business areas', 'url' => array('view', 'id' => $model->idBusinessArea)),
  array('label' => 'Manage Business areas', 'url' => array('admin')),
);
?>

    <h3>Update Business area: <?php echo $model->idBusinessArea; ?></h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>