<?php
/* @var $this AbaPartnerBusinessareasController */
/* @var $model AbaPartnerbusinessareas */

$this->breadcrumbs = array(
  'Aba Business areas',
);

$this->menu = array(
  array('label' => 'Create Business areas', 'url' => array('create')),
  array('label' => 'Manage Business areas', 'url' => array('admin')),
);
?>

<h3>Business areas</h3>

<?php $this->widget(
  'zii.widgets.CListView',
  array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
  )
); ?>
