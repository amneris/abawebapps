<?php
/* @var $this AbaUserCreditFormsController */
/* @var $model AbaUserCreditForms */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'userId'); ?>
		<?php echo $form->textField($model,'userId',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kind'); ?>
		<?php echo $form->textField($model,'kind',array('size'=>2,'maxlength'=>2)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cardNumber'); ?>
		<?php echo $form->textField($model,'cardNumber'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cardYear'); ?>
		<?php echo $form->textField($model,'cardYear'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cardMonth'); ?>
		<?php echo $form->textField($model,'cardMonth'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cardCvc'); ?>
		<?php echo $form->textField($model,'cardCvc'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cardName'); ?>
		<?php echo $form->textField($model,'cardName',array('size'=>45,'maxlength'=>45)); ?>
	</div>
<!--
	<div class="row">
		<?php /*echo $form->label($model,'cardLastName'); 
		 echo $form->textField($model,'cardLastName',array('size'=>45,'maxlength'=>45)); */?>
	</div> -->

	<div class="row">
		<?php echo $form->label($model,'default'); ?>
		<?php echo $form->textField($model,'default'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->