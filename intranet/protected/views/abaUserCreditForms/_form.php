<?php
/* @var $this AbaUserCreditFormsController */
/* @var $model AbaUserCreditForms */
/* @var $moLastPayment AbaPayments*/
/* @var $form CActiveForm */
?>
<div class="form">
	<p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php $aMethods = HeList::supplierExtGatewayList(); ?>
    <p class="note">Last payment with this data bank details was done via <?php echo $aMethods[$moLastPayment->paySuppExtId]; ?></p>

	<?php echo $form->errorSummary($model); 
	?>

    <table id="myForm">
        
        <tr>
            <td>
                <?php echo $form->labelEx($model,'userId'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'userId',array('size'=>10,'maxlength'=>4, "disabled"=>"true"));
                      echo $form->error($model,'userId'); ?>
            </td>
        </tr>
        
        <tr class="highlight">
            <td>
                <?php echo $form->labelEx($model,'kind'); ?>
            </td>
            <td>
                <?php
                    $isDisabled = array();
//                    $isDisabled = array("disabled"=>"true");
                    if($model->kind!=KIND_CC_PAYPAL){
                        $isDisabled = array();
                    }
                    echo $form->DropDownList($model,'kind',HeList::creditCardDescription(), $isDisabled );
                    echo $form->error($model,'kind');
                ?>
            </td>
        </tr>
        
        <tr class="highlight">
            <td>
                <?php echo $form->labelEx($model,'cardNumber'); ?>
            </td>
            <td>

                <?php
                echo $form->textField($model, 'cardNumber', array('value' => AbaUserCreditForms::AES_decode($model->cardNumber)));
                echo $form->error($model, 'cardNumber');?>
            </td>
        </tr>
        
        <tr class="highlight">
            <td>
                <?php echo $form->labelEx($model,'cardYear'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'cardYear', array('value'=>AbaUserCreditForms::AES_decode($model->cardYear))); ?>
                <?php echo $form->error($model,'cardYear'); ?>
            </td>
        </tr>
        
        <tr class="highlight">
            <td>
                <?php echo $form->labelEx($model,'cardMonth'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'cardMonth', array('value'=>AbaUserCreditForms::AES_decode($model->cardMonth),'minlength'=>2)).'(mm, two digits)'; ?>
                <?php echo $form->error($model,'cardMonth'); ?>
            </td>
        </tr>
        
        <tr class="highlight">
            <td>
                <?php echo $form->labelEx($model,'cardCvc'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'cardCvc', array('value'=>AbaUserCreditForms::AES_decode($model->cardCvc))); ?>
                <?php echo $form->error($model,'cardCvc'); ?>
            </td>
        </tr>
        
         <tr class="highlight">
            <td>
                <?php echo $form->labelEx($model,'cardName'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'cardName',array('size'=>45,'maxlength'=>45)); ?>
                <?php echo $form->error($model,'cardName'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'default'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'default', array("disabled"=>"true")); ?>
                <?php echo $form->error($model,'default'); ?>
            </td>
        </tr>
        <tr class="highlight">
            <td>
                <?php echo $form->labelEx($model,'typeCpf'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'typeCpf', HeList::getTypeCpfList()  );
                echo $form->error($model,'typeCpf'); ?>
            </td>
        </tr>
        <tr class="highlight">
            <td>
                <?php echo $form->labelEx($model, 'cpfBrasil'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'cpfBrasil', array('value'=>AbaUserCreditForms::AES_decode($model->cpfBrasil), 'size'=>30,'maxlength'=>14)); ?>
                <?php echo $form->error($model, 'cpfBrasil'); ?>
            </td>
        </tr>
        <tr class="highlight">
            <td>
                <?php echo $form->labelEx($model,'registrationId'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'registrationId', array('size'=>40,'maxlength'=>32)); ?>
                <?php echo $form->error($model,'registrationId'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'lastModified'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'lastModified', array( 'disabled'=>'true', 'size'=>20, 'maxlength'=>20 )); ?>
                <?php echo $form->error($model,'lastModified'); ?>
            </td>
        </tr>
    </table>


</div>