<?php
/* @var $this AbaPaymentsController */
/* @var $model AbaPayments */
/* @var $actionTitle string */

$this->breadcrumbs=array(
    'User'=> array('abaUser/admin_id','id'=>$model->userId),
    'Paylines from user '.$model->userId=> array('abaPayments/admin_id','id'=>$model->userId),
    'Bank details for user '.$model->userId=> array('abaUserCreditForms/admin_id','id'=>$model->userId),
    $actionTitle,
);
?>

<h3>
    <?php echo strtoupper($actionTitle);?>: Credit card via La Caixa, AllPago or PayPal account from user <?php echo $model->userId; ?>
</h3>

</br>
