<?php
/* @var $this AbaUserCreditFormsController */
/* @var $model AbaUserCreditForms */
/* @var $moPayments AbaPayments */


$this->renderPartial("_formheader",array(
    "model"=>$model,
    "actionTitle"=>"View full details"));



$this->menu=array(
/*	array('label'=>'List AbaUserCreditForms', 'url'=>array('index')),
	array('label'=>'Create AbaUserCreditForms', 'url'=>array('create')),
	array('label'=>'Update AbaUserCreditForms', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AbaUserCreditForms', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),*/
	array('label'=>'Manage AbaUserCreditForms', 'url'=>array('admin')),
);
?>

<h3>View credit card information for user: <?php echo $model->cardName; ?></h3>

<?php $this->widget('application.components.widgets.CDetailViewABA', array(
	'data'=>$model,
	'attributes'=>array(
		'userId',
		'id',
		'kind'=>
		array(               // related city displayed as a link
              'name'=>'kind',
              'type'=>'cardsList',
          ),
		'cardNumber'=>
		array(               // related city displayed as a link
              'name'=>'cardNumber',
              'type'=>'AES',
          ),
		
		'cardYear'=>
		array(               // related city displayed as a link
              'name'=>'cardYear',
              'type'=>'AES',
          ),
		'cardMonth'=>
		array(               // related city displayed as a link
              'name'=>'cardMonth',
              'type'=>'AES',
          ),
		'cardCvc'=>
		array(               // related city displayed as a link
              'name'=>'cardCvc',
              'type'=>'AES',
          ),
		'cardName',
		'default',
        'cpfBrasil'=>
        array(               // related city displayed as a link
            'name'=>'cpfBrasil',
            'type'=>'AES',
        ),
        'registrationId'
	),
));

$this->renderPartial("_formfooter",array( "model"=>$model, "moPayments"=> $moPayments));

