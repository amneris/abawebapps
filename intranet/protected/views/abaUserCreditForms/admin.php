<?php
/* @var $this AbaUserCreditFormsController */
/* @var $model AbaUserCreditForms */

if ($model->userId != "")
	$this->breadcrumbs=array(
		'Bank Details'=>array('admin'),
		'Current User'=>array('abaUser/admin_id','id'=>HeString::getIntValue($model->userId)),
        'Paylines from user '.$model->userId=> array('abaPayments/admin_id','id'=>$model->userId),
	);
else
	$this->breadcrumbs=array(
		'Bank Details',
	);

$this->menu=array(
//	array('label'=>'List AbaUserCreditForms', 'url'=>array('index')),
	array('label'=>'Create AbaUserCreditForms', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('aba-user-credit-forms-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Bank details</h3>

<?php /*echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); */ ?>
<div class="search-form" style="display:none">
<?php /* $this->renderPartial('_search',array(
	'model'=>$model,
)); */ ?>
</div><!-- search-form -->

<?php $this->widget('application.components.widgets.CGridViewABA', array(
	'id'=>'aba-user-credit-forms-grid',
	'dataProvider'=>$model->search(),
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
                    'prevPageLabel'  => '<',
                    'nextPageLabel'  => '>',
                    'firstPageLabel' => '<<',
                    'lastPageLabel'  => '>>',),
	'filter'=>$model,
	'extraInfo'=>array('view','update'),
	'extraButtons'=>array(
		'extraButton1'=> array(
	//	'iconExtraLink'=>'user.png',
		'textExtraLink'=>'User',
		'classExtraLink'=>'payments',
		'urlExtraLink'=>'"abaUser/admin_id",array("id"=>$data->userId)'),
		
		'extraButton2'=> array(
	//	'iconExtraLink'=>'user.png',
		'textExtraLink'=>'Payments',
		'classExtraLink'=>'payments',
		'urlExtraLink'=>'"abaPayments/admin_id",array("id"=>$data->userId)'),
	),
	'columns'=>array(
        'id',
        'default'=> array(
                    'header'=>'Active',
                    'name'=>'default',
                    'type'=>'boolean',
                    ),
		'userEmail'=> array(
			        'name'=>'userId',
                    'type'=>'#user#id#email',
                    ),
        'cardName',
		'kind'=>    array(
                          'name'=>'kind',
                          'type'=>'cardsList',
                          'filter'=>HeList::creditCardDescription(),
                      ),
		'cardNumber'=>array(
                          'name'=>'cardNumber',
                          'type'=>'AES',

                      ),
		'cardYear'=> array(
                      'name'=>'cardYear',
                      'type'=>'AES',
                      'filter'=>'',
                    ),
		'cardMonth'=> array(
                      'name'=>'cardMonth',
                      'type'=>'AES',
                      'filter'=>'',
                    ),
		'cardCvc'=> array(
                      'name'=>'cardCvc',
                      'type'=>'AES',
                      'filter'=>'',
                    ),
		array(
			'class'=>'CButtonColumnABA',
            'header'=>'Options',
		),
	),
    )
   );
