<?php
/* @var $this AbaUserCreditFormsController */
/* @var $moUserCreditForm AbaUserCreditForms */
/* @var $moPayPending AbaPayments*/


$this->renderPartial("_formheader",array(
    "model"=>$moUserCreditForm,
    "actionTitle"=>"Change Credit Card"));


$this->menu=array(
    array('label'=>'List AbaUserCreditForms', 'url'=>array('index')),
    array('label'=>'Create AbaUserCreditForms', 'url'=>array('create')),
    array('label'=>'View AbaUserCreditForms', 'url'=>array('view', 'id'=>$moUserCreditForm->id)),
    array('label'=>'Manage AbaUserCreditForms', 'url'=>array('admin')),
);

?>

<h3>Change to new credit card for the pending payment id <?php echo $moPayPending->id; ?></h3>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id'=>'aba-user-credit-forms-form',
    'enableAjaxValidation'=>false) );

    echo $this->renderPartial('_form', array('model'=>$moUserCreditForm, 'moLastPayment'=>$moPayPending, 'form'=>$form));
?>

    <div class="row buttons" align="center">
        <?php if ($moUserCreditForm->isNewRecord){
            echo CHtml::submitButton('Create', array( "class"=>"buttonsAction"));
        } else{
            echo CHtml::submitButton('Save', array( "disabled"=>true,"class"=>"buttonsAction"));
        }

        echo "&nbsp;";
        echo CHtml::button('Cancel', array("class"=>"buttonsAction", 'onClick'=>'window.location="'.Yii::app()->createUrl('abaUserCreditForms/admin_id',
            array('id'=>$moUserCreditForm->userId)).'";')); ?>
    </div>
<?php $this->endWidget(); ?>