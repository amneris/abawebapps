<?php
/* @var $this AbaUserCreditFormsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Aba User Credit Forms',
);

$this->menu=array(
	array('label'=>'Create AbaUserCreditForms', 'url'=>array('create')),
	array('label'=>'Manage AbaUserCreditForms', 'url'=>array('admin')),
);
?>

<h3>Aba User Credit Forms</h3>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
