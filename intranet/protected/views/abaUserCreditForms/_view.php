<?php
/* @var $this AbaUserCreditFormsController */
/* @var $data AbaUserCreditForms */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userId')); ?>:</b>
	<?php echo CHtml::encode($data->userId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kind')); ?>:</b>
	<?php echo CHtml::encode($data->kind); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cardNumber')); ?>:</b>
	<?php echo CHtml::encode($data->cardNumber); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cardYear')); ?>:</b>
	<?php echo CHtml::encode($data->cardYear); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cardMonth')); ?>:</b>
	<?php echo CHtml::encode($data->cardMonth); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cardCvc')); ?>:</b>
	<?php echo CHtml::encode($data->cardCvc); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('cardName')); ?>:</b>
	<?php echo CHtml::encode($data->cardName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cardLastName')); ?>:</b>
	<?php echo CHtml::encode($data->cardLastName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('default')); ?>:</b>
	<?php echo CHtml::encode($data->default); ?>
	<br />

	*/ ?>

</div>