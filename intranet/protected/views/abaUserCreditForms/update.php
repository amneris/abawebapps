<?php
/* @var $this AbaUserCreditFormsController */
/* @var $model AbaUserCreditForms */
/* @var $moLastPayment AbaPayments*/


$this->renderPartial("_formheader",array(
    "model"=>$model,
    "actionTitle"=>"Update Credit Card"));



$this->menu=array(
	array('label'=>'List AbaUserCreditForms', 'url'=>array('index')),
	array('label'=>'Create AbaUserCreditForms', 'url'=>array('create')),
	array('label'=>'View AbaUserCreditForms', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AbaUserCreditForms', 'url'=>array('admin')),
);
?>

<h3>Update credit card information for user: <?php echo $model->cardName; ?></h3>
<?php
    /* @var CActiveForm $form */
    $form = $this->beginWidget('CActiveForm', array(
                                                'id'=>'aba-user-credit-forms-form',
                                                'enableAjaxValidation'=>false) );
     echo $this->renderPartial('_form', array('model'=>$model, 'moLastPayment'=>$moLastPayment, 'form'=>$form)); ?>
    <div class="row buttons" align="center">
            <?php if ($model->isNewRecord){
                    echo CHtml::submitButton('Create', array( "class"=>"buttonsAction"));
                  } else{
                    echo CHtml::submitButton('Save', array( "disabled"=>true,"class"=>"buttonsAction"));
                  }

                  echo "&nbsp;";
                  echo CHtml::button('Cancel', array("class"=>"buttonsAction", 'onClick'=>'window.location="'.Yii::app()->createUrl('abaUserCreditForms/admin_id',
                                                array('id'=>$model->userId)).'";')); ?>
    </div>
<?php $this->endWidget(); ?>