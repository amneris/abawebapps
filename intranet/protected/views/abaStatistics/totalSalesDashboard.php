<?php
/* @var $this AbaPaymentsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Sales dashboard',
);
?>

<h3>Sales dashboard</h3>

<?php

$dollar = new AbaCurrency();
$dollar->getCurrencyById('USD');
$dollarToday = $dollar->outEur;

$real = new AbaCurrency();
$real->getCurrencyById('BRL');
$realToday = $real->outEur;

$peso = new AbaCurrency();
$peso->getCurrencyById('MXN');
$pesoToday = $peso->outEur;

//function just to make syntax more easy
function toMoney($number)
{
    $money = number_format($number,2,',','.');
    return $money;
}

?>

<div style="text-align: center; font-size: 15px">
    Sales and renovations for period from <b><?php echo $startDate ?></b> to <b><?php echo $endDate ?> </b><br><br>
    <?php
        echo CHtml::form(Yii::app()->createUrl('abaStatistics/totalSalesDashboard'), 'post', array('id'=>'form', 'class'=>'myForm'));
        echo "range from ";
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'startDate',
            'value' => $startDate,
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth'=>true,
                'changeYear'=>true,
            ),
                'htmlOptions' => array(
                'size' => '10',
                'maxlength' => '10',
            ),
        ));
        echo " to ";
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'endDate',
            'value' => $endDate,
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth'=>true,
                'changeYear'=>true,
            ),
            'htmlOptions' => array(
                'size' => '10',
                'maxlength' => '10',
            ),
        ));

        echo CHtml::button("Search",array('onclick' => 'this.form.submit();','title'=>"Search",'id'=>'searchButton'));
        echo CHtml::endForm();
    ?>
</div>

<br>
<div style="width: 100%; text-align: center;"><button id="showVatBut" onclick="showVat();">Show VAT</button></div>
<br>

<table id="salesTable">
    <tr>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            Type of Sale
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center">
            Currency
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Average Ticket
        </td>
        <td class="withVat" style="background-color: #9ebacf; color: #ffffff; font-weight: bold; text-align: center; width: 200px; display: none;">
            Average with VAT
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Quantity
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Amount
        </td>
        <td class="withVat" style="background-color: #9ebacf; color: #ffffff; font-weight: bold; text-align: center; width: 200px; display: none;">
            Amount with  VAT
        </td>
    </tr>

    <tr>
        <td>
            New sale
        </td>
        <td style="text-align: center">
            US Dollar
        </td>
        <td style="text-align: center">
            <?php
                if($total['countUSD']>0)
                    echo toMoney(($total['totalUSDnoVat']* $dollarToday) /$total['countUSD']);
            ?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <?php
            if($total['countUSD']>0)
                echo toMoney(($total['totalUSD']* $dollarToday) /$total['countUSD']);
            ?>€
        </td>
        <td style="text-align: right;">
            <?php echo $total['countUSD'] ?>
        </td>
        <td style="text-align: right;">
            <span style="font-size: 11px">*(<?php echo toMoney($total['totalUSDnoVat'])."$)"; ?></span>
            <?php echo toMoney($total['totalUSDnoVat']* $dollarToday)?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <span style="font-size: 11px">*(<?php echo toMoney($total['totalUSD'])."$)"; ?></span>
            <?php echo toMoney($total['totalUSD']* $dollarToday)?>€
        </td>

    </tr>

    <tr>
        <td>
            Renovation
        </td>
        <td style="text-align: center">
            US Dollar
        </td>
        <td style="text-align: center">
            <?php
                if($total['countRenovationUSD']>0)
                    echo toMoney(($total['totalRenovationUSDnoVat'] * $dollarToday) / $total['countRenovationUSD']);
            ?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <?php
            if($total['countRenovationUSD']>0)
                echo toMoney(($total['totalRenovationUSD'] * $dollarToday) / $total['countRenovationUSD']);
            ?>€
        </td>
        <td style="text-align: right;">
            <?php echo $total['countRenovationUSD'] ?>
        </td>
        <td style="text-align: right;">
            <span style="font-size: 11px">*(<?php echo toMoney($total['totalRenovationUSDnoVat'])."$)" ?></span>
            <?php echo toMoney($total['totalRenovationUSDnoVat'] * $dollarToday)?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <span style="font-size: 11px">*(<?php echo toMoney($total['totalRenovationUSD'])."$)" ?></span>
            <?php echo toMoney($total['totalRenovationUSD'] * $dollarToday)?>€
        </td>
    </tr>

    <tr>
        <td>
            New sale
        </td>
        <td style="text-align: center">
            BRL Real
        </td>
        <td style="text-align: center">
            <?php
            if($total['countBRL']>0)
                echo toMoney(($total['totalBRLnoVat']* $realToday) /$total['countBRL']);
            ?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <?php
            if($total['countBRL']>0)
                echo toMoney(($total['totalBRL']* $realToday) /$total['countBRL']);
            ?>€
        </td>
        <td style="text-align: right;">
            <?php echo $total['countBRL'] ?>
        </td>
        <td style="text-align: right;">
            <span style="font-size: 11px">*(<?php echo toMoney($total['totalBRLnoVat'])."BRL)"; ?></span>
            <?php echo toMoney($total['totalBRLnoVat']* $realToday)?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <span style="font-size: 11px">*(<?php echo toMoney($total['totalBRL'])."BRL)"; ?></span>
            <?php echo toMoney($total['totalBRL']* $realToday)?>€
        </td>
    </tr>

    <tr>
        <td>
            Renovation
        </td>
        <td style="text-align: center">
            BRL Real
        </td>
        <td style="text-align: center">
            <?php
            if($total['countRenovationBRL']>0)
                echo toMoney(($total['totalRenovationBRLnoVat'] * $realToday) / $total['countRenovationBRL']);
            ?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <?php
            if($total['countRenovationBRL']>0)
                echo toMoney(($total['totalRenovationBRL'] * $realToday) / $total['countRenovationBRL']);
            ?>€
        </td>
        <td style="text-align: right;">
            <?php echo $total['countRenovationBRL'] ?>
        </td>
        <td style="text-align: right;">
            <span style="font-size: 11px">*(<?php echo toMoney($total['totalRenovationBRLnoVat'])."BRL)" ?></span>
            <?php echo toMoney($total['totalRenovationBRLnoVat'] * $realToday)?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <span style="font-size: 11px">*(<?php echo toMoney($total['totalRenovationBRL'])."BRL)" ?></span>
            <?php echo toMoney($total['totalRenovationBRL'] * $realToday)?>€
        </td>
    </tr>

    <tr>
        <td>
            New sale
        </td>
        <td style="text-align: center">
            Euro
        </td>
        <td style="text-align: center">
            <?php
                if($total['countEUR']>0)
                    echo toMoney($total['totalEURnoVat'] / $total['countEUR']);
            ?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <?php
            if($total['countEUR']>0)
                echo toMoney($total['totalEUR'] / $total['countEUR']);
            ?>€
        </td>
        <td style="text-align: right;">
            <?php echo $total['countEUR'] ?>
        </td>
        <td style="text-align: right;">
            <?php echo toMoney($total['totalEURnoVat'])."€" ?>
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <?php echo toMoney($total['totalEUR'])."€" ?>
        </td>
    </tr>

    <tr>
        <td>
            Renovation
        </td>
        <td style="text-align: center">
            Euro
        </td>
        <td style="text-align: center">
            <?php
                if($total['countRenovationEUR']>0)
                    echo toMoney($total['totalRenovationEURnoVat'] / $total['countRenovationEUR']);
            ?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <?php
            if($total['countRenovationEUR']>0)
                echo toMoney($total['totalRenovationEUR'] / $total['countRenovationEUR']);
            ?>€
        </td>
        <td style="text-align: right;">
            <?php echo $total['countRenovationEUR'] ?>
        </td>
        <td style="text-align: right;">
            <?php echo toMoney($total['totalRenovationEURnoVat'])."€" ?>
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <?php echo toMoney($total['totalRenovationEUR'])."€" ?>
        </td>
    </tr>

    <tr>
        <td>
            New sale
        </td>
        <td style="text-align: center">
            MXN Peso mexicano
        </td>
        <td style="text-align: center">
            <?php
            if($total['countMXN']>0)
                echo toMoney(($total['totalMXNnoVat'] * $pesoToday) / $total['countMXN']);
            ?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <?php
            if($total['countMXN']>0)
                echo toMoney(($total['totalMXN'] * $pesoToday) / $total['countMXN']);
            ?>€
        </td>
        <td style="text-align: right;">
            <?php echo $total['countMXN'] ?>
        </td>
        <td style="text-align: right;">
            <span style="font-size: 11px">*(<?php echo toMoney($total['totalMXNnoVat'])."MXN)"; ?></span>
            <?php echo toMoney($total['totalMXNnoVat'] * $pesoToday)?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <span style="font-size: 11px">*(<?php echo toMoney($total['totalMXN'])."MXN)"; ?></span>
            <?php echo toMoney($total['totalMXN'] * $pesoToday)?>€
        </td>
    </tr>

    <tr>
        <td>
            Renovation
        </td>
        <td style="text-align: center">
            MXN Peso mexicano
        </td>
        <td style="text-align: center">
            <?php
            if($total['countRenovationMXN']>0)
                echo toMoney(($total['totalRenovationMXNnoVat'] * $pesoToday) / $total['countRenovationMXN']);
            ?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <?php
            if($total['countRenovationMXN']>0)
                echo toMoney(($total['totalRenovationMXN'] * $pesoToday) / $total['countRenovationMXN']);
            ?>€
        </td>
        <td style="text-align: right;">
            <?php echo $total['countRenovationMXN'] ?>
        </td>
        <td style="text-align: right;">
            <span style="font-size: 11px">*(<?php echo toMoney($total['totalRenovationMXNnoVat'])."MXN)" ?></span>
            <?php echo toMoney($total['totalRenovationMXNnoVat'] * $pesoToday)?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <span style="font-size: 11px">*(<?php echo toMoney($total['totalRenovationMXN'])."MXN)" ?></span>
            <?php echo toMoney($total['totalRenovationMXN'] * $pesoToday)?>€
        </td>
    </tr>

    <tr>
        <td style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px" colspan="2">
            Total
        </td>
        <td style="background-color: greenyellow; text-align: center; font-weight: bold; font-size: 15px">
            <?php
                if($total['countRenovationUSD'] + $total['countRenovationBRL'] + $total['countRenovationMXN'] + $total['countRenovationEUR'] + $total['countUSD'] + $total['countBRL'] + $total['countMXN'] + $total['countEUR'] > 0) {
                    echo toMoney(($total['totalUSDnoVat'] * $dollarToday + $total['totalRenovationUSDnoVat'] * $dollarToday +
                                $total['totalBRLnoVat'] * $realToday + $total['totalRenovationBRLnoVat'] * $realToday +
                                $total['totalMXNnoVat'] * $pesoToday + $total['totalRenovationMXNnoVat'] * $pesoToday +
                                $total['totalEURnoVat'] + $total['totalRenovationEURnoVat']) /
                        ($total['countRenovationUSD'] + $total['countRenovationBRL'] + $total['countRenovationMXN'] + $total['countRenovationEUR'] + $total['countUSD'] + $total['countBRL'] + $total['countMXN'] + $total['countEUR']));
                }
            ?>€
        </td>
        <td class="withVat" style="background-color: #d2ff8a; text-align: right; font-weight: bold; font-size: 15px; display: none;">
            <?php
            if($total['countRenovationUSD'] + $total['countRenovationBRL'] + $total['countRenovationMXN'] + $total['countRenovationEUR'] + $total['countUSD'] + $total['countBRL'] + $total['countMXN'] + $total['countEUR'] > 0) {
                echo toMoney(($total['totalUSD'] * $dollarToday + $total['totalRenovationUSD'] * $dollarToday +
                        $total['totalBRL'] * $realToday + $total['totalRenovationBRL'] * $realToday +
                        $total['totalMXN'] * $pesoToday + $total['totalRenovationMXN'] * $pesoToday +
                        $total['totalEUR'] + $total['totalRenovationEUR']) /
                    ($total['countRenovationUSD'] + $total['countRenovationBRL'] + $total['countRenovationMXN'] + $total['countRenovationEUR'] + $total['countUSD'] + $total['countBRL'] + $total['countMXN'] + $total['countEUR']));
            }
            ?>€
        </td>
        <td style="background-color: greenyellow; text-align: right; font-weight: bold; font-size: 15px">
            <?php echo $total['countRenovationUSD'] + $total['countRenovationBRL'] + $total['countRenovationMXN'] + $total['countRenovationEUR'] + $total['countUSD'] + $total['countBRL'] + $total['countMXN'] + $total['countEUR']?>
        </td>
        <td style="background-color: greenyellow; text-align: right; font-weight: bold; font-size: 15px">
            <?php echo toMoney($total['totalUSDnoVat'] * $dollarToday + $total['totalRenovationUSDnoVat'] * $dollarToday + $total['totalBRLnoVat'] * $realToday + $total['totalRenovationBRLnoVat'] * $realToday + $total['totalMXNnoVat'] * $pesoToday + $total['totalRenovationMXNnoVat'] * $pesoToday + $total['totalEURnoVat'] + $total['totalRenovationEURnoVat']) ?>€
        </td>
        <td class="withVat" style="background-color: #d2ff8a; text-align: right; font-weight: bold; font-size: 15px; display: none;">
            <?php echo toMoney($total['totalUSD'] * $dollarToday + $total['totalRenovationUSD'] * $dollarToday + $total['totalBRL'] * $realToday + $total['totalRenovationBRL'] * $realToday + $total['totalMXN'] * $pesoToday + $total['totalRenovationMXN'] * $pesoToday + $total['totalEUR'] + $total['totalRenovationEUR']) ?>€
        </td>
    </tr>
</table>

<table id="salesTable">

    <tr>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Type of Refund
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Quantity
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Amount
        </td>
        <td class="withVat" style="background-color: #9ebacf; color: #ffffff; font-weight: bold; text-align: center; width: 200px; display: none;">
            Amount with VAT
        </td>
    </tr>

    <tr>
        <td>
            Refund in US Dollar
        </td>
        <td style="text-align: right;">
            <?php echo $total['contRefundUSD'] ?>
        </td>
        <td style="text-align: right;">
            <span style="font-size: 11px">*(<?php echo toMoney($total['totalRefundUSDnoVat'])."$)"  ?></span>
            <?php echo toMoney($total['totalRefundUSDnoVat'] * $dollarToday)?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <span style="font-size: 11px">*(<?php echo toMoney($total['totalRefundUSD'])."$)"  ?></span>
            <?php echo toMoney($total['totalRefundUSD'] * $dollarToday)?>€
        </td>
    </tr>

    <tr>
        <td>
            Refund in BRL Real
        </td>
        <td style="text-align: right;">
            <?php echo $total['contRefundBRL'] ?>
        </td>
        <td style="text-align: right;">
            <span style="font-size: 11px">*(<?php echo toMoney($total['totalRefundBRLnoVat'])."BRL)"  ?></span>
            <?php echo toMoney($total['totalRefundBRLnoVat'] * $realToday)?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <span style="font-size: 11px">*(<?php echo toMoney($total['totalRefundBRL'])."BRL)"  ?></span>
            <?php echo toMoney($total['totalRefundBRL'] * $realToday)?>€
        </td>
    </tr>

    <tr>
        <td>
            Refund in MXN Peso mexicano
        </td>
        <td style="text-align: right;">
            <?php echo $total['contRefundMXN'] ?>
        </td>
        <td style="text-align: right;">
            <span style="font-size: 11px">*(<?php echo toMoney($total['totalRefundMXNnoVat'])."MXN)"  ?></span>
            <?php echo toMoney($total['totalRefundMXNnoVat'] * $pesoToday)?>€
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <span style="font-size: 11px">*(<?php echo toMoney($total['totalRefundMXN'])."MXN)"  ?></span>
            <?php echo toMoney($total['totalRefundMXN'] * $pesoToday)?>€
        </td>
    </tr>

    <tr>
        <td>
            Refund in Euro
        </td>
        <td style="text-align: right;">
            <?php echo $total['contRefundEUR'] ?>
        </td>
        <td style="text-align: right;">
            <?php echo toMoney($total['totalRefundEURnoVat'])."€" ?>
        </td>
        <td class="withVat" style="text-align: right; display: none;">
            <?php echo toMoney($total['totalRefundEUR'])."€" ?>
        </td>
    </tr>

    <tr>
        <td style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px">
            Total Refund
        </td>
        <td style="background-color: #ff0000; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo $total['contRefundUSD'] + $total['contRefundBRL'] + $total['contRefundMXN'] + $total['contRefundEUR'] ?>
        </td>
        <td style="background-color: #ff0000; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo toMoney($total['totalRefundEURnoVat'] + $total['totalRefundUSDnoVat'] * $dollarToday + $total['totalRefundBRLnoVat'] * $realToday + $total['totalRefundMXNnoVat'] * $pesoToday)."€" ?>
        </td>
        <td class="withVat" style="background-color: #ff8482; text-align: right; font-weight: bold; font-size: 15px; display: none; color: #ffffff;">
            <?php echo toMoney($total['totalRefundEUR'] + $total['totalRefundUSD'] * $dollarToday + $total['totalRefundBRL'] * $realToday + $total['totalRefundMXN'] * $pesoToday)."€" ?>
        </td>
    </tr>

    <tr>
        <td style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px" colspan="2">
            Total Net
        </td>
        <td style="background-color: green; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo toMoney($total['totalUSDnoVat'] * $dollarToday + $total['totalRenovationUSDnoVat'] * $dollarToday + $total['totalBRLnoVat'] * $realToday + $total['totalRenovationBRLnoVat'] * $realToday + $total['totalMXNnoVat'] * $pesoToday + $total['totalRenovationMXNnoVat'] * $pesoToday + $total['totalEURnoVat'] + $total['totalRenovationEURnoVat'] - ($total['totalRefundEURnoVat'] + $total['totalRefundUSDnoVat'] * $dollarToday + $total['totalRefundBRLnoVat'] * $realToday + $total['totalRefundMXNnoVat'] * $pesoToday )) ?>€
        </td>
        <td class="withVat" style="background-color: #65af66; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff; display: none;">
            <?php echo toMoney($total['totalUSD'] * $dollarToday + $total['totalRenovationUSD'] * $dollarToday + $total['totalBRL'] * $realToday + $total['totalRenovationBRL'] * $realToday + $total['totalMXN'] * $pesoToday + $total['totalRenovationMXN'] * $pesoToday + $total['totalEUR'] + $total['totalRenovationEUR'] - ($total['totalRefundEUR'] + $total['totalRefundUSD'] * $dollarToday + $total['totalRefundBRL'] * $realToday + $total['totalRefundMXN'] * $pesoToday )) ?>€
        </td>
    </tr>
</table>

<table id="salesTable">
    <tr>
        <td style="border: 0">
            <h3>Income</h3>
            <?php

            $this->widget('ext.Hzl.google.HzlVisualizationChart', array('visualization' => 'PieChart',
                'data' => array(
                    array('Concept', 'Amount'),
                    array('New sales '.toMoney($total['totalUSD'] * $dollarToday + $total['totalBRL'] * $realToday + $total['totalMXN'] * $pesoToday + $total['totalEUR']).'€',$total['countUSD'] + $total['countBRL'] + $total['countMXN'] + $total['countEUR']),
                    array('Renovation '.toMoney($total['totalRenovationUSD'] * $dollarToday + $total['totalRenovationBRL'] * $realToday + $total['totalRenovationMXN'] * $pesoToday + $total['totalRenovationEUR']).'€', $total['countRenovationUSD'] + $total['countRenovationBRL'] + $total['countRenovationMXN'] + $total['countRenovationEUR']),

                ),
                'options' => array(
                    //'title'=>'Income',
                    'width'=>500,
                    'height'=>180,
                    //'legend'=>'none',
                    'backgroundColor'=>'#fcfcfc',
                    'colors'=> array('#3366cc', '#109618', '#a60000', '#ff9900', '#dc3912'),
                    'chartArea'=>array('width'=>'100%', 'height'=>'90%'),
                    'legend'=> array('position'=> 'right'),
                    //'is3D'=>true,
                    'fontSize'=>11,
                )
            ));
            ?>
        </td>
        <td style="border: 0">
            <h3>Lost Renewals & Cancels</h3>

            <?php

            $this->widget('ext.Hzl.google.HzlVisualizationChart', array('visualization' => 'PieChart',
                'data' => array(
                    array('Concept', 'Amount'),
                    array('Refund '.toMoney($total['totalRefundEUR'] + $total['totalRefundUSD'] * $dollarToday + $total['totalRefundBRL'] * $realToday + $total['totalRefundMXN'] * $pesoToday).'€',$total['contRefundUSD'] + $total['contRefundBRL'] + $total['contRefundMXN'] + $total['contRefundEUR']),
                    array('Cancel no Ref. '.toMoney(($total['totalCanceledUSD'] * $dollarToday + $total['totalCanceledBRL'] * $realToday + $total['totalCanceledMXN'] * $pesoToday + $total['totalCanceledEUR']) - ($total['totalRefundEUR'] + $total['totalRefundUSD'] * $dollarToday + $total['totalRefundBRL'] * $realToday + $total['totalRefundMXN'] * $pesoToday)).'€', ($total['countCanceledUSD'] + $total['countCanceledBRL'] + $total['countCanceledMXN'] + $total['countCanceledEUR']) - ($total['contRefundUSD'] + $total['contRefundBRL'] + $total['contRefundMXN'] + $total['contRefundEUR'])),
                    array('Failed '.toMoney($total['totalFailedUSD'] * $dollarToday + $total['totalFailedBRL'] * $realToday + $total['totalFailedMXN'] * $pesoToday + $total['totalFailedEUR']).'€', $total['countFailedUSD'] + $total['countFailedBRL'] + $total['countFailedMXN'] + $total['countFailedEUR']),
                ),
                'options' => array(
                    //'title'=>'Lost renewals',
                    'width'=>500,
                    'height'=>200,
                    //'legend'=>'none',
                    'backgroundColor'=>'#fcfcfc',
                    'colors'=> array('#a60000', '#ff9900', '#dc3912', '#8A2908'),
                    'chartArea'=>array('width'=>'100%', 'height'=>'90%'),
                    'legend'=> array('position'=> 'right'),
                    //'is3D'=>true,
                    'fontSize'=>11,
                )
            ));
            ?>
        </td>
    </tr>
</table>

<div style="text-align: right">
    <?php
        echo "<span style=\"font-size:11px;\">* 1$ today is about ".$dollarToday."€</span><br />";
        echo "<span style=\"font-size:11px;\">* 1<span style='font-size:8px'>BRL</span> today is about ".$realToday."€</span><br />";
        echo "<span style=\"font-size:11px;\">* 1<span style='font-size:8px'>MXN</span> today is about ".$pesoToday."€</span>";
    ?>
</div>
<script>
    function showVat()
    {
        $('.withVat').toggle();
        if($('#showVatBut').text()=='Show VAT')
        {
            $('#showVatBut').text('Hidde Vat');
        }
        else
        {
            $('#showVatBut').text('Show VAT');
        }
    }
</script>
