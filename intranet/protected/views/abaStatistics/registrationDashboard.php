<?php
/* @var $this AbaPaymentsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Registration dashboard',
);
?>

<h3>Registration dashboard</h3>

<div style="text-align: center; font-size: 15px">
    Show new registrations for period from <b><?php echo $startDate ?></b> to <b><?php echo $endDate ?></b><br><br>
    <?php
        echo CHtml::form(Yii::app()->createUrl('abaStatistics/registrationDashboard'), 'post', array('id'=>'form', 'class'=>'myForm'));
        echo "range from ";
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'startDate',
            'value' => $startDate,
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth'=>true,
                'changeYear'=>true,
            ),
                'htmlOptions' => array(
                'size' => '10',
                'maxlength' => '10',
            ),
        ));
        echo " to ";
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'endDate',
            'value' => $endDate,
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth'=>true,
                'changeYear'=>true,
            ),
            'htmlOptions' => array(
                'size' => '10',
                'maxlength' => '10',
            ),
        ));
        echo " <i>and check this</i> ";
        if($flashSales)
            echo "<input type='checkbox' name='flashSales' value='true' checked/>";
        else
            echo "<input type='checkbox' name='flashSales' value='false'/>";
        echo " <i>if you like to include flash sales</i> ";
        echo CHtml::button("Search",array('onclick' => 'this.form.submit();','title'=>"Search",'id'=>'searchButton'));
        echo CHtml::endForm();
    ?>
</div>
<br><br>

<table id="salesTable">
    <tr>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Channel
        </td>
        <td style="background-color: #6FACCF; color: #ffffff; font-weight: bold; text-align: center;">
            Number of Registered
        </td>
    </tr>

    <?php
    $totalCounting = 0;
    foreach($channels as $channel)
    {
        $results = $this->getUsersByChannel($startDate, $endDate, $channel);
        if($results)
        {
            echo "<tr>";
            echo "<td style='font-weight: bold; background-color: #7f7f7f; color: #ffffff; border-bottom-right-radius: 0; border-bottom-left-radius: 0' colspan='2'>";
            echo  ucfirst($channel['nameGroup']);
            echo "</td>";
            echo "</tr>";

            //bucle para insertar los registros.
            $i=0;
            $subTotal=0;
            foreach($results  as $result)
            {
                echo "<tr>";
                echo "<td style='border-radius: 0'>";
                echo  "<li>".ucfirst($results[$i]['partnerName'])."</li>";
                echo "</td>";

                echo "<td style='text-align: right; border-radius: 0'>";
                echo $results[$i]['total'];
                echo "</td>";
                echo "</tr>";
                $totalCounting += $results[$i]['total'];
                $subTotal += $results[$i]['total'];
                $i++;

            }
            echo "<tr>";
            echo '<td style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px">';
            echo "Sub Total";
            echo "</td>";
            echo "<td style='background-color: greenyellow; text-align: center; font-weight: bold; font-size: 15px; text-align: right'>";
            echo $subTotal;
            echo "</td>";
            echo "</tr>";
        }
    }
    ?>
</table>

<table id="salesTable">
    <tr>
        <td style="font-weight: bold; height: 25px; font-size: 15px; text-align: right; border: 0px;">
            Total
        </td>
        <td style="background-color: green; text-align: right; font-weight: bold; font-size: 15px; color: #ffffff;">
            <?php echo $totalCounting; ?>
        </td>
    </tr>
</table>