<?php
/* @var $this AbaPartnergroupsController */
/* @var $model AbaPartnergroups */

$this->breadcrumbs = array(
  'Partnergroups',
);

$this->menu = array(
  array('label' => 'Create Partnergroup', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript(
  'search',
  "
   $('.search-button').click(function(){
     $('.search-form').toggle();
     return false;
   });
   $('.search-form form').submit(function(){
     $.fn.yiiGridView.update('aba-partnergroups-grid', {
       data: $(this).serialize()
     });
     return false;
   });
   "
);
?>

<h3>Partners groups</h3>

<?php echo CHtml::link('Create new partnergroup', array('AbaPartnerGroups/create')); ?>
<br>
<?php //echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial(
      '_search',
      array(
        'model' => $model,
      )
    ); ?>
</div><!-- search-form -->

<?php $this->widget(
  'application.components.widgets.CGridViewABA',
  array(
    'id' => 'aba-partnergroups-grid',
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager' => array(
      'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
      'prevPageLabel' => '<',
      'nextPageLabel' => '>',
      'firstPageLabel' => '<<',
      'lastPageLabel' => '>>',
    ),
    'extraInfo' => array('view', 'update'),
    'dataProvider' => $model->search(),
    'filter' => $model,
    'filterSelector' => '{filter}',
    'columns' => array(
      'idGroup',
      'nameGroup',
      'dateAdd',
      array(
        'class' => 'CButtonColumnABA',
        'header' => 'Options',
      ),
    ),
  )
); ?>
