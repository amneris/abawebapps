<?php
/* @var $this AbaPartnergroupsController */
/* @var $model AbaPartnergroups */

$this->breadcrumbs = array(
  'Partnergroups' => array('admin'),
  'Create',
);

$this->menu = array(
  array('label' => 'Manage Partnergroup', 'url' => array('admin')),
);
?>

    <h3>Partnergroups</h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>