<?php
/* @var $this AbaPartnergroupsController */
/* @var $model AbaPartnergroups */

$this->breadcrumbs = array(
  'Aba Partnergroups',
);

$this->menu = array(
  array('label' => 'Create Partnergroups', 'url' => array('create')),
  array('label' => 'Manage Partnergroups', 'url' => array('admin')),
);
?>

<h3>Partnergroups</h3>

<?php $this->widget(
  'zii.widgets.CListView',
  array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
  )
); ?>
