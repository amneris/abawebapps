<?php
/* @var $this AbaPartnergroupsController */
/* @var $model AbaPartnergroups */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget(
      'CActiveForm',
      array(
        'id' => 'aba-partnergroups-form',
        'enableAjaxValidation' => false,
      )
    );

    $cssPrefix = (isset($wAction) AND $wAction == 'update' ? 'Cm' : '');
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <table id="myForm">
        <?php if (is_numeric($model->idGroup)): ?>
            <tr>
                <td>
                    <?php echo $form->labelEx($model, 'idGroup'); ?>
                </td>
                <td>
                    <?php echo $model->idGroup; ?>
                    <?php echo $form->error($model, 'idGroup'); ?>
                </td>
            </tr>
        <?php endif; ?>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'nameGroup'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'nameGroup',
                  array('size' => 30, 'maxlength' => 45)); ?>
                <?php echo $form->error($model, 'nameGroup'); ?>
            </td>
        </tr>
        <?php if (is_numeric($model->idGroup)): ?>
            <tr>
                <td>
                    <?php echo $form->labelEx($model, 'dateAdd'); ?>
                </td>
                <td>
                    <?php echo $model->dateAdd; ?>
                </td>
            </tr>
        <?php endif; ?>
    </table>
    <div class="row buttons" align="center">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->