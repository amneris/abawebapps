<?php
/* @var $this AbaPartnergroupsController */
/* @var $model AbaPartnergroups */

$this->breadcrumbs = array(
  'Partnergroups' => array('admin'),
  $model->idGroup,
);

$this->menu = array(
  array('label' => 'List Partnergroups', 'url' => array('index')),
  array('label' => 'Create Partnergroup', 'url' => array('create')),
  array('label' => 'Update Partnergroup', 'url' => array('update', 'id' => $model->idGroup)),
  array(
    'label' => 'Delete Partnergroup',
    'url' => '#',
    'linkOptions' => array(
      'submit' => array('delete', 'id' => $model->idGroup),
      'confirm' => 'Are you sure you want to delete this item?'
    )
  ),
  array('label' => 'Manage Partnergroups', 'url' => array('admin')),
);
?>

<h3>View Partnergroups <b><?php echo $model->idGroup . ": " . $model->nameGroup; ?></b></h3>

<?php $this->widget(
  'application.components.widgets.CDetailViewABA',
  array(
    'data' => $model,
    'attributes' => array(
      'idGroup',
      'nameGroup',
      'dateAdd',
    ),
  )
); ?>
