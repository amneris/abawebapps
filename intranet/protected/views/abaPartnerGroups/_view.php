<?php
/* @var $this AbaPartnergroupsController */
/* @var $model AbaPartnergroups */
?>

<div class="view">

    <b/><?php echo CHtml::encode($data->getAttributeLabel('idGroup')); ?>:</b>
    <?php echo CHtml::encode($data->idGroup); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('nameGroup')); ?>:</b>
    <?php echo CHtml::encode($data->nameGroup); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('dateAdd')); ?>:</b>
    <?php echo CHtml::encode($data->dateAdd); ?>
    <br/>

</div>