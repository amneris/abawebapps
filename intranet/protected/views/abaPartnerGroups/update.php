<?php
/* @var $this AbaPartnergroupsController */
/* @var $model AbaPartnergroups */

$this->breadcrumbs = array(
  'Aba Partnergroups' => array('admin'),
  $model->idGroup => array('view', 'id' => $model->idGroup),
  'Update',
);

$this->menu = array(
  array('label' => 'List Partnergroups', 'url' => array('index')),
  array('label' => 'Create Partnergroup', 'url' => array('create')),
  array('label' => 'View Partnergroups', 'url' => array('view', 'id' => $model->idGroup)),
  array('label' => 'Manage Partnergroups', 'url' => array('admin')),
);
?>

    <h3>Update Partnergroup: <?php echo $model->idGroup; ?></h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>