<?php
/* @var $this AbaPaymentsBoletoController */
/* @var $model AbaPaymentsBoleto */

$this->breadcrumbs=array(
	'Boletos',
);

$this->menu=array(
	array('label'=>'List AbaPaymentsBoleto', 'url'=>array('index')),
	array('label'=>'Create AbaPaymentsBoleto', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#aba-payments-boleto-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Boletos</h3>

<?php

$allStatus = HeList::boletosStates();

echo CHtml::form(Yii::app()->createUrl('abaPaymentsBoleto/admin'), 'get');
echo "Custom search with partner ";
echo CHtml::textField('idPayment', $idPayment, array('size'=>'15px','placeholder'=>'x000x000'));
echo " between request dates ";
$this->widget('zii.widgets.jui.CJuiDatePicker', array(
    'name' => 'startDate',
    'value' => $startDate,
    'options' => array(
        'dateFormat' => 'yy-mm-dd',
        'changeMonth'=>true,
        'changeYear'=>true,
    ),
    'htmlOptions' => array(
        'size' => '10',
        'maxlength' => '10',
    ),
));
echo " and ";
$this->widget('zii.widgets.jui.CJuiDatePicker', array(
    'name' => 'endDate',
    'value' => $endDate,
    'options' => array(
        'dateFormat' => 'yy-mm-dd',
        'changeMonth'=>true,
        'changeYear'=>true,
    ),
    'htmlOptions' => array(
        'size' => '10',
        'maxlength' => '10',
    ),
));
echo " with status ";
echo CHtml::dropDownList('boletoStatus', $boletoStatus, $allStatus);
echo " , filter by email ";
echo CHtml::textField('criterio', $criterio, array('size'=>'30px','placeholder'=>'emailexample@abaenglish.com'));
echo CHtml::submitButton('Submit',array('onchange'=>'this.form.submit();'));
echo CHtml::endForm();
echo "<br>";
?>


<?php
/*zii.widgets.grid.CGridView*/
$this->widget('application.components.widgets.CGridViewABA', array(
	'id'=>'aba-payments-boleto-grid',
	'dataProvider'=>$model->getAllBoletosWithUserInfo($pages, $startDate, $endDate, $boletoStatus, $criterio, $idPayment),
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'emptyText'=>'<b>No results found. Type in any text in any column to execute a search.</b>',
    'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
        'prevPageLabel'  => '<',
        'nextPageLabel'  => '>',
        'firstPageLabel' => '<<',
        'lastPageLabel'  => '>>',),
	'columns'=>array(
        'name'=>array(
            'header'=>'Name',
            'name'=>'name',
        ),
        'email',
		/*'id',*/
		'idPayment',
		/*'idPaySupplier',*/
		/*'paySuppExtUniId',*/
		/*'url',*/
        'requestDate'=>array(
            'header'=>'Request date',
            'name'=>'requestDate',
        ),
        'dueDate'=>array(
            'header'=>'Due date',
            'name'=>'dueDate',
        ),
		/*'status',*/
        array(
            'name' => 'status',
            'header' => "Status",
            'type' => 'boletosStates',
            'filter' => HeList::boletosStates(),
        ),
        'userType'=>array(
            'header'=>'User type',
            'name'=>'userType',
        ),

		'xmlOrderId'=>array(
            'header'=>'XML order id',
            'name'=>'xmlOrderId',
        ),
        'View'=>array(
            'header'=>'View',
            'name'=>'id',
            'type'=>'@more...@index.php?r=abaPaymentsBoleto/view&id=*id*',
        ),
	),
)); ?>


<h3>The last two months by date</h3>

<table id="boletoTable">
    <tr>
        <td>Data Payments</td>
        <td>Num. Payments</td>
        <td>Status Payments</td>
    </tr>
<?php
    foreach($totalsPayments as $iKey => $stValue) {
?>
    <tr>
        <td style="margin: 10px; background-color: transparent; "><?php echo $stValue['dataPayments']; ?></td>
        <td style="background-color: greenyellow; font-weight: bold; font-size: 15px;"><?php echo $stValue['numPayments']; ?></td>
        <td style="margin: 10px; background-color: transparent;"><?php echo $stValue['statusPayments']; ?></td>
    </tr>
<?php
    }
?>
</table>

