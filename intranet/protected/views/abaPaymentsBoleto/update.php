<?php
/* @var $this AbaPaymentsBoletoController */
/* @var $model AbaPaymentsBoleto */

$this->breadcrumbs=array(
	'Aba Payments Boletos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AbaPaymentsBoleto', 'url'=>array('index')),
	array('label'=>'Create AbaPaymentsBoleto', 'url'=>array('create')),
	array('label'=>'View AbaPaymentsBoleto', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AbaPaymentsBoleto', 'url'=>array('admin')),
);
?>

<h1>Update AbaPaymentsBoleto <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>