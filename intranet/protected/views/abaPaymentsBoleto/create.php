<?php
/* @var $this AbaPaymentsBoletoController */
/* @var $model AbaPaymentsBoleto */

$this->breadcrumbs=array(
	'Aba Payments Boletos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AbaPaymentsBoleto', 'url'=>array('index')),
	array('label'=>'Manage AbaPaymentsBoleto', 'url'=>array('admin')),
);
?>

<h1>Create AbaPaymentsBoleto</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>