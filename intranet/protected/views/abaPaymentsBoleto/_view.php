<?php
/* @var $this AbaPaymentsBoletoController */
/* @var $data AbaPaymentsBoleto */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPayment')); ?>:</b>
	<?php echo CHtml::encode($data->idPayment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPaySupplier')); ?>:</b>
	<?php echo CHtml::encode($data->idPaySupplier); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paySuppExtUniId')); ?>:</b>
	<?php echo CHtml::encode($data->paySuppExtUniId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('url')); ?>:</b>
	<?php echo CHtml::encode($data->url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('requestDate')); ?>:</b>
	<?php echo CHtml::encode($data->requestDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dueDate')); ?>:</b>
	<?php echo CHtml::encode($data->dueDate); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xmlOrderId')); ?>:</b>
	<?php echo CHtml::encode($data->xmlOrderId); ?>
	<br />

	*/ ?>

</div>