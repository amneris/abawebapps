<?php
/* @var $this AbaPaymentsBoletoController */
/* @var $model AbaPaymentsBoleto */

$this->breadcrumbs=array(
	'Boletos'=>array('admin'),
	$model->idPayment,
);

?>

<h3>Detail view for boleto: <?php echo $model->id; ?></h3>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
        'name',
        'email',
		'idPayment',
		'idPaySupplier',
		'paySuppExtUniId',
		'url',
		'requestDate',
		'dueDate',
		'status',
        'userType',
		'xmlOrderId',
	),
)); ?>
