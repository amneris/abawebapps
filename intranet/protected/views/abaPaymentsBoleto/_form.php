<?php
/* @var $this AbaPaymentsBoletoController */
/* @var $model AbaPaymentsBoleto */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aba-payments-boleto-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'idPayment'); ?>
		<?php echo $form->textField($model,'idPayment',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'idPayment'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idPaySupplier'); ?>
		<?php echo $form->textField($model,'idPaySupplier',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'idPaySupplier'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'paySuppExtUniId'); ?>
		<?php echo $form->textField($model,'paySuppExtUniId',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'paySuppExtUniId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'url'); ?>
		<?php echo $form->textArea($model,'url',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'url'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'requestDate'); ?>
		<?php echo $form->textField($model,'requestDate'); ?>
		<?php echo $form->error($model,'requestDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dueDate'); ?>
		<?php echo $form->textField($model,'dueDate'); ?>
		<?php echo $form->error($model,'dueDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xmlOrderId'); ?>
		<?php echo $form->textField($model,'xmlOrderId',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'xmlOrderId'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->