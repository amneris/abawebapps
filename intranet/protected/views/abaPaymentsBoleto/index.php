<?php
/* @var $this AbaPaymentsBoletoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Aba Payments Boletos',
);

$this->menu=array(
	array('label'=>'Create AbaPaymentsBoleto', 'url'=>array('create')),
	array('label'=>'Manage AbaPaymentsBoleto', 'url'=>array('admin')),
);
?>

<h1>Aba Payments Boletos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
