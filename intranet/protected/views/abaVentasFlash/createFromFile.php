<?php
/* @var $this AbaVentasFlashController */
/* @var $model AbaVentasFlash */

$this->breadcrumbs=array(
    'Coupons'=>array('index'),
	'Load coupons from a file',
);

?>
<script type="text/javascript">
    window.onload = function() 
    {
        var d = new Date();
        
        var curr_date = d.getDate();
        if(curr_date.toString().length ==1)
            curr_date = '0' + curr_date.toString();
        
        var curr_month = d.getMonth() + 1; //Months are zero based
        if(curr_month.toString().length ==1)
            curr_month = '0' + curr_month.toString();
        
        var curr_year = d.getFullYear();
     
        document.getElementById('fecha').value = curr_year  + "/" + curr_month + "/" + curr_date;
    }


</script>
<div class="form" id="formulario">
<h3>Load coupons from a file</h3>
<p>This script help us when we need to insert new codes for an specific campaign. Notice that when the script is working the screen is totally white.
Do not panic, just wait for the results. If you see red lines, take a look... there was some problems. ;)</p>


<?php $form = $this->beginWidget('CActiveForm', array(
	'id'=>'aba-ventasflash-form',
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array('enctype' => 'multipart/form-data', 'name'=>'insertForm')
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
    
    <?php if(Yii::app()->user->hasFlash('errorCheckBox')): ?>
    
        <div class="flash-error">
            <?php echo Yii::app()->user->getFlash('errorCheckBox'); ?>
        </div>

    <?php endif; ?>
    

        <table id="myForm">
        
            <tr>
                <td><?php echo $form->labelEx($model,'agrupador'); ?></td>
                <td>
                    <?php
                        echo $form->DropDownList($model,'agrupador',$list);
                        echo $form->error($model,'agrupador'); 
                    ?>
                </td>
            </tr>
            
            <tr>
                <td><?php echo $form->labelEx($model,'deal'); ?></td>
                <td>
                    <?php echo $form->textField($model,'deal',array('size'=>2,'maxlength'=>10)); ?>
                    <?php echo $form->error($model,'deal'); ?>
                </td>
            </tr>
            
            <tr>
                <td><?php echo $form->labelEx($model,'count'); ?></td>
                <td>
                    <?php echo $form->textField($model,'count',array('size'=>2,'maxlength'=>10)); ?>
                    <?php echo $form->error($model,'count'); ?>
                </td>
            </tr>
            
            <tr >
                <td><label class="required">Select products <span class="required">*</span></label></td>
                <td class="oneLine">
                    <?php echo $form->labelEx($model,'m1');?> 
                    <?php echo $form->checkBox($model,'m1', array('value'=>1, 'uncheckValue'=>0, 'id'=>'m1')); ?>
                    &nbsp;
                    <?php echo $form->labelEx($model,'m2');?>
                    <?php echo $form->checkBox($model,'m2', array('value'=>1, 'uncheckValue'=>0, 'id'=>'m2')); ?>
                    &nbsp;
                    <?php echo $form->labelEx($model,'m3');?>
                    <?php echo $form->checkBox($model,'m3', array('value'=>1, 'uncheckValue'=>0, 'id'=>'m3')); ?>
                    &nbsp;
                    <?php echo $form->labelEx($model,'m6'); ?>
                    <?php echo $form->checkBox($model,'m6', array('value'=>1, 'uncheckValue'=>0, 'id'=>'m6')); ?>
                    &nbsp;
                    <?php echo $form->labelEx($model,'m12'); ?>
                    <?php echo $form->checkBox($model,'m12', array('value'=>1, 'uncheckValue'=>0, 'id'=>'m12')); ?>
                    &nbsp;
                    <?php echo $form->labelEx($model,'m18'); ?>
                    <?php echo $form->checkBox($model,'m18', array('value'=>1, 'uncheckValue'=>0, 'id'=>'m18')); ?>
                    &nbsp;
                    <?php echo $form->labelEx($model,'m24'); ?>
                    <?php echo $form->checkBox($model,'m24', array('value'=>1, 'uncheckValue'=>0, 'id'=>'m24')); ?>
                   
                </td>
            </tr>

            <tr>
                <td><?php echo $form->labelEx($model,'fecha');?></td>
                <td>
                    <?php
                        $form->widget('zii.widgets.jui.CJuiDatePicker',array(
                        'model'=>$model,
                        'attribute'=>'fecha',
                        // additional javascript options for the date picker plugin
                        'options'=>array(
                            'showAnim'=>'fold',
                            'dateFormat'=>'yy/mm/dd',
                        ),
                        'htmlOptions'=>array('style'=>'width:70px;', 'id'=>'fecha'),
                    ));
                    ?>
                </td>
            </tr>
            
            <tr>
                <td><?php echo $form->labelEx($model,'fileName');?></td>
                <td>
                    <?php echo $form->fileField($model,'fileName'); ?>
                    <?php echo $form->error($model,'fileName'); ?>
                    
                </td>
            </tr>
            

        </table>
        
	<div class="row buttons" align="center">
		<?php echo CHtml::submitButton('Insert')?>
	</div>
    
<?php $this->endWidget(); ?>

</div><!-- form -->

