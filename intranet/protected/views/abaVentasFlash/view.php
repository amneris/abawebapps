<?php
/* @var $this AbaAgrupadoresController */
$model->setAgrupadorName($agrupador);

$this->breadcrumbs=array(
	'Cupones'=>array('index'),
    $model->agrupadorName=>array('index','agrupador'=>$selected,'criterio'=>$criterio),
	$model->code1,
);
?>
<h3>Coupon detail</h3> 

 <?php if(Yii::app()->user->hasFlash('freeSucceded')): ?>
        <div class="flash-success">
            <?php echo Yii::app()->user->getFlash('freeSucceded'); ?>
        </div>

<?php endif; ?>

<?php 
    
    $this->widget('application.components.widgets.CDetailViewABA', array(
	'data'=>$model,
	'attributes'=>array(
        'agrupadorName',
		'code1',
        'code2',
        'email',
        'used',
        'date',
        'campaign',
        'count',
        
	),
)); ?>