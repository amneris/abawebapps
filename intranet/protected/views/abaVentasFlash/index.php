<?php
/* @var $this AbaVentasFlashController */
$this->breadcrumbs=array(
	'Coupons',
);
?>

<h3>Coupons</h3>

<?php echo CHtml::link('Create new coupon',array('AbaVentasFlash/create')); ?>
<br>
<?php
if(AbaUserBackoffice::isValid(array('marketing', 'root')))
{
    echo CHtml::link('Load coupons from a file',array('AbaVentasFlash/createFromFile'))."<br><br>";
}
?>
<br>
<?php   
echo CHtml::form(Yii::app()->createUrl('AbaVentasFlash/index'), 'post');
echo CHtml::hiddenField('criterio',$criterio);
echo CHtml::dropDownList('agrupadores', $selected, $list, array('onchange' => 'this.form.submit();'));
echo CHtml::endForm();
?>
<?php if(Yii::app()->user->hasFlash('delSucceded')): ?>
    <?php echo "<br>"; ?>
        <div class="flash-success">
            <?php echo Yii::app()->user->getFlash('delSucceded'); ?>
        </div>

<?php endif; ?>

 <?php if(Yii::app()->user->hasFlash('delError')): ?>
    <?php echo "<br>"; ?>
        <div class="flash-error">
            <?php echo Yii::app()->user->getFlash('delError'); ?>
        </div>

<?php endif; ?>

<br>
<hr>
<b>Search</b><br><br>
<?php
echo CHtml::form(Yii::app()->createUrl('AbaVentasFlash/index'), 'post', array('id'=>'criterio-form', 'class'=>'myForm'));
echo "Enter coupon code, security code or email to initiate a search: ";
echo CHtml::textField('criterio', $criterio, array('size'=>'40px','placeholder'=>'emailexample@abaenglish.com'));
echo CHtml::hiddenField('agrupadores',$selected);
echo CHtml::button("Search",array('onclick' => 'this.form.submit();','title'=>"Search",'id'=>'searchButton'));
echo CHtml::endForm();

$this->widget('application.components.widgets.CGridViewABA', array(
	'id'=>'myGrid',
    'selectableRows' => 1,
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
                    'prevPageLabel'  => '<',
                    'nextPageLabel'  => '>',
                    'firstPageLabel' => '<<',
                    'lastPageLabel'  => '>>',),
	'dataProvider'=> $model->findAllCodes($allTables, $pages, $criterio),
	'columns'=>array(
                 array('name'=>'code1', 
                'header'=>'Voucher Code'),
                 array('name'=>'code2', 
                'header'=>'Security Code'),
                'email'=>array(
                    'header'=>'Email'	,
                    'name'=>'email',
                    'type'=>'?index.php?r=abaUser/viewByEmail&email=*email*' ,
                ),

                'used',
                'product',                
                
                'View'=>array(  
                'header'=>'View',
                'name'=>'email',
                'type'=>'@View@index.php?r=abaVentasFlash/view&id=*code1*&table=*tableName*&agrupador='.$list[$selected].'&selected='.$selected.'&criterio='.$criterio,
                            ),
                'Free'=>array(  
                'header'=>'Free',
                'name'=>'email',
                'type'=>'@Free@index.php?r=abaVentasFlash/freeCode&id=*code1*&table=*tableName*&agrupador='.$list[$selected].'&selected='.$selected.'&criterio='.$criterio,
                            ),
                'Del'=>array(  
                'header'=>'Delete',
                'name'=>'email',
                'type'=>'@Del@index.php?r=abaVentasFlash/deleteCode&id=*code1*&table=*tableName*&agrupador='.$list[$selected].'&selected='.$selected.'&criterio='.$criterio,
                            ),
                ),  
));

?>
