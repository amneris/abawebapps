<?php
/* @var $this AbaVentasFlashController */
/* @var $model AbaVentasFlash */

$this->breadcrumbs=array(
	'Grouper Sales',
);

?>
<script type="text/javascript">
    window.onload = function()
    { 
        var d = new Date();
        
        var curr_date = d.getDate();
        if(curr_date.toString().length ==1)
            curr_date = '0' + curr_date.toString();
        
        var curr_month = d.getMonth() + 1; //Months are zero based
        if(curr_month.toString().length ==1)
            curr_month = '0' + curr_month.toString();
        
        var curr_year = d.getFullYear();
     
        document.getElementById('AbaAgrupadorSales_startDate').value = curr_year  + "-" + curr_month + "-" + curr_date;
        document.getElementById('AbaAgrupadorSales_endDate').value = curr_year  + "-" + curr_month + "-" + curr_date;
    }

</script>

<h3>Campaigns update</h3>
<p>The following script searches all groupers by the highest number of campaign and updates the database to includes all campaigns. It is advisable to run before beginning our search.</p>

<?php echo CHtml::link('Campaigns Update',array('AbaVentasFlash/updateDeals')); ?>
<br>
<br><br>

<h3>Grouper sales</h3>


<div class="form" id="formulario">
<?php $form = $this->beginWidget('CActiveForm', array(
	'id'=>'aba-ventasflash-form',
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array('name'=>'ventasAgrupador')
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
    
    <table id="myForm">

        <tr>
            <td><?php echo $form->labelEx($model,'agrupador'); ?></td>
            <td>
                <?php   
                echo $form->dropDownList($model,'agrupador', $list);
                ?>
            </td>
        </tr>

        <tr>
            <td><?php echo $form->labelEx($model,'startDate');?></td>
            <td>
                <?php
                echo $form->textField($model,'startDate',array('style'=>'width:70px;'));
                echo CHtml::image("images/calendar.jpg","calendar0", array("id"=>"b_startDate","class"=>"pointer"));
                $form->widget('application.extensions.calendar.SCalendar',
                    array(
                        'inputField'=>'AbaAgrupadorSales_startDate',
                        'button'=>'b_startDate',
                        'ifFormat'=>'%Y-%m-%d',
                    ));
                ?>
            </td>
        </tr>
        
        <tr>
            <td><?php echo $form->labelEx($model,'endDate');?></td>
            <td>
                <?php
                echo $form->textField($model,'endDate',array('style'=>'width:70px;'));
                echo CHtml::image("images/calendar.jpg","calendar0", array("id"=>"b_endDate","class"=>"pointer"));
                $form->widget('application.extensions.calendar.SCalendar',
                    array(
                        'inputField'=>'AbaAgrupadorSales_endDate',
                        'button'=>'b_endDate',
                        'ifFormat'=>'%Y-%m-%d',
                    ));
                ?>
            </td>
        </tr>

        <tr>
            <td><?php echo $form->labelEx($model,'showDate'); ?></td>
            <td>
                <?php   
                $options = array(1=>'Yes', 0=>'No');
                echo $form->dropDownList($model, 'showDate', $options);
                ?>
            </td>
        </tr>

    </table>
        
	<div class="row buttons" align="center">
		<?php echo CHtml::submitButton('Get sales'); ?>
	</div>
    
<?php $this->endWidget(); ?>

</div><!-- form -->