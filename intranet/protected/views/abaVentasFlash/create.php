<?php
/* @var $this AbaVentasFlashController */
/* @var $model AbaVentasFlash */

$this->breadcrumbs=array(
    'Coupons'=>array('index'),
	'Create',
);

?>

<h3>Create new coupon</h3>

<?php echo $this->renderPartial('_form', 
        array(  'model'=>$model,
                'list'=>$list,
                'selected'=>$selected, 
                'products'=>$products,
                'selectedProd'=>$selectedProd));
?>