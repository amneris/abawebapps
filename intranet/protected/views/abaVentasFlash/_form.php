<?php
/* @var $this AbaAgrupadorController */
/* @var $model AbaAgrupador */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aba-ventasflash-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
    
    <?php if(Yii::app()->user->hasFlash('prodExist')): ?>
    
        <div class="flash-error">
            <?php echo Yii::app()->user->getFlash('prodExist'); ?>
        </div>

    <?php endif; ?>
    

        <table id="myForm">
        
            <tr>
                <td><?php echo $form->labelEx($model,'agrupador'); ?></td>
                <td>
                    <?php
                        echo CHtml::dropDownList('agrupador', $selected, $list);
                        echo $form->error($model,'agrupador'); 
                    ?>
                </td>
            </tr>
            
            <tr>
                <td><?php echo $form->labelEx($model,'producto'); ?></td>
                <td>
                    <?php
                        echo CHtml::dropDownList('producto', $selectedProd, $products);
                        echo $form->error($model,'producto');
                    ?>
                </td>
            </tr>
           
            <tr>
                <td><?php echo $form->labelEx($model,'code1'); ?></td>
                <td>
                    <?php echo $form->textField($model,'code1',array('size'=>20,'maxlength'=>50)); ?>
                    <?php echo $form->error($model,'code1'); ?>
                </td>
            </tr>   
           
           
            <tr>
                <td><?php echo $form->labelEx($model,'campaign'); ?></td>
                <td>
                    <?php echo $form->textField($model,'campaign',array('size'=>2,'maxlength'=>10)); ?>
                    <?php echo $form->error($model,'campaign'); ?>
                </td>
            </tr>
            <tr>
                <td><?php echo $form->labelEx($model,'count'); ?></td>
                <td>
                    <?php echo $form->textField($model,'count',array('size'=>2,'maxlength'=>10)); ?>
                    <?php echo $form->error($model,'count'); ?>
                </td>
            </tr>

        </table>
        
	<div class="row buttons" align="center">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->