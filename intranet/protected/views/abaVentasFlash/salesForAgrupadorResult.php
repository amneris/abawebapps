<?php
/* @var $this AbaVentasFlashController */
$this->breadcrumbs=array(
    'Ventas Flash por Agrupador'=>array('salesForAgrupador'),
	$agrupador->agrupadorName,
);
?>
<script type="text/javascript">
    function selectElementContents(el) 
	{
        var desde = el.getElementsByTagName('tbody');
       
		var body = document.body, range, sel;
		if (body.createTextRange) 
		{
			range = body.createTextRange();
			range.moveToElementText(desde[0]);
			range.select();
		} 
		else if (document.createRange && window.getSelection) 
		{
			range = document.createRange();
			range.selectNodeContents(desde[0]);
			sel = window.getSelection();
			sel.removeAllRanges();
			sel.addRange(range);
			
		}
    }
</script>

<h3>Total de ventas</h3> 
Agrupador: <b><?php echo $agrupador->agrupadorName ?></b>
<br>
Mostrando ventas desde: <b><?php echo $salesSearch->startDate ?></b> hasta:  <b><?php echo $salesSearch->endDate ?></b>
<br>
Total de campañas: <b><?php echo $agrupador->totalDeals ?></b>
<br>
<br>

<?php
$prodLabel = array(0=>'Mensual', 1=>'Bimensual', 2=>'Trimestral', 3=>'Semestral', 4=>'Anual', 5=>'18 Meses', 6=>'24 Meses');

//muetra resultados con fecha
if($salesSearch->showDate)
{
    //esto for es por cada campaña del agrupador
    for ($deal = $agrupador->totalDeals; $deal >= 1; $deal--)
    {
        echo "<hr><center><h4>Campaña número: ".$deal."</h4></center><hr>";
        
        $currentProduct=0;
        
        foreach($allTables as $product)
        {
            if($allTables[$currentProduct]<>'no')
            {
                $ident = $prodLabel[$currentProduct] . $deal;
                    
                if($agrupador->oneOrTwoCodes==2)
                {
                    echo "Producto <b>".$prodLabel[$currentProduct]."</b> - Campaña: <b>".$deal."</b><br>";

                    echo CHtml::button('Seleccionar', array('onclick'=>'js:selectElementContents(document.getElementById("'.$ident.'"))'));

                    $this->widget('application.components.widgets.CGridViewABA', array(
                    'id'=>$ident,
                    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
                    'selectableRows' => 1,
                    'dataProvider'=> $model->findAllCodesByDate($allTables[$currentProduct], $salesSearch, $deal, $agrupador),
                    'columns'=>array(
                                'code1',
                                'code2',
                                'email',
                                'name',
                                'surnames', 
                                'telephone',
                                'date'         
                                ),  
                    'htmlOptions'=>array('name'=>$ident),
                    ));  
                }
                else 
                {
                    echo "Producto <b>".$prodLabel[$currentProduct]."</b> - Campaña: <b>".$deal."</b><br>";

                    echo CHtml::button('Seleccionar', array('onclick'=>'js:selectElementContents(document.getElementById("'.$ident.'"))'));
                     
                    $this->widget('application.components.widgets.CGridViewABA', array(
                    'id'=>$ident,
                    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
                    'selectableRows' => 1,
                    'dataProvider'=> $model->findAllCodesByDate($allTables[$currentProduct], $salesSearch, $deal, $agrupador),
                    'columns'=>array(
                                'code1',
                                'email',
                                'name',
                                'surnames', 
                                'telephone',
                                'date'         
                                ),  
                    'htmlOptions'=>array('name'=>$ident),
                    )); 
                }
            }
            $currentProduct++;
        }
        

    } 
}
//muetra resultados sin fecha
else
{
    //esto for es por cada campaña del agrupador
    for ($deal = $agrupador->totalDeals; $deal >= 1; $deal--)
    {
         echo "<hr><center><h4>Campaña número: ".$deal."</h4></center><hr>";
        
        $currentProduct=0;
        
        foreach($allTables as $product)
        {
            if($allTables[$currentProduct]<>'no')
            {
                $ident = $prodLabel[$currentProduct] . $deal;
                
                if($agrupador->oneOrTwoCodes==2)
                {
                    echo "Producto <b>".$prodLabel[$currentProduct]."</b> - Campaña: <b>".$deal."</b><br>";

                    echo CHtml::button('Seleccionar', array('onclick'=>'js:selectElementContents(document.getElementById("'.$ident.'"))'));
                     
                    $this->widget('application.components.widgets.CGridViewABA', array(
                    'id'=>$ident,
                    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
                    'selectableRows' => 1,
                    'dataProvider'=> $model->findAllCodesByDate($allTables[$currentProduct], $salesSearch, $deal, $agrupador),
                    'columns'=>array(
                                'code1',
                                'code2',
                                'email',
                                'name',
                                'surnames', 
                                'telephone',        
                                ),  
                    'htmlOptions'=>array('name'=>$ident),
                    ));  
                }
                else
                {
                    echo "Producto <b>".$prodLabel[$currentProduct]."</b> - Campaña: <b>".$deal."</b><br>";

                    echo CHtml::button('Seleccionar', array('onclick'=>'js:selectElementContents(document.getElementById("'.$ident.'"))'));
                     
                    $this->widget('application.components.widgets.CGridViewABA', array(
                    'id'=>$ident,
                    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
                    'selectableRows' => 1,
                    'dataProvider'=> $model->findAllCodesByDate($allTables[$currentProduct], $salesSearch, $deal, $agrupador),
                    'columns'=>array(
                                'code1',
                                'email',
                                'name',
                                'surnames', 
                                'telephone',        
                                ),  
                    'htmlOptions'=>array('name'=>$ident),
                    ));  
                }
            }
            $currentProduct++;
        }

    } 
}

?>
