<?php
ini_set('max_execution_time',0);
ob_implicit_flush(true);

$this->breadcrumbs=array(
	'Cupones'=>array('index'),
        'Result',
);
?>
<p id="resultado"><b>Resultado de la inserción</b></p>
<b>Nota: </b>
<i>Los productos seleccionados en <b><span style="color:red; font-size: 10px">ROJO</span></b> no se insertaran porque no existen el producto en el agrupador especificado.</i><br>
<br>
<br>
<table id="myForm">
<tr>
<td>
Agrupador: <?php echo "<i><b>".$agrupador->agrupadorName."</b></i>"; ?>
<br>
<br>
Prefijo de tabla: <?php echo "<i><b>".$agrupador->tableName."</b></i>"; ?>
<br>
<br>
Productos del agrupador:
<?php

$band=0;

if($agrupador->prefijoMensual<>'no')
{
    echo "<i><b>Mensual</b></i>";
    $band=1;
}

if($agrupador->prefijoBimensual<>'no')
{
    if($band)
        echo " - ";

    echo "<i><b> Bimensual</b></i>";
    $band=1;
}

if($agrupador->prefijoTrimestral<>'no')
{
    if($band)
        echo " - ";

    echo "<i><b> Trimestral</b></i>";
    $band=1;
}
  
if($agrupador->prefijoSemestral<>'no')
{
    if($band)
        echo " - ";
    echo "<i><b> Semestral</b></i>";
    $band=1;
}
  
if($agrupador->prefijoAnual<>'no')
{
    if($band)
        echo " - ";

    echo "<i><b> Anual</b></i>";
    $band=1;
}
 
if($agrupador->prefijo18Meses<>'no')
{
    if($band)
        echo " - ";
    echo "<i><b> 18 Meses</b></i>";
    $band=1;
}
  
if($agrupador->prefijo24Meses<>'no')
{
    if($band)
        echo " - ";
    echo "<i><b> 24 Meses</b></i>";
    $band=1;
}
?>
<br>
<br>
Productos seleccionados para la inserción:
<?php

$band=0;
$products = array();
$prodTables = array();
$tempTable ='';
$noMatchProduct = array();

if($insertModel->m1==1)
{
    if($agrupador->prefijoMensual<>'no')
    {
        echo "<i><b><span style=\"color:blue\"> Mensual</span></b></i>";
        array_push($products, 1);
        $prodTables[1]= $agrupador->tableName . $agrupador->prefijoMensual;
    }
    else
    {
        echo "<i><b><span style=\"color:red\"> Mensual</span></b></i>";
        array_push($prodTables, 'no');
        array_push($noMatchProduct, 1);
    }
    $band=1;
}

if($insertModel->m2==1)
{
    if($band)
        echo " - ";

    if($agrupador->prefijoBimensual<>'no')
    {
        echo "<i><b><span style=\"color:blue\"> Bimensual</span></b></i>";
        array_push($products, 2);
        $prodTables[2]= $agrupador->tableName . $agrupador->prefijoBimensual;
    }
    else
    {
        echo "<i><b><span style=\"color:red\"> Bimensual</span></b></i>";
        array_push($prodTables, 'no');
        array_push($noMatchProduct, 2);
    }
    $band=1;
}


if($insertModel->m3==1)
{
    if($band)
        echo " - ";

    if($agrupador->prefijoTrimestral<>'no')
    {
        echo "<i><b><span style=\"color:blue\"> Trimestral</span></b></i>";
        array_push($products, 3);
        $prodTables[3]= $agrupador->tableName . $agrupador->prefijoTrimestral;
    }
    else
    {
        echo "<i><b><span style=\"color:red\"> Trimestral</span></b></i>";
        array_push($prodTables, 'no');
        array_push($noMatchProduct, 3);
    }
    $band=1;
}
  
if($insertModel->m6==1)
{
    if($band)
        echo " - ";
    
    if($agrupador->prefijoSemestral<>'no')
    {
        echo "<i><b><span style=\"color:blue\"> Semestral</span></b></i>";
        array_push($products, 6);
        $prodTables[6]= $agrupador->tableName . $agrupador->prefijoSemestral;
    }
    else
    {
        echo "<i><b><span style=\"color:red\"> Semestral</span></b></i>";
        array_push($prodTables, 'no');
        array_push($noMatchProduct, 6);
    }
    $band=1;
}
  
if($insertModel->m12==1)
{
    if($band)
        echo " - ";
    
    if($agrupador->prefijoAnual<>'no')
    {
        echo "<i><b><span style=\"color:blue\"> Anual</span></b></i>";
        array_push($products, 12);
        $prodTables[12]= $agrupador->tableName . $agrupador->prefijoAnual;
    }
    else
    {
        echo "<i><b><span style=\"color:red\"> Anual</span></b></i>";
        array_push($prodTables, 'no');
        array_push($noMatchProduct, 12);
    }
    $band=1;
}

if($insertModel->m18==1)
{
    if($band)
        echo " - ";
    
    if($agrupador->prefijo18Meses<>'no')
    {
        echo "<i><b><span style=\"color:blue\"> 18 meses</span></b></i>";
        array_push($products, 18);
        $prodTables[18]= $agrupador->tableName . $agrupador->prefijo18Meses;
    }
    else
    {
        echo "<i><b><span style=\"color:red\"> 18 meses</span></b></i>";
        array_push($prodTables, 'no');
        array_push($noMatchProduct, 18);
    }
    $band=1;
}

if($insertModel->m24==1)
{
    if($band)
        echo " - ";
    
     if($agrupador->prefijo24Meses<>'no')
    {
        echo "<i><b><span style=\"color:blue\"> 24 meses</span></b></i>";
        array_push($products, 24);
        $prodTables[24]= $agrupador->tableName . $agrupador->prefijo24Meses;
    }
    else
    {
        echo "<i><b><span style=\"color:red\"> 24 meses</span></b></i>";
        array_push($prodTables, 'no');
        array_push($noMatchProduct, 24);
    }
    $band=1;
}
    
?>
</td>
</tr>
</table>
<?php
    $readerXLS->processCodes($products, $prodTables, $insertModel, $agrupador, $noMatchProduct);
?>
