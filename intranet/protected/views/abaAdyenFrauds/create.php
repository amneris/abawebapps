<?php
/* @var $this AbaAdyenFraudsController */
/* @var $model AbaPaymentsAdyenFraud */

$this->breadcrumbs=array(
	'Adyen Fraudes'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Adyen Fraudes', 'url'=>array('index')),
	array('label'=>'Manage Adyen Fraudes', 'url'=>array('admin')),
);
?>

<h1>Create Adyen Fraudes</h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>