<?php
/* @var $this AbaAdyenFraudsController */
/* @var $model AbaPaymentsAdyenFraud */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
      'id' => 'tax-adyenfrauds-form',
        // Please note: When you enable ajax validation, make sure the correspondingº
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
      'enableAjaxValidation' => false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>


    <div class="row">
        <?php echo $form->labelEx($model, 'userId'); ?>
        <?php echo $form->textField($model, 'userId', array('size' => 10, 'maxlength' => 10)); ?>
        <?php echo $form->error($model, 'userId'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'idPayment'); ?>
        <?php echo $form->textField($model, 'idPayment', array('size' => 10, 'maxlength' => 20)); ?>
        <?php echo $form->error($model, 'idPayment'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'amountCurrency'); ?>
        <?php echo $form->textField($model, 'amountCurrency', array('size' => 10, 'maxlength' => 6)); ?>
        <?php echo $form->error($model, 'amountCurrency'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'amountValue'); ?>
        <?php echo $form->textField($model, 'amountValue', array('size' => 10, 'maxlength' => 10)); ?>
        <?php echo $form->error($model, 'amountValue'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'eventCode'); ?>
        <?php echo $form->textField($model, 'eventCode', array('size' => 40, 'maxlength' => 100)); ?>
        <?php echo $form->error($model, 'eventCode'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'merchantReference'); ?>
        <?php echo $form->textField($model, 'merchantReference', array('size' => 40, 'maxlength' => 100)); ?>
        <?php echo $form->error($model, 'merchantReference'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'originalReference'); ?>
        <?php echo $form->textField($model, 'originalReference', array('size' => 40, 'maxlength' => 100)); ?>
        <?php echo $form->error($model, 'originalReference'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'pspReference'); ?>
        <?php echo $form->textField($model, 'pspReference', array('size' => 40, 'maxlength' => 100)); ?>
        <?php echo $form->error($model, 'pspReference'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'paymentMethod'); ?>
        <?php echo $form->textField($model, 'paymentMethod', array('size' => 40, 'maxlength' => 100)); ?>
        <?php echo $form->error($model, 'paymentMethod'); ?>
    </div>
    <?php echo $form->hiddenField($model, 'status'); ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'dateAdd'); ?>
        <?php echo $form->textField($model, 'dateAdd'); ?>
        <?php
        echo CHtml::image("images/calendar.jpg", "calendar1", array("id" => "c_buttonStart", "class" => "pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
          array(
            'inputField' => 'AbaPaymentsAdyenFraud_dateAdd',
            'button' => 'c_buttonStart',
            'ifFormat' => '%Y-%m-%d 00:00:00',
          ));
        ?>
        <?php echo $form->error($model, 'dateAdd'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->