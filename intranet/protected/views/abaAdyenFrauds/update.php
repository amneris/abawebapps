<?php
/* @var $this AbaAdyenFraudsController */
/* @var $model AbaPaymentsAdyenFraud */

$this->breadcrumbs=array(
	'Adyen Fraudes'=>array('admin'),
	$model->idFraud=>array('view','id'=>$model->idFraud),
	'Update',
);

$this->menu=array(
	array('label'=>'List Adyen Fraudes', 'url'=>array('index')),
	array('label'=>'View Adyen Fraudes', 'url'=>array('view', 'id'=>$model->idFraud)),
	array('label'=>'Manage Adyen Fraudes', 'url'=>array('admin')),
);
?>

<h1>Update Adyen Fraudes <?php echo $model->idFraud; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>