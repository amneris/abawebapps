<?php
/* @var $this AbaAdyenFraudsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Adyen Fraudes',
);

$this->menu=array(
	array('label'=>'Manage Adyen Fraudes', 'url'=>array('admin')),
);
?>

<h1>Adyen Fraudes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
