<?php
/* @var $this AbaAdyenFraudsController */
/* @var $data AbaPaymentsAdyenFraud */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('idFraud')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->idFraud), array('view', 'id' => $data->idFraud)); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('userId')); ?>:</b>
    <?php echo CHtml::encode($data->userId); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('idPayment')); ?>:</b>
    <?php echo CHtml::encode($data->idPayment); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('amountCurrency')); ?>:</b>
    <?php echo CHtml::encode($data->amountCurrency); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('amountValue')); ?>:</b>
    <?php echo CHtml::encode($data->amountValue); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('eventCode')); ?>:</b>
    <?php echo CHtml::encode($data->eventCode); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('merchantReference')); ?>:</b>
    <?php echo CHtml::encode($data->merchantReference); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('originalReference')); ?>:</b>
    <?php echo CHtml::encode($data->originalReference); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('pspReference')); ?>:</b>
    <?php echo CHtml::encode($data->pspReference); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('reason')); ?>:</b>
    <?php echo CHtml::encode($data->reason); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('paymentMethod')); ?>:</b>
    <?php echo CHtml::encode($data->paymentMethod); ?>
    <br/>

<!--    <b>--><?php //echo CHtml::encode($data->getAttributeLabel('status')); ?><!--:</b>-->
<!--    --><?php //echo CHtml::encode($data->status); ?>
<!--    <br/>-->

    <b><?php echo CHtml::encode($data->getAttributeLabel('dateAdd')); ?>:</b>
    <?php echo CHtml::encode($data->dateEndCommission); ?>
    <br/>

</div>