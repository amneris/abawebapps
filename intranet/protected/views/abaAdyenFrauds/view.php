<?php
/* @var $this AbaAdyenFraudsController */
/* @var $model AbaPaymentsAdyenFraud */

$this->breadcrumbs = array(
  'Adyen Fraudes' => array('admin'),
  $model->idFraud,
);

$this->menu = array(
  array('label' => 'List Adyen Fraudes', 'url' => array('index')),
  array('label' => 'Update Adyen Fraudes', 'url' => array('update', 'id' => $model->idFraud)),
  array(
    'label' => 'Delete Adyen Fraudes',
    'url' => '#',
    'linkOptions' => array(
      'submit' => array('delete', 'id' => $model->idFraud),
      'confirm' => 'Are you sure you want to delete this item?'
    )
  ),
  array('label' => 'Manage Adyen Fraudes', 'url' => array('admin')),
);
?>

<h1>View Adyen Fraudes #<?php echo $model->idFraud; ?></h1>

<?php $this->widget('application.components.widgets.CDetailViewABA', array(
  'data' => $model,
  'attributes' => array(
    'idFraud',
    'userId',
    'idPayment',
    'amountCurrency',
    'amountValue',
    'eventCode',
    'merchantReference',
    'originalReference',
    'pspReference',
    'reason',
    'paymentMethod',
//    'status' => array(
//      'name' => 'status',
//      'type' => 'adyenFraudsStates',
//      'filter' => HeList::adyenFraudsStates()
//    ),
    'dateAdd',
  ),
)); ?>
