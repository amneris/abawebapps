<?php
/* @var $this AbaAdyenFraudsController */
/* @var $model AbaPaymentsAdyenFraud */

$this->breadcrumbs = array('Adyen Fraudes');

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tax-adyenfraudes-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
//");
?>

<h3>Adyen Fraudes</h3>

<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>

<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
      'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('application.components.widgets.CGridViewABA', array(
  'id' => 'tax-adyenfraudes-grid',
  'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
  'pager' => array(
    'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
    'prevPageLabel' => '<',
    'nextPageLabel' => '>',
    'firstPageLabel' => '<<',
    'lastPageLabel' => '>>',
  ),
  'extraInfo' => array('view', 'update'),
  'extraButtons' => array(
    'extraButton1' => array(
      'textExtraLink' => 'User',
      'classExtraLink' => 'payments',
      'urlExtraLink' => '"abaUser/admin_id",array("id"=>$data->userId)'
    ),
    'extraButton2' => array(
      'textExtraLink' => '<br />Payment',
      'classExtraLink' => 'user',
      'urlExtraLink' => '"abaPayments/view",array("id"=>$data->idPayment)'
    ),
    'extraButton3' => array(
      'textExtraLink' => '<br />Refund',
      'classExtraLink' => 'user',
      'fnValidate' => 'HeQuery::checkForChargebackRefund($data->status, $data->eventCode)',
      'urlExtraLink' => '"abaPayments/refund",array("id"=>$data->userId,"payment"=>$data->idPayment,"chargebackrefundid"=>$data->idFraud)'
    ),
  ),
  'dataProvider' => $model->search(),
  'filter' => $model,
  'columns' => array(
    'idFraud',
    'userId',
    'idPayment',
    'amountCurrency',
    'amountValue',
    'eventCode',
    'merchantReference',
    'originalReference',
    'pspReference',
    'reason',
    'paymentMethod',
//    array(
//      'name' => 'status',
//      'header' => "Status",
//      'value' => 'AbaPaymentsAdyenFraud::adyenFraudsState($data->status)',
//      'filter' => CHtml::listData(AbaPaymentsAdyenFraud::adyenFraudsStates(), 'id', 'title'),
//    ),
    'dateAdd',
    array(
      'class' => 'CButtonColumnABA',
      'htmlOptions' => array('style' => 'width: 110px;'),
      'header' => 'Options',
    ),
  ),
)); ?>
