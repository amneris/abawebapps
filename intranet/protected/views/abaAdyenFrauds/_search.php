<?php
/* @var $this AbaAdyenFraudsController */
/* @var $model AbaPaymentsAdyenFraud */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget('CActiveForm', array(
      'action' => Yii::app()->createUrl($this->route),
      'method' => 'get',
    )); ?>

    <div class="row">
        <?php echo $form->label($model, 'idFraud'); ?>
        <?php echo $form->textField($model, 'idFraud', array('size' => 10, 'maxlength' => 10)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'userId'); ?>
        <?php echo $form->textField($model, 'userId', array('size' => 10, 'maxlength' => 10)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'idPayment'); ?>
        <?php echo $form->textField($model, 'idPayment', array('size' => 10, 'maxlength' => 20)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'eventCode'); ?>
        <?php echo $form->textField($model, 'eventCode', array('size' => 40, 'maxlength' => 100)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'merchantReference'); ?>
        <?php echo $form->textField($model, 'merchantReference', array('size' => 40, 'maxlength' => 100)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'originalReference'); ?>
        <?php echo $form->textField($model, 'originalReference', array('size' => 40, 'maxlength' => 100)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'pspReference'); ?>
        <?php echo $form->textField($model, 'pspReference', array('size' => 40, 'maxlength' => 100)); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->