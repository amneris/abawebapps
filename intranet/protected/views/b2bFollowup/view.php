<?php
/* @var $this B2bFollowupController */
/* @var $model b2bFollowup */

$this->breadcrumbs=array(
	'B2B Study Report'=>array('admin&company='.$company.'&level='.$level.'&criterio='.$criterio),
    $model->sc_email,
);
$progressBarStyle="width:100%; height:12px; float:left; color:#000000 ;background:#FFFFFF";
?>

<h3>B2B Study Report</h3>

<table id="myForm">

    <tr>
        <td colspan="2" style="text-align: center; padding: 20px">
            <span style="font-size: 15px; font-weight: bold"><?php echo $model->getAttributeLabel('ID_theme')." "; echo $model->ID_theme; ?></span><br>
            <?php echo '<b style="font-size: 10px"> ('.$model->getAttributeLabel('ID_followup').'</b>' ?> <?php echo '<span style="font-size: 10px">'.$model->ID_followup.')</span>'; ?><br>
            <?php echo '<b style="font-size: 10px">'.$model->sc_level.'</b>' ?>
        </td>
    </tr>

    <tr>
        <td style="width: 120px">
            <?php echo '<b>'.$model->getAttributeLabel('sc_name').'</b>' ?>
        </td>
        <td>
            <?php echo ucfirst(strtolower($model->sc_name)).' '. ucfirst(strtolower($model->sc_surnames)); ?>
            <?php echo '<br><b style="font-size: 10px"> ('.$model->getAttributeLabel('ID_learner').'</b>' ?> <?php echo '<span style="font-size: 10px">'.$model->ID_learner.')<b style="font-size: 10px"> (Company </b>'.substr(strtoupper($company),8).')</span>'; ?>
        </td>
    </tr>

    <tr>
        <td>
            <?php echo '<b>'.$model->getAttributeLabel('sc_email').'</b>' ?>
        </td>
        <td>
            <?php echo $model->sc_email; ?>
        </td>
    </tr>

    <?php
    if($model->sc_phone)
    {
        echo '<tr><td><b>';
        echo $model->getAttributeLabel('sc_phone');
        echo '</b></td><td>';
        echo $model->sc_phone;
        echo '</td></tr>';
    }
    ?>

    <tr>
        <td>
            <?php echo '<b>'.$model->getAttributeLabel('sc_date').'</b>' ?>
        </td>
        <td>
            <?php echo $model->sc_date; ?>
        </td>
    </tr>

    <tr>
        <td>
            <?php echo '<b>'.$model->getAttributeLabel('sc_date_limit').'</b>' ?>
        </td>
        <td>
            <?php echo $model->sc_date_limit; ?>
        </td>
    </tr>

    <tr>
        <td>
            <?php echo '<b>'.$model->getAttributeLabel('lastchange').'</b>' ?>
        </td>
        <td>
            <?php echo $model->lastchange; ?>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center; padding: 20px">
            <span style="font-size: 15px; font-weight: bold">Resumen</span>
        </td>
    </tr>

    <tr>
        <td style="padding-top: 15px; padding-bottom: 15px">
            <?php echo '<b>'.$model->getAttributeLabel('sit_por').$totalsPercent['sitPercent'].'</b>' ?>
        </td>
        <td>
            <?php
            $this->createWidget("zii.widgets.jui.CJuiProgressBar",array(
                "value"=>intval($totalsPercent['sitPercent']),
                "htmlOptions"=>array(
                    "style"=>$progressBarStyle,
                    "color" => "red"
                ),
            ))->run()
            ?>
        </td>
    </tr>

    <tr>
        <td style="padding-top: 15px; padding-bottom: 15px">
            <?php echo '<b>'.$model->getAttributeLabel('stu_por').$totalsPercent['stuPercent'].'</b>' ?>
        </td>
        <td>
            <?php
            $this->createWidget("zii.widgets.jui.CJuiProgressBar",array(
                "value"=>intval($totalsPercent['stuPercent']),
                "htmlOptions"=>array(
                    "style"=>$progressBarStyle,
                    "color" => "red"
                ),
            ))->run()
            ?>
        </td>
    </tr>

    <tr>
        <td style="padding-top: 15px; padding-bottom: 15px">
            <?php echo '<b>'.$model->getAttributeLabel('dic_por').$totalsPercent['dicPercent'].'</b>' ?>
        </td>
        <td>
            <?php
            $this->createWidget("zii.widgets.jui.CJuiProgressBar",array(
                "value"=>intval($totalsPercent['dicPercent']),
                "htmlOptions"=>array(
                    "style"=>$progressBarStyle,
                    "color" => "red"
                ),
            ))->run()
            ?>
            <br>
            <?php echo '<b style="color:red">'.$model->getAttributeLabel('dic_err').'</b>: '.$model->dic_err?>
            <?php echo '<b style="color:green">'.$model->getAttributeLabel('dic_ans').'</b>: '.$model->dic_ans?>
        </td>
    </tr>

    <tr>
        <td style="padding-top: 15px; padding-bottom: 15px">
            <?php echo '<b>'.$model->getAttributeLabel('rol_por').$totalsPercent['rolPercent'].'</b>' ?>
        </td>
        <td>
            <?php
            $this->createWidget("zii.widgets.jui.CJuiProgressBar",array(
                "value"=>intval($totalsPercent['rolPercent']),
                "htmlOptions"=>array(
                    "style"=>$progressBarStyle,
                    "color" => "red"
                ),
            ))->run()
            ?>
        </td>
    </tr>

    <tr>
        <td style="padding-top: 15px; padding-bottom: 15px">
            <?php echo '<b>'.$model->getAttributeLabel('gra_por').$totalsPercent['graPercent'].'</b>' ?>
        </td>
        <td>
            <?php
            $this->createWidget("zii.widgets.jui.CJuiProgressBar",array(
                "value"=>intval($totalsPercent['graPercent']),
                "htmlOptions"=>array(
                    "style"=>$progressBarStyle,
                    "color" => "red"
                ),
            ))->run()
            ?>
            <br>
            <?php echo '<b style="color:red">'.$model->getAttributeLabel('gra_err').'</b>: '.$model->gra_err?>
            <?php echo '<b style="color:green">'.$model->getAttributeLabel('gra_ans').'</b>: '.$model->gra_ans?>
        </td>
    </tr>

    <tr>
        <td style="padding-top: 15px; padding-bottom: 15px">
            <?php echo '<b>'.$model->getAttributeLabel('wri_por').$totalsPercent['wriPercent'].'</b>' ?>
        </td>
        <td>
            <?php
            $this->createWidget("zii.widgets.jui.CJuiProgressBar",array(
                "value"=>intval($totalsPercent['wriPercent']),
                "htmlOptions"=>array(
                    "style"=>$progressBarStyle,
                    "color" => "red"
                ),
            ))->run()
            ?>
            <br>
            <?php echo '<b style="color:red">'.$model->getAttributeLabel('wri_err').'</b>: '.$model->wri_err?>
            <?php echo '<b style="color:green">'.$model->getAttributeLabel('wri_ans').'</b>: '.$model->wri_ans?>
        </td>
    </tr>

    <tr>
        <td style="padding-top: 15px; padding-bottom: 15px">
            <?php echo '<b>'.$model->getAttributeLabel('new_por').$totalsPercent['newPercent'].'</b>' ?>
        </td>
        <td>
            <?php
            $this->createWidget("zii.widgets.jui.CJuiProgressBar",array(
                "value"=>intval($totalsPercent['newPercent']),
                "htmlOptions"=>array(
                    "style"=>$progressBarStyle,
                    "color" => "red"
                ),
            ))->run()
            ?>
        </td>
    </tr>

    <tr>
        <td style="padding-top: 15px; padding-bottom: 15px">
            <?php echo '<b>'.$model->getAttributeLabel('spe_por').$totalsPercent['spePercent'].'</b>' ?>
        </td>
        <td>
            <?php
            $this->createWidget("zii.widgets.jui.CJuiProgressBar",array(
                "value"=>intval($totalsPercent['spePercent']),
                "htmlOptions"=>array(
                    "style"=>$progressBarStyle,
                    "color" => "red"
                ),
            ))->run()
            ?>
            <br>
            <?php echo '<b style="color:green">'.$model->getAttributeLabel('spe_ans').'</b>: '.$model->spe_ans?>
        </td>

    </tr>

    <tr>
        <td style="padding: 30px">
            <?php echo '<b>'.$model->getAttributeLabel('all_por').$totalsPercent['total'].'</b>' ?>
        </td>
        <td>
            <?php
            $this->createWidget("zii.widgets.jui.CJuiProgressBar",array(
                "value"=>intval($totalsPercent['total']),
                "htmlOptions"=>array(
                    "style"=>$progressBarStyle,

                ),
            ))->run()
            ?>
            <br>
            <?php echo '<b style="color:red">'.$model->getAttributeLabel('all_err').'</b>: '.$model->all_err?>
            <?php echo '<b style="color:green">'.$model->getAttributeLabel('all_ans').'</b>: '.$model->all_ans?>
            <?php echo '<b style="color:blue">'.$model->getAttributeLabel('all_time').'</b>: '.$model->all_time?>
        </td>
    </tr>
</table>



