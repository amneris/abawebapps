<?php
/* @var $this B2bFollowupController */
/* @var $model b2bFollowup */

$this->breadcrumbs=array(
	'B2B Study Report',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('b2b-followup-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>B2B Study Report</h3>

<?php
echo CHtml::form(Yii::app()->createUrl('b2bFollowup/admin'), 'get');
echo "Please choose a level ";
echo CHtml::dropDownList('level', $level, $levelList, array('onchange' => 'this.form.submit();'));
echo " , company ";
echo CHtml::dropDownList('company', $company, $list, array('onchange' => 'this.form.submit();'));
echo " , filter by by email ";
echo CHtml::textField('criterio', $criterio, array('size'=>'40px','placeholder'=>'emailexample@abaenglish.com','onchange' => 'this.form.submit();'));
echo CHtml::endForm();
echo "<br>";
echo CHtml::link('Export all units to CSV (Compact Mode)',array('b2bFollowup/exportCompact&level='.$level.'&company='.$company.'&criterio='.$criterio.'&complete=1'));
echo "<br>";
echo "<br>";
echo CHtml::link('Export all units to CSV (Extended Mode)',array('b2bFollowup/export&level='.$level.'&company='.$company.'&criterio='.$criterio.'&complete=0'));
echo "<br>";
?>
<?php $this->widget('application.components.widgets.CGridViewABA', array(
	'id'=>'b2b-followup-grid',
    'selectableRows' => 1,
    'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
    'pager'=>array('cssFile'=>Yii::app()->request->baseUrl . '/css/pager.css',
        'prevPageLabel'  => '<',
        'nextPageLabel'  => '>',
        'firstPageLabel' => '<<',
        'lastPageLabel'  => '>>',),
	'dataProvider'=>$model->findByLevel($level, $pages, $company, $criterio),
	'columns'=>array(
        'sc_name'=>array(
            'header'=>$model->getAttributeLabel('sc_name'),
            'name'=>'sc_name',
        ),
        'sc_surnames'=>array(
            'header'=>$model->getAttributeLabel('sc_surnames'),
            'name'=>'sc_surnames',
        ),
        'sc_email'=>array(
            'header'=>$model->getAttributeLabel('sc_email'),
            'name'=>'sc_email',
        ),
        //'sc_phone',
        'sc_level'=>array(
            'header'=>$model->getAttributeLabel('sc_level'),
            'name'=>'sc_level',
        ),
        'ID_theme'=>array(
            'header'=>$model->getAttributeLabel('ID_theme'),
            'name'=>'ID_theme',
        ),
        //'sc_date',
        //'sc_date_limit'=>array(
        //    'header'=>$model->getAttributeLabel('sc_date_limit'),
        //    'name'=>'sc_date_limit',
        //),
        //'ID_followup',
		//'ID_learner',
        'lastchange'=>array(
            'header'=>$model->getAttributeLabel('lastchange'),
            'name'=>'lastchange',
        ),

        'all_por'=>array(
            'header'=>$model->getAttributeLabel('all_por'),
            'name'=>'all_por',
        ),
		//'all_err',
		//'all_ans',
		//'all_time',
        'sit_por'=>array(
            'header'=>$model->getAttributeLabel('sit_por'),
            'name'=>'sit_por',
        ),
        'stu_por'=>array(
            'header'=>$model->getAttributeLabel('stu_por'),
            'name'=>'stu_por',
        ),
        'dic_por'=>array(
            'header'=>$model->getAttributeLabel('dic_por'),
            'name'=>'dic_por',
        ),
		//'dic_ans',
		//'dic_err',
        'rol_por'=>array(
            'header'=>$model->getAttributeLabel('rol_por'),
            'name'=>'rol_por',
        ),
        'gra_por'=>array(
            'header'=>$model->getAttributeLabel('gra_por'),
            'name'=>'gra_por',
        ),
		//'gra_ans',
		//'gra_err',
        'wri_por'=>array(
            'header'=>$model->getAttributeLabel('wri_por'),
            'name'=>'wri_por',
        ),
		//'wri_ans',
		//'wri_err',
        'new_por'=>array(
            'header'=>$model->getAttributeLabel('new_por'),
            'name'=>'new_por',
        ),
        'spe_por'=>array(
            'header'=>$model->getAttributeLabel('spe_por'),
            'name'=>'spe_por',
        ),
		//'spe_ans',
		//'time_aux',
        'View'=>array(
            'header'=>'View',
            'name'=>'ID_followup',
            'type'=>'@View@index.php?r=b2bFollowup/view&id=*ID_followup*&company='.$company.'&level='.$level.'&criterio='.urlencode($criterio),
        ),

	),
)); ?>
