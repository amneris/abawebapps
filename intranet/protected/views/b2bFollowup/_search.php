<?php
/* @var $this B2bFollowupController */
/* @var $model b2bFollowup */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID_followup'); ?>
		<?php echo $form->textField($model,'ID_followup',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ID_theme'); ?>
		<?php echo $form->textField($model,'ID_theme',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ID_learner'); ?>
		<?php echo $form->textField($model,'ID_learner',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lastchange'); ?>
		<?php echo $form->textField($model,'lastchange'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'all_por'); ?>
		<?php echo $form->textField($model,'all_por',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'all_err'); ?>
		<?php echo $form->textField($model,'all_err',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'all_ans'); ?>
		<?php echo $form->textField($model,'all_ans',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'all_time'); ?>
		<?php echo $form->textField($model,'all_time',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sit_por'); ?>
		<?php echo $form->textField($model,'sit_por',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stu_por'); ?>
		<?php echo $form->textField($model,'stu_por',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dic_por'); ?>
		<?php echo $form->textField($model,'dic_por',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dic_ans'); ?>
		<?php echo $form->textField($model,'dic_ans',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dic_err'); ?>
		<?php echo $form->textField($model,'dic_err',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rol_por'); ?>
		<?php echo $form->textField($model,'rol_por',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gra_por'); ?>
		<?php echo $form->textField($model,'gra_por',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gra_ans'); ?>
		<?php echo $form->textField($model,'gra_ans',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gra_err'); ?>
		<?php echo $form->textField($model,'gra_err',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wri_por'); ?>
		<?php echo $form->textField($model,'wri_por',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wri_ans'); ?>
		<?php echo $form->textField($model,'wri_ans',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wri_err'); ?>
		<?php echo $form->textField($model,'wri_err',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'new_por'); ?>
		<?php echo $form->textField($model,'new_por',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'spe_por'); ?>
		<?php echo $form->textField($model,'spe_por',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'spe_ans'); ?>
		<?php echo $form->textField($model,'spe_ans',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'time_aux'); ?>
		<?php echo $form->textField($model,'time_aux',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->