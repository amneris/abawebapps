<?php
/* @var $this B2bFollowupController */
/* @var $model b2bFollowup */

$this->breadcrumbs=array(
	'B2b Followups'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List b2bFollowup', 'url'=>array('index')),
	array('label'=>'Manage b2bFollowup', 'url'=>array('admin')),
);
?>

<h1>Create b2bFollowup</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>