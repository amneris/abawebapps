<?php
/* @var $this B2bFollowupController */
/* @var $model b2bFollowup */

$this->breadcrumbs=array(
	'B2b Followups'=>array('index'),
	$model->ID_followup=>array('view','id'=>$model->ID_followup),
	'Update',
);

$this->menu=array(
	array('label'=>'List b2bFollowup', 'url'=>array('index')),
	array('label'=>'Create b2bFollowup', 'url'=>array('create')),
	array('label'=>'View b2bFollowup', 'url'=>array('view', 'id'=>$model->ID_followup)),
	array('label'=>'Manage b2bFollowup', 'url'=>array('admin')),
);
?>

<h1>Update b2bFollowup <?php echo $model->ID_followup; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>