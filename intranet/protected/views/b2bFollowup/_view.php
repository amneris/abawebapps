<?php
/* @var $this B2bFollowupController */
/* @var $data b2bFollowup */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_followup')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID_followup), array('view', 'id'=>$data->ID_followup)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_theme')); ?>:</b>
	<?php echo CHtml::encode($data->ID_theme); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_learner')); ?>:</b>
	<?php echo CHtml::encode($data->ID_learner); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastchange')); ?>:</b>
	<?php echo CHtml::encode($data->lastchange); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('all_por')); ?>:</b>
	<?php echo CHtml::encode($data->all_por); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('all_err')); ?>:</b>
	<?php echo CHtml::encode($data->all_err); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('all_ans')); ?>:</b>
	<?php echo CHtml::encode($data->all_ans); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('all_time')); ?>:</b>
	<?php echo CHtml::encode($data->all_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sit_por')); ?>:</b>
	<?php echo CHtml::encode($data->sit_por); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stu_por')); ?>:</b>
	<?php echo CHtml::encode($data->stu_por); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dic_por')); ?>:</b>
	<?php echo CHtml::encode($data->dic_por); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dic_ans')); ?>:</b>
	<?php echo CHtml::encode($data->dic_ans); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dic_err')); ?>:</b>
	<?php echo CHtml::encode($data->dic_err); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rol_por')); ?>:</b>
	<?php echo CHtml::encode($data->rol_por); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gra_por')); ?>:</b>
	<?php echo CHtml::encode($data->gra_por); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gra_ans')); ?>:</b>
	<?php echo CHtml::encode($data->gra_ans); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gra_err')); ?>:</b>
	<?php echo CHtml::encode($data->gra_err); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wri_por')); ?>:</b>
	<?php echo CHtml::encode($data->wri_por); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wri_ans')); ?>:</b>
	<?php echo CHtml::encode($data->wri_ans); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wri_err')); ?>:</b>
	<?php echo CHtml::encode($data->wri_err); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('new_por')); ?>:</b>
	<?php echo CHtml::encode($data->new_por); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('spe_por')); ?>:</b>
	<?php echo CHtml::encode($data->spe_por); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('spe_ans')); ?>:</b>
	<?php echo CHtml::encode($data->spe_ans); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('time_aux')); ?>:</b>
	<?php echo CHtml::encode($data->time_aux); ?>
	<br />

	*/ ?>

</div>