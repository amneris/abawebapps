<?php
/* @var $this B2bFollowupController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'B2b Followups',
);

$this->menu=array(
	array('label'=>'Create b2bFollowup', 'url'=>array('create')),
	array('label'=>'Manage b2bFollowup', 'url'=>array('admin')),
);
?>

<h1>B2b Followups</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
