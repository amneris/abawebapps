<?php
/* @var $this AbaExperimentsVariationsAttributesController */
/* @var $model AbaExperimentsVariationsAttributes */

$this->breadcrumbs = array(
    'Scenarios Prices' => array('admin'),
    $model->experimentVariationAttributeId,
);

$this->menu = array(
    array('label' => 'List Scenarios Prices', 'url' => array('index')),
    array('label' => 'Create Scenarios Price', 'url' => array('create')),
    array('label' => 'Update Scenarios Price', 'url' => array('update', 'id' => $model->experimentVariationAttributeId)),
    array(
        'label' => 'Delete Scenarios Price',
        'url' => '#',
        'linkOptions' => array(
            'submit' => array('delete', 'id' => $model->experimentVariationAttributeId),
            'confirm' => 'Are you sure you want to delete this item?'
        )
    ),
    array('label' => 'Manage Scenarios Prices', 'url' => array('admin')),
);
?>

<h3>View Scenarios Price <b><?php echo $model->experimentVariationAttributeId; ?></b></h3>

<?php $this->widget(
    'application.components.widgets.CDetailViewABA',
    array(
        'data' => $model,
        'attributes' => array(
            'experimentVariationAttributeId',
            'experimentVariationId' =>
              array(
                  'name' => 'experimentVariationId',
                  'type' => 'experimentsVariationsList',
                  'filter' => HeList::getExperimentsVariationsList()
              ),
            'idProduct',
            'idCountry' =>
              array(
                  'name' => 'idCountry',
                  'type' => 'countryList',
              ),
            'price',
            'enabled' =>
              array(
                  'name' => 'enabled',
                  'type' => 'booleanList',
                  'filter' => HeList::booleanList()
              ),
        ),
    )
); ?>
