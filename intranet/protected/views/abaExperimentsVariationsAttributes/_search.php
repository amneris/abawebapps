<?php
/* @var $this AbaExperimentsVariationsAttributesController */
/* @var $model AbaExperimentsVariationsAttributes */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'experimentVariationAttributeId'); ?>
        <?php echo $form->textField($model, 'experimentVariationAttributeId', array('size' => 5, 'maxlength' => 20)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'experimentVariationId'); ?>
        <?php echo $form->textField($model, 'experimentVariationId', array('size' => 5, 'maxlength' => 20)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'idProduct'); ?>
        <?php echo $form->textField($model, 'idProduct', array('size' => 5, 'maxlength' => 50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'idCountry');
        echo $form->DropDownList($model, 'idCountry', HeList::getCountryList(), array('empty' => '--Any Country--'));
        ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'price'); ?>
        <?php echo $form->textField($model, 'price', array('size' => 5, 'maxlength' => 50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'enabled'); ?>
        <?php echo $form->DropDownList($model, 'enabled', HeList::getYesNoAliasBool(), array('empty' => '--')); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->