<?php
/* @var $this AbaExperimentsVariationsAttributesController */
/* @var $model AbaExperimentsVariationsAttributes */
?>

<div class="view">

    <b/><?php echo CHtml::encode($data->getAttributeLabel('experimentVariationId')); ?>:</b>
    <?php echo CHtml::encode($data->experimentVariationId); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('experimentVariationIdentifier')); ?>:</b>
    <?php echo CHtml::encode($data->experimentVariationIdentifier); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('experimentVariationDescription')); ?>:</b>
    <?php echo CHtml::encode($data->experimentVariationDescription); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('dateStart')); ?>:</b>
    <?php echo CHtml::encode($data->dateStart); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('dateEnd')); ?>:</b>
    <?php echo CHtml::encode($data->dateEnd); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('counterUsers')); ?>:</b>
    <?php echo CHtml::encode($data->counterUsers); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('limitUsers')); ?>:</b>
    <?php echo CHtml::encode($data->limitUsers); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('enabled')); ?>:</b>
    <?php echo CHtml::encode($data->enabled); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('isDefault')); ?>:</b>
    <?php echo CHtml::encode($data->isDefault); ?>
    <br/>

    <b/><?php echo CHtml::encode($data->getAttributeLabel('experimentVariationStatus')); ?>:</b>
    <?php echo CHtml::encode($data->experimentVariationStatus); ?>
    <br/>

</div>