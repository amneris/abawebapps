<?php
/* @var $this AbaExperimentsVariationsAttributesController */
/* @var $model AbaExperimentsVariationsAttributes */

$this->breadcrumbs = array(
  'Scenarios Prices',
);

$this->menu = array(
  array('label' => 'Create scenarios price', 'url' => array('create')),
  array('label' => 'Manage scenarios prices', 'url' => array('admin')),
);
?>

<h3>Scenarios Prices</h3>

<?php $this->widget(
  'zii.widgets.CListView',
  array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
  )
); ?>
