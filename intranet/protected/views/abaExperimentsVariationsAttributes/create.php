<?php
/* @var $this AbaExperimentsVariationsAttributesController */
/* @var $model AbaExperimentsVariationsAttributes */

$this->breadcrumbs = array(
  'Scenarios Prices' => array('admin'),
  'Create',
);

$this->menu = array(
  array('label' => 'Manage scenarios prices', 'url' => array('admin')),
);
?>

    <h3>Scenarios Prices</h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>