<?php
/* @var $this AbaExperimentsVariationsAttributesController */
/* @var $model AbaExperimentsVariationsAttributes */

$this->breadcrumbs = array(
    'Scenarios Prices' => array('admin'),
    $model->experimentVariationAttributeId => array('view', 'id' => $model->experimentVariationAttributeId),
    'Update',
);

$this->menu = array(
    array('label' => 'List Scenarios Prices', 'url' => array('index')),
    array('label' => 'Create Scenarios Price', 'url' => array('create')),
    array('label' => 'View Scenarios Price', 'url' => array('view', 'id' => $model->experimentVariationAttributeId)),
    array('label' => 'Manage Scenarios Prices', 'url' => array('admin')),
);
?>

<h3>Scenarios Prices: <?php echo $model->experimentVariationAttributeId; ?></h3>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>