<?php
/* @var $this AbaExperimentsVariationsAttributesController */
/* @var $model AbaExperimentsVariationsAttributes */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'id' => 'aba-experiments-variations-attributes-form',
            'enableAjaxValidation' => false,
        )
    );

    $modelCountry = new AbaCountry;
    $modelExperimentsVariations = new AbaExperimentsVariations();

    $optionsCountry = $modelCountry->getFullList();
    $optionsProductGroup = $modelExperimentsVariations->getFullList(true);

    $options = HeList::getYesNoAliasBool();

    $cssPrefix = (isset($wAction) AND $wAction == 'update' ? 'Cm' : '');

    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <table id="myForm">
        <?php if (is_numeric($model->experimentVariationAttributeId)): ?>
            <tr>
                <td>
                    <?php echo $form->labelEx($model, 'experimentVariationAttributeId'); ?>
                </td>
                <td>
                    <?php echo $model->experimentVariationAttributeId; ?>
                    <?php echo $form->error($model, 'experimentVariationAttributeId'); ?>
                </td>
            </tr>
        <?php endif; ?>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'experimentVariationId'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'experimentVariationId', $optionsProductGroup, array()); ?>
                <?php echo $form->error($model, 'experimentVariationId'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'idProduct'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'idProduct', array('size' => 10, 'maxlength' => 20)); ?>
                <?php echo $form->error($model, 'idProduct'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'idCountry'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'idCountry', $optionsCountry, array()); ?>
                <?php echo $form->error($model, 'idCountry'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'price'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'price', array('size' => 5, 'maxlength' => 20)); ?>
                <?php echo $form->error($model, 'price'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'enabled'); ?>
            </td>
            <td>
                <?php echo $form->DropDownList($model, 'enabled', $options); ?>
                <?php echo $form->error($model, 'enabled'); ?>
            </td>
        </tr>
    </table>
    <div class="row buttons" align="center">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->