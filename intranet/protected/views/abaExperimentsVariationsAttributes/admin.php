<?php
/* @var $this AbaExperimentsVariationsAttributesController */
/* @var $model AbaExperimentsVariationsAttributes */

$this->breadcrumbs = array(
    'Scenarios Prices',
);

$this->menu = array(
    array('label' => 'Create Scenarios Price', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript(
    'search',
    "
   $('.search-button').click(function(){
     $('.search-form').toggle();
     return false;
   });
   $('.search-form form').submit(function(){
     $.fn.yiiGridView.update('aba-experiments-variations-attributes-grid', {
       data: $(this).serialize()
     });
     return false;
   });
   "
);
?>

<h3>Scenarios Prices</h3>

<?php echo CHtml::link('Create new scenarios prices', array('AbaExperimentsVariationsAttributes/create')); ?>
<br>
<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial(
        '_search',
        array(
            'model' => $model,
        )
    ); ?>
</div><!-- search-form -->

<?php $this->widget(
    'application.components.widgets.CGridViewABA',
    array(
        'id' => 'aba-experiments-variations-attributes-grid',
        'cssFile' => Yii::app()->request->baseUrl . '/css/styles.css',
        'pager' => array(
            'cssFile' => Yii::app()->request->baseUrl . '/css/pager.css',
            'prevPageLabel' => '<',
            'nextPageLabel' => '>',
            'firstPageLabel' => '<<',
            'lastPageLabel' => '>>',
        ),
        'extraInfo' => array('view', 'update', 'delete'),
        'dataProvider' => $model->search(),
        'filter' => $model,
        'filterSelector' => '{filter}',
        'columns' => array(
            'experimentVariationAttributeId',
            'experimentVariationId' => array(
                'name' => 'experimentVariationId',
                'header' => "Scenarios Price",
                'filter' => CHtml::listData(HeList::getExperimentsVariationsList(true), 'id', 'title'),
            ),
            'idProduct',
            'idCountry' =>
                array(
                    'name' => 'idCountry',
                    'type' => 'countryList',
                    'filter' => HeList::getCountryList(),
                    'htmlOptions' => array('width' => "120px")
                ),
            'price',
            'enabled' =>
              array(
                'name' => 'enabled',
                'type' => 'YesNoBoolList',
                'filter' => HeList::getYesNoAliasBool(),
                'htmlOptions' => array('width' => "120px")
              ),
            array(
                'class' => 'CButtonColumnABA',
                'header' => 'Options',
            ),
        ),
    )
); ?>
