#!/usr/bin/php
<?php
ini_set('max_execution_time',0);
echo("=====Start GeneraMessage==". date("Y-m-d H:i:s")."]"."\n\n");

///////////////////////////////////////////////////////////////////////////
$g_languageArray=array (
"es",
"en",
"fr",
"it",
"pt",
);
$g_languageTranslatorArray=array (
"es"=>"es",
"en"=>"es",
"fr"=>"fr",
"it"=>"es",
"pt"=>"es",
);

$g_Translations=array ();
$g_Teachers=array (
"es"=>"Graham",
"en"=>"Graham",
"fr"=>"Nicky",
"it"=>"Donato",
"pt"=>"Priscilla",
);

$g_TeachersName=array (
200001=>"Graham", //en, es
200087=>"Nicky", //fr
200071=>"Donato", //it
200085=>"Priscilla", //pt
200036=>"Miles", 
200015=>"Martine",
200084=>"Kate",
);


$g_IdTeachers=array (

	/*Graham*/ "es"=>200001,
	/*Graham*/ "en"=>200001,
	/*Priscilla*/ "pt" =>200085,
	/*Donato*/ "it"=> 200071, 
	/*Nicky*/
				"fr"=>200087 ,
	/*Miles*/ 	
	//			""=>200036 
	/*Martine j*/ 
	//			""=>  200015,
	/*KATE -> ABA Teachers( My	Teacher)*/
	//			""=> 200084,
	//			""=>  200083
);

$g_IdTeachersLangs=array (

	/*Graham*/ 	200001=>"es;en",
	/*Priscilla*/ 200085=>"pt",
	/*Donato*/  200071=>"it", 
	/*Nicky*/	200087=>"fr" ,
);

$g_IdTeachersExceptions=array (

	/*Graham*/200001 =>"200085,200071,200087,200036,200015,200084,200083",
	/*Priscilla*/200085 =>"200001,200071,200087,200036,200015,200084,200083",
	/*Donato*/200071 =>"200001,200085,200087,200036,200015,200084,200083",
	/*Nicky*/200087 =>"200001,200085,200071,200036,200015,200084,200083",
);
//parametros conexion produccion 
$kDB_IP_Production="172.16.1.19";
$kDB_User_Production="root_aba";
$kDB_Password_Production="enero26";

//parametros conexion ABAB2C 
/*
$kDB_IP_ABAB2C_Production="172.16.1.100";
$kDB_User_ABAB2C_Production="root_aba";
$kDB_Password_ABAB2C_Production="setiembre27";
$DDBB_TRG_ABAB2C="aba_b2c_prod";//"ABA_B2C";
*/

$kDB_IP_ABAB2C_Production="172.16.1.20";
$kDB_User_ABAB2C_Production="abaadmin";
$kDB_Password_ABAB2C_Production="YghIySj4bhunc";
$DDBB_TRG_ABAB2C="aba_b2c";//"ABA_B2C";


$WORD4CARDS="Yii ABA English Key";
$today = date("Y-m-d H:i:s");
$yesterday = date("Y-m-d", mktime(0, 0, 0, date("m"),date("d")-1,date("Y")));
$yesterdayMark="2013-01-06";
$today2 = date("Y-m-d 00:00:00");

///////////////////////////////////////////////////////////////////////////
///////////////////////Conexion Mysql//////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
class ConexionMySQL
{
   function ConexionMySQL()     /** Constructor. No hace nada. **/
   {}
/***************************************************************************
FUNCION connect(servidor, usuario, clave)
Conecta con el servidor, identificandose como usuario y clave
****************************************************************************/
function connect($servidor,$usuario,$clave)
{
     //$this->link = pg_pconnect($params); //crea una conexion persistente con la bd
    $this->link = mysql_connect($servidor,$usuario,$clave); //crea una conexion con la bd
    if (!$this->link) {
      exit();
	}
     return $this->link;
}
/***************************************************************************
FUNCION close()
Finaliza la conexion con el servidor
****************************************************************************/
function close()
   {
     return mysql_close($this->link);
   }
/***************************************************************************
FUNCION select_db(base_datos)
Elige la base de datos con la que se va a trabajar
***************************************************************************/
function select_db($base_datos)
   {
     return  mysql_select_db($base_datos, $this->link);
   }

/***************************************************************************
FUNCION query(query)
Realiza una consulta o actualizacion de la BD
****************************************************************************/
function query($query)
   {
     return mysql_query($query,$this->link);
   }
/***************************************************************************
FUNCION fetch_array(resultado)
Realiza una consulta o actualizacion de la BD
***************************************************************************/
function fetch_array($resultado)
   {
     return mysql_fetch_array($resultado);
   }
/***************************************************************************
FUNCION num_rows(resultado)
Devuelve numero de filas
***************************************************************************/
function num_rows($resultado) 
   {
     return mysql_num_rows($resultado);
   }
/***************************************************************************
FUNCION fetch_object(resultado)
Extrae una fila de resultado como un objeto
***************************************************************************/
function fetch_object($resultado) 
   {
     return mysql_fetch_object($resultado);
   }
/***************************************************************************
FUNCION insert_id()
Devuelve el identificador generado en la �ltima llamada a INSERT 
***************************************************************************/
function insert_id() 
   {
     return mysql_insert_id($this->link); //JANUS  "return mysql_insert_id();" pasa a ser mysql_insert_id($this->link);  29-10-2008
   }
/***************************************************************************
FUNCION free_result(resultado)
Libera la memoria ocupada por un resultado
***************************************************************************/
function free_result($resultado)
   {
     return mysql_free_result($resultado);
   }
}   
///////////////////////////////////////////////////////////
function replaceText($buffer,$toreplace,$replacement)
{
	$theKeys = array($toreplace);
    $theValues = array($replacement);
    $thebuffer = str_replace($theKeys, $theValues, $buffer);
	return $thebuffer;
}
///////////////////////////////////////////////////////////
function getTranslation($lg)
{
	global $g_languageArray, $g_languageTranslatorArray,$g_Translations;
//	return $g_Translations[$g_languageTranslatorArray[$lg]];
	if($lg != 'es' && $lg != 'fr' && $lg != 'pt' && $lg != 'it')
		$lg='en';
	return $g_Translations[$lg];
}
///////////////////////////////////////////////////////////

function getTexts($prefix="premium_")
{
	global $g_languageArray, $g_languageTranslatorArray, $g_Translations;
	$g_Translations=array ();
	$basePathTrans = dirname(__FILE__);

	foreach($g_languageArray as $lg)
	{
		$theVal=file_get_contents ($prefix."title_".$lg.".txt");
		$theVal2=file_get_contents ($prefix."body_".$lg.".txt");
		$auxArray=array("title"=>$theVal, "body"=>$theVal2);
		
		$g_Translations[$lg]= $auxArray;
/*		$theVal=file_get_contents ($prefix.$lg.".php");
		$lines=explode('",',$theVal);
		$auxArray=array();
		foreach($lines as $line)
		{
			$theVal=$line;
			//$theVal=replaceText($theVal,"\',","\'");
			$item=explode("=>",$theVal);
			$title= replaceText(trim($item[0]),'##',"#");	
			$title= replaceText($title,'#"',"#'#");	
			$title= replaceText($title,'"',"");			
			$title= replaceText($title," ","");	
			$value= replaceText($item[1], '##',"#");	
			$value= replaceText($value, '#"',"#'#");
			$value= replaceText($value, '"',"");
			$value= replaceText($value, "#'#",'"');
			$auxArray[$title]=$value;
		}
		$g_Translations[$lg]= $auxArray;*/
		
	}
	return $g_Translations;
}
///////////////////////////////////////////////////////////
function getTeacher($lg)
{
	global $g_Teachers;
	$ret=$g_Teachers[$lg];
	return $ret==""?$g_Teachers["en"]:$ret;
}

function getNameTeacher($lg, $id)
{
	global $g_TeachersName;
	if($g_TeachersName[$id] == "")
		return getTeacher($lg);
	else
		return $g_TeachersName[$id];
}
///////////////////////////////////////////////////////////

/////////////////////////////////
function updateToFinalTeacherId() 
{
	
/*Graham*/ //es 200001 
/*Priscilla*/ //es 200085 
/*Donato*/ // 200071 y 200086
/*Nicky*/ //200087
/*Miles*/ //200036
/*Martine Jeffrey*/ // 200015
/*ABA Teachers*/ // 200084 
/*	My	Teacher*/ // 200083
	global $conexionSrc, $conexionTrg;
	global $kDB_IP_Production,$kDB_User_Production,$kDB_Password_Production;
	global $kDB_IP_ABAB2C_Production, $kDB_User_ABAB2C_Production,$kDB_Password_ABAB2C_Production;
	global $DDBB_TRG_ABAB2C;
	global $g_IdTeachers,$g_IdTeachersLangs,$g_IdTeachersExceptions;
    /* */
	$queryTrg="UPDATE `$DDBB_TRG_ABAB2C`.`user` u SET u.teacherId='200071' WHERE u.teacherId='200086';"; //all to Dongallo
	echo("$queryTrg\n");
	$conexionTrg->query($queryTrg);
	$queryTrg="UPDATE `$DDBB_TRG_ABAB2C`.`user` u SET u.teacherId='200084' WHERE u.teacherId='200083';"; //all to Kate
	echo("$queryTrg\n");
	$conexionTrg->query($queryTrg);
	foreach ($g_IdTeachers as $lg=>$id) 
	{
		$queryTrg="UPDATE `$DDBB_TRG_ABAB2C`.`user` u SET u.teacherId='$id' WHERE langCourse='$lg' and (u.userType=1 or u.userType=2) and u.teacherId not in(".$g_IdTeachersExceptions[$id].");";
		echo("$queryTrg\n");
		$conexionTrg->query($queryTrg);
	}
}

function cleanMessages() 
{
	global $conexionSrc, $conexionTrg;
	global $kDB_IP_Production,$kDB_User_Production,$kDB_Password_Production;
	global $kDB_IP_ABAB2C_Production, $kDB_User_ABAB2C_Production,$kDB_Password_ABAB2C_Production;
	global $DDBB_TRG_ABAB2C;
	global $g_IdTeachers,$g_IdTeachersLangs,$g_IdTeachersExceptions;
	$queryTrg="truncate `$DDBB_TRG_ABAB2C`.`messages`"; //all to Dongallo
	echo("$queryTrg\n");
	$conexionTrg->query($queryTrg);
}
/////////////////////////////////
function changeTeacher()
{
/**
Los alumnos de Graham Weeks se quedan con Graham.
Graham: se le asignar�n nuevos alumnos en espa�ol y en ingl�s.

Los alumnos de Priscilla Oliveira se quedan con Priscilla
Priscilla: se le asignar�n nuevos alumnos en portugu�s.

Los alumnos de Donato Gallo y de Don Gallo se quedan con Donato.
Donato: se le asignar�n nuevos alumnos en italiano.

Los alumnos de Nicola Cowan se quedan con Nicky.
Nicky: se le asignar�n nuevos alumnos en franc�s.

Los alumnos de Miles Prat se quedan con Miles
Miles: no se le asignan nuevos alumnos.

Los alumnos de Martine Jeffries se quedan con Martine
Martine: no se le asignan nuevos alumnos.

Los alumnos de ABA Teachers y My teahers se queda con Kate
Kate: no se le asignar�n nuevos alumnos.
#3

**/
	global $conexionSrc, $conexionTrg;
	global $kDB_IP_Production,$kDB_User_Production,$kDB_Password_Production;
	global $DDBB_TRG_ABAB2C;
	$theTeacher=' ';
	$queryTrg="SELECT * FROM `$DDBB_TRG_ABAB2C`.user WHERE userType='1' or userType='2'";
	echo("$queryTrg\n");
	$res_m=$conexionTrg->query($queryTrg);
	/*
CREATE TABLE  `aba_b2c_prod`.`messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `subject` varchar(256) NOT NULL DEFAULT '',
  `body` text,
  `is_read` enum('0','1') NOT NULL DEFAULT '0',
  `deleted_by` enum('sender','receiver') DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sender` (`sender_id`),
  KEY `reciever` (`receiver_id`)
) ENGINE=MyISAM AUTO_INCREMENT=113 DEFAULT CHARSET=utf8;
	*/

}


function SendMessageToUser($type,$extra="", $inLang="") 
{
	global $conexionSrc, $conexionTrg;
	global $kDB_IP_Production,$kDB_User_Production,$kDB_Password_Production;
	global $DDBB_TRG_ABAB2C;
	
	if($type==1)
		getTexts("free_");
	else	
		getTexts("premium_");
	$filter="";//"and id='779' limit 1";		
	//$filter=" and teacherId='200084'";//"and id='779' limit 1";		
	if($inLang)
		$queryTrg="SELECT '$inLang' as langCourse , $type as userType, name, teacherId, id FROM `$DDBB_TRG_ABAB2C`.user WHERE $filter $extra";
	else
		$queryTrg="SELECT langCourse, name, teacherId, id FROM `$DDBB_TRG_ABAB2C`.user WHERE userType='$type' $filter";
	echo("$queryTrg\n");
	$res_m=$conexionTrg->query($queryTrg);
	while ($row_m = @$conexionTrg->fetch_array($res_m)){
	
		$theArray = getTranslation($row_m['langCourse']);
		$theTeacher = getTeacher($row_m['langCourse']);
		$theTeacher = getNameTeacher($row_m['langCourse'], $row_m['teacherId']);
		$theBody = replaceText($theArray['body'],'<Juan>',$row_m['name']);	
		$theBody = mysql_real_escape_string(replaceText($theBody,'<Donato>',$theTeacher));	
		$theBody =replaceText($theBody,'\n',"<br/>");	
		$sql = "INSERT INTO `$DDBB_TRG_ABAB2C`.`messages` (sender_id, receiver_id, subject, body, created_at) VALUES ('{$row_m['teacherId']}','{$row_m['id']}','{$theArray['title']}','$theBody',NOW())";
		//echo("$sql\n");	
		echo(".");	
		$res_m2=$conexionTrg->query($sql);
	}
}
//////////////////////////////////////////////////////////////
function UpdateEmailTeacher()
{
	global $conexionSrc, $conexionTrg;
	global $kDB_IP_Production,$kDB_User_Production,$kDB_Password_Production;
	global $kDB_IP_ABAB2C_Production, $kDB_User_ABAB2C_Production,$kDB_Password_ABAB2C_Production;
	global $DDBB_TRG_ABAB2C;
	global $g_IdTeachers,$g_IdTeachersLangs;
	$theEmail =array(
	/*Graham*/  	200001 => "gweeks@abaenglish.com",
	/*Priscilla*/ 	200085 => "poliveira@abaenglish.com",
	/*Donato*/ 
					200071 /* y 200086*/ => "dgallo@abaenglish.com",
	/*Nicky*/		200087 => "ncowan@abenglish.com",
	/*Miles*/ 		200036 => "mprat@abaenglish.com",
	/*Martine j*/   200015 => "mjeffries@abaenglish.com",
	/*KATE -> ABA Teachers( My	Teacher)*/
					200084 /*y 200083*/ => "kestivill@abaenglish.com",
	/*	My	Teacher*/ // 200083  => "",
	/*Nicky*/ //200087
	/*Miles*/ //200036
	/*Martine Jeffrey*/ // 200015
	/*ABA Teachers*/ // 200084 
	/*	My	Teacher*/ // 200083

	);
	
	$query="UPDATE `$DDBB_TRG_ABAB2C`.`user` u SET email ='old_mjeffries@abaenglish.com' WHERE email ='mjeffries@abaenglish.com';";
	echo($query."\n");
	$conexionTrg->query($query);
	
	$query="UPDATE `$DDBB_TRG_ABAB2C`.`user` u SET email ='old_kestivill@abaenglish.com' WHERE email ='kestivill@abaenglish.com';";
	echo($query."\n");
	$conexionTrg->query($query);
	
	$query="UPDATE `$DDBB_TRG_ABAB2C`.`user` u SET email ='old_ncowan@abenglish.com' WHERE email ='ncowan@abenglish.com';";
	echo($query."\n");
	$conexionTrg->query($query);
	
	foreach ($theEmail as $key=>$value) 
	{
		$query="UPDATE `$DDBB_TRG_ABAB2C`.`user` u SET email ='$value' WHERE id='$key';";
		echo($query."\n");
		$conexionTrg->query($query);
	}
	foreach ($g_IdTeachersLangs as $key=>$lg) 
	{
		$query="UPDATE `$DDBB_TRG_ABAB2C`.`user_teacher` u SET language='$lg' WHERE userid ='$key'";
		echo($query."\n");
		$conexionTrg->query($query);
	}
}

//////////////////////////////////////////////////////End Classe/////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
$conexionSrc = new ConexionMySQL();
$conexionTrg = new ConexionMySQL();

//Production Old Abaenglish database settings
$link= $conexionSrc->connect($kDB_IP_Production,$kDB_User_Production,$kDB_Password_Production);
$conexionSrc->select_db("DBABAELEARNING",$link);
//ABA_B2C database settings
$link2= $conexionTrg->connect($kDB_IP_ABAB2C_Production, $kDB_User_ABAB2C_Production, $kDB_Password_ABAB2C_Production);
$conexionTrg->select_db("$DDBB_TRG_ABAB2C",$link2);


///////////////////////////////////////////////////////////
/*
$textArray=getTexts();
//echo("=======VARDUMP===========\n");
//	var_dump($textArray);

foreach($textArray as $lg=>$value)
{
	echo("$lg ". "title ".$value["title"]."\n body ".$value["body"]."\n");
}
*/
$theArray=getTranslation('es');
//echo($theArray["title"]. "============>".$theArray["body"]);
//updateToFinalTeacherId(); //done at migra.php
//UpdateEmailTeacher();//done at migra.php
CleanMessages();
SendMessageToUser(1) ; //free
SendMessageToUser(2) ; //Premium
/*
SendMessageToUser(1, " id=779", "es") ; //free
SendMessageToUser(1, " id=779", "en") ; //free
SendMessageToUser(1, " id=779", "fr") ; //free
SendMessageToUser(1, " id=779", "it") ; //free
SendMessageToUser(1, " id=779", "pt") ; //free

SendMessageToUser(2, " id=779", "es") ; //Premium
SendMessageToUser(2, " id=779", "en") ; //Premium
SendMessageToUser(2, " id=779", "fr") ; //Premium
SendMessageToUser(2, " id=779", "it") ; //Premium
SendMessageToUser(2, " id=779", "pt") ; //Premium
*/
echo("=====End==". date("Y-m-d H:i:s")."]"."\n\n");
?>