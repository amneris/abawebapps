ALTER TABLE `db_ventasflash`.`ventasFlashScript` ADD COLUMN `url` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Landing Url of our website' AFTER `agrupadorName`;
/*--------------------------------------*/
/* ----ENH-1578-------- */
CREATE USER 'abaapps'@'%' IDENTIFIED BY 'abaenglish';

GRANT SELECT ON aba_b2c.* TO 'abaapps'@'%';
GRANT UPDATE ON aba_b2c.* TO 'abaapps'@'%';
GRANT DELETE ON aba_b2c.* TO 'abaapps'@'%';
GRANT INSERT ON aba_b2c.* TO 'abaapps'@'%';
GRANT EXECUTE ON aba_b2c.* TO 'abaapps'@'%';
GRANT SELECT ON aba_b2c_logs.* TO 'abaapps'@'%';
GRANT UPDATE ON aba_b2c_logs.* TO 'abaapps'@'%';
GRANT DELETE ON aba_b2c_logs.* TO 'abaapps'@'%';
GRANT INSERT ON aba_b2c_logs.* TO 'abaapps'@'%';
/* ------------ */
GRANT SELECT ON db_ventasflash.* TO 'abaapps'@'%';
GRANT UPDATE ON db_ventasflash.* TO 'abaapps'@'%';
GRANT INSERT ON db_ventasflash.* TO 'abaapps'@'%';
/* --------------------------------------------------- */
CREATE DATABASE `aba_b2c_summary` /*!40100 DEFAULT CHARACTER SET utf8 */;
ALTER TABLE aba_b2c.payments_export_mem_aux RENAME aba_b2c_summary.payments_export_mem_aux;

CREATE USER 'abaslave'@'%' IDENTIFIED BY 'abaenglish';

GRANT SELECT ON aba_b2c.* TO 'abaslave'@'%';
GRANT SELECT ON aba_b2c_logs.* TO 'abaslave'@'%';
GRANT SELECT ON db_ventasflash.* TO 'abaslave'@'%';
GRANT SELECT ON aba_b2c_summary.* TO 'abaslave'@'%';
GRANT UPDATE ON aba_b2c_summary.* TO 'abaslave'@'%';
GRANT DELETE ON aba_b2c_summary.* TO 'abaslave'@'%';
GRANT INSERT ON aba_b2c_summary.* TO 'abaslave'@'%';
GRANT EXECUTE ON aba_b2c_summary.* TO 'abaslave'@'%';
GRANT DROP ON aba_b2c_summary.* TO 'abaslave'@'%';
GRANT CREATE TEMPORARY TABLES ON aba_b2c_summary.* TO 'abaslave'@'%';
/* --------------------------------------------------- */
/* --------------------------------------------------- */

/*------- ENH-2833 --------------------------------*/
INSERT INTO config (`key`, `value`,`description`)
VALUES( 'NEXT_ITERATION_SHOT_PROGRESS', '2014-05-26, 0, 1000', 'Traffic light for process ShotUserProgressIncremental: date, Start, Quantity')
ON DUPLICATE KEY UPDATE `value`='2014-05-26, 0, 1000';
/* ------------------------------------------------*/
/*------- ENH-2833 --------------------------------*/
DROP TABLE IF EXISTS `aba_b2c_summary`.`users_progress_lastweek`;
CREATE TABLE  `aba_b2c_summary`.`users_progress_lastweek` (
  `userId` int(4) unsigned NOT NULL COMMENT 'FK to aba_b2c.user',
  `unitId` tinyint(3) unsigned NOT NULL COMMENT 'theme Id, course unit id',
  `dateCapture` date NOT NULL COMMENT 'Date progress from user was collected. Day after weekRef.',
  `yearRef` tinyint(3) unsigned NOT NULL COMMENT 'Year of this capture',
  `weekRef` tinyint(3) unsigned NOT NULL COMMENT 'Week of the year, YYYYWW',
  `currentLevel` tinyint(3) unsigned NOT NULL COMMENT 'Current level of the user at dateCapture',
  `totalProgress` tinyint(3) unsigned default NULL COMMENT 'Total followUp',
  `abaFilm` tinyint(3) unsigned default NULL COMMENT 'Section 1',
  `speak` tinyint(3) unsigned default NULL COMMENT 'Section 2',
  `write` tinyint(3) unsigned default NULL COMMENT 'Section 3',
  `interpret` tinyint(3) unsigned default NULL COMMENT 'Section 4',
  `videoClass` tinyint(3) unsigned default NULL COMMENT 'Section 5',
  `exercises` tinyint(3) unsigned default NULL COMMENT 'Section 6',
  `vocabulary` tinyint(3) unsigned default NULL COMMENT 'Section7',
  `assessment` tinyint(3) unsigned default NULL COMMENT 'Evaluation section, 100 or 0',
  PRIMARY KEY  (`userId`,`unitId`),
  KEY `byDate` (`dateCapture`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Temporary table to capture progress increments';
/*------- ENH-2833 --------------------------------*/
DROP TABLE IF EXISTS `aba_b2c_summary`.`users_progress_byweek`;
CREATE TABLE  `aba_b2c_summary`.`users_progress_byweek` (
  `autoId` int(10) unsigned NOT NULL auto_increment,
  `userId` int(4) unsigned NOT NULL COMMENT 'FK to aba_b2c.user',
  `unitId` tinyint(3) unsigned NOT NULL COMMENT 'theme Id, course unit id',
  `dateCapture` date NOT NULL COMMENT 'Date progress from user was collected. Day after weekRef.',
  `yearRef` int(10) unsigned NOT NULL COMMENT 'Year of this capture',
  `weekRef` tinyint(3) unsigned NOT NULL COMMENT 'Week of the year, WW',
  `currentLevel` tinyint(3) unsigned NOT NULL COMMENT 'Current level of the user at dateCapture',
  `totalProgress` tinyint(3) unsigned default NULL COMMENT 'Total followUp',
  `abaFilm` tinyint(3) unsigned default NULL COMMENT 'Section 1',
  `speak` tinyint(3) unsigned default NULL COMMENT 'Section 2',
  `write` tinyint(3) unsigned default NULL COMMENT 'Section 3',
  `interpret` tinyint(3) unsigned default NULL COMMENT 'Section 4',
  `videoClass` tinyint(3) unsigned default NULL COMMENT 'Section 5',
  `exercises` tinyint(3) unsigned default NULL COMMENT 'Section 6',
  `vocabulary` tinyint(3) unsigned default NULL COMMENT 'Section7',
  `assessment` tinyint(3) unsigned default NULL COMMENT 'Evaluation section, 100 or 0',
  PRIMARY KEY  USING BTREE (`autoId`),
  UNIQUE KEY `byYearWeek` USING BTREE (`yearRef`,`weekRef`,`userId`,`unitId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=1 ROW_FORMAT=DYNAMIC COMMENT='Temporary table to capture progress increments';