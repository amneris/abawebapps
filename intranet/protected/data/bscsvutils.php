<?php
class BsReaderCSV
{
	public $mCountLines;
	public $mCountField;
	public $mLines;
	public $mSeparator;
	function __construct() 
	{
		$this->mCountLines = 0;
		$this->mCountField = 0;
		$this->mLines= array();
		$this->mSeparator=";";
    }
	
	function __destruct() 
	{
	}
	function SetSeparator($inSeparator)
	{
		$this->mSeparator=$inSeparator;
	}
	function LoadFile($inFilename,$doClear= true)
	{
		if($doClear)
			array_splice($this->mLines,0); // se suprime todo el array 
		if (!file_exists($inFilename))
		{
			echo('file not found: '.$inFilename);
			return false;
		}	
		if (!$fp = fopen($inFilename,"r")) 
		{
			return false;
		}else 
		{
			while (!feof($fp)) 
			{ 
				$linea = fgets($fp, 4096); 
				$linea =str_replace("\n", "", $linea);
				$linea =str_replace("\r", "", $linea);
				if ($linea!="")
					$this->mLines[]=$linea;
			}
			$this->mCountLines = count($this->mLines);	
			fclose($fp); 
		}	
		return true;
	} 	
	function ShowArray(&$inArray)
	{
		$theCountField = 0;
		echo "<table border='1'>\n";	
		foreach ($inArray as $value)
		{
			echo "<tr>\n";	
			$theArray=split($this->mSeparator,$value);
			$theCountField = 0;
			foreach ($theArray as $field)
			{
				echo "<td>";
				echo $field;	
				echo "</td>\n";
				$theCountField = $theCountField + 1;
			}
			echo "</tr>\n";	
		}
		echo "</table>\n";
		return $theCountField;
	}
	function ShowData()
	{
	
		$this->mCountField = $this->ShowArray($this->mLines);
		return $this->mCountField;
	}
	function ExistValue(&$inArray, $inValue)
	{
		foreach ($inArray as $value)
		{
			if ($value == $inValue)
			{
			//echo "[$value == $inValue] ";
				return true;
			}	
		}
		return false; 
	}
	
	function GetFields(&$inArrayFields, &$outArray, $doUseSort)
	{
		array_splice($outArray,0); // se suprime todo el array 
		foreach ($this->mLines as $line)
		{
			$theArray=split($this->mSeparator,$line);
			$newline="";
			$theCounter = 0; 
			if($doUseSort)
			{
				foreach ($inArrayFields as $field)
				{
					if(!$theCounter)
						$newline=$theArray[$field]; 
					else
						$newline=$newline.$this->mSeparator.$theArray[$field]; 
					$theCounter++;
				}
			}	
			else	
				foreach ($theArray as $field)
				{
					if($this->ExistValue($inArrayFields, $theCounter))
					{
						if($newline=="")
							$newline=$theArray[$theCounter]; 
						else
							$newline = $newline.$this->mSeparator.$theArray[$theCounter]; 
					}
					$theCounter++;
				}
			$outArray[] = $newline;
		}
	}	
	function GetArrayFieldIndex($inIndex, &$outArray)
	{
		array_splice($outArray,0); // se suprime todo el array 
		$inArray=$this->mLines;
		$isFirst= true;
		foreach ($inArray as $value)
		{
			if($isFirst)
				$isFirst= false; 
			else
			{
				$theArray=split($this->mSeparator,$value);
				$theCountField = 0;
				foreach ($theArray as $field)
				{
					if($theCountField==$inIndex)
					{
						$outArray[]= $field;
						//echo($field."<br>");
						break;
					}
					$theCountField = $theCountField + 1;
				}
			}
		}
		return $outArray;
	}
	function GetArrayByFieldname($inName)
	{
		$theIndex= -1;
		$outArray = array();
		$outArray = split($this->mSeparator,$this->mLines[0]); 
		$theLen = count($outArray); 
		for($n=0; $n< $theLen; $n++)
		{
			//echo("'".$outArray[$n]."' '".$inName."' ");
			if($inName==$outArray[$n])
			{
				$theIndex = $n;
				break;
			}
		} 
		return $this->GetArrayFieldIndex($theIndex, &$outArray);
	} 
	function GetArrayByIndex($inIndex)
	{
		$theIndex= -1;
		$outArray = array();
		$outArray = split($this->mSeparator,$this->mLines[0]); 
		$theLen = count($outArray); 
		if($theIndex<0 || $theIndex>$thecount)
			return array(); 
		return $this->GetArrayFieldIndex($inIndex, &$outArray);
	} 
	function ShowFields(&$inArrayFields, $doUseSort= true)
	{
		$outArray=array();
		$this->GetFields($inArrayFields, $outArray, $doUseSort);
		$this->ShowArray($outArray);
	}
}

?>