Ol� <Juan>,

Meu nome �<Donato> e sou seu professor de ABA English. Boas-vindas ao nosso novo m�todo de aprendizagem. Estou certo que com este m�todo voc� ser� capaz de dominar o Ingl�s at� o nivel que voc� mesmo se prop�e a realizar. 

Como um estudante ABA Free, voc� tem acesso todos os dias  a uma videoclasse a fim de aprender de uma forma sistem�tica e organizada toda a  gram�tica Ingl�sa, a partir do conte�do b�sico, at� chegar ao mais complexo.

Voc� tamb�m pode ver a  primeira unidade do curso de forma completamente gr�tis, segundo o n�vel selecionado. Isso permitir� que voc� possa experimentar a nossa metodologia �nica. Cada unidade do curso � composto por 8 se��es em que, a partir do ABA Film inicial, voc� pode estudar os conte�dos, realizar o ditado, desempenhar o papel de um personagem, completar exerc�cios escritos em Ingl�s, estudar a gram�tica, o vocabul�rio e em particular voc� pode praticar continuamente a express�o oral (o essencial para falar rapidamente o Ingl�s !)

Os ABA Films s�o curtas-metragens exclusivos da ABA English e servem como um ponto de partida em cada unidade. Os ABA Films cont�m fun��es comunicativas e as estruturas gramaticais que voc� vai aprender em cada unidade, onde, ent�o, vai estudar em profundidade o di�logo entre os personagens e onde voc� ser� capaz de usar todas as suas express�es para que eles possam ser usados no seu Ingl�s falado e escrito. Voc� vai aprender o Ingl�s online, assim como se voc� estivesse no estrangeiro!

Com o metodo natural, que foi projetado em ABA English, voc� vai aprender Ingl�s como as crian�as aprendem sua primeira l�ngua. Primeiro voc� precisa entender e falar naturalmente e espontaneamente, participando ativamente as situa��es atuais da vida real. Aos poucos, voc� vai consolidar outros aspectos, como a leitura ea escritura. Aprender ser� gratificante e voc�  vai ver muito progresso sem perceber.

Se voc� decidir ser um aluno ABA Premium, voc� pode tamb�m tirar proveito de todos os conte�dos do curso e eu mesmo vou responder a todas suas perguntas sobre o n�vel lingu�stico, acompanhando voc� nesta grande aventura que � dominar uma l�ngua.

Al�m disso, em ABA Premium, quando terminar cada n�vel do curso, voc� receber� o certificado oficial de American &British Academy, correspondente ao n�vel que voc� passou.

Uma sauda��o afectuosa
Keep on learning English,

<Donato>
