Hi, <Juan>!

My name is <Donato> and I am your ABA English teacher. I would like to welcome you to the New ABA English Campus which is more intuitive and complete but with the same effective method as before! 

I am going to explain the main changes which we have made in this new phase for ABA English.

Firstly, we have recorded the 144 "situations" with native actors in the United States and in Europe. In this way we have brought to life the course�s 300 characters and we have created the ABA Films!

Then we have improved our virtual study environment but keeping our unique learning system. We have also improved the usability of all the different tools in the Campus. Now our voice recognition technology, "Intonation", is more intuitive and easier to use. We have named it <b>Listen-Record-Compare</b>.

However, the good news doesn�t stop here! We have also improved the grammar section! Now the American & British Academy teachers will explain English grammar to you via video classes.  Learning grammar will now be even easier and more enjoyable!

Finally, when you finish each level in the course, you will continue getting the American & British Academy official certificate for the level passed, but beforehand you will have to do the final test for each unit which you will find in the new "Assessment" section.
I hope that you continue enjoying our unique learning method. Remember, I will continue to be there for you during your English learning process and you will be able to contact me with any doubts of a linguistic nature that you may have. 

Keep on learning English!
Best wishes,

<Donato>