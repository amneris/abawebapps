<?php

ini_set('memory_limit','256M');

// It includes all common files and folders shared between different projects:
require dirname(__FILE__) . '/../../common/bootstrap.php';


// These keys overwrite the ones in the table config in the database. It is useful for customize a local environment ignoring the development
// database and without annoying ur sensitive buddies.
if (file_exists(ROOT_DIR . '/campus/protected/config/ProgrammerConfigKeys.php')) {
    include_once(ROOT_DIR . '/campus/protected/config/ProgrammerConfigKeys.php');
}


// Change the following paths if necessary
$yii=dirname(__FILE__).'/../../yii/framework/yii.php';
$config=dirname(__FILE__).'/../protected/config/console.php';

/*// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',false);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);*/

require_once($yii);

// creating and running console application
Yii::createConsoleApplication( $config )->run();