<?php

require dirname(__FILE__) . '/../../common/bootstrap.php';


if (file_exists(ROOT_DIR . '/intranet/protected/config/ProgrammerConfigKeys.php')) {
    include_once(ROOT_DIR . '/intranet/protected/config/ProgrammerConfigKeys.php');
}

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
// LIVE ENVIRONMENT ****
//defined('YII_DEBUG') or define('YII_DEBUG', false);


$config = ROOT_DIR . '/intranet/protected/config/test.php';
Yii::createWebApplication($config)->run();
