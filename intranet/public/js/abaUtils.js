/**
 * Created with JetBrains PhpStorm.
 * User: Quino
 * Date: 14/10/13
 * Time: 12:03
 * To change this template use File | Settings | File Templates.
 */

function enableEmailField(fieldEmail) {
    $("#AbaUser_email").attr("disabled",!$("#AbaUser_email").attr("disabled"));
    $("#helpEmailEdit").toggle();
}

function enableCountryField(fieldCountry) {
    $("#AbaUser_countryId").attr("disabled", !$("#AbaUser_countryId").attr("disabled"));
}

/**
 * Displays or hides the help in the form update user.
 */
function displayHelpExpirationDate()
{
    $("#helpExpirationDateEdit").toggle();
}

// ++++++++++++++ VIEW /views/LogReconciliation/_search.php ++++++++++++++++
function exportLogReconcToExcel()
{
    frmExportLogReconc = $('#formLogReconcSearch') ;
    document.location.href =  '/index.php?r=reconciliationParser/logreconciliation/export';
//    frmExportLogReconc.setAttribute('action', '/index.php?r=reconciliationParser/logreconciliation/export');
//    frmExportLogReconc.submit();
//    $(this).disable();
}
// ------------------------------------------------------------------------------
// ++++++++++++++ VIEW /views/payments/admin.php ++++++++++++++++
function highlightPayment( row, markReference )
{
    idRow = row.attr("id");
    aIdsPays = idRow.split('-');
    if(aIdsPays.count<2 || aIdsPays[2].trim()==''){
        return;
    }
    rowIdRef = aIdsPays[2];
    cellOne = $("[id^=trPay-"+rowIdRef+"-]").find('td:nth-child(2)');
    if( cellOne.length == 0 ){
        return;
    }
    currentTxt = cellOne.text();
    cstTxt = '(*origin) ';
    if (markReference){
        currentTxt = currentTxt + cstTxt;
        cellOne.text(currentTxt);
    } else {
        currentTxt = currentTxt.replace(cstTxt, '');
        cellOne.text( currentTxt );
    }
}
// --------------------------------------------------------------