$(document).ready(function(){
/*------------EVENTS BINDING---------------*/

// ++++++++++++++ VIEW ImportReconciliation/views/importFile.php ++++++++++++++++
    if ($("#myFormReconciliation").length > 0){
        $("#btnSubmit").click( function() {
            $(this).css("display","none");
            $('#IdWaitingSendFile').show();
        });
    }
// ------------------------------------------------------------------------------
// ++++++++++++++ VIEW ImportReconciliation/views/importFile.php ++++++++++++++++
    if ($("#aba-refund-form").length > 0){
        if ( $("#AbaPayments_paySuppExtId").val()=='4' ){
            var str = $("#AbaPayments_paySuppOrderId").val();
            if ( str.indexOf("REF", (str.length - 3)) < 0 ) {
                $("#AbaPayments_paySuppOrderId").val( str + "REF" );
            } else {
                $("#AbaPayments_paySuppOrderId").val( str );
            }
        }

    }
// ------------------------------------------------------------------------------

// ++++++++++++++ VIEW aba-payments-grid ++++++++++++++++
    if( $('#aba-payments-grid').length > 0){
        $('#aba-payments-grid table tbody tr').mouseenter( function(){
            highlightPayment($( this ), true);
        });
        $('#aba-payments-grid table tbody tr').mouseout( function(){
            highlightPayment($( this ), false);
        });
    }
// ------------------------------------------------------------------------------

});