<?php

/**
 * A Yii application can run in either debug or production mode, as determined by the value of the constant YII_DEBUG.
 * By default, this constant value is defined as false, meaning production mode. To run in debug mode, define this
 * constant as true before including the yii.php file. Running the application in debug mode is less efficient because
 * it keeps many internal logs. On the other hand, debug mode is also more helpful during the development stage because
 * it provides richer debugging information when an error occurs.
 */
if (getenv('YII_DEBUG')) {
// DEVELOPMENT, INTEGRATION and STAGE: ****
    define('YII_DEBUG', true);
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
} else {
// LIVE ENVIRONMENT ****
    defined('YII_DEBUG') or define('YII_DEBUG', false);
}

require dirname(__FILE__) . '/../../common/bootstrap.php';


if (file_exists(ROOT_DIR . '/campus/protected/config/ProgrammerConfigKeys.php')) {
    include_once(ROOT_DIR . '/campus/protected/config/ProgrammerConfigKeys.php');
}

$config = ROOT_DIR . '/intranet/protected/config/main.php';
Yii::createWebApplication($config)->run();
