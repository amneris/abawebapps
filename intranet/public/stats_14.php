<?php

require dirname(__FILE__) . '/../../common/bootstrap.php';

// remove the following line when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);

$config = ROOT_DIR . '/intranet/protected/config/stats.php';

require_once($yii);
Yii::createWebApplication($config)->run();
