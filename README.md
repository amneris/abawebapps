# abawebapps

## Development Environment Setup

- Install the [*Development Environment Tools*](#development-environment-tools)
- Setup SSH keys https://help.github.com/articles/generating-ssh-keys/#platform-mac
- Clone repo
    - `git clone git@github.com:abaenglish/abawebapps.git`
- Install *composer* locally:
    - `cd abawebapps`
    - `curl -sS https://getcomposer.org/installer | php`
- Install *unix2dos* (convert line breaks in subtitles (.srt) files from Unix format (Line feed) to DOS format)
    - `brew install unix2dos`
- Install dependencies:
    - `./composer.phar install`
- Setup /etc/hosts:
    - [Check the hosts configuration!](docs/hosts.md)

- Clone de [transactions project ](https://github.com/abaenglish/transactions) (in the parent folder)
    - `cd ..`
    - `git clone git@github.com:abaenglish/transactions.git`

- Clone de [infrastructure project ](https://github.com/abaenglish/infrastructure) (in the parent folder)
    - `cd ..`
    - `git clone git@github.com:abaenglish/infrastructure.git`

- In order to run the local environments, you need:
  
  - All the [*Development Environment Tools*](#development-environment-tools) installed and working.
  

- Run it!.
    - `cd abawebapps`
    - `vagrant up`

- browse to https://campus.aba.local
- enter username and password
    - see also - http://redmine.abaenglish.com/issues/5830
    - and - https://docs.google.com/spreadsheets/d/1R49cr9eY5ZZLbvSgdO5Rnkewh-mtAbfPpWDqCTgOo1Q/edit#gid=0

| username | password | lang | usertype |
| --- | --- | --- | --- |
| student+de@abaenglish.com | abaenglish | de | premium |
| student+en@abaenglish.com | abaenglish | en | premium |
| student+es@abaenglish.com | abaenglish | es | premium |
| student+fr@abaenglish.com | abaenglish | fr | premium |
| student+it@abaenglish.com | abaenglish | it | premium |
| student+pt@abaenglish.com | abaenglish | pt | premium |
| student+ru@abaenglish.com | abaenglish | ru | premium |
| student+zh@abaenglish.com | abaenglish | zh | premium |
| student+tr@abaenglish.com | abaenglish | tr | premium |
| student+def@abaenglish.com | abaenglish | de | free |
| student+enf@abaenglish.com | abaenglish | en | free |
| student+esf@abaenglish.com | abaenglish | es | free |
| student+frf@abaenglish.com | abaenglish | fr | free |
| student+itf@abaenglish.com | abaenglish | it | free |
| student+ptf@abaenglish.com | abaenglish | pt | free |
| student+ruf@abaenglish.com | abaenglish | ru | free |
| student+zhf@abaenglish.com | abaenglish | zh | free |
| student+trf@abaenglish.com | abaenglish | tr | free |

   
- database setup
    - The default setup is using an Amazon RDS instance (dbdev.aba.land) as a development DB. If you are working from outside the office you need to add your IP to the security group.  
    - **TBD** - the ideal setup would be that the vagrant provision step should create a minimal MySQL database with enough data to run the app.
    - if you want to use a different DB change the configuration in the Apache env.conf or ansible's template.


### Development Environment Tools 
> (tested on a Mac w/ macOS Sierra 10.12.4 (16E195) on 20170404)

* If you are on a Mac make sure you have the [Command Line Tools Package](https://developer.apple.com/library/content/technotes/tn2339/_index.html) installed.

```
    xcode-select --install
```

* VirtualBox: http://download.virtualbox.org/virtualbox/5.0.32/VirtualBox-5.0.32-112930-OSX.dmg
* Vagrant: https://releases.hashicorp.com/vagrant/1.9.2/vagrant_1.9.2.dmg
  - Then, install vagrant's plugin vbguest:
```
    sudo vagrant plugin install vagrant-vbguest
```
* PHPStorm: https://www.jetbrains.com/phpstorm/download/download_thanks.jsp?os=mac
* HomeBrew (http://brew.sh/):
```
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
* Ansible (>= 2.2)

    > If you are wishing to run the latest released version of Ansible and you are running Red Hat Enterprise Linux (TM), CentOS, Fedora, Debian, or Ubuntu, we recommend using the OS package manager.
    > For other installation options, we recommend installing via “pip”, which is the Python package manager, though other options are also available.

    > ⚠ [**Latest Releases on Mac OSX**](https://docs.ansible.com/ansible/intro_installation.html#latest-releases-on-mac-osx) : The preferred way to install ansible on a Mac is via pip.
    * on macOS make sure you have *pip* installed and updated   
    ```
        sudo python -m ensurepip
        sudo pip install -U pip
    ```
    * then:
 
    ```
    sudo pip install "ansible>=2.2" 
    ```

For more info check the *ansible* folder in the infrastructure repo: https://github.com/abaenglish/infrastructure/blob/master/ansible/README.md

    
* [AWS Command Line Interface](https://aws.amazon.com/cli/) >= 1.6.1 with working ABA Land's AWS keys.
    - Mac OSX with HomeBrew ```$ brew install awscli```
    - OR with PIP: ```$ pip install awscli```
    - ask for the AWS credentials to a fellow developer and set them up running 
    ```
    aws configure
    ```
    - the credentials are used to encrypt vault variables, for more information check https://github.com/abaenglish/infrastructure/blob/master/ansible/README.md#using-vault-variables

* SourceTree: https://www.sourcetreeapp.com/download/
* Java for OSX 2015-01: https://support.apple.com/kb/DL1572?viewlocale=en_US&locale=en_US
* iTerm: https://iterm2.com/

## CODE CONVENTIONS

 * [Git Commit Message Guidelines](https://github.com/abaenglish/software-guidebook/blob/master/commits.md)

 To automate the process of checking code for coding standars and to spare humans of this boring (but important) task follow these instructions: [[docs/conventions-checkers.md]]
 

#### RELEASE TO PRODUCTION

our projects follows [semantic versioning](http://semver.org/) and the [gitflow branching model](http://nvie.com/posts/a-successful-git-branching-model/)
for more information check our [software-guidebook](https://github.com/abaenglish/software-guidebook)

1. following gitflow create a release or a hotfix branch.
2. upgrade version number in /composer.json
3. upgrade version number in /common/protected/config/version
4. update the changelog, run command: composer changelog 
5. update the dependency list, run command: composer update
6. finish the branch and push it to the central repo.
7. notify the team in the deploy channel.
