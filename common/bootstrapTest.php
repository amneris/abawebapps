<?php
# Global bootstrap file containing. Must be called for every application entry point
# Path to the real root of project
define('ROOT_DIR', realpath(dirname(__FILE__) . '/../'));

// ABA English general constants, global and specific settings:
require_once(ROOT_DIR.'/common/protected/config/AbaSettings.php');

# Launching the Yii framework.
$yiit = ROOT_DIR . '/yii/framework/yiit.php';
require_once( $yiit );

# Some global aliases
Yii::setPathOfAlias('root', ROOT_DIR);
Yii::setPathOfAlias('common', ROOT_DIR . '/common');

