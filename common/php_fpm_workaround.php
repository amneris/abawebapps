<?php

/**
 * Support for custom headers in PHP with FastCGI Process Manager (aka: PHP-FPM)
 *
 * https://serverfault.com/questions/153092/how-do-i-enable-php-apache-request-headers-or-change-php-into-an-apache-module
 */
if (!is_callable('getallheaders')) {
    # Convert a string to mixed-case on word boundaries.
    function uc_all($string)
    {
        $temp = preg_split('/(\W)/', str_replace("_", "-", $string), -1, PREG_SPLIT_DELIM_CAPTURE);
        foreach ($temp as $key => $word) {
            $temp[$key] = ucfirst(strtolower($word));
        }
        return join('', $temp);
    }

    function getallheaders()
    {
        $headers = array();
        foreach ($_SERVER as $h => $v) {
            if (preg_match('/HTTP_(.+)/', $h, $hp)) {
                $headers[str_replace("_", "-", uc_all($hp[1]))] = $v;
            } else {
                $headers[$h] = $v; // @ascandroli: customization to allow headers like: ABA_API_AUTH_TOKEN
            }

        }
        return $headers;
    }

    function apache_request_headers()
    {
        return getallheaders();
    }
}
