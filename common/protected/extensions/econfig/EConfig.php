<?php

class EConfig extends CApplicationComponent
{
	
	const CACHE_KEY = 'Extension.Config';

	public $configTableName = 'aba_b2c.config';
	public $autoCreateConfigTable = false;
	public $connectionID = 'db';
	public $cacheID = false;
	public $strictMode = true;
	
	private $_db;
	private $_cache;
	private $_config;

    /**
     * @param $key
     * @param bool $multipleValue
     * @param string $delimiter
     *
     * @return null|string|array
     * @throws CException
     */
    public function get($key, $multipleValue = false, $delimiter=',')
	{
        $ownKey = $this->_getDeveloperOwnKey($key);
        if (!is_null($ownKey)) {
            return $ownKey;
        }

        $db = $this->_getDb();
        $cache = $this->_getCache();

        if (NULL === $this->_config) {
            $this->_getConfig($db, $cache);
        }

        if (false === is_array($this->_config) || false === array_key_exists($key, $this->_config)) {
            if (true === $this->strictMode) {
                throw new CException("Unable to get value - no entry present with key \"{$key}\".");
            } else {
                return NULL;
            }
        }

        if($multipleValue){
            if( strpos($this->_config[$key], $delimiter)>=1 ){
                $aConfigsValues = explode($delimiter, $this->_config[$key]);
                return $aConfigsValues;
            }else{
                return array($this->_config[$key]);
            }
        }

        return (NULL === $this->_config[$key]) ? NULL : ($this->_config[$key]);
		
	}
	
	public function set($key, $value)
	{

		$db = $this->_getDb();
		$cache = $this->_getCache();

		if (NULL === $this->_config)
		{
			$this->_getConfig($db, $cache);
		}

		if (false === is_array($this->_config) || false === array_key_exists($key, $this->_config))
		{
			if (true === $this->strictMode)
			{
				throw new CException("Unable to set value - no entry present with key \"{$key}\".");
			}
			else
			{
				$dbCommand = $db->createCommand("INSERT INTO {$this->configTableName} (`key`, `value`) VALUES (:key, :value)");
				$dbCommand->bindParam(':key', $key, PDO::PARAM_STR);				
				$dbCommand->bindValue(':value', ($value), PDO::PARAM_STR);
				$dbCommand->execute();
			}
		}

		if (false === isset($dbCommand))
		{
			$dbCommand = $db->createCommand("UPDATE {$this->configTableName} SET `value` = :value WHERE `key` = :key LIMIT 1");
			$dbCommand->bindValue(':value', ($value), PDO::PARAM_STR);
			$dbCommand->bindParam(':key', $key, PDO::PARAM_STR);
			$dbCommand->execute();
		}

		$this->_config[$key] = ($value);

		if (false !== $cache)
		{
			$cache->set(self::CACHE_KEY, $this->_config);
		}
		
	}

    /** Custom function to overwrite database setting with the ones the programmer has in his local machine.
     *
     * @param $key
     *
     * @return null
     */
    private function _getDeveloperOwnKey($key)
    {
        global $aMyOwnConfigKeys;
        if( isset($aMyOwnConfigKeys) && is_array($aMyOwnConfigKeys) )
        {
            if( array_key_exists($key,$aMyOwnConfigKeys) )
            {
                return $aMyOwnConfigKeys[$key];
            }
        }

        return NULL;
    }

	private function _getDb()
	{

		if (NULL !== $this->_db)
		{
			return $this->_db;
		}
		elseif (($this->_db = Yii::app()->getComponent($this->connectionID)) instanceof CDbConnection)
		{
			return $this->_db;
		}
		else
		{
			throw new CException("Config.connectionID \"{$this->connectionID}\" is invalid. Please make sure it refers to the ID of a CDbConnection application component.");
		}
		
	}
	
	private function _getCache()
	{
		
		if (false === $this->cacheID)
		{
			return false;
		}
		elseif (NULL !== $this->_cache)
		{
			return $this->_cache;
		}
		elseif (($this->_cache = Yii::app()->getComponent($this->cacheID)) instanceof CCache)
		{
			return $this->_cache;
		}
		elseif (($this->_cache = Yii::app()->getComponent($this->cacheID)) instanceof CDummyCache)
		{
			return $this->_cache;
		}
		else
		{
			throw new CException("Config.cacheID \"{$this->cacheID}\" is invalid. Please make sure it refers to the ID of a CCache application component.");
		}
		
	}
	
	private function _getConfig($db, $cache)
	{

		if (true === $this->autoCreateConfigTable)
		{
			$this->_createConfigTable($db);
		}
		
		if (false === $cache || false === ($this->_config = $cache->get(self::CACHE_KEY)))
		{
			
			$dbReader = $db->createCommand("SELECT * FROM {$this->configTableName}")->query();

			while (false !== ($row = $dbReader->read()))
			{
				$this->_config[$row['key']] = $row['value'];
			}
			
			if (false !== $cache)
			{
				$cache->set(self::CACHE_KEY, $this->_config);
			}
			
		}

	}
	
	private function _createConfigTable($db)
	{
		$db->createCommand("CREATE TABLE IF NOT EXISTS {$this->configTableName} (`key` VARCHAR(100) PRIMARY KEY, `value` TEXT) COLLATE = utf8_bin")->execute();
	}
	
}

?>