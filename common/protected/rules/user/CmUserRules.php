<?php
/**
 * CmUserRules
 * @author: mgadegaard
 * Date: 03/07/2015
 * Time: 13:15
 * © ABA English 2015
 */

class CmUserRules
{
    private $errMessage;
    private $errorCode;

    /**
     * Function to update partnerSource and sourceList if current partnerSource is defined as updatable in
     * aba_b2c.config (UPDATABLE_PARTNER_IDS)
     * @param CmAbaUser $user
     * @param integer $idPartner
     * @param integer $idSource
     * @return bool
     */
    public function updateAttribution($user, $idPartner, $idSource)
    {
        if ($this->isUpdatablePartnerSource($user)) {
            $user->idPartnerSource = $idPartner;
            $user->idSourceList = $idSource;
            $user->save(false);
            return true;
        } else {
            return false;
        }
    }

    private function isUpdatablePartnerSource(CmAbaUser $user)
    {
        $updatableIds = explode(',', Yii::app()->config->get('UPDATABLE_PARTNER_IDS'));
        return in_array($user->idPartnerSource, $updatableIds);
    }

    public function free2premiumAndroid(
      CmPaySupplierPlayStore $paymentSupplier,
      $user,
      $periodId,
      $currency,
      $priceAppStore,
      $countryId,
      $userId,
      $purchaseReceipt,
      $StartTimeMillis,
      $ExpiryTimeMillis,
      $promocode,
      $actionSource
    )
    {
        // Validate user type
        if ($user->userType != FREE) {
            $this->errorCode = '410';
            $this->errMessage = 'User id ' . $user->id . ' already PREMIUM ';
            return false;
        }

        // Validate product
        $prodPeriodicity = new CmProdPeriodicity();
        if (!$prodPeriodicity->getProdPeriodByMonths($periodId)) {
            $this->errorCode = '508';
            $this->errMessage = "The period for this product is not valid. There is no such periodicity in the database
             with ID " . $periodId;
            return false;
        }
        $errorEmail = $user->email;
        if (!is_numeric($countryId)) {
            $oCountry = new CmAbaCountry();
            if ($oCountry->getCountryByIsoCode($countryId)) {
                $countryId = $oCountry->id;
            } else {
                $countryId = $user->countryId;
                if (!is_numeric($countryId)) {
                    if ($oCountry->getCountryByIsoCode(Yii::app()->config->get("DefaultIPCountryCode"))) {
                        $countryId = $oCountry->id;
                    } else {
                        $countryId = Yii::app()->config->get("ABACountryId");
                    }
                }
            }
        }

        switch($paymentSupplier->validatePurchaseReceipt($user)) {
            case -1:
                $this->errorCode = '410';
                $this->errMessage = 'The object $paymentSupplier have not been properly inicializated.';
                return false;
            case 1:
                $this->errorCode = '410';
                $this->errMessage = 'The purchase receipt has come with purchaseState <> 0 (purchased) : ' . $paymentSupplier->getReceiptPurchaseState() ;
                return false;
            case 2:
                $this->errorCode = '410';
                $this->errMessage = 'The purchase receipt is not valid. There is already a user with this orderId : ' . $paymentSupplier->getReceiptOrderId();
                return false;
        }

        $product = new CmAbaProductPrice();
        if (!$product->getProductByCountryIdPeriodId($countryId, $prodPeriodicity->id, PREMIUM)) {
            $this->errorCode = '508';
            $this->errMessage = "Invalid product. There is no product in the database for period with Id = " . $periodId;
            return false;
        }

        $oAbaPaymentsPlayStore = new AbaPaymentsPlaystore();
        $oAbaPaymentsPlayStore->userId=$userId;
        $oAbaPaymentsPlayStore->idCountry=$countryId;
        $oAbaPaymentsPlayStore->purchaseReceipt=$paymentSupplier->getPurchaseReceipt();
        $oAbaPaymentsPlayStore->autoRenewing=$paymentSupplier->getReceiptAutoRenewing();
        $oAbaPaymentsPlayStore->orderId=$paymentSupplier->getReceiptOrderId();
        $oAbaPaymentsPlayStore->packageName =$paymentSupplier->getReceiptPackageName();
        $oAbaPaymentsPlayStore->productId=$paymentSupplier->getReceiptProductId();
        $oAbaPaymentsPlayStore->purchaseTime=$paymentSupplier->getReceiptPurchaseTime();
        $oAbaPaymentsPlayStore->purchaseState=$paymentSupplier->getReceiptPurchaseState();
        $oAbaPaymentsPlayStore->developerPayload=$paymentSupplier->getReceiptDeveloperPayload();
        $oAbaPaymentsPlayStore->purchaseToken=$paymentSupplier->getReceiptPurchaseToken();
        $oAbaPaymentsPlayStore->status=PAY_APP_STORE_PAY_PENDING;
        $oAbaPaymentsPlayStore->datePaymentPlayStore =date("Y-m-d H:i:s",($paymentSupplier->getReceiptPurchaseTime() /1000));
        $oAbaPaymentsPlayStore->startTimeMillis=date("Y-m-d H:i:s",($StartTimeMillis /1000));
        $oAbaPaymentsPlayStore->expiryTimeMillis=date("Y-m-d H:i:s",($ExpiryTimeMillis /1000));
        $oAbaPaymentsPlayStore->orderId=$paymentSupplier->getReceiptOrderId();

        // SUBSCRIPTION
        //
        $lastPayment = new CmAbaPayment();
        $recurringPaymentRules = new CmRecurringPaymentRules();
        $typeSubscriptionStatus = $recurringPaymentRules->getTypeSubscriptionStatus($user, $lastPayment);

        $expirationDate = $this->getNextExpirationDate($user, $prodPeriodicity);

        $user = $this->convertToPremium($user, $user->idPartnerCurrent, $expirationDate, $product->userType);
        if (!$user) {
            $this->errMessage = "Converting user: " . $errorEmail . " to PREMIUM has failed.";
            return false;
        }
        $sNow = HeDate::todaySQL(true);

        // Insert a mock payment row ---------------------------------------------------------------------------
        $moPayment = new CmAbaPayment();
        $moPayment->id = HePayments::generateIdPayment($user);
        $moPayment->idUserProdStamp = 1;
        $moPayment->userId = $user->id;
        $moPayment->idProduct = $product->idProduct;
        $moPayment->idCountry = $countryId;
        $moPayment->idPeriodPay = $prodPeriodicity->id;
        $moPayment->idUserCreditForm = null;
        $moPayment->paySuppExtId = PAY_SUPPLIER_ANDROID_PLAY_STORE;
        $moPayment->status = PAY_SUCCESS;
        $moPayment->dateStartTransaction = $sNow;
        $moPayment->dateEndTransaction = $sNow;
        $moPayment->dateToPay = $sNow;
        // get app store currency
        $abaIdCurrency = $product->getAppStoreCurrency($currency);
        $xRates = new CmCurrencies($currency);
        $moPayment->xRateToEUR = $xRates->getConversionRateTo(CCY_DEFAULT);
        $moPayment->xRateToUSD = $xRates->getConversionRateTo(CCY_2DEF_USD);
        $moPayment->currencyTrans = $abaIdCurrency;
        $moPayment->foreignCurrencyTrans = $abaIdCurrency;
        $moPayment->amountOriginal = $priceAppStore;
        $moPayment->amountDiscount = 0;
        $moPayment->amountPrice = $priceAppStore;
        $moPayment->idPromoCode = $promocode;
        $moPayment->idPartner = PARTNER_ID_MOBILE_ANDROID;
        if (!empty($moLastPay)) {
            $moPayment->idPartnerPrePay = $moLastPay->idPartner;
        }
        $moPayment->isRecurring = 0;
        $moPayment->paySuppLinkStatus = PAY_RECONC_ST_MATCHED;
        $moPayment->paySuppOrderId = $moPayment->id . ":" . substr("" . date("Hms"), 3);

        $initialTransactionId = $paymentSupplier->getReceiptOrderId();
        $moPayment->paySuppExtUniId = $initialTransactionId;
        $moPayment->paySuppExtProfId = $initialTransactionId;

        $moPayment->setAmountWithoutTax($moPayment->dateEndTransaction);
        if (!$moPayment->paymentProcess($moPayment->userId)) {
            $this->errMessage = "Payment could not be successfully inserted for " .
              $product->idProduct . $countryId . $prodPeriodicity->id;
            return false;
        }
        $nextPayment = $moPayment->createDuplicateNextPayment();
        $nextPayment->id = HePayments::generateIdPayment($user, $nextPayment->dateToPay);
        $nextPayment->idPayControlCheck = $moPayment->id;
        $nextPayment->isPeriodPayChange = null;
        $nextPayment->paySuppExtUniId = null;
        $nextPayment->paySuppExtProfId = null;
        $nextPayment->idPromoCode = "";
        //#ABAWEBAPPS-870
        $nextPayment->idPartner = PARTNER_ID_ANDROID_RECURRING;

        if (!$nextPayment->insertPayment()) {
            $this->errMessage = "Payment could not be successfully inserted for " .
              $product->idProduct . $countryId . $prodPeriodicity->id;
            return false;
        }
        $user->logUserActivity->saveUserLogActivity(
          'user',
          'Registration from type ' . $typeSubscriptionStatus . ' to Premium AppStore',
          'free2premium',
          $user->id
        );
        $oAbaPaymentsPlayStore->idPayment = $moPayment->id;
        if (!$oAbaPaymentsPlayStore->insertPaymentPlayStore()) {

            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_LOGIC_L . ' Payment AppStore could not be successfully inserted for payment',
              HeLogger::PAYMENTS_MOBILE,
              HeLogger::CRITICAL,
              "Payment AppStore could not be successfully inserted for payment " .
              $moPayment->id . " and " . $product->idProduct . $countryId . $prodPeriodicity->id,
              'free2premium'
            );
        }
        $stPaymentInfo = $moPayment->getPaymentInfoForSelligent();
        $commConnSelligent = new CmSelligentConnector();
        $commConnSelligent->sendOperationToSelligent(
            CmSelligentConnector::PAYMENTS,
            $user,
            array(
                "MONTH_PERIOD" => $stPaymentInfo['MONTH_PERIOD'],
                "PAY_DATE_START" => $stPaymentInfo['PAY_DATE_START'],
                "PAY_DATE_END" => $stPaymentInfo['PAY_DATE_END'],
                "AMOUNT_PRICE" => $stPaymentInfo['AMOUNT_PRICE'],
                "CURRENCY" => $stPaymentInfo['CURRENCY'],
                "PROMOCODE" => $promocode,
                "PRICE_CURR" => $stPaymentInfo['PRICE_CURR'],
                "TRANSID" => $stPaymentInfo['TRANSID'],
            ),
            $actionSource . ' - free2premium'
        );
        return true;
    }

    /**
     * @param $user
     * @param $periodId
     * @param $currency
     * @param $priceAppStore
     * @param $countryId
     * @param $userId
     * @param $purchaseReceipt
     * @param $actionSource
     * @return bool
     */
    public function free2premium(
        $user,
        $periodId,
        $currency,
        $priceAppStore,
        $countryId,
        $userId,
        $purchaseReceipt,
        $idSession,
        $promocode,
        $actionSource
    )
    {
        // Validate user type
        if ($user->userType != FREE) {
            $this->errorCode = '410';
            $this->errMessage = 'User id ' . $user->id . ' already PREMIUM ';
            return false;
        }

        // Validate product
        $prodPeriodicity = new CmProdPeriodicity();
        if (!$prodPeriodicity->getProdPeriodByMonths($periodId)) {
            $this->errorCode = '508';
            $this->errMessage = "The period for this product is not valid. There is no such periodicity in the database
             with ID " . $periodId;
            return false;
        }
        $errorEmail = $user->email;
        if (!is_numeric($countryId)) {
            $oCountry = new CmAbaCountry();
            if ($oCountry->getCountryByIsoCode($countryId)) {
                $countryId = $oCountry->id;
            } else {
                $countryId = $user->countryId;
                if (!is_numeric($countryId)) {
                    if ($oCountry->getCountryByIsoCode(Yii::app()->config->get("DefaultIPCountryCode"))) {
                        $countryId = $oCountry->id;
                    } else {
                        $countryId = Yii::app()->config->get("ABACountryId");
                    }
                }
            }
        }
        // AppStore payment receipt
        $oAbaPaymentsAppStore = new AbaPaymentsAppstore();
        $oAbaPaymentsAppStore->idCountry = $countryId;
        $oAbaPaymentsAppStore->userId = $userId;
        $oAbaPaymentsAppStore->purchaseReceipt = $purchaseReceipt;

        $paymentSupplier = CmPaymentRules::createPaymentSupplier(PAY_SUPPLIER_APP_STORE);
        $paymentSupplier->setJsonResponse($oAbaPaymentsAppStore->purchaseReceipt);

        // Validate json appstore receipt
        if (!$paymentSupplier->validatePurchaseReceipt($user)) {
            $this->errorCode = '410';
            $this->errMessage = 'The purchase receipt is not valid. There is already a user with this' .
                ' original_transaction_id.';
            return false;
        }

        $oAbaPaymentsAppStore->originalTransactionId = $paymentSupplier->getOriginalTransactionId();

        // Validate Product and periodicity's:
        $product = new CmAbaProductPrice();
        if (!$product->getProductByCountryIdPeriodId($countryId, $prodPeriodicity->id, PREMIUM)) {
            $this->errorCode = '508';
            $this->errMessage = "Invalid product. There is no product in the database for period with Id = " . $periodId;
            return false;
        }
        // SUBSCRIPTION
        //
        $lastPayment = new CmAbaPayment();
        $recurringPaymentRules = new CmRecurringPaymentRules();
        $typeSubscriptionStatus = $recurringPaymentRules->getTypeSubscriptionStatus($user, $lastPayment);

        $expirationDate = $this->getNextExpirationDate($user, $prodPeriodicity);

        $user = $this->convertToPremium($user, $user->idPartnerCurrent, $expirationDate, $product->userType);
        if (!$user) {
            $this->errMessage = "Converting user: " . $errorEmail . " to PREMIUM has failed.";
            return false;
        }
        $sNow = HeDate::todaySQL(true);

        // Insert a mock payment row ---------------------------------------------------------------------------
        $moPayment = new CmAbaPayment();
        $moPayment->id = HePayments::generateIdPayment($user);
        $moPayment->idUserProdStamp = 1;
        $moPayment->userId = $user->id;
        $moPayment->idProduct = $product->idProduct;
        $moPayment->idCountry = $countryId;
        $moPayment->idCountry = $countryId;
        $moPayment->idPeriodPay = $prodPeriodicity->id;
        $moPayment->idUserCreditForm = null;
        $moPayment->paySuppExtId = PAY_SUPPLIER_APP_STORE;
        $moPayment->status = PAY_SUCCESS;
        $moPayment->dateStartTransaction = $sNow;
        $moPayment->dateEndTransaction = $sNow;
        $moPayment->dateToPay = $sNow;
        $moPayment->idSession= $idSession;
        // get app store currency
        $abaIdCurrency = $product->getAppStoreCurrency($currency);
        $xRates = new CmCurrencies($currency);
        $moPayment->xRateToEUR = $xRates->getConversionRateTo(CCY_DEFAULT);
        $moPayment->xRateToUSD = $xRates->getConversionRateTo(CCY_2DEF_USD);
        $moPayment->currencyTrans = $abaIdCurrency;
        $moPayment->foreignCurrencyTrans = $abaIdCurrency;
        $moPayment->amountOriginal = $priceAppStore;
        $moPayment->amountDiscount = 0;
        $moPayment->amountPrice = $priceAppStore;
        $moPayment->idPromoCode = $promocode;
        $moPayment->idPartner = PARTNER_ID_MOBILE;
        if (!empty($moLastPay)) {
            $moPayment->idPartnerPrePay = $moLastPay->idPartner;
        }
        $moPayment->isRecurring = 0;
        $moPayment->paySuppLinkStatus = PAY_RECONC_ST_MATCHED;
        $moPayment->paySuppOrderId = $moPayment->id . ":" . substr("" . date("Hms"), 3);
        if ($paymentSupplier->loadInitialPaymentJson()) {
            $initialTransactionId = $paymentSupplier->getOrderNumber();
            $moPayment->paySuppExtUniId = $initialTransactionId;
            $moPayment->paySuppExtProfId = $initialTransactionId;
        }
        $moPayment->setAmountWithoutTax($moPayment->dateEndTransaction);
        if (!$moPayment->paymentProcess($moPayment->userId)) {
            $this->errMessage = "Payment could not be successfully inserted for " .
                $product->idProduct . $countryId . $prodPeriodicity->id;
            return false;
        }

        //
        //#5739
        $amountOriginal = $moPayment->amountOriginal;

        $nextPayment = $moPayment->createDuplicateNextPayment();
        $nextPayment->id = HePayments::generateIdPayment($user, $nextPayment->dateToPay);
        $nextPayment->idPayControlCheck = $moPayment->id;
        $nextPayment->isPeriodPayChange = null;
        $nextPayment->paySuppExtUniId = null;
        $nextPayment->paySuppExtProfId = null;
        $nextPayment->idPromoCode ="";
        // $nextPayment->idPartner =           PaySupplierAppstore::getAppstoreRecurringPartner();
        $nextPayment->idPartner =           PARTNER_ID_IOS_RECURRING;

        //
        //#5739
        $nextPayment->amountOriginal =  $amountOriginal;
        $nextPayment->amountPrice =     $amountOriginal;

        // --------------------------------------
        if (!$nextPayment->insertPayment()) {
            $this->errMessage = "Payment could not be successfully inserted for " .
                $product->idProduct . $countryId . $prodPeriodicity->id;
            return false;
        }
        $user->logUserActivity->saveUserLogActivity(
            'user',
            'Registration from type ' . $typeSubscriptionStatus . ' to Premium AppStore',
            'free2premium',
            $user->id
        );
        $oAbaPaymentsAppStore->idPayment = $moPayment->id;
        if (!$oAbaPaymentsAppStore->insertPaymentAppstore()) {
            HeLogger::sendLog("Payment AppStore could not be successfully inserted for payment " .
              $moPayment->id . " and " . $product->idProduct . $countryId . $prodPeriodicity->id,
              HeLogger::PAYMENTS_MOBILE, HeLogger::CRITICAL, 'free2premium');
        }
        $stPaymentInfo = $moPayment->getPaymentInfoForSelligent();
        $commConnSelligent = new CmSelligentConnector();
        $commConnSelligent->sendOperationToSelligent(
            CmSelligentConnector::PAYMENTS,
            $user,
            array(
                "MONTH_PERIOD" => $stPaymentInfo['MONTH_PERIOD'],
                "PAY_DATE_START" => $stPaymentInfo['PAY_DATE_START'],
                "PAY_DATE_END" => $stPaymentInfo['PAY_DATE_END'],
                "AMOUNT_PRICE" => $stPaymentInfo['AMOUNT_PRICE'],
                "CURRENCY" => $stPaymentInfo['CURRENCY'],
                "PROMOCODE" => $promocode,
                "PRICE_CURR" => $stPaymentInfo['PRICE_CURR'],
                "TRANSID" => $stPaymentInfo['TRANSID'],
            ),
            $actionSource . ' - free2premium'
        );
        return true;
    }

    /** Returns the new expiration date for the user.
     *
     * @param $user
     * @param CmProdPeriodicity $periodicity
     *
     * @return bool|string; YYYY-MM-DD
     */
    public function getNextExpirationDate($user, $periodicity)
    {
        if (HeDate::validDateSQL(HeDate::europeanToSQL($user->expirationDate)) &&
            HeDate::isGreaterThanToday(HeDate::europeanToSQL($user->expirationDate), true)
        ) {
            $expirationDate = HeDate::europeanToSQL($user->expirationDate);
        } else {
            $expirationDate = HeDate::todaySQL(true);
        }
        $expirationDate = $periodicity->getNextDateBasedOnDate($expirationDate);
        // Case FREE|LEAD|DELETED-----------------------------------------------------------------------******
        if ($user->userType == FREE || $user->userType == DELETED) {
            $expirationDate = $periodicity->getNextDateBasedOnDate(HeDate::todaySQL(true));
        }
        return $expirationDate;
    }

    /** Alias of function updateUserType
     * Should update these columns: user.idPartnerFirstPay, user.userType, user.ExpirationDate
     * @param CmAbaUser $user
     * @param integer $idPartner
     * @param string $expirationDate
     * @param integer|null $userType
     *
     * @return bool|CmAbaUser
     */
    public function convertToPremium($user, $idPartner, $expirationDate, $userType = null)
    {
        if (empty($userType)) {
            $userType = PREMIUM;
        }
        $pendingPayment = new CmAbaPayment();
        if ($pendingPayment->getLastPayPendingByUserId($user->id)) {
            $pendingPayment->updateDateToPayInPending($idPartner, $expirationDate);
        }
        return $this->updateUserType($user, $userType, $expirationDate, $idPartner);
    }

    /** Updates user Type, usually to PREMIUM only.
     * @param CmAbaUser $user
     * @param integer $userType
     * @param string $expirationDate
     * @param integer $idPartner
     *
     * @return CmAbaUser|bool
     */
    private function updateUserType($user, $userType, $expirationDate, $idPartner = null)
    {
        $errMessage = false;

        if (!$user->updateUserType($userType, $idPartner)) {
            $errMessage = true;
        }
        $user->expirationDate = $expirationDate;
        /** @var $aColumns2Upd array */
        $aColumns2Upd = array('expirationDate');
        if (empty($user->idPartnerFirstPay)) {
            $user->idPartnerFirstPay = $idPartner;
            $aColumns2Upd[] = 'idPartnerFirstPay';
        }
        if (!$user->update($aColumns2Upd)) {
            $errMessage = true;
        }
        $firstUnitForLevels = HeUnits::getArrayStartUnitsByLevel(1, 6);
        $followUp = new AbaFollowup();
        $maxProgress = $followUp->getMaxFollowUp($user->id, $firstUnitForLevels);
        if (!$user->updateFollowUpAtFirstPay($maxProgress)) {
            $errMessage = true;
        }
        $watchedVideos = $followUp->getNumberOfVideosWatched($user->id);
        if (!$user->updateWatchedVideos($watchedVideos)) {
            $errMessage = true;
        }
        if ($errMessage) {
            $this->errMessage = ' User ' . $user->email . ' could not be converted to Premium. Review fields ' .
                " userType($userType), expirationDate($expirationDate), idPartnerSource&FirstPay($idPartner)." .
                " Their values should be the ones between parenthesis";
            return false;
        }
        return $user;
    }

    /**
     * @param $sEmail
     *
     * @return bool
     */
    public function sendRecoverPasswordEmail($sEmail)
    {
        $oUser = new CmAbaUser();
        if (!$oUser->populateUserByEmail($sEmail)) {
            $this->errMessage = Yii::t('mainApp', 'email_not_exists_key');
            return false;
        }
        $selligentConnector = new CmSelligentConnector();
        $selligentConnector->sendOperationToSelligent(
            CmSelligentConnector::RECOVER_PASSWORD,
            $oUser,
            array(),
            "sendRecoverPasswordEmail"
        );
        return true;
    }

    /** In case of error of any of this class functions, this method returns the detailed message
     * /** Changes password , updates to database and informs Selligent.
     *
     * @param CmAbaUser $user
     * @param string $newPassword
     * @param string $actionSource
     * @param bool $updateSQL
     * @param bool $fromRecover
     *
     * @return bool
     */
    public function changePassword(&$user, $newPassword, $actionSource, $updateSQL = true, $fromRecover = true)
    {
        $user->setPassword($newPassword);
        if ($updateSQL) {
            if (!$user->update(array("password", "keyExternalLogin"))) {
                $this->errMessage = "User password could not be updated.";
                return false;
            }
        }
        // Get all devices and recreate tokens and change their tokens
        $userDevice = new CmAbaUserDevice();
        $userDevices = $userDevice->getAllUserDevicesByUserId($user->id);
        if (is_array($userDevices) && count($userDevices) >= 1) {
            /* @var CmAbaUserDevice $userDeviceToUpdate */
            foreach ($userDevices as $userDeviceToUpdate) {
                // Right now, we decide to change the tokens of all devices if password changes. If
                // we do not want this, we only want to users to keep entering mobile devices without asking for
                // password then just change password by Email!
                $tmpToken = HeUser::genTokenForMobile(
                    $userDeviceToUpdate->sysOper,
                    $userDeviceToUpdate->id,
                    $user->email,
                    $newPassword
                );
                $userDeviceToUpdate->updateToken($tmpToken);
            }
        }
        //  Send data to Selligent only when user is recovering password, not changing manually
        $selligentConnector = new CmSelligentConnector();
        if ($fromRecover) {
            $selligentConnector->sendOperationToSelligent(
                CmSelligentConnector::CHANGE_PASSWORD,
                $user,
                array("PASS_TMP" => $newPassword),
                $actionSource
            );
        } else {
            $selligentConnector->sendOperationToSelligent(
                CmSelligentConnector::SYNCHRO_USER,
                $user,
                array(),
                'profile-changePassword'
            );
        }

        return true;
    }


    /**
     * @param $oAbaUser
     * @param $experimentTypeId
     * @param null $experimentVariationId
     *
     * @return bool
     */
    public static function insertUserExperimentVariationForRegistration(
        $oAbaUser,
        $experimentTypeId,
        $experimentVariationId = null
    )
    {
        $iEnabledExperiments = Yii::app()->config->get("ENABLED_EXPERIMENTS");
        if ($iEnabledExperiments == 1) {
            $oAbaUserExperimentsVariations = new CmAbaUserExperimentsVariations();
            return $oAbaUserExperimentsVariations->insertUserExperimentVariationForRegistration($oAbaUser,
                $experimentTypeId, $experimentVariationId);
        }
        return true;
    }

    /**
     * @param $stAllProfiles
     * @param $oAbaUser
     *
     * @return array
     */
    public static function filterExperimentsAvailablesProfiles($stAllProfiles, $oAbaUser)
    {
        $stProfiles = array();

        foreach ($stAllProfiles as $iKey => $stProfile) {
            if ($stProfile["isCreatedUserVariation"] == 1) {
                if ($stProfile["availableExperiment"] == 1) {
                    switch ($stProfile["availableModeId"]) {
                        case CmAbaExperimentsAvailabilityModes::EXPERIMENT_AVAILABLE_MODE_WHILE_EXPERIMENT:
                        case CmAbaExperimentsAvailabilityModes::EXPERIMENT_AVAILABLE_MODE_ALWAYS:
                            $stProfiles[] = $stProfile;
                            break;
                    }
                } else {
                    switch ($stProfile["availableModeId"]) {
                        case CmAbaExperimentsAvailabilityModes::EXPERIMENT_AVAILABLE_MODE_ALWAYS:
                        case CmAbaExperimentsAvailabilityModes::EXPERIMENT_AVAILABLE_MODE_AFTER:
                            $stProfiles[] = $stProfile;
                            break;
                    }
                }
            }
        }

        $stFormattedProfiles = array();

        foreach ($stProfiles as $iKey => $stProfile) {

            if ($stProfile["availableExperiment"] == 1) {

                $stFormattedProfiles[] = array(

                    $stProfile["experimentIdentifier"] =>
                        array(
                            "variation" => $stProfile["experimentVariationIdentifier"],
                            "type" => $stProfile["experimentTypeIdentifier"],
                        )
                );
            }
        }

        return $stFormattedProfiles;
    }


    /**
     * @param $stAllProfiles
     * @param $oAbaUser
     *
     * @return array
     */
    public static function filterExperimentsAvailablesUsers($stAllProfiles, $oAbaUser)
    {

        $stProfiles = array();

        if (isset($oAbaUser->userType)) {

            // filter availables user profiles
            foreach ($stAllProfiles as $iKey => $stProfile) {

                if (HeMixed::filterCountry($oAbaUser->langEnv, $stProfile["languages"])) {

                    switch ($oAbaUser->userType) {
                        case FREE :
                            switch ($stProfile["userCategoryId"]) {
                                case CmAbaExperimentsUserCategories::EXPERIMENT_CATEGORY_FREE_PREMIUM:
                                case CmAbaExperimentsUserCategories::EXPERIMENT_CATEGORY_FREE:
                                    $stProfiles[$iKey] = $stProfile;
                                    break;
                            }
                            break;
                        case PREMIUM :
                            switch ($stProfile["userCategoryId"]) {
                                case CmAbaExperimentsUserCategories::EXPERIMENT_CATEGORY_FREE_PREMIUM:
                                case CmAbaExperimentsUserCategories::EXPERIMENT_CATEGORY_PREMIUM:
                                    $stProfiles[$iKey] = $stProfile;
                                    break;
                            }
                            break;
                    }
                }
            }
        }

        return $stProfiles;
    }


    /**
     * @param array $stProfiles
     *
     * @return array
     */
    protected static function formatExperimentsProfiles($stProfiles = array())
    {
        $stFormattedProfiles = array();

        foreach ($stProfiles as $iKey => $stProfile) {
            if ($stProfile["availableExperiment"] == 1) {
                $stFormattedProfiles[] = array(
                    $stProfile["experimentIdentifier"] =>
                        array(
                            "variation" => $stProfile["experimentVariationIdentifier"],
                            "type" => $stProfile["experimentTypeIdentifier"],
                        )
                );
            }
        }

        return $stFormattedProfiles;
    }


    /**
     * @param $oAbaUser
     * @param null $experimentTypeId
     * @param bool|false $bCreateProfile
     * @param null $experimentVariationId
     *
     * @return array
     */
    public static function getOrCreateUserExperimentProfilesWithType(
        $oAbaUser,
        $experimentTypeId = null,
        $bCreateProfile = false,
        $experimentVariationId = null
    )
    {
        $stAllProfiles = array();

        $iEnabledExperiments = Yii::app()->config->get("ENABLED_EXPERIMENTS");

        if ($iEnabledExperiments <> 1) {
            return array();
        }

        try {

            $oAbaUserExperimentsVariations = new CmAbaUserExperimentsVariations();

            //
            // 1. Get all user profiles
            //
            $stAllProfiles = $oAbaUserExperimentsVariations->getAllUserExperimentsVariationsByUserAndExperimentType($oAbaUser,
                $experimentTypeId, $bCreateProfile);

            //
            // 2. Check availables user profiles
            //
            // 2.1  Check availables user profiles
            $stAllProfiles = self::filterExperimentsAvailablesUsers($stAllProfiles, $oAbaUser);

            //
            // 2.2  Check availables experimets profiles
            if (!$bCreateProfile) {
                return self::filterExperimentsAvailablesProfiles($stAllProfiles, $oAbaUser);
            }

            //
            // 3. Insert user profile
            if ($bCreateProfile and is_numeric($experimentTypeId)) {
                foreach ($stAllProfiles as $iKey => $stProfile) {
                    if ($stProfile["isCreatedUserVariation"] == 0 and is_numeric($stProfile["experimentTypeId"])) {
                        if (!self::insertUserExperimentVariationForRegistration(
                            $oAbaUser,
                            $stProfile["experimentTypeId"],
                            $experimentVariationId
                        )
                        ) {

                            HeLogger::sendLog(
                              HeLogger::PREFIX_ERR_LOGIC_L . ' Create (Insert) user ' . $oAbaUser->id
                              . ' experiment variation inconsistency.',
                              HeLogger::IT_BUGS,
                              HeLogger::CRITICAL,
                              'Function actionLogin / getOrCreateUserExperimentProfiles'
                            );
                        }
                    }
                }
                $stAllProfiles = $oAbaUserExperimentsVariations->getAllUserExperimentsVariationsByUserAndExperimentType(
                    $oAbaUser,
                    $experimentTypeId
                );
                $stAllProfiles = self::filterExperimentsAvailablesUsers($stAllProfiles, $oAbaUser);
                return self::filterExperimentsAvailablesProfiles($stAllProfiles, $oAbaUser);
            }
        } catch (Exception $ex) {
        }
        return array();
    }


    /**
     * In case of error of any of this class functions, this method returns the detailed message
     *
     * @return mixed
     */
    public function getErrMessage()
    {
        return $this->errMessage;
    }

    public function getErrCode()
    {
        return $this->errorCode;
    }

    /**
     * @param $userId
     * @param int $periodId
     * @param string $currency
     * @param int $basePrice
     * @param string $countryId
     * @param string $idSession
     * @param string $idDiscount
     * @param string $invoiceNumber
     * @param int $finalPrice
     * @param string $zuoraSubscriptionNumber
     * @param array $subscriptionsInfo
     * @param bool|true $userToPremium
     *
     * @return array
     * @throws CDbException
     */
    public function free2premiumZ(
      $userId,
      $periodId=1,
      $currency="USD",
      $basePrice=0,
      $countryId="ABA",
      $idSession="",
      $idDiscount="",
      $invoiceNumber="",
      $finalPrice=0,
      $zuoraSubscriptionNumber = "",
      $subscriptionsInfo=array(),
      $userToPremium=true
    ) {

        $user = new CmAbaUser();
        $user->populateUserById($userId);

        $prodPeriodicity = new CmProdPeriodicity();
        if (!$prodPeriodicity->getProdPeriodByMonths($periodId)) {
            $this->errorCode = '508';
            $this->errMessage = "The period for this product is not valid. There is no such periodicity in the database
             with ID " . $periodId;

            HeLogger::sendLog("free2premiumZ ", HeLogger::IT_BUGS,
              HeLogger::CRITICAL, "free2premiumZ - getProdPeriodByMonths: userId=" . $userId . ";periodId:" . $periodId . ";currency:" . $currency . ";countryId:" . $countryId . ";finalPrice" . $finalPrice);

            return array("success" => false, "paymentId" => "", "partnerId" => "", "warnings" => $this->errMessage);
        }

        if(trim($countryId) <> '') {

            $oCmAbaUser = new CmAbaUser();
            $stCountryData = $oCmAbaUser->getCountryByIso3($countryId);
            if($stCountryData AND isset($stCountryData["idCountry"])) {
                $countryId = $stCountryData["idCountry"];
            }
        }
        else {
            $countryId = $user->countryId;
        }

        if (!is_numeric($countryId)) {

            $oCountry = new CmAbaCountry();

            if ($oCountry->getCountryByIsoCode(Yii::app()->config->get("DefaultIPCountryCode"))) {
                $countryId = $oCountry->id;
            } else {
                $countryId = Yii::app()->config->get("ABACountryId");
            }
        }

        $product = new CmAbaProductPrice();
        if (!$product->getProductByCountryIdPeriodId($countryId, $prodPeriodicity->id, PREMIUM)) {
            $this->errorCode = '508';
            $this->errMessage = "Invalid product. There is no product in the database for period with Id = " . $periodId;

            HeLogger::sendLog("free2premiumZ ", HeLogger::IT_BUGS,
              HeLogger::CRITICAL, "free2premiumZ - getProductByCountryIdPeriodId: userId=" . $userId . ";periodId:" . $periodId . ";currency:" . $currency . ";countryId:" . $countryId . ";finalPrice" . $finalPrice);

            return array("success" => false, "paymentId" => "", "partnerId" => "", "warnings" => $this->errMessage);
        }

        $lastPayment = new CmAbaPayment();
        $recurringPaymentRules = new CmRecurringPaymentRules();
        $typeSubscriptionStatus = $recurringPaymentRules->getTypeSubscriptionStatus($user, $lastPayment);


        //#SPIDER-198
        $expirationDate = $this->getNextExpirationDate($user, $prodPeriodicity);

        if($userToPremium) {

            $sEmail = $user->email;

            $user = $this->convertToPremium($user, $user->idPartnerCurrent, $expirationDate, $product->userType);
            if (!$user) {
                $this->errMessage = "Converting user: " . $sEmail . " to PREMIUM has failed.";

                HeLogger::sendLog("free2premiumZ ", HeLogger::IT_BUGS,
                  HeLogger::CRITICAL, "free2premiumZ - convertToPremium: userId=" . $userId . ";periodId:" . $periodId . ";currency:" . $currency . ";countryId:" . $countryId . ";finalPrice" . $finalPrice);

                return array("success" => false, "paymentId" => "", "partnerId" => "", "warnings" => $this->errMessage);
            }
        }
        else {
            try {
                $pendingPayment = new CmAbaPayment();
                if ($pendingPayment->getLastPayPendingByUserId($user->id)) {
                    $pendingPayment->updateDateToPayInPending($user->idPartnerCurrent, $expirationDate);
                }
            }
            catch(Exception $e) { }
        }


        $sNow = HeDate::todaySQL(true);

        //#ZOR-265
        $amountDiscount = 0;

        if (is_numeric($basePrice) AND is_numeric($finalPrice) AND $basePrice != $finalPrice) {
            $amountDiscount = $basePrice - $finalPrice;
        }

        $dateStartTransaction = $sNow;
        $dateEndTransaction = $sNow;
        $dateToPay = $sNow;

        if(isset($subscriptionsInfo[0])) {
            if(isset($subscriptionsInfo[0]["termStartDate"])) {
                $dateStartTransaction = trim(substr($subscriptionsInfo[0]["termStartDate"], 0, 10)) . " 00:00:00";
            }
            if(isset($subscriptionsInfo[0]["termEndDate"])) {
                $dateToPay = trim(substr($subscriptionsInfo[0]["termEndDate"], 0, 10)) . " 00:00:00";
            }
        }

        // Insert a mock payment row ---------------------------------------------------------------------------
        $moPayment = new CmAbaPayment();
        $moPayment->id = HePayments::generateIdPayment($user);
        $moPayment->idUserProdStamp = 1;
        $moPayment->userId = $user->id;
        $moPayment->idProduct = $product->idProduct;
        $moPayment->idCountry = $countryId;
        $moPayment->idCountry = $countryId;
        $moPayment->idPeriodPay = $prodPeriodicity->id;
        $moPayment->idUserCreditForm = null;
        $moPayment->paySuppExtId = PAY_SUPPLIER_Z;
        $moPayment->status = PAY_SUCCESS;
        $moPayment->dateStartTransaction = $sNow;
        $moPayment->dateEndTransaction = $sNow;
        $moPayment->dateToPay = $sNow;
        $moPayment->idSession = $idSession;
        // get app store currency
        $abaIdCurrency = $product->getAppStoreCurrency($currency);
        $xRates = new CmCurrencies($currency);
        $moPayment->xRateToEUR = $xRates->getConversionRateTo(CCY_DEFAULT);
        $moPayment->xRateToUSD = $xRates->getConversionRateTo(CCY_2DEF_USD);
        $moPayment->currencyTrans = $abaIdCurrency;
        $moPayment->foreignCurrencyTrans = $abaIdCurrency;
        $moPayment->amountOriginal = $basePrice;
        $moPayment->amountDiscount = $amountDiscount;
        $moPayment->amountPrice = $finalPrice;
        $moPayment->idPromoCode = $idDiscount;
        $moPayment->idPartner = PARTNER_ID_Z;

        $moPayment->isRecurring = 0;
        $moPayment->paySuppLinkStatus = PAY_RECONC_ST_MATCHED;
        $moPayment->paySuppOrderId = $moPayment->id . ":" . substr("" . date("Hms"), 3);
        $moPayment->paySuppExtUniId = $invoiceNumber;
        $moPayment->paySuppExtProfId = $zuoraSubscriptionNumber;
        $moPayment->setAmountWithoutTax($moPayment->dateEndTransaction);
        if (!$moPayment->paymentProcess($moPayment->userId)) {
            $this->errMessage = "Payment could not be successfully inserted for " .
              $product->idProduct . " - " . $countryId . " - " . $prodPeriodicity->id;

            HeLogger::sendLog("free2premiumZ ", HeLogger::IT_BUGS,
              HeLogger::CRITICAL, "free2premiumZ - paymentProcess: userId=" . $userId . ";periodId:" . $periodId . ";currency:" . $currency . ";countryId:" . $countryId . ";finalPrice" . $finalPrice);

            return array("success" => false, "paymentId" => "", "partnerId" => "", "warnings" => $this->errMessage);
        }

        $amountOriginal = $moPayment->amountOriginal;

        $warnings = "";
        $paymentId = $moPayment->id;
        $partnerId = $moPayment->idPartner;

        $nextPayment = $moPayment->createDuplicateNextPayment();
        $nextPayment->id = HePayments::generateIdPayment($user, $nextPayment->dateToPay);
        $nextPayment->idPayControlCheck = $moPayment->id;
        $nextPayment->isPeriodPayChange = null;
        $nextPayment->paySuppExtUniId = null;
        $nextPayment->paySuppExtProfId = $zuoraSubscriptionNumber;
        $nextPayment->idPromoCode = "";
        $nextPayment->idPartner = PARTNER_ID_Z_RECURRING;

        //#5739
        $nextPayment->amountOriginal = $amountOriginal;
        $nextPayment->amountPrice = $amountOriginal;

        $nextPayment->dateStartTransaction = $dateStartTransaction;
        $nextPayment->dateEndTransaction = $dateEndTransaction;
        $nextPayment->dateToPay = $dateToPay;

        // --------------------------------------
        if (!$nextPayment->insertPayment()) {
            $this->errMessage = "Payment could not be successfully inserted for " .
              $product->idProduct . $countryId . $prodPeriodicity->id;
            $warnings .= "; " . $this->errMessage;
        }

        try {
            $user->logUserActivity->saveUserLogActivity(
              'user',
              'Registration from type ' . $typeSubscriptionStatus . ' to Premium Z',
              'free2premiumZ',
              $user->id
            );
        }
        catch(Exception $e) {
            $warnings .= "; " . $e->getMessage();
        }

        try {
            $stPaymentInfo = $moPayment->getPaymentInfoForSelligent();
            $commConnSelligent = new CmSelligentConnector();
            $commConnSelligent->sendOperationToSelligent(
              CmSelligentConnector::PAYMENTS,
              $user,
              array(
                "MONTH_PERIOD" => $stPaymentInfo['MONTH_PERIOD'],
                "PAY_DATE_START" => $stPaymentInfo['PAY_DATE_START'],
                "PAY_DATE_END" => $stPaymentInfo['PAY_DATE_END'],
                "AMOUNT_PRICE" => $stPaymentInfo['AMOUNT_PRICE'],
                "CURRENCY" => $stPaymentInfo['CURRENCY'],
                "PROMOCODE" => $idDiscount,
                "PRICE_CURR" => $stPaymentInfo['PRICE_CURR'],
                "TRANSID" => $stPaymentInfo['TRANSID'],
              ),
              'Action Rest Web Service - free2premiumZ'
            );
        }
        catch(Exception $e) {
            $warnings .= "; " . $e->getMessage();
        }

        return array("success" => true, "paymentId" => $paymentId, "partnerId" => $partnerId, "warnings" => $warnings);
    }

    /**
     * #ZOR-268
     *
     * @param array $cancelledSubscription
     *
     * @return bool
     */
    public function cancelSubscriptionsZ($cancelledSubscription = array())
    {

        $warnings = "";

        try {

            if (!isset($cancelledSubscription["originalSubscriptionId"]) || !isset($cancelledSubscription['accountId'])
            || !isset($cancelledSubscription['cancelledDate'])) {
                return array("success" => false, "warnings" => 'Incorrect Params');
            }

            $userZuora = new CmAbaUserZuora();

            if (!$userZuora->getUserBySubscriptionId($cancelledSubscription["originalSubscriptionId"])) {
                HeLogger::sendLog("actionCancelSubscriptionsZuora ", HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    'actionCancelSubscriptionsZuora - getUserBySubscriptionId("' . $cancelledSubscription["originalSubscriptionId"] . '") user not found ');
                return array("success" => false, "warnings" => 'actionCancelSubscriptionsZuora - getUserBySubscriptionId("' . $cancelledSubscription["originalSubscriptionId"] . '") user not found ');
            }

            $userAba = new CmAbaUser();

            if (!$userAba->getUserByApiUserId($userZuora->userId)) {
                HeLogger::sendLog("actionCancelSubscriptionsZuora ", HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    'actionCancelSubscriptionsZuora - getUserBySubscriptionId("' . $userZuora->userId . '") user not found ');
                return array("success" => false, "warnings" => 'actionCancelSubscriptionsZuora - getUserBySubscriptionId("' .
                    $userZuora->userId . '") user not found ');
            }

            $userPaymentAba = new CmAbaPayment();

            if (!$userPaymentAba->getPaymentByPaySuppExtProfId($userZuora->userId,
              $userZuora->zuoraSubscriptionNumber, PAY_PENDING)
            ) {
                HeLogger::sendLog("actionCancelSubscriptionsZuora ", HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    'actionCancelSubscriptionsZuora - getUserBySubscriptionId("' . $userZuora->userId . '", "' . $userZuora->zuoraSubscriptionId . '") payment not found ');

                return array("success" => false, "warnings" => 'actionCancelSubscriptionsZuora - getUserBySubscriptionId("' .
                    $userZuora->userId . '", "' . $userZuora->zuoraSubscriptionId . '") payment not found ');
            }

            $userPaymentAba->foreignCurrencyTrans = $userPaymentAba->currencyTrans;
            $userPaymentAba->isRecurring = 1;
            $userPaymentAba->cancelOrigin = STATUS_CANCEL_EXPFAIL;
            $userPaymentAba->lastAction = '[' . HeDate::todaySQL(true) .
              '], Cancelled subscription from zuora. UserId=' .
              $userZuora->userId . ', ' . $userPaymentAba->lastAction;
            $userPaymentAba->status = PAY_CANCEL;

            $cancelDate = substr(trim($cancelledSubscription["cancelledDate"]), 0, 10) . " 00:00:00";

            if (!HeDate::validDateSQL($cancelDate, true)) {
                $cancelDate = $userPaymentAba->dateToPay;
            }

            $userPaymentAba->dateToPay = $cancelDate;

            if (!$userPaymentAba->updatePaymentToCancel()) {

                HeLogger::sendLog("actionCancelSubscriptionsZuora ", HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    'actionCancelSubscriptionsZuora - updatePaymentToCancel fail. PaymentID=' . $this->id . ' ');

                return array("success" => false, "warnings" => 'actionCancelSubscriptionsZuora - updatePaymentToCancel fail. PaymentID=' .
                    $this->id . ' ');
            }

            $userZuora->updateToCancelled();

            //#ZOR-500
            try {

                $dateToday = trim(substr(HeDate::todaySQL(), 0, 10));
                $dateCancelDate = trim(substr($cancelDate, 0, 10));

                if (strtotime($dateCancelDate) <= strtotime($dateToday)) {
                    $userAba->updateCancelReasonAndUserFree(PAY_CANCEL_FAILED_RENEW, $cancelDate);
                }
                else {
                    $userAba->updateCancelReason(PAY_CANCEL_FAILED_RENEW, $cancelDate);
                }

            } catch (Exception $eSel) {
                $warnings = "Error: " . $this->replaceWarningsMessages($eSel->getMessage());
            }

            try {
                $commSelligent = new CmSelligentConnector();
                $commSelligent->sendOperationToSelligent(
                  CmSelligentConnector::CANCELLATIONS,
                  $userAba,
                  array(),
                  "ApiZ-Cancellation"
                );
            } catch (Exception $eSel) {
                $warnings .= "Error: " . $this->replaceWarningsMessages($eSel->getMessage());
            }

            return array("success" => true, "warnings" => $warnings);

        } catch (Exception $e) {

            HeLogger::sendLog("actionCancelSubscriptionsZuora ", HeLogger::IT_BUGS,
              HeLogger::CRITICAL,
              'actionCancelSubscriptionsZuora - cancelSubscriptionsZ Exception: ' . $e->getMessage());
            $warnings = "Error: " . $this->replaceWarningsMessages($e->getMessage());

        }

        return array("success" => false, "warnings" => $warnings);
    }

    private function replaceWarningsMessages($message = "") {

        return str_replace(";","",$message);
    }

    /**
     * #ZOR-558
     *
     * @param array $renewedSubscription
     *
     * @return bool
     */
    public function renewSubscriptionsZ($renewedSubscription = array())
    {

        $warnings = "";
        try {

            if (!isset($renewedSubscription["originalSubscriptionId"]) || !isset($renewedSubscription["userExpirationDate"])) {
                return array("success" => false, "warnings" => 'Incorrect Params');
            }

            $userZuora = new CmAbaUserZuora();

            if (!$userZuora->getUserBySubscriptionId($renewedSubscription["originalSubscriptionId"])) {
                HeLogger::sendLog("actionRenewSubscriptionsZuora ", HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    'actionRenewSubscriptionsZuora - getUserBySubscriptionId("' . $renewedSubscription["originalSubscriptionId"] . '") user not found in table user_zuora');

                return array("success" => false, "warnings" => 'actionRenewSubscriptionsZuora - getUserBySubscriptionId("' .
                    $this->replaceWarningsMessages($renewedSubscription["originalSubscriptionId"]) . '") user not found in table user_zuora');
            }

            $userAba = new CmAbaUser();

            if (!$userAba->getUserByApiUserId($userZuora->userId)) {
                HeLogger::sendLog("actionRenewSubscriptionsZuora ", HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    'actionRenewSubscriptionsZuora - getUserByApiUserId("' . $userZuora->userId . '") user not found in table user');

                return array("success" => false, "warnings" => 'actionRenewSubscriptionsZuora - getUserByApiUserId("' . $userZuora->userId . '") user not found in table user');
            }

            if(!HeDate::validDateSQL($renewedSubscription["userExpirationDate"])) {
                HeLogger::sendLog("actionRenewSubscriptionsZuora ", HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    'User Expiration Date is not correct ' . $renewedSubscription["userExpirationDate"]);

                return array("success" => false, "warnings" => 'User Expiration Date is not correct ' . $renewedSubscription["userExpirationDate"]);
            }

            if ($userAba->updateUserRenewal(PREMIUM, $renewedSubscription["userExpirationDate"], PARTNER_ID_Z_RECURRING)) {

                return array("success" => true, "warnings" => "");
            } else {
                $this->errorMessage = " Save user Expiration date  " . $renewedSubscription["userExpirationDate"] . " has failed.";
                HeLogger::sendLog("Recurring payment user could not be updated. ", HeLogger::IT_BUGS, HeLogger::CRITICAL, $this->errorMessage);

                $warnings = " Save user Expiration date  " . $renewedSubscription["userExpirationDate"] . " has failed.";
            }

        } catch (Exception $e) {
            $warnings = "Error: " . $this->replaceWarningsMessages($e->getMessage());
        }

        return array("success" => false, "warnings" => $warnings);
    }

    /**
     * #ZOR-558
     *
     * @param array $renewedPayment
     *
     * @return bool
     */
    public function renewPaymentsZ($renewedPayment = array())
    {
        $warnings = "";
        try {

            if (!isset($renewedPayment["originalSubscriptionId"]) || !isset($renewedPayment["paymentDate"]) ||
                !isset($renewedPayment["nextRenewalDate"]) || !isset($renewedPayment["invoiceId"]) ||
                !isset($renewedPayment["invoiceNumber"])
            ) {
                return array("success" => false, "warnings" => 'Incorrect Params');
            }

            $userZuora = new CmAbaUserZuora();

            if (!$userZuora->getUserBySubscriptionId($renewedPayment["originalSubscriptionId"])) {

                HeLogger::sendLog("actionRenewPaymentsZuora ", HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    'actionRenewPaymentsZuora - getUserBySubscriptionId("' . $renewedPayment["originalSubscriptionId"] . '") user zuora not found');
                return array("success" => false, "warnings" => 'actionRenewPaymentsZuora - getUserBySubscriptionId("' .
                    $renewedPayment["originalSubscriptionId"] . '") user zuora not found');
            }

            $userAba = new CmAbaUser();

            if (!$userAba->getUserByApiUserId($userZuora->userId)) {
                HeLogger::sendLog("actionRenewPaymentsZuora ", HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    'actionRenewPaymentsZuora - getUserByApiUserId("' . $userZuora->userId . '") user not found');
                return array("success" => false, "warnings" => 'actionRenewPaymentsZuora - getUserByApiUserId("' .
                    $userZuora->userId . '") user not found');
            }

            $cmAbaPayment = new CmAbaPayment();

            if (!$cmAbaPayment->getPaymentByPaySuppExtProfId($userZuora->userId,
                $userZuora->zuoraSubscriptionNumber, PAY_PENDING)
            ) {
                HeLogger::sendLog("actionRenewPaymentsZuora ", HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    'actionRenewPaymentsZuora - getPaymentByPaySuppExtProfId("' . $userZuora->userId . '", "' . $userZuora->zuoraSubscriptionNumber . '") payment not found');
                return array("success" => false, "warnings" => 'actionRenewPaymentsZuora - getPaymentByPaySuppExtProfId("'
                    . $userZuora->userId . '", "' . $userZuora->zuoraSubscriptionNumber . '") payment not found');
            }

            $commRecurrPayment = new CmRecurringPaymentRules();

            if (!$commRecurrPayment->renewPaymentZbyPayment(
                $cmAbaPayment,
                $userAba,
                $renewedPayment
            )
            ) {
                HeLogger::sendLog("actionRenewPaymentsZuora ", HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    'actionRenewPaymentsZuora - renewPaymentZbyPayment("' . $userZuora->userId . '", "' . $userZuora->zuoraSubscriptionNumber . '") problem renew payment');
                return array("success" => false, "warnings" => 'actionRenewPaymentsZuora - renewPaymentZbyPayment("' .
                    $userZuora->userId . '", "' . $userZuora->zuoraSubscriptionNumber . '") problem renew payment');

            }

            $cmAbaUserZuoraInvoices = new CmAbaUserZuoraInvoices();

            if (!$cmAbaUserZuoraInvoices->insertUserZuoraInvoice(
                $userZuora->id,
                $cmAbaPayment->id,
                $userAba->id,
                $renewedPayment["invoiceId"],
                $renewedPayment["invoiceNumber"]
            )
            ) {
                HeLogger::sendLog("actionRenewPaymentsZuora ", HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    'actionRenewPaymentsZuora - insertUserZuoraInvoice("' . $userZuora->userId . '", "' . $userZuora->zuoraSubscriptionNumber . '") problem insert invoice');
                return array("success" => false, "warnings" => 'actionRenewPaymentsZuora - insertUserZuoraInvoice("' . $userZuora->userId . '", "' . $userZuora->zuoraSubscriptionNumber . '") problem insert invoice');
            }

            try {
                $commSelligent = new CmSelligentConnector();
                $commSelligent->sendOperationToSelligent(
                    CmSelligentConnector::RECURRING_PAYMENTS,
                    $userAba,
                    array(),
                    "ApiZ-Renewals"
                );

            } catch (Exception $eSel) {
                $warnings = "Error send to Selligent";
            }

            return array("success" => true, "warnings" => $warnings);

        } catch (Exception $e) {
            $warnings = "Error: " . $this->replaceWarningsMessages($e->getMessage());
        }

        return array("success" => false, "warnings" => $warnings);
    }


    /**
     * #ZOR-303
     *
     * @param array $refundedPayments
     *
     * @return bool
     */
    public function refundPaymentsZ($refundedPayments = array())
    {

        $warnings = "";
        try {

            if (!isset($refundedPayments["id"]) || !isset($refundedPayments["accountId"]) ||
                !isset($refundedPayments["amount"]) || !isset($refundedPayments["refundDate"]))
            {
                return array("success" => false, "warnings" => 'Incorrect Params');
            }
            $userZuora = new CmAbaUserZuora();

            if (!$userZuora->getUserByAccountId($refundedPayments["accountId"])) {
                HeLogger::sendLog("refundPaymentsZ ", HeLogger::IT_BUGS,
                    HeLogger::CRITICAL, 'refundPaymentsZ - getUserByAccountId("' . $refundedPayments["accountId"] . '") user not found');

                return array("success" => false, "warnings" => 'refundPaymentsZ - getUserByAccountId("' .
                    $refundedPayments["accountId"] . '") user not found');
            }

            $userAba = new CmAbaUser();

            if (!$userAba->getUserByApiUserId($userZuora->userId)) {
                HeLogger::sendLog("refundPaymentsZ ", HeLogger::IT_BUGS,
                    HeLogger::CRITICAL, 'refundPaymentsZ - getUserByApiUserId("' . $userZuora->userId . '") user not found');

                return array("success" => false, "warnings" => 'refundPaymentsZ - getUserByApiUserId("' .
                    $userZuora->userId . '") user not found');
            }

            $cmAbaPayment = new CmAbaPayment();

            if ($cmAbaPayment->getPaymentByPaySuppExtUniId($userZuora->userId,
              $refundedPayments["id"], PAY_REFUND)
            ) {
                HeLogger::sendLog("refundPaymentsZ ", HeLogger::IT_BUGS,
                    HeLogger::CRITICAL, 'refundPaymentsZ - getPaymentByPaySuppExtUniId("' . $userZuora->userId . '", "' . $refundedPayments["id"] . '") refund already exist');

                return array("success" => false, "warnings" => 'refundPaymentsZ - getPaymentByPaySuppExtUniId("' .
                    $userZuora->userId . '", "' . $refundedPayments["id"] . '") refund already exist');
            }

            $commRecurrPayment = new CmRecurringPaymentRules();

            $periodMonth = "";
            $currency = "";

            if(isset($refundedPayments["periodMonth"]) AND trim($refundedPayments["periodMonth"]) <> "") {
                $periodMonth = $refundedPayments["periodMonth"];
            }

            if(isset($refundedPayments["currency"]) AND trim($refundedPayments["currency"]) <> "") {
                $currency = $refundedPayments["currency"];
            }

            if ($commRecurrPayment->refundPaymentZ(
              $userAba,
              $refundedPayments["refundDate"],
              $refundedPayments["amount"],
              $refundedPayments["accountId"],
              $refundedPayments["id"],
              $periodMonth,
              $currency
            )
            ) {
                return array("success" => true, "warnings" => $warnings);
            }

        } catch (Exception $e) {
            $warnings = "Error: " . $this->replaceWarningsMessages($e->getMessage());
        }

        HeLogger::sendLog("refundPaymentsZ ", HeLogger::IT_BUGS,
          HeLogger::CRITICAL, 'refundPaymentsZ ');

        return array("success" => false, "warnings" => $warnings);
    }

}
