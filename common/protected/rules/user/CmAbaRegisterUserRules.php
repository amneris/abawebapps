<?php
/**
 * CmAbaRegisterUserRules
 * @author: mgadegaard
 * Date: 01/07/2015
 * Time: 9:11
 * © ABA English 2015
 */

class CmAbaRegisterUserRules {

    private $errorMessage;
    private $errorCode;

    /**
     * Only creation of a new user into our database
     *
     * @param $email
     * @param $pwd
     * @param $langEnv
     * @param integer $idCountry
     * @param string $name
     * @param string $surnames
     * @param integer $currentLevel
     * @param integer $idPartner
     * @param integer $idSourceList Optional
     * @param string $deviceTypeSource
     *
     * @param int $registerSource
     * @return CmAbaUser|bool
     */
    public function registerUser(
        $email,
        $pwd,
        $langEnv,
        $idCountry,
        $name = "",
        $surnames = "",
        $currentLevel = null,
        $idPartner = null,
        $idSourceList = null,
        $deviceTypeSource = null,
        $registerSource = REGISTER_USER
    ) {
        if (!array_key_exists($langEnv, Yii::app()->params['languages'])) {
            $langEnv = HeMixed::getAutoLangDecision();
        }
        $registerUserRules = new CmAbaRegisterUserRules();
        $user = $registerUserRules->registerNewStandardUser(
            $email,
            $pwd,
            $langEnv,
            $name,
            $surnames,
            $currentLevel,
            $idPartner,
            $idCountry,
            $registerSource,
            $idSourceList,
            $deviceTypeSource
        );
        if (!$user) {
            $this->errorCode = "506" ;
            $this->errorMessage = $registerUserRules->getErrorMessage() ;
            return false;
        }
        $moTeacher = new CmAbaUserTeacher();
        $moTeacher->getTeacherById($user->teacherId);
        // Sends an internal message within the Campus, using Messages of Yii. A welcome message basically.
        $gender = $moTeacher->gender;
        HeAbaMail::sendCmMessageCampus(
            $user->teacherId,
            $user->getId(),
            Yii::t('mainApp', 'subjectwelcome_key', array(), null, $langEnv),
            Yii::t(
                'mainApp',
                'welcomeemailcampus_key'.$gender,
                array("{user.name}"=>$user->name,
                    "{tag_br}"=>"<br/>",
                    "{teacher.name}"=>$moTeacher->name),
                null,
                $langEnv
            )
        );

        //  Send data to Selligent
        $selligentConnector = new CmSelligentConnector();
        $selligentConnector->sendOperationToSelligent(
            CmSelligentConnector::REGISTER_WEB,
            $user,
            array("PROMOCODE"=>"NOSENSE", "PASS_TMP"=>$pwd),
            "registerUser web service"
        );

        // check if we have a member get member registration
        if (($idPartner == PARTNER_MGM_EMAIL || $idPartner == PARTNER_MGM_FACEBOOK) && $idSourceList > 0) {
            $mgm = new AbaMemberGetMember();
            $mgm->addRegister($idSourceList);
            // send invitation register to selligent
            $sourceUser = new CmAbaUser();
            $sourceUser->populateUserById($idSourceList);
            $params = array('MAIL' => $email, 'NAME' => $name);
            $selligentConnector->sendOperationToSelligent(
                CmSelligentConnector::INVITATION_REGISTER,
                $sourceUser,
                $params,
                "Invitation register"
            );
        }
        return $user;
    }

    /**
     * @param string $email
     * @param string $pwd
     * @param string $langEnv
     * @param string $name
     * @param string $surnames
     * @param int    $currentLevel
     * @param null   $idPartnerSource
     * @param integer $idCountry
     * @param integer $registerSource
     * @param integer $idSourceList
     * @param string $deviceTypeSource
     *
     * @return CmAbaUser|bool
     */
    public function registerNewStandardUser(
        $email,
        $pwd,
        $langEnv,
        $name = "",
        $surnames = "",
        $currentLevel = null,
        $idPartnerSource = null,
        $idCountry = null,
        $registerSource = null,
        $idSourceList = null,
        $deviceTypeSource = null
    ) {
        $updateUser = 0;
        if ($email == "") {
            $this->errorMessage = " Email not valid.";
            return false;
        }
        $registerLevel = $currentLevel;
        if (empty($currentLevel) || !is_numeric($currentLevel)) {
            $currentLevel = 1;
        }
        $user = new CmAbaUser();
        if ($user->populateUserByEmail($email)) {
            // Let's check if user is DELETED, on that case we have to update it anyway.
            if ($user->userType == DELETED) {
                $updateUser=1;
            } else {
                $this->errorMessage = Yii::t('mainApp', 'emailExist');
                return false;
            }
        }


        //
        //#ABAWEBAPPS-557
        if (HeMixed::checkEmailForFraud($email)) {
            $this->errorMessage = " Email not valid.";

            try {
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_UNKNOWN_H . ': Suspected fraud',
                  HeLogger::PAYMENTS_MOBILE,
                  HeLogger::CRITICAL,
                  " The user with the email " . $email . " could not register for suspected fraud"
                );
            } catch (Exception $e) { }

            return false;
        }
        //


        $user->scenario = "registerUser"; //Scenarios property from Yii Models.
        $user->email= $email;
        $user->setPassword($pwd, true);
        $user->langEnv = $langEnv;
        $user->langCourse = $langEnv;
        $user->name = $name;
        $user->surnames = $surnames;
        $user->currentLevel = $currentLevel;
        $user->registerLevel = $registerLevel;
        $user->countryId = $idCountry;
        $user->countryIdCustom = $idCountry;
        $user->idSourceList = $idSourceList;
        $user->deviceTypeSource = $deviceTypeSource;

        $teacher = new CmAbaUserTeacher();
        $teacher->getTeacherDefaultByLang($langEnv);
        $user->teacherId = $teacher->id;
        // 2.10.Assign [userType]=FREE y [expirationDate]=Today, we set Today, for FREE user have no use this field.
        $user->userType = FREE;
        $user->expirationDate = HeDate::todaySQL(true);
        // 2.11. [EntryDate] current date.
        $user->entryDate = HeDate::todaySQL(true);

        // From which service web user has been registered:
        $user->registerUserSource = REGISTER_USER;
        if (isset($registerSource)) {
            $user->registerUserSource = $registerSource;
        }

        // Id de Level Test Partner or maybe Aba Web site:
        if (!isset($idPartnerSource)) {
            $user->idPartnerSource = "". Yii::app()->config->get("ABA_PARTNERID");
            $user->idPartnerCurrent = $user->idPartnerSource;
        } else {
            $user->idPartnerSource = "". $idPartnerSource;
            $user->idPartnerCurrent = $user->idPartnerSource;
        }
        if (!$user->validate(array("email", "password", "langEnv"))) {
            $msgErrores = "";
            $errores = $user->getErrors();
            foreach ($errores as $field => $message) {
                $msgErrores .= " Parameter ".$field." is incorrect ".$message[0];
            }
            $this->errorMessage = " Some parameters were not correct ".$msgErrores;
            return false;
        }
        if (!$user->insertUserForRegistration($updateUser)) {
            $this->errorMessage = " Insert into database failed.";
            return false;
        }
        if (!$user->populateUserByEmail($email, "", false)) {
            return false;
        }
        return $user;
    }

    /**
     * Function used for registering a user with Facebook ID
     * @param string $sEmail
     * @param string $sName
     * @param string $sSurname
     * @param null $iFbid
     * @param string $sGender
     * @param string $langEnv
     * @param string $idCountry
     * @param int $idPartner
     * @param int $idSourceList
     * @param string $device
     * @param int $currentLevel
     * @return CmAbaUser|bool
     * @throws CDbException
     */
    public function registerUserFacebook(
        $sEmail = '',
        $sName = '',
        $sSurname = '',
        $iFbid = null,
        $sGender = '',
        $langEnv = '',
        $idCountry = '',
        $idPartner = null,
        $idSourceList = null,
        $device = '',
        $currentLevel = null
    ) {
        $this->errorMessage = "";
        $sUserGender = (trim($sGender) == 'female' ? 'F' : (trim($sGender) == 'male' ? 'M' : ''));
        if ($idCountry == '') {
            $idCountry = Yii::app()->config->get("ABACountryId");
        }

        if ($sEmail == "") {
            $this->errorCode = '507';
            $this->errorMessage = "The user email is empty or is not valid.";
            return false;
        }

        $abaUser = new CmAbaUser();
        $userExists = $abaUser->populateUserByEmail($sEmail);

        $abaUserFb = new CmAbaUser();
        $fbUserExists = $abaUserFb->populateUserByFacebookId($iFbid);

        //
        // Add user, if no exist
        //
        if (!$userExists && !$fbUserExists) {
            $sPwd = $this->genRandomPasswordNewUser($sEmail, "LT");
            //
            // Register user
            //
            $newUser = $this->registerUser(
                $sEmail,
                $sPwd,
                $langEnv,
                $idCountry,
                trim($sName),
                trim($sSurname),
                $currentLevel,
                $idPartner,
                $idSourceList,
                $device,
                REGISTER_USER_FROM_FACEBOOK
            );

            if ($newUser) {
                $newUser->fbId = $iFbid;
                $aUpdate = array("fbId");
                if ($sUserGender != '') {
                    array_push($aUpdate, "gender");
                    $newUser->gender = $sUserGender;
                }
                $newUser->update($aUpdate);
            }

            return $newUser;
        }

        //
        // Update user, if exists
        //
        if ($userExists && !$fbUserExists) {
            return $this->changeUserFbData($abaUser, $sName, $sSurname, $sUserGender, $iFbid);
        }
        return false;
    }

    /**
     * Registers a new device used by an user and communicates this to Selligent. If user/device combination is already
     * registered the existing token is returned otherwise the newly created token is returned. If it is not possible
     * to create a token or the device false is returned.
     *
     * @param integer    $deviceId
     * @param string     $sysOper
     * @param string     $deviceName
     * @param CmAbaUser  $user
     * @param string     $appVersion
     *
     * @return bool|string False or token is returned
     */
    public function registerNewDevice($deviceId, $sysOper, $deviceName, CmAbaUser $user, $appVersion)
    {
        // Check if device already exist for this user:
        $userDevice = new CmAbaUserDevice();
        if ($userDevice->getDeviceByIdUserId($deviceId, $user->id)) {
            return $userDevice->token;
        }
        $token = HeUtils::genTokenForMobile($sysOper, $deviceId, $user->email, $user->password);
        if (!$token) {
            return false;
        }
        $appVersion = is_null($appVersion) ? "n/a" : $appVersion;
        if ($userDevice->insertNewDevice($deviceId, $user->id, $deviceName, $sysOper, $token, $appVersion)) {
            $commSelligent = new CmSelligentConnector();
            $commSelligent->sendOperationToSelligent(
                CmSelligentConnector::REGISTER_DEVICE,
                $user,
                array(
                  "DEVICE_NAME" => $deviceName,
                  "SYSOPER" => $sysOper,
                  "APP_VERSION" => $appVersion),
                "registerNewDevice"
            );
            return $token;
        }
        return false;
    }

    /**
     * Checks if geo location is available, and in that case we save all possible data into database.
     * @param CmAbaUser $user
     * @param $ipCgi
     *
     * @return bool
     */
    public function registerGeoLocation(CmAbaUser $user, $ipCgi)
    {
        if (!ip2long($ipCgi)) {
            return false;
        }
        $userLocation = new CmUserLocation();
        $geoUserLoc = Yii::app()->geoip->lookupLocation($ipCgi);
        if ($geoUserLoc) {
            if ($userLocation->getLocationByIdUserIdIp($user->getId(), '', 0)) {
                $userLocation->updateRestNoDefault();
            }
            $userLocation->insertNewLocation(
                $user->getId(),
                $ipCgi,
                $geoUserLoc->longitude,
                $geoUserLoc->latitude,
                $geoUserLoc->countryCode,
                $geoUserLoc->region,
                $geoUserLoc->city,
                1
            );
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $fbId
     *
     * @return CmAbaUser|bool
     */
    public function checkIfFbUserExists($fbId)
    {
        $user = new CmAbaUser();
        $fbUserExists = $user->populateUserByFacebookId($fbId);
        if ($fbUserExists) {
            return $user;
        }
        return false;
    }

    /**
     * @param CmAbaUser $abaUser
     * @param $sName
     * @param $sSurname
     * @param $iFbid
     * @param $sUserGender
     *
     * @return AbaUser
     */
    public function changeUserFbData($abaUser, $sName, $sSurname, $sUserGender, $iFbid = null)
    {
        if (trim($abaUser->fbId) == '') {
            $fbUser = $this->checkIfFbUserExists($iFbid);
            if (!$fbUser) {
                $aUpdate = array();
                if ($iFbid != '' || $iFbid != 0) {
                    $aUpdate = array("fbId");
                    $abaUser->fbId = $iFbid;
                }
                if (trim($abaUser->name) == '' && trim($sName) != '') {
                    $abaUser->name = $sName;
                    array_push($aUpdate, "name");
                }
                if (trim($abaUser->surnames) == '' && trim($sSurname) != '') {
                    $abaUser->surnames = $sSurname;
                    array_push($aUpdate, "surnames");
                }
                if (trim($abaUser->gender) == '' && $sUserGender != '') {
                    $abaUser->gender = $sUserGender;
                    array_push($aUpdate, "gender");
                }
                if (count($aUpdate) > 0) {
                    $abaUser->update($aUpdate);
                }
            }
        }
        return $abaUser;
    }

    /** Returns text associated with last error.
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /** Returns code number error
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param string $email a valid email account
     * @param string $source source of the creation of the user, just descriptive
     *
     * @return string
     */
    public function genRandomPasswordNewUser($email, $source)
    {
        $email = substr($email, 0, (strpos($email, "@")-1));
        $str = '2014abaenglish'."welcome".$email.'14201209'.strval(rand(100, 999));
        $shuffled = $source.substr(str_shuffle($str), 0, 8);
        return strtolower($shuffled);
    }
    
}
