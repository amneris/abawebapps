<?php
/**
 * CmAbaProductPriceRules
 * @author: mgadegaard
 * Date: 30/06/2015
 * Time: 16:37
 * © ABA English 2015
 */

class CmAbaProductPriceRules
{

    /**
     * Finds and returns product subscription periodicity
     *
     * @param CmAbaUser $user
     *
     * @return string Product subscription periodicity
     */
    public function getUserProductPeriodPay(CmAbaUser $user)
    {
        $idPeriodPay = '';
        if ($user->userType == PREMIUM) {
            $lastPayment = new CmAbaPayment();
            if ($lastPayment->getLastPayPendingByUserId($user->id)) {
                $productPrice = new CmAbaProductPrice();
                $productPrice->getProductById($lastPayment->idProduct);
                $idPeriodPay =  $productPrice->idPeriodPay;
            }
        }
        return $idPeriodPay;
    }

    public function getProductsTiersByUserCountry(CmAbaUser $user)
    {
        $productsPricesAppstore = new CmAbaProductsPricesAppstore();

        $productsTiers = $productsPricesAppstore->getProductsTiersByUserCountry($user);

        if (count($productsTiers) == 0) {
            $productsTiers = $productsPricesAppstore->getDefaultProductsTiersByUser($user);
        }

        return $productsTiers;
    }
}