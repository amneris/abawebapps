<?php

/**
 * CmSelligentConnector
 * @author: mgadegaard
 * Date: 02/07/2015
 * Time: 13:39
 * © ABA English 2015
 */
class CmSelligentConnector
{
    //Selligent Operation Constants
    const FREE_TO_B2B = "freeToB2b";
    const REGISTER_DEVICE = "registerDevice";
    const REGISTER_TEACHER = "registerTeacher";
    const SYNCHRO_USER = "synchroUser";
    const PERFIL = "perfil";
    const CHANGE_PASSWORD = "cambioPassword";
    const LOGIN = "login";
    const RECOVER_PASSWORD = "recoverPassword";
    const REGISTER_WEB = "registroWeb";
    const INVITATION_REGISTER = "invitationRegister";
    const PAYMENTS = "pagos";
    const CANCELLATIONS = "cancelaciones";
    const RECURRING_PAYMENTS = "pagosRecurrentes";
    const PARTNER_SOURCE = "partnerSource";

    const ERROR_USER_DOES_NOT_EXIST = "ERR: DON'T EXIST IDUSER_WEB" ;

    //Selligent operation parameter arrays
    protected static $selligentOperationParameters = array(
        'cambioPassword' => array(
            'IDUSER_WEB' => '',
            'MAIL' => '',
            'PASSWORD' => '',
            'PASS_TMP' => '',
        ),
        'freeToB2b' => array(
            'IDUSER_WEB' => '',
            'MAIL' => '',
            'NAME' => '',
            'SURNAME' => '',
            'COUNTRY_IP' => '',
            'PASS_TMP' => '',
            'PASSWORD' => '',
            'TELEPHONE' => '',
            'PREF_LANG' => '',
            'ENTRY_DATE' => '',
            'USER_TYPE' => '',
            'EXPIRATION_DATE' => '',
            'PRODUCT_TYPE' => '',
            'PROMOCODE' => '',
            'LEVEL_COURSE' => '',
            'FLAG_CAMPUS_LOGIN' => '',
            'FLAG_CAMPUS_WORK' => '',
            'PREMIUM_HISTORIC' => '',
            'PARTNERT_SOURCE' => '',
            'PARTNERT_SOURCE_GROUP' => '',
            'PARTNERT_FIRSTPAY' => '',
            'PARTNERT_FIRSTPAY_GROUP' => '',
            'PARTNERT_CURRENT' => '',
            'PARTNERT_CURRENT_GROUP' => '',
            'MONTH_PERIOD' => '',
            'PAY_DATE_START' => '',
            'PAY_DATE_END' => '',
            'AMOUNT_PRICE' => '',
            'CURRENCY' => '',
            'PRICE_CURR' => '',
            'TRANSID' => '',
        ),
        'perfil' => array(
            'IDUSER_WEB' => '',
            'NAME' => '',
            'SURNAME' => '',
            'BIRTHDAY_DT' => '',
            'LEVEL_COURSE' => '',
            'COUNTRY_USER' => '',
            'TELEPHONE' => '',
            'PREF_LANG' => '',
        ),
        'recoverPassword' => array(
            'IDUSER_WEB' => '',
        ),
        'registerDevice' => array(
            'IDUSER_WEB' => '',
            'MAIL' => '',
            'DEVICE_NAME' => '',
            'SYSOPER' => '',
            'APP_VERSION' => ''
        ),
        'registroWeb' => array(
            'IDUSER_WEB' => '',
            'MAIL' => '',
            'NAME' => '',
            'SURNAME' => '',
            'COUNTRY_IP' => '',
            'COUNTRY_USER' => '',
            'PASS_TMP' => '',
            'PASSWORD' => '',
            'PREF_LANG' => '',
            'ENTRY_DATE' => '',
            'USER_TYPE' => '',
            'EXPIRATION_DATE' => '',
            'PRODUCT_TYPE' => '',
            'PROMOCODE' => '',
            'LEVEL_COURSE' => '',
            'FLAG_CAMPUS_LOGIN' => '',
            'FLAG_CAMPUS_WORK' => '',
            'PREMIUM_HISTORIC' => '',
            'PARTNERT_SOURCE' => '',
            'PARTNERT_SOURCE_GROUP' => '',
            'PARTNERT_FIRSTPAY' => '',
            'PARTNERT_FIRSTPAY_GROUP' => '',
            'PARTNERT_CURRENT' => '',
            'PARTNERT_CURRENT_GROUP' => '',
            'TEACHERID' => ''
        ),
        'synchroUser' => array(
            'IDUSER_WEB' => '',
            'PREF_LANG' => '',
            'MAIL' => '',
            'NAME' => '',
            'SURNAME' => '',
            'BIRTHDAY_DT' => '',
            'SEX' => '',
            'COUNTRY_USER' => '',
            'WHY_STUDY' => '',
            'PREVIOUS_EXPERIENCE' => '',
            'OCCUPATION' => '',
            'PRODUCT_TYPE' => '',
            'PASSWORD' => '',
            'TELEPHONE' => '',
            'USER_TYPE' => '',
            'EXPIRATION_DATE' => '',
            'CANCEL_REASON' => '',
            'PROMOCODE' => '',
            'LEVEL_COURSE' => '',
            'PREMIUM_HISTORIC' => '',
            'PARTNERT_SOURCE' => '',
            'PARTNERT_SOURCE_GROUP' => '',
            'PARTNERT_FIRSTPAY' => '',
            'PARTNERT_FIRSTPAY_GROUP' => '',
            'PARTNERT_CURRENT' => '',
            'PARTNERT_CURRENT_GROUP' => '',
            'TEACHERID' => '',
            'COUNTRY_IP' => '',
        ),
        'pagos'=> array(
            'IDUSER_WEB'=>'',
            'USER_TYPE'=>'',
            'NAME'=>'',
            'SURNAME'=>'',
            'PRODUCT_TYPE'=>'',
            'PREMIUM_HISTORIC'=>'',
            'EXPIRATION_DATE'=>'',
            'PARTNERT_CURRENT'=>'',
            'PARTNERT_CURRENT_GROUP'=>'',
            'PARTNERT_FIRSTPAY'=>'',
            'PARTNERT_FIRSTPAY_GROUP'=>'',
            'PROMOCODE'=>'',
            'MONTH_PERIOD' =>   '',
            'PAY_DATE_START' => '',
            'PAY_DATE_END' =>   '',
            'AMOUNT_PRICE' =>   '',
            'CURRENCY' =>       '',
            'PRICE_CURR' =>       '',
            'TRANSID' =>       '',
        ),
        'cancelaciones' => array(
            'IDUSER_WEB' => '',
            'USER_TYPE' => '',
            'EXPIRATION_DATE' => '',
            'CANCEL_REASON' => '',
        ),
        'pagosRecurrentes' => array(
            'IDUSER_WEB' => '',
            'USER_TYPE' => '',
            'NAME' => '',
            'SURNAME' => '',
            'PRODUCT_TYPE' => '',
            'PREMIUM_HISTORIC' => '',
            'EXPIRATION_DATE' => '',
            'PARTNERT_CURRENT' => '',
            'PARTNERT_CURRENT_GROUP' => '',
            'PARTNERT_FIRSTPAY' => '',
            'PARTNERT_FIRSTPAY_GROUP' => '',
            'PROMOCODE' => '',
        ),
        'partnerSource'=> array(
            'ID_PARTNER'=>'',
            'PARTNER_NAME'=>'',
            'ID_PARTNER_GROUP'=>'',
            'NAME_GROUP'=>'',
            'ID_CATEGORY'=>'',
            'ID_BUSINESS_AREA'=>'',
            'ID_TYPE'=>'',
            'ID_GROUP'=>'',
            'ID_CHANNEL'=>'',
            'ID_COUNTRY'=>'',
        ),
    );

    protected static $selligentFields = array(
        'IDUSER_WEB' => 'user.id',
        'MAIL' => 'user.email',
        'NAME' => 'user.name',
        'SURNAME' => 'user.surnames',
        'BIRTHDAY_DT' => 'user.birthDate',
        'SEX' => 'user.gender',
        'COUNTRY_USER' => 'user.countryIdCustom',
        'PREF_LANG' => 'user.langEnv',
        'COUNTRY_IP' => 'user.countryId',
        'PASSWORD' => 'user.keyExternalLogin',
        'PASS_TMP' => 'user.password',
        'TELEPHONE' => 'user.telephone',
        'LEVEL_COURSE' => 'user.currentLevel',
        'USER_TYPE' => 'user.userType',
        'ENTRY_DATE' => 'user.entryDate',
        'EXPIRATION_DATE' => 'user.expirationDate',
        'FLAG_CAMPUS_LOGIN' => 'user.firstLoginDone',
        'FLAG_CAMPUS_WORK' => 'user.hasWorkedCourse',
        'CANCEL_REASON' => 'user.cancelReason',
        'TEACHERID' => 'user.teacherId',
        'AMOUNT_PRICE' => 'payments.amountPrice',
        'CURRENCY '=> 'payments.currencyTrans',
        'MONTH_PERIOD' => 'payments.monthPeriod',
        'PAY_DATE_END' => 'payments.dateEnd',
        'PAY_DATE_START' => 'payments.dateStart',
        'PRODUCT_TYPE' => 'payments.idPeriodPay',
        'PRICE_CURR' => '',
        'TRANSID' => 'payments.id',
        'PROMOCODE' => '.',
        'WHY_STUDY' => 'user_objective.whyStudy',
        'PREVIOUS_EXPERIENCE' => 'user_objective.previousEngExperience',
        'OCCUPATION' => 'user_objective.occupationText',
        'PREMIUM_HISTORIC' => 'payments.LastSuccessPayment',
        'PARTNERT_SOURCE' => 'user.idPartnerSource',
        'PARTNERT_SOURCE_GROUP' => 'aba_partners_list.idPartnerGroup_Source',
        'PARTNERT_FIRSTPAY' => 'user.idPartnerFirstPay',
        'PARTNERT_FIRSTPAY_GROUP' => 'aba_partners_list.idPartnerGroup_FirstPay',
        'PARTNERT_CURRENT' => 'user.idPartnerCurrent',
        'PARTNERT_CURRENT_GROUP' => 'aba_partners_list.idPartnerGroup_Current',
        'DEVICE_NAME'=> 'user_devices.deviceName',
        'SYSOPER'=> 'user_devices.sysOper',
        'APP_VERSION'=> 'user_devices.appVersion',
    );
    protected static $simulateEmailForOperations = array(
        'cambioPassword'=> true,
        'freeToB2b' => true,
        'login'=> false,
        'recoverPassword' => true,
        'registerDevice'=> true,
        'registroWeb'=> true,
        'perfil' => true,
        'synchroUser'=> false,
        'recoverPassword' => true,
        'pagos' => true,
        'cancelaciones'=> false,
        'pagosRecurrentes'=> true,
        'partnerSource'=> true,
    );
    protected static $yes = 'si';
    protected static $no = 'no';
    protected $operationName;
    protected $operationsUrls;
    protected $errMessage;
    protected $response;
    protected $aFieldsToBeSent;
    /* @var CmLogSentSelligent $log */
    protected $log;

    public function __construct()
    {
        $this->log = new CmLogSentSelligent();
        $this->operationsUrls = array(
            'cambioPassword' => Yii::app()->config->get("OP_URL_SELLIGENT_CAMBIOPASSWORD"),
            'freeToB2b' => Yii::app()->config->get("OP_URL_SELLIGENT_REGISTROAGRUPADORES"),
            'login' => Yii::app()->config->get("OP_URL_SELLIGENT_LOGIN"),
            'perfil' => Yii::app()->config->get("OP_URL_SELLIGENT_PERFIL"),
            'registerDevice' => Yii::app()->config->get("OP_URL_SELLIGENT_REGISTERDEVICE"),
            'registroWeb' => Yii::app()->config->get("OP_URL_SELLIGENT_REGISTROWEB"),
            'synchroUser' => Yii::app()->config->get("OP_URL_SELLIGENT_SINCROUSER"),
            'recoverPassword' => Yii::app()->config->get("OP_URL_SELLIGENT_RECOVERPASSWORD"),
            'pagos' => Yii::app()->config->get("OP_URL_SELLIGENT_PAGOS"),
            'cancelaciones' => Yii::app()->config->get("OP_URL_SELLIGENT_CANCELACIONES"),
            'pagosRecurrentes' => Yii::app()->config->get("OP_URL_SELLIGENT_PAGOSRECURRENTES"),
            'partnerSource' => Yii::app()->config->get("OP_URL_SELLIGENT_PARTNERSOURCE"),
        );
    }

    public function sendOperationToSelligent($operationName, CmAbaUser $user, $extraFields = array(), $from = "")
    {
        // We refresh all values from database, but $extraFields will overwrite any values coming from database:
        $user->populateUserById($user->getId());
        // We only send information to Selligent for FREE and/or PREMIUM users. Ignore LEADS, TEACHERS and PLUS
        if ($operationName == self::REGISTER_TEACHER) {
            if (intval($user->userType) !== TEACHER) {
                return true;
            }
        } else {
            if (intval($user->userType) !== PREMIUM &&
                intval($user->userType) !== FREE &&
                intval($user->userType) !== DELETED
            ) {
                return true;
            }
        }
        $this->operationName = $operationName;
        if ($this->buildData($user, $extraFields)) {
            $url = Yii::app()->config->get("URL_SELLIGENT") . $this->operationsUrls[$this->operationName];
            if (!$this->sendData($url, $user)) {
                $this->errMessage = " The data for the web service call for Selligent could not be sent to " . $url .
                    "Operation = " . $this->operationName . " , detailed error: " . $this->errMessage;
                if (Yii::app()->config->get("ENABLE_LOG_SELLIGENT") == 1) {
                    $this->log->insertLog($url, $operationName, serialize($this->aFieldsToBeSent), 'false:' .
                        $this->response, $user->getId(), $from, $this->errMessage);
                }
                $user->updateDateLastSentSelligent(null);
                return false;
            }
            $user->updateDateLastSentSelligent(HeDate::todaySQL(true));
        } else {
            $this->errMessage = " The data for the web service call for Selligent could not be resolved. Operation = " .
                $this->operationName . " , detailed error: " . $this->errMessage;
            HeLogger::sendLog(" WARNING=Error when building data to Selligent from user " . $user->email,
              HeLogger::IT_BUGS, HeLogger::CRITICAL,
              " For user=" . $user->email . " in the operation $operationName (from action $from) the" .
              "  data build syncronization with Selligent Failed for some reason: " . $this->errMessage);
            if (Yii::app()->config->get("ENABLE_LOG_SELLIGENT") == 1) {
                $this->log->insertLog(
                    '',
                    $operationName,
                    serialize($this->aFieldsToBeSent),
                    'false',
                    $user->getId(),
                    $from,
                    $this->errMessage
                );
            }
            return false;
        }
        // Save in log_sent_selligent all request
        if (Yii::app()->config->get("ENABLE_LOG_SELLIGENT") == 1) {
            $this->log->insertLog(
                $url,
                $operationName,
                serialize($this->aFieldsToBeSent),
                "OK, ".$this->response,
                $user->getId(),
                $from,
                ""
            );
        }
        return true;
    }

    /**
     * @param CmAbaUser $user
     * @param array   $extraFields
     *
     * @return bool
     */
    private function buildData(CmAbaUser $user, $extraFields = array())
    {
        if (!array_key_exists($this->operationName, self::$selligentOperationParameters)) {
            $this->errMessage=" There is no web service in Selligent called operation = ".$this->operationName;
            return false;
        }
        // We find out the fields for this web service and fill them with the proper values:
        $this->aFieldsToBeSent = array();
        foreach (self::$selligentOperationParameters[$this->operationName] as $fieldSelligent => $staticValue) {
            $finalValue = $this->calculateField($user, $fieldSelligent, $staticValue, $extraFields);
            if ($finalValue === false) {
                $this->errMessage = " Calculating value for selligent field($fieldSelligent=".$finalValue.") failed: " .
                    $this->errMessage;
                return false;
            }
            if ($finalValue !== "" && (!empty($finalValue) ||  $finalValue===0)) {
                $this->aFieldsToBeSent[$fieldSelligent] = $finalValue;
            }
        }
        return true;
    }

    private function sendData($url, CmAbaUser $user)
    {
        if (Yii::app()->config->get("ENABLE_SELLIGENT")=='1') {
            $response = HeComm::sendHTTPPostCurl($url, null, $this->aFieldsToBeSent);
            if (is_array($response)) {
                $this->errMessage = $this->errMessage." Sending POST fields to Selligent failed: CURL error details: ".
                    $response["description"];
                $this->response = $this->errMessage;
                return false;
            }
        } else {
            $this->response = "Ok, because SendData disabled in config";
            return true;
        }
        // It connects and we have a RESPONSE, but we have to analyse the content of the response: 3 cases:
        if (trim(strip_tags($response))=='KO') {
            $this->errMessage = $this->errMessage .
                " Sending POST fields to Selligent was successful but SELLIGENT returned K.O.";
            $this->response = $response;
            return false;
        } elseif (is_string($response) && substr(trim(strip_tags($response)), 0, 3) == 'ERR') {
            // The string could be = ERR: Don't exist ID_USER_WEB: 300000
            // Exceptional case, in case of a FREE user it does not exist in SELLIGENT, then we force its automatic
            // creation through registerUser operation by ourselves.
            if (($this->operationName == self::LOGIN || $this->operationName == self::CHANGE_PASSWORD) &&
                strpos(strtoupper(trim($response)), self::ERROR_USER_DOES_NOT_EXIST) > 0) {
                $aPriorityFields = array();
                if (array_key_exists("PASS_TMP", $this->aFieldsToBeSent)) {
                    $aPriorityFields = array("PASS_TMP" => $this->aFieldsToBeSent["PASS_TMP"] );
                }
                $ret = $this->sendOperationToSelligent(
                    self::REGISTER_WEB,
                    $user,
                    $aPriorityFields,
                    "sendData in Selligent, auto-register"
                );
                if ($ret) {
                    return true;
                }
            }
            $this->errMessage = $this->errMessage . " Sending POST fields to Selligent was successful but some fields" .
                " were missing or went wrong, details by SELLIGENT= " . $response;
            $this->response = $response;
            return false;
        } elseif (substr(trim(strip_tags($response)), 0, 2) !== 'OK' && substr(trim($response), 0, 2) !== 'OK') {
            $this->errMessage = $this->errMessage." Sending POST fields to Selligent was successful but SELLIGENT " .
                "server response was a new error, details by SELLIGENT= " . $response;
            $this->response = $response;
            return false;
        }
        $this->response = $response;
        return true;
    }

    private function calculateField(CmAbaUser $user, $fieldSelligent, $staticValue, $extraFields = array())
    {
        if ($staticValue !== "") {
            $finalValue =  $staticValue;
        } elseif (is_array($extraFields) && array_key_exists($fieldSelligent, $extraFields)) {
            $finalValue = $extraFields[$fieldSelligent];
        } else {
            if (!array_key_exists($fieldSelligent, self::$selligentFields)) {
                $this->errMessage = " Field  = " . $fieldSelligent . " does not appear in Selligent Fields list.";
                return false;
            }
            $aFieldCampus = explode(".", self::$selligentFields[$fieldSelligent]);
            if (count($aFieldCampus)<1) {
                $this->errMessage = " The format of field correspondence ".$fieldSelligent.
                    " is incorrect. Check self::aFieldsSelligent list please.";
                return false;
            }
            $finalValue = null;
            $tableCampus = $aFieldCampus[0];
            $fieldCampus = $aFieldCampus[1];
            switch ($tableCampus) {
                case "user":
                    if (isset($user->$fieldCampus)) {
                        $finalValue= $user->$fieldCampus;
                        if ($fieldCampus == 'firstLoginDone') {
                            $finalValue = (!$finalValue) ? self::$no : self::$yes;
                        }
                        // All DATES fields are within the user TABLE. The are named with Date suffix.
                        if (strpos($fieldCampus, "Date")>0 && HeDate::validDate($finalValue, false, "-")) {
                            $finalValue = HeDate::europeanToSQL($finalValue, false, "-");
                        }
                    }
                    break;
                case "user_devices":
                    switch ($fieldCampus) {
                        case 'deviceName':
                            $finalValue = $this->getUserDeviceValueByUser($user, $fieldCampus);
                            break;
                        case 'sysOper':
                            $finalValue = $this->getUserDeviceValueByUser($user, $fieldCampus);
                            break;
                        case 'appVersion':
                            $finalValue = $this->getUserDeviceValueByUser($user, $fieldCampus);
                            break;
                        default:
                            $this->errMessage = " Value ".$fieldCampus." not implemented.";
                            return false;
                            break;
                    }
                    break;
                case "user_objective":
                    $finalValue = "";
                    $moUserObjective = new CmUserObjective();
                    if ($moUserObjective->getObjectivesByUserId($user->getId())) {
                        if (isset($moUserObjective->$fieldCampus)) {
                            $finalValue = $moUserObjective->$fieldCampus;
                        }
                    }
                    break;
                case "payments":
                    switch ($fieldCampus) {
                        case 'idPeriodPay':
                            $finalValue = $this->getPaymentsIdPeriodPay($user);
                            break;
                        case 'LastSuccessPayment':
                            $finalValue = $this->getPaymentsLastSuccessPayment($user);
                            break;
                        default:
                            $this->errMessage = " Value ".$fieldCampus." not implemented.";
                            return false;
                            break;
                    }
                    break;
                case "aba_partners_list":
                    switch ($fieldCampus) {
                        case 'idPartnerGroup_Source':
                            $finalValue = $this->getPartnerGroupName($user->idPartnerSource);
                            break;
                        case 'idPartnerGroup_FirstPay':
                            $finalValue = $this->getPartnerGroupName($user->idPartnerFirstPay);
                            break;
                        case 'idPartnerGroup_Current':
                            $finalValue = $this->getPartnerGroupName($user->idPartnerCurrent);
                            break;
                        default:
                            $this->errMessage = " Value ".$fieldCampus." not implemented.";
                            return false;
                            break;
                    }
                    break;
                default:
                    $finalValue = 'n/a';
                    break;
            }
        }
        return $finalValue;
    }

    private function getUserDeviceValueByUser($user, $field)
    {
        $userDevice = new CmAbaUserDevice();
        if ($userDevice->getDeviceByUserId($user->id)) {
            $value = $userDevice->$field;
        } else {
            $value = "n/a";
        }
        return $value;
    }

    /**
     * payments.idPeriodPay
     * @param CmAbaUser $user
     *
     * @return string
     */
    private function getPaymentsIdPeriodPay(CmAbaUser $user)
    {
        $payment = new CmAbaPayment();
        if (!$payment->getLastPaymentByUserId($user->getId())) {
            return '0';
        }

        return strval($payment->idPeriodPay);
    }

    /**
     * Returns if the user has ever paid at least once, meaning he's been at least once a PREMIUM user.
     * @param CmAbaUser $user
     *
     * @return string
     */
    private function getPaymentsLastSuccessPayment(CmAbaUser $user)
    {
        if ($user->idPartnerFirstPay !== 0 && !is_null($user->idPartnerFirstPay) && $user->idPartnerFirstPay !== '') {
            return self::$yes;
        }
        return self::$no;
    }

    /**
     * Based on an Id Partner return the partner group name
     * @param $idPartner
     *
     * @return string
     */
    private function getPartnerGroupName($idPartner)
    {
        $partner = new CmPartnersList();
        if (!$partner->getPartnerByIdPartner($idPartner)) {
            return "NO PARTNER INFORMED";
        }

        return $partner->nameGroup;
    }
}
