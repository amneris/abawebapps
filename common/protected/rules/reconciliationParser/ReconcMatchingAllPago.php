<?php
/**
 * Created by Joaquim Forcada (Quino) in Aba English with PHP Storm
 * User: Joaquim Forcada (Quino)
 * Date: 20/02/14
 * Time: 17:28

Implementation for La Caixa
 */

class ReconcMatchingAllPago extends ReconcMatching
{

    /**
     * @param LogReconciliation[] $aMoLogsReconcs
     * @param string $filePath
     *
     * @return bool
     */
    public function processPackTransactions(array $aMoLogsReconcs, $filePath)
    {
        $allSuccess = parent::processPackTransactions($aMoLogsReconcs, $filePath);

        //$this->processResponseNewFile($aMoLogsReconcs, $filePath);

        return $allSuccess;
    }

    /**
     * @param LogReconciliation $moLogReconciliation
     *
     * @return bool|ReconcPayment
     */
    protected function findPaymentSpecificGateway(LogReconciliation $moLogReconciliation)
    {
        $moPayment = new ReconcPayment();
        if ( $moPayment->getPaymentByPaySuppExtUnid($moLogReconciliation->refPaySuppExtUniId) ) {
            // If found, we link them automatically on the log reconciliation side:
            $moLogReconciliation->idPayment = $moPayment->id;
            $moLogReconciliation->save(true, array("idPayment"));
            return $moPayment;
        } else {
            return false;
        }
    }

}