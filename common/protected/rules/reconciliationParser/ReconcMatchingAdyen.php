<?php

class ReconcMatchingAdyen extends ReconcMatching
{

    /**
     * @param LogReconciliation[] $aMoLogsReconcs
     * @param string $filePath
     *
     * @return bool
     */
    public function processPackTransactions(array $aMoLogsReconcs, $filePath)
    {
        $allSuccess = parent::processPackTransactions($aMoLogsReconcs, $filePath);

        return $allSuccess;
    }

    /**
     * @param LogReconciliation $moLogReconciliation
     *
     * @return bool|ReconcPayment
     */
    protected function findPaymentSpecificGateway(LogReconciliation $moLogReconciliation)
    {
        $moPayment = new ReconcPayment();
        if ($moPayment->getPaymentByPaySuppOrderId($moLogReconciliation->refPaySuppOrderId)) {
            // If found, we link them automatically on the log reconciliation side:
            $moLogReconciliation->idPayment = $moPayment->id;
            $moLogReconciliation->save(true, array("idPayment"));
            return $moPayment;
        } else {
            return false;
        }
    }
}