<?php
/**
 * Created by Joaquim Forcada (Quino) in Aba English with PHP Storm
 * User: Joaquim Forcada (Quino)
 * Date: 19/02/14
 * Time: 13:48
 
 ---Please brief description here---- 
 */
class FileParserAllPago extends FileParser
{
    /* @var $xmlObj SimpleXMLElement */
    protected $xmlObj;

    /**
     * @param int $paySuppExtId
     */
    function __construct($paySuppExtId=null)
    {
        parent::__construct($paySuppExtId);
        $this->paySuppExtId = $paySuppExtId;
        $this->rootPathToReconcFiles = Yii::app()->config->get('PAY_PATH_RECONCILIATION') . 'ALLPAGO_'.$paySuppExtId.'/';
        return $this;
    }

    /**
     * @return bool
     */
    public function validateFile()
    {
        //@TODO: It should validate that is an XML valid document and something to verify that it is ffrom ALLPAGO
        //, for example SECURITY WORD.
//        $docXML = new DOMDocument();
//        $docXML->loadXML($this->xmlObj->asXML());
//        return $docXML->validate();
        return true;
    }

    /**
     * @return LogReconciliation[]
     */
    public function parseToModels()
    {
        $aMoLogReconc = array();
        $nodeNumber = 0;

        // TODO: Implement parseToModels() method.
        $this->numberTransactions = $this->xmlObj->xpath("/Response/Result/@count");
        $this->numberTransactions = intval($this->numberTransactions[0]);
        if ($this->numberTransactions==0){
            return $aMoLogReconc;
        }

        $aNodesXML = $this->xmlObj->xpath("/Response/Result"); ///Response/Result

        foreach($aNodesXML[0]->Transaction as $nodeTransXML){
            $nodeNumber = $nodeNumber +1 ;
            $aMoLogReconc[] = $this->parseSingleTransactionToModel($nodeTransXML, $nodeNumber);
        }

        return $aMoLogReconc;

    }

    /**
     * @param mixed $transaction
     * @param integer $lineNumber
     * @return LogReconciliation
     *
     */
    protected function parseSingleTransactionToModel($transaction, $lineNumber)
    {
        $moLogReconc = $this->initLogReconc();
        $moLogReconc->fileOrigin = "CampusCron_AllPago";

        $moLogReconc->refPaySuppOrderId = strval($transaction[0]->Identification->TransactionID[0]);
        $moLogReconc->refPaySuppExtUniId = strval($transaction[0]->Identification->UniqueID[0]);
        $moLogReconc->refAmountPrice = abs(floatval( $transaction[0]->Payment->Presentation->Amount[0]));
        $moLogReconc->refCurrencyTrans = strval($transaction[0]->Payment->Presentation->Currency[0]);
        $moLogReconc->paySuppExtId = $this->paySuppExtId;
        if ($moLogReconc->refCurrencyTrans == 'MXN'){
            $moLogReconc->paySuppExtId = PAY_SUPPLIER_ALLPAGO_MX;
        }
        $dateEndTrans = strval($transaction[0]->Payment->Clearing->FxDate[0]);
        if(HeDate::validDateSQL($dateEndTrans,true)){
            $moLogReconc->refDateEndTransaction = HeDate::changeFromUTCTo( $dateEndTrans );
        }

        $code = $transaction[0]->Processing[0]->xpath("@code");
        $code = strval($code[0]->code);
        $parseStatus = substr(trim(strval($code)),0,5);
        switch ($parseStatus) {
            case 'CC.DB':
                $moLogReconc->refPaySuppStatus = PAY_SUCCESS;
                break;
            case 'CC.RF':
                $moLogReconc->refPaySuppStatus = PAY_REFUND;
                break;
            case 'CC.RG':
                $moLogReconc->refPaySuppStatus = "REGISTRATION";
                break;
            case 'PP.RC': //Boleto
                $moLogReconc->refPaySuppExtUniId = strval($transaction[0]->Identification->ReferenceID[0]);
                $moLogReconc->refPaySuppStatus = PAY_SUCCESS;
                if ($moLogReconc->refCurrencyTrans == 'MXN') {
                    $moLogReconc->paySuppExtId = PAY_SUPPLIER_ALLPAGO_MX_OXO;
                }
                break;
        }
        // Let's check if there are any confirmations that are not SUCCESSFUL.
        $code = $transaction[0]->Processing[0]->xpath("@code");
        $code = strval($code[0]->code);
        $parseCodeStatus = substr(trim(strval($code)), 6, 2);
        if (intval($parseCodeStatus)!==90) {
            HeLogger::sendLog(
                HeLogger::PREFIX_ERR_UNKNOWN_H.' Reconciliation AllPago'.
                $this->paySuppExtId. ', confirmation NOT SUCCESSFUL',
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                'The payment with PaySuppOrderId='. $moLogReconc->refPaySuppOrderId. ' has not been successfully '.
                'confirmed. It might mean that the money has not reached ALLPAGO Bank account?'
            );
        }

        $moLogReconc->lineNode = $lineNumber;
        $moLogReconc->insertRowBeforeMatch() ;
        return $moLogReconc;
    }

    /**
     * Depending on the object passed by argument we save in one way or another.
     * @param SimpleXMLElement $file
     *
     * @return bool
     */
    protected function saveFileToDisk($file)
    {
        $fileName = $this->paySuppExtId.'_'.HeDate::todayNoSeparators(true)
            .'_'.'CronRequest.xml';
        $this->fileName = $fileName;
        $tempPath = $this->rootPathToReconcFiles.$fileName;

        $this->xmlObj = new SimpleXMLElement($file->asXML());

        $isSaved = $file->saveXML($tempPath);
        if($isSaved){
            return $tempPath;
        } else{
            return false;
        }

        return false;
    }
}