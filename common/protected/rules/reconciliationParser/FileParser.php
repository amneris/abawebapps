<?php
/**
 * User: Quino
 * Date: 17/02/2014
 * Time: 17:00
 * To change this template use File | Settings | File Templates.
 */
abstract class FileParser
{
    protected $paySuppExtId;
    protected $fileName;
    protected $filePath;
    protected $subDirOut = 'OUT/';
    protected $errorMsg;
    protected $numberTransactions;

    /**
     * @param integer $paySuppExtId
     */
    function __construct($paySuppExtId)
    {
        $this->rootPathToReconcFiles = Yii::app()->config->get('PAY_PATH_RECONCILIATION');
        $this->paySuppExtId = $paySuppExtId;
        return $this;
    }


    /**
     * @return bool
     */
    abstract public function validateFile();

    /**
     * @return LogReconciliation[]
     */
    abstract public function parseToModels();

    /**
     * @param mixed $transaction
     * @param integer $lineNumber
     * @return LogReconciliation
     *
     */
    abstract protected function parseSingleTransactionToModel($transaction, $lineNumber);

    /**
     * Depending on the object passed by argument we save in one way or another.
     * @param CUploadedFile|CGettextFile $file
     *
     * @return bool
     */
    abstract protected function saveFileToDisk($file);

    /**
     * @param CUploadedFile|SimpleXMLElement $file
     * @return bool|string
     */
    public function saveFile( $file ){
        if (!file_exists($this->rootPathToReconcFiles)){
            mkdir($this->rootPathToReconcFiles);
        }
        if (!file_exists($this->rootPathToReconcFiles.$this->subDirOut)){
            mkdir($this->rootPathToReconcFiles.$this->subDirOut);
        }

        $tempPath = $this->saveFileToDisk($file);
        if(!$tempPath){
            return false;
        }

        $fileName = basename($tempPath);
        if (!copy($tempPath, $this->rootPathToReconcFiles.$this->subDirOut.$fileName)){
            $this->errorMsg = "Copying file to OUT subfolder did not work.";
            return false;
        }

        $this->filePath = $this->rootPathToReconcFiles.$this->subDirOut.$fileName;

        return true;
    }

    /**
     * @return string
     */
    public function getErrorMsg()
    {
        return $this->errorMsg;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     *
     * @return string
     */
    public function getFilePath($fileName='')
    {
        if($fileName!=''){
            $this->fileName =$fileName;
            $this->filePath = $this->rootPathToReconcFiles.$this->subDirOut.$fileName.'.out';
        }

        $tempPath = $this->filePath;
        if( file_exists( $tempPath ) ){
            return $tempPath;
        } else{
            return false;
        }
    }

    /**
     * @return LogReconciliation
     */
    protected function initLogReconc()
    {
        $moLogReconc = new LogReconciliation();
        $moLogReconc->filePath = $this->filePath;
        $moLogReconc->paySuppExtId = $this->paySuppExtId;
        $moLogReconc->dateOperation = HeDate::todaySQL(true);

        return $moLogReconc;
    }

}
