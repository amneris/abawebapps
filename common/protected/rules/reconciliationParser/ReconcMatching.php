<?php

/**
 * Created by Joaquim Forcada (Quino) in Aba English
 * User: Joaquim Forcada (Quino)
 * Date: 20/02/14
 * Time: 15:59
 * ---Please brief description here----
 * Processes all entries in log_reconciliation and links them with table payments.
 * Depending on the matching of payments it will stamp MATCH, NOT FOUND or INCONSISTENT as a return value and will
 * set paySuppStatus to MATCHED
 */
abstract class ReconcMatching
{
    /*@var $paySuppExtId integer */
    protected $paySuppExtId;
    protected $fieldsNotMatched = array();
    protected $aMoLogsErrors = array();

    function __construct($paySuppExtId)
    {
        $this->paySuppExtId = $paySuppExtId;
    }


    /**
     * @param LogReconciliation[] $aMoLogsReconcs
     * @param string $filePath
     *
     * @return bool
     */
    public function processPackTransactions(array $aMoLogsReconcs, $filePath)
    {
        $allSuccess = true;
        $aMoLogsErrors = array();
        $aMoLogsSuccess = array();
        /* @var $moLogReconc LogReconciliation */
        foreach ($aMoLogsReconcs as $moLogReconc) {
            $moLogReconc->success = FALSE;
            /* @var $moPayment ReconcPayment */
            $moPayment = $this->findAndLinkByPaySuppOrderId($moLogReconc->refPaySuppOrderId, $moLogReconc);
            if ($moPayment) {
                // If found, we link them automatically on the log reconciliation side:
                $moLogReconc->idPayment = $moPayment->id;
                $moLogReconc->matchType = $this->checkConsistencyFields($moLogReconc, $moPayment);

                if ($moLogReconc->matchType == PAY_FILE_STATUS_MATCHED) {
                    $moLogReconc->success = TRUE;
                    if (intval($moPayment->paySuppLinkStatus) == PAY_RECONC_ST_MATCHED) {
                        $moLogReconc->matchType = PAY_FILE_STATUS_PREV_MATCHED;
                    } else {
                        $moPayment->markAsReconciled();
                    }
                } else {
                    // FOUND AND NOT MATCHED
                    if ($moLogReconc->refPaySuppStatus == PAY_REFUND) {
                        $moPayPartialSuccess = $this->isCasePartialRefund($moLogReconc, $moPayment);
                        if ($moPayPartialSuccess) {
                            $moLogReconc->success = TRUE;
                            $moLogReconc->matchType = PAY_FILE_STATUS_MATCHED;
                            $moPayment->markAsReconciled($moLogReconc->refPaySuppExtUniId);
                            $moPayPartialSuccess->markAsReconciled($moLogReconc->refPaySuppExtUniId);
                        }
                    }
                }
            } else {
                $moLogReconc->success = FALSE;
                $moLogReconc->matchType = PAY_FILE_STATUS_NOTFOUND;
            }

            if ($moLogReconc->success != TRUE) {
                $allSuccess = false;
                $aMoLogsErrors[] = $moLogReconc;
            } else {
                $aMoLogsSuccess[] = $moLogReconc;
            }

            $moLogReconc->update(array("idPayment", "matchType", "success"));
        }


        $this->aMoLogsErrors = $aMoLogsErrors;
        if (!$allSuccess) {
            $this->sendSummaryErrors($aMoLogsErrors, $filePath);
        } else {
            $this->sendSummarySuccess($aMoLogsSuccess);
        }

        return $allSuccess;
    }

    /**
     * @param $paySuppOrderId
     * @param LogReconciliation $moLogReconciliation
     *
     * @return ReconcPayment|bool
     */
    protected function findAndLinkByPaySuppOrderId($paySuppOrderId, LogReconciliation $moLogReconciliation)
    {
        $moPayment = new ReconcPayment();

        #4657
        if(isset($moLogReconciliation->paySuppExtId) AND $moLogReconciliation->paySuppExtId == PAY_SUPPLIER_CAIXA) {
            if ($moPayment->getPaymentByPaySuppOrderIdForReconciliation($paySuppOrderId, $moLogReconciliation->refPaySuppStatus)) {
                return $moPayment;
            } else {
                return $this->findPaymentSpecificGateway($moLogReconciliation);
            }
        }
        else {
            if ($moPayment->getPaymentByPaySuppOrderId($paySuppOrderId, $moLogReconciliation->refPaySuppStatus)) {
                return $moPayment;
            } else {
                return $this->findPaymentSpecificGateway($moLogReconciliation);
            }
        }
    }

    /**
     * @param LogReconciliation $moLogReconciliation
     * @return bool|ReconcPayment
     */
    protected abstract function findPaymentSpecificGateway(LogReconciliation $moLogReconciliation);

    /**
     * @param LogReconciliation $moLogReconciliation
     * @param ReconcPayment $moPayment
     *
     * @return string
     */
    protected function checkConsistencyFields(LogReconciliation $moLogReconciliation, ReconcPayment $moPayment)
    {
        $aFieldsNotMatched = array();
        $retPaySuppLinkStatus = NULL;

        $delimiter = "(File) / ";
        $postDelimiter = "(Intranet) <br/>";

        if ($moLogReconciliation->refPaySuppStatus !== $moPayment->status) {
            $aFieldsNotMatched["status"] = $moLogReconciliation->refPaySuppStatus . $delimiter .
                $moPayment->status . $postDelimiter;
        }

        if (floatval($moLogReconciliation->refAmountPrice) !== floatval($moPayment->amountPrice)) {
            $aFieldsNotMatched["amountPrice"] = $moLogReconciliation->refAmountPrice . $delimiter .
                $moPayment->amountPrice . $postDelimiter;
        }

        if ($moLogReconciliation->refCurrencyTrans !== $moPayment->currencyTrans) {
            $aFieldsNotMatched["currencyTrans"] = $moLogReconciliation->refCurrencyTrans . $delimiter .
                $moPayment->currencyTrans . $postDelimiter;
        }

        if($moLogReconciliation->paySuppExtId == PAY_SUPPLIER_ADYEN OR $moLogReconciliation->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP) {
            $diffMinutes = HeDate::getDifferenceDateSQL(HeDate::removeTimeFromSQL($moLogReconciliation->refDateEndTransaction) . " 00:00:00", HeDate::removeTimeFromSQL($moPayment->dateEndTransaction) . " 00:00:00", HeDate::DAY_SECS);
        }
        else {
            $diffMinutes = HeDate::getDifferenceDateSQL($moLogReconciliation->refDateEndTransaction, $moPayment->dateEndTransaction, HeDate::MINUTES);
        }

        if (abs($diffMinutes) > PAY_THRESHOLD_RECONC_PAYMENTS) {
            $aFieldsNotMatched["dateEndTransaction"] = $moLogReconciliation->refDateEndTransaction . $delimiter .
                $moPayment->dateEndTransaction . $postDelimiter;
        }

        if ($moLogReconciliation->refPaySuppExtUniId !== $moPayment->paySuppExtUniId) {
            $aFieldsNotMatched["paySuppExtUniId"] = $moLogReconciliation->refPaySuppExtUniId . $delimiter .
                $moPayment->paySuppExtUniId . $postDelimiter;
        }

        if (intval($moLogReconciliation->paySuppExtId) !== intval($moPayment->paySuppExtId)) {
            $aFieldsNotMatched["paySuppExtId"] = $moLogReconciliation->paySuppExtId . $delimiter .
                $moPayment->paySuppExtId . $postDelimiter;
        }

        if (count($aFieldsNotMatched) > 0) {
            $retPaySuppLinkStatus = PAY_FILE_STATUS_INCONSISTENT;
        } else {
            $retPaySuppLinkStatus = PAY_FILE_STATUS_MATCHED;
        }

        $this->fieldsNotMatched[$moLogReconciliation->id] = $aFieldsNotMatched;
        return $retPaySuppLinkStatus;
    }

    /**
     * @param LogReconciliation[] $aMoLogsErrors
     *
     * @param null $filePath
     * @return bool
     */
    protected function sendSummaryErrors(array $aMoLogsErrors, $filePath = null)
    {
        $supplier = ($this->paySuppExtId == PAY_SUPPLIER_CAIXA) ? 'CAIXA' : 'ALLPAGO-BR/ALLPAGO-MX';
        $supplier = ($this->paySuppExtId == PAY_SUPPLIER_ALLPAGO_BR_BOL) ? 'BOLETOS BRAZIL' : $supplier;
        $supplier = ($this->paySuppExtId == PAY_SUPPLIER_ADYEN) ? 'ADYEN' : $supplier;
        $supplier = ($this->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP) ? 'ADYEN HPP' : $supplier;

        $aReconcErrors =        array();
        $aSummReconcErrors =    array();
        foreach ($aMoLogsErrors as $moLogReconc) {
            $moLogReconc->refresh();
            $aReconcErrors["PaySupplier"] =             $moLogReconc->paySuppExtId;
            $aReconcErrors["TheirFile"] =               basename($moLogReconc->filePath);
            $aReconcErrors["TheirLineNumber"] =         $moLogReconc->lineNode;
            $aReconcErrors["TheirIdentifier"] =         $moLogReconc->refPaySuppExtUniId;
            $aReconcErrors["TheirCurrency"] =           $moLogReconc->refCurrencyTrans;
            $aReconcErrors["TheirDateEndTransaction"] = $moLogReconc->refDateEndTransaction;
            $aReconcErrors["OurPaySuppOrderId"] =       $moLogReconc->refPaySuppOrderId;
            $aReconcErrors["ReconciliationStatus"] =    $moLogReconc->matchType;
            $inconsistencyLine = "";
            if (array_key_exists($moLogReconc->id, $this->fieldsNotMatched)) {
                $inconsistencyLine = "Fields inconsistents: ";
                foreach ($this->fieldsNotMatched[$moLogReconc->id] as $nameField => $aFieldNotMatched) {
                    $inconsistencyLine = $inconsistencyLine . "<b>" . strtoupper($nameField) . "</b> = " . $aFieldNotMatched . "";
                }
            }
            $aReconcErrors["FieldsUnmatched"] = $inconsistencyLine;
            $aSummReconcErrors[] =              $aReconcErrors;
        }

        $bodyDesc = "Below you can see the list of transactions downloaded from $supplier and NOT correctly matched" .
            " Please review them, try to find them within our system." .
            " In case of INCONSISTENCY try to decide if you want to  mark them as a reconciled or not. " .
            "It depends entirely on your criteria. Remember you have always the log of this operation for this file
            in the database.<br/>";

        //
        //#4400
        $bodyDescErrors = "<br />";
        foreach($aSummReconcErrors as $iKey => $aSummReconcError) {
            foreach($aSummReconcError as $iErrKey => $aErrMsg) {
                $bodyDescErrors .= "[" . $iErrKey . "=>" . $aErrMsg . "],  ";
            }
            $bodyDescErrors .= "<br /><br />";
        }
        $bodyDesc .= "<br /><br />" . $bodyDescErrors;
        //

        HeLogger::sendLog(HeAbaMail::PREFIX_NOTIF_INTRA . ":RECONCILIATION PROCESS FROM $supplier ON " . HeDate::todaySQL(true) . ", Errors list summary to be reviewed.", HeLogger::FINANCE,
          HeLogger::CRITICAL, $bodyDesc);

        return true;
    }

    /**
     * @param LogReconciliation[] $aMoLogsSuccess
     *
     * @param null $filePath
     * @return bool
     */
    private function sendSummarySuccess(array $aMoLogsSuccess, $filePath = null)
    {
        $supplier = ($this->paySuppExtId == PAY_SUPPLIER_CAIXA) ? 'CAIXA' : 'ALLPAGO-BR/ALLPAGO-MX';
        $supplier = ($this->paySuppExtId == PAY_SUPPLIER_ALLPAGO_BR_BOL) ? 'BOLETOS BRAZIL' : $supplier;
        $supplier = ($this->paySuppExtId == PAY_SUPPLIER_ADYEN) ? 'ADYEN' : $supplier;
        $supplier = ($this->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP) ? 'ADYEN HPP' : $supplier;

        $aReconcSuccess =       array();
        $aSummReconcSuccess =   array();
        foreach ($aMoLogsSuccess as $moLogReconc) {
            $aReconcSuccess["Pay_Supplier"] =               $moLogReconc->paySuppExtId;
            $aReconcSuccess["Their_File"] =                 basename($moLogReconc->filePath);
            $aReconcSuccess["TheirLine_Number"] =           $moLogReconc->lineNode;
            $aReconcSuccess["TheirIdentifier"] =            $moLogReconc->refPaySuppExtUniId;
            $aReconcSuccess["TheirDateEndTransaction"] =    $moLogReconc->refDateEndTransaction;
            $aReconcSuccess["OurPaySuppOrderId"] =          $moLogReconc->refPaySuppOrderId;
            $aReconcSuccess["ReconciliationStatus"] =       $moLogReconc->matchType;
            $aSummReconcSuccess[] =                         $aReconcSuccess;
        }

        $bodyDesc = " All transactions have been successfully matched. Hope everyday was that way. Great!!";

        //
        //#4400
        $bodyDescErrors = "<br />";
        foreach($aSummReconcSuccess as $iKey => $aSummReconcSucces) {
            foreach($aSummReconcSucces as $iSucKey => $aSucMsg) {
                $bodyDescErrors .= "[" . $iSucKey . "=>" . $aSucMsg . "],  ";
            }
            $bodyDescErrors .= "<br /><br />";
        }
        $bodyDesc .= "<br /><br />" . $bodyDescErrors;
        //

        HeLogger::sendLog(HeAbaMail::PREFIX_NOTIF_INTRA . ": RECONCILIATION PROCESS FROM $supplier ON " . HeDate::todaySQL(true) .
          " ALL LINES successfully processed.", HeLogger::FINANCE, HeLogger::CRITICAL, $bodyDesc);

        return true;
    }

    /**
     * @return array
     */
    public function getAMoLogsErrors()
    {
        return $this->aMoLogsErrors;
    }

    /**
     * @param LogReconciliation $moLogReconc
     * @param ReconcPayment $moPayment
     *
     * @return bool|ReconcPayment
     */
    protected function isCasePartialRefund(LogReconciliation $moLogReconc, ReconcPayment $moPayment)
    {
        $moPayPartialSuccess = new ReconcPayment();
        if (!$moPayPartialSuccess->getPaymentSuccessPartial($moLogReconc->refPaySuppOrderId,
            $moPayment->amountPrice, PAY_SUCCESS)
        ) {
            return false;
        }

        if ($moPayPartialSuccess->idPayControlCheck == $moPayment->idPayControlCheck) {
            $amountRefund = $moPayment->amountPrice - $moPayPartialSuccess->amountPrice;
            if ($amountRefund >= (floatval($moLogReconc->refAmountPrice) - 5) &&
                $amountRefund <= (floatval($moLogReconc->refAmountPrice) + 5)
            ) {
                return $moPayPartialSuccess;
            }
        }
        return false;
    }
}