<?php

class FileParserAdyen extends FileParser
{
    protected $csvPath;

    /**
     * @param int $paySuppExtId
     */
    function __construct($paySuppExtId=null)
    {
        parent::__construct($paySuppExtId);
        $this->paySuppExtId =           $paySuppExtId;
        $this->rootPathToReconcFiles =  Yii::app()->config->get('PAY_PATH_RECONCILIATION') . 'ADYEN_' . $paySuppExtId . '/';
        return $this;
    }

    protected $fileReport;

    /**
     * @param $fileFormat
     */
    public function setFileFormat($fileFormat) {
        $this->fileReport = $fileFormat;
    }

    /**
     * @return bool
     */
    public function validateFile()
    {
        //@TODO: It should validate that is an CSV valid document and something to verify that it is ffrom ADYEN
        //, for example SECURITY WORD.
//        $docXML = new DOMDocument();
//        $docXML->loadXML($this->xmlObj->asXML());
//        return $docXML->validate();
        return true;
    }

    /** Adyen fields definition
     *
     * I -     Integer (whole number)
     * FL –    Floating point number
     * F -     Fixed point number (for amounts, precision is based on the currency exponent)
     *
     * A -     Text feld (alpha characters only)
     * N -     Text feld (numeric characters only, treat as text)
     * AN -    Text feld (alphanumeric characters only)
     * C -     Text feld (unicode character set)
     *
     * D -     Date feld (Format: YYYY-MM-DD)
     * T -     Time feld (format: HH:MM:SS)
     */

    private static $receivedPaymentsReportFieldsDefinition = array
    (
        "COMPANY_ACCOUNT" =>                array(0, "Company Account",                     array("STRING", 80, "C X80"),       "The static name of your company account within the Adyen system."),
        "MERCHANT_ACCOUNT" =>               array(1, "Merchant Account",                    array("STRING", 80, "C X80"),       "The static name of the merchant account that was used to process the original payment request. The report may contain line items for multiple merchant accounts."),
        "PSP_REFERENCE" =>                  array(2, "Psp Reference",                       array("INTEGER", 16, "N 16"),       "This is Adyen's 16 digit unique reference number for the payment; it denotes the PSP reference for the original authorisation request."),
        "MERCHANT_REFERENCE" =>             array(3, "Merchant Reference",                  array("STRING", 80, "C X80"),       "This is the reference number that was provided when initiating the payment request."),
        "PAYMENT_METHOD" =>                 array(4, "Payment Method",                      array("STRING", 30, "C X30"),       "The payment method type of the payment which was processed. For example: visa, mc, amex."),
        "CREATION_DATE" =>                  array(5, "Creation Date",                       array("DATETIME", 80, "D+' '+T"),   "The timestamp of when the payment was created as entered into Adyen's system. The format is YYYY-MM-DD HH:MM:SS, for example: 2012-07-26 19:58:55."),
        "TIMEZONE" =>                       array(6, "TimeZone",                            array("STRING", 4, "A 4"),          "The time zone of the previous column. This is the ISO Code for the timezone which is set up on your company account: Customer Area >> Settings >> Company Settings >> Time Zone (BST, CET, CEST etc)"),
        "CURRENCY" =>                       array(7, "Currency",                            array("STRING", 3, "A 3"),          "This is the three character ISO code for the payment currency."),
        "AMOUNT" =>                         array(8, "Amount",                              array("INTEGER", 16, "N 16"),       "The amount of the payment (authorisation amount)"),
        "TYPE" =>                           array(9, "Type",                                array("STRING", 50, "C X50"),       "The record type, for this report alway the value 'Payment'... The possible values are:, Fee*, MiscCosts*VMerchantPayout*, Refunded, Settled, Chargeback, ChargebackReversed, RefundedReversed, DepositCorrection*, InvoiceDeduction*, MatchedStatement, ManualCorrected, AuthorisationSchemeFee*, BankInstructionReturned, InternalCompanyPayoutVEpaPaid, BalanceTransfer, PaymentCost, SettleCost"),
        "RISK_SCORING" =>                   array(10, "Risk Scoring",                       array("INTEGER", 1, "I"),           "The total risk scoring value of the payment"),
        "SHOPPER_INTERFACE" =>              array(11, "Shopper Interaction",                array("STRING", 30, "A 30"),        "The transaction type (values: Ecommerce, ContAuth, POS, Moto)"),
        "SHOPPER_NAME" =>                   array(12, "Shopper Name",                       array("STRING", 80, "C X80"),       "The name of the shopper/consumer as provided with the transaction (if available)"),
        "SHOPPER_PAN" =>                    array(13, "Shopper PAN",                        array("STRING", 80, "AN X80"),      "The account number of the shopper/consumer. In case of card payments, the last four digits of the card number. For SEPA this feld show the IBAN and BIC in the following format: IBAN/BIC."),
        "SHOPPER_IP" =>                     array(14, "Shopper IP",                         array("STRING", 50, "C X50"),       "The IP address (IPV4 or IPV6) of the shopper/consumer as provided during the original transaction (if available)"),
        "SHOPPER_COUNTRY" =>                array(15, "Shopper Country",                    array("STRING", 2, "A 2"),          "The ISO country code of the shopper (if available, calculated from IP if not supplied)"),
        "ISSUER_NAME" =>                    array(16, "Issuer Name",                        array("STRING", 256, "C X256"),     "The name of the issuing bank (if available)"),
        "ISSUER_ID" =>                      array(17, "Issuer ID",                          array("STRING", 30, "AN 30"),       "The unique identifer of the issuer. In case of card payments this will be the issuer BIN (frst 6 digits of the card number)."),
        "ISSUER_CITY" =>                    array(18, "Issuer City",                        array("STRING", 80, "C X80"),       "The city of the issuer (if available)"),
        "ISSUER_COUNTRY" =>                 array(19, "Issuer Country",                     array("STRING", 2, "A 2"),          "The ISO country code of the issuer"),
        "ACQUIER_RESPONSE" =>               array(20, "Acquirer Response",                  array("STRING", 30, "A X30"),       "The normalised response from the acquirer. Not necessarily the final status of the payment."),
        "AUTHORIZATION_CODE" =>             array(21, "Authorisation Code",                 array("STRING", 50, "C X50"),       "If the payment was not declined, the authorisation code."),
        "SHOPPER_EMAIL" =>                  array(22, "Shopper Email",                      array("STRING", 256, "C X256"),     "The email address of the shopper/consumer as provided during the original transaction (if available)"),
        "SHOPPER_REFERNCE" =>               array(23, "Shopper Reference",                  array("STRING", 256, "C X256"),     "The supplied customer id/reference as supplied in the original transaction"),
        "3D_DIRECTORY_RESPONSE" =>          array(24, "3D Directory Response",              array("STRING", 1, "A 1"),          "For payments which were processed using 3D Secure (Secure Code / Verifed by Visa), the directory response (values: Y,N,U,E)"),
        "3D_AUTHENTICATION_RESPONSE" =>     array(25, "3D Authentication Response",         array("STRING", 1, "A 1"),          "For payments which were processed using 3D Secure (Secure Code / Verifed by Visa) and had directory response 'Y', the authenticationresponse (values: Y,N,U,A,E)"),
        "CVC2_RESPONSE" =>                  array(26, "CVC2 Response",                      array("INTEGER", 1, "N 1"),         "The result passed back by the issuer after verifying the CVC2 (or CVV2/CID in case of Visa/Amex)."),
        "AVC_RESPONSE" =>                   array(27, "AVS Response",                       array("INTEGER", 2, "N 2"),         "If AVS (Address Verifcation) was performed, the result of the AVS check."),
        "BILLING_STREET" =>                 array(28, "Billing Street",                     array("STRING", 256, "C X256"),     "The street part of the billing address"),
        "BILLING_HOUSE_NUMBER_NAME" =>      array(29, "Billing House Number / Name",        array("STRING", 256, "C X256"),     "The house number or name part of the billing address"),
        "BILLING_CITY" =>                   array(30, "Billing City",                       array("STRING", 256, "C X256"),     "The city part of the billing address"),
        "BILLING_POSTAL_CODE_ZIP_CODE" =>   array(31, "Billing Postal Code / Zip Code",     array("STRING", 30, "C X30"),       "The zip code or postal code part of the billing address"),
        "BILLING_STATE_PROVINCE" =>         array(32, "Billing State / Province",           array("STRING", 256, "C X256"),     "The state or province of the billing address"),
        "BILLING_COUNTRY" =>                array(33, "Billing Country",                    array("STRING", 2, "A 2"),          "The ISO country code of the of the billing address"),
        "DELIVERY_STREET" =>                array(34, "Delivery Street",                    array("STRING", 256, "C X256"),     "The street part of the delivery address"),
        "DELIVERY_HOUSE_NUMBER_NAME" =>     array(35, "Delivery House Number / Name",       array("STRING", 256, "C X256"),     "The house number or name part of the delivery address"),
        "DELIVERY_CITY" =>                  array(36, "Delivery City",                      array("STRING", 256, "C X256"),     "The city part of the delivery address"),
        "DELIVERY_POSTAL_CODE_ZIP_CODE" =>  array(37, "Delivery Postal Code / Zip Code",    array("STRING", 30, "C X30"),       "The zip code or postal code part of the delivery address"),
        "DELIVERY_STATE_PROVINCE" =>        array(38, "Delivery State / Province",          array("STRING", 256, "C X256"),     "The state or province of the delivery address"),
        "DELIVERY_COUNTRY" =>               array(39, "Delivery Country",                   array("STRING", 2, "A 2"),          "The ISO country code of the of the delivery address"),
        "ACQUIRER_REFERENCE" =>             array(40, "Acquirer Reference",                 array("STRING", 256, "C X256"),     "Reference number for the transaction provided by the acquirer"),
        "PAYMENT_METHOD_VARIANT" =>         array(41, "Payment Method Variant",             array("STRING", 30, "C X30"),       "The payment method sub-brand, or the main payment method if not available (e.g. mccredit or visagold)"),
        "RAW_ACQUIRER_RESPONSE" =>          array(42, "Raw acquirer response",              array("STRING", 30, "C X30"),       ""),
        "RESERVED_4" =>                     array(43, "Reserved 4",                         array("STRING", 0, ""),             "Reserved for future enhancements"),
        "RESERVED_5" =>                     array(44, "Reserved 5",                         array("STRING", 0, ""),             "Reserved for future enhancements"),
        "RESERVED_6" =>                     array(45, "Reserved 6",                         array("STRING", 0, ""),             "Reserved for future enhancements"),
        "RESERVED_7" =>                     array(46, "Reserved 7",                         array("STRING", 0, ""),             "Reserved for future enhancements"),
        "RESERVED_8" =>                     array(47, "Reserved 8",                         array("STRING", 0, ""),             "Reserved for future enhancements"),
        "RESERVED_9" =>                     array(48, "Reserved 9",                         array("STRING", 0, ""),             "Reserved for future enhancements"),
        "RESERVED_10" =>                    array(49, "Reserved 10",                        array("STRING", 0, ""),             "Reserved for future enhancements"),
    );

    private static $paymentsAccountingReportFieldsDefinition = array
    (
        "COMPANY_ACCOUNT" =>                    array(0, "Company Account",                     array("STRING", 80, "C X80"),       "The static name of your company account within the Adyen system."),
        "MERCHANT_ACCOUNT" =>                   array(1, "Merchant Account",                    array("STRING", 80, "C X80"),       "The static name of the merchant account that was used to process the original payment request. The report may contain line items for multiple merchant accounts."),
        "PSP_REFERENCE" =>                      array(2, "Psp Reference",                       array("INTEGER", 16, "N 16"),       "This is Adyen's 16 digit unique reference number for the payment; it denotes the PSP reference for the original authorisation request."),
        "MERCHANT_REFERENCE" =>                 array(3, "Merchant Reference",                  array("STRING", 80, "C X80"),       "This is the reference number that was provided when initiating the payment request."),
        "PAYMENT_METHOD" =>                     array(4, "Payment Method",                      array("STRING", 30, "C X30"),       "The payment method type of the payment which was processed. For example: visa, mc, amex."),
        "BOOKING_DATE" =>                       array(5, "Booking Date",                        array("DATETIME", 80, "D+' '+T"),   "The timestamp of when the event took place as entered into Adyen's accounting system. The format is YYYY-MM-DD HH:MM:SS, for example: 2012-07-26 19:58:55."),
        "TIMEZONE" =>                           array(6, "TimeZone",                            array("STRING", 4, "A 4"),          "The time zone of the previous column. This is the ISO Code for the timezone which is set up on your company account: Customer Area >> Settings >> Company Settings >> Time Zone (BST, CET, CEST etc)"),
        "MAIN_CURRENCY" =>                      array(7, "Main Currency",                       array("STRING", 3, "A 3"),          "This is the three character ISO code for the payment currency."),
        "MAIN_AMOUNT" =>                        array(8, "Main Amount",                         array("INTEGER", 16, "N 16"),       "The amount of the payment (authorisation amount)"),
        "RECORD_TYPE" =>                        array(9, "Record Type",                         array("STRING", 50, "C X50"),       "The accounting record type, e.g.: Received, Authorised, Refused, SentForSettle, Settled, SentForRefund, Refunded"),
        "PAYMENT_CURRENCY" =>                   array(10, "Payment Currency",                   array("STRING", 3, "A 3"),          "The three character ISO code for the currency which was used for processing the payment."),
        "RECEIVED_PC" =>                        array(11, "Received (PC)",                      array("FLOAT", 16, "F"),            "The amount (in Payment Currency) debited or credited on the Received accounting register."),
        "AUTHORISED_PC" =>                      array(12, "Authorised (PC)",                    array("FLOAT", 16, "F"),            "The amount (in Payment Currency) debited or credited on the Authorised accounting register."),
        "CAPTURED_PC" =>                        array(13, "Captured (PC)",                      array("FLOAT", 16, "F"),            "The amount (in Payment Currency) debited or credited on the Captured accounting register."),
        "SETTLEMENT_CURRENCY" =>                array(14, "Settlement Currency",                array("STRING", 3, "A 3"),          "The three character ISO code for the currency which was used when settling the payment."),
        "PAYABLE_SC" =>                         array(15, "Payable (SC)",                       array("FLOAT", 16, "F"),            "The amount (in Settlement Currency) debited or credited on the Payable accounting register (i.e. funds which will be paid out to your bank account)."),
        "COMISSION_PC" =>                       array(16, "Commission (SC)",                    array("FLOAT", 16, "F"),            "The commission fee that was withheld by the acquirer (in Settlement Currency) If the Acquirer provides the transaction information at the interchange level Adyen will split the commission into the Markup, Scheme Fees and Interchange registers instead."),
        "MARKUP_PC" =>                          array(17, "Markup (SC)",                        array("FLOAT", 16, "F"),            "The fee charged by the Acquiring bank (in Settlement Currency)."),
        "SCHEME_FEES_PC" =>                     array(18, "Scheme Fees (SC)",                   array("FLOAT", 16, "F"),            "The fee which is charged by e.g. Visa / MC (in Settlement Currency)."),
        "REINTERCHANGE_PC" =>                   array(19, "Interchange (SC)",                   array("FLOAT", 16, "F"),            "The fee charged by the Issuing bank (in Settlement Currency)."),
        "PROCESSING_FEE_CURRENCY" =>            array(20, "Processing Fee Currency",            array("STRING", 3, "A 3"),          "The currency in which the processing (or gateway) fee is charged."),
        "PROCESSING_FEE_FC" =>                  array(21, "Processing Fee (FC))",               array("FLOAT", 16, "F"),            "The amount (in Processing Fee Currency) charged on the Processing Fee accounting register."),
        "USER_NAME" =>                          array(22, "User Name",                          array("STRING", 80, "C X80"),       "The user (interactive or webservice user) who performed the action resulting in the accounting record. If the record was created by an automated internal process the value 'system'  is used."),
        "PAYMENT_METHOD_VARIANT" =>             array(23, "Payment Method Variant",             array("STRING", 30, "C X30"),       "The subbrand of the payment method (if applicable) For example: visaclassic, visadebit, mccorporate."),
        "MODIFICATION_MERCHANT_REFERENCE" =>    array(24, "Modifcation Merchant Reference",     array("STRING", 80, "C X80"),       "For modifcations like capture or refund, the (optional) modifcation reference supplied by the merchant."),
        "RESERVED_3" =>                         array(25, "Reserved 3",                         array("STRING", 0, ""),             "Reserved for future enhancements"),
        "RESERVED_4" =>                         array(26, "Reserved 4",                         array("STRING", 0, ""),             "Reserved for future enhancements"),
        "RESERVED_5" =>                         array(27, "Reserved 5",                         array("STRING", 0, ""),             "Reserved for future enhancements"),
        "RESERVED_6" =>                         array(28, "Reserved 6",                         array("STRING", 0, ""),             "Reserved for future enhancements"),
        "RESERVED_7" =>                         array(29, "Reserved 7",                         array("STRING", 0, ""),             "Reserved for future enhancements"),
        "RESERVED_8" =>                         array(30, "Reserved 8",                         array("STRING", 0, ""),             "Reserved for future enhancements"),
        "RESERVED_9" =>                         array(31, "Reserved 9",                         array("STRING", 0, ""),             "Reserved for future enhancements"),
        "RESERVED_10" =>                        array(32, "Reserved 10",                        array("STRING", 0, ""),             "Reserved for future enhancements"),
    );


    /**
     * @return LogReconciliation[]
     */
    public function parseToModels()
    {
        $aMoLogReconc = array();
        $iRowCounter =  0;

        if (($oFile = fopen($this->csvPath, "r")) !== false) {
            while (($oLine = fgetcsv($oFile, 1000, ",")) !== false) {
                if($iRowCounter > 0) {

                    $aMoLogReconcRow = $this->parseSingleTransactionToModel($oLine, $iRowCounter);

                    if($aMoLogReconcRow) {
                        $aMoLogReconc[] = $aMoLogReconcRow;
                    }

                }
                ++$iRowCounter;
            }
            fclose($oFile);
        }
        $this->numberTransactions = $iRowCounter;

        return $aMoLogReconc;
    }


    /**
     * @param mixed $transaction
     * @param integer $lineNumber
     * @return LogReconciliation
     *
     */
    protected function parseSingleTransactionToModel($transaction, $lineNumber)
    {
        switch($this->fileReport) {

            case PaySupplierAdyen::ADYEN_REPORT_TYPE_PAYMENTS_ACCOUNTING;

                $paymentStatus = trim($transaction[self::$paymentsAccountingReportFieldsDefinition['RECORD_TYPE'][0]]);

                switch($paymentStatus) {

                    /*
                        The accounting record type, e.g.:
                        Received
                        Authorised
                        Refused
                        SentForSettle
                        Settled
                        SentForRefund
                        Refunded
                    */

                    case PaySupplierAdyen::ADYEN_REPORT_PAYMENTS_ACCOUNTING_STATE_SUCCESS;  // SentForSettle
                    case PaySupplierAdyen::ADYEN_REPORT_PAYMENTS_ACCOUNTING_STATE_REFUND;   // SentForRefund

                        $moLogReconc =              $this->initLogReconc();
                        $moLogReconc->fileOrigin =  "CampusCron_Adyen";

                        $moLogReconc->refPaySuppStatus = PAY_SUCCESS;

                        if($paymentStatus == PaySupplierAdyen::ADYEN_REPORT_PAYMENTS_ACCOUNTING_STATE_REFUND) {
                            $moLogReconc->refPaySuppStatus = PAY_REFUND;
                        }

                        $moLogReconc->refPaySuppOrderId =   trim($transaction[self::$paymentsAccountingReportFieldsDefinition['MERCHANT_REFERENCE'][0]]);
                        $moLogReconc->refPaySuppExtUniId =  trim($transaction[self::$paymentsAccountingReportFieldsDefinition['PSP_REFERENCE'][0]]);
//                        $moLogReconc->refAmountPrice =      abs(floatval($transaction[self::$paymentsAccountingReportFieldsDefinition['MAIN_AMOUNT'][0]]));
                        $moLogReconc->refAmountPrice =      abs(floatval($transaction[self::$paymentsAccountingReportFieldsDefinition['CAPTURED_PC'][0]]));
                        $moLogReconc->refCurrencyTrans =    trim($transaction[self::$paymentsAccountingReportFieldsDefinition['PAYMENT_CURRENCY'][0]]);
                        $moLogReconc->paySuppExtId =        $this->paySuppExtId;

                        $dateEndTrans = trim($transaction[self::$paymentsAccountingReportFieldsDefinition['BOOKING_DATE'][0]]);
                        if(HeDate::validDateSQL($dateEndTrans, true)){
                            $moLogReconc->refDateEndTransaction = HeDate::changeFromUTCTo( $dateEndTrans );
                        }

                        $moLogReconc->lineNode = $lineNumber;
                        $moLogReconc->insertRowBeforeMatch() ;
                        return $moLogReconc;
                        break;
                }
                break;
        }

        return false;
    }

    /**
     * Depending on the object passed by argument we save in one way or another.
     * @param SimpleXMLElement $file
     *
     * @return bool
     */
    protected function saveFileToDisk($file)
    {
        $fileName = $this->paySuppExtId . '_' . HeDate::todayNoSeparators(true) . '_' . 'ReportRequest.csv';

        $this->fileName = $fileName;

        $tempPath = $this->rootPathToReconcFiles . $fileName;

//        rename ($file, $tempPath);
        copy($file, $tempPath);

        $this->csvPath = $tempPath;

        return $this->csvPath;
    }


}