<?php
/**
 * Created by Joaquim Forcada (Quino) in Aba English with PHP Storm
 * User: Joaquim Forcada (Quino)
 * Date: 20/02/14
 * Time: 17:28

Implementation for La Caixa
 */

class ReconcMatchingAllPagoBoletoBr extends ReconcMatchingAllPago
{

    /**
     * @param LogReconciliation[] $aMoLogsReconcs
     * @param string $filePath
     *
     * @return bool
     */
    public function processPackTransactions( array $aMoLogsReconcs, $filePath)
    {
        $allSuccess = parent::processPackTransactions($aMoLogsReconcs, $filePath);

        return $allSuccess;
    }

    /** Find Boletos by refPaySuppExtUniId or $draftIdPayment and changes status to SUCCESS.
     *
     * @param LogReconciliation[] $aMoLogsReconcs
     * @param string $filePath
     *
     * @return array
     */
    public function matchBoletosOnly( array $aMoLogsReconcs, $filePath)
    {
        $allSuccess = true;
        $aMoLogsErrors = array();
        $aMoLogsSuccess = array();
        $aMoBoletos = array();


        /* @var $moLogReconc LogReconciliation */
        foreach ($aMoLogsReconcs as $moLogReconc) {
            $bolFound = false;
            $draftIdPayment = substr($moLogReconc->refPaySuppOrderId, 0, strpos($moLogReconc->refPaySuppOrderId, ':'));
            $boleto = new AbaPaymentsBoleto();
            if ($boleto->getByPaySuppExtUnid($moLogReconc->refPaySuppExtUniId)) {
                $bolFound = true;
                $aMoLogsSuccess[$moLogReconc->refPaySuppOrderId] = $moLogReconc;
            } elseif ($boleto->getByIdPayment($draftIdPayment)) {
                $bolFound = true;
                $aMoLogsSuccess[$moLogReconc->refPaySuppOrderId] = $moLogReconc;
            } else {
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_LOGIC_H.' Boleto in Intranet not found '. $moLogReconc->refPaySuppExtUniId,
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    'Boleto confirmed by AllPago with UniqueId='.$moLogReconc->refPaySuppExtUniId.
                    ' and paySuppOrderId='.$moLogReconc->refPaySuppOrderId.'. Please review this boleto in payments_'.
                    'boleto table, and find the payment corresponding to this Boleto.'
                );
                $allSuccess= false;
                $aMoLogsErrors[$moLogReconc->refPaySuppOrderId] = $moLogReconc;
            }
            if ($bolFound) {
                if ($boleto->status==PAY_BOLETO_FAIL) {
                    $aMoLogsErrors[$moLogReconc->refPaySuppOrderId] = $moLogReconc;
                    // @TODO: What if the Boleto has expired? We should create a SUCCESS payment?
                    HeLogger::sendLog(
                      HeLogger::PREFIX_ERR_LOGIC_L.' Boleto already expired and failed.',
                        HeLogger::IT_BUGS,
                        HeLogger::CRITICAL,
                        'Boleto with unique id='.$boleto->paySuppExtUniId.' already expired and assumed FAILED.'
                    );
                } elseif ($boleto->status==PAY_BOLETO_SUCCESS) {
                    $aMoBoletos[$moLogReconc->refPaySuppOrderId] = $boleto;
                } else {
                    $aMoBoletos[$moLogReconc->refPaySuppOrderId] = $boleto;
                    $boleto->updateSuccess();
                }
            }
        }
        return array('allSuccess'=>$allSuccess, 'aMoLogsSuccess'=>$aMoLogsSuccess,
                    'aMoLogsErrors'=> $aMoLogsErrors, 'aMoBoletos'=>$aMoBoletos);
    }

    protected function checkConsistencyFields(LogReconciliation $moLogReconciliation, ReconcPayment $moPayment)
    {
        $aFieldsNotMatched = array();
        $retPaySuppLinkStatus = null;
        $delimiter = "(File) / ";
        $postDelimiter = "(Intranet) <br/>";
        if ($moLogReconciliation->refPaySuppStatus !== $moPayment->status) {
            $aFieldsNotMatched["status"] = $moLogReconciliation->refPaySuppStatus . $delimiter .
                $moPayment->status . $postDelimiter;
        }
        if (floatval($moLogReconciliation->refAmountPrice) !== floatval($moPayment->amountPrice)) {
            $aFieldsNotMatched["amountPrice"] = $moLogReconciliation->refAmountPrice . $delimiter .
                $moPayment->amountPrice . $postDelimiter;
        }
        if ($moLogReconciliation->refCurrencyTrans !== $moPayment->currencyTrans) {
            $aFieldsNotMatched["currencyTrans"] = $moLogReconciliation->refCurrencyTrans . $delimiter .
                $moPayment->currencyTrans . $postDelimiter;
        }
        if ($moLogReconciliation->refPaySuppExtUniId !== $moPayment->paySuppExtUniId) {
            $aFieldsNotMatched["paySuppExtUniId"] = $moLogReconciliation->refPaySuppExtUniId . $delimiter .
                $moPayment->paySuppExtUniId . $postDelimiter;
        }
        if (intval($moLogReconciliation->paySuppExtId) !== intval($moPayment->paySuppExtId)) {
            $aFieldsNotMatched["paySuppExtId"] = $moLogReconciliation->paySuppExtId . $delimiter .
                $moPayment->paySuppExtId . $postDelimiter;
        }
        if (count($aFieldsNotMatched) > 0) {
            $retPaySuppLinkStatus = PAY_FILE_STATUS_INCONSISTENT;
        } else {
            $retPaySuppLinkStatus = PAY_FILE_STATUS_MATCHED;
        }
        $this->fieldsNotMatched[$moLogReconciliation->id] = $aFieldsNotMatched;
        return $retPaySuppLinkStatus;
    }
}
