<?php
/**
 * Created by Joaquim Forcada (Quino) in Aba English with PHP Storm
 * User: Joaquim Forcada (Quino)
 * Date: 20/02/14
 * Time: 17:28

Implementation for La Caixa
 */

class ReconcMatchingLaCaixa extends ReconcMatching
{

    /**
     * @param LogReconciliation[] $aMoLogsReconcs
     * @param string $filePath
     *
     * @return bool
     */
    public function processPackTransactions(array $aMoLogsReconcs, $filePath)
    {
        $allSuccess = parent::processPackTransactions($aMoLogsReconcs, $filePath);

        $this->processResponseNewFile($aMoLogsReconcs, $filePath);

        return $allSuccess;
    }

    /**
     * @param array $aMoLogsReconcs
     * @param $filePath
     *
     * @return bool
     */
    protected function processResponseOriginalFile_20130225(array $aMoLogsReconcs, $filePath)
    {
        try {
            /* @var $aLogReconc LogReconciliation[] */
            foreach ($aMoLogsReconcs as $moLogReconc) {
                $aLogReconc[$moLogReconc->lineNode] = $moLogReconc;
            }

            $pReaderFile = fopen($filePath, "r");
            $pWriterFile = fopen($filePath . '.tmp2', "w");

            $lineIn = rtrim(fgets($pReaderFile, 4096)); // Line 1 is ignored
            fputs($pWriterFile, rtrim($lineIn) . PHP_EOL);
            $numberLines = 1;
            while (($lineIn = fgets($pReaderFile, 4096)) !== false) {
                $numberLines = $numberLines + 1;
                if (array_key_exists($numberLines, $aLogReconc)) {
                    fputs($pWriterFile, rtrim($lineIn) . ',' . $aLogReconc[$numberLines]->matchType . PHP_EOL);
                } else {
                    fputs($pWriterFile, rtrim($lineIn) . PHP_EOL);
                }
            }

            fclose($pReaderFile);
            fclose($pWriterFile);

            rename($filePath . '.tmp2', $filePath);
        } catch (Exception $e) {
            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_UNKNOWN_H . ": Processing file response of reconciliation Caixa.",
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                "Error processing file from La Caixa: " . $e->getMessage()
            );
            return false;
        }
        return true;
    }

    protected function processResponseNewFile(array $aMoLogsReconcs, $filePath)
    {
        try {
            /* @var $aLogReconc LogReconciliation[] */
            foreach ($aMoLogsReconcs as $moLogReconc) {
                $aLogReconc[$moLogReconc->lineNode] = $moLogReconc;
            }

            $pReaderFile = fopen($filePath, "r");
            $pWriterFile = fopen($filePath . '.tmp2', "w");

            fgets($pReaderFile, 4096); // Line 1 is ignored
            $delimiter = ',';
            $aColumnsToWrite = array('matchType', 'lineNode', 'idPayment', 'refPaySuppOrderId', 'refPaySuppStatus',
                'refAmountPrice', 'refDateEndTransaction', 'refCurrencyTrans', 'refPaySuppExtUniId');
            $aEmptyLine = array_pad(array(), count($aColumnsToWrite), 'ERROR');

            fputs($pWriterFile, implode($delimiter, $aColumnsToWrite).$delimiter."originalLine" . PHP_EOL);
            $numberLines = 1;
            while (($lineIn = fgets($pReaderFile, 4096)) !== false) {
                $numberLines = $numberLines + 1;
                if (array_key_exists($numberLines, $aLogReconc)) {
                    fputs($pWriterFile, $aLogReconc[$numberLines]->getMainColumnsAsLine($aColumnsToWrite, $delimiter).
                                        $delimiter.rtrim($lineIn).PHP_EOL);
                } else {
                    $typeOfLine = substr($lineIn, 0, 2);
                    // Header and Total lines are skipped:
                    if ($typeOfLine == FileParserLaCaixa::LINE_HEADER ||
                        $typeOfLine == FileParserLaCaixa::LINE_TOTAL ||
                        $typeOfLine == FileParserLaCaixa::LINE_ENDOFFILE) {
                        // We ignore lines of type HEADER, TOTAL, CHANGE OF CURRENCY. We do not write them back.
                        continue;
                    }

                    fputs($pWriterFile, implode($delimiter, $aEmptyLine).$delimiter.rtrim($lineIn) . PHP_EOL);
                }
            }

            fclose($pReaderFile);
            fclose($pWriterFile);

            rename($filePath . '.tmp2', $filePath);
        } catch (Exception $e) {
            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_UNKNOWN_H . ": Processing file response of reconciliation Caixa.",
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                "Error processing file from La Caixa: " . $e->getMessage()
            );
            return false;
        }
        return true;
    }

    /**
     * @param LogReconciliation $moLogReconciliation
     *
     * @return bool|ReconcPayment
     */
    protected function findPaymentSpecificGateway(LogReconciliation $moLogReconciliation)
    {
        $moPayment = new ReconcPayment();
        if ($moPayment->getPaymentByPaySuppOrderId($moLogReconciliation->refPaySuppOrderId)) {
            // If found, we link them automatically on the log reconciliation side:
            $moLogReconciliation->idPayment = $moPayment->id;
            $moLogReconciliation->save(true, array("idPayment"));
            return $moPayment;
        } else {
            return false;
        }
    }
}
