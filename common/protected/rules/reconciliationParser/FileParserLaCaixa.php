<?php
/**
 * Created by Joaquim Forcada (Quino) in Aba English with PHP Storm
 * User: Joaquim Forcada (Quino)
 * Date: 19/02/14
 * Time: 13:48

---Please brief description here----
 */

class FileParserLaCaixa extends FileParser
{
    protected $eurCode;
    protected $usdCode;
    protected $numberLines;
    protected $numberHeaders;
    protected $numberTransError;
    protected $numberTransSuccess;
    const LINE_HEADER = '10';
    const LINE_TRANSACTION = '20';
    const LINE_TOTAL = '90';
    const LINE_ENDOFFILE = '30';


    const EUR_CAIXA_CODE = 978;
    const USD_CAIXA_CODE = 840;

    /**
     * @param int $paySuppExtId
     */
    function __construct($paySuppExtId = null)
    {
        parent::__construct($paySuppExtId);
        $this->eurCode = Yii::app()->config->get("PAY_CAIXA_EURCODE_TPV");
        $this->usdCode = Yii::app()->config->get("PAY_CAIXA_USDCODE_TPV");
        $this->rootPathToReconcFiles = Yii::app()->config->get('PAY_PATH_RECONCILIATION') . 'CAIXA/';
        return $this;
    }

    /**
     * @return bool
     */
    public function validateFile()
    {
        $aSuppliers = array(PAY_SUPPLIER_ALLPAGO_BR, PAY_SUPPLIER_CAIXA, PAY_SUPPLIER_ALLPAGO_MX);
        if ( !in_array( $this->paySuppExtId, $aSuppliers, FALSE) ) {
            return false;
        }

        return true;

    }

    /**
     * @return array|LogReconciliation[]
     *
     * @throws CException
     */
    public function parseToModels()
    {
        $aMoLogReconc = array();

        $pReaderFile = fopen(str_replace($this->subDirOut,'',$this->filePath), "r");

        rename($this->filePath, $this->filePath.'.tmp1');
        file_put_contents($this->filePath.'.tmp1', '');
        $pWriterFile = fopen($this->filePath.'.tmp1', "w");

        $lineIn = fgets($pReaderFile, 4096); // Line 1 is ignored
        if(substr($lineIn,0,4)!='0001'){
            return false;
        }
        fputs($pWriterFile, $lineIn);

        $aLinesChungas = array();
        $this->numberTransactions = 0;
        $this->numberTransSuccess = 0;
        $this->numberTransError = 0;
        $this->numberHeaders = 0;
        $this->numberLines = 1;
        while (($lineIn = fgets($pReaderFile, 4096)) !== false) {
            $this->numberLines = $this->numberLines + 1;

            $typeOfLine = substr($lineIn, 0, 2);
            // Header and Total lines are skipped:
            if ($typeOfLine == self::LINE_HEADER ||
                $typeOfLine == self::LINE_TOTAL ||
                $typeOfLine == self::LINE_ENDOFFILE) {
                $this->numberHeaders = $this->numberHeaders + 1;
                fputs($pWriterFile, $lineIn);
                continue;
            }

            if ($typeOfLine == self::LINE_TRANSACTION) {
                $this->numberTransactions = $this->numberTransactions + 1;
                $moLogReconc = $this->parseSingleTransactionToModel($lineIn, $this->numberLines);
                if (!$moLogReconc) {
                    $this->numberTransError = $this->numberTransError+1;
                    $lineOut = rtrim($lineIn).'ERROR'.PHP_EOL;
                    fputs($pWriterFile, $lineOut);
                    $aLinesChungas[strval($this->numberLines)] = $lineIn;
                } else {
                    $this->numberTransSuccess = $this->numberTransSuccess+1;
                    fputs($pWriterFile, $lineIn);
                    $aMoLogReconc[] = $moLogReconc;
                }

            } elseif (strlen($typeOfLine)>1 && trim($typeOfLine) !=='') {
                //Line not expected in Caixa File.
                $aLinesChungas[strval($this->numberLines)] = $lineIn;
                $this->numberHeaders = $this->numberHeaders +1;
                $lineOut = rtrim($lineIn).'ERROR'.PHP_EOL;
                fputs($pWriterFile, $lineOut);
            }
        }

        $processedLines = 1+($this->numberHeaders+$this->numberTransError+$this->numberTransSuccess);
        if (!feof($pReaderFile) ||
            $this->numberLines!=$processedLines){
            $this->errorMsg = "While reading file " . $this->getFilePath() .
                " has ocurred some error. Please review Parser Caixa file.";
            $linesChungas = implode(',',array_keys($aLinesChungas));

            HeLogger::sendLog(HeAbaMail::PREFIX_NOTIF_INTRA . ": " . $this->errorMsg, HeLogger::PAYMENTS,
              HeLogger::CRITICAL, $this->errorMsg.". The lines with error were ".
              $linesChungas.":<br/>". implode('<br/>', $aLinesChungas));

            throw new CException();
        }


        fclose($pReaderFile);
        fclose($pWriterFile);
        rename($this->filePath.'.tmp1', $this->filePath.'.out');
        $this->filePath = $this->filePath.'.out';

        return $aMoLogReconc;
    }

    /** Parses one line from La Caixa to model into database.
     * See document PDF in http://redmine.abaenglish.com/issues/1972 for absolute positions
     * @param string $transaction
     * @param integer $lineNumber
     *
     * @return LogReconciliation
     */
    protected function parseSingleTransactionToModel($transaction, $lineNumber)
    {
        $moLogReconc = $this->initLogReconc();
        $moLogReconc->fileOrigin = Yii::app()->name."_Caixa";
        $moLogReconc->refPaySuppOrderId = substr($transaction, 103, 12);

        // Status or operation type translation:
        $moLogReconc->refPaySuppStatus = PAY_FAIL;
        $tmpStatus = trim(substr($transaction, 33, 3)) ;
        if ($tmpStatus=='010'){
            $moLogReconc->refPaySuppStatus = PAY_SUCCESS;
        } elseif($tmpStatus=='110'){
            $moLogReconc->refPaySuppStatus = PAY_REFUND;
        }
        // Amount Translation:
        $tmpAmount = trim(substr($transaction, 64, 10));
        if ($moLogReconc->refPaySuppStatus == PAY_REFUND){
            $tmpAmount = trim(substr($transaction, 64, 9));
        }
        //$tmpAmount = $tmpAmount);
        $moLogReconc->refAmountPrice = abs(HeString::convStr2DecToCurrency($tmpAmount)); //Convert last 2 chars in decimals.
        // DateEndTransaction translation:
        $aDateEndTrans = str_split(trim(substr($transaction, 23, 6)), 2);
        $aTimeEndTrans =  str_split(trim(substr($transaction, 29, 4)), 2);
        $dateEndTrans = HeDate::europeanToSQL($aDateEndTrans[0].'-'.$aDateEndTrans[1].'-'.$aDateEndTrans[2].
                        ' '.$aTimeEndTrans[0].':'.$aTimeEndTrans[1].':00', true, '-') ;

        $moLogReconc->refDateEndTransaction = '';
        if(HeDate::validDateSQL($dateEndTrans, true)){
            $moLogReconc->refDateEndTransaction = $dateEndTrans;
        }
        // Currency translation
        $tmpCurrency = substr($transaction, 100, 3);
        if($tmpCurrency == self::EUR_CAIXA_CODE){
            $tmpCurrency = 'EUR';
        } else if($tmpCurrency == self::USD_CAIXA_CODE){
            $tmpCurrency = 'USD';
        }
        $moLogReconc->refCurrencyTrans = $tmpCurrency;
        $moLogReconc->refPaySuppExtUniId = substr($transaction, 36, 6);
        $moLogReconc->lineNode = $lineNumber;
        $moLogReconc->insertRowBeforeMatch() ;
        return $moLogReconc;
    }


    /**
     * Depending on the object passed by argument we save in one way or another.
     * @param CUploadedFile $file
     *
     * @return bool|string
     */
    protected function saveFileToDisk($file)
    {
        $fileName = $this->paySuppExtId.'_'.HeDate::todayNoSeparators(true)
            .'_'.$file->getName();
        $this->fileName = $fileName;
        $tempPath = $this->rootPathToReconcFiles.$fileName;

        $isSaved = $file->saveAs($tempPath);
        if(!$isSaved){
            $this->errorMsg = $file->getError();
            return false;
        }

        return $tempPath;
    }
}