<?php

/**
 * Created by PhpStorm.
 * User: jmerchanj
 * Date: 17/11/15
 * Time: 16:33
 */
class CmPaySupplierPlayStore extends CmPaySupplier
{

    const PURCHASE_STATE_PURCHASE = 0;
    const PURCHASE_STATE_CANCELED = 1;
    const PURCHASE_STATE_REFUNDED = 2;

    const SIGNATURE_ALGORITHM = OPENSSL_ALGO_SHA1;

    private $purchaseReceipt="";
    private $signature="";
    private $receipt="";

    private $ready=false;


    private $receipt_autoRenewing=0;
    private $receipt_orderId="";
    private $receipt_packageName="";
    private $receipt_productId="";
    private $receipt_purchaseTime=null;
    private $receipt_purchaseState=null;
    private $receipt_developerPayload="";
    private $receipt_purchaseToken="";

    /**
     * CmPaySupplierPlayStore constructor.
     */
    public function __construct($purchaseReceipt="",$signature="")
    {
        $this->purchaseReceipt=$purchaseReceipt;
        $this->signature=$signature;

        if ($purchaseReceipt!="" && $signature!=""){
            $this->decodeReceipt();
        }
    }

    /**
     * @return string
     */
    public function getPurchaseReceipt()
    {
        return $this->purchaseReceipt;
    }

    /**
     * @return string
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * @param string $purchaseReceipt
     */
    public function setPurchaseReceipt($purchaseReceipt)
    {
        $this->purchaseReceipt = $purchaseReceipt;
        $this->ready=false;
        $this->decodeReceipt();
    }

    /**
     * @param string $signature
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;
        $this->ready=false;
        $this->decodeReceipt();
    }

    /**
     * @return string
     */
    public function getReceipt()
    {
        return $this->receipt;
    }



    /**
     * @return null
     */
    public function getReceiptAutoRenewing()
    {
        return $this->receipt_autoRenewing;
    }

    /**
     * @return null
     */
    public function getReceiptOrderId()
    {
        return $this->receipt_orderId;
    }

    /**
     * @return null
     */
    public function getReceiptPackageName()
    {
        return $this->receipt_packageName;
    }

    /**
     * @return null
     */
    public function getReceiptProductId()
    {
        return $this->receipt_productId;
    }

    /**
     * @return null
     */
    public function getReceiptPurchaseTime()
    {
        return $this->receipt_purchaseTime;
    }

    /**
     * @return null
     */
    public function getReceiptPurchaseState()
    {
        return $this->receipt_purchaseState;
    }

    /**
     * @return null
     */
    public function getReceiptDeveloperPayload()
    {
        return $this->receipt_developerPayload;
    }

    /**
     * @return null
     */
    public function getReceiptPurchaseToken()
    {
        return $this->receipt_purchaseToken;
    }





    public function getErrorCode()
    {
        // TODO: Implement getErrorCode() method.
    }

    public function getErrorString()
    {
        // TODO: Implement getErrorString() method.
    }

    public function runDebiting($curPayment, $transactionType = "A", $isRecurringPay = false)
    {
        // TODO: Implement runDebiting() method.
    }

    public function getMoLogResponse()
    {
        // TODO: Implement getMoLogResponse() method.
    }

    public function runRefund($curPayment, $moPayOrigin)
    {
        // TODO: Implement runRefund() method.
    }

    public function getPaySupplierId()
    {
        // TODO: Implement getPaySupplierId() method.
    }

    public function getListOfCards()
    {
        // TODO: Implement getListOfCards() method.
    }

    public function getListSpecificConditions()
    {
        // TODO: Implement getListSpecificConditions() method.
    }

    public function CheckSignature(){
        $public_key_str = Yii::app()->config->get("PAY_ANDROID_PUBLIC_KEY");
        $public_key_formated = "-----BEGIN PUBLIC KEY-----\r\n" . chunk_split($public_key_str, 64, "\r\n") . "-----END PUBLIC KEY-----";
        $publicKeyId = openssl_get_publickey($public_key_formated);

        $result = openssl_verify($this->purchaseReceipt,base64_decode($this->signature), $publicKeyId,$this::SIGNATURE_ALGORITHM);
        switch ($result) {
            case 1:
                return true;
                break;
            case 0;
                return false;
                break;
            default:  // ugly, error checking signature
                return false;
        }
    }

    private function decodeReceipt(){

        if($this->CheckSignature())
        {
            try
            {
                $this->receipt=json_decode($this->purchaseReceipt,true);
                $this->receipt_autoRenewing=$this->receipt['autoRenewing'];
                if (!array_key_exists('orderId',$this->receipt)){
                    //In test mode may not send orderId then we use 'purchaseTime' just like unic number
                    $this->receipt_orderId=$this->receipt['purchaseTime'];
                }else{
                    $this->receipt_orderId=$this->receipt['orderId'];
                }

                $this->receipt_packageName=$this->receipt['packageName'];
                $this->receipt_productId=$this->receipt['productId'];
                $this->receipt_purchaseTime=$this->receipt['purchaseTime'];
                $this->receipt_purchaseState=$this->receipt['purchaseState'];
                if (array_key_exists('developerPayload',$this->receipt)){
                    $this->receipt_developerPayload=$this->receipt['developerPayload'];
                };
                $this->receipt_purchaseToken=$this->receipt['purchaseToken'];
                $this->ready=true;
            }
            catch (Exception $problem){
                $this->receipt=false;
            }
        }else{
            $this->receipt=false;
            $this->receipt_autoRenewing=0;
            $this->receipt_orderId="";
            $this->receipt_packageName="";
            $this->receipt_productId="";
            $this->receipt_purchaseTime=null;
            $this->receipt_purchaseState=null;
            $this->receipt_developerPayload="";
            $this->receipt_purchaseToken="";
        }
    }

    /**
     * @param $moUser
     *
     * @return int
     */
    public function validatePurchaseReceipt($moUser)
    {
        if ($this->ready == true) {

            if ($this->receipt_purchaseState <> $this::PURCHASE_STATE_PURCHASE) {
                return 1;
            }

            $clsResultPaymentsPlayStore = CActiveRecord::model("AbaPaymentsPlaystore")->find('orderid=:orderid_param',
              array(':orderid_param' => $this->receipt_orderId));
            if ($clsResultPaymentsPlayStore != null) {

                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_LOGIC_L . ' EXISTING PlayStore receipt',
                  HeLogger::PAYMENTS,
                  HeLogger::CRITICAL,
                  "EXISTING PlayStore receipt. There is already a user with this orderId: " . $this->receipt_orderId . ". "
                );
                return 2;
            }
            return 0;

        } else {
            return -1;
        }
    }


}