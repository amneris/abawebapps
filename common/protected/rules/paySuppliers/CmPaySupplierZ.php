<?php

class CmPaySupplierZ extends CmPaySupplier
{
    const NOERROR = 0;
    const ERROR_HTTP = 1001;

    protected $connectToGateway;
    protected $url;
    protected $version;
    protected $jsonRequest;
    protected $jsonResponse;
    protected $errorCode;
    protected $moLogResponse;
    protected $originalTransactionId;
    protected $orderNumber;
    protected $transactionId;
    protected $originalProductId;
    protected $productId;
    protected $errorMsg = array
    (
        CmPaySupplierZ::NOERROR => "Sin error",
        CmPaySupplierZ::ERROR_HTTP => "Error Http",
    );

    public function __construct()
    {
        parent::__construct();
    }

    /** Returns an array of integer as a key, and string as a value. Each of implementations for this definition class
     * should have at least one element within the array.
     *
     * @return array
     */
    public function getListOfCards()
    {
        return array();
    }

    /**
     * @return int
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return string
     */
    public function getErrorString()
    {
        if (isset($this->errorMsg[$this->errorCode])) {
            return $this->errorMsg[$this->errorCode];
        } else {
            return "ErrorCode " . $this->errorCode;
        }
    }

    public function runRequestReconciliation($stAppStorePayment)
    {
        return true;
    }

    /**
     * @return mixed
     */
    public function getOutput()
    {
//        return $this->stOutput;
        return true;
    }

    /**
     * @return int|mixed|string
     */
    public function getPaySupplierId()
    {
        if (!is_numeric($this->idPaySupplier)) {
            return PAY_SUPPLIER_Z;
        }
        return $this->idPaySupplier;
    }

    /**
     * @param int $idPaySupplier
     */
    public function setPaySupplierId($idPaySupplier = PAY_SUPPLIER_Z)
    {
        $this->idPaySupplier = $idPaySupplier;
    }

    public function runDebiting($curPayment, $transactionType = 'A', $isRecurringPay = false)
    {
        $this->orderNumber = $curPayment->id . ":" . substr("" . date("Hms"), 3);
        return $this->getMoLogResponse();
    }

    /**
     * @return mixed
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @return mixed
     */
    public function getOriginalTransactionId()
    {
        return $this->originalTransactionId;
    }

    /**
     * @return mixed
     */
    public function getFirstTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @return mixed
     */
    public function getOriginalProductId()
    {
        return $this->originalProductId;
    }

    /**
     * @param $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @param $originalTransactionId
     */
    public function setOriginalTransactionId($originalTransactionId)
    {
        $this->originalTransactionId = $originalTransactionId;
    }

    /**
     * @param $transactionId
     */
    public function setFirstTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;
    }

    /**
     * @param $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @param $originalProductId
     */
    public function setOriginalProductId($originalProductId)
    {
        $this->originalProductId = $originalProductId;
    }

    /**
     * @param $oResponse
     *
     * @return mixed
     */
    public static function decodeResponse($oResponse)
    {
        return json_decode($oResponse, true);
    }

    /** Returns id of translation specific for every payment gateway
     *
     * @return string
     */
    public function getListSpecificConditions()
    {
        return "";
    }

    /**
     * @param $curPayment
     * @param $moPayOrigin
     *
     * @return bool|PayGatewayLogZ
     */
    public function runRefund($curPayment, $moPayOrigin)
    {
        return $this->getMoLogResponse();
    }

    /** ++++++++++++Pay supplier log++++++++++++ */

    /**
     * @param $idPayment
     * @param $userId
     * @param $operation
     *
     * @return bool|int
     */
    protected function startLogOperation($idPayment, $userId, $operation)
    {
//        $this->moLogResponse = new CmPayGatewayLogZ($this->idPaySupplier);
//
//        $this->moLogResponse->idPaymentIos = $idPayment;
//        $this->moLogResponse->userId = $userId;
//        $this->moLogResponse->operation = $operation;
//        $this->moLogResponse->dateRequest = HeDate::todaySQL(true);
//        $this->moLogResponse->jsonRequest = $this->jsonRequest;
//        $this->moLogResponse->errorCode = null;
//        $this->moLogResponse->success = 0;
//        $this->moLogResponse->idPaySupplier = $this->getPaySupplierId();
//
//        return $this->moLogResponse->insertLog();
    }

    /**
     * @param $success
     * @return bool
     */
    protected function endLogOperation($success)
    {
//        $this->moLogResponse->jsonResponse = "";
//        $this->moLogResponse->dateResponse = HeDate::todaySQL(true);
//        $this->moLogResponse->errorCode = $this->errorCode;
//        $this->moLogResponse->success = $success;
//
//        return $this->moLogResponse->update(array("jsonResponse", "dateResponse", "errorCode", "success"));
    }

    /**
     * @return PayGatewayLogZ
     */
    public function getMoLogResponse()
    {
        return $this->moLogResponse;
    }
}
