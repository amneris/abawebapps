<?php

/**
 * Created by PhpStorm.
 * User: mikkel
 * Date: 25/08/15
 * Time: 18:12
 */
abstract class CmPaySupplier
{
    /* @var $connectToGateway integer
     * If the current gateway should connect to the Supplier platform
    or use the simulator */
    protected $connectToGateway;
    /* @var $specialFields Array Fields not common between payment gateways */
    protected $specialFields;
    /* @var integer $idPaySupplier; Pay_supplier: Caixa, AllPago, Paypal, B2b, etc. */
    protected $idPaySupplier;
    /**
     *
     */
    public function __construct()
    {
        $this->connectToGateway = intval(Yii::app()->config->get("PAY_CONNECT_GATEWAY"));
    }

    /**
     * @abstract
     * @return string
     */
    abstract public function getErrorCode();

    /**
     * @abstract
     * @return mixed
     */
    abstract public function getErrorString();


    //TODO: Split in <method>Payment and <method>PaymentControlCheck
    /*
    * @param Payment|PaymentControlCheck|AbaUserCreditForms $objSpecialFields
    *
    * @return bool
    */
    public function setSpecialFields($objSpecialFields)
    {
        return true;
    }


    /** Returns a list of fields that should be filled in the payment process because
     * the Gateway payment supplier demands them. For La Caixa for example no special is required.
     *
     * @return array
     */
    public function getSpecialDisplayFields()
    {
        return array();
    }

    //TODO: Split in <method>Payment and <method>PaymentControlCheck
    /**
     * @param PaymentControlCheck|Payment $curPayment
     * @param string $transactionType
     * @param bool $isRecurringPay
     *
     * @return bool|PayGatewayLogLaCaixaResRec|PayGatewayLogAllpago|PayGatewayLogPayPal|PayGatewayLogAdyenResRec
     */
    abstract public function runDebiting($curPayment, $transactionType = "A", $isRecurringPay = false);

    /**
     * @return PayGatewayLog
     */
    abstract public function getMoLogResponse();

    /**
     * @param $curPayment
     * @param $moPayOrigin
     *
     * @return bool|PayGatewayLogLaCaixaResRec|PayGatewayLogAllpago|PayGatewayLogPayPal|PayGatewayLogAdyenResRec
     */
    abstract public function runRefund($curPayment, $moPayOrigin);

    /**
     * @return bool|PayGatewayLogLaCaixaResRec|PayGatewayLogAllpago|PayGatewayLogPayPal|PayGatewayLogAdyenResRec
     * @return mixed
     */
    public function runDebitingRenewal($moPayCtrlCheck, $moUser, $moUserCredit)
    {
        return false;
    }

    /**
     * It must match the integer in the database
     * Currently only there is La Caixa(1) and PayPal(2)
     * @return int|mixed
     */
    abstract public function getPaySupplierId();

    /** Returns the name of the supplier, but from database table pay_suppliers
     * @return bool|string
     */
    public function getSupplierName()
    {
        $moPaySupplier = new AbaPaySuppliers();
        return $moPaySupplier->getNameByIdSupplier($this->idPaySupplier);
    }
    /** Returns an array of integer as a key, and string as a value. Each of implementations for this definition class
     * should have at least one element within the array.
     *
     * @return array
     */
    abstract public function getListOfCards();


    /** Returns id of translation specific for every payment gatewayf
     *
     * @return string
     */
    abstract public function getListSpecificConditions();

    /** Requests the XML file, CSV or whatever type of File containing group of transactions
     * for the corresponding supplier
     *
     * @return bool
     *
    public function runRequestReconciliation()
    {
        return false;
    }*/

    //Todo: Update params
    /** Send a change in the payment details for a user, it can be a new credit card, Paypal account, etc.
     * Returns true if success, false if has not achieved the alteration.
     *
     * @param AbaUserCreditForms $moOldUserCredit
     * @param AbaUserCreditForms $moNewUserCredit
     *
     * @return bool
     */
    public function runChangeCreditDetails(AbaUserCreditForms $moOldUserCredit, AbaUserCreditForms $moNewUserCredit)
    {
        return false;
    }

    //Todo: Update params
    /** Connects to the payment gateway and cancels any possible link or subscription for a certain user.     *
     * @param Payment $moPayment
     * @param AbaUserCreditForms $moUserCredit
     *
     * @return bool
     */
    public function runCancelRecurring(Payment $moPayment, AbaUserCreditForms $moUserCredit)
    {
        return false;
    }

    /** It connects to the payment gateway and returns relevant information on the subscription or recurring payment.
     * In case of Paypal it calls to GetRecurringPaymentsProfileDetails().
     * Depending on the platform it will return different information format.
     *
     * @param integer $userId
     * @param string $registrationId
     *
     * @return bool|Payment Returns remote payment.
     */
    public function runGetSubscriptionInfo($userId, $registrationId)
    {
        return false;
    }

    //Todo: Update params
    /** Creates in the remote gateway the initial recurring payment or maybe the subscription profile.
     * It depends a lot on each platform.
     *
     * @param Payment $nextPayment
     * @param Payment|PaymentControlCheck $curPayment
     * @param AbaUser $moUser
     *
     * @return bool
     */
    public function runCreateRecurring(Payment $nextPayment, $curPayment, AbaUser $moUser)
    {
        return true;
    }

    //#4400
    protected $optionalData = array();

    /**
     * @param array $optionalData
     */
    public function setOptionalData($optionalData = array())
    {
        $this->optionalData = $optionalData;
    }

    /**
     * @return array
     */
    public function getOptionalData()
    {
        return $this->optionalData;
    }
}
