<?php

/**
 * Created by PhpStorm.
 * User: mikkel
 * Date: 25/08/15
 * Time: 17:33
 */
class CmPaymentRules
{
    /** For a single payment checks its supplier and returns the corresponding class: method + supplier
     *
     * @param integer $paySupplierId
     *
     * @return PaySupplierLaCaixa|PaySupplierPayPal|PaySupplierAllPagoBr|PaySupplierAllPagoMx|CmPaySupplierAppStore|PaySupplierAdyen|PaySupplierAdyenHpp|CmPaySupplierZ
     */
    public static function createPaymentSupplier($paySupplierId)
    {
        switch (intval($paySupplierId)) {
            case PAY_SUPPLIER_PAYPAL:
                return new PaySupplierPayPal();
                break;
            case PAY_SUPPLIER_ALLPAGO_BR:
                return new PaySupplierAllPagoBr();
                break;
            case PAY_SUPPLIER_ALLPAGO_MX:
                return new PaySupplierAllPagoMx();
                break;
            case PAY_SUPPLIER_CAIXA:
                return new PaySupplierLaCaixa();
                break;
            case PAY_SUPPLIER_ALLPAGO_BR_BOL:
                return new PaySupplierAllPagoBoleto();
                break;
            case PAY_SUPPLIER_APP_STORE:
                return new CmPaySupplierAppStore();
                break;
            case PAY_SUPPLIER_ADYEN:
                return new PaySupplierAdyen();
                break;
            case PAY_SUPPLIER_ADYEN_HPP:
                return new PaySupplierAdyenHpp();
                break;
            case PAY_SUPPLIER_ANDROID_PLAY_STORE:
                return new CmPaySupplierPlayStore();
                break;
            case PAY_SUPPLIER_Z:
                return new CmPaySupplierZ();
                break;
            default:
                return new PaySupplierLaCaixa();
                break;
        }
    }
}