<?php

/**
 * Created by PhpStorm.
 * User: mikkel
 * Date: 25/08/15
 * Time: 18:56
 */
class CmRecurringPaymentRules
{
    private $errorMessage;

    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * Returns  the user status for the current or last subscription. It can only be one of these status:
     *   SUBS_PREMIUM_A_CARD_A, SUBS_PREMIUM_C_PAYPAL, etc.
     *
     * @param $user
     * @param $lastPaymentSubscription The latest payment subscription, can only be PENDING or CANCEL.
     *
     * @return int|null
     */
    public function getTypeSubscriptionStatus($user, $lastPaymentSubscription = null)
    {
        if (empty($user->id)) {
            return SUBS_NULL;
        }
        // we compose PREMIUM(2) + ACTIVE (0-PENDING) + CARD (ALLPAGO)
        $userType = intval($user->userType);

        if (empty($moLastPaySubs) || empty($moLastPaySubs->id)) {
            $moLastPaySubs = new CmAbaPayment();
            if (!$moLastPaySubs->getLastPayLastSubscription($user->id)) {
                if (!$moLastPaySubs->getLastPaymentByUserId($user->id, true)) {
                    // If there is no PAYMENT is obviously FREE;
                    $moLastPaySubs = null;
                    if ($userType == FREE) {
                        return SUBS_FREE;
                    } else {
                        if ($userType == DELETED) {
                            return SUBS_FREE;
                        } else {
                            return false;
                        }
                    }
                }
            }
        }

        if ($moLastPaySubs->paySuppExtId == PAY_SUPPLIER_GROUPON ||
            $moLastPaySubs->paySuppExtId == PAY_SUPPLIER_B2B
        ) {
            $paySuppExtId = intval($moLastPaySubs->paySuppExtId);
            $payStatus = str_pad(PAY_CANCEL, 2, '00', STR_PAD_LEFT);
        } else {
            $paySuppExtId = intval($moLastPaySubs->paySuppExtId);
            $payStatus = $moLastPaySubs->status;
            if ($payStatus == PAY_FAIL) {
                $payStatus = PAY_CANCEL;
            } elseif ($payStatus == PAY_SUCCESS) {
                // Case PAYPAL not creation of RECURRING:
                $payStatus = PAY_CANCEL;
            }
            $payStatus = str_pad($payStatus, 2, '00', STR_PAD_LEFT);
        }
        if (($paySuppExtId == PAY_SUPPLIER_ALLPAGO_BR || $paySuppExtId == PAY_SUPPLIER_ALLPAGO_MX) &&
            $userType == FREE) {
            // In theory this case could not happen:
            $paySuppExtId = PAY_METHOD_CARD;
        }

        $typeSubsStatus = '' . $userType . $payStatus . $paySuppExtId;
        switch ($typeSubsStatus) {
            case SUBS_PREMIUM_A_CARD_A:
            case SUBS_PREMIUM_A_CARD_C:
            case SUBS_PREMIUM_A_CARD_M:
            case SUBS_PREMIUM_A_CARD_AD:
            case SUBS_PREMIUM_A_CARD_IOS:
            case SUBS_PREMIUM_A_PAYPAL:
            case SUBS_PREMIUM_C_CARD_A:
            case SUBS_PREMIUM_C_CARD_M:
            case SUBS_PREMIUM_C_CARD_C:
            case SUBS_PREMIUM_C_CARD_AD:
            case SUBS_PREMIUM_C_CARD_IOS:
            case SUBS_PREMIUM_C_PAYPAL:
            case SUBS_EXPREMIUM_CARD:
            case SUBS_EXPREMIUM_PAYPAL:
            case SUBS_PREMIUM_THROUGHPARTNER:
            case SUBS_PREMIUM_THROUGHB2B:
            case SUBS_EXPREMIUM_THROUGHPARTNER:
            case SUBS_PREMIUM_C_BOLETO_BR:
            case SUBS_FREE:
            case SUBS_PREMIUM_A_ANDROID:
            case SUBS_PREMIUM_C_ANDROID:
                //continue, we have correctly matched a scenario.
                break;
            default:
                $typeSubsStatus = SUBS_FREE;
                if ($userType == PREMIUM) {
                    $typeSubsStatus = SUBS_PREMIUM_C_CARD_A;
                }
                break;
        }

        return $typeSubsStatus;
    }

    /**
     * #ZOR-267
     *
     * @param $cmAbaPayment
     * @param $cmUser
     *
     * @return bool
     */
    public function renewPaymentZbyPayment( $cmAbaPayment, $cmUser, $renewedPayment )
    {
        $cmAbaPayment->foreignCurrencyTrans = $cmAbaPayment->currencyTrans;

        $moCurrency = new CmCurrencies($cmAbaPayment->foreignCurrencyTrans);
        $xRateToEUR = $moCurrency->getConversionRateTo(CCY_DEFAULT);
        $xRateToUSD = $moCurrency->getConversionRateTo(CCY_2DEF_USD);
        $paySuppExtUnid = $renewedPayment["invoiceNumber"];

        if(!HeDate::validDateSQL($renewedPayment["paymentDate"]) || !HeDate::validDateSQL($renewedPayment["nextRenewalDate"])) {

            $this->errorMessage = "CmRecurringPaymentRules - renewPaymentZbyPayment dates not correct paymentDate = " . $renewedPayment["paymentDate"]. " and nextRenewalDate = " . $renewedPayment["nextRenewalDate"];
            HeLogger::sendLog(HeLogger::PREFIX_ERR_UNKNOWN_H . ": Recurring payment , dates not correct validation. ",
                HeLogger::IT_BUGS, HeLogger::CRITICAL, $this->errorMessage);

            return false;
        }

        $paymentDate = trim(substr($renewedPayment["paymentDate"], 0, 10)) . " 00:00:00";
        $paymentRenewalDate = trim(substr($renewedPayment["nextRenewalDate"], 0, 10)) . " 00:00:00";

        $fSuccUpd = $cmAbaPayment->recurringPaymentProcess(PAY_SUPPLIER_Z, $cmAbaPayment->paySuppOrderId, PAY_SUCCESS, $paymentDate, $xRateToEUR, $xRateToUSD, $paySuppExtUnid);

        if (!$fSuccUpd) {
            $this->errorMessage = "ABA: Could not be updated SQL for old payment = " . $cmAbaPayment->id;
            HeLogger::sendLog(HeLogger::PREFIX_ERR_UNKNOWN_H . ": Recurring payment , old payment could not be updated. ",
              HeLogger::IT_BUGS, HeLogger::CRITICAL, $this->errorMessage);
            return false;
        }

        $moNextPayment = $cmAbaPayment->createDuplicateNextPayment();
        $moNextPayment->id = HePayments::generateIdPayment($cmUser, $moNextPayment->dateToPay);
        $moNextPayment->idPayControlCheck = $cmAbaPayment->id;
        $moNextPayment->xRateToEUR = $xRateToEUR;
        $moNextPayment->xRateToUSD = $xRateToUSD;
        $moNextPayment->isPeriodPayChange = null;
        $moNextPayment->idPartner = PARTNER_ID_Z_RECURRING;
        $moNextPayment->dateToPay = $paymentRenewalDate;

        if (!$moNextPayment->paymentProcess($moNextPayment->userId)) {
            $this->errorMessage = "ABA: Recurring payment. Next payment could not be created into database. = " . $moNextPayment->idPayControlCheck;
            HeLogger::sendLog("Recurring payment , Next Payment could not be created. ",HeLogger::IT_BUGS,HeLogger::CRITICAL,$this->errorMessage);
            return false;
        }

        return true;
    }

    /**
     * @param $cmUser
     * @param string $refundDate
     * @param int $amount
     * @param string $accountId
     * @param string $refundId
     * @param string $periodMonth
     * @param string $currency
     *
     * @return bool
     * @throws CDbException
     */
    public function refundPaymentZ(
      $cmUser,
      $refundDate = "",
      $amount = 0,
      $accountId = "",
      $refundId = "",
      $periodMonth = "",
      $currency = ""
    ) {
        try {

            $dateStart = HeDate::todaySQL(true);
            $amountToRefund = floatval($amount);
            $sPaymentId = "A" . (HePayments::generateIdPaymentRefund($cmUser->id));

            $cmAbaPayment = new CmAbaPayment();

            $cmAbaPayment->id = $sPaymentId;
            $cmAbaPayment->userId = $cmUser->id;

            $cmAbaPayment->idUserProdStamp = 0;
            $cmAbaPayment->idPromoCode = "";
            $cmAbaPayment->lastAction = "";
            $cmAbaPayment->idPartnerPrePay = 0;
            $cmAbaPayment->cancelOrigin = 0;
            $cmAbaPayment->idSession = "";
            $cmAbaPayment->idUserCreditForm = 0;
            $cmAbaPayment->idPayControlCheck = 0;
            $cmAbaPayment->paySuppLinkStatus = 0;
            $cmAbaPayment->isRecurring = 0;
            $cmAbaPayment->isExtend = 0;

            $cmAbaPayment->paySuppExtId = PAY_SUPPLIER_Z;
            $cmAbaPayment->experimentVariationAttributeId = 0;
            $cmAbaPayment->idUserCreditForm = 0;


            //
            $cmAbaPayment->idCountry = $cmUser->countryId;
            $cmAbaPayment->idPeriodPay = CmProdPeriodicity::MONTH_12;
            $cmAbaPayment->idProduct = $cmAbaPayment->idCountry . "_" . CmProdPeriodicity::MONTH_12;
            $cmAbaPayment->currencyTrans = "EUR";

            $cmAbaSuccessPayment = new CmAbaPayment();
            $existsLastPayment = false;

            if (is_numeric(trim($periodMonth))) {

                $cmProdPeriodicity = new CmProdPeriodicity();

                if ($cmProdPeriodicity->getProdPeriodByMonths($periodMonth)) {
                    $cmAbaPayment->idPeriodPay = $cmProdPeriodicity->days;
                } else {
                    $cmAbaPayment->idPeriodPay = HePayments::monthToPeriod($periodMonth);
                }
                $cmAbaPayment->idProduct = $cmAbaPayment->idCountry . "_" . $cmAbaPayment->idPeriodPay;
            } else {

                if ($cmAbaSuccessPayment->getLastSuccessPayment($cmUser->id)) {

                    $cmAbaPayment->idCountry = $cmAbaSuccessPayment->idCountry;
                    $cmAbaPayment->idPeriodPay = $cmAbaSuccessPayment->idPeriodPay;
                    $cmAbaPayment->idProduct = $cmAbaSuccessPayment->idProduct;

                    $existsLastPayment = true;
                }
            }

            //
            if (trim($currency) <> "") {
                $cmAbaPayment->currencyTrans = $currency;
            } else {
                if ($existsLastPayment AND $cmAbaSuccessPayment->currencyTrans <> "") {
                    $cmAbaPayment->currencyTrans = $cmAbaSuccessPayment->currencyTrans;
                }
            }
            $cmAbaPayment->foreignCurrencyTrans = $cmAbaPayment->currencyTrans;


            //
            $cmAbaPayment->status = intval(PAY_REFUND);

            $cmAbaPayment->dateStartTransaction = $dateStart;
            $cmAbaPayment->dateEndTransaction = trim(substr($refundDate, 0, 10)) . " 00:00:00";
            $cmAbaPayment->dateToPay = trim(substr($refundDate, 0, 10)) . " 00:00:00";

            $cmAbaPayment->amountOriginal = $amountToRefund;
            $cmAbaPayment->amountDiscount = 0;
            $cmAbaPayment->amountPrice = $amountToRefund;

            $cmAbaPayment->paySuppOrderId = $refundId;
            $cmAbaPayment->paySuppExtProfId = $accountId;
            $cmAbaPayment->paySuppExtUniId = $refundId;

            $cmAbaPayment->taxRateValue = 0;
            $cmAbaPayment->amountTax = 0;
            $cmAbaPayment->amountPriceWithoutTax = 0;

            $cmAbaPayment->idPartner = PARTNER_ID_Z;

            //
            $moCurrency = new CmCurrencies($cmAbaPayment->foreignCurrencyTrans);
            $xRateToEUR = $moCurrency->getConversionRateTo(CCY_DEFAULT);
            $xRateToUSD = $moCurrency->getConversionRateTo(CCY_2DEF_USD);

            $cmAbaPayment->xRateToEUR = $xRateToEUR;
            $cmAbaPayment->xRateToUSD = $xRateToUSD;

            $cmAbaPayment->setAmountWithoutTax($dateStart);

//            $cmAbaPayment->isNewRecord = true;

            if (!$cmAbaPayment->paymentProcess($cmAbaPayment->userId)) {

                $this->errorMessage = "ABA: Zuora refund could not be created into database. = " . $cmAbaPayment->id;
                HeLogger::sendLog("Recurring payment , Next Payment could not be created. ", HeLogger::IT_BUGS,
                  HeLogger::CRITICAL, $this->errorMessage);
                return false;
            }

            return true;
        } catch (Exception $e) {

            HeLogger::sendLog("refundPaymentsZ ", HeLogger::IT_BUGS,
              HeLogger::CRITICAL, 'refundPaymentsZ - refundPaymentZ Exception: ' . $e->getMessage());

        }

        return false;
    }

}
