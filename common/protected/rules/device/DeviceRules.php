<?php

/**
 * DeviceRules
 * Author: mgadegaard
 * Date: 07/01/2015
 * Time: 16:54
 * © ABA English 2015
 */
class DeviceRules {

    /**
     * @param $headerDevice
     * @param $teacher
     * @return string
     */
    public function getMobileImageByDevice($headerDevice, $teacher)
    {
        $teacherMobileImage = '';
        $device = new Devices();
        $size = $device->getDeviceSize($headerDevice);

        switch($size) {
        case '2x':
            $teacherMobileImage = $teacher->mobileImage2x;
            break;
        case '3x':
            $teacherMobileImage = $teacher->mobileImage3x;
            break;
        default:
            break;
        }
        return $teacherMobileImage;
    }
}