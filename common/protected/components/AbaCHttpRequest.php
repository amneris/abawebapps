<?php

/**
 * workarounds para varios de los problemas de detección de headers que tenemos en los diferentes entornos.
 *
 * WARN: Mucho cuidado al modificar aquí porque los entornos Vagrant (DEV), AWS (INT) y Nexica (PROD) se comportan de
 * manera bastante diferente.
 *
 * - DEV CentOS 6, Apache, PHP-FPM (CentOS 6 no tiene soporte oficial para mod_rpaf) así que tenemos que propagar
 * cabeceras manualmente
 *
 * - INT CentOS 6, Apache, mod_php. Los apaches están detrás del ElasticLoadBalancer. Es la implementación más estandar
 * de proxy de cualquiera de nuestros 3 entornos.
 *
 * - PROD CentOS, Apache, mod_php. Los apaches están detrás de un balanceador llamado Zeus que tiene sus propias
 * particularidades a la hora de tratar con los headers.
 *
 */
class AbaCHttpRequest extends CHttpRequest
{

    /**
     * Return if the request is sent via secure channel (https).
     * @return boolean if the request is sent via secure channel (https)
     *
     * @SuppressWarnings(PHPMD.BooleanGetMethodName, PHPMD.Superglobals)
     */
    public function getIsSecureConnection()
    {
        // f*cking Zeus Load Balancer doesn't use HTTP_X_FORWARDED_PROTO
        return parent::getIsSecureConnection() ||
                                        (isset($_SERVER['HTTP_SSLSESSIONID']) && !empty($_SERVER['HTTP_SSLSESSIONID']));
    }

    /**
     * Returns the user IP address.
     * @return string user IP address
     *
     * @SuppressWarnings(PHPMD.Superglobals, PHPMD.XXXXX)
     */
    public function getUserHostAddress()
    {
        /**
         * Because ZEUS balance loader alters the original HTTP header request, we must search and replace the IP to
         * CGI variable:
         */
        if (isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && $_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];

        if (isset($_SERVER['HTTP_CLIENT_IP']) && $_SERVER['HTTP_CLIENT_IP']) return $_SERVER['HTTP_CLIENT_IP'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR']) {
            /**
             * HTTP_X_FORWARDED_FOR can sometimes have a comma delimited list of IP addresses
             */
            $httpXForwardedFor = $_SERVER['HTTP_X_FORWARDED_FOR'];
            return stristr($httpXForwardedFor, ',') ? trim(reset(explode(',', $httpXForwardedFor))) : $httpXForwardedFor;
        }
        if (isset($_SERVER['HTTP_X_FORWARDED']) && $_SERVER['HTTP_X_FORWARDED']) return $_SERVER['HTTP_X_FORWARDED'];
        if (isset($_SERVER['HTTP_FORWARDED_FOR']) && $_SERVER['HTTP_FORWARDED_FOR']) return $_SERVER['HTTP_FORWARDED_FOR'];
        if (isset($_SERVER['HTTP_FORWARDED']) && $_SERVER['HTTP_FORWARDED']) return $_SERVER['HTTP_FORWARDED'];

        return parent::getUserHostAddress();
    }
}