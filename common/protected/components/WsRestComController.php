<?php

/**
 * WsRestComController is the customized base Ws Rest Controller class.
 * All WS Rest controller classes for this application should extend from this base class.
 */
abstract class WsRestComController extends CController
{
    const SUCCESS_CODE = 200;
    protected $_format = 'application/json';
    protected static $aErrorCodes = array(
        200 => 'OK',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Bad signature',
        410 => 'The purchase receipt is not valid',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        555 => 'Unidentified error',
    );
    static protected $version = "1.0";
    static protected $aHistoryVersion = array("1.0" => " Common version of REST, need to write history version");
    /* @var array $reqDataDecoded */
    protected $reqDataDecoded;
    protected $reqMethod;
    /* @var LogWebService $moLogWs */
    protected $moLogWs;
    /* @var AbaUser $moUser */
    protected $moUser;
    /* @var AbaUserDevice $moDevice */
    protected $moDevice;
    protected $nameService;
    protected $aResponse;
    protected $isSuccess;
    protected $errorCode;

    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
        $this->nameService = null;
        Yii::app()->params["isTransRunning"] = false;
    }

    public function beforeAction($action)
    {
        $this->disableCWebLogRoute();
        $this->logStart();
        if (!$this->checkIfLogined()) {
            return false;
        }
        $requestMethod = strtolower($_SERVER['REQUEST_METHOD']);
        $data = array();
        switch(strtoupper($requestMethod)) {
            case 'GET': // LIST, VIEW, INFO
                //$data = $_GET;
                $data = $this->actionParams;
                break;
            case 'POST': // CREATE
                $data = HeMixed::getPostArgs();
                break;
            case 'PUT': //UPDATE
                // The action still is defined in the GET argument:
                // $data = file_get_contents('php://input');
                $data = $this->actionParams;
                break;
            case 'DELETE': // DELETE DATABASE
                $data = $this->actionParams;
                break;
        }
        $this->reqDataDecoded = $data;
        $this->reqMethod = strtoupper($requestMethod);
        return true;
    }

    protected function afterAction($action)
    {
        $this->isSuccess = true;
        $this->logEnd($this->aResponse);
        parent::afterAction($action);
    }

    /****************************************************************/
    /*  CUSTOM METHODS, OUTSIDE YII  */
    /****************************************************************/
    /**
     * Public and generic method for listing elements with  REST SERVICES.
     * Probably overwritten in children implementations
     */
    public function actionList()
    {
        $this->sendResponse(
            200,
            array('status' => 'OK', 'message' => 'ActionList, params by GET.'),
            $this->_format
        );
    }

    /**
     * Public and generic method for view info detailed for a certain element with  REST SERVICES.
     * Probably overwritten in children implementations
     */
    public function actionView()
    {
        $this->sendResponse(
            200,
            array('status' => 'OK', 'message' => 'actionView, parameters by GET.'),
            $this->_format
        );
        return true;
    }

    /**
     * Public and generic method for creation actions with  REST SERVICES.
     * Probably overwritten in children implementations
     */
    public function actionCreate()
    {
        $this->sendResponse(200, array('status' => 'OK', 'message' => 'ActionCreate POST.',
            $this->reqDataDecoded), $this->_format);
    }

    /**
     * Unique Public and generic method for update actions with  REST SERVICES.
     */
    public function actionUpdate()
    {
        $this->sendResponse(200, array('status' => 'OK', 'message' => 'ActionUpdate PUT.',
            $this->reqDataDecoded), $this->_format);
    }

    /**
     * Unique Public and generic method for deletion actions with  REST SERVICES. Probably overwrite
     * in children implementations of this class.
     */
    public function actionDelete()
    {
        $this->sendResponse(200, array('status' => 'OK', 'message' => 'actionDelete'), $this->_format);
    }

    /* ^^^^^END OF CUSTOM METHODS, OUTSIDE YII^^^^^ */

    /**
     * @param integer $code
     * @param string $additionalErrorMsg
     * @return mixed
     */
    protected function retErrorWs($code = null, $additionalErrorMsg = "")
    {
        if (!isset($code)) {
            $code = '555'; // generic error code
        }
        $aResponse = array($code => self::$aErrorCodes[$code] . " - " . $additionalErrorMsg);
        $this->aResponse = $aResponse;
        $this->isSuccess = 0;
        $this->errorCode = $code;
        return $aResponse[$code];
    }

    /**
     * @param integer $code
     * @param string $additionalErrorMsg
     * @return string
     */
    protected function getStatusHeader($code = null, $additionalErrorMsg = '')
    {
        $aResponse = array($code => self::$aErrorCodes[$code] . " - " . $additionalErrorMsg);
        return 'HTTP/1.1 ' . $code . ' ' . $aResponse[$code];
    }

    /**
     * @param string $signature
     * @param array $params
     *
     * @return bool
     */
    abstract protected function isValidSignature($signature, $params);

    /** Checks Signature in params received. Can be overridden in case the magic word is different
     *
     * @param $params
     *
     * @return string
     */
    protected function getSignature($params)
    {
        $joinParams = implode("", $params);
        $joinParams = HeMixed::getWord4MobileWs() . $joinParams;
        $signature = md5($joinParams);
        return $signature;
    }

    /**
     * Checks authentication for the requests. If none, just implement with return true;
     *
     * @return bool
     */
    abstract protected function checkIfLogined();

    /**
     * @param int $statusCode
     * @param string $body
     * @param string $contentType
     *
     * @return bool
     */
    protected function sendResponse($statusCode = 200, $body = '', $contentType = 'application/json')
    {
        if ($statusCode !== self::SUCCESS_CODE) {
            $this->retErrorWs($statusCode);
        }
        // Set the status http code:
        $statusHeader = $this->getStatusHeader($statusCode);
        header($statusHeader);
        // Add the content type:
        header('Content-type: ' . $contentType);
        // Format body based on content type:
        if (!is_array($body) && trim($body) == '') {
            if (array_search($statusCode, array_keys(self::$aErrorCodes)) !== false) {
                $body = self::$aErrorCodes[$statusCode];
            } else {
                $body = " Status code = " . $statusCode . ", undefined status code, no description.";
            }
        }
        switch ($contentType) {
            case 'text/html':
                $body = $body . " ($statusCode)";
                break;
            case 'json':
            case 'application/json':
                if ($statusCode == 200) {
                    if (is_array($body)) {
                        $body = CJSON::encode($body);
                    } else {
                        $body = CJSON::encode(array('httpCode' => 200,
                            'message' => $body));
                    }
                } else {
                    if (is_array($body)) {
                        $body = CJSON::encode(array_merge(array('httpCode' => $statusCode), $body));
                    } else {
                        $body = CJSON::encode(array('httpCode' => $statusCode, 'message' => $body));
                    }
                }
                break;
            default:
                $body = 'Undefined format and undefined status code.';
                break;
        }
        if ($statusCode == 200) {
            $this->logEnd($body, true);
        } else {
            $this->logEnd($body, false, $this->errorCode);
        }
        echo $body;
        Yii::app()->end();
        return true;
    }

    /**
     * @param $body
     * @return bool
     */
    public static function genericSendResponse($body)
    {
        header('Content-type: application/json');
        echo CJSON::encode($body);
        Yii::app()->end();
        return true;
    }

    /** At the moment we do not want this function to be overridden
     * @param      $aResponse
     * @param bool $success
     * @param int $errorCode
     *
     * @param string $log
     * @return bool
     */
    final protected function logEnd(
        $aResponse = null,
        $success = false,
        $errorCode = null,
        $log = "ENABLE_LOG_WEBSERVICES"
    ) {
        if (Yii::app()->config->get($log)) {
            if (is_null($aResponse)) {
                $aResponse = $this->aResponse;
            }
            if (!is_null($this->nameService) && is_subclass_of($this->moLogWs, 'CmLogWebService')) {
                $this->moLogWs->dateResponse = HeDate::todaySQL(true);
                $this->moLogWs->errorCode = $errorCode;
                $this->moLogWs->paramsResponse = substr(HeMixed::serializeToStoreInDb($aResponse),0,299);
                $this->moLogWs->successResponse = $success;
                if (!$this->moLogWs->updateLogEndRequest()) {
                    HeLogger::sendLog(
                        "Model Error SQL update LogWebservice",
                        HeLogger::IT_BUGS,
                        HeLogger::CRITICAL,
                        " Function moLogWs->updateEndRequest() has raised an error. Please review."
                    );
                }
            }
        }
        return true;
    }

    /** At the moment we do not want this function to be overridden
     * @param string $log
     * @return bool
     */
    final protected function logStart($log = "ENABLE_LOG_WEBSERVICES")
    {
        if (Yii::app()->config->get($log)) {
            $this->moLogWs = new LogWebService();
            $this->moLogWs->getLogById();
            $this->moLogWs->dateRequest = HeDate::todaySQL(true);
            $this->moLogWs->ipFromRequest = Yii::app()->request->getUserHostAddress();
            $this->moLogWs->appNameFromRequest = HeMixed::getNameKnownHost(Yii::app()->request->getUserHostAddress());
            $this->moLogWs->urlRequest = Yii::app()->request->getHostInfo() . Yii::app()->request->getUrl();
            $this->moLogWs->paramsRequest = '';
            if (isset($_SERVER["HTTP_PHP_AUTH_TOKEN"])) {
                $this->moLogWs->paramsRequest .= 'HTTP_PHP_AUTH_TOKEN=' . $_SERVER["HTTP_PHP_AUTH_TOKEN"] . ';';
            }
            $postdata = file_get_contents("php://input");
            if (!empty($postdata)) {
                $this->moLogWs->paramsRequest .= $postdata;
            } else {
                $postdata = HeMixed::getPostArgs();
                $postdata = implode(";", $postdata);
                $this->moLogWs->paramsRequest .= $postdata;
            }
            if (!is_null($this->action->id)) {
                $this->nameService = $this->action->id;
            } else {
                $this->nameService = $this->retServiceName();
            }
            $this->moLogWs->nameService = $this->nameService;
            $this->moLogWs->successResponse = false;
            $this->moLogWs->errorCode = null;
            $this->moLogWs->insertLog();
        }
        return true;
    }

    /**
     * It extracts the name of the web service from one of the params of CGI variables
     * @return string
     */
    protected function retServiceName()
    {
        $nameService = HeUtils::getSuperGlobal("SERVER", "REQUEST_URI");
        return $nameService;
    }

    /**
     * BUG #5725
     *
     * Cuando YII_DEBUG está habilitado y se utiliza showInFireBug en el CWebLogRoute aparece código HTML al final de
     * cada JSON de cada llamada REST.
     *
     */
    protected function disableCWebLogRoute()
    {
        if (YII_DEBUG) {
            foreach (Yii::app()->log->routes as $route) {
                if ($route instanceof CWebLogRoute) {
                    $route->enabled = false; // disable any weblogroutes
                }
            }
        }
    }
}
