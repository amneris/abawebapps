<?php

class AbaCAssetManager extends CAssetManager
{
    /**
     *
     * Force to always hashByName instead of using the dirname of the path being published AND path mtime.
     * Given our git based provisioning configuration the mtime of the files is ALWAYS DIFFERENT on each server.
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function publish($path, $hashByName = false, $level = -1, $forceCopy = null)
    {
        return parent::publish($path, true, $level, $forceCopy);
    }
}
