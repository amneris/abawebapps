<?php

class HeDateTest  extends CTestCase
{

    /**
     * @return bool
     */
    public function testGetDateTimeAdd( )
    {
        $dateToTest = '2014-02-28 09:00:01';
        $dateSubstracted = HeDate::getDateTimeAdd($dateToTest, -2, HeDate::HOURS);
        $this->assertEquals('2014-02-28 07:00:01', $dateSubstracted);

        $dateSubstracted = HeDate::getDateTimeAdd($dateToTest, 24, HeDate::HOURS);
        $this->assertEquals('2014-03-01 09:00:01', $dateSubstracted);

        return true;
    }


    /**
     *
     */
    public function testChangeFromUTCTo()
    {
        $dateToTestUTC = '2014-04-28 07:00:01';
        $dateSubstracted = HeDate::changeFromUTCTo($dateToTestUTC);
        $this->assertEquals('2014-04-28 09:00:01', $dateSubstracted);

        $dateToTestUTC = '2014-10-31 22:00:00';
        $dateSubstracted = HeDate::changeFromUTCTo($dateToTestUTC, 'Europe/Moscow');
        $this->assertEquals('2014-11-01 02:00:00', $dateSubstracted);

        return true;
    }


}
