<?php
/**
 * CmUserObjective
 * @author: mgadegaard
 * Date: 24/07/2015
 * Time: 18:23
 * © ABA English 2015
 *
 * This is the model class for table "user_objective".
 *
 * The followings are the available columns in table 'user_objective':
 * @property string $userId
 * @property string $id
 * @property integer $travel
 * @property integer $estudy
 * @property integer $work
 * @property integer $level
 * @property string $selEngPrev
 * @property integer $engPrev
 * @property string $dedication
 * @property string $birthDate
 * @property string $gender20130327
 * @property integer $others
 */
class CmUserObjective extends AbaActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_objective';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('userId', 'required'),
            array('travel, estudy, work, level, engPrev, others', 'numerical', 'integerOnly'=>true),
            array('userId', 'length', 'max'=>4),
            array('selEngPrev, dedication, birthDate', 'length', 'max'=>50),
            array('gender20130327', 'length', 'max'=>1),
            array(
                'userId, id, travel, estudy, work, level, selEngPrev, engPrev, dedication, birthDate, gender20130327, '.
                'others',
                'safe',
                'on'=>'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'userId' => 'user id FK to users',
            'id' => 'ID',
            'travel' => 'Why do u want to learn: To travel 1/0',
            'estudy' => 'Why do u want to learn: To study 1/0',
            'work' => 'Why do u want to learn: To work 1/0',
            'level' => 'It might not be the same as user.currentLevel',
            'selEngPrev' => 'Where did u study: 1,Colegio2,Academia3,Online4,Clases particulares5,Otros',
            'engPrev' => 'did u study English before: Yes or no',
            'dedication' => '1,Estudio/2,Trabajo/3,Busco trabajo',
            'birthDate' => 'Birth Date',
            'gender20130327' => 'Renamed bcoz we ve moved it to users',
            'others' => 'Why do u want to learn: Orther 1/0',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('userId', $this->userId, true);
        $criteria->compare('id', $this->id, true);
        $criteria->compare('travel', $this->travel);
        $criteria->compare('estudy', $this->estudy);
        $criteria->compare('work', $this->work);
        $criteria->compare('level', $this->level);
        $criteria->compare('selEngPrev', $this->selEngPrev, true);
        $criteria->compare('engPrev', $this->engPrev);
        $criteria->compare('dedication', $this->dedication, true);
        $criteria->compare('birthDate', $this->birthDate, true);
        $criteria->compare('gender20130327', $this->gender20130327, true);
        $criteria->compare('others', $this->others);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmUserObjective the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function mainFields()
    {
        return " uo.`userId`, uo.`id`, uo.`travel`, uo.`estudy`, uo.`work`, uo.`others`, uo.`level`, uo.`selEngPrev`,
                 uo.`engPrev`, uo.`dedication`,
                 CONCAT(IF(uo.`travel`=1,'travel,',''),
                        IF(uo.`estudy`=1,'study,',''),
                        IF(uo.`work`=1,'work,',''),
                        IF(uo.`others`=1,'other,','') ) as `whyStudy`,
                 CASE uo.`level`
                     WHEN '1'
                     THEN 'medium' WHEN '2' THEN 'high' WHEN '3' THEN 'native'
                 END as `levelToReach`,
                 CASE uo.`selEngPrev` WHEN 1 THEN 'school'
                     WHEN 2 THEN 'academy'
                     WHEN 3 THEN 'online'
                     WHEN 4 THEN 'personal class'
                     WHEN 5 THEN 'other'
                      ELSE 'no'
                 END as `previousEngExperience`,
                 CASE uo.`dedication`
                     WHEN 1 THEN 'study' WHEN 2 THEN 'work' WHEN 3 THEN 'looking for a job'
                 END as `occupationText`";
    }

    /**
     * Selects and populates User Objective object for user defined by input parameter userId
     *
     * @param $userId
     *
     * @return CmUserObjective|bool
     */
    public function getObjectivesByUserId($userId)
    {
        $sql= " SELECT  ".$this->mainFields()." FROM ".$this->tableName()." uo WHERE uo.`userId`=:USERID ";
        $paramsToSQL = array( ":USERID" => $userId );

        $dataReader=$this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }

        return false;
    }
}
