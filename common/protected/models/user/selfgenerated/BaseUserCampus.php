<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $name
 * @property string $surnames
 * @property string $email
 * @property string $password
 * @property string $complete
 * @property string $city
 * @property integer $countryId
 * @property string $countryIdCustom
 * @property string $birthDate
 * @property string $telephone
 * @property string $langEnv
 * @property string $langCourse
 * @property string $userType
 * @property string $currentLevel
 * @property string $entryDate
 * @property string $expirationDate
 * @property string $teacherId
 * @property string $idPartnerFirstPay
 * @property string $releaseCampus
 * @property string $idPartnerSource
 * @property string $idPartnerCurrent
 * @property integer $firstLoginDone
 * @property integer $savedUserObjective
 * @property string $keyExternalLogin
 * @property string $gender
 * @property string $cancelReason
 * @property string $registerUserSource
 * @property string $hasWorkedCourse
 * @property string $dateLastSentSelligent
 * @property string $lastActionFixSql
 * @property string $followUpAtFirstPay
 * @property string $watchedVideosAtFirstPay
 * @property string $idSourceList
 * @property string $deviceTypeSource
 * @property string $dateUpdateEmma
 * @property string $lastLoginTime
 * @property string $fbId
 */
class BaseUserCampus extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userType', 'required'),
			array('countryId, firstLoginDone, savedUserObjective', 'numerical', 'integerOnly'=>true),
			array('name, surnames, email, city', 'length', 'max'=>200),
			array('password, lastActionFixSql', 'length', 'max'=>50),
			array('complete', 'length', 'max'=>3),
			array('countryIdCustom', 'length', 'max'=>4),
			array('telephone', 'length', 'max'=>20),
			array('langEnv, langCourse, currentLevel, idSourceList', 'length', 'max'=>5),
			array('userType, watchedVideosAtFirstPay', 'length', 'max'=>2),
			array('releaseCampus, cancelReason, followUpAtFirstPay', 'length', 'max'=>10),
			array('keyExternalLogin', 'length', 'max'=>100),
			array('gender, registerUserSource, hasWorkedCourse, deviceTypeSource', 'length', 'max'=>1),
			array('fbId', 'length', 'max'=>32),
			array('birthDate, entryDate, expirationDate, dateLastSentSelligent, dateUpdateEmma, lastLoginTime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, surnames, email, password, complete, city, countryId, countryIdCustom, birthDate, telephone, langEnv, langCourse, userType, currentLevel, entryDate, expirationDate, teacherId, idPartnerFirstPay, releaseCampus, idPartnerSource, idPartnerCurrent, firstLoginDone, savedUserObjective, keyExternalLogin, gender, cancelReason, registerUserSource, hasWorkedCourse, dateLastSentSelligent, lastActionFixSql, followUpAtFirstPay, watchedVideosAtFirstPay, idSourceList, deviceTypeSource, dateUpdateEmma, lastLoginTime, fbId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'surnames' => 'Surnames',
			'email' => 'Email',
			'password' => 'Password',
			'complete' => 'Complete',
			'city' => 'City',
			'countryId' => 'Country',
			'countryIdCustom' => 'Country Id Custom',
			'birthDate' => 'Birth Date',
			'telephone' => 'Telephone',
			'langEnv' => 'Lang Env',
			'langCourse' => 'Lang Course',
			'userType' => 'User Type',
			'currentLevel' => 'Current Level',
			'entryDate' => 'Entry Date',
			'expirationDate' => 'Expiration Date',
			'teacherId' => 'Teacher',
			'idPartnerFirstPay' => 'Id Partner First Pay',
			'releaseCampus' => 'Release Campus',
			'idPartnerSource' => 'Id Partner Source',
			'idPartnerCurrent' => 'Id Partner Current',
			'firstLoginDone' => 'First Login Done',
			'savedUserObjective' => 'Saved User Objective',
			'keyExternalLogin' => 'Key External Login',
			'gender' => 'Gender',
			'cancelReason' => 'Cancel Reason',
			'registerUserSource' => 'Register User Source',
			'hasWorkedCourse' => 'Has Worked Course',
			'dateLastSentSelligent' => 'Date Last Sent Selligent',
			'lastActionFixSql' => 'Last Action Fix Sql',
			'followUpAtFirstPay' => 'Follow Up At First Pay',
			'watchedVideosAtFirstPay' => 'Watched Videos At First Pay',
			'idSourceList' => 'Id Source List',
			'deviceTypeSource' => 'Device Type Source',
			'dateUpdateEmma' => 'Date Update Emma',
			'lastLoginTime' => 'Last Login Time',
			'fbId' => 'Fb',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('surnames',$this->surnames,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('complete',$this->complete,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('countryId',$this->countryId);
		$criteria->compare('countryIdCustom',$this->countryIdCustom,true);
		$criteria->compare('birthDate',$this->birthDate,true);
		$criteria->compare('telephone',$this->telephone,true);
		$criteria->compare('langEnv',$this->langEnv,true);
		$criteria->compare('langCourse',$this->langEnv,true);
		$criteria->compare('userType',$this->userType,true);
		$criteria->compare('currentLevel',$this->currentLevel,true);
		$criteria->compare('entryDate',$this->entryDate,true);
		$criteria->compare('expirationDate',$this->expirationDate,true);
		$criteria->compare('teacherId',$this->teacherId,true);
		$criteria->compare('idPartnerFirstPay',$this->idPartnerFirstPay,true);
		$criteria->compare('releaseCampus',$this->releaseCampus,true);
		$criteria->compare('idPartnerSource',$this->idPartnerSource,true);
		$criteria->compare('idPartnerCurrent',$this->idPartnerCurrent,true);
		$criteria->compare('firstLoginDone',$this->firstLoginDone);
		$criteria->compare('savedUserObjective',$this->savedUserObjective);
		$criteria->compare('keyExternalLogin',$this->keyExternalLogin,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('cancelReason',$this->cancelReason,true);
		$criteria->compare('registerUserSource',$this->registerUserSource,true);
		$criteria->compare('hasWorkedCourse',$this->hasWorkedCourse,true);
		$criteria->compare('dateLastSentSelligent',$this->dateLastSentSelligent,true);
		$criteria->compare('lastActionFixSql',$this->lastActionFixSql,true);
		$criteria->compare('followUpAtFirstPay',$this->followUpAtFirstPay,true);
		$criteria->compare('watchedVideosAtFirstPay',$this->watchedVideosAtFirstPay,true);
		$criteria->compare('idSourceList',$this->idSourceList,true);
		$criteria->compare('deviceTypeSource',$this->deviceTypeSource,true);
		$criteria->compare('dateUpdateEmma',$this->dateUpdateEmma,true);
		$criteria->compare('lastLoginTime',$this->lastLoginTime,true);
		$criteria->compare('fbId',$this->fbId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseCampusUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
