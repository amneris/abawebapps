<?php

/**
 * CmAbaUserProgressQueues
 *
 * This is the model class for table "user_progress_queues".
 *
 * The followings are the available columns in table 'user_progress_queues':
 * @property integer $queueId
 * @property integer $userId
 * @property string $units
 * @property integer $evaluation
 * @property string $dateAdd
 * @property string $dateStart
 * @property string $dateEnd
 * @property integer $status
 */
class CmAbaUserProgressQueues extends AbaActiveRecord
{
    const QUEUE_STATUS_PENDING = 0;
    const QUEUE_STATUS_INPROCESS = 10;
    const QUEUE_STATUS_FAIL = 20;
    const QUEUE_STATUS_PROCESSED = 30;
    const QUEUE_STATUS_DELETED = 40;

    const SECTION_NAME_WRITING = 'WRITING';
    const SECTION_NAME_NEWWORDS = 'NEWWORDS';
    const SECTION_NAME_GRAMMAR = 'GRAMMAR';
    const SECTION_NAME_ROLEPLAY = 'ROLEPLAY';
    const SECTION_NAME_DICTATION = 'DICTATION';
    const SECTION_NAME_STUDY = 'STUDY';
    const SECTION_NAME_ABAFILM = 'ABAFILM';
    const SECTION_NAME_MINITEST = 'MINITEST';

    const SECTION_NAME_ACTION_LISTENED = 'LISTENED';
    const SECTION_NAME_ACTION_RECORDED = 'RECORDED';
    const SECTION_NAME_ACTION_DICTATION = 'DICTATION';
    const SECTION_NAME_ACTION_WRITTEN = 'WRITTEN';

    const SECTION_ID_MINITEST = 8;

    private $firstUnitsLevel = array(
      'beginner' => 1,
      'lowerIntermediate' => 25,
      'intermediate' => 49,
      'upperIntermediate' => 73,
      'advanced' => 97,
      'business' => 121
    );

    private $sections = array(
      array(
        'typeId' => 1,
        'name' => self::SECTION_NAME_ABAFILM,
        'progressField' => 'sit_por',
        'hasTest' => false,
        'phrasesType' => 'none',
        'actions' => array()
      ),
      array(
        'typeId' => 2,
        'name' => self::SECTION_NAME_STUDY,
        'progressField' => 'stu_por',
        'hasTest' => true,
        'phrasesType' => 'complex',
        'actions' => array(self::SECTION_NAME_ACTION_LISTENED, self::SECTION_NAME_ACTION_RECORDED)
      ),
      array(
        'typeId' => 3,
        'name' => self::SECTION_NAME_DICTATION,
        'progressField' => 'dic_por',
        'hasTest' => false,
        'phrasesType' => 'simple',
        'actions' => array(self::SECTION_NAME_ACTION_DICTATION)
      ),
      array(
        'typeId' => 4,
        'name' => self::SECTION_NAME_ROLEPLAY,
        'progressField' => 'rol_por',
        'hasTest' => false,
        'phrasesType' => 'simple',
        'actions' => array(self::SECTION_NAME_ACTION_LISTENED, self::SECTION_NAME_ACTION_RECORDED)
      ),
      array(
        'typeId' => 5,
        'name' => self::SECTION_NAME_GRAMMAR,
        'progressField' => 'gra_vid',
        'hasTest' => false,
        'phrasesType' => 'none',
        'actions' => array()
      ),
      array(
        'typeId' => 6,
        'name' => self::SECTION_NAME_WRITING,
        'progressField' => 'wri_por',
        'hasTest' => false,
        'phrasesType' => 'complex',
        'actions' => array(self::SECTION_NAME_ACTION_WRITTEN)
      ),
      array(
        'typeId' => 7,
        'name' => self::SECTION_NAME_NEWWORDS,
        'progressField' => 'new_por',
        'hasTest' => true,
        'phrasesType' => 'simple',
        'actions' => array(self::SECTION_NAME_ACTION_LISTENED, self::SECTION_NAME_ACTION_RECORDED)
      ),
      array(
        'typeId' => 8,
        'name' => self::SECTION_NAME_MINITEST,
        'progressField' => 'eva_por',
        'hasTest' => false,
        'phrasesType' => 'none',
        'actions' => array()
      )
    );


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_progress_queues';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaUserProgressQueues the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
          array('userId, units', 'required'),
          array(
            'queueId, userId, status, evaluation',
            'numerical',
            'integerOnly' => true
          ),
          array('units, dateAdd', 'type', 'type' => 'string', 'allowEmpty' => false),
          array('dateStart, dateEnd', 'type', 'type' => 'string', 'allowEmpty' => true),
          array(
            'queueId, userId, units, evaluation, dateAdd, dateStart, dateEnd, status',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " upq.`queueId`, upq.`userId`, upq.`units`, upq.`evaluation`, upq.`dateAdd`, upq.`dateStart`, upq.`dateEnd`, upq.`status` ";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }


    /**
     * @param $iQueueId
     *
     * @return $this|bool
     */
    public function getUserProgressQueueById($iQueueId)
    {

        $paramsToSQL = array(
          ":QUEUEID" => $iQueueId,
        );

        $sSql = "
            SELECT
                " . $this->mainFields() . "
            FROM " . $this->tableName() . " upq
            WHERE upq.queueId = :QUEUEID
            LIMIT 0, 1
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }


    /**
     * @param int $iStatus
     * @param int $iLimitOffset
     *
     * @return array
     */
    public function getUserProgressQueuesForProcess(
      $iStatus = self::QUEUE_STATUS_PENDING,
      $iLimitOffset = 5
    ) {

        $paramsToSQL = array(
          ":QUEUESTATUS" => $iStatus,
          ":LIMITOFFSET" => $iLimitOffset,
        );

        $sSql = "
            SELECT
                " . $this->mainFields() . "
            FROM " . $this->tableName() . " upq
            WHERE upq.status = :QUEUESTATUS
            ORDER BY upq.`queueId` ASC
            LIMIT 0, :LIMITOFFSET
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        $stUserProgressQueues = array();

        if (!$dataReader) {
            return $stUserProgressQueues;
        }

        while (($row = $dataReader->read()) !== false) {
            $stUserProgressQueues[] = $row;
        }
        return $stUserProgressQueues;
    }


    /**
     * @return bool
     *
     * @throws CDbException
     */
    public function updateToInProgress($iQueueId, $sDateStart = '', $sDateEnd = '')
    {

        $sSqlDateStart = '';
        $sSqlDateEnd = '';

        if (trim($sDateStart) <> '') {
            $sSqlDateStart = "
                , `dateStart` = '" . $sDateStart . "'
            ";
        }

        if (trim($sDateEnd) <> '') {
            $sSqlDateEnd = "
                , `dateEnd` = '" . $sDateEnd . "'
            ";
        }

        $this->beginTrans();
        $sSql = "
            UPDATE " . $this->tableName() . "
                SET `status`='" . self::QUEUE_STATUS_INPROCESS . "'
                " . $sSqlDateStart . " " . $sSqlDateEnd . "
            WHERE `queueId` = '" . $iQueueId . "'
        ; ";
        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }


    /**
     * @return bool
     *
     * @throws CDbException
     */
    public function updateToFail($iQueueId, $sDateStart = '', $sDateEnd = '')
    {

        $sSqlDateStart = '';
        $sSqlDateEnd = '';

        if (trim($sDateStart) <> '') {
            $sSqlDateStart = "
                , `dateStart` = '" . $sDateStart . "'
            ";
        }

        if (trim($sDateEnd) <> '') {
            $sSqlDateEnd = "
                , `dateEnd` = '" . $sDateEnd . "'
            ";
        }

        $this->beginTrans();
        $sSql = "
            UPDATE " . $this->tableName() . "
                SET `status`='" . self::QUEUE_STATUS_FAIL . "'
                " . $sSqlDateStart . " " . $sSqlDateEnd . "
            WHERE `queueId` = '" . $iQueueId . "'
        ; ";
        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }


    /**
     * @return bool
     *
     * @throws CDbException
     */
    public function updateToSuccess($iQueueId, $sDateStart = '', $sDateEnd = '')
    {

        $sSqlDateStart = '';
        $sSqlDateEnd = '';

        if (trim($sDateStart) <> '') {
            $sSqlDateStart = "
                , `dateStart` = '" . $sDateStart . "'
            ";
        }

        if (trim($sDateEnd) <> '') {
            $sSqlDateEnd = "
                , `dateEnd` = '" . $sDateEnd . "'
            ";
        }

        $this->beginTrans();
        $sSql = "
            UPDATE " . $this->tableName() . "
                SET `status`='" . self::QUEUE_STATUS_PROCESSED . "'
                " . $sSqlDateStart . " " . $sSqlDateEnd . "
            WHERE `queueId` = '" . $iQueueId . "'
        ; ";
        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }


    /**
     * @return bool
     *
     * @throws CDbException
     */
    public function updateToDeleted($iQueueId, $sDateStart = '', $sDateEnd = '')
    {

        $sSqlDateStart = '';
        $sSqlDateEnd = '';

        if (trim($sDateStart) <> '') {
            $sSqlDateStart = "
                , `dateStart` = '" . $sDateStart . "'
            ";
        }

        if (trim($sDateEnd) <> '') {
            $sSqlDateEnd = "
                , `dateEnd` = '" . $sDateEnd . "'
            ";
        }

        $this->beginTrans();
        $sSql = "
            UPDATE " . $this->tableName() . "
                SET `status`='" . self::QUEUE_STATUS_DELETED . "'
                " . $sSqlDateStart . " " . $sSqlDateEnd . "
            WHERE `queueId` = '" . $iQueueId . "'
        ; ";
        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }


    /**
     * @return bool
     */
    public function startProcessQueue()
    {
        try {
            $sDateStart = date("Y-m-d h:i:s", time());

            $this->updateToInProgress($this->queueId, $sDateStart);

        } catch (Exception $e) {
            return false;
        }
        return true;
    }


    /**
     * @param bool|true $bSuccess
     *
     * @return bool
     */
    public function endProcessQueue($bSuccess = true)
    {
        try {
            $sDateStart = date("Y-m-d h:i:s", time());

            if ($bSuccess) {
                $this->updateToSuccess($this->queueId, "", $sDateStart);

                //
                $oAbaUserProgressQueues = new AbaUserProgressQueuesDetails();
                $oAbaUserProgressQueues->updateAllToSuccess($this->queueId, '', $sDateStart);

            } else {
                $this->updateToFail($this->queueId, "", $sDateStart);

                //
                $oAbaUserProgressQueues = new AbaUserProgressQueuesDetails();
                $oAbaUserProgressQueues->updateAllToFail($this->queueId, '', $sDateStart);

            }
        } catch (Exception $e) {
            return false;
        }
        return true;
    }



    /**
     * @param null $userId
     * @param array $unit
     *
     * @return mixed
     */
    private function getUsersProgressToFill($userId = null, $unit = array())
    {
        $usersProgressToFill = array();

        //
        $stFollowupVersionData = $this->getUserFollowupVersion($userId);

        $paramsToSQL = array(
          ":USERID" => $userId,
        );

        $sSql2 = "
            SELECT
              f.`userid` AS `userId`, f.`themeid` AS `unit`,
              IF(f.`" . $this->sections[0]['progressField'] . "` < 100, 1, 0) AS `" . $this->sections[0]['name'] . "`,
              IF(f.`" . $this->sections[1]['progressField'] . "` < 100, 1, 0) AS `" . $this->sections[1]['name'] . "`,
              IF(f.`" . $this->sections[2]['progressField'] . "` < 100, 1, 0) AS `" . $this->sections[2]['name'] . "`,
              IF(f.`" . $this->sections[3]['progressField'] . "` < 100, 1, 0) AS `" . $this->sections[3]['name'] . "`,
              IF(f.`" . $this->sections[4]['progressField'] . "` < 100, 1, 0) AS `" . $this->sections[4]['name'] . "`,
              IF(f.`" . $this->sections[5]['progressField'] . "` < 100, 1, 0) AS `" . $this->sections[5]['name'] . "`,
              IF(f.`" . $this->sections[6]['progressField'] . "` < 100, 1, 0) AS `" . $this->sections[6]['name'] . "`,
              IF(f.`" . $this->sections[7]['progressField'] . "` < 10, 1, 0) AS `" . $this->sections[7]['name'] . "`
              " . "
            FROM
                aba_b2c.`" . $stFollowupVersionData['followup4table'] . "` f
            WHERE
                f.`userid` = :USERID
        ";

        foreach ($unit as $iKey => $iValue) {
            $unit[$iKey] = trim($iValue);
        }
        $sUnits = implode(", ", $unit);

        if (trim($sUnits) <> '') {

            $sSql2 .= "
                AND f.themeid IN (" . $sUnits . ")
            ";
        }

//self::printSql($sSql2, $paramsToSQL);

        $stDataReader = $this->querySQL($sSql2, $paramsToSQL);

        while (($row = $stDataReader->read()) !== false) {

            if (!isset($usersProgressToFill[$row['userId']])) {
                $usersProgressToFill[$row['userId']] = array();
            }

            if (!isset($usersProgressToFill[$row['userId']][$row['unit']])) {
                $usersProgressToFill[$row['userId']][$row['unit']] = array();
            }

            foreach ($this->sections as $section) {
                if ($row[$section['name']] == 1) {
                    array_push($usersProgressToFill[$row['userId']][$row['unit']], $section);
                }
            }
        }

        return $usersProgressToFill;
    }


    /**
     * @param int $unit
     *
     * @return bool
     */
    private function isFirstUnitLevel($unit = 0)
    {
        return in_array($unit, $this->firstUnitsLevel);
    }


    /**
     * @param int $userId
     *
     * @return array
     */
    private function getUserCourseInfo($userId = 0)
    {

        $stUser = array(
          'id' => $userId,
          'course' => 0,
          'language' => null,
          'progressTable' => 'followup4',
          'progressHtmlTable' => 'followup_html4'
        );

        $paramsToSQL = array(
          ":USERID" => $userId,
        );

        $sSql = "
            SELECT
              ct.`courseId`
              " . "
            FROM aba_b2c.`course_test` ct

            JOIN aba_b2c.`course_test_user` ctu
                ON ctu.`courseId` = ct.`id`

            WHERE
                ctu.`userId` = :USERID

            LIMIT 0, 1
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if ($dataReader) {
            while (($row = $dataReader->read()) !== false) {
                $stUser['course'] = $row['courseId'];
                break;
            }
        }

        $sSql = "
            SELECT
              us.`langEnv`
              " . "
            FROM aba_b2c.`user` us

            WHERE
                us.`id` = :USERID

            LIMIT 0, 1
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if ($dataReader) {
            while (($row = $dataReader->read()) !== false) {
                $stUser['language'] = $row['langEnv'];
                break;
            }
        }

        //
        $stFollowupVersionData = $this->getUserFollowupVersion($userId);

        $stUser['progressTable'] = $stFollowupVersionData['followup4table'];
        $stUser['progressHtmlTable'] = $stFollowupVersionData['followupHtml4table'];

        return $stUser;
    }


    /**
     * @param int $userId
     *
     * @return array
     */
    private function getUserFollowupVersion($userId = 0)
    {

        $paramsToSQL = array(
          ":USERID" => $userId,
        );

        $sSql = "
            SELECT
              ud.`progressVersion`, ud.`progressVersionHtml`
              " . "
            FROM aba_b2c.`user_dictionary` ud
            WHERE
                ud.`idUser` = :USERID
            LIMIT 0, 1
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        $stFollowupVersionData = array(
          'followup4version' => '',
          'followupHtml4version' => '',
          'followup4table' => 'followup4',
          'followupHtml4table' => 'followup_html4',
        );

        if ($dataReader) {
            while (($row = $dataReader->read()) !== false) {
                $stFollowupVersionData['followup4version'] = $row['progressVersion'];
                $stFollowupVersionData['followupHtml4version'] = $row['progressVersionHtml'];
                $stFollowupVersionData['followup4table'] = $stFollowupVersionData['followup4table'] . '_' . $row['progressVersion'];
                $stFollowupVersionData['followupHtml4table'] = $stFollowupVersionData['followupHtml4table'] . '_' . $row['progressVersionHtml'];
                break;
            }
        }

        return $stFollowupVersionData;
    }


    /**
     * @param $sLanguage
     * @param $unitId
     * @param $sSection
     *
     * @return string
     */
    private function getPhraseTable($sLanguage, $unitId, $sSection)
    {

        if (
          ($unitId >= $this->firstUnitsLevel['advanced'] && $sLanguage == 'fr')
          ||
          ($unitId >= $this->firstUnitsLevel['upperIntermediate'] && (in_array(trim($sLanguage),
              array('ru', 'zh', 'de'))))
        ) {

            $phraseTable = 'phrase_en';
        } else {

            if ($sLanguage == 'es' || $sSection['phrasesType'] == 'simple') {

                $phraseTable = 'phrase';
            } else {

                $phraseTable = 'phrase_' . $sLanguage;
            }
        }

        return $phraseTable;
    }


    /**
     * @param $stUser
     * @param $unitId
     * @param array $stSections
     * @param bool|false $isTestUnit
     *
     * @return bool
     */
    private function getUnitProgressToFill($stUser, $unitId, $stSections = array(), $isTestUnit = false)
    {

        //
        try {
            $oAbaUserProgressQueuesDetail = new AbaUserProgressQueuesDetails();
            if ($oAbaUserProgressQueuesDetail->getUserProgressQueueDetailByQueueIdAndUnitId($this->queueId, $unitId)) {
                $oAbaUserProgressQueuesDetail->startProcessQueueDetail();
            }
        }
        catch(Exception $e) { }


        foreach ($stSections as $iKey => $stSection) {

            $sSectionTypeId = $stSection['typeId'];

            if ($stSection['phrasesType'] !== 'none') {

                $sPhraseTable = $this->getPhraseTable($stUser['language'], $unitId, $stSection);

                $paramsToSQL = array(
                  ":UNITID" => $unitId,
                  ":SECTIONTYPEID" => $sSectionTypeId,
                );

                $sSqlGroupBy = "
                    GROUP BY p.`audio`, p.`page`
                ";

                if ($stSection['name'] == self::SECTION_NAME_WRITING) {
                    $sSqlGroupBy = "
                        GROUP BY
                            p.`audio`, p.`posAudio`, p.`page`
                    ";
                }

                if ($isTestUnit && $stSection['hasTest']) {

                    $paramsToSQL[":TESTID"] = $stUser['course'];

                    $sSql = "
                        SELECT
                        " . "
                            s.`id` AS `sectionId`, p.`audio`, p.`page`, p.`posAudio`
                        FROM aba_course.`section` s
                        JOIN aba_course.`course_test_phrase` ctp
                            ON ctp.`idSection` = s.`id`
                        JOIN aba_course.`" . $sPhraseTable . "` p
                            ON p.`id` = ctp.`idPhrase`
                        WHERE
                            ctp.`idTest` = :TESTID
                            AND s.`idUnit` = :UNITID
                    ";
                } else {

                    $sSql = "
                        SELECT
                        " . "
                            s.`id` AS `sectionId`, p.`audio`, p.`page`, p.`posAudio`
                        FROM aba_course.`section` s
                        JOIN aba_course.`" . $sPhraseTable . "` p
                            ON p.`idSection` = s.`id`
                        WHERE
                            s.`idUnit` = :UNITID
                    ";
                }

                $sSql .= "
                            AND s.`idSectionType` = :SECTIONTYPEID
                            AND p.`audio` <> ''
                            AND p.`phraseType` <> 'example'
                            " . $sSqlGroupBy;

//self::printSql($sSql, $paramsToSQL);

                //
                //
                $dataReader = $this->querySQL($sSql, $paramsToSQL);

                if (!$dataReader) {
                    return false;
                }

                $this->fillSectionProgress($stUser, $unitId, $stSection, $dataReader);

            }

            $this->fillResumeProgress($stUser, $unitId, $stSection);
        }

        //
        try {
            $oAbaUserProgressQueuesDetail = new AbaUserProgressQueuesDetails();
            if ($oAbaUserProgressQueuesDetail->getUserProgressQueueDetailByQueueIdAndUnitId($this->queueId, $unitId)) {
                $oAbaUserProgressQueuesDetail->endProcessQueueDetail(true);
            }
        }
        catch(Exception $e) { }

        return true;
    }


    /**
     * @param $stUser
     * @param $unitId
     * @param $stSection
     * @param null $dataReader
     *
     * @return bool
     */
    private function fillSectionProgress($stUser, $unitId, $stSection, $dataReader = null)
    {

        // followup_html
        $sTableName = $stUser['progressHtmlTable'];
        $userId = $stUser['id'];
        $sSectionName = $stSection['name'];

        while (($rowProgress = $dataReader->read()) !== false) {

            $iPage = $rowProgress['page'];
            $sAudio = $rowProgress['audio'];

            if ($sSectionName == self::SECTION_NAME_WRITING) {
                $sAudio = $rowProgress['audio'] . '_' . $rowProgress['posAudio'];
            }

            foreach ($stSection['actions'] as $iKey => $action) {

                $sSql = "

                  INSERT INTO aba_b2c.`" . $sTableName . "`
                  ( `userid`, `themeid`, `section`, `page`, `audio`, `action`, `bien`, `mal`, `lastchange`, `wtext`, `difficulty`, `time_aux`, `contador` )
                  SELECT
                    '" . $userId . "', '" . $unitId . "', '" . $sSectionName . "', '" . $iPage . "', '" . $sAudio . "', '" . $action . "', 1, 0, NOW(), '', 0, 0, 1
                  FROM DUAL
                  WHERE NOT EXISTS (
                    SELECT f.`id`
                    FROM aba_b2c.`" . $sTableName . "` AS f
                    WHERE
                        f.`userid` = '" . $userId . "'
                        AND f.`themeid` = '" . $unitId . "'
                        AND f.`section` = '" . $sSectionName . "'
                        AND f.`audio` = '" . $sAudio . "'
                        AND f.`page` = '" . $iPage . "'
                        AND f.`action` = '" . $action . "'
                  );
                 ";


//self::printSql($sSql, array(), "INSERT");


                $iFollowuoId = $this->executeSQL($sSql);

                if($iFollowuoId <= 0) {
                    return false;
                }
            }
        }

        return true;
    }


    /**
     * @param $stUser
     * @param $unitId
     * @param $section
     *
     * @return bool
     */
    private function fillResumeProgress($stUser, $unitId, $section)
    {
        // followup
        $sTableName = $stUser['progressTable'];
        $sFieldName = $section['progressField'];
        $userId = $stUser['id'];

        $sSql = "
            UPDATE aba_b2c.`" . $sTableName . "` f
            SET
                f.`" . $sFieldName . "` = 100,
                f.`lastchange` = NOW(),
                f.`all_por` = FLOOR(
                    (
                        f.`" . $this->sections[0]['progressField'] . "` +
                        f.`" . $this->sections[1]['progressField'] . "` +
                        f.`" . $this->sections[2]['progressField'] . "` +
                        f.`" . $this->sections[3]['progressField'] . "` +
                        f.`" . $this->sections[4]['progressField'] . "` +
                        f.`" . $this->sections[5]['progressField'] . "` +
                        f.`" . $this->sections[6]['progressField'] . "`
                    ) / 7
                )
            WHERE
                f.`userid` = '" . $userId . "'
                AND f.`themeid` = '" . $unitId . "'
            ;
        ";

//self::printSql($sSql, array(), "UPDATE");

        try {
            $this->executeSQL($sSql);
        }
        catch(Exception $e) { }

        return true;
    }


    /**
     * @param $stUser
     * @param $unitId
     * @return bool
     *
     * @throws CDbException
     */
    private function insertResumeProgress($stUser, $unitId, $exercises="0,0,0,0,0,0,0,0,0,0", $eva_por=0)
    {

        try {
            $oAbaUserProgressQueuesDetail = new AbaUserProgressQueuesDetails();
            if ($oAbaUserProgressQueuesDetail->getUserProgressQueueDetailByQueueIdAndUnitId($this->queueId, $unitId)) {
                $oAbaUserProgressQueuesDetail->startProcessQueueDetail();
            }
        }
        catch(Exception $e) { }

        // followup
        $sTableName = $stUser['progressTable'];
        $userId = $stUser['id'];

        $sSql = "
            INSERT INTO aba_b2c.`" . $sTableName . "` " .
          " (
                `themeid`, `userid`, `lastchange`, `all_por`, `all_err`, `all_ans`, `all_time`,
                `sit_por`, `stu_por`, `dic_por`, `dic_ans`, `dic_err`,
                `rol_por`, `gra_por`, `gra_ans`, `gra_err`,
                `wri_por`,  `wri_ans`, `wri_err`,
                `new_por`, `spe_por`, `spe_ans`, `time_aux`,
                `gra_vid`, `rol_on`, `exercises`, `eva_por`
            )
            VALUES
            (
                '" . $unitId . "', '" . $userId . "', NOW(), 100, 0, 0, 0,
                100, 100, 100, 0, 0,
                100, 0, 0, 0,
                100, 0, 0,
                100, 0, 0, 0,
                100, 0, '" . $exercises . "', " . $eva_por . "
            )
        ";

//self::printSql($sSql, array(), "INSERT");

        try {
            $iFollowuoId = $this->executeSQL($sSql);

            $oAbaUserProgressQueuesDetail = new AbaUserProgressQueuesDetails();
            if ($oAbaUserProgressQueuesDetail->getUserProgressQueueDetailByQueueIdAndUnitId($this->queueId, $unitId)) {
                $oAbaUserProgressQueuesDetail->endProcessQueueDetail();
            }
        }
        catch(Exception $e) { }

        if ($iFollowuoId <= 0) {
            return false;
        }

        return true;
    }


    /**
     * @param int $userId
     * @param array $stUnits
     *
     * @return bool
     */
    private function loopThroughUnits($userId = 0, $stUnits = array())
    {

        $stUserData = $this->getUserCourseInfo($userId);

        foreach ($stUnits as $unitId => $sections) {

            $isTestUnit = false;

            if ($stUserData['course'] > 0 && $this->isFirstUnitLevel($unitId)) {

                $isTestUnit = true;
            }

            $this->getUnitProgressToFill($stUserData, $unitId, $sections, $isTestUnit);
        }

        return true;
    }


    /**
     * @return bool
     */
    public function fillTheProgress()
    {
        $iUserId = $this->userId;

        $stUnits = explode(",", $this->units);

        if (count($stUnits) <= 0) {
            return false;
        }

        /**
         * Exists followup4_XX rows
         *
         */
        $stUsersProgressToFillData = $this->getUsersProgressToFill($iUserId, $stUnits);

        //
        $stUsersProgressUnitsToFillForInsert = array();

        foreach ($stUnits as $iKey => $iUnit) {

            $bUnitExists = false;

            foreach ($stUsersProgressToFillData as $iKeyUserId => $stProgressUnits) {

                if ($iKeyUserId == $iUserId) {

                    foreach ($stProgressUnits as $iKeyUnit => $stSections) {

                        if ($iKeyUnit == $iUnit) {

                            $bUnitExists = true;
                            break;
                        }
                    }
                }
            }

            if (!$bUnitExists) {
                $stUsersProgressUnitsToFillForInsert[] = $iUnit;
            }
        }

        /**
         * INSERT NO EXISTS ROWS
         *
         */
        $stUserData = $this->getUserCourseInfo($iUserId);

        foreach ($stUsersProgressUnitsToFillForInsert as $iKey => $iUnitId) {

            //#ZOR-372
            $exercises = "0,0,0,0,0,0,0,0,0,0";
            $eva_por = 0;

            if($this->evaluation == 1) {

                $abaProgressEvaluationAnswers = new AbaProgressEvaluationAnswers();

                if($abaProgressEvaluationAnswers->getAnswerBySectionTypeAndUnitId(self::SECTION_ID_MINITEST, $iUnitId)) {
                    if(trim($abaProgressEvaluationAnswers->evaluation) <> '' AND count(explode(",", $abaProgressEvaluationAnswers->evaluation)) > 1) {
                        $exercises = trim($abaProgressEvaluationAnswers->evaluation);
                    }
                }

                $eva_por = 10;
            }

            $this->insertResumeProgress($stUserData, $iUnitId, $exercises, $eva_por);
        }

        /**
         * Process progress
         *
         */
        return $this->fillUsersProgress($stUsersProgressToFillData);
    }


    /**
     * @param array $usersProgressToFill
     *
     * @return bool
     */
    private function fillUsersProgress($usersProgressToFill = array())
    {
        foreach ($usersProgressToFill as $userId => $units) {
            $this->loopThroughUnits($userId, $units);
        }
        return true;
    }


    /**
     * @param $sSql
     * @param array $stParams
     * @param string $sType
     *
     * @return bool
     */
    public static function printSql($sSql, $stParams = array(), $sType = "SELECT")
    {

        $sSql2_2 = $sSql;
        foreach ($stParams as $iKey => $sValue) {
            $sSql2_2 = str_replace($iKey, "'" . $sValue . "'", $sSql2_2);
        }

        $sSql2_2 = trim($sSql2_2);
        $sSql2_2 = str_replace("\n", " ", $sSql2_2);
        $sSql2_2 = str_replace("\r", " ", $sSql2_2);
        $sSql2_2 = str_replace("\n\r", " ", $sSql2_2);
        $sSql2_2 = str_replace("\r\n", " ", $sSql2_2);

        echo "\n + + + PROGRESS::SQLTYPE=" . $sType . " + + + + + + + + + + + + + + + + + + \n";
        echo $sSql2_2;
        echo "\n";

//        error_log("\n + + + PROGRESS::SQLTYPE=" . $sType . "::SQL= " . $sSql2_2 . " + + + \n");

        return true;
    }


    /**
     * @return bool
     */
    public function saveDetails() {

        try {
            $stUnits = explode(",", $this->units);

            if (count($stUnits) > 0) {

                foreach($stUnits as $iKey => $iUnit) {
                    $oAbaUserProgressQueuesDetail = new AbaUserProgressQueuesDetails();

                    $oAbaUserProgressQueuesDetail->queueId = $this->queueId;
                    $oAbaUserProgressQueuesDetail->unitId = $iUnit;
                    $oAbaUserProgressQueuesDetail->dateStart = $this->dateStart;
                    $oAbaUserProgressQueuesDetail->dateEnd = $this->dateEnd;
                    $oAbaUserProgressQueuesDetail->status = AbaUserProgressQueuesDetails::QUEUEDETAIL_STATUS_PENDING;

                    $oAbaUserProgressQueuesDetail->insertUserProgressQueueDetail();
                }
            }
        }
        catch(Exception $e) {
            return false;
        }

        return true;
    }

}
