<?php

/**
 * This is the model class for table "user_zuora_invoices".
 *
 * The followings are the available columns in table 'user_zuora_invoices':
 * @property integer $id
 * @property integer $idUserZuora
 * @property string $paymentId
 * @property integer $userId
 * @property string $invoiceId
 * @property string $invoiceNumber
 * @property integer $status
 * @property string $dateAdd
 */
class CmAbaUserZuoraInvoices extends AbaActiveRecord
{
    const SUBSCRIPTION_STATUS_OK = 1;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_zuora_invoices';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
          array('dateAdd', 'required'),
          array('idUserZuora, userId, status', 'numerical', 'integerOnly' => true),
          array('paymentId, invoiceId, invoiceNumber', 'length', 'max' => 64),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
          array('id, idUserZuora, paymentId, userId, invoiceId, invoiceNumber, status, dateAdd', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'id' => 'ID',
          'idUserZuora' => 'User Zuora Id',
          'paymentId' => 'Payment Id',
          'userId' => 'User',
          'invoiceId' => 'Zuora Invoice ID',
          'invoiceNumber' => 'Zuora Invoice Number',
          'status' => 'Status',
          'dateAdd' => 'Date Add'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('idUserZuora', $this->idUserZuora);
        $criteria->compare('paymentId', $this->paymentId);
        $criteria->compare('userId', $this->userId);
        $criteria->compare('invoiceId', $this->invoiceId, true);
        $criteria->compare('invoiceNumber', $this->invoiceNumber, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('dateAdd', $this->dateAdd, true);

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaUserZuoraInvoices the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * All fields available in this model
     *
     * @return string
     */
    public function mainFields()
    {
        return "uzs.`id`, uzs.`idUserZuora`, uzs.`paymentId`, uzs.`userId`, uzs.`invoiceId`, uzs.`invoiceNumber`, uzs.`status`, uzs.`dateAdd`";
    }

    /**
     * @param $userId
     * @param $invoiceNumber
     *
     * @return $this|bool
     */
    public function getSubscriptionByNumber($userId, $invoiceNumber)
    {
        $sql = "
            SELECT " . $this->mainFields() . " " .
          " FROM " . $this->tableName() .
          " AS uzs " .
          " WHERE 
                uzs.invoiceNumber = :SUBSCRIPTIOIDENTIFIER
                AND uzs.userId = :USERID 
          " .
          " LIMIT 0, 1 
        ";

        $paramsToSQL = array(
          ":USERID" => $userId,
          ":SUBSCRIPTIOIDENTIFIER" => $invoiceNumber
        );

        $dataReader = $this->querySQL($sql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $userZuoraId
     * @param $paymentId
     * @param $userId
     * @param $invoiceId
     * @param $invoiceNumber
     *
     * @return bool
     * @throws CDbException
     */
    public function insertUserZuoraInvoice($userZuoraId, $paymentId, $userId, $invoiceId, $invoiceNumber)
    {
        $this->beginTrans();

        $sql = "
            INSERT INTO " . $this->tableName() . " (`idUserZuora`, `paymentId`, `userId`, `invoiceId`, `invoiceNumber`, `status`)
            VALUES (:IDUSERZUORA, :PAYMENTID, :USERID, :INVOICEID, :INVOICENUMBER, :STATUS)
        ";

        $aBrowserValues = array(
          ":IDUSERZUORA" => $userZuoraId,
          ":PAYMENTID" => $paymentId,
          ":USERID" => $userId,
          ":INVOICEID" => $invoiceId,
          ":INVOICENUMBER" => $invoiceNumber,
          ":STATUS" => self::SUBSCRIPTION_STATUS_OK
        );
        $idUser = $this->executeSQL($sql, $aBrowserValues);

        if ($idUser > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @param $invoiceId
     *
     * @return $this|bool
     */
    public function getSubscriptionByInvoiceId($invoiceId)
    {
        $sql = "
            SELECT " . $this->mainFields() . " " .
          " FROM " . $this->tableName() .
          " AS uzs " .
          " WHERE
                uzs.invoiceId = :INVOICEID
          " .
          " LIMIT 0, 1
        ";

        $paramsToSQL = array(
          ":INVOICEID" => $invoiceId
        );

        $dataReader = $this->querySQL($sql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return bool
     *
     * @throws CDbException
     */
    public function updateInvoiceNumber()
    {

        $aBrowserValues = array(
          ':INVOICENUMBER' => $this->invoiceNumber,
          ':INVOICEID' => $this->invoiceId,
          ':USERID' => $this->userId
        );

        $this->beginTrans();

        $sSql = "
            UPDATE " . $this->tableName() . " AS uzs
            SET
                uzs.`invoiceNumber`=:INVOICENUMBER
            WHERE
                uzs.`invoiceId` = :INVOICEID
                AND uzs.`userId` = :USERID
        ; ";

        if ($this->updateSQL($sSql, $aBrowserValues) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }


    /**
     * @param $invoiceId
     *
     * @return $this|bool
     */
    public function getSubscriptionInfoByInvoiceId($invoiceId)
    {
        $sSql = "
            SELECT
                " . $this->mainFields() . " " .
          "     , uz.zuoraSubscriptionNumber " .
          " FROM " . $this->tableName() . " AS uzs " .
          " JOIN user_zuora AS uz " .
          "     ON uzs.idUserZuora = uz.id " .
          "         AND uzs.userId = uz.userId " .
          " WHERE
                uzs.`invoiceId` = :INVOICEID
            ORDER BY uzs.id DESC
          " .
          " LIMIT 0, 1
        ";

        $paramsToSQL = array(
          ":INVOICEID" => $invoiceId
        );

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if (!$dataReader) {
            return array();
        }

        if (($row = $dataReader->read()) !== false) {
            return $row;
        }
        return array();

    }

}
