<?php
/**
 * @property integer $id
 * @property integer $userId
 * @property integer $default
 * @property string $name
 * @property string $lastName
 * @property string $vatNumber
 * @property string $street
 * @property string $streetMore
 * @property string $zipCode
 * @property string $city
 * @property string $state
 * @property string $country
 *
 */
class CmAbaUserAddressInvoiceForm extends AbaActiveRecord
{
    public $id;

    /**
     *
     */
    public function __construct($scenario=NULL, $dbConnName=NULL) {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_address_invoice';
    }

    /**
     * Before the save method is executed.
     * @return array
     */
    public function rules()
    {
        return array(
            array('userId',     'required'),
            array('default',    'required'),
            array('name',       'required'),
            array('lastName',   'required'),
            array('vatNumber',  'required'),
            array('street',     'required'),
            array('zipCode',    'required'),
            array('city',       'required'),
            array('state',      'required'),
            array('country',    'required'),
            array('userId, default', 'type', 'type'=>'integer', 'allowEmpty'=>false),
            array('name, lastName, vatNumber, street, zipCode, city, state', 'type', 'type'=>'string', 'allowEmpty'=>false),
//            array('country',    'type', 'type'=>'string', 'allowEmpty'=>false),
            array('streetMore', 'type', 'type'=>'string', 'allowEmpty'=>true),
        );
    }
//            array('cardCvc',      'isValidTPVRequired')

    /**
     * @return string
     */
    public function mainFields()
    {
        return "`id`, `userId`, `default`, `name`, `lastName`, `vatNumber`, `street`, `streetMore`, `zipCode`, `city`, `state`, `country` ";
    }

    /**
     * @param array $formParams
     *
     * @return bool
     */
    public function setUserAddressInvoiceForm(AbaUserAddressInvoice $moUserAddressInvoice)
    {
        $this->userId =     $moUserAddressInvoice->userId;
        $this->default =    $moUserAddressInvoice->default;
        $this->name =       $moUserAddressInvoice->name;
        $this->lastName =   $moUserAddressInvoice->lastName;
        $this->vatNumber =  $moUserAddressInvoice->vatNumber;
        $this->street =     $moUserAddressInvoice->street;
        $this->streetMore = $moUserAddressInvoice->streetMore;
        $this->zipCode =    $moUserAddressInvoice->zipCode;
        $this->city =       $moUserAddressInvoice->city;
        $this->state =      $moUserAddressInvoice->state;
        $this->country =    $moUserAddressInvoice->country;

        if( !$this->validate() ) {
            return false;
        }
        return true;
    }

}
