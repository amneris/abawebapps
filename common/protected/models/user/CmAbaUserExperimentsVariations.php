<?php

/**
 * CmAbaUserExperimentsVariations
 *
 * This is the model class for table "user_experiments_variations".
 *
 * The followings are the available columns in table 'user_experiments_variations':
 * @property integer $userExperimentVariationId
 * @property integer $userId
 * @property integer $experimentVariationId
 * @property integer $status
 * @property string $dateAdd
 */
class CmAbaUserExperimentsVariations extends AbaActiveRecord
{
    const USER_EXPERIMENT_VARIATION_STATUS_DEFAULT = 0;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_experiments_variations';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaProductPrice the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
          array('userId, experimentVariationId', 'required'),
          array('userExperimentVariationId, userId, experimentVariationId, status', 'numerical', 'integerOnly' => true),
          array('dateAdd', 'date', 'format' => 'yyyy-mm-dd hh:mm:ss'),
            // @todo Please remove those attributes that should not be searched.
          array('userExperimentVariationId, userId, experimentVariationId, status, dateAdd', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " uexv.`userExperimentVariationId`, uexv.`userId`, uexv.`experimentVariationId`, uexv.`status`, uexv.`dateAdd` ";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * Specific scenarios payments function
     *
     * @param $userId
     * @param $countryId
     * @param $experimentTypeId
     * @param bool|true $bEnabled
     *
     * @return array
     */
    public function getAllUserExperimentsVariationsForScenariosByUserId(
        $userId,
        $countryId,
        $experimentTypeId,
        $bEnabled = true
    ) {
        $paramsToSQL = array(
          ":USERID" => $userId,
          ":IDCOUNTRY" => $countryId,
          ":EXPERIMENTTYPE" => $experimentTypeId,
          ":STATUS" => self::USER_EXPERIMENT_VARIATION_STATUS_DEFAULT,
          ":EXPERIMENTSTATUS" => CmAbaExperiments::EXPERIMENT_STATUS_ENABLED
        );

        $sSql = "
            SELECT
                " . $this->mainFields() . ",
                exv.`experimentVariationId`,
                ex.`dateStart`, ex.`dateEnd`, ex.`userCategoryId`, ex.`languages`, ex.`availableModeId`,
                IF(NOW() BETWEEN ex.dateStart AND IFNULL(ex.dateEnd, NOW()), 1, 0) AS availableExperiment,
                exva.`experimentVariationAttributeId`, exva.`idProduct`, exva.`idCountry`, exva.`price`
                FROM " . $this->tableName() . " uexv
            JOIN experiments_variations AS exv
                ON exv.experimentVariationId = uexv.experimentVariationId
            JOIN experiments AS ex
                ON exv.experimentId = ex.experimentId
                AND ex.`experimentTypeId` = :EXPERIMENTTYPE
                AND ex.experimentStatus = :EXPERIMENTSTATUS
            JOIN experiments_variations_attributes_scenarios AS exva
                ON exva.experimentVariationId = uexv.experimentVariationId
                AND exva.idCountry = :IDCOUNTRY
        ";
//        AND NOW() BETWEEN ex.dateStart AND IFNULL(ex.dateEnd, NOW())

        if ($bEnabled) {
            $paramsToSQL[":ENABLED"] = 1;

            $sSql .= "
                AND exva.enabled = :ENABLED
            ";
        }

        $sSql .= "
            WHERE
                uexv.userId = :USERID
                AND uexv.`status` = :STATUS
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        $aProdsPrices = array();

        if (!$dataReader) {
            return $aProdsPrices;
        }

        while (($row = $dataReader->read()) !== false) {
            $aProdsPrices[] = $row;
        }
        return $aProdsPrices;
    }

    /**
     * @param $oAbaUser
     * @param null $experimentTypeId
     * @param bool|false $bCreateProfile
     *
     * @return array
     */
    public function getAllUserExperimentsVariationsByUserAndExperimentType(
        $oAbaUser,
        $experimentTypeId = null,
        $bCreateProfile = false
    ) {
        $paramsToSQL = array(
          ":USERID" => $oAbaUser->id,
          ":STATUS" => self::USER_EXPERIMENT_VARIATION_STATUS_DEFAULT,
          ":EXPERIMENTSTATUS" => CmAbaExperiments::EXPERIMENT_STATUS_ENABLED,
        );
        if (is_numeric($experimentTypeId)) {
            $paramsToSQL[":EXPERIMENTTYPE"] = $experimentTypeId;
        }

        $sSql = "
            SELECT ex2.*
" . "
            FROM
            (
                SELECT
                    uexv.userExperimentVariationId, uexv.experimentVariationId,
                    exv.experimentVariationIdentifier,
                    ex.experimentId, ex.experimentIdentifier, ex.userCategoryId, ex.availableModeId,
                    ext.experimentTypeId, ext.experimentTypeIdentifier,
                    IF(NOW() BETWEEN ex.dateStart AND IFNULL(ex.dateEnd, NOW()), 1, 0) AS availableExperiment,
                    1 AS isCreatedUserVariation, ex.languages
                FROM user_experiments_variations uexv
                JOIN experiments_variations AS exv
                    ON exv.experimentVariationId = uexv.experimentVariationId
                JOIN experiments AS ex
                    ON exv.experimentId = ex.experimentId
                    AND ex.experimentStatus = :EXPERIMENTSTATUS
        ";
//        AND ex.languages LIKE :LANGUAGES

        if(is_numeric($experimentTypeId)) {
            $sSql .= "
                    AND ex.experimentTypeId = :EXPERIMENTTYPE
            ";
        }

        $sSql .= "
                JOIN experiments_types AS ext
                  ON ex.experimentTypeId = ext.experimentTypeId
                WHERE
                    uexv.userId = :USERID
                    AND uexv.`status` = :STATUS
        ";

        if($bCreateProfile) {
            $sSql .= "
UNION
              SELECT
                NULL AS userExperimentVariationId, NULL AS experimentVariationId,
                NULL AS experimentVariationIdentifier,
                  ex1.experimentId, ex1.experimentIdentifier, ex1.userCategoryId, ex1.availableModeId,
                    ext1.experimentTypeId, ext1.experimentTypeIdentifier,
                    1 AS availableExperiment,
                    0 AS isCreatedUserVariation, ex1.languages
              FROM experiments AS ex1
                JOIN experiments_types AS ext1
                  ON ex1.experimentTypeId = ext1.experimentTypeId
              WHERE
                NOW() BETWEEN ex1.dateStart AND IFNULL(ex1.dateEnd, NOW())
                AND ex1.experimentStatus = :EXPERIMENTSTATUS
                AND ex1.counterUsers < ex1.limitUsers
        ";
//            AND ex1.languages LIKE :LANGUAGES

            if(is_numeric($experimentTypeId)) {
                $sSql .= "
                    AND ex1.experimentTypeId = :EXPERIMENTTYPE
                ";
            }
        }

        $sSql .= "
            ) AS ex2
            GROUP BY ex2.experimentId, ex2.userCategoryId, ex2.availableModeId, ex2.experimentTypeId, ex2.experimentTypeIdentifier
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        $aUserExperiments = array();

        if (!$dataReader) {
            return $aUserExperiments;
        }

        while (($row = $dataReader->read()) !== false) {
            $aUserExperiments[] = $row;
        }
        return $aUserExperiments;
    }

    /**
     * @return $this|bool
     */
    public function insertUserExperimentVariation()
    {
        $this->beginTrans();
        $successTrans = true;
        //-----------------
        $sSql = " INSERT INTO " . $this->tableName() . " ( `userId`, `experimentVariationId`, `status`, `dateAdd` )
                VALUES ( :USERID, :EXPERIMENTVARIATIONID, :STATUS, :DATEADD )
        ";

        $aParamValues = array
        (
          ':USERID' => (is_numeric($this->userId) ? $this->userId : 0),
          ':EXPERIMENTVARIATIONID' => (is_numeric($this->userExperimentVariationId) ? $this->userExperimentVariationId : 0),
          ':STATUS' => (is_numeric($this->status) ? $this->status : self::USER_EXPERIMENT_VARIATION_STATUS_DEFAULT),
          ':DATEADD' => (trim($this->dateAdd) != '' ? $this->dateAdd : HeDate::todaySQL(true)),
        );

        $this->userExperimentVariationId = $this->executeSQL($sSql, $aParamValues);
        if ($this->userExperimentVariationId <= 0) {
            $successTrans = false;
        }
        //---------------
        if ($successTrans) {
            $this->commitTrans();
            return $this;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

    /**
     * @param $oAbaUser
     * @param $experimentTypeId
     * @param null $experimentVariationId
     *
     * @return bool
     */
    public function insertUserExperimentVariationForRegistration(
        $oAbaUser,
        $experimentTypeId,
        $experimentVariationId = null
    ) {
        try {
            $oAbaExperimentsVariation = new CmAbaExperimentsVariations();
            $oAbaExperiments = new CmAbaExperiments();

            if (!is_numeric($experimentVariationId) OR $experimentVariationId == 0 OR empty($experimentVariationId)) {
                $experimentVariationId = $oAbaExperimentsVariation->getRandomExperimentVariationId($oAbaUser->countryId,
                  $experimentTypeId, $oAbaUser->langEnv);
            }
            if (is_numeric($experimentVariationId) and $experimentVariationId <> 0) {
                if ($oAbaExperimentsVariation->getExperimentVariationById($experimentVariationId)) {
                    if ($oAbaExperiments->getExperimentById($oAbaExperimentsVariation->experimentId)) {
                        $this->userExperimentVariationId = $experimentVariationId;
                        $this->userId = $oAbaUser->id;
                        $this->status = self::USER_EXPERIMENT_VARIATION_STATUS_DEFAULT;
                        if ($this->insertUserExperimentVariation()) {
                            $oAbaExperimentsVariation->incrementExperimentVariationCounter();
                            $oAbaExperiments->incrementExperimentCounter();
                        }
                        return true;
                    }
                }
            }
        } catch (Exception $e) {
            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_LOGIC_L.' Process Insert User '.$oAbaUser->id.' Experiment Variation failed: ' .
                $e->getMessage(),
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                'insertUserExperimentVariationForRegistration'
            );
        }
        return false;
    }
}
