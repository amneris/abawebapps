<?php
/**
 * CmAbaUserDevice
 *
 * @author Mikkel Gadegaard <mgadegaard@abaenglish.com>
 * Date: 26/06/2015
 * Time: 13:19
 * © ABA English 2015
 *
 * @property string id Probably IMEI of device
 * @property integer userId` INT(4) UNSIGNED NOT NULL COMMENT 'Foreign Key to users',
 * @property string deviceName` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Name of the device, HTC Sense, Samsung SIII, etc.',
 * @property string sysOper` VARCHAR(85) NULL DEFAULT NULL COMMENT 'Operative System, Android JellyBeans, etc.',
 * @property string token Access Token for this device.',
 * @property string dateLastModified Date last modification
 * @property string appVersion Version of application installed on device
 */

class CmAbaUserDevice extends AbaActiveRecord
{

    /**
     * Returns database table name
     *
     * @return string name of table
     */
    public function tableName()
    {
        return 'user_devices';
    }

    /**
     * Returns all table columns (like *)
     *
     * @return string
     */
    public function mainFields()
    {
        return " ud.`id`, ud.`userId`, ud.`deviceName`, ud.`sysOper`, ud.`token`, ud.`dateLastModified`, ud.`appVersion` ";
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return CmAbaUserDevice the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Finds and returns a user device by user id.
     *
     * @param int $idUser User id
     *
     * @return CmAbaUserDevice|bool
     */
    public function getDeviceByUserId($idUser)
    {
        $sql = " SELECT  " . $this->mainFields() . " FROM " . $this->tableName() .
            " ud WHERE ud.`userId`=:USERID ORDER BY ud.dateLastModified DESC LIMIT 1";
        $paramsToSQL = array(":USERID" => $idUser);
        $dataReader = $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * Finds and returns a user device by user and device id. (Used to ensure proper device is returned,
     * since users can have multiple devices.
     *
     * @param string $idDevice Device id (typically IMEI of mobile device)
     * @param int    $idUser   User id
     *
     * @return $this|bool
     */
    public function getDeviceByIdUserId($idDevice, $idUser)
    {
        $sql = " SELECT  " . $this->mainFields() . " FROM " . $this->tableName() .
            " ud WHERE ud.id=:DEVICEID AND  ud.`userId`=:USERID ";
        $paramsToSQL = array(":USERID" => $idUser, ":DEVICEID" => $idDevice);
        $dataReader = $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        $this->id = $idDevice;
        $this->userId = $idUser;
        return false;
    }

    /**
     * Populates User Device object based on given token
     *
     * @param string $token
     *
     * @return $this|bool returns the object if it exists otherwise false.
     */
    public function getDeviceByToken($token)
    {
        $sql = " SELECT  " . $this->mainFields() . " FROM " . $this->tableName() . " ud WHERE ud.token=:TOKEN ";
        $paramsToSQL = array(":TOKEN" => $token);
        $dataReader = $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * Inserts a new record into database. Returns true if successful. Returns false whether it already exists or there
     * were an error inserting.
     *
     * @param $deviceId
     * @param $userId
     * @param $deviceName
     * @param $sysOper
     * @param $token
     * @param $appVersion
     *
     * @return bool
     */
    public function insertNewDevice($deviceId, $userId, $deviceName, $sysOper, $token, $appVersion)
    {
        $sql = "INSERT INTO " . $this->tableName() .
            " ( id, userId, deviceName, sysOper, token, appVersion )
            VALUES ( :ID, :USERID, :DEVICENAME, :SYSOPER, :TOKEN, :APPVERSION ) ";
        $params = array(":ID"      => $deviceId, ":USERID" => $userId, ":DEVICENAME" => $deviceName,
            ":SYSOPER" => $sysOper, ":TOKEN" => $token, ":APPVERSION" => $appVersion);
        $returnId = $this->executeSQL($sql, $params);
        if ($returnId != false) {
            $this->id = $deviceId;
            $this->userId = $userId ;
            $this->deviceName = $deviceName ;
            $this->sysOper = $sysOper;
            $this->token = $token ;
            $this->appVersion = $appVersion;
            return true;
        }
        return false;
    }

    public function getUserIdByToken($token)
    {
        $query = "SELECT u.userId FROM " . $this->tableName() . " u WHERE u.token = :token_param";
        $param = array('token_param' => $token);
        $dataReader = $this->querySQL($query, $param);
        if (($row = $dataReader->read()) !== false) {
            return $row['userId'];
        }

        return false;
    }

    /**
     * @param $userId
     *
     * @return CmAbaUserDevice[]|bool
     */
    public function getAllUserDevicesByUserId($userId)
    {
        $devices = array();
        $sql = " SELECT ud.*
                 FROM " . $this->tableName() . " ud
                 WHERE  ud.userId=:USERID
                 ORDER BY ud.`dateLastModified` DESC ";
        $parameters = array( ":USERID"=>$userId );
        $dataRows = $this->queryAllSQL($sql, $parameters);
        if (!$dataRows) {
            return false;
        }
        foreach ($dataRows as $row) {
            $moDevice = new CmAbaUserDevice();
            $moDevice->getDeviceByIdUserId($row['id'], $row['userId']);
            $devices[] = $moDevice;
        }
        return $devices;
    }

    /**
     * Updates 3 columns of an existing device. Never updates user nor device Id.
     *
     * @return bool
     */
    public function updateDevice()
    {
        $query = "UPDATE " .
            $this->tableName() .
            " SET deviceName=:DEVICENAME, sysOper=:SYSOPER, token=:TOKEN, appVersion=:APPVERSION
            WHERE userId=" . $this->userId . " AND id='" . $this->id . "' ";
        $params = array(":DEVICENAME" => $this->deviceName, ":SYSOPER" => $this->sysOper,
            ":TOKEN" => $this->token, ":APPVERSION" => $this->appVersion);

        if ($this->updateSQL($query, $params) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Updates the token column only.
     * @param string $token
     *
     * @return bool
     */
    public function updateToken($token)
    {
        $query = "UPDATE " . $this->tableName() . " SET  token=:TOKEN
                    WHERE userId=" . $this->userId . " AND id='" . $this->id . "' ";
        $params = array( ":TOKEN" => $token );

        if ($this->updateSQL($query, $params) > 0) {
            $this->token = $token;
            return true;
        } else {
            return false;
        }
    }
}
