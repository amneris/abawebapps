<?php
/**
 * This is the model class for table "user_invoices".
 *
 * The followings are the available columns in table 'user_invoices':
 * @property string $id
 * @property string $prefixInvoice
 * @property string $numInvoiceId
 * @property string $numberInvoice
 * @property string $idPayment
 * @property string $dateInvoice
 * @property string $dateFirstRequest
 * @property string $dateLastDownload
 * @property string $nameAba
 * @property string $cifAba
 * @property string $addressAba
 * @property string $addressAbaMore
 * @property string $telephoneAba
 * @property string $userName
 * @property string $userLastName
 * @property string $userStreet
 * @property string $userStreetMore
 * @property string $userVatNumber
 * @property string $userZipCode
 * @property string $userCityName
 * @property string $userStateName
 * @property string $userCountryName
 * @property string $productDesc
 * @property string $amountPrice
 * @property string $currencyTrans
 * @property string $taxRateValue
 * @property string $amountTax
 * @property string $amountPriceWithoutTax
 * @property integer $usedByUser
 */
class CmAbaUserInvoices extends AbaActiveRecord
{
    /**
     *
     */
    public function __construct($scenario=NULL, $dbConnName=NULL) {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_invoices';
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return "`id`, `prefixInvoice`, `numInvoiceId`, `numberInvoice`, `idPayment`, `dateInvoice`, `dateFirstRequest`,
                `dateLastDownload`, `nameAba`, `cifAba`, `addressAba`, `addressAbaMore`, `telephoneAba`, `userName`,
                `userLastName`, `userStreet`, `userStreetMore`, `userVatNumber`, `userZipCode`, `userCityName`,
                `userStateName`, `userCountryName`, `productDesc`, `amountPrice`, `currencyTrans`,
                `taxRateValue`, `amountTax`, `amountPriceWithoutTax`, `usedByUser`
        ";
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'prefixInvoice' => 'Prefix Invoice',
            'numInvoiceId' => 'Num Invoice',
            'numberInvoice' => 'Number Invoice',
            'idPayment' => 'Id Payment',
            'dateInvoice' => 'Date Invoice',
            'dateFirstRequest' => 'Date First Request',
            'dateLastDownload' => 'Date Last Download',
            'nameAba' => 'Name Aba',
            'cifAba' => 'Cif Aba',
            'addressAba' => 'Address Aba',
            'addressAbaMore' => 'City Aba',
            'telephoneAba' => 'Telephone Aba',
            'userName' => 'User Name',
            'userLastName' => 'User Last Name',
            'userStreet' => 'User Street',
            'userStreetMore' => 'User Street More',
            'userVatNumber' => 'User Vat Number',
            'userZipCode' => 'User Zip Code',
            'userCityName' => 'User City Name',
            'userStateName' => 'User State Name',
            'userCountryName' => 'User Country Name',
            'productDesc' => 'Product Desc',
            'amountPrice' => 'Amount Price',
            'currencyTrans' => 'Currency Trans',
            'taxRateValue' => 'Tax value',
            'amountTax' => 'Amount Tax',
            'amountPriceWithoutTax' => 'Amount Without Tax',
            'usedByUser' => 'User downloaded invoice',
        );
    }

    /**
     * @param string $idPayment
     * @return $this|bool
     */
    public function getByPaymentId($idPayment)
    {
        $query = "SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " i WHERE i.idPayment=:IDPAYMENT ";
        $paramsToSQL = array(":IDPAYMENT" => $idPayment);
        $dataReader = $this->querySQL($query, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $moPayment
     * @param $moUserAddress
     * @param int $usedByUser
     *
     * @return $this
     */
    public function setFromPaymentAddress($moPayment, $moUserAddress, $usedByUser=0)
    {
        //#4480
        $paySuppExtId = $moPayment->paySuppExtId;

        $this->idPayment =      $moPayment->id;
        $this->dateInvoice =    HeDate::removeTimeFromSQL($moPayment->dateEndTransaction);

        $sPrefix =              $this->getPrefixInvoice($this->idPayment, $this->dateInvoice, $paySuppExtId);
        $this->prefixInvoice =  $sPrefix;
        $this->numberInvoice =  $this->buildNumberInvoice($sPrefix, $paySuppExtId);

        $this->dateFirstRequest =   HeDate::todaySQL(true);
        $this->dateLastDownload =   HeDate::todaySQL(true);

        $this->nameAba =        NAME_ADDRESS_ABA_INVOICE;
        $this->cifAba =         CIF_ADDRESS_ABA_INVOICE;
        $this->addressAba =     ADDRESS_ABA_INVOICE;
        $this->addressAbaMore = MORE_ADDRESS_ABA_INVOICE;
        $this->telephoneAba =   TELEPHONE_ADDRESS_ABA_INVOICE;

        if ($moUserAddress->vatNumber == '' && $usedByUser == 1) {
            return false;
        }

        //#4480 - MANDA APPSTORE
        if($paySuppExtId == PAY_SUPPLIER_APP_STORE) {
            $moCountry =                new CmAbaCountry($moPayment->idCountry);
            $this->userCountryName =    $moCountry->name_m;
        }
        else {
            $moCountry =                new CmAbaCountry($moUserAddress->country);
            $this->userCountryName =    $moCountry->name_m;
        }

        //#4480
        if($paySuppExtId == PAY_SUPPLIER_APP_STORE AND $moCountry->isUeCountry()) {
            $this->userName =       NAME_INVOICE_APPSTORE;
            $this->userLastName =   "";
            $this->userStreet =     "";
            $this->userStreetMore = "";
            $this->userStateName=   "";
            $this->userZipCode =    "";
            $this->userCityName =   "";
            $this->userVatNumber =  USERVATNUMBER_INVOICE_APPSTORE;
        }
        else {
            $this->userName =       $moUserAddress->name;
            $this->userLastName =   $moUserAddress->lastName;
            $this->userStreet =     $moUserAddress->street;
            $this->userStreetMore = $moUserAddress->streetMore;
            $this->userStateName=   $moUserAddress->state;
            $this->userZipCode =    $moUserAddress->zipCode;
            $this->userCityName =   $moUserAddress->city;
            $this->userVatNumber =  $moUserAddress->vatNumber;
        }

        //$moUser = new AbaUser();
        //$moUser->getUserById($moPayment->userId);
        //$this->productDesc = $moPayment->getProductPeriodDesc($moUser->langEnv, $moPayment->userId);
        $this->productDesc = '';

        $this->amountPrice =            $moPayment->amountPrice;
        $this->taxRateValue =           $moPayment->taxRateValue;
        $this->amountTax =              $moPayment->amountTax;
        $this->amountPriceWithoutTax =  $moPayment->amountPriceWithoutTax;

        if($moPayment->status == PAY_REFUND) {
            $this->amountPrice =            -1 * $moPayment->amountPrice;
            $this->amountTax =              -1 * $moPayment->amountTax;
            $this->amountPriceWithoutTax =  -1 * $moPayment->amountPriceWithoutTax;
        }
        $this->currencyTrans = $moPayment->currencyTrans;

        $this->usedByUser = 0;

        return $this;
    }

    /**
     * @return bool
     */
    public function insertInvoice($userId=null)
    {
        $this->beginTrans();

        $sql = " INSERT INTO " . $this->tableName() . " ( `idPayment`, `prefixInvoice`, `numInvoiceId`, `numberInvoice`, `dateInvoice`, `dateFirstRequest`,
                            `dateLastDownload`, `nameAba`, `cifAba`,
                            `addressAba`, `addressAbaMore`, `telephoneAba`, `userName`, `userLastName`, `userStreet`,
                            `userStreetMore`, `userVatNumber`, `userZipCode`, `userCityName`, `userStateName`,
                            `userCountryName`, `productDesc`, `amountPrice`, `currencyTrans`,
                            `taxRateValue`, `amountTax`, `amountPriceWithoutTax`, `usedByUser`)
                VALUES (  :IDPAYMENT, :PREFIXINVOICE, :NUMINVOICEID, :NUMBERINVOICE, :DATEINVOICE, :DATEFIRSTREQUEST,
                            :DATELASTDOWNLOAD, :NAMEABA, :CIFABA,
                            :ADDRESSABA, :ADDRESSABAMORE, :TELEPHONEABA, :USERNAME, :USERLASTNAME, :USERSTREET,
                            :USERSTREETMORE, :USERVATNUMBER, :USERZIPCODE, :USERCITYNAME, :USERSTATENAME,
                            :USERCOUNTRYNAME, :PRODUCTDESC, :AMOUNTPRICE, :CURRENCYTRANS,
                            :TAXRATEVALUE, :AMOUNTTAX, :AMOUNTPRICEWITHOUTTAX, :USEDBYUSER )
		        ON DUPLICATE KEY UPDATE `dateLastDownload`=:DATELASTDOWNLOAD ;";

        $aBrowserValues = array(':IDPAYMENT' => $this->idPayment,
            ':PREFIXINVOICE' => $this->prefixInvoice,
            ':NUMINVOICEID' => $this->numInvoiceId,
            ':NUMBERINVOICE' => $this->numberInvoice, ':DATEINVOICE' => $this->dateInvoice,
            ':DATEFIRSTREQUEST' => $this->dateFirstRequest, ':DATELASTDOWNLOAD' => $this->dateLastDownload,
            ':NAMEABA' => $this->nameAba, ':CIFABA' => $this->cifAba,
            ':ADDRESSABA' => $this->addressAba, ':ADDRESSABAMORE' => $this->addressAbaMore,
            ':TELEPHONEABA' => $this->telephoneAba, ':USERNAME' => $this->userName,
            ':USERLASTNAME' => $this->userLastName, ':USERSTREET' => $this->userStreet,
            ':USERSTREETMORE' => $this->userStreetMore, ':USERVATNUMBER' => $this->userVatNumber,
            ':USERZIPCODE' => $this->userZipCode, ':USERCITYNAME' => $this->userCityName,
            ':USERSTATENAME' => $this->userStateName, ':USERCOUNTRYNAME' => $this->userCountryName,
            ':PRODUCTDESC' => $this->productDesc, ':AMOUNTPRICE' => $this->amountPrice,
            ':CURRENCYTRANS' => $this->currencyTrans,
            ':TAXRATEVALUE' => $this->taxRateValue, ':AMOUNTTAX' => $this->amountTax,
            ':AMOUNTPRICEWITHOUTTAX' => $this->amountPriceWithoutTax, ':USEDBYUSER' => $this->usedByUser
        );
        $retId = $this->executeSQL($sql, $aBrowserValues);

        if ($retId > 0) {
            $this->id = $retId;
            $moUserLogUser = new CmLogUserActivity();
            if ($moUserLogUser->saveUserLogActivity("invoices", " New invoice requested and created.", "insertInvoice", $userId)) {
                $this->commitTrans();
                return $retId;
            } else {
                $this->rollbackTrans();
                return false;
            }
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @param $iInvoiceId
     *
     * @return bool
     */
    public function updateInvoice($iInvoiceId, $userId=null)
    {
        $this->beginTrans();

        $this->usedByUser = 1;

        $aBrowserValues = array(
            ':USERNAME' => $this->userName,
            ':USERLASTNAME' => $this->userLastName, ':USERSTREET' => $this->userStreet,
            ':USERSTREETMORE' => $this->userStreetMore, ':USERVATNUMBER' => $this->userVatNumber,
            ':USERZIPCODE' => $this->userZipCode, ':USERCITYNAME' => $this->userCityName,
            ':USERSTATENAME' => $this->userStateName, ':USERCOUNTRYNAME' => $this->userCountryName,
            ':USEDBYUSER' => $this->usedByUser, ':DATELASTDOWNLOAD' => $this->dateLastDownload,
            ':PRODUCTDESC' => $this->productDesc, ':ID' => $iInvoiceId
        );

        $sql = " UPDATE " . $this->tableName() . " SET `userName`=:USERNAME, `userLastName`= :USERLASTNAME, `userStreet`=:USERSTREET, `userStreetMore`= :USERSTREETMORE,
            `userVatNumber`=:USERVATNUMBER, `userZipCode`= :USERZIPCODE, `userCityName`=:USERCITYNAME, `userStateName`= :USERSTATENAME,
            `userCountryName`= :USERCOUNTRYNAME, `usedByUser`= :USEDBYUSER, `dateLastDownload`= :DATELASTDOWNLOAD, `productDesc`= :PRODUCTDESC
            WHERE `id`=:ID
        ";

        if( $this->updateSQL($sql, $aBrowserValues) > 0 ) {
            $this->id = $iInvoiceId;
            $moUserLogUser = new CmLogUserActivity();
            if ($moUserLogUser->saveUserLogActivity("invoices", " New invoice requested and updated.", "updateInvoice", $userId)) {
                $this->commitTrans();
                return $iInvoiceId;
            } else {
                $this->rollbackTrans();
                return false;
            }
        }
        else
        {
            $this->rollbackTrans();
            return false;
        }
    }


    /**
     * @param integer $iInvoiceId
     *
     * @return bool|int|string
     */
    public function insertUpdateInvoice($iInvoiceId=null, $userId=null)
    {
        if(!is_numeric($userId)) {
            $userId = Yii::app()->user->getId();
        }
        if(is_numeric($iInvoiceId) && $iInvoiceId > 0) {
            return $this->updateInvoice($iInvoiceId, $userId);
        }
        else {
            return $this->insertInvoice($userId);
        }
    }


    /**
     * @return string
     *
     */
   protected function getPrefixInvoice($idPayment=false, $dateInvoice='', $paySuppExtId=0) {

        if(!$idPayment) {
            $idPayment = $this->idPayment;
        }

        $abaPayment = new CmAbaPayment();
        $abaPayment->getPaymentById($idPayment);

        $abaCountry = new CmAbaCountry();
        $abaCountry->getUeCountryById($abaPayment->idCountry);

        $sPrefix = DEFAULT_PREFIX_INVOICE;

        if(trim($dateInvoice) != '') {
            if(HeDate::isSmallerThan($dateInvoice, DEFAULT_INVOICE_DATE_START)) {
                return DEFAULT_PREFIX_INVOICE_PREVIOUS;
            }
        }

        //#4480
        if($paySuppExtId == PAY_SUPPLIER_APP_STORE) {
            return DEFAULT_PREFIX_INVOICE_APPSTORE;
        }

        if(trim($abaCountry->iso) != '') {
            return HeMixed::toUpper($abaCountry->iso);
        }

        return $sPrefix;
    }

    /** Builds based on Next Number available, the invoice complex format new number.
     * @param string $sPrefix
     *
     * @return string
     */
    protected function buildNumberInvoice($sPrefix='', $paySuppExtId=0)
    {
        if(trim($sPrefix) == '') {
            $sPrefix = $this->getPrefixInvoice(false, '', $paySuppExtId);
        }

        $this->numInvoiceId = $this->getNextNumInvoiceId($sPrefix);

        $number = $sPrefix . '-' . str_pad(strval($this->numInvoiceId), FORMAT_NUMBER_INVOICE, "0", STR_PAD_LEFT);

        return $number;
    }

    /** Returns the next Number, checks last Invoice and adds one unit.
     * @param string $sPrefixInvoice
     *
     * @return int
     */
    protected function getNextNumInvoiceId($sPrefixInvoice=DEFAULT_PREFIX_INVOICE)
    {
        $sql =  " SELECT MAX(i.numInvoiceId) as numInvoiceId FROM " . $this->tableName() . " i ";
        $sql .= " WHERE i.prefixInvoice = '" . $sPrefixInvoice . "' ";

        $dataReader = $this->querySQL($sql);

        if (($row = $dataReader->read()) !== false) {
            if (FIRST_INVOICE <= intval($row["numInvoiceId"]) && intval($row["numInvoiceId"]) != 0) {
                return intval($row["numInvoiceId"]) + 1;
            }
        }
        return FIRST_INVOICE;
    }

    /**
     * @param $moPayment
     * @param integer $userId
     *
     * @return bool
     */
    public function createPreInvoice($moPayment, $userId)
    {
        $moUserAddressInvoice = new CmAbaUserAddressInvoice();
        if (!$moUserAddressInvoice->getUserAddressByUserId($userId)) {

            $oUser = new CmAbaUser();
            $oUser->populateUserById($userId);

            $moUserAddressInvoice->setUserAddressForm($oUser->id, $oUser->name, $oUser->surnames, null, null, null,
                                                                    null, $oUser->city, null, $oUser->countryIdCustom);
            if (!$moUserAddressInvoice->insertUserAddress()) {
                return false;
            }
        }

        if ($this->setFromPaymentAddress($moPayment, $moUserAddressInvoice)) {
            // Generate Invoice, if already exists then we only update dateLastDownload
            $this->insertUpdateInvoice(null, $userId);
            return true;
        }
        return false;
    }

    /**
     * @return bool
     *
     * @throws CDbException
     */
    public function updateZuoraInvoice()
    {
        $this->beginTrans();

        $aBrowserValues = array(
          ':AMOUNTPRICE' => $this->amountPrice,
          ':TAXRATEVALUE' => $this->taxRateValue,
          ':AMOUNTTAX' => $this->amountTax,
          ':AMOUNTPRICEWITHOUTTAX' => $this->amountPriceWithoutTax,
          ':ID' => $this->id
        );

        $sSql = "
            UPDATE " . $this->tableName() . "
            SET
                `amountPrice`=:AMOUNTPRICE,
                `taxRateValue`=:TAXRATEVALUE,
                `amountTax`=:AMOUNTTAX,
                `amountPriceWithoutTax`=:AMOUNTPRICEWITHOUTTAX
            WHERE
                `id`=:ID
            ;
        ";

        if ($this->updateSQL($sSql, $aBrowserValues) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }

        return false;
    }

}

