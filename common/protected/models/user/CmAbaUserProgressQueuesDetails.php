<?php

/**
 * CmAbaUserProgressQueuesDetails
 *
 * This is the model class for table "user_progress_queues_details".
 *
 * The followings are the available columns in table 'user_progress_queues_details':
 * @property integer $queueDetailId
 * @property integer $queueId
 * @property integer $unitId
 * @property string $dateStart
 * @property string $dateEnd
 * @property integer $status
 */
class CmAbaUserProgressQueuesDetails extends AbaActiveRecord
{
    const QUEUEDETAIL_STATUS_PENDING = 0;
    const QUEUEDETAIL_STATUS_INPROCESS = 10;
    const QUEUEDETAIL_STATUS_FAIL = 20;
    const QUEUEDETAIL_STATUS_PROCESSED = 30;
    const QUEUEDETAIL_STATUS_DELETED = 40;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_progress_queues_details';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaUserProgressQueuesDetails the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
          array('queueId, unitId', 'required'),
          array(
            'queueDetailId, queueId, unitId, status',
            'numerical',
            'integerOnly' => true
          ),
          array('unitId', 'type', 'type' => 'integer', 'allowEmpty' => false),
          array('dateStart, dateEnd', 'type', 'type' => 'string', 'allowEmpty' => true),
          array(
            'queueDetailId, queueId, unitId, dateStart, dateEnd, status',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " upqd.`queueDetailId`, upqd.`queueId`, upqd.`unitId`, upqd.`dateStart`, upqd.`dateEnd`, upqd.`status` ";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }


    /**
     * @param $iQueueId
     *
     * @return $this|bool
     */
    public function getUserProgressQueueDetailById($iQueueDetailId)
    {

        $paramsToSQL = array(
          ":QUEUEDETAILID" => $iQueueDetailId,
        );

        $sSql = "
            SELECT
                " . $this->mainFields() . "
            FROM " . $this->tableName() . " upqd
            WHERE upqd.queueDetailId = :QUEUEDETAILID
            LIMIT 0, 1
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }


    /**
     * @param $iQueueId
     *
     * @return $this|bool
     */
    public function getUserProgressQueueDetailByQueueIdAndUnitId($iQueueId, $iUnitId)
    {

        $paramsToSQL = array(
          ":QUEUEID" => $iQueueId,
          ":UNITID" => $iUnitId,
        );

        $sSql = "
            SELECT
                " . $this->mainFields() . "
            FROM " . $this->tableName() . " upqd
            WHERE
                upqd.queueId = :QUEUEID
                AND upqd.unitId = :UNITID
            LIMIT 0, 1
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }


    /**
     * @return bool
     *
     * @throws CDbException
     */
    public function updateAllToInProgress($iQueueId, $sDateStart = '', $sDateEnd = '')
    {

        $sSqlDateStart = '';
        $sSqlDateEnd = '';

        if (trim($sDateStart) <> '') {
            $sSqlDateStart = "
                , `dateStart` = '" . $sDateStart . "'
            ";
        }

        if (trim($sDateEnd) <> '') {
            $sSqlDateEnd = "
                , `dateEnd` = '" . $sDateEnd . "'
            ";
        }

        $this->beginTrans();
        $sSql = "
            UPDATE " . $this->tableName() . "
                SET `status`='" . self::QUEUEDETAIL_STATUS_INPROCESS . "'
                " . $sSqlDateStart . " " . $sSqlDateEnd . "
            WHERE `queueId` = '" . $iQueueId . "'
        ; ";
        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }


    /**
     * @return bool
     *
     * @throws CDbException
     */
    public function updateToInProgress($iQueueDetailId, $sDateStart = '', $sDateEnd = '')
    {

        $sSqlDateStart = '';
        $sSqlDateEnd = '';

        if (trim($sDateStart) <> '') {
            $sSqlDateStart = "
                , `dateStart` = '" . $sDateStart . "'
            ";
        }

        if (trim($sDateEnd) <> '') {
            $sSqlDateEnd = "
                , `dateEnd` = '" . $sDateEnd . "'
            ";
        }

        $this->beginTrans();
        $sSql = "
            UPDATE " . $this->tableName() . "
                SET `status`='" . self::QUEUEDETAIL_STATUS_INPROCESS . "'
                " . $sSqlDateStart . " " . $sSqlDateEnd . "
            WHERE `queueDetailId` = '" . $iQueueDetailId . "'
        ; ";
        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }


    /**
     * @return bool
     *
     * @throws CDbException
     */
    public function updateToFail($iQueueDetailId, $sDateStart = '', $sDateEnd = '')
    {

        $sSqlDateStart = '';
        $sSqlDateEnd = '';

        if (trim($sDateStart) <> '') {
            $sSqlDateStart = "
                , `dateStart` = '" . $sDateStart . "'
            ";
        }

        if (trim($sDateEnd) <> '') {
            $sSqlDateEnd = "
                , `dateEnd` = '" . $sDateEnd . "'
            ";
        }

        $this->beginTrans();
        $sSql = "
            UPDATE " . $this->tableName() . "
                SET `status`='" . self::QUEUEDETAIL_STATUS_FAIL . "'
                " . $sSqlDateStart . " " . $sSqlDateEnd . "
            WHERE `queueDetailId` = '" . $iQueueDetailId . "'
        ; ";
        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }


    /**
     * @return bool
     *
     * @throws CDbException
     */
    public function updateAllToFail($iQueueId, $sDateStart = '', $sDateEnd = '')
    {

        $sSqlDateStart = '';
        $sSqlDateEnd = '';

        if (trim($sDateStart) <> '') {
            $sSqlDateStart = "
                , `dateStart` = '" . $sDateStart . "'
            ";
        }

        if (trim($sDateEnd) <> '') {
            $sSqlDateEnd = "
                , `dateEnd` = '" . $sDateEnd . "'
            ";
        }

        $this->beginTrans();
        $sSql = "
            UPDATE " . $this->tableName() . "
                SET `status`='" . self::QUEUEDETAIL_STATUS_FAIL . "'
                " . $sSqlDateStart . " " . $sSqlDateEnd . "
            WHERE
                `queueId` = '" . $iQueueId . "'
                AND `status`='" . self::QUEUEDETAIL_STATUS_INPROCESS . "'
        ; ";
        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }


    /**
     * @return bool
     *
     * @throws CDbException
     */
    public function updateToSuccess($iQueueDetailId, $sDateStart = '', $sDateEnd = '')
    {

        $sSqlDateStart = '';
        $sSqlDateEnd = '';

        if (trim($sDateStart) <> '') {
            $sSqlDateStart = "
                , `dateStart` = '" . $sDateStart . "'
            ";
        }

        if (trim($sDateEnd) <> '') {
            $sSqlDateEnd = "
                , `dateEnd` = '" . $sDateEnd . "'
            ";
        }

        $this->beginTrans();
        $sSql = "
            UPDATE " . $this->tableName() . "
                SET `status`='" . self::QUEUEDETAIL_STATUS_PROCESSED . "'
                " . $sSqlDateStart . " " . $sSqlDateEnd . "
            WHERE `queueDetailId` = '" . $iQueueDetailId . "'
        ; ";

        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }


    /**
     * @return bool
     *
     * @throws CDbException
     */
    public function updateAllToSuccess($iQueueId, $sDateStart = '', $sDateEnd = '')
    {

        $sSqlDateStart = '';
        $sSqlDateEnd = '';

        if (trim($sDateStart) <> '') {
            $sSqlDateStart = "
                , `dateStart` = '" . $sDateStart . "'
            ";
        }

        if (trim($sDateEnd) <> '') {
            $sSqlDateEnd = "
                , `dateEnd` = '" . $sDateEnd . "'
            ";
        }

        $this->beginTrans();
        $sSql = "
            UPDATE " . $this->tableName() . "
                SET `status`='" . self::QUEUEDETAIL_STATUS_PROCESSED . "'
                " . $sSqlDateStart . " " . $sSqlDateEnd . "
            WHERE
                `queueId` = '" . $iQueueId . "'
                AND `status`='" . self::QUEUEDETAIL_STATUS_INPROCESS . "'
        ; ";
        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }


    /**
     * @return bool
     *
     * @throws CDbException
     */
    public function updateToDeleted($iQueueDetailId, $sDateStart = '', $sDateEnd = '')
    {

        $sSqlDateStart = '';
        $sSqlDateEnd = '';

        if (trim($sDateStart) <> '') {
            $sSqlDateStart = "
                , `dateStart` = '" . $sDateStart . "'
            ";
        }

        if (trim($sDateEnd) <> '') {
            $sSqlDateEnd = "
                , `dateEnd` = '" . $sDateEnd . "'
            ";
        }

        $this->beginTrans();
        $sSql = "
            UPDATE " . $this->tableName() . "
                SET `status`='" . self::QUEUEDETAIL_STATUS_DELETED . "'
                " . $sSqlDateStart . " " . $sSqlDateEnd . "
            WHERE `queueDetailId` = '" . $iQueueDetailId . "'
        ; ";
        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }


    /**
     * @return bool
     */
    public function startProcessQueueDetail()
    {
        try {
            $sDateStart = date("Y-m-d h:i:s", time());

            $this->updateToInProgress($this->queueDetailId, $sDateStart);
        } catch (Exception $e) {
            return false;
        }
        return true;
    }


    /**
     * @param bool|true $bSuccess
     *
     * @return bool
     */
    public function endProcessQueueDetail($bSuccess = true)
    {
        try {
            $sDateStart = date("Y-m-d h:i:s", time());

            if ($bSuccess) {
                $this->updateToSuccess($this->queueDetailId, "", $sDateStart);
            } else {
                $this->updateToFail($this->queueDetailId, "", $sDateStart);
            }
        } catch (Exception $e) {
            return false;
        }
        return true;
    }


    /**
     * @return $this|bool
     */
    public function insertUserProgressQueueDetail()
    {
        $this->beginTrans();
        $successTrans = true;
        //-----------------
        $sSql = "
            INSERT INTO " . $this->tableName() . "
            ( `queueId`, `unitId`, `dateStart`, `dateEnd`, `status` )
            VALUES ( :QUEUEID, :UNITID, :DATESTART, :DATEEND, :STATUS )
        ";

        $aParamValues = array
        (
          ':QUEUEID' => $this->queueId,
          ':UNITID' => (is_numeric($this->unitId) ? $this->unitId : 0),
          ':DATESTART' => $this->dateStart,
          ':DATEEND' => $this->dateEnd,
          ':STATUS' => (is_numeric($this->status) ? $this->status : self::QUEUEDETAIL_STATUS_PENDING),
        );

        $this->queueDetailId = $this->executeSQL($sSql, $aParamValues);
        if ($this->queueDetailId <= 0) {
            $successTrans = false;
        }
        //---------------
        if ($successTrans) {
            $this->commitTrans();
            return $this;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }


}

