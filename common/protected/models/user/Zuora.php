<?php

/**
 * Created by PhpStorm.
 * User: earandes
 * Date: 01/07/16
 * Time: 09:53
 */
class Zuora
{
    public $zuoraAccountId;
    public $status;
    public $zuoraAccountNumber;
    public $zuoraSubscriptionId;
    public $zuoraSubscriptionNumber;
    public $dateAdd;

    /**
     * @return mixed
     */
    public function getZuoraAccountId()
    {
        return $this->zuoraAccountId;
    }

    /**
     * @return mixed
     */
    public function getZuoraSubscriptionId()
    {
        return $this->zuoraSubscriptionId;
    }

    /**
     * @return mixed
     */
    public function getZuoraSubscriptionNumber()
    {
        return $this->zuoraSubscriptionNumber;
    }

    /**
     * @param mixed $zuoraAccountId
     */
    public function setZuoraAccountId($zuoraAccountId)
    {
        $this->zuoraAccountId = $zuoraAccountId;
    }

    /**
     * @param $zuoraSubscriptionId
     */
    public function setZuoraSubscriptionId($zuoraSubscriptionId)
    {
        $this->zuoraSubscriptionId = $zuoraSubscriptionId;
    }

    /**
     * @param $zuoraSubscriptionNumber
     */
    public function setZuoraSubscriptionNumber($zuoraSubscriptionNumber)
    {
        $this->zuoraSubscriptionNumber = $zuoraSubscriptionNumber;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getZuoraAccountNumber()
    {
        return $this->zuoraAccountNumber;
    }

    /**
     * @param mixed $zuoraAccountNumber
     */
    public function setZuoraAccountNumber($zuoraAccountNumber)
    {
        $this->zuoraAccountNumber = $zuoraAccountNumber;
    }

    /**
     * @return mixed
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * @param mixed $dateAdd
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;
    }


}