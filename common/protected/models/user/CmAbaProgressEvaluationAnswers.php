<?php

/**
 * CmAbaProgressEvaluationAnswers
 *
 * This is the model class for table "progress_evaluation_answers".
 *
 * The followings are the available columns in table 'progress_evaluation_answers':
 * @property integer $id
 * @property integer $idSectionType
 * @property integer $idUnit
 * @property string $evaluation
 */

class CmAbaProgressEvaluationAnswers extends AbaActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'progress_evaluation_answers';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaProgressEvaluationAnswers the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
          array('idSectionType, idUnit, evaluation', 'required'),
          array(
            'idSectionType, idUnit',
            'numerical',
            'integerOnly' => true
          ),
          array('evaluation', 'type', 'type' => 'string', 'allowEmpty' => false),
          array(
            'id, idSectionType, idUnit, evaluation',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " pea.`id`, pea.`idSectionType`, pea.`idUnit`, pea.`evaluation` ";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @param $idSectionType
     * @param $idUnit
     *
     * @return $this|bool
     */
    public function getAnswerBySectionTypeAndUnitId($idSectionType, $idUnit)
    {

        $paramsToSQL = array(
          ":IDSECTIONTYPE" => $idSectionType,
          ":IDUNIT" => $idUnit
        );

        $sSql = "
            SELECT
                " . $this->mainFields() . "
            FROM
                " . $this->tableName() . " pea 
            WHERE
                pea.idSectionType = :IDSECTIONTYPE
                AND pea.idUnit = :IDUNIT
            LIMIT 0, 1
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

}
