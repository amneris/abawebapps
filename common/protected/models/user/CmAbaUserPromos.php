<?php
/**
 * @property integer $id
 * @property integer $userId
 * @property string $idPromoCode
 * @property string $email
 * @property string $idProduct
 * @property integer $used
 * @property integer $dateAdd
 *
 */
class CmAbaUserPromos extends AbaActiveRecord
{
    public $id;

    /**
     *
     */
    public function __construct($scenario=NULL, $dbConnName=NULL) {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user_promos';
    }

    /**
     * @return string
     */
    public static function getTableName() {
        return 'user_promos';
    }

    /**
     * Before the save method is executed.
     * @return array
     */
    public function rules() {
        return array(
            array('userId', 'required'),
        );
    }

    /**
     * @return string
     */
    public function mainFields() {
        return "`id`, `userId`, `idPromoCode`, `email`, `idProduct`, `used`, `dateAdd` ";
    }

    /**
     * @param $userId
     *
     * @return $this|bool
     */
    public function getUserPromosByUserId( $userId ) {

        $sSql =         " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " up WHERE up.userId=:USERID ";
        $paramsToSQL =  array(":USERID" => $userId);
        $dataReader =   $this->querySQL($sSql, $paramsToSQL);
        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties( $row );
            return $this;
        }
        return false;
    }

    /**
     * @return $this|bool
     */
    public function insertUserPromo( )
    {
        $sSql = "
            INSERT INTO  " . $this->tableName() . "
            (`userId`, `idPromoCode`, `email`, `idProduct`, `used`, `dateAdd`)
            VALUES (:USERID, :IDPROMOCODE, :EMAIL, :IDPRODUCT, :USED, :DATEADD)
        ";
        $aBrowserValues = array(
            ":USERID" =>        $this->userId,
            ":IDPROMOCODE" =>   strval($this->idPromoCode),
            ":EMAIL" =>         strval($this->email),
            ":IDPRODUCT" =>     strval($this->idProduct),
            ":USED" =>          $this->used,
            ':DATEADD'=>        HeDate::todaySQL(true),
        );
        $this->id = $this->executeSQL( $sSql, $aBrowserValues, false );
        if ( $this->id > 0) {
            return $this;
        }
        return false;
   }

}
