<?php
/**
 * @property integer id
 * @property integer idUser
 * @property integer progressVersion
 * @property integer progressVersionHtml
 * @property integer audioVersion
 */
class CmAbaUserDictionary extends AbaActiveRecord
{
    public $id;

    /**
     * @param null $scenario
     * @param null $dbConnName
     */
    public function __construct($scenario = null, $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_dictionary';
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " ud.`id`, ud.`idUser`, ud.`progressVersion`, ud.`progressVersionHtml`, ud.`audioVersion` ";
    }

    public function rules()
    {
        return array(
            array('id', 'required'),
            array('idUser', 'required'),
            array('progressVersion', 'required'),
            array('progressVersionHtml', 'required'),
            array('audioVersion', 'required'),
            array('idUser, progressVersion, progressVersionHtml, audioVersion',
                'type', 'type' => 'integer', 'allowEmpty' => true),
        );
    }

    /**
     * @param $idUser
     *
     * @return $this|bool
     */
    public function getUserDictionaryByUserId($idUser)
    {
        $sql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() .
            " ud WHERE ud.idUser=:IDUSER ";
        $paramsToSQL =  array(":IDUSER" => $idUser);
        $dataReader =   $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return $this|bool
     */
    public function insertUserDictionary()
    {
        $sql = " INSERT INTO " . $this->tableName() .
            " (`idUser`, `progressVersion`, `progressVersionHtml`, `audioVersion`)
                 VALUES( :IDUSER, :PROGRESSVERSION, :PROGRESSVERSIONHTML, :AUDIOVERSION ); ";
        $aBrowserValues = array(
            ":IDUSER" =>                $this->idUser,
            ":PROGRESSVERSION" =>       $this->progressVersion,
            ":PROGRESSVERSIONHTML" =>   $this->progressVersionHtml,
            ":AUDIOVERSION" =>          $this->audioVersion
        );
        $this->id = $this->executeSQL($sql, $aBrowserValues, false);
        if ($this->id > 0) {
            return $this;
        }
        return false;
    }

    /**
     * @param $user
     *
     * @return bool
     */
    public function insertUserDictionaryForRegistration($user)
    {
        $abaFollowUpProgressVersion =       new CmAbaFollowUpVersion();
        $abaFollowUpProgressVersionHtml =   new CmAbaFollowUpVersion();
        $abaFollowUpAudioVersion =          new CmAbaFollowUpVersion();
        $abaFollowUpProgressVersion->loadProgressVersionByVersion();
        $abaFollowUpProgressVersionHtml->loadProgressVersionHtmlByVersion();
        $abaFollowUpAudioVersion->loadAudioVersionByVersion();
        $abaFollowUpProgressVersion->setNextProgressVersion();
        $abaFollowUpProgressVersionHtml->setNextProgressVersionHtml();
        $abaFollowUpAudioVersion->setNextAudioVersion();
        $this->idUser =                 $user->id;
        $this->progressVersion =        $abaFollowUpProgressVersion->idVersion;
        $this->progressVersionHtml =    $abaFollowUpProgressVersionHtml->idVersion;
        $this->audioVersion =           $abaFollowUpAudioVersion->idVersion;
        if ($this->validate(array("idUser", "progressVersion", "progressVersionHtml", "audioVersion"))) {
            if ($this->insertUserDictionary()) {
                if (!$abaFollowUpProgressVersion->saveNextProgressVersionByVersion($this->progressVersion)) {
                    HeLogger::sendLog(
                      HeLogger::PREFIX_ERR_LOGIC_L . ' Update next process version inconsistency.',
                        HeLogger::IT_BUGS,
                        HeLogger::CRITICAL,
                        ' Function insertUserDictionaryForRegistration - saveNextProgressVersionByVersion - USERID:: '.
                        $user->id
                    );
                }
                if (!$abaFollowUpProgressVersionHtml->saveNextProgressVersionHtmlByVersion(
                    $this->progressVersionHtml
                )) {
                    HeLogger::sendLog(
                      HeLogger::PREFIX_ERR_LOGIC_L . ' Update next process version inconsistency.',
                        HeLogger::IT_BUGS,
                        HeLogger::CRITICAL,
                        ' Function insertUserDictionaryForRegistration - saveNextProgressVersionHtmlByVersion -
                        USERID:: ' . $user->id
                    );
                }
                if (!$abaFollowUpAudioVersion->saveNextAudioVersionByVersion($this->audioVersion)) {
                    HeLogger::sendLog(
                      HeLogger::PREFIX_ERR_LOGIC_L .' Update next audio version inconsistency.',
                        HeLogger::IT_BUGS,
                        HeLogger::CRITICAL,
                        ' Function insertUserDictionaryForRegistration - saveNextAudioVersionByVersion - USERID:: ' .
                        $user->id
                    );
                }
                return true;
            }
        }
        return false;
    }
}
