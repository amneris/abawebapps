<?php
/**
 * @property integer $id
 * @property integer $userId
 * @property integer $default
 * @property string $name
 * @property string $lastName
 * @property string $vatNumber
 * @property string $street
 * @property string $streetMore
 * @property string $zipCode
 * @property string $city
 * @property string $state
 * @property string $country
 *
 */
class CmAbaUserAddressInvoice extends AbaActiveRecord
{
    public $id;

    /**
     *
     */
    public function __construct($scenario=NULL, $dbConnName=NULL) {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_address_invoice';
    }

    /**
     * Before the save method is executed.
     * @return array
     */
    public function rules()
    {
        return array(
            array('userId', 'required'),
        );
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return "`id`, `userId`, `default`, `name`, `lastName`, `vatNumber`, `street`,
                            `streetMore`, `zipCode`, `city`, `state`, `country` ";
    }

    /**
     * @param $userId
     *
     * @return $this|bool
     */
    public function getUserAddressByUserId( $userId )
    {
        $query = "SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " ucf
                    WHERE ucf.userId=:USERID ";
        $paramsToSQL = array(":USERID" => $userId);

        $dataReader = $this->querySQL($query, $paramsToSQL);
        if(($row = $dataReader->read()) !== false)
        {
            $this->fillDataColsToProperties( $row );
            return $this;
        }

        return false;
    }

    /**
     * @param $userId
     * @param $name
     * @param $lastName
     * @param $vatNumber
     * @param $street
     * @param $streetMore
     * @param $zipCode
     * @param $city
     * @param $state
     * @param $country
     * @param int $default
     *
     * @return bool
     */
    public function setUserAddressForm( $userId, $name, $lastName, $vatNumber, $street,
                                                        $streetMore, $zipCode, $city, $state, $country, $default=1 )
    {
        $this->userId = $userId;
        $this->name = $name;
        $this->lastName = $lastName;
        $this->vatNumber = $vatNumber;
        $this->street = $street;
        $this->streetMore = $streetMore;
        $this->zipCode = $zipCode;
        $this->city = $city;
        $this->state = $state;
        $this->country = $country;
        $this->default = $default;

        if( !$this->validate() )
        {
            return false;
        }
        return true;
    }

    public function insertUserAddress()
    {
        $this->beginTrans();
        $successTrans = true;
        //-----------------
        if ($successTrans) {
            $sql = " INSERT INTO  " . $this->tableName() . " " .
                " (`userId`, `default`, `name`, `lastName`, `vatNumber`, `street`,
                        `streetMore`, `zipCode`, `city`, `state`, `country`) " .
                " VALUES (" . $this->userId . ", " . $this->default . ", :NAME, :LASTNAME, :VATNUMBER, :STREET,
                        :STREETMORE, :ZIPCODE, :CITY, :STATE, :COUNTRY)
                   ON DUPLICATE KEY UPDATE
		                `street` = :STREET,
                        `streetMore` = :STREETMORE,
                        `zipCode` = :ZIPCODE,
                        `city` = :CITY,
                        `state`= :STATE,
                        `country`= :COUNTRY
                        ";
            $aBrowserValues = array(":NAME" => $this->name,
                ":LASTNAME" => $this->lastName,
                ":VATNUMBER" => $this->vatNumber,
                ":STREET" => $this->street,
                ":STREETMORE" => $this->streetMore,
                ":ZIPCODE" => strval($this->zipCode),
                ":CITY" => strval($this->city),
                ":STATE" => strval($this->state),
                ":COUNTRY" => strval($this->country),
            );
            $this->id = $this->executeSQL($sql, $aBrowserValues);
            if ($this->id > 0) {
                $user = new CmAbaUser();
                if ($user->populateUserById($this->userId)) {
                    if (!$user->logUserActivity->saveUserLogActivity(
                        $this->tableName(),
                        "New address inserted, default true" . $this->default,
                        "Payment"
                    )) {
                        $successTrans = false;
                    }
                }
            } else {
                $successTrans = false;
            }
        }
        //---------------
        if ($successTrans) {
            $this->commitTrans();
            return $this->id;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }
}
