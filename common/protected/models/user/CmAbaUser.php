<?php

/**
 * CmAbaUser
 * Author: mgadegaard
 * Date: 02/04/2015
 * Time: 16:30
 * © ABA English 2015
 *
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $name
 * @property string $surnames
 * @property string $email
 * @property string $password
 * @property string $complete
 * @property string $city
 * @property integer $countryId
 * @property string $countryIdCustom
 * @property string $birthDate
 * @property string $telephone
 * @property string $langEnv
 * @property string $langCourse
 * @property string $userType
 * @property string $currentLevel
 * @property string $entryDate
 * @property string $expirationDate
 * @property string $teacherId
 * @property string $idPartnerFirstPay
 * @property string $releaseCampus
 * @property string $idPartnerSource
 * @property string $idPartnerCurrent
 * @property integer $firstLoginDone
 * @property integer $savedUserObjective
 * @property string $keyExternalLogin
 * @property string $gender
 * @property string $cancelReason
 * @property string $registerUserSource
 * @property string $hasWorkedCourse
 * @property string $dateLastSentSelligent
 * @property string $lastActionFixSql
 * @property string $followUpAtFirstPay
 * @property string $watchedVideosAtFirstPay
 * @property string $idSourceList
 * @property string $deviceTypeSource
 * @property string $dateUpdateEmma
 * @property string $lastLoginTime
 * @property string $fbId
 * @property string $registerLevel
 */

class CmAbaUser extends AbaActiveRecord
{
//    public $id; //primary key
    /**
     * @var CmLogUserActivity
     */
    public $logUserActivity;
//    protected $errorMsg;
//    protected $_md;

    // **********************---------------START OF YII NECESSARY FUNCTIONS---------------*****************************
    /**
     *  Main constructor
     *
     * @param null $scenario Variable controls functionality of Active Record instance
     * @param null $dbConnName possibility to define the database connection
     */
    public function __construct($scenario = null, $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * Returns the static CmAbaUser of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return CmAbaUser the static CmAbaUser class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user';
    }

    /**
     * Before the save method is executed.
     * @return array
     */
    public function rules()
    {
        return array(
            array('id', 'required'),
            array('name', 'required'),
            array('surnames', 'required'),
            array('email', 'email', 'allowEmpty' => false),
            array('birthDate', 'date', 'format' => 'dd-mm-yyyy'),
            array('countryId', 'type', 'type' => 'integer', 'allowEmpty' => true),
            array('city', 'type', 'type' => 'string', 'allowEmpty' => true),
            array('telephone', 'type', 'type' => 'string', 'allowEmpty' => true),
            array('currentLevel', 'type', 'type' => 'integer', 'allowEmpty' => true),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }
    // **********************----------------END OF YII NECESSARY FUNCTIONS----------------*****************************

    /**
     * Used in config/main.php, messages Extension.
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->email;
    }

    /**
     * Used from Messages extension
     *
     * @param $email
     *
     * @return array
     */
    public function getSuggest($email)
    {
        $cDbCriteria = new CDbCriteria();
        $cDbCriteria->alias = 'u';
        $cDbCriteria->condition = " u.teacherId=" . Yii::app()->user->getId() . " AND u.email LIKE '%" . $email . "%' ";
        $cDbCriteria->order = ' u.name ASC';
        $users = $this->findAll($cDbCriteria);

        return $users;
    }

    /**
     * All fields available in this model
     *
     * @return string
     */
    public function mainFields()
    {
        return "u.`id`, u.`name`, u.`surnames`, u.`email`, u.`password` as `password`, u.`complete`, " .
        " u.`city`, u.`countryId`, u.`countryIdCustom`, " .
        "  DATE_FORMAT(u.birthDate,'%d-%m-%Y') as 'birthDate'," .
        " u.`telephone`, u.`langEnv`, u.`langCourse`, u.`userType`, u.`currentLevel`, " .
        " DATE_FORMAT(u.entryDate,'%d-%m-%Y') as 'entryDate', " .
        " DATE_FORMAT(u.expirationDate,'%d-%m-%Y') as 'expirationDate', " .
        " u.`teacherId`, u.`idPartnerFirstPay`, u.`releaseCampus`, u.`idPartnerSource`, u.`idPartnerCurrent`, " .
        " u.`savedUserObjective`, u.`firstLoginDone`, u.`keyExternalLogin`, u.`gender`, " .
        " u.`cancelReason`, u.`registerUserSource`, u.`hasWorkedCourse`, u.`dateLastSentSelligent`, " .
        "u.`lastActionFixSql`, u.`followUpAtFirstPay`, u.`watchedVideosAtFirstPay`, u.`idSourceList`, " .
        "u.`deviceTypeSource`, u.`dateUpdateEmma`, u.`lastLoginTime`, u.`fbId`, u.`registerLevel` ";
    }

    /**
     * @param $password
     *
     * @return bool
     */
    public function validatePassword($password)
    {
        return $this->hashPassword($password) === $this->password;
    }

    /**
     * @param $password
     *
     * @return string
     */
    public function hashPassword($password)
    {
        return md5($password);
    }


    /**
     * @param $idUser
     */
    public function setId($idUser)
    {
        $this->id = $idUser;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $inId
     *
     * @return bool|CmAbaUser
     */
    public function populateUserById($inId = 0)
    {
        if ($inId == 0) {
            $inId = $this->id;
        }
        $query = "SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " u WHERE id=$inId";
        $dataReader = $this->querySQL($query);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            $this->logUserActivity = new CmLogUserActivity($this);

            return $this;
        }

        return false;
    }

    /**
     * Initializes the object AbaUser based on email. Can be used to authenticate an user.
     *
     * @param string $email
     * @param string $password
     * @param bool   $usePassword
     * @param bool   $pwdEncrypted
     *
     * @return bool
     */
    public function populateUserByEmail($email, $password = "", $usePassword = false, $pwdEncrypted = false)
    {
        if (is_array($email)) {
            $email = $email['email'];
        }
        $verifyPassword = ($usePassword == true) ? " AND (u.`password`='" . md5($password) .
            "' OR u.`password`=CONCAT('(',:PASSWORD,')') )" :
            "";
        $verifyPassword = ($usePassword && $pwdEncrypted) ? " AND (u.`password`='" . $password .
            "' OR u.`password`=CONCAT('(',:PASSWORD,')') )" :
            $verifyPassword;
        $sql = "SELECT " . $this->mainFields() . " FROM " . $this->tableName() .
            " u WHERE u.`email` = :EMAIL  $verifyPassword";
        $paramsToSQL = array(":EMAIL" => $email);
        if ($verifyPassword !== '') {
            $paramsToSQL[":PASSWORD"] = $password;
        }
        $dataReader = $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            $this->logUserActivity = new CmLogUserActivity($this);

            return $this;
        }
        return false;
    }

    public function getUserIdByKeyExternalLogin($keyExternalLogin)
    {
        $query = "SELECT u.id FROM " . $this->tableName() . " u WHERE u.keyExternalLogin = :key_param";
        $param = array('key_param' => $keyExternalLogin);
        $dataReader = $this->querySQL($query, $param);
        if (($row = $dataReader->read()) !== false) {
            return $row['id'];
        }

        return false;
    }

    public function getPartnerIdbyUserId($id) {
        $query = "SELECT u.idPartnerSource FROM " . $this->tableName() . " u WHERE u.id = :key_param";
        $param = array('key_param' => $id);
        $dataReader = $this->querySQL($query, $param);
        if (($row = $dataReader->read()) !== false) {
            return $row['idPartnerSource'];
        }

        return false;
    }

    public function getSourceIdbyUserId($id) {
        $query = "SELECT u.idSourceList FROM " . $this->tableName() . " u WHERE u.id = :key_param";
        $param = array('key_param' => $id);
        $dataReader = $this->querySQL($query, $param);
        if (($row = $dataReader->read()) !== false) {
            return $row['idSourceList'];
        }

        return false;
    }

    /**
     * It is used to save the date in which operation was sent to Selligent
     *
     * @param $value
     * @return bool
     */
    public function updateDateLastSentSelligent($value = null)
    {
        $sql = "UPDATE user SET `dateLastSentSelligent`=:DATE_LAST_SENT_SELLIGENT WHERE id = " . $this->id . ";";
        $this->dateLastSentSelligent = $value;
        $aBrowserValues = array(":DATE_LAST_SENT_SELLIGENT" => $value);
        if ($this->updateSQL($sql, $aBrowserValues) > 0) {
            if ($this->logUserActivity->saveUserChangesData(
                $this,
                "Just sent an operation to selligent, save dateLastSent.",
                "Sent info to Selligent"
            )) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    /**
         * @param string $newPassword
         * @param bool $toEncrypt
         *
         * @throws CException
         */
    public function setPassword($newPassword, $toEncrypt = true)
    {
        if ($newPassword == "" || $this->email == "") {
            throw new CException("ABA email $this->email or password $newPassword are not valid.
            Trying to set password.");
        }
        if (!$toEncrypt) {
            $this->password = "(" . $newPassword . ")";
        } else {
            $this->password = md5($newPassword);
        }
        $this->keyExternalLogin = md5($this->email . $this->password);
    }

    /**
     * Used to insert new FREE user into the database. This should be the only function used to insert users into DB!!
     * @param int $updateUser
     * @return bool
     */
    public function insertUserForRegistration($updateUser = 0)
    {
        $result = $this->insertUser($updateUser);
        // User dictionary
        if ($result) {
            $oAbaUserDictionary = new CmAbaUserDictionary();
            if (!$oAbaUserDictionary->insertUserDictionaryForRegistration($this)) {
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_LOGIC_L .' Create (Insert) user dictionary inconsistency.',
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    ' Function insertUserForRegistration / insertUserDictionaryForRegistration '
                );
            } else {
                //Check if we need to apply a test
                $clsAbaCouseTest = new CmAbaCourseTest();
                $tmpidTest = $clsAbaCouseTest->retrieveTest($this);
            }
            //#6254
            $stProfiles = CmUserRules::getOrCreateUserExperimentProfilesWithType($this, CmAbaExperimentsTypes::EXPERIMENT_TYPE_SCENARIOS_PAYMENTS, true);
        }
        return $result;
    }

    /**
         * It updates the field currentLevel.
         *
         * @param $level
         * @return bool|int
         */
    public function updateUserCourseLevel($level)
    {
        $this->currentLevel = $level;
        if (!is_null($level)) {
            $query = "UPDATE " . $this->tableName() . " SET `currentLevel`=:LEVEL where `id`=:USERID";
            $params = array(":LEVEL" => $this->currentLevel, ":USERID" => $this->getId());
            if ($this->updateSQL($query, $params) > 0) {
                /*if ($this->abaUserLogUserActivity->saveUserChangesData(
                    $this,
                    "Change user level from objectives form.",
                    "User Objectives Form"
                )) {
                    return true;
                }
                return false;*/
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * @param int $updateUser
     *
     * @return bool
     */
    public function insertUser()
    {
        $this->beginTrans();
        $sql = " INSERT INTO user (`name`, `surnames`, `email`, `password`, `langEnv`, `langCourse`,
                                `teacherId`, `userType`, `currentLevel`,
                                 `expirationDate`, `entryDate`,
                                `idPartnerSource`, `keyExternalLogin`, `countryId`, `countryIdCustom`,
                                `registerUserSource`, `idPartnerCurrent`, `idSourceList`, `deviceTypeSource` )
                VALUES           ( :NAME, :SURNAMES, :EMAIL, :PASSWORD, :LANGENV, :LANGCOURSE,
                                  :TEACHERID, :USERTYPE, :CURRENTLEVEL,
                                  :EXPIRATIONDATE, :ENTRYDATE,
                                  :IDPARTNERSOURCE, '" . $this->keyExternalLogin . "', :COUNTRYID, :COUNTRYIDCUSTOM,
                                  :REGISTERUSERSOURCE, :IDPARTNERCURRENT, :IDSOURCELIST, :DEVICETYPESOURCE  )
		        ON DUPLICATE KEY UPDATE
		                `name` = :NAME,
                        `surnames` = :SURNAMES,
                        `password` = :PASSWORD,
                        `langEnv` = :LANGENV,
                        `langCourse` = :LANGCOURSE,
                        `teacherId` = :TEACHERID,
                        `userType` = :USERTYPE,
                        `currentLevel`= :CURRENTLEVEL,
                        `expirationDate` = :EXPIRATIONDATE,
                        `entryDate` = :ENTRYDATE,
                        `keyExternalLogin` = '" . $this->keyExternalLogin . "',
                        `countryId` = :COUNTRYID,
                        `countryIdCustom` = :COUNTRYIDCUSTOM,
                        `registerUserSource` = :REGISTERUSERSOURCE,
                        `idPartnerSource`= :IDPARTNERSOURCE,
                        `idSourceList`= :IDSOURCELIST,
                        `deviceTypeSource`= :DEVICETYPESOURCE
                        ";
        $aBrowserValues = array(":EMAIL" => $this->email, ":PASSWORD" => $this->password, ":NAME" => $this->name,
            ":SURNAMES" => $this->surnames, ":LANGENV" => $this->langEnv, ":LANGCOURSE" => $this->langEnv,
            ":TEACHERID" => $this->teacherId, ":USERTYPE" => $this->userType, ":CURRENTLEVEL" => $this->currentLevel,
            ":EXPIRATIONDATE" => $this->expirationDate, ":ENTRYDATE" => $this->entryDate,
            ":IDPARTNERSOURCE" => $this->idPartnerSource, ":COUNTRYID" => $this->countryId,
            ":COUNTRYIDCUSTOM" => $this->countryIdCustom, ':REGISTERUSERSOURCE' => $this->registerUserSource,
            ":IDPARTNERCURRENT" => $this->idPartnerCurrent, ":IDSOURCELIST" => $this->idSourceList,
            ":DEVICETYPESOURCE" => $this->deviceTypeSource);
        $idUser = $this->executeSQL($sql, $aBrowserValues);
        if ($idUser > 0) {
            $this->id = $idUser;
            $this->logUserActivity = new CmLogUserActivity($this);
            if ($this->logUserActivity->saveUserLogActivity(
                "user",
                " New user registered from createUserRegistration " . $this->email,
                "insertUserForRegistration"
            )) {
                $this->commitTrans();
                return true;
            } else {
                $this->rollbackTrans();
                return false;
            }
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @param $iFbid
     * @return bool
     */
    public function populateUserByFacebookId($iFbid)
    {
        if (!is_numeric($iFbid) || $iFbid == 0) {
            return false;
        }
        $query = "SELECT ".$this->mainFields()." FROM ".$this->tableName()." u WHERE fbId='$iFbid'";
        $dataReader =  $this->querySQL($query);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            $this->logUserActivity = new CmLogUserActivity($this);
            return true;
        }
        return false;
    }

    /**
     * @param integer $userType
     * @param null $idPartner
     *
     * @return bool
     */
    public function updateUserType($userType, $idPartner = null)
    {
        $this->beginTrans();
        if (!isset($idPartner)) {
            $idPartner = Yii::app()->config->get("ABA_PARTNERID") ;
        }
        $sql= "UPDATE ".$this->tableName()." set userType=".$userType.", idPartnerCurrent=:IDPARTNERCURRENT  WHERE id=".
            $this->id." ";
        $aBrowserValues = array( ":IDPARTNERCURRENT"=> $idPartner  );
        if ($this->updateSQL($sql, $aBrowserValues) > 0) {
            if ($this->logUserActivity->saveUserChangesData(
                $this,
                "Payment done: user paid so user type becomes ".$userType,
                "Modificar Datos"
            )) {
                $this->commitTrans();
                return true;
            }
        } else {
            $this->rollbackTrans();
            return false;
        }
        return false;
    }

    /**
     * Updates only Progress field
     * @param $maxProgress
     *
     * @return bool
     */
    public function updateFollowUpAtFirstPay($maxProgress)
    {
        $this->followUpAtFirstPay = $maxProgress;
        $this->beginTrans();
        $sql= " UPDATE ".$this->tableName()." SET followUpAtFirstPay=".$maxProgress.
            " WHERE id=".$this->id." ";
        if ($this->updateSQL($sql)>0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @param $quantity
     *
     * @return bool
     */
    public function updateWatchedVideos($quantity)
    {
        $this->watchedVideosAtFirstPay = $quantity;
        $this->beginTrans();
        $sql= " UPDATE ".$this->tableName()." SET watchedVideosAtFirstPay=".$quantity." WHERE id=".$this->id." ";
        if ($this->updateSQL($sql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @param $iId
     *
     * @return $this|bool
     */
    public function getUserByApiUserId($iId)
    {

        $sSql = "SELECT  u.*  FROM " . $this->tableName() . " AS u WHERE u.`id` = :ID ";

        $paramsToSQL = array(":ID" => $iId);

        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    public function getCurrencyByUserId($userId)
    {

        $sql ="SELECT ABAIdCurrency
                FROM user u
                INNER JOIN country c ON u.countryId = c.id
                WHERE u.id = :ID;";

        $paramsToSQL = array(":ID" => $userId);

        $dataReader = $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            return $row;
        }
        return false;
    }

    public function getCountryByUserId($userId)
    {

        $sql ="SELECT iso3, c.id AS idCountry
                FROM user u
                INNER JOIN country c ON u.countryId = c.id
                WHERE u.id = :ID;";

        $paramsToSQL = array(":ID" => $userId);

        $dataReader = $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {

            if($row['iso3'] == "AAA"){
                $row['iso3'] = "ABA";
                $row['idCountry'] = 301;
            }

            return $row;
        }
        return false;
    }

    /**
     * @param $iso3
     *
     * @return array|bool|false
     */
    public function getCountryByIso3($iso3)
    {

        $sql = "
            SELECT c.iso3, c.id AS idCountry
            FROM country AS c
            WHERE c.iso3 = :ISO3;
        ";

        $paramsToSQL = array(":ISO3" => $iso3);

        $dataReader = $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {

            if ($row['iso3'] == "AAA") {
                $row['iso3'] = "ABA";
                $row['idCountry'] = 301;
            }
            return $row;
        }
        return false;
    }

    /**
     * #ZOR-558
     *
     * @param $cancelReason
     * @param $expirationDate
     * @return bool
     *
     * @throws CDbException
     */
    public function updateCancelReasonAndUserFree($cancelReason, $expirationDate)
    {
        $this->beginTrans();
        $sSql = "
            UPDATE " . $this->tableName() . "
            SET
                `cancelReason`='" . $cancelReason . "',
                `expirationDate`='" . $expirationDate . "',
                `userType`='" . FREE . "'
            WHERE
                `id` = '" . $this->id . "'
        ; ";
        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }


    /**
     * #ZOR-267
     *
     * @param $cancelReason
     * @param $expirationDate
     * @return bool
     *
     * @throws CDbException
     */
    public function updateCancelReason($cancelReason, $expirationDate)
    {
        $this->beginTrans();
        $sSql = "
            UPDATE " . $this->tableName() . "
            SET
                `cancelReason`='" . $cancelReason . "',
                `expirationDate`='" . $expirationDate . "'
            WHERE
                `id` = '" . $this->id . "'
        ; ";
        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

    /**
     * #ZOR-267
     *
     * @param $userType
     * @param $expirationDate
     * @param $idPartnerCurrent
     *
     * @return bool
     * @throws CDbException
     */
    public function updateUserRenewal($userType, $expirationDate, $idPartnerCurrent)
    {
        $this->beginTrans();
        $sql = " UPDATE user set `userType` = " . $userType . ", `expirationDate`='$expirationDate', `idPartnerCurrent` = '$idPartnerCurrent'
                WHERE id=" . $this->id . " ";
        if ($this->updateSQL($sql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }

        return false;
    }

    /**
     * @param $moUser
     * @param $expirationDate
     * @param $typeUser
     *
     * @return bool
     */
    public function updateRenewalsCancels($moUser, $expirationDate, $userType){

        if(!$this->updateExpirationAndType($expirationDate,$userType)) {
            return false;
        }

        if($userType==PREMIUM){
            try {
                $firstUnitForLevels = HeUnits::getArrayStartUnitsByLevel(1, 6);

                $followUp = new AbaFollowup();

                $maxProgress = $followUp->getMaxFollowUp($moUser->id, $firstUnitForLevels);

                $this->updateFollowUpAtFirstPay($maxProgress);

                $watchedVideos = $followUp->getNumberOfVideosWatched($moUser->id);

                $this->updateWatchedVideos($watchedVideos);
            }
            catch (Exception $e) { }
        }

        try {
            $commSelligent = new CmSelligentConnector();
            $commSelligent->sendOperationToSelligent(
                CmSelligentConnector::SYNCHRO_USER,
                $moUser,
                array(),
                "Update User expiration and type"
            );
        }
        catch(Exception $e) { }

        return true;
    }

    /**
     * @param $expirationDate
     * @param $moUser
     *
     * @return bool
     */
    public function updateUserToPremium($expirationDate, $moUser)
    {
        if(!$this->updateExpirationAndType($expirationDate)) {
            return false;
        }

        try {
            $firstUnitForLevels = HeUnits::getArrayStartUnitsByLevel(1, 6);

            $followUp = new AbaFollowup();

            $maxProgress = $followUp->getMaxFollowUp($moUser->id, $firstUnitForLevels);

            $this->updateFollowUpAtFirstPay($maxProgress);

            $watchedVideos = $followUp->getNumberOfVideosWatched($moUser->id);

            $this->updateWatchedVideos($watchedVideos);
        }
        catch (Exception $e) { }


        try {
            $commSelligent = new CmSelligentConnector();
            $commSelligent->sendOperationToSelligent(
              CmSelligentConnector::SYNCHRO_USER,
              $moUser,
              array(),
              "Update User To Premium"
            );
        }
        catch(Exception $e) { }

        return true;
    }

    /**
     * @param $expirationDate
     * @param int $userType
     *
     * @return bool
     */
    public function updateExpirationAndType ($expirationDate, $userType=PREMIUM) {

        $this->beginTrans();
        $sSql = "
            UPDATE " . $this->tableName() . "
            SET
                `expirationDate`='" . $expirationDate . "',
                `userType`='" . $userType . "'
            WHERE
                `id` = '" . $this->id . "'
        ; ";
        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

    /**
     * @param $email
     *
     * @return bool
     */
    public function getUserByEmail($email)
    {
        $sql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " u
            WHERE
                u.`email` = :EMAIL
        ";
        $paramsToSQL = array(":EMAIL" => $email);
        $dataReader = $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return true;
        }
        return false;
    }

}
