<?php
/**
 * Created by PhpStorm.
 * User: jordi
 * Date: 02/02/2015
 * Time: 15:48
 */

class CmAbaUserMigration extends AbaActiveRecord
{
    public function __construct($scenario = null, $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('userType', 'required'),
            array('countryId, firstLoginDone, savedUserObjective', 'numerical', 'integerOnly' => true),
            array('name, surnames, email, city', 'length', 'max' => 200),
            array('password, lastActionFixSql', 'length', 'max' => 50),
            array('complete, idPartnerFirstPay, idPartnerSource, idPartnerCurrent', 'length', 'max' => 3),
            array('countryIdCustom, teacherId', 'length', 'max' => 4),
            array('telephone', 'length', 'max' => 20),
            array('langEnv, langCourse, currentLevel, idSourceList', 'length', 'max' => 5),
            array('userType, watchedVideosAtFirstPay', 'length', 'max' => 2),
            array('releaseCampus, cancelReason, followUpAtFirstPay', 'length', 'max' => 10),
            array('keyExternalLogin', 'length', 'max' => 100),
            array('gender, registerUserSource, hasWorkedCourse, deviceTypeSource', 'length', 'max' => 1),
            array('fbId', 'length', 'max' => 32),
            array('birthDate, entryDate, expirationDate, dateLastSentSelligent, dateUpdateEmma, lastLoginTime', 'safe'),
            array('id, name, surnames, email, password, complete, city, countryId, countryIdCustom, birthDate,
            telephone, langEnv, langCourse, userType, currentLevel, entryDate, expirationDate, teacherId,
            idPartnerFirstPay, releaseCampus, idPartnerSource, idPartnerCurrent, firstLoginDone, savedUserObjective,
            keyExternalLogin, gender, cancelReason, registerUserSource, hasWorkedCourse, dateLastSentSelligent,
            lastActionFixSql, followUpAtFirstPay, watchedVideosAtFirstPay, idSourceList, deviceTypeSource,
            dateUpdateEmma, lastLoginTime, fbId', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'surnames' => 'Surnames',
            'email' => 'Email',
            'password' => 'Password',
            'complete' => 'Complete',
            'city' => 'City',
            'countryId' => 'Country',
            'countryIdCustom' => 'Country Id Custom',
            'birthDate' => 'Birth Date',
            'telephone' => 'Telephone',
            'langEnv' => 'Lang Env',
            'langCourse' => 'Lang Course',
            'userType' => 'User Type',
            'currentLevel' => 'Current Level',
            'entryDate' => 'Entry Date',
            'expirationDate' => 'Expiration Date',
            'teacherId' => 'Teacher',
            'idPartnerFirstPay' => 'Id Partner First Pay',
            'releaseCampus' => 'Release Campus',
            'idPartnerSource' => 'Id Partner Source',
            'idPartnerCurrent' => 'Id Partner Current',
            'firstLoginDone' => 'First Login Done',
            'savedUserObjective' => 'Saved User Objective',
            'keyExternalLogin' => 'Key External Login',
            'gender' => 'Gender',
            'cancelReason' => 'Cancel Reason',
            'registerUserSource' => 'Register User Source',
            'hasWorkedCourse' => 'Has Worked Course',
            'dateLastSentSelligent' => 'Date Last Sent Selligent',
            'lastActionFixSql' => 'Last Action Fix Sql',
            'followUpAtFirstPay' => 'Follow Up At First Pay',
            'watchedVideosAtFirstPay' => 'Watched Videos At First Pay',
            'idSourceList' => 'Id Source List',
            'deviceTypeSource' => 'Device Type Source',
            'dateUpdateEmma' => 'Date Update Emma',
            'lastLoginTime' => 'Last Login Time',
            'fbId' => 'Fb',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('surnames', $this->surnames, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('complete', $this->complete, true);
        $criteria->compare('city', $this->city, true);
        $criteria->compare('countryId', $this->countryId);
        $criteria->compare('countryIdCustom', $this->countryIdCustom, true);
        $criteria->compare('birthDate', $this->birthDate, true);
        $criteria->compare('telephone', $this->telephone, true);
        $criteria->compare('langEnv', $this->langEnv, true);
        $criteria->compare('langCourse', $this->langEnv, true);
        $criteria->compare('userType', $this->userType, true);
        $criteria->compare('currentLevel', $this->currentLevel, true);
        $criteria->compare('entryDate', $this->entryDate, true);
        $criteria->compare('expirationDate', $this->expirationDate, true);
        $criteria->compare('teacherId', $this->teacherId, true);
        $criteria->compare('idPartnerFirstPay', $this->idPartnerFirstPay, true);
        $criteria->compare('releaseCampus', $this->releaseCampus, true);
        $criteria->compare('idPartnerSource', $this->idPartnerSource, true);
        $criteria->compare('idPartnerCurrent', $this->idPartnerCurrent, true);
        $criteria->compare('firstLoginDone', $this->firstLoginDone);
        $criteria->compare('savedUserObjective', $this->savedUserObjective);
        $criteria->compare('keyExternalLogin', $this->keyExternalLogin, true);
        $criteria->compare('gender', $this->gender, true);
        $criteria->compare('cancelReason', $this->cancelReason, true);
        $criteria->compare('registerUserSource', $this->registerUserSource, true);
        $criteria->compare('hasWorkedCourse', $this->hasWorkedCourse, true);
        $criteria->compare('dateLastSentSelligent', $this->dateLastSentSelligent, true);
        $criteria->compare('lastActionFixSql', $this->lastActionFixSql, true);
        $criteria->compare('followUpAtFirstPay', $this->followUpAtFirstPay, true);
        $criteria->compare('watchedVideosAtFirstPay', $this->watchedVideosAtFirstPay, true);
        $criteria->compare('idSourceList', $this->idSourceList, true);
        $criteria->compare('deviceTypeSource', $this->deviceTypeSource, true);
        $criteria->compare('dateUpdateEmma', $this->dateUpdateEmma, true);
        $criteria->compare('lastLoginTime', $this->lastLoginTime, true);
        $criteria->compare('fbId', $this->fbId, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaUserMigration the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getByUserId($idUser)
    {
        $query = "SELECT * FROM " . $this->tableName() . " i WHERE i.id=:IDUSER ";
        $paramsToSQL = array(":IDUSER" => $idUser);
        $dataReader = $this->querySQL($query, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    public function getUserNUsersToMigrate($nUsers = null)
    {   //Devolvemos $nUsers usuarios que no hayan sido migrados.
        if (is_null($nUsers)) {
            throw new CDbException("ERROR: We need to know the numbers of users to migrate.");
        }

        If (!is_numeric($nUsers)) {
            throw new CDbException("ERROR: The parameters provided are not numbers.");
        }

        $sql = "Select t1.id from user t1 left join user_dictionary t2 on t1.id=t2.idUser where t2.idUser IS null limit $nUsers;";

        $dataReader = $this->querySQL($sql);
        $usersToMigrate = $dataReader->readAll();
        if (sizeof($usersToMigrate) > 0) {
            return $usersToMigrate;
        }

        return false;
    }

    public function copyUserData($idUsers = null, $progressVersion = null, $progressVersionHtml = null, $audioVersion = null)
    {   //Devolvemos $nUsers usuarios que no hayan sido migrados.
        if (is_null($idUsers) || is_null($progressVersion) || is_null($progressVersionHtml) || is_null($audioVersion)) {
            throw new CDbException("ERROR: We need to know the user number to migrate.");
        }
        If (!is_numeric($idUsers) || !is_numeric($progressVersion) || !is_numeric($progressVersionHtml) || !is_numeric($audioVersion)) {
            throw new CDbException("ERROR: The parameters provided are not numbers.");
        }

        //Hacemos atómico el proceso.
        Yii::app()->params['transRunning'] = $this->connection->beginTransaction();

        $Sql = "insert into aba_b2c.user_dictionary (idUser,progressVersion,progressVersionHtml,audioVersion) VALUES (:idUsers,:progressVersion,:progressVersionHtml,:audioVersion)";
        $paramsToSQL = array(":idUsers" => $idUsers, ":progressVersion" => $progressVersion, ":progressVersionHtml" => $progressVersionHtml, ":audioVersion" => $audioVersion);

        if (!($this->executeSQL($Sql, $paramsToSQL, true) === false)) {
            $Sql = "insert into aba_b2c.followup4_$progressVersion (themeid, userid, lastchange, all_por, all_err, all_ans, all_time, sit_por, stu_por, dic_por, dic_ans, dic_err, rol_por, gra_por, gra_ans, gra_err, wri_por, wri_ans, wri_err, new_por, spe_por, spe_ans, time_aux, gra_vid, rol_on, exercises, eva_por) " .
                "Select themeid, userid, lastchange, all_por, all_err, all_ans, all_time, sit_por, stu_por, dic_por, dic_ans, dic_err, rol_por, gra_por, gra_ans, gra_err, wri_por, wri_ans, wri_err, new_por, spe_por, spe_ans, time_aux, gra_vid, rol_on, exercises, eva_por  from followup4 " .
                "WHERE userid=:idUsers and themeid<145 group by userid,themeid";
            $paramsToSQL = array(":idUsers" => $idUsers);
            if (!(($insertadosFollowup4 = $this->executeSQL($Sql, $paramsToSQL, true)) === false)) {
                $Sql = "" .
                    "INSERT INTO aba_b2c.followup_html4_$progressVersionHtml (userid,themeid,page,section,`action`,audio,bien,mal,lastchange,wtext,difficulty,time_aux,contador) " .
                    "SELECT userid,themeid,page,section,`action`,audio,bien,mal,lastchange,wtext,difficulty,time_aux,contador AS ultimo " .
                    "FROM aba_b2c.followup_html4 T1 " .
                    "INNER JOIN  " .
                    " ( " .
                    "SELECT MAX(followup_html4.id) AS LastUpdated " .
                    "FROM aba_b2c.followup_html4 " .
                    "WHERE followup_html4.userid=:idUsers AND 	 " .
                    "	( " .
                    "	 (followup_html4.section='STUDY' AND followup_html4.action='RECORDED') OR 	 " .
                    "	 (followup_html4.section='DICTATION' AND followup_html4.action='DICTATION') OR 	 " .
                    "	 (followup_html4.section='ROLEPLAY' AND followup_html4.action='RECORDED') OR 	 " .
                    "	 (followup_html4.section='WRITING' AND followup_html4.action='WRITTEN') OR 	 " .
                    "	 (followup_html4.section='NEWWORDS' AND followup_html4.action='RECORDED') OR 	 " .
                    "	 (followup_html4.section='STUDY' AND followup_html4.action='LISTENED') OR 	 " .
                    "	 (followup_html4.section='ROLEPLAY' AND followup_html4.action='LISTENED') OR 	 " .
                    "	 (followup_html4.section='NEWWORDS' AND followup_html4.action='LISTENED') OR 	 " .
                    "	 (followup_html4.section='GRAMMAR' AND followup_html4.action='LISTENED') 	 " .
                    "	) " .
                    "GROUP BY followup_html4.themeid,followup_html4.page, followup_html4.section,followup_html4.action, followup_html4.audio " .
                    "HAVING followup_html4.audio<>'' AND  " .
                    "			 followup_html4.section<>'' AND  " .
                    "			 followup_html4.action<>'' AND followup_html4.themeid<145 " .
                    ") T2 ON T1.id=T2.LastUpdated ";

                $paramsToSQL = array(":idUsers" => $idUsers);
                if (!(($insertadosFollowup_html4 = $this->executeSQL($Sql, $paramsToSQL, true)) === false)) {
                    Yii::app()->params['transRunning']->commit();
                    return ['followup4' => $insertadosFollowup4, 'followup_html4' => $insertadosFollowup_html4];

                } else {
                    HeLogger::sendLog(
                        " Error migrando usuarios",
                        HeLogger::IT_BUGS,
                        HeLogger::CRITICAL,
                        "Aba Exception in insert " . $Sql
                    );
                    Yii::app()->params['transRunning']->rollback();
                    return false;
                }
            } else {
                HeLogger::sendLog(
                    " Error migrando usuarios",
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    "Aba Exception in insert " . $Sql
                );
                Yii::app()->params['transRunning']->rollback();
                return false;
            }
        } else {
            HeLogger::sendLog(
                " Error migrando usuarios",
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                "Aba Exception in insert " . $Sql
            );
            Yii::app()->params['transRunning']->rollback();
            return false;
        }
    }

    public function createUserInUserDictionary($idUser = null)
    {
        if (is_null($idUser)) {
            throw new CDbException("ERROR: We need to know the user number to migrate.");
        }
        if (!is_numeric($idUser)) {
            throw new CDbException("ERROR: The parameters provided are not numbers.");
        }
        $this->getByUserId($idUser);
        $oAbaUserDictionary = new AbaUserDictionary();
        if (($userDictionary = $oAbaUserDictionary->insertUserDictionaryForRegistration($this)) == false) {
            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_LOGIC_L . ' Create (Insert) user dictionary inconsistency.',
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                ' Function insertUserForRegistration / insertUserDictionaryForRegistration '
            );
            return false;
        }
        return $userDictionary;
    }

    public function deleteAlreadyMigrateuserData($idUser = null)
    {
        if (is_null($idUser)) {
            throw new CDbException("ERROR: We need to know the user number to migrate.");
        }
        if (!is_numeric($idUser)) {
            throw new CDbException("ERROR: The parameters provided are not numbers.");
        }
        $sql = "delete from followup4 where userid=:idUsers;";
        $paramsToSQL = array(":idUsers" => $idUser);
        try {
            $borradosFollowUp4=$this->executeSQL($sql, $paramsToSQL, true);
        } catch (Exception $Error) {
            HeLogger::sendLog(
                "Error borrando restos de migración",
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                "Aba Exception in delete " . $sql . "; exception=" . $Error->getMessage()
            );
            return false;
        }

        $sql = "delete from followup_html4 where userid=:idUsers;";
        $paramsToSQL = array(":idUsers" => $idUser);
        try {
            $borradosFollowUpHtml4 = $this->executeSQL($sql, $paramsToSQL, true);
        } catch (Exception $Error) {
            HeLogger::sendLog(
                "Error borrando restos de migración",
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                "Aba Exception in delete " . $sql . "; exception=" . $Error->getMessage()
            );
            return false;
        }
        return ['followup4'=>$borradosFollowUp4,'followup_html4'=>$borradosFollowUpHtml4];
    }

}