<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $name
 * @property string $surnames
 * @property string $email
 * @property string $password
 * @property string $complete
 * @property string $city
 * @property integer $countryId
 * @property string $countryIdCustom
 * @property string $birthDate
 * @property string $telephone
 * @property string $langEnv
 * @property string $langCourse
 * @property string $userType
 * @property string $currentLevel
 * @property string $entryDate
 * @property string $expirationDate
 * @property string $teacherId
 * @property string $idPartnerFirstPay
 * @property string $releaseCampus
 * @property string $idPartnerSource
 * @property string $idPartnerCurrent
 * @property integer $firstLoginDone
 * @property integer $savedUserObjective
 * @property string $keyExternalLogin
 * @property string $gender
 * @property string $cancelReason
 * @property string $registerUserSource
 * @property string $hasWorkedCourse
 * @property string $dateLastSentSelligent
 * @property string $lastActionFixSql
 * @property string $followUpAtFirstPay
 * @property string $watchedVideosAtFirstPay
 * @property string $idSourceList
 * @property string $deviceTypeSource
 * @property string $dateUpdateEmma
 * @property string $lastLoginTime
 * @property string $fbId
 */
class UserCampus extends BaseUserCampus
{
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseCampusUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getTeacherByLanguage( $isoLanguage ) {
		$isoLanguage2 = "%".$isoLanguage."%";

		$sql = "select u.* from aba_b2c.user u, aba_b2c.user_teacher ut where ut.language like :isoLanguage and ut.userid = u.id ";
		$connection = UserCampus::model()->getDbConnection();
		$command = $connection->createCommand($sql);
		$command->bindParam(":isoLanguage", $isoLanguage2, PDO::PARAM_STR);
		$arUser = $command->queryRow();
		if ( empty($arUser) ) {
			$isoLanguage2 = '%es%';

			$sql = "select u.* from aba_b2c.user u, aba_b2c.user_teacher ut where ut.language like :isoLanguage and ut.userid = u.id ";
			$connection = UserCampus::model()->getDbConnection();
			$command = $connection->createCommand($sql);
			$command->bindParam(":isoLanguage", $isoLanguage2, PDO::PARAM_STR);
			$arUser = $command->queryRow();
		}

		$mUserCampus = new stdClass();
		foreach( $arUser as $field => $value ) {
			$mUserCampus->$field = $value;
		}

		return $mUserCampus;
	}

}
