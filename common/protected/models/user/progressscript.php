<?php

/**
 * Created by PhpStorm.
 * User: annamora
 * Date: 10/12/15
 * Time: 12:33
 */

$_SERVER['DB_HOSTNAME'] = 'dbdev.aba.land';
$_SERVER['DB_USERNAME'] = 'abaadmin';
$_SERVER['DB_PASSWORD'] = 'XZ!FXYT^wDZLCX';

require_once('DbConnection.php');

class FillTheProgress
{
    private $db = null;
    private $firstUnitsLevel = array('beginner' => 1, 'lowerIntermediate' => 25, 'intermediate' => 49,
      'upperIntermediate' => 73, 'advanced' => 97, 'business' => 121);
    private $sections = array(
      array('typeId' => 1, 'name' => 'ABAFILM',    'progressField' => 'sit_por', 'hasTest' => false, 'phrasesType' => 'none',     'actions' => array()),
      array('typeId' => 2, 'name' => 'STUDY',      'progressField' => 'stu_por', 'hasTest' => true, 'phrasesType' => 'complex',  'actions' => array('LISTENED', 'RECORDED')),
      array('typeId' => 3, 'name' => 'DICTATION',  'progressField' => 'dic_por', 'hasTest' => false, 'phrasesType' => 'simple',   'actions' => array('DICTATION')),
      array('typeId' => 4, 'name' => 'ROLEPLAY',   'progressField' => 'rol_por', 'hasTest' => false, 'phrasesType' => 'simple',   'actions' => array('LISTENED', 'RECORDED')),
      array('typeId' => 5, 'name' => 'GRAMMAR',    'progressField' => 'gra_vid', 'hasTest' => false, 'phrasesType' => 'none',     'actions' => array()),
      array('typeId' => 6, 'name' => 'WRITING',    'progressField' => 'wri_por', 'hasTest' => false, 'phrasesType' => 'complex',  'actions' => array('WRITTEN')),
      array('typeId' => 7, 'name' => 'NEWWORDS',   'progressField' => 'new_por', 'hasTest' => true, 'phrasesType' => 'simple',   'actions' => array('LISTENED', 'RECORDED'))
    );

    private $filesOutput = array();
    private $fileLinesCount = 0;

    function FillTheProgress($userId = null, $unit = null) {
        if ($this->dbConnect()) {

            $this->filesOutput[0] = fopen("queries_000000.sql", "w") or die("Unable to open file!");

            $usersProgressToFill = $this->getUsersProgressToFill($userId, $unit);
            $this->fillUsersProgress($usersProgressToFill);

            fclose($this->filesOutput[count($this->filesOutput) - 1]);
        }
    }

    private function dbConnect() {
        $this->db = new DbConnection($_SERVER['DB_HOSTNAME'], $_SERVER['DB_USERNAME'], $_SERVER['DB_PASSWORD']);

        return $this->db !== null;
    }

    private function getUsersProgressToFill($userId = null, $unit = null) {
        $usersProgressToFill = array();

        $query = "SELECT '' AS `progressVersion`
                  UNION ALL
                  SELECT DISTINCT(CONCAT('_', ud.`progressVersion`)) AS `progressVersion`
                  FROM aba_b2c.`user_dictionary` ud;";

        $progressTables = $this->db->query($query);

        while ($progressTable = $this->db->fetch($progressTables)) {
            $query = "SELECT f.`userid` AS `userId`, f.`themeid` AS `unit`,
            IF(f.`".$this->sections[0]['progressField']."` < 100, 1, 0) AS `".$this->sections[0]['name']."`,
            IF(f.`".$this->sections[1]['progressField']."` < 100, 1, 0) AS `".$this->sections[1]['name']."`,
            IF(f.`".$this->sections[2]['progressField']."` < 100, 1, 0) AS `".$this->sections[2]['name']."`,
            IF(f.`".$this->sections[3]['progressField']."` < 100, 1, 0) AS `".$this->sections[3]['name']."`,
            IF(f.`".$this->sections[4]['progressField']."` < 100, 1, 0) AS `".$this->sections[4]['name']."`,
            IF(f.`".$this->sections[5]['progressField']."` < 100, 1, 0) AS `".$this->sections[5]['name']."`,
            IF(f.`".$this->sections[6]['progressField']."` < 100, 1, 0) AS `".$this->sections[6]['name']."`
            FROM aba_b2c.`followup4".$progressTable['progressVersion']."` f
            WHERE ";

            if (is_numeric($unit)) {
                $query .= " f.themeid = $unit AND ";
            }

            if (is_numeric($userId)) {
                $query .= " f.userid = $userId AND ";
            }

            $query .= "(f.`".$this->sections[0]['progressField']."`
                + f.`".$this->sections[1]['progressField']."`
                + f.`".$this->sections[2]['progressField']."`
                + f.`".$this->sections[3]['progressField']."`
                + f.`".$this->sections[4]['progressField']."`
                + f.`".$this->sections[5]['progressField']."`
                + f.`".$this->sections[6]['progressField']."`
            ) < 700

            AND f.`eva_por` >= 8

            ;";

            $tmpProgressToFill = $this->db->query($query);

            while ($progressToFill = $this->db->fetch($tmpProgressToFill)) {

                if (!isset($usersProgressToFill[$progressToFill['userId']])) {
                    $usersProgressToFill[$progressToFill['userId']] = array();
                }

                if (!isset($usersProgressToFill[$progressToFill['userId']][$progressToFill['unit']])) {
                    $usersProgressToFill[$progressToFill['userId']][$progressToFill['unit']] = array();
                }

                foreach($this->sections as $section) {
                    if ($progressToFill[$section['name']] == 1) {
                        array_push($usersProgressToFill[$progressToFill['userId']][$progressToFill['unit']], $section);
                    }
                }
            }

            $this->db->free($tmpProgressToFill);
        }

        $this->db->free($progressTables);

        return $usersProgressToFill;
    }

    private function fillUsersProgress($usersProgressToFill = array()) {
        foreach ($usersProgressToFill as $userId => $units) {
            $this->loopThroughUnits($userId, $units);
        }
    }

    private function loopThroughUnits($userId = 0, $units = array()) {
        $user = $this->getUserCourseInfo($userId);
        foreach ($units as $unitId => $sections) {
            $isTestUnit = $user['course'] > 0 && $this->isFirstUnitLevel($unitId);
            $this->getUnitProgressToFill($user, $unitId, $sections, $isTestUnit);
        }
    }

    private function isFirstUnitLevel($unit = 0) {
        return in_array($unit, $this->firstUnitsLevel);
    }

    private function getUserCourseInfo($userId = 0) {
        $user = array('id' => $userId, 'course' => 0, 'language' => null,
          'progressTable' => 'followup4', 'progressHtmlTable' => 'followup_html4');

        $query = "SELECT ct.`courseId`
        FROM aba_b2c.`course_test` ct
        JOIN aba_b2c.`course_test_user` ctu ON ctu.`courseId` = ct.`id`
        WHERE ctu.`userId` = $userId";

        $row = $this->db->fetch($this->db->query($query));
        if ($row) {
            $user['course'] = $row['courseId'];
        }

        $query = "SELECT u.`langEnv` AS `language`
        FROM aba_b2c.`user` u
        WHERE u.`id` = $userId";

        $row = $this->db->fetch($this->db->query($query));
        if ($row) {
            $user['language'] = $row['language'];
        }

        $query = "SELECT ud.`progressVersion`, ud.`progressVersionHtml`
        FROM aba_b2c.`user_dictionary` ud
        WHERE ud.`idUser` = $userId";

        $row = $this->db->fetch($this->db->query($query));
        if ($row) {
            $user['progressTable'] = 'followup4_'.$row['progressVersion'];
            $user['progressHtmlTable'] = 'followup_html4_'.$row['progressVersionHtml'];
        }

        return $user;
    }

    private function getPhraseTable($language, $unitId, $section) {
        if ($unitId >= $this->firstUnitsLevel['advanced'] && $language == 'fr'
          || $unitId >= $this->firstUnitsLevel['upperIntermediate'] && (in_array($language, array('ru', 'zh', 'de')))) {
            $phraseTable = 'phrase_en';
        }
        else if ($language == 'es' || $section['phrasesType'] == 'simple') {
            $phraseTable = 'phrase';
        }
        else {
            $phraseTable = 'phrase_'.$language;
        }

        return $phraseTable;
    }

    private function getUnitProgressToFill($user, $unitId, $sections = array(), $isTestUnit = false) {
        foreach ($sections as $section) {
            $sectionTypeId = $section['typeId'];

            if ($section['phrasesType'] !== 'none') {
                $phraseTable = $this->getPhraseTable($user['language'], $unitId, $section);
                $groupBy = 'p.`audio`, p.`page`';

                if ($section['name'] == 'WRITING') {
                    $groupBy = 'p.`audio`, p.`posAudio`, p.`page`';
                }

                if ($isTestUnit && $section['hasTest'])  {
                    $courseId = $user['course'];
                    $query = "SELECT s.`id` AS `sectionId`,
                    p.`audio`, p.`page`, p.`posAudio`
                    FROM aba_course.`section` s
                    JOIN aba_course.`course_test_phrase` ctp ON ctp.`idSection` = s.`id`
                    JOIN aba_course.`$phraseTable` p ON p.`id` = ctp.`idPhrase`
                    WHERE ctp.`idTest` = $courseId
                    AND s.`idUnit` = $unitId AND s.`idSectionType` = $sectionTypeId
                    AND p.`audio` <> '' AND p.`phraseType` <> 'example'
				            GROUP BY $groupBy";
                } else {
                    $query = "SELECT s.`id` AS `sectionId`,
                    p.`audio`, p.`page`, p.`posAudio`
                    FROM aba_course.`section` s
                    JOIN aba_course.`$phraseTable` p ON p.`idSection` = s.`id`
                    WHERE s.`idUnit` = $unitId AND s.`idSectionType` = $sectionTypeId
                    AND p.`audio` <> '' AND p.`phraseType` <> 'example'
				            GROUP BY $groupBy";
                }


                $progressToFill = $this->db->query($query);

                $this->fillSectionProgress($user, $unitId, $section, $progressToFill);
            }

            $this->fillResumeProgress($user, $unitId, $section);
        }
    }

    private function fillSectionProgress($user, $unitId, $section, $progressToFill) {
        // followup_html
        $tableName = $user['progressHtmlTable'];
        $userId = $user['id'];
        $sectionName = $section['name'];

        while ($progress = $this->db->fetch($progressToFill)) {
            $page = $progress['page'];
            $audio = $progress['audio'];

            if ($sectionName == 'WRITING') {
                $audio = $progress['audio'].'_'.$progress['posAudio'];
            }

            foreach ($section['actions'] as $action) {

                $query = "INSERT INTO aba_b2c.`$tableName` (`userid`, `themeid`, `section`, `page`, `audio`, `action`, `bien`, `mal`, `lastchange`, `wtext`, `difficulty`, `time_aux`, `contador`) SELECT $userId, $unitId, '$sectionName', $page, '$audio', '$action', 1, 0, NOW(), '', 0, 0, 1 FROM DUAL WHERE NOT EXISTS (SELECT f.`id` FROM aba_b2c.`$tableName` f WHERE f.`userid` = $userId AND f.`themeid` = $unitId AND f.`section` = '$sectionName' AND f.`audio` = '$audio' AND f.`page` = $page AND f.`action` = '$action');\n";

                fwrite ($this->filesOutput[count($this->filesOutput) - 1] , $query);

                $this->fileLinesCount ++;

                //$this->db->query($query);
            }
        }
    }

    private function fillResumeProgress($user, $unitId, $section) {
        // followup
        $tableName = $user['progressTable'];
        $fieldName = $section['progressField'];
        $userId = $user['id'];

        $query = "UPDATE aba_b2c.`$tableName` f SET f.`$fieldName` = 100, f.`lastchange` = NOW(), f.`all_por` = FLOOR(f.`".$this->sections[0]['progressField']."` + f.`".$this->sections[1]['progressField']."` + f.`".$this->sections[2]['progressField']."` + f.`".$this->sections[3]['progressField']."` + f.`".$this->sections[4]['progressField']."` + f.`".$this->sections[5]['progressField']."` + f.`".$this->sections[6]['progressField']."`) / 7 WHERE f.`userid` = $userId AND f.`themeid` = $unitId;\n";

        fwrite ($this->filesOutput[count($this->filesOutput) - 1] , $query);

        $this->fileLinesCount ++;

        //$this->db->query($query);

        if ($this->fileLinesCount > 500000) {

            $this->fileLinesCount = 0;

            fclose($this->filesOutput[count($this->filesOutput) - 1]);

            $filesNum = str_pad(count($this->filesOutput), 6, '0', STR_PAD_LEFT);

            $this->filesOutput[count($this->filesOutput)] = fopen("queries_$filesNum.sql", "w") or die("Unable to open file!");

        }
    }
}

if($argc < 2) {
    echo "Please, send \"ALL\" or a \"userId\" and (optional) a unit number\n\n";
    exit;
}

$userId = isset($argv[1]) && is_numeric($argv[1]) ? $argv[1] : null;
$unit = isset($argv[2]) && is_numeric($argv[2]) ? $argv[2] : null;

$fillTheProgress = new FillTheProgress($userId, $unit);
