<?php
/**
 * CmAbaUserTeacher
 * @author: mgadegaard
 * Date: 30/06/2015
 * Time: 13:48
 * © ABA English 2015
 *
 * This is the model class for table "user_teacher".
 *
 * The followings are the available columns in table 'user_teacher':
 * @property integer $userid
 * @property string $nationality
 * @property string $language
 * @property string $photo
 * @property string $mobileImage2x
 * @property string $mobileImage3x
 * @property integer $registerTeacherSource
 * @property string $lastUpdate
 */

class CmAbaUserTeacher extends CmAbaUser
{
    /**
     * Initialize default values
     * @param null $userId
     */
    public function __construct($userId = null)
    {
        parent::__construct();
        if (isset($userId)) {
            return $this->getTeacherById($userId);
        }
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_teacher';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('userid, registerTeacherSource', 'numerical', 'integerOnly'=>true),
            array('nationality', 'length', 'max'=>100),
            array('language, photo', 'length', 'max'=>200),
            array('mobileImage2x, mobileImage3x', 'length', 'max'=>512),
            array('lastUpdate', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('userid, nationality, language, photo, mobileImage2x, mobileImage3x, registerTeacherSource,
            lastUpdate', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'userid' => 'Userid',
            'nationality' => 'Nationality',
            'language' => 'Language',
            'photo' => 'Photo',
            'mobileImage2x' => 'Small (x2) image for mobile devices (used in app).',
            'mobileImage3x' => 'Small (x3) image for mobile devices (used in app).',
            'registerTeacherSource' => 'Register from ABA = 1, from extranet = 2...',
            'lastUpdate' => 'Last Update',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('userid', $this->userid);
        $criteria->compare('nationality', $this->nationality, true);
        $criteria->compare('language', $this->language, true);
        $criteria->compare('photo', $this->photo, true);
        $criteria->compare('mobileImage2x', $this->mobileImage2x, true);
        $criteria->compare('mobileImage3x', $this->mobileImage3x, true);
        $criteria->compare('registerTeacherSource', $this->registerTeacherSource);
        $criteria->compare('lastUpdate', $this->lastUpdate, true);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaUserTeacher the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @param int $inTeacherId
     *
     * @return AbaTeacher|bool
     */
    public function getTeacherById($inTeacherId = 0)
    {
        if ($inTeacherId == 0) {
            $inTeacherId = $this->id;
        }
        if (!is_numeric($inTeacherId)) {
            $inTeacherId = 0;
        }
        $sql = "SELECT ".$this->mainFields().
            ", ut.nationality, ut.language, ut.photo, ut.mobileImage2x, ut.mobileImage3x " .
            " FROM user u " .
            " LEFT JOIN " . $this->tableName() . " ut ON u.id=ut.userId " .
            " WHERE u.id='$inTeacherId' AND u.userType=" . TEACHER . ";";
        $dataReader=$this->querySQL($sql);
        if (($row = $dataReader->read())!==false) {
            foreach ($row as $fieldName => $value) {
                $this->$fieldName = $value;
            }
            return $this;
        }
        return false;
    }

    /**
     * @param string $lang
     *
     * @return $this|bool
     */
    public function getTeacherDefaultByLang($lang = 'en')
    {
        $lang = ucfirst($lang);
        $sql= "SELECT ".$this->mainFields().
            ", ut.nationality, ut.language, ut.photo, ut.mobileImage2x, ut.mobileImage3x ".
            " FROM user u ".
            " LEFT JOIN ".$this->tableName()." ut ON u.id=ut.userId ".
            " WHERE u.userType=".TEACHER." AND ut.language LIKE '%$lang%' LIMIT  1 ;";

        $dataReader = $this->querySQL($sql);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }
}
