<?php

/**
 * This is the model class for table "user_zuora".
 *
 * The followings are the available columns in table 'user_zuora':
 * @property integer $id
 * @property integer $userId
 * @property string $zuoraAccountId
 * @property string $zuoraAccountNumber
 * @property string $zuoraSubscriptionId
 * @property string $zuoraSubscriptionNumber
 * @property integer $status
 * @property string $dateAdd
 */
class CmAbaUserZuora extends AbaActiveRecord
{
    const ACCOUNT_STATUS_ACTIVE = 10;
    const ACCOUNT_STATUS_CANCELLED = 20;

    const USER_TYPE_DELETED = "DELETED";
    const USER_TYPE_FREE = "FREE";
    const USER_TYPE_PREMIUM = "PREMIUM";
    const USER_TYPE_PLUS = "PLUS";
    const USER_TYPE_TEACHER = "TEACHER";

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_zuora';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
          array('dateAdd', 'required'),
          array('userId, status', 'numerical', 'integerOnly' => true),
          array(
            'zuoraAccountId, zuoraAccountNumber, zuoraSubscriptionId, zuoraSubscriptionNumber',
            'length',
            'max' => 64
          ),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
          array(
            'id, userId, zuoraAccountId, zuoraAccountNumber, zuoraSubscriptionId, zuoraSubscriptionNumber, status, dateAdd',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'id' => 'ID',
          'userId' => 'User',
          'zuoraAccountId' => 'Zuora Account',
          'zuoraAccountNumber' => 'Zuora Account Number',
          'zuoraSubscriptionId' => 'Zuora Subscription',
          'zuoraSubscriptionNumber' => 'Zuora Subscription Number',
          'status' => 'Status',
          'dateAdd' => 'Date Add'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('userId', $this->userId);
        $criteria->compare('zuoraAccountId', $this->zuoraAccountId, true);
        $criteria->compare('zuoraAccountNumber', $this->zuoraAccountNumber, true);
        $criteria->compare('zuoraSubscriptionId', $this->zuoraSubscriptionId, true);
        $criteria->compare('zuoraSubscriptionNumber', $this->zuoraSubscriptionNumber, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('dateAdd', $this->dateAdd, true);

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaUserZuora the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * All fields available in this model
     *
     * @return string
     */
    public function mainFields()
    {
        return "uz.`id`, uz.`userId`, uz.`zuoraAccountId`, uz.`zuoraAccountNumber`, uz.`zuoraSubscriptionId`, uz.`zuoraSubscriptionNumber`, uz.`status`, uz.`dateAdd`";
    }

    /**
     * @param $sSubscriptionId
     *
     * @return $this|bool
     */
    public function getUserBySubscriptionId($sSubscriptionId)
    {

        $paramsToSQL = array(
          ":SUBSCRIPTIONID" => $sSubscriptionId,
        );

        $sSql = "
            SELECT
                " . $this->mainFields() . "
            FROM " . $this->tableName() . " uz
            WHERE
                uz.zuoraSubscriptionId = :SUBSCRIPTIONID
            ORDER BY uz.id DESC
            LIMIT 0, 1
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $sAccountId
     *
     * @return $this|bool
     */
    public function getUserByAccountId($sAccountId)
    {

        $paramsToSQL = array(
          ":ACCOUNTID" => $sAccountId,
        );

        $sSql = "
            SELECT
                " . $this->mainFields() . "
            FROM " . $this->tableName() . " uz
            WHERE
                uz.zuoraAccountId = :ACCOUNTID
            ORDER BY uz.id DESC
            LIMIT 0, 1
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param int $userType
     *
     * @return string
     */
    public static function getUserType($userType = FREE)
    {
        switch ($userType) {
//            case DELETED:
//                return self::USER_TYPE_DELETED;
//                break;
//            case PLUS:
//                return self::USER_TYPE_PLUS;
//                break;
//            case TEACHER:
//                return self::USER_TYPE_TEACHER;
//                break;
            case PREMIUM:
                return self::USER_TYPE_PREMIUM;
                break;
            default:
                return self::USER_TYPE_FREE;
                break;
        }
        return self::USER_TYPE_FREE;
    }

    /**
     * @param $userId
     *
     * @return $this|bool
     */
    public function existUserinZuora($userId)
    {
        $sql = "
            SELECT
                u.`id`,
                uz.`zuoraAccountId`, uz.`zuoraAccountNumber`, uz.`zuoraSubscriptionId`, uz.`zuoraSubscriptionNumber`,
                uz.`status`, uz.`dateAdd`, u.`name`, u.`surnames`, u.`email`, u.`userType`,
                c.`iso3` country, c.`ABAIdCurrency` currency
            FROM user AS u
            INNER JOIN country AS c on u.`countryId` = c.`id`
            LEFT JOIN user_zuora AS uz on uz.`userId` = u.`id`
            WHERE u.`id` = :ID;
        ";

        $paramsToSQL = array(":ID" => $userId);

        $dataReader = $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            return $row;
        }
        return false;
    }

    public function insertUser(
      $userId,
      $zuoraAccountId,
      $zuoraAccountNumber,
      $zuoraSubscriptionId,
      $zuoraSubscriptionNumber
    ) {
        $this->beginTrans();

        $sql = "
            INSERT INTO " . $this->tableName() . "
            (`userId`, `zuoraAccountId`, `zuoraAccountNumber`, `zuoraSubscriptionId`, `zuoraSubscriptionNumber`, `status`)
            VALUES (:USERID, :ZUORAACCOUNTID, :ZUORAACCOUNTNUMBER, :ZUORASUBSCRIPTIONID, :ZUORASUBSCRIPTIONNUMBER, :STATUS)
        ";

        $aBrowserValues = array(
          ":USERID" => $userId,
          ":ZUORAACCOUNTID" => $zuoraAccountId,
          ":ZUORAACCOUNTNUMBER" => $zuoraAccountNumber,
          ":ZUORASUBSCRIPTIONID" => $zuoraSubscriptionId,
          ":ZUORASUBSCRIPTIONNUMBER" => $zuoraSubscriptionNumber,
          ":STATUS" => self::ACCOUNT_STATUS_ACTIVE
        );
        $this->id = $this->executeSQL( $sql, $aBrowserValues, false );

        if ($this->id > 0) {
            $this->commitTrans();
            return $this;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }


    /**
     * @return bool
     *
     * @throws CDbException
     */
    public function updateToCancelled()
    {
        $this->beginTrans();
        $sSql = "
            UPDATE " . $this->tableName() . "
            SET
                `status`='" . self::ACCOUNT_STATUS_CANCELLED . "'
            WHERE
                `id` = '" . $this->id . "'
        ; ";
        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

    /**
     * @param $idUserZuora
     * @param $userId
     *
     * @return $this|bool
     */
    public function getUserByUserZuoraId()
//    public function getUserByUserZuoraId($idUserZuora, $userId)
    {

        $paramsToSQL = array(
          ":ID" => $this->id,
          ":USERID" => $this->userId,
        );

        $sSql = "
            SELECT
                " . $this->mainFields() . "
            FROM " . $this->tableName() . " uz
            WHERE
                uz.id = :ID
                AND uz.userId = :USERID
            ORDER BY uz.id DESC
            LIMIT 0, 1
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }


}
