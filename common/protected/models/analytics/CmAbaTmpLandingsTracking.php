<?php

/**
 * CmAbaTmpLandingsTracking
 *
 * This is the model class for table "tmp_landings_tracking".
 *
 * The followings are the available columns in table 'tmp_landings_tracking':
 *
 * @property integer $tmpLandingTrackingId
 * @property string $tmpAbaIdentifier
 * @property string $tmpLandingIdentifier
 * @property string $landingData
 * @property string $dateAdd
 */
class CmAbaTmpLandingsTracking extends AbaActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tmp_landings_tracking';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaTmpLandingsTracking the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
          array('tmpLandingTrackingId, tmpAbaIdentifier, tmpLandingIdentifier, dateAdd', 'required'),

          array('tmpAbaIdentifier, tmpLandingIdentifier, landingData', 'type', 'type'=>'string', 'allowEmpty' => true),

          array('dateAdd', 'date', 'format' => 'yyyy-mm-dd hh:mm:ss'),

          array(
            'tmpLandingTrackingId, tmpAbaIdentifier, tmpLandingIdentifier, landingData, dateAdd',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " tlt.`tmpLandingTrackingId`, tlt.`tmpAbaIdentifier`, tlt.`tmpLandingIdentifier`, tlt.`landingData`, tlt.`dateAdd` ";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @return $this|bool
     */
    public function insertUserExperimentVariation()
    {
        $this->beginTrans();
        $successTrans = true;

        $sSql = "
          INSERT INTO " . $this->tableName() . " ( `tmpAbaIdentifier`, `tmpLandingIdentifier`, `landingData`, `dateAdd` )
          VALUES ( :TMPABAIDENTIFIER, :TMPLANDINGIDENTIFIER, :LANDINGLDATA, :DATEADD )
        ";

        $aParamValues = array
        (
          ':TMPABAIDENTIFIER' => (trim($this->tmpAbaIdentifier) != '' ? $this->tmpAbaIdentifier : ""),
          ':TMPLANDINGIDENTIFIER' => (trim($this->tmpLandingIdentifier) != '' ? $this->tmpLandingIdentifier : "unknown"),
          ':LANDINGLDATA' => (trim($this->landingData) != '' ? $this->landingData : ""),
          ':DATEADD' => (trim($this->dateAdd) != '' ? $this->dateAdd : HeDate::todaySQL(true)),
        );

        $this->tmpLandingTrackingId = $this->executeSQL($sSql, $aParamValues);
        if ($this->tmpLandingTrackingId <= 0) {
            $successTrans = false;
        }

        if ($successTrans) {
            $this->commitTrans();
            return $this;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

}
