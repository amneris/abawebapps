<?php

/**
 * CmAbaTmpLandingsTracking
 *
 * This is the model class for table "tmp_landings_fields_tracking".
 *
 * The followings are the available columns in table 'tmp_landings_fields_tracking':
 *
 * @property integer $tmpLandingFieldsTrackingId
 * @property integer $tmpLandingTrackingId
 * @property string $fieldKey
 * @property string $fieldValue
 * @property string $dateAdd
 */
class CmAbaTmpLandingsFieldsTracking extends AbaActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tmp_landings_fields_tracking';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaTmpLandingsTracking the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
          array('tmpLandingFieldsTrackingId, tmpLandingTrackingId, fieldKey, fieldValue, dateAdd', 'required'),

          array('fieldKey, fieldValue, dateAdd', 'type', 'type'=>'string', 'allowEmpty' => true),

          array('dateAdd', 'date', 'format' => 'yyyy-mm-dd hh:mm:ss'),

          array(
            'tmpLandingFieldsTrackingId, tmpLandingTrackingId, fieldKey, fieldValue, dateAdd',
            'safe',
            'on' => 'search'
          ),
        );
    }


    /**
     * @return string
     */
    public function mainFields()
    {
        return " tlft.`tmpLandingFieldsTrackingId`, tlft.`tmpLandingTrackingId`, tlft.`fieldKey`, tlft.`fieldValue`, tlft.`dateAdd` ";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @return $this|bool
     */
    public function insertUserExperimentVariation()
    {
        $this->beginTrans();
        $successTrans = true;

        $sSql = "
          INSERT INTO " . $this->tableName() . " (`tmpLandingTrackingId`, `fieldKey`, `fieldValue`, `dateAdd` )
          VALUES ( :TMPLANDINGTRACKINGID, :FIELDKEY, :FIELDVALUE, :DATEADD )
        ";

        $aParamValues = array
        (
          ':TMPLANDINGTRACKINGID' => (is_numeric($this->tmpLandingTrackingId) ? $this->tmpLandingTrackingId : 0),
          ':FIELDKEY' => (trim($this->fieldKey) != '' ? $this->fieldKey : ""),
          ':FIELDVALUE' => (trim($this->fieldValue) != '' ? $this->fieldValue : ""),
          ':DATEADD' => (trim($this->dateAdd) != '' ? $this->dateAdd : HeDate::todaySQL(true)),
        );

        $this->tmpLandingFieldsTrackingId = $this->executeSQL($sSql, $aParamValues);
        if ($this->tmpLandingFieldsTrackingId <= 0) {
            $successTrans = false;
        }

        if ($successTrans) {
            $this->commitTrans();
            return $this;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

}
