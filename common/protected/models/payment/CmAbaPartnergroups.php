<?php

/**
 * CmAbaPartnergroups
 *
 * This is the model class for table "aba_partners_groups".
 *
 * The followings are the available columns in table 'aba_partners_groups':
 * @property integer $idGroup
 * @property string $nameGroup
 * @property string $dateAdd
 */
class CmAbaPartnergroups extends AbaActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'aba_partners_groups';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaPartnergroups the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
          array('nameGroup', 'required'),
          array(
            'idGroup',
            'numerical',
            'integerOnly' => true
          ),
          array('nameGroup', 'type', 'type' => 'string', 'allowEmpty' => true),
          array('dateAdd', 'date', 'format' => 'yyyy-mm-dd hh:mm:ss', 'allowEmpty' => true),
//          array(
//            'nameGroup',
//            'match',
//            'pattern' => '/^[A-Za-z0-9_]+$/u',
//            'message' => 'Group name can contain only alphanumeric characters.'
//          ),
            // @todo Please remove those attributes that should not be searched.
          array(
            'idGroup, nameGroup, dateAdd',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " pgx.`idGroup`, pgx.`nameGroup`, pgx.`dateAdd` ";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @param $partnergroupId
     *
     * @return $this|bool
     */
    public function getPartnergroupById($partnergroupId)
    {
        $paramsToSQL = array(":PARTNERGROUPID" => $partnergroupId);

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " pgx
            WHERE pgx.idGroup = :PARTNERGROUPID
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $partnergroupName
     *
     * @return $this|bool
     */
    public function getPartnergroupByNameGroup($partnergroupName)
    {
        $paramsToSQL = array(":NAMEGROUP" => $partnergroupName);

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " pgx
            WHERE pgx.nameGroup = :NAMEGROUP
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return array|bool
     */
    public function getAllPartnerGroups()
    {
        $sSql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " AS pgx
            order by (idGroup = 1) desc, nameGroup
        ";

        $paramsToSQL = array();
        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (!$dataReader) {
            return false;
        }

        $aProds = array();
        while (($row = $dataReader->read()) !== false) {
            $aProds[] = array(
              "idGroup" => $row['idGroup'],
              "nameGroup" => $row['nameGroup'],
            );
        }

        return $aProds;
    }

}
