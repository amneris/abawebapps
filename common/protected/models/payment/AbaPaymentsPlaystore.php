<?php

/**
 * This is the model class for table "payments_playstore".
 *
 * The followings are the available columns in table 'payments_playstore':
 * @property string $id
 * @property string $idPayment
 * @property string $idCountry
 * @property string $userId
 * @property integer $autoRenewing
 * @property string $orderId
 * @property string $packageName
 * @property string $productId
 * @property string $purchaseTime
 * @property integer $purchaseState
 * @property string $developerPayload
 * @property string $purchaseToken
 * @property string $datePaymentPlayStore
 * @property integer $status
 * @property string $purchaseReceipt
 * @property string $startTimeMillis
 * @property string $expiryTimeMillis
 */
class AbaPaymentsPlaystore extends AbaActiveRecord
{
    /** Constructor by default, no scenarios.
     *
     */
    public function __construct() {
        parent::__construct();
    }

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payments_playstore';
	}

	/**gitr
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('packageName, purchaseToken, datePaymentPlayStore, purchaseReceipt', 'required'),
			array('autoRenewing, purchaseState, status', 'numerical', 'integerOnly'=>true),
			array('idPayment', 'length', 'max'=>15),
			array('userId','idCountry','length', 'max'=>11),
			array('orderId, packageName, productId', 'length', 'max'=>50),
			array('purchaseTime', 'length', 'max'=>20),
			array('developerPayload', 'length', 'max'=>100),
			array('startTimeMillis, expiryTimeMillis', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idPayment, userId, autoRenewing, orderId, packageName, productId, purchaseTime, purchaseState, developerPayload, purchaseToken, datePaymentPlayStore, status, purchaseReceipt, startTimeMillis, expiryTimeMillis', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idPayment' => 'Id Payment',
      'idCountry' => 'idCountry',
			'userId' => 'User',
			'autoRenewing' => 'Auto Renewing',
			'orderId' => 'Order',
			'packageName' => 'Package Name',
			'productId' => 'Product',
			'purchaseTime' => 'Purchase Time',
			'purchaseState' => 'Purchase State',
			'developerPayload' => 'Developer Payload',
			'purchaseToken' => 'Purchase Token',
			'datePaymentPlayStore' => 'Date Payment Play Store',
			'status' => 'Status',
			'purchaseReceipt' => 'Purchase Receipt',
			'startTimeMillis' => 'Start Time Millis',
			'expiryTimeMillis' => 'Expiry Time Millis',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idPayment',$this->idPayment,true);
    $criteria->compare('idCountry',$this->idCountry,true);
		$criteria->compare('userId',$this->userId,true);
		$criteria->compare('autoRenewing',$this->autoRenewing);
		$criteria->compare('orderId',$this->orderId,true);
		$criteria->compare('packageName',$this->packageName,true);
		$criteria->compare('productId',$this->productId,true);
		$criteria->compare('purchaseTime',$this->purchaseTime,true);
		$criteria->compare('purchaseState',$this->purchaseState);
		$criteria->compare('developerPayload',$this->developerPayload,true);
		$criteria->compare('purchaseToken',$this->purchaseToken,true);
		$criteria->compare('datePaymentPlayStore',$this->datePaymentPlayStore,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('purchaseReceipt',$this->purchaseReceipt,true);
		$criteria->compare('startTimeMillis',$this->startTimeMillis,true);
		$criteria->compare('expiryTimeMillis',$this->expiryTimeMillis,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AbaPaymentsPlaystore the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function insertPaymentPlayStore()
	{
		$this->beginTrans();
		$successTrans = true;
		//-----------------

		$sql = " INSERT INTO " . $this->tableName() . " (`idPayment`,`idCountry`, `userId`, `autoRenewing`, `orderId`, `packageName`, `productId`, `purchaseTime`, `purchaseState`, `developerPayload`, `purchaseToken`, `datePaymentPlayStore`, `status`, `purchaseReceipt`, `startTimeMillis`, `expiryTimeMillis`)"
				. "VALUES "
				. "(:IDPAYMENT, :IDCOUNTRY, :USERID, :AUTORENEWING, :ORDERID, :PACKAGENAME, :PRODUCTID, :PURCHASETIME, :PURCHASESTATE, :DEVELOPERPAYLOAD, :PURCHASETOKEN, :DATEPAYMENTPLAYSTORE, :STATUS, :PURCHASERECEIPT, :STARTTIMEMILLIS, :EXPIRYTIMEMILLIS);";

		$aParamValues = array
		(
        ':IDPAYMENT' =>  $this->idPayment,
        ':IDCOUNTRY' =>  $this->idCountry,
				':USERID' =>  $this->userId,
				':AUTORENEWING' =>  $this->autoRenewing,
				':ORDERID' =>  $this->orderId,
				':PACKAGENAME' =>  $this->packageName,
				':PRODUCTID' =>  $this->productId,
				':PURCHASETIME' =>  $this->purchaseTime,
				':PURCHASESTATE' =>  $this->purchaseState,
				':DEVELOPERPAYLOAD' =>  $this->developerPayload,
				':PURCHASETOKEN' =>  $this->purchaseToken,
				':DATEPAYMENTPLAYSTORE' =>  $this->datePaymentPlayStore,
				':STATUS' =>  $this->status,
				':PURCHASERECEIPT' => $this->purchaseReceipt,
				':STARTTIMEMILLIS' =>  $this->startTimeMillis,
				':EXPIRYTIMEMILLIS' => $this->expiryTimeMillis
		);

		$this->id = $this->executeSQL($sql, $aParamValues);
		if ($this->id <= 0) {
			$successTrans = false;
		}
		//---------------
		if ($successTrans) {
			$this->commitTrans();
			return $this;
		} else {
			$this->rollbackTrans();
		}
		return false;
	}

	public function getAllPaymentsInWarranty()
	{

		$responseArray = array();

		$sSql =     "SELECT `id`,`idPayment`, `packageName`, `productId`, `purchaseToken`";
		$sSql .=    "FROM " . $this->tableName() . " ";
		$sSql .=    "WHERE `startTimeMillis` BETWEEN DATE_SUB(NOW(), INTERVAL 7 DAY) AND NOW() AND `status`=0;";


		$dataReader = $this->querySQL($sSql);
		while (($row = $dataReader->read()) !== false)
    {
			$responseArray[] = $row;
		}
      if (!is_array($responseArray))
      {
			return array();
		  }
		return $responseArray;
	}
    public function getPaymentPlayStoreById( $id )
    {
        $sql = " SELECT * FROM " . $this->tableName() . " AS pio WHERE pio.`id`=:IDPAYMENT ";
        $paramsToSQL =  array(":IDPAYMENT" => $id);
        $dataReader =   $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    public function updateToCancel()
    {
        $this->beginTrans();
        $sql = 'UPDATE ' . $this->tableName() . ' SET `status`=' . PAY_PLAY_STORE_PAY_CANCEL .' WHERE `id` = ' . $this->id . ' ;';
        if ( $this->updateSQL( $sql ) > 0 ) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }
    public function updateToSuccess()
    {
        $this->beginTrans();
        $sql = 'UPDATE ' . $this->tableName() . ' SET `status`=' . PAY_PLAY_STORE_PAY_SUCCESS .' WHERE `id` = ' . $this->id . ' ;';
        if ( $this->updateSQL( $sql ) > 0 ) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }
    public function createDuplicateNextPlayStorePayment($user, $stPlayStorePayment, $moPendingPay, $newTransactionId, $responsePlayStore)
    {
        $moLastPayPending = new Payment();

        if (!$moLastPayPending->getLastPayPendingByUserId($user->id)) {

						HeLogger::sendLog(
							HeLogger::PREFIX_ERR_LOGIC_L . ' Process AppStore Receipt failed',
							HeLogger::IT_BUGS,
							HeLogger::CRITICAL,
							"Process AppStore Receipt failed trying to create duplicate next payment . " .
							"IDENTIFICATION:  UserId " . $user->id
						);

            echo "\n+++++++++++!!! NO LAST PENDING PAYMENT for USER: " . $user->id . " !!!++++++++++++++++++\n";

            return false;
        }

        $this->idPayment = $moPendingPay->id;
        $this->idCountry = $moLastPayPending->idCountry;
        $this->userId = $moLastPayPending->userId;
        $this->autoRenewing = $stPlayStorePayment->autoRenewing;
        $this->orderId = $newTransactionId;
        $this->packageName = $stPlayStorePayment->packageName;
        $this->productId = $stPlayStorePayment->productId;
        $this->purchaseTime = $stPlayStorePayment->purchaseTime;
        $this->purchaseState = $stPlayStorePayment->purchaseState;
        $this->developerPayload = $stPlayStorePayment->developerPayload;
        $this->purchaseToken = $stPlayStorePayment->purchaseToken;
        $this->datePaymentPlayStore = $moLastPayPending->dateEndTransaction;
        $this->status = $moLastPayPending->status;
        $this->purchaseReceipt = $stPlayStorePayment->purchaseReceipt;
        $this->startTimeMillis = date("Y-m-d H:i:s", ($responsePlayStore->getStartTimeMillis() / 1000));
        $this->expiryTimeMillis = date("Y-m-d H:i:s", ($responsePlayStore->getexpiryTimeMillis() / 1000));

        //$this->originalTransactionId =  $paySupplier->getOriginalTransactionId();

        if(!$this->insertPaymentPlayStore()) {

						HeLogger::sendLog(
							HeLogger::PREFIX_ERR_LOGIC_L . ' Process AppStore Receipt failed',
							HeLogger::IT_BUGS,
							HeLogger::CRITICAL,
							"Process AppStore Receipt failed trying to create duplicate next payment . " .
							"IDENTIFICATION:  UserId " . $user->id
						);

            echo "\n+++++++++++!!! NOT INSERT PENDING PAYMENT: " . $this->idPayment . " for USER: " . $user->id . "  !!!++++++++++++++++++\n";

            return false;
        }
        return true;
    }
}
