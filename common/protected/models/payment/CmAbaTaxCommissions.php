<?php

/**
 * This is the model class for table "tax_commissions".
 *
 * The followings are the available columns in table 'tax_commissions':
 * @property integer $idCommission
 * @property integer $paySuppExtId
 * @property integer $idCountry
 * @property float $taxCommissionValue
 * @property string $dateStartCommission
 * @property string $dateEndCommission
 */
class CmAbaTaxCommissions extends AbaActiveRecord
{
    /**
     * Constructor, null implementation
     */
    public function __construct($scenario = null, $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    /** Table of this model
     * @return string
     */
    public function tableName()
    {
        return 'tax_commissions';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AbaTaxCommissions the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " tr.`idCommission`, tr.`idCountry`, tr.`paySuppExtId`, tr.`taxCommissionValue`, tr.`dateStartCommission`, tr.`dateEndCommission` ";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
          array('idCountry, paySuppExtId, taxCommissionValue, dateStartCommission', 'required'),
          array('taxCommissionValue', 'type', 'type' => 'float', 'allowEmpty' => false),
          array('dateStartCommission, dateEndCommission', 'date', 'format' => 'yyyy-mm-dd hh:mm:ss'),
          array('idCountry, paySuppExtId', 'length', 'max' => 4, 'allowEmpty' => false),
          array('taxCommissionValue', 'length', 'max' => 6, 'allowEmpty' => false),
          array('dateEndCommission', 'safe'),
          array('dateEndCommission', 'type', 'type' => 'string', 'allowEmpty' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
          array(
            'idCommission, idCountry, paySuppExtId, taxCommissionValue, dateStartCommission, dateEndCommission',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @param $idCommission
     *
     * @return $this|bool
     */
    public function getTaxCommissionById($idCommission)
    {
        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " tr WHERE tr.idCommission = " . (int)$idCommission . " ";

        $dataReader = $this->querySQL($sSql);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param int $idCountry
     * @param string $sNow
     *
     * @return $this|bool
     */
    public function getTaxCommissionByCountry($idCountry = 0, $sNow = '')
    {
        if (trim($sNow) == '') {
            $sNow = HeDate::todaySQL(true);
        }

        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " tr ";
        $sSql .= " WHERE tr.idCountry = " . $idCountry . " ";
        $sSql .= " AND '" . $sNow . "' BETWEEN tr.dateStartCommission AND IFNULL(NULLIF(tr.dateEndCommission, ''), '" . $sNow . "') ";

        $dataReader = $this->querySQL($sSql);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

//    /**
//     * @param $iPrice
//     * @return float
//     */
//    public function getPriceWithoutTax($iPrice) {
//
//        if(!is_numeric($this->taxCommissionValue) || $this->taxCommissionValue == 0) {
//            return $iPrice;
//        }
//
//        return $iPrice / (($this->taxCommissionValue / 100) + 1);
//    }
//
//    /**
//     * @return int|string
//     */
//    public function gettaxCommissionValue() {
//
//        if(!is_numeric($this->taxCommissionValue)) {
//            return 0;
//        }
//
//        return $this->taxCommissionValue;
//    }

}
