<?php

/**
 * Class CmAbaPaymentsAdyenFraud
 * This is the model class for table "payments_adyen_frauds". *
 * The followings are the available columns in table 'payments_adyen_frauds':
 *
 * @property integer $idFraud
 * @property integer $userId
 * @property string $idPayment
 * @property string $amountCurrency
 * @property float $amountValue
 * @property string $eventCode
 * @property string $merchantReference
 * @property string $originalReference
 * @property string $pspReference
 * @property string $reason
 * @property string $paymentMethod
 * @property integer $status
 * @property string $dateAdd
 *
 */
class CmAbaPaymentsAdyenFraud extends AbaActiveRecord
{

    const STATUS_PENDING = 0;
    const STATUS_MANUALLY_REFUNDED = 10;
    const STATUS_INITIATED_DISPUTE = 20;
    const STATUS_SUCCESS_AFTER_DISPUTE = 30;

    /** Constructor by default, no scenarios.
     *
     * @param null $scenario
     *
     * @param null $dbConnName
     */
    public function __construct($scenario = null, $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
//		return  Yii::app()->params['dbCampus'].'.payments_adyen_frauds';
        return 'payments_adyen_frauds';
    }

    /** All fields to be returned
     * @return string
     */
    public function mainFields()
    {
        return " paf.idFraud, paf.userId, paf.idPayment, paf.amountCurrency, paf.amountValue, paf.eventCode,
        paf.merchantReference, paf.originalReference, paf.pspReference, paf.reason, paf.paymentMethod, paf.status, paf.dateAdd";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
          array('userId, idPayment, eventCode, merchantReference, originalReference', 'required'),
          array('amountValue', 'type', 'type' => 'float', 'allowEmpty' => true),
          array('dateAdd', 'date', 'format' => 'yyyy-mm-dd hh:mm:ss'),
          array('amountCurrency, status', 'length', 'max' => 4, 'allowEmpty' => true),
          array(
            'idPayment, amountCurrency, eventCode, idPayment, merchantReference, originalReference, pspReference, reason, paymentMethod',
            'type',
            'type' => 'string',
            'allowEmpty' => true
          ),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
          array(
            'idFraud, userId, idPayment, amountCurrency, amountValue, eventCode,
                    merchantReference, originalReference, pspReference, reason, paymentMethod, status, dateAdd',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     *
     * @return AbaPaymentsAdyenPendingRefunds the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @param integer $idFraud
     *
     * @return $this|bool
     */
    public function getAdyenFraudById($idFraud)
    {
        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS paf WHERE paf.`idFraud`=:IDFRAUD ";
        $paramsToSQL = array(":IDFRAUD" => $idFraud);
        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }
//
//    /**
//     * @param $idFraud
//     *
//     * @return $this|bool
//     */
//    public function getAdyenPendingRefundById($idFraud)
//    {
//        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS par WHERE paf.`idFraud`=:IDREFUND AND paf.`status`=:STATUS  ";
//        $paramsToSQL = array(":IDREFUND" => $idFraud, ":STATUS" => PAY_ADYEN_PENDING_REFUND_PENDING);
//        $dataReader = $this->querySQL($sSql, $paramsToSQL);
//        if (($row = $dataReader->read()) !== false) {
//            $this->fillDataColsToProperties($row);
//            return $this;
//        }
//        return false;
//    }
//
//    /**
//     * @param $idFraud
//     * @param bool $status
//     *
//     * @return $this|bool
//     */
//    public function getAdyenPendingRefundByPaymentIdOrigin($idFraud, $status = array())
//    {
//        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS par ";
//        $sSql .= "WHERE paf.`idPaymentOrigin`=:IDREFUNDORIGIN AND paf.`status` IN (:STATUS) ORDER BY paf.`idFraud` DESC LIMIT 0, 1  ";
//        $paramsToSQL = array(":IDREFUNDORIGIN" => $idFraud);
//
//        if (count($status) > 0) {
//            $paramsToSQL[":STATUS"] = implode(",", $status);
//        }
//
//        $dataReader = $this->querySQL($sSql, $paramsToSQL);
//        if (($row = $dataReader->read()) !== false) {
//            $this->fillDataColsToProperties($row);
//            return $this;
//        }
//        return false;
//    }
//
//    /**
//     * @param $paymentId
//     * @param $pspReference
//     * @param $merchantReference
//     *
//     * @return $this|bool
//     */
//    public function getAdyenPaymentByPaymentAndReferences($paymentId, $pspReference, $merchantReference)
//    {
//        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS par ";
//        $sSql .= " WHERE paf.`idPayment`=:PAYMENTID AND paf.`pspReference`=:PSPREFERENCES AND paf.`merchantReference`=:MERCHANTREFERENCE "; // AND pa.`status`=:STATUS
//        $paramsToSQL = array(
//          ":PAYMENTID" => $paymentId,
//          ":PSPREFERENCES" => $pspReference,
//          ":MERCHANTREFERENCE" => $merchantReference
//        );
//        $dataReader = $this->querySQL($sSql, $paramsToSQL);
//        if (($row = $dataReader->read()) !== false) {
//            $this->fillDataColsToProperties($row);
//            return $this;
//        }
//        return false;
//    }
//
//    /**
//     * @param $pspReference
//     * @param $merchantReference
//     *
//     * @return $this|bool
//     */
//    public function getAdyenPaymentByReferences($pspReference, $merchantReference)
//    {
//        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS par ";
//        $sSql .= " WHERE paf.`pspReference`=:PSPREFERENCES AND paf.`merchantReference`=:MERCHANTREFERENCE "; // AND pa.`status`=:STATUS
//        $paramsToSQL = array(":PSPREFERENCES" => $pspReference, ":MERCHANTREFERENCE" => $merchantReference);
//        $dataReader = $this->querySQL($sSql, $paramsToSQL);
//        if (($row = $dataReader->read()) !== false) {
//            $this->fillDataColsToProperties($row);
//            return $this;
//        }
//        return false;
//    }

    /**
     * @return $this|bool
     */
    public function insertFraudAdyen()
    {
        $this->beginTrans();
        $successTrans = true;

        $sSql = "
            INSERT INTO " . $this->tableName() . " ( `userId`, `idPayment`, `amountCurrency`, `amountValue`, `eventCode`,
            `merchantReference`, `originalReference`, `pspReference`,
            `reason`, `paymentMethod`, `status`, `dateAdd` )
            VALUES ( :IDUSER, :IDPAYMENT, :AMOUNTCURRENCY, :AMOUNTVALUE, :EVENTCODE,
            :MERCHANTREFERENCE, :ORIGINALREFERENCE, :PSPREFERENCES,
            :REASON, :PAYMENTMETHOD, :STATUS, :DATEADD )
        ";

        $aParamValues = array(
          ':IDUSER' => $this->userId,
          ':IDPAYMENT' => $this->idPayment,
          ':AMOUNTCURRENCY' => $this->amountCurrency,
          ':AMOUNTVALUE' => $this->amountValue,
          ':EVENTCODE' => $this->eventCode,
          ':MERCHANTREFERENCE' => $this->merchantReference,
          ':ORIGINALREFERENCE' => $this->originalReference,
          ':PSPREFERENCES' => $this->pspReference,
          ':REASON' => $this->reason,
          ':PAYMENTMETHOD' => $this->paymentMethod,
          ':STATUS' => $this->status,
          ':DATEADD' => $this->dateAdd,
        );

        $this->idFraud = $this->executeSQL($sSql, $aParamValues);

        if ($this->idFraud <= 0) {
            $successTrans = false;
        }
        //---------------
        if ($successTrans) {
            $this->commitTrans();
            return $this;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function updateToRefund()
    {
        $this->beginTrans();
        $sSql = 'UPDATE ' . $this->tableName() . ' SET `status`=' . self::STATUS_MANUALLY_REFUNDED . ' WHERE `idFraud`=' . $this->idFraud . '; ';
        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @return bool
     */
    public function updateToInitiatedDispute()
    {
        $this->beginTrans();
        $sSql = 'UPDATE ' . $this->tableName() . ' SET `status`=' . self::STATUS_INITIATED_DISPUTE . ' WHERE `idFraud`=' . $this->idFraud . '; ';
        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @return bool
     */
    public function updateToSuccessAfterDispute()
    {
        $this->beginTrans();
        $sSql = 'UPDATE ' . $this->tableName() . ' SET `status`=' . self::STATUS_SUCCESS_AFTER_DISPUTE . ' WHERE `idFraud`=' . $this->idFraud . '; ';
        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }
}
