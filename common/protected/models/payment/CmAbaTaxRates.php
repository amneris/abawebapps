<?php

/**
 * This is the model class for table "tax_rates".
 *
 * The followings are the available columns in table 'tax_rates':
 * @property integer $idTaxRate
 * @property integer $idCountry
 * @property float $taxRateValue
 * @property string $dateStartRate
 * @property string $dateEndRate
 */
class CmAbaTaxRates extends AbaActiveRecord
{
    /**
     * Constructor, null implementation
     */
    public function __construct($scenario = null, $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

    /** Table of this model
     * @return string
     */
    public function tableName()
    {
        return 'tax_rates';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AbaTaxRates the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " tr.`idTaxRate`, tr.`idCountry`, tr.`taxRateValue`, tr.`dateStartRate`, tr.`dateEndRate` ";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
          array('idCountry, taxRateValue, dateStartRate', 'required'),
          array('taxRateValue', 'type', 'type' => 'float', 'allowEmpty' => false),
          array('dateStartRate, dateEndRate', 'date', 'format' => 'yyyy-mm-dd hh:mm:ss'),
          array('idCountry', 'length', 'max' => 4, 'allowEmpty' => false),
          array('taxRateValue', 'length', 'max' => 6, 'allowEmpty' => false),
          array('dateEndRate', 'safe'),
          array('dateEndRate', 'type', 'type' => 'string', 'allowEmpty' => true),
//            array('dateEndRate', 'allowEmpty' => true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idTaxRate, idCountry, taxRateValue, dateStartRate, dateEndRate', 'safe', 'on'=>'search'),
		);
	}

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @param $idTaxRate
     *
     * @return $this|bool
     */
    public function getTaxRateById($idTaxRate)
    {
        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " tr WHERE tr.idTaxRate = " . (int)$idTaxRate . " ";

        $dataReader = $this->querySQL($sSql);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param int $idCountry
     * @return $this|bool
     */
    public function getTaxRateByCountry($idCountry = 0, $sNow = '')
    {
        if (trim($sNow) == '') {
            $sNow = HeDate::todaySQL(true);
        }

        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " tr ";
        $sSql .= " WHERE tr.idCountry = " . $idCountry . " ";
        $sSql .= " AND '" . $sNow . "' BETWEEN tr.dateStartRate AND IFNULL(NULLIF(tr.dateEndRate, ''), '" . $sNow . "') ";

        $dataReader = $this->querySQL($sSql);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $iPrice
     * @return float
     */
    public function getPriceWithoutTax($iPrice)
    {

        if (!is_numeric($this->taxRateValue) || $this->taxRateValue == 0) {
            return $iPrice;
        }

        return $iPrice / (($this->taxRateValue / 100) + 1);
    }

    /**
     * @return int|string
     */
    public function getTaxRateValue()
    {

        if (!is_numeric($this->taxRateValue)) {
            return 0;
        }

        return $this->taxRateValue;
    }

}
