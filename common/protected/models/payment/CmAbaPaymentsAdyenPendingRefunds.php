<?php

/**
 * Class CmAbaPaymentsAdyenPendingRefunds
 * This is the model class for table "payments_adyen_pending_refunds". *
 * The followings are the available columns in table 'payments_adyen_pending_refunds':
 *
 * @property string $id
 * @property string $idPayment
 * @property string $idPaymentOrigin
 * @property string $pspReference
 * @property string $paySuppOrderId
 * @property float $amountPrice
 * @property string $dateToPay
 * @property string $merchantReference
 * @property string $status
 * @property string $requestDate
 *
 */
class CmAbaPaymentsAdyenPendingRefunds extends AbaActiveRecord
{
    /** Constructor by default, no scenarios.
     *
     * @param null $scenario
     *
     * @param null $dbConnName
     */
    public function __construct($scenario = null, $dbConnName = null)
    {
        parent::__construct($scenario, $dbConnName);
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return  'payments_adyen_pending_refunds';
	}

    /** All fields to be returned
     * @return string
     */
    public function mainFields()
    {
        return " par.id, par.idPayment, par.idPaymentOrigin, par.pspReference, par.paySuppOrderId, par.amountPrice, par.dateToPay, par.merchantReference, par.status, par.requestDate ";
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array(
                'id, idPayment, idPaymentOrigin, pspReference, paySuppOrderId, amountPrice, dateToPay, merchantReference, status, requestDate',
                'safe',
                'on' => 'search'
            ),
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' =>                 'Id',
			'idPayment' =>          'PayControlCheck ID.',
			'idPaymentOrigin' =>    'Payment ID.',
			'pspReference' =>       'Adyen reference',
            'paySuppOrderId' => 'Pay. Supplier Order Id',
            'amountPrice' => 'Amount price',
            'dateToPay' => 'Payment date',
			'merchantReference' =>  'PaySuppliermerchantReference',
			'status' =>             'Status',
			'requestDate' =>        'The time the event was generated.',
		);
	}

    /**
     * @return CActiveDataProvider
     */
    public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

        $criteria->compare('id',                $this->id, true);
		$criteria->compare('idPayment',         $this->idPayment, true);
		$criteria->compare('idPaymentOrigin',   $this->idPaymentOrigin, true);
		$criteria->compare('pspReference',      $this->pspReference, true);
        $criteria->compare('paySuppOrderId', $this->paySuppOrderId, true);
        $criteria->compare('amountPrice', $this->amountPrice, true);
        $criteria->compare('dateToPay', $this->dateToPay, true);
		$criteria->compare('merchantReference', $this->merchantReference, true);
		$criteria->compare('status',            $this->status, true);
		$criteria->compare('requestDate',       $this->requestDate, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
     *
	 * @return AbaPaymentsAdyenPendingRefunds the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    /**
     * @param integer $id
     *
     * @return $this|bool
     */
    public function getAdyenPaymentById( $id )
    {
        $sSql =         " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS par WHERE par.`id`=:IDREFUND ";
        $paramsToSQL =  array(":IDREFUND" => $id);
        $dataReader =   $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $id
     *
     * @return $this|bool
     */
    public function getAdyenPendingRefundById( $id )
    {
        $sSql =         " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS par WHERE par.`id`=:IDREFUND AND par.`status`=:STATUS  ";
        $paramsToSQL =  array(":IDREFUND" => $id, ":STATUS" => PAY_ADYEN_PENDING_REFUND_PENDING);
        $dataReader =   $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $id
     * @param bool $status
     *
     * @return $this|bool
     */
    public function getAdyenPendingRefundByPaymentIdOrigin( $id, $status=array() )
    {
        $sSql =     " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS par ";
        $sSql .=    "WHERE par.`idPaymentOrigin`=:IDREFUNDORIGIN AND par.`status` IN (:STATUS) ORDER BY par.`id` DESC LIMIT 0, 1  ";
        $paramsToSQL =  array(":IDREFUNDORIGIN" => $id);

        if(count($status) > 0 ) {
            $paramsToSQL[":STATUS"] = implode(",", $status);
        }

        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $paymentId
     * @param $pspReference
     * @param $merchantReference
     *
     * @return $this|bool
     */
    public function getAdyenPaymentByPaymentAndReferences( $paymentId, $pspReference, $merchantReference )
    {
        $sSql =         " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS par ";
        $sSql .=        " WHERE par.`idPayment`=:PAYMENTID AND par.`pspReference`=:PSPREFERENCES AND par.`merchantReference`=:MERCHANTREFERENCE "; // AND pa.`status`=:STATUS
        $paramsToSQL =  array(":PAYMENTID" => $paymentId, ":PSPREFERENCES" => $pspReference, ":MERCHANTREFERENCE" => $merchantReference);
        $dataReader =   $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $pspReference
     * @param $merchantReference
     *
     * @return $this|bool
     */
    public function getAdyenPaymentByReferences( $pspReference, $merchantReference )
    {
        $sSql =         " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS par ";
        $sSql .=        " WHERE par.`pspReference`=:PSPREFERENCES AND par.`merchantReference`=:MERCHANTREFERENCE "; // AND pa.`status`=:STATUS
        $paramsToSQL =  array(":PSPREFERENCES" => $pspReference, ":MERCHANTREFERENCE" => $merchantReference);
        $dataReader =   $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return $this|bool
     */
    public function insertPaymentAdyen()
    {
        $this->beginTrans();
        $successTrans = true;

        $sSql = "
            INSERT INTO " . $this->tableName() . " ( `idPayment`, `idPaymentOrigin`, `pspReference`, `paySuppOrderId`, `amountPrice`, `dateToPay`, `merchantReference`, `status`, `requestDate` )
            VALUES ( :IDPAYMENT, :IDPAYMENTORIGIN, :PSPREFERENCES, :PAYSUPPORDERID, :AMOUNTPRICE, :DATETOPAY, :MERCHANTREFERENCE, :STATUS, :REQUESTDATE )
        ";

        $aParamValues= array(
            ':IDPAYMENT' =>         $this->idPayment,
            ':IDPAYMENTORIGIN' =>   $this->idPaymentOrigin,
            ':PSPREFERENCES' =>     $this->pspReference,
            ':PAYSUPPORDERID' => $this->paySuppOrderId,
            ':AMOUNTPRICE' => $this->amountPrice,
            ':DATETOPAY' => $this->dateToPay,
            ':MERCHANTREFERENCE' => $this->merchantReference,
            ':STATUS' =>            $this->status,
            ':REQUESTDATE' =>       $this->requestDate,
        ) ;

        $this->id = $this->executeSQL($sSql, $aParamValues);

        if ($this->id <= 0) {
            $successTrans = false;
        }
        //---------------
        if ($successTrans) {
            $this->commitTrans();
            return $this;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function updateToRefund()
    {
        $this->beginTrans();
        $sSql = 'UPDATE ' . $this->tableName() . ' SET `status`=' . PAY_ADYEN_PENDING_REFUND_REFUNDED . ' WHERE `id`=' . $this->id . '; ';
        if ( $this->updateSQL( $sSql ) > 0 ) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @return bool
     */
    public function updateToFail()
    {
        $this->beginTrans();
        $sSql = 'UPDATE ' . $this->tableName() . ' SET `status`=' . PAY_ADYEN_PENDING_REFUND_FAIL . ' WHERE `id`=' . $this->id . '; ';
        if ( $this->updateSQL( $sSql ) > 0 ) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @return bool
     */
    public function updateToSuccessNotification()
    {
        $this->beginTrans();
        $sSql = 'UPDATE ' . $this->tableName() . ' SET `status`=' . PAY_ADYEN_PENDING_REFUND_SUCCESS_NOTIFICATION . ' WHERE `id`=' . $this->id . '; ';
        if ( $this->updateSQL( $sSql ) > 0 ) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

}
