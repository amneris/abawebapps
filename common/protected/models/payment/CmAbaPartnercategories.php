<?php

/**
 * CmAbaPartnercategories
 *
 * This is the model class for table "aba_partners_categories".
 *
 * The followings are the available columns in table 'aba_partners_categories':
 * @property integer $idCategory
 * @property string $nameCategory
 * @property string $dateAdd
 */
class CmAbaPartnercategories extends AbaActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'aba_partners_categories';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaPartnercategories the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
          array('nameCategory', 'required'),
          array(
            'idCategory',
            'numerical',
            'integerOnly' => true
          ),
          array('nameCategory', 'type', 'type' => 'string', 'allowEmpty' => true),
          array('dateAdd', 'date', 'format' => 'yyyy-mm-dd hh:mm:ss', 'allowEmpty' => true),
//          array(
//            'nameCategory',
//            'match',
//            'pattern' => '/^[A-Za-z0-9_]+$/u',
//            'message' => 'Group name can contain only alphanumeric characters.'
//          ),
            // @todo Please remove those attributes that should not be searched.
          array(
            'idCategory, nameCategory, dateAdd',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " pgc.`idCategory`, pgc.`nameCategory`, pgc.`dateAdd` ";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @param $partnerCategory
     *
     * @return $this|bool
     */
    public function getPartnercategoryById($partnerCategory)
    {
        $paramsToSQL = array(":PARTNERCATEGORY" => $partnerCategory);

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " pgc
            WHERE pgc.idCategory = :PARTNERCATEGORY
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $partnercategoryName
     *
     * @return $this|bool
     */
    public function getPartnercategoryByName($partnercategoryName)
    {
        $paramsToSQL = array(":NAMECATEGORY" => $partnercategoryName);

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " pgc
            WHERE pgc.nameCategory = :NAMECATEGORY
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return array|bool
     */
    public function getAllPartnerCategories()
    {
        $sSql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " AS pgc
        ";

        $paramsToSQL = array();
        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (!$dataReader) {
            return false;
        }

        $aProds = array();
        while (($row = $dataReader->read()) !== false) {
            $aProds[] = array(
              "idCategory" => $row['idCategory'],
              "nameCategory" => $row['nameCategory'],
            );
        }

        return $aProds;
    }

}
