<?php

/**
 * This is the model class for table "payments_AppStorePricingMatrix".
 *
 * The followings are the available columns in table 'payments_AppStorePricingMatrix':
 * @property string $id
 * @property string $Tier
 * @property string $USD
 * @property string $MXN
 * @property string $EUR
 */
class CmPaymentsAppStorePricingMatrix extends AbaActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'payments_AppStorePricingMatrix';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
          array('Tier, USD, MXN, EUR', 'length', 'max' => 10),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
          array('id, Tier, USD, MXN, EUR', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'id' => 'ID',
          'Tier' => 'Tier',
          'USD' => 'Usd',
          'MXN' => 'Mxn',
          'EUR' => 'Eur',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('Tier', $this->Tier, true);
        $criteria->compare('USD', $this->USD, true);
        $criteria->compare('MXN', $this->MXN, true);
        $criteria->compare('EUR', $this->EUR, true);

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PaymentsAppStorePricingMatrix the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getTierbyCurrency( $typeCurrency, $price )
    {
        $sql = "SELECT * FROM ".$this->tableName()." WHERE ".$typeCurrency." <= :PRICE  ORDER BY ".$typeCurrency." DESC LIMIT 1;";
        $paramsToSQL = array(":PRICE" => $price );
        $dataReader=$this->querySQL($sql, $paramsToSQL);
        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        else
        {
            $sql = "SELECT * FROM ".$this->tableName()." WHERE ".$typeCurrency." > :PRICE  ORDER BY ".$typeCurrency." ASC LIMIT 1;";
            $dataReader=$this->querySQL($sql, $paramsToSQL);
            if(($row = $dataReader->read()) !== false)
            {
                $this->fillDataColsToProperties($row);
                return $this;
            }
        }
        return false;

    }
}
