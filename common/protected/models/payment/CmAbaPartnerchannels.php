<?php

/**
 * CmAbaPartnerchannels
 *
 * This is the model class for table "aba_partners_channels".
 *
 * The followings are the available columns in table 'aba_partners_channels':
 * @property integer $idChannel
 * @property string $nameChannel
 * @property string $dateAdd
 */
class CmAbaPartnerchannels extends AbaActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'aba_partners_channels';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaPartnerchannels the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
          array('nameChannel', 'required'),
          array(
            'idChannel',
            'numerical',
            'integerOnly' => true
          ),
          array('nameChannel', 'type', 'type' => 'string', 'allowEmpty' => true),
          array('dateAdd', 'date', 'format' => 'yyyy-mm-dd hh:mm:ss', 'allowEmpty' => true),
//          array(
//            'nameChannel',
//            'match',
//            'pattern' => '/^[A-Za-z0-9_]+$/u',
//            'message' => 'Group name can contain only alphanumeric characters.'
//          ),
            // @todo Please remove those attributes that should not be searched.
          array(
            'idChannel, nameChannel, dateAdd',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " pgch.`idChannel`, pgch.`nameChannel`, pgch.`dateAdd` ";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @param $partnerChannel
     *
     * @return $this|bool
     */
    public function getPartnertypeById($partnerChannel)
    {
        $paramsToSQL = array(":PARTNERCHANNEL" => $partnerChannel);

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " pgch
            WHERE pgch.idChannel = :PARTNERCHANNEL
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $partnerchannelName
     *
     * @return $this|bool
     */
    public function getPartnertypeByName($partnerchannelName)
    {
        $paramsToSQL = array(":NAMECHANNEL" => $partnerchannelName);

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " pgch
            WHERE pgch.nameChannel = :NAMECHANNEL
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return array|bool
     */
    public function getAllPartnerChannels()
    {
        $sSql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " AS pgch
        ";

        $paramsToSQL = array();
        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (!$dataReader) {
            return false;
        }

        $aProds = array();
        while (($row = $dataReader->read()) !== false) {
            $aProds[] = array(
              "idChannel" => $row['idChannel'],
              "nameChannel" => $row['nameChannel'],
            );
        }

        return $aProds;
    }

}
