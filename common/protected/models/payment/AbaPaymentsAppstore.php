<?php
/**
 * Class AbaPaymentsAppstore
 *
 * @property string $id
 * @property string $idPayment
 * @property string $idCountry
 * @property string $userId
 * @property string $purchaseReceipt
 * @property string $originalTransactionId
 * @property string $datePaymentAppstore
 * @property string $status
 */
class AbaPaymentsAppstore extends AbaActiveRecord
{
    /** Constructor by default, no scenarios.
     *
     */
    public function __construct() {
        parent::__construct();
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return  Yii::app()->params['dbCampus'].'.payments_appstore';
	}

    /** All fields to be returned
     * @return string
     */
    public function mainFields()
    {
        return " pio.id, pio.idPayment, pio.idCountry, pio.userId, pio.purchaseReceipt, pio.originalTransactionId, ".
        "pio.datePaymentAppstore, pio.`status` ";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id, idPayment', 'required'),
            array('id, idPayment, idCountry, userId', 'length', 'max' => 15),
            // @todo Please remove those attributes that should not be searched.
            array('id, idPayment idCountry, userId, purchaseReceipt, originalTransactionId, datePaymentAppstore, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'AutoId, PK',
            'idPayment' => 'FK to payments control check and to payments too.',
            'idCountry' => 'FK to countries.',
            'userId' => 'FK to user.',
            'purchaseReceipt' => 'Json app store purchase summary.',
            'originalTransactionId' => 'Origin transaction identifier',
            'datePaymentAppstore' => 'Date creation.',
            'status' => 'Status.',
            );
    }

    /**
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('id', $this->id, true);
        $criteria->compare('idPayment', $this->idPayment, true);
        $criteria->compare('idCountry', $this->idCountry, true);
        $criteria->compare('userId', $this->userId, true);
        $criteria->compare('purchaseReceipt', $this->purchaseReceipt, true);
        $criteria->compare('datePaymentAppstore', $this->datePaymentAppstore, true);
        $criteria->compare('originalTransactionId', $this->originalTransactionId, true);
        $criteria->compare('status', $this->status, true);
        return new CActiveDataProvider($this, array('criteria'=>$criteria,));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AbaPaymentsAppstore the static model class
     */
    public static function model($className = __CLASS_)
    {
        return parent::model($className);
    }

    /**
     * @param $id
     *
     * @return $this|bool
     */
    public function getPaymentAppstoreById($id)
    {
        $sql = " SELECT " . $this->mainFields() .
            " FROM " . $this->tableName() . " AS pio WHERE pio.`id`=:IDPAYMENTIOS ";
        $paramsToSQL =  array(":IDPAYMENTIOS" => $id);
        $dataReader =   $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $idPayment
     *
     * @return $this|bool
     */
    public function getPaymentAppstoreByPaymentId($idPayment)
    {
        $sql =          " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS pio WHERE pio.`idPayment`=:IDPAYMENT ";
        $paramsToSQL =  array(":IDPAYMENT" => $idPayment);
        $dataReader =   $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $originalTransactionId
     * @param $userId
     *
     * @return bool
     */
    public function checkPaymentAppstoreByOriginalTransactionId( $originalTransactionId, $userId )
    {
        $sSql =  " SELECT " . $this->mainFields() . " ";
        $sSql .= " FROM " . $this->tableName() . " AS pio ";
        $sSql .= " WHERE ( pio.`originalTransactionId`=:ORIGINALTRANSACTIONID ) ";
//        $sSql .= " WHERE ( pio.`originalTransactionId`=:ORIGINALTRANSACTIONID AND pio.`userId`<>:USERID ) ";
//        $sSql .= " OR ( pio.`originalTransactionId`=:ORIGINALTRANSACTIONID AND pio.`userId`=:USERID AND pio.`status`=:STATUS ) ";
        $sSql .= " LIMIT 0, 1 ";
        $paramsToSQL =  array(
            ":ORIGINALTRANSACTIONID" => $originalTransactionId,
//            ":USERID" =>                $userId,
//            ":STATUS" =>                PAY_APP_STORE_PAY_CANCEL
        );
        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            return true;
        }
        return false;
    }

    /**
     * @param $originalTransactionId
     * @param null $status
     *
     * @return array|bool
     */
    public function getPaymentsAppstoreByOriginalTransactionId( $originalTransactionId, $status=null )
    {
        $sSql =  " SELECT " . $this->mainFields() . " ";
        $sSql .= " FROM " . $this->tableName() . " AS pio ";
        $sSql .= " WHERE pio.`originalTransactionId`=:ORIGINALTRANSACTIONID ";

        $paramsToSQL =  array( ":ORIGINALTRANSACTIONID" => $originalTransactionId );

        if(is_numeric($status)) {
            $sSql .= " AND pio.`status`=:STATUS ";

            $paramsToSQL[":STATUS"] = $status;
        }
        $sSql .= " ORDER BY pio.id DESC ";

        $stAllPayments =    array();
        $dataReader =       $this->querySQL($sSql, $paramsToSQL);
        while (($row = $dataReader->read()) !== false) {
            $stAllPayments[] = $row;
        }

        if (count($stAllPayments) == 0) {
            return false;
        }
        return $stAllPayments;
    }

    /**
     * @return $this|bool
     */
    public function insertPaymentAppstore()
    {
        $this->beginTrans();
        $successTrans = true;
        //-----------------
        $sql = " INSERT INTO " . $this->tableName() . " ( `idPayment`, `idCountry`, `userId`, `purchaseReceipt`, `datePaymentAppstore`, `originalTransactionId`, `status` )
                VALUES ( :IDPAYMENT, :IDCOUNTRY, :USERID, :PURCHASERECEIPT, :DATEPAYMENT, :ORIGINALTRANSACTIONID, :STATUS ) ";
        $aParamValues = array
        (
            ':IDPAYMENT' =>             $this->idPayment,
            ':IDCOUNTRY' =>             (is_numeric($this->idCountry) ? $this->idCountry : null),
            ':USERID' =>                (is_numeric($this->userId) ? $this->userId : null),
            ':PURCHASERECEIPT' =>       (trim($this->purchaseReceipt) != '' ? $this->purchaseReceipt : ''),
            ':DATEPAYMENT' =>           HeDate::todaySQL(true),
            ':ORIGINALTRANSACTIONID' => $this->originalTransactionId,
            ':STATUS' =>                PAY_APP_STORE_PAY_PENDING
        );

        $this->id = $this->executeSQL($sql, $aParamValues);
        if ($this->id <= 0) {
            $successTrans = false;
        }
        //---------------
        if ($successTrans) {
            $this->commitTrans();
            return $this;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function updateToSuccess()
    {
        $this->beginTrans();
        $sql = 'UPDATE ' . $this->tableName() . ' SET `status`=' . PAY_APP_STORE_PAY_SUCCESS .' WHERE `id` = ' . $this->id . ' ;';
        if ( $this->updateSQL( $sql ) > 0 ) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function updateToCancel()
    {
        $this->beginTrans();
        $sql = 'UPDATE ' . $this->tableName() . ' SET `status`=' . PAY_APP_STORE_PAY_CANCEL .' WHERE `id` = ' . $this->id . ' ;';
        if ( $this->updateSQL( $sql ) > 0 ) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

    /**
     * @param AbaUser $user
     * @param $paySupplier
     *
     * @return bool
     */
    public function createDuplicateNextAppstorePayment(AbaUser $user, $paySupplier, $moPendingPay)
    {
        $moLastPayPending = new Payment();
        if (!$moLastPayPending->getLastPayPendingByUserId($user->id)) {
            HeLogger::sendLog(
                "Process AppStore Receipt failed trying to create duplicate payment .",
                HeLogger::IT_BUGS_PAYMENTS,
                HeLogger::CRITICAL,
                "Process AppStore Receipt failed trying to create duplicate next payment . " .
                "IDENTIFICATION:  UserId " . $user->id
            );
            echo "\n+++++++++++!!! NO LAST PENDING PAYMENT for USER: " . $user->id . " !!!++++++++++++++++++\n";
            return false;
        }
        $this->idPayment =              $moPendingPay->id;
        $this->idCountry =              $moLastPayPending->idCountry;
        $this->userId =                 $moLastPayPending->userId;
        $this->purchaseReceipt =        $paySupplier->getJsonResponse();
        $this->originalTransactionId =  $paySupplier->getOriginalTransactionId();
        $this->datePaymentAppstore =    $moLastPayPending->dateEndTransaction;
        if(!$this->insertPaymentAppstore()) {
            HeLogger::sendLog(
                "Process AppStore Receipt failed trying to create duplicate payment .",
                HeLogger::IT_BUGS_PAYMENTS,
                HeLogger::CRITICAL,
                "Process AppStore Receipt failed trying to create duplicate next payment . " .
                "IDENTIFICATION:  UserId " . $user->id
            );
            echo "\n+++++++++++!!! NOT INSERT PENDING PAYMENT: " . $this->idPayment . " for USER: " . $user->id . "  !!!++++++++++++++++++\n";
            return false;
        }
        return true;
    }
}
