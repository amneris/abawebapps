<?php

/**
 * This is the model class for table "payments_AppABAPricingMatrix".
 *
 * The followings are the available columns in table 'payments_AppABAPricingMatrix':
 * @property string $id
 * @property string $Tier
 * @property string $Duration
 * @property string $Name
 */
class CmPaymentsAppABAPricingMatrix extends AbaActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payments_AppABAPricingMatrix';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Tier', 'length', 'max'=>10),
			array('Duration', 'length', 'max'=>11),
			array('Name', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Tier, Duration, Name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Tier' => 'Tier',
			'Duration' => 'Duration',
			'Name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('Tier',$this->Tier,true);
		$criteria->compare('Duration',$this->Duration,true);
		$criteria->compare('Name',$this->Name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CmPaymentsAppABAPricingMatrix the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

		public function getTierABA( $tier, $duration )
		{
				$sql = "SELECT * FROM ".$this->tableName()." WHERE Tier <= :TIER AND Duration = :DURATION ORDER BY Tier DESC LIMIT 1;";
				$paramsToSQL = array(":TIER" => $tier, ":DURATION" => $duration );
				$dataReader=$this->querySQL($sql, $paramsToSQL);
				if(($row = $dataReader->read()) !== false) {
						$this->fillDataColsToProperties($row);
						return $this;
				}
        else
        {
            $sql = "SELECT * FROM ".$this->tableName()." WHERE Tier > :TIER AND Duration = :DURATION ORDER BY Tier ASC LIMIT 1;";
            $dataReader=$this->querySQL($sql, $paramsToSQL);
            if(($row = $dataReader->read()) !== false) {
                $this->fillDataColsToProperties($row);
                return $this;
            }
        }
				return false;
		}

		public function checkTier($tier, $user, $promocode, $tierApple, $duration)
		{
				if (!isset($tier->Tier)) {

						HeLogger::sendLog(
							HeLogger::PREFIX_ERR_LOGIC_L . ', asking for a tier from Promocode',
							HeLogger::IT_BUGS,
							HeLogger::CRITICAL,
							"Hemos detectado un promocode que no tiene correspondencia con las tiers de ABA. Tier Apple: " .
							$tierApple->Tier . "Duration: " . $duration . " User: " . $user->id . " Promocode: " . $promocode
						);
				}
		}
}
