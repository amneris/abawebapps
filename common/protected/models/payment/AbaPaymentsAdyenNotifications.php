<?php

/**
 * Class AbaPaymentsAdyenNotifications
 * This is the model class for table "payments_adyen_notifications". *
 * The followings are the available columns in table 'payments_adyen_notifications':
 *
 * @property string $live
 * @property string $amountCurrency
 * @property string $amountValue
 * @property string $eventCode
 * @property string $eventDate
 * @property string $merchantAccountCode
 * @property string $merchantReference
 * @property string $originalReference
 * @property string $pspReference
 * @property string $reason
 * @property string $success
 * @property string $paymentMethod
 * @property string $operations
 * @property string $additionalData
 * @property string $notification
 * @property string $dateAdd
 *
 */
class AbaPaymentsAdyenNotifications extends AbaActiveRecord
{

    // Notifications types
    const ADYEN_NOTIFICATAION_TYPE_AUTHORISATION = 'AUTHORISATION';
    const ADYEN_NOTIFICATAION_TYPE_REPORT_AVAILABLE = 'REPORT_AVAILABLE';
    const ADYEN_NOTIFICATAION_TYPE_REFUND = 'REFUND';

    const ADYEN_NOTIFICATAION_TYPE_CANCELLATION = 'CANCELLATION';
    const ADYEN_NOTIFICATAION_TYPE_CAPTURE = 'CAPTURE';
    const ADYEN_NOTIFICATAION_TYPE_CAPTURE_FAILED = 'CAPTURE_FAILED';
    const ADYEN_NOTIFICATAION_TYPE_REFUND_FAILED = 'REFUND_FAILED';
    const ADYEN_NOTIFICATAION_TYPE_CANCEL_OR_REFUND = 'CANCEL_OR_REFUND';
    const ADYEN_NOTIFICATAION_TYPE_REQUEST_FOR_INFORMATION = 'REQUEST_FOR_INFORMATION';
    const ADYEN_NOTIFICATAION_TYPE_NOTIFICATION_OF_CHARGEBACK = 'NOTIFICATION_OF_CHARGEBACK';
    const ADYEN_NOTIFICATAION_TYPE_CHARGEBACK = 'CHARGEBACK';
    const ADYEN_NOTIFICATAION_TYPE_CHARGEBACK_REVERSED = 'CHARGEBACK_REVERSED';
    const ADYEN_NOTIFICATAION_TYPE_REFUND_WITH_DATA = 'REFUND_WITH_DATA';
    const ADYEN_NOTIFICATAION_TYPE_PAYOUT_DECLINE = 'PAYOUT_DECLINE';
    const ADYEN_NOTIFICATAION_TYPE_ORDER_OPENED = 'ORDER_OPENED';
    const ADYEN_NOTIFICATAION_TYPE_ORDER_CLOSED = 'ORDER_CLOSED';

    const ADYEN_NOTIFICATAION_TYPE_NOTIFICATION_OF_FRAUD = 'NOTIFICATION_OF_FRAUD';
    const ADYEN_NOTIFICATAION_TYPE_ADVICE_OF_DEBIT = 'ADVICE_OF_DEBIT';

    /** Constructor by default, no scenarios.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'payments_adyen_notifications';
    }

    /** All fields to be returned
     * @return string
     */
    public function mainFields()
    {
        return " pan.live, pan.amountCurrency, pan.amountValue, pan.eventCode, pan.eventDate, pan.merchantAccountCode, pan.merchantReference, pan.originalReference, pan.pspReference, pan.reason, pan.success, pan.paymentMethod, pan.operations, pan.additionalData, pan.notification, pan.dateAdd ";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
//          array('id', 'required'),
          array('amountCurrency, eventCode, eventDate, merchantAccountCode, merchantReference, originalReference, pspReference,
            reason, paymentMethod, operations, additionalData, notification',
            'type', 'type' => 'string', 'allowEmpty' => true),
          array('live, success', 'type', 'type' => 'integer', 'allowEmpty' => true),
          array('amountValue', 'type', 'type' => 'float', 'allowEmpty' => true),
          array('dateAdd', 'date', 'format' => 'yyyy-mm-dd hh:mm:ss'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
          array(
            'id, live, amountCurrency, amountValue, eventCode, eventDate, merchantAccountCode, merchantReference, originalReference, pspReference, reason, success,
               paymentMethod, operations, additionalData, notification, dateAdd',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'id' => 'Id',
          'live' => 'Test/Live',
          'amountCurrency' => 'Currency', // The amount (currency), if applicable, associated with the payment or modification.
          'amountValue' => 'Amount', // The amount (value), if applicable, associated with the payment or modification.
          'eventCode' => 'Event code', // The event type of the notification.
          'eventDate' => 'Event date', // The time the event was generated.
          'merchantAccountCode' => 'Merchant account', // The merchant Account the payment or modification was processed with.
          'merchantReference' => 'Order ID', // This is the reference you assigned to the original payment.
          'originalReference' => 'Original Reference', // If this is a notification for a modification request this will be the pspReference that was originally assigned to the authorisation, for a payment it will be blank.
          'pspReference' => 'Reference', // FK to paySuppExtUniId. The unique reference that Adyen assigned to the payment or modification.
          'reason' => 'Reason', // Text field with information depending on whether the result is successful or not.
          'success' => 'Success', // Whether or not the event succeeded (boolean true/false).
          'paymentMethod' => 'Payment method', // The payment method used, this is only populated for an AUTHORISATION. e.g. visa, mc, ideal, elv, wallie, etc.
          'operations' => 'Operations', // This field displays the modification operations supported by this payment as a list of strings, this is only populated for AUTHORISATION notifications.
          'additionalData' => 'Additional data', // Optional
          'notification' => 'Notification',
          'dateAdd' => 'Data',
        );
    }

    /**
     * @return CActiveDataProvider
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, false);
        $criteria->compare('live', $this->live, false);
        $criteria->compare('amountCurrency', $this->amountCurrency, true);
        $criteria->compare('amountValue', $this->amountValue, true);
        $criteria->compare('eventCode', $this->eventCode, true);
        $criteria->compare('eventDate', $this->eventDate, true);
        $criteria->compare('merchantAccountCode', $this->merchantAccountCode, true);
        $criteria->compare('merchantReference', $this->merchantReference, true);
        $criteria->compare('originalReference', $this->originalReference, true);
        $criteria->compare('pspReference', $this->pspReference, true);
        $criteria->compare('reason', $this->reason, true);
        $criteria->compare('success', $this->success, false);
        $criteria->compare('paymentMethod', $this->paymentMethod, true);
        $criteria->compare('operations', $this->operations, true);
        $criteria->compare('additionalData', $this->additionalData, true);
        $criteria->compare('notification', $this->additionalData, true);
        $criteria->compare('dateAdd', $this->dateAdd, true);

        $criteria->order = 'id DESC';

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
          'pagination' => array(
            'pageSize' => 100,
          ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AbaPaymentsAdyen the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @param $reference
     *
     * @return $this|bool
     */
    public function getAdyenNotificationByReference($reference)
    {
        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS pan WHERE pan.`pspReference`=:REFERENCE ";
        $paramsToSQL = array(":REFERENCE" => $reference);
        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $pspReference
     * @param $merchantReference
     *
     * @return $this|bool
     */
    public function getAdyenNotificationByReferences($pspReference, $merchantReference)
    {
        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS pan WHERE pan.`pspReference`=:REFERENCE AND pan.`merchantReference`=:MERCHANTREFERENCE ";
        $sSql .= " ORDER BY pan.id DESC LIMIT 0, 1 ";
        $paramsToSQL = array(":REFERENCE" => $pspReference, ":MERCHANTREFERENCE" => $merchantReference);
        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return $this|bool
     */
    public function insertPaymentAdyenNotification()
    {
        $this->beginTrans();
        $successTrans = true;

        $sSql = "
            INSERT INTO " . $this->tableName() . " ( `live`, `amountCurrency`, `amountValue`, `eventCode`, `eventDate`, `merchantAccountCode`, `merchantReference`, `originalReference`,
            `pspReference`, `reason`, `success`, `paymentMethod`, `operations`, `additionalData`, `notification`, `dateAdd` )
            VALUES ( :LIVE, :AMOUNTCURRENCY, :AMOUNTVALUE, :EVENTCODE, :EVENTDATE, :MERCHANTACCOUNTCODE, :MERCHANTREFERENCE, :ORIGINALREFERENCE,
            :PSPREFERENCE, :REASON, :SUCCESS, :PAYMENTMETHOD, :OPERATIONS, :ADDITIONALDATA, :NOTIFICATION, :DATEADD )
        ";

        $aParamValues = array(
          ':LIVE' => $this->live,
          ':AMOUNTCURRENCY' => $this->amountCurrency,
          ':AMOUNTVALUE' => $this->amountValue,
          ':EVENTCODE' => $this->eventCode,
          ':EVENTDATE' => $this->eventDate,
          ':MERCHANTACCOUNTCODE' => $this->merchantAccountCode,
          ':MERCHANTREFERENCE' => $this->merchantReference,
          ':ORIGINALREFERENCE' => $this->originalReference,
          ':PSPREFERENCE' => $this->pspReference,
          ':REASON' => $this->reason,
          ':SUCCESS' => $this->success,
          ':PAYMENTMETHOD' => $this->paymentMethod,
          ':OPERATIONS' => $this->operations,
          ':ADDITIONALDATA' => $this->additionalData,
          ':NOTIFICATION' => $this->notification,
          ':DATEADD' => HeDate::todaySQL(true),
        );

        $this->id = $this->executeSQL($sSql, $aParamValues);

        if ($this->id <= 0) {
            $successTrans = false;
        }
        //---------------
        if ($successTrans) {
            $this->commitTrans();
            return $this;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

    /**
     * @param $notificationRequestItem
     * @param bool $isObject
     *
     * @return $this
     */
    public function setNotificationData($notificationRequestItem, $isObject = false)
    {
        //
        // 1. Insert PaymentAdyen
        //

        $live = 0;
        $success = 0;

        if ($isObject) {

            //!ak
            if (isset($notificationRequestItem->live)) {
                if (is_string($notificationRequestItem->live) AND ($notificationRequestItem->live === "true" OR $notificationRequestItem->live === "1")) {
                    $live = 1;
                } elseif (is_numeric($notificationRequestItem->live) AND $notificationRequestItem->live === 1) {
                    $live = 1;
                } elseif ($notificationRequestItem->live === true) {
                    $live = 1;
                }
            }
            if (isset($notificationRequestItem->success)) {
                if (is_string($notificationRequestItem->success) AND ($notificationRequestItem->success === "true" OR $notificationRequestItem->success === "1")) {
                    $success = 1;
                } elseif (is_numeric($notificationRequestItem->success) AND $notificationRequestItem->success === 1) {
                    $success = 1;
                } elseif ($notificationRequestItem->success === true) {
                    $success = 1;
                }
            }

            $amountCurrency = (isset($notificationRequestItem->amount->currency) ? $notificationRequestItem->amount->currency : '');
            $amountValue = (isset($notificationRequestItem->amount->value) ? PaySupplierAdyen::getFormatAmount($notificationRequestItem->amount->value) : 0);
            $operations = (isset($notificationRequestItem->operations) ? (is_array($notificationRequestItem->operations) ? json_encode($notificationRequestItem->operations) : trim($notificationRequestItem->operations)) : '');
            $additionalData = (isset($notificationRequestItem->additionalData) ? json_encode($notificationRequestItem->additionalData) : '');
        } else {

            //!ak
            if (isset($notificationRequestItem['live'])) {
                if (is_string($notificationRequestItem['live']) AND ($notificationRequestItem['live'] === "true" OR $notificationRequestItem['live'] === "1")) {
                    $live = 1;
                } elseif (is_numeric($notificationRequestItem['live']) AND $notificationRequestItem['live'] === 1) {
                    $live = 1;
                } elseif ($notificationRequestItem['live'] === true) {
                    $live = 1;
                }
            }
            if (isset($notificationRequestItem['success'])) {
                if (is_string($notificationRequestItem['success']) AND ($notificationRequestItem['success'] === "true" OR $notificationRequestItem['success'] === "1")) {
                    $success = 1;
                } elseif (is_numeric($notificationRequestItem['success']) AND $notificationRequestItem['success'] === 1) {
                    $success = 1;
                } elseif ($notificationRequestItem['success'] === true) {
                    $success = 1;
                }
            }

            $amountCurrency = (isset($notificationRequestItem['currency']) ? $notificationRequestItem['currency'] : '');
            $amountValue = (isset($notificationRequestItem['value']) ? PaySupplierAdyen::getFormatAmount($notificationRequestItem['value']) : 0);
            $operations = (isset($notificationRequestItem['operations']) ? (is_array($notificationRequestItem['operations']) ? json_encode($notificationRequestItem['operations']) : trim($notificationRequestItem['operations'])) : '');
            $additionalData = (isset($notificationRequestItem['additionalData']) ? json_encode($notificationRequestItem['additionalData']) : '');
        }

        $this->notification = "";

        if (!$isObject) {
            $this->notification = json_encode($notificationRequestItem);
        }

        $this->live = $live;
        $this->amountCurrency = $amountCurrency;
        $this->amountValue = $amountValue;
        $this->operations = $operations;
        $this->additionalData = $additionalData;
        $this->success = $success;

        if ($isObject) {

            $this->eventCode = (isset($notificationRequestItem->eventCode) ? $notificationRequestItem->eventCode : '');
            $this->eventDate = (isset($notificationRequestItem->eventDate) ? $notificationRequestItem->eventDate : '');
            $this->merchantAccountCode = (isset($notificationRequestItem->merchantAccountCode) ? $notificationRequestItem->merchantAccountCode : '');
            $this->merchantReference = (isset($notificationRequestItem->merchantReference) ? $notificationRequestItem->merchantReference : '');
            $this->originalReference = (isset($notificationRequestItem->originalReference) ? $notificationRequestItem->originalReference : '');
            $this->pspReference = (isset($notificationRequestItem->pspReference) ? $notificationRequestItem->pspReference : '');
            $this->reason = (isset($notificationRequestItem->reason) ? $notificationRequestItem->reason : '');
            $this->paymentMethod = (isset($notificationRequestItem->paymentMethod) ? $notificationRequestItem->paymentMethod : '');
        } else {

            $this->eventCode = (isset($notificationRequestItem['eventCode']) ? $notificationRequestItem['eventCode'] : '');
            $this->eventDate = (isset($notificationRequestItem['eventDate']) ? $notificationRequestItem['eventDate'] : '');
            $this->merchantAccountCode = (isset($notificationRequestItem['merchantAccountCode']) ? $notificationRequestItem['merchantAccountCode'] : '');
            $this->merchantReference = (isset($notificationRequestItem['merchantReference']) ? $notificationRequestItem['merchantReference'] : '');
            $this->originalReference = (isset($notificationRequestItem['originalReference']) ? $notificationRequestItem['originalReference'] : '');
            $this->pspReference = (isset($notificationRequestItem['pspReference']) ? $notificationRequestItem['pspReference'] : '');
            $this->reason = (isset($notificationRequestItem['reason']) ? $notificationRequestItem['reason'] : '');
            $this->paymentMethod = (isset($notificationRequestItem['paymentMethod']) ? $notificationRequestItem['paymentMethod'] : '');
        }

        return $this;
    }

    /**
     * @param $type
     * @param $notificationRequestItem
     * @param bool $isObject
     *
     * @return bool
     */
    public function saveDefaultNotification($type, $notificationRequestItem, $isObject = false)
    {

        $this->setNotificationData($notificationRequestItem, $isObject);

        if (!$this->insertPaymentAdyenNotification()) {

            HeLogger::sendLog(HeLogger::PREFIX_ERR_LOGIC_L, HeLogger::PAYMENTS, HeLogger::CRITICAL, ": ADYEN NOTIFICATIONS Listener ABA service received " . $type . " request and , details= " . $type);
            return false;
        }
        return true;
    }

    /**
     * @param $paymentControlCheck
     * @param $paySupplier
     * @param $hppData
     *
     * @return bool
     */
    public function validateNotificationByResponse($paymentControlCheck, $paySupplier, $hppData)
    {

        //
        if (trim($this->eventCode) != self::ADYEN_NOTIFICATAION_TYPE_AUTHORISATION) {
            return false;
        }

        //
        if (trim($this->merchantReference) != trim($hppData['merchantReference'])) {
            return false;
        }
        if (trim($this->paymentMethod) != trim($hppData['paymentMethod'])) {
            return false;
        }
        if (trim($this->pspReference) != trim($hppData['pspReference'])) {
            return false;
        }

        // @TODO
        if (trim($hppData['authResult']) != 'AUTHORISED') {
            return false;
        }

        return true;
    }

    /**
     * @param $hppData
     *
     * @return bool
     */
    public function validateRefundNotification($hppData)
    {

        //
        if (trim($this->eventCode) != self::ADYEN_NOTIFICATAION_TYPE_REFUND) {
            return false;
        }

        //
        if (trim($this->merchantReference) != trim($hppData['merchantReference'])) {
            return false;
        }
        if (trim($this->pspReference) != trim($hppData['pspReference'])) {
            return false;
        }

        //
        if ($this->success != 1) {
            return false;
        }

        return true;
    }

    /**
     * @param $merchantReference
     *
     * @return bool
     */
    public function isHppNotification()
    {
//        if(mb_substr($this->merchantReference, 0, 3) == PaySupplierAdyen::HPP_MERCHANT_REFERENCE_PREFIX) {
        if (mb_substr($this->merchantReference, 0,
            mb_strlen(PaySupplierAdyen::HPP_MERCHANT_REFERENCE_PREFIX)) == PaySupplierAdyen::HPP_MERCHANT_REFERENCE_PREFIX
        ) {

            $pPaymentsAdyen = new AbaPaymentsAdyen();

            if (!$pPaymentsAdyen->getAdyenPaymentByMerchantReference($this->merchantReference, PAY_ADYEN_PENDING)) {
                return false;
            }
            return true;
        }
        return false;
    }

}

