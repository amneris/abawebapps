<?php

/**
 * Class AbaPaymentsAdyenRenewals
 * This is the model class for table "payments_adyen_hpp_renewals". *
 * The followings are the available columns in table 'payments_adyen_hpp_renewals':
 *
 * @property string $id
 * @property string $idPayment
 * @property string $merchantReference
 * @property string $status
 * @property string $requestDate
 *
 */
class AbaPaymentsAdyenRenewals extends AbaActiveRecord
{

    const STATUS_HPP_RENEWALS_PENDING = 0;
    const STATUS_HPP_RENEWALS_FAIL = 10;
    const STATUS_HPP_RENEWALS_SUCCESS = 30;

    /** Constructor by default, no scenarios.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'payments_adyen_hpp_renewals';
    }

    /** All fields to be returned
     * @return string
     */
    public function mainFields()
    {
        return " pa.id, pa.idPayment, pa.merchantReference, pa.status, pa.requestDate ";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
          array(
            'id, idPayment, merchantReference, status, requestDate',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'id' => 'Id',
          'idPayment' => 'Payment ID.',
          'merchantReference' => 'PaySuppliermerchantReference',
          'status' => 'Status',
          'requestDate' => 'The time the event was generated.',
        );
    }

    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('idPayment', $this->idPayment, true);
        $criteria->compare('merchantReference', $this->merchantReference, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('requestDate', $this->requestDate, true);

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AbaPaymentsAdyen the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @param integer $id
     *
     * @return $this|bool
     */
    public function getAdyenPaymentById($id)
    {
        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS pa WHERE pa.`id`=:IDPAYMENTADYEN ";
        $paramsToSQL = array(":IDPAYMENTADYEN" => $id);
        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $merchantReference
     *
     * @return $this|bool
     */
    public function getAdyenPaymentByMerchantReference($merchantReference, $status)
    {
        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS pa WHERE pa.`merchantReference`=:MERCHANTREFERENCE AND pa.`status`=:STATUS ORDER BY pa.id DESC LIMIT 0, 1 ";
        $paramsToSQL = array(":MERCHANTREFERENCE" => $merchantReference, ":STATUS" => $status);
        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $id
     * @param $merchantReference
     *
     * @return $this|bool
     */
    public function getAdyenPaymentByPaymentIdAndMerchantReference($paymentId, $merchantReference, $status = null)
    {
        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS pa WHERE pa.`idPayment`=:PAYMENTID AND pa.`merchantReference`=:MERCHANTREFERENCE ";
        $paramsToSQL = array(":PAYMENTID" => $paymentId, ":MERCHANTREFERENCE" => $merchantReference);

        if (is_numeric($status)) {
            $sSql .= " AND pa.`status`=:STATUS ";
            $paramsToSQL[":STATUS"] = $status;
        }

        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return $this|bool
     */
    public function insertPaymentAdyen()
    {
        $this->beginTrans();
        $successTrans = true;

        $sSql = "
            INSERT INTO " . $this->tableName() . " ( `idPayment`, `merchantReference`, `status`, `requestDate` )
            VALUES ( :IDPAYMENT, :MERCHANTREFERENCE, :STATUS, :REQUESTDATE )
        ";

        $aParamValues = array(
          ':IDPAYMENT' => $this->idPayment,
          ':MERCHANTREFERENCE' => $this->merchantReference,
          ':STATUS' => $this->status,
          ':REQUESTDATE' => $this->requestDate,
        );

        $this->id = $this->executeSQL($sSql, $aParamValues);

        if ($this->id <= 0) {
            $successTrans = false;
        }
        //---------------
        if ($successTrans) {
            $this->commitTrans();
            return $this;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function updateSuccess()
    {
        $this->beginTrans();
        $sSql = 'UPDATE ' . $this->tableName() . ' SET `status`=' . self::STATUS_HPP_RENEWALS_SUCCESS . ' WHERE `id`=' . $this->id . '; ';
        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

}
