<?php

/** * Class AbaPaymentsBoleto
 * This is the model class for table "payments_boleto". *
 * The followings are the available columns in table 'payments_boleto':
 *
 * @property string $id
 * @property string $idPayment
 * @property string $idPaySupplier
 * @property string $paySuppExtUniId
 * @property string $url
 * @property string $requestDate
 * @property string $dueDate
 * @property string $status
 * @property string $xmlOrderId
 * @property string $dateLastOpenLink
 */
class AbaPaymentsBoleto extends AbaActiveRecord
{
    /** Constructor by default, no scenarios.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return Yii::app()->params['dbCampus'] . '.payments_boleto';
    }

    /** All fields to be returned
     * @return string
     */
    public function mainFields()
    {
        return " * ";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idPayment, idPaySupplier', 'required'),
            array('idPayment', 'length', 'max' => 15),
            array('idPaySupplier', 'length', 'max' => 1),
            array('paySuppExtUniId', 'length', 'max' => 45),
            array('url', 'length', 'max' => 10240),
            array('status', 'length', 'max' => 2),
            array('xmlOrderId', 'length', 'max' => 20),
            array('dateLastOpenLink', 'length', 'max' => 20),
            array('requestDate, dueDate', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array(
                'id, idPayment, idPaySupplier, paySuppExtUniId, url, requestDate, dueDate, status, xmlOrderId, dateLastOpenLink',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'AutoId, PK',
            'idPayment' => 'FK to payments control check and to payments too.',
            'idPaySupplier' => 'FK to pay_suppliers.',
            'paySuppExtUniId' => 'External Id to payment gateway',
            'url' => 'URL to Boleto to payment gateway, PDF',
            'requestDate' => 'Date request',
            'dueDate' => 'Date the Boleto should change status to OFF',
            'status' => 'Status',
            'xmlOrderId' => 'parameter OrderId from XML response',
            'dateLastOpenLink' => 'Open from e-mail link',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('idPayment', $this->idPayment, true);
        $criteria->compare('idPaySupplier', $this->idPaySupplier, true);
        $criteria->compare('paySuppExtUniId', $this->paySuppExtUniId, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('requestDate', $this->requestDate, true);
        $criteria->compare('dueDate', $this->dueDate, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('xmlOrderId', $this->xmlOrderId, true);
        $criteria->compare('dateLastOpenLink', $this->dateLastOpenLink, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AbaPaymentsBoleto the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @param integer $id
     *
     * @return $this|bool
     */
    public function getBoletoById($id)
    {
        $sql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " WHERE `id`=:IDBOLETO ";
        $paramsToSQL = array(":IDBOLETO" => $id);
        $dataReader = $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }

        return false;
    }

    /** Searches and loads by the uniqueId of the payment gateway.
     * @param string $paySuppExtUniId
     * @return $this|bool
     */
    public function getByPaySuppExtUniId($paySuppExtUniId)
    {
        $sql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " WHERE `paySuppExtUniId`=:IDUNIID ";
        $paramsToSQL = array(":IDUNIID" => $paySuppExtUniId);
        $dataReader = $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }

        return false;
    }

    /** Inserts for the first time a Boleto, when is requested from the payment workflow.
     *
     * @param string $idPayment
     * @param integer $idPaySupplier
     * @param string $paySuppExtUniId
     * @param string $requestDate : date
     * @param string $dueDate
     * @param string $url
     * @param string $xmlOrderId
     *
     * @return bool|int|string
     */
    public function insertRequest(
        $idPayment,
        $idPaySupplier,
        $paySuppExtUniId,
        $requestDate,
        $dueDate,
        $url,
        $xmlOrderId
    ) {
        $this->beginTrans();
        $successTrans = true;
        //-----------------
        $sql = " INSERT INTO " . $this->tableName() . " ( `idPayment`, `idPaySupplier`, `paySuppExtUniId`, `requestDate`,
                                                    `status`, `dueDate`, `url`, `xmlOrderId` )
                                        VALUES ( :IDPAYMENT, :IDPAYSUPPLIER, :PAYSUPPEXTUNIID, :REQUESTDATE,
                                        :STATUS, :DUEDATE, :URL, :XMLORDERID ) ";
        $aParamValues = array(
            ':IDPAYMENT' => $idPayment,
            ':IDPAYSUPPLIER' => $idPaySupplier,
            ':PAYSUPPEXTUNIID' => $paySuppExtUniId,
            ':REQUESTDATE' => $requestDate,
            ':STATUS' => PAY_BOLETO_PENDING,
            ':DUEDATE' => $dueDate,
            ':URL' => $url,
            ':XMLORDERID' => $xmlOrderId
        );

        $this->id = $this->executeSQL($sql, $aParamValues);
        if ($this->id <= 0) {
            $successTrans = false;
        }

        //---------------
        if ($successTrans) {
            $this->commitTrans();
            $this->idPayment = $idPayment;
            $this->idPaySupplier = $idPaySupplier;
            $this->dueDate = $dueDate;
            $this->requestDate = $requestDate;
            $this->paySuppExtUniId = $paySuppExtUniId;
            $this->url = $url;
            $this->xmlOrderId = $xmlOrderId;
            $this->status = PAY_BOLETO_PENDING;
            return $this;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @return bool
     */
    public function updateOffDate()
    {
        $this->beginTrans();
        $sql = 'UPDATE ' . $this->tableName(
          ) . ' SET `status`=' . PAY_BOLETO_FAIL . ' WHERE `id` = ' . $this->id . ' ;';
        if ($this->updateSQL($sql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @return bool
     */
    public function updateSuccess()
    {
        $this->beginTrans();
        $sql = 'UPDATE ' . $this->tableName(
          ) . ' SET `status`=' . PAY_BOLETO_SUCCESS . ' WHERE `id` = ' . $this->id . ' ;';
        if ($this->updateSQL($sql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @return bool
     */
    public function updateLastOpenLink()
    {
        $this->beginTrans();
        $sql = 'UPDATE ' . $this->tableName() . ' SET `dateLastOpenLink` = NOW() WHERE `id` = ' . $this->id . ' ;';
        if ($this->updateSQL($sql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /** Returns all Boletos that still have time to be paid by the user.
     *
     * @param bool $bDateOff
     * @return array|bool
     */
    public function getAllPending($bDateOff = false)
    {
        $strDueDate = ' DATE(p.`dueDate`)>=DATE(NOW()) ';
        if ($bDateOff) {
            $strDueDate = " DATE(p.`dueDate`) < DATE('" . $bDateOff . "') ";
        }

        $sql = 'SELECT p.*, pc.userId, pc.`status`, pc.paySuppOrderId, u.email, u.userType
                    FROM ' . $this->tableName() . ' p
                      LEFT JOIN ' . Yii::app()->params['dbCampus'] . '.payments_control_check pc ON p.idPayment=pc.id
                      INNER JOIN ' . Yii::app()->params['dbCampus'] . '.`user` u ON pc.userId=u.id
                WHERE p.`status`=' . PAY_BOLETO_PENDING . ' AND ' . $strDueDate . ';';

        $paramsToSQL = array();
        $dataRows = $this->queryAllSQL($sql, $paramsToSQL);
        if (!$dataRows) {
            return array();
        } else {
            return $dataRows;
        }
    }

    /** Returns any Boleto in the last minutes.
     *
     * @param integer $userId
     * @param int $minutes
     *
     * @return $this|bool
     */
    public function anyPendingLastTime($userId, $minutes = 10)
    {
        $sql = " SELECT pb." . $this->mainFields() .
          " FROM " . $this->tableName() . " pb
              LEFT JOIN " . Yii::app()->params['dbCampus'] . ".payments_control_check pc ON pb.idPayment=pc.id
            WHERE pb.`status`=" . PAY_BOLETO_PENDING . " AND pc.userId=" . $userId .
          " AND  DATE( pb.requestDate )>(NOW() - INTERVAL " . $minutes . " MINUTE) ";
        $paramsToSQL = array();
        $dataReader = $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            return true;
        }

        return false;
    }

    /** Returns the last PENDING Boleto requested by user within 3 last days
     *
     * @param integer $userId
     *
     * @return $this|bool
     */
    public function getLastPendingByUserId($userId)
    {
        $sql = " SELECT pb." . $this->mainFields() .
          " FROM " . $this->tableName() . " pb
              LEFT JOIN " . Yii::app()->params['dbCampus'] . ".payments_control_check pc ON pb.idPayment=pc.id
            WHERE pb.`status`=" . PAY_BOLETO_PENDING . " AND pc.userId=" . $userId . " AND pb.dueDate>=DATE(NOW())
             ORDER BY pb.requestDate DESC LIMIT 1 ";

        $paramsToSQL = array();
        $dataReader = $this->querySQL($sql, $paramsToSQL);
        return $this->readRowFillDataColsThis($dataReader);
    }

    /** Searches by universal id from gateway
     *
     * @param string $paySuppExtUniId
     * @return $this|bool
     */
    public function getByPaySuppExtUnid($paySuppExtUniId)
    {
        $sql = "SELECT pb." . $this->mainFields() . " FROM " . $this->tableName() . " pb
                WHERE pb.`paySuppExtUniId` = '$paySuppExtUniId' ";
        $paramsToSQL = array();
        $dataReader = $this->querySQL($sql, $paramsToSQL);
        return $this->readRowFillDataColsThis($dataReader);
    }

    /** Searches by idPayment from gateway ( paymentsControlCheckId )
     *
     * @param string $idPayment
     *
     * @return $this|bool
     */
    public function getByIdPayment($idPayment)
    {
        $sql = "SELECT pb." . $this->mainFields() . " FROM " . $this->tableName() . " pb
                WHERE pb.`idPayment` = :IDPAYMENT ";

        $paramsToSQL = array(':IDPAYMENT' => $idPayment);
        $dataReader = $this->querySQL($sql, $paramsToSQL);
        return $this->readRowFillDataColsThis($dataReader);
    }

    /** It skips weekend days when determining the due date
     *
     * @param $dateReference
     * @return string
     */
    public function getDueDate($dateReference)
    {
        $daysThreshold = DAYS_THRESHOLD_BOLETO;
        switch (HeDate::dayOfWeek($dateReference)) {
            case 6 :
                /*Saturday*/
                $daysThreshold = $daysThreshold + 1;
                break;
            case 5 :
                /*Friday*/
                $daysThreshold = $daysThreshold + 2;
                break;
            case 4 :
                /*Thursday*/
                $daysThreshold = $daysThreshold + 1;
                break;
        }

        return HeDate::getDateAdd($dateReference, $daysThreshold, 0);
    }

}
