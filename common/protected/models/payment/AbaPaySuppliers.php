<?php

/**
 * This is the model class for table "pay_suppliers".
 *
 * The followings are the available columns in table 'pay_suppliers':
 * @property string $id
 * @property string $name
 * @property integer $invoice
 */
class AbaPaySuppliers extends AbaActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AbaPaySuppliers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return Yii::app()->params['dbCampus'].'.pay_suppliers';
	}

    /** All fields to be returned
     * @return string
     */
    public function mainFields()
    {
        return " * ";
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, name', 'required'),
			array('id', 'length', 'max'=>10),
			array('name', 'length', 'max'=>45),
			array('invoice', 'length', 'max'=>1),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, invoice', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'invoice' => 'Invoice',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('invoice',$this->invoice,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * Returns the name of the supplier: Caixa, ALLPAGO-BR, BOLETO, etc.
     * @param integer $idPaySupplier
     * @return bool|string
     */
    public function getNameByIdSupplier( $idPaySupplier )
    {
        $sql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " ps  WHERE ps.`id`=" .
            $idPaySupplier . " ;";
        $dataReader = $this->querySQL($sql);
        if ($this->readRowFillDataColsThis($dataReader)){
            return $this->name;
        }
        return false;
    }


    /** Returns the list id - Name for all supliers.
     * @return array|bool
     */
    public function returnAllSuppliers()
    {
        $sql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " ps  ;";
        $dataReader = $this->queryAllSQL( $sql );
        return $dataReader;
    }
}