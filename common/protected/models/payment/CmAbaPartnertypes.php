<?php

/**
 * CmAbaPartnertypes
 *
 * This is the model class for table "aba_partners_types".
 *
 * The followings are the available columns in table 'aba_partners_types':
 * @property integer $idType
 * @property string $nameType
 * @property string $dateAdd
 */
class CmAbaPartnertypes extends AbaActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'aba_partners_types';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaPartnertypes the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
          array('nameType', 'required'),
          array(
            'idType',
            'numerical',
            'integerOnly' => true
          ),
          array('nameType', 'type', 'type' => 'string', 'allowEmpty' => true),
          array('dateAdd', 'date', 'format' => 'yyyy-mm-dd hh:mm:ss', 'allowEmpty' => true),
//          array(
//            'nameType',
//            'match',
//            'pattern' => '/^[A-Za-z0-9_]+$/u',
//            'message' => 'Group name can contain only alphanumeric characters.'
//          ),
            // @todo Please remove those attributes that should not be searched.
          array(
            'idType, nameType, dateAdd',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " pgt.`idType`, pgt.`nameType`, pgt.`dateAdd` ";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @param $partnerType
     *
     * @return $this|bool
     */
    public function getPartnertypeById($partnerType)
    {
        $paramsToSQL = array(":PARTNERTYPE" => $partnerType);

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " pgt
            WHERE pgt.idType = :PARTNERTYPE
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $partnertypeName
     *
     * @return $this|bool
     */
    public function getPartnertypeByName($partnertypeName)
    {
        $paramsToSQL = array(":NAMETYPE" => $partnertypeName);

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " pgt
            WHERE pgt.nameType = :NAMETYPE
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return array|bool
     */
    public function getAllPartnerTypes()
    {
        $sSql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " AS pgt
        ";

        $paramsToSQL = array();
        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (!$dataReader) {
            return false;
        }

        $aProds = array();
        while (($row = $dataReader->read()) !== false) {
            $aProds[] = array(
              "idType" => $row['idType'],
              "nameType" => $row['nameType'],
            );
        }

        return $aProds;
    }

}
