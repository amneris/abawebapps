<?php
/**
 * CmPartnersList
 * @author: mgadegaard
 * Date: 27/07/2015
 * Time: 13:44
 * © ABA English 2015
 *
 * This is the model class for table "aba_partners_list".
 *
 * The followings are the available columns in table 'aba_partners_list':
 * @property string $idPartner
 * @property string $name
 * @property string $idPartnerGroup
 * @property string $nameGroup
 * @property integer idCategory
 * @property integer idBusinessArea
 * @property integer idType
 * @property integer idGroup
 * @property integer idChannel
 * @property integer idCountry
 */
class CmPartnersList extends AbaActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'aba_partners_list';
    }

    public function mainFields()
    {
        return "
            a.`idPartner`, a.`name`, a.`idPartnerGroup`, a.`nameGroup`, a.`idCategory`, 
            a.`idBusinessArea`, a.`idType`, a.`idGroup`, a.`idChannel`, a.`idCountry`
        ";

    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('idPartner, name', 'required'),
            array('idPartner, idPartnerGroup', 'length', 'max'=>3),
            array('name', 'length', 'max'=>60),
            array('nameGroup', 'length', 'max'=>45),
            array('idPartner, name, idPartnerGroup, nameGroup', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idPartner' => 'internal id',
            'name' => 'Name',
            'idPartnerGroup' => '0-Aba English, 1-Ventas Flash, 2-Social, 3-Affiliates, 4-B2b, 5-PostAffili, 6-Mobile,
             20-Others',
            'nameGroup' => 'Nameof the group, not used anywhere, just for info.',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('idPartner', $this->idPartner, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('idPartnerGroup', $this->idPartnerGroup, true);
        $criteria->compare('nameGroup', $this->nameGroup, true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmPartnersList the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /** get Alias of Partner Group for Selligent
     * @param $idPartner
     *
     * @return bool|Partners
     */
    public function getPartnerByIdPartner($idPartner)
    {
        $sql= " SELECT a.*, CASE a.`idPartnerGroup` WHEN ".PARTNER_ABA_G." THEN '".PARTNER_ABA_W.
            "' WHEN ".PARTNER_GROUPON_G." THEN '".PARTNER_GROUPON_W.
            "' WHEN ".PARTNER_AFFILIATES_G." THEN '".PARTNER_AFFILIATES_W.
            "' WHEN ".PARTNER_SOCIAL_G." THEN '".PARTNER_SOCIAL_W.
            "' WHEN ".PARTNER_B2B_G." THEN '".PARTNER_B2B_W.
            "' WHEN ".PARTNER_ABAMOBILE_G." THEN '".PARTNER_ABAMOBILE_W.
            "' WHEN ".PARTNER_POSTAFFILIATES_G." THEN '".PARTNER_POSTAFFILIATES_W.
            "' ELSE a.nameGroup END as `nameGroup`
		            FROM aba_partners_list a WHERE a.`idPartner`=:IDPARTNER ; ";
        $paramsToSQL = array(":IDPARTNER" => $idPartner);

        $dataReader=$this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }

        return false;
    }

    /** Loads all columns for main table
     * @param integer @id
     *
     * @return bool|Partners
     */
    public function getPartnerById( $id )
    {
        $sql= " SELECT ".$this->mainFields()." FROM  ".$this->tableName().
            " a WHERE a.`idPartner`=:IDPARTNER ; ";
        $paramsToSQL = array(":IDPARTNER" => $id);

        $dataReader=$this->querySQL($sql, $paramsToSQL);
        if( $this->readRowFillDataCols($dataReader) )
        {
            return $this;
        }

        return false;
    }
}
