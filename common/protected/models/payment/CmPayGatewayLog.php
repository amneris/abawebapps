<?php

/**
 * Created by PhpStorm.
 * User: mikkel
 * Date: 27/08/15
 * Time: 15:58
 */
abstract class CmPayGatewayLog extends AbaActiveRecord
{
    /* @var integer $idPaySupplier; Pay_supplier: Caixa, AllPago, Paypal, B2b, Appstore, Adyen, etc. */
    protected $idPaySupplier;

    public function __construct($idPaySupplier = null)
    {
        parent::__construct();
        $this->idPaySupplier = $idPaySupplier;
    }

    public function setXMLFieldsToProperties($aXMLFields)
    {
        foreach ($aXMLFields as $fieldName => $value) {
            $this->$fieldName = $value;
        }
        return true;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getOrderNumber()
    {
        return $this->Order_number;
    }

    public function getPaySuppExtId()
    {
        return $this->idPaySupplier;
    }

    /** Returns the unique id of the gateway supplier platform. Usually "their transaction identification"
     * Each platform returns its value in several ways.
     * @return mixed
     */
    abstract public function getUniqueId();
}
