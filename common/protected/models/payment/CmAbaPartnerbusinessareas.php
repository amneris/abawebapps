<?php

/**
 * CmAbaPartnerbusinessareas
 *
 * This is the model class for table "aba_partners_business_areas".
 *
 * The followings are the available columns in table 'aba_partners_business_areas':
 * @property integer $idBusinessArea
 * @property string $nameBusinessArea
 * @property string $dateAdd
 */
class CmAbaPartnerbusinessareas extends AbaActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'aba_partners_business_areas';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaPartnerbusinessareas the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
          array('nameBusinessArea', 'required'),
          array(
            'idBusinessArea',
            'numerical',
            'integerOnly' => true
          ),
          array('nameBusinessArea', 'type', 'type' => 'string', 'allowEmpty' => true),
          array('dateAdd', 'date', 'format' => 'yyyy-mm-dd hh:mm:ss', 'allowEmpty' => true),
//          array(
//            'nameBusinessArea',
//            'match',
//            'pattern' => '/^[A-Za-z0-9_]+$/u',
//            'message' => 'Group name can contain only alphanumeric characters.'
//          ),
            // @todo Please remove those attributes that should not be searched.
          array(
            'idBusinessArea, nameBusinessArea, dateAdd',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " pgba.`idBusinessArea`, pgba.`nameBusinessArea`, pgba.`dateAdd` ";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @param $partnerBusinessarea
     * 
     * @return $this|bool
     */
    public function getPartnercategoryById($partnerBusinessarea)
    {
        $paramsToSQL = array(":PARTNEBUSINESSAREA" => $partnerBusinessarea);

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " pgba
            WHERE pgba.idBusinessArea = :PARTNEBUSINESSAREA
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $partnerBusinessArea
     * 
     * @return $this|bool
     */
    public function getPartnerBusinessareaByName($partnerBusinessArea)
    {
        $paramsToSQL = array(":BUSINESSAREA" => $partnerBusinessArea);

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " pgba
            WHERE pgba.nameBusinessArea = :BUSINESSAREA
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return array|bool
     */
    public function getAllPartnerBusinessAreas()
    {
        $sSql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " AS pgba
        ";

        $paramsToSQL = array();
        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (!$dataReader) {
            return false;
        }

        $aProds = array();
        while (($row = $dataReader->read()) !== false) {
            $aProds[] = array(
              "idBusinessArea" => $row['idBusinessArea'],
              "nameBusinessArea" => $row['nameBusinessArea'],
            );
        }

        return $aProds;
    }

}
