<?php

/**
 * This is the model class for table "renewals_plans"
 *
 * The followings are the available columns in table 'renewals_plans':
 *
 * @property integer $idRenewal
 * @property string $idPayment
 * @property integer $userId
 * @property string $langEnv
 * @property float $amountPrice
 * @property string $currencyTrans
 * @property integer $idPeriodPay
 * @property string $dateToPay
 * @property integer $origin
 * @property integer $status
 * @property string $dateAdd
 */

class CmAbaRenewalsPlans extends AbaActiveRecord
{
    /**
     * Constructor, null implementation
     */
    public function __construct($scenario=NULL, $dbConnName=NULL) {
        parent::__construct($scenario, $dbConnName);
    }

    const RENEWALS_ORIGIN_FAMILY_PLAN =         1;
    const RENEWALS_ORIGIN_FAMILY_PLAN_UPGRADE = 2;

    const RENEWALS_STATUS_PENDING =     0;
    const RENEWALS_STATUS_NOTIFIED =    1;
    const RENEWALS_STATUS_RENEWED =     10;

    /** Table of this model
     * @return string
     */
    public function tableName() {
        return 'renewals_plans';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaRenewalsPlans the static model class
     */
   public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string
     */
    public function mainFields() {
        return " rp.`idRenewal`, rp.`idPayment`, rp.`userId`, rp.`langEnv`, rp.`amountPrice`, rp.`currencyTrans`, rp.`idPeriodPay`, rp.`dateToPay`, rp.`origin`, rp.`status`, rp.`dateAdd` ";
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idPayment, userId', 'required'),
            array('amountPrice', 'type', 'type' => 'float', 'allowEmpty' => false),
            array('dateToPay, dateAdd', 'date', 'format' => 'yyyy-mm-dd hh:mm:ss'),
			array('idRenewal, idPayment, userId, langEnv, amountPrice, currencyTrans, idPeriodPay, dateToPay, origin, status, dateAdd', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}

    /**
     * @param $userId
     *
     * @return $this|bool
     */
    public function getRenewalByUserAndPaymentId($userId, $idPayment)
    {
        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " rp WHERE rp.userId = '" . $userId . "' AND rp.idPayment = '" . $idPayment . "' ";

        $dataReader = $this->querySQL($sSql);
        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $userId
     * @param null $status
     *
     * @return $this|bool
     */
    public function getRenewalByUser($userId, $status=null)
    {
        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " rp WHERE rp.userId = '" . $userId . "'";

        if(is_numeric($status)) {
            $sSql .= " AND rp.`status` = '" . $status . "' ";
        }

        $dataReader = $this->querySQL($sSql);
        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return $this|bool
     */
    public function insertRenewal()
    {
        $this->beginTrans();
        $successTrans = true;
        //-----------------
        $sSql = " INSERT INTO " . $this->tableName() . " ( `idPayment`, `userId`, `langEnv`, `amountPrice`, `currencyTrans`, `idPeriodPay`, `dateToPay`, `origin`, `status`, `dateAdd` )
                VALUES ( :IDPAYMENT, :USERID, :LANGENV, :AMOUNTPRICE, :CURRENCYTRANS, :IDPERIODPAY, :DATETOPAY, :ORIGIN, :STATUS, :DATEADD )
                ON DUPLICATE KEY UPDATE
                    `origin`=:ORIGIN,
                    `dateAdd`=:DATEADD
                ";

        $aParamValues = array
        (
            ':IDPAYMENT' =>     $this->idPayment,
            ':USERID' =>        (is_numeric($this->userId) ? $this->userId : null),
            ':LANGENV' =>       (trim($this->langEnv) != '' ? $this->langEnv : ''),
            ':AMOUNTPRICE' =>   (is_numeric($this->amountPrice) ? $this->amountPrice : null),
            ':CURRENCYTRANS' => (trim($this->currencyTrans) != '' ? $this->currencyTrans : ''),
            ':IDPERIODPAY' =>   (is_numeric($this->idPeriodPay) ? $this->idPeriodPay : null),
            ':DATETOPAY' =>     (trim($this->dateToPay) != '' ? $this->dateToPay : ''),
            ':ORIGIN' =>        (is_numeric($this->origin) ? $this->origin : null),
            ':STATUS' =>        (is_numeric($this->status) ? $this->status : 0),
            ':DATEADD' =>       HeDate::todaySQL(true),
        );

        $this->idRenewal = $this->executeSQL($sSql, $aParamValues);
        if ($this->idRenewal <= 0) {
            $successTrans = false;
        }
        //---------------
        if ($successTrans) {
            $this->commitTrans();
            return $this;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function updateToRenewed()
    {
        $this->beginTrans();
        $sql = "UPDATE " . $this->tableName() . " SET `status`='" . self::RENEWALS_STATUS_RENEWED . "' WHERE `idRenewal`='" . $this->idRenewal . "' ; ";
        if ( $this->updateSQL( $sql ) > 0 ) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }


    /**
     * @param $stData
     */
    public function _fillDataColsToProperties($stData) {
        $this->fillDataColsToProperties($stData);
    }

    /**
     * NOW() BETWEEN STARDATE & ENDDATE
     *
     * @return bool
     */
    public static function isValidNotification() {

        $renewalsEnabled =  Yii::app()->config->get("PLAN_FAMILY_RENEWALS_ENABLED");
        $processDateStart = Yii::app()->config->get("PLAN_FAMILY_DATE_START");
        $processDateEnd =   Yii::app()->config->get("PLAN_FAMILY_DATE_END");
        $processExtraDays = Yii::app()->config->get("PLAN_FAMILY_RENEWAL_DAYS_PLUS");

        /**
         * @TODO - REVISAR !!!
         * @TODO - REVISAR !!!
         * @TODO - REVISAR !!!
         */
        if($renewalsEnabled == 0) {
            return false;
        }

        $dateNow =      HeDate::todaySQL(false);
        $renewalDate =  HeDate::getDateAdd($dateNow, $processExtraDays);

//echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
//echo " \n +++ processDateStart: " . $processDateStart . " +++";
//echo " \n +++ processDateEnd: " . $processDateEnd . " +++";
//echo " \n +++ dateNow: " . $dateNow . " +++";
//echo " \n +++ processExtraDays: " . $processExtraDays . " +++";
//echo " \n +++ renewalDate: " . $renewalDate . " +++";
//echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";

        //
        // NOW() BETWEEN STARDATE & ENDDATE
        if(HeDate::isGreaterThan($dateNow, $processDateStart, true) AND HeDate::isGreaterThan($processDateEnd, $dateNow, true)) {
            return true;
        }
        return false;
    }

    /**
     * @param AbaUser $moUser
     * @param Payment $moPayment
     *
     * @return bool
     */
    public function isValidRenewal(AbaUser $moUser, Payment $moPayment) {

        /**
         * @TODO - REVISAR !!!
         * @TODO - REVISAR !!!
         * @TODO - REVISAR !!!
         */
        $renewalsEnabled =  Yii::app()->config->get("PLAN_FAMILY_RENEWALS_ENABLED");
        if($renewalsEnabled == 0) {
            return false;
        }


        /**
         * @TODO - REVISAR !!!
         * @TODO - REVISAR !!!
         * @TODO - REVISAR !!!
         */
//        $processDateEnd =   Yii::app()->config->get("PLAN_FAMILY_DATE_END");
//        $processExtraDays = Yii::app()->config->get("PLAN_FAMILY_RENEWAL_DAYS_PLUS");
//
//        $dateNow =          HeDate::todaySQL(false);
//        $renewalDateLimit = HeDate::getDateAdd($processDateEnd, $processExtraDays);
//
//        if(HeDate::isGreaterThan($dateNow, $renewalDateLimit, false)) {
//            return false;
//        }
        /**
         * @TODO - REVISAR !!!
         * @TODO - REVISAR !!!
         * @TODO - REVISAR !!!
         */

        if( $moPayment->paySuppExtId == PAY_SUPPLIER_CAIXA OR $moPayment->paySuppExtId == PAY_SUPPLIER_ALLPAGO_BR
            OR $moPayment->paySuppExtId == PAY_SUPPLIER_ALLPAGO_MX OR $moPayment->paySuppExtId == PAY_SUPPLIER_ADYEN
            OR $moPayment->paySuppExtId == PAY_SUPPLIER_PAYPAL ) {

            if($this->getRenewalByUserAndPaymentId($moUser->id, $moPayment->id)) {

                if($this->status == self::RENEWALS_STATUS_NOTIFIED) {
                    return true;
                }
                return false;
            }
        }

        return false;
    }

}
