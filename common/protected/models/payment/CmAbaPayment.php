<?php
/**
 * CmAbaPayment
 * @author: mgadegaard
 * Date: 30/06/2015
 * Time: 16:53
 * © ABA English 2015
 * This is the model class for table "payments".
 *
 * The followings are the available columns in table 'payments':
 * @property string $id
 * @property string $idUserProdStamp
 * @property string $userId
 * @property string $idProduct
 * @property string $idCountry
 * @property string $idPeriodPay
 * @property string $idUserCreditForm
 * @property string $paySuppExtId
 * @property string $paySuppOrderId
 * @property string $status
 * @property string $dateStartTransaction
 * @property string $dateEndTransaction
 * @property string $dateToPay
 * @property string $xRateToEUR
 * @property string $xRateToUSD
 * @property string $currencyTrans
 * @property string $foreignCurrencyTrans
 * @property string $idPromoCode
 * @property string $amountOriginal
 * @property string $amountDiscount
 * @property string $amountPrice
 * @property string $idPayControlCheck
 * @property string $idPartner
 * @property string $paySuppExtProfId
 * @property string $autoId
 * @property string $lastAction
 * @property string $idPartnerPrePay
 * @property integer $isRecurring
 * @property string $paySuppExtUniId
 * @property integer $paySuppLinkStatus
 * @property integer $isPeriodPayChange
 * @property integer $cancelOrigin
 * @property string $taxRateValue
 * @property string $amountTax
 * @property string $amountPriceWithoutTax
 * @property integer $isExtend
 * @property integer $experimentVariationAttributeId
 * @property string $idSession
 */
class CmAbaPayment extends AbaActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'payments';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('idProduct',     'required'),
            array('idCountry',     'required'),
            array('idPeriodPay',   'required'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'idUserProdStamp' => 'FK to user_prodcuts_stamp.',
            'userId' => 'FK to users',
            'idProduct' => 'FK mixed to user_products_stamp',
            'idCountry' => 'FK mixed to user_products_stamp',
            'idPeriodPay' => 'FK mixed to user_products_stamp',
            'idUserCreditForm' => 'FK linked to user_credit_forms',
            'paySuppExtId' => 'Link to Services payments suppliers 1 La Caixa, 2 PayPal',
            'paySuppOrderId' => 'Id transaction of external gateway payment',
            'status' => '0-PENDING,10-FAIL, 30-SUCCESS, 40-REFUND, 50-CANCEL',
            'dateStartTransaction' => 'Time of INSERT, creation of the transaction',
            'dateEndTransaction' => 'Time of the end of execution of this Transaction',
            'dateToPay' => 'Future date for PENDING STATUS only',
            'xRateToEUR' => 'Exchange rate of  EUR cur transaction ',
            'xRateToUSD' => 'Exchange rate of  USD cur transaction ',
            'currencyTrans' => 'FK to currencies',
            'foreignCurrencyTrans' => 'FK to currencies, the currency of the country',
            'idPromoCode' => 'FK to products_promo',
            'amountOriginal' => 'Original price before discount.',
            'amountDiscount' => 'Amount discounted. NO PERCENTAGE',
            'amountPrice' => 'Total final amount.',
            'idPayControlCheck' => 'FK to payments control check',
            'idPartner' => 'FK to aba_partner_list, to know which partner was involved in the transaction.',
            'paySuppExtProfId' => 'Profile ID in the payment gateway. PayPal recurring Profile Id or AllPago pay' .
            'reference for subscriptions.',
            'autoId' => 'Auto id, just to control the insertion order and find out last one.',
            'lastAction' => 'Last Action',
            'idPartnerPrePay' => 'A stamp of field user.idpartnerCurrent at the moment of this transaction.',
            'isRecurring' => 'Informs if it is a renewal of payment.',
            'paySuppExtUniId' => 'Unique Id of gateway payment supplier. Their ID',
            'paySuppLinkStatus' => 'Reconciliation status, if it is linked to gateway. MATCHED or not MATCHED.',
            'isPeriodPayChange' => 'Previous product. Upgrade-downgrade of plan.negative downgrade, positive upgrade.',
            'cancelOrigin' => '1-User cancels,3-Expiration,4-PayPal IPN,5-Intranet employee,6-Change Product',
            'taxRateValue' => 'Tax rate value (%)',
            'amountTax' => 'Total tax amount',
            'amountPriceWithoutTax' => 'Total final amount without tax',
            'isExtend' => 'If is an extra payment from a Premium',
            'experimentVariationAttributeId' => 'Product price group ID',
            'idSession' => 'ID Session'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('id', $this->id, true);
        $criteria->compare('idUserProdStamp', $this->idUserProdStamp, true);
        $criteria->compare('userId', $this->userId, true);
        $criteria->compare('idProduct', $this->idProduct, true);
        $criteria->compare('idCountry', $this->idCountry, true);
        $criteria->compare('idPeriodPay', $this->idPeriodPay, true);
        $criteria->compare('idUserCreditForm', $this->idUserCreditForm, true);
        $criteria->compare('paySuppExtId', $this->paySuppExtId, true);
        $criteria->compare('paySuppOrderId', $this->paySuppOrderId, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('dateStartTransaction', $this->dateStartTransaction, true);
        $criteria->compare('dateEndTransaction', $this->dateEndTransaction, true);
        $criteria->compare('dateToPay', $this->dateToPay, true);
        $criteria->compare('xRateToEUR', $this->xRateToEUR, true);
        $criteria->compare('xRateToUSD', $this->xRateToUSD, true);
        $criteria->compare('currencyTrans', $this->currencyTrans, true);
        $criteria->compare('foreignCurrencyTrans', $this->foreignCurrencyTrans, true);
        $criteria->compare('idPromoCode', $this->idPromoCode, true);
        $criteria->compare('amountOriginal', $this->amountOriginal, true);
        $criteria->compare('amountDiscount', $this->amountDiscount, true);
        $criteria->compare('amountPrice', $this->amountPrice, true);
        $criteria->compare('idPayControlCheck', $this->idPayControlCheck, true);
        $criteria->compare('idPartner', $this->idPartner, true);
        $criteria->compare('paySuppExtProfId', $this->paySuppExtProfId, true);
        $criteria->compare('autoId', $this->autoId, true);
        $criteria->compare('lastAction', $this->lastAction, true);
        $criteria->compare('idPartnerPrePay', $this->idPartnerPrePay, true);
        $criteria->compare('isRecurring', $this->isRecurring);
        $criteria->compare('paySuppExtUniId', $this->paySuppExtUniId, true);
        $criteria->compare('paySuppLinkStatus', $this->paySuppLinkStatus);
        $criteria->compare('isPeriodPayChange', $this->isPeriodPayChange);
        $criteria->compare('cancelOrigin', $this->cancelOrigin);
        $criteria->compare('taxRateValue', $this->taxRateValue, true);
        $criteria->compare('amountTax', $this->amountTax, true);
        $criteria->compare('amountPriceWithoutTax', $this->amountPriceWithoutTax, true);
        $criteria->compare('isExtend', $this->isExtend);
        $criteria->compare('experimentVariationAttributeId', $this->experimentVariationAttributeId);
        $criteria->compare('idSession', $this->idSession);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaPayment the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return CmAbaPayment
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function mainFields()
    {
        return "
         p.`id`, p.`idUserProdStamp`, p.`userId`, p.`idProduct`, p.`idCountry`, p.`idPeriodPay`,
            p.`idUserCreditForm`, p.`paySuppExtId`, p.`paySuppOrderId`, p.`status`, p.`dateStartTransaction`,
            p.`dateEndTransaction`,p.`dateToPay`, p.`xRateToEUR`, p.`xRateToUSD`, p.`currencyTrans`,
            p.`foreignCurrencyTrans`,p.`idPromoCode`, p.`amountOriginal`, p.`amountDiscount`, p.`amountPrice`,
            p.`idPayControlCheck`,p.`idPartner`, p.`idPartnerPrePay`, p.`paySuppExtProfId`, p.autoId, p.`lastAction`,
            p.`isRecurring`, p.`paySuppExtUniId`, p.`paySuppLinkStatus`, p.`isPeriodPayChange`, p.`cancelOrigin`,
            p.`taxRateValue`, p.`amountTax`, p.`amountPriceWithoutTax`, p.`isExtend`, p.`experimentVariationAttributeId`,
            p.`idSession`
        ";
    }

    /** Get latest PENDING PAYMENT for this user.
     *
     * @param integer $userId
     * @return $this|bool
     */
    public function getLastPayPendingByUserId($userId)
    {
        $sql = " SELECT ".$this->mainFields()."
                    FROM ".$this->tableName()." p
                      LEFT JOIN `pay_suppliers` ps ON p.paySuppExtId=ps.id
                    WHERE p.`userId`=$userId AND p.`status` = ".PAY_PENDING."
                    ORDER BY p.dateToPay DESC, p.`dateEndTransaction` DESC,
                             p.`dateStartTransaction` DESC, p.`autoId` DESC
                    LIMIT 1; ";
        $dataReader=$this->querySQL($sql);
        if (($row = $dataReader->read())!==false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * Returns calculated columns for the last payment user has done or should have been done.
     * @param $userId
     * @param bool $transactionExecuted
     *
     * @return bool|CmAbaPayment
     */
    public function getLastPaymentByUserId($userId, $transactionExecuted = false)
    {
        $whTransDone = " ";
        if ($transactionExecuted) {
            $whTransDone = " AND p.`status` <>  ".PAY_PENDING;
        }
        $sql = " SELECT p.`idProduct`, p.idPeriodPay, p.`status`, p.`amountOriginal`, p.`amountPrice`, ps.`name` as 'supplierName',
                        p.`paySuppOrderId`, DATE_FORMAT(p.`dateToPay`,'%d-%m-%Y') as 'dateToPay', p.paySuppExtId,
                        p.id, p.idPartner, p.idPartnerPrePay, p.dateEndTransaction,
                        p.idUserCreditForm,
                        p.taxRateValue, p.amountTax, p.amountPriceWithoutTax
                    FROM ".$this->tableName()." p
                      LEFT JOIN `pay_suppliers` ps ON p.paySuppExtId=ps.id
                    WHERE p.userId=$userId
                    ".$whTransDone."
                    ORDER BY p.dateToPay DESC, p.dateEndTransaction DESC
                    LIMIT 1; ";

        $dataReader=$this->querySQL($sql);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /** Get the last payment for a certain profile.
     *
     * @param $userId
     * @param $paySuppExtId
     * @param $paySuppExtProfId
     * @param string $status
     *
     * @return $this|bool
     */
    public function getLastByPaySuppExtProfId($userId, $paySuppExtId, $paySuppExtProfId, $status = "")
    {
        $strPayStatus = " AND p.`status` = $status ";
        if ($status == "") {
            $strPayStatus = "";
        }
        if (empty($paySuppExtProfId)) {
            return false;
        }
        $sql = " SELECT ".$this->mainFields()."
                    FROM ".$this->tableName()." p
                    WHERE p.userId = $userId AND
                        p.paySuppExtId = $paySuppExtId AND
                        paySuppExtProfId = '$paySuppExtProfId' ".
            $strPayStatus."
                    ORDER BY p.dateToPay DESC LIMIT 1; ";

        $dataReader=$this->querySQL($sql);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * Used to update the dateToPay, in the scenario that an active PREMIUM user has executed another payment,
     * so that the current pending payment is due to be executed further ahead.
     *
     * @param string $sName
     * @param $dateToPay
     *
     * @return bool
     */
    public function updateDateToPayInPending($sName = 'Groupon', $dateToPay = '')
    {
        if ($dateToPay == '') {
            $dateToPay = $this->dateToPay;
        }
        $this->beginTrans();
        $sql = " UPDATE ".$this->tableName()." p SET p.`dateToPay`= :DATETOPAY,
                        p.`lastAction` = IF(`lastAction` IS NULL,
                                        CONCAT( NOW(), ' [ ', 'Extend dateToPay due to " .$sName. " purchase', ' ] ' ),
                                        CONCAT(`lastAction`, ' ; ', NOW(), ' [ ', 'Extend dateToPay due to " .$sName.
                                        " purchase', ' ] ')
                                  )
                WHERE p.`id`='".$this->id."' ; ";

        $aParamValues = array(':DATETOPAY' => $dateToPay);
        if ($this->updateSQL($sql, $aParamValues) > 0) {
            $this->dateToPay = $dateToPay;
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * Set tax value, tax amount and total amount without tax
     *
     * @return bool
     */
    public function setAmountWithoutTax($dateEnd = '')
    {
        if ($this->paySuppExtId == PAY_SUPPLIER_APP_STORE) {
            $this->taxRateValue =           0;
            $this->amountTax =              0;
            $this->amountPriceWithoutTax =  $this->amountPrice;
            return true;
        }

        $oTaxRate = new CmAbaTaxRates();
        $oTaxRate->getTaxRateByCountry($this->idCountry, $dateEnd);

        $iAmountPriceWithoutTax =       $oTaxRate->getPriceWithoutTax($this->amountPrice);
        $this->taxRateValue =           $oTaxRate->getTaxRateValue();
        $this->amountTax =              HeMixed::getRoundAmount($this->amountPrice - $iAmountPriceWithoutTax);
        $this->amountPriceWithoutTax =  HeMixed::getRoundAmount($iAmountPriceWithoutTax);
        return true;
    }

    /** Based on the original payment creates the PENDING one, so the next one.
     * @return bool|CmAbaPayment
     */
    public function createNextPayment()
    {
        $this->idPromoCode = "";
        $this->idPartner = PARTNER_ID_RECURRINGPAY;
        $this->isRecurring = 1;
        $this->paySuppExtUniId = null;
        $this->paySuppLinkStatus = null;
        $this->amountDiscount = 0.00;
        $this->amountPrice = $this->amountOriginal;
        $this->status = PAY_PENDING ;
        $this->dateStartTransaction = '0000-00-00';
        $this->dateEndTransaction = '0000-00-00';

        $prodPeriodicity = new CmProdPeriodicity();
        $prodPeriodicity->getProdPeriodById($this->idPeriodPay);
        if (!$prodPeriodicity) {
            return false;
        }
        $this->dateToPay = $prodPeriodicity->getNextDateBasedOnDate($this->dateToPay);
        $this->setAmountWithoutTax();
        if (!$this->validate()) {
            return false;
        }
        return $this;
    }

    /**
     * @return CmAbaPayment
     */
    public function createDuplicateNextPayment()
    {
        $newPay = new CmAbaPayment();
        $newPay = clone($this);
        $newPay = $newPay->createNextPayment();
        return $newPay;
    }

    /**
     * @param integer $userId; optional
     * @return bool
     */
    public function paymentProcess($userId = null)
    {
        $bInsert = $this->insertPayment();
        if ($bInsert && ($this->status == PAY_SUCCESS || $this->status == PAY_REFUND)
            && $this->validateInvoicePartner()) {
            $moInvoice = new CmAbaUserInvoices();
            if (!is_numeric($userId)) {
                $userId = Yii::app()->user->getId();
            }
            if (!$moInvoice->createPreInvoice($this, $userId)) {
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_LOGIC_L . ' Process create pre invoice failed',
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    'paymentProcess - createPreInvoice'
                );
            }
        }
        return $bInsert;
    }

    /**
     * @return bool
     */
    public function insertPayment()
    {
        $this->beginTrans();
        $successTrans = true;
        $sql = ' INSERT INTO payments
        (`id`,`idUserProdStamp`, `userId`, `idProduct`, `idCountry`, `idPeriodPay`,
        `idUserCreditForm`, `paySuppExtId`, `paySuppOrderId`, `status`, `dateStartTransaction`,
        `dateEndTransaction`,`dateToPay`, `xRateToEUR`, `xRateToUSD`, `currencyTrans`,
        `foreignCurrencyTrans`, `idPromoCode`, `amountOriginal`,  `amountDiscount`, `amountPrice`, `idPayControlCheck`,
        `idPartner`, `paySuppExtProfId`, `idPartnerPrePay`, `isRecurring`, `paySuppExtUniId`, `isPeriodPayChange`,
        `cancelOrigin`, `taxRateValue`, `amountTax`, `amountPriceWithoutTax`, `isExtend`, `idSession` )
             VALUES(:ID, :IDUSERPRODSTAMP , :USERID , :IDPRODUCT , :IDCOUNTRY , :IDPERIODPAY ,
            :IDUSERCREDITFORM , :PAYSUPPEXTID , :PAYSUPPORDER , :STATUS , :DATESTARTTRANSACTION ,
            :DATEENDTRANSACTION ,:DATETOPAY , :XRATETOEUR , :XRATETOUSD , :CURRENCYTRANS ,
            :FOREIGNCURRENCYTRANS ,:IDPROMOCODE , :AMOUNTORIGINAL , :AMOUNTDISCOUNT , :AMOUNTPRICE , :IDPAYCONTROLCHECK,
            :IDPARTNER, :PAYSUPPEXTPROFID, :IDPARTNERPREPAY, :ISRECURRING, :PAYSUPPEXTUNIID, :ISPERIODPAYCHANGE,
            :CANCELORIGIN, :TAXRATEVALUE, :AMOUNTTAX, :AMOUNTPRICEWITHOUTTAX, :ISEXTEND, :IDSESSION) ';
        $aParamValues = array(':ID'=>$this->id,':IDUSERPRODSTAMP'=> $this->idUserProdStamp,  ':USERID'=> $this->userId,
            ':IDPRODUCT'=> $this->idProduct, ':IDCOUNTRY'=> $this->idCountry,  ':IDPERIODPAY'=> $this->idPeriodPay,
            ':IDUSERCREDITFORM'=> $this->idUserCreditForm, ':PAYSUPPEXTID'=> $this->paySuppExtId,
            ':PAYSUPPORDER'=> $this->paySuppOrderId,  ':STATUS'=> $this->status,
            ':DATESTARTTRANSACTION'=> $this->dateStartTransaction, ':DATEENDTRANSACTION'=> $this->dateEndTransaction,
            ':DATETOPAY'=> $this->dateToPay,  ':XRATETOEUR'=> $this->xRateToEUR,  ':XRATETOUSD'=> $this->xRateToUSD,
            ':CURRENCYTRANS'=> $this->currencyTrans, ':FOREIGNCURRENCYTRANS'=> $this->foreignCurrencyTrans,
            ':IDPROMOCODE'=> $this->idPromoCode,  ':AMOUNTORIGINAL'=> $this->amountOriginal,
            ':AMOUNTDISCOUNT'=> $this->amountDiscount, ':AMOUNTPRICE'=> $this->amountPrice,
            ':IDPAYCONTROLCHECK'=>$this->idPayControlCheck, ':IDPARTNER'=> $this->idPartner,
            ':PAYSUPPEXTPROFID'=>$this->paySuppExtProfId, ':IDPARTNERPREPAY'=> $this->idPartnerPrePay,
            ':ISRECURRING'=> $this->isRecurring, ':PAYSUPPEXTUNIID'=> $this->paySuppExtUniId,
            ':ISPERIODPAYCHANGE'=>$this->isPeriodPayChange, ':CANCELORIGIN'=> $this->cancelOrigin,
            ':TAXRATEVALUE'=> $this->taxRateValue, ':AMOUNTTAX'=> $this->amountTax,
            ':AMOUNTPRICEWITHOUTTAX'=> $this->amountPriceWithoutTax, ':ISEXTEND'=>$this->isExtend,
            ':IDSESSION'=>$this->idSession);
        $this->autoId = $this->executeSQL($sql, $aParamValues);
        if ($this->autoId > 0) {
            $user = new CmAbaUser();
            if ($user->populateUserById($this->userId)) {
                if (!$user->logUserActivity->saveUserLogActivity(
                    $this->tableName(),
                    "New payment DONE AND INSERTED through ".$this->paySuppExtId." gateway",
                    "Payment confirmed"
                )) {
                    $successTrans= false;
                }
            }
        } else {
            $successTrans = false;
        }
        if ($successTrans) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * NO GROUPON OR B2B OR APPSTORE
     *
     * @return bool
     */
    public function validateInvoicePartner()
    {
        if ($this->paySuppExtId == PAY_SUPPLIER_GROUPON || $this->paySuppExtId == PAY_SUPPLIER_B2B) {
            return false;
        }
        return true;
    }

    /**
     * @return array
     */
    public function getPaymentInfoForSelligent()
    {
        try {
            $iPeriodMonth = HePayments::periodToMonth($this->idPeriodPay);
            $stDateStart =  explode(" ", $this->dateToPay);
            $sDateStart =   $stDateStart[0];
            $sDateEnd =     HeDate::getDateAdd($this->dateToPay, 0, $iPeriodMonth);

            $amountPrice = HeMixed::numberFormat($this->amountPrice);
            $formattedPrice = HeViews::formatAmountInCurrency($amountPrice, $this->currencyTrans);

            return array(
                "MONTH_PERIOD" =>   strval($iPeriodMonth),
                "PAY_DATE_START" => strval($sDateStart . " 00:00:00"),
                "PAY_DATE_END" =>   strval($sDateEnd . " 00:00:00"),
                "AMOUNT_PRICE" =>   $amountPrice,
                "CURRENCY" =>       strval($this->currencyTrans),
                "PRICE_CURR" =>     strval($formattedPrice),
                "TRANSID" =>        strval($this->id),
            );
        } catch (Exception $e) { }

        return array(
            "MONTH_PERIOD" =>   "",
            "PAY_DATE_START" => "",
            "PAY_DATE_END" =>   "",
            "AMOUNT_PRICE" =>   "",
            "CURRENCY" =>       "",
            "PRICE_CURR" =>        "",
            "TRANSID" =>        "",
        );
    }

    /**
     * Returns last payment whether PENDING or CANCEL from the last subscription.
     *
     * @param integer $userId
     *
     * @return $this|bool
     */
    public function getLastPayLastSubscription($userId)
    {
        if (!$this->getLastPayPendingByUserId($userId)) {
            if (!$this->getLastCancelPayment($userId)) {
                return false;
            }
        }
        return $this;
    }

    /**
     * Returns last FAILED OR CANCEL PAYMENT for a certain user.
     *
     * @param integer $userId
     *
     * @return $this|bool
     */
    public function getLastCancelPayment($userId)
    {
        $sql = " SELECT " . $this->mainFields() .
            " FROM " . $this->tableName() . " p " .
            " WHERE p.`userId`=".$userId.
            " AND p.`status` IN (" . PAY_CANCEL .",".PAY_FAIL. ")".
            " ORDER BY p.`dateEndTransaction` DESC LIMIT 1; ";
        $dataReader = $this->querySQL($sql);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param bool $idPayment
     *
     * @return bool|Payment
     */
    public function getPaymentById($idPayment = false)
    {
        $sql = "SELECT ".$this->mainFields(). " FROM ".$this->tableName()." p WHERE p.`id`= :ID";
        $paramsToSQL = array(":ID" => $idPayment);
        $dataReader=$this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read())!== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * #ZOR-267
     *
     * @param $userId
     * @param $paySuppExtProfId
     * @param string $status
     *
     * @return $this|bool
     */
    public function getPaymentByPaySuppExtProfId($userId=0, $paySuppExtProfId="", $status="")
    {

        $paramsToSQL = array(":USERID" => $userId, ":PAYSUPPEXTPROFID" => $paySuppExtProfId);
        $strPayStatus = "";

        if ($status != "") {

            $strPayStatus = "
                AND p.`status` = :STATUS ";

            $paramsToSQL[":STATUS"] = $status;
        }

        $sSql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " p
            WHERE
                p.userId = :USERID
                AND p.paySuppExtProfId = :PAYSUPPEXTPROFID
            " . $strPayStatus . "
            ORDER BY p.dateToPay DESC
            LIMIT 1
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * #ZOR-303
     *
     * @param int $userId
     * @param string $paySuppExtUniId
     * @param string $status
     *
     * @return $this|bool
     */
    public function getPaymentByPaySuppExtUniId($userId=0, $paySuppExtUniId="", $status="")
    {

        $paramsToSQL = array(":USERID" => $userId, ":PAYSUPPEXTUNIID" => $paySuppExtUniId);
        $strPayStatus = "";

        if ($status != "") {

            $strPayStatus = "
                AND p.`status` = :STATUS ";

            $paramsToSQL[":STATUS"] = $status;
        }

        $sSql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " p
            WHERE
                p.userId = :USERID
                AND p.paySuppExtUniId = :PAYSUPPEXTUNIID
            " . $strPayStatus . "
            ORDER BY p.dateToPay DESC
            LIMIT 1
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }
    /**
     * #ZOR-267
     *
     * @return bool
     *
     * @throws CDbException
     */
    public function updatePaymentToCancel()
    {
        $this->beginTrans();
        $sql = "
            UPDATE
                " . $this->tableName() . " p
            SET
                p.`dateStartTransaction`= NOW(),
                p.`dateEndTransaction`= NOW(),
                p.`dateToPay`= :DATETOPAY,
                p.`foreignCurrencyTrans`= :FOREIGNCURRENCYTRTANS,
                p.`isRecurring`= :ISRECURRING,
                p.`cancelOrigin`= :CANCELORIGIN,
                p.`lastAction`= :LASTACTION,
                p.`status`= :STATUS
            WHERE
                p.`id`=:ID ;
        ";

        $aParamValues = array(
          ':DATETOPAY' => $this->dateToPay,
          ':FOREIGNCURRENCYTRTANS' => $this->foreignCurrencyTrans,
          ':ISRECURRING' => $this->isRecurring,
          ':CANCELORIGIN' => $this->cancelOrigin,
          ':LASTACTION' => $this->lastAction,
          ':STATUS' => $this->status,
          ':ID' => $this->id,
        );

        if ($this->updateSQL($sql, $aParamValues) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

    /**
     * #ZOR-267
     *
     * @param $paySuppExtId
     * @param $paySuppOrderId
     * @param $status
     * @param $dateStart
     * @param $xRateToEUR
     * @param $xRateToUSD
     * @param null $paySuppExtUniId
     *
     * @return bool
     */
    public function recurringPaymentProcess( $paySuppExtId, $paySuppOrderId, $status, $dateStart, $xRateToEUR, $xRateToUSD, $paySuppExtUniId=NULL )
    {

        $bUpdate = $this->updateRecurringPayment($paySuppExtId, $paySuppOrderId, $status, $dateStart, $xRateToEUR, $xRateToUSD, $paySuppExtUniId);

        try {
            if ($bUpdate) {

                $oPayment = new CmAbaPayment();
                $oPayment->getPaymentById($this->id);

                $moInvoice = new CmAbaUserInvoices();

                if(!$moInvoice->createPreInvoice($oPayment, $oPayment->userId)) {
                    HeLogger::sendLog(
                      HeLogger::PREFIX_ERR_LOGIC_L . ' Process create pre invoice failed',
                      HeLogger::IT_BUGS,
                      HeLogger::CRITICAL,
                      'recurringPaymentProcess - createPreInvoice'
                    );
                }
            }
            else {
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_LOGIC_L . ' Process create pre invoice failed',
                  HeLogger::IT_BUGS,
                  HeLogger::CRITICAL,
                  'recurringPaymentProcess - validateInvoicePartner'
                );
            }
        }
        catch (Exception $e) {
            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_LOGIC_L . ' Process create pre invoice failed',
              HeLogger::IT_BUGS,
              HeLogger::CRITICAL,
              'recurringPaymentProcess'
            );
        }

        return $bUpdate;
    }

    /**
     * #ZOR-267
     *
     * @param $paySuppExtId
     * @param $paySuppOrderId
     * @param $status
     * @param $dateStart
     * @param $xRateToEUR
     * @param $xRateToUSD
     * @param null $paySuppExtUniId
     *
     * @return bool
     *
     * @throws CDbException
     */
    public function updateRecurringPayment(
      $paySuppExtId,
      $paySuppOrderId,
      $status,
      $dateStart,
      $xRateToEUR,
      $xRateToUSD,
      $paySuppExtUniId = null
    ) {

        //#4480
        if ($paySuppExtId != $this->paySuppExtId) {

            $paySuppExtIdTmp = $this->paySuppExtId;
            $this->paySuppExtId = $paySuppExtId;
            $this->setAmountWithoutTax($dateStart);
            $this->paySuppExtId = $paySuppExtIdTmp;
        } else {
            $this->setAmountWithoutTax($dateStart);
        }
        $this->beginTrans();

        $stData = array(
          ":PAYSUPPEXTUNIID" => $paySuppExtUniId,
          ":TAXRATEVALUE" => $this->taxRateValue,
          ":AMOUNTTAX" => $this->amountTax,
          ":AMOUNTPRICEWITHOUTTAX" => $this->amountPriceWithoutTax,
          ":PAYSUPPEXTID" => $paySuppExtId,
          ":PAYSUPPORDERID" => $paySuppOrderId,
          ":STATUS" => $status,
          ":DATESTART" => $dateStart,
          ":DATEEND" => $dateStart,
          ":RATETOEUR" => $xRateToEUR,
          ":RATETOUSD" => $xRateToUSD,
          ":ID" => $this->id
        );

        $sql = "
            UPDATE " . $this->tableName() . " p
            SET
                p.`paySuppExtId`= :PAYSUPPEXTID,
                p.`paySuppOrderId`= :PAYSUPPORDERID,
                p.`status`= :STATUS,
                p.`dateStartTransaction`= :DATESTART,
                p.`dateEndTransaction`= :DATEEND,
                p.`xRateToEUR`= :RATETOEUR,
                p.`xRateToUSD`= :RATETOUSD,
                p.`lastAction` = IF(`lastAction` IS NULL,
                                CONCAT( NOW(), ' [ ', 'Recurrent payment zuora SUCCESS', ' ] ' ),
                                CONCAT(`lastAction`, ' ; ', NOW(), ' [ ', 'Recurrent payment zuora SUCCESS', ' ] ')
                                ),
                p.`paySuppExtUniId`= :PAYSUPPEXTUNIID,
                p.`taxRateValue`= :TAXRATEVALUE,
                p.`amountTax`= :AMOUNTTAX,
                p.`amountPriceWithoutTax`= :AMOUNTPRICEWITHOUTTAX
            WHERE p.`id`= :ID
        ";

        if ($this->updateSQL($sql, $stData) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }


    /**
     * @param $userId
     *
     * @return $this|bool
     */
    public function getLastSuccessPayment($userId)
    {
        $sql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " p
            WHERE
                p.userId='$userId' AND
                p.`status`='" . PAY_SUCCESS . "'
            ORDER BY p.dateStartTransaction DESC, p.dateEndTransaction DESC
            LIMIT 1;
        ";

        $dataReader = $this->querySQL($sql);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }


    /**
     * @return $this|bool
     */
    public function getFirstSuccessPaymentZuoraByPaySuppExtProfId()
    {

        $stData = array(
          ":USERID" => $this->userId,
          ":PAYSUPPEXTID" => PAY_SUPPLIER_Z,
          ":STATUS" => PAY_SUCCESS,
          ":PAYSUPPEXTPROFID" => $this->paySuppExtProfId,
          ":ISRECURRING" => 0
        );

        $sql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " AS p
            WHERE
                p.userId=:USERID
                AND p.paySuppExtId=:PAYSUPPEXTID
                AND p.`status`=:STATUS
                AND p.`paySuppExtProfId`=:PAYSUPPEXTPROFID
                AND p.`isRecurring`=:ISRECURRING
            ORDER BY p.autoId DESC
            LIMIT 1 ;
        ";

        $dataReader = $this->querySQL($sql, $stData);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return bool
     *
     * @throws CDbException
     */
    public function updateInvoiceZuoraPayment()
    {
        $this->beginTrans();

        $sSql = "
            UPDATE
                " . $this->tableName() . " p
            SET
                p.`paySuppExtUniId`= :PAYSUPPEXTUNIID,
                p.`amountPrice`= :AMOUNTPRICE,
                p.`amountDiscount`= :AMOUNTDISCOUNT,
                p.`taxRateValue`= :TAXRATEVALUE,
                p.`amountTax`= :AMOUNTTAX,
                p.`amountPriceWithoutTax`= :AMOUNTPRICEWITHOUTTAX
            WHERE
                p.`id`=:ID
            ;
        ";

        $aParamValues = array(
          ':PAYSUPPEXTUNIID' => $this->paySuppExtUniId,
          ':AMOUNTPRICE' => $this->amountPrice,
          ':AMOUNTDISCOUNT' => $this->amountDiscount,
          ':TAXRATEVALUE' => $this->taxRateValue,
          ':AMOUNTTAX' => $this->amountTax,
          ':AMOUNTPRICEWITHOUTTAX' => $this->amountPriceWithoutTax,
          ':ID' => $this->id,
        );

        if ($this->updateSQL($sSql, $aParamValues) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

    public function getPaymentsCancelledbyZuora() {

        $stData = array(
            ":PAYSUPPEXTID" => PAY_SUPPLIER_Z,
            ":STATUS" => PAY_CANCEL
        );

        $sql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " AS p
            WHERE
                p.paySuppExtId=:PAYSUPPEXTID
                AND p.`status`=:STATUS;
        ";

        $dataReader = $this->querySQL($sql, $stData);

        $payments = array();
        while (($row = $dataReader->read()) !== false) {
            $payments[] = $row;
        }

        return $payments;

    }

    public function getPaymentsbySubscriptionId($subscriptionId = '') {

        $stData = array(
            ":PAYSUPPEXTID" => PAY_SUPPLIER_Z,
            ":SUBSCRIPTIONID" => $subscriptionId
        );

        $sql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " AS p, user_zuora AS u
            WHERE
                u.userId = p.userId
                AND p.`paySuppExtId`=:PAYSUPPEXTID
                AND u.`zuoraSubscriptionId`=:SUBSCRIPTIONID;
        ";

        $dataReader = $this->querySQL($sql, $stData);

        $payments = array();
        while (($row = $dataReader->read()) !== false) {
            $payments[] = $row;
        }

        return $payments;

    }

    public function getPaymentsbyAccountId($accountId = '') {

    $stData = array(
        ":PAYSUPPEXTID" => PAY_SUPPLIER_Z,
        ":ACCOUNTID" => $accountId
    );

    $sql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " AS p, user_zuora AS u
            WHERE
                u.userId = p.userId
                AND p.`paySuppExtId`=:PAYSUPPEXTID
                AND u.`zuoraAccountId`=:ACCOUNTID;
        ";

    $dataReader = $this->querySQL($sql, $stData);

    $payments = array();
    while (($row = $dataReader->read()) !== false) {
        $payments[] = $row;
    }

    return $payments;

}

}
