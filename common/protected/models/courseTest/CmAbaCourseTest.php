<?php

/**
 * This is the model class for table "course_test".
 *
 * The followings are the available columns in table 'course_test':
 * @property string $id
 * @property string $description
 * @property string $expired
 * @property string $numUsers
 * @property string $locale
 * @property integer $sections
 *
 * The followings are the available model relations:
 * @property CourseTestUser[] $courseTestUsers
 */
class CmAbaCourseTest extends AbaActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'course_test';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sections', 'numerical', 'integerOnly'=>true),
			array('description', 'length', 'max'=>120),
			array('numUsers', 'length', 'max'=>10),
			array('locale', 'length', 'max'=>2),
			array('expired', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, description, expired, numUsers, locale, sections', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'courseTestUsers' => array(self::HAS_MANY, 'CourseTestUser', 'courseId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'description' => 'Description',
			'expired' => 'Expired',
			'numUsers' => 'Num Users',
			'locale' => 'Locale',
			'sections' => 'Value in binary 8 bits idicating de sections affected (11111111)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('expired',$this->expired,true);
		$criteria->compare('numUsers',$this->numUsers,true);
		$criteria->compare('locale',$this->locale,true);
		$criteria->compare('sections',$this->sections);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CourseTest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public  function retrieveCourse($courseId,$locale)
    {

        $sql="select sections from " . $this->tableName() . " where courseId=$courseId and locale like '%($locale)%' Limit 1";
        $dataReader =   $this->querySQL($sql);
        if(($row = $dataReader->read()) !== false)
        {
             return $row['sections'];
        }

        return 0;

    }

	public  function retrieveTest($user)
    {
			$this->beginTrans();
			$sql="select id,courseId,numUsers from " . $this->tableName() . " where expired>=now() and locale like '%($user->langEnv)%' order by numUsers Limit 1 for update";
			$dataReader =   $this->querySQL($sql);
			if(($row = $dataReader->read()) !== false)
			{
				$tmpCourse=$row['courseId'];
				$tmpId=$row['id'];
				$tmpNumUsers=$row['numUsers']+1;

				$updateParams = array (
						':numUsers_param'=>$tmpNumUsers,
						':id_param'=>$tmpId
				);

				$sql = "update course_test set numUsers=:numUsers_param where id=:id_param";

				if($this->updateSQL($sql, $updateParams)>0)
				{
					$this->commitTrans();
					//Now we insert the user
					$clsCmAbaCourseTestUser= new CmAbaCourseTestUser();
					$clsCmAbaCourseTestUser->userId=$user->id;
					$clsCmAbaCourseTestUser->courseId=$tmpCourse;
					$clsCmAbaCourseTestUser->save();
					return $tmpCourse;
				}
				else
				{
					$this->rollbackTrans();
					return 0;
				}
			}

			$this->rollbackTrans();
			return 0;


    }
}
