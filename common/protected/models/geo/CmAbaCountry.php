<?php
/**
 * @property integer id
 * @property string name_m
 * @property string name
 * @property string iso
 * @property string iso3
 * @property string days
 * @property string numcode
 * @property string ABAIdCurrency
 * @property string ABALanguage
 * @property string decimalPoint
 * @property integer invoice
 */
class CmAbaCountry  extends AbaActiveRecord
{
    public $id;
    /* @var GeoIP_Location $geoLocation */
    public $geoLocation;

    const COUNTRY_CODE_ISO2_CHINA_ZH = 'zh';
    const COUNTRY_CODE_ISO2_CHINA_CN = 'cn';

    /**
     * @param integer $id Id of the country, can be NULL
     */
    public function  __construct($id=NULL)
    {
        parent::__construct();
        if(!is_null($id)){
            return $this->getCountryById($id);
        }
        return $this;
    }


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'aba_b2c.country';
    }


    public function mainFields()
    {
        return " c.`id`, c.`days`, c.`ID_month`, c.`iso`, c.`name_m`, c.`name`, c.`iso3`, c.`numcode`,
                c.`officialIdCurrency`, c.`ABAIdCurrency`, c.`ABALanguage`, c.`decimalPoint`, c.`invoice` ";
    }

    /**
     * @param $id
     *
     * @return AbaCountry|bool
     */
    public function getCountryById($id)
    {
        $sql = " SELECT ".$this->mainFields()." FROM ".$this->tableName()." c WHERE c.id=:IDCOUNTRY ";
        $paramsToSQL = array(":IDCOUNTRY"=>$id);

        $dataReader=$this->querySQL($sql,$paramsToSQL);
        if(($row = $dataReader->read())!==false)
        {
            $this->fillDataColsToProperties( $row );
            return $this;
        }

        return false;
    }

    /**
     * @param $id
     *
     * @return $this|bool
     */
    public function getUeCountryById($id)
    {
        $sql =          " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " c WHERE c.invoice = 1 AND c.id=:IDCOUNTRY ";
        $paramsToSQL =  array(":IDCOUNTRY" => $id);
        $dataReader =   $this->querySQL($sql,$paramsToSQL);
        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties( $row );
            return $this;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isUeCountry()
    {
        if($this->invoice == 1) {
            return true;
        }
        return false;
    }

    /**
     * @param $code
     *
     * @return bool
     */
    public function getCountryByIsoCode($code)
    {
        $sql = " SELECT c.`id`, c.`iso`, c.`name` FROM ".$this->tableName()." c WHERE c.iso=:ISO_CODE ";
        $paramsToSQL = array(":ISO_CODE"=>$code);
        $dataReader=$this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return true;
        }
        return false;
    }

    /**
     * @param null $deviceIp
     *
     * @return CmAbaCountry|bool
     */
    public function getCountryCodeByIP($deviceIp = null)
    {
        $this->geoLocation = Yii::app()->geoip->lookupLocation($deviceIp);
        if (is_null($this->geoLocation)) {
            $this->geoLocation = new GeoIP_Location();
            $this->geoLocation->longitude = 0.000;
            $this->geoLocation->latitude = 0.000;
            $this->geoLocation->countryCode = 0.000;
            $this->geoLocation->areaCode = "";
            $this->geoLocation->city = "";
            $code = Yii::app()->config->get("DefaultIPCountryCode");
        } else {
            $code = $this->geoLocation->countryCode;
        }
        if ($this->getCountryByIsoCode($code)) {
            return $this;
        }
        return false;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }


    public function getFullUeList($idCountry=-1)
    {
        $sql =  " SELECT id, name FROM country ";
        $sql .= " WHERE invoice = 1 ";
        if($idCountry != -1) {
            $sql.=" AND id = " . $idCountry . " ";
        }
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();
        $theArray = array();
        while(($row = $dataReader->read()) !== false){
            $theArray[$row['id']] = $row['name'];
        }
        return $theArray;
    }

    /**
     * @param bool|false $bEmpty
     * @param array $stParams
     *
     * @return array
     */
    public static function getDefaultLanguages($bEmpty=false, $stParams=array()) {

        $stDefaultLanguages = Yii::app()->config->get("INTRANET_DEFAULT_LANGUAGES");

        if(trim($stDefaultLanguages) == '') {
            if($bEmpty) {
                return array('' => '', 'es' => 'es', 'fr' => 'fr', 'it' => 'it', 'pt' => 'pt', 'en' => 'en', 'de' => 'de', 'ru' => 'ru', 'zh' => 'zh', 'tr' => 'tr');
            }
            else {
                return array('es' => 'es', 'fr' => 'fr', 'it' => 'it', 'pt' => 'pt', 'en' => 'en', 'de' => 'de', 'ru' => 'ru', 'zh' => 'zh', 'tr' => 'tr');
            }
        }

        $stLanguages = explode(",", $stDefaultLanguages);

        $stFormattedLanguages = array();
        if($bEmpty) {
            $stFormattedLanguages[''] = '';
        }

        foreach($stLanguages AS $iKey => $sLanguage) {
            $stFormattedLanguages[$sLanguage] = $sLanguage;
        }

        return $stFormattedLanguages;
    }

}

