<?php

/**
 * This is the model class for table "products_promos_description".
 *
 * The followings are the available columns in table 'products_promos_description':
 * @property integer $idDescription
 * @property string $translationKey
 * @property string $descriptionText
 * @property string $createDate
 */
class CmAbaProductsPromosDescription extends AbaActiveRecord
{
    /**
     * Constructor, null implementation
     */
    public function __construct($scenario=NULL, $dbConnName=NULL) {
        parent::__construct($scenario, $dbConnName);
    }

    /** Table of this model
     * @return string
     */
    public function tableName() {
        return 'products_promos_description';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AbaProductsPromosDescription the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string
     */
    public function mainFields() {
        return " pd.`idDescription`, pd.`translationKey`, pd.`descriptionText`, pd.`createDate` ";
    }

    /**
    * @return array validation rules for model attributes.
    */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('translationKey', 'required'),
            array('translationKey, descriptionText, createDate', 'type', 'type' => 'string', 'allowEmpty' => true),
            // @todo Please remove those attributes that should not be searched.
            array('idDescription, translationKey, descriptionText, createDate', 'safe', 'on'=>'search'),
        );
    }

    /**
    * @return array relational rules.
    */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @param $idDescription
     *
     * @return $this|bool
     */
    public function getDescriptionById( $idDescription )
    {
        $sSql =         " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS pd WHERE pd.`idDescription`=:IDDESCRIPTION ";
        $paramsToSQL =  array(":IDDESCRIPTION" => $idDescription);
        $dataReader =   $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return array|bool
     */
    public function getAllDescriptions()
    {
        $sSql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " AS pd
        ";
        $paramsToSQL =  array();
        $dataReader =   $this->querySQL($sSql, $paramsToSQL);
        if( !$dataReader ) {
            return false;
        }

        $aProds = array();
        while(($row = $dataReader->read()) !== false) {
            $aProds[] = array(
                "idDescription" =>      $row['idDescription'],
                "translationKey" =>     $row['translationKey'],
                "descriptionText" =>    $row['descriptionText'],
            );
        }

        return $aProds;
    }

}
