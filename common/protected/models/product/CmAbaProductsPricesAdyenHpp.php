<?php

/**
 * CmAbaProductsPricesAdyenHpp
 *
 * This is the model class for table "products_prices_adyen_hpp".
 *
 * The followings are the available columns in table 'products_prices_adyen_hpp':
 * @property integer $idProductPrice
 * @property string $idProduct
 * @property string $idPaymentMethod
 * @property string $currencyAdyenHpp
 * @property string $priceAdyenHpp
 * @property integer $enabled
 * @property string $modified
 * @property string $priceAdyenHppExtranet
 * @property integer $enabledExtranet
 */
class CmAbaProductsPricesAdyenHpp extends AbaActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'products_prices_adyen_hpp';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
          array(
            'idProduct, idPaymentMethod, currencyAdyenHpp, priceAdyenHpp, enabled, modified, priceAdyenHppExtranet, enabledExtranet',
            'required'
          ),
          array('idProductPrice, idPaymentMethod, enabled, enabledExtranet', 'numerical', 'integerOnly' => true),
          array('priceAdyenHpp, priceAdyenHppExtranet', 'length', 'max' => 18),
            // @todo Please remove those attributes that should not be searched.
          array(
            'idProductPrice, idProduct, idPaymentMethod, currencyAdyenHpp, priceAdyenHpp, enabled, modified, priceAdyenHppExtranet, enabledExtranet',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaProductsPricesAdyenHpp the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @param $idProduct
     * @param int $enabled
     *
     * @return $this|bool
     */
    public function getProductById($idProduct, $enabled=1)
    {
        $paramsToSQL = array(":IDPRODUCT" => $idProduct);

        $sql = " SELECT pr.* ";
        $sql .= " FROM " . $this->tableName() . " pr ";
        $sql .= " WHERE pr.idProduct = :IDPRODUCT ";

        if ($enabled == 1) {

            $paramsToSQL[":ENABLED"] = $enabled;

            $sql .= " AND pr.enabled = :ENABLED ";
        }

        $dataReader = $this->querySQL($sql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $idProduct
     * @param int $enabled
     *
     * @return $this|bool
     */
    public function getAdyenHppProductPrice($paymentMethod, $idProduct, $enabled=1, $pmaenabled=1)
    {
        $paramsToSQL = array(
          ":IDPRODUCT" => $idProduct,
          ":ENABLED" => $enabled,
          ":PMAENABLED" => $pmaenabled,
          ":PAYMENTMETHOD" => $paymentMethod,
        );

        $sql = " SELECT pma.paymentMethod, cu.symbol, pr.idProductPrice, pr.idProduct, pr.idPaymentMethod, pr.currencyAdyenHpp, pr.priceAdyenHpp, pp.isPlanPrice ";
        $sql .= " FROM " . $this->tableName() . " pr ";
        $sql .= " JOIN pay_methods_adyen_hpp pma ";
        $sql .= "     ON pma.idPaymentMethod = pr.idPaymentMethod AND pma.enabled = :PMAENABLED AND pma.paymentMethod = :PAYMENTMETHOD ";
        $sql .= " LEFT JOIN currencies cu ";
        $sql .= "     ON cu.id = pr.currencyAdyenHpp ";
        $sql .= " LEFT JOIN products_prices pp ";
        $sql .= "     ON pp.idProduct = pr.idProduct ";
        $sql .= " WHERE pr.idProduct = :IDPRODUCT ";
        $sql .= "     AND pr.enabled = :ENABLED ";
        $sql .= " LIMIT 0, 1 ";

        $dataReader = $this->querySQL($sql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            return array(
                "paymentMethod" => $row["paymentMethod"],
                "symbol" => $row["symbol"],
                "idProductPrice" => $row["idProductPrice"],
                "idProduct" => $row["idProduct"],
                "idPaymentMethod" => $row["idPaymentMethod"],
                "currencyAdyenHpp" => $row["currencyAdyenHpp"],
                "priceAdyenHpp" => $row["priceAdyenHpp"],
                "finalPriceAdyenHpp" => $row["priceAdyenHpp"],
                "isPlanPrice" => $row["isPlanPrice"],
            );
        }
        return false;
    }

    /**
     * @param $idProduct
     * @param int $enabled
     * @param int $pmaenabled
     *
     * @return array|bool
     */
    public function getAdyenHppCurrenciesByProduct($idProduct, $enabled=1, $pmaenabled=1)
    {
        $paramsToSQL = array(
          ":IDPRODUCT" => $idProduct,
          ":ENABLED" => $enabled,
          ":PMAENABLED" => $pmaenabled
        );

        $sql = " SELECT pma.paymentMethod, pr.idProduct, pr.currencyAdyenHpp, pr.priceAdyenHpp ";
        $sql .= " FROM " . $this->tableName() . " pr ";
        $sql .= " JOIN pay_methods_adyen_hpp pma ";
        $sql .= "     ON pma.idPaymentMethod = pr.idPaymentMethod AND pma.enabled = :PMAENABLED ";
        $sql .= " LEFT JOIN currencies cu ";
        $sql .= "     ON cu.id = pr.currencyAdyenHpp ";
        $sql .= " WHERE pr.idProduct = :IDPRODUCT ";
        $sql .= "     AND pr.enabled = :ENABLED ";
        $sql .= " GROUP BY pma.paymentMethod, pr.idProduct, pr.currencyAdyenHpp, pr.priceAdyenHpp ";

        $dataReader = $this->querySQL($sql, $paramsToSQL);

        $stProductCurrencies = array();

        while (($row = $dataReader->read()) !== false) {
            $stProductCurrencies[] = array(
              "paymentMethod" => $row["paymentMethod"],
              "idProduct" => $row["idProduct"],
              "currencyAdyenHpp" => $row["currencyAdyenHpp"],
              "priceAdyenHpp" => $row["priceAdyenHpp"],
            );
        }

        return $stProductCurrencies;
    }

}
