<?php

/**
 * This is the model class for table "products_prices_appstore".
 *
 * The followings are the available columns in table 'products_prices_appstore':
 * @property integer $idProductTier
 * @property string $idProductApp
 * @property integer $idPeriodPay
 * @property string $currencyAppStore
 * @property integer $tier
 * @property float $priceAppStore
 * @property integer $isBestOffer
 * @property string $idProduct
 * @property integer $idCountry
 * @property string $ABALanguage
 * @property integer $isDefault
 */
class CmAbaProductsPricesAppstore extends AbaActiveRecord
{
    const DEFAULT_MONTH =       1;
    const DEFAULT_CURRENCY =    'EUR';

    /**
     * Constructor, null implementation
     */
    public function __construct($scenario=NULL, $dbConnName=NULL) {
        parent::__construct($scenario, $dbConnName);
    }

    /** Table of this model
     * @return string
     */
    public function tableName() {
        return 'products_prices_appstore';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AbaProductsPricesAppstore the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string
     */
    public function mainFields() {
        return " pp.`idProductTier`, pp.`idProductApp`, pp.`idPeriodPay`, pp.`currencyAppStore`, pp.`tier`, pp.`priceAppStore`, pp.`isBestOffer`, pp.`idProduct`, pp.`idCountry`, pp.`ABALanguage`, pp.`isDefault` ";
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idProductApp, idPeriodPay, tier', 'required'),
            array('priceAppStore', 'type', 'type' => 'float', 'allowEmpty' => true),
            array('idCountry', 'type', 'type' => 'integer', 'allowEmpty' => true),
            array('isBestOffer', 'type', 'type' => 'integer', 'allowEmpty' => true),
            array('isDefault', 'type', 'type' => 'integer', 'allowEmpty' => true),
            array('idProduct, currencyAppStore, ABALanguage', 'type', 'type' => 'string', 'allowEmpty' => true),
            array('idCountry', 'length', 'max' => 4),
			// @todo Please remove those attributes that should not be searched.
			array('idProductTier, idProductApp, idPeriodPay, currencyAppStore, tier, priceAppStore, isBestOffer, idProduct, idCountry, ABALanguage, isDefault', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    /**
     * @param AbaUser $moUser
     *
     * @return array|bool
     */
    public function getProductsTiersByUserCurrency(AbaUser $moUser)
    {
        $sSql = "
            SELECT pp.idProductApp, pp.idPeriodPay, pp.tier, pp.isBestOffer
            FROM `user` AS us
            JOIN country AS co ON us.countryId = co.id
            JOIN " . $this->tableName() . " AS pp ON pp.currencyAppStore = co.ABAIdCurrency
            WHERE us.id = :USERID
        ";

        $paramsToSQL =  array(":USERID" => $moUser->id);

        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if( !$dataReader ) {
            return false;
        }

        $aProds = array();
        while(($row = $dataReader->read()) !== false) {
            $aProds[] = array(
                "tier" =>       $row['tier'],
                "identifier" => $row['idProductApp'],
                "days" =>       $row['idPeriodPay'],
                "best_offer" => $row['isBestOffer'],
            );
        }

        return $aProds;
    }

    /**
     * @param AbaUser $moUser
     *
     * @return array|bool
     */
    public function getProductsTiersByUserCountry($moUser, $stParams=array())
    {
        $paramsToSQL =  array(":USERID" => $moUser->id);

        $sSql = "
            SELECT pp.idProductApp, pp.idPeriodPay, pp.tier, pp.isBestOffer
            FROM `user` AS us
            JOIN " . $this->tableName() . " AS pp ON pp.idCountry = us.countryId
            WHERE us.id = :USERID
        ";

        if(isset($stParams["idPeriodPay"])) {

            $paramsToSQL[":IDPERIODPAY"] = $stParams["idPeriodPay"];

            $sSql .= "
                AND pp.idPeriodPay = :IDPERIODPAY
            ";
        }

        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if( !$dataReader ) {
            return false;
        }

        $aProds = array();
        while(($row = $dataReader->read()) !== false) {
            $aProds[] = array(
                "tier" =>       $row['tier'],
                "identifier" => $row['idProductApp'],
                "days" =>       $row['idPeriodPay'],
                "best_offer" => $row['isBestOffer'],
            );
        }
        return $aProds;
    }

    /**
     * @param AbaUser $moUser
     *
     * @return array|bool
     */
    public function getDefaultProductsTiersByUser($moUser, $stParams=array()) {

        $paramsToSQL =  array();

        //
        // Defualt prices
        $sSql = "
            SELECT pp.idProductApp, pp.idPeriodPay, pp.tier, pp.isBestOffer
            FROM " . $this->tableName() . " AS pp
            WHERE pp.isDefault = 1
        ";

        if(isset($stParams["idPeriodPay"])) {

            $paramsToSQL[":IDPERIODPAY"] = $stParams["idPeriodPay"];

            $sSql .= "
                AND pp.idPeriodPay = :IDPERIODPAY
            ";
        }

        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if( !$dataReader ) {
            return false;
        }

        $aProds = array();
        while(($row = $dataReader->read()) !== false) {
            $aProds[] = array(
                "tier" =>       $row['tier'],
                "identifier" => $row['idProductApp'],
                "days" =>       $row['idPeriodPay'],
                "best_offer" => $row['isBestOffer'],
            );
        }
        return $aProds;
    }


    /**
     * @param $idProductApp
     *
     * @return $this|bool
     */
    public function getProductTierByIdProductApp($idProductApp)
    {
        $sSql =         " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS pp ";
        $sSql .=        " WHERE pp.idProductApp=:IDPRODUCTAPP ";
        $paramsToSQL =  array(":IDPRODUCTAPP" => $idProductApp);
        $dataReader =   $this->querySQL($sSql, $paramsToSQL);
        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return int
     */
    public function getPeriodMonths() {
        $aIdProductApp = explode("_", $this->idProductApp);
        if(isset($aIdProductApp[2])) {
            return (int) $aIdProductApp[2];
        }
        HeLogger::sendLog("Update to premium. updateFreetopremium->getPeriodMonths. Warning. ", HeLogger::PAYMENTS,
          HeLogger::CRITICAL, 'Not valid idProductApp: ' . $this->idProductApp . '. Empty period id');
        return self::DEFAULT_MONTH;
    }

    /**
     * @return string
     */
    public function getCurrency() {
        $aIdProductApp = explode("_", $this->idProductApp);
        if(isset($aIdProductApp[0])) {
            return $aIdProductApp[0];
        }
        HeLogger::sendLog("Update to premium. updateFreetopremium->getCurrency. Warning. ",HeLogger::PAYMENTS,
          HeLogger::CRITICAL,'Not valid idProductApp: ' . $this->idProductApp . '. Empty currency');
        return self::DEFAULT_CURRENCY;
    }

    /**
     * @return string
     */
    public function getCurrencyAppStore() {
        if(trim($this->currencyAppStore) == '') {
            return self::DEFAULT_CURRENCY;
        }
        return $this->currencyAppStore;
    }

}
