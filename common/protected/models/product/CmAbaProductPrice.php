<?php
/**
 * CmAbaProductPrice
 * @author: mgadegaard
 * Date: 30/06/2015
 * Time: 17:32
 * © ABA English 2015
 *
 * This is the model class for table "products_prices".
 *
 * The followings are the available columns in table 'products_prices':
 * @property string $idProduct
 * @property string $idCountry
 * @property string $idPeriodPay
 * @property string $priceOfficialCry
 * @property integer $enabled
 * @property string $modified
 * @property string $descriptionText
 * @property string $userType
 * @property string $hierarchy
 * @property integer $visibleWeb
 * @property string $priceExtranet
 * @property integer $enabledExtranet
 * @property integer $isPlanPrice
 */

class CmAbaProductPrice extends AbaActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'products_prices';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('idProduct, idCountry, idPeriodPay, priceOfficialCry, modified, hierarchy', 'required'),
            array('enabled, visibleWeb, enabledExtranet, isPlanPrice', 'numerical', 'integerOnly'=>true),
            array('idProduct', 'length', 'max'=>15),
            array('idCountry', 'length', 'max'=>4),
            array('idPeriodPay', 'length', 'max'=>5),
            array('priceOfficialCry, priceExtranet', 'length', 'max'=>18),
            array('descriptionText', 'length', 'max'=>30),
            array('userType, hierarchy', 'length', 'max'=>2),
            // @todo Please remove those attributes that should not be searched.
            array('idProduct, idCountry, idPeriodPay, priceOfficialCry, enabled, modified, descriptionText, userType, hierarchy, visibleWeb, priceExtranet, enabledExtranet, isPlanPrice', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idProduct' => 'foreign key to products_list',
            'idCountry' => 'foreign key to countries. Default country 0',
            'idPeriodPay' => 'foreign keys to periodicities(also quantity of days)',
            'priceOfficialCry' => 'Price in the original currency of idCountry:countries.officialIdCurrency',
            'enabled' => 'If is active or not this price',
            'modified' => 'Date this record is accessed and modified.',
            'descriptionText' => 'It should be as same as product description with periodicity.',
            'userType' => 'Correspondence with UserAccess:0-lead,Free-1,Premium-2,Plus-3, Teacher-4',
            'hierarchy' => 'Just to set an order quality, precedence',
            'visibleWeb' => 'If it is a public product or only for special purposes.',
            'priceExtranet' => 'Extranet price',
            'enabledExtranet' => 'If is active or not extranet price',
            'isPlanPrice' => 'Family plan...',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('idProduct', $this->idProduct, true);
        $criteria->compare('idCountry', $this->idCountry, true);
        $criteria->compare('idPeriodPay', $this->idPeriodPay, true);
        $criteria->compare('priceOfficialCry', $this->priceOfficialCry, true);
        $criteria->compare('enabled', $this->enabled);
        $criteria->compare('modified', $this->modified, true);
        $criteria->compare('descriptionText', $this->descriptionText, true);
        $criteria->compare('userType', $this->userType, true);
        $criteria->compare('hierarchy', $this->hierarchy, true);
        $criteria->compare('visibleWeb', $this->visibleWeb);
        $criteria->compare('priceExtranet', $this->priceExtranet, true);
        $criteria->compare('enabledExtranet', $this->enabledExtranet);
        $criteria->compare('isPlanPrice', $this->isPlanPrice);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaProductPrice the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @param     $idProduct
     * @param int $enabled
     *
     * @return $this|bool
     */
    public function getProductById($idProduct, $enabled = 1)
    {
        $wEnabled = "";
        if ($enabled) {
            $wEnabled = " AND pr.enabled = ".$enabled ;
        }
        $sql = " SELECT pr.* FROM products_prices pr WHERE pr.idProduct = :IDPRODUCT " . $wEnabled . " " ;
        $paramsToSQL = array(":IDPRODUCT" => $idProduct );
        $dataReader=$this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * Return the Product for a periodicity and a country and a UserType. If 2 are encountered then the first one
     * is returned.
     * @param     $idCountry
     * @param     $idPeriodPay
     * @param int $userType
     * @param bool $enabledForExtranet Boolean that indicates whether product should also be available for extranet
     *
     * @return bool|ProductPrice
     */
    public function getProductByCountryIdPeriodId($idCountry, $idPeriodPay, $userType = 2, $enabledForExtranet = false)
    {
        $sql = " SELECT pr.*
                    FROM products_prices pr
                    WHERE pr.idPeriodPay=$idPeriodPay AND
                          pr.idCountry IN (" . $idCountry . ", " . Yii::app()->config->get("ABACountryId") . ") AND
                          pr.enabled=1 AND
                          pr.userType=$userType
        ";
        if ($enabledForExtranet) {
            $sql .= "
                    AND pr.enabledExtranet=1
            ";
        }
        $sql .= "
                    ORDER BY pr.idCountry ASC
                    LIMIT 1 ";
        $dataReader = $this->querySQL($sql);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $currency
     *
     * @return string
     */
    public function getAppStoreCurrency($currency)
    {
        if (trim($currency) != '') {
            return $currency;
        }
        $moCountry = new CmAbaCountry($this->idCountry);
        return $moCountry->ABAIdCurrency;
    }

    /**
     * @param     $idProduct
     * @param int $enabled
     *
     * @return $this|bool
     */
    public function getProductByIdCountryandPeriod($idCountry, $period)
    {
        $sql = " SELECT pr.* FROM products_prices pr WHERE pr.idCountry = :IDCOUNTRY  and pr.idPeriodPay = :PERIOD";
        $paramsToSQL = array(":IDCOUNTRY" => $idCountry, ":PERIOD" => $period );
        $dataReader=$this->querySQL($sql, $paramsToSQL);
        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }
}
