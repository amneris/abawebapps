<?php

/**
 * CmAbaAdyenHppPaymentMethods
 *
 * This is the model class for table "pay_methods_adyen_hpp".
 *
 * The followings are the available columns in table 'pay_methods_adyen_hpp':
 * @property integer $idPaymentMethod
 * @property string $paymentMethod
 * @property integer $enabled
 */
class CmAbaAdyenHppPaymentMethods extends AbaActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'pay_methods_adyen_hpp';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaProductPrice the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
          array('paymentMethod, enabled', 'required'),
          array(
            'idPaymentMethod, enabled',
            'numerical',
            'integerOnly' => true
          ),
          array(
            'idPaymentMethod, paymentMethod, enabled',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " apm.`idPaymentMethod`, apm.`paymentMethod`, apm.`enabled` ";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @param $idPaymentMethod
     * @param null $bEnabled
     *
     * @return $this|bool
     */
    public function getPaymentMethodById($idPaymentMethod, $bEnabled=null)
    {
        $paramsToSQL = array(":IDPAYMENTMETHOD" => $idPaymentMethod);

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " apm
            WHERE apm.idPaymentMethod = :IDPAYMENTMETHOD
        ";

        if(is_numeric($bEnabled)) {
            $sSql .= "
                AND apm.enabled = :ENABLED
            ";
            $paramsToSQL[":ENABLED"] = $bEnabled;
        }

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $paymentMethod
     * @param null $bEnabled
     *
     * @return $this|bool
     */
    public function getPaymentMethodByMethod($paymentMethod, $bEnabled=null)
    {
        $paramsToSQL = array(":PAYMENTMETHOD" => $paymentMethod);

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " apm
            WHERE apm.paymentMethod = :PAYMENTMETHOD
        ";

        if(is_numeric($bEnabled)) {
            $sSql .= "
                AND apm.enabled = :ENABLED
            ";
            $paramsToSQL[":ENABLED"] = $bEnabled;
        }

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param null $bEnabled
     *
     * @return array|bool
     */
    public function getAllAdyenHppPaymentMethods($bEnabled=null)
    {
        $sSql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " AS apm
        ";

        if(is_numeric($bEnabled)) {
            $sSql .= "
                WHERE apm.enabled = :ENABLED
            ";
            $paramsToSQL[":ENABLED"] = $bEnabled;
        }

        $paramsToSQL = array();
        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (!$dataReader) {
            return false;
        }

        $aProds = array();
        while (($row = $dataReader->read()) !== false) {
            $aProds[] = array(
              "idPaymentMethod" => $row['idPaymentMethod'],
              "paymentMethod" => $row['paymentMethod'],
            );
        }

        return $aProds;
    }

}
