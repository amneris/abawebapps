<?php
/**
 * @property integer id
 * @property integer idVersion
 * @property integer counterProgress
 * @property integer counterProgressHtml
 * @property integer limitProgress
 * @property integer limitProgressHtml
 * @property integer counterAudio
 * @property integer limitAudio
 */
class CmAbaFollowUpVersion extends AbaActiveRecord
{
    public $id;

    /**
     * @param null $scenario
     * @param null $dbConnName
     */
    public function __construct($scenario=NULL, $dbConnName=NULL) {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'followup_version';
    }

    /**
     * @return string
     */
    public function mainFields() {
        return " fv.`id`, fv.`idVersion`, fv.`counterProgress`, fv.`counterProgressHtml`, fv.`limitProgress`, fv.`limitProgressHtml`, fv.`counterAudio`, fv.`limitAudio` ";
    }

    /**
     * @return array
     */
    public function rules()
    {
        return array(
            array('id',                     'required'),
            array('idVersion',              'required'),
            array('counterProgress',  	    'required'),
            array('counterProgressHtml',  	'required'),
            array('limitProgress',  	    'required'),
            array('limitProgressHtml',  	'required'),
            array('counterAudio',  	        'required'),
            array('limitAudio',  	        'required'),
            array('idVersion, counterProgress, counterProgressHtml, limitProgress, limitProgressHtml, counterAudio, limitAudio', 'type', 'type' => 'integer', 'allowEmpty' => true),
        );
    }

    /**
     * @return $this|bool
     */
    public function loadProgressVersionByVersion()
    {
        $sql =          " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " fv WHERE fv.limitProgress > fv.counterProgress ORDER BY fv.id ASC LIMIT 0, 1 ";
        $dataReader =   $this->querySQL($sql);
        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties( $row );
            return $this;
        }
        if(!is_numeric($this->idVersion)) {
            $this->idVersion = USER_DICTIONARY_DEF_VERSION_PROGRESS;
        }
        return false;
    }

    /**
     * @return $this|bool
     */
    public function loadProgressVersionHtmlByVersion()
    {
        $sql =          " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " fv WHERE fv.limitProgressHtml > fv.counterProgressHtml ORDER BY fv.id ASC LIMIT 0, 1 ";
        $dataReader =   $this->querySQL($sql);
        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties( $row );
            return $this;
        }
        if(!is_numeric($this->idVersion)) {
            $this->idVersion = USER_DICTIONARY_DEF_VERSIONHTML_PROGRESS;
        }
        return false;
    }

    /**
     * @return $this|bool
     */
    public function loadAudioVersionByVersion()
    {
        $sql =          " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " fv WHERE fv.limitAudio > fv.counterAudio ORDER BY fv.id ASC LIMIT 0, 1 ";
        $dataReader =   $this->querySQL($sql);
        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties( $row );
            return $this;
        }
        if(!is_numeric($this->idVersion)) {
            $this->idVersion = USER_DICTIONARY_DEF_VERSION_AUDIO;
        }
        return false;
    }

    /**
     * @return int|string
     */
    public function setNextProgressVersion() {
        if(!is_numeric($this->counterProgress)) {
            $this->counterProgress = 0;
        }
        $this->counterProgress = ($this->counterProgress + 1);
        return $this->counterProgress;
    }

    /**
     * @return int|string
     */
    public function setNextProgressVersionHtml() {
        if(!is_numeric($this->counterProgressHtml)) {
            $this->counterProgressHtml = 0;
        }
        $this->counterProgressHtml = ($this->counterProgressHtml + 1);
        return $this->counterProgressHtml;
    }

    /**
     * @return int|string
     */
    public function setNextAudioVersion() {
        if(!is_numeric($this->counterAudio)) {
            $this->counterAudio = 0;
        }
        $this->counterAudio = ($this->counterAudio + 1);
        return $this->counterAudio;
    }

    /**
     * @param $counterProgress
     *
     * @return bool
     */
    public function saveNextProgressVersionByVersion($idVersion) {
        $aBrowserValues = array (
            ':COUNTERPROGRESS' =>   $this->counterProgress,
            ':IDVERSION' =>         $idVersion,
        );
        $sql = "
            UPDATE " . $this->tableName() . " SET `counterProgress`=:COUNTERPROGRESS
            WHERE `idVersion`=:IDVERSION
        ";
        if( $this->updateSQL($sql, $aBrowserValues) > 0 ) {
            return true;
        }
        return false;
    }

    /**
     * @param $counterProgress
     *
     * @return bool
     */
    public function saveNextProgressVersionHtmlByVersion($idVersion) {
        $aBrowserValues = array (
            ':COUNTERPROGRESSHTML' =>   $this->counterProgressHtml,
            ':IDVERSION' =>         $idVersion,
        );
        $sql = "
            UPDATE " . $this->tableName() . " SET `counterProgressHtml`=:COUNTERPROGRESSHTML
            WHERE `idVersion`=:IDVERSION
        ";
        if( $this->updateSQL($sql, $aBrowserValues) > 0 ) {
            return true;
        }
        return false;
    }

    /**
     * @param $counterAudio
     *
     * @return bool
     */
    public function saveNextAudioVersionByVersion($idVersion) {

        $aBrowserValues = array (
            ':COUNTERAUDIO' =>  $this->counterAudio,
            ':IDVERSION' =>     $idVersion,
        );

        $sql = "
            UPDATE " . $this->tableName() . " SET `counterAudio`=:COUNTERAUDIO
            WHERE `idVersion`=:IDVERSION
        ";

        if( $this->updateSQL($sql, $aBrowserValues) > 0 ) {
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getAllVersions()
    {
        $responseArray =    array();
        $sSql =             " SELECT idVersion FROM " . $this->tableName() . " fv WHERE fv.counterProgress > 0 ";
        $dataReader =       $this->querySQL($sSql);
        $rows =             $dataReader->readAll();
        foreach ($rows as $row){
            $responseArray[$row['idVersion']] = $row['idVersion'];
        }
        return $responseArray;
    }

    /**
     * @return int
     */
    public function getMaxVersion()
    {

        $idVersion = 100;

        $sSql = "
            SELECT MAX(idVersion) AS idVersion
        ";

        $sSql .= "
            FROM " . $this->tableName() . "
        ";

        $dataReader = $this->querySQL($sSql);

        if (($row = $dataReader->read()) !== false) {
            $idVersion = $row["idVersion"];
        }

        return $idVersion;
    }

}



