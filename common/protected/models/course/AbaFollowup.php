<?php
/**
 *
 * @property integer id
 * @property mixed themeid
 *
 * @property  integer sectionAbaFilm  (sit_por)
 * @property  integer sectionSpeak  (stu_por)
 * @property  integer sectionWrite  (dic_por)
 * @property  integer sectionInterpret (rol_por)
 * @property  integer sectionVideoclass  (gra_vid)
 * @property  integer sectionExercises  (wri_por)
 * @property  integer sectionVocabulary  (new_por)
 * @property  integer sectionEvaluation  (eva_por)
 * @property  integer markEvaluation
 *
 * @property  mixed all_por
 * @property  mixed all_ans
 * @property  mixed all_err
 * @property  mixed all_time
 * @property  mixed dic_ans
 * @property  mixed dic_err
 * @property  mixed exercises
 * @property  mixed gra_ans
 * @property  mixed gra_err
 * @property  mixed rol_on
 * @property  mixed spe_ans
 * @property  mixed time_aux
 * @property  mixed userid
 * @property  mixed wri_ans
 * @property  mixed wri_err
 */
class AbaFollowup extends AbaActiveRecord
{
    private $unitProgress; //Full Unit progress with EXAM
    private $sectionsProgress; //Full Unit progress NO EXAM (without evaluation)
    public $markEvaluation;

    private $colsSections = array('abaFilm'=>'sit_por',
                                    'speak'  =>'stu_por' ,
                                    'write'  =>'dic_por' ,
                                    'interpret'=>'rol_por' ,
                                    'videoclass'=>'gra_vid' ,
                                    'exercises'=> 'wri_por' ,
                                    'vocabulary'=> 'new_por' ,
                                    'assessment'=> 'eva_por' ,);

    private $tableFollow = "";
    private $userId = "";

    /**
     * @param null $scenario
     *
     * @param null $dbConnName
     */
    public function setConnection($scenario = null, $dbConnName = null) {
        parent::__construct($scenario, $dbConnName);
        $this->connection = Yii::app()->getComponent(AbaActiveRecord::DB_SLAVE);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName($userId = "")
    {
        if ($userId == "") {
            $userId = Yii::app()->user->getId();
        }

        if ($this->userId == "") {
            $this->userId = $userId;
        } elseif ($this->userId != $userId) {
            Yii::log("different userIDs for AbaFollowup in the same session", CLogger::LEVEL_ERROR, 'itbugs');
            return  Yii::app()->params['dbCampus'].'.followup4'. $this->tableFollow;
        }

        if ($this->tableFollow == "") {
            $cache = $this->_getCache();
            $cacheKey = "user_dictionary.progressVersion." . $this->userId;

            if (false === $cache || false === ($this->tableFollow = $cache->get($cacheKey))) {

                $sql = "Select progressVersion from aba_b2c.user_dictionary where idUser=:USERID;";
                $sqlParameters = array(":USERID" => $this->userId);
                $dataReader = $this->querySQL($sql, $sqlParameters);
                if (($row = $dataReader->read()) !== false) {
                    $this->tableFollow = "_" . $row['progressVersion'];
                }

                if (false !== $cache) {
                    $cache->set($cacheKey, $this->tableFollow, 1800); //cache it for 30 minutes
                }
            }
        }

        return  Yii::app()->params['dbCampus'].'.followup4'. $this->tableFollow;
    }

    /**
     * @param integer   $unitId
     * @param integer $userId
     */
    protected function initDefaults($unitId, $userId = null)
    {
        $this->themeid = $unitId;
        if (!isset($userId)) {
            $userId= Yii::app()->user->getId();
        }
        $this->userid = $userId;
        $this->lastchange = "0000-00-00 00:00:00";
        $this->all_por = 0;
        $this->all_err = 0;
        $this->all_ans = 0;
        $this->all_time = 0;
        $this->sit_por = 0;
        $this->stu_por = 0;
        $this->dic_por = 0;
        $this->dic_ans = 0;
        $this->dic_err = 0;
        $this->rol_por = 0;
        $this->gra_por = 0;
        $this->gra_ans = 0;
        $this->gra_err = 0;
        $this->wri_por = 0;
        $this->wri_ans = 0;
        $this->wri_err = 0;
        $this->new_por = 0;
        $this->spe_ans = 0;
        $this->time_aux = 0;
        $this->gra_vid = 0;
        $this->rol_on = 0;
        $this->exercises = "0,0,0,0,0,0,0,0,0,0";
        $this->eva_por = 0;
        $this->unitProgress = 0;
        $this->sectionsProgress = 0;
        $this->sectionVideoclass = 0;
    }

    /** Returns the summary of columns and formula of computing the progress in SQL.
     * @param bool $withExam
     *
     * @return string
     */
    protected function getSQLComputeFullProgress($withExam = true)
    {
        $numTabs = 8;
        $strExamColumn = "+ IF((f.eva_por*10)>=80,100,0) ";
        if (!$withExam) {
            $numTabs = 7;
            $strExamColumn = " ";
        }
        $sqlFields = "FLOOR(f.sit_por+f.stu_por+f.dic_por+f.rol_por+f.gra_vid+f.wri_por+f.new_por$strExamColumn)/".
            $numTabs;
        return $sqlFields;
    }

    /** SQL criteria to find out if a VideoClass has been watched or not.
     * @return string
     */
    protected function getSQLVideoclassProgress()
    {
        return ' IF(f.gra_vid>0, 100, 0) ';
    }

    /** It computes each section progress, the full sections progress and the unit progress (with exam)
     * and updates protected fields.
     *
     * @param Array $aUnitSections
     *
     * @return array
     */
    protected function computeSectionsProgress($aUnitSections)
    {
        $this->sectionAbaFilm = $aUnitSections['sit_por'];
        $this->sectionSpeak = $aUnitSections['stu_por'];
        $this->sectionWrite = $aUnitSections['dic_por'];
        $this->sectionInterpret = $aUnitSections['rol_por'];
        $this->sectionVideoclass = $aUnitSections['gra_vid'];
        $this->sectionExercises = $aUnitSections['wri_por'];
        $this->sectionVocabulary = $aUnitSections['new_por'];
        $this->markEvaluation = $aUnitSections['eva_por'];
        $aUnitSections['eva_por'] = $aUnitSections['eva_por'] * 10;
        if ($aUnitSections['eva_por'] >= 80) {
            $aUnitSections['eva_por'] = 100;
        } else {
            $aUnitSections['eva_por'] = 0;
        }
        $this->sectionEvaluation = $aUnitSections['eva_por'];

        // Total Sections Progress (NO EVALUATION):
        $this->updateSectionsProgress($aUnitSections);

        // Total Unit Progress (WITH EXAMS / EVALUATION):
        $this->updateUnitProgress($aUnitSections);

        $this->all_por = $this->unitProgress;
        $aUnitSections['all_por'] = $this->unitProgress;
        return $aUnitSections;
    }

    /** Returns all percentage columns rounded to 100
     * @param array $aUnitAllCols
     * @return array
     */
    protected function roundTo100($aUnitAllCols)
    {
        foreach ($aUnitAllCols as $key => $value) {
            if ($aUnitAllCols[$key] > 100 &&
                $key != 'id' &&
                $key != 'themeid' &&
                $key != 'userid' &&
                $key != 'lastchange' &&
                $key != 'exercises') {
                $aUnitAllCols[$key] = 100;
            }
        }

        return $aUnitAllCols;
    }

    /** It computes all percentage values, parses and round up numbers.
     * It returns the same elements but modified.
     *
     * @param array $aUnitAllCols
     * @return array
     */
    protected function parseColumns($aUnitAllCols)
    {
        $aUnitAllCols = $this->computeSectionsProgress($aUnitAllCols);
        $aUnitAllCols = $this->roundTo100($aUnitAllCols);
        return $aUnitAllCols;
    }

    /**
     * @param integer $userId
     * @param Integer $unitId
     *
     * @return AbaFollowup|bool
     * @throws CDbException
     */
    public function getFollowupByIds($userId = null, $unitId = null)
    {
        if (!isset($userId)) {
            $userId = Yii::app()->user->getId();
        }
        if (is_null($unitId)) {
            throw new CDbException(" To be able to load a FollowUp an user Id and Unit Id is a must. ");
        }
        $this->initDefaults($unitId, $userId);
        $sql = " SELECT uc.* FROM  " . $this->tableName($userId) .
            " uc WHERE uc.userid=:USERID AND uc.themeid=:UNITID ; ";
        $aBrowserValues = array(":USERID" => $userId, ":UNITID" => $unitId);
        $dataReader = $this->querySQL($sql, $aBrowserValues);
        if (($row = $dataReader->read()) !== false) {
            $row = $this->parseColumns($row);
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /** Returns the MAX percentage of progress for all $aUnits for $UserId
     * @param integer $userId
     * @param array $aUnits
     *
     * @return integer
     */
    public function getMaxFollowUp($userId, array $aUnits)
    {
        $units = implode(',', $aUnits);
        $sql = ' SELECT MAX(' . $this->getSQLComputeFullProgress(true) . ') as `unitProgress`
                    FROM ' . $this->tableName($userId) . ' f
                    WHERE f.`userid`=:USERID AND f.`themeid` IN (' . $units . ') GROUP BY f.userid; ';
        $dataReader = $this->querySQL($sql, array(':USERID' => $userId));
        if ($this->readRowFillDataCols($dataReader)) {
            return intval($this->unitProgress);
        }
        return 0;
    }

    /**
     * @param $userId
     * @param array $aUnits
     *
     * @return int
     */
    public function getMaxFollowUpCron($userId, array $aUnits)
    {
        if (trim($userId) != "") {
            $units = implode(',', $aUnits);
            $sql = ' SELECT MAX(' . $this->getSQLComputeFullProgress(true) . ') as `unitProgress`
                    FROM ' . $this->tableName($userId) . ' f
                    WHERE f.`userid`=:USERID AND f.`themeid` IN (' . $units . ') GROUP BY f.userid; ';

            $dataReader =   $this->querySQL($sql, array(':USERID' => $userId));
            $row =          $dataReader->read();
            if ($row !== false) {
                if (isset($row['unitProgress'])) {
                    return intval($row['unitProgress']);
                }
            }
        }
        return 0;
    }

    /** Returns first unit the user has not completed 100% progress
     * First unit not completed or not accessed
     *
     * @param integer $userId
     * @param integer $userLevel
     *
     * @return integer|bool
     */
    public function getFirstUnitNotProgressCompleted($userId, $userLevel)
    {
        $moUnits = new UnitsCourses();
        if ($userLevel > MAX_ENGLISH_LEVEL) {
            return $moUnits->getStartUnitByLevel(MAX_ENGLISH_LEVEL);
        }
        $maxUnitLevel = $moUnits->getEndUnitByLevel($userLevel);

        $sql = "SELECT f.`themeid`, (" . $this->getSQLComputeFullProgress(true) . ") as unitProgress
                  FROM " . $this->tableName($userId) . " f
                  WHERE f.userId=:USERID
                            AND f.`themeid`>=" . ($moUnits->getStartUnitByLevel($userLevel)) . "
                            AND f.`themeid`<=" . $maxUnitLevel . "
                  ORDER BY f.themeid ASC;";

        $aBrowserValues = array(":USERID" => $userId);
        $dataRows = $this->queryAllSQL($sql, $aBrowserValues);
        if ($dataRows !== false) {
            $allUnitsWithProgress = array();
            foreach ($dataRows as $row) {
                $allUnitsWithProgress[$row["themeid"]] = $row["unitProgress"];
            }
            for ($unitId = $moUnits->getStartUnitByLevel($userLevel); $unitId <= $maxUnitLevel; $unitId++) {
                if (array_key_exists($unitId, $allUnitsWithProgress)) {
                    if ($allUnitsWithProgress[$unitId] < 100) {
                        return $unitId;
                    }
                } else {
                    return $unitId;
                }
            }
        }

//        return $this->getLastUnitProgressCompleted($userId, $userLevel);
        if (count($dataRows) == UNITS_PER_LEVEL) {
            // case all 24 are finished
            return $this->getFirstUnitNotProgressCompleted($userId, $userLevel + 1);
        }

        return $moUnits->getStartUnitByLevel($userLevel);


    }

    /** Searches the last unit completed and gets the following one. If no unit
     * completed then it searches the first one for the level being.
     *
     * @param $userId
     * @param $userLevel
     * @return int
     */
    public function getLastUnitProgressCompleted($userId, $userLevel)
    {
        $moUnits = new UnitsCourses();

        $sql = "SELECT f.`themeid`, ROUND(" . $this->getSQLComputeFullProgress(true) . ",0) as unitProgress
                  FROM " . $this->tableName($userId) . " f
                  WHERE f.userId=:USERID
                            AND f.`themeid`>=" . ($moUnits->getStartUnitByLevel($userLevel)) . "
                            AND f.`themeid`<=" . HeUnits::getMaxUnits() . "
                  HAVING unitProgress>=100
                  ORDER BY f.themeid DESC
                LIMIT 1 ";

        $aBrowserValues = array(":USERID" => $userId);
        $dataReader = $this->querySQL($sql, $aBrowserValues);
        if (($row = $dataReader->read()) !== false) {
            if ($row['unitProgress'] > 100) {
                $row['unitProgress'] = 100;
            }
            $this->fillDataColsToProperties($row);
            return (intval($row["themeid"]) + 1);
        } else {
            return $moUnits->getStartUnitByLevel($userLevel);
        }
    }

    /** Searches the last unit the user made some progress. If no unit
     * with any progress, return false
     *
     * @param $userId
     * @return int
     */
    public function getLastUnitProgressIncreased($userId)
    {
        $sql = "SELECT f.`themeid`, ROUND(" . $this->getSQLComputeFullProgress(true) . ",0) as unitProgress
                  FROM " . $this->tableName() . " f
                  WHERE f.userId=:USERID AND f.all_por > 0
                  ORDER BY f.lastchange DESC
                LIMIT 1 ";

        $aBrowserValues = array(":USERID" => $userId);
        $dataReader = $this->querySQL($sql, $aBrowserValues);
        if (($row = $dataReader->read()) !== false) {
            if ($row['unitProgress'] > 100) {
                $row['unitProgress'] = 100;
            }
            $this->fillDataColsToProperties($row);
            return (intval($row["themeid"]));
        } else {
            return false;
        }
    }


    /** Returns the number of units completed for the level of the user.
     *
     * @param integer $idUser
     * @param integer $userLevel
     * @return int
     */
    public function getUnitsCompletedOnLevel($idUser, $userLevel)
    {
        $moUnits = new UnitsCourses();

        $sql = "SELECT f.`themeid`, ROUND(".$this->getSQLComputeFullProgress(true).",0) as unitProgress
                  FROM " . $this->tableName($idUser) . " f
                  WHERE f.userId=:USERID
                            AND f.`themeid`>=".($moUnits->getStartUnitByLevel($userLevel))."
                            AND f.`themeid`<=".($moUnits->getEndUnitByLevel($userLevel))."
                  HAVING unitProgress>=100 ";

        $aBrowserValues = array(":USERID"=> $idUser);
        $dataReader = $this->queryAllSQL($sql, $aBrowserValues);
        if (!$dataReader) {
            return 0;
        } else {
            return count($dataReader);
        }
    }


    /** It returns the first videoclass number that has not been watched by the user.
     * It starts searching on the first unit from the user current level upwards. If everything is 100% it will return
     * the last one: 144.
     *
     * @param integer $userId
     * @param integer $userLevel
     * @return int
     */
    public function getFirstVideoclassNotWatched($userId, $userLevel)
    {
        $moUnits = new UnitsCourses();
        if ($userLevel > MAX_ENGLISH_LEVEL) {
            return $moUnits->getEndUnitByLevel(MAX_ENGLISH_LEVEL);
        }

        $lastUnitCheck = 1;
        $maxUnitLevel = $moUnits->getEndUnitByLevel(MAX_ENGLISH_LEVEL);
        $sql = "SELECT f.`themeid`, (" . $this->getSQLVideoclassProgress() . ") as videoclassProgress
                  FROM " . $this->tableName($userId) . " f
                  WHERE f.userId=:USERID
                    AND f.`themeid`>=" . ($moUnits->getStartUnitByLevel($userLevel)) . "
                    AND f.`themeid`<=" . $maxUnitLevel . "
                  ORDER BY f.themeid ASC;";

        $aBrowserValues = array(":USERID" => $userId);
        $dataRows = $this->queryAllSQL($sql, $aBrowserValues);
        if ($dataRows !== false) {
            $aVideosProgress = array();

            foreach ($dataRows as $row) {
                $aVideosProgress[$row["themeid"]] = intval($row["videoclassProgress"]);
            }

            for ($unitId = $moUnits->getStartUnitByLevel($userLevel); $unitId <= $maxUnitLevel; $unitId++) {
                if (array_key_exists($unitId, $aVideosProgress)) {
                    if ($aVideosProgress[$unitId] < 100) {
                        return $unitId;
                    } else {
                        $lastUnitCheck = $unitId;
                    }
                } else {
                    return $unitId;
                }
            }
        }

        return $lastUnitCheck;
    }

    /**
     * Returns >0 if the user has started any exercise or activity in any unit.
     * @param $userId
     *
     * @return bool|int
     */
    public function getHasStartedCourse($userId)
    {
        $sql = " SELECT count(*) as `hasStarted` FROM  " . $this->tableName($userId) . " uc WHERE uc.userid=:USERID ;";
        $aBrowserValues = array(":USERID" => $userId);
        $dataReader = $this->querySQL($sql, $aBrowserValues);
        if (($row = $dataReader->read()) !== false) {
            return intval($row["hasStarted"]);
        }

        return false;
    }

    /**
     * @param null $userId
     * @param null $unitId
     *
     * @return bool|datetime YYYY-MM-DD HH:NN
     */
    public function getUnitStartDate($idUser=null, $unitId=null)
    {
        /*
         * -- First time a unit was accessed. No need to have done some exercise
        */
        $sql = " SELECT  DATE_FORMAT(f.lastchange,'%d-%m-%Y') as 'unitStartDate' FROM " . $this->tableName($idUser) . " f
                    WHERE f.themeid=:UNITID AND f.userId=:USERID
                    ORDER BY f.lastchange ASC LIMIT 1; " ;

        $aBrowserValues = array(":USERID"=> $idUser, ":UNITID"=> $unitId );

        $dataReader=$this->querySQL($sql, $aBrowserValues);
        if (($row = $dataReader->read())!==false) {
            return $row["unitStartDate"];
        }
        return false;
    }

    /**
     * @param integer $userId
     * @param integer $unitId
     *
     * @return bool|integer
     */
    public function getDaysOnUnit($idUser, $unitId)
    {
        $sql = " SELECT count(distinct(DATE(f.lastchange))) as 'daysOnUnit'
                    FROM " . $this->tableName($idUser) . " f
                    WHERE f.`userid`=".$idUser." AND f.`themeid`=:UNITID
                    ORDER BY f.`lastchange` ASC; ";
        $aBrowserValues = array(":UNITID"=> $unitId );

        $dataReader=$this->querySQL($sql, $aBrowserValues);
        if (($row = $dataReader->read())!==false) {
            return $row["daysOnUnit"];
        }

        return false;

    }

    /**
     * @param integer $userId
     * @param integer $unitId
     *
     * @return bool|integer Number of minutes the user has spent on this unit
     */
    public function getTimeSpentOnUnit($userId, $unitId)
    {
        /*
          @TODO: Incorrect, all_time is not right. Should face up refactor some day:
        */
        $sql = "SELECT f.`all_time` FROM ".$this->tableName($userId)." f WHERE f.`userid`=".$userId." AND f.`themeid`=:UNITID";
        $aBrowserValues = array(":UNITID"=> $unitId );

        $dataReader=$this->querySQL($sql, $aBrowserValues);
        if(($row = $dataReader->read())!==false)
        {
            return $row["all_time"];
        }

        return false;
    }

    /** Returns unit progress with evaluation included,
     * if we only want the sections with no evaluation included then we
     * set false withExam parameter.
     *
     * @param bool $withExam
     * @return mixed
     */
    public function getUnitProgress($withExam=true)
    {
        if($withExam){
            return $this->unitProgress;
        } else{
            return $this->sectionsProgress;
        }
    }

    /**
     * @return integer
     */
    public function getVideoClassProgress()
    {
        return $this->sectionVideoclass;
    }

    /** Updates the column Video class progress. Used mainly from the web service Rest UpdateFollowup
     * @param integer $graVid
     *
     * @return bool
     */
    public function updateVideoclassProgress( $graVid )
    {
        // @TODO: We should replace this properties for the good ones:$this->sit_por-->>>   $this->sectionAbaFilm
        $allPor = ($this->sit_por + $this->stu_por + $this->dic_por + $this->rol_por + $graVid +
                $this->wri_por + $this->new_por) / 7;

        $sql = " UPDATE ".$this->tableName($this->userid)." f SET `lastchange`=NOW(), f.gra_vid = :GRAVID, f.all_por = $allPor
                    WHERE f.themeid=".$this->themeid." AND f.`userid`=".$this->userid."";
        $params = array( ":GRAVID"=>$graVid );
        if ($this->updateSQL( $sql, $params) > 0) {
            $this->gra_vid = $graVid;
            return true;

        }

        return false;
    }

    /**
     * @param integer $userId
     * @param integer $unitId
     * @param integer $graVid
     *
     * @return bool|int
     */
    public function insertVideoclassProgress( $idUser, $unitId, $graVid )
    {
        $sql= ' INSERT INTO ' . $this->tableName($idUser) . '  (`themeid`, `userid`, `lastchange`, `gra_vid`)
                VALUES( :UNITID, :USERID, NOW(), :GRAVID) ' ;
        $aBrowserValues = array( ":UNITID"=>$unitId, ":USERID"=>$idUser, ":GRAVID"=>$graVid );
        $this->id = $this->executeSQL($sql, $aBrowserValues, false);
        if ($this->id > 0) {
            return $this->id;
        } else {
            return false;
        }
    }

    /** Returns the first section not completed by the user. Used mainly in the Home page, to find out
     * which section user should focus on.
     *
     * @return array
     *
     * @throws CException
     */
    public function getFirstSectionNotCompleted()
    {
        $iSection=0;
        // We are looking for the first section not completed:
        foreach ($this->colsSections as $property) {
            $iSection=$iSection+1;
            if ($this->$property < 100) {
                break;
            }
            // In case of Assessment section, if is less than 80% (8 out of 10 questions) then user must finish it:
            if ($property == 'eva_por' && $this->$property < 80) {
                break;
            }

            if ($iSection == 8) {
                return array(1, intval($this->$property) );
            }
        }

        return array($iSection, intval($this->$property) );
    }

    public function updateField($field, $value)
    {
        $sql = "UPDATE ".$this->tableName($this->userid)." f SET `lastchange`=NOW(), f.".$field." = '" .$value. "' WHERE f.themeid=".$this->themeid." AND f.`userid`=".$this->userid."";
        $dataReader = $this->updateSQL($sql);
        if ($dataReader) {
            return true;
        }
        return false;
    }

    /**
     * Calculates the percentage of Course total progress for the User
     *
     * @param integer $idUser
     * @param $levelId
     *
     * @return array containing a key,value pair where the value is an integer that is the rounded percentage
     */
    public function getUserTotalProgress($idUser, $levelId = 0)
    {
        if ($levelId == 0) {
            $startUnit = HeUnits::getStartUnitByLevel(1);
            $endUnit = HeUnits::getEndUnitByLevel(count(Yii::app()->params['levels']));
            $divisor= HeUnits::getMaxUnits();
        } else {
            $startUnit = HeUnits::getStartUnitByLevel($levelId);
            $endUnit = HeUnits::getEndUnitByLevel($levelId);
            $divisor= $endUnit-$startUnit+1;
        }
        $sql = ' SELECT SUM(' . $this->getSQLComputeFullProgress(true) . ')/'.$divisor.' AS overall_progress
                    FROM ' . $this->tableName($idUser) . ' f
                    WHERE f.`userid`=:idUser_param AND f.`themeid` BETWEEN '. $startUnit .' AND '. $endUnit .'; ';
        $params = array(":idUser_param" => $idUser );
        $dataReader=$this->querySQL($sql, $params, $this->selectDbConnection());
        if (($row = $dataReader->read())!==false) {
            return array('OverallProgress' => intval($row["overall_progress"]));
        }
        return false;
    }

    /**
     * Returns the progress percentages for all sections for the given user/level combination
     *
     * @param integer $idUser
     * @param integer $idLevel
     *
     * @return array JSONArray type containing all relevant section progress percentages
     */
    public function getLevelSectionsProgress($idUser, $idLevel)
    {
        $responseArray = array();
        $sql = "SELECT f.themeid, f.lastchange, f.all_por, f.sit_por, f.stu_por, f.dic_por, f.rol_por, f.wri_por, ".
            "f.new_por, f.gra_vid, f.eva_por, f.rol_on, f.spe_por FROM " . $this->tableName($idUser) .
            " f WHERE f.userid=:idUser_param AND f.themeid BETWEEN " . HeUnits::getStartUnitByLevel($idLevel) .
            " AND " . HeUnits::getEndUnitByLevel($idLevel) . ";";
        $params = array(":idUser_param"=> $idUser );
        $dataReader=$this->querySQL($sql, $params, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        foreach ($rows as $row) {
            $row = $this->computeSectionsProgress($row);
            $responseArray[] = array('IdUnit'=>$row['themeid'], 'LastChange'=>$row['lastchange'],
                'TotalUnitPct'=>intval($row['all_por']), 'AbaFilmPct'=>$row['sit_por'],
                'SpeakPct'=>$row['stu_por'], 'WritePct'=>$row['dic_por'], 'InterpretPct'=>$row['rol_por'],
                'VideoClassPct'=>$row['gra_vid'], 'ExercisesPct'=>$row['wri_por'], 'VocabularyPct'=>$row['new_por'],
                'AssessmentPct'=>($row['eva_por']));
        }
        return $responseArray;
    }

    /**
     * Returns the progress percentages for all sections for the given user
     *
     * @param integer $idUser
     *
     * @return array JSONArray type containing all relevant section progress percentages
     */
    public function getCourseSectionsProgress($idUser)
    {
        $responseArray = array();
        $sql = "SELECT f.themeid, f.lastchange, f.all_por, f.sit_por, f.stu_por, f.dic_por, f.rol_por, f.wri_por, ".
            "f.new_por, f.gra_vid, f.eva_por, f.rol_on, f.spe_por FROM " . $this->tableName($idUser) .
            " f WHERE f.userid=:idUser_param";
        $params = array(":idUser_param"=> $idUser );
        $dataReader=$this->querySQL($sql, $params, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        foreach ($rows as $row) {
            $row = $this->computeSectionsProgress($row);
            $responseArray[] = array('IdUnit'=>$row['themeid'], 'LastChange'=>$row['lastchange'],
                'TotalUnitPct'=>intval($row['all_por']), 'AbaFilmPct'=>$row['sit_por'], 'SpeakPct'=>$row['stu_por'],
                'WritePct'=>$row['dic_por'], 'InterpretPct'=>$row['rol_por'], 'VideoClassPct'=>$row['gra_vid'],
                'ExercisesPct'=>$row['wri_por'], 'VocabularyPct'=>$row['new_por'], 'AssessmentPct'=>($row['eva_por']));
        }
        return $responseArray;
    }

    public function insertRecord($idUser, $unitId)
    {
        $sql= ' INSERT INTO '.$this->tableName($idUser).' (`themeid`, `userid`, `lastchange`) '.
            'VALUES( :UNITID, :USERID, NOW())';
        $aBrowserValues = array( ":UNITID"=>$unitId, ":USERID"=>$idUser);

        $this->id = $this->executeSQL($sql, $aBrowserValues, false);
        if ($this->id > 0) {
            $this->userid=$idUser;
            $this->themeid=$unitId;
            return $this->id;
        } else {
            return false;
        }
    }

    /**
     * Function used to update a progress percentage entry en followup4 for a given user/unit combination
     *
     * @param integer $idUser the user we want to update
     * @param integer $unit the unit we want to update
     * @param string $campo the DB field we want to update
     * @param integer $valor_por The percentage value we want to update to the database
     * @return array JSONArray type containing all relevant section progress percentages for the unit updated
     * @throws CDbException
     */
    public function updatePercent($idUser, $unit, $campo = null, $valor_por = null)
    {
        $sql = "SELECT sit_por,stu_por,dic_por,rol_por,gra_por,wri_por,new_por,eva_por,gra_vid,rol_on,all_por, lastchange " .
            " FROM " . $this->tableName($idUser) ." WHERE userid=$idUser AND themeid=$unit; ";
        $dataReader = $this->querySQL($sql);
        $rowTmp = $dataReader->read();

        if (empty($rowTmp)) {
            $this->insertRecord($idUser, $unit);
            $dataReader = $this->querySQL($sql);
            $rowTmp = $dataReader->read();
        }
        $row = $rowTmp;

        if (!empty($row)) {
            //Este control es por que el coursegen genera mal algunos contadores de ejercicios y el resultado es mayor de 100
            if ($valor_por > 100) {
                $valor_por = 100;
            }

            $update = false;
            if ($campo != null && $row[$campo] < $valor_por) {
                $update = true;
                $row[$campo] = $valor_por;
            }

            $this->updateUnitProgress($row);
            $this->updateSectionsProgress($row);

            if ($update) {
                $sql = " UPDATE " . $this->tableName($idUser) . " SET $campo=$valor_por, all_por=".$this->sectionsProgress.", lastchange=now()" .
                  " WHERE userid=$idUser AND themeid=$unit";

                $this->updateSQL($sql);
            }

            $arrayResponse = array("IdUnit"=>$unit ,"AbaFilmPct" => $row['sit_por'], 'SpeakPct' => $row['stu_por'], 'WritePct' => $row['dic_por'], 'InterpretPct' => $row['rol_por'], 'VideoClassPct' => $row['gra_vid'], 'ExercisesPct' => $row['wri_por'], 'VocabularyPct' => $row['new_por'], 'AssessmentPct' => ($row['eva_por']*10),'Total' => $this->sectionsProgress, 'totalWithEval' => $this->unitProgress, 'lastchange' => $row['lastchange']);
            return $arrayResponse;
        }
    }

    public function updateCount($idUser, $unit, $section, $type)
    {
        $post_fix = '';
        if ($type == 'err') {
            $campo = strtolower(substr_replace($section, '', 3).'_err');
            $post_fix = $type;
        } elseif ($type == 'ans') {
            $campo = strtolower(substr_replace($section, '', 3).'_ans');
            $post_fix = $type;
        }
        $sql = "SELECT * FROM " . $this->tableName($idUser) . " WHERE userid=$idUser AND themeid=$unit";

        $dataReader = $this->querySQL($sql);
        $rowTmp = $dataReader->read();

        if (empty($rowTmp)) {
            $this->insertRecord($idUser, $unit);
            $dataReader = $this->querySQL($sql);
            $rowTmp = $dataReader->read();
        }
        $row = $rowTmp;

        if (!empty($row))
        {
            //MMCC - calculamos el n?mero de errores en la secci?n
            $row[$campo] = $row[$campo] + 1;
            $valor = $row[$campo];
            //MMCC - calculamos el n?mero de errores en total
            $row["all_$post_fix"] = $row["all_$post_fix"] + 1;
            $valor_all= $row["all_$post_fix"];
            //MMCC - y los ponemos los nuevos valores en la tabla
            $sql = "UPDATE " . $this->tableName($idUser) . " SET $campo=$valor, all_$post_fix = $valor_all, lastchange=now() WHERE userid=$idUser AND themeid=$unit";
            $this->updateSQL($sql) > 0;
        }

        return $this->readableFollowUp($row);
    }

    public function readableFollowUp($attributeArray)
    {
        $jsonResponse = array(
            'IdUnit' => $attributeArray['themeid'],
            'LastChanged' => $attributeArray['lastchange'],
            'TotalUnitPct' => $attributeArray['all_por'],
            'TotalErrors' => $attributeArray['all_err'],
            'AllAnswers' => $attributeArray['all_ans'],
            'AllTime' => $attributeArray['all_time'],
            'AbaFilmPct' => $attributeArray['sit_por'],
            'SpeakPct' => $attributeArray['stu_por'],
            'WritePct' => $attributeArray['dic_por'],
            'WriteHelpNumber' => $attributeArray['dic_ans'],
            'WriteErrors' => $attributeArray['dic_err'],
            'InterpretPct' => $attributeArray['rol_por'],
            'ExercisesPct' => $attributeArray['wri_por'],
            'ExerciseHelpNumber' => $attributeArray['wri_ans'],
            'ExerciseErrors' => $attributeArray['wri_err'],
            'VocabularyPct' => $attributeArray['new_por'],
            'VideoClassPct' => $attributeArray['gra_vid'],
            'AssessmentPct' => ($attributeArray['eva_por']),
        );
        return $jsonResponse;
    }

    public function progressTableIsPartitioned($idUser)
    {
        $sql = "Select progressVersion from user_dictionary where idUser=:USERID AND progressVersion >= 30;";
        $sqlParameters = array(":USERID" => $idUser);
        $dataReader = $this->querySQL($sql, $sqlParameters);
        if (($dataReader->read()) !== false) {
            return true;
        }
        return false;
    }

    public function getProgressSummary($dateStart, $iHours=2, $dbConnection = null)
    {
        if (!is_numeric($iHours)) {
            $iHours = Yii::app()->config->get("FOLLOWUP_CRON_PROCESS_SUMMARY_HOURS");
        }

        $endNow =       HeDate::now();
        $startDate =    HeDate::getDateTimeAdd($endNow, (-1 * $iHours), HeDate::HOURS);
        $startDate =    HeDate::getDateTimeAdd($startDate, -1, HeDate::MINUTES);
        $abaFollowUpVersion =   new AbaFollowUpVersion();
        $allVersions =          $abaFollowUpVersion->getAllVersions();
        $responseArray = array();
        ini_set('memory_limit', '4096m');
        $sSql = " SELECT 0 AS progressVersion, f.id AS idFollowup, themeid, userid, lastchange, all_por, all_err, all_ans, all_time, sit_por, stu_por, dic_por, dic_ans, dic_err, rol_por, gra_por, gra_ans, ";
        $sSql .= " gra_err, wri_por, wri_ans, wri_err, new_por, spe_por, spe_ans, time_aux, gra_vid, rol_on, exercises, eva_por ";
        $sSql .= " FROM aba_b2c.followup4 f ";
        $sSql .= " WHERE f.lastchange >= :LASTCHANGE ";
        $params = array(":LASTCHANGE" => $startDate );
        // !!! SLAVE !!!
        $dataReader =   $this->querySQL($sSql, $params, $dbConnection);
        $rows =         $dataReader->readAll();
        foreach ($rows as $row) {
            $responseArray[] = $row;
        }
        foreach ($allVersions as $iKey => $iVersion) {
            $sSql =     " SELECT " . ((int) $iVersion) . " AS progressVersion, f.id AS idFollowup, themeid, userid, lastchange, all_por, all_err, all_ans, all_time, sit_por, stu_por, dic_por, dic_ans, dic_err, rol_por, gra_por, gra_ans, ";
            $sSql .=    " gra_err, wri_por, wri_ans, wri_err, new_por, spe_por, spe_ans, time_aux, gra_vid, rol_on, exercises, eva_por ";
            $sSql .=    " FROM aba_b2c.followup4_" . $iVersion . " f ";
            $sSql .=    " WHERE f.lastchange >= :LASTCHANGE ";
            $params =   array(":LASTCHANGE" => $startDate );
            //
            // !!! SLAVE !!!
            $dataReader = $this->querySQL($sSql, $params, $dbConnection);
            $rows = $dataReader->readAll();
            foreach ($rows as $row) {
                $responseArray[] = $row;
            }
        }
        return $responseArray;
    }

    /**
     * @return int
     */
    public function getTotalProgressSummary($dbConnection){
        $sSql =     " SELECT COUNT(*) AS total ";
        $sSql .=    " FROM aba_b2c.`followup4` f ";
        // !!! SLAVE !!!
        $dataReader = $this->querySQL($sSql, array(), $dbConnection);
        if (($row = $dataReader->read()) !== false) {
            return intval($row["total"]);
        }
        return 0;
    }

    /**
     * @param array $limit
     *
     * @return array
     */
    public function getLimitProgressSummary($limit = array(0, 10000), $dbConnection)
    {
        $responseArray = array();
        ini_set('memory_limit', '4096m');
        $sSql =     " SELECT 0 AS progressVersion, f.id AS idFollowup, themeid, userid, lastchange, all_por, all_err, all_ans, all_time, sit_por, stu_por, dic_por, dic_ans, dic_err, rol_por, gra_por, gra_ans, ";
        $sSql .=    " gra_err, wri_por, wri_ans, wri_err, new_por, spe_por, spe_ans, time_aux, gra_vid, rol_on, exercises, eva_por ";
        $sSql .=    " FROM aba_b2c.`followup4` f ";
        $sSql .=    " LIMIT :START, :LIMIT ";
        $params =   array(":START" => $limit[0], ":LIMIT" => $limit[1]);

        $dataReader = $this->querySQL($sSql, $params, $dbConnection);
        $rows = $dataReader->readAll();
        foreach ($rows as $row) {
            $responseArray[] = $row;
        }

        return $responseArray;
    }

    /**
     * @param array $limit
     *
     * @return bool
     */
    public function setLimitProgressSummary($limit=array(0, 10000)){

        ini_set('memory_limit', '4096m');

        $sSql =         " INSERT INTO " . Yii::app()->params["dbCampusSummary"] . ".`followup4_summary` ";
        $sSql .=        " (progressVersion, idFollowup, themeid, userid, lastchange, all_por, all_err, all_ans, all_time, sit_por, stu_por, dic_por, dic_ans, dic_err, rol_por, gra_por, gra_ans, gra_err, wri_por, wri_ans, wri_err, new_por, spe_por, spe_ans, time_aux, gra_vid, rol_on, exercises, eva_por) ";
        $sSql .=        " SELECT 0 AS progressVersion, f.id AS idFollowup, themeid, userid, lastchange, all_por, all_err, all_ans, all_time, sit_por, stu_por, dic_por, dic_ans, dic_err, rol_por, gra_por, gra_ans, ";
        $sSql .=        " gra_err, wri_por, wri_ans, wri_err, new_por, spe_por, spe_ans, time_aux, gra_vid, rol_on, exercises, eva_por ";
        $sSql .=        " FROM aba_b2c.`followup4` f ";
        $sSql .=        " LIMIT " . (int) $limit[0] . ", " . (int) $limit[1] . " ";
        $connection =   Yii::app()->db;
        $command =      $connection->createCommand($sSql);
        $dataReader =   $command->query();
        return true;
    }

    /**
     * Returns the quantity of video classes watched by specified user
     * @param int $userId
     *
     * @return int
     */
    public function getNumberOfVideosWatched($userId)
    {
        $sql = "SELECT count(f.id) as `videosWatched` FROM followup4 f WHERE f.`userid`=:USERID AND f.`gra_vid`>=100 ";
        $params = array( ":USERID"=>$userId );
        $dataReader=$this->querySQL($sql, $params);
        $row = $dataReader->read();
        if ($row !== false) {
            return intval($row["videosWatched"]);
        }
        return 0;
    }

    /**
     * Updates Total Progress (with evaluation)
     * @param $sections
     */
    private function updateUnitProgress($sections)
    {
        $sumSections = $sections['sit_por'] + $sections['stu_por'] + $sections['dic_por'] + $sections['rol_por'] + $sections['gra_vid'] + $sections['wri_por'] + $sections['new_por'] + $sections['eva_por'] * 10;
        $this->unitProgress = intval(floor($sumSections / 8));

        if ($this->unitProgress > 100) {
            $this->unitProgress = 100;
        }
    }

    /**
     * Updates Total Progress (without evaluation)
     * @param $sections
     */
    private function updateSectionsProgress($sections)
    {
        $sumSections = $sections['sit_por'] + $sections['stu_por'] + $sections['dic_por'] + $sections['rol_por'] + $sections['gra_vid'] + $sections['wri_por'] + $sections['new_por'];
        $this->sectionsProgress = intval(floor($sumSections / 7));

        if ($this->sectionsProgress > 100) {
            $this->sectionsProgress = 100;
        }
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Remove user progress tool
     *
     */

    /**
     * @param $sTableFollowup4
     * @param $userId
     *
     * @return bool|string
     */
    public function getAllUserProgressUnitsString($sTableFollowup4, $userId)
    {

        $sUnits = '';

        try {

            $paramsToSQL = array(
              ':USERID' => $userId,
            );

            $sSql = "
                SELECT DISTINCT fu.themeid
                FROM " . $sTableFollowup4 . " AS fu
                WHERE
                    fu.userid = :USERID
                ORDER BY fu.themeid ASC 
            ";

            $dataReader = $this->querySQL($sSql, $paramsToSQL);
            if (!$dataReader) {
                return false;
            }

            $aUnits = array();
            while (($row = $dataReader->read()) !== false) {
                $aUnits[] = $row['themeid'];
            }

            $sUnits = implode(",", $aUnits);
        } catch (Exception $e) {

            HeLogger::sendLog(
              "Remove progress tool ERROR",
              HeLogger::IT_BUGS,
              HeLogger::ERROR,
              "Exception in AbaFollowup->getAllUserProgressUnitsString: " . $e->getMessage()
            );
        }

        return $sUnits;
    }

    /**
     * @param $sTableFollowup4
     * @param $userId
     *
     * @return bool|int
     */
    public function deleteAllUserProgressF4($sTableFollowup4, $userId)
    {

        if(!is_numeric($userId)) {
            return false;
        }

        try {

            $paramsToSQL = array(
              ':USERID' => $userId,
            );

            $sSql = "
              DELETE FROM " . $sTableFollowup4 . "
              WHERE
                  userid=:USERID
            ";

            $deletedResult = $this->deleteSQL($sSql, $paramsToSQL);

            return $deletedResult;
        } catch (Exception $e) {

            HeLogger::sendLog(
              "Remove progress tool ERROR",
              HeLogger::IT_BUGS,
              HeLogger::ERROR,
              "Exception in AbaFollowup->deleteAllUserProgressF4: " . $e->getMessage()
            );
        }

        return false;
    }

    /**
     * @param $sTableFollowupHtml4
     * @param $userId
     *
     * @return bool|int
     */
    public function deleteAllUserProgressFhtml4($sTableFollowupHtml4, $userId)
    {

        if(!is_numeric($userId)) {
            return false;
        }

        try {

            $paramsToSQL = array(
              ':USERID' => $userId,
            );

            $sSql = "
              DELETE FROM " . $sTableFollowupHtml4 . "
              WHERE
                  userid=:USERID
            ";

            $deletedResult = $this->deleteSQL($sSql, $paramsToSQL);

            return $deletedResult;
        } catch (Exception $e) {

            HeLogger::sendLog(
              "Remove progress tool ERROR",
              HeLogger::IT_BUGS,
              HeLogger::ERROR,
              "Exception in AbaFollowup->deleteAllUserProgressFhtml4: " . $e->getMessage()
            );
        }

        return false;
    }

    /**
     * @param $userId
     *
     * @return bool|int
     */
    public function deleteAllUserProgressByWeek($userId)
    {

        if(!is_numeric($userId)) {
            return false;
        }

        try {

            $paramsToSQL = array(
              ':USERID' => $userId,
            );

            $sSql = "
              DELETE FROM aba_b2c_summary.users_progress_byweek
              WHERE
                  userId=:USERID
            ";

            $deletedResult = $this->deleteSQL($sSql, $paramsToSQL);

            return $deletedResult;
        } catch (Exception $e) {

            HeLogger::sendLog(
              "Remove progress tool ERROR",
              HeLogger::IT_BUGS,
              HeLogger::ERROR,
              "Exception in AbaFollowup->deleteAllUserProgressByWeek: " . $e->getMessage()
            );
        }

        return false;
    }

    /**
     *
     * Remove user progress tool
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

}
