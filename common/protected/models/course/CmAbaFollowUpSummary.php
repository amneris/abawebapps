<?php
/**
 * @property integer id
 * @property integer progressVersion
 * @property integer idFollowup
 * @property integer themeid
 * @property integer userid
 * @property string lastchange
 * @property integer all_por
 * @property integer all_err
 * @property integer all_ans
 * @property integer all_time
 * @property integer sit_por
 * @property integer stu_por
 * @property integer dic_por
 * @property integer dic_ans
 * @property integer dic_err
 * @property integer rol_por
 * @property integer gra_por
 * @property integer gra_ans
 * @property integer gra_err
 * @property integer wri_por
 * @property integer wri_ans
 * @property integer wri_err
 * @property integer new_por
 * @property integer spe_por
 * @property integer spe_ans
 * @property integer time_aux
 * @property integer gra_vid
 * @property integer rol_on
 * @property string exercises
 * @property integer eva_por
 */

class CmAbaFollowUpSummary extends AbaActiveRecord
{
    public $id;

    /**
     * @param null $scenario
     * @param null $dbConnName
     */
    public function __construct($scenario=NULL, $dbConnName=NULL) {
        parent::__construct($scenario, $dbConnName);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return Yii::app()->params['dbCampusSummary'] . '.followup4_summary';
    }

    /**
     * @return string
     */
    public function mainFields() {
        return "
            fs.`id`, fs.`progressVersion`, fs.`idFollowup`, fs.`themeid`, fs.`userid`, fs.`lastchange`, fs.`all_por`, fs.`all_err`, fs.`all_ans`,
            fs.`all_time`, fs.`sit_por`, fs.`stu_por`, fs.`dic_por`, fs.`dic_ans`, fs.`dic_err`, fs.`rol_por`, fs.`gra_por`, fs.`gra_ans`,
            fs.`gra_err`, fs.`wri_por`, fs.`wri_ans`, fs.`wri_err`, fs.`new_por`, fs.`spe_por`, fs.`spe_ans`, fs.`time_aux`, fs.`gra_vid`,
            fs.`rol_on`, fs.`exercises`, fs.`eva_por`
        ";
    }

    /**
     * @return array
     */
    public function rules()
    {
        return array(
            array('progressVersion',    'required'),
            array('idFollowup',  	    'required'),
            array('themeid',  	        'required'),
            array('userid',  	        'required'),
            array('id, progressVersion, idFollowup, themeid, userid, all_por, all_err, all_ans, all_time, sit_por, stu_por, dic_por, dic_ans, dic_err, rol_por, gra_por, gra_ans, gra_err, wri_por, wri_ans, wri_err, new_por, spe_por, spe_ans, time_aux, gra_vid, rol_on, eva_por', 'type', 'type' => 'integer', 'allowEmpty' => true),
            array('lastchange, exercises', 'type', 'type' => 'string', 'allowEmpty' => true),
        );
    }

    /**
     * @return bool|int|string
     */
    public function saveFollowup4summary( )
    {
        $this->beginTrans();

        $sql = "
            INSERT INTO  " . $this->tableName() . " " .
            " (
                `progressVersion`, `idFollowup`, `themeid`, `userid`, `lastchange`, `all_por`, `all_err`, `all_ans`,
                `all_time`, `sit_por`, `stu_por`, `dic_por`, `dic_ans`, `dic_err`, `rol_por`, `gra_por`, `gra_ans`,
                `gra_err`, `wri_por`, `wri_ans`, `wri_err`, `new_por`, `spe_por`, `spe_ans`, `time_aux`, `gra_vid`,
                `rol_on`, `exercises`, `eva_por`
            )
            VALUES
            (
                :PROGRESSVERSION, :IDFOLLOWUP, :THEMEID, :USERID, :LASTCHANGE, :ALL_POR, :ALL_ERR, :ALL_ANS,
                :ALL_TIME, :SIT_POR, :STU_POR, :DIC_POR, :DIC_ANS, :DIC_ERR, :ROL_POR, :GRA_POR, :GRA_ANS,
                :GRA_ERR, :WRI_POR, :WRI_ANS, :WRI_ERR, :NEW_POR, :SPE_POR, :SPE_ANS, :TIME_AUX, :GRA_VID,
                :ROL_ON, :EXERCISES, :EVA_POR
            )
            ON DUPLICATE KEY UPDATE
                `progressVersion` = :PROGRESSVERSION,
                `lastchange` = :LASTCHANGE,
                `all_por` = :ALL_POR,
                `all_err` = :ALL_ERR,
                `all_ans` = :ALL_ANS,
                `all_time` = :ALL_TIME,
                `sit_por` = :SIT_POR,
                `stu_por` = :STU_POR,
                `dic_por` = :DIC_POR,
                `dic_ans` = :DIC_ANS,
                `dic_err` = :DIC_ERR,
                `rol_por` = :ROL_POR,
                `gra_por` = :GRA_POR,
                `gra_ans` = :GRA_ANS,
                `gra_err` = :GRA_ERR,
                `wri_por` = :WRI_POR,
                `wri_ans` = :WRI_ANS,
                `wri_err` = :WRI_ERR,
                `new_por` = :NEW_POR,
                `spe_por` = :SPE_POR,
                `spe_ans` = :SPE_ANS,
                `time_aux` = :TIME_AUX,
                `gra_vid` = :GRA_VID,
                `rol_on` = :ROL_ON,
                `exercises` = :EXERCISES,
                `eva_por` = :EVA_POR
        ";

        $aBrowserValues = array(
            ":PROGRESSVERSION" => $this->progressVersion,
            ":IDFOLLOWUP" => $this->idFollowup,
            ":THEMEID" => $this->themeid,
            ":USERID" => $this->userid,
            ":LASTCHANGE" => strval($this->lastchange),
            ":ALL_POR" => $this->all_por,
            ":ALL_ERR" => $this->all_err,
            ":ALL_ANS" => $this->all_ans,
            ":ALL_TIME" => $this->all_time,
            ":SIT_POR" => $this->sit_por,
            ":STU_POR" => $this->stu_por,
            ":DIC_POR" => $this->dic_por,
            ":DIC_ANS" => $this->dic_ans,
            ":DIC_ERR" => $this->dic_err,
            ":ROL_POR" => $this->rol_por,
            ":GRA_POR" => $this->gra_por,
            ":GRA_ANS" => $this->gra_ans,
            ":GRA_ERR" => $this->gra_err,
            ":WRI_POR" => $this->wri_por,
            ":WRI_ANS" => $this->wri_ans,
            ":WRI_ERR" => $this->wri_err,
            ":NEW_POR" => $this->new_por,
            ":SPE_POR" => $this->spe_por,
            ":SPE_ANS" => $this->spe_ans,
            ":TIME_AUX" => $this->time_aux,
            ":GRA_VID" => $this->gra_vid,
            ":ROL_ON" => $this->rol_on,
            ":EXERCISES" => strval($this->exercises),
            ":EVA_POR" => $this->eva_por,
        );
        $this->id = $this->executeSQL($sql, $aBrowserValues);

        if ($this->id > 0) {
            $this->commitTrans();
            return $this->id;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @param $row
     *
     * @return bool
     */
    public function loadFromFollowup($row)
    {
        $this->fillDataColsToProperties($row);

//        $abaUserDictionary = new AbaUserDictionary();
//        $abaUserDictionary->getUserDictionaryByUserId($this->userid);
//
//        /* @TODO - change after "migration" */
////        $this->progressVersion = ( is_numeric($abaUserDictionary->progressVersion) ? $abaUserDictionary->progressVersion : USER_DICTIONARY_DEF_VERSION_PROGRESS);
//        $this->progressVersion = ( is_numeric($abaUserDictionary->progressVersion) ? $abaUserDictionary->progressVersion : 0);

        return true;
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Remove user progress tool
     *
     */

    /**
     * @param $userId
     *
     * @return bool|int
     */
    public function deleteAllUserProgress($userId)
    {

        if(!is_numeric($userId)) {
            return false;
        }

        try {

            $paramsToSQL = array(
              ':USERID' => $userId,
            );

            $sSql = "
              DELETE FROM " . $this->tableName() . "
              WHERE
                  userid=:USERID
            ";

            $deletedResult = $this->deleteSQL($sSql, $paramsToSQL);

            return $deletedResult;
        } catch (Exception $e) {

            HeLogger::sendLog(
              "Remove progress tool ERROR",
              HeLogger::IT_BUGS,
              HeLogger::ERROR,
              "Exception in CmAbaFollowUpSummary->deleteAllUserProgress: " . $e->getMessage()
            );
        }

        return false;
    }

    /**
     *
     * Remove user progress tool
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */

}





