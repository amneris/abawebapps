<?php
/**
 * Finally is not linked to the database
 */
class UnitCourse
{
    public  $id;
    // -- Info for thumbnails, only for the views, precalculated--
    protected  $unitId;             //  1. Número
    protected  $mainNameNoTrans;   //  2. Título No traducible (Helper)
    protected  $titleTrans;        //  3. Título traducible (mainApp.php, implmemetnar función en el Helper)
    protected  $thumbUrlLink;    //5. Mirar el getUrl dinámico, debería estar en el modelo este cálculo.
    protected  $thumbPath;       // 6. Thumbnails mirar de donde c. está.(????)
    protected  $thumbHomePath;       // Thumbnail for Home Page
    protected  $teacherVideo;       // 7. Profesor. (Hardcodearlo en el helper)
    protected  $thumbVideoPath;      //9. Thumbnail (Mirar método de donde sale.??)
    protected  $thumbVideoHomePath; //Thumbnail for the Home
    protected  $userAccess; //Depending and calculated based on user id.

    const TRANS_KEY="key_title_subject_";
    //-------------------------

    /**
     * @param Integer $unitId
     */
    public function  __construct($unitId=NULL)
    {
        if( isset($unitId) )
        {
            return $this->getUnit($unitId);
        }
    }


    /**
     * @param integer $id
     * @param integer $userId If user id is passed, it will check if user has a access to this unit
     *
     * @return bool
     * @throws CDbException
     */
    public function getUnit($id=NULL, $userId=NULL)
    {
        if( is_null($id) )
        {
            throw new CDbException(" to load a unit an ID is a must. ");
        }

        try{
            $this->unitId = $id;
            $this->mainNameNoTrans = HeUnits::getUnitNameByIdUnit( $id );
            $this->titleTrans = HeUnits::getVideoClassTitleByUnit( self::TRANS_KEY.$this->getFormatUnitId($id) );
            $this->thumbUrlLink = Yii::app()->createUrl('/course/AllUnits', array('unit'=> $this->getFormatUnitId($id)));
            $this->thumbPath = HeMixed::getUrlPathImg("curso/".$id.".jpg");
            $this->thumbHomePath = HeMixed::getUrlPathImg("filmhome/".$id.".jpg");
            $this->thumbVideoPath = HeMixed::getUrlPathImg("videoclass/".$id.".jpg");
            $this->thumbVideoHomePath = HeMixed::getUrlPathImg("videoclasshome/".$id.".jpg");
            $this->teacherVideo = HeUnits::getTeacherVideoByUnit($id);
            $this->userAccess = false;
            if(isset($userId)){
                $this->userAccess = $this->hasAccess(NULL, $userId);
            }
        }
        catch(Exception $eHelper)
        {
            throw new CDbException(" ABA: The mock database HeUnits probably has returned an error. Course->getUnit ");
        }

        return true;
    }

    /**
     * @param $sectionId
     *
     * @return mixed
     */
    public function getThumbSectionHomePath( $sectionId )
    {
        switch($sectionId)
        {
            case 0: //'abaFilm'=>'sit_por',
                    return HeMixed::getUrlPathImg("filmhome/".$this->getFormatUnitId($this->unitId)."_unit_clean.jpg");
                break;
            case 1: //'abaFilm'=>'sit_por',
                    return HeMixed::getUrlPathImg("filmhome/".$this->getFormatUnitId($this->unitId)."_unit_clean.jpg");
                break;
            case 2: //'speak'  =>'stu_por' ,
                    return HeMixed::getUrlPathImg("speakSectionHome.jpg");
                break;
            case 3:  //'write'  =>'dic_por' ,
                    return HeMixed::getUrlPathImg("writeSectionHome.jpg");
                break;
            case 4:  //'interpret'=>'rol_por' ,
                    return HeMixed::getUrlPathImg("interprethome/unit_".$this->unitId.".jpg");
                break;
            case 5:  //'videoclass'=>'gra_vid' ,
                    return HeMixed::getUrlPathImg("sectionvchome/".$this->unitId.".jpg");
                break;
            case 6: //'exercises'=> 'wri_por' ,
                    return HeMixed::getUrlPathImg("exercisesSectionHome.jpg");
                break;
            case 7:  //'vocabulary'=> 'new_por' ,
                    return HeMixed::getUrlPathImg("vocabularySectionHome.jpg");
                break;
            case 8:  //'assessment'=> 'eva_por'
                    return HeMixed::getUrlPathImg("assessmentSectionHome.jpg");
                break;
        }

    }

    /**
     * Returns true if user can access the video-class.
     * @param null $role
     * @param null $userId
     *
     * @return bool
     */
    public function hasAccess( $role=NULL, $userId=NULL )
    {
        if( is_null($userId) )
        {
            if( isset(Yii::app()->user) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    /**
     * @param null $id
     *
     * @return string
     */
    public function getFormatUnitId($id=NULL)
    {
        if(!isset($id)){
            return sprintf("%03d", $this->id);  //substr("000".$this->id, -3) ;
        }
        else{
            return sprintf("%03d", $id);  //substr("000".$this->id, -3) ;
        }

    }


    // ----------- THUMBNAILS INFO ---------------------
    public function getMainNameNoTrans()
    {
        return $this->mainNameNoTrans;
    }

    public function getTeacherVideo()
    {
        return $this->teacherVideo;
    }

    public function getThumbPath()
    {
        return $this->thumbPath;
    }

    public function getThumbUrlLink()
    {
        return $this->thumbUrlLink;
    }

    public function getThumbVideoPath()
    {
        return $this->thumbVideoPath;
    }

    public function getTitleTrans()
    {
        return $this->titleTrans;
    }

    public function getUnitId()
    {
        return $this->unitId;
    }

    public function getThumbHomePath()
    {
        return $this->thumbHomePath;
    }

    public function getThumbVideoHomePath()
    {
        return $this->thumbVideoHomePath;
    }
    //-----------------------------------------

}
?>