<?php
/**
 * Model of table [followup_html4]
 *
 */
class AbaFollowUpHtml4 extends AbaActiveRecord
{
    private $tableFollowHtml = "";
    private $idUser = "";

    public function tableName($idUser = "") {

        if ($this->idUser == "") {
            $this->idUser = $idUser;
        } elseif ($this->idUser != $idUser) {
            Yii::log("different userIDs for AbaFollowUpHtml4 in the same session", CLogger::LEVEL_ERROR, 'itbugs');
            return Yii::app()->params['dbCampus'].'.followup_html4'. $this->tableFollowHtml;
        }

        if ($this->tableFollowHtml == "") {
            $cache = $this->_getCache();
            $cacheKey = "user_dictionary.progressVersionHtml." . $this->idUser;

            if (false === $cache || false === ($this->tableFollowHtml = $cache->get($cacheKey))) {

                $sql = "Select progressVersionHtml from user_dictionary where idUser=:USERID;";
                $sqlParameters = array(":USERID" => $this->idUser);
                $dataReader = $this->querySQL($sql, $sqlParameters);
                if (($row = $dataReader->read()) !== false) {
                    $this->tableFollowHtml = "_" . $row['progressVersionHtml'];
                }

                if (false !== $cache) {
                    $cache->set($cacheKey, $this->tableFollowHtml, 1800); //cache it for 30 minutes
                }
            }
        }

        return Yii::app()->params['dbCampus'].'.followup_html4'. $this->tableFollowHtml;
    }

    public function mainFields()
    {
        return "  `id`, `userid`, `themeid`, `page`, `section`, `action`, `audio`, `bien`,
                    `mal`, `lastchange`, `wtext`, `difficulty`, `time_aux`, `contador` ";
    }


    /** Returns max PK id of this table.
     * @return bool
     */
    public function getLastId()
    {
        //TODO JMJ cambiar el sql para que sea mas eficiente.
        $sql = " SELECT MAX(f.id) as `lastId` FROM ".$this->tableName()." f LIMIT 1 ";

        $dataReader=$this->querySQL($sql);
        if(($row = $dataReader->read())!==false)
        {
            return $row["lastId"];
        }

        return false;
    }

    /** Selects several rows from one table and inserts them into the other one.
     *
     * @param     $originTable
     * @param     $destinationTable
     * @param     $fromPosition
     * @param int $topPosition
     *
     * @return bool|int|string
     */
    public function getAndInsertNextChunkFollowUpHtml( $originTable, $destinationTable,
                                                                $fromPosition, $topPosition=100000 )
    {
        $this->beginTrans();

        $sql = " INSERT INTO `$destinationTable`
                    (  `id`,  `userid`,  `themeid`,  `page`,  `section`,  `action`,
                     `audio`,  `bien`,  `mal`,  `lastchange`,  `wtext`,  `difficulty`,
                     `time_aux`,  `contador` )

                 SELECT f.* FROM ".$originTable." f
                    WHERE f.`id`>=".$fromPosition." AND f.`id`<".$topPosition." ";

        $dataRow = $this->executeSQL( $sql , null, true);
        if (!$dataRow || $dataRow<=0) {
            $this->rollbackTrans();
            return false;
        }

        $this->commitTrans();

        return $dataRow;
    }

    /**
     *
     * FUNCTION DEPRECATED USED FOR MIGRATION BEFORE INTRODUCTION OF PARTITION OF PROGRESS TABLES
     *
     *
     * @param $lastId
     * @return bool|int
     */
    public function getAnyRowAboveId($lastId)
    {
        $sql = " SELECT COUNT(*) as `any` FROM followup_html4 f WHERE f.`id`> ".$lastId." LIMIT 20";

        $dataReader=$this->querySQL($sql);
        if(($row = $dataReader->read())!==false)
        {
            return intval($row["any"]);
        }

        return false;
    }


    public function getAndInsertLastChangedRows( $originTable, $destinationTable, $startId, $dateFilter)
    {
        $this->beginTrans();

        $sql = " REPLACE INTO `$destinationTable`
                    (  `id`,  `userid`,  `themeid`,  `page`,  `section`,  `action`,
                     `audio`,  `bien`,  `mal`,  `lastchange`,  `wtext`,  `difficulty`,
                     `time_aux`,  `contador` )

                 SELECT f.* FROM ".$originTable." f
                    WHERE f.`id`>=".$startId." AND f.`lastchange`>=DATE('".$dateFilter."')  ";

        $dataRow = $this->executeSQL( $sql , null, true);
        if (!$dataRow || $dataRow<=0) {
            $this->rollbackTrans();
            return false;
        }

        $this->commitTrans();

        return $dataRow;

    }

    /**
     * This function returns an array of completed or erroneous Course actions. If relevant the erroneous action text is
     * returned as well.
     *
     * @param $idUser
     * @param $idUnit
     * @param $sectionTypeId
     *
     * @return array rows from followup_html4
     */
    public function getSectionListCompletedElements($idUser, $idUnit, $sectionTypeId, $since = null){
        $responseArray = array();
        $lastChanged = '0000-00-00 00:00:00';
        $section = HeUnits::getOldSectionTitle($sectionTypeId);
        $action = $section == 'DICTATION' ? $section : ($section == 'WRITING' ? 'WRITTEN' : 'RECORDED');
        if($since != null){
            $date = new DateTime();
            $date->setTimestamp(intval($since));
            $lastChanged = $date->format('Y-m-d H:i:s');
        }
        $sql = "SELECT T1.audio, T1.action, T1.wtext, T1.page FROM ".$this->tableName($idUser)." T1
                    INNER JOIN (SELECT Max(f_html4.id) AS LastUpdated
			                    FROM ".$this->tableName($idUser)." AS f_html4
			                    WHERE f_html4.userid=$idUser
                                AND f_html4.themeid=$idUnit
                                AND f_html4.section='$section'
                                AND f_html4.action='$action'
                                AND f_html4.lastchange >= '$lastChanged'
                                GROUP BY f_html4.section, f_html4.page, f_html4.audio) T2
                    ON T1.id=T2.LastUpdated ";
        if($section == 'STUDY' || $section == 'ROLEPLAY' || $section == 'NEWWORDS' )
        {
            $sql .=  "UNION
                     SELECT T1.audio, T1.action, T1.wtext, T1.page FROM ".$this->tableName($idUser)." T1
                    INNER JOIN (SELECT Max(f_html4.id) AS LastUpdated
			                    FROM ".$this->tableName($idUser)." AS f_html4
			                    WHERE f_html4.userid=$idUser
                                AND f_html4.themeid=$idUnit
                                AND f_html4.section ='$section'
                                AND f_html4.action='LISTENED'
                                AND f_html4.lastchange >= '$lastChanged'
                                GROUP BY f_html4.section, f_html4.page, f_html4.audio) T2
                    ON T1.id=T2.LastUpdated ORDER BY page, audio";
        }
        $dataReader=$this->querySQL($sql, null, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        if ($section == 'STUDY' || $section == 'NEWWORDS' || $section == 'ROLEPLAY') {
            foreach($rows as $key => $row) {
                $responseArray[] = array('Audio'=>$row['audio'], 'Action' => $row['action'], 'Page' => $row['page']);
            }
        } elseif ($section == 'DICTATION' || $section = 'WRITING') {
            foreach($rows as $key => $row){
                $responseArray[] = array('Audio'=>$row['audio'], 'Action' => $row['action'], 'Page' => $row['page'], 'IncorrectText'=> $row['wtext'] != '' ? $row['wtext'] : null);
            }
        }
        return $responseArray;
    }

    /**
     * Returns an array of completed or erroneous Course actions for the specified user/unit combination. If relevant the erroneous action text is
     * returned as well.
     *
     * @param $idUser
     * @param $idUnit
     * @param null $since
     *
     * @return array rows from followup_html4
     */
    public function getUnitListCompletedElements($idUser, $idUnit, $since = null){
        $responseArray = array();
        $lastChanged = '0000-00-00 00:00:00';
        if($since != null){
            $date = new DateTime();
            $date->setTimestamp(intval($since));
            $lastChanged = $date->format('Y-m-d H:i:s');
        }
        $sql = "SELECT T1.audio, T1.action, T1.bien, T1.wtext, T1.page, T1.section FROM ".$this->tableName($idUser)." T1
                    INNER JOIN (SELECT Max(f_html4.id) AS LastUpdated
			                    FROM ".$this->tableName($idUser)." AS f_html4
			                    WHERE f_html4.userid=$idUser
			                    AND f_html4.themeid=$idUnit
                                AND f_html4.lastchange >= '$lastChanged'
                                AND (f_html4.section = 'STUDY' AND action IN ('RECORDED','LISTENED'))
                                GROUP BY f_html4.section, f_html4.action, f_html4.page, f_html4.audio) T2
                    ON T1.id=T2.LastUpdated UNION
                SELECT T1.audio, T1.action, T1.bien, T1.wtext, T1.page, T1.section FROM ".$this->tableName($idUser)." T1
                    INNER JOIN (SELECT Max(f_html4.id) AS LastUpdated
			                    FROM ".$this->tableName($idUser)." AS f_html4
			                    WHERE f_html4.userid=$idUser
			                    AND f_html4.themeid=$idUnit
                                AND f_html4.lastchange >= '$lastChanged'
                                AND (f_html4.section = 'DICTATION' AND action='DICTATION')
                                GROUP BY f_html4.section, f_html4.page, f_html4.audio) T2
                    ON T1.id=T2.LastUpdated UNION
                SELECT T1.audio, T1.action, T1.bien, T1.wtext, T1.page, T1.section FROM ".$this->tableName($idUser)." T1
                    INNER JOIN (SELECT Max(f_html4.id) AS LastUpdated
			                    FROM ".$this->tableName($idUser)." AS f_html4
			                    WHERE f_html4.userid=$idUser
                                AND f_html4.themeid=$idUnit
                                AND f_html4.lastchange >= '$lastChanged'
                                AND (f_html4.section = 'ROLEPLAY' AND action IN ('RECORDED','LISTENED'))
                                GROUP BY f_html4.section, f_html4.action, f_html4.page, f_html4.audio) T2
                    ON T1.id=T2.LastUpdated UNION
                SELECT T1.audio, T1.action, T1.bien, T1.wtext, T1.page, T1.section FROM ".$this->tableName($idUser)." T1
                    INNER JOIN (SELECT Max(f_html4.id) AS LastUpdated
			                    FROM ".$this->tableName($idUser)." AS f_html4
			                    WHERE f_html4.userid=$idUser
                                AND f_html4.themeid=$idUnit
                                AND f_html4.lastchange >= '$lastChanged'
                                AND (f_html4.section = 'WRITING' AND action = 'WRITTEN')
                                GROUP BY f_html4.section, f_html4.page, f_html4.audio) T2
                    ON T1.id=T2.LastUpdated UNION
                SELECT T1.audio, T1.action, T1.bien, T1.wtext, T1.page, T1.section FROM ".$this->tableName($idUser)." T1
                    INNER JOIN (SELECT Max(f_html4.id) AS LastUpdated
			                    FROM ".$this->tableName($idUser)." AS f_html4
			                    WHERE f_html4.userid=$idUser
                                AND f_html4.themeid=$idUnit
                                AND f_html4.lastchange >= '$lastChanged'
                                AND (f_html4.section = 'NEWWORDS' AND action IN ('RECORDED','LISTENED'))
                                GROUP BY f_html4.section, f_html4.action, f_html4.page, f_html4.audio) T2
                    ON T1.id=T2.LastUpdated";
        $dataReader=$this->querySQL($sql, null, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        $parentKey = 0;
        $title = 'SectionTypeId';
        $sectionTypeId = 0;
        foreach($rows as $key => $row){
            $rowSectionTypeId = HeUnits::getSectionTypeIdByOldSectionName($row['section']);
            if($row['bien'] == '0' && (is_null($row['wtext']) || $row['wtext'] == '')) {
                $text = 'Error';
            } else {
                $text = $row['wtext'];
            }
            if($rowSectionTypeId !== $sectionTypeId){
                $sectionTypeId = $rowSectionTypeId;
                $responseArray[$key] = array($title=>$sectionTypeId, 'Elements'=>array(array('Audio' => $row['audio'], 'Action' => $row['action'], 'Page'=>$row['page'], 'IncorrectText'=> $text != '' ? $text : null)));
                $parentKey = $key;
            } else {
                $responseArray[$parentKey]['Elements'][] = array('Audio' => $row['audio'], 'Action' => $row['action'], 'Page'=>$row['page'], 'IncorrectText'=> $text != '' ? $text : null);
            }
        }
        return $responseArray;
    }

    /**
     * Returns an array of completed or erroneous Course actions for the specified user/level combination. If relevant the erroneous action text is
     * returned as well.
     *
     * @param integer $idUser
     * @param integer $idLevel
     *
     * @return array rows from followup_html4
     */
    public function getLevelListCompletedElements($idUser, $idLevel, $since = null){
        $responseArray = array();
        $lastChanged = '0000-00-00 00:00:00';
        $firstUnit = HeUnits::getStartUnitByLevel($idLevel);
        $lastUnit = HeUnits::getEndUnitByLevel($idLevel);
        if($since != null){
            $date = new DateTime();
            $date->setTimestamp(intval($since));
            $lastChanged = $date->format('Y-m-d H:i:s');
        }
        $sql = "SELECT T1.audio, T1.action, T1.wtext, T1.page, T1.section, T1.themeid FROM ".$this->tableName($idUser)." T1
                    INNER JOIN (SELECT Max(f_html4.id) AS LastUpdated
			                    FROM ".$this->tableName($idUser)." AS f_html4
			                    WHERE f_html4.userid = $idUser
                                AND f_html4.themeid BETWEEN $firstUnit AND $lastUnit
                                AND f_html4.section NOT IN ('GRAMMAR', 'SPEAKING')
                                AND f_html4.action IN ('DICTATION', 'WRITTEN', 'RECORDED')
                                AND f_html4.lastchange >= '$lastChanged'
                                GROUP BY f_html4.section, f_html4.page, f_html4.audio) T2
                    ON T1.id=T2.LastUpdated UNION
                    SELECT T1.audio, T1.action, T1.wtext, T1.page, T1.section, T1.themeid FROM ".$this->tableName($idUser)." T1
                    INNER JOIN (SELECT Max(f_html4.id) AS LastUpdated
			                    FROM ".$this->tableName($idUser)." AS f_html4
			                    WHERE f_html4.userid = $idUser
                                AND f_html4.themeid BETWEEN $firstUnit AND $lastUnit
                                AND f_html4.section NOT IN ('GRAMMAR', 'SPEAKING', 'DICTATION', 'WRITING')
                                AND f_html4.action IN ('LISTENED')
                                AND f_html4.lastchange >= '$lastChanged'
                                GROUP BY f_html4.section, f_html4.page, f_html4.audio) T2
                    ON T1.id=T2.LastUpdated ORDER BY themeid, section, page";
        $dataReader=$this->querySQL($sql, null, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        $title = 'SectionTypeId';
        $sectionTypeId = 0;
        $unitId = 0;
        foreach($rows as $key => $row){
            $rowUnitId = $row['themeid'];
            $rowSectionTypeId = HeUnits::getSectionTypeIdByOldSectionName($row['section']);
            if($rowUnitId !== $unitId){
                $responseArray[$key] = array('Unit'=>$rowUnitId, 'Sections'=>array(array($title=>$rowSectionTypeId, 'Elements'=>array(array('Audio' => $row['audio'], 'Action' => $row['action'], 'Page'=>$row['page'], 'IncorrectText'=> $row['wtext'] != '' ? $row['wtext'] : null)))));
                $unitId = $row['themeid'];
                $parentKey = $key;
                $sectionsIndex = count($responseArray[$parentKey]['Sections']);
                $sectionTypeId = $rowSectionTypeId;
            } else {
                if($rowSectionTypeId !== $sectionTypeId){
                    $sectionTypeId = $rowSectionTypeId;
                    $responseArray[$parentKey]['Sections'][$sectionsIndex] = array($title=>$sectionTypeId, 'Elements'=>array(array('Audio' => $row['audio'], 'Action' => $row['action'], 'Page'=>$row['page'], 'IncorrectText'=> $row['wtext'] != '' ? $row['wtext'] : null)));
                    $sectionsIndex = count($responseArray[$parentKey]['Sections']);
                } else {
                    $responseArray[$parentKey]['Sections'][$sectionsIndex-1]['Elements'][] = array('Audio' => $row['audio'], 'Action' => $row['action'], 'Page'=>$row['page'], 'IncorrectText'=> $row['wtext'] != '' ? $row['wtext'] : null);
                }
            }
        }
        return $responseArray;
    }

    /**
     * Returns an array of completed or erroneous Course actions for the specified user/course combination. If relevant
     * the erroneous action text is returned as well.
     *
     * @param integer $idUser
     * @param integer $idCourse
     *
     * @return array rows from followup_html4
     */
    public function getCourseListCompletedElements($idUser, $idCourse, $since = null){
        $responseArray = array();
        $lastChanged = '0000-00-00 00:00:00';
        if($since != null){
            $date = new DateTime();
            $date->setTimestamp(intval($since));
            $lastChanged = $date->format('Y-m-d H:i:s');
        }
        $sql = "SELECT T1.audio, T1.action, T1.wtext, T1.page, T1.section, T1.themeid FROM ".$this->tableName($idUser)." T1
                    INNER JOIN (SELECT Max(f_html4.id) AS LastUpdated
			                    FROM ".$this->tableName($idUser)." AS f_html4
			                    WHERE f_html4.userid = $idUser
			                    AND f_html4.themeid>0
                                AND f_html4.section not in ('GRAMMAR', 'SPEAKING')
                                AND f_html4.action in ('DICTATION', 'WRITTEN', 'RECORDED')
                                AND f_html4.lastchange >= '$lastChanged'
                                GROUP BY f_html4.section, f_html4.page, f_html4.audio) T2
                    ON T1.id=T2.LastUpdated UNION
                    SELECT T1.audio, T1.action, T1.wtext, T1.page, T1.section, T1.themeid FROM ".$this->tableName($idUser)." T1
                    INNER JOIN (SELECT Max(f_html4.id) AS LastUpdated
			                    FROM ".$this->tableName($idUser)." AS f_html4
			                    WHERE f_html4.userid = $idUser
                                AND f_html4.themeid>0
                                AND f_html4.section not in ('GRAMMAR', 'SPEAKING', 'DICTATION', 'WRITING')
                                AND f_html4.action in ('LISTENED')
                                AND f_html4.lastchange >= '$lastChanged'
                                GROUP BY f_html4.section, f_html4.page, f_html4.audio) T2
                    ON T1.id=T2.LastUpdated  ORDER BY themeid, section, page";
        $dataReader=$this->querySQL($sql, null, $this->selectDbConnection());
        $rows = $dataReader->readAll();
        $title = 'SectionTypeId';
        $sectionTypeId = 0;
        $unitId = 0;
        foreach($rows as $key => $row){
            $rowUnitId = $row['themeid'];
            $rowSectionTypeId = HeUnits::getSectionTypeIdByOldSectionName($row['section']);
            if($rowUnitId !== $unitId){
                $responseArray[$key] = array('Unit'=>$rowUnitId, 'Sections'=>array(array($title=>$rowSectionTypeId, 'Elements'=>array(array('Audio' => $row['audio'], 'Action' => $row['action'], 'Page'=>$row['page'], 'IncorrectText'=> $row['wtext'] != '' ? $row['wtext'] : null)))));
                $unitId = $row['themeid'];
                $parentKey = $key;
                $sectionsIndex = count($responseArray[$parentKey]['Sections']);
                $sectionTypeId = $rowSectionTypeId;
            } else {
                if($rowSectionTypeId !== $sectionTypeId){
                    $sectionTypeId = $rowSectionTypeId;
                    $responseArray[$parentKey]['Sections'][$sectionsIndex] = array($title=>$sectionTypeId, 'Elements'=>array(array('Audio' => $row['audio'], 'Action' => $row['action'], 'Page'=>$row['page'], 'IncorrectText'=> $row['wtext'] != '' ? $row['wtext'] : null)));
                    $sectionsIndex = count($responseArray[$parentKey]['Sections']);
                } else {
                    $responseArray[$parentKey]['Sections'][$sectionsIndex-1]['Elements'][] = array('Audio' => $row['audio'], 'Action' => $row['action'], 'Page'=>$row['page'], 'IncorrectText'=> $row['wtext'] != '' ? $row['wtext'] : null);
                }
            }
        }
        return $responseArray;
    }

    /**
     * Function writes student progress to database. Returns percentage of completed exercises for the section that were
     * updated.
     *
     * @param $idUser
     * @param $unit
     * @param $sectionTypeId
     * @param $page
     * @param $id_elem
     * @param $action
     * @param $bien
     * @param $wtext
     * @param $totalExercises
     * @return float Percentage of completed exercises
     * @throws CDbException
     */
    public function  registerProgress($rowProgress, $totalExercises )
    {//$idUser, $unit, $sectionTypeId, $page, $id_elem, $action, $bien, $wtext,
        if (!isset($rowProgress->idUser) || is_null($rowProgress->idUnit))
        {
            throw new CDbException(" To be able to load a FollowUp a user Id and Unit Id is a must. ");
        }
        $difficulty = 0;
        $wtext = str_replace("?", "", $rowProgress->text);
        $mal = $rowProgress->correct == 1 ? 0 : 1;
        $sqlUpdate ="";
        $section = HeUnits::getOldSectionTitle($rowProgress->idSectionType);
        $sqlCount = "";
        $sqlCount1 = "";
        $newSql="";
        $specialSection = 0;

        switch ($section)
        {
            case 'STUDY': // Speak, 2,HABLA
            {
                $sql = "SELECT id FROM ".$this->tableName($rowProgress->idUser)." WHERE userid=$rowProgress->idUser AND themeid=$rowProgress->idUnit AND section='$section' AND audio='$rowProgress->audio'  AND bien=1 AND action='$rowProgress->action' AND page='$rowProgress->page' LIMIT 1;";
                $sqlCount = "SELECT count(distinct audio,page) AS cntReg FROM ".$this->tableName($rowProgress->idUser)." WHERE userid=$rowProgress->idUser AND themeid=$rowProgress->idUnit AND section='$section' AND bien=1 AND action='RECORDED';";
                $sqlCount1 = "SELECT count(distinct audio,page) AS cntReg FROM ".$this->tableName($rowProgress->idUser)." WHERE userid=$rowProgress->idUser AND themeid=$rowProgress->idUnit AND section='$section' AND bien=1 AND action='LISTENED';";
                //$totalExercises /=2;
                $specialSection = 1;
                break;
            }
            case 'DICTATION': // Write, 3, ESCRIBE
            {

                if ($rowProgress->action=='DICTATION') {
                    $sql = "SELECT id FROM ".$this->tableName($rowProgress->idUser)." WHERE userid=$rowProgress->idUser AND themeid=$rowProgress->idUnit AND page='$rowProgress->page' AND section='$section' AND action='$rowProgress->action' AND audio='$rowProgress->audio' ORDER BY id LIMIT 1;";
                    //$sqlUpdate = "update ".$this->tableName($rowProgress->idUser)." T1 inner join (SELECT Max(".$this->tableName($rowProgress->idUser).".id) AS LastUpdated FROM ".$this->tableName($rowProgress->idUser)." WHERE ((".$this->tableName($rowProgress->idUser).".userid=$rowProgress->idUser) AND (".$this->tableName($rowProgress->idUser).".page='$rowProgress->page') AND (".$this->tableName($rowProgress->idUser).".section='DICTATION') AND (".$this->tableName($rowProgress->idUser).".action='DICTATION') AND (".$this->tableName($rowProgress->idUser).".themeid=$rowProgress->idUnit) AND (".$this->tableName($rowProgress->idUser).".audio)='$rowProgress->audio')) T2 ON T1.id=T2.LastUpdated SET T1.wtext=:text_param, T1.lastchange=now(), T1.bien=$rowProgress->correct, T1.mal=$mal";
                    $sqlUpdate = "update ".$this->tableName($rowProgress->idUser)." T1 SET T1.wtext=:text_param, T1.lastchange=now(), T1.bien=$rowProgress->correct, T1.mal=$mal WHERE (T1.userid=$rowProgress->idUser) AND (T1.page='$rowProgress->page') AND (T1.section='DICTATION') AND (T1.action='DICTATION') AND (T1.themeid=$rowProgress->idUnit) AND (T1.audio='$rowProgress->audio') ";
                    $params = array(':text_param' => $wtext);
                }
                else
                { //LISTENED
                    $sql = "SELECT id FROM ".$this->tableName($rowProgress->idUser)." WHERE  userid=$rowProgress->idUser AND themeid=$rowProgress->idUnit AND page='$rowProgress->page' AND section='$section' AND action='$rowProgress->action' AND audio='$rowProgress->audio' AND bien=1 LIMIT 1;";
                }
                $sqlCount = "SELECT count(distinct audio,page) AS cntReg FROM ".$this->tableName($rowProgress->idUser)." WHERE userid=$rowProgress->idUser AND themeid=$rowProgress->idUnit AND section='$section' AND bien=1 AND action='DICTATION';";
                break;
            }
            case 'ROLEPLAY': // Interpret, 4, INTERPRETA
            {
                $sql = "SELECT id FROM ".$this->tableName($rowProgress->idUser)." WHERE userid=$rowProgress->idUser AND themeid=$rowProgress->idUnit AND section='$section' AND audio='$rowProgress->audio' AND bien=1 AND action='$rowProgress->action' AND page='$rowProgress->page' LIMIT 1;";
                $sqlCount = "SELECT count(distinct audio,page) AS cntReg FROM ".$this->tableName($rowProgress->idUser)." WHERE userid=$rowProgress->idUser AND themeid=$rowProgress->idUnit AND section='$section' AND bien=1 AND action='RECORDED';";
                $specialSection = 1;
                break;
            }
            case 'WRITING': // Exercises, 6, EJERCICIOS
            {

                if ($rowProgress->action=='WRITTEN') {
                    $sql = "SELECT id FROM ".$this->tableName($rowProgress->idUser)." WHERE userid=$rowProgress->idUser AND themeid=$rowProgress->idUnit AND section='$section' AND audio='$rowProgress->audio' AND action='$rowProgress->action' AND page='$rowProgress->page' ORDER BY id LIMIT 1;";
//                    $sqlUpdate = "update ".$this->tableName($rowProgress->idUser)." T1 inner join (SELECT Max(".$this->tableName($rowProgress->idUser).".id) AS LastUpdated FROM ".$this->tableName($rowProgress->idUser)." WHERE ((".$this->tableName($rowProgress->idUser).".userid=$rowProgress->idUser)  AND (".$this->tableName($rowProgress->idUser).".page='$rowProgress->page') AND (".$this->tableName($rowProgress->idUser).".section='WRITING') AND (".$this->tableName($rowProgress->idUser).".action='WRITTEN') AND (".$this->tableName($rowProgress->idUser).".themeid=$rowProgress->idUnit) AND (".$this->tableName($rowProgress->idUser).".audio)='$rowProgress->audio')) T2 ON T1.id=T2.LastUpdated SET T1.wtext=:text_param, T1.lastchange=now(), T1.bien=$rowProgress->correct, T1.mal=$mal";
                    $sqlUpdate = "update ".$this->tableName($rowProgress->idUser)." T1 SET T1.wtext=:text_param, T1.lastchange=now(), T1.bien=$rowProgress->correct, T1.mal=$mal WHERE (T1.userid=$rowProgress->idUser)  AND (T1.page='$rowProgress->page') AND (T1.section='WRITING') AND (T1.action='WRITTEN') AND (T1.themeid=$rowProgress->idUnit) AND (T1.audio='$rowProgress->audio') ";
                    $params = array(':text_param' => $wtext);
                }
                else
                {   //LISTENED
                    $sql = "SELECT id FROM ".$this->tableName($rowProgress->idUser)." WHERE userid=$rowProgress->idUser AND themeid=$rowProgress->idUnit AND section='$section' AND audio='$rowProgress->audio' AND bien=1 AND action='$rowProgress->action' AND page='$rowProgress->page' LIMIT 1;";
                }
                $sqlCount = "SELECT count(distinct audio,page) AS cntReg FROM ".$this->tableName($rowProgress->idUser)." WHERE userid=$rowProgress->idUser AND themeid=$rowProgress->idUnit AND section='$section' AND bien=1 AND action='WRITTEN';";
                break;
            }
            case 'NEWWORDS': //Vocabulary, 7, "VOCABULARIO"
            {
                $sql = "SELECT id FROM ".$this->tableName($rowProgress->idUser)." WHERE userid=$rowProgress->idUser AND themeid=$rowProgress->idUnit AND section='$section' AND audio='$rowProgress->audio' AND bien=1 AND action='$rowProgress->action' AND page='$rowProgress->page' LIMIT 1;";
                $sqlCount = "SELECT count(distinct audio,page) AS cntReg FROM ".$this->tableName($rowProgress->idUser)." WHERE userid=$rowProgress->idUser AND themeid=$rowProgress->idUnit AND section='$section' AND bien=1 AND action='RECORDED';";
                $sqlCount1 = "SELECT count(distinct audio,page) AS cntReg FROM ".$this->tableName($rowProgress->idUser)." WHERE userid=$rowProgress->idUser AND themeid=$rowProgress->idUnit AND section='$section' AND bien=1 AND action='LISTENED';";
                $specialSection = 1;
                break;
            }
            case 'GRAMMAR': //"GRAMMAR", (Not used/monitored actively anymore)
            {
                $sql = "SELECT id FROM ".$this->tableName($rowProgress->idUser)." WHERE userid=$rowProgress->idUser AND themeid=$rowProgress->idUnit AND section='$section' AND audio='$rowProgress->audio' AND bien=1 AND action='$rowProgress->action' AND page='$rowProgress->page' LIMIT 1;";
                $sqlCount = "SELECT count(distinct audio,page) AS cntReg FROM ".$this->tableName($rowProgress->idUser)." WHERE userid=$rowProgress->idUser AND themeid=$rowProgress->idUnit AND section='$section' AND bien=1 AND action='LISTENED';";
                break;
            }
            case 'ENTER':
                break;

            default :
                {
                $sql = "SELECT id FROM ".$this->tableName($rowProgress->idUser)." WHERE userid=$rowProgress->idUser AND themeid=$rowProgress->idUnit AND section='$section' AND audio='$rowProgress->audio' AND bien=1 AND action='$rowProgress->action' AND page='$rowProgress->page' LIMIT 1;";
                $sqlCount = "SELECT count(distinct audio,action,page) AS cntReg FROM ".$this->tableName($rowProgress->idUser)." WHERE userid=$rowProgress->idUser AND themeid=$rowProgress->idUnit AND section='$section' AND bien=1;";
                break;
                }
        }

        //Miramos si ya existe el registro, sino lo insertarmos en la table followup_html4/followup_html4_30
        $dataReader=$this->querySQL($sql);
        if (!$dataReader->readAll()) {
            $newSql=$sql; //We save del sql beacause we delete in the next sentence.
            $sql = "INSERT INTO ".$this->tableName($rowProgress->idUser)." (userid,themeid,page,section,action,audio,bien,mal,lastchange,wtext,difficulty) VALUES ($rowProgress->idUser,$rowProgress->idUnit,$rowProgress->page,'$section','$rowProgress->action','$rowProgress->audio',$rowProgress->correct,$mal,now(),:text_param,$difficulty)";
            $params = array(':text_param' => $wtext);
            $this->querySQL($sql, $params);
            //Fix that insert a LISTENED every time RECORDED is executed.
            if ($specialSection == 1 && $rowProgress->action=='RECORDED'){
                $newSql=str_replace('RECORDED','LISTENED',$newSql);
                $dataReader=$this->querySQL($newSql);
                if (!$dataReader->readAll()) {
                    $sql = "INSERT INTO ".$this->tableName($rowProgress->idUser)." (userid,themeid,page,section,action,audio,bien,mal,lastchange,wtext,difficulty) VALUES ($rowProgress->idUser,$rowProgress->idUnit,$rowProgress->page,'$section','LISTENED','$rowProgress->audio',$rowProgress->correct,$mal,now(),:text_param,$difficulty)";
                    $params = array(':text_param' => $wtext);
                    $this->querySQL($sql, $params);
                }
            }

        } else {
            // Here we Fix the users that already have a error.
            if ($specialSection == 1 && $rowProgress->action=='RECORDED'){
                $newSql=str_replace('RECORDED','LISTENED',$sql);
                $dataReader=$this->querySQL($newSql);
                if (!$dataReader->readAll()) {
                    $sql = "INSERT INTO ".$this->tableName($rowProgress->idUser)." (userid,themeid,page,section,action,audio,bien,mal,lastchange,wtext,difficulty) VALUES ($rowProgress->idUser,$rowProgress->idUnit,$rowProgress->page,'$section','LISTENED','$rowProgress->audio',$rowProgress->correct,$mal,now(),:text_param,$difficulty)";
                    $params = array(':text_param' => $wtext);
                    $this->querySQL($sql, $params);
                }
            }

            if ($sqlUpdate!="") {
                $this->querySQL($sqlUpdate, $params);
            }
        }

        if ($rowProgress->correct==1) {
            if ($section == 'GRAMMAR') //En esta sección solo se usa para calcular el progreso ver el video y eso se gestiona en wsetVideoClassEv4.php y se actualiza un campo diferente 'gra_vid'.
            {
                return;
            }

            $dataReader=$this->querySQL($sqlCount);
            $rows=$dataReader->readAll();
            if ($rows) {
                $recordCount= $rows[0]['cntReg'];
                if ($sqlCount1!="")
                {
                    $dataReader=$this->querySQL($sqlCount1);
                    $rows=$dataReader->readAll();
                    if ($rows)
                    {
                        $recordCount = ($recordCount + $rows[0]['cntReg'])/2;
                    }
                }
            }
            else
            {
                $recordCount= 0;
                if ($sqlCount1!="")
                {
                    $dataReader=$this->querySQL($sqlCount1);
                    $rows=$dataReader->readAll();
                    if ($rows)
                    {
                        $recordCount = $rows[0]['cntReg']/2;
                    }
                }
            }
            $valor_por = floor(100*($recordCount)/$totalExercises);
            return $valor_por;
        }
    }
}

