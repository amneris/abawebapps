<?php namespace commonNamespace\models\intranet;

/**
 * Filterform to use filters in combination with CArrayDataProvider and CGridView
 * @see http://www.yiiframework.com/wiki/232/using-filters-with-cgridview-and-carraydataprovider/
 */
class ExtranetUserListForm extends FiltersForm
{

  /**
   * Override magic setter for filters
   * @param string $name
   * @param mixed $value
   */
  public function __set($name, $value)
  {
    if ( $name == 'idEnterprise' && $value !== '' ) {
      $this->originalFilters[$name] = $value;

      $enterpriseName = $value;
      $value = array();

      $criteria = new \CDbCriteria();
      $criteria->addSearchCondition('name', $enterpriseName);
      $mlEnterprise = \Enterprise::model()->findAll( $criteria );

      foreach( $mlEnterprise as $mEnterprise ) $value[] = $mEnterprise->id;
    }

    \Yii::log($name, \CLogger::LEVEL_INFO);

    parent::__set( $name, $value );
  }

}