<?php namespace commonNamespace\models\intranet;

/**
 * Filterform to use filters in combination with CArrayDataProvider and CGridView
 * @see http://www.yiiframework.com/wiki/232/using-filters-with-cgridview-and-carraydataprovider/
 */
class FiltersForm extends \CFormModel
{
  /**
   * Original filters, without modification.
   *
   * @var array filters, key => filter string
   */
  protected $originalFilters = array();

  /**
   * @var array filters, key => filter string
   */
  protected $filters = array();

  /**
   * Override magic getter for filters
   * @param string $name
   */
  public function __get($name)
  {
    if (!array_key_exists($name, $this->filters)) {
      $this->filters[$name] = '';
    }

    if ( array_key_exists($name, $this->originalFilters) )
      return $this->originalFilters[$name];
    else return $this->filters[$name];
  }

  /**
   * Override magic setter for especial filters
   * @param string $name
   * @param mixed $value
   */
  public function __set($name, $value)
  {
    $this->filters[$name] = $value;
  }

  public function setFilters( $filters ) {
    if ( !is_array($filters) ) throw new \CException("Filters parameter is not an array.");

    foreach( $filters as $name=>$value ) $this->$name = $value;

//    $this->filters = $filters;
  }

  /**
   * Filter input array by key value pairs
   * @param array $data rawData
   * @return array filtered data array
   */
  public function filter(array $data)
  {
    \Yii::log(var_export($this->filters, true), \CLogger::LEVEL_INFO);

    foreach ($data AS $rowIndex => $row) {
      foreach ($this->filters AS $key => $searchValue) {
        if (!is_null($searchValue) AND $searchValue !== '') {
          $compareValue = null;

          if ($row instanceof \CModel) {
            if (isset($row->$key) == false) {
              throw new \CException("Property " . get_class($row) . "::{$key} does not exist!");
            }
            $compareValue = $row->$key;
          } elseif (is_array($row)) {
            if (!array_key_exists($key, $row)) {
              throw new \CException("Key {$key} does not exist in array!");
            }
            $compareValue = $row[$key];
          } else {
            throw new \CException("Data in CArrayDataProvider must be an array of arrays or an array of CModels!");
          }

          if ( !is_array($searchValue) && stripos($compareValue, $searchValue) === false) {
            unset($data[$rowIndex]);
          } else if ( is_array($searchValue) && in_array($compareValue, $searchValue) === false ) {
            unset($data[$rowIndex]);
          }
        }
      }
    }
    return $data;
  }

}