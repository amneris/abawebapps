<?php
/**
 * SELECT l.`id`, l.`dateRequest`, l.`ipFromRequest`, l.`appNameFromRequest`, l.`urlRequest`,
 * l.`paramsRequest`, l.`nameService`,
 * l.`dateResponse`, l.`paramsResponse`, l.`successResponse`, l.`errorCode`
 * FROM log_web_service l;
 */
/**
 * @property integer id
 * @property string appNameFromRequest
 * @property string dateRequest
 * @property string dateResponse
 * @property string ipFromRequest
 * @property string nameService
 * @property string paramsRequest
 * @property string paramsResponse
 * @property integer successResponse
 * @property integer errorCode
 * @property string urlRequest
 */
class CmLogWebService extends AbaActiveRecord
{
    public $id;

    /**
     * Constructor, null implementation
     */
    public function __construct($scenario=NULL, $dbConnName=NULL)
    {
        parent::__construct($scenario, $dbConnName);
    }

    /** Table of this model
     * @return string
     */
    public function tableName()
    {
        return Yii::app()->params['dbCampusLogs'].'.log_web_service';
    }

    /** All fields to be returned
     * @return string
     */
    public function mainFields()
    {
        return "  l.`id`, l.`dateRequest`, l.`ipFromRequest`, l.`appNameFromRequest`, l.`urlRequest`, l.`paramsRequest`, l.`nameService`,
                    l.`dateResponse`, l.`paramsResponse`, l.`successResponse`, l.`errorCode` ";
    }

    /** Returns the row of the id and fills the object
     * @param integer $id
     *
     * @return bool|LogWebService
     */
    public function getLogById( $id=NULL )
    {
        if(is_null($id))
        {
            $id= -156156106;
        }
        $sql=" SELECT ".$this->mainFields()." FROM ".$this->tableName()." l  WHERE l.`id`=".$id."; ";

        $dataReader=$this->querySQL( $sql );
        if(($row = $dataReader->read())!==false)
        {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function insertLog()
    {
        $sql= " INSERT INTO ".$this->tableName()." (`dateRequest`, `ipFromRequest`, `appNameFromRequest`, `urlRequest`,
                                                    `paramsRequest`, `nameService`,
                                                    `dateResponse`, `paramsResponse`, `successResponse`, `errorCode`)
                                  VALUES( :DATEREQUEST, :IPFROMREQUEST, :APPNAMEFROMREQUEST, :URLREQUEST,
                                                    :PARAMSREQUEST, :NAMESERVICE,
                                                    :DATERESPONSE, :PARAMSRESPONSE, :SUCCESSRESPONSE, :ERRORCODE ); ";

        $aBrowserValues = array( ":DATEREQUEST" => $this->dateRequest  , ":IPFROMREQUEST" => $this->ipFromRequest  ,
                                 ":APPNAMEFROMREQUEST" => $this->appNameFromRequest  ,
                                 ":URLREQUEST" => $this->urlRequest  , ":PARAMSREQUEST" => $this->paramsRequest  ,
                                 ":NAMESERVICE" => $this->nameService  ,
                                 ":DATERESPONSE" => $this->dateResponse  , ":PARAMSRESPONSE" => $this->paramsResponse  ,
                                 ":SUCCESSRESPONSE" => $this->successResponse  , ":ERRORCODE" => $this->errorCode );

        $this->id = $this->executeSQL( $sql, $aBrowserValues , false);
        if ( $this->id > 0)
        {
            return $this->id;
        }
        else
        {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function updateLogEndRequest()
    {
        $query = "UPDATE " . $this->tableName() . " SET `dateResponse`=:DATERESPONSE,
                                                    `paramsResponse`=:PARAMSRESPONSE,
                                                    `successResponse`=:SUCCESSRESPONSE,
                                                    `errorCode`=:ERRORCODE
                        WHERE id=" . $this->id;

        $aBrowserValues = array(":DATERESPONSE" => $this->dateResponse,
            ":PARAMSRESPONSE" => $this->paramsResponse,
            ":SUCCESSRESPONSE" => $this->successResponse,
            ":ERRORCODE" => $this->errorCode );

        if ($this->updateSQL($query, $aBrowserValues) > 0) {
            return true;
        } else {
            return false;
        }
    }

}
