<?php

/**
 * CmLogUserActivity
 * @author: mgadegaard
 * Date: 01/07/2015
 * Time: 16:56
 * © ABA English 2015
 *
 * This is the model class for table "log_user_activity".
 *
 * The following are the available columns in table 'log_user_activity':
 * @property string $userId
 * @property string $id
 * @property string $time
 * @property string $table
 * @property string $fields
 * @property string $description
 * @property string $action
 * @property string $ip
 */
class CmLogUserActivity extends AbaActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'log_user_activity';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
          array('userId, time, table, description, action', 'required'),
          array('userId', 'length', 'max' => 10),
          array('table, action, ip', 'length', 'max' => 45),
          array('description', 'length', 'max' => 150),
          array('fields', 'safe'),
            // The following rule is used by search().
          array('userId, id, time, table, fields, description, action, ip', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'userId' => 'FK to users',
          'id' => 'ID',
          'time' => 'Time',
          'table' => 'LIKE a FK to a table name',
          'fields' => 'LIKE a FK to a fields name',
          'description' => 'Description',
          'action' => 'Operation on Campus, payment, Crons, etc.',
          'ip' => 'REMOTE IP',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('userId', $this->userId, true);
        $criteria->compare('id', $this->id, true);
        $criteria->compare('time', $this->time, true);
        $criteria->compare('table', $this->table, true);
        $criteria->compare('fields', $this->fields, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('action', $this->action, true);
        $criteria->compare('ip', $this->ip, true);
        return new CActiveDataProvider($this, array('criteria' => $criteria,));
    }

    /**
     * @return CDbConnection the database connection used for this class
     */
    public function getDbConnection()
    {
        return Yii::app()->dbLogs;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return CmLogUserActivity the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public $userId;
    private $aOldUserFields = array();
    private $aNewUserFields = array();
    private $aChangedFields = array();

    /**
     * @param CmAbaUser $user
     */
    public function __construct($user = null)
    {
        parent::__construct();
        if (!empty($user)) {
            $this->aOldUserFields = $this->getFieldsClass($user);
            $this->userId = $user->id;
        }
    }

    /**
     * @param CmAbaUser $user
     * @param string $desc
     * @param string $action
     *
     * @return bool
     */
    public function saveUserChangesData($user, $desc = "saveUserChangesData", $action = "Modificar Datos")
    {
        if (Yii::app()->config->get("ENABLE_LOG_USERACTIVITY") != 1) {
            return true;
        }
        if ($this->hasDiffFields($user)) {
            $fieldsChanged = "";
            foreach ($this->aChangedFields as $fieldName => $aValues) {
                $fieldsChanged .= ", [" . $fieldName . "] changed from " . $aValues[0] . " to " . $aValues[1];
            }
            $saveLog = $this->insertLog($user->tableName(), $fieldsChanged, $desc, $action);
        } else {
            // There are no changes at all, but we consider that everything was successful.
            $saveLog = true;
        }
        return $saveLog;
    }

    /**
     * @param $tableName
     * @param $desc
     * @param $action
     * @param integer $userId
     *
     * @return bool
     */
    public function saveUserLogActivity($tableName, $desc, $action, $userId = null)
    {
        if (Yii::app()->config->get("ENABLE_LOG_USERACTIVITY") != 1) {
            return true;
        }

        if (isset($userId)) {
            $moUser = new CmAbaUser();
            if (!$moUser->populateUserById($userId)) {
                return false;
            }
            $this->userId = $moUser->id;
        }
        $saveLog = $this->insertLog($tableName, "", $desc, $action);
        return $saveLog;
    }

    /**
     * @param $table
     * @param $fields
     * @param $desc
     * @param $action
     *
     * @return bool
     */
    public function insertLog($table, $fields, $desc, $action)
    {
        $machine = Yii::app()->request->getUserHostAddress();

//        $stLogData = array(
//          "userId" => $this->userId,
//          "table" => $table,
//          "fields" => $fields,
//          "description" => $desc,
//          "action" => $action,
//          "ip" => $machine,
//        );
//
//        return HeLogger::sendLog("User activity", HeLogger::USERACTIVITY, HeLogger::INFO,
//          $stLogData);

        $sql = " INSERT INTO ".$this->tableName().
            " (userId, `time`, `table`, `fields`, description, `action`, `ip`) ".
            " VALUES( ".$this->userId .", NOW(),'".$table ."',:FIELD ,'".$desc."','".$action."','".$machine."'); ";
        $this->id = $this->executeSQL($sql, array(":FIELD" => $fields));
        if ($this->id > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param CmAbaUser $user
     *
     * @return bool|
     */
    private function hasDiffFields($user)
    {
        $fDiffs = false;
        $this->aNewUserFields = $this->getFieldsClass($user);
        foreach ($this->aNewUserFields as $fieldName => $value) {
            if ($this->aOldUserFields[$fieldName] !== $value) {
                $this->aChangedFields[$fieldName] = array($this->aOldUserFields[$fieldName], $value);
                $fDiffs = true;
            }
        }
        return $fDiffs;
    }
}
