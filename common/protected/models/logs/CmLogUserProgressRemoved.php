<?php

/**
 * CmLogUserProgressRemoved
 *
 * This is the model class for table "log_user_progress_removed".
 *
 * The followings are the available columns in table 'log_user_progress_removed':
 * @property integer $removedProgressId
 * @property integer $userId
 * @property string $units
 * @property string $dateAdd
 */
class CmLogUserProgressRemoved extends AbaActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'log_user_progress_removed';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmLogUserProgressRemoved the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
          array('userId', 'required'),
          array(
            'removedProgressId, userId',
            'numerical',
            'integerOnly' => true
          ),
          array('dateAdd', 'type', 'type' => 'string', 'allowEmpty' => false),
          array('units', 'type', 'type' => 'string', 'allowEmpty' => true),
          array(
            'removedProgressId, userId, units, dateAdd',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " lupr.`removedProgressId`, lupr.`userId`, lupr.`units`, lupr.`dateAdd` ";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @return $this|bool
     */
    public function getUserRemovedProgressByUserId($userId)
    {

        $paramsToSQL = array(
          ":USERID" => $userId,
        );

        $sSql = "
            SELECT
                " . $this->mainFields() . "
            FROM " . $this->tableName() . " lupr
            WHERE
                lupr.userId = :USERID
            LIMIT 0, 1
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $userId
     *
     * @return array
     */
    public function removeAllUserProgress($userId)
    {

        try {

            // 1. Get user dictionary number

            $cmAbaUserDictionary = new CmAbaUserDictionary();

            $sTableFollowup4 = 'followup4';
            $sTableFollowupHtml4 = 'followup_html4';

            if ($cmAbaUserDictionary->getUserDictionaryByUserId($userId)) {

                $sTableFollowup4 = 'followup4_' . $cmAbaUserDictionary->progressVersion;
                $sTableFollowupHtml4 = 'followup_html4_' . $cmAbaUserDictionary->progressVersionHtml;
            }

            // 2. Select users for removed user progress log

            $oAbaFollowup = new AbaFollowup();
            $sUnits = $oAbaFollowup->getAllUserProgressUnitsString($sTableFollowup4, $userId);

            $successFollowup4 = true;
            $successFollowupHtml4 = true;
            $successFollowup4Summary = true;
            $successFollowup4ByWeek = true;

            // 3. Delete user progress

            // 3.1 DELETE FROM aba_b2c.followup4[_XX]
            if (!$oAbaFollowup->deleteAllUserProgressF4($sTableFollowup4, $userId)) {

                $successFollowup4 = false;
            }

            // 3.2 DELETE FROM aba_b2c.followup_html4[_XX]
            if (!$oAbaFollowup->deleteAllUserProgressFhtml4($sTableFollowupHtml4, $userId)) {

                $successFollowupHtml4 = false;
            }

            // 3.3 DELETE FROM aba_b2c_summary.followup4_summary
            $oCmAbaFollowUpSummary = new CmAbaFollowUpSummary();

            if (!$oCmAbaFollowUpSummary->deleteAllUserProgress($userId)) {

                $successFollowup4Summary = false;
            }

            // 3.4 DELETE FROM aba_b2c_summary.users_progress_byweek
            if (!$oAbaFollowup->deleteAllUserProgressByWeek($userId)) {

                $successFollowup4ByWeek = false;
            }

            return array(
              "success" => true,
              "removedUnits" => $sUnits,
              "successFollowup4" => $successFollowup4,
              "successFollowupHtml4" => $successFollowupHtml4,
              "successFollowup4Summary" => $successFollowup4Summary,
              "successFollowup4ByWeek" => $successFollowup4ByWeek,
            );

        } catch (Exception $ex) {

            return array(
              "success" => false,
              "removedUnits" => "ERROR: " . $ex->getMessage(),
              "successFollowup4" => false,
              "successFollowupHtml4" => false,
              "successFollowup4Summary" => false,
              "successFollowup4ByWeek" => false,
            );
        }
    }

}
