<?php
/**
 * CmLogSentSelligent
 * @author: mgadegaard
 * Date: 02/07/2015
 * Time: 17:40
 * © ABA English 2015
 * This is the model class for table "log_sent_selligent".
 *
 * The followings are the available columns in table 'log_sent_selligent':
 * @property string $id
 * @property string $dateSent
 * @property string $url
 * @property string $webservice
 * @property string $fields
 * @property string $response
 * @property string $userId
 * @property string $sourceAction
 * @property string $errorDesc
 */
class CmLogSentSelligent extends AbaActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'log_sent_selligent';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('dateSent, url, webservice, fields, response', 'required'),
            array('url, sourceAction', 'length', 'max'=>100),
            array('webservice', 'length', 'max'=>45),
            array('userId', 'length', 'max'=>6),
            array('errorDesc', 'length', 'max'=>250),
            // The following rule is used by search().
            array(
                'id, dateSent, url, webservice, fields, response, userId, sourceAction, errorDesc',
                'safe',
                'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'dateSent' => 'Date that was sent the request to Selligent. Whether successful or not.',
            'url' => 'url where it was sent to.',
            'webservice' => 'name of the operation in Selligent',
            'fields' => 'field names sent and their values.',
            'response' => 'text replied from selligent if successful, -1 otherwise.',
            'userId' => 'User Id that triggered this request.',
            'sourceAction' => 'Action or process from which this op was triggered',
            'errorDesc' => 'In case of error, the details.',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical use case:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('id', $this->id, true);
        $criteria->compare('dateSent', $this->dateSent, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('webservice', $this->webservice, true);
        $criteria->compare('fields', $this->fields, true);
        $criteria->compare('response', $this->response, true);
        $criteria->compare('userId', $this->userId, true);
        $criteria->compare('sourceAction', $this->sourceAction, true);
        $criteria->compare('errorDesc', $this->errorDesc, true);

        return new CActiveDataProvider($this, array('criteria'=>$criteria,));
    }

    /**
     * @return CDbConnection the database connection used for this class
     */
    public function getDbConnection()
    {
        return Yii::app()->dbLogs;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmLogSentSelligent the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return CmLogSentSelligent
     */
    public function __construct()
    {
        parent::__construct('insert', 'dbLogs');
    }

    /**
     * @param $url
     * @param $webservice
     * @param $fields
     * @param $response
     * @param $userId
     * @param $sourceAction
     * @param $errMessage
     *
     * @return bool
     */
    public function insertLog($url, $webservice, $fields, $response, $userId, $sourceAction, $errMessage)
    {
        $sql= "INSERT INTO " .
               $this->tableName()." (`url`, `webservice`, `fields`, `response`, `userId`, `sourceAction`, `errorDesc`)".
               " VALUES ".
               "( :URL, :WEBSERVICE, :FIELDS, :RESPONSE, :USER_ID, '".$sourceAction."', :ERROR_DESC ); ";
        $params = array(
            ":URL" => $url,
            ":WEBSERVICE" => $webservice,
            ":FIELDS" => $fields,
            ":RESPONSE" => $response,
            ":USER_ID" => $userId,
            ":ERROR_DESC" => $errMessage
        );
        $this->id = $this->executeSQL($sql, $params, false);
        if ($this->id > 0) {
            return $this->id;
        } else {
            return false;
        }
    }
}
