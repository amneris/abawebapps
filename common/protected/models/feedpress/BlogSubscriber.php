<?php

/**
 * This is the model class for table "blog_subscriber".
 *
 * The followings are the available columns in table 'blog_subscriber':
 * @property integer $id
 * @property string $email
 * @property integer $idBlog
 * @property integer $confirmed
 * @property string $updated
 * @property string $created
 * @property string $deleted
 * @property integer $isdeleted
 */
class BlogSubscriber extends BaseBlogSubscriber
{
  const _FREE = 1;
  const _PREMIUM = 2;
  const _NOUSER = 3;

  protected $arInitialValues = array();

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseBlogSubscriber the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

  public function freezeInitialValues() {
    $this->arInitialValues = $this->attributes;
  }

  public function itWasModified() {
    $bEqual = true;
    foreach( $this->attributes as $key => $value ) {
      if ( !isset($this->arInitialValues[$key]) || $this->arInitialValues[$key] != $value ) {
        $bEqual = false;
        break;
      }
    }

    return $bEqual;
  }
}
