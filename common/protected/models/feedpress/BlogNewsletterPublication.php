<?php

/**
 * This is the model class for table "blog_newsletter_publication".
 *
 * The followings are the available columns in table 'blog_newsletter_publication':
 * @property integer $id
 * @property integer $idBlog
 * @property integer $status
 * @property string $dateToSend
 * @property string $lastupdated
 * @property string $created
 * @property string $deleted
 * @property integer $isdeleted
 */
class BlogNewsletterPublication extends BaseBlogNewsletterPublication
{
  const _NEW = 1;
  const _INPROGRESS = 2;
  const _CLOSED = 3;

	protected $arStatus = array( 1 => 'New', 2 => 'In Progress', 3 => 'Closed' );

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseBlogNewsletterPublication the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function searchIndex()
	{
		$criteria=new CDbCriteria;
		if ( $this->idBlog !== '' && $this->idBlog !== 0 ) $criteria->compare('idBlog',$this->idBlog);
		if ( $this->status !== '' && $this->status !== 0 ) $criteria->compare('status',$this->status);
		$criteria->addCondition('isdeleted = 0');
		if ( $this->dateToSend !== '' && $this->dateToSend !== '2000-01-01 00:00:00' && $this->dateToSend !== '0000-00-00 00:00:00' )
			$criteria->compare('dateToSend',$this->dateToSend,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function blogName( $idBlog ) {
		$mBlog = Blog::model()->findByPk($idBlog);
		if ( !$mBlog ) $blogName = 'Undefined';
		else $blogName = $mBlog->name;

		return $blogName;
	}

	public function blogNames( ) {
		$arFinal = array();
		$mlBlog = Blog::model()->findAllByAttributes( array('isdeleted' => 0) );
		foreach( $mlBlog as $mBlog ) $arFinal[$mBlog->id] = $mBlog->name;

		return $arFinal;
	}

	public function blogStatus( $idStatus ) {
		if ( !isset( $this->arStatus[$idStatus] ) ) return 'Undefined';
		return $this->arStatus[$idStatus];
	}

	public function blogStatusFilter() {
		return $this->arStatus;
	}

	public function blogFecha( $fecha ) {
		return date("Y-m-d", strtotime($fecha));
	}

	public function blogFechaFilter() {
		$arFinal = array();
		$mlBlog = BlogNewsletterPublication::model()->findAllByAttributes( array('isdeleted' => 0) );
		foreach( $mlBlog as $mBlog ) {
			$fecha = date("Y-m-d", strtotime($mBlog->dateToSend));
			$arFinal[ $fecha ] = $fecha;
		}

		return $arFinal;
	}

}
