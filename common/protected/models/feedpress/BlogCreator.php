<?php

/**
 * This is the model class for table "blog_creator".
 *
 * The followings are the available columns in table 'blog_creator':
 * @property integer $id
 * @property string $name
 * @property string $created
 * @property string $deleted
 * @property integer $isdeleted
 */
class BlogCreator extends BaseBlogCreator
{
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseBlogCreator the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
