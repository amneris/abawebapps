<?php

/**
 * This is the model class for table "blog_post".
 *
 * The followings are the available columns in table 'blog_post':
 * @property integer $id
 * @property integer $idBlog
 * @property integer $idPost
 * @property string $title
 * @property string $link
 * @property string $urlImage
 * @property string $linkComments
 * @property string $pubDate
 * @property integer $idCreator
 * @property string $description
 * @property string $created
 * @property string $deleted
 * @property integer $isdeleted
 */
class BlogPost extends BaseBlogPost
{
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseBlogPost the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
