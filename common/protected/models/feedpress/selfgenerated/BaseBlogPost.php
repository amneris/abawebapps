<?php

/**
 * This is the model class for table "blog_post".
 *
 * The followings are the available columns in table 'blog_post':
 * @property integer $id
 * @property integer $idBlog
 * @property integer $idPost
 * @property string $title
 * @property string $link
 * @property string $urlImage
 * @property string $linkComments
 * @property string $pubDate
 * @property integer $idCreator
 * @property string $description
 * @property string $created
 * @property string $deleted
 * @property integer $isdeleted
 */
class BaseBlogPost extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'blog_post';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idBlog, idPost, title, link, urlImage, linkComments, idCreator, description', 'required'),
			array('idBlog, idPost, idCreator, isdeleted', 'numerical', 'integerOnly'=>true),
			array('title, link, urlImage, linkComments', 'length', 'max'=>256),
			array('description', 'length', 'max'=>2000),
			array('pubDate, created, deleted', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idBlog, idPost, title, link, urlImage, linkComments, pubDate, idCreator, description, created, deleted, isdeleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idBlog' => 'Id Blog',
			'idPost' => 'Id Post',
			'title' => 'Title',
			'link' => 'Link',
			'urlImage' => 'Url Image',
			'linkComments' => 'Link Comments',
			'pubDate' => 'Pub Date',
			'idCreator' => 'Id Creator',
			'description' => 'Description',
			'created' => 'Created',
			'deleted' => 'Deleted',
			'isdeleted' => 'Isdeleted',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idBlog',$this->idBlog);
		$criteria->compare('idPost',$this->idPost);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('urlImage',$this->urlImage,true);
		$criteria->compare('linkComments',$this->linkComments,true);
		$criteria->compare('pubDate',$this->pubDate,true);
		$criteria->compare('idCreator',$this->idCreator);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('deleted',$this->deleted,true);
		$criteria->compare('isdeleted',$this->isdeleted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbExtranet;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseBlogPost the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
