<?php

/**
 * CmAbaExperiments
 *
 * This is the model class for table "experiments".
 *
 * The followings are the available columns in table 'experiments':
 * @property integer $experimentId
 * @property integer $userCategoryId
 * @property integer $experimentTypeId
 * @property string $experimentIdentifier
 * @property string $experimentDescription
 * @property integer $counterUsers
 * @property integer $limitUsers
 * @property integer $randomMode
 * @property integer $availableModeId
 * @property string $dateStart
 * @property string $dateEnd
 * @property string $languages
 * @property integer $experimentStatus
 */
class CmAbaExperiments extends AbaActiveRecord
{

    const EXPERIMENT_RANDOM_MODE_SEQUENTIAL = 1;
    const EXPERIMENT_RANDOM_MODE_PHPRANDOM = 2;

    const EXPERIMENT_STATUS_ENABLED = 1;
    const EXPERIMENT_STATUS_PAUSED = 2;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'experiments';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaProductPrice the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
          array('experimentIdentifier, userCategoryId, experimentTypeId, experimentDescription, randomMode, availableModeId, dateStart, experimentStatus, limitUsers', 'required'),
          array(
            'experimentStatus, userCategoryId, experimentTypeId, counterUsers, limitUsers, randomMode, availableModeId',
            'numerical',
            'integerOnly' => true
          ),
          array('dateStart, dateEnd', 'date', 'format' => 'yyyy-mm-dd hh:mm:ss', 'allowEmpty' => true),
          array('dateEnd, languages', 'type', 'type' => 'string', 'allowEmpty' => true),
          array(
            'experimentIdentifier',
            'match',
            'pattern' => '/^[A-Za-z0-9_]+$/u',
            'message' => 'Experiment identifier can contain only alphanumeric characters.'
          ),
            // @todo Please remove those attributes that should not be searched.
          array(
            'experimentId, userCategoryId, experimentTypeId, experimentIdentifier, experimentDescription, counterUsers, limitUsers, randomMode, availableModeId,
                dateStart, dateEnd, experimentStatus, languages',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " ex.`experimentId`, ex.`userCategoryId`, ex.`experimentTypeId`, ex.`experimentIdentifier`, ex.`experimentDescription`, ex.`counterUsers`, ex.`limitUsers`, ex.`randomMode`, ex.`languages`, ex.`availableModeId`, ex.`dateStart`, ex.`dateEnd`, ex.`experimentStatus` ";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @param $experimentId
     * @param null $iStatus
     *
     * @return $this|bool
     */
    public function getExperimentById($experimentId, $iStatus = null)
    {
        $paramsToSQL = array(":EXPERIMENTID" => $experimentId);

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " ex
            WHERE ex.experimentId = :EXPERIMENTID
        ";

        if (is_numeric($iStatus)) {
            $paramsToSQL[":EXPERIMENTSTATUS"] = $iStatus;

            $sSql .= "
                AND ex.experimentStatus = :EXPERIMENTSTATUS
            ";
        }

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return array|bool
     */
    public function getAllExperiments()
    {
        $sSql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " AS ex
        ";

        $paramsToSQL = array();
        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (!$dataReader) {
            return false;
        }

        $aProds = array();
        while (($row = $dataReader->read()) !== false) {
            $aProds[] = array(
              "experimentId" => $row['experimentId'],
              "experimentIdentifier" => $row['experimentIdentifier'],
              "experimentDescription" => $row['experimentDescription'],
            );
        }

        return $aProds;
    }

    /**
     * @return bool
     */
    public function incrementExperimentCounter()
    {
        $this->beginTrans();

        $sSql = "
            UPDATE " . $this->tableName() . "
                SET counterUsers = counterUsers + 1
            WHERE
                experimentId = " . $this->experimentId . "
        ";

        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

}
