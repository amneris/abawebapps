<?php

/**
 * CmAbaExperimentsTypes
 *
 * This is the model class for table "experiments_types".
 *
 * The followings are the available columns in table 'experiments_types':
 * @property integer $experimentTypeId
 * @property string $experimentTypeIdentifier
 * @property string $experimentTypeDescription
 * @property string $dateAdd
 */
class CmAbaExperimentsTypes extends AbaActiveRecord
{

    const EXPERIMENT_TYPE_MOBILE_STYLE_CSS = 1;
    const EXPERIMENT_TYPE_SCENARIOS_PAYMENTS = 2;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'experiments_types';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaProductPrice the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
          array('experimentTypeIdentifier, experimentTypeDescription, dateAdd', 'required'),
          array(
            'experimentTypeId',
            'numerical',
            'integerOnly' => true
          ),
          array('dateAdd', 'date', 'format' => 'yyyy-mm-dd hh:mm:ss', 'allowEmpty' => true),
            // @todo Please remove those attributes that should not be searched.
          array(
            'experimentTypeId, experimentTypeIdentifier, experimentTypeDescription, dateAdd',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " ext.`experimentTypeId`, ext.`experimentTypeIdentifier`, ext.`experimentTypeDescription`, ext.`dateAdd` ";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @param $experimentTypeId
     *
     * @return $this|bool
     */
    public function getExperimentTypeById($experimentTypeId)
    {
        $paramsToSQL = array(":EXPERIMENTTYPEID" => $experimentTypeId);

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " ext
            WHERE ext.experimentTypeId = :EXPERIMENTTYPEID
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $experimentTypeIdentifier
     *
     * @return $this|bool
     */
    public function getExperimentTypeByIdentifier($experimentTypeIdentifier)
    {
        $paramsToSQL = array(":EXPERIMENTTYPEIDENTIFIER" => $experimentTypeIdentifier);

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " ext
            WHERE ext.experimentTypeIdentifier = :EXPERIMENTTYPEIDENTIFIER
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return array|bool
     */
    public function getAllExperimentsTypes()
    {
        $sSql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " AS ext
        ";

        $paramsToSQL = array();
        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (!$dataReader) {
            return false;
        }

        $aProds = array();
        while (($row = $dataReader->read()) !== false) {
            $aProds[] = array(
              "experimentTypeId" => $row['experimentTypeId'],
              "experimentTypeIdentifier" => $row['experimentTypeIdentifier'],
              "experimentTypeDescription" => $row['experimentTypeDescription'],
            );
        }

        return $aProds;
    }

}
