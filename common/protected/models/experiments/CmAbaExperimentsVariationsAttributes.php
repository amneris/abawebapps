<?php

/**
 * Specific scenarios payments class
 *
 * CmAbaExperimentsVariationsAttributes
 *
 * This is the model class for table "experiments_variations_attributes".
 *
 * The followings are the available columns in table 'experiments_variations_attributes':
 * @property integer $experimentVariationAttributeId
 * @property integer $experimentVariationId
 * @property integer $idProduct
 * @property integer $idCountry
 * @property float $price
 * @property integer $enabled
 */
class CmAbaExperimentsVariationsAttributes extends AbaActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'experiments_variations_attributes_scenarios';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaProductPrice the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
          array('experimentVariationId, idProduct, idCountry', 'required'),
          array('enabled', 'numerical', 'integerOnly' => true),
          array('price', 'type', 'type' => 'float', 'allowEmpty' => false),
            // @todo Please remove those attributes that should not be searched.
          array(
            'experimentVariationAttributeId, experimentVariationId, idProduct, idCountry, price, enabled',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " exva.`experimentVariationAttributeId`, exva.`experimentVariationId`, exva.`idProduct`, exva.`idCountry`, exva.`price`, exva.`enabled` ";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @param $idProductsGroup
     * @param null $bEnabled
     *
     * @return $this|bool
     */
    public function getExperimentsVariationsAttributesPriceById($experimentVariationAttributeId, $bEnabled = null)
    {
        $paramsToSQL = array(":experimentVariationAttributeId" => $experimentVariationAttributeId);
        $sSql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " exva WHERE exva.experimentVariationAttributeId = :experimentVariationAttributeId ";

        if (is_numeric($bEnabled)) {
            $sSql .= " AND exva.enabled = :ENABLED ";
            $paramsToSQL[":ENABLED"] = $bEnabled;
        }

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $idCountry
     *
     * @return array
     */
    public function getAllAvailablesPricesByCountry($idCountry)
    {

        $paramsToSQL = array(
          ":IDCOUNTRY" => $idCountry,
          ":EXPERIMENTTYPE" => CmAbaExperimentsTypes::EXPERIMENT_TYPE_SCENARIOS_PAYMENTS,
          ":EXPERIMENTSTATUS" => CmAbaExperiments::EXPERIMENT_STATUS_ENABLED
        );

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " exva
            JOIN experiments_variations AS exv
                ON exv.experimentVariationId = exva.experimentVariationId
                AND exv.counterUsers < exv.limitUsers
            JOIN experiments AS ex
                ON ex.experimentId = exv.experimentId
                AND ex.`experimentTypeId` = :EXPERIMENTTYPE
                AND NOW() BETWEEN ex.dateStart AND IFNULL(ex.dateEnd, NOW())
                AND ex.counterUsers < ex.limitUsers
                AND ex.experimentStatus = :EXPERIMENTSTATUS
            WHERE
                exva.idCountry = :IDCOUNTRY
                AND exva.enabled = 1
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        $aProdsPrices = array();

        if (!$dataReader) {
            return $aProdsPrices;
        }

        while (($row = $dataReader->read()) !== false) {
            $aProdsPrices[] = $row;
        }
        return $aProdsPrices;
    }

    /**
     * @param $countryId
     * @param $experimentVariationId
     *
     * @return array
     */
    public function getAllAvailableUserExperimentsVariationsPricesByCountry($countryId, $experimentVariationId)
    {

        $paramsToSQL = array(
          ":IDCOUNTRY" => $countryId,
          ":EXPERIMENTVARIATIONID" => $experimentVariationId,
          ":EXPERIMENTTYPE" => ExperimentsTypes::EXPERIMENT_TYPE_SCENARIOS_PAYMENTS,
          ":EXPERIMENTSTATUS" => CmAbaExperiments::EXPERIMENT_STATUS_ENABLED
        );

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " exva
            JOIN experiments_variations AS exv
                ON exv.experimentVariationId = exva.experimentVariationId
                AND exv.counterUsers < exv.limitUsers
            JOIN experiments AS ex
                ON ex.experimentId = exv.experimentId
                AND ex.`experimentTypeId` = :EXPERIMENTTYPE
                AND NOW() BETWEEN ex.dateStart AND IFNULL(ex.dateEnd, NOW())
                AND ex.counterUsers < ex.limitUsers
                AND ex.experimentStatus = :EXPERIMENTSTATUS
            WHERE
                exva.idCountry = :IDCOUNTRY
                exva.experimentVariationId = :EXPERIMENTVARIATIONID
                AND exva.enabled = 1
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        $aProdsPrices = array();

        if (!$dataReader) {
            return $aProdsPrices;
        }

        while (($row = $dataReader->read()) !== false) {
            $aProdsPrices[] = $row;
        }
        return $aProdsPrices;
    }

    /**
     * @param $experimentVariationId
     * @param $countryId
     * @param bool|true $bEnabled
     *
     * @return array
     */
    public function getUserExperimentsVariationsByCountryId($experimentVariationId, $countryId, $bEnabled = true)
    {

        $paramsToSQL = array(
          ":IDCOUNTRY" => $countryId,
          ":EXPERIMENTVARIATIONID" => $experimentVariationId,
          ":EXPERIMENTTYPE" => ExperimentsTypes::EXPERIMENT_TYPE_SCENARIOS_PAYMENTS,
          ":EXPERIMENTSTATUS" => CmAbaExperiments::EXPERIMENT_STATUS_ENABLED
        );

        $sSql = "
            SELECT
                " . $this->mainFields() . ",
                exv.`experimentVariationId`, ex.`dateStart`, ex.`dateEnd`
            FROM " . $this->tableName() . " exva
            JOIN experiments_variations AS exv
                ON exv.experimentVariationId = exva.experimentVariationId
            JOIN experiments AS ex
                ON ex.experimentId = exv.experimentId
                AND ex.`experimentTypeId` = :EXPERIMENTTYPE
                AND NOW() BETWEEN ex.dateStart AND IFNULL(ex.dateEnd, NOW())
                AND ex.experimentStatus = :EXPERIMENTSTATUS
        ";

        $sSql .= "
            WHERE
                exva.experimentVariationId = :EXPERIMENTVARIATIONID
                AND exva.idCountry = :IDCOUNTRY
        ";

        if ($bEnabled) {
            $paramsToSQL[":ENABLED"] = 1;

            $sSql .= "
                AND exva.enabled = :ENABLED
            ";
        }

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        $aProdsPrices = array();

        if (!$dataReader) {
            return $aProdsPrices;
        }

        while (($row = $dataReader->read()) !== false) {
            $aProdsPrices[] = $row;
        }
        return $aProdsPrices;
    }

}
