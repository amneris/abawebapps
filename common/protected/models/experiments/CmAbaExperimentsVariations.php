<?php

/**
 * CmAbaExperimentsVariations
 *
 * This is the model class for table "experiments_variations".
 *
 * The followings are the available columns in table 'experiments_variations':
 * @property integer $experimentVariationId
 * @property integer $experimentId
 * @property integer $experimentVariationIdentifier
 * @property string $experimentVariationDescription
 * @property integer $counterUsers
 * @property integer $limitUsers
 * @property integer $isDefault
 * @property integer $experimentVariationStatus
 */
class CmAbaExperimentsVariations extends AbaActiveRecord
{

    const GROUP_STATUS_INTEST = 1;
    const GROUP_STATUS_FAIL_TEST = 2;
    const GROUP_STATUS_SUCCESS_TEST = 3;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'experiments_variations';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaProductPrice the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
//            array('dateStart', 'required'),
          array(
            'experimentVariationId, experimentId, counterUsers, limitUsers, isDefault, experimentVariationStatus',
            'numerical',
            'integerOnly' => true
          ),
          array(
            'experimentVariationIdentifier',
            'match',
            'pattern' => '/^[A-Za-z0-9_]+$/u',
            'message' => 'Experiment variation identifier can contain only alphanumeric characters.'
          ),
          array('experimentVariationDescription', 'type', 'type' => 'string', 'allowEmpty' => true),
//            array('dateStart, dateEnd', 'date', 'format' => 'yyyy-mm-dd hh:mm:ss'),
            // @todo Please remove those attributes that should not be searched.
          array(
            'experimentVariationId, experimentId, experimentVariationIdentifier, experimentVariationDescription, counterUsers, limitUsers, isDefault, experimentVariationStatus',
            'safe',
            'on' => 'search'
          ),
        );
    }


    /**
     * @return string
     */
    public function mainFields()
    {
        return " exv.`experimentVariationId`, exv.`experimentId`, exv.`experimentVariationIdentifier`, exv.`experimentVariationDescription`, exv.`counterUsers`, exv.`limitUsers`, exv.`isDefault`, exv.`experimentVariationStatus` ";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @param $experimentVariationId
     * @param null $iExperimentStatus
     *
     * @return $this|bool
     */
    public function getExperimentVariationById($experimentVariationId, $iExperimentStatus = null)
    {
        $paramsToSQL = array(":EXPERIMENTVARIATIONID" => $experimentVariationId);

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " exv
            WHERE exv.experimentVariationId = :EXPERIMENTVARIATIONID
        ";

        if (is_numeric($iExperimentStatus)) {
            $paramsToSQL[":EXPERIMENTSTATUS"] = $iExperimentStatus;

            $sSql .= "
            JOIN experiments AS ex
                ON ex.experimentId = exv.experimentId
                AND ex.experimentStatus = :EXPERIMENTSTATUS
            ";
        }

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * Specific scenarios payments function
     *
     * @param $countryId
     * @param $experimentTypeId
     * @param bool|true $bWithoutDefaultGroup
     *
     * @return array
     */
    public function getAllAvailablesScenariosPricesByCountryForRandom($countryId, $experimentTypeId, $bWithoutDefaultGroup = true)
    {

        $paramsToSQL = array(":COUNTRYID" => $countryId, ":EXPERIMENTTYPE" => $experimentTypeId);

        $sSql = "
            SELECT " . $this->mainFields() . ", ex.languages
            FROM " . $this->tableName() . " exv
            JOIN experiments AS ex
                ON ex.experimentId = exv.experimentId
                AND ex.`experimentTypeId` = :EXPERIMENTTYPE
                AND NOW() BETWEEN ex.dateStart AND IFNULL(ex.dateEnd, NOW())
                AND ex.experimentStatus = '" . CmAbaExperiments::EXPERIMENT_STATUS_ENABLED . "'
                AND ex.counterUsers < ex.limitUsers
            JOIN experiments_variations_attributes_scenarios AS prgp
                ON exv.experimentVariationId = prgp.experimentVariationId
                AND prgp.idCountry = :COUNTRYID
                AND prgp.enabled = 1
            WHERE
                exv.counterUsers < exv.limitUsers
        ";

        if ($bWithoutDefaultGroup) {
            $sSql .= "
                AND exv.isDefault = 0
            ";
        }

        $sSql .= "
            GROUP BY exv.experimentVariationId
            ORDER BY exv.experimentVariationId ASC
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        $aProdsPrices = array();

        if (!$dataReader) {
            return $aProdsPrices;
        }

        while (($row = $dataReader->read()) !== false) {
            $aProdsPrices[] = $row;
        }
        return $aProdsPrices;
    }

    /**
     * Specific scenarios payments function
     *
     * @param $countryId
     * @param $experimentTypeId
     * @param bool|true $bWithoutDefaultGroup
     *
     * @return array
     */
    public function getAvailableScenariosPricesByCountryForRandom(
      $countryId,
      $experimentTypeId,
      $bWithoutDefaultGroup = true
    ) {

        $paramsToSQL = array(":COUNTRYID" => $countryId, ":EXPERIMENTTYPE" => $experimentTypeId);

        $sSql = "
            SELECT " . $this->mainFields() . ", ex.languages
            FROM " . $this->tableName() . " exv
            JOIN experiments AS ex
                ON ex.experimentId = exv.experimentId
                AND ex.`experimentTypeId` = :EXPERIMENTTYPE
                AND NOW() BETWEEN ex.dateStart AND IFNULL(ex.dateEnd, NOW())
                AND ex.experimentStatus = '" . CmAbaExperiments::EXPERIMENT_STATUS_ENABLED . "'
                AND ex.counterUsers < ex.limitUsers
            JOIN experiments_variations_attributes_scenarios AS prgp
                ON exv.experimentVariationId = prgp.experimentVariationId
                AND prgp.idCountry = :COUNTRYID
                AND prgp.enabled = 1
            WHERE
                exv.counterUsers < exv.limitUsers
        ";

        if ($bWithoutDefaultGroup) {
            $sSql .= "
                AND exv.isDefault = 0
            ";
        }

        $sSql .= "
            GROUP BY exv.experimentVariationId, exv.counterUsers
            ORDER BY exv.counterUsers ASC, exv.experimentVariationId ASC
            LIMIT 0, 1
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        $aProdsPrices = array();

        if (!$dataReader) {
            return $aProdsPrices;
        }

        while (($row = $dataReader->read()) !== false) {
            $aProdsPrices[] = $row;
        }
        return $aProdsPrices;
    }

    /**
     * @TODO @TODO @TODO
     * @TODO @TODO @TODO
     * @TODO @TODO @TODO
     *
     * @param $countryId
     * @param $experimentTypeId
     *
     * @return bool
     */
    public function getRandomExperimentVariationId($countryId, $experimentTypeId, $language='en')
    {

        $iEnabledExperiments = Yii::app()->config->get("ENABLED_EXPERIMENTS");

        if($iEnabledExperiments <> 1) {
            return false;
        }

        $stAllAvailableGroups = array();

        $iRandomMode = Yii::app()->config->get("EXPERIMENT_RANDOM_MODE_DEFAULT");

        if ($iRandomMode == CmAbaExperiments::EXPERIMENT_RANDOM_MODE_SEQUENTIAL) {

            //
            // sequential
            //
            switch($experimentTypeId) {
                case CmAbaExperimentsTypes::EXPERIMENT_TYPE_MOBILE_STYLE_CSS:
                    $stAllAvailableGroups = $this->getAvailableExperimentsForRandom($experimentTypeId, false);
                    break;
                case CmAbaExperimentsTypes::EXPERIMENT_TYPE_SCENARIOS_PAYMENTS:
                    $stAllAvailableGroups = $this->getAvailableScenariosPricesByCountryForRandom($countryId, $experimentTypeId, true);
                    break;
            }

            // filtrar
            $stAllAvailableGroups = self::filterExperimentsAvailablesUsersByLanguage($stAllAvailableGroups, $language);

            foreach ($stAllAvailableGroups as $stAvailableGroup) {
                return $stAvailableGroup["experimentVariationId"];
            }

        } elseif ($iRandomMode == CmAbaExperiments::EXPERIMENT_RANDOM_MODE_PHPRANDOM) {
            //
            // random
            //
            switch($experimentTypeId) {
                case CmAbaExperimentsTypes::EXPERIMENT_TYPE_MOBILE_STYLE_CSS:
                    $stAllAvailableGroups = $this->getAllAvailablesExperimentsForRandom($experimentTypeId, false);
                    break;
                case CmAbaExperimentsTypes::EXPERIMENT_TYPE_SCENARIOS_PAYMENTS:
                    $stAllAvailableGroups = $this->getAllAvailablesScenariosPricesByCountryForRandom($countryId, $experimentTypeId, true);
                    break;
            }

            // filtrar
            $stAllAvailableGroups = self::filterExperimentsAvailablesUsersByLanguage($stAllAvailableGroups, $language);

            $stAllFormattedAvailableGroups = array();

            $iGroupNumber = 0;

            foreach ($stAllAvailableGroups as $stAvailableGroup) {
                $stAllFormattedAvailableGroups[] = array(
                  'groupNumber' => $iGroupNumber,
                  'experimentVariationId' => $stAvailableGroup['experimentVariationId'],
                  'counterUsers' => $stAvailableGroup['counterUsers'],
                  'limitUsers' => $stAvailableGroup['limitUsers'],
                );

                ++$iGroupNumber;
            }

            if (count($stAllFormattedAvailableGroups) > 0) {
                $iRandom = mt_rand(0, (count($stAllFormattedAvailableGroups)) - 1);

                $stSelectedGroup = $stAllFormattedAvailableGroups[$iRandom];

                return $stSelectedGroup["experimentVariationId"];
            }
        }
        return false;
    }


    /**
     * @param $stAllProfiles
     * @param $language
     *
     * @return array
     */
    protected static function filterExperimentsAvailablesUsersByLanguage($stAllProfiles, $language)
    {
        $stProfiles = array();

        // filter availables user profiles
        foreach ($stAllProfiles as $iKey => $stProfile) {
//            if (preg_match("/" . $language . "/i", $stProfile["languages"])) {
            if(HeMixed::filterCountry($language, $stProfile["languages"])) {
                $stProfiles[$iKey] = $stProfile;
            }
        }
        return $stProfiles;
    }


    /**
     * @return bool
     */
    public function incrementExperimentVariationCounter()
    {
        $this->beginTrans();

        $sSql = "
            UPDATE " . $this->tableName() . "
                SET counterUsers = counterUsers + 1
            WHERE
                experimentVariationId = " . $this->experimentVariationId . "
        ";

        if ($this->updateSQL($sSql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

    /**
     * @return array|bool
     */
    public function getAllExperimentsVariations()
    {
        $sSql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " AS exv
        ";

        $paramsToSQL = array();
        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (!$dataReader) {
            return false;
        }

        $aProds = array();
        while (($row = $dataReader->read()) !== false) {
            $aProds[] = array(
              "experimentVariationId" => $row['experimentVariationId'],
              "experimentVariationIdentifier" => $row['experimentVariationIdentifier'],
              "experimentVariationDescription" => $row['experimentVariationDescription'],
            );
        }

        return $aProds;
    }

    /**
     * @param $experimentTypeId
     *
     * @return array
     */
    public function getAllAvailableExperimentsVariationsByExperimentType(
      $experimentTypeId
    ) {
        $paramsToSQL = array(
          ":EXPERIMENTTYPE" => $experimentTypeId,
          ":EXPERIMENTSTATUS" => CmAbaExperiments::EXPERIMENT_STATUS_ENABLED,
        );

        $sSql = "
            SELECT
              exv.experimentVariationIdentifier, ex.experimentId, ex.experimentIdentifier, ex.userCategoryId
              " . "
            FROM " . $this->tableName() . " exv

            JOIN experiments AS ex
                ON exv.experimentId = ex.experimentId
                AND ex.experimentTypeId = :EXPERIMENTTYPE
                AND NOW() BETWEEN ex.dateStart AND IFNULL(ex.dateEnd, NOW())
                AND ex.experimentStatus = :EXPERIMENTSTATUS
                AND ex.counterUsers < ex.limitUsers
        ";

        $sSql .= "
            WHERE
                exv.counterUsers < exv.limitUsers
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        $aUserExperiments = array();

        if (!$dataReader) {
            return $aUserExperiments;
        }

        while (($row = $dataReader->read()) !== false) {
            $aUserExperiments[] = $row;
        }
        return $aUserExperiments;
    }


    /**
     * @param $experimentTypeId
     * @param bool|true $bWithoutDefaultGroup
     *
     * @return array
     */
    public function getAvailableExperimentsForRandom(
        $experimentTypeId,
        $bWithoutDefaultGroup = true
    ) {

        $paramsToSQL = array(
          ":EXPERIMENTTYPE" => $experimentTypeId,
          ":EXPERIMENTSTATUS" => CmAbaExperiments::EXPERIMENT_STATUS_ENABLED,
        );

        $sSql = "
            SELECT " . $this->mainFields() . ", ex.languages
            FROM " . $this->tableName() . " exv
            JOIN experiments AS ex
                ON ex.experimentId = exv.experimentId
                AND ex.`experimentTypeId` = :EXPERIMENTTYPE
                AND NOW() BETWEEN ex.dateStart AND IFNULL(ex.dateEnd, NOW())
                AND ex.experimentStatus = :EXPERIMENTSTATUS
                AND ex.counterUsers < ex.limitUsers
            WHERE
                exv.counterUsers < exv.limitUsers
        ";

        if ($bWithoutDefaultGroup) {
            $sSql .= "
                AND exv.isDefault = 0
            ";
        }

        $sSql .= "
            GROUP BY exv.experimentVariationId, exv.counterUsers
            ORDER BY exv.counterUsers ASC, exv.experimentVariationId ASC
            LIMIT 0, 1
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        $aExperiments = array();

        if (!$dataReader) {
            return $aExperiments;
        }

        while (($row = $dataReader->read()) !== false) {
            $aExperiments[] = $row;
        }
        return $aExperiments;
    }

    /**
     * @param $experimentTypeId
     * @param bool|true $bWithoutDefaultGroup
     *
     * @return array
     */
    public function getAllAvailablesExperimentsForRandom($experimentTypeId, $bWithoutDefaultGroup = true)
    {

        $paramsToSQL = array(":EXPERIMENTTYPE" => $experimentTypeId);

        $sSql = "
            SELECT " . $this->mainFields() . ", ex.languages
            FROM " . $this->tableName() . " exv
            JOIN experiments AS ex
                ON ex.experimentId = exv.experimentId
                AND ex.`experimentTypeId` = :EXPERIMENTTYPE
                AND NOW() BETWEEN ex.dateStart AND IFNULL(ex.dateEnd, NOW())
                AND ex.experimentStatus = '" . CmAbaExperiments::EXPERIMENT_STATUS_ENABLED . "'
                AND ex.counterUsers < ex.limitUsers
            WHERE
                exv.counterUsers < exv.limitUsers
        ";

        if ($bWithoutDefaultGroup) {
            $sSql .= "
                AND exv.isDefault = 0
            ";
        }

        $sSql .= "
            GROUP BY exv.experimentVariationId
            ORDER BY exv.experimentVariationId ASC
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        $aProdsPrices = array();

        if (!$dataReader) {
            return $aProdsPrices;
        }

        while (($row = $dataReader->read()) !== false) {
            $aProdsPrices[] = $row;
        }
        return $aProdsPrices;
    }


    /**
     * Specific scenarios payments function
     *
     * @TODO  -  INSERT, CHANGE STATUS, ACTUALIZAR (BORRAR) DE LA TABLA DE GRUPOS X USUARIO EL TEST QUE HA FALLADO
     */

}
