<?php

/**
 * CmAbaExperimentsAvailabilityModes
 *
 * This is the model class for table "experiments_availability_modes".
 *
 * The followings are the available columns in table 'experiments_availability_modes':
 * @property integer $availableModeId
 * @property string $availableModeDescription
 * @property string $dateAdd
 */
class CmAbaExperimentsAvailabilityModes extends AbaActiveRecord
{

    const EXPERIMENT_AVAILABLE_MODE_WHILE_EXPERIMENT = 1;
    const EXPERIMENT_AVAILABLE_MODE_ALWAYS = 2;
    const EXPERIMENT_AVAILABLE_MODE_AFTER = 3; // after finish experiment

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'experiments_availability_modes';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaProductPrice the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
          array('availableModeDescription, dateAdd', 'required'),
          array(
            'availableModeId',
            'numerical',
            'integerOnly' => true
          ),
          array('dateAdd', 'date', 'format' => 'yyyy-mm-dd hh:mm:ss', 'allowEmpty' => true),
            // @todo Please remove those attributes that should not be searched.
          array(
            'availableModeId, availableModeDescription, dateAdd',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " exam.`availableModeId`, exam.`availableModeDescription`, exam.`dateAdd` ";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @param $availableModeId
     *
     * @return $this|bool
     */
    public function getExperimentAvailabilityModeById($availableModeId)
    {
        $paramsToSQL = array(":AVAILABLEMODEID" => $availableModeId);

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " exam
            WHERE exam.availableModeId = :AVAILABLEMODEID
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return array|bool
     */
    public function getAllExperimentsAvailabilityModes()
    {
        $sSql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " AS exam
        ";

        $paramsToSQL = array();
        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (!$dataReader) {
            return false;
        }

        $aProds = array();
        while (($row = $dataReader->read()) !== false) {
            $aProds[] = array(
              "availableModeId" => $row['availableModeId'],
              "availableModeDescription" => $row['availableModeDescription'],
            );
        }

        return $aProds;
    }

}
