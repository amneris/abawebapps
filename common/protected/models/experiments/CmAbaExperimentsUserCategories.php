<?php

/**
 * CmAbaExperimentsUserCategories
 *
 * This is the model class for table "experiments_user_categories".
 *
 * The followings are the available columns in table 'experiments_user_categories':
 * @property integer $userCategoryId
 * @property string $userCategoryDescription
 * @property string $dateAdd
 */
class CmAbaExperimentsUserCategories extends AbaActiveRecord
{

    const EXPERIMENT_CATEGORY_FREE_PREMIUM = 1;
    const EXPERIMENT_CATEGORY_FREE = 2;
    const EXPERIMENT_CATEGORY_PREMIUM = 3;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'experiments_user_categories';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CmAbaProductPrice the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
          array('userCategoryDescription, dateAdd', 'required'),
          array(
            'userCategoryId',
            'numerical',
            'integerOnly' => true
          ),
          array('dateAdd', 'date', 'format' => 'yyyy-mm-dd hh:mm:ss', 'allowEmpty' => true),
            // @todo Please remove those attributes that should not be searched.
          array(
            'userCategoryId, userCategoryDescription, dateAdd',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " exuc.`userCategoryId`, exuc.`userCategoryDescription`, exuc.`dateAdd` ";
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @param $userCategoryId
     *
     * @return $this|bool
     */
    public function getExperimentUserCategoryById($userCategoryId)
    {
        $paramsToSQL = array(":USERCATEGORYID" => $userCategoryId);

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " exuc
            WHERE exuc.userCategoryId = :USERCATEGORYID
        ";

        $dataReader = $this->querySQL($sSql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return array|bool
     */
    public function getAllExperimentsUserCategories()
    {
        $sSql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " AS exuc
        ";

        $paramsToSQL = array();
        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (!$dataReader) {
            return false;
        }

        $aProds = array();
        while (($row = $dataReader->read()) !== false) {
            $aProds[] = array(
              "userCategoryId" => $row['userCategoryId'],
              "userCategoryDescription" => $row['userCategoryDescription'],
            );
        }

        return $aProds;
    }

}
