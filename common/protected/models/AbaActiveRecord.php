<?php

class AbaActiveRecord extends CActiveRecord
{
    const DB_DEFAULT = 'db';
    const DB_CAMPUS = 'db';
    const DB_SLAVE = 'dbSlave';
    const DB_LOGS = 'dbLogs';
    const DB_EXTRANET = 'dbExtranet';
    const DB_WEBSERVICE = 'dbWebservice';
    public static $dbSlave;
    /* @var CDbConnection  $connection*/
    protected $connection;
    /* @Deprecated $dbReadOnly */
    private $dbReadOnly = false; // For debugging purposes only, if it's true do not execute INSERT or UPDATE
    public $aFieldsNames = array();

    /* private properties from CActiveRecord */
    private static $_models=array();			// class name => model
    private static $_md=array();				// class name => meta data

    private $_new=false;						// whether this instance is new or not
//    private $_attributes=array();				// attribute name => attribute value
    private $_related=array();					// attribute name => related objects
    private $_c;								// query criteria (used by finder only)
    private $_pk;								// old primary key value
    private $_alias='t';						// the table alias being used for query


    private $cacheID = 'cache';
    private $_cache;

    /**
     * Overrides constructor of Yii
     * @param string $scenario scenario name. See {@link CModel::scenario} for more details about this parameter.
     * @param optional string $dbConnName
     *
     */
    public function __construct( $scenario = null, $dbConnName = null)
    {
        if (!empty($dbConnName)) {
            if ($dbConnName == AbaActiveRecord::DB_SLAVE) {
                $this->connection = $this->getSlaveDbConnection();
            } else {
                $this->connection = $this->getDbConnection();
            }
        } else {
            $this->connection = $this->getDbConnection();
        }
        parent::__construct($scenario);
        return;
    }

    /**
     * @return mixed
     * @throws CDbException
     */
    public function getSlaveDbConnection()
    {

        /* @TODO: Quino must do refactor of connections to be able for every project/model class/object instance/method
         * to override connections by default */
        $this->dbAba = $this->getDbConnection();
        if (self::$dbSlave !== null) {
            return self::$dbSlave;
        } else {
            self::$dbSlave = $this->getSlaveDb();
            if (self::$dbSlave instanceof CDbConnection) {
                self::$dbSlave->setActive(true);
                return self::$dbSlave;
            } else {
                throw new CDbException(Yii::t('yii', 'Active Record requires a "dbSlave" CDbConnection application component.'));
            }
        }
        /* @TODO: Quino must do refactor of connections to be able for every project/model class/object instance/method
         * to override connections by default */
         $this->dbAba = $this->getDbConnection();
    }

    /**
     * @return mixed
     */
    public function getSlaveDb()
    {
        return Yii::app()->getComponent(AbaActiveRecord::DB_SLAVE);
    }

    protected function selectDbConnection(){
        $dbConnection = null;
        if (Yii::app()->config->get("MASTER_SLAVE_SELECTOR") == 1) {
            $dbConnection = AbaActiveRecord::DB_SLAVE;
        }
        return $dbConnection;
    }

    /** Magic setter to convert columns of table to properties of this class.
     * @param string $name
     * @param mixed  $value
     *
     * @return mixed|void
     */
    public function __set($name, $value)
    {
        if (is_array($value)) {
            parent::__set($name, $value);
        } else {
            if ($this->setAttribute($name, $value)===false) {
                if (isset($this->getMetaData()->relations[$name])) {
                    $this->_related[$name]=$value;
                } else {
                    if (property_exists($this, $name)) {
                        parent::__set($name, $value);
                    } else {
                        $this->$name=$value;
                    }
                }
            }
        }
    }

    /**
     * @static
     *
     * @param string $className
     *
     * @return array|CActiveRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @throws CDbException
     * returns true
     */
    public function beginTrans()
    {
        if (Yii::app()->params["isTransRunning"]) {
            throw new CDbException(" A new transaction tried to start running when already one was active.");
        }
        if (Yii::app()->params['DbTransactional']) {
            Yii::app()->params["isTransRunning"] = true;
            Yii::app()->params['transRunning'] = $this->connection->beginTransaction();
            return true;
        }
        return true;
    }

    /**
     * @throws CdbException
     * returns true
     */
    public function commitTrans()
    {
        if (Yii::app()->params['DbTransactional'] && !Yii::app()->params["isTransRunning"]) {
            throw new CDbException(" A new transaction tried to finish running when none was begun.");
        }
        if (Yii::app()->params['DbTransactional']) {
            Yii::app()->params['transRunning']->commit();
            Yii::app()->params["isTransRunning"] = false;
            return true;
        }
        return true;
    }

    /**
     * @throws CDbException
     * returns true
     */
    public function rollbackTrans()
    {
        if (Yii::app()->params['DbTransactional'] && !Yii::app()->params["isTransRunning"]) {
            throw new CDbException(" A new transaction tried to rollback running when none was begun.");
        }
        if(Yii::app()->params['DbTransactional']) {
            Yii::app()->params['transRunning']->rollback();
            Yii::app()->params["isTransRunning"] = false;
            return true;
        }
        return true;
    }

    /**
     * @param $variable
     *
     * @return int
     */
    private function transPHPTypePDO($variable)
    {
        switch( gettype($variable))
        {
            case 'boolean':
                return PDO::PARAM_BOOL;
                break;
            case 'integer':
                return PDO::PARAM_INT;
                break;
            case 'double':
                return PDO::PARAM_STR;
                break;
            case 'string':
                return PDO::PARAM_STR;
                break;
            case 'null':
                return PDO::PARAM_NULL;
                break;
            default:
                return PDO::PARAM_STR;
                break;
        }
    }

    /** Performs a single SQL Select query on default or customized connection.
     *
     * @param $sql
     * @param array $aBindParams
     * @param string $dbConnName  DB_DEFAULT , DB_SLAVE ,  DB_LOGS , DB_EXTRANET , DB_LEARNING , DB_WEBSERVICE
     *
     * @return CDbDataReader
     */
    final public function querySQL($sql, $aBindParams=array(), $dbConnName=null)
    {
        if (!empty($dbConnName)) {
            $this->connection = Yii::app()->getComponent($dbConnName);
        }

        /* @var CDbCommand $command */
        $command = $this->connection->createCommand($sql);

        if (count($aBindParams) > 0) {
            $command->bindValues($aBindParams);
        }

        Yii::trace($command->getText(), "sql.select");
        $dataReader = $command->query();
        return $dataReader;

    }

    /** Return all Rows from the query SQL
     * @param string $sql
     * @param array $aBindParams
     * @param string $dbConnName 'dbSlave' in case we want to connect to slave
     *
     * @return array|bool
     */
    final public function queryAllSQL($sql, $aBindParams=array(), $dbConnName=null)
    {
        if (!empty($dbConnName)){
            $this->connection = Yii::app()->getComponent($dbConnName);
        }

        /* @var CDbCommand $command*/
        $command = $this->connection->createCommand($sql);

        if(count($aBindParams)>0)
        {
            $command->bindValues($aBindParams);
        }

      Yii::trace($command->getText(), "sql.select");
        $dataReader=$command->queryAll();
        if( empty($dataReader) || count($dataReader)==0){
            return false;
        }

        return $dataReader;
    }

    /** Execute an INSERT and return the Last Id Inserted or num Rows affected
     * @param       $sql
     * @param array $aBindParams
     * @param bool $returnNumRows If it is a massive insert and we want the affected rows.
     * @param string $dbConnName 'dbSlave' in case we want to connect to slave
     *
     * @return bool|int|string
     * @throws CDbException
     */
    final public function executeSQL($sql, $aBindParams=array(), $returnNumRows=false, $dbConnName=null, $bSendEmail=true)
    {
        if (!empty($dbConnName)) {
            if ($dbConnName==AbaActiveRecord::DB_SLAVE){
                throw new CDbException( 'Aba Exception: '.
                    'Trying to execute an update/insert/execute/delete query on SLAVE.'.
                    'This is not allowed.');
            }
            $this->connection = Yii::app()->getComponent($dbConnName);
        }

        /* @var CDbCommand $command */
        $command = $this->connection->createCommand($sql);

        if (count($aBindParams) > 0) {
            $command->bindValues($aBindParams);
        }
        Yii::trace($command->getText(), "sql.insert");

        $dataReader = $this->dbReadOnly;
        try {
            if (!$this->dbReadOnly) {
                $dataReader = $command->execute();
            }
        } catch (Exception $e) {

            //
            //#5442
            $sErrMessage =  "Aba Exception in insert " . $sql . "; exception=" . $e->getMessage();
            $sErrTitle =    "Error in Campus, Insert SQL DATABASE";

            if(HeLog::reportToLog()) {
                Yii::log($sErrTitle . ": " . $sErrMessage, CLogger::LEVEL_ERROR, 'sql');
            }

            if(HeLog::reportToEmail()) {
                if($bSendEmail) {
                    HeAbaMail::sendEmailInternalBug(" " . $sErrTitle, $sErrMessage);
                }
            }
            return false;
        }
        if ($returnNumRows) {
            return $dataReader;
        }
        if ($this->connection->getLastInsertID() > 0) {
            return $this->connection->lastInsertID;
        }
        return $dataReader;
    }

    /**
     * @param       $sql
     * @param array $aBindParams
     * @param string $dbConnName 'dbSlave' in case we want to connect to slave
     *
     * @return bool|int
     * @throws CDbException
     */
    final public function executeCallSQL($sql, $aBindParams=array(), $dbConnName=null, $bSendEmail=true)
    {
        if (!empty($dbConnName)) {
            if ($dbConnName==AbaActiveRecord::DB_SLAVE){
                throw new CDbException( 'Aba Exception: '.
                    'Trying to execute an update/insert/execute/delete query on SLAVE.'.
                    'This is not allowed.');
            }
            $this->connection = Yii::app()->getComponent($dbConnName);
        }
        /* @var CDbCommand $command*/
        $command = $this->connection->createCommand($sql);
        if(count($aBindParams)>0) {
            $command->bindValues($aBindParams);
        }
        $dataReader = $this->dbReadOnly;
        try {
            if(!$this->dbReadOnly) {
                $dataReader=$command->execute();
            }
        } catch(Exception $e) {
            //#5442
            $sErrMessage =  "Aba Exception in CALL " . $sql . "; exception=" . $e->getMessage();
            $sErrTitle =    "Error in Campus, Calling a Procedure in SQL DATABASE";
            if(HeLog::reportToLog()) {
                Yii::log($sErrTitle . ": " . $sErrMessage, CLogger::LEVEL_ERROR, 'sql');
            }

            if(HeLog::reportToEmail()) {
                if($bSendEmail) {
                    HeAbaMail::sendEmailInternalBug(" " . $sErrTitle, $sErrMessage);
                }
            }
            //

            throw new CDbException( "Aba Exception in CALL ". $e->getMessage() );
        }
        return !intval($dataReader);
    }

    /**
     * Only UPDATE queries SQL or MASSIVE INSERTS
     * @param       $sql
     * @param array $aBindParams
     * @param string $dbConnName 'dbSlave' in case we want to connect to slave
     *
     * @return bool|int
     * @throws CDbException
     */
    final public function updateSQL($sql, $aBindParams=array(), $dbConnName=null, $bSendEmail=true)
    {
        if (!empty($dbConnName)) {
            if ($dbConnName==AbaActiveRecord::DB_SLAVE) {
                throw new CDbException( 'Aba Exception: '.
                    'Trying to execute an update/insert/execute/delete query on SLAVE.'.
                    'This is not allowed.');
            }
            $this->connection = Yii::app()->getComponent($dbConnName);
        }
        /* @var CDbCommand $command*/
        $command = $this->connection->createCommand($sql);
        foreach ($aBindParams as $column=>$value) {
            $command->bindValue($column, $value, $this->transPHPTypePDO($value));
        }
       Yii::trace($command->getText(), "sql.update");
        try {
            $dataReader = $this->dbReadOnly;
            if (!$this->dbReadOnly) {
                $dataReader=$command->execute();
            }
        } catch(Exception $e) {
            //#5442
            $sErrMessage =  "Aba: Exception in update:" . $sql . " " . $e->getMessage();
            $sErrTitle =    "Error in Campus, Update SQL DATABASE";
            if(HeLog::reportToLog()) {
                Yii::log($sErrTitle . ": " . $sErrMessage, CLogger::LEVEL_ERROR, 'sql');
            }

            if(HeLog::reportToEmail()) {
                if($bSendEmail) {
                    HeAbaMail::sendEmailInternalBug(" " . $sErrTitle, $sErrMessage);
                }
            }
            //

            throw new CDbException( "Aba: Exception in update ". $e->getMessage() );
        }
        if ($dataReader>0) {
            return $dataReader;
        }
        return true;
    }

    /**
     * Transform the results of a query to properties of a class.
     * @param Array $rowResult
     */
    final protected function fillDataColsToProperties($rowResult)
    {
        if (is_array($rowResult)) {
            foreach($rowResult as $fieldName=>$value) {
                $this->$fieldName = $value;
                $this->aFieldsNames[] = $fieldName;
            }
        }
    }

    /** Returns this same object if it is collected successfully rows and cols, otherwise returns false
     *
     * @param CDbDataReader|bool $dataReader
     *
     * @return bool|$this
     */
    final protected function readRowFillDataColsThis($dataReader)
    {
        $row = $dataReader->read();
        if ($row !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param CDbDataReader|bool $dataReader
     *
     * @return bool
     * Return true if successful collecting row and columns
     */
    final protected function readRowFillDataCols($dataReader)
    {
        $row = $dataReader->read();
        if ($row !== false) {
            $this->fillDataColsToProperties($row);
            return true;
        }
        return false;
    }

    /**
     * @param AbaActiveRecord|nulll $objetoExt
     *
     * @return array
     */
    public function getFieldsClass( $objetoExt=null )
    {
        $aFields = array();
        $objeto = $this;
        if(!is_null($objetoExt)) {
            $objeto = $objetoExt;
        }

        foreach ($objeto->aFieldsNames as $fieldName) {
            $aFields[$fieldName] = $objeto->$fieldName;
        }
        return $aFields;
    }

    /**
     * Returns last error created by the validation of Rules of Yii.
     * @return array|string
     */
    public function  getLastErrorAdded()
    {
        $lastError = "";
        if(is_array($this->getErrors())) {
            $lastError = $this->getErrors();
            if (count($lastError)>0) {
                $lastError = $lastError[ count($lastError)-1 ][1];
            } else {
                return "";
            }
        }
        return $lastError;
    }

    /** Deletes and Truncates a full table.
     * @return bool|int
     */
    final public function fullTruncate()
    {
        $sql = " TRUNCATE ".$this->tableName();
        return $this->executeCallSQL($sql);
    }

    /** Performs a sql deletion from some conditional rows from a certain o several tables.
     * @param string $sql
     * @param array $aBindParams
     * @param string $dbConnName
     * @return bool|int
     * @throws CDbException
     */
    final public function deleteSQL($sql, $aBindParams=array(), $dbConnName=null, $bSendEmail=true)
    {
        if (!empty($dbConnName)) {
            if ($dbConnName==AbaActiveRecord::DB_SLAVE){
                throw new CDbException( 'Aba Exception: '.
                    'Trying to execute an update/insert/execute/delete query on SLAVE.'.
                    'This is not allowed.');
            }
            $this->connection = Yii::app()->getComponent($dbConnName);
        }
        /* @var CDbCommand $command */
        $command = $this->connection->createCommand($sql);
        foreach ($aBindParams as $column => $value) {
            $command->bindValue($column, $value, $this->transPHPTypePDO($value));
        }
        Yii::trace($command->getText(), "sql.delete");
        try {
            $dataReader = $this->dbReadOnly;
            if (!$this->dbReadOnly) {
                $dataReader = $command->execute();
            }
        } catch (Exception $e) {
            //#5442
            $sErrMessage =  "Aba: Exception in delete: " . $sql . " " . $e->getMessage();
            $sErrTitle =    "Error in Campus, Delete SQL DATABASE";
            if(HeLog::reportToLog()) {
                Yii::log($sErrTitle . ": " . $sErrMessage, CLogger::LEVEL_ERROR, 'sql');
            }

            if(HeLog::reportToEmail()) {
                if($bSendEmail) {
                    HeAbaMail::sendEmailInternalBug(" " . $sErrTitle, $sErrMessage);
                }
            }
            //

            return false;
        }
        if ($dataReader > 0) {
            return $dataReader;
        }
        return true;
    }

    protected function _getCache()
    {
        if (false === $this->cacheID) {
            return false;
        } elseif (NULL !== $this->_cache) {
            return $this->_cache;
        } elseif (($this->_cache = Yii::app()->getComponent($this->cacheID)) instanceof CCache) {
            return $this->_cache;
        } elseif (($this->_cache = Yii::app()->getComponent($this->cacheID)) instanceof CDummyCache) {
            return $this->_cache;
        } else {
            throw new CException("AbaActiveRecord.cacheID \"{$this->cacheID}\" is invalid. Please make sure it refers to the ID of a CCache application component.");
        }
    }
}
