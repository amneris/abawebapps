<?php
/** Magic properties extracted from table in database dynamically
 * @property string id
 * @property float amountDiscount
 * @property float amountOriginal
 * @property float amountPrice
 * @property string autoId
 * @property string currencyTrans
 * @property string dateEndTransaction
 * @property string dateStartTransaction
 * @property string dateToPay
 * @property string foreignCurrencyTrans
 * @property string idCountry
 * @property string idPartner
 * @property string idPayControlCheck
 * @property string idPeriodPay
 * @property string idProduct
 * @property string idPromoCode
 * @property string idUserCreditForm
 * @property string idUserProdStamp
 * @property string lastAction
 * @property string paySuppExtId
 * @property string paySuppExtProfId
 * @property string paySuppOrderId
 * @property integer status
 * @property integer userId
 * @property float xRateToEUR
 * @property float xRateToUSD
 * @property integer $idPartnerPrePay
 * @property integer $isRecurring
 * @property string  $paySuppExtUniId
 * @property integer $paySuppLinkStatus
 */
class ReconcPayment extends AbaActiveRecord
{
    public $id;
    private $autoId;

    /**
     * @return ReconcPayment
     */
    public function __construct( )
    {
        parent::__construct();
    }

    public function tableName()
    {
        return 'payments';
    }

    public function mainFields()
    {
        return "
         p.`id`, p.`idUserProdStamp`, p.`userId`, p.`idProduct`, p.`idCountry`, p.`idPeriodPay`,
            p.`idUserCreditForm`, p.`paySuppExtId`, p.`paySuppOrderId`, p.`status`, p.`dateStartTransaction`,
            p.`dateEndTransaction`,p.`dateToPay`, p.`xRateToEUR`, p.`xRateToUSD`, p.`currencyTrans`,
            p.`foreignCurrencyTrans`,p.`idPromoCode`, p.`amountOriginal`, p.`amountDiscount`, p.`amountPrice`,
            p.`idPayControlCheck`,p.`idPartner`, p.`idPartnerPrePay`, p.`paySuppExtProfId`, p.autoId, p.`lastAction`,
            p.`isRecurring`, p.`paySuppExtUniId`, p.`paySuppLinkStatus`
        ";

    }

    /**
     * SQL FUNCTIONS ---------------------------------------------------------------------------
      */

    /**
     * Returns the last payment pending to be paid. Used to find the recurrent
     * payments both for PAYPAL and for La Caixa.
     * @param $paySuppOrderId
     * @param integer $status
     *
     * @return bool
     */
    public function getPaymentByPaySuppOrderId( $paySuppOrderId, $status=NULL)
    {
        if (empty($status)){
            $wStatus = " p.`status`<>".PAY_PENDING." ";
        } else {
            $wStatus = " p.`status`=".$status." ";
        }

        $sql = "SELECT ".$this->mainFields()." FROM ".$this->tableName()." p
                WHERE ".$wStatus." AND p.`paySuppOrderId` = :PAYSUPPORDERID ";
        $paramsToSQL = array(":PAYSUPPORDERID" => $paySuppOrderId);
        $dataReader=$this->querySQL( $sql, $paramsToSQL);
        if(($row = $dataReader->read())!==false)
        {
            $this->fillDataColsToProperties( $row );
            return true;
        }

        return false;
    }

    /**
     * #4657
     *
     * Returns the last payment pending to be paid. Used to find the recurrent
     * payments both for PAYPAL and for La Caixa.
     * @param $paySuppOrderId
     * @param integer $status
     *
     * @return bool
     */
    public function getPaymentByPaySuppOrderIdForReconciliation( $paySuppOrderId, $status=NULL)
    {
        if (empty($status)){
            $wStatus = " p.`status`<>".PAY_PENDING." ";
        } else {
            $wStatus = " p.`status`=".$status." ";
        }

        $sql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " p
            WHERE
                " . $wStatus . " AND p.`paySuppOrderId` = :PAYSUPPORDERID
            ORDER BY p.dateStartTransaction DESC
            LIMIT 0, 1
        ";
        $paramsToSQL = array(":PAYSUPPORDERID" => $paySuppOrderId);
        $dataReader=$this->querySQL( $sql, $paramsToSQL);

        if(($row = $dataReader->read())!==false) {
            $this->fillDataColsToProperties( $row );
            return true;
        }
        return false;
    }


    /** Searches by universal id from gateway
     *
     * @param string $paySuppExtUniId
     * @return bool
     */
    public function getPaymentByPaySuppExtUnid( $paySuppExtUniId )
    {
        $sql = "SELECT ".$this->mainFields()." FROM ".$this->tableName()." p
                WHERE p.`paySuppExtUniId` = '$paySuppExtUniId' ";
        $dataReader=$this->querySQL( $sql );
        if(($row = $dataReader->read())!==false) {
            $this->fillDataColsToProperties( $row );
            return true;
        }

        return false;
    }

    /**
     * @param $paySuppOrderId
     * @param $amount
     * @param int $status
     *
     * @return bool
     */
    public function getPaymentSuccessPartial( $paySuppOrderId, $amount, $status=30 )
    {
        $sql = "SELECT ".$this->mainFields()." FROM ".$this->tableName()." p
                WHERE p.`paySuppOrderId` = '$paySuppOrderId'
                  AND p.`amountPrice`<$amount
                  AND p.`status`=$status
                  AND ( p.`paySuppLinkStatus` IS NULL OR p.`paySuppLinkStatus` = 0 ) ;";
        $dataReader=$this->querySQL( $sql );
        if(($row = $dataReader->read())!==false) {
            $this->fillDataColsToProperties( $row );
            return true;
        }

        return false;
    }

    /**
     * @param null $paySuppExtUniId
     *
     * @return bool
     */
    public function markAsReconciled ($paySuppExtUniId=null)
    {
        $this->paySuppLinkStatus = 1;
        $ret = $this->update(array("paySuppLinkStatus"));

        if($ret && !empty($paySuppExtUniId)){
            $this->paySuppExtUniId = $paySuppExtUniId;
            $ret = $this->update(array("paySuppExtUniId"));
        }

        return $ret;
    }

    /**
     * @param null $paySuppExtUniId
     *
     * @return bool
     */
    public function markAsNotReconciled ($paySuppExtUniId=null)
    {
        $this->paySuppLinkStatus = 0;
        $ret = $this->update(array("paySuppLinkStatus"));

        if($ret && !empty($paySuppExtUniId)){
            $this->paySuppExtUniId = $paySuppExtUniId;
            $ret = $this->update(array("paySuppExtUniId"));
        }

        return $ret;
    }

}
