<?php
/**
 * Class ImportReconciliationForm
 */
class ImportReconciliationForm extends CModel
{
    public $paySupplierExtId;
    public $fileName;
    public $dateOperation;


    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function validate($attributes=null, $clearErrors=true)
    {
        if (!parent::validate($attributes, $clearErrors)) {
            return false;
        }

        if (strpos($this->fileName, '.csv') === false) {
            Yii::app()->user->setFlash('errorCheckBox', 'Error in the file type, should be CSV type.');
            return false;
        }

        if ($this->paySupplierExtId!=PAY_SUPPLIER_CAIXA){
            $this->addError("paySupplierExtId", "Currently only LA CAIXA is implemented for manual reconiliation.");
            return false;
        }

        return true;
    }


    public function rules()
    {
        return array(
           array('fileName', 'file', 'allowEmpty' => false, 'types' => 'csv'),
           array('paySupplierExtId', 'required'),
           array('dateOperation', 'required'),
        );
    }


    public function relations()
    {
        return array();
    }

    public function attributeLabels()
    {

        return array(
            'paySupplierExtId' => 'Supplier',
            'fileName' => 'Filename to import',
            'dateOperation' => 'Date',
        );
    }

    public function attributeNames()
    {
        return array(
            'paySupplierExtId',
            'fileName',
            'dateOperation',
        );
    }

}
