<?php

/**
 * This is the model class for table "log_reconciliation".
 *
 * The followings are the available columns in table 'log_reconciliation':
 * @property string $id
 * @property string $filePath
 * @property string $fileOrigin
 * @property integer $lineNode
 * @property string $paySuppExtId
 * @property string $dateOperation
 * @property string $idPayment
 * @property string $refPaySuppOrderId
 * @property integer $refPaySuppStatus
 * @property float $refAmountPrice
 * @property string $refDateEndTransaction
 * @property string $refCurrencyTrans
 * @property string $refPaySuppExtUniId
 * @property integer $success
 * @property string $matchType
 */
class LogReconciliation extends AbaActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LogReconciliation the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'log_reconciliation';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
          array('paySuppExtId, dateOperation', 'required'),
          array('success', 'numerical', 'integerOnly' => true),
          array('filePath', 'length', 'max' => 150),
          array('fileOrigin, refAmountPrice', 'length', 'max' => 20),
          array('lineNode', 'numerical', 'integerOnly' => true),
          array('paySuppExtId', 'length', 'max' => 2),
          array('idPayment', 'length', 'max' => 15),
          array('refPaySuppOrderId', 'length', 'max' => 80),
          array('refCurrencyTrans', 'length', 'max' => 3),
          array('refPaySuppExtUniId', 'length', 'max' => 50),
          array('matchType', 'length', 'max' => 20),
          array('refDateEndTransaction, refPaySuppStatus', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
          array(
            'id, filePath, fileOrigin, lineNode, paySuppExtId, dateOperation, idPayment, refPaySuppOrderId,
			refAmountPrice, refDateEndTransaction, refCurrencyTrans, refPaySuppExtUniId, success, matchType',
            'safe',
            'on' => 'search'
          ),
        );
    }

    /**
     * @param bool $limit
     * @return CActiveDataProvider
     */
    public function search($limit = true)
    {
        $criteria = new CDbCriteria;

        $criteria->compare('filePath', trim($this->filePath), true);
        $criteria->compare('fileOrigin', trim($this->fileOrigin), true);
        $criteria->compare('paySuppExtId', trim($this->paySuppExtId), false);
        $criteria->compare('dateOperation', trim($this->dateOperation));
        $criteria->compare('idPayment', trim($this->idPayment));
        $criteria->compare('refPaySuppOrderId', trim($this->refPaySuppOrderId));
        $criteria->compare('refCurrencyTrans', trim($this->refCurrencyTrans));
        $criteria->compare('refPaySuppStatus', trim($this->refPaySuppStatus), false);
        $criteria->compare('success', trim($this->success));
        $criteria->compare('matchType', trim($this->matchType), false);

        if ($limit) {
            $pageLimit = 20;
        } else {
            $pageLimit = 1000000;
        }

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
          'pagination' => array('pageSize' => $pageLimit),
        ));
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'id' => 'ID',
          'filePath' => 'File Path',
          'fileOrigin' => 'File Origin',
          'lineNode' => 'Line or node position',
          'paySuppExtId' => 'Pay Supp Ext',
          'dateOperation' => 'Date Operation',
          'idPayment' => 'Id Payment',
          'refPaySuppOrderId' => 'Ref Pay Supp Order',
          'refAmountPrice' => 'Ref Amount Price',
          'refDateEndTransaction' => 'Ref Date End Transaction',
          'refCurrencyTrans' => 'Ref Currency Trans',
          'refPaySuppExtUniId' => 'Ref Pay Supp Ext Uni',
          'success' => 'Success',
          'matchType' => 'Type of matching',
        );
    }

    /**
     * @return integer|bool
     */
    public function insertRowBeforeMatch()
    {
        $sql = " INSERT INTO " . $this->tableName() . " ( `filePath`, `fileOrigin`, `lineNode`, `paySuppExtId`,
                                    `dateOperation`, `refPaySuppOrderId`,
                                    `refPaySuppStatus`, `refAmountPrice`, `refDateEndTransaction`,
                                    `refCurrencyTrans`, `refPaySuppExtUniId`, `success`, `matchType` )
                                  VALUES(  :FILEPATH, :FILEORIGIN, :LINENODE, :PAYSUPPEXTID,
                                    :DATEOPERATION, :REFPAYSUPPORDERID,
                                    :REFPAYSUPPSTATUS, :REFAMOUNTPRICE, :REFDATEENDTRANSACTION,
                                    :REFCURRENCYTRANS, :REFPAYSUPPEXTUNIID, 0, :MATCHTYPE ); ";

        $aBrowserValues = array(
          ':FILEPATH' => $this->filePath,
          ':FILEORIGIN' => $this->fileOrigin,
          ':LINENODE' => $this->lineNode,
          ':PAYSUPPEXTID' => $this->paySuppExtId,
          ':DATEOPERATION' => $this->dateOperation,
          ':REFPAYSUPPORDERID' => $this->refPaySuppOrderId,
          ':REFPAYSUPPSTATUS' => $this->refPaySuppStatus,
          ':REFAMOUNTPRICE' => $this->refAmountPrice,
          ':REFDATEENDTRANSACTION' => $this->refDateEndTransaction,
          ':REFCURRENCYTRANS' => $this->refCurrencyTrans,
          ':REFPAYSUPPEXTUNIID' => $this->refPaySuppExtUniId,
          ':MATCHTYPE' => $this->matchType
        );
        $this->id = $this->executeSQL($sql, $aBrowserValues, false);
        if ($this->id > 0) {
            return $this->id;
        } else {
            return false;
        }
    }

    /**
     * @param array $aColumns
     * @param string $delimiter
     *
     * @return string
     */
    public function getMainColumnsAsLine(array $aColumns, $delimiter = ",")
    {
        $lineText = "";
        foreach ($aColumns as $column) {
            if ($lineText !== "") {
                $lineText .= $delimiter;
            }
            $lineText .= $this->$column;
        }

        return $lineText;
    }

}
