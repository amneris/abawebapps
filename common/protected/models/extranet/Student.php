<?php

/**
 * This is the model class for table "t_student".
 *
 * The followings are the available columns in table 't_student':
 * @property integer $id
 */
class Student extends BaseStudent
{

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseTStudent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
