<?php

/**
 * This is the model class for table "license_asigned".
 *
 * The followings are the available columns in table 'license_asigned':
 * @property integer $id
 */
class LicenseAsigned extends BaseLicenseAsigned
{
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseLicenseAsigned the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
