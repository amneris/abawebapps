<?php

/**
 * This is the model class for table "student_group".
 *
 * The followings are the available columns in table 'student_group':
 * @property integer $id
 */
class StudentGroup extends BaseStudentGroup
{

	const _STATUS_ACTIVE = 1;
	const _STATUS_INACTIVE = 2;

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseStudentGroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
