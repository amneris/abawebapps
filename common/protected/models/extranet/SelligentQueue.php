<?php

/**
 * This is the model class for table "selligent_queue".
 *
 * The followings are the available columns in table 'selligent_queue':
 * @property integer $id
 */
class SelligentQueue extends BaseSelligentQueue
{

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseSelligentQueue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
