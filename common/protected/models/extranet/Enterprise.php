<?php

/**
 * This is the model class for table "enterprise".
 *
 * The followings are the available columns in table 'enterprise':
 * @property integer $id
 */
class Enterprise extends BaseEnterprise {

  static public $lastErrorMsg;

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'idMainContact' => 'Main Contact',
            'idCountry' => 'Id Country',
            'idLanguage' => 'Id Language',
            'idPartner' => 'Id Partner',
            'idPartnerEnterprise' => 'Id Partner Enterprise',
            'lastupdated' => 'Lastupdated',
            'created' => 'Created',
            'deleted' => 'Deleted',
            'isdeleted' => 'Isdeleted',
        );
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public static function idPartnerNextVal($name) {
        $mEnterprise = new Enterprise();

//      try {
//        $connection = self::getDbConnection();
//        $sql = "SELECT nextval('idPartner') as next_sequence;";
//        $command = $connection->createCommand($sql);
//        Yii::trace($command->getText(), "sql.select");
//        $nextVal = $command->queryScalar();
//      } catch (Exception $e) {
//        $nextVal = false;
//      }

        try {
            $connection = $mEnterprise->getDbConnection();

            do {
                $sql = "select max(idPartner)+1 from ".Yii::app()->params['aba_b2c'].".aba_partners_list where idPartnerGroup = ".PARTNER_B2B_G." and idPartner >= '".Yii::app()->params['minValueIdParner']."' and idPartner < '".Yii::app()->params['maxValueIdParner']."' ";
                $command = $connection->createCommand($sql);
                Yii::trace($command->getText(), "sql.select");
                $nextVal = $command->queryScalar();
                if ( empty($nextVal) ) $nextVal = Yii::app()->params['minValueIdParner'];

                $command = $connection->createCommand($sql);
                $affectedRows = $command->insert( Yii::app()->params['aba_b2c'].'.aba_partners_list', array(
                    'idPartner' => $nextVal,
                    'name' => $name,
                    'idPartnerGroup' => PARTNER_B2B_G,
                    'nameGroup' => PARTNER_B2B_W,
                    'idBusinessArea' => 3,
                    'idCategory' => 1
                ));
            } while( $affectedRows <= 0 );

//        $sql = "insert into ".Yii::app()->params['aba_b2c'].".aba_partners_list (idPartner, name, idPartnerGroup, nameGroup) values (:nextVal, :name, :idPartnerGroup, :nameGroup)";
//        $command = $connection->createCommand($sql);
//        $command->bindParam(':name', $name);
//        $command->bindParam(':nextVal', $nextVal);
//        $command->bindParam(':idPartnerGroup', 4);
//        $command->bindParam(':nameGroup', 'b2b');
//        $command->execute();

        } catch (Exception $e) {
            $nextVal = false;
        }

        return $nextVal;
    }

    public static function idPartnerEnterpriseNextVal() {

        try {
            $connection = self::getDbConnection();
            $sql = "SELECT nextval('idPartnerEnterprise') as next_sequence;";
            $command = $connection->createCommand($sql);
            Yii::trace($command->getText(), "sql.select");
            $nextVal = $command->queryScalar();
        } catch (Exception $e) {
            $nextVal = false;
        }

        return $nextVal;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'group'=>array(self::HAS_MANY, 'Group', 'idEnterprise'),
            'period'=>array(self::HAS_MANY, 'Period', 'idEnterprise'),
            'mainContact'=>array(self::BELONGS_TO, 'User', 'idMainContact'),
        );
    }

    public static function getEnterpriseByUserRole($idUser, $arFilters = array(), $orderBy = 'total', $limit = 10, $offset = 0 ) {
        $mUserTmp = new User();
        $connection = $mUserTmp->getDbConnection();

        $mlEnterprise = array();
        if ( Utils::getSessionVar('isAbaManager', false) ) {
            $command = $connection->createCommand()
                ->select('t1.total, e.id as idRole, e.name as nameRole, e.id as idEnterprise, e.name as nameEnterprise, e.id as idRoleEnterprise, e.created as Creation, c.name as Country, CONCAT(u.surname,", ",u.name) as contactName, u.email as Email, e.idUserCreator, e.urlLogo')
                ->from('enterprise_user_role eur')
                ->join('role r', 'eur.idRole = r.id')
                ->join('enterprise e', 'eur.idEnterprise = e.id and e.isdeleted = 0')
                ->join('user u', 'e.idMainContact = u.id')
                ->join('aba_b2c.country c','e.idCountry = c.id')
                ->leftJoin('(SELECT count(id) as total, idEnterprise FROM abaenglish_extranet.purchase where validated=0 and isdeleted=0 group by idEnterprise) t1', 't1.idEnterprise = e.id')
                ->where('e.isdeleted = 0 and e.id != 1')
                ->group('e.id');
        } else {
            $command = $connection->createCommand()
                ->select('t1.total, e.id as idRole, e.name as nameRole, e.id as idEnterprise, e.name as nameEnterprise, e.id as idRoleEnterprise, e.created as Creation, c.name as Country, CONCAT(u.name,", ",u.surname) as contactName, u.email as Email, e.idUserCreator, e.urlLogo')
                ->from('enterprise_user_role eur')
                ->join('role r', 'eur.idRole = r.id')
                ->join('enterprise e', 'eur.idEnterprise = e.id and e.isdeleted = 0')
                ->join('user u', 'e.idMainContact = u.id')
                ->join('aba_b2c.country c','e.idCountry = c.id')
                ->leftJoin('(SELECT count(id) as total,idEnterprise FROM abaenglish_extranet.purchase where validated=0 and isdeleted=0 group by idEnterprise ) t1', 't1.idEnterprise = e.id')
                ->where('eur.idUser = :idUser and eur.isdeleted = 0 and e.id != 1 and eur.idRole not in (' . Role::_RESPONSIBLE . ', '. Role::_TEACHER . ')' );
        }
        $command->limit($limit, $offset);

        if ( !empty($orderBy) )
        {
            $orderBy_tmp = str_replace(array('created', 'name', 'role', 'country', 'email', 'contact'), array('e.created', 'e.name', 'nameRole','c.name', 'u.email', 'u.surname'), $orderBy);
            $command->order( $orderBy_tmp );
        }
        if ( !empty($arFilters) )
        {
            $contador = 0;
            $arValues = array();
            foreach( $arFilters as $key => $value )
            {
                $band = false;
                if ( $key == 'name' ) $key = 'e.name';
                else if ( $key == 'role' ) $key = 'r.name';
                else if ( $key == 'created' ) $key = 'e.created';
                else if ( $key == 'country' ) $key = 'c.name';
                //LUIS: i did a little fix to look for both columns.. name and username.. because.. on the grid both are concatenated.
                else if ( $key == 'contact' )
                {
                    $value =  str_replace('%','',$value);
                    $whereParam='';
                    $arrayOpt = explode(' ', $value);

                    //LUIS: first.. i check for a mach in surname column
                    foreach($arrayOpt as $singleOpt)
                    {
                        $singleOpt=str_replace(',','',$singleOpt);
                        if(!$band)
                        {
                            $whereParam.="u.surname LIKE '%".$singleOpt."%' ";
                            $band=true;
                        }
                        else
                        {
                            $whereParam.=" OR u.surname LIKE '%".$singleOpt."%' ";
                        }
                    }

                    //LUIS: now we search for a match in name column
                    foreach($arrayOpt as $singleOpt)
                    {
                        $whereParam.=" OR u.name LIKE '%".$singleOpt."%' ";
                    }

                    $command->andWhere( $whereParam );
                    $contador++;

                } else if ( $key == 'idType' ) {
                  $command->andWhere( $key.' = '.$value );
                  $band = true;
                  $contador++;
                }
                if(!$band)
                {
                    $value_tmp = addcslashes($value, '%_');
                    $value_tmp = "%$value_tmp%";
                    $command->andWhere( $key.' like :value'.$contador );
                    $arValues[':value'.$contador] = $value_tmp;
//                    $command->bindParam(':value'.$contador, $value_tmp);
                    $contador++;
                }
            }

            foreach( $arValues as $key => $value ) {
              $command->bindParam($key, $value);
            }

        }
        if ( !Utils::getSessionVar('isAbaManager', false) ) {
            $command->bindParam(":idUser", $idUser, PDO::PARAM_INT);
        }
        Yii::trace($command->getText(), "sql.select");
        //file_put_contents('/tmp/extra.log', "SQL: ".$command->getText()."\n", FILE_APPEND);
        //file_put_contents('/tmp/extra.log', "SQL2: $sql\n", FILE_APPEND);

        try
        {
            $mlRoles_tmp = $command->queryAll();
            $mlRoles = array();
            foreach( $mlRoles_tmp as $rowRole ) {
                $mRole = new stdClass();
                foreach( $rowRole as $key => $value ) $mRole->$key = $value;

                if ( Utils::getSessionVar('isAbaManager', false) ) {
                    $mRole->id = Role::_ABAMANAGER;
                    $mRole->nameRole = 'Aba Manager';
                }

                $mlEnterprise[] = $mRole;
            }

        } catch(Exception $e)
        {
            //file_put_contents('/tmp/extra.log', "SQL3: ".$e->getMessage()."\n", FILE_APPEND);
            Yii::log( 'Error to execute last SQL.', 'error', "sql.select" );
        }
        return $mlEnterprise;
    }

    public static function countEnterpriseByUserRole($idUser, $arFilters = array() ) {
        $mUserTmp = new User();
        $connection = $mUserTmp->getDbConnection();

        if ( Utils::getSessionVar('isAbaManager', false) ) {
            $command = $connection->createCommand()
                ->select('count(*)')
                ->from('enterprise e')
                ->join('user u', 'e.idMainContact = u.id')
                ->join('aba_b2c.country c','e.idCountry = c.id')
                ->where('e.isdeleted = 0 and e.id != 1');
        } else {
            $command = $connection->createCommand()
                ->select('count(*)')
                ->from('enterprise_user_role eur')
                ->join('role r', 'eur.idRole = r.id')
                ->join('enterprise e', 'eur.idEnterprise = e.id and e.isdeleted = 0')
                ->join('user u', 'e.idMainContact = u.id')
                ->join('aba_b2c.country c','e.idCountry = c.id')
                ->where('eur.idUser = :idUser and eur.isdeleted = 0 and e.id != 1 and eur.idRole not in (' . Role::_RESPONSIBLE . ', '. Role::_TEACHER .')' );
        }

        if ( !empty($arFilters) ) {
            $contador = 0;
            $arValues = array();
            foreach( $arFilters as $key => $value )
            {
                if ( $key == 'name' ) $key = 'e.name';
                else if ( $key == 'role' ) $key = 'r.name';
                else if ( $key == 'created' ) $key = 'e.created';
                else if ( $key == 'country' ) $key = 'c.name';
                else if ( $key == 'contact' ) $key = 'u.surname';

                if ( $key != 'idType' ) {
                  $value_tmp = addcslashes($value, '%_');
                  $value_tmp = "%$value_tmp%";
                  $command->andWhere( $key.' like :value'.$contador );
                  $arValues[':value'.$contador] = $value_tmp;
                  $contador++;
                } else {
                  $command->andWhere( $key.' = '.$value );
                  $contador++;
                }
            }

            foreach( $arValues as $key => $value ) {
              $command->bindParam($key, $value);
            }

        }
        if ( !Utils::getSessionVar('isAbaManager', false) ) {
            $command->bindParam(":idUser", $idUser, PDO::PARAM_INT);
        }

        Yii::trace($command->getText(), "sql.select");
        try {
          $count = $command->queryScalar();
        } catch(Exception $e) {
          $count = 0;
          Yii::log( 'Error to execute last SQL.'."\n".$command->getText(), 'error', "sql.select" );
        }
        return $count;
    }


    /**
     * Returns array with al the requested register.
     * @param integer $idUser current user id.
     * @param array $arFilters array with filters for the query
     * @param string $orderBy string that says with field to take to order the query
     * @return mlEnterprise array with all the query result
     */
    public static function enterpriseExport($idUser, $arFilters = array(), $orderBy = 'created') {

        $organization = Yii::app()->request->getParam('idType');

        $mUserTmp = new User();
        $connection = $mUserTmp->getDbConnection();

        $mlEnterprise = array();

        if ( Utils::getSessionVar('isAbaManager', false) ) {
            $command = $connection->createCommand()
                ->select('e.name as Enterprise, e.created as Creation, c.name as Country, u.name as Name, u.surname as Surname, u.email as Email')
                ->from('enterprise_user_role eur')
                ->join('role r', 'eur.idRole = r.id')
                ->join('enterprise e', 'eur.idEnterprise = e.id and e.isdeleted = 0')
                ->join('user u', 'e.idMainContact = u.id')
                ->join('aba_b2c.country c','e.idCountry = c.id');
                if($organization<>0)
                {
                    $command->where('e.isdeleted = 0 and e.id != 1 and e.idType = :idType group by e.id');
                    $command->bindParam(":idType", $organization);
                }
                else
                {
                    $command->where('e.isdeleted = 0 and e.id != 1 group by e.id');
                }

        } else {
            $command = $connection->createCommand()
                ->select('e.name as Enterprise, e.created as Creation, c.name as Country, u.name as Name, u.surname as Surname, u.email as Email')
                ->from('enterprise_user_role eur')
                ->join('role r', 'eur.idRole = r.id')
                ->join('enterprise e', 'eur.idEnterprise = e.id and e.isdeleted = 0')
                ->join('user u', 'e.idMainContact = u.id')
                ->join('aba_b2c.country c','e.idCountry = c.id');
                if($organization<>0)
                {
                    $command->where('eur.idUser = :idUser and eur.isdeleted = 0 and e.id != 1 and e.idType = :idType and eur.idRole not in (' . Role::_RESPONSIBLE .', '. Role::_TEACHER . ')' );
                    $command->bindParam(":idType", $organization);
                }
                else
                {
                    $command->where('eur.idUser = :idUser and eur.isdeleted = 0 and e.id != 1 and eur.idRole not in (' . Role::_RESPONSIBLE . ', ' . Role::_TEACHER . ')' );
                }
        }

        if ( !empty($orderBy) ) {
            $orderBy_tmp = str_replace(array('created', 'name', 'role'), array('e.created', 'e.name', 'nameRole'), $orderBy);
            $command->order( $orderBy_tmp );
        }
        if ( !empty($arFilters) ) {
            $contador = 0;
            foreach( $arFilters as $key => $value ) {
                if ( $key == 'name' ) $key = 'e.name';
                else if ( $key == 'role' ) $key = 'r.name';

                $value_tmp = addcslashes($value, '%_');
                $value_tmp = "%$value_tmp%";
                $command->andWhere( $key.' like :value'.$contador );
                $command->bindParam(':value'.$contador, $value_tmp);
                $contador++;
            }

        }
        if ( !Utils::getSessionVar('isAbaManager', false) ) {
            $command->bindParam(":idUser", $idUser, PDO::PARAM_INT);
        }

        Yii::trace($command->getText(), "sql.select");

        try
        {
            $mlRoles_tmp = $command->queryAll();
            $mlRoles = array();
            foreach( $mlRoles_tmp as $rowRole ) {
                $mRole = new stdClass();
                foreach( $rowRole as $key => $value ) $mRole->$key = $value;

                /*if ( Utils::getSessionVar('isAbaManager', false) ) {
                    $mRole->id = Role::_ABAMANAGER;
                    $mRole->nameRole = 'Aba Manager';
                }*/

                $mlEnterprise[] = $mRole;
            }

        } catch(Exception $e)
        {
            Yii::log( 'Error to execute last SQL.', 'error', "sql.select" );
        }
        return $mlEnterprise;
    }

  public function updateSelligentData( ) {
    if ( !empty($this->urlLogo) ) $urlLogo = getenv('AWS_URL_DOMAIN').$this->urlLogo;
    else $urlLogo = '';

    $allOk = HeAbaMail::sendEmail( array(
      'action' => HeAbaSelligent::_EDITENTERPRISE,
      'idEnterprise' => $this->id,
      'name' => $this->name,
      'idPartnerEnterprise' => $this->idPartner,
      'idUser' => $this->idMainContact,
      'logo' => $urlLogo,
      'callback' => 'ExtranetController::actionMailSent',
    ) );

    return $allOk;
  }

  static public function deleteItems( $arItems ) {
    $randomCode = 'zTmp_'.date("YmdHis").'_'.md5( Utils::getSessionVar('idUser').'_'.Utils::getSessionVar('idEnterprise').'_'.microtime(true).'_'.rand(0,100) );

    foreach( $arItems as $item ) {
      self::deleteItem( $item->id, $randomCode );
    }
  }

  static public function deleteItem( $idEnterprise, $undoCode = '' ) {
    if ( empty($undoCode) ) {
      $undoCode = 'zTmp_'.date("YmdHis").'_'.md5( Utils::getSessionVar('idUser').'_'.Utils::getSessionVar('idEnterprise').'_'.microtime(true).'_'.rand(0,100) );
    }

    $deletedDate = date("Y-m-d H:i:s");
    $connection = Yii::app()->dbExtranet;
    $transaction = $connection->beginTransaction();
    try {
        // ** Borro la empresa.
      $mEnterprise = Enterprise::model()->findByPk( $idEnterprise );
      $mEnterprise->deleted = $deletedDate;
      $mEnterprise->isdeleted = 1;
      $mEnterprise->save();

      $tmpUndoCode = $undoCode.'e';
      $sql = "insert into toundo (idelement, code) values (:idEnterprise,:code) ";
      $command = $connection->createCommand($sql);
      $command->bindParam(":idEnterprise", $idEnterprise, PDO::PARAM_INT);
      $command->bindParam(":code", $tmpUndoCode, PDO::PARAM_STR);
      $command->execute();

        // ** Borro los roles de los usuarios de esta empresa.
      $tmpUndoCode = $undoCode.'eur';
      $sql = "insert into toundo (idelement, code)
                      select eur.id, :code
                      from enterprise_user_role eur
                      where eur.idEnterprise = :idEnterprise
                        and eur.isdeleted = 0
                        ";
      $command = $connection->createCommand($sql);
      $command->bindParam(":idEnterprise", $idEnterprise, PDO::PARAM_INT);
      $command->bindParam(":code", $tmpUndoCode, PDO::PARAM_STR);
      $command->execute();

        $sql = "update " . EnterpriseUserRole::model()->tableName() . " eur
              inner join abaenglish_extranet.toundo t on
                eur.id = t.idElement and t.code = '" . $undoCode . "' and eur.idEnterprise = $idEnterprise
            set
              eur.deleted = '$deletedDate',
              eur.isdeleted = 1";
        $command = Yii::app()->dbExtranet->createCommand($sql);
        $command->execute();

        // ** Los usuarios que estaban realacionados con esta empresa y ya no tienen ningun otro rol.
      $criteria = new CDbCriteria();
      $criteria->addCondition( ' idEnterprise = :idEnterprise ' );
      $criteria->params = array( ':idEnterprise' => $idEnterprise );
      $mlEnterpriseUserROle = EnterpriseUserRole::model()->findAll( $criteria );
      foreach( $mlEnterpriseUserROle as $mEnterpriseUserRole ) {
        $mUser = User::model()->findByPk( $mEnterpriseUserRole->idUser );
          // ** Si el usuario ya esta borrado.
        if ( !$mUser || $mUser->isdeleted ) continue;
        $idUser = $mUser->id;

        $criteria = new CDbCriteria();
        $criteria->addCondition( ' idUser = :idUser ' );
        $criteria->addCondition( ' isdeleted = :isdeleted ' );
        $criteria->params = array( ':idUser' => $idUser, ':isdeleted' => 0 );
        $count = EnterpriseUserRole::model()->count($criteria);
        if ( $count > 0 ) continue;

        $mUser->deleted = $deletedDate;
        $mUser->isdeleted = 1;
        $mUser->save();

        $tmpUndoCode = $undoCode.'u';
        $sql = "insert into toundo (idelement, code) values (:idUser,:code) ";
        $command = $connection->createCommand($sql);
        $command->bindParam(":idUser", $idUser, PDO::PARAM_INT);
        $command->bindParam(":code", $tmpUndoCode, PDO::PARAM_STR);
        $command->execute();
      }

        // ** Los alumnos que estan asociados a esta empresa.
      $tmpUndoCode = $undoCode.'s';
      $sql = "insert into toundo (idelement, code)
                      select s.id, :code
                      from student s
                      where s.idEnterprise = :idEnterprise
                        and s.isdeleted = 0
                        ";
      $command = $connection->createCommand($sql);
      $command->bindParam(":idEnterprise", $idEnterprise, PDO::PARAM_INT);
      $command->bindParam(":code", $tmpUndoCode, PDO::PARAM_STR);
      $command->execute();

        $sql = "update ".Student::model()->tableName()." s
              inner join abaenglish_extranet.toundo t on
                s.id = t.idElement and t.code = '".$undoCode."s'
            set
              s.deleted = now(), s.isdeleted = 1, s.lastupdated = now()";
        $command = Yii::app()->dbExtranet->createCommand($sql);
        $command->execute();

      $transaction->commit();
    } catch (Exception $e) {
      $transaction->rollback();
      $undoCode = false;
      self::$lastErrorMsg = $e->getMessage();
    }

    return $undoCode;
  }

  static public function undoDelete( $undoCode ) {
    $allOk = true;
    $connection = Yii::app()->dbExtranet;

    $arTables = array(
      array('tableName' => 'enterprise', 'pseudo' => 'e'),
      array('tableName' => 'enterprise_user_role', 'pseudo' => 'eur'),
      array('tableName' => 'user', 'pseudo' => 'u'),
      array('tableName' => 'student', 'pseudo' => 's'),
      array('tableName' => 'student_group', 'pseudo' => 'sg'),
      array('tableName' => 'egroup', 'pseudo' => 'g'),
      array('tableName' => 'enterprise_timerange', 'pseudo' => 'etr'),
      array('tableName' => 'license_asigned', 'pseudo' => 'la'),
//      array('tableName' => 'license_available', 'pseudo' => 'lv'),
      array('tableName' => 'log_activacion', 'pseudo' => 'lc'),
      array('tableName' => 'LevelTest_Data', 'pseudo' => 'ltd'),
      array('tableName' => 'period', 'pseudo' => 'p'),
      array('tableName' => 'purchase', 'pseudo' => 'pu'),
      array('tableName' => 'student_level_test', 'pseudo' => 'slt'),
    );

    $transaction = $connection->beginTransaction();
    try {

      foreach( $arTables as $arTable ) {
        $tableName = $arTable['tableName'];
        $pseudo = $arTable['pseudo'];

        $sql = "UPDATE {$tableName} as {$pseudo} inner Join (select idElement from toundo where code = '{$undoCode}{$pseudo}') as t on {$pseudo}.id = t.idElement SET deleted=created, isdeleted = 0";
        $command = $connection->createCommand($sql);
        $command->execute();
      }

      $transaction->commit();
    } catch ( Exception $e ) {
      $allOk = false;
      $transaction->rollback();
      Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, 'enterprise');
    }



//    $sql = "UPDATE enterprise as e inner Join (select idElement from toundo where code = '{$undoCode}e') as t on e.id = t.idElement SET deleted=created";
//    $command = $connection->createCommand($sql);
//    $command->execute();
//
//    $sql = "UPDATE enterpriseuserrole as eur inner Join (select idElement from toundo where code = '{$undoCode}eur') as t on eur.id = t.idElement SET deleted=created";
//    $command = $connection->createCommand($sql);
//    $command->execute();
//
//    $sql = "UPDATE user as u inner Join (select idElement from toundo where code = '{$undoCode}u') as t on u.id = t.idElement SET deleted=created";
//    $command = $connection->createCommand($sql);
//    $command->execute();
//
//    $sql = "UPDATE student as s inner Join (select idElement from toundo where code = '{$undoCode}s') as t on s.id = t.idElement SET deleted=created";
//    $command = $connection->createCommand($sql);
//    $command->execute();
//
//    $sql = "UPDATE student_group as sg inner Join (select idElement from toundo where code = '{$undoCode}sg') as t on sg.id = t.idElement SET deleted=created";
//    $command = $connection->createCommand($sql);
//    $command->execute();
//
//    $sql = "UPDATE egroup as g inner Join (select idElement from toundo where code = '{$undoCode}g') as t on g.id = t.idElement SET deleted=created";
//    $command = $connection->createCommand($sql);
//    $command->execute();
//
//    $sql = "UPDATE enterprise_timerange as etr inner Join (select idElement from toundo where code = '{$undoCode}etr') as t on etr.id = t.idElement SET deleted=created";
//    $command = $connection->createCommand($sql);
//    $command->execute();
//
//    $sql = "UPDATE license_asigned as la inner Join (select idElement from toundo where code = '{$undoCode}la') as t on la.id = t.idElement SET deleted=created";
//    $command = $connection->createCommand($sql);
//    $command->execute();
//
//    $sql = "UPDATE license_available as lv inner Join (select idElement from toundo where code = '{$undoCode}lv') as t on lv.id = t.idElement SET deleted=created";
//    $command = $connection->createCommand($sql);
//    $command->execute();
//
//    $sql = "UPDATE log_activacion as lc inner Join (select idElement from toundo where code = '{$undoCode}lc') as t on lc.id = t.idElement SET deleted=created";
//    $command = $connection->createCommand($sql);
//    $command->execute();
//
//    $sql = "UPDATE LevelTest_Data as ltd inner Join (select idElement from toundo where code = '{$undoCode}ltd') as t on ltd.id = t.idElement SET deleted=created";
//    $command = $connection->createCommand($sql);
//    $command->execute();

    return $allOk;
  }

}
