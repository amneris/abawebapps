<?php

/**
 * Created by PhpStorm.
 * User: mikkel
 * Date: 21/08/15
 * Time: 11:41
 */
class CmMessage extends CActiveRecord
{
    const DELETED_BY_RECEIVER = 'receiver';
    const DELETED_BY_SENDER = 'sender';

    public function __construct($scenario = 'insert')
    {
        return parent::__construct($scenario);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'messages';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('sender_id, receiver_id', 'required'),
            array('sender_id, receiver_id', 'numerical', 'integerOnly' => true),
            array('subject', 'required'),
            array('subject', 'length', 'max' => 256),
            array('is_read', 'length', 'max' => 1),
            array('deleted_by', 'length', 'max' => 8),
            array('body', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array(
                'id, sender_id, receiver_id, subject, body, is_read, deleted_by, created_at',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'          => 'ID',
            'sender_id'   => Yii::t('mainApp', 'Remitente'),
            'receiver_id' => Yii::t('mainApp', 'Destinatario'),
            'subject'     => Yii::t('mainApp', 'Asunto'),
            'body'        => Yii::t('mainApp', 'Cuerpo'),
            'is_read'     => 'Is Read',
            'deleted_by'  => Yii::t('mainApp', 'Borrado por'),
            'created_at'  => Yii::t('mainApp', 'Creado en'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->compare('sender_id', $this->sender_id);
        $criteria->compare('receiver_id', $this->receiver_id);
        $criteria->compare('subject', $this->subject, true);
        $criteria->compare('body', $this->body, true);
        $criteria->compare('is_read', $this->is_read, true);
        $criteria->compare('deleted_by', $this->deleted_by, true);
        $criteria->compare('created_at', $this->created_at, true);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    protected function beforeSave()
    {
        if ($this->isNewRecord) {
            if ($this->hasAttribute('created_at')) {
                $this->created_at = Date('Y-m-d H:i:s');
            }
        }

        return parent::beforeSave();
    }
}
