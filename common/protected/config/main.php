<?php
/**
 * This is the code which assembles the global configuration for all entry points of the app.
 *
 * Config is being constructed from three parts: base, environment and local configuration overrides,
 * all of this files are in `overrides` subdirectory.
 *
 * NOTE that this global config will be overridden further by the configuration of each individual entry point
 * for example local.php.
 *
 */
return CMap::mergeArray(
    ( require dirname(__FILE__). '/base.php' ),
    ( require dirname(__FILE__). '/database.php' ),
     array()
    );

