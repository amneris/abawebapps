<?php

// TECHNICAL STUFF-------------------------------------------------
define("DELIMITER_KEYS", "|@|");
define("DELIMITER_PRODUCT", "_");

//User Types-------------------------------------------------------
define("MAX_ALLOWED_LEVEL", 2);
define('LEAD', 0); // Converted to DELETED in 2013-09-09
define('DELETED', 0); // Soft Deletegit checkout -b feature-5345
define('FREE', 1);
define('PREMIUM', 2);
define('PLUS', 3);
define('TEACHER', 4);

/* Operations: The whole list of operations associated to the campus,
NOT different behaviours for Banners or whatever, but access to specific operations
//----------------------------------------------------------------- */
define('OP_CAMPUSACCESS', 1); // Access to the Campus
define('OP_COURSE', 2); // Access to the Course section menu.
define('OP_GRAMATICA', 3); // Access to the Grammar menu section.
define('OP_MESSAGES_SECTION', 4); // Access to the Messages section Menu.
define('OP_VIDEOCLASSES', 5); // Access to video-classes

define('OP_FULLUNIT', 6); // If has access to see the unit of the FULL COURSE
define('OP_INCOMPLETEUNIT', 7); // If has access to see the unit of the INCOMPLETE COURSE
define('OP_NOACTIONACCESSUNIT', 600);

define('OP_MESSAGES_COMPOSE_ANY', 8); // Allows to send a message to anyone in the Campus
define('OP_MESSAGES_COMPOSE_TEACHER', 9); // Allows to send a message to teachers.

// Access type to units----------------------
define("UNLOCKED_VC", 0);
define("LOCKED_VC", -1);
// Paths-----------------------------------------------------------
define("PATH_UNITS", "allUnits/{language}/{unitId}/master{masterId}");
define("PATH_PARTIAL_UNITS", "PartialUnits/{language}/{unitId}/master{masterId}");
define("UNITS_PER_LEVEL", 24);
define("MAX_ENGLISH_LEVEL", 6);

// Payments related------------------------------------------------
define("CCY_DEFAULT", "EUR");
define("CCY_2DEF_USD", "USD");
define("KIND_CC_VISA", 0);
define("KIND_CC_MASTERCARD", 1);
define("KIND_CC_AMERICAN_EXPRESS", 2);
define("KIND_CC_DINERS_CLUB", 3);
define("KIND_CC_MAESTRO", 4);
define("KIND_CC_VISA_ELECTRON", 5);
define("KIND_CC_DISCOVER", 6);
define("KIND_CC_ELO", 7);
define("KIND_CC_JCB", 8);
define("KIND_CC_PAYPAL", 10);
define("KIND_CC_BOLETO", 11);
define("KIND_CC_APPSTORE", 12);
define("KIND_CC_UATP", 13);
define("KIND_CC_CUP", 14);
define("KIND_CC_VIAS", 15);
define("KIND_CC_NO_CARD", 99);

// Invoices
define("FIRST_INVOICE",                     1000); // Number to start the first invoice.
define("FORMAT_NUMBER_INVOICE",             7); // 7 zeroes
define("DEFAULT_PREFIX_INVOICE",            "ABA");
define("NAME_ADDRESS_ABA_INVOICE",          "ENGLISH WORLDWIDE, S.L.");
define("CIF_ADDRESS_ABA_INVOICE",           "B64401482");
define("ADDRESS_ABA_INVOICE",               "c/ Aribau, 240 - 7º L");
define("MORE_ADDRESS_ABA_INVOICE",          "08006 BARCELONA (SPAIN)");
define("TELEPHONE_ADDRESS_ABA_INVOICE",     "+34 902 024 386");
define("DEFAULT_INVOICE_DATE_START",        "2015-01-01");
define("DEFAULT_PREFIX_INVOICE_PREVIOUS",   "ABP");
define("DEFAULT_PREFIX_INVOICE_APPSTORE",   "IOS");
define("USERVATNUMBER_INVOICE_APPSTORE",    "LU20165772");
define("NAME_INVOICE_APPSTORE",             "ITUNES SARL");


// Key sentence to code sensitive information:
define("WORD4CARDS", "Yii ABA English Key");
// Status of Payment transactions:
define("PAY_PENDING", "0");
define("PAY_INITIATED", "5");
define("PAY_FAIL", "10");
define("PAY_CANCEL", "50");
define("PAY_SUCCESS", "30");
define("PAY_REFUND", "40");
// STATUS OF RECONCILIATION STATUS
define("PAY_RECONC_ST_NOT_PROCESSED", NULL);
define("PAY_RECONC_ST_NOT_MATCHED", 0);
define("PAY_RECONC_ST_MATCHED", 1);
// THRESHOLD to compare date and time for payments from our side and gateway(minutes):
define("PAY_THRESHOLD_RECONC_PAYMENTS", 120);
// STATUS OF PAYMENT RECONCILIATION STATUS
define("PAY_FILE_STATUS_MATCHED", "MATCHED");
define("PAY_FILE_STATUS_INCONSISTENT", "INCONSISTENT");
define("PAY_FILE_STATUS_NOTFOUND", "NOT_FOUND");
define("PAY_FILE_STATUS_PREV_MATCHED", "PREVIOUSLY_MATCHED");
define("PAY_FILE_STATUS_ERROR", "ERROR");
// Status of BOLETO REQUESTS:
define("PAY_BOLETO_PENDING", 0);
define("PAY_BOLETO_FAIL", 10);
define("PAY_BOLETO_SUCCESS", 30);
// DISCOUNT TYPES:
define('PERCENTAGE', 'PERCENTAGE');
define('FINALPRICE', 'FINALPRICE');
define('AMOUNT', 'AMOUNT');
// Cancellation origin:values can be 1-User cancelled,2-Groupon expired,3-Failed subscription
define('PAY_CANCEL_USER', 1);
define('PAY_CANCEL_DATEOFF_GROUPON', 2);
define('PAY_CANCEL_FAILED_RENEW', 3);
define('PAY_CANCEL_UNDEFINED', 4);
// Origins from cancellations: 1-User cancels,3-Expiration,4-PayPal IPN,5-Intranet employee,6-Change Product, 7-Contact new plan (family plan)
define('STATUS_CANCEL_USER', 1);
define('STATUS_CANCEL_EXPFAIL', 3);
define('STATUS_CANCEL_IPN', 4);
define('STATUS_CANCEL_INTRANET', 5);
define('STATUS_CANCEL_CHANGEPROD', 6);
define('STATUS_CANCEL_CONTRACT_NEW_PLAN', 7);
// From this day on next payment will be inserted on 1st.
define('LAST_DAY_PAYMENT', 28);
// Methods of payments available for the user :
define("PAY_METHOD_CARD", 1);
define("PAY_METHOD_PAYPAL", 2); // Wallet / PayPal
define("PAY_METHOD_BOLETO", 4);

//define("PAY_METHOD_ADYEN_RU", 5);
// pagos alternativos adyen HPP
define("PAY_METHOD_ADYEN_HPP", 6);


// Payments suppliers alias available in the system :
define("PAY_SUPPLIER_XFRIENDS", 'NULL');
define("PAY_SUPPLIER_CAIXA", 1); // MUST BE EQUAL TO table [pay_suppliers].["LA Caixa"]
define("PAY_SUPPLIER_PAYPAL", 2);
define("PAY_SUPPLIER_GROUPON", 3);
define("PAY_SUPPLIER_ALLPAGO_BR", 4);
define("PAY_SUPPLIER_B2B", 5);
define("PAY_SUPPLIER_ALLPAGO_MX", 6);
define("PAY_SUPPLIER_ALLPAGO_BR_BOL", 7);
define("PAY_SUPPLIER_ALLPAGO_MX_OXO", 8);
define("PAY_SUPPLIER_APP_STORE", 9);
define("PAY_SUPPLIER_ADYEN", 10);
define("PAY_SUPPLIER_ADYEN_HPP", 11);
define("PAY_SUPPLIER_ANDROID_PLAY_STORE", 12);
define("PAY_SUPPLIER_Z", 13);


// Status of AppStore Pending Payments
define("PAY_APP_STORE_PAY_PENDING", 0);
define("PAY_APP_STORE_PAY_SUCCESS", 10);
define("PAY_APP_STORE_PAY_CANCEL", 20);

// Status of PlayStore Payments
define("PAY_PLAY_STORE_PAY_PENDING", 0);
define("PAY_PLAY_STORE_PAY_SUCCESS", 10);
define("PAY_PLAY_STORE_PAY_CANCEL", 20);

// Status of Adyen Pending Payments
define("PAY_ADYEN_PAY_NOTIFICATION_PENDING", 0);
define("PAY_ADYEN_PAY_NOTIFICATION_SUCCESS", 10);
define("PAY_ADYEN_PAY_NOTIFICATION_FAIL", 20);


// Status of subscriptions plus type of subscription / last subscription
//The value equals to: user.usertype + payment.status + payment.paySuppExtId
define("SUBS_PREMIUM_A_CARD_A",   2004); // PREMIUM(2) + ACTIVE (0-PENDING) + CARD (ALLPAGO-BR)
define("SUBS_PREMIUM_A_CARD_C",   2001); // PREMIUM(2) + ACTIVE (0-PENDING) + CARD (CAIXA)
define("SUBS_PREMIUM_A_CARD_M",   2006); // PREMIUM(2) + ACTIVE (0-PENDING) + CARD (ALLPAGO-MX)
define("SUBS_PREMIUM_A_PAYPAL",   2002); // PREMIUM(2) + ACTIVE (0-PENDING) + PAYPAL
define("SUBS_PREMIUM_A_CARD_AD", 20010); // PREMIUM(2) + ACTIVE (0-PENDING) + ADYEN
define("SUBS_PREMIUM_A_CARD_IOS", 2009); // PREMIUM(2) + ACTIVE (0-PENDING) + IOS
define("SUBS_PREMIUM_A_ZUORA", 20013); // PREMIUM(2) + ACTIVE (0-PENDING) + ZUORA
define("SUBS_PREMIUM_A_ANDROID", 20012); // PREMIUM(2) + ACTIVE (0-PENDING) + ANDROID

define("SUBS_PREMIUM_C_CARD_A",   2504); // PREMIUM(2) + CANCELLED (50-PENDING) + CARD (ALLPAGO-BR)
define("SUBS_PREMIUM_C_CARD_M",   2506); // PREMIUM(2) + ACTIVE (0-PENDING) + CARD (ALLPAGO-MX)
define("SUBS_PREMIUM_C_CARD_C",   2501); // PREMIUM(2) + CANCELLED (50-PENDING) + CAIXA
define("SUBS_PREMIUM_C_CARD_AD", 25010); // PREMIUM(2) + CANCELLED (50-PENDING) + ADYEN
define("SUBS_PREMIUM_C_CARD_IOS", 2509); // PREMIUM(2) + CANCELLED (50-PENDING) + IOS
define("SUBS_PREMIUM_C_PAYPAL", 2502); // PREMIUM(2) + CANCELLED (50-PENDING) + PAYPAL
define("SUBS_PREMIUM_C_BOLETO_BR", 2507); // PREMIUM(2) + ACTIVE (0-PENDING) + CARD (ALLPAGO-MX)
define("SUBS_PREMIUM_THROUGHPARTNER", 2503);
define("SUBS_PREMIUM_THROUGHB2B", 2505);
define("SUBS_PREMIUM_C_ANDROID", 25012); // PREMIUM(2) + CANCELLED (50-PENDING) + IOS

define("SUBS_FREE", 100);
define("SUBS_EXPREMIUM_CARD", 1501);
define("SUBS_EXPREMIUM_PAYPAL", 1502);
define("SUBS_EXPREMIUM_THROUGHPARTNER", 1503);
define("SUBS_DELETED", -100);
define("SUBS_NULL", NULL);
// Key Words used by PayPal language to understand status, etc.:
define("PAY_PAL_VERIFIED", "VERIFIED");
define("PAY_PAL_INVALID", "INVALID");
define("PAY_PAL_SEPARATOR", ",");
// Payment form for Brazil, for AllPago gateway. Type of cadastro number:
define("PAY_ID_BR_CPF", "CPF");
define("PAY_ID_BR_CNPJ", "CNPJ");
// LEVELS OF VALIDATION FOR CARDS in PAYMENTS:
define("PAY_VALID_CARD_LEVEL_NONE", "NONE");
define("PAY_VALID_CARD_LEVEL_LOW", "NUMERIC");
define("PAY_VALID_CARD_LEVEL_MEDIUM", "LUHN");
define("PAY_VALID_CARD_LEVEL_HIGH", "HIGH");

// The days we give away to users after they expire:
define("WAIT_TPV_EXPIRE_PERIOD_DAYS", 1);
define("WAIT_PAYPAL_EXPIRE_PERIOD_DAYS", 16);
define("WAIT_ADYEN_HPP_EXPIRE_PERIOD_DAYS", 13);
define("WAIT_ANDROID_PLAYSTORE_EXPIRE_PERIOD_DAYS", 2);
// Web Services configuration and login stuff -----------------------------------
define("WORD4WS", "Yii ABA English Key 2");
// Web Services configuration and login stuff for partners-----------------------------------
define("WORD4WSPARTNERS", "Aba English Partner Key");
// Web Services configuration and login stuff for extranet -----------------------------------
define("WORD4WSEXTRANET", "Aba English Extranet Key");
// Web Services configuration and login stuff for external apps -----------------------------------
define("WORD4WSEXTERNAL", "Aba English External Login Key");

define("URL_WS_NO_LOGIN", "wsoapaba/usersapi" .
    "@ws/usersapi" .
    "@wspostaba/paypal" .
    "@wsoapabaregister/usersapi" .
    "@wspostaba/simulatepaypalverification" .
    "@partnersimulators/simulateselligent" .
    "@wspostaba/simulatorbannerdynselligent" .
    "@wspostaba/proxybannerdynselligent" .
    "@wspostaba/simulatortrackings" .
    "@wsoapabafeed/feed" .
    "@wsoapabaselligent/senddata" .
    "@site/recoverpassword" .
    "@site/recoverpasswordprocess" .
    "@site/fblogin" .
    "@site/inlogin" .
    "@site/resetpassword" .
    "@site/login" .
    "@login" .
    "@external/signup" .
    "@pages/oops" .
    "@pages/delco" .
    "@recover-password" .
    "@reset-password" .
    "@mobile/tryforfree" .
    "@mobile/trycomplete" .
    "@mobile/route" .
    "@wsrestregister/view/object/version" .
    "@wsrestregister/view/object/iprecognition" .
    "@wsrestregister/login" .
    "@wsrestregister/register" .
    "@wsrestregister/recoverpassword" .
    "@wsrestregister/registerfacebook" .
    "@wsrestuser/update" .
    "@wsrestuser/update/object/userlevel" .
    "@wsrestuser/update/object/followupvideo" .
    "@wsrestuser/update/object/freetopremium" .
    "@wsrestuser/list/object/videoclasses" .
    "@wsrestuser/list/object/productstiers" .
    "@wsrestuser/list/object/userprofiles" .
    "@wsrestuser/view/object/videoclass" .
    "@partnersimulators/paymentallpago" .
    "@wscourseapiinfo/view/object/version" .
    "@wsoapaba/sendnotification" .
    "@wsoapadyen/usersapi" .
    "@wsoapadyen/sendnotification" .
    "@wspostaba/sendnotification" .
    "@wsrestregister/updateattribution" .
    "@site/products" .
    "@site/directpayment" .
    "@direct-payment" .
    "@direct-payment-process" .
    "@acceso-directo-aba-premium" .
    "@accesso-diretto-aba-premium" .
    "@acesso-direto-aba-premium" .
    "@direct-access-aba-premium" .
    "@acces-direct-aba-premium" .
    "@direkt-zugang-aba-premium" .
    "@leveltest/index" .
    "@leveltest" .
    "@pryamoy-dostup-aba-premium" .
    "@payments/wsGetUserData"
);

// auto Login keyword
define("MAGIC_LOGIN_KEY", "autol");
// Duration in days of login cookie Yii
define("DURATION_LOGIN", "365");
// Max attempts to try a public URL, it is a light measure, maybe a dumb one:
define("MAX_ATTEMPTS", 30);
// Cookie login binding to the web
define("CKN_FLAG_WEB_LOGIN", "abawebcampus");
// Language default for any language we don't have:
define("LANG_CAMPUS_DEFAULT", "en");
// Link to landing payment action:
define("LAND_PAYMENT", "payments/payment");

// Partners related stuff --------------------------------------------------------
define("PARTNER_ID_MOBILE", 300031);
define("PARTNER_ID_MOBILE_ANDROID", 400002);
define("PARTNER_ID_RECURRINGPAY", 300006);
define("PARTNER_ID_ADWORDS", 300007);
define("PARTNER_ID_EXABAENGLISH", 300011);
define("PARTNER_ID_EXFLASHSALES", 300013);
define("PARTNER_ID_EXSOCIALAFFILIATES", 300014);
define("PARTNER_ID_NETAFFILIATION", 6001);
define("PARTNER_ID_ADGOESDOUBLE", 6021);
define("PARTNER_ID_ADGOFRDOUBLE", 6022);
define("PARTNER_ID_ADGOITDOUBLE", 6023);
define("PARTNER_ID_CLICKPOINT_ES", 6024);
define("PARTNER_ID_CLICKPOINT_FR", 6025);
define("PARTNER_ID_CLICKPOINT_IT", 6026);
define("PARTNER_ID_CLICKPOINT_ES_SGL", 6036);
define("PARTNER_ID_CLICKPOINT_FR_SGL", 6037);
define("PARTNER_ID_CLICKPOINT_IT_SGL", 6038);
define("PARTNER_ID_CLICKPOINT_BR_SGL", 6062);
define("PARTNER_ID_PERMISSION_ES", 6029);
define("PARTNER_ID_ZANOX", 6003);
define("PARTNER_ID_ZANOX_CPL", 6008);
define("PARTNER_ID_NETAFFILIATIONDISPLAY", 6010);
define("PARTNER_ID_ELOGIA_ES", 6014);
define("PARTNER_ID_ELOGIA_FR", 6015);
define("PARTNER_ID_ELOGIA_IT", 6016);
define("PARTNER_ID_WALVA_IT_SGL", 6066);
define("PARTNER_ID_WALVA_IT_DISPLAY", 6067);
define("PARTNER_ID_WALVA_BR_SGL", 6068);
define("PARTNER_ID_WALVA_BR_DISPLAY", 6069);
define("PARTNER_ID_ADWORDS_ITALY", 300009); // Campaign AdWords Italy
define("PARTNER_ID_ADWORDS_FRANCE", 300020); // Campaign AdWords France
define("PARTNER_ID_FACEBOOK", 300010); // FACEBOOK
define("PARTNER_ID_YOUTUBE_ADS_SPAIN", 300019); // YOUTUBE ADS ADWORDS SPAIN
define("PARTNER_ID_ADWORDS_MEXICO", 300024); // Campaign AdWords Mexico

define("PARTNER_ID_BING_ADS_SPAIN", 300021); // Bing Ads Spain
define("PARTNER_ID_BING_ADS_FRANCE", 300022); // Bing Ads France
define("PARTNER_ID_BING_ADS_ITALY", 300023); // Bing Ads Italy
define("PARTNER_ID_BING_ADS_USA", 300032); // Bing Ads USA
define("PARTNER_ID_BING_ADS_STREAM_FR", 300184); // Bing Ads Italy
define("PARTNER_ID_BING_ADS_STREAM_IT", 300185); // Bing Ads USA

define("PARTNER_ID_POWERSPACE_ES", 300186); // Power Space

define("PARTNER_ID_FACEBOOK_ADSFR", 300026); // Facebook Ads France
define("PARTNER_ID_ADWORDS_USA", 300027); // Ad Words Usa
define("PARTNER_ID_ADWORDS_CANADA", 300029); // Ad Words Canada
define("PARTNER_ID_ADWORDS_UK", 300030); // Ad Words UK
define("PARTNER_ID_ADWORDS_BR", 300034); // adWords-br, 300034;
define("PARTNER_ID_ADWORDS_GE", 300036); // adWords-Germany, 300036;
define("PARTNER_ID_ADWORDS_PT", 300062); // adWords-Portugal
define("PARTNER_ID_ADWORDS_CO", 300075); // adWords-Colombia

define("PARTNER_ID_ADWORDS_PO", 300089); // adWords-Polonia
define("PARTNER_ID_ADWORDS_AT", 300093); // adWords-Austria
define("PARTNER_ID_ADWORDS_PE", 300097); // adWords-Peru
define("PARTNER_ID_ADWORDS_CL", 300076); // adWords-Chile

define("PARTNER_ID_ADWORDS_EC", 300200);
define("PARTNER_ID_ADWORDS_DO", 300201);

define("PARTNER_ID_ADWORDS_ELSALVADOR", 300210); //adWords-El Salvador
define("PARTNER_ID_ADWORDS_GUATEMALA", 300211); //adWords-Guatemala
define("PARTNER_ID_ADWORDS_BOLIVIA", 300212); // adWords-Bolivia

define("PARTNER_ID_MAVERICK", 6002);
define("PARTNER_ID_MAVERICKFRDBL17", 6017);
define("PARTNER_ID_MAVERICKFRDBL19", 6019);
define("PARTNER_ID_MAVERICK_ES_SGL", 6033); // Maverick-es-single

define("PARTNER_ID_ACAI_MX_SGL", 6095);

define("PARTNER_ID_INKAMARKETING_MX_DISPLAY", 6114);
define("PARTNER_ID_INKAMARKETING_MX_SGL", 6115);
define("PARTNER_ID_INKAMARKETING_IT_DISPLAY", 6116);
define("PARTNER_ID_INKAMARKETING_IT_SGL", 6117);

define("PARTNER_ID_CLICKPOINT_CL_SGL", 6093);
define("PARTNER_ID_CLICKPOINT_CO_SGL", 6078);
define("PARTNER_ID_CLICKPOINT_PE_SGL", 6094);

define("PARTNER_ID_02B_ES", 300091); // Economia Digital
define("PARTNER_ID_DIARIO_GOL_ES", 300092); //

define("PARTNER_ID_SOFTONIC_ADS", 300016 ); //softonic-ads
define("PARTNER_ID_SOFTONIC_ADS_FR", 300017); //softonic-ads-fr
define("PARTNER_ID_SOFTONIC_ADS_IT", 300018 ); //softonic-ads-it
define("PARTNER_ID_SOFTONIC_ADS_PT", 300028); //softonic-ads-pt
define("PARTNER_ID_SOFTONIC_ADS_EN", 300035); //softonic-ads-en

define("PARTNER_ID_SOFTONIC_ADS_DE", 300041); //softonic-ads-de
define("PARTNER_ID_SOFTONIC_ADS_JAP", 300083); //softonic-ads-jap
define("PARTNER_ID_SOFTONIC_ADS_PLK", 300084); //softonic-ads-plk
define("PARTNER_ID_SOFTONIC_ADS_RUS", 300085); //softonic-ads-rus




define("PARTNER_ID_LIGATUS_NET_FR", 300037); //Ligatus-net-fr
define("PARTNER_ID_LIGATUS_NET_ES", 300038); //Ligatus-net-es
define("PARTNER_ID_ORANGE_NET_FR", 300039); //orange-net-fr
define("PARTNER_ID_VEESIBLE_NET_IT", 300040); //veesible-net-it
define("PARTNER_ID_ERETAIL_NET_FR", 300042); //Eretail-net-fr
define("PARTNER_ID_CPM_NET_ES", 300043); //CPM-net-es

define("PARTNER_ID_FACEBOOK_ADS_IT", 300053); //Facebook-ads-it
define("PARTNER_ID_FACEBOOK_ADS_BR", 300054); //Facebook-ads-br
define("PARTNER_ID_FACEBOOK_ADS_DE", 300055); //Facebook-ads-de
define("PARTNER_ID_FACEBOOK_ADS_MX", 300069); //Facebook-ads-mx
define("PARTNER_ID_FACEBOOK_ADS_CO", 300070); //Facebook-ads-co
define("PARTNER_ID_FACEBOOK_ADS_AR", 300071); //Facebook-ads-ar
define("PARTNER_ID_FACEBOOK_ADS_CL", 300072); //Facebook-ads-cl
define("PARTNER_ID_FACEBOOK_ADS_PE", 300073); //Facebook-ads-pe
define("PARTNER_ID_FACEBOOK_ADS_EC", 300074); //Facebook-ads-ec
define("PARTNER_ID_FACEBOOK_ADS_PT", 300130); //Facebook-ads-pt

define("PARTNER_ID_FACEBOOK_ADS_USA", 300162); //Facebook-ads-usa
define("PARTNER_ID_FACEBOOK_ADS_BO", 300163); //Facebook-ads-bo
define("PARTNER_ID_FACEBOOK_ADS_PY", 300164); //Facebook-ads-py
define("PARTNER_ID_FACEBOOK_ADS_CR", 300167); //Facebook-ads-py
define("PARTNER_ID_FACEBOOK_ADS_DO", 300168); //Facebook-ads-py
define("PARTNER_ID_FACEBOOK_ADS_VE", 300169); //Facebook-ads-py
define("PARTNER_ID_FACEBOOK_ADS_GR", 300190); //facebook-ads-gr
define("PARTNER_ID_FACEBOOK_ADS_TR", 300191); //facebook-ads-tr
define("PARTNER_ID_FACEBOOK_ADS_GT", 300192); //facebook-ads-gt
define("PARTNER_ID_FACEBOOK_ADS_SV", 300193); //facebook-ads-sv
define("PARTNER_ID_FACEBOOK_ADS_UY", 300194); //facebook-ads-uy
define("PARTNER_ID_FACEBOOK_ADS_HU", 300195); //facebook-ads-hu
define("PARTNER_ID_FACEBOOK_ADS_BG", 300196); //facebook-ads-bg

define("PARTNER_ID_B2B_ES", 300165); //B2B España
define("PARTNER_ID_B2B_IT", 300166); //B2B Italia

define("PARTNER_ID_FACEBOOK_VID_ES", 300101); //Facebook-vid-es
define("PARTNER_ID_FACEBOOK_VID_FR", 300102); //Facebook-vid-fr
define("PARTNER_ID_FACEBOOK_VID_IT", 300103); //Facebook-vid-it
define("PARTNER_ID_FACEBOOK_VID_BR", 300104); //Facebook-vid-br
define("PARTNER_ID_FACEBOOK_VID_DE", 300105); //Facebook-vid-de
define("PARTNER_ID_FACEBOOK_VID_MX", 300107); //Facebook-vid-mx
define("PARTNER_ID_FACEBOOK_VID_CO", 300108); //Facebook-vid-co
define("PARTNER_ID_FACEBOOK_VID_CL", 300109); //Facebook-vid-cl
define("PARTNER_ID_FACEBOOK_VID_PE", 300110); //Facebook-vid-pe
define("PARTNER_ID_FACEBOOK_VID_EC", 300111); //Facebook-vid-ec
define("PARTNER_ID_FACEBOOK_VID_PT", 300131); //Facebook-vid-pt

define("PARTNER_ID_FACEBOOK_RMKT_IT", 300133);
define("PARTNER_ID_FACEBOOK_RMKT_FT", 300134);
define("PARTNER_ID_FACEBOOK_RMKT_ES", 300135);
define("PARTNER_ID_FACEBOOK_RMKT_BR", 300136);

define("PARTNER_ID_TWITTER_ES", 300129);
define("PARTNER_ID_TWITTER_IT", 300141);
define("PARTNER_ID_TWITTER_FR", 300142);
define("PARTNER_ID_TWITTER_PT", 300143);
define("PARTNER_ID_TWITTER_BR", 300144);

define("PARTNER_ADWORDS_LONGTAIL_ES", 300156);
define("PARTNER_ADWORDS_LONGTAIL_IT", 300157);
define("PARTNER_ADWORDS_LONGTAIL_FR", 300158);
define("PARTNER_ADWORDS_LONGTAIL_EN", 300159);
define("PARTNER_ADWORDS_LONGTAIL_PT", 300160);
define("PARTNER_ADWORDS_LONGTAIL_DE", 300161);

define("PARTNER_TARINGA_ES", 300170);
define("PARTNER_TARINGA_MX", 300171);

define("PARTNER_HARREN_MEDIA_ES", 300176);
define("PARTNER_HARREN_MEDIA_FR", 300177);
define("PARTNER_HARREN_MEDIA_IT", 300178);
define("PARTNER_HARREN_MEDIA_DE", 300179);

define("PARTNER_ID_2X1_MKT_ABAENGLISH", 300181); // ABA2X1
define("PARTNER_ID_2X1_RENEWALS_FAMILY_PLAN", 300247); // RENEWALS FAMILYPLAN
define("PARTNER_ID_ABA_FAMILYPLAN", 300255); // FAMILYPLAN


define("PARTNER_ID_REDADVERTISEMENT_IT", 300137);
define("PARTNER_ID_REDADVERTISEMENT_FR", 300138);
define("PARTNER_ID_ERETAIL_DISPLAY_ES", 300139);

define("PARTNER_ID_PLISTA_ES", 300106); //Plista-Es

define("PARTNER_ID_GMAIL_ADS_ES", 300056); //Gmail-ads-es
define("PARTNER_ID_GMAIL_ADS_FR", 300057); //Gmail-ads-fr
define("PARTNER_ID_GMAIL_ADS_IT", 300058); //Gmail-ads-it

define("PARTNER_ID_ADGO_ES", 6045); //adgo-es-single
define("PARTNER_ID_ADGO_FR", 6046); //adgo-fr-single
define("PARTNER_ID_ADGO_IT", 6047); //adgo-it-single

define("PARTNER_ID_ADREACH",300095); // AdReach Germany, single optin

define("PARTNER_ACAI_ES", 6053); //acai-es-single
define("PARTNER_ACAI_FR", 6054); //acai-fr-single
define("PARTNER_ACAI_IT", 6055); //acai-it-single
define("PARTNER_ACAI_BR", 6056); //acai-br-single

define("PARTNER_COLIBRI_FR", 6060); //colibri-fr-single
define("PARTNER_COLIBRI_IT", 6061); //colibri-it-single

define("PARTNER_KWANCO_IT", 6102); //Kwanco-it-single
define("PARTNER_KWANCO_DISPLAY", 6103); //Kwanco-Display

define("PARTNER_GEKO_IT", '6030'); // GEKO IT
define("PARTNER_UNIEDI_ES", '300033'); // Unidad-editorial-es
define("PARTNER_JUNTAEXTREMADURA", 7004);

define("PARTNER_IKREATE", 300087); //Ikreate-single
define("PARTNER_DISPLAY_ES_CHICA", 300090); // Experimento Display ES Chica

define("PARTNER_ID_NIVORIA_IT", 6070); // NIVORIA_IT single
define("PARTNER_ID_NIVORIA_DE", 6071); // NIVORIA_DE single
define("PARTNER_ID_NIVORIA_BR", 6072); // NIVORIA_BR single
define("PARTNER_ID_NIVORIA_MX", 6089); // NIVORIA_MX single

define("PARTNER_ID_EXOCLICK_ES", 6074); // Pixel Exoclick - ES
define("PARTNER_ID_EXOCLICK_FR", 6075); // Pixel Exoclick - FR
define("PARTNER_ID_EXOCLICK_MX", 6076); // Pixel Exoclick - MX
define("PARTNER_ID_EXOCLICK_IT", 6077); // Pixel Exoclick - IT

define("PARTNER_ID_CLICKPOINT_MX", 6079); // Pixel Clickpoint - MX

define("PARTNER_ID_DBM", 300189); // Pixel DBM

define("PARTNER_ID_ANTUNEZ_INGLES_ES", 8013);
define("PARTNER_ID_RUBEN_BARREIROS_ES", 8005);
define("PARTNER_ID_MARTINEZ_PELICULATOR_ES", 8039);
define("PARTNER_ID_OMAR_VASQUEZ_ES", 8010);
define("PARTNER_ID_PENARRUBIA_ES", 300059);
define("PARTNER_ID_SCAMBI_EUROPEI_ES", 8023);
define("PARTNER_ID_WALKER_SOUZA_PT", 8012);
define("PARTNER_ID_KEYLANGUAGE_SERVICES_ES", 8052);

define("PARTNER_ID_CINCODEDITOS_ES", 8059);
define("PARTNER_ID_MOIJEFAIS_FR", 8060);
define("PARTNER_ID_LONDRADAVIVERE_IT", 8060);
define("PARTNER_ID_CHARLOTTE_LANGUAGES_ES", 8056);

define("PARTNER_ID_INGLES_ONLINE_PRACTICA", 8062);
define("PARTNER_ID_ME_VOY_AL_MUNDO", 8064);

define("PARTNER_ID_APRENDER_GRATIS_ES", 8066);
define("PARTNER_ID_NOEXPERIENCEWORK", 8043);

define("PARTNER_ID_ITALOEUROPEO_COM", 8069);
define("PARTNER_ID_CV_IN_INGLESE_IT", 8070);

define("PARTNER_ID_INGENIEROGEEK", 8067);
define("PARTNER_ID_ELEVENREVIEWS", 8071);
define("PARTNER_ID_INGLESPORTUCUENTA", 8072);

define("PARTNER_ID_PAYCLICK_IT_SINGLE", 6124);
//define("PARTNER_ID_PAYCLICK_IT_DISPLAY", 6127);
define("PARTNER_ID_PAYCLICK_FR_SINGLE", 6125);
//define("PARTNER_ID_PAYCLICK_FR_DISPLAY", 6126);

define("PARTNER_ID_ADDOOR_ES_DISPLAY", 6128);
define("PARTNER_ID_ADDOOR_ES_NATIVEADS", 6129);

define("PARTNER_ID_APP_IOS_MOB", 400004);
define("PARTNER_ID_APP_IOS_TAB", 400005);

define("PARTNER_ID_QUIEROAPRENDERIDIOMAS", 8073);
define("PARTNER_ID_TRILABO", 8074);
define("PARTNER_ID_MASSAO_NET_BR", 8075);
define("PARTNER_ID_ADVISATO_IT", 8076);
define("PARTNER_ID_FCEPASS_COM", 8077);

define("PARTNER_ID_UPGRADE", 300265);
define("PARTNER_ID_IOS_RECURRING", 400116);

define("PARTNER_ID_DIRECT_PAYMENT_LACAIXA", 300269);

define("PARTNER_ID_ANDROID_RECURRING", 400223);

define("PARTNER_ID_Z", 600016);
define("PARTNER_ID_Z_RECURRING", 600017);

// B2B Channel Groups
define("CHANNEL_ABA_G", 1);
define("CHANNEL_ABA_W", "B2B Extranet");

// 0-Aba English., 1-Ventas Flash,  2-Social, 3-Affiliates, 20-Others';
define("PARTNER_ABA_G", 0);
define("PARTNER_GROUPON_G", 1);
define("PARTNER_SOCIAL_G", 2);
define("PARTNER_AFFILIATES_G", 3);
define("PARTNER_B2B_G", 4);
define("PARTNER_POSTAFFILIATES_G", 5);
define("PARTNER_ABAMOBILE_G", 6);
define("PARTNER_OTHER_G", 20);
define("PARTNER_ABA_W", "abaenglish");
define("PARTNER_GROUPON_W", "flashsales");
define("PARTNER_SOCIAL_W", "social");
define("PARTNER_AFFILIATES_W", "affiliates");
define("PARTNER_B2B_W", "b2b");
define("PARTNER_POSTAFFILIATES_W", "post-affiliates-pro");
define("PARTNER_ABAMOBILE_W", "abaenglish-mobile");
define("PARTNER_OTHER_W", "other");

define("PARTNER_GROUP_B2B", 1);
define("PARTNER_GROUP_B2C", 2);

// Member get member partners
define("PARTNER_MGM_EMAIL", 9000);
define("PARTNER_MGM_FACEBOOK", 9001);

// Constants related to column user.registerSource, possible values: -------------
define("REGISTER_USER", 1);
define("REGISTER_USER_FROM_LEVELTEST", 2);
define("REGISTER_USER_FROM_PARTNER", 3);
define("REGISTER_USER_FROM_B2B", 3);
define("REGISTER_USER_FROM_B2B_EXTRANET", 5);
define("REGISTER_USER_FROM_FACEBOOK", 6);
define("REGISTER_USER_FROM_DIRECT_PAYMENT", 7);
// SELLIGENT communications related -----------------------------------------------
//Maximum days the user is allowed not to be synchronized with SELLIGENT
define("INTERVAL_SELLIGENT", -2);

// Maximum days that the process in charge of releasing free units to users is able to recover
// from NO success execution. From values above 2, we can create inconsistencies in the released units
// for certain users
define("MAX_RELEASE_UNITS_ATONCE", 2);

// Unique domain for LIVE environment. Useful to execute certain scenarios only available for production:
define("DOMAIN_LIVE", "campus.abaenglish.com");

// List of countries. Imp: The number does reference to the countryID from the Data Base
define("COUNTRY_ITALY", 105);
define("COUNTRY_FRANCE", 73);
define("COUNTRY_BRAZIL", 30);
define("COUNTRY_MEXICO", 138);
define("COUNTRY_PORTUGAL", 172);
define("COUNTRY_SPAIN", 199);
define("COUNTRY_US", 226);
define("COUNTRY_CO", 47);
define("COUNTRY_ABACOUNTRY_OLD", 300);

// Signature Key for Mobile web services:
define("WORD4MOBILEWS", "Mobile Outsource Aba AgnKey");

// keywords, letters to find out the device source from which user was registered:
define("DEVICE_SOURCE_MOBILE", "m");
define("DEVICE_SOURCE_PC", "c");
define("DEVICE_SOURCE_TABLET", "t");
define("DEVICE_SOURCE_EXTRANET", "d");

// welcome workflow
define("WELCOME_3_STEPS", 0);
define("WELCOME_USER_DATA", 1);
define("WELCOME_LEVEL_SELECT", 2);
define("WELCOME_PRICE_PLANS", 3);
define("WELCOME_MGM", 4);
define("WELCOME_TESTLEVEL", -1);

// Users to reach in order to give PREMIUM FRIEND ACCESS to users.
define("MGM_REQUIRED_USERS", 5);
define("MGM_PREMIUM_DAYS", 7);

// Days threshold since registration double opt-in is printed.
define("DAYS_THRESHOLD_DBLOPTIN", 30);

// Days threshold since Boleto requested until Boleto is considered OFF.
//define("DAYS_THRESHOLD_BOLETO", 29); // 30 dais
define("DAYS_THRESHOLD_BOLETO", 4); // 5 dais

// Days threshold since Boleto requested until Boleto is considered OFF.
define("USER_DICTIONARY_DEF_VERSION_PROGRESS",  30);
define("USER_DICTIONARY_DEF_VERSIONHTML_PROGRESS",  30);
define("USER_DICTIONARY_DEF_VERSION_AUDIO",     24);

// App store data
// Shared secret apple
define("PAY_APP_STORE_API_PWD", "b7d5f16ee3b94964ae363b2785a7694d");
define("APP_URL", "https://app.adjust.com/wz8stb");
define("APP_URL_PARTNER_5010", "https://app.adjust.com/vx2wc4");

// Teachers
define("TEACHER_REGISTER_SOURCE_ABA",       "1");
define("TEACHER_REGISTER_SOURCE_EXTRANET",  "2");

//
//define("ADYEN_HPP_ATTEMPTS_MAX_REFUND", 150);
define("ADYEN_HPP_ATTEMPTS_MAX_REFUND", 30);
define("ADYEN_HPP_ATTEMPTS_SLEEP",      2);
define("ADYEN_HPP_ATTEMPTS_INTERVAL",   3000);

define("ADYEN_HPP_RESULT_OK",                   600);
define("ADYEN_HPP_RESULT_INTERNAL_ERR",         601);
define("ADYEN_HPP_RESULT_NOT_VALID_RESPONSE",   602);
define("ADYEN_HPP_RESULT_PENDING",              603);
define("ADYEN_HPP_RESULT_REFUSED",              604);
define("ADYEN_HPP_RESULT_CANCELLED",            605);

define("PAY_ADYEN_PENDING", 0);
define("PAY_ADYEN_SUCCESS", 30);
define("PAY_ADYEN_PENDING_REFUND_PENDING", 0);
define("PAY_ADYEN_PENDING_REFUND_FAIL", 10);
define("PAY_ADYEN_PENDING_REFUND_SUCCESS_NOTIFICATION", 30);
define("PAY_ADYEN_PENDING_REFUND_REFUNDED", 40);
define("FAMILYPLAN_NUM_USERS_360", 4);

// ABA DEFAULT COUNTRY ID in the aba_b2c.country, see DefaultIPCountryCode in aba_b2c.config
define("DEFAULT_COUNTRY_ID", 301);
// Website
define("DEFAULT_PARTNER_ID", 300001);

// ABAWEBAPPS VERSION
define("ABAWEBAPPS_VERSION", file_get_contents(dirname(__FILE__).DIRECTORY_SEPARATOR.'version'));

define('STATIC_PAGES_SITE', 'http://pages.abaenglish.com');

// ABA PARTNERID FOR USERS WHO BOUGHT PREMIUM THROUGH ALT CS BLOCK AND HAVE CUSTOMER SUPPORT
define('BCSPARTNERID','300277');

define('COOLDATA_USER_LOGIN', '1');
define('COOLDATA_USER_REGISTER', '1');

define('SELLIGENT_PARTNER_DEFAULT_USERID', 16);
