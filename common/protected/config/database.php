<?php
/**
 * Connections to database servers
 */
return array(
    'components' => array(
        'db'=>array(
            'class'=>'CDbConnection',
            'connectionString' => 'mysql:host='.$_SERVER["DB_HOSTNAME"].';dbname=aba_b2c',
            'emulatePrepare' => true,
            'username' => $_SERVER["DB_USERNAME"],
            'password' => $_SERVER["DB_PASSWORD"],
            'charset' => 'utf8',
        ),
        'dbSlave'=>array(
            'class'=>'CDbConnection',
            'charset'=>'utf8',
            'emulatePrepare' => true,
            'connectionString'=>'mysql:host='.$_SERVER["DBSLAVE_HOSTNAME"].';dbname=aba_b2c_summary',
            'username' => $_SERVER["DBSLAVE_USERNAME"],
            'password' => $_SERVER["DBSLAVE_PASSWORD"],
        ),
        'db2'=>array(
            'class'=>'CDbConnection',
            'charset'=>'utf8',
            'emulatePrepare' => true,
            'connectionString'=>'mysql:host='.$_SERVER["DB_HOSTNAME"].';dbname=db_ventasflash',
            'username' => $_SERVER["DB_USERNAME"],
            'password' => $_SERVER["DB_PASSWORD"],
        ),
        'dbLogs'=>array(
            'class'=>'CDbConnection',
            'connectionString' => 'mysql:host='.$_SERVER["DB_HOSTNAME"].';dbname=aba_b2c_logs',
            'emulatePrepare' => true,
            'username' => $_SERVER["DB_USERNAME"],
            'password' => $_SERVER["DB_PASSWORD"],
            'charset' => 'utf8',
        ),
        'dbExtranet'=>array(
            'class'=>'CDbConnection',
            'charset' => 'utf8',
            'connectionString' => 'mysql:host='.$_SERVER["DB_HOSTNAME"].';dbname=abaenglish_extranet',
            'username' => $_SERVER["DB_USERNAME"],
            'password' => $_SERVER["DB_PASSWORD"],
        ),
        'dbWebservice'=>array(
            'class'=>'CDbConnection',
            'charset' => 'utf8',
            'connectionString' => 'mysql:host='.$_SERVER["DB_HOSTNAME"].';dbname=aba_course',
            'emulatePrepare' => true,
            'username' => $_SERVER["DB_USERNAME"],
            'password' => $_SERVER["DB_PASSWORD"],
        ),
    )
);
