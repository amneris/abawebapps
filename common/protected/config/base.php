<?php
Yii::setPathOfAlias('commonNamespace', dirname(__FILE__).DIRECTORY_SEPARATOR.'..' );
Yii::setPathOfAlias('appCampus', dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'campus'.DIRECTORY_SEPARATOR.'protected' );
Yii::setPathOfAlias('Firebase', dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'common'.DIRECTORY_SEPARATOR.'protected'.DIRECTORY_SEPARATOR.'extensions'.DIRECTORY_SEPARATOR.'firebase'.DIRECTORY_SEPARATOR.'php-jwt'.DIRECTORY_SEPARATOR.'src' );
//Yii::setPathOfAlias('OpusOnline.Loggly', ROOT_DIR .DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'abaenglish'.DIRECTORY_SEPARATOR.'yii-loggly');
//Yii::setPathOfAlias('Monolog', ROOT_DIR .DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'monolog'.DIRECTORY_SEPARATOR.'monolog'.DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR.'Monolog');

    // ** AWS
require_once dirname(__FILE__).'/../../../common/protected/extensions/aws/aws-autoloader.php';

$runtimePath = ROOT_DIR . DIRECTORY_SEPARATOR . ABAWEBAPPS_APPNAME . DIRECTORY_SEPARATOR . 'runtime';


/**
 * Configuration parameters common to all entry points.
 */
return array(

    'runtimePath' => $runtimePath,

    'import' => array(
        'common.protected.models.selfgenerated.*',
        'common.protected.models.*',
        'common.protected.models.devices.*',
        'common.protected.models.logs.*',
        'common.protected.models.geo.*',
        'common.protected.models.course.*',
        'common.protected.models.payment.*',
        'common.protected.models.user.selfgenerated.*',
        'common.protected.models.user.*',
        'common.protected.models.courseTest.*',
        'common.protected.models.product.*',
        'common.protected.models.analytics.*',
        'common.protected.models.intranet.*',
        'common.protected.helpers.*',
        'common.protected.rules.*',
        'common.protected.rules.device.*',
        'common.protected.rules.payment.*',
        'common.protected.rules.paySuppliers.*',
        'common.protected.rules.product.*',
        'common.protected.rules.user.*',
        'common.protected.rules.reconciliationParser.*',
        'common.protected.components.*',
        'common.protected.extensions.*',
        'common.protected.models.reconciliationParser.*',
        'common.protected.models.feedpress.selfgenerated.*',
        'common.protected.models.feedpress.*',
        'common.protected.models.extranet.selfgenerated.*',
        'common.protected.models.extranet.*',
        'common.protected.models.experiments.*',
        'application.models.leveltest._gii.*',

        // The following two imports are polymorphic and will resolve against wherever the `basePath` is pointing to.
        // We have components, models, helpers in all entry points anyway
        'application.components.*',
        'application.models.*',
        'application.helpers.*',
        'application.rules.*',
    ),
    'components'=>array(
        'clientScript' => array(
          'packages' => array(
            'jquery' => array(
              'baseUrl' => '//ajax.googleapis.com/ajax/libs/jquery/1.8.3/',
              'js' => array('jquery.min.js'),
            ),
            'jquery.ui' => array(
              'baseUrl' => '//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/',
              'js' => array('jquery-ui.min.js'),
              'depends' => array('jquery'),
            ),
            'bootstrap' => array(
              'baseUrl' => '//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/',
              'css' => array('bootstrap.min.css'),
            ),
            'font-awesome' => array(
              'baseUrl' => '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css',
              'css' => array('font-awesome.min.css'),
            ),
            'jquery.fancybox' => array(
              'baseUrl' => '//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5',
              'css' => array('jquery.fancybox.min.css'),
              'js' => array('jquery.fancybox.pack.js'),
            ),
          ),
        ),
        'assetManager' => array(
            'class' => 'AbaCAssetManager',
        ),
        /**
         * Bug #5429 by @ascandroli
         *
         * The 'validationKey' is a passphrase used as the basis of hash-based message authentication (HMAC) for any
         * object stream data stored on the client.
         *
         * The default implementation of CSecurityManager generates a validation key based on a random number.
         * This key is used to hash the user identification & state cookie, see CHttpRequest's enableCookieValidation
         * and enableCsrfValidation.
         * Identifying users (using autoLogin) and validating cookies across any of the nodes of the cluster won't work
         * because the hashed cookie won't validate on any installation other than the one that generated it!
         *
         * You should configure this to a reasonable value (longer is better) and ensure that all servers in the cluster
         * share the same value.
         *
         */
        'securityManager' => array(
            'validationKey' => 'tcvLWEbgtawfTvyY',
        ),
        'request' => array(
            'class' => 'AbaCHttpRequest',
            'enableCookieValidation' => true,
            'enableCsrfValidation' => false,
        ),
        'mailer' => array(
            'class' => 'common.protected.extensions.mailer.EMailer',
            'pathViews' => 'application.views.email',
            'pathLayouts' => 'application.views.email.layouts'
        ),
        'geoip' => array(
            'class' => 'common.protected.extensions.geoip.CGeoIP',
            // specify filename location for the corresponding database
            'filename' => ROOT_DIR.'/common/protected/extensions/geoip/GeoLiteCity.dat',
            // Choose MEMORY_CACHE or STANDARD mode
            'mode' => 'STANDARD',
        ),
        'config' => array(
            'class' => 'common.protected.extensions.econfig.EConfig'
        ),
        'cache' => array(
            'class' => 'system.caching.CDummyCache',
        ),
       'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class'=>'common.protected.extensions.syslog.ESysLogRoute', //'CFileLogRoute',
                    'levels'=>'trace, warning, error, info, profile',
                    'categories'=>'system.*,itbugs,payments,payments_mobile,selligent,finance,support,reconciliation,user_activity,ibugs',
                    'enabled' => true,
                    'levels' => 'error, warning, info',
                    'logFile' => ABAWEBAPPS_APPNAME . '.log',
                    'logPath' => (getenv('ABAWEBAPPS_LOG_PATH')) ? getenv('ABAWEBAPPS_LOG_PATH') : $runtimePath,
                    'maxFileSize' => 100 * 1024,
                    'maxLogFiles' => 30,
                    'except' => 'events,sql.*',
                ),
                array(
                    'class' => 'CFileLogRoute',
                    'enabled' => true,
                    'levels' => 'error, warning, info',
                    'logFile' => ABAWEBAPPS_APPNAME . '-sql.log',
                    'logPath' => (getenv('ABAWEBAPPS_LOG_PATH')) ? getenv('ABAWEBAPPS_LOG_PATH') : $runtimePath,
                    'maxFileSize' => 100 * 1024,
                    'maxLogFiles' => 30,
                    'categories' => 'sql.*',
                ),
              array(
                'class' => 'CFileLogRoute',
                'enabled' => true,
                'levels' => 'error, warning, info',
                'logFile' => 'events.log',
                'logPath' => (getenv('ABAWEBAPPS_LOG_PATH')) ? getenv('ABAWEBAPPS_LOG_PATH') : $runtimePath,
                'maxFileSize' => 100 * 1024,
                'maxLogFiles' => 50,
                'categories' => 'events',
              ),
              array(
                    'class' => 'CWebLogRoute',
                    'enabled' => false,
                    'levels' => 'error, warning, trace, info',
                    'showInFireBug' => true,
                ),
            ),
        ),
    ),
    'params'=>array(
      'dbCampus'=>'aba_b2c',
      'ext_MailHelpFrom' => 'corporate@abaenglish.com',
      'isNotProduction' => (getenv('ABAWEBAPPS_ENV') != 'production'),
		  'INTRANET_URL'        => 'https://intranet.abaenglish.com',

      'ZENDESK_DOMAIN' => 'abaenglish.zendesk.com',
      'ZENDESK_URLLOCALES' => 'https://abaenglish.zendesk.com/api/v2/locales/public.json',
      'ZENDESK_DEFAULTLANGUAGE' => 'en',
      'ZENDESK_PRIVATEKEY' => 'h0uoW73tDmXUNyR2kJ1pLNeFGIGdmlq140m98OaoHC800S4M',

      'logFacility' => (defined('LOG_LOCAL7') ? LOG_LOCAL7 : 8),
      'logGroup' => "common",
      'logName' => "[ABAWEBAPPS]",
    ),
);
