<?php
/**
 * This helper works as a table of a database. Maybe in the future can be replaced by a query in the model Units.
 * this Helper SHOULD ONLY BE called FROM THE MODEL! never from controllers or commands.
 */
class HeUnits
{
    public static function getLevelByUnit($unit)
    {
        $unit=intval($unit);
        switch ($unit)
        {
            case ($unit<=24):
                $level=1;
                break;
            case ($unit<=48):
                $level=2;
                break;
            case ($unit<=72):
                $level=3;
                break;
            case ($unit<=96):
                $level=4;
                break;
            case ($unit<=120):
                $level=5;
                break;
            case ($unit<=144):
                $level=6;
                break;
            default:
                $level=0;
        }

        return $level;
    }
    private static $unitNames = array("1" => "A Day at the Beach",
	"2" => "A New Friend",
	"3" => "A Chat on the Subway",
	"4" => "At Breakfast",
	"5" => "The Hotel Doorman",
	"6" => "The Party",
	"7" => "The Medical Conference",
	"8" => "Mr Memory",
	"9" => "The Delay",
	"10" => "The Question",
	"11" => "Susan's Baby",
	"12" => "The Depressed Workmate",
	"13" => "A Funny Chat at the Pub",
	"14" => "Five Hours a Week",
	"15" => "The Jealous Friend",
	"16" => "The Grumpy Grandpa",
	"17" => "Tom and Mary are Different",
	"18" => "An Unnecessary Question",
	"19" => "A Call from Australia",
	"20" => "A Spectacular Couple",
	"21" => "Happy Birthday",
	"22" => "David's Two Mums",
	"23" => "A Neat Diary",
	"24" => "The Smoke",
	"25" => "A Good Combination",
	"26" => "The Watch",
	"27" => "The Dessert",
	"28" => "A Chat at the Greengrocer's",
	"29" => "The Sales",
	"30" => "The Mountain Bikes",
	"31" => "The Twins",
	"32" => "The Christmas Tree",
	"33" => "The Househusband",
	"34" => "Boss or Lover?",
	"35" => "Allan's Girlfriends",
	"36" => "The Good Life",
	"37" => "Mum's on Strike",
	"38" => "Ben's Birthday",
	"39" => "Vacation in Bahia",
	"40" => "The Efficient Secretary",
	"41" => "Video and Spaghetti",
	"42" => "A Big Favour",
	"43" => "A Wonderful Waiter",
	"44" => "Wedding in Bali",
	"45" => "Tom's Suggestions",
	"46" => "The Birthday Present",
	"47" => "Living Together",
	"48" => "The Weekend",
	"49" => "A Terrific Teacher",
	"50" => "A Brilliant Idea",
	"51" => "Two Secretaries",
	"52" => "The Boss's Mother",
	"53" => "Fashion Café",
	"54" => "A Small Argument",
	"55" => "The Football Match",
	"56" => "The Ghost",
	"57" => "Incompatible Foods",
	"58" => "The Power is Within You",
	"59" => "Linda's worries",
	"60" => "Sam's Intuition",
	"61" => "A New Image",
	"62" => "Too Late",
	"63" => "A Misunderstanding",
	"64" => "The Punishment from the Gods",
	"65" => "Carol's New Book",
	"66" => "The Mystery",
	"67" => "Singing at School",
	"68" => "The Solution",
	"69" => "Kenya",
	"70" => "Memories",
	"71" => "The Stressed Husband",
	"72" => "An Incredible Arrangement",
	"73" => "The Boss In Love",
	"74" => "Driving at Night",
	"75" => "Albert the Robot",
	"76" => "The Full Restaurant",
	"77" => "British Pride",
	"78" => "Two Friends",
	"79" => "Dinner and Dominoes",
	"80" => "Our Dear Edward",
	"81" => "Appreciate What You Have",
	"82" => "Boys",
	"83" => "The Hospital",
	"84" => "A Seductive Woman",
	"85" => "A Village in Africa",
	"86" => "George is Absent-minded",
	"87" => "Good Advice",
	"88" => "The Debt",
	"89" => "Matthew's Cars",
	"90" => "The Last Chance",
	"91" => "The Blonde Dream",
	"92" => "Alexis's Men",
	"93" => "Breakfast in Bed",
	"94" => "Hannah",
	"95" => "A Novel Affair",
	"96" => "Dancing in the Rain",
	"97" => "The Invitation",
	"98" => "The Weekly Schedule",
	"99" => "Sundays",
	"100" => "Prince Charming",
	"101" => "The Vanity Case",
	"102" => "The Beach Bar",
	"103" => "The Hairdresser's",
	"104" => "29th February",
	"105" => "Irene's Story",
	"106" => "Listening to Music",
	"107" => "The Lady in Red",
	"108" => "Supper at Samuel's House",
	"109" => "The Journalist and the Architect",
	"110" => "The Zodiac",
	"111" => "Sunshine's Wigs",
	"112" => "Emotions",
	"113" => "Multi-screen Movie Theaters",
	"114" => "Tobacco",
	"115" => "Moving In",
	"116" => "Mr Doer",
	"117" => "Vacation",
	"118" => "Flat Swap",
	"119" => "Frank's Doubts",
	"120" => "Mexico City",
	"121" => "Newspapers",
	"122" => "An Exciting Future",
	"123" => "Anne and Languages",
	"124" => "The Opening",
	"125" => "Noel in his Sixties",
	"126" => "Jenny the Baby-sitter",
	"127" => "Little Dreams",
	"128" => "Melatonin",
	"129" => "The Midnight Express",
	"130" => "Complete Success",
	"131" => "The Mayor of New Rock",
	"132" => "Grosvenor House",
	"133" => "Encyclopedias for children",
	"134" => "The Advert",
	"135" => "Big Lemon",
	"136" => "Dominique",
	"137" => "Algra – Alistar & Graham",
	"138" => "How to Speak in Public",
	"139" => "Theresa's Shares",
	"140" => "My Husband's Ex",
	"141" => "Sailing along the Aegean",
	"142" => "The Budget",
	"143" => "Tramonti",
	"144" => "The European Union");

    private static $videoClassesTeachers= array( 1 => 'Fabiola',
        2 => 'Sophie',
        3 => 'Cristina',
        4 => 'Ben',
        5 => 'Carlos',
        6 => 'Fabiola',
        7 => 'Julia',
        8 => 'David',
        9 => 'Mike',
        10 => 'Sophie',
        11 => 'Cristina',
        12 => 'Lisa',
        13 => 'Carlos',
        14 => 'Fabiola',
        15 => 'Julia',
        16 => 'Sarah',
        17 => 'Mike',
        18 => 'Sophie',
        19 => 'Cristina',
        20 => 'Lisa',
        21 => 'Carlos',
        22 => 'Fabiola',
        23 => 'Julia',
        24 => 'David',
        25 => 'Mike',
        26 => 'Sophie',
        27 => 'Cristina',
        28 => 'Lisa',
        29 => 'Carlos',
        30 => 'Fabiola',
        31 => 'Julia',
        32 => 'David',
        33 => 'Cristina',
        34 => 'Fabiola',
        35 => 'Cristina',
        36 => 'Lisa',
        37 => 'Carlos',
        38 => 'Fabiola',
        39 => 'Julia',
        40 => 'David',
        41 => 'Lisa',
        42 => 'Sophie',
        43 => 'Cristina',
        44 => 'Ben',
        45 => 'Sarah',
        46 => 'Ben',
        47 => 'Cristina',
        48 => 'Fabiola',
        49 => 'David',
        50 => 'Sarah',
        51 => 'Cristina',
        52 => 'Lisa',
        53 => 'Ben',
        54 => 'Ben',
        55 => 'Cristina',
        56 => 'Fabiola',
        57 => 'Ella',
        58 => 'Sarah',
        59 => 'Cristina',
        60 => 'Sarah',
        61 => 'Ella',
        62 => 'Ben',
        63 => 'Cristina',
        64 => 'Fabiola',
        65 => 'David',
        66 => 'Sarah',
        67 => 'Ben',
        68 => 'Ella',
        69 => 'Ben',
        70 => 'Ben',
        71 => 'Cristina',
        72 => 'Fabiola',
        73 => 'David',
        74 => 'Sarah',
        75 => 'Cristina',
        76 => 'Lisa',
        77 => 'Ben',
        78 => 'Ben',
        79 => 'Cristina',
        80 => 'Fabiola',
        81 => 'Graham',
        82 => 'Ben',
        83 => 'Sarah',
        84 => 'Ella',
        85 => 'Fabiola',
        86 => 'Ella',
        87 => 'Fabiola',
        88 => 'Ben',
        89 => 'Ben',
        90 => 'Ella',
        91 => 'Graham',
        92 => 'Graham',
        93 => 'Fabiola',
        94 => 'Sarah',
        95 => 'Fabiola',
        96 => 'Ella',
        97 => 'Ben',
        98 => 'Fabiola',
        99 => 'Cristina',
        100 => 'Ben',
        101 => 'Fabiola',
        102 => 'Ella',
        103 => 'Graham',
        104 => 'Ella',
        105 => 'Fabiola',
        106 => 'Ben',
        107 => 'Graham',
        108 => 'Ben',
        109 => 'Fabiola',
        110 => 'Graham',
        111 => 'Graham',
        112 => 'Graham',
        113 => 'Fabiola',
        114 => 'Fabiola',
        115 => 'Graham',
        116 => 'Ben',
        117 => 'Fabiola',
        118 => 'Graham',
        119 => 'Graham',
        120 => 'Graham',
        121 => 'Fabiola',
        122 => 'Fabiola',
        123 => 'Fabiola',
        124 => 'Fabiola',
        125 => 'Fabiola',
        126 => 'Cristina',
        127 => 'Lisa',
        128 => 'Ben',
        129 => 'Fabiola',
        130 => 'Fabiola',
        131 => 'Ella',
        132 => 'Fabiola',
        133 => 'Graham',
        134 => 'Ella',
        135 => 'Ben',
        136 => 'Ella',
        137 => 'Fabiola',
        138 => 'Ella',
        139 => 'Ben',
        140 => 'Graham',
        141 => 'Cristina',
        142 => 'Ben',
        143 => 'Cristina',
        144 => 'Sarah',
    );

    private static $teachersCountries = array(
        "Julia" => "SCO",
        "Mike" => "CAN",
        "Lisa" => "IR",
        "Graham" => "UK",
        "Fabiola" => "MEX",
        "Ella" => "FIL",
        "David" => "SA",
        "Cristina" => "USA",
        "Carlos" => "UK",
        "Sarah" => "USA",
        "Sophie" => "UK",
        "Ben" => "USA"
    );

    private static $sectionTitles = array("1" => 'ABA Film',
                                          "2" => 'Habla',
                                          "3" => 'Escribe',
                                          "4" => 'Interpreta',
                                          "5" => 'Videoclase',
                                          "6" => 'Ejercicios',
                                          "7" => 'Vocabulario',
                                          "8" => 'Evaluación');

    private static $oldSectionTitles = array("1" => 'ABAFILM',
                                             "2" => 'STUDY',
                                             "3" => 'DICTATION',
                                             "4" => 'ROLEPLAY',
                                             "5" => 'VIDEOCLASS',
                                             "6" => 'WRITING',
                                             "7" => 'NEWWORDS',
                                             "8" => 'MINITEST');

    private static $followUpColumnNames = array("1" => 'sit_por',
                                                  "2" => 'stu_por',
                                                  "3" => 'dic_por',
                                                  "4" => 'rol_por',
                                                  "5" => 'gra_vid',
                                                  "6" => 'wri_por',
                                                  "7" => 'new_por',
                                                  "8" => 'eva_por');

    private static $enSectionTitles = array("1" => 'ABA Film',
        "2" => 'Speak',
        "3" => 'Write',
        "4" => 'Interpreta',
        "5" => 'Videoclase',
        "6" => 'Ejercicios',
        "7" => 'Vocabulario',
        "8" => 'Evaluación');

    /**
     * @static
     *
     * @param $key
     *
     * @return mixed
     */
    public static function getUnitNameByIdUnit($key)
    {
        //$name = UnitNames;
        return self::$unitNames[$key];
    }

    /**
     * @static
     * @return array
     */
    public static function getUnitNameAll(){
        return self::$unitNames;
    }

    /**
     * @static
     *
     * @param $level
     *
     * @return bool|array|integer
     */
    public static function getUnitNameByLevel($level)
    {
        if(array_key_exists($level, Yii::app()->params['levels'])){
            $arrayLevel = array_chunk(self::$unitNames, UNITS_PER_LEVEL, true);
            return $arrayLevel[$level-1];
        }
        else{
            //die('el nivel no existe');
            return false;
        }
    }

    /**
     * @static
     *
     * @param $key
     *
     * @return string
     */
    public static function getVideoClassTitleByUnit($key)
    {
        return Yii::t('mainApp', $key);
    }

    /**
     * Returns the bottom bound of the Level units
     * @static
     *
     * @param $level
     *
     * @return int
     */
    public static function getStartUnitByLevel($level)
    {
        $offset = (UNITS_PER_LEVEL * ($level-1) );
        $start = ( $offset + 1 );
        return intval( $start );
    }


    /** Returns an array containing the first unit of every level.
     * @static
     *
     * @param int $lowerBoundLevel
     * @param int $topBoundLevel
     *
     * @return array|bool Should be 1, 25, 49, 54, etc.
     */
    public static function getArrayStartUnitsByLevel($lowerBoundLevel = 1, $topBoundLevel = 6)
    {
        if ($topBoundLevel < $lowerBoundLevel) {
            return false;
        }

        $aStartUnits = array();
        for ($i = ($lowerBoundLevel ); $i <= $topBoundLevel; $i++) {
            $aStartUnits[$i] = HeUnits::getStartUnitByLevel($i);
        }

        return $aStartUnits;
    }

    /**
     * Returns the upper bound of the Level units
     * @static
     *
     * @param $level
     *
     * @return int
     */
    public static function getEndUnitByLevel($level)
    {
        $offset = (UNITS_PER_LEVEL * ($level-1) );
        $end = ($offset + UNITS_PER_LEVEL);
        return intval( $end );
    }

    /**
     * Returns the id of the unit at position X depending on the level (ex: 2nd unit of the current level)
     * @param $level
     * @param $position
     * @return int
     */
    public static function getUnitAtPos($level, $position)
    {
        $offset = (UNITS_PER_LEVEL * ($level-1) );
        $end = ($offset + $position);
        return intval( $end );
    }

    /**
     * * Returns the teacher Name. Later on we should maybe return the Teacher Id and access database.
     * @static
     *
     * @param $key
     *
     * @return string
     */
    public static function getTeacherVideoByUnit($key)
    {

        return self::$videoClassesTeachers[$key];
    }

    /** Gets the total amount of units available, should be always 144
     * @static
     * @return int
     */
    public static function getMaxUnits()
    {
        return count(self::$unitNames);
    }

    /**
     * Returns if the $id is the first unit of any level
     * @static
     *
     * @param $id
     *
     * @return bool
     */
    public static function isFirstIdLevel( $id )
    {
        foreach(array_keys(Yii::app()->params['levels']) as $level)
        {
            if(intval($id)==self::getStartUnitByLevel(intval($level)))
            {
                return true;
            }
        }
        return false;
    }

    /** Returns the keyword title of one section out of 8.
     * @static
     *
     * @param $id
     *
     * @return string
     */
    public static function getSectionTitle( $id )
    {
        return self::$sectionTitles[''.$id];
    }

    /**
     * Translates and returns the original title for Course Sections.
     * (Except for ABA Film and Video Class which did not exist in their current form).
     * @param integer $sectionTypeId
     * @return string old section title
     */
    public static function getOldSectionTitle($sectionTypeId){
        return self::$oldSectionTitles[''.$sectionTypeId];
    }

    /**
     * Translates and returns section type id to followup4 column names.
     * @param integer $sectionTypeId
     * @return string followup4 column name
     */
    public static function getFollowUpColumn($sectionTypeId){
        return self::$followUpColumnNames[''.$sectionTypeId];
    }

    /**
     * Returns the section type id for given (original/old) section name
     *
     * @param string $oldSectionName
     * @return int sectionTypeId
     */
    public static function getSectionTypeIdByOldSectionName($oldSectionName)
    {
        $sectionTypeID = 0;
        switch ($oldSectionName) {
            case 'STUDY':
                $sectionTypeID = 2;
                break;
            case 'DICTATION':
                $sectionTypeID = 3;
                break;
            case 'ROLEPLAY':
                $sectionTypeID = 4;
                break;
            case 'WRITING':
                $sectionTypeID = 6;
                break;
            case 'NEWWORDS':
                $sectionTypeID = 7;
                break;
            case 'MINITEST':
                $sectionTypeID = 8;
            default;
                break;
        }
        return $sectionTypeID;
    }

    public static function getSectionTypeIdByNewSectionName($sectionName)
    {
        $sectionTypeID = 0;
        switch ($sectionName) {
            case 'ABAFILM':
                $sectionTypeID = 1;
                break;
            case 'SPEAK':
                $sectionTypeID = 2;
                break;
            case 'WRITE':
                $sectionTypeID = 3;
                break;
            case 'INTERPRET':
                $sectionTypeID = 4;
                break;
            case 'VIDEOCLASS':
                $sectionTypeID = 5;
                break;
            case 'EXERCISES':
                $sectionTypeID = 6;
                break;
            case 'VOCABULARY':
                $sectionTypeID = 7;
                break;
            case 'ASSESSMENT':
                $sectionTypeID = 8;
            default;
                break;
        }
        return $sectionTypeID;
    }

    /**
     * * Returns the Countries.
     * @static
     */
    public static function getTeachersCountries()
    {
        return self::$teachersCountries;
    }
}

