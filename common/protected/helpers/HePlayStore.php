<?php

/**
 * Created by PhpStorm.
 * User: jmerchanj
 * Date: 19/11/15
 * Time: 15:09
 */
class HePlayStore
{

    private $GooglePlayAPIP12 = null;
    private $MobileAplicationName = 'abaenglish-videoclass';
    private $packageName = 'com.abaenglish.videoclass';
    private $ServiceAccountEmailAddress = 'account-1@abaenglish-videoclass.iam.gserviceaccount.com';
    private $subscription="";

    private $autoRenewing;
    private $expiryTimeMillis;
    private $kind;
    private $startTimeMillis;

    public static $service=null;

    /**
     * @return string
     */
    public function getPackageName()
    {
        return $this->packageName;
    }

    /**
     * @param string $packageName
     */
    public function setPackageName($packageName)
    {
        $this->packageName = $packageName;
    }


    /**
     * @return mixed
     */
    public function getAutoRenewing()
    {
        return $this->autoRenewing;
    }

    /**
     * @return mixed
     */
    public function getExpiryTimeMillis()
    {
        return $this->expiryTimeMillis;
    }

    /**
     * @return mixed
     */
    public function getKind()
    {
        return $this->kind;
    }

    /**
     * @return mixed
     */
    public function getStartTimeMillis()
    {
        return $this->startTimeMillis;
    }


    /**
     * @return string
     */
    public function getSubscription()
    {
        return $this->subscription;
    }


    public function __construct()
    {

        require_once Yii::app()->basePath . '/../../vendor/autoload.php';
        if (!isset(static::$service)){
            $GooglePlayAPIP12_BASE64 = Yii::app()->config->get("PAY_ANDROID_PRIVATE_KEY");
            $this->GooglePlayAPIP12= base64_decode($GooglePlayAPIP12_BASE64);

            $client = new Google_Client();

            // set Application Name to the name of the mobile app
            $client->setApplicationName($this->MobileAplicationName);
            $key = $this->GooglePlayAPIP12;
            $cred = new Google_Auth_AssertionCredentials(
              "$this->ServiceAccountEmailAddress",
              ['https://www.googleapis.com/auth/androidpublisher'],
              $key
            );
            $client->setAssertionCredentials($cred);

            static::$service = new Google_Service_AndroidPublisher($client);
        }
    }

    /**
     * @return null
     */
    public function getMobileAplicationName()
    {
        return $this->MobileAplicationName;
    }

    /**
     * @param null $MobileAplicationName
     */
    public function setMobileAplicationName($MobileAplicationName)
    {
        $this->MobileAplicationName = $MobileAplicationName;
    }

    /**
     * @return null|string
     */
    public function getServiceAccountEmailAddress()
    {
        return $this->ServiceAccountEmailAddress;
    }

    /**
     * @param null|string $ServiceAccountEmailAddress
     */
    public function setServiceAccountEmailAddress($ServiceAccountEmailAddress)
    {
        $this->ServiceAccountEmailAddress = $ServiceAccountEmailAddress;
    }

    Public function Subscription($subscriptionId = "", $purchaseToken = "")
    {

        $subscription = false;

        try {

            // use the purchase token to make a call to Google to get the subscription info
            $subscription = static::$service->purchases_subscriptions->get($this->packageName, $subscriptionId,
              $purchaseToken);
            //$purchases = $service->purchases_products->get($this->packageName, $subscriptionId, $purchaseToken);

            if (is_null($subscription)) {
                // query returned no data
                throw new Exception('Error no response from subscription.', 500);
            } elseif (isset($subscription['error']['code'])) {
                // query returned an error
                throw new Exception($subscription, 500);
            } elseif (!isset($subscription['expiryTimeMillis'])) {
                // query did not return a subscription expiration time
                throw new Exception($subscription, 500);
            }

            $this->autoRenewing = $subscription['autoRenewing'];
            $this->expiryTimeMillis = $subscription['expiryTimeMillis'];
            $this->kind = $subscription['kind'];
            $this->startTimeMillis = $subscription['startTimeMillis'];

            $this->subscription = $subscription;

            return true;

        } catch (Exception $problem) {
            // if the call to Google fails, throw an exception
            //throw new Exception('Error validating transaction', 500);
            $this->subscription = $subscription;
            return false;
        }
    }

    Public function Revoke($subscriptionId="",$purchaseToken=""){

        try {

            // use the purchase token to make a call to Google to get the subscription info
            $subscription = static::$service->purchases_subscriptions->revoke($this->packageName, $subscriptionId, $purchaseToken);

            if (!empty($subscription)) {
                // query returned no data
                return $subscription;
            }

            return "";

        } catch (Exception $problem) {
            // if the call to Google fails, throw an exception
            //throw new Exception('Error revoking transaction: ' . $problem.message  , 500);
            return "ERROR: ". $problem.message;
        }

    }

    Public function Refund($subscriptionId="",$purchaseToken=""){


        try {

            // use the purchase token to make a call to Google to get the subscription info
            $subscription = static::$service->purchases_subscriptions->refund($this->packageName, $subscriptionId, $purchaseToken);

            if (!empty($subscription)) {
                // query returned no data
                return $subscription;
            }

            return "";

        } catch (Exception $problem) {
            // if the call to Google fails, throw an exception
            //throw new Exception('Error refunding transaction: ' . $problem.message  , 500);
            return "ERROR: ". $problem.message;
        }

    }

    Public function Cancel($subscriptionId="",$purchaseToken=""){


        try {

            // use the purchase token to make a call to Google to get the subscription info
            $subscription = static::$service->purchases_subscriptions->cancel($this->packageName, $subscriptionId, $purchaseToken);

            if (!empty($subscription)) {
                // query returned no data
                return $subscription;
            }

            return "";

        } catch (Exception $problem) {
            // if the call to Google fails, throw an exception
            //throw new Exception('Error refunding transaction: ' . $problem.message  , 500);
            return "ERROR: ". $problem.message;
        }

    }

}