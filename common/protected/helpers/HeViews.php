<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 *
 * Date: 5/10/12
 * Time: 16:44
 * To change this template use File | Settings | File Templates.
 */
class HeViews
{

    /** Returns product selected
     * @static
     *
     * @param array $products
     * @param       $packageSelected
     *
     * @return mixed
     */
    public static function getSelectedProduct( array $products, $packageSelected )
    {
        foreach ($products as $key => $productPrice)
        {
            if($packageSelected==$productPrice["productPricePK"])
            {
                 return $productPrice;
            }
            $lastProd = $productPrice;
        }
        return $lastProd;
    }


    /** Formats the amount with decimal positions based on regional settings and also changes positions
     * to the currency symbol. Returns a string to be displayed.
     *
     * @static
     *
     * @param $amount
     * @param $idCurrency
     * @param $symbolCurrency
     *
     * @return string
     */
    public static function formatAmountInCurrency($amount, $idCurrency, $symbolCurrency = '', $bWithoutSymbol = false)
    {
        $strAmount = strval($amount);
        $moAbaCurr = new CmCurrencies();
        if (!$moAbaCurr->getCurrencyById($idCurrency)) {
            // EURO
            if ($symbolCurrency == '€') {
                $strAmount = str_replace(".", ",", $amount) . " " . $symbolCurrency;
            } // USD $
            else {
                $strAmount = $symbolCurrency . $amount;
            }
            return $strAmount;
        }

        if ($moAbaCurr->symbol !== $symbolCurrency && $symbolCurrency !== '') {
            $symbolCurrency = $moAbaCurr->symbol . "***";
        }

        if (strpos($amount, '.') !== false) {
            $strAmount = str_replace('.', $moAbaCurr->decimalPoint, strval($amount));
        }

        if (!$bWithoutSymbol) {
            if ($symbolCurrency == '') {
                $symbolCurrency = $moAbaCurr->symbol;
            }
        }

        switch ($idCurrency) {
            case 'CAD':
                $strAmount = $symbolCurrency . $strAmount . $idCurrency;
                break;
            case 'USD':
            case 'BRL':
                $strAmount = $symbolCurrency . $strAmount;
                break;
            case 'EUR':
            case 'MXN':
            default:
                $strAmount = $strAmount . " " . $symbolCurrency;
                break;
        }

        return $strAmount;
    }

    /**
     * @param $idCurrency
     * @param string $symbolCurrency
     * @param bool|false $bWithoutSymbol
     *
     * @return string
     */
    public static function formatWithoutAmountInCurrency($idCurrency, $symbolCurrency = '', $bWithoutSymbol = false)
    {
        $strAmount = "";

        $moAbaCurr = new CmCurrencies();
        if (!$moAbaCurr->getCurrencyById($idCurrency)) {
            return $symbolCurrency;
        }

        if ($moAbaCurr->symbol !== $symbolCurrency && $symbolCurrency !== '') {
            $symbolCurrency = $moAbaCurr->symbol . "***";
        }

        if (!$bWithoutSymbol) {
            if ($symbolCurrency == '') {
                $symbolCurrency = $moAbaCurr->symbol;
            }
        }

        switch ($idCurrency) {
            case 'CAD':
                $strAmount = $idCurrency . " " . $symbolCurrency;
                break;
            case 'USD':
            case 'BRL':
                $strAmount = $symbolCurrency;
                break;
            case 'EUR':
            case 'MXN':
            default:
                $strAmount = $symbolCurrency;
                break;
        }

        return trim($strAmount);
    }

    public static function applyStylesInAmount($price, $productPrice, $cssForCurrency = '', $cssForAmountInt = '', $cssForAmountCents = '')
    {
        $price = HeViews::formatAmountInCurrency(number_format($price, 2), $productPrice['ccyIsoCode'], $productPrice["ccyDisplaySymbol"]);

        $pos = max(strrpos($price, ','), strrpos($price, '.'));
        if($pos > 0) {
            $priceInt = substr($price, 0, $pos);
            $priceDecimals = substr($price, $pos, strlen($price));
        }
        else {
            $priceInt = $price;
            $priceDecimals = NULL;
        }

        if ($productPrice['ccyIsoCode'] === 'USD' or $productPrice['ccyIsoCode'] === 'BRL') {
            $aux = explode($productPrice["ccyDisplaySymbol"], $priceInt);
            $priceInt = '<span class="'.$cssForCurrency.'">'.$productPrice["ccyDisplaySymbol"].' </span>'.$aux[1];
        }

        $priceStyled = array(
            '<span class="'.$cssForAmountInt.'">'.$priceInt.'</span>',
            '<span class="'.$cssForAmountCents.'">'.$priceDecimals.'</span>',
        );

        return $priceStyled;
    }

    /**
     * @return array
     */
    public static function getListOfCards()
    {
        return array(
            KIND_CC_VISA => '</br><div class=iconVisa></div>',
            KIND_CC_VISA_ELECTRON => '</br><div class=iconVisaElectron></div>',
            KIND_CC_MASTERCARD => '</br><div class=iconMasterCard></div>',
            KIND_CC_MAESTRO => '</br><div class=iconMaestro></div>',
            KIND_CC_AMERICAN_EXPRESS => '</br><div class=iconAmEx></div>',
            KIND_CC_DINERS_CLUB => '</br><div class=iconDiners></div>',
            KIND_CC_JCB => '</br><div class=iconJCB></div>',
            KIND_CC_ELO => '</br><div class=iconelo></div>',
            KIND_CC_DISCOVER => '</br><div class=iconDiscover></div>',
            KIND_CC_PAYPAL => '<div class=iconPaypal></div>',
            KIND_CC_BOLETO => '<div class=iconBoleto></div>',
            KIND_CC_NO_CARD => '<div class=iconNocard></div>',
            KIND_CC_APPSTORE => '<div class=iconAppstore></div>',
            KIND_CC_UATP => '<div class=iconAdyenuatp></div>',
            KIND_CC_CUP => '<div class=iconAdyencup></div>',
            KIND_CC_VIAS => '<div class=iconAdyenvias></div>'
        );
    }

    /** Returns the visa card number with **** partially or fully disguised.
     *
     * @param string $creditCardNumber
     * @param bool $fullSecret
     * @return string
     */
    public static function formatCardNumberSecret($creditCardNumber, $fullSecret=true)
    {
        if($fullSecret){
            return str_repeat('*', strlen($creditCardNumber->cardNumber));
        } else {
            return '**** **** **** '.substr($creditCardNumber, 12, 15);
        }
    }
}
