<?php

class HeAmazon
{
    const _AWS_REGION = 'eu-west-1';
    const _AWS_VERSION = 'latest';
//    const _AWS_BUCKET = 'abadevimages';

    public static function newClient()
    {
        return new Aws\S3\S3Client([
          'version' => self::_AWS_VERSION,
          'region'  => self::_AWS_REGION,
//          'debug'   => ( YII_DEBUG ) ? true : false
        ]);
    }

    public static function listBuckets()
    {
//        $s3Client = self::newClient();
//
//        try {
//            $arBuckets = $s3Client->listBuckets();
//        } catch (Exception $e) {
//            $arBuckets = array();
//        }
//
//        return $arBuckets;
    }

    public static function uploadFile($file, $fileTgt, $bucket = '')
    {
        if (empty($bucket)) {
            $bucket = getenv('AWS_BUCKET');
        }

//        $mimeType = mime_content_type($file);
//        , array( 'params' => array('ContentType'  => $mimeType))

        $fdFile = fopen($file, 'r');

        $s3Client = self::newClient();
        $s3Client->upload($bucket, $fileTgt, $fdFile, 'public-read' );
        $urlFile = $s3Client->getObjectUrl($bucket, $fileTgt);

        return $urlFile;
    }

    public static function deleteFile($filename, $bucket = '') {
        if (empty($bucket)) {
            $bucket = getenv('AWS_BUCKET');
        }

        $s3Client = self::newClient();

        $result = $s3Client->deleteObject(array(
            // Bucket is required
          'Bucket' => $bucket,
            // Key is required
          'Key' => $filename,
//          'MFA' => 'string',
//          'VersionId' => 'string',
//          'RequestPayer' => 'string',
        ));
    }

    public static function queueFile2Delete($filename) {
        $filename = basename($filename);

        $mQueue = new QueueAmazonDeleteFiles();
        $mQueue->filename = $filename;
        $mQueue->save();
    }
}
