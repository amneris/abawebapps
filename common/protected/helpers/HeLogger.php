<?php

class HeLogger
{
    const IT_BUGS = "itbugs";
    const PAYMENTS = "payments";
    const PAYMENTS_MOBILE = "payments_mobile";
    const SELLIGENT = "selligent";
    const FINANCE = "finance";
    const SUPPORT = "support";
    const RECONCILIATION = "reconciliation";
    const USERACTIVITY = "user_activity";
    const IT_BUGS_PAYMENTS = "ibugs";

    const PREFIX_ERR_UNKNOWN_L = "CAMPUS ERROR UNKNOWN(LOW)";
    const PREFIX_ERR_UNKNOWN_H = "CAMPUS ERROR UNKNOWN(HIGH)";
    const PREFIX_ERR_LOGIC_H = "CAMPUS INCONSISTENCY (HIGH)";
    const PREFIX_ERR_LOGIC_L = "CAMPUS INCONSISTENCY (LOW)";
    const PREFIX_NOTIF_L = "CAMPUS NOTIFICATION (LOW)";
    const PREFIX_NOTIF_H = "CAMPUS NOTIFICATION (HIGH)";
    const PREFIX_TESTS = "CAMPUS LOCAL TESTS SELENIUM (LOW)";
    const PREFIX_APP_LOGIC_L = "APP INCONSISTENCY (LOW)";

    const DEBUG = 'trace';     // info      // 100      // NO SE GENERA LA LINEA DE SYSLOG
    const INFO = 'info';       // info      // 200
    const NOTICE = 'profile';  // info      // 250      // NO SE GENERA LA LINEA DE SYSLOG
    const WARNING = 'warning'; // warning   // 300
    const ERROR = 'error';     // error     // 400
    const CRITICAL = 'error';  // error     // 500
    const ALERT = 'info';      // info      // 550
    const EMERGENCY = 'error'; // error     // 600

    const LOGGER_TYPE_NOLOGGER = 0;
    const LOGGER_TYPE_SYSLOG = 1;
    const LOGGER_TYPE_EMAIL = 2;
    const LOGGER_TYPE_LOGGLY = 3;

    public static function sendLog($channel, $tag, $type, $body)
    {

        $iLoggerEnabledMode = Yii::app()->config->get("LOGGER_ENABLED_MODE");

        try {

            switch ($iLoggerEnabledMode) {
                case self::LOGGER_TYPE_SYSLOG:
                    Yii::log($channel . "::" . $body . " ", $type, $tag);
                    break;

                case self::LOGGER_TYPE_EMAIL:

                    $sLayout = HeAbaMail::BUGS_LAYOUT;

                    switch ($type) {
                        case self::DEBUG:
                        case self::INFO:
                        case self::NOTICE:
                        case self::ALERT:
                            $sLayout = HeAbaMail::INFOPROC_LAYOUT;
                            break;
                        case self::WARNING:
                        case self::ERROR:
                        case self::CRITICAL:
                        case self::EMERGENCY:
                        default:
                            $sLayout = HeAbaMail::BUGS_LAYOUT;
                            break;
                    }

                    switch ($tag) {
                        case self::IT_BUGS:
                            HeAbaMail::sendEmailInternal(
                              Yii::app()->config->get("EMAIL_IT_BUGS_PAYMENTS"), "",
                              $channel, $body, true, null, HeAbaMail::BUGS_LAYOUT);
                            break;
                        case self::PAYMENTS:
                            HeAbaMail::sendEmailInternal(
                              Yii::app()->config->get("EMAIL_DPT_PAYMENT"), "",
                              $channel, $body, true, null, $sLayout);
                            break;
                        case self::PAYMENTS_MOBILE:
                            HeAbaMail::sendEmailInternal(
                              Yii::app()->config->get("EMAIL_DPT_PAYMENT_MOBILE"), "",
                              $channel, $body, true, null, HeAbaMail::BUGS_LAYOUT);
                            break;
                        case self::SELLIGENT:
                            HeAbaMail::sendEmailInternal(
                              Yii::app()->config->get("EMAIL_DPT_SELLIGENT"), "",
                              $channel, $body, true, null, HeAbaMail::ABA_LAYOUT);
                            break;
                        case self::FINANCE:
                            HeAbaMail::sendEmailInternal(
                              Yii::app()->config->get("EMAIL_FINANCE"), "",
                              $channel, $body, true, null, HeAbaMail::INFOPROC_LAYOUT);
                            break;
                        case self::SUPPORT:
                            HeAbaMail::sendEmailInternal(
                              Yii::app()->config->get("EMAIL_DPT_SUPPORT"), "",
                              $channel, $body, true, null, HeAbaMail::INFOPROC_LAYOUT);
                            break;
                        case self::IT_BUGS_PAYMENTS:
                            HeAbaMail::sendEmailInternal(
                              Yii::app()->config->get("EMAIL_IT_BUGS_PAYMENTS"),
                              Yii::app()->config->get("EMAIL_DPT_PAYMENT"),
                              $channel, $body, true, null, HeAbaMail::BUGS_LAYOUT);
                            break;
                        case self::RECONCILIATION:
                        case self::USERACTIVITY:
                        default:
                            HeAbaMail::sendEmailInternal(
                              Yii::app()->config->get("EMAIL_IT_BUGS_PAYMENTS"), "",
                              $channel, $body, true, null, HeAbaMail::BUGS_LAYOUT);
                            break;
                    }

                    break;

                case self::LOGGER_TYPE_LOGGLY:

//require_once ROOT_DIR . '/vendor/autoload.php';

//use Monolog\Logger;
//use Monolog\Handler\LogglyHandler;
//use Monolog\Formatter\LogglyFormatter;
//use Monolog\Handler\FirePHPHandler;

// class HeLogger extends Logger

//                    $log = new Logger($channel);
//                    $log->pushHandler(new LogglyHandler('6ab16bc9-0d27-496a-a96a-f885708e8252/tag/' . $tag, $type));
//
//                    if (is_array($body)) {
//                        return $log->addRecord($type, '', $body);
//                    } else {
//                        return $log->addRecord($type, $body);
//                    }
                    break;

                case self::LOGGER_TYPE_NOLOGGER:
                default:
                    break;

            }

        } catch (Exeption $e) {
        }

        return false;
    }

}