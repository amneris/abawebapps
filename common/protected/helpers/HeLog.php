<?php
/**
 *
 */
class HeLog
{

    const ERRORS_REPORTING_MODE_NOREPORT =  0;
    const ERRORS_REPORTING_MODE_LOGS =      1;
    const ERRORS_REPORTING_MODE_EMAIL =     2;
    const ERRORS_REPORTING_MODE_ALL =       3;

    /**
     * @return bool
     */
    static public function reportToEmail() {

        $iReportingMode = Yii::app()->config->get("ERRORS_REPORTING_MODE");

        if($iReportingMode == self::ERRORS_REPORTING_MODE_EMAIL OR $iReportingMode == self::ERRORS_REPORTING_MODE_ALL) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    static public function reportToLog() {

        $iReportingMode = Yii::app()->config->get("ERRORS_REPORTING_MODE");

        if($iReportingMode == self::ERRORS_REPORTING_MODE_LOGS OR $iReportingMode == self::ERRORS_REPORTING_MODE_ALL) {
            return true;
        }
        return false;
    }

}
