<?php

class HeAbaSelligent {

  static public $errorMsg = array();
  
  const _URL_EDITUSER = 'http://abaenglish.emsecure.net/optiext/optiextension.dll?ID=s6Xs0%2B9VsrKI2yyjRUiF9H8J%2B8l1R8xWhHUeIbfBW5az9_ZIIaBVrZFSrhOJGFr7Pc4tIU4LiAUQaDtfqk&IDUSER={$idUser}&IDENTERPRISE={$idEnterprise}&NAME={$name}&SURNAME={$surname}&EMAIL={$email}&PASS_TMP={$password}&PREF_LANG={$isoLanguage}';
  const _URL_EDITENTERPRISE = 'http://abaenglish.emsecure.net/optiext/optiextension.dll?ID=7ty7A6agKv1tP3FvedtmQBKp9SmziSQD5OMf9Yedm2Lkj3tlk0gy%2B9%2BteJaDMXZkqVJe1tQAaU8QtGAJzb&IDENTERPRISE={$idEnterprise}&NAME={$name}&IDCHANNEL={$idPartnerEnterprise}&IDMAINCONTACT={$idUser}&URL_LOGO={$logo}';
  const _URL_EDITSTUDENT = 'http://abaenglish.emsecure.net/optiext/optiextension.dll?ID=XzsXscdeffoHphXJ47ZmCwyOseTnFEzJr5G8BEM23FPRV1QbbzsnAJPIsUM36zIRRSjw4i0ye7CQinwofp&IDSTUDENT={$idStudent}&IDENTERPRISE={$idEnterprise}&NAME={$name}&SURNAME={$surname}&EMAIL={$email}&PREF_LANG={$isoLanguage}';
  const _URL_SENDLEVELTEST = 'http://abaenglish.emsecure.net/optiext/optiextension.dll?ID=MofMdYt56zRvkQosDwARYa7aGb3KHDhMr_eKbDUuWrpeV0XwxR5Wm4YupcoDaQBs3MDiGYeVy4yqQ5UccK&IDSTUDENT={$idStudent}&IDUSER={$idUser}&URL_CODE={$code}';
  const _URL_RECOVERYPASS = 'http://abaenglish.emsecure.net/optiext/optiextension.dll?ID=hVKhIDlFEp%2BLvPZv9MZrR%2Bc3_aWZy10RvQkp3MCU1i0vFrdx29ZVdNnI922xTsW25aQHrS8wm5mwteqlZz&IDUSER={$idUser}&PASS_TMP={$password}';
  const _URL_EDITBLOGUSER = 'http://abaenglish.emsecure.net/optiext/optiextension.dll?ID=_wD_EaOa3GivKgA8zBL0OlBj8PoX1h7hulGRynfuUbiU3PPUoTouKybDvO5%2BB%2BRsMxRCO3NRGWvOuZ0HeO&MAIL={$email}&NAME={$name}&PREF_LANG={$isoLanguage}&USER_TYPE={$userType}&REGISTERED={$registered}&IDUSER_WEB={$idUser}&IDUSER_BLOG={$idUserBlog}';
  const _URL_EDITBLOG = 'http://abaenglish.emsecure.net/optiext/optiextension.dll?ID=D1_D_2ddOoC9cS630QSudJX8qAv6YUEDKdcEsjCgUg%2BC8%2B7nT3XXNVoKjd6y0CrOJf6v220G9EevW0AOZ5&SUBJECT={$subject}&INTRO={$intro}&TWEET{$tweet}&TWITTER_LINK={$linkTweet}&ENVIAR={$enviar}&PREF_LANG={$isoLanguage}&IMG_1={$urlImg1}&TITULO_1={$titPost1}&DESC_1={$descPost1}&TEXT_LINK_1={$textPost1}&LINK_1={$linkPost1}&IMG_2={$urlImg2}&TITULO_2={$titPost2}&DESC_2={$descPost2}&TEXT_LINK_2={$textPost2}&LINK_2={$linkPost2}&IMG_3={$urlImg3}&TITULO_3={$titPost3}&DESC_3={$descPost3}&TEXT_LINK_3={$textPost3}&LINK_3={$linkPost3}&IMG_4={$urlImg4}&TITULO_4={$titPost4}&DESC_4={$descPost4}&TEXT_LINK_4={$textPost4}&LINK_4={$linkPost4}&IMG_5={$urlImg5}&TITULO_5={$titPost5}&DESC_5={$descPost5}&TEXT_LINK_5={$textPost5}&LINK_5={$linkPost5}';
  const _URL_RESENDWELCOMEMSG = 'http://abaenglish.emsecure.net/optiext/optiextension.dll?ID=ZBqZqQIQ%2BYPtJnc4IZ2mJbQush8ievGMsEH3pKVAUP9vYqmdc962zgUwCsmC_AQ1RGkhby028tlalhg1fd&IDUSER_WEB={$idStudent}&PASS_TMP={$password}';

  const _URL_EDITUSER_TEST = '/selligentemulator/edituser/iduser/$idUser/idEnterprise/$idEnterprise/name/$name/surname/$surname/email/$email/pass_tmp/$password/pref_lang/$isoLanguage';
  const _URL_EDITENTERPRISE_TEST = '/selligentemulator/editenterprise/idEnterprise/$idEnterprise/name/$name/idChannel/$idPartnerEnterprise/idMainContact/$idUser';
  const _URL_EDITSTUDENT_TEST = '/selligentemulator/editstudent/idStudent/$idStudent/idEnterprise/$idEnterprise/name/$name/surname/$surname/email/$email/pref_lang/$isoLanguage';
  const _URL_SENDLEVELTEST_TEST = '/selligentemulator/sendLevelTest/idStudent/{$idStudent}/idUser/{$idUser}/urlCode/{$code}';
  
  const _INTERNALMAIL = 'internal';
  const _EDITUSER = 'editUser';
  const _EDITENTERPRISE = 'editEnterprise';
  const _EDITSTUDENT = 'editStudent';
  const _SENDLEVELTEST = 'sendLevelTest';
  const _RECOVERYPASS = 'recoveryPass';
  const _EDITBLOGUSER = 'editBlogUser';
  const _EDITBLOG = 'editBlog';
  const _RESENDWELCOMEMSG = 'resendWelcomeMsg';
  
  static public function addToQueue( $action, $data, $callback = 'none' ) {
    $mSelligentElement = new SelligentQueue;
    $mSelligentElement->action = $action;
    $mSelligentElement->data = serialize($data);
    $mSelligentElement->callback = $callback;
    $allOk = $mSelligentElement->save();
    
//file_put_contents('/tmp/extra.log', "Error QUeue: ".var_Export($mSelligentElement->getErrors(), true)."\n", FILE_APPEND);
    return $allOk;
  }
  
  static public function send( $action, $data, $callback = null, $args = array() ) {
    $allOk = true;
    switch( $action ) {
      case self::_EDITUSER:
        $url = HeAbaSelligent::_URL_EDITUSER;
        break;
      case self::_EDITENTERPRISE:
        $url = HeAbaSelligent::_URL_EDITENTERPRISE;
        break;
      case self::_EDITSTUDENT:
        $url = HeAbaSelligent::_URL_EDITSTUDENT;
        if ( !empty($data->idStudentCampus) ) HeWebServices::synchroUser( $data->idStudentCampus );
        break;
      case self::_SENDLEVELTEST:
        if ( empty($data['code']) ) {
          Yii::app()->getModule('student');
          $data['code'] = ModuleStudentModel::generateNewLevelTestCode($data['idStudent']);
        }

        $url = HeAbaSelligent::_URL_SENDLEVELTEST;
        break;
      case self::_RECOVERYPASS:
        $url = HeAbaSelligent::_URL_RECOVERYPASS;
        break;
      case self::_EDITBLOGUSER:
        $url = HeAbaSelligent::_URL_EDITBLOGUSER;
        break;
      case self::_EDITBLOG:
        $url = HeAbaSelligent::_URL_EDITBLOG;
        break;
      case self::_RESENDWELCOMEMSG:
        $url = HeAbaSelligent::_URL_RESENDWELCOMEMSG;
        break;
      case self::_INTERNALMAIL:
        $allOk = HeAbaMail::sendEmailInternalTest( $data );
        
        self::setErrorMsg( HeAbaMail::getLastErrorMsg() );
        return $allOk;
      default:
        HeAbaSelligent::$errorMsg[] = 'Unkown action.';
        $allOk = false;
        return $allOk;
        break;
    }

    if ( $allOk && getenv('CORP_SIMULATESELLIGENT') === 'false' ) {
      foreach( $data as $k=>$v ) $data[$k] = urlencode($v);
      extract($data);
      eval("\$url = \"$url\";");

      $ch = curl_init();
      //set the url, number of POST vars, POST data
      curl_setopt($ch,CURLOPT_URL, $url);
      //execute post
      $result = curl_exec($ch);
      //close connection
      curl_close($ch);
      
      $allOk = (substr($result, 0, -2) == 'OK' || $result == 1);
      if ( !$allOk ) self::setErrorMsg ( "CURL: ".$result );
      
        // ** Send to oCallback
//      if ( $callback && $callback != 'none' ) {
//        $args['result'] = $result;
//        $arTmp = explode('::', $callback);
//        if ( count($arTmp) == 1 && function_exists($arTmp[0]) ) {
//          $allOk = call_user_func_array($arTmp[0], $args);
//        } else if ( count($arTmp) == 2  && function_exists($arTmp[0].'::'.$arTmp[1])) {
//          $allOk = call_user_func_array(array($arTmp[0], $arTmp[1]), $args);
//        } else $allOk = false;
//        
//        if ( !$allOk ) self::setErrorMsg ( Yii::t('app', 'Error trying to call callback: ').$callback );
//      }
      
    } else if ( getenv('CORP_SIMULATESELLIGENT') !== 'false' ) {
      return true;
      $allOk = HeAbaMail::sendEmailInternalTest( array( 'msg' => var_export($data, true) ) );

      self::setErrorMsg( HeAbaMail::getLastErrorMsg() );
      return $allOk;
    }
    
    return $allOk;
  }
  
  public function createUser() {
    sendEmailInternalTest( $arParameters );
  }  
  
  static public function setErrorMsg( $msg ) {
    self::$errorMsg[] = $msg;
    
    return $msg;
  }
  
  static public function getLastErrorMsg() {
    if ( empty(self::$errorMsg) ) return false;
    
    return self::$errorMsg[ count(self::$errorMsg)-1 ];
  }
  
}
