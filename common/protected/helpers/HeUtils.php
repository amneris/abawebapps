<?php
/**
 */
class HeUtils
{
    const ZERO_EXPIRATION_DATE = '2000-01-01 00:00:00';
    const GET = 'GET';
    const SERVER = 'SERVER';
    const POST = 'POST';
    const DEVICE_TOKEN = 'deviceToken';
    const KEY_EXTERNAL_LOGIN_TOKEN = 'keyExternalLoginToken';

    /**
     * Class casting
     *
     * @param string|object $destination
     * @param object $sourceObject
     * @return object
     */
    public static function cast($destination, $sourceObject)
    {
        if (is_string($destination)) {
            $destination = new $destination();
        }
        $sourceReflection = new ReflectionObject($sourceObject);
        $destinationReflection = new ReflectionObject($destination);
        $sourceProperties = $sourceReflection->getProperties();
        foreach ($sourceProperties as $sourceProperty) {
            $sourceProperty->setAccessible(true);
            $name = $sourceProperty->getName();
            $value = $sourceProperty->getValue($sourceObject);
            if ($destinationReflection->hasProperty($name)) {
                $propDest = $destinationReflection->getProperty($name);
                $propDest->setAccessible(true);
                $propDest->setValue($destination, $value);
            } else {
                $destination->$name = $value;
            }
        }
        return $destination;
    }

    public static function delTree($dir)
    {
        if (empty($dir)) {
            return;
        }
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? self::delTree("$dir/$file") : unlink("$dir/$file");
        }

        return rmdir($dir);
    }

    public static function newExpirationDate($action, $actualExpirationDate, $relativeTime)
    {
        if ($actualExpirationDate == HeUtils::ZERO_EXPIRATION_DATE) {
            $actualTime = strtotime(date("Y-m-d") . ' 00:00:00');
        } else {
            $actualTime = strtotime($actualExpirationDate);
        }

        $month = ($relativeTime / 30);
        if ($action == 'add') {
            $newDate = strtotime("+{$month} month", $actualTime);
            $newDate = mktime(0, 0, 0, date("m", $actualTime) + $month, date("d", $actualTime), date("Y", $actualTime));
            $expirationDate = date("Y-m-d H:i:s", $newDate);
        } elseif ($action == 'del') {
            $newDate = strtotime("-{$month} month", $actualTime);
            $expirationDate = date("Y-m-d H:i:s", $newDate);
        } else {
            $expirationDate = $actualExpirationDate;
        }

        return $expirationDate;
    }

    public static function getClientIP()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']) && $_SERVER['HTTP_CLIENT_IP']) {
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR']) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED']) && $_SERVER['HTTP_X_FORWARDED']) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        } elseif (isset($_SERVER['HTTP_FORWARDED_FOR']) && $_SERVER['HTTP_FORWARDED_FOR']) {
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_FORWARDED']) && $_SERVER['HTTP_FORWARDED']) {
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        } elseif (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR']) {
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        } else {
            $ipaddress = '';
        }

        return $ipaddress;
    }

    /**
     * Function that handles differences in how PHP handles header variables by looking for a key in the two possible
     * places and returns the value found if any.
     *
     * @param string $key The key to look for in the $_SERVER/Apache Headers array
     *
     * @return string/bool $returnValue The value found for input key, or false if key is not found
     */
    public static function getServerValue($key)
    {
        $returnValue = false;
        if (isset($_SERVER[$key])) {
            $returnValue = $_SERVER[$key];
        }
        if (trim($returnValue) == '' && function_exists('apache_request_headers')) {
            $headers = apache_request_headers();
            if (isset($headers[$key])) {
                $returnValue = $headers[$key];
            }
        }
        return $returnValue;
    }

    /**
     * Utility function for getting PHP Super Globals, also takes into account the difference for PHP HEADER fields on
     * Linux and Windows.
     *
     * @param string $superGlobal Currently either SERVER, GET, POST
     * @param string $key The key to look for in the $_SERVER/Apache Headers array
     *
     * @return bool/string $returnValue The value found for input key, or false if key is not found
     * @SuppressWarnings(PHPMD.Superglobals)
     * TODO: When all ABA applications use PHP > 5.4 change function to not access super globals directly!
     */
    public static function getSuperGlobal($superGlobal, $key)
    {
        $returnValue = false;
        switch ($superGlobal) {
            case 'SERVER':
                if (isset($_SERVER[$key])) {
                    $returnValue = $_SERVER[$key];
                }
                if (trim($returnValue) == '' && function_exists('apache_request_headers')) {
                    $headers = apache_request_headers();
                    if (isset($headers[$key])) {
                        $returnValue = $headers[$key];
                    }
                }
                break;
            case 'GET':
                if (isset($_GET[$key])) {
                    $returnValue = $_GET[$key];
                }
                break;
            case 'POST':
                if (isset($_POST[$key])) {
                    $returnValue = $_POST[$key];
                }
                break;
            default:
                $returnValue = false;
                break;
        }
        return $returnValue;
    }

    public static function uploadImg($arParams = array())
    {
        if (!isset($arParams['scaleX']) || !is_numeric($arParams['scaleX'])) {
            $arParams['scaleX'] = 100000;
        }
        if (!isset($arParams['scaleY']) || !is_numeric($arParams['scaleY'])) {
            $arParams['scaleY'] = 100000;
        }
        if (!isset($arParams['folder'])) {
            $arParams['folder'] = 'default';
        }

        if (!isset($arParams['inputFile'])) {
            if (!isset($arParams['inputName'])) {
                $arParams['inputName'] = 'fileName';
            }

            $inputName = $arParams['inputName'];
            $fileSrc = $arParams['inputFile'] = $_FILES[$inputName]['tmp_name'];
            $errorUploading = $_FILES[$inputName]['error'];

            $srcMimeType = $_FILES[$inputName]['type'];
        } else {
            $errorUploading = UPLOAD_ERR_OK;

            $fileSrc = $arParams['inputFile'];
            $arDatos = getimagesize($fileSrc);
            $srcMimeType = $arDatos['mime'];
        }

        $errorInputType = false;
        if ($srcMimeType == 'image/jpeg') {
            $img = imagecreatefromjpeg($fileSrc);
        } elseif ($srcMimeType == 'image/gif') {
            $img = imagecreatefromgif($fileSrc);
        } elseif ($srcMimeType == 'image/png') {
            $img = imagecreatefrompng($fileSrc);
        } else {
            $errorInputType = true;
        }

        $resultMessage = '';
        try {
            // ** Upload logo.
            $fileSrc = $arParams['inputFile'];
            if ($errorUploading != UPLOAD_ERR_OK) {
                throw new Exception(Yii::t('app', 'Error trying to upload file.'));
            }

            if ($errorInputType) {
                throw new Exception(Yii::t('app', 'Sorry, PNG or JPG are the only allowed formats at the moment.'));
            }

            $srcX = imagesx($img);
            $srcY = imagesy($img);
            if ($srcX >= $srcY) {
                $dstX = $dstY = $srcX;
            } else {
                $dstX = $dstY = $srcY;
            }
            if ($dstX > $arParams['scaleX']) {
                $scaleX = ($arParams['scaleX'] / $dstX);
            } else {
                $scaleX = 1;
            }
            if ($dstY > $arParams['scaleY']) {
                $scaleY = ($arParams['scaleY'] / $dstY);
            } else {
                $scaleY = 1;
            }
            $imgFinal = imagecreatetruecolor($dstX * $scaleX, $dstX * $scaleY);
            $color = imagecolorallocatealpha($imgFinal, 0, 0, 0, 127);
            imagefill($imgFinal, 0, 0, $color);

            $background = imagecolorallocate($imgFinal, 0, 0, 0);
            imagecolortransparent($imgFinal, $background);
            imagealphablending($imgFinal, false);
            imagesavealpha($imgFinal, true);

            imagecopyresized(
                $imgFinal,
                $img,
                (int) ((($dstX - $srcX) / 2) * $scaleX),
                (int) ((($dstY - $srcY) / 2) * $scaleY),
                0,
                0,
                ($srcX * $scaleX),
                ($srcY * $scaleY),
                $srcX,
                $srcY
            );

            $tmpFile = tempnam(Yii::app()->getRuntimePath(), 'im_');
            $fileName = time() . md5($tmpFile) . '.png';
            imagepng($imgFinal, $tmpFile);

            $imgPath = $arParams['folder'] . '/' . $fileName;
            HeAmazon::uploadFile($tmpFile, $imgPath);
        } catch (Exception $e) {
            $resultMessage = $e->getMessage();
        }

        return array('resultMessage' => $resultMessage, 'urlImg' => '/' . $imgPath);
    }

    /** Generates a new token based on the email, device and operative system.
     * Returns a string.
     *
     * @param string $sysOper
     * @param string $deviceId
     * @param string $email
     * @param string $password
     *
     * @return string
     */
    public static function genTokenForMobile($sysOper, $deviceId, $email, $password)
    {
        return md5(WORD4MOBILEWS . $sysOper . $deviceId . $email . $password);
    }

    /**
     * Specific function for Authorization Controller
     *
     * @param $paramName
     *
     * @return string
     */
    public function getRequestParamValue($paramName)
    {

        if (isset($_SERVER[$paramName])) {
            return $_SERVER[$paramName];
        }

        if (function_exists('apache_request_headers')) {
            $headers = apache_request_headers();
            if (isset($headers[$paramName])) {
                return $headers[$paramName];
            }
        }

        return '';
    }

}

