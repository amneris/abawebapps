<?php

class HeString
{
    static function isValidEmail($email)
	{	
		return preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^", $email);
	}
	static function getIntValue($id)
	{
		$in = array("=");
		$out = array("");
		return str_replace($in, $out, $id);
	}
	static function replaceText($buffer,$toreplace,$replacement)
	{
		$theKeys = array($toreplace);
		$theValues = array($replacement);
		$thebuffer = str_replace($theKeys, $theValues, $buffer);
		return $thebuffer;
	}
	static function cropStr($inText, $inLen=30)
	{	
		$len = strlen($inText);
		if($len<=$inLen)
			return $inText;
		return substr($inText,0, $inLen-3).'...';	
	}
	static function tooltip($inText)
	{	
		return "<STRONG>".$inText."</STRONG>";	
	}
    static function isReviewed($id, $reviewed)
    {
        if($reviewed)
                return 'Uncheck';
        return 'Check';
    }
    static function showIdStr($kind)
    {
        $array = HeList::creditCardShortDescription();
        return $array[$kind];
    }

    static function showKindTooltip($kind)
    {
        $array = HeList::creditCardDescription();
        return $array[$kind];
    }

    /** It expects an amount without decimal separator, but assuming it
     * has two positions at the rear being cents.
     *
     * @param $amountStr
     * @param int $decimalPos
     *
     * @return bool|float
     */
    static function convStr2DecToCurrency($amountStr, $decimalPos = 2)
    {
        if (!is_numeric($amountStr) || strlen($amountStr)<3){
            return false;
        }

        $amountStr = trim($amountStr);
        $tmpDouble = floatval( substr( strval(intval($amountStr)), 0, -2).'.'.substr( strval(intval($amountStr)), -2) );

        return $tmpDouble;
    }

    static function generateNewPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $alphabet = "abcdefghjkmnpqrstuwxyzABCDEFGHJKMNPQRSTUWXYZ123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    /**
     *
     * @param string $str
     * @param string $file
     * @return string
     */
    static public function toCharset( $str, $toUtf8 = true ) {

        $encoding = mb_detect_encoding($str, "UTF-8,MACINTOSH,ISO-8859-1");
        $auxEncoding = mb_detect_encoding($str);
        if ( $encoding == 'UTF-8' ) {
            if (!$toUtf8) $str = utf8_decode($str);
        } else if ( $encoding == 'ISO-8859-1' && $auxEncoding != 'UTF-8' ) {
            $encoding = ( $auxEncoding == 'ISO-8859-1' ) ? $encoding : 'MACINTOSH';

            if ($toUtf8) $str = iconv($encoding, 'UTF-8', $str);
            else $str = utf8_decode($str);

        } else {
//            if ($toUtf8) $str = preg_replace_callback("/([\x80-\xFF])/", function($matches) { return chr(0xC0|ord($matches[0])>>6).chr(0x80|ord($matches[0])&0x3F); }, $str);
        }

        return $str;
    }

}
