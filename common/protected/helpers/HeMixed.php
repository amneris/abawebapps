<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abaenglish
 *
 * Date: 5/10/12
 * Time: 16:44
 * To change this template use File | Settings | File Templates.
 */
class HeMixed
{
    /**
     * @static
     *
     * @param $imgFileName
     *
     * @return string
     */
    public static function getUrlPathImg($imgFileName)
    {
        return Yii::app()->theme->baseUrl.Yii::app()->config->get("PATH_STATIC_IMAGES").$imgFileName;
    }

    /**
     * @static
     *
     * @param        $amount
     * @param string $currency
     *
     * @return float
     */
    public static function getRoundAmount($amount, $currency="")
    {
        // @todo Special currencies expect to have special needs for rounding, so in the future we might want to check out which ones.
        return round($amount,2);
    }

    /**
     * @param $amount
     * @param int $precision
     *
     * @return float
     */
    public static function roundAmount($amount, $precision=2) {
        return round($amount, $precision);
    }

    /**
     * @param $number
     * @param int $decimals
     * @param string $decPoint
     * @param string $thousandsSep
     *
     * @return string
     */
    public static function numberFormat($number, $decimals=2, $decPoint='.', $thousandsSep='') {
        return number_format($number, $decimals, $decPoint, $thousandsSep);
    }

    /**
     * @static
     *
     * @param $amount
     *
     * @return float
     */
    public static function getCcyFromSQL($amount)
    {
        return doubleval($amount);
    }


    /**
     * @param $number
     * @return bool
     */
    public static function isValidNumberCard($number)
    {
        return (is_numeric($number));
    }

    /**
     * @param $number
     *
     * @return bool
     */
    public static function isValidLuhn( $number )
    {
        settype($number, 'string');
        $sumTable = array(
            array(0,1,2,3,4,5,6,7,8,9),
            array(0,2,4,6,8,1,3,5,7,9));
        $sum = 0;
        $flip = 0;
        for ($i = strlen($number) - 1; $i >= 0; $i--) {
            $sum += $sumTable[$flip++ & 0x1][$number[$i]];
        }
        return $sum % 10 === 0;
    }

    /** Returns kind of card or true for positive validations.
     *  for negative validation returns false.
     *
     * @param $creditCardNumber
     *
     * @return bool|string
     */
    public static function isValidCreditCard( $creditCardNumber )
    {
        $typeOfValidation = Yii::app()->config->get("PAY_ENFORCE__LEVEL_CARD");
        if(empty($typeOfValidation)){
            $typeOfValidation = PAY_VALID_CARD_LEVEL_MEDIUM;
        }
        $cc_number = $creditCardNumber;

        if( $typeOfValidation ==PAY_VALID_CARD_LEVEL_NONE ){
            return true;
        }
        if (Yii::app()->config->get("PAY_VISA_NUMBER_TEST") == $cc_number) {
            return true;
        }
        if( $typeOfValidation==PAY_VALID_CARD_LEVEL_LOW ){
            return self::isValidNumberCard($cc_number);
        }
        if( $typeOfValidation==PAY_VALID_CARD_LEVEL_MEDIUM ){
            return self::isValidLuhn($cc_number);
        }

        /* Validate; return value is card type if valid.
                DEFAULT = PAY_VALID_CARD_LEVEL_HIGH */
        $false = false;
        $card_type = "";
        $card_regexes = array(
            "/^4\d{12}(\d\d\d){0,1}$/" => "visa",
            "/^5[12345]\d{14}$/" => "mastercard",
            "/^3[47]\d{13}$/" => "amex",
            "/^6011\d{12}$/" => "discover",
            "/^30[012345]\d{11}$/" => "diners",
            "/^3[68]\d{12}$/" => "diners",
            "/^(?:2131|1800|35\d{3})\d{11}$/" => "jcb",
            "/^(?:2131|1800|35\d{3})\d{12}$/" => "jcb",
        );

        foreach ( $card_regexes as $regex => $type )
        {
            if ( preg_match($regex, $cc_number) )
            {
                $card_type = $type;
                break;
            }
        }

        if (!$card_type) {
            return $false;
        }

        /* mod 10 checksum algorithm */
        $revcode = strrev($cc_number);
        $checksum = 0;

        for ($i = 0; $i < strlen($revcode); $i++) {
            $current_num = intval($revcode[$i]);
            if($i & 1) { /* Odd position */
                $current_num *= 2;
            }
            /* Split digits and add. */
            $checksum += $current_num % 10; if
            ($current_num > 9) {
                $checksum += 1;
            }
        }

        if ($checksum % 10 == 0)
        {
            return $card_type;
        }
        else
        {
            return $false;
        }
    }

    /**
     *
     * @param integer $cpf 11 digits
     *
     * @return bool
     */
    public static function isValidateCPF($cpf)
    {
        // Should not be empty
        if (empty($cpf)) {
            return false;
        }

        // Any char non numeric? so it is invalid:
        if (!preg_match('/^[0-9]*$/', $cpf)) {
            return false;
        }
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
        $cpf = preg_replace('/[^0-9+]/', '', $cpf);

        if (strlen($cpf) != 11) {
            return false;
        }

        if ($cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999'
        ) {
            return false;
        } else {

            for ($t = 9; $t < 11; $t++) {

                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }

            return true;
        }
    }


    /**
     *
     * @param integer $cnpj 14 digits
     *
     * @return bool
     */
    public static function isValidateCNPJ($cnpj)
    {
        // Should not be empty
        if (empty($cnpj)) {
            return false;
        }

        if (strlen($cnpj) <> 14)
            return false;

        $soma = 0;

        $soma += ($cnpj[0] * 5);
        $soma += ($cnpj[1] * 4);
        $soma += ($cnpj[2] * 3);
        $soma += ($cnpj[3] * 2);
        $soma += ($cnpj[4] * 9);
        $soma += ($cnpj[5] * 8);
        $soma += ($cnpj[6] * 7);
        $soma += ($cnpj[7] * 6);
        $soma += ($cnpj[8] * 5);
        $soma += ($cnpj[9] * 4);
        $soma += ($cnpj[10] * 3);
        $soma += ($cnpj[11] * 2);

        $d1 = $soma % 11;
        $d1 = $d1 < 2 ? 0 : 11 - $d1;

        $soma = 0;
        $soma += ($cnpj[0] * 6);
        $soma += ($cnpj[1] * 5);
        $soma += ($cnpj[2] * 4);
        $soma += ($cnpj[3] * 3);
        $soma += ($cnpj[4] * 2);
        $soma += ($cnpj[5] * 9);
        $soma += ($cnpj[6] * 8);
        $soma += ($cnpj[7] * 7);
        $soma += ($cnpj[8] * 6);
        $soma += ($cnpj[9] * 5);
        $soma += ($cnpj[10] * 4);
        $soma += ($cnpj[11] * 3);
        $soma += ($cnpj[12] * 2);


        $d2 = $soma % 11;
        $d2 = $d2 < 2 ? 0 : 11 - $d2;

        if ($cnpj[12] == $d1 && $cnpj[13] == $d2) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * @static
     *
     * @throws CException
     *
     * @return bool|string
     */
    public static function getWord4Card(  )
    {
        if( md5(WORD4CARDS) == Yii::app()->config->get("WORD4CARDS") )
        {
            return WORD4CARDS;
        }

        throw new CException(" Important Problem with security keys in ABA");
    }

    /**
     * Checks and returns word for encryption
     * @static
     * @return string
     * @throws CException
     */
    public static function getWord4Ws(  )
    {
        if( md5(WORD4WS) == Yii::app()->config->get("WORD4WS") )
        {
            return WORD4WS;
        }

        throw new CException(" Important Problem with security keys in ABA 2");
    }

    /** Generic signature, not associated with web services but it uses word ws.
     * @param $params
     * @return string
     */
    public static function getSignatureUrlParams($params)
    {
        $joinParams = implode("", $params);
        $joinParams = HeMixed::getWord4Ws().$joinParams;
        $signature = md5( $joinParams );

        return $signature;
    }

    /**
     * Checks and returns word for encryption for affilites and partners
     * @static
     * @return string
     * @throws CException
     */
    public static function getWord4WsPartners(  )
    {
        if( md5(WORD4WSPARTNERS) == Yii::app()->config->get("WORD4WSPARTNERS") )
        {
            return WORD4WSPARTNERS;
        }

        throw new CException(" Important Problem with security keys in ABA 3rd key");
    }

    /**
     * @return string
     * @throws CException
     */
    public static function getWord4WsExtranet(  )
    {
        if( md5(WORD4WSEXTRANET) == Yii::app()->config->get("WORD4WSEXTRANET") )
        {
            return WORD4WSEXTRANET;
        }

        throw new CException(" Important Problem with security keys in ABA 3rd key");
    }

    /**
     * @return string
     *
     * @throws CException
     */
    public static function getWord4WsExternal() {
        if( md5(WORD4WSEXTERNAL) == Yii::app()->config->get("WORD4WSEXTERNAL") ) {
            return WORD4WSEXTERNAL;
        }
        throw new CException(" Important Problem with security keys in ABA 3rd key");
    }

    /**
     * Checks and returns word for encryption for Mobile web services
     * @static
     * @return string
     * @throws CException
     */
    public static function getWord4MobileWs(  )
    {
        if( md5(WORD4MOBILEWS) == Yii::app()->config->get("WORD4MOBILEWS") )
        {
            return WORD4MOBILEWS;
        }

        throw new CException(" Important Problem with security keys in ABA 2");
    }

    /**
     * @return string
     *
     * @throws CException
     */
    public static function getApiPwdAppStore(  )
    {
        if( md5(PAY_APP_STORE_API_PWD) == Yii::app()->config->get("PAY_APP_STORE_API_PWD") )
        {
            return PAY_APP_STORE_API_PWD;
        }

        throw new CException(" Important Problem with security keys in ABA 3rd key");
    }

    /**
     * Returns the value of an arg wether the url is HTTP original form or Yii friendly format.
     *
     * @static
     *
     * @param $name
     *
     * @return null|string
     */
    public static function getGetArgsRawMethod( $name )
    {
        if( $name == "" )
        {
            return NULL;
        }

        if ( ($getName = Yii::app()->getRequest()->getQuery( $name )) !== null )
        {
            return $getName;
        }

        $originalUrl = Yii::app()->getRequest()->url;
        if( strpos( $originalUrl, $name)>0 )
        {
            $start = strpos( $originalUrl, $name) + strlen($name)+1;
            $end = strpos($originalUrl, "/", $start);

            if( $end <= $start ){
                $end = $start + strpos($originalUrl, "&", $start);
            }
            if( $end <= $start ){
                $value = substr($originalUrl, $start);
            }else{
                $value = substr($originalUrl, $start, ($end-$start));
            }
            return $value;
        }
        else
        {
            return NULL;
        }
    }

    /**
     * To have unified in one place all browser data that comes from user, in order to
     * filter it or secure it in any specific way.
     * @param string $name
     *
     * @return array|mixed|null
     */
    public static function getPostArgs( $name="" )
    {
        // could use Yii POST Yii::app()->request->getPost. At the moment raw PHP.
        /*  @todo  Implement Yii input module for security purposes!! */
        if( $name == "" )
        {
            return $_POST;
        }
        else
        {
            if( isset($_POST[$name]) )
            {
                return $_POST[$name];
            }
            else
            {
                return NULL;
            }
        }

    }

    public static function getGetArgs( $name="" )
    {
        if( $name == "" )
        {
            return $_GET;
        }
        else
        {
            return self::getGetArgsRawMethod($name);
        }
    }

    /**Returns a two letter language
     * @static
     * @return array|null|string
     */
    public static function getAutoLangDecision()
    {
        // Then URL language
        $getLang = HeMixed::getGetArgs('language');
        if( !is_null($getLang) )
        {
            return $getLang;
        }

        // form or Widget change
        $postLang = HeMixed::getPostArgs('language');
        if( !is_null($postLang) )
        {
            return $postLang;
        }

        // Database has preference
        if( !empty( Yii::app()->user ) )
        {
            if( trim(Yii::app()->user->getId())!=='')
            {
                if( Yii::app()->user->getLanguage()!=='' )
                {
                    return Yii::app()->user->getLanguage();
                }
            }
        }

        // Browser settings
        $browserLangs = Yii::app()->getRequest()->getPreferredLanguages( );
        if( !empty($browserLangs) )
        {
          foreach( $browserLangs as $browserLang ) {
            $browserLang = strtolower(substr($browserLang, 0, 2));
            $langAvailable = array_search( $browserLang, array_keys(Yii::app()->params["languages"]));
            if( $langAvailable!==false )
            {
                return $browserLang;
            }
          }
        }

        // Default english
        return LANG_CAMPUS_DEFAULT;
    }


    public static function securePublicUrlRequest( $url )
    {
        /* @todo Temporary code, we have to enforce a real preventing behaviour for a DOS attack:
                this will work meanwhile: */
        $attempt = Yii::app()->user->getState("attemptPublicUrl");
        $attempt = $attempt + 1;
        Yii::app()->user->setState("attemptPublicUrl", $attempt );

        if( $attempt>MAX_ATTEMPTS )
        {
            sleep( $attempt-MAX_ATTEMPTS );
        }

        return true;
    }

    public static function isValidLevel($level)
    {
        return array_key_exists($level, Yii::app()->params['levels']) ? true : false;
    }


    /** Replaces the decimal separator. In the near future we will add the regional settings standard.
     *NOT BASED on THE CURRENCY BUT ON THE COUNTRY ID OF THE USER.
     * **** See HeView::formatAmountInCurrency()
     * For example for UK should be a point.
     * @static
     *
     * @param        $amount
     * @param string $currency
     * @param integer $userCountryId
     * @param string $originalDelimiter
     * @param string $newDelimiter
     *
     * @return mixed
     */
    public static function convDecDelimiterRegion($amount, $currency="EUR", $userCountryId=199, $originalDelimiter = ".", $newDelimiter = ",")
    {
        if( !is_string($amount) )
        {
            $amount = (string)$amount;
        }

        if($originalDelimiter!==$newDelimiter)
        {
            return str_replace( $originalDelimiter, $newDelimiter, $amount);
        }
        else{
            return $amount;
        }
    }


    /** To be used in views to display amounts, prices, etc. with decimal , currency symbols, etc. based on regional setting of the user/country.
     * Recommended; Much better than repeat str_replace, concatenate, format again and all over again on views.
     * @static
     *
     * @param        $amount
     * @param string $ccySymbol
     * @param int    $userCountryId
     * @param string $originalDelimiter
     * @param string $newDelimiter
     *
     * @return string
     */
    public static function retDisplayAmountRegionalFormat($amount, $ccySymbol="EUR", $userCountryId=199, $originalDelimiter = ".", $newDelimiter = ",")
    {

        $amount = HeMixed::convDecDelimiterRegion( $amount, $ccySymbol, $userCountryId, $originalDelimiter, $newDelimiter );
        if( $ccySymbol=="EUR" || $ccySymbol=="€" )
        {
            // Currently on 18/03/2013 only EUR scenario, in the near future several cases:
            return $amount." ".$ccySymbol;
        }
        else
        {
            // Currently on 18/03/2013 only USD scenario
            return $ccySymbol.$amount;
        }

    }

    public static function serializeToStoreInDb($mixedVar)
    {
        $newMixedVar = serialize($mixedVar);

        if(is_array($mixedVar) && count($mixedVar)==count($mixedVar,COUNT_RECURSIVE) )
        {
            $newMixedVar = "";
            foreach( $mixedVar as $key=>$value )
            {
                $newMixedVar = $newMixedVar. $key . "=".$value." || ";
            }
        }
        else
        {
            return json_encode($mixedVar);
        }

        return $newMixedVar;
    }


    /** To know if it is an ABA COUNTRY or it is a real one
     * @static
     *
     * @param integer $idCountry
     * @param array $aCountriesNoReal
     *
     * @return bool Return true if is NOT REAL
     */
    public static function isCountryNoReal( $idCountry, $aCountriesNoReal=null )
    {
        $aDefaultNoCountries = array( intval(Yii::app()->config->get("ABACountryId")), intval(COUNTRY_ABACOUNTRY_OLD) );
        $aNoCountries = $aDefaultNoCountries;
        if( is_array($aCountriesNoReal)){
            array_push( $aNoCountries, $aCountriesNoReal);
        }

        if( array_search( $idCountry, $aNoCountries )!==false || empty($idCountry) ){
            return true;
        }

        return false;

    }

    /** Returns the name of the machine if it is part of our network.
     * If it is not known it returns 'unknown'
     * @param string $ipFrom Ip requested
     *
     * @return string
     */
    public static function getNameKnownHost( $ipFrom )
    {
        switch( $ipFrom )
        {
            case '217.13.114.159':
                $hostName = "intranet.abaenglish.com";
                break;
            case '172.16.1.125':
            case '172.16.1.126':
                $hostName = "www.abaenglish.com";
                break;
            case '80.28.104.114':
                $hostName = "loadBalancer";
                break;
            case '192.168.56.1':
                $hostName = "quino.local.machine";
                break;
            case '127.0.0.1':
                $hostName = "localhost";
                break;
            default:
                $hostName = "unknown";
                break;
        }

        return $hostName;
    }

    /**
     * Checks if a XML file's structure is valid.

     * @param $file string
     * @return bool
     */
    public static function isXmlStructureValid($file) {
        $prev = libxml_use_internal_errors(true);
        $ret = true;
        try {
            new SimpleXMLElement($file, 0, false);
        } catch(Exception $e) {
            $ret = false;
        }
        if(count(libxml_get_errors()) > 0) {
            // There has been XML errors
            $ret = false;
        }
        // Tidy up.
        libxml_clear_errors();
        libxml_use_internal_errors($prev);
        return $ret;
    }

    /**
     * @param string $signature
     * @param array $aFields
     * @return bool
     */
    public static function isValidSignature($signature, $aFields)
    {
        if( substr($signature,-3) == "123"  && Yii::app()->config->get("SELF_DOMAIN")!==DOMAIN_LIVE )
        {
            return true ;
        }
        // We have to a check on signature. For example a hash with all arguments
        if( $signature === HeMixed::getSignatureUrlParams($aFields) ){
            return true;
        }

        return false;
    }

    /** Returns true if current PHP version is above $whichPHP. Only checks first 2 version numbers.
     *
     * @param string $whichPHP; 5.4.11 should be 54, 5.3.25 should be 53, 5.2.1 should be 52, 5.5.1 then 55
     * @return bool
     */
    public static function isPhpUpperEquals( $whichPHP='53' )
    {
        $aVersionPHP = explode ('.', phpversion());
        $currentPHP = intval($aVersionPHP[0].$aVersionPHP[1]);

        if ( $currentPHP>=$whichPHP ){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * @param string $sStr
     *
     * @return string
     */
    public static function toUpper( $sStr='' ) {
        if(function_exists('mb_strtoupper')) {
            return mb_strtoupper($sStr);
        }
        return strtoupper($sStr);
    }

    /**
     * Function creates CmAbaCountry object and attempts to use input IP to identify country. If no country can be found
     * for given IP or no IP is given Aba standard country will be returned.
     *
     * @param string/null $ip
     *
     * @return int Identifier for country
     */
    public static function getCountryId($ip = null) {
        $idCountry = Yii::app()->config->get('ABACountryId');
        $country = new CmAbaCountry();
        if ($country->getCountryCodeByIP($ip)) {
            $idCountry = $country->getId();
        }
        return $idCountry;
    }

    /**
     * @param $stArray
     * @param $stKey
     * @param $stValue
     */
    public static function arrayUnshiftAssoc(&$stArray, $stKey, $stValue) {

        $stArray = array_reverse($stArray, true);
        $stArray[$stKey] = $stValue;
        $stArray = array_reverse($stArray, true);
    }

    /**
     * @param $sLanguagePattern
     * @param $sLanguageSubject
     *
     * @return int
     */
    public static function filterCountry($sLanguagePattern, $sLanguageSubject) {
        return preg_match("/" . $sLanguagePattern . "/i", $sLanguageSubject);
//        if(preg_match("/" . $sLanguagePattern . "/i", $sLanguageSubject) == 1) {
//            return true;
//        }
//        return false;
    }

    /**
     * @param $sUserCountryCode
     *
     * @return string
     */
    public static function getCountryCodeIso2ForAdyen($sUserCountryCode) {
        switch(strtoupper(trim($sUserCountryCode))) {
            case strtoupper(CmAbaCountry::COUNTRY_CODE_ISO2_CHINA_ZH):
                return strtoupper(CmAbaCountry::COUNTRY_CODE_ISO2_CHINA_CN);
                break;
        }
        return strtoupper($sUserCountryCode);
    }

    /**
     * @param $sEmailSubject
     *
     * @return bool
     */
    public static function checkEmailForFraud($sEmailSubject) {
        try {
            $bCheckFrauds = Yii::app()->config->get('ADYEN_FRAUDS_EMAIL_ENABLED');

            if($bCheckFrauds == "1") {
                $sEmailPatterns = Yii::app()->config->get('ADYEN_FRAUDS_EMAIL_PATTERNS');
                $oEmailPatterns = explode(",", $sEmailPatterns);

                foreach($oEmailPatterns as $sKey => $sPattern) {
//                    if(preg_match("/(.*)@(.*)" . $sPattern . "(.*)/i", $sEmailSubject)) {
                    if(preg_match("/" . $sPattern . "/i", $sEmailSubject)) {
                        return true;
                    }
                }
            }
        } catch (Exception $e) { }

        return false;
    }

    /**
     * @param array $stParams
     *
     * @return bool
     */
    public static function trackLandings($stParams = array())
    {

        try {

            $bEnabledTracking = Yii::app()->config->get("TMP_LANDINGS_TRACKING_ENABLED");

            if ($bEnabledTracking == "1") {

                $sReferrer = "";

                if (isset($stParams['server']['HTTP_REFERER'])) {

                    $aReferrer = parse_url($stParams['server']['HTTP_REFERER']);
                    $sReferrer = trim($aReferrer["host"]) . $aReferrer["path"];
                }

                $oCmAbaTmpLandingsTracking = new CmAbaTmpLandingsTracking();

                $oCmAbaTmpLandingsTracking->tmpAbaIdentifier = $stParams['identifier'];
                $oCmAbaTmpLandingsTracking->tmpLandingIdentifier = $sReferrer;
                $oCmAbaTmpLandingsTracking->landingData = json_encode($stParams['post']);

                if ($oCmAbaTmpLandingsTracking->insertUserExperimentVariation()) {

                    foreach ($stParams['post'] AS $sKey => $sValue) {

                        $oCmAbaTmpLandingsFieldsTracking = new CmAbaTmpLandingsFieldsTracking();

                        $oCmAbaTmpLandingsFieldsTracking->tmpLandingTrackingId = $oCmAbaTmpLandingsTracking->tmpLandingTrackingId;

                        $oCmAbaTmpLandingsFieldsTracking->fieldKey = $sKey;
                        $oCmAbaTmpLandingsFieldsTracking->fieldValue = $sValue;

                        $oCmAbaTmpLandingsFieldsTracking->insertUserExperimentVariation();
                    }
                }
            }
        } catch (Exception $e) {

            HeAbaMail::sendEmailInternal(
              Yii::app()->config->get("EMAIL_IT_BUGS"),
              "",
              "LANDINGS TRACKING ::trackLandings",
              $e->getMessage() . "::" . $e->getMessage());

            return false;
        }
        return true;
    }

}

