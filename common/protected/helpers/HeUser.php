<?php
/**
 * HeUser
 * @author: mgadegaard
 * Date: 13/07/2015
 * Time: 17:58
 * © ABA English 2015
 */

class HeUser
{
    /**
     * Function takes gender char/varchar and outputs readable string (typically used to translate value from DB
     * (aba_b2c.user).
     *
     * @param $genderChar
     *
     * @return string
     */
    public static function getGenderDescription($genderChar)
    {
        switch (strtoupper($genderChar)) {
            case "M":
                $genderDescription = "Male";
                break;
            case "F":
                $genderDescription = "Female";
                break;
            default:
                $genderDescription = "";
        }

        return $genderDescription;
    }

    /** Generates a new token based on the email, device and operative system.
     * Returns a string.
     *
     * @param string $sysOper
     * @param string $deviceId
     * @param string $email
     * @param string $password
     *
     * @return string
     */
    public static function genTokenForMobile($sysOper, $deviceId, $email, $password)
    {
        return md5(WORD4MOBILEWS.$sysOper.$deviceId.$email.$password);
    }

    /**
     * @param array $names
     *
     * @return array
     */
    public static function getDefaultNames($names=array())
    {

        try {

            $defaultString = Yii::app()->config->get("ZUORA_DEFAULT_SURNAMES_STRING");

            $names["name"] = trim($names["name"]);
            $names["surnames"] = trim($names["surnames"]);

            if($names["name"] == "") {
                $names["name"] = $defaultString;
            }

            //
            if ($names["surnames"] == "") {

                $aName = explode(" ", $names["name"]);

                for ($i = count($aName) - 1; $i >= 0; $i--) {
                    if (trim($aName[$i]) <> "") {
                        $names["surnames"] = trim($aName[$i]);
                        break;
                    }
                }
            }

            if (trim($names["surnames"]) == "") {
                $names["surnames"] = $defaultString;
            }

        } catch (Exception $e) {
        }

        return $names;
    }

}
