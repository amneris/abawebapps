<?php

/**
 * User: Quino
 * Date: 15/11/12
 * Time: 11:57
 * To change this template use File | Settings | File Templates.
 */
class HeAbaMail
{

    const ABA_LAYOUT = "emailAbaInternal";
    const BUGS_LAYOUT = "bugs";
    const INFOIT_LAYOUT = "infoIT";
    const INFOPROC_LAYOUT = "infoProcesses";
    const FROM_CAMPUS_USER = "fromCampusUser";

    const PREFIX_ERR_UNKNOWN_L = "CAMPUS ERROR UNKNOWN(LOW)";
    const PREFIX_ERR_UNKNOWN_H = "CAMPUS ERROR UNKNOWN(HIGH)";
    const PREFIX_ERR_LOGIC_H = "CAMPUS INCONSISTENCY (HIGH)";
    const PREFIX_ERR_LOGIC_L = "CAMPUS INCONSISTENCY (LOW)";
    const PREFIX_NOTIF_L = "CAMPUS NOTIFICATION (LOW)";
    const PREFIX_NOTIF_H = "CAMPUS NOTIFICATION (HIGH)";
    const PREFIX_TESTS = "CAMPUS LOCAL TESTS SELENIUM (LOW)";
    const PREFIX_APP_LOGIC_L = "APP INCONSISTENCY (LOW)";

    const PREFIX_NOTIF_INTRA = "INTRANET NOTIFICATION";

    static public $errorMsg = array();

    /**
     * Uses the module of Yii to create a message into the database.
     * NOTHING TO DO WITH EMAILS!!!! They are internal message of the Campus
     * @static
     *
     * @param $senderId
     * @param integer $receiverId
     * @param string $receiverName
     * @param string $subject
     * @param string $body
     * @return bool
     */
    public static function sendMessageCampus($senderId, $receiverId, $receiverName = "", $subject = "", $body = "")
    {
//        $message = Message::model("Message");
        $message = new Message();

        if ($body !== '' && $receiverId !== '') {
            $message->receiver_id = $receiverId;
            $message->subject = $subject;
            $message->body = $body;
            $message->sender_id = $senderId;
            $message->is_read = 0;
            $message->deleted_by = Message::DELETED_BY_SENDER;
            if ($message->save(false)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Uses the module of Yii to create a message into the database.
     * NOTHING TO DO WITH EMAILS!!!! They are internal message of the Campus
     * @static
     *
     * @param $senderId
     * @param integer $receiverId
     * @param string $subject
     * @param string $body
     * @return bool
     */
    public static function sendCmMessageCampus($senderId, $receiverId, $subject = "", $body = "")
    {
        $message = new CmMessage();
        if ($body !== '' && $receiverId !== '') {
            $message->receiver_id = $receiverId;
            $message->subject = $subject;
            $message->body = $body;
            $message->sender_id = $senderId;
            $message->is_read = 0;
            $message->deleted_by = CmMessage::DELETED_BY_SENDER;
            if ($message->save(false)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @static
     *
     * @param string $to
     * @param string $cc
     * @param string $subject
     * @param string $body
     * @param bool $richHtml
     * @param null $controller
     * @param string $viewEmail
     * @param string $fromAlias
     * @param string $from
     * @param null $fileAttachment fle path to file
     *
     * @return bool
     */
    public static function sendEmailInternal($to, $cc, $subject = "", $body = "", $richHtml = false, $controller = NULL,
                                             $viewEmail = "bugs", $fromAlias = "Campus ABA English", $from = "", $fileAttachment = null, $ReplyTo = "")
    {

        if (intval(Yii::app()->config->get("EMAIL_SMTP_ENABLE")) <= 0) {
            error_log(" \n sendEmailInternal ; to=" . $to . " ; Subject= " . $subject . " ; Body=" . $body);
            return true;
        }

        if (trim(Yii::app()->config->get("EMAIL_SMTP_DOMAINS")) == '') {
            error_log(" \n sendEmailInternal ; to=" . $to . " ; Subject= " . $subject . " ; Body=" . $body);
            return true;
        }

        $aAllowedDomains = Yii::app()->config->get("EMAIL_SMTP_DOMAINS");
        $aAllowedDomains = explode(";", $aAllowedDomains);

        /* @var PHPMailer $platformEmail */
        /* @var EMailer $platformEmail */

        $platformEmail = Yii::app()->mailer;

        $platformEmail->ClearAllRecipients();
        $platformEmail->ClearAttachments();
        $platformEmail->ClearAddresses();
        $platformEmail->ClearCCs();
        $platformEmail->ClearBCCs();
        $platformEmail->ClearReplyTos();
        $platformEmail->ClearCustomHeaders();

        $platformEmail->CharSet = 'UTF-8';
        $platformEmail->SMTPAuth = true;
        $platformEmail->SingleTo = true;
        $platformEmail->IsSMTP();
        $platformEmail->Host = Yii::app()->config->get("EMAIL_HOST");
        $platformEmail->Username = Yii::app()->config->get("EMAIL_SMTP_USERNAME");
        $platformEmail->Password = Yii::app()->config->get("EMAIL_SMTP_PWD");
        $platformEmail->FromName = $fromAlias;
        if (!empty($fileAttachment)) {
            $platformEmail->AddAttachment($fileAttachment, basename($fileAttachment)
            /*, $encoding = 'base64', $type = 'application/octet-stream'*/);
        }

        if ($from != "")
            $platformEmail->From = $from;
        else
            $platformEmail->From = Yii::app()->config->get("EMAIL_SMTP_FROM");

        $platformEmail->Subject = $subject . " (" . Yii::app()->config->get("SELF_DOMAIN") . ") ";

        if ($ReplyTo != "")
            $platformEmail->AddReplyTo($ReplyTo);
            //$platformEmail->AddReplyTo(Yii::app()->config->get("EMAIL_SMTP_REPLYTO"));


        // TO:
        $auxTo = array($to);
        if (strpos($to, ";") > 0) {
            $auxTo = explode(";", $to);
        }
        $anyTo = false;
        foreach ($auxTo as $toEmail) {
            $domainTo = explode("@", $toEmail);
            $domainTo = $domainTo[1];
            if ($aAllowedDomains == "*" || array_search($domainTo, $aAllowedDomains, true) !== false) {
                $anyTo = true;
                $platformEmail->AddAddress($toEmail);
            }
        }

        // CC:
        if ($cc != "") {
            $auxTo = array($cc);
            if (strpos($cc, ";") > 0) {
                $auxTo = explode(";", $cc);
            }
            foreach ($auxTo as $toEmail) {
                $platformEmail->AddCC($toEmail);
            }
        }
        $simulController = $controller;
        if (!isset($simulController)) {
            $simulController = $platformEmail;
        }

        if (isset($simulController) && method_exists($simulController, 'renderFile')) {
            if (intval(Yii::app()->config->get("EMAIL_SMTP_ALLOW_HTML")) >= 1 && $richHtml) {
                $platformEmail->MsgHTML($simulController->renderFile($platformEmail->getPathView($viewEmail), array("body" => $body, "subject" => $platformEmail->Subject), true));
            } else {
                $platformEmail->Body = $simulController->renderFile($platformEmail->getPathView($viewEmail), array("body" => $body, "subject" => $platformEmail->Subject), true);
            }
        } else {
            $platformEmail->Body = $body;
        }

        try {
            if (!$anyTo) {
                throw new CException(" No address for sending emails or not allowed: " . $to);
            }
            if ($platformEmail->Send()) {
                return true;
            } else {
                //throw new CException(" El envío de emails está fallando");
                error_log(" Sending emails to the recipients: " . $to . " HAS FAILED due to SMTP issues. The subject content was <" . $subject . "> .");
                return false;
            }
        } catch (Exception $eSMTP) {
            error_log(" Sending emails to the recipients: " . $to . " HAS FAILED due to SMTP issues. The subject content was <" . $subject . "> . Details on error:" . $eSMTP->getMessage());
            return false;
        }

        return true;
    }

    /**
     *
     * @param array $arParameters
     * @return boolean
     */
    public static function sendEmailInternalTest( $arParameters ) {
      if ( !is_array($arParameters) ) return false;
      if( !isset($arParameters['msg']) ) $arParameters['msg'] = '';
      if( !isset($arParameters['msgHtml']) ) $arParameters['msgHtml'] = $arParameters['msg'];
      if( !isset($arParameters['msgText']) ) $arParameters['msgText'] = $arParameters['msg'];
      if( !isset($arParameters['to']) ) $arParameters['to'] = array( array( 'name' => 'Carles', 'mail' => 'cejarque@abaenglish.com' ) );
      else if( !is_array($arParameters['to']) && is_string($arParameters['to'])) $arParameters['to'] = array( array( 'mail' => $arParameters['to'] ) );
      else if( !is_array($arParameters['to']) ) return false;
      if( !isset($arParameters['bcc']) ) $arParameters['bcc'] = array();
      if( !isset($arParameters['isHtml']) ) $arParameters['isHtml'] = false;
      if( !isset($arParameters['subject']) ) $arParameters['subject'] = 'Here is the subject';

      $mail = new PHPMailer;
      try {
        $mail->CharSet = 'UTF-8';

    //  $mail->SMTPDebug = 3;                                   // Enable verbose debug output

        $mail->isSMTP();                                        // Set mailer to use SMTP
        $mail->Host = Yii::app()->params['EMAIL_SMTP_HOST'];
        if ( isset(Yii::app()->params['EMAIL_SMTP_SECURE']) ) {
          $mail->SMTPSecure = Yii::app()->params['EMAIL_SMTP_SECURE'];
        }

        $mail->SMTPAuth = true;                                       // Enable SMTP authentication
        $mail->Username = Yii::app()->params['EMAIL_SMTP_USERNAME'];  // SMTP username
        $mail->Password = Yii::app()->params['EMAIL_SMTP_PASSWORD'];  // SMTP password
    //    $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
    //    $mail->Port = 587;                                          // TCP port to connect to

        $mail->From = Yii::app()->params['ext_MailHelpFrom'];;
        $mail->FromName = 'Extranet Mailer';
        $validator=new CEmailValidator;
        foreach( $arParameters['to'] as $arMail ) {
          if ( getenv('CORP_SIMULATESELLIGENT') ) {
            $mail->addAddress('cejarque@abaenglish.com', 'Carles'); // Add a recipient
          } else {
            if ( !isset($arMail['mail']) ) continue;              // ** Malformed mail.
            if ( !$validator->validateValue($arMail['mail']) ) continue;// ** Malformed mail.
            if ( isset($arMail['name']) ) $mail->addAddress($arMail['mail'], $arMail['name']); // Add a recipient
            else $mail->addAddress($arMail['mail']); // Add a recipient
          }
        }

        foreach( $arParameters['bcc'] as $arMail ) {
          if ( !isset($arMail['mail']) ) continue;                      // ** Malformed mail.
          if ( !$validator->validateValue($arMail['mail']) ) continue;  // ** Malformed mail.
          if ( isset($arMail['name']) ) $mail->addBCC($arMail['mail'], $arMail['name']); // Add a recipient
          else $mail->addBCC($arMail['mail']);                          // Add a recipient
        }

        $mail->WordWrap = 50;                                   // Set word wrap to 50 characters
  //      $mail->isHTML(true);                                    // Set email format to HTML
        $mail->isHTML($arParameters['isHtml']);

        $mail->Subject = $arParameters['subject'];
//        $mail->Body    = "Mail enviado a: ".var_export($arParameters['to'], true)."\n<br>".$arParameters['msgHtml'];
        $mail->Body    = $arParameters['msgHtml'];
        $mail->AltBody = $arParameters['msgText'];
        if ( !$mail->send() ) {
          self::setErrorMsg( $mail->ErrorInfo );
          $allOk = false;
        } else $allOk = true;

      } catch (phpmailerException $e) {
        self::setErrorMsg( $e->errorMessage() );
        $allOk = false;
      } catch (Exception $e) {
        $allOk = false;
        self::setErrorMsg( $e->getMessage() );
      }

      return $allOk;
    }

    static public function sendEmail( $arParameters ) {
      if ( !is_array($arParameters) ) return false;
      if( !isset($arParameters['msg']) ) $arParameters['msg'] = '';
      if( !isset($arParameters['to']) ) $arParameters['to'] = array( array( 'name' => 'Carles', 'mail' => 'cejarque@abaenglish.com' ) );
      else if( !is_array($arParameters['to']) && is_string($arParameters['to'])) $arParameters['to'] = array( array( 'mail' => $arParameters['to'] ) );
      else if( !is_array($arParameters['to']) ) return false;
      if ( !isset($arParameters['action']) ) $arParameters['action'] = 'default';
      if ( !isset($arParameters['idStudent']) ) $arParameters['idStudent'] = '';
      if ( !isset($arParameters['idUser']) ) $arParameters['idUser'] = '';
      if ( !isset($arParameters['idEnterprise']) ) $arParameters['idEnterprise'] = '';
      if ( !isset($arParameters['name']) ) $arParameters['name'] = '';
      if ( !isset($arParameters['surname']) ) $arParameters['surname'] = '';
      if ( !isset($arParameters['email']) ) $arParameters['email'] = '';
      if ( !isset($arParameters['callback']) ) $arParameters['callback'] = 'ExtranetController::actionMailSent';
      if ( !isset($arParameters['code']) ) $arParameters['code'] = '';

      $allOk = false;
      $action = $arParameters['action'];
      switch( $action ) {
        case HeAbaSelligent::_EDITUSER:
          $allOk = HeAbaSelligent::addToQueue($action, array('idUser' => $arParameters['idUser'], 'idEnterprise' => $arParameters['idEnterprise'], 'name' => $arParameters['name'], 'surname' => $arParameters['surname'], 'email' => $arParameters['email'], 'password' => $arParameters['password'], 'isoLanguage' => $arParameters['isoLanguage']), $arParameters['callback']);
          break;
        case HeAbaSelligent::_EDITENTERPRISE:
          $allOk = HeAbaSelligent::addToQueue($action, array('idEnterprise' => $arParameters['idEnterprise'], 'name' => $arParameters['name'], 'idPartnerEnterprise' => $arParameters['idPartnerEnterprise'], 'idUser' => $arParameters['idUser'], 'logo' => $arParameters['logo'] ), $arParameters['callback']);
          break;
        case HeAbaSelligent::_EDITSTUDENT:
          $allOk = HeAbaSelligent::addToQueue($action, array('idStudent' => $arParameters['idStudent'], 'idEnterprise' => $arParameters['idEnterprise'], 'name' => $arParameters['name'], 'surname' => $arParameters['surname'], 'email' => $arParameters['email'], 'isoLanguage' => $arParameters['isoLanguage'] ), $arParameters['callback']);
          break;
        case HeAbaSelligent::_SENDLEVELTEST:
          $allOk = HeAbaSelligent::addToQueue($action, array('idStudent' => $arParameters['idStudent'], 'idUser' => $arParameters['idUser'], 'code' => $arParameters['code'] ), $arParameters['callback']);
          break;
        case HeAbaSelligent::_RECOVERYPASS:
          $allOk = HeAbaSelligent::addToQueue($action, array('idUser' => $arParameters['idUser'], 'password' => $arParameters['password']), $arParameters['callback']);
          break;
        case HeAbaSelligent::_EDITBLOGUSER:
          $allOk = HeAbaSelligent::addToQueue($action, array('email' => $arParameters['email'], 'name' => $arParameters['name'], 'isoLanguage' => $arParameters['isoLanguage'], 'userType' => $arParameters['userType'], 'registered' => $arParameters['registered'], 'idUser' => $arParameters['idUser'], 'idUserBlog' => $arParameters['idUserBlog']), $arParameters['callback']);
          break;
        case HeAbaSelligent::_EDITBLOG:
          $allOk = HeAbaSelligent::addToQueue($action, array( 'subject' => $arParameters['subject'], 'intro' => $arParameters['intro'], 'tweet' => $arParameters['tweet'], 'linkTweet' => $arParameters['linkTweet'], 'enviar' => $arParameters['enviar'], 'isoLanguage' => $arParameters['isoLanguage'], 'urlImg1' => $arParameters['urlImg1'], 'titPost1' => $arParameters['titPost1'], 'descPost1' => $arParameters['descPost1'], 'textPost1' => $arParameters['textPost1'], 'linkPost1' => $arParameters['linkPost1'], 'urlImg2' => $arParameters['urlImg2'], 'titPost2' => $arParameters['titPost2'], 'descPost2' => $arParameters['descPost2'], 'textPost2' => $arParameters['textPost2'], 'linkPost2' => $arParameters['linkPost2'], 'urlImg3' => $arParameters['urlImg3'], 'titPost3' => $arParameters['titPost3'], 'descPost3' => $arParameters['descPost3'], 'textPost3' => $arParameters['textPost3'], 'linkPost3' => $arParameters['linkPost3'], 'urlImg4' => $arParameters['urlImg4'], 'titPost4' => $arParameters['titPost4'], 'descPost4' => $arParameters['descPost4'], 'textPost4' => $arParameters['textPost4'], 'linkPost4' => $arParameters['linkPost4'], 'urlImg5' => $arParameters['urlImg5'], 'titPost5' => $arParameters['titPost5'], 'descPost5' => $arParameters['descPost5'], 'textPost5' => $arParameters['textPost5'], 'linkPost5' => $arParameters['linkPost5']  ), $arParameters['callback']);
          break;
        case HeAbaSelligent::_RESENDWELCOMEMSG:
          $allOk = HeAbaSelligent::addToQueue($action, array('idStudent' => $arParameters['idStudent'], 'password' => $arParameters['password']), $arParameters['callback']);
          break;
        default:
          $allOk = HeAbaSelligent::addToQueue( HeAbaSelligent::_INTERNALMAIL, $arParameters );
          break;
      }

      return $allOk;
    }

    static public function setErrorMsg( $msg ) {
      self::$errorMsg[] = $msg;

      return $msg;
    }


    /**
     *
     * @param type $index
     * @return string/boolean
     */
    static public function getErrorMsg( $index = -1 ) {
      if ( $index < 0 ) return self::$errorMsg;
      else if ( isset(self::$errorMsg[$index]) ) return self::$errorMsg[$index];
      else return false;
    }

    /**
     *
     * @return string/boolean
     */
    static public function getLastErrorMsg() {
      if ( empty(self::$errorMsg) ) return false;
      else return self::$errorMsg[ count(self::$errorMsg) - 1 ];
    }

}
