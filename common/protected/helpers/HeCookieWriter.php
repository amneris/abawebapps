<?php

class HeCookieWriter implements iCookieWriter
{
    private $cookieName;

    public function __construct($cookieName)
    {
        $this->cookieName = $cookieName;
    }

    public function write($cookieValue)
    {
        setcookie($this->cookieName, $cookieValue, 0, '/');
    }
}