<?php

/**
 * HePayments
 * Author: mgadegaard
 * Date: 08/04/2015
 * Time: 15:30
 * © ABA English 2015
 */
class HePayments {

    /**
     * Function to check whether a payments payment method is Credit/debit Card or not.
     * @param $payment
     * @return bool $isCreditDebitCard True if Payment Method is Credit Card, otherwise false
     */
    public static function isCreditDebitCard($payment) {
        switch ($payment->attributes['paySuppExtId']) {
            case PAY_SUPPLIER_CAIXA:
                $isCreditCard = true;
                break;
            case PAY_SUPPLIER_ALLPAGO_BR:
                $isCreditCard = true;
                break;
            case PAY_SUPPLIER_ALLPAGO_MX:
                $isCreditCard = true;
                break;
            case PAY_SUPPLIER_ADYEN:
            case PAY_SUPPLIER_ADYEN_HPP:
                $isCreditCard = true;
                break;
            default:
                $isCreditCard = false;
        }
        return $isCreditCard;
    }

    public static function isCandidateForRefund($payment, $idUser)
    {
        if ($payment) {
            if (HePayments::isB2b($payment) ||
                HePayments::isPaymentOlderThan90($payment) ||
                !HePayments::isCreditDebitCard($payment) ||
                !HeQuery::testRegisterSuccess($idUser, $payment->id)) {
                return false;
            } else {
                return true;
            }
        }
    }

    public static function isB2b($payment)
    {
        if ($payment->attributes['paySuppExtId'] == PAY_SUPPLIER_B2B) {
            return true;
        } else {
            return false;
        }
    }

    public static function isPaymentOlderThan90($payment)
    {
        if (abs(HeDate::getDifferenceDateSQL($payment->dateEndTransaction,
                HeDate::todaySQL(true),
                HeDate::DAY_SECS)) >= 90) {
            return true;
        } else {
            return false;
        }
    }

    public static function hasRefundPayment($payment) {
        return false;
    }

    public static function isCandidateForCancel($payment)
    {
        if (HePayments::isB2b($payment) || !HePayments::isCreditDebitCard($payment)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param $idProduct
     * @param $idCountry
     * @param $idPeriodPay
     *
     * @return string
     */
    public static function getProductPricePK($idProduct, $idCountry, $idPeriodPay)
    {
        return $idProduct . DELIMITER_KEYS . $idCountry . DELIMITER_KEYS . $idPeriodPay;
    }

    /**
     * @param $idCountry
     * @param $idPeriodPay
     *
     * @return string
     */
    public static function makeFamilyPlanProductId($idCountry, $idPeriodPay) {
        return $idCountry . DELIMITER_PRODUCT . $idPeriodPay . DELIMITER_PRODUCT . "2";
    }

    /**
     * @param $idCountry
     * @param $idPeriodPay
     *
     * @return string
     */
    public static function makeCashBackProductId($idCountry, $idPeriodPay) {
        return $idCountry . DELIMITER_PRODUCT . $idPeriodPay;
    }

    /**
     * @param $packageSelected
     *
     * @return array
     */
    public static function parseProductPricePK($packageSelected)
    {
        return explode(DELIMITER_KEYS, $packageSelected);
    }

    /**
     * @param $idPeriodPay
     *
     * @return float
     */
    public static function periodToMonth($idPeriodPay)
    {
        return $idPeriodPay / 30;
    }

    /**
     * @param $periodMonth
     *
     * @return mixed
     */
    public static function monthToPeriod($periodMonth)
    {
        return $periodMonth * 30;
    }

    /**
     * @param $idProduct
     *
     * @return array
     */
    public static function parseProductId($idProduct) {
        return explode(DELIMITER_PRODUCT, $idProduct);
    }

    /**
     * @param $payId
     *
     * @return bool
     */
    public static function isParcialSuccess($payId)
    {
        if (substr($payId, 0, 1) == 'S') {
            return true;
        }
        return false;
    }

    /**
     * It returns  crc32(UserId + Email + Time now) + timeMMDD;
     * @param $user
     * @param string $dateToPay
     *
     * @return string
     */
    public static function generateIdPayment($user, $dateToPay = null)
    {
        if (!isset($dateToPay)) {
            $timeNow = time();
            $timeNow = date('YmdHis', $timeNow);
        } else {
            $dateToPay = substr($dateToPay, 0, 10);
            $timeNow = new DateTime($dateToPay.date('H:i:s', time()));
            $timeNow = date('YmdHis', $timeNow->format("YmdHis"));
        }
        $theString=$user->id.$user->email.$timeNow ;
        return hash("crc32b", $theString);
    }

    /**
     * @param $id
     * @param null $dateToPay
     *
     * @return string
     */
    static function generateIdPaymentRefund($id, $dateToPay = null)
    {
        $timeNow = $dateToPay;
        if (!isset($dateToPay)) {
            $timeNow = time();
            $timeNow = date('YmdHis', $timeNow);
        } else {
            $timeNow = new DateTime($dateToPay . date('H:i:s', time()));
            $timeNow = date('YmdHis', $timeNow->format("YmdHis"));
        }
        $theString = $id . $timeNow;
        $theStringRet = sprintf("%s_%02X", hash("crc32b", $theString), strlen($theString));
        return $theStringRet;
    }

    /**
     * @param $creditCard
     * @return int
     */
    public static function getCreditCardType($creditCard) {

        $creditCard = trim(str_replace(array('-', ' '), '', $creditCard));

        if((bool) preg_match('/^(506699[0-9]{10}|5067[0-6][0-9]{11}|50677[0-8][0-9]{10}|401178[0-9]{10}|438935[0-9]{10}|451416[0-9]{10}|457631[0-9]{10}|457632[0-9]{10}|504175[0-9]{10}|627780[0-9]{10}|636297[0-9]{10}|636368[0-9]{10})$/', $creditCard)) {
            return KIND_CC_ELO;
        }

        if((bool) preg_match('/^9[0-9]{15}$/', $creditCard)) {
            return KIND_CC_VIAS;
        }

        if((bool) preg_match('/^(6011[0-9]{12}|64[4-9][0-9]{13}|65[0-9]{14})$/', $creditCard)) {
            return KIND_CC_DISCOVER;
        }

//        if((bool) preg_match('/^62([0-9]{12}|[0-9]{13}|[0-9]{14}|[0-9]{15}|[0-9]{16}|[0-9]{17})$/', $creditCard)) {
        if((bool) preg_match('/^62[0-9]{14}$/', $creditCard)) {
            return KIND_CC_CUP;
        }

        if((bool) preg_match('/^(50[0-9]{14}|5[6-9][0-9]{14}|6[0-9]{15})$/', $creditCard)) {
            return KIND_CC_MAESTRO;
        }

        if((bool) preg_match('/^5[1-5][0-9]{14}$/', $creditCard)) {
            return KIND_CC_MASTERCARD;
        }

        if((bool) preg_match('/^4([0-9]{12}|[0-9]{15})$/', $creditCard)) {
            return KIND_CC_VISA;
        }

        if((bool) preg_match('/^(352[8-9][0-9]{12}|35[3-8][0-9]{13})$/', $creditCard)) {
            return KIND_CC_JCB;
        }

        if((bool) preg_match('/^3(0[0-5][0-9]{11}|[68][0-9]{12})$/', $creditCard)) {
            return KIND_CC_DINERS_CLUB;
        }

        if((bool) preg_match('/^3[47][0-9]{13}$/', $creditCard)) {
            return KIND_CC_AMERICAN_EXPRESS;
        }

        if((bool) preg_match('/^1([0-9]{13}|[0-9]{14})$/', $creditCard)) {
            return KIND_CC_UATP;
        }

        return KIND_CC_NO_CARD;
    }

}
