<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 18/09/12
 * Time: 17:48
 * Class to manage all validations, calculations and conversions with Dates.
 */
class HeDate
{
    const separator = "-";
    const separatorTime = ":";

    const SECS = 1;
    const MINUTES = 60;
    const HOURS = 3600;
    const DAY_SECS = 86400;

    /**
     * @static
     *
     * @param string $d   dd-mm-yyyy
     * @param bool $monthFirst
     * @param      $separator
     *
     * @return bool
     */
    public static function validDate($d, $monthFirst = false, $separator='-')
    {
        if (!is_string($d))
            return false;

        if (strlen($d) != 10)
            return false;

        $a = explode($separator, $d);
        $m = (int)($monthFirst ? $a[0] : $a[1]);
        $d = (int)($monthFirst ? $a[1] : $a[0]);
        $y = (int)($a[2]);

        return $d >= 1 && $d <= 31 && $m >= 1 && $m <= 12 && $y >=1900 && $y<=2900;
    }

    /**
     * Checks if the date passed matches the format YYYY-MM-DD
     * @static
     *
     * @param string $dateYYYYMMDD
     * @param bool $withTime
     *
     * @return bool
     */
    public static function validDateSQL( $dateYYYYMMDD, $withTime=false )
    {
        if ( !is_string($dateYYYYMMDD) ){
            return false;
        }

        if ( !$withTime && strlen($dateYYYYMMDD) != 10){
            return false;
        }

        if ( $withTime && strlen($dateYYYYMMDD) != 19){
            return false;
        }
        elseif( $withTime ){
            $timeHHMMSS = substr($dateYYYYMMDD,11,8);
            $aTime = explode(self::separatorTime, $timeHHMMSS);
            $hh = (int)$aTime[0];
            $mm = (int)$aTime[1];
            $ss = (int)$aTime[2];
            if( !($hh >= 0 && $hh <= 24 && $mm >= 0 && $mm <= 59 && $ss >= 0 && $ss <= 59) ){
                return false;
            }

            $dateYYYYMMDD = substr($dateYYYYMMDD, 0, 10);
        }

        $a = explode(self::separator, $dateYYYYMMDD);
        $m = (int)$a[1];
        $d = (int)$a[2];
        $y = (int)$a[0];

        return $d >= 1 && $d <= 31 && $m >= 1 && $m <= 12 && $y >=1900 && $y<=2900;
    }

    /**
     * @static
     *
     * @param        $s
     * @param string $separatorIN
     * @param string $format
     *
     * @return string
     */
    public static function SQLToEuropean($s, $separatorIN='-', $format='')
    {
        $a = explode($separatorIN, $s);
        $d = intval($a[2]);
        $m = intval($a[1]);
        $y = intval($a[0]);
        if ($format == 'dd/mm/yyyy'){
            $d = substr('00'.$d,-2);
            $m = substr('00'.$m,-2);
        }

        $s = $d.$separatorIN.$m.$separatorIN.$y;

        return $s;
    }
	
    /**
     * @static
     *
     * @param        $s
     * @param string $separatorIN
     *
     * @return string
     */
    public static function SQLToAmerican($s, $separatorIN='-')
    {
        $a = explode($separatorIN, $s);
        $d = intval($a[2]);
        $m = intval($a[1]);
        $y = intval($a[0]);

        $s = $m.$separatorIN.$d.$separatorIN.$y;

        return $s;
    }


    /** Returns based on the country the date in the regional format.
     * At the moment only US is taken into account
     *
     * @param string $s YYYY-MM-DD
     * @param string $separatorIN
     * @param integer $idCountry
     *
     * @return string
     */
    public static function SQLToUserRegionalDate($s, $separatorIN='-', $idCountry=null)
    {
        if(HeDate::validDateSQL($s)){
            if($idCountry==COUNTRY_US){
                return HeDate::SQLToAmerican($s, $separatorIN);
            } else{
                return HeDate::SQLToEuropean($s, $separatorIN);
            }
        }

        return false;
    }

    /**
     * @static
     *
     * @param        $s
     * @param bool   $time
     * @param string $separatorIN
     * @param bool   $allowNullOut
     * @param bool   $setApostrophes
     *
     * @return bool|string
     */
    public static function europeanToSQL($s, $time=false, $separatorIN=self::separator, $allowNullOut=true, $setApostrophes = false)
    {
        if (trim($s) == '') {
            return "null";
        }

        if ($time) {
            $aDateAndTime = explode(" ", trim($s));
            $s = $aDateAndTime[0];
            $sTime = $aDateAndTime[1];
        } else {
            $sTime = '00:00:00';
        }

        $a = explode($separatorIN, $s);
        $d = intval($a[0]);
        $m = intval($a[1]);
        $y = intval($a[2]);

        $ymd = $y . $separatorIN . $m . $separatorIN . $d;

        if ($time) {
            $aTime = explode(":", $sTime);
            $h = $aTime[0];
            $n = $aTime[1];
            $s = $aTime[2];
            if (!$setApostrophes) {
                return date('Y-m-d H:i:s', mktime($h, $n, $s, $m, $d, $y));
            } else {
                return "'" . date('Y-m-d H:i:s', mktime($h, $n, $s, $m, $d, $y)) . "'";
            }
        }

        if (!$setApostrophes) {
            return self::removeTimeFromSQL(date("Y-m-d", mktime(0, 0, 0, $m, $d, $y)));
        } else {
            return "'" . date("Y-m-d", mktime(0, 0, 0, $m, $d, $y)) . "'";
        }
    }

    /**
     * Returns Today in format dd-mm-yyyy
     * @static
     *
     * @return string
     */
    public static function echoToday()
    {
        echo "".date("d-M-Y",time());
    }

    /** Returns current Date and/or Time
     * @static
     *
     * @param bool $withTime
     *
     * @return string YYYY-MM-DD
     */
    public static function todaySQL($withTime=false)
    {
        if($withTime){
            return date("Y-m-d H:i:s",time());
        }
        return date("Y-m-d",time());
    }

    /** Returns time now without
     * @param bool $withTime
     * @param string $delimiter
     *
     * @return bool|string
     */
    public static function todayNoSeparators($withTime=false, $delimiter='_')
    {
        if($withTime){
            return date("Ymd".$delimiter."His",time());
        }
        return date("Ymd",time());
    }

    /** Returns time in format hh:mm:ss.milliseconds
     * @static
     *
     * @param bool $withMilliseconds
     *
     * @return string
     */
    public static function timeNow($withMilliseconds = false)
    {
        $timeNow = date("H:i:s", time());

        if ( $withMilliseconds )
        {
            $timeNow = date("H:i:s.u", time() );
            $aMicroTime = explode(" ",microtime() );
            $timeNow = str_replace( '000000', substr($aMicroTime[0],2),$timeNow );
        }

        return $timeNow;
    }

    /**
     * @static
     *
     * @param null $start
     * @param null $before
     * @param null $after
     * @param null $sort
     *
     * @return array|bool
     */
    public static function getDateYears( $start = NULL, $before = NULL, $after = NULL, $sort = NULL )
    {
        $years = array();
        if ( empty( $start ) || ( empty( $before ) && empty( $after ) ) ) {
            return false;
        }
        $years[$start] = $start;
        if ( $before ) {
            for( $i = 1; $i <= $before; $i++ ) {
                $years[$start - $i] = $start - $i;
            }
        }
        if ( $after ) {
            for( $i = 1; $i <= $after; $i++ ) {
                $years[$start + $i] = $start + $i;
            }
        }
        asort( $years );
        if ( $sort ) {
            rsort( $years );
        }
        return $years;
    }

    /**
     * Add Day To Date
     * @param string $sDateYYYYMMDD = date(YYYY-MM-DD)
     * @param integer $iSumDay = integer, number of days
     * @param integer $iSumMonth
     *
     * @return string date(YYYY-MM-DD)
     */
    public static function getDateAdd( $sDateYYYYMMDD, $iSumDay=0, $iSumMonth=0)
    {
        $oDate = new DateTime($sDateYYYYMMDD);
        $iYear=0; $iMonth=0; $iDay=0;
        $iH=0; $iM=0; $iS=0;
        if( strlen($sDateYYYYMMDD)>10 ){
            $aDateTime = explode(" ", $sDateYYYYMMDD );
            list($iYear, $iMonth, $iDay)=explode('-', $aDateTime[0]);
            list($iH, $iM, $iS)=explode(':', $aDateTime[1]);
        }
        else{
            list($iYear, $iMonth, $iDay)=explode('-',$sDateYYYYMMDD);
            $iH=0; $iM=0; $iS=0;
        }

        if( abs($iSumDay)>0 )
        {
            /* >= PHP 5.3.
            $intervalSum = new DateInterval( "P".$iSumDay."D" );
            $oDate->add($intervalSum);
            return $oDate->format('Y-m-d');
            */
            return date('Y-m-d',mktime($iH,$iM,$iS, $iMonth, ($iDay+intval($iSumDay)), $iYear));
        }
        elseif( abs($iSumMonth)>0 )
        {
           /*
            $intervalSum = new DateInterval( "P".$iSumMonth."M" );
            $oDate->add($intervalSum);
            return $oDate->format('Y-m-d');
           */
            return date('Y-m-d',mktime( $iH, $iM, $iS, ($iMonth+intval($iSumMonth)), $iDay, $iYear));
        }
        else
        {
            return $sDateYYYYMMDD;
        }


    }

    /**
     * @param string $sDateYYYYMMDD HHMMSS
     * @param integer $iSumUnits
     * @param integer $formatUnits
     *
     * @return string
     */
    public static function getDateTimeAdd( $sDateYYYYMMDD, $iSumUnits, $formatUnits )
    {
        $oDate = new DateTime($sDateYYYYMMDD);
        $iYear=0; $iMonth=0; $iDay=0;
        $iH=0; $iM=0; $iS=0;
        if( strlen($sDateYYYYMMDD)>10 ){
            $aDateTime = explode(" ", $sDateYYYYMMDD );
            list($iYear, $iMonth, $iDay)=explode('-', $aDateTime[0]);
            list($iH, $iM, $iS)=explode(':', $aDateTime[1]);
        }
        else{
            list($iYear, $iMonth, $iDay)=explode('-',$sDateYYYYMMDD);
            $iH=0; $iM=0; $iS=0;
        }

        if( abs($iSumUnits)>0 ) {
            return date('Y-m-d H:i:s', mktime($iH,$iM,$iS+($iSumUnits*$formatUnits), $iMonth, $iDay, $iYear));
        }else{
            return $sDateYYYYMMDD;
        }
    }

    /**
     * @param string $dateYYMMDDUTC
     * @param string $strTimeZone
     *
     * @return string
     */
    public static function changeFromUTCTo( $dateYYMMDDUTC, $strTimeZone='Europe/Madrid')
    {
        $zoneDateMadrid = new DateTimeZone($strTimeZone);
        $zoneDateUTC = new DateTimeZone('UTC');

        $dtimeYYMMDDUTC = new DateTime( $dateYYMMDDUTC, $zoneDateUTC );

        $dtimeYYMMDDUTC->setTimezone($zoneDateMadrid);

        return $dtimeYYMMDDUTC->format('Y-m-d H:i:s');
    }


    /**
     * Returns the DAY in 2 numbers format. The date should be in SQL format YYYY-MM-DD.
     * @param string $sSQLDate
     * @return 2 days digit
     */
    public static function getDayFromDate($sSQLDate)
    {
        list($iYear, $iMonth, $iDay)=explode(self::separator,$sSQLDate);
        return $iDay;
    }

    /**
     * Returns YYYY-MM-DD with replacing DD by $newDay
     * @static
     *
     * @param $sSQLDate
     * @param $newDay
     *
     * @return string
     */
    public static function setDayFromDate($sSQLDate, $newDay)
    {
        list($iYear, $iMonth, $iDay)=explode(self::separator,$sSQLDate);
        return $iYear.self::separator.$iMonth.self::separator.$newDay;
    }

    /**
     * Returns the month in 2 numbers format. The date should be in SQL format YYYY-MM-DD.
     * @param string $sSQLDate
     * @return 4 year digit
     */
    public static function getMonthFromDate($sSQLDate)
    {

        list($iYear, $iMonth, $iDay)=explode(self::separator,$sSQLDate);
        return $iMonth;
    }

    /**
     * Returns the year in 4 numbers format. The date should be in SQL format.
     * @param string $sSQLDate
     * @return 4 year digit
     */
    public static function getYearFromDate($sSQLDate)
    {

        list($iYear, $iMonth, $iDay)=explode(self::separator,$sSQLDate);
        return $iYear;
    }

    /**
     * Returns the date but replaces the day part with last day of the month.
     * @static
     *
     * @param string $sSQLDate format YYYY-MM-DD
     *
     * @return string
     */
    public static function getDateWithLastDay( $sSQLDate )
    {
        return date("Y-m-t", strtotime($sSQLDate) );
    }

    /**
     * @static
     *
     * @param integer $quantityOfTimeSecs Time in seconds
     *
     * @return string X months X days X hours X minutes
     */
    public static function getFormatQuantitiesTime( $quantityOfTimeSecs )
    {
        if($quantityOfTimeSecs==0)
        {
            return " 0 minutes";
        }

        $months = (intval(date('m', $quantityOfTimeSecs))-1)." months ";

        $days = (intval(date('d', $quantityOfTimeSecs))-1)." days ";

        $hours =(intval(date('h', $quantityOfTimeSecs))-1)." hours ";

        $minutes = (intval(date('i', $quantityOfTimeSecs)))." minutes ";

        // @todo Missing the translations for these words Yii:t("mainApp","month_key")
        return ((trim($months)=="0 months")?"":$months) . ((trim($days)=="0 days")?"":$days). ((trim($hours)=="0 hours")?"":$hours). ((trim($minutes)=="0 minutes")?"":$minutes);
    }


    /**
     * @static
     *
     * @param      $dateYYYYMMDD
     * @param bool $equal
     *
     * @return bool
     */
    public static function isGreaterThanToday($dateYYYYMMDD, $equal= false )
    {
        return self::isGreaterThan($dateYYYYMMDD, self::todaySQL(false), $equal );
    }

    /**
     * Returns true if $dateYYYYMMDD is more recent than $dateOlderYYYYMMDD
     * @static
     *
     * @param string $dateYYYYMMDD
     * @param string $dateOlderYYYYMMDD
     * @param bool $equal
     *
     * @return bool
     * @throws CException
     */
    public static function isGreaterThan( $dateYYYYMMDD, $dateOlderYYYYMMDD, $equal=false )
    {
        if( !self::validDateSQL($dateYYYYMMDD) ||  !self::validDateSQL($dateOlderYYYYMMDD) )
        {
            throw   new CException("ABA The dates are not valid: ".$dateYYYYMMDD. " , ".$dateOlderYYYYMMDD);
        }

        $mostRecentDate = strtotime( $dateYYYYMMDD );
        $olderDate = strtotime( $dateOlderYYYYMMDD );

        if( $equal && $mostRecentDate >= $olderDate )
        {
            return true;
        }
        elseif( $mostRecentDate > $olderDate )
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    /**
     * @static
     *
     * @param string $dateYYYYMMDD
     * @param string $dateOlderYYYYMMDD
     *
     * @return bool
     */
    public static function isSmallerThan( $dateYYYYMMDD, $dateOlderYYYYMMDD)
    {
        return self::isGreaterThan($dateOlderYYYYMMDD, $dateYYYYMMDD);
    }

    /** Returns true if $dateYYYYMMDD is today. Otherwise false.
     * @param $dateYYYYMMDD
     * @param bool $withTime
     *
     * @return bool
     */
    public static function isTodaySQL( $dateYYYYMMDD, $withTime=false)
    {
        $today = HeDate::todaySQL($withTime);

        if( $today==$dateYYYYMMDD ) {
            return true;
        }

        return false;
    }

    /**
     * @static
     *
     * @param string $dateYYYYMMDD
     *
     * @return string
     */
    public static function removeTimeFromSQL ($dateYYYYMMDD)
    {
        if(strlen($dateYYYYMMDD)>10){
            return substr($dateYYYYMMDD, 0, 10);
        }

        return $dateYYYYMMDD;
    }

    /** Return the time in $format requested. Usually in seconds.
     * Dates in format SQL DATE YYYY-MM-DD HH:NN:SS are expected
     *
     * @param $dateLast
     * @param $dateFirst
     * @param int $format Should be Constants HOURS, HeDate::MINUTES, etc.
     * @param bool $withTime
     *
     * @throws CException
     * @return float
     */
    public static function getDifferenceDateSQL( $dateLast, $dateFirst, $format = 1, $withTime = true)
    {
        if( !HeDate::validDateSQL( $dateLast, $withTime ) || !HeDate::validDateSQL( $dateFirst, $withTime )){
            throw new CException(" HeDate: dates SQL format are expected");
        }

        $timeFirst  = strtotime($dateFirst);
        $timeSecond = strtotime($dateLast);

        $differenceInSeconds = $timeSecond - $timeFirst;

        if($differenceInSeconds == 0 ){
            return 0.00;
        }
        // HOURS = 1, MINUTES=60, etc.
        $returnFormat = $differenceInSeconds / $format ;

        return $returnFormat;
    }

    static function now()
    {
        return date('Y-m-d H:i:s', time());
    }
    static function AddSeconds($seconds=60, $date=null)
    {
        if($date === null)
            $date=HeDate::now();
        return date('Y-m-d H:i:s', strtotime($date. " + $seconds seconds"));
    }
    static function today($full=false)
    {
        if($full)
            return date('Y-m-d 00:00:00', time());
        else
            return date('Y-m-d', time());
    }
    static function todayComplete($full=false)
    {
        if($full)
            return date('Y-m-d 23:59:59', time());
        else
            return date('Y-m-d', time());
    }
    static function tomorrow($full=false)
    {
        if($full)
            return date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"),date("d")+1,date("Y")));
        else
            return date("Y-m-d", mktime(0, 0, 0, date("m"),date("d")+1,date("Y")));
    }
    static function yesterday($full=false)
    {
        if($full)
            return date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"),date("d")-1,date("Y")));
        else
            return date("Y-m-d", mktime(0, 0, 0, date("m"),date("d")-1,date("Y")));
    }
    static function getFirstDayCurrentMonth($full=false)
    {
        if($full)
            return date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m"),1,date("Y")));
        else
            return date("Y-m-d", mktime(0, 0, 0, date("m"),1,date("Y")));
    }
    static function resetDate($full=false)
    {
        if($full)
            return "0000-00-00 00:00:00";
        else
            return "0000-00-00";
    }
    static function calculateMonth($month=-1,$full=false)
    {
        if($full)
            return date("Y-m-d 00:00:00", mktime(0, 0, 0, date("m")+($month),date("d"),date("Y")));
        else
            return date("Y-m-d", mktime(0, 0, 0, date("m")+($month),date("d"),date("Y")));
    }
    static function compareDateString($inDate1,$inDate2)
    {
        $firstDate = strtotime($inDate1);
        $secondDate = strtotime($inDate2);
        if ($secondDate > $firstDate)
            return 1;
        if ($secondDate == $firstDate)
            return 0;
        return -1;
    }

    /** Returns the day of the week, being Friday 5, Saturday 6 and Sunday 0, etc.
     * @param string $dateYYYYMMDD
     * @return bool|string
     */
    static function dayOfWeek($dateYYYYMMDD)
    {
        $dw = date( "w", strtotime($dateYYYYMMDD.' 09:00:00'));
        return $dw;

    }
}
