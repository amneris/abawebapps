<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 19/11/12
 * Time: 12:49
 * To group all external calls through SOAP, curl, stream_open, etc. It's a layer of abstraction and it will help to refactor
 * or fix some issues fast and easily.
 */
class HeComm
{

    /** Encodes values to be sent by PST or GET
     * @static
     *
     * @param $oldValue
     *
     * @return mixed|string
     */
    public static function encodeValuesHTTP($oldValue)
    {
        $newValue = str_replace("\"", "'", $oldValue);
        $newValue = urlencode($newValue);
        $newValue = str_replace("%2f", "/", $newValue);
        return $newValue;
    }


    public static function sendRawPostSocket( $abaPayPalWeb, $header, $req, $port=443 )
    {
        $resFile = "";

        $header .= "Content-Length: " . strlen($req) . "\r\n\r\n";

        if (intval($port)!==443) {
            $fp = fsockopen($abaPayPalWeb, intval($port), $errno, $errstr, 30);
        } else {
            $fp = fsockopen("ssl://".$abaPayPalWeb, intval($port), $errno, $errstr, 30);
        }


        if(!$fp || trim($errstr)!=='')
        {
            HeAbaMail::sendEmailInternalBug("La función fsockopen (requested URL= $abaPayPalWeb, 443, $errno, $errstr, 30) ha dado un error.");
            return false;
        } else {
            fputs($fp, $header . $req);
        }
        while (!feof($fp)) {
            $res = fgets($fp, 1024);
            $resFile .= $res;
            if (strcmp($res, "VERIFIED") == 0) {
                fclose($fp);
                return $res;
            } elseif (strcmp($res, "INVALID") == 0) {
                // log for manual investigation
                fclose($fp);
                return "INVALID";
            }
        }
        fclose($fp);
        return false;
    }

    /**
     * @static
     *
     * @param      $url
     * @param null $headers
     * @param null $data
     * @param null $extra
     *
     * @return string
     * @throws Exception
     */
    public static function sendHTTPPost($url, $headers = null, $data = null, $extra = null )
    {

        $params = array('http' => array(
            'method' => 'POST',
            'content' => $data
        ));

//        $header = implode("\r\n", array(
//          'Content-type: application/x-www-form-urlencoded',
//        ));

        if (HeMixed::isPhpUpperEquals(54)) {
            $params = array('http' => array(
                'method' => 'POST',
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
//                'header' => $header,
                'content' => $data
            ));
        }
        if ($headers !== null) {
            $params['http']['header'] = $headers;
        }
        $ctx = stream_context_create($params);

        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            // under Windows
            $fp = fopen($url, 'rb', false, $ctx);
        } else {

//            //#ABAWEBAPPS-XXX
//            try {
//                $iDefaultSocketTimeout = ini_get('default_socket_timeout');
//
//                if(is_numeric($iDefaultSocketTimeout)) {
//                    ini_set('default_socket_timeout', $iDefaultSocketTimeout * 3);
//                }
//            }
//            catch (Exception $e) { }

            // under Linux
            $fp = @fopen($url, 'rb', false, $ctx);
        }

        if (!$fp) {
            throw new Exception("Problem with $url");
        }

        $response = @stream_get_contents($fp);

        if ($response === false) {
            throw new Exception("Problem reading data from ".$url );
        }

        return $response;
    }

    /**
     * @static
     *
     * @param string $url
     * @param array $headers
     * @param array $aData
     *
     * @return array|mixed
     */
    public static function sendHTTPPostCurl($url, $headers = null, $aData = null)
    {
        $ch = curl_init();
        //Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        if (!is_null($aData)) {
            curl_setopt($ch, CURLOPT_POST, count($aData));
            $fields_string = "";
            foreach ($aData as $key=>$value) {
                $fields_string .= $key.'='.urlencode($value).'&';
            }
            $fields_string = rtrim($fields_string, '&');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if (!is_null($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        $result = curl_exec($ch);
        if (!$result) {
            return array("status"=>false, "description"=>curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

    /** Used for AllPago gateway for payments
     *
     * @param $host
     * @param $path
     * @param $data
     * @param $userAgent
     *
     * @return array
     */
    public static function sendHTTPPostCurlRetAll($host, $path, $data, $userAgent)
    {
        $cpt = curl_init();
        $xmlpost="load=".urlencode($data);
        curl_setopt($cpt, CURLOPT_URL, "$host$path");
        //#PHP5.5
        $verifyHost = 1;
        if (HeMixed::isPhpUpperEquals(54)) {
            $verifyHost = 2;
        }
        curl_setopt($cpt, CURLOPT_SSL_VERIFYHOST, $verifyHost);
        curl_setopt($cpt, CURLOPT_USERAGENT, $userAgent);
        curl_setopt($cpt, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($cpt, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($cpt, CURLOPT_POST, 1);
        curl_setopt($cpt, CURLOPT_POSTFIELDS, $xmlpost);
        curl_setopt($cpt, CURLOPT_TIMEOUT, 40);
        $resultURL = curl_exec($cpt);
        $error = curl_error($cpt);
        $info = curl_getinfo($cpt);
        curl_close($cpt);
        return array("resultUrl"=>$resultURL,
                     "error"=>$error,
                     "info"=>$info);
    }

    /**
     * @param $host
     * @param $path
     * @param $data
     * @param $userAgent
     * @return array
     */
    public static function sendHTTPPostCurlRetAllQuery($host, $path, $data, $userAgent)
    {
        $cpt = curl_init();
        $xmlpost="load=".urlencode($data);
        curl_setopt($cpt, CURLOPT_URL, "$host$path");
        curl_setopt($cpt, CURLINFO_HEADER_OUT, true);
        curl_setopt($cpt, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/x-www-form-urlencoded;charset=UTF-8"
        ));
        //#PHP5.5
        $verifyHost = 1;
        if (HeMixed::isPhpUpperEquals(54)) {
            $verifyHost = 2;
        }
        curl_setopt($cpt, CURLOPT_SSL_VERIFYHOST, $verifyHost);
        curl_setopt($cpt, CURLOPT_USERAGENT, $userAgent);
        curl_setopt($cpt, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($cpt, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($cpt, CURLOPT_POST, 1);
        curl_setopt($cpt, CURLOPT_POSTFIELDS, $xmlpost);
        curl_setopt($cpt, CURLOPT_TIMEOUT, 20);
        $resultURL = curl_exec($cpt);
        $error = curl_error($cpt);
        $info = curl_getinfo($cpt);
        curl_close($cpt);
        return array("resultUrl"=>$resultURL,
            "error"=>$error,
            "info"=>$info);
    }

    /**
     * @param $url
     * @param array $aData
     *
     * @return array|mixed
     */
    public static function sendHTTPPostCurlSimpleAdyen($url, $aData = array())
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, count($aData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($aData));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        if ($result === false) {
            return false;
        }
        curl_close($ch);
        return $result;
    }

    /**
     * @param $url
     * @param array $headers
     * @param array $aData
     * @param bool $jSonEncode
     *
     * @return array|mixed
     */
    public static function sendHTTPPostCurlSimple($url, $headers = array(), $aData = array(), $jSonEncode = false)
    {
        $curlHandle = curl_init($url);
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        if (count($aData) > 0) {
            curl_setopt($curlHandle, CURLOPT_POST, count($aData));
            $fieldsString = "";
            if ($jSonEncode) {
                $fieldsString = json_encode($aData);
            } else {
                foreach ($aData as $key => $value) {
                    $fieldsString .= $key.'='.urlencode($value).'&';
                }
                rtrim($fieldsString, '&');
            }
            curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $fieldsString);
        }
        if (count($headers) > 0) {
            curl_setopt($curlHandle, CURLOPT_HTTPHEADER, $headers);
        }
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, 15);
        $output   = curl_exec($curlHandle);
        $httpCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
        if (!$output) {
            return array("status" => false, "description" => curl_error($curlHandle));
        }
        curl_close($curlHandle);
        if (200 != $httpCode) {
            die("Error validating App Store transaction receipt. Response HTTP code $httpCode");
        }
        return $output;
    }

    /**
     * @param string $sUrl
     * @param array $headers
     * @param string $sUserpwd
     * @param string $sUrlCsv
     *
     * @return array|mixed
     */
    public static function sendHTTPCurlSimpleFile($sUrl = "", $headers = array(), $sUserpwd = "", $sUrlCsv = "")
    {
        $fileOpen = fopen($sUrlCsv, 'w+');
        $curlHandle = curl_init();
        //#PHP5.5
        $verifyHost = 2;
        curl_setopt($curlHandle, CURLOPT_URL, $sUrl);
        curl_setopt($curlHandle, CURLOPT_USERPWD, $sUserpwd);
        curl_setopt($curlHandle, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, false);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, $verifyHost);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, false);
        if (count($headers) > 0) {
            curl_setopt($curlHandle, CURLOPT_HTTPHEADER, $headers);
        }
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, 15);
        curl_setopt($curlHandle, CURLOPT_FILE, $fileOpen);
        $output = curl_exec($curlHandle);
        if (!$output) {
            return array("output" => $output, "status" => false, "description" => curl_error($curlHandle));
        }
        curl_close($curlHandle);
        fclose($fileOpen);
        return array("output" => $output, "status" => true, "description" => $sUrlCsv);
    }

    /**
     * @param $url
     * @param $action
     * @param array $configData
     * @param array $sendData
     *
     * @return null
     */
    public static function sendSoapRequest($url, $action, $configData = array(), $sendData = array())
    {
        ini_set("soap.wsdl_cache_enabled", 0);
        $result = null;
        $client = new SoapClient($url, $configData);
        try {
            $result = $client->$action($sendData);
        } catch (SoapFault $exc) {
            return array('abaExceptions' => array('abaExcMessage' => $exc->getMessage(),
                'abaExcCode' => $exc->getCode()));
        }
        return $result;
    }

    /**
     * @param $type
     * @param array $stParams
     *
     * @return string
     */
    public static function getMobileRedirectUrl($type, $stParams = array())
    {
        $sUrlMobile = '';
        switch ($type) {
            case 'ANDROID_PAYMENT_CONFIRMATION':
                $sUrlMobile = "abavideoclass://freeToPremium?purchaseID=" . $stParams["paymentId"] .
                  "@price=" . HeMixed::getRoundAmount($stParams["paymentAmountPrice"]) .
                  "@transactionID=" . $stParams["paymentProductiD"] .
                  "@currency=" . $stParams["paymentCurrencyTrans"];
                break;
            case 'ANDROID_DEFAULT_REDIRECT':
                $sUrlMobile = "abavideoclass://openApp";
                break;
            case 'IOS_REDIRECT':
                $sUrlMobile = "campusapp://";
                break;
        }
        return $sUrlMobile;
    }
}
