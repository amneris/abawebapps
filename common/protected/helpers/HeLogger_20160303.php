<?php
/**
 * Created by PhpStorm.
 * User: earandes
 * Date: 22/10/15
 * Time: 12:44
 */

//require_once ROOT_DIR . '/vendor/autoload.php';
//
//use Monolog\Logger;
//use Monolog\Handler\LogglyHandler;
//use Monolog\Formatter\LogglyFormatter;
//use Monolog\Handler\FirePHPHandler;

// class HeLogger extends Logger
class HeLogger
{
    const IT_BUGS = "itbugs";
    const PAYMENTS = "payments";
    const PAYMENTS_MOBILE = "payments_mobile";
    const SELLIGENT = "selligent";
    const FINANCE = "finance";
    const SUPPORT = "support";
    const RECONCILIATION = "reconciliation";
    const USERACTIVITY = "user_activity";
    const IT_BUGS_PAYMENTS = "ibugs";

    const PREFIX_ERR_UNKNOWN_L = "CAMPUS ERROR UNKNOWN(LOW)";
    const PREFIX_ERR_UNKNOWN_H = "CAMPUS ERROR UNKNOWN(HIGH)";
    const PREFIX_ERR_LOGIC_H = "CAMPUS INCONSISTENCY (HIGH)";
    const PREFIX_ERR_LOGIC_L = "CAMPUS INCONSISTENCY (LOW)";
    const PREFIX_NOTIF_L = "CAMPUS NOTIFICATION (LOW)";
    const PREFIX_NOTIF_H = "CAMPUS NOTIFICATION (HIGH)";
    const PREFIX_TESTS = "CAMPUS LOCAL TESTS SELENIUM (LOW)";
    const PREFIX_APP_LOGIC_L = "APP INCONSISTENCY (LOW)";

//    // profile
//    const DEBUG = 100;      // ?!?!? info ?!?!?
//    const INFO = 200;       // info
//    const NOTICE = 250;     // ?!?!? info ?!?!?
//    const WARNING = 300;    // warning
//    const ERROR = 400;      // error
//    const CRITICAL = 500;   // ?!?!? error ?!?!?
//    const ALERT = 550;      // ?!?!? info ?!?!?
//    const EMERGENCY = 600;  // ?!?!? error ?!?!?

    // profile
    const DEBUG = 'trace';       // ?!?!? info ?!?!?
    const INFO = 'info';        // info
    const NOTICE = 'profile';      // ?!?!? info ?!?!?
    const WARNING = 'warning';  // warning
    const ERROR = 'error';      // error
    const CRITICAL = 'error';   // ?!?!? error ?!?!?
    const ALERT = 'info';       // ?!?!? info ?!?!?
    const EMERGENCY = 'error';  // ?!?!? error ?!?!?

    const LOGGER_TYPE_NOLOGGER = 0;
    const LOGGER_TYPE_SYSLOG = 1;
    const LOGGER_TYPE_EMAIL = 2;
    const LOGGER_TYPE_LOGGLY = 3;

    public static function sendLog($channel, $tag, $type, $body)
    {

        $iLoggerEnabledMode = Yii::app()->config->get("LOGGER_ENABLED_MODE");

        try {

            switch($iLoggerEnabledMode) {
                case self::LOGGER_TYPE_SYSLOG:
//                    Yii::log($body . " ", CLogger::LEVEL_ERROR, 'itbugs');
                    Yii::log($channel . "::" . $body . " ", $type, $tag);
                    break;

                case self::LOGGER_TYPE_EMAIL:

//                    const PAYMENTS = "payments";                      EMAIL_DPT_PAYMENT               payments@abaenglish.com
//                    const PAYMENTS_MOBILE = "payments_mobile";        EMAIL_DPT_PAYMENT_MOBILE        itbugs@abaenglish.com; akorotkov@abaenglish.com; ascandroli@abaenglish.com
//                    const SELLIGENT = "selligent";                    EMAIL_DPT_SELLIGENT             ipetrov@abaenglish.com;iruiz@abaenglish.com
//                    const FINANCE = "finance";                        EMAIL_FINANCE                   dbande@abaenglish.com;jsanz@abaenglish.com;jlambea@abaenglish.com
//                    const SUPPORT = "support";                        EMAIL_DPT_SUPPORT               support@abaenglish.com
//                    const RECONCILIATION = "reconciliation";
//                    const USERACTIVITY = "user_activity";
//                    const IT_BUGS_PAYMENTS = "ibugs";                 EMAIL_IT_BUGS_PAYMENTS          itbugs@abaenglish.com
//                    EMAIL_IT_BUGS_PAYMENTS        itbugs@abaenglish.com
//                    EMAIL_IT_BUGS_FINANCE         itbugs@abaenglish.com
//                    EMAIL_IT_BUGS                 itbugs@abaenglish.com

                    switch($tag) {
                        case self::IT_BUGS:
                            HeAbaMail::sendEmailInternal(
                              Yii::app()->config->get("EMAIL_IT_BUGS_PAYMENTS"), "",
                              $channel, $body, true, null, HeAbaMail::BUGS_LAYOUT ); // INFOPROC_LAYOUT
                            break;
                        case self::PAYMENTS:
                            HeAbaMail::sendEmailInternal(
                              Yii::app()->config->get("EMAIL_DPT_PAYMENT"), "",
                              $channel, $body, true, null);
                            break;
                        case self::PAYMENTS_MOBILE:
                            HeAbaMail::sendEmailInternal(
                              Yii::app()->config->get("EMAIL_DPT_PAYMENT_MOBILE"), "",
                              $channel, $body, true, null);
                            break;
                        case self::SELLIGENT:
                            HeAbaMail::sendEmailInternal(
                              Yii::app()->config->get("EMAIL_DPT_SELLIGENT"), "",
                              $channel, $body, true, null);
                            break;
                        case self::FINANCE:
                            HeAbaMail::sendEmailInternal(
                              Yii::app()->config->get("EMAIL_FINANCE"), "",
                              $channel, $body, true, null);
                            break;
                        case self::SUPPORT:
                            HeAbaMail::sendEmailInternal(
                              Yii::app()->config->get("EMAIL_DPT_SUPPORT"), "",
                              $channel, $body, true, null);
                            break;
                        case self::IT_BUGS_PAYMENTS:
                            HeAbaMail::sendEmailInternal(
                                Yii::app()->config->get("EMAIL_IT_BUGS_PAYMENTS"), Yii::app()->config->get("EMAIL_DPT_PAYMENT"),
                                $channel, $body, true, null, HeAbaMail::BUGS_LAYOUT );
                            break;
                        case self::RECONCILIATION:
                        case self::USERACTIVITY:
                        default:
                            HeAbaMail::sendEmailInternal(
                              Yii::app()->config->get("EMAIL_IT_BUGS_PAYMENTS"), "",
                              $channel, $body, true, null, HeAbaMail::BUGS_LAYOUT ); // INFOPROC_LAYOUT
                            break;
                    }

                    break;

                case self::LOGGER_TYPE_LOGGLY:
//                    $log = new Logger($channel);
//                    $log->pushHandler(new LogglyHandler('6ab16bc9-0d27-496a-a96a-f885708e8252/tag/' . $tag, $type));
//
//                    if (is_array($body)) {
//                        return $log->addRecord($type, '', $body);
//                    } else {
//                        return $log->addRecord($type, $body);
//                    }
                    break;

                case self::LOGGER_TYPE_NOLOGGER:
                default:
                    break;

            }

        } catch (Exeption $e) { }

        return false;
    }

}