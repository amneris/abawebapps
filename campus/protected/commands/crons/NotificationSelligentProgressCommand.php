<?php

/**
 * In case of any failure of synchronization between Campus and Selligent
 *  this script can be used to force the creation of a group of users in SELLIGENT.
 * Created by Quino
 * User: Quino
 * Date: 08/03/2013
 * Time: 12:11
 */
class NotificationSelligentProgressCommand extends AbaConsoleCommand
{
    protected $aListUsersErrors = array();
    protected $aListUsersSuccess = array();

    public function getHelp()
    {
        return parent::getHelp() . " First argument: You can set a number to set how many records you want " .
        "to send to SELLIGENT. \n " .
        " Second argument: Optional, days in advance for warning. \n " .
        " Otherwise 200 users and 15 days in advance will be used.";
    }


    public function run($args)
    {
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n START. NotificationSelligentProgressCommand: executing auto process at ".date("Y-m-d h:i:s", time());
        $limitUsers = 200;
        $allSuccess = true;
        $daysInAdvance = 15;

        if (count($args) >= 1) {
            if (!is_numeric($args[0])) {
                echo " The first argument should be an integer telling how many records " .
                    "we want to process for every iteration.";
                return false;
            }
            $limitUsers = intval($args[0]);
        }

        if (count($args) == 2) {
            if (is_numeric($args[1])) {
                $daysInAdvance = $args[1];
            } else {
                echo " The argument should be a valid integer.";
                return false;
            }
        }

        $checkAlreadySent = false;
        if (count($args) == 3) {
            $checkAlreadySent = intval($args[2]);
        }

        $colPayments = new Payments();
        $dateNext = HeDate::getDateAdd(HeDate::todaySQL(false), $daysInAdvance, 0);
        echo " \n Collecting payments PENDING on $dateNext .";
        /* @var Payment[] $aNextPayments */
        $aNextPayments = $colPayments->getAllPaymentsPendingOnDate($dateNext, $limitUsers, true);
        if (!$aNextPayments) {
            $this->processError("No payments PENDING on the next " . $dateNext);
            return true;
        }
        /* @var Payment $moPayment */
        foreach ($aNextPayments as $moPayment) {
            $moUser = new AbaUser();
            if (!$moUser->getUserById($moPayment->userId)) {
                $this->aListUsersErrors[] = array("userId" => $moPayment->userId, "email" => "", "idPayment" => $moPayment->id,
                    "dateToPay" => $moPayment->dateToPay, "unitsCompleted" => false,
                    "desc" => "User not found in database.");
                $allSuccess = false;
                continue;
            }

            if ($checkAlreadySent) {
                $logSelligent = new LogSentSelligent();
                if ($logSelligent->getLogByUserId($moUser->id, "infoUnitsCompleted")) {
                    $this->aListUsersSuccess[] = array("userId" => $moPayment->userId, "email" => "",
                        "idPayment" => $moPayment->id, "dateToPay" => $moPayment->dateToPay, "cardYearMonth" => false,
                        "desc" => "NOT SENT, because it was already sent on " . $logSelligent->dateSent);
                    continue;
                }
            }

            $moProgress = new AbaFollowup();
            $unitsCompleted = $moProgress->getUnitsCompletedOnLevel($moUser->id, $moUser->currentLevel);

            $commSelligent = new SelligentConnectorCommon();
            if (!$commSelligent->sendOperationToSelligent(SelligentConnectorCommon::infoUnitsCompleted, $moUser,
                array("NUM_UNITS" => intval($unitsCompleted)), "NotificationSelligentProgress")
            ) {

                $this->aListUsersErrors[] = array("userId" => $moPayment->userId, "email" => $moUser->email,
                    "idPayment" => $moPayment->id, "dateToPay" => $moPayment->dateToPay,
                    "unitsCompleted" => $unitsCompleted,
                    "desc" => "Selligent not updated error: " . $commSelligent->getErrMessage());
                $allSuccess = false;
                continue;
            }

            $this->aListUsersSuccess[] = array("userId" => $moPayment->userId, "email" => $moUser->email,
                "idPayment" => $moPayment->id, "dateToPay" => $moPayment->dateToPay,
                "unitsCompleted" => $unitsCompleted,
                "desc" => "Selligent response: " . $commSelligent->getResponse());
        }

        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n ";

        if (!$allSuccess) {
            $this->sendSummaryError( "The process NotificationSelligentProgress has finished but with some errors, ".
                " users that will pay on $dateNext  ");
            echo " \n " . date("Y-m-d h:i:s", time()) . " Some errors in process NotificationSelligentProgressCommand. END";
            echo " \n -----------------------------------------------------------------------------------------------";
            return true;
        }

        $this->sendSummarySuccess( "All users that will pay on $dateNext have been informed to Selligent" );
        echo " \n " . date("Y-m-d h:i:s", time()) . " Success in process NotificationSelligentProgressCommand. END";
        echo " \n -----------------------------------------------------------------------------------------------";
        return $allSuccess;

    }

    /** Send general error in the process.
     *
     * @param string $actionDetails
     *
     * @return bool
     */
    private function processError($actionDetails)
    {
        HeLogger::sendLog("CRON NotificationSelligentProgressCommand finished with errors ", HeLogger::IT_BUGS,
          HeLogger::CRITICAL," Error processing NotificationSelligentProgressCommand: " . $actionDetails);

        return true;
    }

    /** Sends the e-mail with errors summary.
     *
     * @param $actionDetails
     */
    private function sendSummaryError($actionDetails)
    {
        $bodyDesc = $actionDetails;
        $aSummAll = array_merge($this->aListUsersErrors, $this->aListUsersSuccess);

        $bodyDesc .= " == " . json_encode($aSummAll);

        HeLogger::sendLog(HeLogger::PREFIX_ERR_LOGIC_L . ": NotificationSelligentProgress " . HeDate::todaySQL(true) .
          " The lines of users below were processed with errors, please review them: ",
          HeLogger::SELLIGENT, HeLogger::CRITICAL, $bodyDesc);
    }

    /**
     * @param $actionDetails
     */
    private function sendSummarySuccess($actionDetails)
    {
        $bodyDesc = $actionDetails;
        $aSummAll = $this->aListUsersSuccess;

        $bodyDesc .= " == " . json_encode($aSummAll);

        HeLogger::sendLog(HeLogger::PREFIX_NOTIF_L . ": NOTIFICATIONSELLIGENTPROGRESS " . HeDate::todaySQL(true) .
          " The lines of users below were processed SUCCESSFULLY. You do not have to do any shit: ",
          HeLogger::SELLIGENT, HeLogger::CRITICAL, $bodyDesc);
    }

}
