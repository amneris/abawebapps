<?php

class FamilyPlanNotificationCommand extends AbaConsoleCommand
{
    public function run($args)
    {
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n FamilyPlanNotificationCommand: executing auto process at " . date("Y-m-d H:i:s", time());

        $allSuccess = true;

        if (RenewalsPlans::isValidNotification()) {

            $processExtraDays = Yii::app()->config->get("PLAN_FAMILY_RENEWAL_DAYS_PLUS");
            $dateNow = HeDate::todaySQL(false);
            $renewalDate = HeDate::getDateAdd($dateNow, $processExtraDays);

            $moPayments = new Payments();

            $allRenewalPayments = $moPayments->getAllPaymentsPendingAllSuppliers($renewalDate);

            echo " \n +++ Total renewals: " . count($allRenewalPayments) . " +++";
            echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";

            if (count($allRenewalPayments) == 0) {
                $actionDetails = "No data.";

                echo "\n +++ " . $actionDetails . " +++ \n";

                HeLogger::sendLog("CRON FamilyPlanNotificationCommand finished ",HeLogger::PAYMENTS, HeLogger::CRITICAL,
                  " FamilyPlanNotificationCommand: " . $actionDetails);

            }

            foreach ($allRenewalPayments as $stRenewalPayment) {

                $moUser = new AbaUser();
                $moUser->getUserById($stRenewalPayment['userId']);

                $stWsData = array(
                    'IDUSER_WEB' => $moUser->id,
                    'PREF_LANG' => $moUser->langEnv,
                    'COUNTRY_IP' => $moUser->countryId,
                    'PRICE' => $stRenewalPayment['amountPrice'],
                    'CURRENCY' => $stRenewalPayment['currencyTrans'],
                    'PRODUCT' => $stRenewalPayment['idPeriodPay'],
                    'RENOV_DT' => $stRenewalPayment['dateToPay'],
                );

                $oRenewalsPlans = new RenewalsPlans();

                $renewalData = array(
                    "idPayment" => $stRenewalPayment["id"],
                    "userId" => $moUser->id,
                    "langEnv" => $moUser->langEnv,
                    "amountPrice" => $stRenewalPayment['amountPrice'],
                    "currencyTrans" => $stRenewalPayment['currencyTrans'],
                    "idPeriodPay" => $stRenewalPayment['idPeriodPay'],
                    "dateToPay" => $stRenewalPayment['dateToPay'],
                    "origin" => RenewalsPlans::RENEWALS_ORIGIN_FAMILY_PLAN,
                    "status" => RenewalsPlans::RENEWALS_STATUS_NOTIFIED,
                );
                $oRenewalsPlans->_fillDataColsToProperties($renewalData);

                if (!$oRenewalsPlans->insertRenewal()) {

                    $actionDetails = "Insert to renewals_plans table failed. UserId=" . $moUser->id . "; PaymentId=" . $stRenewalPayment["id"];

                    echo "\n +++ " . $actionDetails . " +++ \n";

                    HeLogger::sendLog("CRON FamilyPlanNotificationCommand finished with errors ",HeLogger::PAYMENTS,
                      HeLogger::CRITICAL, " Error processing FamilyPlanNotificationCommand: " . $actionDetails);

                } else {
                    echo "\n+ + + + + OK + + + + + UserId=" . $moUser->id . "; PaymentId=" . $stRenewalPayment["id"] . " + + + + +\n";
                }

                $commConnSelligent = new SelligentConnectorCommon();
                if (!$commConnSelligent->sendOperationToSelligent(
                    SelligentConnectorCommon::renewalsPlanNotification,
                    $moUser,
                    $stWsData,
                    "FamilyPlanNotificationCommand"
                )
                ) {

                    $actionDetails = "Connection to Selligent Server failed. Probably Selligent still is down. UserId=" . $moUser->id . "; PaymentId=" . $stRenewalPayment["id"];

                    echo "\n +++ " . $actionDetails . " +++ \n";


                    HeLogger::sendLog("CRON FamilyPlanNotificationCommand finished with errors ", HeLogger::PAYMENTS,
                      HeLogger::CRITICAL, " Error processing FamilyPlanNotificationCommand: " . $actionDetails);

                    continue;
                }
            }
        } else {

            HeLogger::sendLog("CRON NO DATA PROCESSED FOR THIS DATE",HeLogger::PAYMENTS,
              HeLogger::CRITICAL,"CRON NO DATA PROCESSED FOR THIS DATES");

            echo "\n+ + + + + CRON NO DATA PROCESSE FOR THIS DATE + + + + +\n";

            $allSuccess = false;
        }

        echo " \n  Finished process FamilyPlanNotificationCommand at " . date('Y-M-D H:m:s');
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
        return $allSuccess;
    }

}
