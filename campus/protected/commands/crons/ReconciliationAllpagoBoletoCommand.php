<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 09/12/2014
 * Time: 12:11
 */
class ReconciliationAllpagoBoletoCommand extends AbaConsoleCommand
{
    // It checks for Boleto receipts in order to convert users to Premium
    protected $idPaySupplier = PAY_SUPPLIER_ALLPAGO_BR_BOL;

    public function getHelp()
    {
        return parent::getHelp() . " Downloads XML from All pago for BOLETO Brazil requests RC.\n " .
        " As an argument a valid date is accepted. Format required is YYYY-MM-DD.";
    }

    public function run($args)
    {
        $allSuccess = true;
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n " . $this->getName() . ": executing auto process at " . date("Y-m-d H:i:s", time());

        $dateCheckPrevious = "";
        $today = HeDate::todaySQL(false);
        $dateRequest = HeDate::getDateAdd($today, -1 /*day*/);
        if (count($args) > 0) {
            if (!HeDate::validDateSQL($args[0])) {
                echo 'Date is not valid. Please respect format YYYY-MM-DD';
                return false;
            }
            $dateRequest = $args[0];
        }

        /* ------------------For $dateRequest starts process of reconciliation -------------------------- */
        $dateRequestFrom = $dateRequest . ' 00:00:00';
        $dateRequestUntil = $dateRequest . ' 23:59:59';
        /* @var PaySupplierAllPagoBoleto $paySupplier */
        $paySupplier = PaymentFactory::createPayment(null, $this->idPaySupplier);
        $moLogAllPago = $paySupplier->runRequestReconciliation($dateRequestFrom, $dateRequestUntil);
        /* @var $fileXml SimpleXMLElement */
        $fileXml = $moLogAllPago->getXmlResponse();
        $fileParserXML = new FileParserAllPago($this->idPaySupplier);
        if (!$fileParserXML->saveFile($fileXml)) {
            $this->processEndsWithError("File XML was requested and obtained, but could not be saved to disk.");
            return false;
        }
        if (!$fileParserXML->validateFile()) {
            $this->processEndsWithError(
                "File XML was requested SAVED to disk,but apparently it's not a valid XML file."
            );
            return false;
        }

        /* @var LogReconciliation[] $aMoLogsReconc */
        $aMoLogsReconc = $fileParserXML->parseToModels();
        if (count($aMoLogsReconc) == 0) {
            HeLogger::sendLog(HeLogger::PREFIX_NOTIF_H . "CRON Reconciliation AllPago Boleto executed and finished on day " .
              date("Y-m-d H:i:s", time()) . "", HeLogger::IT_BUGS, HeLogger::INFO,
              "CRON Reconciliation AllPago Boletos Brazil executed and finished on day " . date("Y-m-d H:i:s", time()) .
              " There was no transactions executed on BIP platform during day " . $dateRequest);
            echo " \n Success and end of process execution -------------------";
            return true;
        }

        /* -------------If there are any Boletos RC (receipts) transactions available in XML then we
                                        first update status for Boletos ------------------- */
        $commMatching = new ReconcMatchingAllPagoBoletoBr($this->idPaySupplier);
        $aMatchResponse = $commMatching->matchBoletosOnly($aMoLogsReconc, $fileParserXML->getFilePath());

        /* @var LogReconciliation[] $aMoLogsSuccess */
        $aMoBoletos = $aMatchResponse['aMoBoletos'];
        $aMoLogsSuccess = $aMatchResponse['aMoLogsSuccess'];
        $aMoLogsErrors = $aMatchResponse['aMoLogsErrors'];
        $allSuccess = $aMatchResponse['allSuccess'];

        if (!$allSuccess) {
            foreach ($aMoLogsErrors as $moLogError) {
                // @TODO: We should take an extra operation for every Boleto not matched
                // 1- Create Payment SUCCESS for every Boleto (PP.RC) not found. ?????
            }
        }

        $allSuccess = true;
        /* @var AbaPaymentsBoleto $boleto */
        foreach ($aMoBoletos as $refPaySuppOrderId => $boleto) {
            $moLogPayAttempt = new PayGatewayLogAllpago();
            $moLogPayAttempt->getByIdPayment($boleto->idPayment);
            if ($aMoLogsSuccess[$refPaySuppOrderId]->refPaySuppExtUniId != $moLogPayAttempt->getUniqueId()) {
                // Something does not match, inconsistency on logs from reconciliation and logs from
                // payments attempts
                $errMsgBoletoConversion = 'Payment attempt unique Id does not match the log of payment.';
                $this->processBoletoSingleError($boleto, $errMsgBoletoConversion);
                $allSuccess = false;
            }

            $moPayCtrlCheck = new PaymentControlCheck();
            if (!$moPayCtrlCheck->getPaymentById($boleto->idPayment)) {
                $errMsgBoletoConversion = ' Boleto found and updated to SUCCESS ' .
                  ' but the PAYMENT draft(payment control check id ' . $boleto->idPayment . ') was not found.';
                $this->processBoletoSingleError($boleto, $errMsgBoletoConversion);
                $allSuccess = false;
                continue;
            } elseif ($moPayCtrlCheck->status == PAY_SUCCESS) {
                // @TODO: Let´s see what we do with this scenario.
                //$errMsgBoletoConversion = 'Payment attempt already SUCCESS, maybe same Boleto re-process, duplicated?';
                //$this->processBoletoSingleError($boleto, $errMsgBoletoConversion);
                //$allSuccess = false;
                continue;
            }

            $moPayment = new Payment();
            if ($moPayment->getPaymentById($boleto->idPayment)) {
                $errMsgBoletoConversion = ' Boleto found and updated to SUCCESS ' .
                  ' but the payment draft(' . $boleto->idPayment . ') was already converted to SUCCESS OR WAS NOT FOUND.' .
                  'Payment real and with status =' . $moPayment->status . ' already in database. REVIEW IT ALL!';
                $this->processBoletoSingleError($boleto, $errMsgBoletoConversion);
                $allSuccess = false;
                continue;
            }
            /* ----------------   Payments are not existent, so we proceed to convert to SUCCESS and UPDATE
             TO PREMIUM --------- */
            $moUser = new AbaUser();
            $moUser->getUserById($moPayCtrlCheck->userId);

            if (!is_numeric($moUser->id)) {

                $errMsgBoletoConversion = 'EMPRTY USER.';
                $this->processBoletoSingleError($boleto, $errMsgBoletoConversion);
                $allSuccess = false;

                continue;
            }

            $foPayForm = new PaymentForm();
            $foPayForm->idPayMethod = PAY_METHOD_BOLETO;
            $foPayForm->mail = $moUser->email;
            $foPayForm->typeCpf = $moPayCtrlCheck->typeCpf;
            $foPayForm->cpfBrasil = $moPayCtrlCheck->cpfBrasil;
            $foPayForm->creditCardType = KIND_CC_BOLETO;
            $foPayForm->creditCardName = $moPayCtrlCheck->cardName;

            //
            // boleto.name.development
            if (trim($moPayCtrlCheck->cardName) != '') {
                $foPayForm->firstName = $moPayCtrlCheck->cardName;
            } else {
                $foPayForm->firstName = $moUser->name;
            }
            $foPayForm->secondName = $moUser->surnames;
            //

            $commOnlPay = new OnlinePayCommon();
            $paySuccess = $commOnlPay->savePaymentCtrlCheckToUserPayment(
                $foPayForm,
                $moPayCtrlCheck,
                $moLogPayAttempt,
                $moPayCtrlCheck->idPartner,
                0
            );
            if (!$paySuccess) {
                $errMsgBoletoConversion = 'Transformation from payment control check to real payment and save ' .
                  'credit card failed. Please review all data related with this boleto.';
                $this->processBoletoSingleError($boleto, $errMsgBoletoConversion);
                $allSuccess = false;
                continue;
            }

            $commUser = new UserCommon();
            $moPeriodicity = new ProdPeriodicity();
            $moPeriodicity->getProdPeriodById($moPayCtrlCheck->idPeriodPay);
            $expirationDate = $commUser->getNextExpirationDate($moUser, $moPeriodicity);
            $moUser = $commUser->convertToPremiumCron($moUser, $moPayCtrlCheck->idPartner, $expirationDate, PREMIUM);
            if (!$moUser) {
                $errMsgBoletoConversion = 'Conversion to Premium with this Boleto failed.';
                $this->processBoletoSingleError($boleto, $errMsgBoletoConversion);
                $allSuccess = false;
                continue;
            }

            //#5635
            $stPaymentInfo = $paySuccess->getPaymentInfoForSelligent();

            $commConnSelligent = new SelligentConnectorCommon();
            $commConnSelligent->sendOperationToSelligent(
                SelligentConnectorCommon::pagos,
                $moUser,
                array(
                    "PROMOCODE" => $paySuccess->idPromoCode,
                    "ISEXTEND" => $paySuccess->isExtend,

                    "MONTH_PERIOD" =>   $stPaymentInfo['MONTH_PERIOD'],
                    "PAY_DATE_START" => $stPaymentInfo['PAY_DATE_START'],
                    "PAY_DATE_END" =>   $stPaymentInfo['PAY_DATE_END'],
                    "AMOUNT_PRICE" =>   $stPaymentInfo['AMOUNT_PRICE'],
                    "CURRENCY" =>       $stPaymentInfo['CURRENCY'],
                    "PRICE_CURR" =>     $stPaymentInfo['PRICE_CURR'],
                    "TRANSID" =>        $stPaymentInfo['TRANSID'],
                ),
                "ReconciliationAllPAgoBoleto"
            );

            //#5087
            $commProducts = new ProductsPricesCommon();
            if ($commProducts->isPlanProduct($paySuccess->idProduct)) {
                $commConnSelligent = new SelligentConnectorCommon();
                // IDUSER_WEB, PRODUCT_TYPE
                $commConnSelligent->sendOperationToSelligent(
                    SelligentConnectorCommon::pagosPlan,
                    $moUser,
                    array(),
                    "ReconciliationAllPAgoBoleto"
                );
            } else {
                $commConnSelligent = new SelligentConnectorCommon();
                $commConnSelligent->sendOperationToSelligent(
                    SelligentConnectorCommon::pagosPlanNotification,
                    $moUser,
                    array(),
                    "ReconciliationAllPAgoBoleto"
                );
            }

            $moPayCtrlCheck->updateStatusAfterPayment(
                PAY_SUCCESS,
                $this->idPaySupplier,
                $aMoLogsSuccess[$refPaySuppOrderId]->refPaySuppOrderId,
              HeDate::today(true) . '-Offline payment through Boleto confirmed'
            );
        }

        // ----------------    After this, it reconciles payments as usual    ------------------------
        $allSuccess2 = $commMatching->processPackTransactions($aMoLogsReconc, $fileParserXML->getFilePath());
        $allSuccess = $allSuccess2 && $allSuccess;

        //
        //#5408
        // ----------------     Now we convert to Off all Boletos that have expired ------------------
        // $comBoleto = new BoletoCommon();
        // $comBoleto->processOff(HeDate::now());
        //

        if (!$allSuccess) {
            $this->processEndsWithError(
                "Not all nodes from XML file were successfully matched. " .
                "Please check for summary email review sent to see which ones."
            );
            echo " \n \n   executed at " . date('Y-M-D H:m:s') . " with ERRORS for some transactions. ";
        }

        echo " \n  Finished process at " . date('Y-M-D H:m:s');
        echo " \n -----------------------------------------------------------------------------------------------";
        return $allSuccess;
    }

    /**
     * @param $actionDetails
     * @return bool
     */
    protected function processEndsWithError($actionDetails)
    {
        HeLogger::sendLog(
          HeLogger::PREFIX_ERR_UNKNOWN_H .
            " Cron " . $this->getName() . " was executed and finished but processed some errors.",
            HeLogger::IT_BUGS,
            HeLogger::CRITICAL,
            $actionDetails
        );

        echo " \n " . date("Y-m-d H:i:s", time()) . " " . $this->getName() . " finished with ERRORS : " .
          $actionDetails;
        echo " \n -----------------------------------------------------------------------------------------------";
        return true;
    }

    /**
     * @param $boleto
     * @param $actionDetails
     * @return bool
     */
    protected function processBoletoSingleError($boleto, $actionDetails)
    {
        HeLogger::sendLog(
          HeLogger::PREFIX_ERR_UNKNOWN_H .
            " Cron reconciliationBoleto had an inconsistency processing with Boleto id Payment = " . $boleto->idPayment,
            HeLogger::IT_BUGS,
            HeLogger::CRITICAL,
            'Reason for warning is: ' . $actionDetails
        );

        echo " \n " . date("Y-m-d H:i:s", time()) . " " . $this->getName() . " had a WARNING or ERROR: " .
          $actionDetails;
        echo " \n -----------------------------------------------------------------------------------------------";
        return true;
    }
}
