<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 22/11/12
 * Time: 12:11
 */
class ReconciliationAllpagoCommand extends AbaConsoleCommand
{
    // At the moment both BRAZIL and MEXICO are goig to be reconciliated through the same process.
    protected $idPaySupplier = PAY_SUPPLIER_ALLPAGO_BR;

    public function getHelp()
    {
        return parent::getHelp()." Downloads XML from All pago, format XML and tries to match their transactions with ours.\n ".
        " As an argument a valid date is accepted. Format required is YYYY-MM-DD.";
    }

    public function run($args)
    {
        $allSuccess = true;
echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
echo " \n ".$this->getName().": executing auto process at ".date("Y-m-d H:i:s", time() );

        $dateCheckPrevious = "";
        $today = HeDate::todaySQL(false);
        $dateRequest = HeDate::getDateAdd($today, -1/*day*/);
        if(count($args)>0)
        {
            if(!HeDate::validDateSQL($args[0])){
                echo 'Date is not valid. Please respect format YYYY-MM-DD';
                return false;
            }
            $dateRequest = $args[0];
        }

        /* ------------------For $dateRequest starts process of reconciliation -------------------------- */
        $dateRequestFrom = $dateRequest.' 00:00:00';
        $dateRequestUntil = $dateRequest.' 23:59:59';
        /* @var $paySupplier PaySupplierAllPago */
        $paySupplier = PaymentFactory::createPayment(null, $this->idPaySupplier);
        $moLogAllPago = $paySupplier->runRequestReconciliation( $dateRequestFrom, $dateRequestUntil);
        /* @var $fileXml SimpleXMLElement */
        $fileXml = $moLogAllPago->getXmlResponse();
        $fileParserXML = new FileParserAllPago($this->idPaySupplier);
        if (!$fileParserXML->saveFile($fileXml)){
            $this->processEndsWithError("File XML was requested and obtained, but could not be saved to disk.");
            return false;
        }
        if (!$fileParserXML->validateFile()){
            $this->processEndsWithError("File XML was requested SAVED to disk,but apparently it's not a valid XML file.");
            return false;
        }

        $aMoLogsReconc = $fileParserXML->parseToModels();
        if (count($aMoLogsReconc)==0){
            HeLogger::sendLog(HeLogger::PREFIX_NOTIF_H."CRON Reconciliation AllPago executed and finished on day ".
              date("Y-m-d H:i:s", time())."", HeLogger::IT_BUGS, HeLogger::INFO,
              "CRON Reconciliation AllPago executed and finished on day ".date("Y-m-d H:i:s", time()).
              " There was no transactions executed on BIP platform during day ".$dateRequest);
            echo " \n Success and end of process execution -------------------";
            return true;
        }

        /* -------------If there are any transactions available in XML then we check them all------------------- */
        $commMatching = new ReconcMatchingAllPago($this->idPaySupplier);
        $allSuccess = $commMatching->processPackTransactions($aMoLogsReconc, $fileParserXML->getFilePath());
        if (!$allSuccess) {
            /*$this->processEndsWithError("Not all nodes from XML file were successfully matched. ".
                    "Please check for summary email review sent to see which ones.");*/
            echo " \n \n   executed at ".date('Y-M-D H:m:s')." with ERRORS for some transactions. ";
        }

        echo " \n  Finished process at ".date('Y-M-D H:m:s');
        echo " \n -----------------------------------------------------------------------------------------------";
        return $allSuccess;
    }

    /**
     * @param $actionDetails
     * @return bool
     */
    protected function processEndsWithError($actionDetails)
    {
        HeLogger::sendLog(
          HeLogger::PREFIX_ERR_UNKNOWN_H .
            " Cron ".$this->getName()." was executed and finished but processed some errors.",
            HeLogger::IT_BUGS,
            HeLogger::CRITICAL,
            $actionDetails
        );

        echo " \n " . date("Y-m-d H:i:s", time()) . " ".$this->getName()." finished with ERRORS : " .
            $actionDetails;
        echo " \n -----------------------------------------------------------------------------------------------";
        return true;
    }
}
