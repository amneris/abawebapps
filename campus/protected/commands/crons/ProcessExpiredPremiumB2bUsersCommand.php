<?php
/**
 * Select all users PREMIUM that his expiration date has been reached out. Only B2b Users.
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 20/10/2013
 * Time: 12:11
 */
class ProcessExpiredPremiumB2bUsersCommand extends AbaConsoleCommand
{
    public function getHelp()
    {
        return parent::getHelp()."Use to expire users belonging B2B groups.Non-standard users.\n No arguments accepted ";
    }

    public function run($args)
    {
      try{
echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
echo " \n ProcessExpiredPremiumB2bUsersCommand: executing auto process at ".date("Y-m-d H:i:s", time() );
        $allSuccess = true;

        /*
         *  A. Select users from B2B group that expirationDate>today.
         * */
        $moUsers = new AbaUsers();
        /* u.`userType`, u.`expirationDate`,u.`id`, u.`name`, u.`surnames`,u.`email`, u.`countryId`, c.`name`,
        u.idPartnerSource, abp.name */

        $aExpiredUsers = $moUsers->getAllB2bUsersPremiumExpired();
        if (!$aExpiredUsers) {
echo " \n There are no expired users. Success on executing ProcessExpiredPremiumUsersCommand";
echo " \n -----------------------------------------------------------------------------------------------";
            return true;
        }

        $totalExpected = count($aExpiredUsers);
        $processedUsers = 0;
        $aUsersWithErrors = array();
         /* B. To Select last payment for each user. */
echo " \n There are several users, processExpiredPremiumB2bUsersCommand, let's run throgh them to expire them:";

        $aUserPayments = array();
        foreach( $aExpiredUsers as $aUser )
        {
            $aUserPayments[$aUser["email"]] = array();
            $aUserPayments[$aUser["email"]]["email"] = $aUser["email"];
            $aUserPayments[$aUser["email"]]["name"] = $aUser["name"];
            $aUserPayments[$aUser["email"]]["surnames"] = $aUser["surnames"];
            $aUserPayments[$aUser["email"]]["countryName"] = $aUser["countryName"];
            $aUserPayments[$aUser["email"]]["namePartnerSource"] = $aUser["namePartnerSource"];

            $moPayment = new Payment();
            $thereIsPays= $moPayment->getLastPaymentByUserId( $aUser["id"] );
            /* p.`idProduct`, p.`status`, p.`amountPrice`, ps.`name` as 'supplierName', p.`paySuppOrderId` */
            if( !$thereIsPays )
            {
                $aUserPayments[$aUser["email"]]["idProduct"] = "NOT FOUND";
                $aUserPayments[$aUser["email"]]["status"] = "NO PAYMENT FOUND";
                $aUserPayments[$aUser["email"]]["amountPrice"] = "N.F.I.";
                $aUserPayments[$aUser["email"]]["nameSupplier"] = " - ";
                $aUserPayments[$aUser["email"]]["dateToPay"] = "NO PAYMENTS";
                $aUserPayments[$aUser["email"]]["expirationDate"] = $aUser["expirationDate"];
            }
            else
            {
                $aUserPayments[$aUser["email"]]["idProduct"] = $moPayment->idProduct;
                $aUserPayments[$aUser["email"]]["status"] = $moPayment->status;
                $aUserPayments[$aUser["email"]]["amountPrice"] = $moPayment->amountPrice;
                /** @noinspection PhpUndefinedFieldInspection */
                $aUserPayments[$aUser["email"]]["supplierName"] = $moPayment->supplierName;
                $aUserPayments[$aUser["email"]]["dateToPay"] = $moPayment->dateToPay;
                $aUserPayments[$aUser["email"]]["expirationDate"] = $aUser["expirationDate"];

            }

            // Cancels the user, userType=1:
            $commUser = new UserCommon();
            if( $commUser->unsubscribe( $aUser["id"], PAY_CANCEL_DATEOFF_GROUPON ) ){
                $processedUsers++;
            } else{
                $allSuccess = false;
                $aUsersWithErrors[$aUser["id"]] = $aUser["email"];
            }



        }

        /* C.  It sends an email to payments@abaenglish.com and IT. */
        if( $totalExpected!==$processedUsers){

            HeLogger::sendLog("CRON Automatic process to check expired B2B users executed on day ".
              date("Y-m-d H:i:s", time()).
              ", check list to review. All of them have been DELETED/UNSUBSCRIBED ", HeLogger::IT_BUGS,
              HeLogger::INFO, $aUserPayments);

echo " \n ".date("Y-m-d H:i:s", time() )." Success in process ProcessExpiredPremiumB2BUsers (B2B). END";
echo " \n -----------------------------------------------------------------------------------------------";
            return $allSuccess;
        }else{
            HeLogger::sendLog("CRON Automatic process to check expired B2B users executed on day ".
              date("Y-m-d H:i:s", time()).
              ", check list to review. NOT All of them have been DELETED/UNSUBSCRIBED ", HeLogger::IT_BUGS,
              HeLogger::INFO, $aUserPayments);
echo " \n ".date("Y-m-d H:i:s", time() )." Finish process ProcessExpiredPremiumB2BUsers with some errors. ".
    "Some emails were not deleted: ".implode($aUsersWithErrors)." (B2B). END";
echo " \n -----------------------------------------------------------------------------------------------";
        }


      }catch (Exception $exc){
          $this->processError("Big exception on ".$exc->getCode().", ".$exc->getMessage());
echo "Big exception on ".$exc->getCode().", ".$exc->getMessage();
      }

      return false;
    }

    /**
     * @param string $actionDetails
     *
     * @return bool
     */
    private function processError($actionDetails)
    {
        HeLogger::sendLog("CRON ProcessExpiredPremiumB2BUsersCommand finished with errors ", HeLogger::IT_BUGS,
          HeLogger::CRITICAL, " Collecting data from users on ProcessExpiredPremiumB2BUsersCommand: ".$actionDetails);

        return true;
    }

}
