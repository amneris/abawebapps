# m h  dom mon dow   command
# ++++++++++++++  At 07:05A.M. Checks PENDING payments for TPV users, and executes the payment, and executes next one: +++++++++++++++++++++++++++
30 0 * * *    /usr/bin/php /var/www/campus/campus.abaenglish.com/public/cron.php RenewalPaymentCaixa >> /var/www/campus/logs/RenewalPaymentCaixa.log 2>&1


# ++++++++++++++ At 23:05 Releases a UNIT for FREE users everyday: ++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/5 * * * *    /usr/bin/php /var/www/campus/campus.abaenglish.com/public/cron.php ReleaseUserFreeUnits 50  >> /var/www/campus/logs/ReleaseUserFreeUnits_AfterRefactor.log 2>&1


# ++++++++++++++ At 23:15 Checks PREMIUM CAIXA users everyday that has expired: ++++++++++++++++++++++++++++++++++++++++++++++++++++++
15 1 * * *    /usr/bin/php /var/www/campus/campus.abaenglish.com/public/cron.php ProcessExpiredPremiumUsers caixa >> /var/www/campus/logs/ProcessExpiredPremiumUsersCaixa.log 2>&1


# ++++++++++++++ At 23:15 Checks PREMIUM GROUPON users everyday that has expired: ++++++++++++++++++++++++++++++++++++++++++++++++++++++
30 1 * * *    /usr/bin/php /var/www/campus/campus.abaenglish.com/public/cron.php ProcessExpiredPremiumUsers groupon >> /var/www/campus/logs/ProcessExpiredPremiumUsersGroupon.log 2>&1


# ++++++++++++++ At 23:15 Checks PREMIUM PAYPAL users everyday that has expired: ++++++++++++++++++++++++++++++++++++++++++++++++++++++
45 1 * * *    /usr/bin/php /var/www/campus/campus.abaenglish.com/public/cron.php ProcessExpiredPremiumUsers paypal >> /var/www/campus/logs/ProcessExpiredPremiumUsersPayapal.log 2>&1


# ++++++++++++++ Every Hour the review if there was mistakes in the other crons: ++++++++++++++++++++++++++++++++++++++++++++++++++++++
30 * * * * /usr/bin/php /var/www/campus/campus.abaenglish.com/public/cron.php reviewlogscron >> /var/www/campus/logs/ReviewCronLogs.log 2>&1