<?php
/**
 * In charge of sending notifications to Selligent to alert about next payments that will be charged.
 *It selects all PREMIUM users that have a PENDING payment in the date today plus X days.
 *
 * Created by Quino
 * User: Quino
 * Date: 08/03/2013
 * Time: 12:11
 */
class NotificationSelligentNextPayCommand extends AbaConsoleCommand
{
    protected $aListUsersErrors = array();
    protected $aListUsersSuccess = array();

    public function getHelp()
    {
        return parent::getHelp()." First argument: You can set a number to set how many records you want " .
                                "to send to SELLIGENT. \n " .
                                " Second argument: Optional, days in advance for warning. \n " .
                                " Otherwise 200 users and 15 days in advance will be used.";
    }


    public function run($args)
    {
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n START. NotificationSelligentNextPayCommand: executing auto process at " . date("Y-m-d h:i:s", time());
        $limitUsers = 200;
        $allSuccess = true;
        $daysInAdvance = 7;

        if (count($args) >= 1) {
            if (!is_numeric($args[0])) {
                echo " The first argument should be an integer telling how many records " .
                    "we want to process for every iteration.";
                return false;
            }
            $limitUsers = intval($args[0]);
        }

        if (count($args) == 2) {
            if (is_numeric($args[1])) {
                $daysInAdvance = $args[1];
            } else {
                echo " The argument should be a valid integer.";
                return false;
            }
        }

        $checkAlreadySent = false;
        if (count($args) == 3) {
            $checkAlreadySent = intval($args[2]);
        }

        $colPayments = new Payments();
        $dateNext = HeDate::getDateAdd(HeDate::todaySQL(false), $daysInAdvance, 0);
        echo " \n Collecting payments PENDING on $dateNext .";
        /* @var Payment[] $aNextPayments */
        $aNextPayments = $colPayments->getAllPaymentsPendingOnDate($dateNext, $limitUsers, true);
        if (!$aNextPayments) {
            $this->processError("No payments PENDING on the next " . $dateNext);
            return true;
        }
        /* @var Payment $moPayment */
        foreach ($aNextPayments as $moPayment) {
            $moUser = new AbaUser();
            if (!$moUser->getUserById($moPayment->userId)) {
                $this->aListUsersErrors[] = array("userId" => $moPayment->userId, "email" => "",
                    "idPayment" => $moPayment->id, "dateToPay" => $moPayment->dateToPay, "cardYearMonth" => false,
                    "desc" => "User not found in database.");
                $allSuccess = false;
                continue;
            }

            if ($checkAlreadySent) {
                $logSelligent = new LogSentSelligent();
                if ($logSelligent->getLogByUserId($moUser->id, "infoNextPayment")) {
                    $this->aListUsersSuccess[] = array("userId" => $moPayment->userId, "email" => "",
                        "idPayment" => $moPayment->id, "dateToPay" => $moPayment->dateToPay, "cardYearMonth" => false,
                        "desc" => "NOT SENT, because it was already sent on " . $logSelligent->dateSent);
                    continue;
                }
            }

            $moUserCreditForm = new AbaUserCreditForms();
            if (!$moUserCreditForm->getUserCreditFormById($moPayment->idUserCreditForm)) {
                $this->aListUsersErrors[] = array("userId" => $moPayment->userId, "email" => "",
                    "idPayment" => $moPayment->id, "dateToPay" => $moPayment->dateToPay, "cardYearMonth" => false,
                    "desc" => "Credit form or paypal account not found in database.");
                $allSuccess = false;
                continue;
            }

            $aFieldsSell = $this->getFieldsToSend($moPayment, $moUserCreditForm);

            $commSelligent = new SelligentConnectorCommon();
            if (!$commSelligent->sendOperationToSelligent(SelligentConnectorCommon::infoNextPayment, $moUser, $aFieldsSell,
                                                                                    "NotificationSelligentNextPay")
            ) {
                $this->aListUsersErrors[] = array("userId" => $moPayment->userId, "email" => $moUser->email,
                    "idPayment" => $moPayment->id, "dateToPay" => $moPayment->dateToPay,
                    "cardYearMonth" => $moUserCreditForm->cardYear."/".$moUserCreditForm->cardMonth,
                    "desc" => "Selligent not updated error: " . $commSelligent->getErrMessage());
                $allSuccess = false;
            } else {
                $this->aListUsersSuccess[] = array("userId" => $moPayment->userId, "email" => $moUser->email,
                    "idPayment" => $moPayment->id, "dateToPay" => $moPayment->dateToPay,
                    "cardYearMonth" => $moUserCreditForm->cardYear."/".$moUserCreditForm->cardMonth,
                    "desc" => "Selligent response: " . $commSelligent->getResponse());
            }
        }

        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n ";

        if (!$allSuccess) {
            $this->sendSummaryError( "The process NotificationSelligentNextPay has finished but with some errors, ".
                " users that will pay on $dateNext  ");
            echo " \n " . date("Y-m-d h:i:s", time()) . " Some errors in process NotificationSelligentNextPayCommand. END";
            echo " \n -----------------------------------------------------------------------------------------------";
            return true;
        }

        $this->sendSummarySuccess( "All users that will pay on $dateNext have been informed to Selligent with ".
                        "all their rellevant data" );
        echo " \n " . date("Y-m-d h:i:s", time()) . " Success in process NotificationSelligentNextPayCommand. END";
        echo " \n -----------------------------------------------------------------------------------------------";
        return $allSuccess;

    }

    /** Send general error in the process.
     *
     * @param string $actionDetails
     *
     * @return bool
     */
    private function processError($actionDetails)
    {
        HeLogger::sendLog("CRON NotificationSelligentNextPayCommand finished with errors ",
          HeLogger::IT_BUGS, HeLogger::CRITICAL,
          " Error processing NotificationSelligentNextPayCommand: " . $actionDetails);

        return true;
    }

    /** Sends the e-mail with errors summary.
     *
     * @param $actionDetails
     */
    private function sendSummaryError($actionDetails)
    {
        $bodyDesc = $actionDetails;
        $aSummAll = array_merge($this->aListUsersErrors, $this->aListUsersSuccess);

        $bodyDesc .= " == " . json_encode($aSummAll);

//        HeLogger::sendLog(HeLogger::PREFIX_ERR_LOGIC_L . ": NotificationSelligentNextPay " . HeDate::todaySQL(true) .
//          " The lines of users below were processed with errors, please review them: ",
//          HeLogger::IT_BUGS,HeLogger::CRITICAL,array("body" => $bodyDesc, "listTable" => $aSummAll));

        HeLogger::sendLog(HeLogger::PREFIX_ERR_LOGIC_L . ": NotificationSelligentNextPay " . HeDate::todaySQL(true) .
          " The lines of users below were processed with errors, please review them: ",
          HeLogger::IT_BUGS, HeLogger::CRITICAL, $bodyDesc);
    }

    /**
     * @param $actionDetails
     */
    private function sendSummarySuccess($actionDetails)
    {
        $bodyDesc = $actionDetails;
        $aSummAll = $this->aListUsersSuccess;

        $bodyDesc .= " == " . json_encode($aSummAll);

//        HeLogger::sendLog(HeLogger::PREFIX_NOTIF_L . ": NOTIFICATIONSELLIGENTNEXTPAY " . HeDate::todaySQL(true) .
//          " The lines of users below were processed SUCCESSFULLY. You do not have to do any shit: ",
//          HeLogger::SELLIGENT , HeLogger::CRITICAL, array("body" => $bodyDesc, "listTable" => $aSummAll));

        HeLogger::sendLog(HeLogger::PREFIX_NOTIF_L . ": NOTIFICATIONSELLIGENTNEXTPAY " . HeDate::todaySQL(true) .
          " The lines of users below were processed SUCCESSFULLY. You do not have to do any shit: ",
          HeLogger::SELLIGENT, HeLogger::CRITICAL, $bodyDesc);
    }


    /** Returns all info to send to Selligent.
     *
     * @param Payment $moPayment
     * @param AbaUserCreditForms $moUserCreditForm
     *
     * @return array
     */
    private function getFieldsToSend(Payment $moPayment, AbaUserCreditForms $moUserCreditForm)
    {
        $moPeriodicity = new ProdPeriodicity();
        $moPeriodicity->getProdPeriodById($moPayment->idPeriodPay);

        $aFieldsSell = array();
        $aFieldsSell['RENOV_DT'] = $moPayment->dateToPay;
        $aFieldsSell['PRICE'] = $moPayment->amountPrice;
        $aFieldsSell['CURRENCY'] = $moPayment->currencyTrans;
        $aFieldsSell['PRODUCT'] = $moPeriodicity->months;
        $aFieldsSell['CARD_CAD'] = 0;
        $aFieldsSell['CARD_NUMBER'] = 0; // PAYPAL Users = 0
        if ($moUserCreditForm->kind != KIND_CC_PAYPAL) {
            if (intval($moUserCreditForm->cardYear) <= HeDate::getYearFromDate(HeDate::todaySQL(false)) &&
                intval($moUserCreditForm->cardMonth) < HeDate::getMonthFromDate(HeDate::todaySQL(false))
            ) {
                $aFieldsSell['CARD_CAD'] = 1;
            }
            $aFieldsSell['CARD_NUMBER'] = substr($moUserCreditForm->cardNumber, -4);
        }

        return $aFieldsSell;
    }
}
