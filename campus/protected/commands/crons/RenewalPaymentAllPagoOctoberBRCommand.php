<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 22/11/12
 * Time: 12:11
 */
class RenewalPaymentAllPagoOctoberBRCommand extends AbaConsoleCommand
{

//    php -f  campus/public/cron.php RenewalPaymentAllPagoOctoberBR

    private function processRenevals($countryMode = 'ALL', $isTestRenewal = false)
    {

        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n RenewalPaymentAllPagoCommand: executing auto process at " . date("Y-m-d H:i:s", time());
        $allSuccess = true;

        // 1- Collect all Allpago Payments Supplier=1, status = PENDING, dateToPay>=Today:
        $moPayments = new Payments();

        $aPendingPays = false;

        switch ($countryMode) {
            case self::COUNTRY_MODE_BR:
                /*  @var Payment[] $aPendingPays */
                $aPendingPays = $moPayments->getAllPaymentsPendingAllpagoBR();
                break;
            case self::COUNTRY_MODE_MX:
                /*  @var Payment[] $aPendingPays */
                $aPendingPays = $moPayments->getAllPaymentsPendingAllpagoMX();
                break;
            default:
                /*  @var Payment[] $aPendingPays */
                $aPendingPays = $moPayments->getAllPaymentsPendingAllpago();
                break;
        }

        if (!$aPendingPays) {
            // No Payments pending for Allpago
            echo " \n There are no users to renew today. Success on executing RenewalPaymentAllpagoCommand";
            echo " \n -----------------------------------------------------------------------------------------------";
            return true;
        }

//        $percentageCounter = 0;

        $moPaySupplierNames = new AbaPaySuppliers();
        /* @var Payment $moPendingPay */
        foreach ($aPendingPays as $moPendingPay) {
            $paySupplierName = $moPaySupplierNames->getNameByIdSupplier($moPendingPay->paySuppExtId);
            echo " \n " . date("Y-m-d H:m:s") . " Processing the renewal of payment id " . $moPendingPay->id . " of user " .
              $moPendingPay->userId;

            $dateStart = HeDate::todaySQL(true);
            /*  For every payment, we send an operation to AllPago.com */
            // 2-Collect all data for the user linked to this payment:
            $user = new AbaUser();
            if ($moPendingPay->userId == '' || !is_numeric($moPendingPay->userId)) {
                throw new CDbException("ABA, User Id present in payment " . $moPendingPay->id . " is not VALID AT ALL.");
            }

            if (!$user->getUserById($moPendingPay->userId)) {
                $this->processErrorNoRenewed($user, $moPendingPay, " User not found by id user if of payment " .
                  $moPendingPay->userId, $dateStart);
                $allSuccess = false;
                continue;
            }

            if (Yii::app()->config->get("ENABLE_LOG_USERACTIVITY") == 1) {
                $user->abaUserLogUserActivity->insertLog("payments", "",
                  " User susceptible of renewal, initiating process.",
                  "(AUTO)recurrent payment");
            }

            $moUserCredit = new AbaUserCreditForms();
            if (!$moUserCredit->getUserCreditFormByUserId($moPendingPay->userId)) {
                $this->processErrorNoRenewed($user, $moPendingPay, " Credit card details not found for the user " .
                  $user->email, $dateStart);
                $allSuccess = false;
                continue;
            }

            // 3-We must validate again the product associated with this payment. It might be changed or disappeared:
            $moProductUser = new ProductPrice();
            if (!$moProductUser->getProductById($moPendingPay->idProduct)) {
                $this->processErrorNoRenewed($user, $moPendingPay,
                  "Product not Found for the user  " . $user->email . ", product= " . $moPendingPay->idProduct,
                  $dateStart);
                $allSuccess = false;
                continue;
            }

            // We compare product details to detect if some attributes has changed from the current ones:
            if (HeMixed::getRoundAmount($moProductUser->priceOfficialCry) !==
              HeMixed::getRoundAmount($moPendingPay->amountPrice)
            ) {
                HeLogger::sendLog(
                  HeLogger::PREFIX_NOTIF_L . " : Cron RenewalPaymentAllPagoCommand, price on product " .
                  $moProductUser->idProduct . " is different.",
                  HeLogger::IT_BUGS,
                  HeLogger::CRITICAL,
                  " : Cron RenewalPaymentAllPagoCommand, payment id " . $moPendingPay->id .
                  " has a product with a price that no longer is equal to the current one. Prices:" .
                  "<br/> Current= " . $moProductUser->priceOfficialCry . " <br/>Payment= " . $moPendingPay->amountPrice
                );
            }

            //  Starts the execution that affects the database data related with these next payments pending:
            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            if (intval(Yii::app()->config->get("ENABLE_ALLPAGO_PAY_RECURRENT")) == 1) {

                $bUpdateNewPendingpayment = false;

                //#ZOR-526
//                if ($isTestRenewal AND ($percentageCounter % 2 == 0)) {
                if ($isTestRenewal) {

                    $moSuccessPayment = new Payment();
                    if ($moSuccessPayment->getFirstPaymentByUserIdAndMore(
                      $moPendingPay->userId, $moPendingPay->paySuppExtId, $moPendingPay->idCountry,
                      $moPendingPay->idUserCreditForm
                    )
                    ) {
                        if ($moPendingPay->amountPrice <> $moSuccessPayment->amountPrice) {

                            //#ZOR-526
                            $bUpdateNewPendingpayment = true;

                            $moPendingPay->idPromoCode = $moSuccessPayment->idPromoCode;
                            $moPendingPay->amountDiscount = $moSuccessPayment->amountDiscount;
                            $moPendingPay->amountPrice = $moSuccessPayment->amountPrice;

                            $moPendingPay->amountPriceWithoutTax = $moSuccessPayment->amountPriceWithoutTax;
                            $moPendingPay->taxRateValue = $moSuccessPayment->taxRateValue;
                            $moPendingPay->amountTax = $moSuccessPayment->amountTax;

                            try {

                                $moUpdatedPayment = new Payment();
                                $moUpdatedPayment->getPaymentById($moPendingPay->id);

                                $moUpdatedPayment->idPromoCode = $moSuccessPayment->idPromoCode;
                                $moUpdatedPayment->amountDiscount = $moSuccessPayment->amountDiscount;
                                $moUpdatedPayment->amountPrice = $moSuccessPayment->amountPrice;

                                $moUpdatedPayment->amountPriceWithoutTax = $moSuccessPayment->amountPriceWithoutTax;
                                $moUpdatedPayment->taxRateValue = $moSuccessPayment->taxRateValue;
                                $moUpdatedPayment->amountTax = $moSuccessPayment->amountTax;

                                $moUpdatedPayment->updatePendingAmounts();

                            } catch (Exception $e) {
                            }

                            echo " \n 100% YES: PAYMENTID=" . $moPendingPay->id . " ORIGINAL SUCCESS PAYMENTID=" . $moSuccessPayment->id . " WITH ORIGINAL AMOUNT PRICE=" . $moSuccessPayment->amountPrice . " at " . date('Y-M-D H:m:s');
                            echo " \n -----------------------------------------------------------------------------------------------";

                        } else {
                            echo " \n 100% YES NO PRICE CHANGE: PAYMENTID=" . $moPendingPay->id . " at " . date('Y-M-D H:m:s');
                            echo " \n -----------------------------------------------------------------------------------------------";
                        }
                    } else {
                        echo " \n 100% YES ERROR - NO FIRST PAYMENT: PAYMENTID=" . $moPendingPay->id . " at " . date('Y-M-D H:m:s');
                        echo " \n -----------------------------------------------------------------------------------------------";
                    }
                } else {
                    echo " \n 100% NO: PAYMENTID=" . $moPendingPay->id . "  " . " at " . date('Y-M-D H:m:s');
                    echo " \n -----------------------------------------------------------------------------------------------";
                }

//                ++$percentageCounter;

                $moPayCtrlCheck = new PaymentControlCheck();
                if (!$moPayCtrlCheck->createPayControlCheckFromPayment($user->id, 1, $moPendingPay->idProduct,
                  $moPendingPay->idCountry, $moPendingPay->idPeriodPay, $moPendingPay->idPromoCode,
                  $moUserCredit->kind, $moUserCredit->cardName, $moUserCredit->cardNumber,
                  $moUserCredit->cardYear, $moUserCredit->cardMonth, $moUserCredit->cardCvc,
                  $moPendingPay->paySuppExtId, $moPendingPay, $moUserCredit->cpfBrasil, '', false)
                ) {
                    $this->processErrorNoRenewed($user, $moPendingPay, " Payment Control check could not be built  " .
                      $user->email . " product= " . $moPendingPay->idProduct, $dateStart);
                    $allSuccess = false;
                    continue;
                }

                /* @var PaySupplierAllPagoBr|PaySupplierAllPagoMx $paySupplier */
                $paySupplier = PaymentFactory::createPayment($moPayCtrlCheck);
                /* @var PayGatewayLogAllpago $logPaymentSupplier */

                $logPaymentSupplier = $paySupplier->runDebitingRenewal($moPayCtrlCheck, $user, $moUserCredit);

                if ($logPaymentSupplier == false) {
                    $errPaymentGateway = Yii::t('mainApp', 'gateway_recurrent_failure_key') .
                      " ( Response from PaymentSupplier = " . $paySupplier->getErrorCode() . "-" .
                      $paySupplier->getErrorString() . " )";
                    $moPendingPay->paySuppOrderId = $paySupplier->getOrderNumber();
                    $this->processErrorNoRenewed($user, $moPendingPay,
                      "Renewal request through $paySupplierName for user " .
                      $user->email . " payment= " . $moPendingPay->id . " Details=" . $errPaymentGateway, $dateStart);
                    $allSuccess = false;
                    continue;
                }

                $successAfterGateway = true;

                // 8-Update older payment.
                // 9-To save next payment.
                // 10-Update user, expiration date basically.
                $commRecurrPayment = new RecurrentPayCommon();
                $retSucc = $commRecurrPayment->renewPaymentByPaySuppOrderId($user->email,
                  $paySupplier->getOrderNumber(),
                  $dateStart, "recurring_payment_ALLPAGO", $moPendingPay->paySuppExtId, $moPendingPay->id);
                if (!$retSucc) {
                    $successAfterGateway = false;
                    $this->processErrorAfterRenewed($user, $moPendingPay,
                      " Error in renewal on $paySupplierName process. User " .
                      $user->email . " payment= " . $moPendingPay->id .
                      " has been carried out successfully in $paySupplierName but processing it" .
                      "into our database has failed. Please review renewal data. More Details= " .
                      $commRecurrPayment->getErrorMessage());
                    $allSuccess = false;
                }

                $moPendingPay->refresh();
                $moPendingPay->paySuppExtUniId = $logPaymentSupplier->getUniqueId();
                $moPendingPay->update(array("paySuppExtUniId"));


                //#ZOR-526
                if ($bUpdateNewPendingpayment) {

                    $moLastNewPayment = new Payment();
                    if ($moLastNewPayment->getLastPendingPaymentByUserIdAndMore(
                      $moPendingPay->userId, $moPendingPay->paySuppExtId, $moPendingPay->idCountry,
                      $moPendingPay->idUserCreditForm
                    )
                    ) {

                        $moLastNewPayment->idPromoCode = $moPendingPay->idPromoCode;

                        $moLastNewPayment->amountOriginal = $moPendingPay->amountPrice;
                        $moLastNewPayment->amountDiscount = 0;
                        $moLastNewPayment->amountPrice = $moPendingPay->amountPrice;

                        $moLastNewPayment->amountPriceWithoutTax = $moPendingPay->amountPriceWithoutTax;
                        $moLastNewPayment->taxRateValue = $moPendingPay->taxRateValue;
                        $moLastNewPayment->amountTax = $moPendingPay->amountTax;

                        $moLastNewPayment->updateNewPendingAmounts();
                    }
                }

                if ($successAfterGateway) {
                    echo " >> " . date("Y-m-d H:m:s") . " Successful payment of " . $user->email . " for payment= " .
                      $moPendingPay->id;
                } else {
                    echo " >> " . date("Y-m-d H:m:s") . " ERROR payment of " . $user->email . " for payment= " .
                      $moPendingPay->id;
                }
            } else {
                HeLogger::sendLog("CRON Process Renewal of payments for $paySupplierName Gateway: " . $user->email,
                  HeLogger::IT_BUGS_PAYMENTS, HeLogger::INFO,
                  " The payment " . $moPendingPay->id . " with the amount " . $moPendingPay->amountPrice .
                  " and date to pay was " . $moPendingPay->dateToPay . " should go under real transaction now." .
                  " Disabled by ENABLE_ALLPAGO_PAY_RECURRENT");
                echo " >> " . date("Y-m-d H:m:s") . " Should be a successful processed user " . $user->email .
                  " for payment with id= " . $moPendingPay->id . " But ENABLE_ALL_PAGO_RECURRENT is disabled.";
            }
        }

        if (!$allSuccess) {
            echo " \n \n  Renewal of $paySupplierName executed at " . date('Y-M-D H:m:s') . " with ERRORS for some payments. ";
        }

        echo " \n  Finished process at " . date('Y-M-D H:m:s');
        echo " \n -----------------------------------------------------------------------------------------------";
        return $allSuccess;
    }

    const COUNTRY_MODE_BR = 'BR';
    const COUNTRY_MODE_MX = 'MX';
    const COUNTRY_MODE_ALL = 'ALL';

    /**
     * @return array
     */
    private function getConfigValues()
    {

        $renewalsDatesString = Yii::app()->config->get("ALLPAGO_BR_OCTOBER_RENEWALS_DATES");

        $renewalsDates = explode(";", $renewalsDatesString);

        $renewalsDateStart = $renewalsDates[0];
        $renewalsDateEnd = $renewalsDates[1];
        $dateNow = HeDate::todaySQL(true);

        return array("dateStart" => $renewalsDateStart, "dateEnd" => $renewalsDateEnd, "dateNow" => $dateNow);
    }

    /**
     * @return bool
     */
    private function isTestRenewal()
    {

        $configValues = $this->getConfigValues();

        if (strtotime($configValues["dateNow"]) >= strtotime($configValues["dateStart"])
          AND strtotime($configValues["dateNow"]) <= strtotime($configValues["dateEnd"])
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param array $args
     *
     * @return bool
     * @throws CDbException
     */
    public function run($args)
    {
        $allSuccess = true;

        if ($this->isTestRenewal()) {
            $allSuccessMX = $this->processRenevals(self::COUNTRY_MODE_MX, false);
            $allSuccessBR = $this->processRenevals(self::COUNTRY_MODE_BR, true);

            if (!$allSuccessMX OR !$allSuccessBR) {
                $allSuccess = false;
            }
        } else {
            $allSuccess = $this->processRenevals(self::COUNTRY_MODE_ALL, false);
        }

        return $allSuccess;
    }

    /**
     * @param AbaUser $user
     * @param Payment $payment
     * @param         $actionDetails
     * @param null $dateStart
     *
     * @return bool
     */
    private function processErrorNoRenewed(AbaUser $user, Payment $payment, $actionDetails, $dateStart = null)
    {
        if (intval(Yii::app()->config->get("ENABLE_ALLPAGO_PAY_RECURRENT")) == 1) {
            // @TODO Replace function updateFailedRenewPay by RecurringCommon::failRecurring
            // Payment has failed to be renewed by Allpago, update  Payment with failure.
            $payment->updateFailedRenewPay($dateStart, $actionDetails);
            // User is to be cancelled, means to change to FREE again.
            $commUser = new UserCommon();
            $commUser->cancelUser($user->getId(), PAY_CANCEL_FAILED_RENEW);
        }

        // Send email to warn us all.
        HeLogger::sendLog(HeLogger::PREFIX_NOTIF_L . ": CRON User Recurring Payment Allpago with error " . $user->email .
          ", NO NEED TO BE REVIEWED BY IT, payment id=" . $payment->id, HeLogger::IT_BUGS_PAYMENTS,
          HeLogger::INFO, " User Recurring Payment Allpago witth error " . $user->email . ", payment id=" .
          $payment->id . " $actionDetails ");
        echo ">> VVV" . " Error processing, NOT RENEWED, user DID NOT PAY and WAS CANCELLED in DATABASE. " .
          "NO NEED TO BE REVIEWED BY IT, payment id " . $payment->id;
        return true;
    }

    /**
     * @param AbaUser $user
     * @param Payment $payment
     * @param         $actionDetails
     *
     * @return bool
     */
    private function processErrorAfterRenewed(AbaUser $user, Payment $payment, $actionDetails)
    {
        HeLogger::sendLog("CRON User Recurring Payment Allpago successful but finished with errors " . $user->email .
          ", It MUST be REVIEWED payment id=" . $payment->id, HeLogger::IT_BUGS_PAYMENTS, HeLogger::CRITICAL,
          " User Recurring Payment Allpago successful but finished with errors " . $user->email .
          ", payment id=" . $payment->id . " $actionDetails ");
        echo ">> XXX" . " Error processing, RENEWED in database but with failures. It MUST be REVIEWED: payment id " .
          $payment->id;
        return false;
    }

}
