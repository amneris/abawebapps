<?php

date_default_timezone_set('Europe/Madrid');

class RenewalPaymentAdyen31Command extends AbaConsoleCommand
{
    public function run($args)
    {
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n RenewalPaymentAdyenCommand: executing auto process at ".date("Y-m-d H:i:s", time() );
        $allSuccess = true;

        //
        // /usr/bin/php  campus/public/cron.php  RenewalPaymentAdyen31  "2017-04-05"  5
        //

        $dateLimit = HeDate::todaySQL(false);
        if (count($args) > 0) {
            if (!HeDate::validDateSQL($args[0])) {
                echo 'Date is not valid. Please respect format YYYY-MM-DD';
                return false;
            }
            $dateLimit = substr($args[0], 0, 10);
        }

        $limitSql = 1000;
        if (count($args) > 1) {
            if (!is_numeric($args[1])) {
                echo 'Limit is not valid.';
                return false;
            }
            $limitSql = $args[1];
        }


        // 1- Collect all Adyen Payments Supplier=10, status = PENDING, dateToPay>=Today; Order by dateToPay ASC, dateStartTransaction ASC
        $moPayments = new Payments();
        /*  @var Payment[] $aPendingPays    */
        $aPendingPays = $moPayments->getAllPaymentsPendingAdyen31($dateLimit, $limitSql);
        if( !$aPendingPays ) {
// No Payments pending for Adyen;
            echo " \n There are no users to renew today. Success on executing RenewalPaymentAdyenCommand";
            echo " \n -----------------------------------------------------------------------------------------------";
            return true;
        }

        /* @var Payment $moPendingPay */
        foreach($aPendingPays as $moPendingPay)
        {
            echo " \n ". date("Y-m-d H:m:s")." Processing the renewal of payment id ".$moPendingPay->id." of user ".$moPendingPay->userId;

            $dateStart = HeDate::todaySQL(true);
            /*  Para cada pago */
            // 2-Collect Datos de pago para el usuario de este pago
            $user = new AbaUser( );
            if( $moPendingPay->userId=='' || !is_numeric($moPendingPay->userId) ) {
                throw new CDbException("ABA, User Id present in payment ".$moPendingPay->id." is not VALID AT ALL.");
            }

            if( !$user->getUserById( $moPendingPay->userId ) ) {
                $this->processErrorNoRenewed($user, $moPendingPay, " User not found by id user of payment ".$moPendingPay->userId , $dateStart);
                $allSuccess = false;
                continue;
            }

            if(Yii::app()->config->get("ENABLE_LOG_USERACTIVITY") == 1) {
                $user->abaUserLogUserActivity->insertLog("payments",""," user susceptible of renewal, initiating process.","(AUTO)recurrent payment");
            }

            $moUserCredit = new AbaUserCreditForms(  );
            if( !$moUserCredit->getUserCreditFormByUserId($moPendingPay->userId) )
            {
                $this->processErrorNoRenewed($user, $moPendingPay, " Credit card Details not found for the user ".$user->email, $dateStart);
                $allSuccess = false;
                continue;
            }
            // 3-Validar Datos de Producto para este usuario/pago
            $moProductUser = new ProductPrice();
            if( !$moProductUser->getProductById( $moPendingPay->idProduct ) )
            {
                $this->processErrorNoRenewed($user, $moPendingPay, " Product not Found for the user  ".$user->email.", product= ".$moPendingPay->idProduct , $dateStart);
                $allSuccess = false;
                continue;
            }

            //  Starts the execution that affects the database data related with these next payments pending:
            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            if( intval( Yii::app()->config->get("ENABLE_ADYEN_PAY_RECURRENT") ) == 1)
            {
                // 4 - Preparar SOAP y validarlo
                // 5 - Insertar petición en Log de Adyen
                // 6 - Enviar petición de Pago
                // 7 - Guardar Reponse en Log
                $moPaymentControlCheck = new PaymentControlCheck();
                if( !$moPaymentControlCheck->createPayControlCheckFromPayment( $user->id, 1, $moPendingPay->idProduct, $moPendingPay->idCountry, $moPendingPay->idPeriodPay,
                  $moPendingPay->idPromoCode, $moUserCredit->kind, $moUserCredit->cardName, $moUserCredit->cardNumber , $moUserCredit->cardYear, $moUserCredit->cardMonth,
                  $moUserCredit->cardCvc, $moPendingPay->paySuppExtId, $moPendingPay, '', '', false ) )
                {
                    $this->processErrorNoRenewed($user, $moPendingPay, " Payment Control check could not be built  ".$user->email." product= ".$moPendingPay->idProduct, $dateStart );
                    $allSuccess = false;
                    continue;
                }

                /* @var PaySupplierAdyen $paySupplier */
                $paySupplier = PaymentFactory::createPayment( $moPendingPay );

                /* @var PayGatewayLogAdyenResRec $retPaymentSupplier */
                $retPaymentSupplier = $paySupplier->runDebitingRenewal( $moPaymentControlCheck, $user, $moPendingPay );

                if( !$retPaymentSupplier )
                {
                    $errPaymentGateway = Yii::t('mainApp', 'gateway_recurrent_failure_key').
                      " ( Response from PaymentSupplier = ".$paySupplier->getErrorCode()."-".$paySupplier->getErrorString()." )";
                    $moPendingPay->paySuppOrderId = $paySupplier->getOrderNumber();
                    $this->processErrorNoRenewed($user, $moPendingPay, " Renewal request through Adyen for user ".$user->email." payment= ".$moPendingPay->id." Details=".$errPaymentGateway , $dateStart);
                    $allSuccess = false;
                    continue;
                }

                // 8 -  Actualizar pago viejo.
                // 9 -  Crear siguiente pago.
                // 10 - Actualizar Usuario (Renovar)
                $commRecurrPayment = new RecurrentPayCommon();
                $retSucc = $commRecurrPayment->renewPaymentByPaySuppOrderId($user->email, $paySupplier->getOrderNumber(),
                  $dateStart,"recurring_payment_ADYEN", PAY_SUPPLIER_ADYEN , $moPendingPay->id, array());
                if (!$retSucc) {
                    $this->processErrorAfterRenewed($user, $moPendingPay, " Error in renewal on Adyen process. User " . $user->email . " payment= " . $moPendingPay->id . " has been carried out successfully in Adyen but processing " .
                      "into our database has failed. Please review renewal data. More Details= " . $commRecurrPayment->getErrorMessage());
                    $allSuccess = false;
                } else {
                    $moPendingPay->paySuppExtUniId = $retPaymentSupplier->getUniqueId();
                    $moPendingPay->update(array("paySuppExtUniId"));
                }
            }
            else {
                HeLogger::sendLog("CRON Process Renewal of payments for Adyen Gateway: ".$user->email,
                  HeLogger::IT_BUGS_PAYMENTS, HeLogger::INFO,
                  " The payment ".$moPendingPay->id." with the amount ".$moPendingPay->amountPrice." and date to pay was ".$moPendingPay->dateToPay." should go under real transaction now. Disabled by ENABLE_ADYEN_PAY_RECURRENT");
            }

            echo " >> ". date("Y-m-d H:m:s")." Successful processed user ".$user->email." for payment with id= ".$moPendingPay->id;
        }

        if( !$allSuccess ) {
            echo " \n \n  Renewal of Adyen executed at ".date('Y-M-D H:m:s')." with ERRORS for some payments. ";
        }

        echo " \n  Finished process at ".date('Y-M-D H:m:s');
        echo " \n -----------------------------------------------------------------------------------------------";
        return $allSuccess;
    }

    /**
     * @param AbaUser $user
     * @param Payment $payment
     * @param         $actionDetails
     * @param null    $dateStart
     *
     * @return bool
     */
    private function processErrorNoRenewed(AbaUser $user, Payment $payment, $actionDetails, $dateStart=null)
    {
        if( intval( Yii::app()->config->get("ENABLE_ADYEN_PAY_RECURRENT") ) == 1)
        {
            // @TODO Replace function updateFailedRenewPay by RecurringCommon::failRecurring
            // Payment has failed to be renewed by Adyen, update  Payment with failure.
            $fUpdPay = $payment->updateFailedRenewPay($dateStart, $actionDetails);
            // User is to be cancelled, means to change to FREE again.
            $commUser = new UserCommon();
            $commUser->cancelUser(  $user->getId(), PAY_CANCEL_FAILED_RENEW );
        }

        echo ">> VVV"." Error processing, NOT RENEWED, user DID NOT PAID and WAS CANCELLED in DATABASE. ".
          " NO NEED TO BE REVIEWED BY IT, payment id ".$payment->id;
        return true;
    }

    /**
     * @param AbaUser $user
     * @param Payment $payment
     * @param         $actionDetails
     *
     * @return bool
     */
    private function processErrorAfterRenewed(AbaUser $user, Payment $payment, $actionDetails)
    {
        HeLogger::sendLog("CRON User Recurring Payment Adyen successful but finished with errors ".$user->email.", It MUST be REVIEWED payment id=".$payment->id ,
          HeLogger::IT_BUGS_PAYMENTS, HeLogger::CRITICAL,
          " User Recurring Payment Adyen successful but finished with errors ".$user->email.", payment id=".$payment->id." $actionDetails ");

        echo ">> XXX"." Error processing, RENEWED in database but with failures. It MUST be REVIEWED: payment id ".$payment->id;
        return true;
    }

}
