<?php
/**
 * Created by JetBrains PhpStorm.
 * Date: 29/06/2015
 * Time: 17:17
 */
class ExpireAllpagoBoletoCommand extends AbaConsoleCommand
{
    // It checks for Boleto receipts in order to convert users to Premium
    protected $idPaySupplier = PAY_SUPPLIER_ALLPAGO_BR_BOL;

    public function getHelp() {
        return parent::getHelp()." Expire BOLETOS Brazil.\n ";
    }

    //
    // TEST:    php -f  /var/www/abawebapps/campus/public/cron.php  ExpireAllpagoBoleto
    //
    public function run($args)
    {
        $allSuccess = true;
echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
echo " \n ".$this->getName().": executing auto process at ".date("Y-m-d H:i:s", time() );

        $today =        HeDate::now();
        $dateRequest =  HeDate::getDateAdd($today, -100/*days*/);

echo "\n+++++++" . $today . "+++++++\n";
echo "\n+++++++" . $dateRequest . "+++++++\n";

        // ----------------     Now we convert to Off all Boletos that have expired ------------------
        $comBoleto = new BoletoCommon();
        $comBoleto->processOff($dateRequest);

echo " \n  Finished process at ".date('Y-M-D H:m:s');
echo " \n -----------------------------------------------------------------------------------------------";
        return $allSuccess;
    }

}
