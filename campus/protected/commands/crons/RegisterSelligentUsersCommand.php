<?php
/**
 * In case of any failure of synchronization between Campus and Selligent
 *  this script can be used to force the creation of a group of users in SELLIGENT.
 * Created by Quino
 * User: Quino
 * Date: 08/03/2013
 * Time: 12:11
 */
class RegisterSelligentUsersCommand extends AbaConsoleCommand
{
    public function getHelp()
    {
        return parent::getHelp() . " First argument: You can set a number to set how many records you want " .
        "to send to SELLIGENT. \n " .
        " Second argument: Optional, you can set a registration date users. " .
        "Otherwise all users no synchronization confirmed by Sellignt will be processed.";
    }

    // TEST:    php -f  /var/www/abawebapps/campus/public/script.php RegisterSelligentUsers 100

    public function run($args)
    {
        // 2 minutes maximum. At the moment we recommend schedule to be 400 users every hour.
        ini_set('max_execution_time', 120);

        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n START. RegisterSelligentUsersCommand: executing auto process at " . date("Y-m-d h:i:s", time());
        // Default values when no arguments set:
        $registrationDate = null;
        $limitUsers =       300;
        $allSuccess =       true;

        if (count($args) >= 1 && is_numeric($args[0])) {
            $limitUsers = intval($args[0]);
        } else {
            echo " The argument should be an integer telling how many records we want to process for every iteration.";
            return false;
        }

        if (count($args) == 2) {
            if (HeDate::validDateSQL($args[1])) {
                $registrationDate = $args[1];
            } else {
                echo " The argument should be a valid SQL format date: YYYY-MM-DD";
                return false;
            }
        }

        /* ---Verification of Selligent--- */
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n Creating user for test Selligent server ... ";

        //
        //#5497
        echo " \n Verifying Selligent server connection... ";

        $commSelligent =    new SelligentConnectorCommon();
        $moUserTest =       new AbaUser();

        //
        // selligent.checkifexists@abaenglish.com
        if (!$moUserTest->getUserByEmail(Yii::app()->config->get("SELLIGENT_CHECKIFEXISTS_EMAIL"), "", false)) {
            $this->createUsersIntoDb(Yii::app()->config->get("SELLIGENT_CHECKIFEXISTS_EMAIL"), FREE);
        }

        if ($moUserTest->getUserByEmail(Yii::app()->config->get("SELLIGENT_CHECKIFEXISTS_EMAIL"), "", false)) {

            if (!$commSelligent->sendOperationToSelligent(
                SelligentConnectorCommon::checkIfUserExists,
                $moUserTest,
                array(),
                "RegisterSelligentUsersCommand"
            )
            ) {
                echo " \n Connection to Selligent Server for test user failed. Probably Selligent still is down.";
                return false;
            }
            echo " \n Connection to Selligent Server SUCCESSFULL. Let's start the creation of users in Selligent.";
        } else {
            echo " \n Failing when collecting user test from database. Process finished unsuccesfully.";
            $this->processError(" Failing when collecting user test from database. Process finished unsuccesfully. ");
            return false;
        }
        //

        $moUserTest = null;

        /*---- After validations and verification of Selligent communications we start to process users: ---*/
        $moUsers =          new AbaUsers();
        $aRowsUsersToSend = $moUsers->getAllUsersToCreateInSelligent($registrationDate, $limitUsers);
        if (!is_array($aRowsUsersToSend)) {
            echo " \n " . date("Y-m-d h:i:s", time()) . " There are no users to be sent to SELLIGENT. " .
              "It looks like everything is synchronized. Process RegisterSelligentUsersCommand. END";
            echo " \n -----------------------------------------------------------------------------------------------";
            return false;
        }

        $totalToSend =  count($aRowsUsersToSend);
        $aRowsNotSent = array();
        $totalSent =    0;
        foreach ($aRowsUsersToSend as $rowUser) {
            // u.`id`, u.`email`, u.`userType`, u.`dateLastSentSelligent`
            $moUserCurrent = new AbaUser();
            if ($moUserCurrent->getUserById($rowUser["id"])) {
                echo " ;\n Sending " . $moUserCurrent->email . " to Selligent. Result ";
                $commSelligent = new SelligentConnectorCommon();
                if ($commSelligent->sendOperationToSelligent(
                    SelligentConnectorCommon::registroWeb,
                    $moUserCurrent,
                    array(),
                    "RegisterSelligentUsersCommand"
                )
                ) {
                    echo " = OK ";
                    $moUserCurrent->dateLastSentSelligent = HeDate::todaySQL(true);
                    if ($moUserCurrent->update(array("dateLastSentSelligent"))) {
                        $totalSent = $totalSent + 1;
                    }
                } else {
                    echo " = " . $commSelligent->getResponse();
                    $aRowsNotSent[] = $moUserCurrent->email;
                    $moUserCurrent->dateLastSentSelligent = '0000-00-00';
                    $moUserCurrent->update(array("dateLastSentSelligent"));
                    $this->processError(
                        " User with email " . $moUserCurrent->email . " not correctly received by Selligent."
                    );
                }
            }
        }

        if ($totalSent !== $totalToSend) {
            echo " \n " . date(
                  "Y-m-d h:i:s",
                  time()
              ) . " The number of user SENT DOES NOT MATCH the user that should be sent to SELLIGENT, process RegisterSelligentUsersCommand. Check emails not sent: " .
              implode(", ", $aRowsNotSent) . " END";
            echo " \n  -----------------------------------------------------------------------------------------------";
            return false;
        }

        echo " \n " . date(
              "Y-m-d h:i:s",
              time()
          ) . " Success in process RegisterSelligentUsersCommand for registration $registrationDate. END";
        echo " \n -----------------------------------------------------------------------------------------------";
        return $allSuccess;

    }

    /**
     * @param string $actionDetails
     *
     * @return bool
     */
    private function processError($actionDetails)
    {
        return true;
    }

    /** Creates a user Test
     * @param      $email
     * @param      $userType
     *
     * @return bool
     */
    private function createUsersIntoDb($email, $userType)
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail($email, "", false);

        $moUser->email =            $email;
        $moUser->userType =         $userType;
        $moUser->countryId =        COUNTRY_SPAIN;
        $moUser->countryIdCustom =  $moUser->countryId;
        $moUser->currentLevel =     6;
        $moUser->entryDate =        HeDate::todaySQL(true);
        if ($userType == PREMIUM) {
            $moUser->expirationDate = HeDate::getDateAdd(HeDate::todaySQL(false), 5 /*days*/);
        } else {
            $moUser->expirationDate = HeDate::todaySQL(false);
        }
        $moUser->teacherId =    0;
        $moUser->name =         "Test.Register.Selligent";
        $moUser->surnames =     "Cron execution ";
        $moUser->langCourse =   "en";
        $moUser->langEnv =      "en";
        if ($userType == PREMIUM) {
            $moUser->idPartnerFirstPay =    Yii::app()->config->get("ABA_PARTNERID");
            $moUser->idPartnerCurrent =     Yii::app()->config->get("ABA_PARTNERID");
        } else {
            $moUser->idPartnerFirstPay =    null;
            $moUser->idPartnerCurrent =     null;
        }
        $moUser->idPartnerSource =  Yii::app()->config->get("ABA_PARTNERID");
        $moUser->telephone =        "931511515";
        $moUser->setPassword("123456789", true);
        $moUser->deviceTypeSource =     DEVICE_SOURCE_PC;

        $moUser->insertUserPremiumForRegistration();

        return true;
    }

}
