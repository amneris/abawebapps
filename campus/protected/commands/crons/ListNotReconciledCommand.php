<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 22/11/12
 * Time: 12:11
 */
class ListNotReconciledCommand extends AbaConsoleCommand
{

    public function getHelp()
    {
        return parent::getHelp() . "Finds all payments SUCCESS OR REFUNDS that have not been reconciliated for more than" .
        " XX days. Accepts parameter: days a payment was without reconciliation.";
    }

    public function run($args)
    {
        $allSuccess = true;
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n ListNotReconciledCommand: executing auto process at " . date("Y-m-d H:i:s", time());

        $daysOld = Yii::app()->config->get("PAY_ABA_RECONCILIATION_DAYS");
        if (count($args) > 0) {
            if (!is_numeric($args[0])) {
                echo 'Number is not valid. End of process.---------------';
                return false;
            }
            $daysOld = intval($args[0]);
        }
        $dateOffReconc = HeDate::getDateAdd(HeDate::todaySQL(false), -$daysOld /*days*/);

        $moPayments = new Payments();
        $aPaysNotReconc = $moPayments->getAllPaymentsPendingReconciliation($dateOffReconc);
        $aUserPayments = array();
        foreach ($aPaysNotReconc as $aPayment) {
            $aPayment["link"] = "<a href='http://intranet.abaenglish.com/index.php?r=abaPayments/view&id=" .
                $aPayment["id"] . "'>open</a>";
            $aUserPayments[$aPayment["id"]] = $aPayment;
        }

        if (count($aUserPayments) > 0) {
            $bodyDesc = "Below you can see the list of payments that after $daysOld days still have not been " .
                "reconciled by the AllPago and Caixa processes. Please review them,
                find them and finally go to the intranet
                and mark them as a reconcile. Otherwise they will keep being sent to your e-mail til the
                end of times.<br/>";
            $allSuccess = HeLogger::sendLog(HeLogger::PREFIX_NOTIF_H . " Process ListNotReconciled",
              HeLogger::FINANCE, HeLogger::CRITICAL,array("body" => $bodyDesc, "listTable" => $aUserPayments));
        }


        if (!$allSuccess) {
            $this->processEndsWithError("ListNotReconciledCommand did not finished success.");
            echo " \n ListNotReconciledCommand executed at " . date('Y-M-D H:m:s') . " with ERRORS.";
        }

        echo " \n  Finished process ListNotReconciledCommand at " . date('Y-M-D H:m:s');
        echo " \n -----------------------------------------------------------------------------------------------";
        return $allSuccess;
    }

    /**
     * @param $actionDetails
     * @return bool
     */
    private function processEndsWithError($actionDetails)
    {
        HeLogger::sendLog(
          HeLogger::PREFIX_ERR_UNKNOWN_H .
            " Cron ListNotReconciledCommand was executed and finished but processed some errors.",
            HeLogger::IT_BUGS,
            HeLogger::CRITICAL,
            $actionDetails
        );

        echo " \n " . date("Y-m-d H:i:s", time()) . " ListNotReconciledCommand finished with ERRORS : " .
            $actionDetails;
        echo " \n -----------------------------------------------------------------------------------------------";
        return true;
    }


}
