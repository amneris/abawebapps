<?php

class RenewalPaymentAppstoreCommand extends AbaConsoleCommand
{
    protected $idPaySupplier = PAY_SUPPLIER_APP_STORE;

    //
    // TEST:    php -f  /var/www/abawebapps/campus/public/script.php  RenewalPaymentAppstore
    //
    public function run($args)
    {
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n ProcessAppstoreRenewalsCommand: executing auto process at " . date("Y-m-d H:i:s", time());

        $allSuccess = true;

        /**@TODO - FECHAS !!! * */
        /**@TODO - FECHAS !!! * */
        /**@TODO - FECHAS !!! * */

        $today = HeDate::todaySQL(false);
//        $dateRequest = HeDate::getDateAdd($today, -1/*day*/);
//        $dateRequestFrom = $dateRequest . ' 00:00:00';
        $dateRequestFrom = $today . ' 00:00:00';
//        $dateRequestUntil = $dateRequest . ' 23:59:59';

        $moPayments = new Payments();

        $allPaymentsAppstore = $moPayments->getAllPaymentsPendingAppstore($dateRequestFrom);

        foreach ($allPaymentsAppstore as $iKey => $stPaymentAppstore) {

            $moPendingPay = $stPaymentAppstore['payment']; // PENDING payment
            $stAppStorePayment = $stPaymentAppstore['appStorePayment']; // AppStore payment related to PENDING payment

            if (trim($stAppStorePayment->purchaseReceipt) != '' AND strpos(
                  $stAppStorePayment->purchaseReceipt,
                  '{'
              ) !== false
            ) {

                $dateStart = HeDate::todaySQL(true);
                $user = new AbaUser();

                if (!is_numeric($moPendingPay->userId)) {
                    $allSuccess = false;
                    continue;
                }

                if (!$user->getUserById($moPendingPay->userId)) {
                    $this->processErrorNoRenewed(
                        $user,
                        $moPendingPay,
                      " User not found by id user of payment " . $moPendingPay->userId,
                        $dateStart
                    );
                    $allSuccess = false;
                    continue;
                }

                if (Yii::app()->config->get("ENABLE_LOG_USERACTIVITY") == 1) {
                    $user->abaUserLogUserActivity->insertLog(
                        "payments",
                        "",
                        " user susceptible of renewal, initiating process.",
                        "(AUTO)recurrent appstore payment"
                    );
                }

                $moProductUser = new ProductPrice();
                if (!$moProductUser->getProductById($moPendingPay->idProduct)) {
                    $this->processErrorNoRenewed(
                        $user,
                        $moPendingPay,
                      " Product not Found for the user  " . $user->email . ", product= " . $moPendingPay->idProduct,
                        $dateStart
                    );
                    $allSuccess = false;
                    continue;
                }

                //
                // PARSE JSON
                // CALL APPSTORE API
                /* @var PaySupplierAppstore $paySupplier */
                $paySupplier = PaymentFactory::createPayment(null, PAY_SUPPLIER_APP_STORE);

                if ($paySupplier->runRequestReconciliation($stAppStorePayment)) {
                    //
                    // Cancelled / Expired Receipt
                    //
                    if ($paySupplier->isExpiredReceipt()) {

                        $commUser = new UserCommon();

                        if ($commUser->cancelPaymentPending($moPendingPay->id, PAY_CANCEL_FAILED_RENEW)) {

                            $cmAbaPaymentAppstore = new AbaPaymentsAppstore();
                            $cmAbaPaymentAppstore->getPaymentAppstoreById($stAppStorePayment->id);
                            $cmAbaPaymentAppstore->updateToCancel();

                            if (!$commUser->cancelUser($moPendingPay->userId, PAY_CANCEL_FAILED_RENEW)) {
                                $this->processErrorAfterRenewed(
                                    $user,
                                    $moPendingPay,
                                  ' Process cancel user (' . $moPendingPay->userId . ') after cancel appstore payment (' . $stAppStorePayment->id . ') failed'
                                );
                                $allSuccess = false;
                            }

                            echo "\n+++++++++++EXPIRED RECEIPT - PAYMENT CANCELLED: " . $moPendingPay->id . "+++++++++++\n";

                        } else {
                            $this->processErrorNoRenewed(
                                $user,
                                $moPendingPay,
                              " User not found by id user of payment " . $moPendingPay->userId,
                                $dateStart
                            );
                            $allSuccess = false;
                        }
                    } elseif ($paySupplier->isValidRenewalReceipt()) {
                        //
                        // check, if is a valid renewal receipt
                        //
                        if (intval(Yii::app()->config->get("PAY_APP_STORE_ENABLE_RECURRENT")) == 1) {

                            $paySupplier->loadAppstorePendingPaymentJson($user);

                            $originalTransactionId = $paySupplier->getFirstTransactionId();
                            $newTransactionId = $paySupplier->getOrderNumber();

                            if ($originalTransactionId != $newTransactionId) {

                                $productId = $paySupplier->getProductId();
                                $originalProductId = $paySupplier->getOriginalProductId();

                                //
                                // product changed - update pending payment
                                //
                                if ($originalProductId != $productId) {

                                    $productsPricesAppstore = new ProductsPricesAppstore();
                                    $productsPricesAppstore->getProductTierByIdProductApp($productId);

                                    /** @TODO * */
                                    $countryId = $moPendingPay->idCountry;

//                                    $periodMonth = HePayments::periodToMonth($productsPricesAppstore->idPeriodPay);
                                    $periodMonth = HePayments::periodToMonth($moPendingPay->idPeriodPay);
                                    $currency = $moPendingPay->currencyTrans;

//                                    $periodMonth = $productsPricesAppstore->getPeriodMonths();
//                                    $currency = $productsPricesAppstore->getCurrency();

                                    //
                                    // Validate Product Periodicity
                                    $moPeriodicity = new ProdPeriodicity();
                                    if (!$moPeriodicity->getProdPeriodByMonths($periodMonth)) {
                                        $this->processErrorNoRenewed(
                                            $user,
                                            $moPendingPay,
                                          "CRON Process Renewal of payments for AppStore Gateway: " . $user->email . "The payment " .
                                          $moPendingPay->id . " with the amount " . $moPendingPay->amountPrice . " and date to pay was " . $moPendingPay->dateToPay .
                                          " into our database has failed. The period for this product is not valid. There is no such periodicity in the database with ID " . $periodMonth,
                                            $dateStart
                                        );
                                        $allSuccess = false;
                                        continue;
                                    }

                                    // Validate Product Price:
                                    $moProduct = new ProductPrice();
                                    if (!$moProduct->getProductByCountryIdPeriodId(
                                        $countryId,
                                        $moPeriodicity->id,
                                        PREMIUM
                                    )
                                    ) {
                                        $this->processErrorNoRenewed(
                                            $user,
                                            $moPendingPay,
                                          "CRON Process Renewal of payments for AppStore Gateway: " . $user->email . "The payment " .
                                          $moPendingPay->id . " with the amount " . $moPendingPay->amountPrice . " and date to pay was " . $moPendingPay->dateToPay .
                                          " into our database has failed. he product is not valid. There is no product in the database for ProductId = " . $productId,
                                            $dateStart
                                        );
                                        $allSuccess = false;
                                        continue;
                                    }

                                    //
                                    // get app store currency
                                    //
                                    $ABAIdCurrency = $moProduct->getAppStoreCurrency($currency);
                                    $xRates = new AbaCurrency($currency);

                                    $moPendingPay->idProduct = $moProduct->idProduct;
                                    $moPendingPay->idCountry = $countryId;
                                    $moPendingPay->idPeriodPay = $moPeriodicity->id;
                                    $moPendingPay->xRateToEUR = $xRates->getConversionRateTo(CCY_DEFAULT);
                                    $moPendingPay->xRateToUSD = $xRates->getConversionRateTo(CCY_2DEF_USD);
                                    $moPendingPay->currencyTrans = $ABAIdCurrency;
                                    $moPendingPay->foreignCurrencyTrans = $ABAIdCurrency;
//                                    $moPendingPay->amountOriginal = $productsPricesAppstore->priceAppStore;
//                                    $moPendingPay->amountPrice = $productsPricesAppstore->priceAppStore;

                                    $moPendingPay->updateAppstoreRecurringPayment(HeDate::todaySQL(true));
                                }

                                echo "\n+++++++++++RENEVAL RECEIPT+++++++++++\n";

                                $commRecurrPayment = new RecurrentPayCommon();

                                $retSucc = $commRecurrPayment->renewPaymentByPaySuppOrderId(
                                    $user->email,
                                    $moPendingPay->paySuppOrderId,
                                    $dateStart,
                                    "recurring_payment_APPSTORE",
                                    PAY_SUPPLIER_APP_STORE,
                                    $moPendingPay->id,
                                    array()
                                );

                                if (!$retSucc) {
                                    $this->processErrorAfterRenewed(
                                        $user,
                                        $moPendingPay,
                                      " Error in renewal on AppStore process. User " . $user->email . " payment= " .
                                      $moPendingPay->id . " has been carried out successfully in AppStore but processing " .
                                      "into our database has failed. Please review renewal data. More Details= " . $commRecurrPayment->getErrorMessage(
                                      )
                                    );
                                    $allSuccess = false;
                                } else {
                                    //
                                    // Create next AppStore payment
                                    //
                                    $oAbaPaymentsAppstore = new AbaPaymentsAppstore();
                                    if ($oAbaPaymentsAppstore->createDuplicateNextAppstorePayment(
                                        $user,
                                        $paySupplier,
                                        $moPendingPay
                                    )
                                    ) {
                                        $stAppStorePayment->updateToSuccess();
                                    }

                                    /**@TODO - REVISAR !!! * */
                                    /**@TODO - REVISAR !!! * */
                                    /**@TODO - REVISAR !!! * */
                                    $moPendingPay->paySuppExtUniId = $newTransactionId;
                                    $moPendingPay->update(array("paySuppExtUniId"));
                                }
                            } else {

                                echo "\n+++++++++++ NO CHANGES FOR TRANSACTION '" . $originalTransactionId . "' ++++++++++++++++++\n";

                                $this->processErrorAfterRenewed(
                                    $user,
                                    $moPendingPay,
                                  "CRON Process Renewal of payments for AppStore Gateway: " . $user->email . "The payment " .
                                  $moPendingPay->id . " with the amount " . $moPendingPay->amountPrice . " and date to pay was " . $moPendingPay->dateToPay .
                                  "... NO CHANGES FOR TRANSACTION '" . $originalTransactionId . "' "
                                );
                            }
                        } else {
                            $this->processErrorAfterRenewed(
                                $user,
                                $moPendingPay,
                              "CRON Process Renewal of payments for AppStore Gateway: " . $user->email . "The payment " .
                              $moPendingPay->id . " with the amount " . $moPendingPay->amountPrice . " and date to pay was " . $moPendingPay->dateToPay .
                              " should go under real transaction now. Disabled by PAY_APP_STORE_ENABLE_RECURRENT"
                            );
                            $allSuccess = false;
                        }
                    } else {
                        $this->processErrorAfterRenewed(
                            $user,
                            $moPendingPay,
                          " Error in renewal on AppStore process. User " . $user->email . " payment= " . $moPendingPay->id .
                          " has been carried out successfully in AppStore but processing " .
                          "into our database has failed. Please review renewal data. More Details= " . $paySupplier->getErrorCode(
                          ) . ": " . $paySupplier->getErrorString()
                        );
                        $allSuccess = false;
                    }
                } else {
                    $allSuccess = false;
                }
            }
        }

        echo " \n  Finished process ProcessAppstoreRenewalsCommand at " . date('Y-M-D H:m:s');
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
        return $allSuccess;
    }

    /**
     * @param AbaUser $user
     * @param Payment $payment
     * @param $actionDetails
     * @param null $dateStart
     *
     * @return bool
     */
    private function processErrorNoRenewed(AbaUser $user, Payment $payment, $actionDetails, $dateStart = null)
    {
        if (intval(Yii::app()->config->get("PAY_APP_STORE_ENABLE_RECURRENT")) == 1) {
            // @TODO Replace function updateFailedRenewPay by RecurringCommon::failRecurring
            // Payment has failed to be renewed by Appstore, update  Payment with failure.
            $fUpdPay = $payment->updateFailedRenewPay($dateStart, $actionDetails);
            // User is to be cancelled, means to change to FREE again.
            $commUser = new UserCommon();
            $commUser->cancelUser($user->getId(), PAY_CANCEL_FAILED_RENEW);
        }

        echo "\n>> VVV" . " Error processing, NOT RENEWED, user DID NOT PAID and WAS CANCELLED in DATABASE. " .
          " NO NEED TO BE REVIEWED BY IT, payment id " . $payment->id . "\n";
        return true;
    }

    /**
     * @param AbaUser $user
     * @param Payment $payment
     * @param         $actionDetails
     *
     * @return bool
     */
    private function processErrorAfterRenewed(AbaUser $user, Payment $payment, $actionDetails)
    {
        HeLogger::sendLog("CRON User Recurring Payment AppStore successful but finished with errors " . $user->email . ", It MUST be REVIEWED payment id=" . $payment->id,
        HeLogger::IT_BUGS_PAYMENTS, HeLogger::CRITICAL,
          " User Recurring Payment AppStore successful but finished with errors " . $user->email . ", payment id=" . $payment->id . " $actionDetails ");
        echo ">> XXX" . " Error processing, RENEWED in database but with failures. It MUST be REVIEWED: payment id " . $payment->id;
        return true;
    }

}
