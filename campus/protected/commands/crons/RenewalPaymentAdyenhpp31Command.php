<?php

date_default_timezone_set('Europe/Madrid');

class RenewalPaymentAdyenhpp31Command extends AbaConsoleCommand
{
    public function run($args)
    {
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n RenewalPaymentAdyenCommand: executing auto process at ".date("Y-m-d H:i:s", time() );
        $allSuccess = true;

        //
        // /usr/bin/php  campus/public/cron.php  RenewalPaymentAdyenhpp31  "2017-04-05"  5
        //

        $dateLimit = HeDate::todaySQL(false);
        if (count($args) > 0) {
            if (!HeDate::validDateSQL($args[0])) {
                echo 'Date is not valid. Please respect format YYYY-MM-DD';
                return false;
            }
            $dateLimit = substr($args[0], 0, 10);
        }

        $limitSql = 1000;
        if (count($args) > 1) {
            if (!is_numeric($args[1])) {
                echo 'Limit is not valid.';
                return false;
            }
            $limitSql = $args[1];
        }


        // 1- Collect all Adyen Payments Supplier=10, status = PENDING, dateToPay>=Today; Order by dateToPay ASC, dateStartTransaction ASC
        $moPayments = new Payments();
        /*  @var Payment[] $aPendingPays    */
        $aPendingPays = $moPayments->getAllPaymentsPendingAdyenhpp31($dateLimit, $limitSql);
        if( !$aPendingPays ) {
// No Payments pending for Adyen;
            echo " \n There are no users to renew today. Success on executing RenewalPaymentAdyenCommand";
            echo " \n -----------------------------------------------------------------------------------------------";
            return true;
        }

//        /* @var Payment $moPendingPay */
        foreach($aPendingPays as $aPendingPay)
        {
            //
            //
            if(isset($aPendingPay['idHppRenewal']) AND is_numeric(trim($aPendingPay['idHppRenewal']))) {
                continue;
            }

            //
            //
            $moPendingPay = new Payment();
            if (!$moPendingPay->getPaymentById($aPendingPay["id"])) {
//                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $aPendingPay["id"]);
                continue;
            }

            //
            //
            echo " \n ". date("Y-m-d H:m:s")." Processing the renewal of payment id ".$moPendingPay->id." of user ".$moPendingPay->userId;

            $dateStart = HeDate::todaySQL(true);
            /*  Para cada pago */
            // 2-Collect Datos de pago para el usuario de este pago
            $user = new AbaUser( );
            if( $moPendingPay->userId=='' || !is_numeric($moPendingPay->userId) ) {
                throw new CDbException("ABA, User Id present in payment ".$moPendingPay->id." is not VALID AT ALL.");
            }

            if( !$user->getUserById( $moPendingPay->userId ) ) {
                $allSuccess = false;
                continue;
            }

            if(Yii::app()->config->get("ENABLE_LOG_USERACTIVITY") == 1) {
                $user->abaUserLogUserActivity->insertLog("payments",""," user susceptible of renewal, initiating process.","(AUTO)recurrent payment");
            }

            $moUserCredit = new AbaUserCreditForms(  );
            if( !$moUserCredit->getUserCreditFormByUserId($moPendingPay->userId) )
            {
                $allSuccess = false;
                continue;
            }
            // 3-Validar Datos de Producto para este usuario/pago
            $moProductUser = new ProductPrice();
            if( !$moProductUser->getProductById( $moPendingPay->idProduct ) )
            {
                $allSuccess = false;
                continue;
            }

            //  Starts the execution that affects the database data related with these next payments pending:
            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            if( intval( Yii::app()->config->get("ENABLE_ADYEN_PAY_RECURRENT") ) == 1)
            {
                // 4 - Preparar SOAP y validarlo
                // 5 - Insertar petición en Log de Adyen
                // 6 - Enviar petición de Pago
                // 7 - Guardar Reponse en Log
                $moPaymentControlCheck = new PaymentControlCheck();
                if( !$moPaymentControlCheck->createPayControlCheckFromPayment( $user->id, 1, $moPendingPay->idProduct, $moPendingPay->idCountry, $moPendingPay->idPeriodPay,
                  $moPendingPay->idPromoCode, $moUserCredit->kind, $moUserCredit->cardName, $moUserCredit->cardNumber , $moUserCredit->cardYear, $moUserCredit->cardMonth,
                  $moUserCredit->cardCvc, $moPendingPay->paySuppExtId, $moPendingPay, '', '', false ) )
                {
                    $allSuccess = false;
                    continue;
                }

                /* @var PaySupplierAdyenHpp $paySupplier */
                $paySupplier = PaymentFactory::createPayment( $moPendingPay );

                /* @var PayGatewayLogAdyenResRec $retPaymentSupplier */
                $retPaymentSupplier = $paySupplier->runDebitingRenewal( $moPaymentControlCheck, $user, $moPendingPay );

                if( !$retPaymentSupplier )
                {
                    $errPaymentGateway = Yii::t('mainApp', 'gateway_recurrent_failure_key').
                      " ( Response from PaymentSupplier = ".$paySupplier->getErrorCode()."-".$paySupplier->getErrorString()." )";
                    $moPendingPay->paySuppOrderId = $paySupplier->getOrderNumber();
                    $allSuccess = false;
                    continue;
                }

                //
                //
                try {
                    $oPaymentAdyenRenewals = new AbaPaymentsAdyenRenewals();

                    $oPaymentAdyenRenewals->idPayment =         $moPendingPay->id;
//                    $oPaymentAdyenRenewals->merchantReference = $moPendingPay->paySuppOrderId;
                    $oPaymentAdyenRenewals->merchantReference = $paySupplier->getOrderNumber();
                    $oPaymentAdyenRenewals->status =            AbaPaymentsAdyenRenewals::STATUS_HPP_RENEWALS_PENDING;
                    $oPaymentAdyenRenewals->requestDate =       $dateStart;

                    $oPaymentAdyenRenewals = $oPaymentAdyenRenewals->insertPaymentAdyen();

                    if( $oPaymentAdyenRenewals && $oPaymentAdyenRenewals instanceof AbaPaymentsAdyen ) { }

                } catch (Exception $e) { }

            }
            else {
                HeLogger::sendLog("CRON Process Renewal of payments for Adyen Gateway: ".$user->email,
                  HeLogger::IT_BUGS_PAYMENTS, HeLogger::INFO,
                  " The payment ".$moPendingPay->id." with the amount ".$moPendingPay->amountPrice." and date to pay was ".$moPendingPay->dateToPay." should go under real transaction now. Disabled by ENABLE_ADYEN_PAY_RECURRENT");
            }

            echo " >> ". date("Y-m-d H:m:s")." Successful processed user ".$user->email." for payment with id= ".$moPendingPay->id;
        }

        if( !$allSuccess ) {
            echo " \n \n  Renewal of Adyen executed at ".date('Y-M-D H:m:s')." with ERRORS for some payments. ";
        }

        echo " \n  Finished process at ".date('Y-M-D H:m:s');
        echo " \n -----------------------------------------------------------------------------------------------";
        return $allSuccess;
    }

}
