<?php

class RenewalPaymentPlayStoreTestCommand extends AbaConsoleCommand
{
    protected $idPaySupplier = PAY_SUPPLIER_ANDROID_PLAY_STORE;

    //
    // TEST:    php -f  campus/public/cron.php  RenewalPaymentPlayStoreTest
    //
    public function run($args)
    {
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n Process PlayStoreRenewalsCommand: executing auto process at " . date("Y-m-d H:i:s", time());


$sUserIds = "";

if(count($args) > 0) {
    if(trim($args[0]) == ''){
        echo "\n1. Not valid user[s]. Please respect format USERID[,USERID,USERID]\n";
        return false;
    }
    $sUserIds = $args[0];
}
else {
    echo "\n2. Not valid user[s]. Please respect format USERID[,USERID,USERID]\n";
    return false;
}

//
//$milesegons = 1453024537790 / 1000;
//echo "\n+++ MILL: " . date("Y-m-d H:i:s", $milesegons);
//// $milesegons = strtotime(HeDate::AddSeconds(60 * 60 * 24 * 31, date("Y-m-d H:i:s", $milesegons))); // +31D
//
//
//$FECHA = "2016-01-17 10:55:37";
//$FECHA = "2016-01-17 04:55:36";
//$iMargeMinutes = Yii::app()->config->get("PAY_ANDROID_EXPIRETIMEMILLIS_MARGE_MINUTS");
//
//echo "\n+++ MILL++: " . date("Y-m-d H:i:s", $milesegons);
//echo "\n+++ FECHA: " . $FECHA;
//$expiryTimeMillis = HeDate::AddSeconds(60 * ($iMargeMinutes), $FECHA);
//echo "\n+++ FECHA++: " . $expiryTimeMillis;
//
//
//if (strtotime(date("Y-m-d H:i:s", $milesegons)) <= strtotime($expiryTimeMillis)
//) {
//    echo "\n+++ EXPIRADO +++\n";
//}
//else {
//    echo "\n+++ OKS +++\n";
//}
//
//return false;


        $allSuccess = true;

        $today = HeDate::todaySQL(false);
//        $dateRequest = HeDate::getDateAdd($today, -1/*day*/);
//        $dateRequestFrom = $dateRequest . ' 00:00:00';
        $dateRequestFrom = $today . ' 00:00:00';
//        $dateRequestUntil = $dateRequest . ' 23:59:59';

        $moPayments = new Payments();

        $allPaymentsPlayStoretore = $moPayments->getAllPaymentsPendingPlayStoreTest($dateRequestFrom, $sUserIds);


//
//$iMargeMinutes = Yii::app()->config->get("PAY_ANDROID_EXPIRETIMEMILLIS_MARGE_MINUTS");
//
//$sAbaexpiryTimeMillis = "2016-06-27 22:24:29";
//$sAndroidexpiryTimeMillis = "2016-06-28 00:47:20";
//$sAndroidexpiryTimeMillis = "2016-06-28 04:24:29"; // iguales
//$sAndroidexpiryTimeMillis = "2016-06-28 04:24:30"; // + 1 segundo
//
//echo "\n+++ +" . ( $iMargeMinutes / 60 ) . "M";
//echo "\n+++ ANTERIOR: " . $sAbaexpiryTimeMillis ;
//echo "\n+++ ACTUAL:   " . $sAndroidexpiryTimeMillis . " +++\n";
//
//$sAbaexpiryTimeMillis = HeDate::AddSeconds(60 * ($iMargeMinutes), $sAbaexpiryTimeMillis);
//
//echo "\n+++ ANTERIOR: " . $sAbaexpiryTimeMillis;
//echo "\n+++ ACTUAL:   " . $sAndroidexpiryTimeMillis;
//echo "\n+++ ANTERIOR: " . strtotime($sAbaexpiryTimeMillis);
//echo "\n+++ ACTUAL:   " . strtotime($sAndroidexpiryTimeMillis);
//echo "\n+++ ANTERIOR: " . date('Y-m-d H:i:s', strtotime($sAbaexpiryTimeMillis));
//echo "\n+++ ACTUAL:   " . date('Y-m-d H:i:s', strtotime($sAndroidexpiryTimeMillis));
//echo " +++\n";
//
//if(strtotime($sAndroidexpiryTimeMillis) <= strtotime($sAbaexpiryTimeMillis)){
//    echo "\n++++++La fecha de renovacion ya ha pasado++++++\n";
//}else{
//    echo "\n++++++Aun falta algun tiempo++++++\n";
//}
//
//// return false;


        /* @var AbaPaymentsPlaystore $stPlayStorePayment */
        /* @var Payment $moPendingPay */

        foreach ($allPaymentsPlayStoretore as $iKey => $stPaymentPlayStore) {

            $moPendingPay = $stPaymentPlayStore['payment']; // PENDING payment
            $stPlayStorePayment = $stPaymentPlayStore['PlayStorePayment']; // AppStore payment related to PENDING payment

            if (trim($stPlayStorePayment->purchaseReceipt) != '' AND strpos(
                  $stPlayStorePayment->purchaseReceipt,
                  '{'
              ) !== false
            ) {

                $dateStart = HeDate::todaySQL(true);
                $user = new AbaUser();

                if (!is_numeric($moPendingPay->userId)) {
                    $allSuccess = false;
                    continue;
                }

                if (!$user->getUserById($moPendingPay->userId)) {
                    $this->processErrorNoRenewed(
                        $user,
                        $moPendingPay,
                      " User not found by id user of payment " . $moPendingPay->userId,
                        $dateStart
                    );
                    $allSuccess = false;
                    continue;
                }

                if (Yii::app()->config->get("ENABLE_LOG_USERACTIVITY") == 1) {
                    $user->abaUserLogUserActivity->insertLog(
                        "payments",
                        "",
                        " user susceptible of renewal, initiating process.",
                        "(AUTO)recurrent appstore payment"
                    );
                }

                $moProductUser = new ProductPrice();
                if (!$moProductUser->getProductById($moPendingPay->idProduct)) {
                    $this->processErrorNoRenewed(
                        $user,
                        $moPendingPay,
                      " Product not Found for the user  " . $user->email . ", product= " . $moPendingPay->idProduct,
                        $dateStart
                    );
                    $allSuccess = false;
                    continue;
                }

                //
                // PARSE JSON
                // CALL APPSTORE API
                /* @var PaySupplierAppstore $paySupplier */

                $tienda= new HePlayStore();
                //$tienda->connect($stPlayStorePayment->productId,$stPlayStorePayment->purchaseToken);
                //$paySupplier = PaymentFactory::createPayment(null, PAY_SUPPLIER_ANDROID_PLAY_STORE);

//
//$iMargeMinutes = Yii::app()->config->get("PAY_ANDROID_EXPIRETIMEMILLIS_MARGE_MINUTS");
//
//$sAbaexpiryTimeMillis = "2016-06-27 22:24:29";
//$sAndroidexpiryTimeMillis = "2016-06-28 00:47:20";
//$sAndroidexpiryTimeMillis = "2016-06-28 04:24:29"; // iguales
//$sAndroidexpiryTimeMillis = "2016-06-28 04:24:30"; // + 1 segundo
//
//echo "\n+++ +" . ( $iMargeMinutes / 60 ) . "M";
//echo "\n+++ ANTERIOR: " . $sAbaexpiryTimeMillis ;
//echo "\n+++ ACTUAL:   " . $sAndroidexpiryTimeMillis . " +++\n";
//
//$sAbaexpiryTimeMillis = HeDate::AddSeconds(60 * ($iMargeMinutes), $sAbaexpiryTimeMillis);
//
//echo "\n+++ ANTERIOR: " . $sAbaexpiryTimeMillis;
//echo "\n+++ ACTUAL:   " . $sAndroidexpiryTimeMillis;
//echo "\n+++ ANTERIOR: " . strtotime($sAbaexpiryTimeMillis);
//echo "\n+++ ACTUAL:   " . strtotime($sAndroidexpiryTimeMillis);
//echo "\n+++ ANTERIOR: " . date('Y-m-d H:i:s', strtotime($sAbaexpiryTimeMillis));
//echo "\n+++ ACTUAL:   " . date('Y-m-d H:i:s', strtotime($sAndroidexpiryTimeMillis));
//echo " +++\n";
//
//if(strtotime($sAndroidexpiryTimeMillis) <= strtotime($sAbaexpiryTimeMillis)){
//echo "\n++++++La fecha de renovacion ya ha pasado++++++\n";
//}else{
//echo "\n++++++Aun falta algun tiempo++++++\n";
//}
//
//return false;

                if ($tienda->Subscription($stPlayStorePayment->productId, $stPlayStorePayment->purchaseToken)) {

                    $iMargeMinutes = Yii::app()->config->get("PAY_ANDROID_EXPIRETIMEMILLIS_MARGE_MINUTS");

                    $expiryTimeMillis = HeDate::AddSeconds(60 * ($iMargeMinutes), $stPlayStorePayment->expiryTimeMillis);

                    //
                    // Cancelled / Expired Receipt
                    // !$tienda->getAutoRenewing() ||
                    if (strtotime(date("Y-m-d H:i:s",
                        ($tienda->getExpiryTimeMillis() / 1000))) <= strtotime($expiryTimeMillis)
                    ) {

                        $commUser = new UserCommon();

                        if ($commUser->cancelPaymentPending($moPendingPay->id, PAY_CANCEL_FAILED_RENEW)) {

                            $cmAbaPaymentPlayStore = new AbaPaymentsPlaystore();
                            $cmAbaPaymentPlayStore->getPaymentPlayStoreById($stPlayStorePayment->id);
                            $cmAbaPaymentPlayStore->updateToCancel();

                            if (!$commUser->cancelUser($moPendingPay->userId, PAY_CANCEL_FAILED_RENEW)) {
                                $this->processErrorAfterRenewed(
                                    $user,
                                    $moPendingPay,
                                  ' Process cancel user (' . $moPendingPay->userId . ') after cancel appstore payment (' . $stPlayStorePayment->id . ') failed'
                                );
                                $allSuccess = false;
                            }

                            echo "\n+++++++++++EXPIRED RECEIPT - PAYMENT CANCELLED: " . $moPendingPay->id . "+++++++++++\n";

                        } else {
                            $this->processErrorNoRenewed(
                                $user,
                                $moPendingPay,
                              " User not found by id user of payment " . $moPendingPay->userId,
                                $dateStart
                            );
                            $allSuccess = false;
                        }
                    } else {
                        //
                        // check, if is a valid renewal receipt
                        //
                        if (intval(Yii::app()->config->get("PAY_PLAY_STORE_ENABLE_RECURRENT")) == 1) {

                            //$paySupplier->loadAppstorePendingPaymentJson($user);

                            //$originalTransactionId = $paySupplier->getFirstTransactionId();
                            //$newTransactionId = $paySupplier->getOrderNumber();
                            $originalTransactionId = $stPlayStorePayment->orderId;

                            if ($pos = strpos($originalTransactionId, '..')){
                                $renewalNum= substr($originalTransactionId,$pos+2);
                                $baseOrderId=substr($originalTransactionId,0,$pos);
                                $newTransactionId=$baseOrderId.'..'.($renewalNum+1);
                            }else{
                                $newTransactionId=$originalTransactionId.'..0';
                            }

                            if ($originalTransactionId != $newTransactionId) {

                                echo "\n+++++++++++RENEVAL RECEIPT+++++++++++\n";

                                $commRecurrPayment = new RecurrentPayCommon();

                                $retSucc = $commRecurrPayment->renewPaymentByPaySuppOrderId(
                                    $user->email,
                                    $moPendingPay->paySuppOrderId,
                                    $dateStart,
                                    "recurring_payment_PLAYSTORE",
                                    PAY_SUPPLIER_ANDROID_PLAY_STORE,
                                    $moPendingPay->id,
                                    array()
                                );

                                if (!$retSucc) {
                                    $this->processErrorAfterRenewed(
                                        $user,
                                        $moPendingPay,
                                      " Error in renewal on AppStore process. User " . $user->email . " payment= " .
                                      $moPendingPay->id . " has been carried out successfully in AppStore but processing " .
                                      "into our database has failed. Please review renewal data. More Details= " . $commRecurrPayment->getErrorMessage(
                                      )
                                    );
                                    $allSuccess = false;
                                } else {
                                    //
                                    // Create next AppStore payment
                                    //

                                    //ojo necesitamos el nuevo orderID


                                    $oAbaPaymentsPlayStore = new AbaPaymentsPlaystore();

                                    $oAbaPaymentsPlayStore = clone $stPlayStorePayment;
                                    unset($oAbaPaymentsPlayStore->id);

                                    if ($oAbaPaymentsPlayStore->createDuplicateNextPlayStorePayment(
                                      $user,
                                      $stPlayStorePayment,
                                      $moPendingPay,
                                      $newTransactionId,
                                      $tienda
                                    )
                                    ) {
                                        $stPlayStorePayment->updateToSuccess();
                                    }


                                    /**@TODO - REVISAR !!! * */
                                    /**@TODO - REVISAR !!! * */
                                    /**@TODO - REVISAR !!! * */
                                    $moPendingPay->paySuppExtUniId = $newTransactionId;
                                    $moPendingPay->update(array("paySuppExtUniId"));
                                }
                            } else {

                                echo "\n+++++++++++ NO CHANGES FOR TRANSACTION '" . $originalTransactionId . "' ++++++++++++++++++\n";

                                $this->processErrorAfterRenewed(
                                    $user,
                                    $moPendingPay,
                                  "CRON Process Renewal of payments for AppStore Gateway: " . $user->email . "The payment " .
                                  $moPendingPay->id . " with the amount " . $moPendingPay->amountPrice . " and date to pay was " . $moPendingPay->dateToPay .
                                  "... NO CHANGES FOR TRANSACTION '" . $originalTransactionId . "' "
                                );
                            }
                        } else {
                            $this->processErrorAfterRenewed(
                                $user,
                                $moPendingPay,
                              "CRON Process Renewal of payments for AppStore Gateway: " . $user->email . "The payment " .
                              $moPendingPay->id . " with the amount " . $moPendingPay->amountPrice . " and date to pay was " . $moPendingPay->dateToPay .
                              " should go under real transaction now. Disabled by PAY_APP_STORE_ENABLE_RECURRENT"
                            );
                            $allSuccess = false;
                        }
                    }
                } else {
                    $allSuccess = false;
                }
            }
        }

        echo " \n  Finished process ProcessPlayStoreRenewalsCommand at " . date('Y-M-D H:m:s');
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
        return $allSuccess;
    }

    /**
     * @param AbaUser $user
     * @param Payment $payment
     * @param $actionDetails
     * @param null $dateStart
     *
     * @return bool
     */
    private function processErrorNoRenewed(AbaUser $user, Payment $payment, $actionDetails, $dateStart = null)
    {
        if (intval(Yii::app()->config->get("PAY_PLAY_STORE_ENABLE_RECURRENT")) == 1) {
            // @TODO Replace function updateFailedRenewPay by RecurringCommon::failRecurring
            // Payment has failed to be renewed by Appstore, update  Payment with failure.
            $fUpdPay = $payment->updateFailedRenewPay($dateStart, $actionDetails);
            // User is to be cancelled, means to change to FREE again.
            $commUser = new UserCommon();
            $commUser->cancelUser($user->getId(), PAY_CANCEL_FAILED_RENEW);
        }

        echo "\n>> VVV" . " Error processing, NOT RENEWED, user DID NOT PAID and WAS CANCELLED in DATABASE. " .
          " NO NEED TO BE REVIEWED BY IT, payment id " . $payment->id . "\n";
        return true;
    }

    /**
     * @param AbaUser $user
     * @param Payment $payment
     * @param         $actionDetails
     *
     * @return bool
     */
    private function processErrorAfterRenewed(AbaUser $user, Payment $payment, $actionDetails)
    {
        HeLogger::sendLog(
          HeLogger::PREFIX_ERR_UNKNOWN_H . ': CRON Recurring Payment AppStore finished with errors',
          HeLogger::IT_BUGS_PAYMENTS,
          HeLogger::CRITICAL,
          " User Recurring Payment AppStore successful but finished with errors " . $user->email . ", payment id=" . $payment->id . " $actionDetails "
        );

        echo ">> XXX" . " Error processing, RENEWED in database but with failures. It MUST be REVIEWED: payment id " . $payment->id;
        return true;
    }

}
