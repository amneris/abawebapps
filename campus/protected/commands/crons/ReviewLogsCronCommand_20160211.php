<?php
/** It sends a message to anyone within abaenglish.com domain only
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 22/11/12
 * Time: 12:11
 */
class ReviewLogsCronCommand extends AbaConsoleCommand
{
    public function getHelp()
    {
        return parent::getHelp()." \n ";
    }

    public function run($args)
    {
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n ReviewLogsCron: Executing auto process at ".date("Y-m-d H:i:s", time());

        try {
            $moCronLog = new LogCron();
            $aCronsUnfinished = $moCronLog->getAllCronsUnfinished($this->moLogCron->id);
            $strCrons = " REVIEW of CRONS execution on ".HeDate::todaySQL(true);

            if ($aCronsUnfinished!==false) {
                foreach ($aCronsUnfinished as $aCron) {
                    echo " \n  Detected process unfinished: ".$aCron["cronName"]." on day ".$aCron["dateStart"];
                    if ($aCron["success"]==0) {
                        $strCrons .= "<br/> *Unfinished cron execution: ";
                    }
                    $strCrons .= " <div> ";
                    $strCrons .= "<br/> Cron name= ".$aCron["cronName"] ;
                    $strCrons .= "<br/> Date Start Execution= ".$aCron["dateStart"] ;
                    $strCrons .= "<br/> Date End Execution= ".$aCron["dateEnd"] ;
                    $strCrons .= "<br/> Success= ".$aCron["success"] ;
                    $strCrons .= " </div>";

                    $moCronLog = new LogCron();
                    $moCronLog->getLogById($aCron["id"]);
                    $moCronLog->reviewed = 1;
                    $moCronLog->update(array("reviewed"));
                }

                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_UNKNOWN_H." Review of crons execution, some crons are NOT finishing.",
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    $strCrons
                );
            } else {
                echo " \n  All crons are under control since last review. GREAT!";
            }

        } catch (Exception $e) {
            $this->errorMessage = "Uncontrolled error on ReviewLogsCron: ".$e->getMessage();
        }

        echo " \n ".date("Y-m-d H:i:s", time())." Success in process ReviewLogsCron. END";
        echo " \n -----------------------------------------------------------------------------------------------";
    }
}
