<?php
/**
 * Select all users PREMIUM that his expiration date has been reached out.
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 22/11/12
 * Time: 12:11
 */
class ProcessExpiredPremiumUsersCommand extends AbaConsoleCommand
{
    public function getHelp()
    {
        return parent::getHelp()." paypal caixa groupon \n "." Use what kind of users pay methods you want to cancel.
                                                            Or none arguments if averything. \n Arguments accepted: \n".
            "paypal \n".
            "caixa \n".
            "groupon \n".
            "xfriends \n".
            "allpago \n".
            "b2b-extranet \n".
            "boleto \n"
            ;
    }

    private static $ALL="";

    //
    // TEST:    php -f  campus/public/cron.php  ProcessExpiredPremiumUsers  adyenhpp
    //
    public function run($args)
    {
      try{
echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
echo " \n ProcessExpiredPremiumUsersCommand: executing auto process at ".date("Y-m-d H:i:s", time() );
        $allSuccess = true;

        /*
         *  A. Select Premium users expired.
         * */
        $moUsers = new AbaUsers();
        /* u.`userType`, u.`expirationDate`,u.`id`, u.`name`, u.`surnames`,u.`email`, u.`countryId`, c.`name` */
        $paySuppExtIdToExpire = self::$ALL;
        if(count($args)>0)
        {
            if (array_search("paypal", $args) !== false) {
                if ($paySuppExtIdToExpire !== '') {
                    $paySuppExtIdToExpire .= ",";
                }
                $paySuppExtIdToExpire .= PAY_SUPPLIER_PAYPAL;
            }
            if (array_search("groupon", $args) !== false) {
                if ($paySuppExtIdToExpire !== '') {
                    $paySuppExtIdToExpire .= ",";
                }
                $paySuppExtIdToExpire .= PAY_SUPPLIER_GROUPON;
            }
            if (array_search("caixa", $args) !== false) {
                if ($paySuppExtIdToExpire !== '') {
                    $paySuppExtIdToExpire .= ",";
                }
                $paySuppExtIdToExpire .= PAY_SUPPLIER_CAIXA;
            }
            if (array_search("xfriends", $args) !== false) {
                if ($paySuppExtIdToExpire !== '') {
                    $paySuppExtIdToExpire .= ",";
                }
                $paySuppExtIdToExpire .= PAY_SUPPLIER_XFRIENDS;
            }
            if (array_search("allpago", $args) !== false) {
                if ($paySuppExtIdToExpire !== '') {
                    $paySuppExtIdToExpire .= ",";
                }
                $paySuppExtIdToExpire .= PAY_SUPPLIER_ALLPAGO_BR;
            }
            if (array_search("allpago-mx", $args) !== false) {
                if ($paySuppExtIdToExpire !== '') {
                    $paySuppExtIdToExpire .= ",";
                }
                $paySuppExtIdToExpire .= PAY_SUPPLIER_ALLPAGO_MX;
            }
            //
            // ENH-4251
            if (array_search("b2b-extranet", $args) !== false) {
                if ($paySuppExtIdToExpire !== '') {
                    $paySuppExtIdToExpire .= ",";
                }
                $paySuppExtIdToExpire .= PAY_SUPPLIER_B2B;
            }
            //
            // ENH-4512
            if (array_search("boleto", $args) !== false) {
                if ($paySuppExtIdToExpire !== '') {
                    $paySuppExtIdToExpire .= ",";
                }
                $paySuppExtIdToExpire .= PAY_SUPPLIER_ALLPAGO_BR_BOL;
            }
            //
            // ENH-APPSTORE
            if (array_search("appstore", $args) !== false) {
                if ($paySuppExtIdToExpire !== '') {
                    $paySuppExtIdToExpire .= ",";
                }
                $paySuppExtIdToExpire .= PAY_SUPPLIER_APP_STORE;
            }
            //
            // ENH-4400
            if (array_search("adyen", $args) !== false) {
                if ($paySuppExtIdToExpire !== '') {
                    $paySuppExtIdToExpire .= ",";
                }
                $paySuppExtIdToExpire .= PAY_SUPPLIER_ADYEN;
            }
            //
            // ENH-4986
            if (array_search("adyenhpp", $args) !== false) {
                if ($paySuppExtIdToExpire !== '') {
                    $paySuppExtIdToExpire .= ",";
                }
                $paySuppExtIdToExpire .= PAY_SUPPLIER_ADYEN_HPP;
            }
            //
            // ABAWEBAPPS-663
            if (array_search("playstore", $args) !== false) {
                if ($paySuppExtIdToExpire !== '') {
                    $paySuppExtIdToExpire .= ",";
                }
                $paySuppExtIdToExpire .= PAY_SUPPLIER_ANDROID_PLAY_STORE;
            }
            //
            // ZOR-500
            if (array_search("zuora", $args) !== false) {
                if ($paySuppExtIdToExpire !== '') {
                    $paySuppExtIdToExpire .= ",";
                }
                $paySuppExtIdToExpire .= PAY_SUPPLIER_Z;
            }
        }
        $aExpiredUsers = $moUsers->getAllUsersPremiumExpired($paySuppExtIdToExpire);
        if(!$aExpiredUsers){

echo " \n There are no expired users. Success on executing ProcessExpiredPremiumUsersCommand";
echo " \n -----------------------------------------------------------------------------------------------";
            return true;
        }

         /* B. Seleccionar el último pago que tienen realizado indiferente a su status.*/
echo " \n There are several users, processExpiredPremiumUsersCommand, pay methods ($paySuppExtIdToExpire): running through users supposedly expired";

        $aUserPayments = array();
        foreach( $aExpiredUsers as $aUser )
        {
            $aUserPayments[$aUser["email"]] = array();
            $aUserPayments[$aUser["email"]]["email"] = $aUser["email"];
            $aUserPayments[$aUser["email"]]["name"] = $aUser["name"];
            $aUserPayments[$aUser["email"]]["surnames"] = $aUser["surnames"];
            $aUserPayments[$aUser["email"]]["countryName"] = $aUser["countryName"];

            $moPayment = new Payment();
            $thereIsPays= $moPayment->getLastPaymentByUserId( $aUser["id"] );
            /* p.`idProduct`, p.`status`, p.`amountPrice`, ps.`name` as 'supplierName', p.`paySuppOrderId` */
            if (!$thereIsPays) {
                $aUserPayments[$aUser["email"]]["idProduct"] = "NOT FOUND";
                $aUserPayments[$aUser["email"]]["status"] = "NO PAYMENT FOUND";
                $aUserPayments[$aUser["email"]]["amountPrice"] = "N.F.I.";
                $aUserPayments[$aUser["email"]]["nameSupplier"] = " - ";
                $aUserPayments[$aUser["email"]]["dateToPay"] = "NO PAYMENTS";
                $aUserPayments[$aUser["email"]]["expirationDate"] = $aUser["expirationDate"];
            } else {
                $aUserPayments[$aUser["email"]]["idProduct"] = $moPayment->idProduct;
                $aUserPayments[$aUser["email"]]["status"] = $moPayment->status;
                $aUserPayments[$aUser["email"]]["amountPrice"] = $moPayment->amountPrice;
                /** @noinspection PhpUndefinedFieldInspection */
                $aUserPayments[$aUser["email"]]["supplierName"] = $moPayment->supplierName;
                $aUserPayments[$aUser["email"]]["dateToPay"] = $moPayment->dateToPay;
                $aUserPayments[$aUser["email"]]["expirationDate"] = $aUser["expirationDate"];

            }

            // Cancels the user, userType=1:
            $cancelReason = PAY_CANCEL_UNDEFINED;
            /** @noinspection PhpUndefinedFieldInspection */
            if($paySuppExtIdToExpire == PAY_SUPPLIER_GROUPON ||
                ($thereIsPays && $moPayment->supplierName=='Groupon') )
            {
                $cancelReason = PAY_CANCEL_DATEOFF_GROUPON;
            }
            elseif( $paySuppExtIdToExpire==PAY_SUPPLIER_XFRIENDS )
            {
                $cancelReason = PAY_CANCEL_UNDEFINED;
            }

            $commUser = new UserCommon();
            $commUser->cancelUser( $aUser["id"], $cancelReason );
        }

        /*
        * We assume that the reason of expiration will be that payment has failed for some reason OR that user belongs to a GROUPON.
        */

echo " \n ".date("Y-m-d H:i:s", time() )." Success in process ProcessExpiredPremiumUsers($paySuppExtIdToExpire). END";
echo " \n -----------------------------------------------------------------------------------------------";
        return $allSuccess;


      } catch(Exception $exc) {
          $this->processError("Big exception on ".$exc->getCode().", ".$exc->getMessage());
      }

        return false;
    }

    /**
     * @param string $actionDetails
     *
     * @return bool
     */
    private function processError($actionDetails)
    {
        HeLogger::sendLog("CRON ProcessExpiredPremiumUsersCommand finished with errors ", HeLogger::IT_BUGS,
          HeLogger::CRITICAL, " Collecting data from users on ProcessExpiredPremiumUsersCommand: ".$actionDetails);

        return true;
    }

}
