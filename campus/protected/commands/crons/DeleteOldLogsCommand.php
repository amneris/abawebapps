<?php
/**
 * Cron used to perform maintenance(deletion) on log tables.
 *
 * In the table aba_b2c.config there is a key/value pair (DELETION_LOGS_DURATION) defining what log tables should be
 * truncated and the cut off in days, in the following form: <table_name>.<date_column>#<number_of_days_to keep> so
 * log_course_api.dateRequest#7 translates to: delete from log_course_api where dateRequest is older than today - 7 days.
 *
 * User: Quino
 * Date: 13/11/2013
 *
 *
 */
class DeleteOldLogsCommand extends AbaConsoleCommand
{
    protected $listLogs = array();

    public function getHelp()
    {
        return parent::getHelp() . " \n Deletes old logs from database aba_b2c_logs.log* .
                            Parameters for persistency are located in the table config as usual.";
    }

    public function run($args)
    {
        if (isset($args) && count($args) > 0) {
            $this->echoCron(" No arguments accepted for this cron process.");
            return false;
        }
        $this->echoCron(" \n +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        $this->echoCron(" \n DeleteOldLogs: Executing auto process at " . date("Y-m-d H:i:s", time()));
        try {
            if (intval(Yii::app()->config->get("ENABLE_DELETION_LOGS")) != 1) {
                $this->echoCron("------- End Of Process DeleteOldLogs because DELETION_LOGS_DURATION is disabled ----");
                return true;
            }
            if (Yii::app()->config->get("DELETION_LOGS_DURATION") !== '') {
                $tmpConfigLogs = Yii::app()->config->get("DELETION_LOGS_DURATION");
                $tmpConfigLogs = explode(",", $tmpConfigLogs);
                $this->listLogs = $tmpConfigLogs;
            }

            foreach ($this->listLogs as $configDeleteLog) {
                if (strpos($configDeleteLog, "#") <= 0) {
                    continue;
                }
                $aLogToDelete = explode("#", $configDeleteLog);
                $aLogTableToDelete = explode(".", $aLogToDelete[0]);

                $tableName = $aLogTableToDelete[0];
                $dynDb = LogCronDeletion::BD_ABA_B2C_LOGS;
                switch($tableName) {
                    case "pay_gateway_paypal_ipn": // log_ipn_paypal
                        $dynDb = LogCronDeletion::BD_ABA_B2C;
                        break;
                }

                $moLogDelete = new LogCronDeletion($tableName, $dynDb);
                $daysFilter = $aLogToDelete[1];
                if (!is_numeric($daysFilter)) {
                    $daysFilter = 12;
                }
                $daysFilter = intval($daysFilter);
                $dateFilter = HeDate::getDateAdd(HeDate::todaySQL(false), -$daysFilter, 0);
                $rowsAffected = $moLogDelete->deleteLog($aLogTableToDelete[1], $dateFilter);
                if ($rowsAffected > 0) {
                    $this->echoCron(" \n There were $rowsAffected rows deleted for table " . $tableName .
                        " for dates smaller than " . $dateFilter);
                } else {
                    $this->echoCron(" \n There were NOT ANY ROWS rows deleted for table " . $tableName .
                        " for dates smaller than " . $dateFilter);
                }
            }
        } catch (Exception $e) {
            $this->errorMessage = "Uncontrolled error on DeleteOldLogs: " . $e->getMessage();
            $this->echoCron(" \n " . date("Y-m-d H:i:s", time()) . " EXCEPTION in process DeleteOldLogs. END");
            $this->echoCron(" \n ---------------------------------------------------------------------------------");
            $this->processError($this->errorMessage);
            return false;
        }
        $this->echoCron(" \n " . date("Y-m-d H:i:s", time()) . " Success in process DeleteOldLogs. END");
        $this->echoCron(" \n --------------------------------------------------------------------------------------");
        return true;
    }

    /**
     * @param string $actionDetails
     *
     * @return bool
     */
    private function processError($actionDetails)
    {
        HeLogger::sendLog("RON DeleteOldLogsCommand finished with errors", HeLogger::IT_BUGS, HeLogger::CRITICAL,
          " Deleting logs from CRON DeleteOldLogsCommand, exception or error: " . $actionDetails);
        return true;
    }
}
