<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 22/11/12
 * Time: 12:11
 */
class RenewalPaymentCaixaCommand extends AbaConsoleCommand
{
    //
    // TEST:    /usr/bin/php  /var/www/abawebapps/campus/public/script.php  RenewalPaymentCaixa
    //
    public function run($args)
    {
echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
echo " \n RenewalPaymentCaixaCommand: executing auto process at ".date("Y-m-d H:i:s", time() );
        $allSuccess = true;

        // 1- Collect all Caixa Payments Supplier=1, status = PENDING, dateToPay>=Today; Order by dateToPay ASC, dateStartTransaction ASC
        $moPayments = new Payments();
        /*  @var Payment[] $aPendingPays    */
        $aPendingPays = $moPayments->getAllPaymentsPendingLaCaixaTest();
        if( !$aPendingPays )
        {
            // No Payments pending for La Caixa;
echo " \n There are no users to renew today. Success on executing RenewalPaymentCaixaCommand";
echo " \n -----------------------------------------------------------------------------------------------";
            return true;
        }

        /* @var Payment $moPendingPay */
       foreach($aPendingPays as $moPendingPay)
       {
           echo " \n ". date("Y-m-d H:m:s")." Processing the renewal of payment id ".$moPendingPay->id." of user ".$moPendingPay->userId;

           $dateStart = HeDate::todaySQL(true);
           /*  Para cada pago */
           // 2-Collect Datos de pago para el usuario de este pago
           $user = new AbaUser( );
           if( $moPendingPay->userId=='' || !is_numeric($moPendingPay->userId) )
           {
               throw new CDbException("ABA, User Id present in payment ".$moPendingPay->id." is not VALID AT ALL.");
           }

           if( !$user->getUserById( $moPendingPay->userId ) )
           {
               $this->processErrorNoRenewed($user, $moPendingPay, " User not found by id user of payment ".$moPendingPay->userId , $dateStart);
               $allSuccess = false;
               continue;
           }

           if(Yii::app()->config->get("ENABLE_LOG_USERACTIVITY") == 1) {
               $user->abaUserLogUserActivity->insertLog("payments",""," user susceptible of renewal, initiating process.","(AUTO)recurrent payment");
           }

           $moUserCredit = new AbaUserCreditForms(  );
           if( !$moUserCredit->getUserCreditFormByUserId($moPendingPay->userId) )
           {
               $this->processErrorNoRenewed($user, $moPendingPay, " Credit card Details not found for the user ".$user->email, $dateStart);
               $allSuccess = false;
               continue;
           }
           // 3-Validar Datos de Producto para este usuario/pago
           $moProductUser = new ProductPrice();
           if( !$moProductUser->getProductById( $moPendingPay->idProduct ) )
           {
               $this->processErrorNoRenewed($user, $moPendingPay, " Product not Found for the user  ".$user->email.", product= ".$moPendingPay->idProduct , $dateStart);
               $allSuccess = false;
               continue;
           }

           //  Starts the execution that affects the database data related with these next payments pending:
           // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
           if( intval( Yii::app()->config->get("ENABLE_LACAIXA_PAY_RECURRENT") )==1)
           {
               //
               //#4681
               $validate = PaySupplierLaCaixa::validateCardDataBypaySuppExtProfId($moPendingPay);

               // 4-Preparar XML y validarlo
               // 5-Insertar petición en Log de La Caixa
               // 6-Enviar petición de Pago
               // 7-Guardar Reponse en Log
               $moPaymentControlCheck = new PaymentControlCheck();
               if( !$moPaymentControlCheck->createPayControlCheckFromPayment( $user->id, 1, $moPendingPay->idProduct, $moPendingPay->idCountry, $moPendingPay->idPeriodPay,
                   $moPendingPay->idPromoCode, $moUserCredit->kind, $moUserCredit->cardName, $moUserCredit->cardNumber , $moUserCredit->cardYear, $moUserCredit->cardMonth,
                   $moUserCredit->cardCvc, $moPendingPay->paySuppExtId, $moPendingPay, '', '', $validate, $moPendingPay->paySuppExtProfId ) )
               {
                   $this->processErrorNoRenewed($user, $moPendingPay, " Payment Control check could not be built  ".$user->email." product= ".$moPendingPay->idProduct, $dateStart );
                   $allSuccess = false;
                   continue;
               }

               /* @var PaySupplierLaCaixa $paySupplier */
               $paySupplier = PaymentFactory::createPayment( $moPendingPay );
               /* @var PayGatewayLogLaCaixaResRec $retPaymentSupplier */
               $retPaymentSupplier = $paySupplier->runDebiting( $moPaymentControlCheck, "A", true );
               if( !$retPaymentSupplier )
               {
                   $errPaymentGateway = Yii::t('mainApp', 'gateway_recurrent_failure_key').
                       " ( Response from PaymentSupplier = ".$paySupplier->getErrorCode()."-".$paySupplier->getErrorString()." )";
                   $moPendingPay->paySuppOrderId = $paySupplier->getOrderNumber();
                   $this->processErrorNoRenewed($user, $moPendingPay, " Renewal request through La Caixa for user ".$user->email." payment= ".$moPendingPay->id." Details=".$errPaymentGateway , $dateStart);
                   $allSuccess = false;
                   continue;
               }

               $successAfterGateway = true;

               //
               //#4681
               $paySuppExtProfId = null;
               if(trim($moPendingPay->paySuppExtProfId) == '') {
                   $paySuppExtProfId = trim($retPaymentSupplier->getPaySuppExtProfId());
               }

               // 8-Actualizar pago viejo.
               // 9-Crear siguiente pago.
               // 10-Actualizar Usuario (Renovar)
               $commRecurrPayment = new RecurrentPayCommon();
               $retSucc = $commRecurrPayment->renewPaymentByPaySuppOrderId($user->email, $paySupplier->getOrderNumber(),
                                    $dateStart, "recurring_payment_LACAIXA", PAY_SUPPLIER_CAIXA , $moPendingPay->id, null, $paySuppExtProfId);

               if (!$retSucc) {
                   $successAfterGateway = false;
                   $this->processErrorAfterRenewed($user, $moPendingPay, " Error in renewal on La Caixa process. User " . $user->email . " payment= " . $moPendingPay->id . " has been carried out successfully in La Caixa but processing " .
                       "into our database has failed. Please review renewal data. More Details= " . $commRecurrPayment->getErrorMessage());
                   $allSuccess = false;
               } else {
                   $moPendingPay->paySuppExtUniId = $retPaymentSupplier->getUniqueId();
                   $moPendingPay->update(array("paySuppExtUniId"));
               }
           }
           else
           {
               HeLogger::sendLog("CRON Process Renewal of payments for La Caixa Gateway: ".$user->email,
                    HeLogger::IT_BUGS_PAYMENTS, HeLogger::INFO,
                    " The payment ".$moPendingPay->id." with the amount ".$moPendingPay->amountPrice." and date to pay was ".$moPendingPay->dateToPay." should go under real transaction now. Disabled by ENABLE_LACAIXA_PAY_RECURRENT");
           }

           echo " >> ". date("Y-m-d H:m:s")." Successful processed user ".$user->email." for payment with id= ".$moPendingPay->id;
       }

        if( !$allSuccess )
        {
            echo " \n \n  Renewal of La Caixa executed at ".date('Y-M-D H:m:s')." with ERRORS for some payments. ";
        }

echo " \n  Finished process at ".date('Y-M-D H:m:s');
echo " \n -----------------------------------------------------------------------------------------------";
        return $allSuccess;
    }

    /**
     * @param AbaUser $user
     * @param Payment $payment
     * @param         $actionDetails
     * @param null    $dateStart
     *
     * @return bool
     */
    private function processErrorNoRenewed(AbaUser $user, Payment $payment, $actionDetails, $dateStart=null)
    {
        if( intval( Yii::app()->config->get("ENABLE_LACAIXA_PAY_RECURRENT") )==1)
        {
            // @TODO Replace function updateFailedRenewPay by RecurringCommon::failRecurring
            // Payment has failed to be renewed by La Caixa, update  Payment with failure.
            $fUpdPay = $payment->updateFailedRenewPay($dateStart, $actionDetails);
            // User is to be cancelled, means to change to FREE again.
            $commUser = new UserCommon();
            $commUser->cancelUser(  $user->getId(), PAY_CANCEL_FAILED_RENEW );
        }

        echo ">> VVV"." Error processing, NOT RENEWED, user DID NOT PAID and WAS CANCELLED in DATABASE. ".
                        " NO NEED TO BE REVIEWED BY IT, payment id ".$payment->id;
        return true;
    }

    /**
     * @param AbaUser $user
     * @param Payment $payment
     * @param         $actionDetails
     *
     * @return bool
     */
    private function processErrorAfterRenewed(AbaUser $user, Payment $payment, $actionDetails)
    {
        HeLogger::sendLog("CRON User Recurring Payment La Caixa successful but finished with errors ".$user->email.", It MUST be REVIEWED payment id=".$payment->id ,
            HeLogger::IT_BUGS_PAYMENTS, HeLogger::CRITICAL,
          " User Recurring Payment La Caixa successful but finished with errors ".$user->email.", payment id=".$payment->id." $actionDetails ");
        echo ">> XXX"." Error processing, RENEWED in database but with failures. It MUST be REVIEWED: payment id ".$payment->id;
        return true;
    }

}
