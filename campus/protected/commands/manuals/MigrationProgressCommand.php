<?php
/**
 * Created by PhpStorm.
 * User: jordi
 * Date: 30/01/2015
 * Time: 15:36
 *
 * php -f /wamp/www/abawebapps/campus/public/script.php MigrationProgress 10 1 1 0 1
 *
 */

class MigrationProgressCommand extends AbaConsoleCommand
{
    public function getHelp()
    {
        return parent::getHelp() .
        "Proceso de migración de los usuarios antiguos desde 'followup4' y 'followup_html4' a nuevas copias de estas con el sufijo '_99'.\n".
        "A este proceso se le puede llamar con 2 formatos: \n ".
        "1 - Requiere 5 parámetros, número de usuarios a migrar, 'progressVersion', 'progressVersionHtml', 'audioVersion', (0|1) Mostrar Log.\n".
        "2 - Requiere 6 parámetros, 'D', usuario a migrar , 'progressVersion', 'progressVersionHtml', 'audioVersion', (0|1) Mostrar Log.\n";
    }

    public function run($args)
    {
        $time_start = microtime(true);

        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n MigrationProgress: Executing auto process at " . date("Y-m-d H:i:s", time());
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n";

        if (!isset($args[0]) || count($args) < 5) {
            echo " Missing parameters: ";
            echo " \n " . $this->getHelp();
            return false;
        }
        $userMigration = new CmAbaUserMigration();
        $usersAMigrar = array();
        $progressVersion = 0;
        $progressVersionHtml =0;
        $audioVersion =0;
        $borradosFollowup4=0;
        $borradosFollowup_html4=0;
        $movedFollowup4=0;
        $movedFollowup_html4=0;
        $cntTmp=0;
        $mostraLog=0;
        if ($args[0]=="D") {
            $usersAMigrar = array(["id"=>$args[1]]);
            $progressVersion = $args[2];
            $progressVersionHtml = $args[3];
            $audioVersion = $args[4];
            $mostraLog = $args[5];
        } else {
            $nUsersAMigrar = $args[0];
            $progressVersion = $args[1];
            $progressVersionHtml = $args[2];
            $audioVersion = $args[3];
            $mostraLog = $args[4];

            $usersAMigrar = $userMigration->getUserNUsersToMigrate($nUsersAMigrar);
        }

        foreach ($usersAMigrar as $IDUser) {
            $cntTmp +=1;
            $userTime_start = microtime(true);
            $recordsMoved=$userMigration->copyUserData(
                intval($IDUser["id"]),
                $progressVersion,
                $progressVersionHtml,
                $audioVersion
            );
            if ($recordsMoved === false) {
                echo " User : ".$IDUser["id"]. " ERROR migrating.\n";
            } else {
                $movedFollowup4 += $recordsMoved['followup4'];
                $movedFollowup_html4 += $recordsMoved['followup_html4'];
                $recordsDeleted=$userMigration->deleteAlreadyMigrateuserData(intval($IDUser["id"]));
                if (!$recordsDeleted===false) {
                    $borradosFollowup4 += $recordsDeleted['followup4'];
                    $borradosFollowup_html4 += $recordsDeleted['followup_html4'];
                    $timeExecution = explode('.', (microtime(true) - $userTime_start));
                    if (!$mostraLog==false) {
                        echo str_pad($cntTmp, 7, " ", STR_PAD_LEFT)." - User : ".
                            str_pad($IDUser["id"], 6, " ", STR_PAD_LEFT)." Migrated" .", moved followup4: ".
                            str_pad($recordsMoved['followup4'], 6, " ", STR_PAD_LEFT).", deleted: ".
                            str_pad($recordsDeleted['followup4'], 6, " ", STR_PAD_LEFT). ", moved followup_html4: ".
                            str_pad($recordsMoved['followup_html4'], 6, " ", STR_PAD_LEFT). ", deleted: ".
                            str_pad($recordsDeleted['followup_html4'], 6, " ", STR_PAD_LEFT). ". Migration time:" .
                            str_pad($timeExecution[0], 5, " ", STR_PAD_LEFT).'.'.
                            str_pad($timeExecution[1], 17, " ", STR_PAD_RIGHT) . " Sec.\n";
                    }
                } else {
                    echo " User : ".$IDUser["id"]." OK migrating, FAIL delete debris in followup4 or followup_html4.\n";
                    $timeExecution = explode('.', (microtime(true) - $userTime_start));
                    echo str_pad($cntTmp, 7, " ", STR_PAD_LEFT) .
                        " - User : " .str_pad($IDUser["id"], 6, " ", STR_PAD_LEFT) . " Migrated" .", moved followup4: ".
                        str_pad($recordsMoved['followup4'], 6, " ", STR_PAD_LEFT). ", moved followup_html4: ".
                        str_pad($recordsMoved['followup_html4'], 6, " ", STR_PAD_LEFT). ". Migration time:" .
                        str_pad($timeExecution[0], 5, " ", STR_PAD_LEFT).'.'.
                        str_pad($timeExecution[1], 17, " ", STR_PAD_RIGHT) ." Sec.\n";
                }
            }
        }
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n MigrationProgress: Finish process at " . date("Y-m-d H:i:s", time());
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n";
        echo " Total inserted records in followup4: $movedFollowup4, followup_html4: $movedFollowup_html4\n";
        echo " Total deleted records in followup4: $borradosFollowup4, followup_html4: $borradosFollowup_html4\n";
        echo " \n";
        $execution_time = (microtime(true) - $time_start)/60;
        echo 'Total Execution Time: '.$execution_time.' Mins';   //execution time of the script
        return true;
    }
}
