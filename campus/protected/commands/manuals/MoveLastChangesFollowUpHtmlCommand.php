<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 23/10/2013
 * Time: 12:11
 *
 * To know how many rows are left: ***************
 SELECT @vTotalRowsToCopy:=(SELECT COUNT(f.id) FROM aba_b2c.`followup_html4` f)  as totalRowsToCopy,
        @vRowsCopied:=(SELECT SUM(l.rowsAffected) FROM  aba_b2c_logs.`log_migration_followup_html` l) as RowsCopied,
        FORMAT((@vTotalRowsToCopy-@vRowsCopied),4) as RowsLeft ;
 */
class MoveLastChangesFollowUpHtmlCommand extends AbaConsoleCommand
{
    private $originTable = "followup_html4";
    private $destinationTable = "followup_html4_I";

    public function getHelp()
    {
        return parent::getHelp()." It accepts 2 parameters, RowStartId and Date in format YYYY-MM-DD
                        This script is to move all rows from table followup_html to table followup_html4_I
                         that have changed from date passed by argument.
                        It checks all last changed rows after the start of the process of MoveFollowUpHtml. \n ";
    }

    public function run($args)
    {
        echo ' \n Starting to check tables and rows last changed at '.date("Y-m-d H:i:s", time());
        if (!isset($args[0]) || count($args)<2) {
            echo " Missing parameters: ";
            echo " \n " . $this->getHelp();
            return false;
        }
        $startId = 1;
        if (isset($args[0])) {
            $startId = intval($args[0]);
        }
        $dateFilterChanges = $args[1];
        if (!HeDate::validDateSQL($dateFilterChanges, false)) {
            echo " Wrong value parameters: ";
            echo " \n " . $this->getHelp();
            return false;
        }
        $moFollowUpHtml = new AbaFollowUpHtml4();
        $aRows = $moFollowUpHtml->getAndInsertLastChangedRows(
            $this->originTable,
            $this->destinationTable,
            $startId,
            $dateFilterChanges
        );
        if (!$aRows) {
            echo " \n --------For values ".
                $this->originTable." ,".$this->destinationTable." , ".$startId.
                ", ".$dateFilterChanges." NO ROWS MODIFIED BY USERS were found------- ";
        } else {
            echo " \n --------For values ".
                $this->originTable." ,".$this->destinationTable." , ".$startId.
                ", ".$dateFilterChanges." ".intval($aRows)." were found to be modfieid by users.------- ";
        }
        return true;
        echo ' \n End MoveLastChangesFollowUpHtmlCommand ended at '.date("Y-m-d H:i:s", time());
    }


    /**
     * @param string $actionDetails
     *
     * @return bool
     */
    protected function processEndsWithError($actionDetails)
    {
        HeLogger::sendLog(
          HeLogger::PREFIX_ERR_UNKNOWN_H." Cron MoveLastChangesFollowUpHtml was executed and
                                                        finished but processed some errors.",
            HeLogger::IT_BUGS,
            HeLogger::CRITICAL,
            $actionDetails
        );
        echo " \n ".date("Y-m-d H:i:s", time())." MoveLastChangesFollowUpHtml finished with ERRORS : ".$actionDetails;
        echo " \n -----------------------------------------------------------------------------------------------";
        return true;
    }
}
