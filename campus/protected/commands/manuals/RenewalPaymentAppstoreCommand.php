<?php

class RenewalPaymentAppstoreCommand extends AbaConsoleCommand
{
    protected $idPaySupplier = PAY_SUPPLIER_APP_STORE;

    //
    // TEST:    php -f  /var/www/abawebapps/campus/public/script.php  RenewalPaymentAppstore
    //
    public function run($args)
    {
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n ProcessAppstoreRenewalsCommand: executing auto process at " . date("Y-m-d H:i:s", time() );

        $allSuccess = true;

        $today = HeDate::todaySQL(false);
        $dateRequestFrom = $today . ' 00:00:00';


        $moPayments = new Payments();

        $allPaymentsAppstore =  $moPayments->getAllPaymentsPendingAppstoreTest($dateRequestFrom);

        foreach($allPaymentsAppstore as $iKey => $stPaymentAppstore) {

            $stAppStorePayment =    $stPaymentAppstore['appStorePayment'];  // AppStore payment related to PENDING payment

            if(trim($stAppStorePayment->purchaseReceipt) != '' AND strpos($stAppStorePayment->purchaseReceipt, '{') !== false) {

                //
                // PARSE JSON
                // CALL APPSTORE API
                /* @var PaySupplierAppstore $paySupplier */
                $paySupplier = PaymentFactory::createPayment(null, PAY_SUPPLIER_APP_STORE);

                // set appstore json receipt
                $paySupplier->setJsonResponse($stAppStorePayment->purchaseReceipt);

                $moUser = new AbaUser();
                $moUser->getUserById($stAppStorePayment->userId);


                /** * * * * * * VALIDAR original_transaction_id DUPLICADOS * * * * * * * * * * * */

                //
                // Validate json appstore receipt
                //
                if(!$paySupplier->validatePurchaseReceipt($moUser)) {
echo "\n+++++++++++NOT VALID RECEIPT :: " . $stAppStorePayment->id . " :: " . $stAppStorePayment->datePaymentAppstore . "+++++++++++";
                }
                else {
echo "\n+++++++++++VALID RECEIPT :: " . $stAppStorePayment->id . " :: " . $stAppStorePayment->datePaymentAppstore . "+++++++++++";
                }

                /** * * * * * * VALIDAR TODOS LOS RECIBOS * * * * * * * * * * * */

//
//                if($paySupplier->runRequestReconciliation($stAppStorePayment)) {
//                    //
//                    // Cancelled / Expired Receipt
//                    //
//                    if($paySupplier->isExpiredReceipt()) {
//
//echo "\n+++++++++++EXPIRED RECEIPT :: " . $stAppStorePayment->id . " :: " . $stAppStorePayment->datePaymentAppstore . "+++++++++++";
//
//                    }
//                    elseif($paySupplier->isValidRenewalReceipt()) {
//
//echo "\n+++++++++++IS VALID RENEWAL RECEIPT :: " . $stAppStorePayment->id . " :: " . $stAppStorePayment->datePaymentAppstore . "+++++++++++";
//
//                    }
//                    else {
//
//echo "\n+++++++++++IS NOT VALID RENEWAL RECEIPT :: " . $stAppStorePayment->id . " :: " . $stAppStorePayment->datePaymentAppstore . "+++++++++++";
//
//                    }
//                }
//                else {
//                    $allSuccess = false;
//                }
            }
        }

        echo "\n";
        echo " \n  Finished process ProcessAppstoreRenewalsCommand at ".date('Y-M-D H:m:s');
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
        return $allSuccess;
    }

    /**
     * @param AbaUser $user
     * @param Payment $payment
     * @param $actionDetails
     * @param null $dateStart
     *
     * @return bool
     */
    private function processErrorNoRenewed(AbaUser $user, Payment $payment, $actionDetails, $dateStart=null)
    {
        if( intval( Yii::app()->config->get("PAY_APP_STORE_ENABLE_RECURRENT") ) == 1)
        {
            // @TODO Replace function updateFailedRenewPay by RecurringCommon::failRecurring
            // Payment has failed to be renewed by Appstore, update  Payment with failure.
            $fUpdPay = $payment->updateFailedRenewPay($dateStart, $actionDetails);
            // User is to be cancelled, means to change to FREE again.
            $commUser = new UserCommon();
            $commUser->cancelUser(  $user->getId(), PAY_CANCEL_FAILED_RENEW );
        }

        echo "\n>> VVV"." Error processing, NOT RENEWED, user DID NOT PAID and WAS CANCELLED in DATABASE. ".
          " NO NEED TO BE REVIEWED BY IT, payment id " . $payment->id . "\n";
        return true;
    }

}
