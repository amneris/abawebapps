<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 23/10/2013
 * Time: 12:11
 *
 * To know how many rows are left:
 * To know how many rows are left: ***************
SELECT @vTotalRowsToCopy:=(SELECT COUNT(f.id) FROM aba_b2c.`followup_html4` f)  as totalRowsToCopy,
@vRowsCopied:=(SELECT SUM(l.rowsAffected) FROM  aba_b2c_logs.`log_migration_followup_html` l) as RowsCopied,
FORMAT((@vTotalRowsToCopy-@vRowsCopied),4) as RowsLeft ;
 *
 *
 * Delete all useless rows: ***********************
 *
SELECT COUNT(f.id) -- , f.userid
FROM followup_html4 f LEFT JOIN `user` u ON f.userid=u.id
WHERE u.id IS NULL ;-- = 258.284 registros

DELETE  f.* FROM followup_html4  as `f`
LEFT JOIN `user` as `u`  ON f.userid=u.id
WHERE u.id IS NULL;
 *
 * Creation of InnoDB Tables ***********************
 *
CREATE TABLE  `aba_b2c`.`followup_html4_I` (
    `id` int(10) unsigned NOT NULL auto_increment,
    `userid` int(10) unsigned NOT NULL,
    `themeid` int(10) unsigned NOT NULL,
    `page` int(10) unsigned NOT NULL,
    `section` varchar(20) NOT NULL,
    `action` varchar(20) NOT NULL,
    `audio` varchar(20) NOT NULL,
    `bien` tinyint(1) NOT NULL,
    `mal` int(10) unsigned NOT NULL,
    `lastchange` datetime NOT NULL,
    `wtext` varchar(100) NOT NULL,
    `difficulty` int(10) unsigned NOT NULL,
    `time_aux` int(4) unsigned NOT NULL,
    `contador` int(10) unsigned NOT NULL,
    PRIMARY KEY  USING BTREE (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8;
 *
CREATE TABLE  `aba_b2c`.`course_var4_I` (
`id` int(4) unsigned NOT NULL auto_increment,
`userId` int(4) unsigned NOT NULL default '0',
`name` varchar(25) NOT NULL COMMENT 'Name ',
`value` varchar(200) NOT NULL default '',
PRIMARY KEY  USING BTREE (`id`),
KEY `index_var` USING BTREE (`userId`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='To store features of the user while surfing the course.';

 *
INSERT INTO followup4_I
SELECT * FROM followup4;
 *
INSERT INTO course_var4_I (`id`, `userId`, `name`, `value`)
SELECT * FROM course_var4;
 *
 *
CREATE TABLE  `aba_b2c`.`followup4_I` (
`id` int(4) unsigned NOT NULL auto_increment,
`themeid` int(4) unsigned NOT NULL default '0',
`userid` int(4) unsigned NOT NULL default '0',
`lastchange` datetime NOT NULL default '0000-00-00 00:00:00',
`all_por` int(4) unsigned default '0' COMMENT 'Total percentage, not used at the moment',
`all_err` int(4) unsigned default '0',
`all_ans` int(4) unsigned default '0',
`all_time` int(4) unsigned default '0',
`sit_por` int(4) unsigned default '0' COMMENT 'Situation video percentage watched, 0, 50 or 100',
`stu_por` int(4) unsigned default '0' COMMENT 'Section Speak',
`dic_por` int(4) unsigned default '0' COMMENT 'Sectoin Write (dictation)',
`dic_ans` int(4) unsigned default '0' COMMENT 'Section Write, help requests',
`dic_err` int(4) unsigned default '0' COMMENT 'Section Write wrong answers',
`rol_por` int(4) unsigned default '0' COMMENT 'Section Role Play percentage',
`gra_por` int(4) unsigned default '0' COMMENT 'Grammar exercises percentage done. Kind of deprecated',
`gra_ans` int(4) unsigned default '0' COMMENT 'Grammar help requests',
`gra_err` int(4) unsigned default '0' COMMENT 'Grammer wrong answers',
`wri_por` int(4) unsigned default '0' COMMENT 'Section Exercises percetnage',
`wri_ans` int(4) unsigned default '0' COMMENT 'Section Exercises help requests',
`wri_err` int(4) unsigned default '0' COMMENT 'Section Exercises wrong answers',
`new_por` int(4) unsigned default '0' COMMENT 'Section Vocab, new words, percentage',
`spe_por` int(4) unsigned default '0' COMMENT 'Deprecated section, older users have data',
`spe_ans` int(4) unsigned default '0' COMMENT 'Deprecated section, older users have data',
`time_aux` int(4) unsigned default '0' COMMENT 'Deprecated, old uses',
`gra_vid` int(1) unsigned default '0' COMMENT 'If user has watched videoclass or not. 0 or 100',
`rol_on` int(1) unsigned default '0' COMMENT 'Section Role Play, linked to rol_por.',
`exercises` varchar(25) NOT NULL default '0,0,0,0,0,0,0,0,0,0' COMMENT 'Exam results, always 10 exercises. Linked to eva_por',
`eva_por` int(4) unsigned NOT NULL default '0' COMMENT 'Section Evaluation, 0/1, to be able to dwload certificate',
PRIMARY KEY  USING BTREE (`id`),
KEY `learner_theme_index` USING BTREE (`userid`,`themeid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
 *
 */
class MoveFollowUpHtmlCommand extends AbaConsoleCommand
{
    private $originTable = "followup_html4";
    private $destinationTable = "followup_html4_I";

    public function getHelp()
    {
        return parent::getHelp()." This script is to move all rows from table followup_html4 to table followup_html4_I.
         The first on being MyISAM, the second one being InnoDb. Due to its volume of data,
          we do this process by iterations. An argument is accepted,
          Id top limit, similar to number of rows. \n ";
    }

    public function run($args)
    {
        echo ' \n Starting to check tables at '.date("Y-m-d H:i:s", time());
        // Set Limit of rows to deal with:
        $nBatchRows = 150000;
        if (isset($args[0])) {
            $nBatchRows = intval($args[0]);
        }
        $lastId = 0;
        // Check if log migration table log_migration_followup_html has any row.
        $moLogMove = new LogMigrationFollowupHtml();
        if (!$moLogMove->getLogLastByTimeEnd()) {
            // Not exists then create the table and set the beginning to one.
            $moLogMove->lastId = $lastId;
            $moLogMove->rowsAffected = 0;
            $moLogMove->timeStart = HeDate::todaySQL(true);
            $moLogMove->timeEnd = HeDate::todaySQL(true);
            if (!$moLogMove->insertLog()) {
                $this->processEndsWithError(" Table Log log_migration_followup_html does not allow an insert query.".
                                " Please review user privileges of MySql.");
            }
        } else {
            // Get the last position of the log migration table.
            $lastId = $moLogMove->lastId;
        }
        // Insert start process log in log_migration_followup_html
        $moLogMove = null;
        $moLogMove = new LogMigrationFollowupHtml();

        $moFollowUpHtml = new AbaFollowUpHtml4();
        $rowsAvailable = $moFollowUpHtml->getAnyRowAboveId($lastId);
        if ($rowsAvailable == 0 || !$rowsAvailable) {
            HeLogger::sendLog(
                HeAbaMail::INFOPROC_LAYOUT." Cron MoveFollowUpHtml is being executed and it has ENDED.",
                HeLogger::IT_BUGS,
                HeLogger::INFO,
                "It looks like there are no more rows to copy. Please disable cron."
            );
            echo " \n ".date("Y-m-d H:i:s", time())." MoveFollowUpHtml finished: ".
                "It looks like there are no any more rows to copy. Please disable cron.";
            echo " \n -----------------------------------------------------------------------------------------------";
            return true;
        }

        $moLogMove->timeStart = HeDate::todaySQL(true);
        $moLogMove->insertLog();
        $affectedRows = 0;
        while ($affectedRows < $nBatchRows) {
            $topMaxId = $lastId + $nBatchRows;
            $moLogMove->timeStart = HeDate::todaySQL(true);
            $moLogMove->update(array("timeStart"));
            // Do INSERT INTO new table with SELECT
            echo " \n Copying rows from ".$this->originTable." to ".$this->destinationTable.
                                                    " from row with Id=".$lastId." to row Id=".$topMaxId." = ";
            $newAffectedRows = $moFollowUpHtml->getAndInsertNextChunkFollowUpHtml(
                $this->originTable,
                $this->destinationTable,
                $lastId,
                $topMaxId
            );
            echo $newAffectedRows." rows found.";
            $moLogMove->timeEnd = HeDate::todaySQL(true);
            $lastId = $topMaxId;
            $affectedRows = $affectedRows + $newAffectedRows;
        }
        // Insert end process log in log_migration_followup_html
        $moLogMove->rowsAffected = $affectedRows;
        $moLogMove->lastId = $lastId;
        $moLogMove->update(array("timeEnd", "rowsAffected", "lastId"));
        echo ' \n End MoveFollowUpHtmlCommand at '.date("Y-m-d H:i:s", time());
        return true;
    }


    /**
     * @param string $actionDetails
     *
     * @return bool
     */
    protected function processEndsWithError($actionDetails)
    {
        HeLogger::sendLog(
          HeLogger::PREFIX_ERR_UNKNOWN_H." Cron MoveFollowUpHtml was executed with some errors.",
            HeLogger::IT_BUGS,
            HeLogger::CRITICAL,
            $actionDetails
        );
        echo " \n ".date("Y-m-d H:i:s", time())." MoveFollowUpHtml finished with ERRORS : ".$actionDetails;
        echo " \n -----------------------------------------------------------------------------------------------";
        return true;
    }

}
