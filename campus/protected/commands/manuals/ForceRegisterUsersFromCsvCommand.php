<?php
/** It is used manually to force register users through Campus web services.
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 22/11/12
 * Time: 12:11
 */
class ForceRegisterUsersFromCsvCommand extends AbaConsoleCommand
{
    private $pathFileIn='C:\Quino\Abaenglish\IncidenciasBBDD\BUG-2269\WebServiceRegisterUserSimpleAllCalls_20131202_to_20131204_4.csv';
    private $pathFileOut='C:\Quino\Abaenglish\IncidenciasBBDD\BUG-2269\out\live20131202_to_20131204_4.csv';

    public function getHelp()
    {
        return parent::getHelp()." \n ";
    }

    public function run($args)
    {
//        $urlWs = 'http://campus.abaenglish.com/wsoapaba/usersapi/';
        $urlWs = 'http://campus.aba.local/wsoapaba/usersapi/';
        if(count($args)>0)
        {
           $this->pathFileIn = $args[0];
           $this->pathFileOut = $args[1];
        }

        if(file_exists($this->pathFileOut)){
            file_put_contents( $this->pathFileOut, " \n", FILE_APPEND);
        }
        if(!file_exists($this->pathFileIn)){
            echo " File does not exist mate ".$this->pathFileIn;
            return;
        }

        ini_set("soap.wsdl_cache_enabled", 0);
        $totalLines = count(file($this->pathFileIn));
        $aAllUsers = array();
        $usersLinesCreated = 0;
        $usersLinesNotCreated = 0;
        $usersLinesRepeated = 0;


        $handle = @fopen($this->pathFileIn, "r");
        if ($handle)
        {
            $iLine = 0;
            while (($newLine = fgets($handle, 4096)) !== false)
            {
                $iLine++;
                if( strpos($newLine,'ateRequest;paramsRequest')>=1 ){
                    continue;
                }

                $aPartsLine = explode(';',$newLine);
                $entryDate = substr( $aPartsLine[0], 0);
                $xmlSoap = substr($aPartsLine[1], 1, strlen(trim($aPartsLine[1]))-2 );
                $xmlSoap = str_replace('""', '"', $xmlSoap);

                $oXml = new SimpleXMLElement($xmlSoap);
                /* Search for xPaths */
                $xmlRegisterUserFields = $oXml->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:registerUser/signature');
                $signature = $xmlRegisterUserFields[0]->__toString();

                $xmlRegisterUserFields = $oXml->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:registerUser/email');
                $email = $xmlRegisterUserFields[0]->__toString();

                $xmlRegisterUserFields = $oXml->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:registerUser/idPartner');
                $idPartner = $xmlRegisterUserFields[0]->__toString();

                $xmlRegisterUserFields = $oXml->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:registerUser/langEnv');
                $langEnv = $xmlRegisterUserFields[0]->__toString();

                $xmlRegisterUserFields = $oXml->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:registerUser/pwd');
                $pwd = $xmlRegisterUserFields[0]->__toString();

                $xmlRegisterUserFields = $oXml->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:registerUser/name');
                $name = $xmlRegisterUserFields[0]->__toString();

                $xmlRegisterUserFields = $oXml->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:registerUser/surnames');
                $surnames = $xmlRegisterUserFields[0]->__toString();

                $xmlRegisterUserFields = $oXml->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:registerUser/idProduct');
                $idProduct = $xmlRegisterUserFields[0]->__toString();

                $xmlRegisterUserFields = $oXml->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:registerUser/promocode');
                $promocode = $xmlRegisterUserFields[0]->__toString();

                $xmlRegisterUserFields = $oXml->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:registerUser/landingPage');
                $landingPage = $xmlRegisterUserFields[0]->__toString();

                $xmlRegisterUserFields = $oXml->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/ns1:registerUser/idCountry');
                $idCountry = $xmlRegisterUserFields[0]->__toString();
                if( empty($idCountry) ){
                    $idCountry = intval(Yii::app()->config->get("ABACountryId"));
                }
                if( empty($langEnv) )
                {
                    $moUser = new AbaUser();
                    if($moUser->getUserByEmail($email)){
                        $langEnv = $moUser->langEnv;
                    }
                    if($langEnv=='' || empty($langEnv) ){
                        $langEnv = 'en';
                    }
                }

                // campus.abaenglish.com
                $client = new SoapClient( $urlWs,
                                    array('soap_version'   => SOAP_1_2, 'port'=> 80, 'trace' => 1)  );

                $restoredValues = $email.','.$pwd.','.$langEnv.','.$name.','.$surnames.','.$idProduct.','.$promocode.','.$landingPage.','.$idCountry.','.$idPartner;
                if( array_search($email, $aAllUsers, true)==false ){
                    /*--------------------------------------------------------------------------*/
                    /*--         SOAP REQUESTS              --*/
                    $signatureWsAbaEnglish = md5( 'Yii ABA English Key 2'. $email.$pwd.$langEnv.$name.$surnames.
                                                            $idProduct.$promocode.$landingPage.$idCountry.$idPartner);
                    $return = NULL;
                    try
                    {
                        /* @var WsoapabaController $client */
                        $return = $client->registerUser( $signatureWsAbaEnglish, $email, $pwd, $langEnv, $name, $surnames,
                                                            $idProduct, $promocode, $landingPage, $idCountry, $idPartner );
                        if( array_key_exists("userId", $return) ){
                            file_put_contents($this->pathFileOut, $email.",".$return["userId"].",".$restoredValues." \n", FILE_APPEND);
                            echo ' \n Registering user '.$email.'= '.$return["result"].' , id='.$return["userId"];
                            $usersLinesCreated++;
                            $aAllUsers[]=$email;
                        }
                        else{
                            $keyRet= array_keys($return);
                            file_put_contents($this->pathFileOut, $email.", error ".$return[$keyRet[0]].",".$restoredValues." \n", FILE_APPEND);
                            echo ' \n Registering user '.$email.'= error '.$keyRet[0]."(".$return[$keyRet[0]].")";
                            $usersLinesNotCreated++;
                        }

                    }
                    catch (Exception $exc)
                    {
                        file_put_contents($this->pathFileOut, $email.", exception ".$exc->getMessage().",".$restoredValues." \n", FILE_APPEND);
                        echo ' \n Registering user '.$email.'= exception '.print_r($return);
                        $usersLinesNotCreated++;
                    }
                    /*--------------------------------------------------------------------------*/
                }
                else{
                    file_put_contents($this->pathFileOut, $email.", repeated so not processed,".$restoredValues." \n", FILE_APPEND);
                    echo '\n Duplicated user on file '.$email.'= repeated so not processed';
                    $usersLinesRepeated++;
                }

            }

            fclose($handle);


            if($iLine!==$totalLines){
                echo "Some errors: The lines processed are ".$iLine." and were expected ".$totalLines.
                        ". Please review file and database mismatch.";
            }
            else{
                if( ($usersLinesCreated+$usersLinesNotCreated+$usersLinesRepeated)!==($iLine-1) ){
                    echo "Some errors, break down: Users lines created= ".$usersLinesCreated." , users lines NOT created=".$usersLinesNotCreated.
                        ", users lines REPEATED=".$usersLinesRepeated;
                }
                else{
                    // TOTAL SUCCESS
                    echo "Success: Total lines processed=".$totalLines." Break down: Users lines created= ".
                        $usersLinesCreated." , users lines NOT created=".$usersLinesNotCreated.
                        ", users lines REPEATED=".$usersLinesRepeated;
                }
            }
        }


    }
}
