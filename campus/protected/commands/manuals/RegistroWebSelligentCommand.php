<?php
/**
 * Class RegistroWebSelligentCommand
 *
 * php -f  /var/www/abawebapps/campus/public/script.php RegistroWebSelligent "aicardi@mc-mutual.com" "hl2h3tKZ"
 * php -f  /var/www/abawebapps/campus/public/script.php RegistroWebSelligent "akorotkov@abaenglish.com" "1qa2ws2ws"
 *
 */
class RegistroWebSelligentCommand extends AbaConsoleCommand
{
    public function run($args)
    {
echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
echo " \n RegistroWebSelligentCommand: executing  at ".date("Y-m-d H:i:s", time() );

        //
//        $today = HeDate::todaySQL(false);

        //
        if(count($args) > 0) {
            if(trim($args[0]) == ''){
                echo 'First argument (e-mail) is not valid';
                return false;
            }
        }
        //
        if(count($args) > 1) {
            if(trim($args[1]) == ''){
                echo 'Second argument (password) is not valid';
                return false;
            }
        }

        //
        $sEmail =   $args[0];
        $sPwd =     $args[1];

        $user =         new AbaUser();
        $userExists =   $user->getUserByEmail($sEmail, $sPwd, true);

//        if($userExists) {
            $commConnSelligent = new SelligentConnectorCommon();
            $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::registroAgrupadoresExtranet, $user, array("PASS_TMP" => $sPwd), "RegisterUserB2Bextranet web service" );
//        }


echo "\n---" . $user->id . "---\n";


echo " \n  Finished process at " . date('Y-M-D H:m:s');
echo " \n -----------------------------------------------------------------------------------------------";
        return true;
    }

}
