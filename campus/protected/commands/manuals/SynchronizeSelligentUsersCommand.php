<?php
/**
 * Sends info to Selligent from users PREMIUM, EX-GROUPON AND FREE that has logged in in the New Campus.
 * Created by Quino
 * User: Quino
 * Date: 08/03/2013
 * Time: 12:11
 */
class SynchronizeSelligentUsersCommand extends AbaConsoleCommand
{
    public function getHelp()
    {
        return parent::getHelp()." You can set a number to set how many records you want to send to SELLIGENT. \n " ;
    }

    public function run($args)
    {
echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
echo " \n START. SynchronizeSelligentUsersCommand: executing auto process at ".date("Y-m-d h:i:s", time() );
        $allSuccess = true;


        $limitUsers = 100 ;
        if(count($args)>0)
        {
            if( is_numeric($args[0]) ){
                $limitUsers = intval($args[0]) ;
            }
            else{
                echo " The argument should be an integer telling how many maximum records we want to process at once.";
                return false;
            }
        }


        $moUsers = new AbaUsers();
        $aRowsUsersToSend = $moUsers->getAllUsersShouldSentSelligent($limitUsers);
        if( !is_array($aRowsUsersToSend) )
        {
            echo " \n ".date("Y-m-d h:i:s", time() )." There are no users to be sent to SELLIGENT. It looks like everything is synchronized. Process SynchronizeSelligentUsersCommand. END";
            echo " \n -----------------------------------------------------------------------------------------------";
            return false;
        }

        $totalToSend = count($aRowsUsersToSend);
        $aRowsNotSent = array();
        $totalSent = 0;
        foreach( $aRowsUsersToSend as $rowUser)
        {
            // u.`id`, u.`email`, u.`userType`, u.`dateLastSentSelligent`
            $moUserCurrent = new AbaUser();
            if($moUserCurrent->getUserById( $rowUser["id"] ))
            {
                echo " ; sending to ".$moUserCurrent->email;
                $commSelligent = new SelligentConnectorCommon();
                if( $commSelligent->sendOperationToSelligent(SelligentConnectorCommon::synchroUser, $moUserCurrent, array(), "SynchronizeSelligentUsersCommand"))
                {
                    echo " =OK ";
                    $moUserCurrent->dateLastSentSelligent = HeDate::todaySQL(true);
                    if($moUserCurrent->update( array("dateLastSentSelligent") )){
                        $totalSent = $totalSent +1;
                    }
                }
                else{
                    echo " =".$commSelligent->getResponse();
                    $aRowsNotSent[]=$moUserCurrent->email;
                    $this->processError(" User with email ".$moUserCurrent->email ." not correctly received by Selligent.");
                }
            }

        }

        if( $totalSent!==$totalToSend)
        {
            echo " \n ".date("Y-m-d h:i:s", time() )." The number of user SENT DOES NOT MATCH the user that should be sent to SELLIGENT, process SynchronizeSelligentUsersCommand. Check emails not sent: ".
                    implode(", ", $aRowsNotSent). " END";
            echo " \n  -----------------------------------------------------------------------------------------------";
            return false;
        }



echo " \n ".date("Y-m-d h:i:s", time() )." Success in process SynchronizeSelligentUsersCommand. END";
echo " \n -----------------------------------------------------------------------------------------------";
        return $allSuccess;

    }

    /**
     * @param string $actionDetails
     *
     * @return bool
     */
    private function processError($actionDetails)
    {
        HeLogger::sendLog("CRON SynchronizeSelligentUsersCommand finished with errors " ,
            HeLogger::IT_BUGS_PAYMENTS, HeLogger::CRITICAL,
            " Error processing SynchronizeSelligentUsersCommand: ".$actionDetails);

        return true;
    }

}
