<?php
/** It sends a message to anyone within abaenglish.com domain only
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 22/11/12
 * Time: 12:11
 */
class SendEmailCommand extends AbaConsoleCommand
{
    public function getHelp()
    {
        return parent::getHelp()." \n ";
    }

    public function run($args)
    {

        // The standard of the Campus:
        
        HeAbaMail::sendEmailInternal( $args[0],"",
            $args[1],
            $args[2],   true,   $this,
            HeAbaMail::INFOPROC_LAYOUT );

        
    }
}
