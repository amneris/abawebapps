<?php

/**
 * Select all users PREMIUM that his expiration date has been reached out.
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 22/11/12
 * Time: 12:11
 */
class CancelProfilesPaypalCommand extends AbaConsoleCommand
{
    const MAX_USERS_PROCESS = 1000;
    const EXECUTE_CANCELLATION = true;

    public $aUsersProcessed;

    public function getHelp()
    {
        return parent::getHelp() . " Expects an userId to search and cancel(userId ). Or quantity of users to process() .";
    }

    public function run($args)
    {
      try{
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n CancelProfilesPaypalCommand: executing auto process at " . date("Y-m-d H:i:s", time());
        $allSuccess = true;
        $start = 1;
        $userIds = array();
        if(count($args)==2 && $args[0]=='userId') {
            $userIds[] = array("userId"=>$args[1]);
            $maxUsersProcess = 1;
        } elseif(count($args)==2) {
            $start = $args[0];
            $maxUsersProcess = $args[1];
        } elseif(count($args)==1) {
            $maxUsersProcess = $args[0];
            if($maxUsersProcess>self::MAX_USERS_PROCESS){
                $maxUsersProcess = self::MAX_USERS_PROCESS;
            }
        }else{
            $maxUsersProcess = self::MAX_USERS_PROCESS;
        }

        if($maxUsersProcess>1){
            $moColUsers = new AbaUsers();
            $userIds = $moColUsers->getAllUsersCheckProfilePayPal($start, $maxUsersProcess);
        }

        $total = count($userIds);
        $numIteration = 0;
        foreach($userIds as $userId){
            $numIteration = $numIteration + 1;
            $moUser = new AbaUser();
            $moUser->getUserById($userId["userId"]);
            echo " \n ***** Processing user ".$moUser->email." ****** (iteration = $numIteration)";
            $allSuccess = $this->processUser($moUser);
            echo "----- \n end ------";
        }

        /*$xls = new JPhpCsv('UTF-8', false, 'PaypalUsersWithTrouble_20140521.csv');
        $xls->addArray($this->aUsersProcessed);
        $xls->generateXML("PaypalUsersWithTrouble_20140521.csv", true, true);*/
        if(is_array($this->aUsersProcessed)){
            foreach($this->aUsersProcessed as $userCsvProc){
                file_put_contents("PaypalUsersWithTrouble_20140521.csv", $userCsvProc. " \n ", FILE_APPEND);
            }
        }



        echo " \n " . date("Y-m-d H:i:s", time()) . " Success in process CancelProfilesPaypal. END";
        echo " \n -----------------------------------------------------------------------------------------------";
        return $allSuccess;


      } catch(Exception $exc) {
          $this->processError("Big exception on iteration $numIteration (out of ".$total.'), error= '.$exc->getCode().", ".$exc->getMessage());
      }

        return false;
    }

    /**
     * @param string $actionDetails
     *
     * @return bool
     */
    private function processError($actionDetails)
    {
        echo " \n ".$actionDetails;
        HeLogger::sendLog("CRON CancelProfilesPaypalCommand finished with errors ", HeLogger::PAYMENTS, HeLogger::CRITICAL, " Collecting data from users on CancelProfilesPaypalCommand: " . $actionDetails);
        return true;
    }

    /** User by User
     *
     * @param AbaUser $moUser
     * @return bool
     */
    private function processUser(AbaUser $moUser)
    {
        $allSuccess = true;
        $userId = $moUser->id;

        $moPendingPay = new Payment();
        if (!$moPendingPay->getLastPaymentNotRefundByUserId($userId)) {
            $this->processError("Payment Pending or profile not found");
            return false;
        }

        $moUserCreditPaypal = new AbaUserCreditForms();
        if (!$moUserCreditPaypal->getUserCreditFormById($moPendingPay->idUserCreditForm)) {
            $this->processError(" Credit Form for user $userId not found, id credit form equals to " .
                $moPendingPay->idUserCreditForm);
            return false;
        }

        if($moUserCreditPaypal->default==0){
            $this->processError(" Credit Form for user $userId found for Paypal payment but it is not ".
                "the DEFAULT one anymore" . $moPendingPay->idUserCreditForm);
            return false;
        }

        if(empty($moUserCreditPaypal->registrationId) || trim($moUserCreditPaypal->registrationId)==''){
            $moUserCreditPaypal->registrationId = $moPendingPay->paySuppExtProfId;
        }

        /* @var PaySupplierPayPal $paySupplier */
        $paySupplier = PaymentFactory::createPayment($moPendingPay);
        /* @var Payment $remotePayment */
        $remotePayment = $paySupplier->runGetSubscriptionInfo($userId, $moUserCreditPaypal->registrationId);
        if (!$remotePayment) {
            $this->aUsersProcessed[] = $moUser->id.";".$moUser->email.";".$moUserCreditPaypal->registrationId.";".
                $moPendingPay->status.";NONE; NOT_FOUND_IN_PAYPAL";
            $this->processError(" The user " . $userId . " with profile Id from Paypal number " .
                $moUserCreditPaypal->registrationId . " has not been found in the remote gateway environment.");
            return false;
        }

        if (($moPendingPay->status==PAY_FAIL || $moPendingPay->status==PAY_CANCEL) &&
            $remotePayment->status !== $moPendingPay->status &&
            $remotePayment->paySuppExtProfId == $moPendingPay->paySuppExtProfId
        ) {
            $this->aUsersProcessed[] = $moUser->id.";".$moUser->email.";".$moUserCreditPaypal->registrationId.";".
                                            $moPendingPay->status.";".$remotePayment->status.";TO_BE_CANCELLED";
            if(self::EXECUTE_CANCELLATION){
                /* The subscription in Paypal remote environment is not the same as in our Intranet. " .
                    " Our Intranet for payment id " . $moPendingPay->id . " marks status " .
                    $moPendingPay->status . " while remote Paypal says " . $remotePayment->status */
                /* @var PayGatewayLogLaCaixaResRec $retPaymentSupplier */
                $retPaymentSupplier = $paySupplier->runCancelRecurring($moPendingPay, $moUserCreditPaypal);
                if (!$retPaymentSupplier) {
                    $allSuccess = false;
                }
                echo " \n User ".$moUser->email." has been cancelled in remote Paypal platform successfully.";
            }else{
                echo " \n Simulation on user User ".$moUser->email.
                    " has been cancelled in remote Paypal platform successfully. NOT CANCELLED, IS DISABLED";

            }

        } elseif ($remotePayment->status == $moPendingPay->status &&
            $remotePayment->paySuppExtProfId == $moPendingPay->paySuppExtProfId) {
            echo " \n User ".$moUser->email." is rightly synchronized in both environments.";
        } elseif ($remotePayment->status !== $moPendingPay->status &&
            $remotePayment->paySuppExtProfId == $moPendingPay->paySuppExtProfId) {
            $this->aUsersProcessed[] = $moUser->id.";".$moUser->email.";".$moUserCreditPaypal->registrationId.";".
                $moPendingPay->status.";".$remotePayment->status.";DIFFERENT_STATUS_PROFILE";
            echo " \n User ".$moUser->email." is not synchronized. Check both status for profile ".
                $remotePayment->paySuppExtProfId.
                $remotePayment->status ." !== ". $moPendingPay->status.".";
            $allSuccess = false;
        } else{
            echo " \n User ".$moUser->email." is ignored.";
        }

        return $allSuccess;
    }

}
