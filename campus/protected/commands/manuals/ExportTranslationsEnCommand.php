<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 22/11/12
 * Time: 12:11
 */
class ExportTranslationsEnCommand extends AbaConsoleCommand
{
    public function getHelp()
    {
        return parent::getHelp()." \n ";
    }

    public function run($args)
    {
        echo 'Exporting English translation to CSV for translators to return new expressions';

            $xls = new JPhpCsv('UTF-8', false, 'TranslationsCampus2013_10_03');

            $aColsTitles = array("Row 1"=>array("Col A"=>"Key sentence for IT department, read only",
                                                "Col B"=>"English translation, the one to be translated",
                                                "Col C"=>"EN characters length",
                                                "Col D"=>"Russian Translation",
                                                "Col E"=>"RU characters length",
                                                ),
                                );
           $aExpressions = CMap::mergeArray(array(),require("/var/www/campus.abaenglish.com/public/protected/messages/en/mainApp.php"));
            $row = 1;
            foreach($aExpressions as $keyExpr => $englishTrans )
            {
                $row =$row+1;
                if( substr($englishTrans, 0, 1)=='='){
                    $englishTrans = "'".substr($englishTrans, 1);
                }elseif(  substr($englishTrans, 0, 1)=='-' ){
                    $englishTrans = "'".substr($englishTrans, 0);
                }
                $aColsTitles["Row $row"] = array("Col A"=>$keyExpr,
                                                 "Col B"=>$englishTrans,
                                                 "Col C"=>"=LARGO(B$row)",
                                                 "Col D"=>"",
                                                 "Col E"=>"=LARGO(D$row)",
                                                );
            }


            $xls->addArray($aColsTitles);
            $xls->generateXML("EN_Translations_Campus_2013_10_03.csv", true, true);

        echo " End of TestCommand ";
    }
}
