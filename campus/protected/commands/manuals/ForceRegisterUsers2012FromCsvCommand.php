<?php
/**
 * Class ForceRegisterUsers1738FromCsvCommand
 * ENHANCEMENT 1738, 2013-09-12, Probably could be deleted after 2013-12-10
 */
class ForceRegisterUsers2012FromCsvCommand extends AbaConsoleCommand
{
    private $pathFileIn='C:\Quino\Abaenglish\IncidenciasBBDD\ENH-2180\IN\Complutense20131118.csv';
    private $pathFileOut='C:\Quino\Abaenglish\IncidenciasBBDD\ENH-2180\OUT\\';
    //        $urlWs = 'http://campus.abaenglish.com/wsoapaba/usersapi/';
    //        $urlWs = 'http://campus.abaenglish.com/wsoapaba/usersapi/';
    private $urlWs = 'http://prod2.abaenglish.com/wsoapaba/usersapi/';
    private $colsPositions = 'email,password,langEnv,name,surnames, telephone,voucherCode,securityCode,idPartnerSource,
    idPeriodPay,countryId,currentLevel';
    private $limit = 140;

    public function getHelp()
    {
        echo ' This expects a CSV file input to register new users. Check ENHANCEMENT 2012 for more info. ';
        return parent::getHelp()." \n ";
    }

    public function run($args)
    {
        if(count($args)>0)
        {
           $this->pathFileIn = $args[0];
           $this->pathFileOut = $args[1];
        }


        if(file_exists($this->pathFileOut)){
            $pathFileUsersNotInserted = $this->pathFileOut.'usersNotInserted_'.date("Ymd_Hi",time()).'.csv';
            $this->pathFileOut =$this->pathFileOut.'usersProcessedOn_'.date("Ymd_Hi",time()).'.csv';
            file_put_contents( $this->pathFileOut, " \n", FILE_APPEND);
            file_put_contents( $pathFileUsersNotInserted, " \n", FILE_APPEND);
        } else {
            echo " File does not exist mate ".$this->pathFileOut;
            return;
        }

        if(!file_exists($this->pathFileIn)){
            echo " File does not exist mate ".$this->pathFileIn;
            return;
        }

        ini_set("soap.wsdl_cache_enabled", 0);

        $totalLines = count(file($this->pathFileIn));
        $aAllUsers = array();
        $usersLinesCreated = 0;
        $usersLinesNotCreated = 0;
        $usersLinesRepeated = 0;


        $handle = @fopen($this->pathFileIn, "r");
        if ($handle)
        {
            $iLine = 0;
            while (($newLine = fgets($handle, 4096)) !== false && $this->limit>$iLine)
            {
                $iLine++;

                $aPartsLine = explode(';',trim($newLine,"\r\n"));
                list($email, $password, $langEnv, $name, $surnames, $telephone, $voucherCode, $securityCode,
                    $idPartnerSource, $idPeriodPay, $countryId, $currentLevel) = $aPartsLine;
                $email = trim($email);
                echo " Processing ".$email;
                if($email=='0' || $email==''){
                    break;
                }


                $client = new SoapClient( $this->urlWs,array('soap_version'=>SOAP_1_2, 'port'=> 80, 'trace' => 1)  );

                if( array_search($email, $aAllUsers, true)==false ){
                    /*--------------------------------------------------------------------------*/
                    /*--         SOAP REQUESTS              --*/
                    // registerUserFromLevelTest( $signature, $email, $langEnv, $currentLevel, $insideCampus, $idCountry, $idPartner=null )
                    $signatureWsAbaEnglish = md5( WORD4WS.$email.$langEnv.$currentLevel.'1'.$countryId.$idPartnerSource);
                    $return = NULL;
                    try
                    {
                        /* @var WsoapabaController $client */
                        /* -First we register user in LevelTest - */
                        $return = $client->registerUserFromLevelTest($signatureWsAbaEnglish,$email,$langEnv,
                                                                    $currentLevel,'1',$countryId,$idPartnerSource);
                        if( array_key_exists("userId", $return) ){

                            /* -First we register Partner User - */
                            $signatureWsAbaEnglish = md5( WORD4WS.$email.$password.$langEnv.$name.$surnames.$telephone.
                                $voucherCode.$securityCode.$idPartnerSource.$idPeriodPay.$countryId.'');
                            $return = $client->registerUserPartner($signatureWsAbaEnglish,$email,$password,$langEnv,
                                $name,$surnames,$telephone,$voucherCode,$securityCode,$idPartnerSource,$idPeriodPay,
                                $countryId,'');
                            if( array_key_exists("userId", $return) ){
                                $moUser = new AbaUser();
                                if( $moUser->getUserByEmail($email, "", false)) {
                                    $moUser->updateSavedUserObjective(1);
                                }

                                file_put_contents($this->pathFileOut, 'OK: '.$email.",".$return["userId"]." \n", FILE_APPEND);
                                echo ' \n Registering Premium Partner user'.$email.'= '.$return["result"].' , id='.$return["userId"];
                                $usersLinesCreated++;
                                $aAllUsers[]=$email;
                            }
                            else{
                                $keyRet= array_keys($return);
                                file_put_contents($this->pathFileOut, "*ERROR,NOT INSERTED: ".$email.", error ".$return[$keyRet[0]]." \n", FILE_APPEND);
                                echo ' \n Registering Premium Partner user '.$email.'= error '.$keyRet[0]."(".$return[$keyRet[0]].")";
                                file_put_contents($pathFileUsersNotInserted, $newLine, FILE_APPEND);
                                $usersLinesNotCreated++;
                            }

                        }
                        else{
                            $keyRet= array_keys($return);
                            file_put_contents($this->pathFileOut, "*ERROR,NOT INSERTED: ".$email.", error ".$return[$keyRet[0]]." \n", FILE_APPEND);
                            echo ' \n Registering Level Test user '.$email.'= error '.$keyRet[0]."(".$return[$keyRet[0]].")";
                            file_put_contents($pathFileUsersNotInserted, $newLine, FILE_APPEND);
                            $usersLinesNotCreated++;
                        }

                    }
                    catch (Exception $exc)
                    {
                        file_put_contents($this->pathFileOut, $email."*EXCEPTION: ".$exc->getMessage().",".$newLine." \n", FILE_APPEND);
                        echo ' \n Registering user '.$email.'= exception '.print_r($return);
                        $usersLinesNotCreated++;
                    }
                    /*--------------------------------------------------------------------------*/
                }
                else{
                    file_put_contents($this->pathFileOut, "*NOT INSERTED: ".$email.", repeated so not processed."." \n", FILE_APPEND);
                    echo '\n Duplicated user on file '.$email.'= repeated so not processed';
                    $usersLinesRepeated++;
                }

            }

            fclose($handle);


            if($iLine!==$totalLines){
                echo "Some errors: The lines processed are ".$iLine." and were expected ".$totalLines.
                        ". Please review file and database mismatch.";
            }
            else{
                if( ($usersLinesCreated+$usersLinesNotCreated+$usersLinesRepeated)!==($iLine-1) ){
                    echo "Some errors, break down: Users lines created= ".$usersLinesCreated." , users lines NOT created=".$usersLinesNotCreated.
                        ", users lines REPEATED=".$usersLinesRepeated;
                }
                else{
                    // TOTAL SUCCESS
                    echo "Success: Total lines processed=".$totalLines." Break down: Users lines created= ".
                        $usersLinesCreated." , users lines NOT created=".$usersLinesNotCreated.
                        ", users lines REPEATED=".$usersLinesRepeated;
                }
            }
        }


    }
}
