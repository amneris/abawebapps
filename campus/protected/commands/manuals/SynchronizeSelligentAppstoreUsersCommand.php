<?php

class SynchronizeSelligentAppstoreUsersCommand extends AbaConsoleCommand {

    public function getHelp() {
        return parent::getHelp()." You can set a number to set how many records you want to send to SELLIGENT. \n " ;
    }

    //
    // TEST:    php -f  /var/www/abawebapps/campus/public/script.php  SynchronizeSelligentAppstoreUsers
    // TEST:    php -f  /var/www/abawebapps/campus/public/script.php  SynchronizeSelligentAppstoreUsers  "/var/www/appstoreUsers.csv"
    //
    public function run($args)
    {
echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
echo " \n START. SynchronizeSelligentUsersCommand: executing auto process at ".date("Y-m-d h:i:s", time() );
        $allSuccess = true;


        $stFilePath = '/var/www/appstoreUsers.csv';
        $stUsersIds = array();

        if(count($args) > 0) {
            $stFilePath = trim($args[0]) ;
        }

        if(!file_exists($stFilePath)){
            echo " \n File does not exist: " . $stFilePath . "\n";
            return;
        }

        //
        $totalLines =   count(file($stFilePath));
        $iLines =       0;

echo " \n Total lines: " . $totalLines . "\n";

        //
        $handle = @fopen($stFilePath, "r");

        if ($handle) {

            while (($newLine = fgets($handle, 4096)) !== false) {

                $iLines++;

                $userId = trim($newLine);

                $stUsersIds[$userId] = $userId;
            }

            fclose($handle);

            if($iLines !== $totalLines){
                echo " \n Some errors: The lines processed are " . $iLines . " and were expected " . $totalLines . ". Please review file and database mismatch. \n ";
            }
        }


        $moUsers =          new AbaUsers();
        $aRowsUsersToSend = $moUsers->getAppstoreUsersShouldSentSelligent($stUsersIds);

        if( !is_array($aRowsUsersToSend) ) {
            echo " \n ".date("Y-m-d h:i:s", time() )." There are no users to be sent to SELLIGENT. It looks like everything is synchronized. Process SynchronizeSelligentUsersCommand. END";
            echo " \n ----------------------------------------------------------------------------------------------- \n ";
            return false;
        }

echo "\n+ + + + + + + + + + + + + + + + + + + + + + + + +\n";

        $totalToSend =  count($aRowsUsersToSend);
        $aRowsNotSent = array();
        $totalSent =    0;

        foreach( $aRowsUsersToSend as $rowUser) {

            $moUserCurrent = new AbaUser();

            if($moUserCurrent->getUserById( $rowUser["id"] )) {

echo "\n+ + + + + sending to " . $moUserCurrent->email;

                $commSelligent = new SelligentConnectorCommon();

                if( $commSelligent->sendOperationToSelligent(SelligentConnectorCommon::synchroUser, $moUserCurrent, array(), "SynchronizeSelligentUsersCommand")) {

                    echo " =OK ";

                    $moUserCurrent->dateLastSentSelligent = HeDate::todaySQL(true);

                    if($moUserCurrent->update( array("dateLastSentSelligent") )){
                        ++$totalSent;
                    }
                }
                else{
                    echo " =" . $commSelligent->getResponse();
                    $aRowsNotSent[] = $moUserCurrent->email;
                    $this->processError(" User with email ".$moUserCurrent->email ." not correctly received by Selligent.");
                }
            }
        }

        if( $totalSent !== $totalToSend) {

            echo " \n ".date("Y-m-d h:i:s", time() )." The number of user SENT DOES NOT MATCH the user that should be sent to SELLIGENT, process SynchronizeSelligentUsersCommand. Check emails not sent: ".
                    implode(", ", $aRowsNotSent). " END";
            echo " \n  ----------------------------------------------------------------------------------------------- \n ";
            return false;
        }

echo " \n ".date("Y-m-d h:i:s", time() )." Success in process SynchronizeSelligentUsersCommand. END";
echo " \n ----------------------------------------------------------------------------------------------- \n ";

        return $allSuccess;
    }

    /**
     * @param string $actionDetails
     *
     * @return bool
     */
    private function processError($actionDetails)
    {
        HeLogger::sendLog("CRON SynchronizeSelligentUsersCommand finished with errors " ,
            HeLogger::IT_BUGS_PAYMENTS, HeLogger::CRITICAL,
            " Error processing SynchronizeSelligentUsersCommand: ".$actionDetails);

        return true;
    }

}
