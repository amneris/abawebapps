<?php

class ForceAbaPaymentCommand extends AbaConsoleCommand
{
    //
    // TEST:    php -f  /var/www/abawebapps/campus/public/script.php ForceAbaPayment 1596748 954dd454
    // TEST:    php -f  /var/www/abawebapps/campus/public/script.php ForceAbaPayment 918845 a680ed0a
    //
    // TEST:    php -f  /var/www/abawebapps/campus/public/script.php ForceAbaPayment 1499038 5db326fc_AK1
    //
    // TEST:    php -f  /var/www/abawebapps/campus/public/script.php ForceAbaPayment 918845 a680ed0a_AK2
    //
    // TEST:    php -f  /var/www/abawebapps/campus/public/script.php ForceAbaPayment 3553235 13ce27d5_AK1
    // TEST:    php -f  /var/www/abawebapps/campus/public/script.php ForceAbaPayment 3553235 13ce27d5_AK2
    // TEST:    php -f  /var/www/abawebapps/campus/public/script.php ForceAbaPayment 3553235 13ce27d5_AK3
    // TEST:    php -f  /var/www/abawebapps/campus/public/script.php ForceAbaPayment 3553235 13ce27d5_AK4
    // TEST:    php -f  /var/www/abawebapps/campus/public/script.php ForceAbaPayment 3553235 13ce27d5_AK5
    // TEST:    php -f  /var/www/abawebapps/campus/public/script.php ForceAbaPayment 3553235 13ce27d5_AK6
    // TEST:    php -f  /var/www/abawebapps/campus/public/script.php ForceAbaPayment 3553235 13ce27d5_AK7

    // TEST:    /usr/bin/php  /var/www/abawebapps/campus/public/script.php ForceAbaPayment 3892190 29ec1295
    //
    // TEST:    /usr/bin/php  /var/www/abawebapps/campus/public/script.php ForceAbaPayment  994104  AK_60d5db8b_2
    //
    // TEST:    /usr/bin/php  /var/www/abawebapps/campus/public/script.php ForceAbaPayment  2152853  AK_7da6eda4_2
    // TEST:    /usr/bin/php  /var/www/abawebapps/campus/public/script.php ForceAbaPayment  2152853  AK_7da6eda4_3
    //
    public function run($args)
    {
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n ForceAbaPaymentCommand: executing auto process at " . date("Y-m-d H:i:s", time() );

        $userId =                   null;
        $paymentControlCheckId =    null;
        $totalToRefund =            null;
        $paySuppOrderId =           null;

        //
        // USER ID
        if (count($args) >= 1) {
            if (!is_numeric($args[0])) {
                echo " The first argument should be an integer telling how many records we want to process for every iteration.";
                return false;
            }
            $userId = intval($args[0]);
        }
        else {
            return false;
        }

        //
        // SUCCESS PAYMENT ID TO REFUND
        if (count($args) >= 2) {
            $paymentControlCheckId = trim($args[1]);
        }
        else {
            return false;
        }

        //
        // SUCCESS PAYMENT ID TO REFUND
        if (count($args) >= 3) {
            $paySuppOrderId = trim($args[2]);
        }


        //
        //
        $oPaymentCtrlChk = new PaymentControlCheck();
        $oPaymentCtrlChk->getPaymentById($paymentControlCheckId);

        if ($oPaymentCtrlChk->userId != $userId) {
            echo "\n+++++++ Error. Datos incorrectos. +++++++\n";
            return false;
        }


        //
        //
        $oPayment = new Payment();
        $oPayment->getPaymentById($paymentControlCheckId);

        if ($oPayment && $oPayment->id == $paymentControlCheckId) {
            echo "\n+++++++ Error. Payment alredy exists. +++++++\n";
            return false;
        }


        //
        $moUser = new AbaUser();
        $moUser->getUserById($oPaymentCtrlChk->userId);

        //
        $idPartner = Yii::app()->config->get("ABA_PARTNERID");

        if(trim($moUser->idPartnerCurrent) != '') {
            $idPartner = $moUser->idPartnerCurrent;
        }

        //
        switch($oPaymentCtrlChk->paySuppExtId) {
            case PAY_SUPPLIER_PAYPAL:

            case PAY_SUPPLIER_CAIXA:
            case PAY_SUPPLIER_ALLPAGO_BR:
            case PAY_SUPPLIER_ALLPAGO_MX:
            case PAY_SUPPLIER_ALLPAGO_MX_OXO: // ???
            case PAY_SUPPLIER_ADYEN:
            case PAY_SUPPLIER_ADYEN_HPP:

                $oOnlinePayCommon = new OnlinePayCommon();
                $userCreditCard =   new AbaUserCreditForms($moUser->id);

                $oPayGatewayLog = false;

                switch($oPaymentCtrlChk->paySuppExtId) {
                    case PAY_SUPPLIER_PAYPAL:
                        $oPayGatewayLog = new PayGatewayLogPayPal($oPaymentCtrlChk->paySuppExtId);
                        break;
                    case PAY_SUPPLIER_CAIXA:
                        $oPayGatewayLog = new PayGatewayLogLaCaixaResRec($oPaymentCtrlChk->paySuppExtId); // PayGatewayLogLaCaixaReqSent
                        break;

                    case PAY_SUPPLIER_ADYEN:
                    case PAY_SUPPLIER_ADYEN_HPP:
                        $oPayGatewayLog = new PayGatewayLogAdyenResRec($oPaymentCtrlChk->paySuppExtId); // PayGatewayLogAdyenResRec
                        break;

                    case PAY_SUPPLIER_ALLPAGO_BR:
                    case PAY_SUPPLIER_ALLPAGO_MX:
                    case PAY_SUPPLIER_ALLPAGO_MX_OXO:
                        $oPayGatewayLog = new PayGatewayLogAllpago($oPaymentCtrlChk->paySuppExtId);
                        break;
                }

                $Order_number = '';

                if(trim($paySuppOrderId) != '') {
                    $Order_number = $paySuppOrderId;
                }
                else {
                    $Order_number = $oPaymentCtrlChk->paySuppOrderId;
                }

                //
                $modFormPayment = new PaymentForm();
                //
                $payment = $oOnlinePayCommon->savePayment($modFormPayment, $oPaymentCtrlChk, $userCreditCard, $oPayGatewayLog, $idPartner, 0);
                if (!$payment) {
                }
                else {
                    $payment->paySuppOrderId = $Order_number;
                    $payment->update(array("paySuppOrderId"));
                }

                //
                $nextPayment = $payment->createDuplicateNextPayment();
                if(!$nextPayment){
                }

                //
                $prodSelected = new ProductPrice();
                $prodSelected = $prodSelected->getProductById($payment->idProduct);
                if (!$prodSelected) {
                    $userTypeProd = PREMIUM;
                } else{
                    $userTypeProd = $prodSelected->userType;
                }

                //
                $commUser = new UserCommon();
                $moUser = $commUser->convertToPremiumCron($moUser, $idPartner, $nextPayment->dateToPay, $userTypeProd);
                if (!$moUser) {
                }

                //
                $nextPayment->id = HeAbaSHA::generateIdPayment($moUser, $nextPayment->dateToPay);

                if (!$nextPayment->paymentProcess()) {
                }
                else {
                    $nextPayment->paySuppOrderId = $Order_number;
                    $nextPayment->update(array("paySuppOrderId"));
                }

                $oPaymentCtrlChk->status = PAY_SUCCESS;
                $oPaymentCtrlChk->update(array("status"));

                break;

            case PAY_SUPPLIER_XFRIENDS: // ???
            case PAY_SUPPLIER_GROUPON: // ???
                break;
            case PAY_SUPPLIER_ALLPAGO_BR_BOL: // ???
                break;
            default:
                break;
        }

        echo " \n---" . $oPaymentCtrlChk->userId . "---" . $oPaymentCtrlChk->id . "---\n";
        echo " \n  Finished process ForceAbaPaymentCommand at ".date('Y-M-D H:m:s');
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
    }

}
