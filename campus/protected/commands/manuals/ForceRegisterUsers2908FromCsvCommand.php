<?php

/**
 * Class ForceRegisterUsers2908FromCsvCommand
 * ENHANCEMENT 2908, 2014-06-12
 */
class ForceRegisterUsers2908FromCsvCommand extends AbaConsoleCommand
{
    private $_pathFileIn = 'C:\Quino\Abaenglish\IncidenciasBBDD\ENH-2908\IN\LoadUsersJuntaExtremadura_20140619_usuario.csv';
    private $_pathFileOut = 'C:\Quino\Abaenglish\IncidenciasBBDD\ENH-2908\OUT\\';
    //        $urlWs = 'http://campus.abaenglish.com/wsoapaba/usersapi/';
//    private $_urlWs = 'http://campus.abaenglish.com/wsoapaba/usersapi/';
    private $_urlWs = 'http://campus.aba.local/wsoapaba/usersapi/';

    private $_colsPositions = 'email,password,langEnv,name,surnames, telephone,voucherCode,securityCode,idPartnerSource,
    idPeriodPay,countryId,currentLevel';
    private $_limit = 121;

    public function getHelp()
    {
        echo ' This expects a CSV file input to register new users. Check ENHANCEMENT 2098 for more info. ';
        return parent::getHelp() . " \n ";
    }

    public function run($args)
    {
        if (count($args) > 0) {
            $this->_pathFileIn = $args[0];
            $this->_pathFileOut = $args[1];
        }

        if (file_exists($this->_pathFileOut)) {
            $pFileNotInserted = $this->_pathFileOut . 'usersNotInserted_' . date("Ymd_Hi", time()) . '.csv';
            $this->_pathFileOut = $this->_pathFileOut . 'usersProcessedOn_' . date("Ymd_Hi", time()) . '.csv';
            file_put_contents($this->_pathFileOut, " \n", FILE_APPEND);
            file_put_contents($pFileNotInserted, " \n", FILE_APPEND);
        } else {
            echo " File does not exist mate " . $this->_pathFileOut;
            return;
        }

        if (!file_exists($this->_pathFileIn)) {
            echo " File does not exist mate " . $this->_pathFileIn;
            return;
        }

        ini_set("soap.wsdl_cache_enabled", 0);

        $totalLines = count(file($this->_pathFileIn));
        $aAllUsers = array();
        $usersLinesCreated = 0;
        $usersLinesNotCreated = 0;
        $usersLinesRepeated = 0;


        $handle = @fopen($this->_pathFileIn, "r");
        if ($handle) {
            $iLine = 0;
            while (($newLine = fgets($handle, 4096)) !== false && $this->_limit > $iLine) {
                $iLine++;

                $aPartsLine = explode(';', trim($newLine, "\r\n"));
                list($email, $password, $langEnv, $name, $surnames, $telephone, $voucherCode, $securityCode,
                    $idPartnerSource, $idPeriodPay, $countryId, $currentLevel) = $aPartsLine;
                $email = trim($email);
                echo " Processing " . $email;
                if ($email == '0' || $email == '') {
                    break;
                }


                $client = new SoapClient($this->_urlWs, array('soap_version' => SOAP_1_2, 'port' => 80, 'trace' => 1));
                if (array_search($email, $aAllUsers, true) == false) {
                    /*--------------------------------------------------------------------------*/
                    /*--         SOAP REQUESTS              --*/
                    $return = NULL;
                    try {
                        /* @var WsoapabaController $client */
                        /* -First we register Partner User - */
                        $signatureWs = md5(WORD4WS . $email . $password . $langEnv . $name .
                                            $surnames . $telephone . $voucherCode . $securityCode . $idPartnerSource .
                                            $idPeriodPay . $countryId . DEVICE_SOURCE_PC);
                        $return = $client->registerUserPartner($signatureWs, $email, $password, $langEnv,
                            $name, $surnames, $telephone, $voucherCode, $securityCode, $idPartnerSource, $idPeriodPay,
                            $countryId, DEVICE_SOURCE_PC);
                        if (array_key_exists("userId", $return)) {
                            $moUser = new AbaUser();
                            if ($moUser->getUserByEmail($email, "", false)) {
                                $moUser->updateSavedUserObjective(1);
                                $moUser->currentLevel = $currentLevel;
                                $moUser->langCourse = $langEnv;
                                $moUser->teacherId = 1428090;
                                $moUser->update(array('currentLevel','teacherId','langCourse'));
                            }

                            file_put_contents($this->_pathFileOut, 'OK: ' . $email . "," . $return["userId"] . " \n",
                                                                        FILE_APPEND);
                            echo ' \n Registering Premium Partner user' . $email . '= ' . $return["result"] .
                                                                        ' , id=' . $return["userId"];
                            $usersLinesCreated++;
                            $aAllUsers[] = $email;
                        } else {
                            $keyRet = array_keys($return);
                            file_put_contents($this->_pathFileOut, "*ERROR,NOT INSERTED: " . $email . ", error " .
                                                    $return[$keyRet[0]] . " \n", FILE_APPEND);
                            echo ' \n Registering Premium Partner user ' . $email . '= error ' . $keyRet[0] .
                                                    "(" . $return[$keyRet[0]] . ")";
                            file_put_contents($pFileNotInserted, $newLine, FILE_APPEND);
                            $usersLinesNotCreated++;
                        }
                    } catch (Exception $exc) {
                        file_put_contents($this->_pathFileOut, $email . "*EXCEPTION: " . $exc->getMessage() . "," . $newLine . " \n", FILE_APPEND);
                        echo ' \n Registering user ' . $email . '= exception ' . print_r($return);
                        $usersLinesNotCreated++;
                    }
                    /*--------------------------------------------------------------------------*/
                } else {
                    file_put_contents($this->_pathFileOut, "*NOT INSERTED: " . $email . ", repeated so not processed." . " \n", FILE_APPEND);
                    echo '\n Duplicated user on file ' . $email . '= repeated so not processed';
                    $usersLinesRepeated++;
                }

            }

            fclose($handle);


            if ($iLine !== $totalLines) {
                echo "Some errors: The lines processed are " . $iLine . " and were expected " . $totalLines .
                    ". Please review file and database mismatch.";
            } else {
                if (($usersLinesCreated + $usersLinesNotCreated + $usersLinesRepeated) !== ($iLine - 1)) {
                    echo "Some errors, break down: Users lines created= " . $usersLinesCreated . " , users lines NOT created=" . $usersLinesNotCreated .
                        ", users lines REPEATED=" . $usersLinesRepeated;
                } else {
                    // TOTAL SUCCESS
                    echo "Success: Total lines processed=" . $totalLines . " Break down: Users lines created= " .
                        $usersLinesCreated . " , users lines NOT created=" . $usersLinesNotCreated .
                        ", users lines REPEATED=" . $usersLinesRepeated;
                }
            }
        }


    }
}
