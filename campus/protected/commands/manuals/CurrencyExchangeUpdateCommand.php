<?php
/**
 * In case of any failure of synchronization between Campus and Selligent
 *  this script can be used to force the creation of a group of users in SELLIGENT.
 * Created by Quino
 * User: Quino
 * Date: 08/03/2013
 * Time: 12:11
 */
class CurrencyExchangeUpdateCommand extends AbaConsoleCommand
{
    const EUR_TO_BRL = 'EUR_BRL';
    const BRL_TO_EUR = 'BRL_EUR';
    const USD_TO_BRL = 'USD_BRL';
    const BRL_TO_USD = 'BRL_USD';
    const USD_TO_EUR = 'USD_EUR';
    const EUR_TO_USD = 'EUR_USD';
    const EUR_TO_MXN = 'EUR_MXN';
    const MXN_TO_EUR = 'MXN_EUR';
    const USD_TO_MXN = 'USD_MXN';
    const MXN_TO_USD = 'MXN_USD';

    const Y_USD_TO_EUR = 'USDEUR';
    const Y_USD_TO_BRL = 'USDBRL';
    const Y_USD_TO_MXN = 'USDMXN';
    const Y_EUR_TO_BRL = 'EURBRL';
    const Y_EUR_TO_USD = 'EURUSD';
    const Y_EUR_TO_MXN = 'EURMXN';
    const Y_BRL_TO_EUR = 'BRLEUR';
    const Y_BRL_TO_USD = 'BRLUSD';
    const Y_MXN_TO_EUR = 'MXNEUR';
    const Y_MXN_TO_USD = 'MXNUSD';


    protected $alerts = false;

    public function getHelp() {
        return parent::getHelp()." First argument: You can set a number to set how many records you want ".
        "to send to SELLIGENT. \n ".
        " Second argument: Optional, you can set a registration date users. ".
        "Otherwise all users no synchronization confirmed by Sellignt will be processed.";
    }

    public function run($args)
    {
        $alert = false;

        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n START. CurrencyExchangeUpdateCommand: executing auto process at ".date("Y-m-d h:i:s", time() );

//$amount =           1;
//$from_Currency =    "EUR";
//$to_Currency =      "USD";
//$amount =           urlencode($amount);
//$from_Currency =    urlencode($from_Currency);
//$to_Currency =      urlencode($to_Currency);
//$get =              file_get_contents("https://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency");
//
//$get =              explode("<span class=bld>",$get);
//$get =              explode("</span>",$get[1]);
//$converted_amount = preg_replace("/[^0-9\.]/", null, $get[0]);
//
//echo "\n+ + + + + + + + + +\n";
//echo $converted_amount;
//echo "\n+ + + + + + + + + +\n";
//
//
//
//$url =      "http://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency";
//$request =  curl_init();
//$timeOut =  0;
//curl_setopt ($request, CURLOPT_URL, $url);
//curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1);
//curl_setopt ($request, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
//curl_setopt ($request, CURLOPT_CONNECTTIMEOUT, $timeOut);
//$get = curl_exec($request);
//curl_close($request);
//
//$get =              explode("<span class=bld>",$get);
//$get =              explode("</span>",$get[1]);
//$converted_amount = preg_replace("/[^0-9\.]/", null, $get[0]);
//
//echo "\n+ + + + + + + + + +\n";
//echo $converted_amount;
//echo "\n+ + + + + + + + + +\n";
//exit();

//
//// "http://apilayer.net/api/live?access_key=53f4f34368a63dee07baa384a1fa3e3"
//
//// set API Endpoint, Access Key, required parameters
//$endpoint =     'convert';
//$access_key =   '53f4f34368a63dee07baa384a1fa3e32';
//
//$from =     'USD';
//$to =       'EUR';
//$amount =   1;
//
//// initialize CURL:
//$ch = curl_init('http://apilayer.net/api/'.$endpoint.'?access_key='.$access_key.'&from='.$from.'&to='.$to.'&amount='.$amount.'');
//curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//// get the (still encoded) JSON data:
//$json = curl_exec($ch);
//curl_close($ch);
//// Decode JSON response:
//$conversionResult = json_decode($json, true);
//
////// access the conversion result
////echo $conversionResult['result'];
//
//echo " \n ".date("Y-m-d h:i:s", time() )." + + + + + + + \n";
//
//print_r($conversionResult);
//
//echo " \n ".date("Y-m-d h:i:s", time() )." Success in process CurrencyExchangeUpdateCommand. END";
//echo " \n -----------------------------------------------------------------------------------------------";




//
//// set API Endpoint, Access Key, required parameters
//$endpoint =     'live';
//$access_key =   '53f4f34368a63dee07baa384a1fa3e32';
//
//$currencies =   'USD,EUR,BRL,MXN';
//$source =       'USD';
//$format =       1;
////
////
////const EUR_TO_BRL = 'EUR_BRL';
////const BRL_TO_EUR = 'BRL_EUR';
////const USD_TO_BRL = 'USD_BRL';
////const BRL_TO_USD = 'BRL_USD';
////const USD_TO_EUR = 'USD_EUR';
////const EUR_TO_USD = 'EUR_USD';
////const EUR_TO_MXN = 'EUR_MXN';
////const MXN_TO_EUR = 'MXN_EUR';
////const USD_TO_MXN = 'USD_MXN';
////const MXN_TO_USD = 'MXN_USD';
//
//
//// initialize CURL:
//$ch = curl_init('http://apilayer.net/api/'.$endpoint.'?access_key='.$access_key.'&currencies='.$currencies.'&source='.$source.'&format='.$format.'');
//curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//// get the (still encoded) JSON data:
//$json = curl_exec($ch);
//curl_close($ch);
//// Decode JSON response:
//$conversionResult = json_decode($json, true);
//
////// access the conversion result
////echo $conversionResult['result'];
//
//echo " \n ".date("Y-m-d h:i:s", time() )." + + + + + + + \n";
//
//print_r($conversionResult);
//
//echo " \n ".date("Y-m-d h:i:s", time() )." Success in process CurrencyExchangeUpdateCommand. END";
//echo " \n -----------------------------------------------------------------------------------------------";
//
//return true;


//        http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.xchange where pair in ("USDEUR", "USDJPY", "USDBGN", "USDCZK", "USDDKK", "USDGBP", "USDHUF", "USDLTL", "USDLVL", "USDPLN", "USDRON", "USDSEK", "USDCHF", "USDNOK", "USDHRK", "USDRUB", "USDTRY", "USDAUD", "USDBRL", "USDCAD", "USDCNY", "USDHKD", "USDIDR", "USDILS", "USDINR", "USDKRW", "USDMXN", "USDMYR", "USDNZD", "USDPHP", "USDSGD", "USDTHB", "USDZAR", "USDISK")&env=store://datatables.org/alltableswithkeys
//        https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22USDMXN%22%2C%20%22USDCHF%22)&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=


//        http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.xchange where pair in (
//        "USDEUR", "USDBRL", "USDMXN",
//        "EURBRL", "EURUSD", "EURMXN",
//        "BRLEUR", "BRLUSD",
//        "MXNEUR", "MXNUSD")&env=store://datatables.org/alltableswithkeys

//        http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.xchange where pair in ("USDEUR", "USDBRL", "USDMXN", "EURBRL", "EURUSD", "EURMXN", "BRLEUR", "BRLUSD", "MXNEUR", "MXNUSD")&env=store://datatables.org/alltableswithkeys


//        http://finance.yahoo.com/webservice/v1/symbols/allcurrencies/quote?format=json


//
//
//const EUR_TO_BRL = 'EUR_BRL';
//const EUR_TO_USD = 'EUR_USD';
//const EUR_TO_MXN = 'EUR_MXN';

//const BRL_TO_EUR = 'BRL_EUR';
//const BRL_TO_USD = 'BRL_USD';

//const USD_TO_BRL = 'USD_BRL';
//const USD_TO_EUR = 'USD_EUR';
//const USD_TO_MXN = 'USD_MXN';

//const MXN_TO_EUR = 'MXN_EUR';
//const MXN_TO_USD = 'MXN_USD';



        /*BRL -> unitEur*/
        /*
                $EurBrl = file_get_contents('http://currencies.apps.grandtrunk.net/getlatest/EUR/BRL');
                $EurBrl = trim($EurBrl);
                if(!is_numeric($EurBrl))
                {
                    $alert .= 'the conversion of EurBrl is not float type. Check if the service is still working at: http://currencies.apps.grandtrunk.net/getlatest/EUR/BRL ***';
                }
                $EurBrl = floatval($EurBrl);
        */
        /*BRL -> outEur*/
        /*
                $BrlEur = file_get_contents('http://currencies.apps.grandtrunk.net/getlatest/BRL/EUR');
                $BrlEur = trim($BrlEur);
                if(!is_numeric($BrlEur))
                {
                    $alert .= 'the conversion of EurBrl is not float type. Check if the service is still working at: http://currencies.apps.grandtrunk.net/getlatest/BRL/EUR ***';
                }
                $BrlEur = floatval($BrlEur);
        */
        /*BRL -> unitUsd*/
        /*
                $UsdBrl = file_get_contents('http://currencies.apps.grandtrunk.net/getlatest/USD/BRL');
                $UsdBrl = trim($UsdBrl);
                if(!is_numeric($UsdBrl))
                {
                    $alert .= 'the conversion of EurBrl is not float type. Check if the service is still working at: http://currencies.apps.grandtrunk.net/getlatest/USD/BRL ***';
                }
                $UsdBrl = floatval($UsdBrl);
        */
        /*BRL -> outUsd*/
        /*
                $BrlUsd = file_get_contents('http://currencies.apps.grandtrunk.net/getlatest/BRL/USD');
                $BrlUsd = trim($BrlUsd);
                if(!is_numeric($BrlUsd))
                {
                    $alert .= 'the conversion of EurBrl is not float type. Check if the service is still working at: http://currencies.apps.grandtrunk.net/getlatest/BRL/USD ***';
                }
                $BrlUsd = floatval($BrlUsd);
        */
        /*EUR -> unitUsd*/
        /*
                $UsdEur = file_get_contents('http://currencies.apps.grandtrunk.net/getlatest/USD/EUR');
                $UsdEur = trim($UsdEur);
                if(!is_numeric($UsdEur))
                {
                    $alert .= 'the conversion of EurBrl is not float type. Check if the service is still working at: http://currencies.apps.grandtrunk.net/getlatest/USD/EUR ***';
                }
                $UsdEur = floatval($UsdEur);
        */
        /*EUR -> outUsd*/
        /*
                $EurUsd = file_get_contents('http://currencies.apps.grandtrunk.net/getlatest/EUR/USD');
                $EurUsd = trim($EurUsd);
                if(!is_numeric($EurUsd))
                {
                    $alert .= 'the conversion of EurBrl is not float type. Check if the service is still working at: http://currencies.apps.grandtrunk.net/getlatest/EUR/USD ***';
                }
                $EurUsd = floatval($EurUsd);
        */

        // + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +
        // + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +
        // + + + + + 2015-01-01 v2.0
        //


//        /*BRL -> unitEur*/
//        $EurBrlJson = file_get_contents('http://rate-exchange.appspot.com/currency?from=EUR&to=BRL');
//        $EurBrlJson = json_decode(trim($EurBrlJson), true);
//        $EurBrl = 0;
//        if(isset($EurBrlJson['rate']) AND is_numeric($EurBrlJson['rate'])) {
//            $EurBrl = floatval($EurBrlJson['rate']);
//        }
//        else {
//            $alert .= 'the conversion of EurBrl is not float type. Check if the service is still working at: http://rate-exchange.appspot.com/currency?from=EUR&to=BRL ***';
//        }
//
//        /*BRL -> outEur*/
//        $BrlEurJson = file_get_contents('http://rate-exchange.appspot.com/currency?from=BRL&to=EUR');
//        $BrlEurJson = json_decode(trim($BrlEurJson), true);
//        $BrlEur = 0;
//        if(isset($BrlEurJson['rate']) AND is_numeric($BrlEurJson['rate'])) {
//            $BrlEur = floatval($BrlEurJson['rate']);
//        }
//        else {
//            $alert .= 'the conversion of BrlEur is not float type. Check if the service is still working at: http://rate-exchange.appspot.com/currency?from=BRL&to=EUR ***';
//        }
//
//        /*BRL -> unitUsd*/
//        $UsdBrlJson = file_get_contents('http://rate-exchange.appspot.com/currency?from=USD&to=BRL');
//        $UsdBrlJson = json_decode(trim($UsdBrlJson), true);
//        $UsdBrl = 0;
//        if(isset($UsdBrlJson['rate']) AND is_numeric($UsdBrlJson['rate'])) {
//            $UsdBrl = floatval($UsdBrlJson['rate']);
//        }
//        else {
//            $alert .= 'the conversion of UsdBrl is not float type. Check if the service is still working at: http://rate-exchange.appspot.com/currency?from=USD&to=BRL ***';
//        }
//
//        /*BRL -> outUsd*/
//        $BrlUsdJson = file_get_contents('http://rate-exchange.appspot.com/currency?from=BRL&to=USD');
//        $BrlUsdJson = json_decode(trim($BrlUsdJson), true);
//        $BrlUsd = 0;
//        if(isset($BrlUsdJson['rate']) AND is_numeric($BrlUsdJson['rate'])) {
//            $BrlUsd = floatval($BrlUsdJson['rate']);
//        }
//        else {
//            $alert .= 'the conversion of BrlUsd is not float type. Check if the service is still working at: http://rate-exchange.appspot.com/currency?from=BRL&to=USD ***';
//        }
//
//        /*EUR -> unitUsd*/
//        $UsdEurJson = file_get_contents('http://rate-exchange.appspot.com/currency?from=USD&to=EUR');
//        $UsdEurJson = json_decode(trim($UsdEurJson), true);
//        $UsdEur = 0;
//        if(isset($UsdEurJson['rate']) AND is_numeric($UsdEurJson['rate'])) {
//            $UsdEur = floatval($UsdEurJson['rate']);
//        }
//        else {
//            $alert .= 'the conversion of UsdEur is not float type. Check if the service is still working at: http://rate-exchange.appspot.com/currency?from=USD&to=EUR ***';
//        }
//
//        /*EUR -> outUsd*/
//        $EurUsdJson = file_get_contents('http://rate-exchange.appspot.com/currency?from=EUR&to=USD');
//        $EurUsdJson = json_decode(trim($EurUsdJson), true);
//        $EurUsd = 0;
//        if(isset($EurUsdJson['rate']) AND is_numeric($EurUsdJson['rate'])) {
//            $EurUsd = floatval($EurUsdJson['rate']);
//        }
//        else {
//            $alert .= 'the conversion of EurUsd is not float type. Check if the service is still working at: http://rate-exchange.appspot.com/currency?from=EUR&to=USD ***';
//        }
//
//
//
//        /*MXN -> unitEur*/
//        $EurMxnJson = file_get_contents('http://rate-exchange.appspot.com/currency?from=EUR&to=MXN');
//        $EurMxnJson = json_decode(trim($EurMxnJson), true);
//        $EurMxn = 0;
//        if(isset($EurMxnJson['rate']) AND is_numeric($EurMxnJson['rate'])) {
//            $EurMxn = floatval($EurMxnJson['rate']);
//        }
//        else {
//            $alert .= 'the conversion of EurMxn is not float type. Check if the service is still working at: http://rate-exchange.appspot.com/currency?from=EUR&to=MXN ***';
//        }
//
//        /*MXN -> outEur*/
//        $MxnEurJson = file_get_contents('http://rate-exchange.appspot.com/currency?from=MXN&to=EUR');
//        $MxnEurJson = json_decode(trim($MxnEurJson), true);
//        $MxnEur = 0;
//        if(isset($MxnEurJson['rate']) AND is_numeric($MxnEurJson['rate'])) {
//            $MxnEur = floatval($MxnEurJson['rate']);
//        }
//        else {
//            $alert .= 'the conversion of MxnEur is not float type. Check if the service is still working at: http://rate-exchange.appspot.com/currency?from=MXN&to=EUR ***';
//        }
//
//        /*MXN -> unitUsd*/
//        $UsdMxnJson = file_get_contents('http://rate-exchange.appspot.com/currency?from=USD&to=MXN');
//        $UsdMxnJson = json_decode(trim($UsdMxnJson), true);
//        $UsdMxn = 0;
//        if(isset($UsdMxnJson['rate']) AND is_numeric($UsdMxnJson['rate'])) {
//            $UsdMxn = floatval($UsdMxnJson['rate']);
//        }
//        else {
//            $alert .= 'the conversion of UsdMxn is not float type. Check if the service is still working at: http://rate-exchange.appspot.com/currency?from=USD&to=MXN ***';
//        }
//
//        /*MXN -> outUsd*/
//        $MxnUsdJson = file_get_contents('http://rate-exchange.appspot.com/currency?from=MXN&to=USD');
//        $MxnUsdJson = json_decode(trim($MxnUsdJson), true);
//        $MxnUsd = 0;
//        if(isset($MxnUsdJson['rate']) AND is_numeric($MxnUsdJson['rate'])) {
//            $MxnUsd = floatval($MxnUsdJson['rate']);
//        }
//        else {
//            $alert .= 'the conversion of MxnUsd is not float type. Check if the service is still working at: http://rate-exchange.appspot.com/currency?from=MXN&to=USD ***';
//        }


//
////
////$sUrl = 'http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.xchange where pair in ("USDEUR", "USDBRL", "USDMXN", "EURBRL", "EURUSD", "EURMXN", "BRLEUR", "BRLUSD", "MXNEUR", "MXNUSD")&env=store://datatables.org/alltableswithkeys&format=json';
//
//        $sYqlUrlBase = 'http://query.yahooapis.com/v1/public/yql';
//        $stYqlParams = array
//        (
//            'q' =>      'select * from yahoo.finance.xchange where pair in ("'
//                . self::Y_USD_TO_EUR . '", "'
//                . self::Y_USD_TO_BRL . '", "'
//                . self::Y_USD_TO_MXN . '", "'
//                . self::Y_EUR_TO_BRL . '", "'
//                . self::Y_EUR_TO_USD . '", "'
//                . self::Y_EUR_TO_MXN . '", "'
//                . self::Y_BRL_TO_EUR . '", "'
//                . self::Y_BRL_TO_USD . '", "'
//                . self::Y_MXN_TO_EUR . '", "'
//                . self::Y_MXN_TO_USD . '")',
//            'env' =>    'store://datatables.org/alltableswithkeys',
//            'format' => 'json',
//        );
//        $stFeedData = file_get_contents($sYqlUrlBase . '?' . http_build_query($stYqlParams));
//
//echo "\n---->";
//$responseJson = json_decode(trim($stFeedData), true);
//print_r($responseJson);
//echo "<----\n";
//
//
//        if(isset($responseJson["query"]["results"]["rate"]) AND is_array($responseJson["query"]["results"]["rate"])) {
//            foreach($responseJson["query"]["results"]["rate"] as $iRateKey => $stRate) {
//
//                if(isset($stRate["id"])) {
//                    switch($stRate["id"]) {
//                        case "USDEUR":
//                            $iRate = $stRate["Rate"];
//                            echo "\n" . $stRate["id"] . " = " . $stRate["Rate"] . "\n";
//                            break;
//                    }
//                }
//            }
//        }
//
//
////        Array
////        (
////            [query] => Array
////                (
////                    [count] => 10
////            [created] => 2015-06-08T14:30:56Z
////            [lang] => en-US
////            [results] => Array
////    (
////        [rate] => Array
////        (
////            [0] => Array
////                (
////                    [id] => USDEUR
////                                    [Name] => USD/EUR
////                                    [Rate] => 0.8921
////                                    [Date] => 6/8/2015
////                                    [Time] => 3:30pm
////                                    [Ask] => 0.8921
////                                    [Bid] => 0.8921
////                                )
////
////                            [1] => Array
////    (
////        [id] => USDBRL
////                                    [Name] => USD/BRL
////                                    [Rate] => 3.1200
////                                    [Date] => 6/8/2015
////                                    [Time] => 3:30pm
////                                    [Ask] => 3.1208
////                                    [Bid] => 3.1200
////                                )
//
//
////$yql_base_url = 'http://query.yahooapis.com/v1/public/yql';
////$yql_query =    "select * from yahoo.finance.xchange where pair in ('USDEUR', 'USDBRL', 'USDMXN', 'EURBRL', 'EURUSD', 'EURMXN', 'BRLEUR', 'BRLUSD', 'MXNEUR', 'MXNUSD')";
////
////$sUrl =     $yql_base_url . '?q=' . urlencode($yql_query);
////$sUrl .=    $yql_base_url . '&env=store://datatables.org/alltableswithkeys&format=json';
////
////
////$session = curl_init($sUrl);
////curl_setopt($session, CURLOPT_RETURNTRANSFER,true);
////$json = curl_exec($session);
////$phpObj =  json_decode($json, true);
////echo "\n---->";
////print_r($phpObj);
////echo "<----\n";
//
//exit();
//
////$responseJson = file_get_contents($sUrl);
////$responseJson = json_decode(trim($responseJson), true);
////print_r($responseJson);
//
//// create curl resource
//$ch = curl_init();
//
//curl_setopt($ch, CURLOPT_URL, $sUrl);
//curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
////curl_setopt($ch, CURLOPT_POST, 0);
////curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
////        curl_setopt($cpt, CURLOPT_USERAGENT, $userAgent);
////curl_setopt ($request, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
////curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//curl_setopt($ch, CURLOPT_TIMEOUT, 40);
//// //curl_setopt( $ch, CURLOPT_BINARYTRANSFER,   false );
//
//// ?!?!?!?!?
//// ?!?!?!?!?
//// ?!?!?!?!?
//curl_setopt($ch, CURLOPT_HEADER, 0);
//// ?!?!?!?!?
//// ?!?!?!?!?
//// ?!?!?!?!?
//
//$output = curl_exec($ch);
//
//curl_close($ch);
//
//
////$url =      'http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.xchange where pair in ("USDEUR", "USDBRL", "USDMXN", "EURBRL", "EURUSD", "EURMXN", "BRLEUR", "BRLUSD", "MXNEUR", "MXNUSD")&env=store://datatables.org/alltableswithkeys&format=json';
////$request =  curl_init();
////$timeOut =  0;
////curl_setopt ($request, CURLOPT_URL, $url);
////curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1);
////curl_setopt ($request, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
////curl_setopt ($request, CURLOPT_CONNECTTIMEOUT, $timeOut);
////
////$results = curl_exec($request);
////
////curl_close($request);
////
//echo "\n--->" . $output . "<---\n\n";
//
//exit();
//
//
//        $from_currency    = 'USD';
//        $to_currency    = 'EUR';
//        $amount            = 1;
////        $results = converCurrency($from_currency,$to_currency,$amount);
//
//        $url = "http://www.google.com/finance/converter?a=$amount&from=$from_currency&to=$to_currency";
//        $request = curl_init();
//        $timeOut = 0;
//        curl_setopt ($request, CURLOPT_URL, $url);
//        curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt ($request, CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
//        curl_setopt ($request, CURLOPT_CONNECTTIMEOUT, $timeOut);
//        $results = curl_exec($request);
//        curl_close($request);
//
//        echo "\n" . $results . "\n\n";
//
////        $isUrl = preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $aResponse['redirectUrl']);
////        $this->assertGreaterThan(0, preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\z/', $lastIp));
////        return preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^", $email);
//
////        <span class=bld>0.8923 EUR</span>
//
//
////        $regularExpression     = "/^(.+?)<span class=bld>(.+?)<\/span>(.+?)$/";
//        $regularExpression     = "#\<span class=bld\>(.+?)\<\/span\>#s";
//
////        [0-9]{14}
//
//        $regularExpression     = "#\<span class=bld\>(.+?)\<\/span\>#s";
//
////        ^(Credit Card type not found for order )(.*)( \(Type: "::"\))$
//
/////\<br(\s*)?\/?\>/i
//
////^(.*)(;)(.*)(;)(.*)$
////\1;\3
////        $pattern_short = '{<div\s+id="pepBandEvents"\s*>((?:(?:(?!<div[^>]*>|</div>).)++|<div[^>]*>(?1)</div>)*)</div>}si';
////        preg_match_all("'<span class=bld>(.*?)</span>'si", $results, $finalData);
//        preg_match("'<span class=bld>([^<]*)</span>'si", $results, $finalData);
//
////echo "\n--->";
////print_r($finalData);
////echo "<---\n";
////
////        foreach($finalData[1] as $val) {
////            echo "\n--->";
////            $curVal = trim(mb_substr($val, 0, mb_strlen($val) - 3));
////            echo $curVal;
////            echo "<---\n";
////        }
//
//        echo "\n--->";
//        $curVal = trim(mb_substr($finalData[1], 0, mb_strlen($finalData[1]) - 3));
//        echo $curVal;
//        echo "<---\n";
//
////        preg_match($regularExpression, $results, $finalData);
//////        echo $finalData[0];
////
////        echo "\n--->";
////        print_r($finalData);
////        echo "<---\n";
//
//        echo "\n FROM " . $from_currency . " TO " . $to_currency . " = " . $finalData[0] . "\n\n";
//
////        function converCurrency($from,$to,$amount){
////            $url = "http://www.google.com/finance/converter?a=$amount&from=$from&to=$to";
////            $request = curl_init();
////            $timeOut = 0;
////            curl_setopt ($request, CURLOPT_URL, $url);
////            curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1);
////            curl_setopt ($request, CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
////            curl_setopt ($request, CURLOPT_CONNECTTIMEOUT, $timeOut);
////            $response = curl_exec($request);
////            curl_close($request);
////            return $response;
////        }
//
//
//
//        exit();
//
//
//
//        // + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +
//        // + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +
//        // + + + + + 2015-04-22 v3.0
//        // + + + + +
//        // + + + + + RESPONSE:
//        // + + + + + {"list":{"meta":{"type":"resource-list","start":0,"count":173},"resources":[{"resource":{"classname":"Quote","fields":{"name":"USD/KRW","price":"1122.505005","symbol":"KRW=X","ts":"1433764020","type":"currency","utctime":"2015-06-08T11:47:00+0000","volume":"0"}}},{"resource":{"classname":"Quote","fields":{"name":"SILVER 1 OZ 999 NY","price":"0.062613","symbol":"XAG=X","ts":"1433541413","type":"currency","utctime":"2015-06-05T21:56:53+0000","volume":"4"}}},{"resource":{"classname":"Quote","fields":{"name":"USD/VND","price":"21815.000000","symbol":"VND=X","ts":"1433764020","type":"currency","utctime":"2015-06-08T11:47:00+0000","volume":"0"}}},{"resource":{"classname":"Quote","fields":{"name":"USD/BOB","price":"6.910000","symbol":"BOB=X","ts":"1433764020","type":"currency","utctime":"2015-06-08T11:47:00+0000","volume":"0"}}},{"resource":{"classname":"Quote","fields":{"name":"USD/MOP","price":"7.984900","symbol":"MOP=X","ts":"1433764020","type":"currency","utctime":"2015-06-08T11:47:00+0000","volume":"0"}}},{"resource":{"classname":"Quote","fields":{"name":"USD/BDT","price":"77.843452","symbol":"BDT=X","ts":"1433764020","type":"currency","utctime":"2015-06-08T11:47:00+0000","volume":"0"}}},{"resource":{"classname":"Quote","fields":{"name":"USD/MDL","price":"18.330000","symbol":"MDL=X","ts":"1433764020","type":"currency","utctime":"2015-06-08T11:47:00+0000","volume":"0"}}},{"resource":{"classname":"Quote","fields":{"name":"USD/VEF","price":"6.350000","symbol":"VEF=X","ts":"1433764020","type":"currency","utctime":"2015-06-08T11:47:00+0000","volume":"0"}}},{"resource":{"classname":"Quote","fields":{"name":"USD/GEL","price":"2.276900","symbol":"GEL=X","ts":"1433764020","type":"currency","utctime":"2015-06-08T11:47:00+0000","volume":"0"}}}]}}
//        // + + + + +
//        // + + + + + http://finance.yahoo.com/webservice/v1/symbols/allcurrencies/quote?format=json
//        // + + + + +
//
//        $sUrl = "http://finance.yahoo.com/webservice/v1/symbols/allcurrencies/quote?format=json";
//
//        $responseJson = file_get_contents($sUrl);
//
////print_r($responseJson);
//
//        $responseJson = json_decode(trim($responseJson), true);
//
//
////print_r($responseJson);
////        {"meta":{"type":"resource-list","start":0,"count":173},"resources":[{"resource":{"classname":"Quote","fields":{"name":"USD\/KRW","price":"1122.079956","symbol":"KRW=X","ts":"1433764260","type":"currency","utctime":"2015-06-08T11:51:00+0000","volume":"0"}}},{"resource":{"classname":"Quote","fields":{"name":"SILVER 1 OZ 999 NY","price":"0.062613","symbol":"XAG=X","ts":"1433541413","type":"currency","utctime":"2015-06-05T21:56:53+0000","volume":"4"}}},{"resource":{"classname":"Quote","fields":{"name":"USD\/VND","price":"21815.000000","symbol":"VND=X","ts":"1433764260","type":"currency","utctime":"2015-06-08T11:51:00+0000","volume":"0"}}},{"resource":{"classname":"Quote","fields":{"name":"USD\/BOB","price":"6.910000","symbol":"BOB=X","ts":"1433764260","type":"currency","utctime":"2015-06-08T11:51:00+0000","volume":"0"}}},{"resource":{"classname":"Quote","fields":{"name":"USD\/MOP","price":"7.984900","symbol":"MOP=X","ts":"1433764260","type":"currency","utctime":"2015-06-08T11:51:00+0000","volume":"0"}}},
//
//        if(isset($responseJson["list"]["resources"])) {
//            foreach($responseJson["list"]["resources"] as $iResource => $stResource) {
//                if(isset($stResource["resource"]["fields"])) {
//
//                    switch($stResource["resource"]["fields"]["name"]) {
//                        case "EUR/BRL":
//                        case "EUR/USD":
//                        case "EUR/MXN":
//                            echo "\n" . $stResource["resource"]["fields"]["name"] . " = " . $stResource["resource"]["fields"]["price"] . "\n\n";
//                            break;
//                        case "USD/BRL":
//                        case "USD/EUR":
//                        case "USD/MXN":
//                            echo "\n" . $stResource["resource"]["fields"]["name"] . " = " . $stResource["resource"]["fields"]["price"] . "\n\n";
//                            break;
//                        case "BRL/EUR":
//                        case "BRL/USD":
//                            echo "\n" . $stResource["resource"]["fields"]["name"] . " = " . $stResource["resource"]["fields"]["price"] . "\n\n";
//                            break;
//                        case "MXN/EUR":
//                        case "MXN/USD":
//                            echo "\n" . $stResource["resource"]["fields"]["name"] . " = " . $stResource["resource"]["fields"]["price"] . "\n\n";
//                            break;
//                    }
//
//                }
//            }
//        }
////
////'{
////"list" : {
////    "meta" : {
////    "type" : "resource-list",
////    "start" : 0,
////    "count" : 173
////    },
////    "resources" : [
////    {
////    "resource" :
////        {
////        "classname" : "Quote",
////        "fields" : {
////            "name" : "USD/KRW",
////            "price" : "1122.505005",
////            "symbol" : "KRW=X",
////            "ts" : "1433764020",
////            "type" : "currency",
////            "utctime" : "2015-06-08T11:47:00+0000",
////            "volume" : "0"
////            }
////        }
////    }
////    ,
////    {
////    "resource" : {
////        "classname" : "Quote",
////        "fields" : {
////            "name" : "SILVER 1 OZ 999 NY",
////            "price" : "0.062613",
////            "symbol" : "XAG=X",
////            "ts" : "1433541413",
////            "type" : "currency",
////            "utctime" : "2015-06-05T21:56:53+0000",
////            "volume" : "4"
////            }
////        }
////    }
////,';
//
//
//exit();
//

        // + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +
        // + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +
        // + + + + + 2015-04-22 v3.0
        // + + + + +
        // + + + + + RESPONSE:
        // + + + + + {"query":{"count":10},"results":{"EUR_BRL":{"fr":"EUR","id":"EUR_BRL","to":"BRL","val":3.2738},"BRL_EUR":{"val":0.3053,"id":"BRL_EUR","to":"EUR","fr":"BRL"},"USD_BRL":{"val":3.034,"id":"USD_BRL","to":"BRL","fr":"USD"},"BRL_USD":{"val":0.3296,"id":"BRL_USD","to":"USD","fr":"BRL"},"USD_EUR":{"fr":"USD","id":"USD_EUR","to":"EUR","val":0.9264},"EUR_USD":{"fr":"EUR","id":"EUR_USD","to":"USD","val":1.0791},"EUR_MXN":{"fr":"EUR","id":"EUR_MXN","to":"MXN","val":16.5705},"MXN_EUR":{"val":0.0604,"id":"MXN_EUR","to":"EUR","fr":"MXN"},"USD_MXN":{"fr":"USD","id":"USD_MXN","to":"MXN","val":15.3564},"MXN_USD":{"val":0.0651,"id":"MXN_USD","to":"USD","fr":"MXN"}}}
        // + + + + +

//        $sUrl =
//            "http://www.freecurrencyconverterapi.com/api/v3/convert?q=" .
//            self::EUR_TO_BRL . "," . self::BRL_TO_EUR . "," . self::USD_TO_BRL . "," . self::BRL_TO_USD . "," . self::USD_TO_EUR . "," .
//            self::EUR_TO_USD . "," . self::EUR_TO_MXN . "," . self::MXN_TO_EUR . "," . self::USD_TO_MXN . "," . self::MXN_TO_USD;
//
//        $responseJson = file_get_contents($sUrl);
//        $responseJson = json_decode(trim($responseJson), true);
//
//        $allTaxRates = array();
//
//        if(isset($responseJson['results']) AND is_array($responseJson['results'])) {
//            $allTaxRates = $responseJson['results'];
//        }
//        else {
//            $alert .= '. Check if the service is still working at: ' . $sUrl . ' ***' . "\n";
//        }
//
//        //
//        $allCurrencies =        $this->getAllCurrencies(1);
//        $allTaxRatesResult =    array();
//
//        foreach($allTaxRates as $sCurrency => $stTaxRate) {
//            if(in_array($sCurrency, $allCurrencies)){
//                if(isset($stTaxRate['val']) AND is_numeric($stTaxRate['val'])) {
//                    $allTaxRatesResult[$sCurrency] = floatval($stTaxRate['val']);
//                }
//                else {
//                    $alert .= 'the conversion of ' . $sCurrency . ' is not float type. Check if the service is still working at: ' . $sUrl . ' ***' . "\n";
//                }
//            }
//            else {
//                $alert .= 'Check if the service is still working at: ' . $sUrl . ' ***' . "\n";
//            }
//        }


        // + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +
        // + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +
        // + + + + + 2015-06-08 v4.0
        // + + + + +
        // + + + + + RESPONSE:
        // + + + + + {"query":{"count":10,"created":"2015-06-08T15:00:00Z","lang":"en-US","results":{"rate":[{"id":"USDEUR","Name":"USD/EUR","Rate":"0.8924","Date":"6/8/2015","Time":"4:00pm","Ask":"0.8924","Bid":"0.8924"},{"id":"USDBRL","Name":"USD/BRL","Rate":"3.1265","Date":"6/8/2015","Time":"4:00pm","Ask":"3.1272","Bid":"3.1265"},{"id":"USDMXN","Name":"USD/MXN","Rate":"15.6863","Date":"6/8/2015","Time":"4:00pm","Ask":"15.6874","Bid":"15.6863"},{"id":"EURBRL","Name":"EUR/BRL","Rate":"3.5036","Date":"6/8/2015","Time":"4:00pm","Ask":"3.5045","Bid":"3.5026"},{"id":"EURUSD","Name":"EUR/USD","Rate":"1.1206","Date":"6/8/2015","Time":"4:00pm","Ask":"1.1207","Bid":"1.1206"},{"id":"EURMXN","Name":"EUR/MXN","Rate":"17.5784","Date":"6/8/2015","Time":"4:00pm","Ask":"17.5801","Bid":"17.5768"},{"id":"BRLEUR","Name":"BRL/EUR","Rate":"0.2854","Date":"6/8/2015","Time":"3:59pm","Ask":"0.2855","Bid":"0.2853"},{"id":"BRLUSD","Name":"BRL/USD","Rate":"0.3199","Date":"6/8/2015","Time":"4:00pm","Ask":"0.3199","Bid":"0.3198"},{"id":"MXNEUR","Name":"MXN/EUR","Rate":"0.0569","Date":"6/8/2015","Time":"4:00pm","Ask":"0.0569","Bid":"0.0569"},{"id":"MXNUSD","Name":"MXN/USD","Rate":"0.0637","Date":"6/8/2015","Time":"4:00pm","Ask":"0.0638","Bid":"0.0637"}]}}}
        // + + + + +


        $sYqlUrlBase = 'http://query.yahooapis.com/v1/public/yql';
        $stYqlParams = array
        (
            'q' =>      'select * from yahoo.finance.xchange where pair in ("'
                . self::Y_USD_TO_EUR . '", "'
                . self::Y_USD_TO_BRL . '", "'
                . self::Y_USD_TO_MXN . '", "'
                . self::Y_EUR_TO_BRL . '", "'
                . self::Y_EUR_TO_USD . '", "'
                . self::Y_EUR_TO_MXN . '", "'
                . self::Y_BRL_TO_EUR . '", "'
                . self::Y_BRL_TO_USD . '", "'
                . self::Y_MXN_TO_EUR . '", "'
                . self::Y_MXN_TO_USD . '")',
            'env' =>    'store://datatables.org/alltableswithkeys',
            'format' => 'json',
        );

        $sUrl =         $sYqlUrlBase . '?' . http_build_query($stYqlParams);
        $responseJson = file_get_contents($sUrl);
        $responseJson = json_decode(trim($responseJson), true);

        $allTaxRates = array();

        if(isset($responseJson["query"]["results"]["rate"]) AND is_array($responseJson["query"]["results"]["rate"])) {
            $allTaxRates = $responseJson["query"]["results"]["rate"];
        }
        else {
            $alert .= '. Check if the service is still working at: ' . $sUrl . ' ***' . "\n";
        }

        //
        $allCurrencies =        $this->getAllCurrencies(2);
        $allTaxRatesResult =    array();

        foreach($allTaxRates as $iRateKey => $stTaxRate) {
            if(isset($stTaxRate["id"]) AND in_array($stTaxRate["id"], $allCurrencies)){
                if(isset($stTaxRate['Rate']) AND is_numeric($stTaxRate['Rate'])) {
                    $allTaxRatesResult[$stTaxRate["id"]] = floatval($stTaxRate['Rate']);
                }
                else {
                    $alert .= 'the conversion of ' . $stTaxRate["id"] . ' is not float type. Check if the service is still working at: ' . $sUrl . ' ***' . "\n";
                }
            }
            else {
                $alert .= 'Check if the service is still working at: ' . $sUrl . ' ***' . "\n";
            }
        }

        $EurBrl = 0;
        $BrlEur = 0;
        $UsdBrl = 0;
        $BrlUsd = 0;
        $UsdEur = 0;
        $EurUsd = 0;
        $EurMxn = 0;
        $MxnEur = 0;
        $UsdMxn = 0;
        $MxnUsd = 0;

        foreach($allTaxRatesResult as $sCurrency => $iTaxRate) {
            switch($sCurrency) {
                case self::Y_EUR_TO_BRL: $EurBrl = $iTaxRate; break;
                case self::Y_BRL_TO_EUR: $BrlEur = $iTaxRate; break;
                case self::Y_USD_TO_BRL: $UsdBrl = $iTaxRate; break;
                case self::Y_BRL_TO_USD: $BrlUsd = $iTaxRate; break;
                case self::Y_USD_TO_EUR: $UsdEur = $iTaxRate; break;
                case self::Y_EUR_TO_USD: $EurUsd = $iTaxRate; break;
                case self::Y_EUR_TO_MXN: $EurMxn = $iTaxRate; break;
                case self::Y_MXN_TO_EUR: $MxnEur = $iTaxRate; break;
                case self::Y_USD_TO_MXN: $UsdMxn = $iTaxRate; break;
                case self::Y_MXN_TO_USD: $MxnUsd = $iTaxRate; break;
            }
        }

//
echo "\n+ + + query.yahooapis.com + + +";
echo "\n+ + + + + + + + + + + + + + + + + +";
echo "\n+ + + EurBrl: " . $EurBrl . " + + +";
echo "\n+ + + BrlEur: " . $BrlEur . " + + +";
echo "\n+ + + UsdBrl: " . $UsdBrl . " + + +";
echo "\n+ + + BrlUsd: " . $BrlUsd . " + + +";
echo "\n+ + + UsdEur: " . $UsdEur . " + + +";
echo "\n+ + + EurUsd: " . $EurUsd . " + + +";
echo "\n+ + + EurMxn: " . $EurMxn . " + + +";
echo "\n+ + + MxnEur: " . $MxnEur . " + + +";
echo "\n+ + + UsdMxn: " . $UsdMxn . " + + +";
echo "\n+ + + MxnUsd: " . $MxnUsd . " + + +\n";

        //
        //
        if($alert) {
            $this->processError($alert);
            return false;
        }



        // + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +
        // + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +
        // + + + + + 2015-06-08 v5.0  -  Plan B
        // + + + + +
        // + + + + + RESPONSE:
        // + + + + + HTML
        // + + + + +
//
//        $get =      file_get_contents("https://www.google.com/finance/converter?a=1&from=EUR&to=BRL");
//        $EurBrl =   $this->getRateValue($get, "EUR to BRL");
//
//        $get =      file_get_contents("https://www.google.com/finance/converter?a=1&from=BRL&to=EUR");
//        $BrlEur =   $this->getRateValue($get, "BRL to EUR");
//
//        $get =      file_get_contents("https://www.google.com/finance/converter?a=1&from=USD&to=BRL");
//        $UsdBrl =   $this->getRateValue($get, "USD to BRL");
//
//        $get =      file_get_contents("https://www.google.com/finance/converter?a=1&from=BRL&to=USD");
//        $BrlUsd =   $this->getRateValue($get, "BRL to USD");
//
//        $get =      file_get_contents("https://www.google.com/finance/converter?a=1&from=USD&to=EUR");
//        $UsdEur =   $this->getRateValue($get, "USD to EUR");
//
//        $get =      file_get_contents("https://www.google.com/finance/converter?a=1&from=EUR&to=USD");
//        $EurUsd =   $this->getRateValue($get, "EUR to USD");
//
//        $get =      file_get_contents("https://www.google.com/finance/converter?a=1&from=EUR&to=MXN");
//        $EurMxn =   $this->getRateValue($get, "EUR to MXN");
//
//        $get =      file_get_contents("https://www.google.com/finance/converter?a=1&from=MXN&to=EUR");
//        $MxnEur =   $this->getRateValue($get, "MXN to EUR");
//
//        $get =      file_get_contents("https://www.google.com/finance/converter?a=1&from=USD&to=MXN");
//        $UsdMxn =   $this->getRateValue($get, "USD to MXN");
//
//        $get =      file_get_contents("https://www.google.com/finance/converter?a=1&from=MXN&to=USD");
//        $MxnUsd =   $this->getRateValue($get, "MXN to USD");
//
//echo "\n+ + + www.google.com/finance/converter + + +";
//echo "\n+ + + + + + + + + + + + + + + + + + + + + + + +";
//echo "\n+ + + EurBrl: " . $EurBrl . " + + +";
//echo "\n+ + + BrlEur: " . $BrlEur . " + + +";
//echo "\n+ + + UsdBrl: " . $UsdBrl . " + + +";
//echo "\n+ + + BrlUsd: " . $BrlUsd . " + + +";
//echo "\n+ + + UsdEur: " . $UsdEur . " + + +";
//echo "\n+ + + EurUsd: " . $EurUsd . " + + +";
//echo "\n+ + + EurMxn: " . $EurMxn . " + + +";
//echo "\n+ + + MxnEur: " . $MxnEur . " + + +";
//echo "\n+ + + UsdMxn: " . $UsdMxn . " + + +";
//echo "\n+ + + MxnUsd: " . $MxnUsd . " + + +\n";
//
//        //
//        //
//        if($this->alerts) {
//            $this->processError($this->alerts);
//            return false;
//        }



        //
        //
        //
        $moCcyBRL = new AbaCurrency();
        $moCcyEUR = new AbaCurrency();
        $moCcyMXN = new AbaCurrency();
        $moCcyUSD = new AbaCurrency();

        $iUno = floatval(1.0000000000);

        //Updating EUR currency
        $moCcyEUR = $moCcyEUR->getCurrencyById('EUR');
        $moCcyEUR->unitEur =    $iUno;
        $moCcyEUR->outEur =     $iUno;
        $moCcyEUR->unitUsd =    floatval($UsdEur);
        $moCcyEUR->outUsd =     floatval($EurUsd);
        if(!$moCcyEUR->update(array('unitEur','outEur','unitUsd', 'outUsd'))) {
            $alert .= 'ERROR trying to save EUR currency. ***';
        }

        //Updating USD currency
        $moCcyUSD = $moCcyUSD->getCurrencyById('USD');
        $moCcyUSD->unitEur =    floatval($EurUsd);
        $moCcyUSD->outEur =     floatval($UsdEur);
        $moCcyUSD->unitUsd =    $iUno;
        $moCcyUSD->outUsd =     $iUno;
        if(!$moCcyUSD->update(array('unitEur', 'outEur', 'unitUsd', 'outUsd'))) {
            $alert .= 'ERROR trying to save USD currency. ***';
        }

        //Updating BRL currency
        $moCcyBRL = $moCcyBRL->getCurrencyById('BRL');
        $moCcyBRL->unitEur =    floatval($EurBrl);
        $moCcyBRL->outEur =     floatval($BrlEur);
        $moCcyBRL->unitUsd =    floatval($UsdBrl);
        $moCcyBRL->outUsd =     floatval($BrlUsd);
        if(!$moCcyBRL->update(array('unitEur', 'outEur', 'unitUsd', 'outUsd'))) {
            $alert .= 'ERROR trying to save BRL currency. ***';
        }

        //Updating MXN currency
        $moCcyMXN = $moCcyMXN->getCurrencyById('MXN');
        $moCcyMXN->unitEur =    floatval($EurMxn);
        $moCcyMXN->outEur =     floatval($MxnEur);
        $moCcyMXN->unitUsd =    floatval($UsdMxn);
        $moCcyMXN->outUsd =     floatval($MxnUsd);
        if(!$moCcyMXN->update(array('unitEur', 'outEur', 'unitUsd', 'outUsd'))) {
            $alert .= 'ERROR trying to save MXN currency. ***';
        }

        if($alert) {
            $this->processError($alert);
        }

        echo " \n ".date("Y-m-d h:i:s", time() )." Success in process CurrencyExchangeUpdateCommand. END";
        echo " \n -----------------------------------------------------------------------------------------------";

        return true;
    }

    /**
     * @param string $actionDetails
     *
     * @return bool
     */
    private function processError($actionDetails)
    {
        HeLogger::sendLog("Check de error listed below.", HeLogger::PAYMENTS, HeLogger::CRITICAL,
          "CRON  finished with errors : ".$actionDetails);

        return true;
    }

    /**
     * @return array
     */
    protected function getAllCurrencies($iType=0) {

        switch($iType) {
            case 1:
                return array(
                    self::EUR_TO_BRL => self::EUR_TO_BRL,
                    self::BRL_TO_EUR => self::BRL_TO_EUR,
                    self::USD_TO_BRL => self::USD_TO_BRL,
                    self::BRL_TO_USD => self::BRL_TO_USD,
                    self::USD_TO_EUR => self::USD_TO_EUR,
                    self::EUR_TO_USD => self::EUR_TO_USD,
                    self::EUR_TO_MXN => self::EUR_TO_MXN,
                    self::MXN_TO_EUR => self::MXN_TO_EUR,
                    self::USD_TO_MXN => self::USD_TO_MXN,
                    self::MXN_TO_USD => self::MXN_TO_USD,
                );
                break;
            case 2:
                return array(
                    self::Y_USD_TO_EUR => self::Y_USD_TO_EUR,
                    self::Y_USD_TO_BRL => self::Y_USD_TO_BRL,
                    self::Y_USD_TO_MXN => self::Y_USD_TO_MXN,
                    self::Y_EUR_TO_BRL => self::Y_EUR_TO_BRL,
                    self::Y_EUR_TO_USD => self::Y_EUR_TO_USD,
                    self::Y_EUR_TO_MXN => self::Y_EUR_TO_MXN,
                    self::Y_BRL_TO_EUR => self::Y_BRL_TO_EUR,
                    self::Y_BRL_TO_USD => self::Y_BRL_TO_USD,
                    self::Y_MXN_TO_EUR => self::Y_MXN_TO_EUR,
                    self::Y_MXN_TO_USD => self::Y_MXN_TO_USD,
                );
                break;
        }

        return array();

    }

    /**
     * @param $sHtml
     */
    protected function getRateValue($sHtml, $sType) {
        $stHtml =   explode("<span class=bld>", $sHtml);
        $stHtml =   explode("</span>", $stHtml[1]);
        $iRate =    trim(preg_replace("/[^0-9\.]/", null, $stHtml[0]));

        if(!is_numeric($iRate)) {

            $this->alerts .= 'The conversion of ' . $sType . ' is not float type. ***' . "\n";

            return 0;
        }

        return floatval($iRate);
    }

}
