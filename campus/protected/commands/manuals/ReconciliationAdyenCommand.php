<?php

class ReconciliationAdyenCommand extends AbaConsoleCommand
{
    protected $idPaySupplier = PAY_SUPPLIER_ADYEN;

    public function run($args)
    {
        $allSuccess = true;
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n ".$this->getName().": executing auto process at ".date("Y-m-d H:i:s", time());
        $today =                HeDate::todaySQL(false);
        $dateRequest =          HeDate::getDateAdd($today, -1/*day*/);
        if (count($args) > 0) {
            if (!HeDate::validDateSQL($args[0])) {
                echo 'Date is not valid. Please respect format YYYY-MM-DD';
                return false;
            }
            $dateRequest = $args[0];
        }
        /* @var $paySupplier PaySupplierAdyen */
        $paySupplier =  PaymentFactory::createPayment(null, $this->idPaySupplier);
        // guardar fichero temporal
        $moLogAdyen = $paySupplier->runRequestReconciliation($dateRequest);
        //// PayGatewayLogAdyenResRec [PayGatewayLogAdyen]
        /////* ___@___var $fileCsv SimpleXMLElement */
        $fileCsv = $paySupplier->getCsvResponse();
        $fileParserCSV = new FileParserAdyen($this->idPaySupplier);
        $fileParserCSV->setFileFormat($paySupplier->getFileReport());
        if (!$fileParserCSV->saveFile($fileCsv)) {
            $this->processEndsWithError("File CSV was requested and obtained, but could not be saved to disk.");
            return false;
        }
        if (!$fileParserCSV->validateFile()) {
            $this->processEndsWithError(
                "File CSV was requested SAVED to disk, but apparently it's not a valid CSV file."
            );
            return false;
        }
        //
        $aMoLogsReconc = $fileParserCSV->parseToModels($fileCsv);
        if (count($aMoLogsReconc) == 0) {
            HeLogger::sendLog(HeLogger::PREFIX_NOTIF_H."CRON Reconciliation ADYEN executed and finished on day ".
              date("Y-m-d H:i:s", time())."", HeLogger::IT_BUGS, HeLogger::INFO,
              "CRON Reconciliation ADYEN executed and finished on day ".date("Y-m-d H:i:s", time()).
              " There was no transactions executed on BIP platform during day ".$dateRequest);
            echo " \n Success and end of process execution -------------------";
            return true;
        }
        /* -------------If there are any transactions available in XML then we check them all------------------- */
        $commMatching = new ReconcMatchingAdyen($this->idPaySupplier);
        $allSuccess = $commMatching->processPackTransactions($aMoLogsReconc, $fileParserCSV->getFilePath());
        if (!$allSuccess) {
            echo " \n \n   executed at ".date('Y-M-D H:m:s')." with ERRORS for some transactions. ";
        }
        echo " \n  Finished process at " . date('Y-M-D H:m:s');
        echo " \n -----------------------------------------------------------------------------------------------";
        return $allSuccess;
    }

    /**
     * @param $actionDetails
     * @return bool
     */
    protected function processEndsWithError($actionDetails)
    {
        HeLogger::sendLog(
          HeLogger::PREFIX_ERR_UNKNOWN_H .
            " Cron ".$this->getName()." was executed and finished but processed some errors.",
            HeLogger::IT_BUGS,
            HeLogger::CRITICAL,
            $actionDetails
        );
        echo " \n " . date("Y-m-d H:i:s", time()) . " ".$this->getName()." finished with ERRORS : " .
            $actionDetails;
        echo " \n -----------------------------------------------------------------------------------------------";
        return true;
    }

}
