<?php

class ForceUnsubscribeUsers3841FromCsvCommand extends AbaConsoleCommand
{
    public $_pathFileIn =  '/var/www/temp/ForceUnsubscribeUsers3841_20150116.csv';
    private $_limit =       1000;
//
//  php -f  /var/www/abawebapps/campus/public/script.php  ForceUnsubscribeUsers3841FromCsv  "/var/www/temp/ForceUnsubscribeUsers3841_20150116.csv"
//
    public function getHelp()
    {
        return parent::getHelp()." You can set a number to set how many records you want to send to SELLIGENT. \n " ;
    }

    public function run($args)
    {
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n START. SynchronizeSelligentUsersCommand: executing auto process at ".date("Y-m-d h:i:s", time() );
        $allSuccess = true;

        if(count($args) > 0) {
            $this->_pathFileIn = trim($args[0]) ;
        }

        $handle = fopen($this->_pathFileIn, "r");

        if ($handle) {

            $iLine = 0;
            while (($newLine = fgets($handle, 4096)) !== false && $this->_limit > $iLine) {

                $iLine++;

                $aPartsLine =       explode(';', trim($newLine, "\r\n"));
                list($email) =      $aPartsLine;
                $moUserCurrent =    new AbaUser();

                if($moUserCurrent->getUserByEmail($email))
                {
                    echo "\n " . $iLine . ". ; sending to " . $moUserCurrent->email . " (" . $moUserCurrent->id . ")";

                    $commSelligent = new SelligentConnectorCommon();

                    if( $commSelligent->sendOperationToSelligent(SelligentConnectorCommon::unsubscribe, $moUserCurrent, array(), "ForceUnsubscribeUsers3841FromCsvCommand")) {
                        echo " =OK ";
                    }
                    else{
                        echo " =".$commSelligent->getResponse();
                        $this->processError(" User with email ".$moUserCurrent->email ." not correctly received by Selligent.");
                    }
                }
            }
        }

        echo " \n ".date("Y-m-d h:i:s", time() )." Success in process SynchronizeSelligentUsersCommand. END";
        echo " \n -----------------------------------------------------------------------------------------------";
        return $allSuccess;
    }

    /**
     * @param string $actionDetails
     *
     * @return bool
     */
    private function processError($actionDetails)
    {
        HeLogger::sendLog("CRON SynchronizeSelligentUsersCommand finished with errors ",
          HeLogger::PAYMENTS, HeLogger::CRITICAL,
          " Error processing SynchronizeSelligentUsersCommand: ".$actionDetails);
        return true;
    }




}
