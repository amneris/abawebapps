<?php

/**
 * Class ParseCaixaMerchantReference4681FromCsvCommand
 *
 * Import Ds_Merchant_Identifier from laCaixa CSV file. Ds_Merchant_Identifier = paySuppExtProfId fiels in payments table
 *
 */
class ParseCaixaMerchantReference4681FromCsvCommand extends AbaConsoleCommand
{
    // PROD
//    private $_pathFileIn =  '/var/log/log_file_reconciliation/lacaixaMigrationFileIn.csv';
//    private $_pathFileOut = '/var/log/log_file_reconciliation/';
    // DEV
//    private $_pathFileIn =  'C:\var\www\log_file_reconciliation\lacaixaMigrationFileIn.csv';
//    private $_pathFileOut = 'C:\var\www\log_file_reconciliation\\';
    private $_pathFileIn =  '/var/www/log_file_reconciliation/lacaixaMigrationFileIn.csv';
    private $_pathFileOut = '/var/www/log_file_reconciliation/';

    private $_colsPositions = 'Comercio;Terminal;Pedido;Fecha;Referencia';

    public function getHelp()
    {
        echo ' This expects a CSV file input to register new users. Check ENHANCEMENT 4681 for more info. ';
        return parent::getHelp() . " \n ";
    }

    //
    // TEST:    php -f  /var/www/abawebapps/campus/public/script.php  ParseCaixaMerchantReference4681FromCsv
    //
    public function run($args)
    {
        echo " \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
        echo " \n ParseCaixaMerchantReference4681FromCsvCommand: executing auto process at " . date("Y-m-d H:i:s", time()) . "\n";

        if (count($args) > 0) {
            $this->_pathFileIn = $args[0];
        }

        if (count($args) > 1) {
            $this->_pathFileOut = $args[1];
        }

        if (!file_exists($this->_pathFileIn)) {
            echo " 1. File does not exist mate " . $this->_pathFileIn . "\n";
            return;
        }

        if (file_exists($this->_pathFileOut)) {

            $this->_pathFileOut = $this->_pathFileOut . 'lacaixaMigrationFileOut_' . date("Ymd_His", time()) . '.txt';

            file_put_contents($this->_pathFileOut, " \n", FILE_APPEND);
        } else {
            echo " 2. File does not exist mate " . $this->_pathFileOut . "\n";
            return;
        }

        $totalLines =               count(file($this->_pathFileIn));
        $aAllUsers =                array();
        $paymentLinesUpdated =      0;
        $paymentLinesNotUpdated =   0;
        $paymentLinesRepeated =     0;

        $handle = @fopen($this->_pathFileIn, "r");

        if ($handle) {

            $iLine = 0;

            while (($newLine = fgets($handle, 4096)) !== false) {

                if($iLine > 0) {

                    ++$iLine;

                    $aPartsLine = explode(',', trim($newLine, "\r\n"));

                    list($comercio, $terminal, $pedido, $fecha, $referencia) = $aPartsLine;

                    $comercio =     trim($comercio);
                    $terminal =     trim($terminal);
                    $pedido =       trim($pedido);
                    $fecha =        trim($fecha);
                    $referencia =   trim($referencia);

                    if ($comercio == '' || $pedido == '' || $referencia == '') {
                        echo "+++++++++++Invalid data: " . $comercio . "-" . $pedido . "-" . $referencia . "+++++++++++\n";
                        break;
                    }

                    if (array_search($referencia, $aAllUsers, true) == false) {
                        /*--------------------------------------------------------------------------*/
                        /*--         UPDATE REFERENCE              --*/

                        $aAllUsers[] = $referencia;

                        try {

                            $moPayments = new Payments();
                            /*  @var Payment[] $aPendingPays    */
                            $aAllPaySuppOrderIdPays = $moPayments->getAllPaymentsLaCaixaMigration($pedido);

                            if($aAllPaySuppOrderIdPays) {
                                foreach($aAllPaySuppOrderIdPays as $oPayment) {

echo "\n++++++++++++START: userId=" . $oPayment->userId . " paymentId=" . $oPayment->id . " paymentStatus=" . $oPayment->status . " +++++++++++\n";

                                    switch($oPayment->status) {
                                        case PAY_PENDING:
                                        case PAY_SUCCESS:
                                        case PAY_REFUND:
                                            //
                                            if($oPayment->updatePaySuppExtProfIdPayment($referencia)) {
                                                file_put_contents($this->_pathFileOut, "* UPDATED OK: comercio=" . $pedido . " referencia=" . $referencia . " paymentId= " . $oPayment->id . "\n", FILE_APPEND);
                                                echo "\n++++++++++++ OK for userId=" . $oPayment->userId . " paymentId=" . $oPayment->id . " paymentStatus=" . $oPayment->status . " ++++++++++++\n";
                                                ++$paymentLinesUpdated;
                                            }
                                            else {
                                                file_put_contents($this->_pathFileOut, "*NOT UPDATED: comercio=" . $pedido . " referencia=" . $referencia . ", ERROR ON updatePaySuppExtProfIdPayment() for paymentId=" . $oPayment->id . "\n", FILE_APPEND);
                                                echo "\n Error on updatePaySuppExtProfIdPayment() for paymentId=" .  $oPayment->id . " , not processed\n";
                                                ++$paymentLinesNotUpdated;
                                            }
                                            break;
                                        default:
                                            file_put_contents($this->_pathFileOut, "*NOT UPDATED: comercio=" . $pedido . " referencia=" . ", INCORRECT STATE OF PAYMENT paymentId=" . $oPayment->id . " paymentStatus=" . $oPayment->status . "\n", FILE_APPEND);
                                            echo "\n Incorrect payment state: " .  $oPayment->id . "= " . $oPayment->status . " . not processed\n";
                                            ++$paymentLinesNotUpdated;
                                            break;
                                    }
                                }
                            }
                        } catch (Exception $exc) {
                            file_put_contents($this->_pathFileOut, "*EXCEPTION: comercio=" . $pedido . " referencia=" . $referencia . $exc->getMessage() . ", " . $newLine . "\n", FILE_APPEND);
                            echo "\n Registering paySuppOrderId: " . $pedido . "= exception " . print_r("", true) . "\n";
                            ++$paymentLinesNotUpdated;
                        }
                        /*--------------------------------------------------------------------------*/
                    } else {
                        file_put_contents($this->_pathFileOut, "*NOT UPDATED: comercio=" . $pedido . " referencia=" . $referencia . " , repeated so not processed" . " \n", FILE_APPEND);
                        echo "\n Duplicated payment on file: comercio=" . $pedido . " referencia=" . $referencia . " , repeated so not processed\n";
                        ++$paymentLinesRepeated;
                    }
                }
                else {
                    ++$iLine;
                }
            }

            fclose($handle);

            if ($iLine !== $totalLines) {
                echo "Some errors: The lines processed are " . $iLine . " and were expected " . $totalLines .
                    ". Please review file and database mismatch.";
            } else {
                if (($paymentLinesUpdated + $paymentLinesNotUpdated + $paymentLinesRepeated) !== ($iLine - 1)) {
                    echo "Some errors, break down: Total lines= " . $iLine . " , Payments lines updated= " . $paymentLinesUpdated . " , payments lines NOT updated=" . $paymentLinesNotUpdated .
                        ", payments lines REPEATED=" . $paymentLinesRepeated;
                } else {
                    // TOTAL SUCCESS
                    echo "Success: Total lines processed=" . $totalLines . " Break down: Payments lines updated= " .
                        $paymentLinesUpdated . " , payments lines NOT updated=" . $paymentLinesNotUpdated .
                        ", payments lines REPEATED=" . $paymentLinesRepeated;
                }
            }
        }

        echo " \n  Finished process at ".date('Y-M-D H:m:s');
        echo " \n -----------------------------------------------------------------------------------------------\n";

    }
}
