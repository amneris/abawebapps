<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 22/11/12
 * Time: 12:11
 */
class TestCommand extends AbaConsoleCommand
{
    public function getHelp()
    {
        return parent::getHelp()." \n ";
    }

    public function run($args)
    {
//ini_set("memory_limit","2M");
        echo "Se ejecuta el cron contra YII. \n";
        echo " Let´s see if it executes every feature correctly.Let s check 1.database, 2. config, 3. params, 4.API/Common, etc.: \n";
        echo "1. Access to database simple = ";
        $moUser = new AbaUser();
        $succ = $moUser->getUserByEmail("joaquin.forcada@yopmail.com");
        echo " apu@yopmail.com , nombre= ".$moUser->name." ".$moUser->surnames;
        if(!$succ){
            echo " Failure accessing database ";
        }
        echo " \n ";

        echo "2. Access to config = ".Yii::app()->config->get("EMAIL_IT_BUGS_PAYMENTS");

        echo " \n ";

        echo "3. Access to params of Yii, languages available= ".implode(", ", Yii::app()->params['languages']);

        echo " \n ";

        echo "4. Access to API : ";

        $commTest = new TestCommon();
        echo $commTest->testAccessHere();
//sleep(130);
        echo " \n ";

        echo "5. Param activation = ";
            Yii::app()->params["testGlobal"] = "Activado";
            echo Yii::app()->params["testGlobal"];

        echo " \n ";
        echo "6. Fecha del sistema PHP= ";
            echo HeDate::todaySQL( true );

        echo " \n ";
        echo "7. argumnetos por linea de comandos=";
             var_dump($args);

        echo " \n ";
        echo "8. Envio de email = ";
            if( count($args)>=2){
                if( HeLogger::sendLog("CRON Proceso programado Intranet de TEST",HeLogger::IT_BUGS, HeLogger::DEBUG, "Detección automático de errores y bugs, Test Command para Crons works.") )
                {
                    echo "Ha funcionado EMAIL";
                }
                else{
                    echo "No funciona el EMAIL";
                }
            }
            else{
                echo " Si quieres probar el envio de email pon 2 argumentos en la llamada a este cron.";
            }

        echo " \n ";
        echo '9. Excel libreria = ';

            $xls = new JPhpCsv('UTF-8', false, 'Users');

            $data = array("Row 1 "=>array("Col A"=>"valor A1 ", "Col B"=>"valor B1 "),
                          "Row 2 "=>array("Col A"=>"valor A2 ", "Col B"=>"valor B2 "));
            $xls->addArray($data);

            $xls->generateXML("users_"."test.csv", true, true);

echo " End of TestCommand ";
    }
}
