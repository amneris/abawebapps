<?php

/**
 * @package   Services_Emma
 * @author    Luis <lassandri@abaenglish.com>
 */
class Emma
{
    /**
     * Cache the API base url
     */
    public $base_url = "http://in.emmasolutions.net/API/";
    /**
     * Cache the user key for API usage
     */
    protected $_key;

    function __construct($key)
    {
        if (empty($key)) {
            return false;
        }

        $this->_key = $key;
    }

    /**
     * get all the users from eMMa.
     */
    function getUsers($key)
    {
        $service = $this->base_url . 'users-report?key=' . $key . '&ext-click-info';
        $result = file_get_contents($service);
        $result = json_decode($result, true);
        return $result;
    }

    /**
     * get all the users from eMMa in a range of dates.
     */
    function getUsersByDateRange($key, $fromDate, $toDate)
    {
        $service = $this->base_url . 'users-report?key=' . $key . '&from-date=' . urlencode($fromDate) . '&to-date=' . urlencode($toDate) . '&ext-click-info';
        $result = file_get_contents($service);
        $result = json_decode($result, true);
        return $result;
    }

}
