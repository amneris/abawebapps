

CREATE TABLE `aba_b2c`.`pay_methods_adyen_hpp` (
  `idPaymentMethod` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique id',
  `paymentMethod` varchar(45) DEFAULT NULL COMMENT 'Name of the method payment',
  `enabled` tinyint(1) unsigned NOT NULL COMMENT 'Is displayed to the user.',
  PRIMARY KEY (`idPaymentMethod`),
  UNIQUE KEY `byAdyenHppPaymentMethod` (`paymentMethod`),
  KEY `byEnabled` (`enabled`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Adyen payment methods (pagos alternativos)';


INSERT INTO `aba_b2c`.`pay_methods_adyen_hpp` (`idPaymentMethod`, `paymentMethod`, `enabled`)
VALUES
  (1, 'alipay', 1),
  (3, 'amex', 1),
  (4, 'bankTransfer_IBAN', 1),
  (5, 'bankTransfer_NL', 1),
  (6, 'bank_ru', 1),
  (7, 'bcmc', 1),
  (8, 'boleto', 1),
  (9, 'cup', 1),
  (10, 'diners', 1),
  (11, 'directEbanking', 1),
  (12, 'directdebit_NL', 1),
  (13, 'discover', 1),
  (14, 'dotpay', 1),
  (15, 'ebanking_FI', 1),
  (16, 'elo', 1),
  (17, 'elv', 1),
  (18, 'giropay', 1),
  (19, 'ideal', 1),
  (20, 'interac', 1),
  (21, 'jcb', 1),
  (22, 'maestro', 1),
  (23, 'mc', 1),
  (24, 'online_RU', 1),
  (25, 'sepadirectdebit', 1),
  (26, 'terminal_RU', 1),
  (27, 'trustly', 1),
  (28, 'trustpay', 1),
  (29, 'unionpay', 1),
  (30, 'visa', 1),
  (31, 'wallet_RU', 1),
  (32, 'qiwiwallet', 1),
  (33, 'safetypay', 1),
  (34, 'doku_mandiri_clickpay', 1),
  (35, 'doku_wallet', 1),
  (36, 'bankTransfer', 1),
  (37, 'visadankort', 1),
  (38, 'dankort', 1),
  (39, 'vias', 1),
  (40, 'maestrouk', 1),
  (41, 'solo', 1),
  (42, 'laser', 1),
  (43, 'bijcard', 1),
  (44, 'hipercard', 1),
  (45, 'uatp', 1),
  (46, 'cartebancaire', 1),
  (47, 'visaalphabankbonus', 1),
  (48, 'mcalphabankbonus', 1);

CREATE TABLE `aba_b2c`.`products_prices_adyen_hpp` (
  `idProductPrice` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idProduct` varchar(15) NOT NULL COMMENT 'foreign key to products_list',
  `idPaymentMethod` int(11) unsigned NOT NULL COMMENT 'foreign keys to pay_methods_adyen_hpp',
  `currencyAdyenHpp` varchar(4) NOT NULL DEFAULT 'EUR' COMMENT 'App store currency. FK to currencies',
  `priceAdyenHpp` decimal(18,4) NOT NULL COMMENT 'Price in the original currency',
  `enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'If is active or not this price',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date this record is accessed and modified.',
  `priceAdyenHppExtranet` decimal(18,4) DEFAULT NULL COMMENT 'Extranet price',
  `enabledExtranet` tinyint(1) DEFAULT '0' COMMENT 'If is active or not extranet price',
  PRIMARY KEY (`idProductPrice`),
  UNIQUE KEY `idProduct` (`idProduct`,`idPaymentMethod`,`currencyAdyenHpp`),
  KEY `byIdPaymentMethod` (`idPaymentMethod`),
  KEY `byCurrencyAdyenHpp` (`currencyAdyenHpp`),
  KEY `byProductCountry` (`idProduct`,`enabled`),
  KEY `byEnabled` (`enabled`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of adyen hpp prices';

ALTER TABLE `aba_b2c`.`payments_adyen_notifications` ADD INDEX `byAmountCurrency` (`amountCurrency`);
ALTER TABLE `aba_b2c`.`payments_adyen_notifications` ADD INDEX `byEventCode` (`eventCode`);
ALTER TABLE `aba_b2c`.`payments_adyen_notifications` ADD INDEX `byPaymentMethod` (`paymentMethod`);

