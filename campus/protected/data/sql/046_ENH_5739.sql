
CREATE TABLE `experiments` (
  `experimentId` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'AutoId, PK',
  `userCategoryId` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1=FREE&PREMIUM, 2=FREE, 3=PREMIUM',
  `experimentTypeId` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1=mobile test A/B, 2=scenarios payments',
  `experimentIdentifier` varchar(32) NOT NULL DEFAULT '' COMMENT 'Unique experiment identifier',
  `experimentDescription` varchar(75) DEFAULT NULL COMMENT 'Experiment description',
  `counterUsers` int(10) unsigned DEFAULT '0' COMMENT 'Global experiment counter',
  `limitUsers` int(10) unsigned DEFAULT '0' COMMENT 'Limit of users by experiment',
  `randomMode` tinyint(3) unsigned DEFAULT '1' COMMENT '1=sequencial, 2=Php Random...',
  `availableModeId` tinyint(3) unsigned DEFAULT '1' COMMENT '1=while, 2=always, 3=after',
  `dateStart` datetime DEFAULT NULL COMMENT 'Start date',
  `dateEnd` datetime DEFAULT NULL COMMENT 'End date',
  `languages` varchar(255) DEFAULT '' COMMENT 'alloved languages',
  `experimentStatus` tinyint(3) unsigned DEFAULT '1' COMMENT '1=enabled, 2=paused...',
  PRIMARY KEY (`experimentId`),
  UNIQUE KEY `byExperimentIdentifier` (`experimentIdentifier`),
  KEY `byCounterUsers` (`counterUsers`),
  KEY `byLimitUsers` (`limitUsers`),
  KEY `byDates` (`dateStart`,`dateEnd`),
  KEY `byExperimentStatus` (`experimentStatus`),
  KEY `byCategoryId` (`userCategoryId`),
  KEY `byAvailableMode` (`availableModeId`),
  KEY `byExperimentTypeId` (`experimentTypeId`),
  KEY `byProfile` (`dateStart`,`dateEnd`,`experimentStatus`,`counterUsers`,`limitUsers`,`experimentTypeId`),
  KEY `byLanguages` (`languages`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `experiments_availability_modes` (
  `availableModeId` tinyint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT 'AutoId, PK',
  `availableModeDescription` varchar(75) DEFAULT NULL COMMENT 'Experiment availability mode description',
  `dateAdd` datetime DEFAULT NULL COMMENT 'Create date',
PRIMARY KEY (`availableModeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `experiments_availability_modes` (`availableModeId`, `availableModeDescription`, `dateAdd`)
VALUES
(1,'Available while ago experiment','2015-10-01 01:01:01'),
(2,'Always available experiment','2015-10-01 01:01:01'),
(3,'Available after finish experiment','2015-10-01 01:01:01');

CREATE TABLE `experiments_types` (
  `experimentTypeId` tinyint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT 'AutoId, PK',
  `experimentTypeIdentifier` varchar(32) NOT NULL DEFAULT '' COMMENT 'Unique type identifier',
  `experimentTypeDescription` varchar(75) DEFAULT NULL COMMENT 'Experiment type description',
  `dateAdd` datetime DEFAULT NULL COMMENT 'Create date',
  PRIMARY KEY (`experimentTypeId`),
  UNIQUE KEY `uniqueByTypeIdentifier` (`experimentTypeIdentifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `experiments_types` (`experimentTypeId`, `experimentTypeIdentifier`, `experimentTypeDescription`, `dateAdd`)
VALUES
(1,'IDENTMOBILETESTAB','Mobile test A/B css','2015-10-01 01:01:01'),
(2,'IDENTSCENARIOSPAY','Scenarios payments','2015-10-01 01:01:01');


CREATE TABLE `experiments_user_categories` (
  `userCategoryId` tinyint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT 'AutoId, PK',
  `userCategoryDescription` varchar(75) DEFAULT NULL COMMENT 'Experiment user category description',
  `dateAdd` datetime DEFAULT NULL COMMENT 'Create date',
  PRIMARY KEY (`userCategoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `experiments_user_categories` (`userCategoryId`, `userCategoryDescription`, `dateAdd`)
VALUES
(1,'FREE & PREMIUM','2015-10-01 01:01:01'),
(2,'FREE','2015-10-01 01:01:01'),
(3,'PREMIUM','2015-10-01 01:01:01');

CREATE TABLE `experiments_variations` (
  `experimentVariationId` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'AutoId, PK',
  `experimentId` int(10) unsigned NOT NULL COMMENT 'Experiment ID',
  `experimentVariationIdentifier` varchar(32) NOT NULL DEFAULT '' COMMENT 'Unique identifier',
  `experimentVariationDescription` varchar(75) DEFAULT NULL COMMENT 'Description',
  `counterUsers` int(10) unsigned DEFAULT '0' COMMENT 'Variation counter',
  `limitUsers` int(10) unsigned DEFAULT '0' COMMENT 'Limit of users by variation',
  `isDefault` tinyint(1) unsigned DEFAULT '0' COMMENT '@TODO fase 2',
  `experimentVariationStatus` tinyint(3) unsigned DEFAULT '0' COMMENT '"Post" experiment status... 1=in test, 2=fail, 3=success,..',
  PRIMARY KEY (`experimentVariationId`),
  UNIQUE KEY `byExperimentVariationIdentifier` (`experimentVariationIdentifier`),
  KEY `byLimitUsers` (`limitUsers`),
  KEY `byCounterUsers` (`counterUsers`),
  KEY `byIsDefault` (`isDefault`),
  KEY `byExperimentId` (`experimentId`),
  KEY `byExperimentVariationStatus` (`experimentVariationStatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `experiments_variations_attributes_scenarios` (
  `experimentVariationAttributeId` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'AutoId, PK',
  `experimentVariationId` int(10) NOT NULL COMMENT 'FK to products_groups',
  `idProduct` varchar(15) NOT NULL COMMENT 'FK to products_prices',
  `idCountry` int(4) unsigned NOT NULL COMMENT 'Foreign key to countries. Default country 0',
  `price` decimal(18,4) DEFAULT '0.0000' COMMENT 'Price in the original currency',
  `enabled` tinyint(1) unsigned DEFAULT '1' COMMENT 'If it is a public product',
  PRIMARY KEY (`experimentVariationAttributeId`),
  UNIQUE KEY `byIdPrGrAndIdPr` (`experimentVariationId`,`idProduct`),
  KEY `byIdProduct` (`idProduct`),
  KEY `byIdCountry` (`idCountry`),
  KEY `byEnabled` (`enabled`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_experiments_variations` (
  `userExperimentVariationId` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Auto id',
  `userId` int(11) unsigned DEFAULT NULL COMMENT 'FK to users',
  `experimentVariationId` int(10) unsigned DEFAULT NULL COMMENT 'FK to experiments_variations',
  `status` smallint(3) unsigned DEFAULT '0' COMMENT '@TODO fase 2',
  `dateAdd` datetime DEFAULT NULL COMMENT 'Create date',
  PRIMARY KEY (`userExperimentVariationId`),
  KEY `byStatus` (`status`),
  KEY `byUserId` (`userId`),
  KEY `byExperimentVariationId` (`experimentVariationId`),
  KEY `byProfileUser` (`userId`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
--

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`) VALUES ('EXPERIMENT_RANDOM_MODE_DEFAULT', '1', '1=sequencial, 2=random (mt_rand) between available experiment variations');

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`) VALUES ('ENABLED_EXPERIMENTS', '1', '0=disabled, 1=enabled');

--
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
--

ALTER TABLE `payments_control_check`
ADD COLUMN `experimentVariationAttributeId` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Test scenarios: selected variation attribute ' AFTER `idPartner`;

ALTER TABLE `payments`
ADD COLUMN `experimentVariationAttributeId` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Test scenarios' AFTER `isExtend`;

--
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
--


DELIMITER //
CREATE PROCEDURE `resetUserFromCampus`(IN `argUserEmail` VARCHAR(65), OUT `userId` INT)
BEGIN

DECLARE varUserId INT;

SELECT u.`id` INTO varUserId FROM `user` u WHERE u.`email`=argUserEmail;

DELETE f.* FROM followup4 f WHERE f.`userid` = varUserId;
DELETE f.* FROM followup_html4 f WHERE f.`userid`=varUserId;

DELETE p.* FROM pay_gateway_response p WHERE p.`idPayment` IN (SELECT p.id FROM payments p WHERE p.`userId`=varUserId);
DELETE p.* FROM pay_gateway_sent p WHERE p.`idPayment` IN (SELECT p.id FROM payments p WHERE p.`userId`=varUserId);
DELETE p.* FROM pay_gateway_adyen_response p WHERE p.`idPayment` IN (SELECT p.id FROM payments_control_check p WHERE p.`userId`=varUserId);
DELETE p.* FROM pay_gateway_adyen_sent p WHERE p.`idPayment` IN (SELECT p.id FROM payments_control_check p WHERE p.`userId`=varUserId);

DELETE p.* FROM payments p WHERE p.`userId`=varUserId;
DELETE p.* FROM payments_control_check p WHERE p.`userId`=varUserId;
DELETE p.* FROM payments_appstore p WHERE p.`userId`=varUserId;

DELETE u.* FROM user_credit_forms u WHERE u.`userId`=varUserId;
DELETE u.* FROM user_objective u WHERE u.`userId`=varUserId;
DELETE u.* FROM user_devices u WHERE u.`userId`=varUserId;
DELETE u.* FROM user_address_invoice u WHERE u.`userId`=varUserId;
DELETE u.* FROM user_locations u WHERE u.`userId`=varUserId;
DELETE up.* FROM user_promos up WHERE up.`userId`=varUserId;
DELETE up.* FROM user_experiments_variations up WHERE up.`userId`=varUserId;
--  DELETE up.* FROM renewals_plans up WHERE up.`userId`=varUserId;

DELETE c.* FROM course_var4 c WHERE c.`userId`=varUserId;

DELETE m.* FROM messages m WHERE m.receiver_id=varUserId;
DELETE m.* FROM messages m WHERE m.sender_id=varUserId;

DELETE FROM `aba_b2c_logs`.`log_user_level_change` WHERE `userId`=varUserId;

DELETE IGNORE f.* FROM followup4_30 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_31 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_32 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_33 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_34 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_35 f WHERE f.`userid`=varUserId;

DELETE IGNORE f.* FROM followup_html4_30 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_31 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_32 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_33 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_34 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_35 f WHERE f.`userid`=varUserId;

DELETE u.* FROM `user` u WHERE u.id=varUserId;

SELECT varUserId INTO userId;

END//
DELIMITER ;


