
CREATE TABLE `user_zuora_invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL DEFAULT '0' COMMENT 'User ABA',
  `invoiceId` varchar(64) DEFAULT '' COMMENT 'Subscription ZUORA',
  `invoiceNumber` varchar(64) DEFAULT '' COMMENT 'Subscription ZUORA',
  `status` tinyint(3) DEFAULT '0',
  `dateAdd` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create date',
  PRIMARY KEY (`id`),
  KEY `byUserId` (`userId`),
  KEY `byInvoiceId` (`invoiceId`),
  KEY `byInvoiceNumber` (`invoiceNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
