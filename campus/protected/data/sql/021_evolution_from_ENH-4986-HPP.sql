
/* START ENH-4986, Adien HPP */

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_HPP_SKIN_DEFAULT_NAME', 'Jvizcgco', 'Name of default skin', '2015-04-28 16:16:16');
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_HPP_SKIN_DEFAULT_KEY', 'SKINTESTAbAenglish1qa2ws', 'Key of default skin', '2015-04-28 16:16:16');
-- INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_HPP_SKIN_DEFAULT_KEY', 'SKINLIVEAbAenglish1qa2ws', 'Key of default skin', '2015-04-28 16:16:16');

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_URL_PAYMENTMETHODS', 'https://test.adyen.com/hpp/directory.shtml', 'Get payment methods URL by SKIN', '2015-04-28 16:16:16');
-- INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_URL_PAYMENTMETHODS', 'https://live.adyen.com/hpp/directory.shtml', 'Get payment methods URL by SKIN', '2015-04-28 16:16:16');

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_URL_HPP_PAYMENT', 'https://test.adyen.com/hpp/select.shtml', 'Url skin hpp payment methods', '2015-04-28 16:16:16');
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_URL_HPP_PAYMENT_METHOD', 'https://test.adyen.com/hpp/details.shtml', 'Url skin hpp payment method', '2015-04-28 16:16:16');


INSERT INTO `aba_b2c`.`pay_methods` (`id`, `name`, `visibleUser`) VALUES (6, 'PAY_METHOD_ADYEN_HPP', 0);


INSERT INTO `aba_b2c`.`country_paymethod_paysupplier` (`idCountry`, `idPayMethod`, `idPaySupplier`) VALUES (47, 6, 10);


--
-- PRE PAYMENTS
--
CREATE TABLE `payments_adyen_hpp` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'AutoId, PK',
  `idPayment` VARCHAR(15) NOT NULL COMMENT 'FK to payments control check and to payments too.',
  `merchantReference` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Internal Id to payment gateway',
  `status` SMALLINT(5) UNSIGNED NULL DEFAULT '0',
  `requestDate` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FKByIdPayment` (`idPayment`, `merchantReference`, `status`),
  INDEX `status` (`status`)
)
COMMENT='Adyen requests linked with payments_control_check' COLLATE='utf8_general_ci' ENGINE=InnoDB;

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

CREATE TABLE `payments_adyen_pending_refunds` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'AutoId, PK',
  `idPayment` VARCHAR(15) NOT NULL COMMENT 'FK to payments control check and to payments too.',
  `pspReference` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Adyen payment reference',
  `merchantReference` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Aba payment reference',
	`status` SMALLINT(5) UNSIGNED NULL DEFAULT '0' COMMENT '0-PENDING, 40-REFUNDED',
  `requestDate` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `byIdPayment` (`idPayment`),
  INDEX `byIdReferences` (`pspReference`, `merchantReference`, `status`),
  INDEX `byStatus` (`status`)
)
COMMENT='Adyen pending of notification refunds' COLLATE='utf8_general_ci' ENGINE=InnoDB ;


INSERT INTO `aba_b2c`.`pay_suppliers` (`id`, `name`, `invoice`) VALUES (11, 'Adyen-HPP', 1);


/* END ENH-4986, Adien HPP */


