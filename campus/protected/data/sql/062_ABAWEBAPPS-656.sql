
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('TMP_LANDINGS_TRACKING_ENABLED', '0', 'Enable landings tracking', '2016-02-18 13:13:13');

CREATE TABLE `tmp_landings_tracking` (
  `tmpLandingTrackingId` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'AutoId, PK',
  `tmpAbaIdentifier` varchar(255) DEFAULT '' COMMENT 'Aba identifier',
  `tmpLandingIdentifier` text COMMENT 'URL',
  `landingData` mediumtext COMMENT 'Landing json',
  `dateAdd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create date',
  PRIMARY KEY (`tmpLandingTrackingId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tmp_landings_fields_tracking` (
  `tmpLandingFieldsTrackingId` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'AutoId, PK',
  `tmpLandingTrackingId` int(11) DEFAULT NULL,
  `fieldKey` varchar(100) DEFAULT NULL,
  `fieldValue` varchar(255) DEFAULT NULL,
  `dateAdd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create date',
  PRIMARY KEY (`tmpLandingFieldsTrackingId`),
  KEY `byTmpLandingTrackingId` (`tmpLandingTrackingId`),
  KEY `byFieldKey` (`fieldKey`),
  KEY `byFieldValue` (`fieldValue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

