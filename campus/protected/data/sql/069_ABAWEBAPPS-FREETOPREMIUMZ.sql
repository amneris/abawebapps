
INSERT INTO `aba_b2c`.`pay_methods` (`id`, `name`, `visibleUser`) VALUES (7, 'Zuora', 0);

INSERT INTO `aba_b2c`.`pay_suppliers` (`id`, `name`, `invoice`) VALUES (13, 'Zuora', 1);
--
-- CREATE TABLE `pay_gateway_zuora_sent` (
--   `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
--   `dateSent` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Sent date',
--   `userId` int(10) unsigned DEFAULT NULL COMMENT 'FK to userId',
--   `idPayment` varchar(15) DEFAULT NULL COMMENT 'FK to payments control check',
--   `amountValue` varchar(45) DEFAULT NULL COMMENT 'Amount value',
--   `amountCurrency` varchar(3) DEFAULT NULL COMMENT 'Currency',
--   `refId` varchar(72) DEFAULT NULL COMMENT 'Payment method ID',
--   `shopperEmail` varchar(54) DEFAULT NULL COMMENT 'Shopper email',
--   `request` mediumtext COMMENT 'Encoded user credit card form data for AUTHORIZATION transaction',
--   `sentType` smallint(5) unsigned DEFAULT '10',
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--
-- CREATE TABLE `pay_gateway_zuora_response` (
--   `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
--   `dateSent` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Sent date',
--   `idPayment` varchar(15) DEFAULT NULL COMMENT 'FK to payments control check',
--   `success` tinyint(3) DEFAULT '0',
--   `Order_number` varchar(45) DEFAULT NULL COMMENT 'Unique adyen identifier',
--   `response` mediumtext COMMENT '"Object" response',
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;



