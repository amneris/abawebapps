

ALTER TABLE `payments_adyen_pending_refunds` ADD COLUMN `amountPrice` DECIMAL(18,9) NULL DEFAULT '0' AFTER `merchantReference`;

ALTER TABLE `payments_adyen_pending_refunds`
  CHANGE COLUMN `amountPrice` `amountPrice` DECIMAL(18,9) NULL DEFAULT '0.000000000' COMMENT 'Total final amount.' AFTER `merchantReference`,
  ADD COLUMN `paySuppOrderId` VARCHAR(80) NULL DEFAULT '' COMMENT 'Id transaction of external gateway payment' AFTER `amountPrice`,
  ADD COLUMN `dateToPay` DATETIME NULL DEFAULT NULL COMMENT 'Future date for PENDING STATUS only' AFTER `paySuppOrderId`,
  CHANGE COLUMN `status` `status` SMALLINT(5) UNSIGNED NULL DEFAULT '0' COMMENT '0-PENDING, 10-FAIL, 30-SUCCESS NOTIFICATION, 40-REFUNDED' AFTER `dateToPay`;


