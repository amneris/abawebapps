ALTER TABLE `user_devices`
ADD COLUMN `appVersion` VARCHAR(25) NULL DEFAULT NULL COMMENT 'Version of application installed on device' AFTER `dateLastModified`;