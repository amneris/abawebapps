

DROP PROCEDURE IF EXISTS `aba_b2c`.`resetUserFromCampus`;


DELIMITER //
CREATE PROCEDURE `resetUserFromCampus`(IN `argUserEmail` VARCHAR(65), OUT `userId` INT)
BEGIN

DECLARE varUserId INT;

SELECT u.`id` INTO varUserId FROM `user` u WHERE u.`email`=argUserEmail;

DELETE f.* FROM followup4 f WHERE f.`userid` = varUserId;
DELETE f.* FROM followup_html4 f WHERE f.`userid`=varUserId;

DELETE p.* FROM pay_gateway_response p WHERE p.`idPayment` IN (SELECT p.id FROM payments p WHERE p.`userId`=varUserId);
DELETE p.* FROM pay_gateway_sent p WHERE p.`idPayment` IN (SELECT p.id FROM payments p WHERE p.`userId`=varUserId);
DELETE p.* FROM pay_gateway_adyen_response p WHERE p.`idPayment` IN (SELECT p.id FROM payments_control_check p WHERE p.`userId`=varUserId);
DELETE p.* FROM pay_gateway_adyen_sent p WHERE p.`idPayment` IN (SELECT p.id FROM payments_control_check p WHERE p.`userId`=varUserId);

DELETE p.* FROM payments p WHERE p.`userId`=varUserId;
DELETE p.* FROM payments_control_check p WHERE p.`userId`=varUserId;
DELETE p.* FROM payments_appstore p WHERE p.`userId`=varUserId;
DELETE p.* FROM payments_playstore p WHERE p.`userId`=varUserId;

DELETE u.* FROM user_credit_forms u WHERE u.`userId`=varUserId;
DELETE u.* FROM user_objective u WHERE u.`userId`=varUserId;
DELETE u.* FROM user_devices u WHERE u.`userId`=varUserId;
DELETE u.* FROM user_address_invoice u WHERE u.`userId`=varUserId;
DELETE u.* FROM user_locations u WHERE u.`userId`=varUserId;
DELETE up.* FROM user_promos up WHERE up.`userId`=varUserId;
DELETE up.* FROM user_experiments_variations up WHERE up.`userId`=varUserId;
--  DELETE up.* FROM renewals_plans up WHERE up.`userId`=varUserId;

DELETE c.* FROM course_var4 c WHERE c.`userId`=varUserId;

DELETE m.* FROM messages m WHERE m.receiver_id=varUserId;
DELETE m.* FROM messages m WHERE m.sender_id=varUserId;

DELETE FROM `aba_b2c_logs`.`log_user_level_change` WHERE `userId`=varUserId;

DELETE IGNORE f.* FROM followup4_30 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_31 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_32 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_33 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_34 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_35 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_36 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_37 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_38 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_39 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_40 f WHERE f.`userid`=varUserId;

DELETE IGNORE f.* FROM followup_html4_30 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_31 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_32 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_33 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_34 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_35 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_36 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_37 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_38 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_39 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_40 f WHERE f.`userid`=varUserId;

DELETE u.* FROM `user` u WHERE u.id=varUserId;

SELECT varUserId INTO userId;

END//
DELIMITER ;





