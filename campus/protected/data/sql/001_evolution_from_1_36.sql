
/* ---- ENH-3191 ------- */
ALTER TABLE `aba_b2c`.`payments` ADD COLUMN `isPeriodPayChange` SMALLINT DEFAULT NULL COMMENT 'Previous product. Upgrade-downgrade of plan.Negative downgrade, positive upgrade.' AFTER `paySuppLinkStatus`;

ALTER TABLE `aba_b2c`.`payments` MODIFY COLUMN `isPeriodPayChange` SMALLINT(6) COMMENT 'Previous product. Upgrade-downgrade of plan.negative downgrade, positive upgrade.',
 ADD COLUMN `cancelOrigin` TINYINT UNSIGNED DEFAULT NULL COMMENT '1-User cancels,3-Expiration,4-PayPal IPN,5-Intranet employee,6-Change Product' AFTER `isPeriodPayChange`;

INSERT INTO config (`key`, `value`,`description`)
      VALUES( 'OP_URL_SELLIGENT_CHANGEPRODUCT', '/optiext/optiextension.dll?ID=jHQjUIF546ZSWwydoHCIv5TPoH46EWCglhj_hMLAaBo7Ux954m93Mwy7mlO2%2BMlJ_Rj8AplvdCl8fFVfrE',
                          'Operation: Change product operation, upgrade/downgrade')
  ON DUPLICATE KEY UPDATE `value`='/optiext/optiextension.dll?ID=jHQjUIF546ZSWwydoHCIv5TPoH46EWCglhj_hMLAaBo7Ux954m93Mwy7mlO2%2BMlJ_Rj8AplvdCl8fFVfrE',
                          `description`='Operation: Change product operation, upgrade/downgrade';

INSERT INTO config (`key`, `value`,`description`)
      VALUES( 'ENABLE_CHANGE_PRODUCT', '0',
                          'Enables 1 or disables 0 the feature to change product for PREMIUM users.')
  ON DUPLICATE KEY UPDATE `value`='0',
                          `description` = 'Enables 1 or disables 0 the feature to change product for PREMIUM users.';
/* ---------------------*/
/* ------  ENH-3445  -------------*/
ALTER TABLE `aba_b2c`.`user` MODIFY COLUMN `name` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci,
 MODIFY COLUMN `surnames` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci;
/* ------------*/
INSERT INTO config (`key`, `value`,`description`)
      VALUES( 'WORD4COURSEWS', MD5('Course Outsource Aba AgnKey'),
                          'Magic sentence to encrypt all requests to the platform of web services of the course')
  ON DUPLICATE KEY UPDATE `value`=MD5('Course Outsource Aba AgnKey'),
                          `description`='Magic sentence to encrypt all requests to the platform of web services of the course';
/* ------------*/

/* ------  ENH-3290  -------------*/
ALTER TABLE `user` ADD COLUMN `lastLoginTime` DATETIME NULL DEFAULT NULL COMMENT 'Last Login DateTime' AFTER `dateUpdateEmma`;
/* ------------*/



/* ------  ENH-2521  -------------*/
ALTER TABLE `log_web_service`
ADD COLUMN `errorCode` SMALLINT(5) NULL DEFAULT NULL COMMENT 'Error code, if successResponse is 0' AFTER `successResponse`;
/* ------------*/

/* ------  ENH-3268  -------------*/
ALTER TABLE `messages`
    ADD COLUMN `sendFromRole` TINYINT(3) UNSIGNED NULL DEFAULT NULL COMMENT 'Type of the user role sent the email'  AFTER `created_at`;

ALTER TABLE `messages`
    ADD INDEX `bySendFromRole` (`sendFromRole`);
/* ------------*/


/* ------  ENH-3582  -------------*/

ALTER TABLE aba_b2c.`products_prices` ADD COLUMN `priceExtranet` DECIMAL(18,4) NULL DEFAULT NULL COMMENT 'Extranet price' AFTER `visibleWeb`, ADD COLUMN `enabledExtranet` TINYINT(1) NULL DEFAULT '0' COMMENT 'If is active or not extranet price' AFTER `priceExtranet`;

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('WORD4WSEXTRANET', '16cd036f530ebfba02be08017cee16bb', 'Light security measure to secure communications between Extranet and Campus.', '2014-10-23 11:11:11');

UPDATE `aba_b2c`.`products_prices` SET `priceExtranet` = `priceOfficialCry`, `enabledExtranet` = true WHERE `idPeriodPay` in (90, 180, 360, 720);

UPDATE `aba_b2c`.`products_prices` SET `priceExtranet` = `priceOfficialCry` WHERE `idPeriodPay` in (30, 540);


INSERT INTO `aba_b2c_extranet`.`partner_deals` (`code`, `idPartner`, `idPeriodPay`, `numValidations`, `expirationDate`, `creationDate`) VALUES ('EXTRANET30', 7002, 30, 1000000, '2015-12-31', '2014-08-19 00:00:00');
INSERT INTO `aba_b2c_extranet`.`partner_deals` (`code`, `idPartner`, `idPeriodPay`, `numValidations`, `expirationDate`, `creationDate`) VALUES ('EXTRANET90', 7002, 90, 1000000, '2015-12-31', '2014-08-19 00:00:00');
INSERT INTO `aba_b2c_extranet`.`partner_deals` (`code`, `idPartner`, `idPeriodPay`, `numValidations`, `expirationDate`, `creationDate`) VALUES ('EXTRANET180', 7002, 180, 1000000, '2015-12-31', '2014-08-19 00:00:00');
INSERT INTO `aba_b2c_extranet`.`partner_deals` (`code`, `idPartner`, `idPeriodPay`, `numValidations`, `expirationDate`, `creationDate`) VALUES ('EXTRANET360', 7002, 360, 1000000, '2015-12-31', '2014-08-19 00:00:00');
INSERT INTO `aba_b2c_extranet`.`partner_deals` (`code`, `idPartner`, `idPeriodPay`, `numValidations`, `expirationDate`, `creationDate`) VALUES ('EXTRANET540', 7002, 540, 1000000, '2015-12-31', '2014-08-19 00:00:00');
INSERT INTO `aba_b2c_extranet`.`partner_deals` (`code`, `idPartner`, `idPeriodPay`, `numValidations`, `expirationDate`, `creationDate`) VALUES ('EXTRANET720', 7002, 720, 1000000, '2015-12-31', '2014-08-19 00:00:00');

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES
('OP_URL_SELLIGENT_EXTRANET_REGISTROAGRUPADORES', '/optiext/optiextension.dll?ID=SepSmrCZaCgau%2BfmPz9pMDyHiB2DpHePEJjUNhgUx0A46O5CC8UTD2roZYlrygD7vhxus2cGUeRnbnerYC', 'Operation: Registro EXTRANET web In Selligent', '2014-12-03 15:47:44');





