INSERT INTO
  `aba_b2c`.`config` (`key`, `value`, `description`)
VALUES (
  'UPDATABLE_PARTNER_IDS',
  '400004, 400005, 400089',
  'Values indicate what partner ids should be updated from Adjust update attribution'
);