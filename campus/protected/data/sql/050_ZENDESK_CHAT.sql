
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`) VALUES ('ZENDESK_CHAT_COUNTRIES', 'es,it', 'Paises, separados por coma: de, en, es, fr, it, pt, ru');

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`) VALUES ('ZENDESK_CHAT_JS_SCRIPT', 'window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=d.createElement(s),e=d.getElementsByTagName(s)[0];
z.set=function(o){z.set._.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3TnP9BIqvzw2Bl97sthQ5EPEGFCvBxYU";z.t=+new Date;$.type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");', 'javascript caht');

