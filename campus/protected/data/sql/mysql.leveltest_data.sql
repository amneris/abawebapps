-- Tabla de datos de los test de nivel.

CREATE TABLE aba_b2c.leveltest_Data (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT                    COMMENT 'Autoincrement.',

    code varchar(255) NOT NULL                                        COMMENT 'Access code.',
    status integer default 1 NOT NULL                                 COMMENT '1: New. 2: Pending. 3: Finished.',
    spentTime integer default 0 NOT NULL                              COMMENT 'Time to complete.',
    jsonData varchar(2000) default '' NOT NULL                        COMMENT 'Mixed data.',

    idLevel integer default 1 not null                                COMMENT 'Current level.',
    numErrors integer default 0 not null                              COMMENT 'Current errors.',
    numQuestions integer default 0 not null                           COMMENT 'Current answered questions number.',

    completed datetime default '2000-01-01 00:00:00' NOT NULL         COMMENT 'When is completed.',

    created DATETIME DEFAULT '2000-01-01 00:00:00' NOT NULL   COMMENT 'Datetime when the register was created.',
    deleted DATETIME DEFAULT '2000-01-01 00:00:00' NOT NULL   COMMENT 'Datetime when the register was deleted.',
    isdeleted int(11) NOT NULL DEFAULT '0'                    COMMENT 'Register is deleted. 0: false. 1: true.'

) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

ALTER TABLE abaenglish_extranet.student_level_test ADD COLUMN code varchar(255) NOT NULL COMMENT 'Access code.' AFTER idLevelTest;
update abaenglish_extranet.student_level_test slt set code = (select ltd.code from abaenglish_extranet.LevelTest_Data ltd where ltd.id = slt.idLevelTest);