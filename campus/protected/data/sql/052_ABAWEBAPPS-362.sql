

CREATE TABLE `payments_adyen_hpp_renewals` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'AutoId, PK',
  `idPayment` varchar(15) NOT NULL COMMENT 'FK to payment.',
  `merchantReference` varchar(45) DEFAULT NULL COMMENT 'Internal Id to payment gateway',
  `status` smallint(5) unsigned DEFAULT '0',
  `requestDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKByIdPayment` (`idPayment`,`merchantReference`,`status`),
  KEY `FKByIdPaymentControlCheck` (`merchantReference`,`status`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Adyen requests linked with payments';


ALTER TABLE `payments_adyen_hpp` ADD `isExtend` TINYINT(1) UNSIGNED  NULL  DEFAULT '0' AFTER `status`;



