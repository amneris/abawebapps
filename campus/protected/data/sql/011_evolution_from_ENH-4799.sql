
/* START ENH-4799, Asignación nuevo tutor alumnos Junta de Castilla y León */

ALTER TABLE `user_teacher` ADD COLUMN `registerTeacherSource` TINYINT(3) NULL DEFAULT '1' COMMENT 'Register from ABA = 1, from extranet = 2...' AFTER `mobileImage3x`;

--
-- TEST
--
-- INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES
-- ('OP_URL_SELLIGENT_TEACHERS', '/wspostaba/SimulateSelligent', 'Operation: Registro Teacher In Selligent', '2015-04-14 15:15:15');
--

--
-- PROD
--
-- INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES
-- ('OP_URL_SELLIGENT_TEACHERS', '/optiext/optiextension.dll?ID=HjcHaiwHxM0ylrypseekT5dVCbHljG2RbuK8JmavOP0Qr37Xr9TbwWm_tKOlp1Xcjvya_0iGX7MuFhZhRT', 'Operation: Registro Teacher In Selligent', '2015-04-14 15:15:15');
--

/* END ENH-4799 */





