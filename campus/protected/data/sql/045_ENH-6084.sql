
CREATE TABLE `payments_adyen_frauds` (
  `idFraud` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'AutoId, PK',
  `userId` int(10) unsigned DEFAULT NULL COMMENT 'FK to users',
  `idPayment` varchar(15) DEFAULT NULL COMMENT 'FK to payments',
  `amountCurrency` varchar(4) DEFAULT NULL COMMENT 'Currency',
  `amountValue` float unsigned DEFAULT '0' COMMENT 'Amount value',
  `eventCode` varchar(45) DEFAULT '' COMMENT 'Notification code: AUTHORISATION, REPORT_AVAILABLE, REFUND,..',
  `merchantReference` varchar(45) DEFAULT '' COMMENT 'Merchant unique referente',
  `originalReference` varchar(45) DEFAULT '' COMMENT 'This is the pspReference that was assigned to the authorisation',
  `pspReference` varchar(45) DEFAULT '' COMMENT 'Adyen unique identifier',
  `reason` varchar(510) DEFAULT '' COMMENT 'Reason code OR  URL of available report file',
  `paymentMethod` varchar(45) DEFAULT '' COMMENT 'Payment method code: visa, directdebit.NL,..',
  `status` tinyint(3) unsigned DEFAULT '0' COMMENT '0=pending,...',
  `dateAdd` datetime DEFAULT NULL COMMENT 'Created data (aba)',
  PRIMARY KEY (`idFraud`),
  KEY `byUserId` (`userId`),
  KEY `byIdPayment` (`idPayment`),
  KEY `byEventCode` (`eventCode`),
  KEY `byMerchantReference` (`merchantReference`(16)),
  KEY `byOriginalReference` (`originalReference`(20)),
  KEY `byPspReference` (`pspReference`(20)),
  KEY `byStatus` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




