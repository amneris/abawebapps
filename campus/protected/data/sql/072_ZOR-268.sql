
CREATE TABLE `user_zuora` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL DEFAULT '0' COMMENT 'User ABA',
  `zuoraAccountId` varchar(64) NOT NULL DEFAULT '' COMMENT 'User ZUORA',
  `zuoraAccountNumber` varchar(64) DEFAULT NULL COMMENT 'User ZUORA',
  `zuoraSubscriptionId` varchar(64) DEFAULT '' COMMENT 'Subscription ZUORA',
  `zuoraSubscriptionNumber` varchar(64) DEFAULT '' COMMENT 'Subscription ZUORA',
  `status` tinyint(3) DEFAULT '0',
  `dateAdd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create date',
  PRIMARY KEY (`id`),
  UNIQUE KEY `byUserSubscription` (`userId`,`zuoraAccountId`,`zuoraSubscriptionId`),
  KEY `byUserId` (`userId`),
  KEY `byUserZuoraId` (`zuoraAccountId`),
  KEY `zuoraAccountId` (`zuoraAccountNumber`),
  KEY `bySubscriptionId` (`zuoraSubscriptionId`),
  KEY `bySubscriptionNumber` (`zuoraSubscriptionNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_zuora_invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUserZuora` int(11) unsigned DEFAULT '0',
  `paymentId` varchar(15) DEFAULT '' COMMENT 'Payment ABA',
  `userId` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'User ABA',
  `invoiceId` varchar(64) DEFAULT '' COMMENT 'Invoice id zuora',
  `invoiceNumber` varchar(64) DEFAULT '' COMMENT 'Invoice number zuora',
  `status` tinyint(3) DEFAULT '0',
  `dateAdd` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create date',
  PRIMARY KEY (`id`),
  KEY `byUserId` (`userId`),
  KEY `byInvoiceId` (`invoiceId`),
  KEY `byInvoiceNumber` (`invoiceNumber`),
  KEY `byUserZuoraId` (`idUserZuora`),
  KEY `paymentId` (`paymentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;