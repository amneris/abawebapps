
/* START ENH-5222 */

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PLAN_FAMILY_RENEWALS_ENABLED', 1, '', '2015-04-28 16:16:16');
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PLAN_FAMILY_DATE_START', '2015-06-09', 'Family Plan Renewals Start Date', '2015-04-28 16:16:16');
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PLAN_FAMILY_DATE_END', '2015-06-16', 'Family Plan Renewals End Date', '2015-04-28 16:16:16');
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PLAN_FAMILY_RENEWAL_DAYS_PLUS', 7, '', '2015-04-28 16:16:16');

CREATE TABLE `renewals_plans` (
`idRenewal` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Auto id',
`idPayment` VARCHAR(15) NOT NULL DEFAULT '',
`userId` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'FK to users',
`langEnv` VARCHAR(3) NULL DEFAULT '' COMMENT 'Language of the Campus.',
`amountPrice` DECIMAL(18,9) NULL DEFAULT NULL COMMENT 'Original price before discount.',
`currencyTrans` VARCHAR(3) NULL DEFAULT 'EUR' COMMENT 'FK to currencies',
`idPeriodPay` INT(5) UNSIGNED NULL DEFAULT NULL COMMENT 'FK mixed to user_products_stamp',
`dateToPay` DATETIME NULL DEFAULT NULL COMMENT 'Future date for PENDING STATUS only',
`origin` SMALLINT(2) UNSIGNED NOT NULL DEFAULT '1' COMMENT '1-Family plan...',
`status` SMALLINT(3) UNSIGNED NULL DEFAULT '0' COMMENT '0-pending, 10-send to selligent...',
`dateAdd` DATETIME NULL DEFAULT NULL COMMENT 'Create date',
PRIMARY KEY (`idRenewal`),
UNIQUE INDEX `renewByPaymentUser` (`idPayment`, `userId`),
INDEX `renewByOrigin` (`origin`),
INDEX `renewByUserId` (`userId`)
)
COMMENT='Log renewlas family plan' COLLATE='utf8_general_ci' ENGINE=InnoDB ;


--
-- TEST - OK
--
-- INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES
-- ('OP_URL_SELLIGENT_RENEWALSPLANNOTIFICATION', '/wspostaba/SimulateSelligent', 'Operation: Premium renewal plan notification In Selligent', '2015-06-06 06:06:06');
--
-- INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES
-- ('OP_URL_SELLIGENT_RENEWALSPLANPAYMENT', '/wspostaba/SimulateSelligent', 'Operation: Renewal premium with 4 plans extra notification In Selligent', '2015-06-06 06:06:06');
--

--
-- PROD - OK
--
-- INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES
-- ('OP_URL_SELLIGENT_RENEWALSPLANNOTIFICATION', '/optiext/optiextension.dll?ID=AY5A5Z5aNlJdrDnYuzxyC9GZmFrP%2BWrQRQMGLo38wIo429EQRfsuDbd1NgtlxBgOazNDYKk3%2BrBGHhKJwL', 'Operation: Premium renewal plan notification In Selligent', '2015-06-06 06:06:06');
--
-- INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES
-- ('OP_URL_SELLIGENT_RENEWALSPLANPAYMENT', '/optiext/optiextension.dll?ID=YArYq3uOrS3StQCLh55dK2TpQ1bP06HoO8_gg3aoz8Gm%2BuTIcZOaqCgZZs6aIvYj1FGozZC7616RRtql65', 'Operation: Renewal premium with 4 plans extra notification In Selligent', '2015-06-06 06:06:06');
--

/* END ENH-5222 */

