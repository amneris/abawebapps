
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES
('PAY_ADYEN_RECURRING_INITIAL_PAYMENT_TYPE', 'ONECLICK,RECURRING', 'Adyen recurring payment type: RECURRING, ONECLICK or ONECLICK,RECURRING', '2016-03-13 13:13:13');

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES
('PAY_ADYEN_RECURRING_RECURRING_PAYMENT_TYPE', 'RECURRING', 'Adyen recurring payment type: RECURRING, ONECLICK or ONECLICK,RECURRING', '2016-03-13 13:13:13');

