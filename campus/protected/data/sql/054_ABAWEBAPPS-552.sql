
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES
('TMP_PIXELS_TRACKING_ENABLED', '1', 'Enable pixels tracking', '2016-02-18 13:13:13');


CREATE TABLE `tmp_pixels_tracking` (
  `tmpPixelTrackingId` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'AutoId, PK',
  `tmpPixelIdentifier` varchar(75) NOT NULL COMMENT 'IDENTIFIER',
  `partnerId` int(11) unsigned DEFAULT NULL COMMENT 'Prtner',
  `origin` tinyint(3) unsigned DEFAULT '1' COMMENT '1=web,2=extranet...',
  `additionalData` text COMMENT 'Additional data',
  `dateAdd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create date',
  PRIMARY KEY (`tmpPixelTrackingId`),
  KEY `tmpPixelIdentifier` (`tmpPixelIdentifier`),
  KEY `partnerId` (`partnerId`),
  KEY `origin` (`origin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

