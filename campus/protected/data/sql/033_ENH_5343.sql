
ALTER TABLE `payments_boleto` ADD COLUMN `dateLastOpenLink` DATETIME NULL DEFAULT NULL COMMENT 'Open link from e-mail' AFTER `xmlOrderId`;
