ALTER TABLE `products_prices_appstore`
    ALTER `idProductApp` DROP DEFAULT;
ALTER TABLE `products_prices_appstore`
    CHANGE COLUMN `idProductApp` `idProductApp` VARCHAR(56) NOT NULL COMMENT 'App store product ID' AFTER `idProductTier`;
