
CREATE TABLE `products_promos_description` (
  `idDescription` INT(11) NOT NULL AUTO_INCREMENT,
  `translationKey` VARCHAR(50) NULL DEFAULT NULL,
  `descriptionText` TEXT NULL,
  `createDate` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`idDescription`)
)
COLLATE='utf8_general_ci' ENGINE=InnoDB;

ALTER TABLE `products_promos` ADD COLUMN `idDescription` INT(11) UNSIGNED NULL DEFAULT NULL AFTER `showValidationType`;
