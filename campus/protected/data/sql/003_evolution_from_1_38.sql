/* ENH-3298, Boleto */

INSERT INTO aba_b2c.pay_methods (`id`, `name`, `visibleUser`)
        VALUES ( 4, 'Boleto', 1);

UPDATE aba_b2c.pay_methods pm SET pm.visibleUser=0  WHERE pm.id=3;
/* Brazil + Boleto + AllPAgo-Boleto  */
INSERT INTO aba_b2c.country_paymethod_paysupplier  (idCountry, idPayMethod, idPaySupplier)
                              VALUES (30, 4, 7);

INSERT INTO `aba_b2c`.config (`key`, `value`,`description`)
    VALUES( 'PAY_ALLPAGO_CHANNEL_INITIAL_BR_BOLETO', '8a8394c249eb576c0149f6385ed02e27', 'Chanel for AllPago Boleto Pa request transactions.')
    ON DUPLICATE KEY UPDATE `value` = '8a8394c249eb576c0149f6385ed02e27';

/* ENH - 3554: mexico currency change */
UPDATE `aba_b2c`.currencies c SET c.symbol = 'MXN' WHERE c.`id`='MXN';

/* ENH-3298, Boleto */
DROP TABLE IF EXISTS `aba_b2c`.`payments_boleto`;

CREATE TABLE  `aba_b2c`.`payments_boleto` (
  `id` int(10) unsigned NOT NULL auto_increment COMMENT 'AutoId, PK',
  `idPayment` varchar(15) NOT NULL COMMENT 'FK to payments control check and to payments too.',
  `idPaySupplier` int(1) unsigned NOT NULL COMMENT 'FK to pay_suppliers.',
  `paySuppExtUniId` varchar(45) default NULL COMMENT 'External Id to payment gateway',
  `url` varchar(255) default NULL COMMENT 'URL to Boleto to payment gateway, PDF',
  `requestDate` datetime default NULL COMMENT 'Date request, should match with payctrlchk.dateStartTransaction',
  `dueDate` datetime default NULL COMMENT 'Date the Boleto should change status to OFF',
  `status` int(2) unsigned default '0' COMMENT '0-PENDING, 10-FAILED, 30-SUCCESFUL',
  `xmlOrderId` varchar(20) default NULL COMMENT 'parameter OrderId from XML response',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `ByPaySuppExtUnid` USING BTREE (`paySuppExtUniId`),
  KEY `FKByIdPayment` (`idPayment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Boleto requests linked with payments_control_check';

INSERT INTO `aba_b2c`.config (`key`, `value`,`description`)
      VALUES( 'OP_URL_SELLIGENT_BOLETO', 'http://abaenglish.emsecure.net/optiext/optiextension.dll?ID=PbkPiMBas3tvSCqTSOnXLqq%2BvGtXpYivDMTB1iKOprwUEEL5y2PERm7Opyb4Ed6VWmu06mg5vjmCVkka9B',
                          'Operation: Boleto requested by user')
  ON DUPLICATE KEY UPDATE `value`='http://abaenglish.emsecure.net/optiext/optiextension.dll?ID=PbkPiMBas3tvSCqTSOnXLqq%2BvGtXpYivDMTB1iKOprwUEEL5y2PERm7Opyb4Ed6VWmu06mg5vjmCVkka9B',
                          `description`= 'Operation: Boleto requested by user';

ALTER TABLE `aba_b2c`.`payments_boleto` MODIFY COLUMN `url` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'URL to Boleto to payment gateway, PDF';

ALTER TABLE `aba_b2c`.`payments_control_check` ADD COLUMN `idPartner` INTEGER UNSIGNED DEFAULT NULL COMMENT 'IdPartner at the moment of the last attempt to pay.' AFTER `typeCpf`;

ALTER TABLE `aba_b2c`.`user_address_invoice` MODIFY COLUMN `zipCode` VARCHAR(14) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Postal code of the user';


