
CREATE TABLE aba_b2c.`log_sent_selligent` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `dateSent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date that was sent the request to Selligent. Wether successful or not.',
  `url` varchar(100) DEFAULT NULL COMMENT 'url where it was sent to.',
  `webservice` varchar(45) DEFAULT NULL COMMENT 'name of the operation in Selligent',
  `fields` text NOT NULL COMMENT 'field names sent and their values.',
  `response` text NOT NULL COMMENT 'text replied from selligent if successful, -1 otherwise.',
  `userId` int(11) unsigned DEFAULT NULL COMMENT 'User Id that triggered this request.',
  `sourceAction` varchar(100) DEFAULT NULL COMMENT 'Action or process from which this op was triggered',
  `errorDesc` varchar(250) DEFAULT NULL COMMENT 'In case of error, the details.',
  PRIMARY KEY (`id`),
  KEY `webservice` (`webservice`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='We store values sent to Selligent. ';
--  AUTO_INCREMENT=40000000

CREATE TABLE aba_b2c.`log_user_activity` (
  `userId` int(10) unsigned NOT NULL COMMENT 'FK to users',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `table` varchar(45) NOT NULL COMMENT 'LIKE a FK to a table name',
  `fields` text COMMENT 'LIKE a FK to a fields name',
  `description` varchar(150) NOT NULL,
  `action` varchar(45) NOT NULL COMMENT 'Operation on Campus, payment, Crons, etc.',
  `ip` varchar(45) DEFAULT NULL COMMENT 'REMOTE IP',
  PRIMARY KEY (`id`),
  KEY `byTime` (`time`),
  KEY `byUserId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Logs fields related with user, states, levels, payments, etc';
--  AUTO_INCREMENT=11000000

CREATE TABLE aba_b2c.`log_user_level_change` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Auto Id.',
  `userId` int(11) unsigned NOT NULL COMMENT 'Foreign key to table users',
  `dateModified` datetime NOT NULL COMMENT 'Date of this insert',
  `oldUserLevel` int(10) unsigned DEFAULT NULL COMMENT 'User level before update',
  `newUserLevel` int(10) unsigned DEFAULT NULL COMMENT 'New user level after change.',
  `sourceAction` varchar(45) DEFAULT NULL COMMENT 'Origin of this change: My account, User Objectives, registerUser.',
  PRIMARY KEY (`id`),
  KEY `byUserId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='It logs every time a user changes his level';
--  AUTO_INCREMENT=13000000

CREATE TABLE aba_b2c.`log_course_api` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Auto id, PK.',
  `dateRequest` datetime DEFAULT NULL COMMENT 'Date and time of request',
  `ipFromRequest` varchar(17) DEFAULT NULL COMMENT 'IP from the web service call.',
  `appNameFromRequest` varchar(45) DEFAULT NULL COMMENT 'IP or name of the app. e.g.: Intranet, web',
  `urlRequest` varchar(250) DEFAULT NULL COMMENT 'Full URL requested of our Campus WS.',
  `paramsRequest` text COMMENT 'QUERY STRING PARAMS',
  `nameService` varchar(30) DEFAULT NULL COMMENT 'Requested endpoint',
  `dateResponse` datetime DEFAULT NULL COMMENT 'Time of response',
  `paramsResponse` mediumtext COMMENT 'Array of fields and values returned',
  `successResponse` tinyint(1) DEFAULT NULL COMMENT '1 successful or 0 error',
  `errorCode` smallint(5) DEFAULT NULL COMMENT 'Error code, if successResponse is 0',
  PRIMARY KEY (`id`),
  KEY `byDateRequest` (`dateRequest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='To log requests for all calls to Course API.';


CREATE TABLE aba_b2c.`log_web_service` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Auto id, PK.',
  `dateRequest` datetime DEFAULT NULL COMMENT 'Date and time from request',
  `ipFromRequest` varchar(17) DEFAULT NULL COMMENT 'IP from the web service call.',
  `appNameFromRequest` varchar(45) DEFAULT NULL COMMENT 'IP or name of the app. e.g.: Intranet, web',
  `urlRequest` varchar(160) DEFAULT NULL COMMENT 'Full URL requested of our Campus WS.',
  `paramsRequest` text COMMENT 'QUERY STRING PARAMS, XML SOAP',
  `nameService` varchar(30) DEFAULT NULL COMMENT 'registerUser, registerUserLevelTest, listProductsByCountry',
  `dateResponse` datetime DEFAULT NULL COMMENT 'Time of response from Campus',
  `paramsResponse` varchar(500) DEFAULT NULL COMMENT 'Array of fields and values returned',
  `successResponse` tinyint(1) DEFAULT NULL COMMENT '1 successful or 0 error',
  `errorCode` smallint(5) DEFAULT NULL COMMENT 'Error code, if successResponse is 0',
  PRIMARY KEY (`id`),
  KEY `byDateRequest` (`dateRequest`),
  KEY `nameService` (`nameService`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='To log requests for all WS calls.';
--  AUTO_INCREMENT=71000000

-- UPDATE aba_b2c.`config` SET `value` = 'log_web_service.dateRequest#30,log_user_level_change.dateModified#180,log_user_activity.time#30,log_sent_selligent.dateSent#90,pay_gateway_paypal_ipn.dateCreation#365,log_course_api.dateRequest#7' WHERE `key` = 'DELETION_LOGS_DURATION';
--
-- UPDATE aba_b2c.`config` c SET c.value = 'log_web_service.dateRequest#30,log_user_level_change.dateModified#180,log_user_activity.time#30,log_sent_selligent.dateSent#90,log_ipn_paypal.dateCreation#365,log_cron.dateStart#180,log_course_api.dateRequest#7' where c.key = 'DELETION_LOGS_DURATION';
--
-- log_web_service.dateRequest#30,
-- log_user_level_change.dateModified#180,
-- log_user_activity.time#30,
-- log_sent_selligent.dateSent#90,
-- pay_gateway_paypal_ipn.dateCreation#365,
--              log_course_api.dateRequest#7
