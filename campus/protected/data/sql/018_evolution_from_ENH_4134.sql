
/* START  ENH-4134, Consultar renovaciones en el Apple Store */

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_APP_STORE_API_PWD', 'fdb65090c02ed20d19448f4701f0ec14', 'Light security measure to secure communications between Partners devices and Campus.', '2015-01-21 11:11:11');
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_APP_STORE_CONNECT_GATEWAY', '1', 'Enable app store api', '2015-01-21 11:11:11');
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_APP_STORE_ENABLE_GENERAL', '1', 'Enable app store', '2015-01-21 11:11:11');
-- SANDBOX -- INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_APP_STORE_API_URL', 'https://sandbox.itunes.apple.com/verifyReceipt', 'Url sandbox', '2015-01-21 11:11:11');
-- PROD --    INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_APP_STORE_API_URL', 'https://buy.itunes.apple.com/verifyReceipt', 'Url prod', '2015-01-21 11:11:11');
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_APP_STORE_API_VERSION', '1.1', 'api version', '2015-01-21 11:11:11');
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_APP_STORE_ENABLE_RECURRENT', '1', 'Enables the execution of recurring payments.', '2015-01-21 11:11:11');


CREATE TABLE `aba_b2c_logs`.`log_appstore` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'auto Id',
	`idPaymentIos` INT(11) NULL DEFAULT NULL COMMENT 'FK to payments_appstore table.',
	`userId` INT(11) UNSIGNED NOT NULL COMMENT 'FK to users table',
	`operation` VARCHAR(20) NOT NULL COMMENT 'Name of operation...',
	`dateRequest` DATETIME NOT NULL COMMENT 'Request date',
	`dateResponse` DATETIME NULL DEFAULT NULL COMMENT 'Response time date',
	`jsonRequest` MEDIUMTEXT NULL COMMENT 'Content sent to App Store',
	`jsonResponse` MEDIUMTEXT NULL COMMENT 'Response from App Store',
	`errorCode` VARCHAR(12) NULL DEFAULT NULL COMMENT 'In case of error, which error.',
	`success` TINYINT(1) NULL DEFAULT '0' COMMENT 'If 0 is error, otherwise success.',
	`idPaySupplier` INT(3) UNSIGNED NULL DEFAULT NULL COMMENT 'FK to pay_suppliers, to know which gateway supplier is.',
	PRIMARY KEY (`id`),
	INDEX `byDateRequest` (`dateRequest`),
	INDEX `idPaymentIos` (`idPaymentIos`),
	INDEX `userId` (`userId`)
)
COLLATE='utf8_general_ci' ENGINE=InnoDB;

/* END  ENH-4134, Consultar renovaciones en el Apple Store */
