
/* ---- ENH-3625 ------- */

CREATE TABLE `aba_b2c`.`tax_rates` (
	`idTaxRate` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`idCountry` INT(4) UNSIGNED NOT NULL,
	`taxRateValue` DECIMAL(6,2) NULL DEFAULT '0.00',
	`dateStartRate` DATETIME NOT NULL COMMENT 'Time of the start of tax rate',
	`dateEndRate` DATETIME NULL DEFAULT NULL COMMENT 'Time of the end of tax rate',
	PRIMARY KEY (`idTaxRate`),
	UNIQUE INDEX `byCountryDateStart` (`idCountry`, `dateStartRate`),
	INDEX `byCountryDateRange` (`idCountry`, `dateStartRate`, `dateEndRate`)
)
COLLATE='utf8_general_ci' ENGINE=InnoDB ;


INSERT INTO `aba_b2c`.`tax_rates` (`idTaxRate`, `idCountry`, `taxRateValue`, `dateStartRate`, `dateEndRate`) VALUES
	(1, 14, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(2, 21, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(3, 33, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(4, 54, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(5, 56, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(6, 57, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(7, 58, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(8, 67, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(9, 72, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(10, 73, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(11, 80, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(12, 83, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(13, 97, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(14, 103, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(15, 105, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(16, 117, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(17, 123, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(18, 124, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(19, 132, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(20, 150, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(21, 171, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(22, 172, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(23, 176, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(24, 193, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(25, 194, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(26, 199, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(27, 205, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(28, 225, 0.00, '2014-12-01 00:00:00', '2014-12-31 23:59:59'),
	(29, 14, 20.00, '2015-01-01 00:00:00', NULL),
	(30, 21, 21.00, '2015-01-01 00:00:00', NULL),
	(31, 33, 20.00, '2015-01-01 00:00:00', NULL),
	(32, 54, 25.00, '2015-01-01 00:00:00', NULL),
	(33, 56, 19.00, '2015-01-01 00:00:00', NULL),
	(34, 57, 21.00, '2015-01-01 00:00:00', NULL),
	(35, 58, 25.00, '2015-01-01 00:00:00', NULL),
	(36, 67, 20.00, '2015-01-01 00:00:00', NULL),
	(37, 72, 24.00, '2015-01-01 00:00:00', NULL),
	(38, 73, 20.00, '2015-01-01 00:00:00', NULL),
	(39, 80, 19.00, '2015-01-01 00:00:00', NULL),
	(40, 83, 23.00, '2015-01-01 00:00:00', NULL),
	(41, 97, 27.00, '2015-01-01 00:00:00', NULL),
	(42, 103, 23.00, '2015-01-01 00:00:00', NULL),
	(43, 105, 22.00, '2015-01-01 00:00:00', NULL),
	(44, 117, 21.00, '2015-01-01 00:00:00', NULL),
	(45, 123, 21.00, '2015-01-01 00:00:00', NULL),
	(46, 124, 15.00, '2015-01-01 00:00:00', NULL),
	(47, 132, 18.00, '2015-01-01 00:00:00', NULL),
	(48, 150, 21.00, '2015-01-01 00:00:00', NULL),
	(49, 171, 23.00, '2015-01-01 00:00:00', NULL),
	(50, 172, 23.00, '2015-01-01 00:00:00', NULL),
	(51, 176, 24.00, '2015-01-01 00:00:00', NULL),
	(52, 193, 20.00, '2015-01-01 00:00:00', NULL),
	(53, 194, 22.00, '2015-01-01 00:00:00', NULL),
	(54, 199, 21.00, '2015-01-01 00:00:00', NULL),
	(55, 205, 25.00, '2015-01-01 00:00:00', NULL),
	(56, 225, 20.00, '2015-01-01 00:00:00', NULL);

/* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- */

ALTER TABLE `aba_b2c`.`country` ADD COLUMN `invoice` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Is in European Union' AFTER `decimalPoint`;

UPDATE `aba_b2c`.`country` SET invoice = 1 WHERE id IN (14, 21, 33, 54, 56, 57, 58, 67, 72, 73, 80, 83, 97, 103, 105, 117, 123, 124, 132, 150, 171, 172, 176, 193, 194, 199, 205, 225);

/* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- */

ALTER TABLE `aba_b2c`.`payments`
 ADD COLUMN `taxRateValue` DECIMAL(6,2) NULL DEFAULT NULL COMMENT 'Tax rate value (%)' AFTER `cancelOrigin`,
 ADD COLUMN `amountTax` DECIMAL(18,9) NULL DEFAULT NULL COMMENT 'Total tax amount' AFTER `taxRateValue`,
 ADD COLUMN `amountPriceWithoutTax` DECIMAL(18,9) NULL DEFAULT NULL COMMENT 'Total final amount without tax' AFTER `amountTax`;

UPDATE `aba_b2c`.`payments` SET taxRateValue=0, amountTax=0, amountPriceWithoutTax=amountPrice;

/* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- */

ALTER TABLE `aba_b2c`.`user_invoices` ADD COLUMN `prefixInvoice` VARCHAR(3) NULL DEFAULT 'ABA' COMMENT 'Frefix invoice. Country ISO (default ABA)' AFTER `id`;
ALTER TABLE `aba_b2c`.`user_invoices` DROP INDEX `byNumInvoiceId`, ADD UNIQUE INDEX `byPrefixNumInvoiceId` (`prefixInvoice`, `numInvoiceId`);

ALTER TABLE `aba_b2c`.`user_invoices`
 ADD COLUMN `taxRateValue` DECIMAL(6,2) NULL DEFAULT NULL COMMENT 'Tax rate value (%)' AFTER `currencyTrans`,
 ADD COLUMN `amountTax` DECIMAL(18,9) NULL DEFAULT NULL COMMENT 'Total tax amount' AFTER `taxRateValue`,
 ADD COLUMN `amountPriceWithoutTax` DECIMAL(18,9) NULL DEFAULT NULL COMMENT 'Total final amount without tax' AFTER `amountTax`;

ALTER TABLE `aba_b2c`.`user_invoices` ADD COLUMN `usedByUser` TINYINT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'User downloaded invoice' AFTER `amountPriceWithoutTax`;
ALTER TABLE `aba_b2c`.`user_invoices` ADD INDEX `usedByUser` (`usedByUser`);

UPDATE `aba_b2c`.`user_invoices` SET taxRateValue=0, amountTax=0, amountPriceWithoutTax=amountPrice;
UPDATE `aba_b2c`.`user_invoices` SET usedByUser=1;

ALTER TABLE `aba_b2c`.`user_invoices`
 CHANGE COLUMN `userName` `userName` VARCHAR(20) NULL DEFAULT NULL AFTER `telephoneAba`,
 CHANGE COLUMN `userStreet` `userStreet` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Address, street, avenue' AFTER `userLastName`,
 CHANGE COLUMN `userVatNumber` `userVatNumber` VARCHAR(45) NULL DEFAULT NULL AFTER `userStreetMore`;

/* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- */

ALTER TABLE `aba_b2c`.`pay_suppliers` ADD COLUMN `invoice` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Invoice' AFTER `name`;

UPDATE `aba_b2c`.`pay_suppliers` SET `invoice`=1 WHERE  `id`=0;
UPDATE `aba_b2c`.`pay_suppliers` SET `invoice`=1 WHERE  `id`=1;
UPDATE `aba_b2c`.`pay_suppliers` SET `invoice`=1 WHERE  `id`=2;
UPDATE `aba_b2c`.`pay_suppliers` SET `invoice`=1 WHERE  `id`=4;
UPDATE `aba_b2c`.`pay_suppliers` SET `invoice`=1 WHERE  `id`=6;
UPDATE `aba_b2c`.`pay_suppliers` SET `invoice`=1 WHERE  `id`=7;

/* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- */

/* ---- ENH-3625 ------- */

