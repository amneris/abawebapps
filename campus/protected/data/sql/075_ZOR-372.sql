
ALTER TABLE `aba_b2c`.`user_progress_queues` ADD `evaluation` TINYINT(3) UNSIGNED NULL DEFAULT '0'
  COMMENT 'Incluir el relleno de evaluación' AFTER `units`
;

CREATE TABLE `aba_b2c`.`progress_evaluation_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idSectionType` int(11) NOT NULL DEFAULT 8 COMMENT 'Evaluation',
  `idUnit` int(11) NOT NULL DEFAULT 0 COMMENT 'Unit ID',
  `evaluation` varchar(64) NOT NULL DEFAULT '' COMMENT 'User ZUORA',
  PRIMARY KEY (`id`),
  KEY `idSectionType` (`idSectionType`),
  KEY `idUnit` (`idUnit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;

INSERT INTO `aba_b2c`.progress_evaluation_answers (`idSectionType`, `idUnit`, `evaluation`)
SELECT
  se.`idSectionType`, se.`idUnit`, GROUP_CONCAT(ev.`answer`) AS evaluation
FROM aba_course.evaluation AS ev
JOIN aba_course.section AS se
  ON ev.`idSection` = se.`id`
WHERE
  se.`idSectionType` = 8
GROUP BY
  se.`idSectionType`, se.`id`, se.`idUnit`
ORDER BY
  se.`idSectionType`, se.`id`, se.`idUnit`
;

