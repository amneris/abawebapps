

ALTER TABLE aba_b2c.`user` ADD COLUMN `fbId` VARCHAR(32) NULL DEFAULT NULL COMMENT 'Facebook ID' AFTER `lastLoginTime`;
-- ALTER TABLE aba_b2c.`user` ADD UNIQUE INDEX `fbId` (`fbId`);
ALTER TABLE aba_b2c.`user` ADD INDEX `fbId` (`fbId`);

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`) VALUES ('FB_APP_LOGIN_ENABLED', '0', 'Enabled login with facebook');
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`) VALUES ('FB_APP_CAMPUS_LOGIN_APPID', '498604353602800', 'Fb login AppId');
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`) VALUES ('FB_APP_CAMPUS_LOGIN_SECRET', 'd83c4eae2c2a4df971efa245fa69d97b', 'Fb login AppKey');
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`) VALUES ('FB_APP_CAMPUS_LOGIN_SCOPE', 'public_profile,email,offline_access', 'Fb login public profiles');

