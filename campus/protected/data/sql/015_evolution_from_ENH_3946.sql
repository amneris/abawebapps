--Particionado de followup4 y followup_hmtl4

/* ---- ENH-3946 ------- */

--
-- 1. ALTER (Solo para stage, dado que en produccion se subira la base entera).
--
ALTER TABLE `aba_course`.`phrase` ADD COLUMN `posAudio` INT NOT NULL COMMENT 'This is the place inside the page in section writing for paint colors.' AFTER `text`;

--
-- 2. Volkar la DB aba_course
--

--
-- 3. Borrar duplicados
--

--  Borrado de duplicados en followup4

CREATE TEMPORARY TABLE `aba_b2c`.`FU4Salvados` (SELECT followup4.id, followup4.themeid, followup4.userid
FROM followup4 INNER JOIN (SELECT themeid, userid FROM aba_b2c.followup4 GROUP BY themeid, userid HAVING count(*)>1)  AS duplicados ON (followup4.themeid = duplicados.themeid) AND (followup4.userid = duplicados.userid)
GROUP BY followup4.themeid, followup4.userid);

CREATE TEMPORARY TABLE `aba_b2c`.`FU4TodosRepetidos` (SELECT followup4.id, followup4.themeid, followup4.userid
FROM `aba_b2c`.`followup4` INNER JOIN `aba_b2c`.`FU4Salvados` ON (`aba_b2c`.`followup4`.themeid = `aba_b2c`.`FU4Salvados`.themeid) AND (`aba_b2c`.`followup4`.userid = `aba_b2c`.`FU4Salvados`.userid)) ;

CREATE TEMPORARY TABLE `aba_b2c`.`FU4ABorrar`(SELECT `aba_b2c`.`FU4TodosRepetidos`.id
FROM `aba_b2c`.`FU4TodosRepetidos` LEFT JOIN `aba_b2c`.`FU4Salvados` ON `aba_b2c`.`FU4TodosRepetidos`.id = `aba_b2c`.`FU4Salvados`.id
WHERE `aba_b2c`.`FU4Salvados`.id Is Null);

delete `aba_b2c`.`followup4`.* from `aba_b2c`.`followup4` inner join `aba_b2c`.`FU4ABorrar` on `aba_b2c`.`followup4`.id = `aba_b2c`.`FU4ABorrar`.id;

DROP TEMPORARY TABLE IF EXISTS `aba_b2c`.`FU4TodosRepetidos`;
DROP TEMPORARY TABLE IF EXISTS `aba_b2c`.`FU4Salvados`;
DROP TEMPORARY TABLE IF EXISTS `aba_b2c`.`FU4ABorrar`;

--
-- 4. Cambiamos el índice a único, ¡ SOLO PUEDE QUEDAR 1 !.
--
ALTER TABLE `aba_b2c`.`followup4`
	DROP INDEX `learner_theme_index`,
	ADD UNIQUE INDEX `learner_theme_index` (`userid`, `themeid`) USING BTREE;

--
-- 5. Creamos índice con el campo lastchange para crear la summarize
--
ALTER TABLE `aba_b2c`.`followup4`
	ADD INDEX `lastchange` (`lastchange`);

--
-- 6. Borramos incongruencias, ThemeId>144.
--
delete from `aba_b2c`.`followup4` where themeid>144;
-- Donde el usuario no exista en user.
CREATE TEMPORARY TABLE `aba_b2c`.`FU4ABorrarHuerfanos`( SELECT t1.id from followup4 t1 left join user t2 on t1.userid = t2.id where t2.id is null);
Delete t1.* From `aba_b2c`.`followup4` t1 inner join `aba_b2c`.`FU4ABorrarHuerfanos` t2 on t1.id=t2.id;
DROP TEMPORARY TABLE IF EXISTS `aba_b2c`.`FU4ABorrarHuerfanos`;

---
--- 7. Creamos tablas Para la partición
---

CREATE TABLE `followup4_1` (
	`id` INT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
	`themeid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`userid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`lastchange` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`all_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total percentage from the unit made.',
	`all_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total wrong answers.',
	`all_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total help requests.',
	`all_time` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total time dedicated to the unit',
	`sit_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Aba Film (SPEAKING), Situation video percentage watched, 0, 50 or 100. New Users (0 or100)',
	`stu_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Speak (STUDY-HABLA), percentage.',
	`dic_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), percentage.',
	`dic_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), help requests.',
	`dic_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), wrong answers.',
	`rol_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Interpret (ROLEPLAY-INTERPRETA), percentage.',
	`gra_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. Deprecated',
	`gra_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), help requests. Deprecated',
	`gra_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), wrong answers. Deprecated',
	`wri_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), percentage.',
	`wri_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), help requests.',
	`wri_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), wrong answers.',
	`new_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Vocabulary (NEWWORDS,VOCABULARIO), percentage.',
	`spe_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`spe_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`time_aux` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`gra_vid` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. (0 or 100), If user has watched video class.',
	`rol_on` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated',
	`exercises` VARCHAR(25) NOT NULL DEFAULT '0,0,0,0,0,0,0,0,0,0' COMMENT 'Responses of the 10 exercises',
	`eva_por` INT(4) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Section Evaluation (MINITEST, TEST DE NIVEL), 1 to 10 (successful exercises)',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `learner_theme_index` (`userid`, `themeid`) USING BTREE,
	INDEX `lastchange` (`lastchange`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `followup4_2` (
	`id` INT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
	`themeid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`userid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`lastchange` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`all_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total percentage from the unit made.',
	`all_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total wrong answers.',
	`all_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total help requests.',
	`all_time` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total time dedicated to the unit',
	`sit_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Aba Film (SPEAKING), Situation video percentage watched, 0, 50 or 100. New Users (0 or100)',
	`stu_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Speak (STUDY-HABLA), percentage.',
	`dic_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), percentage.',
	`dic_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), help requests.',
	`dic_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), wrong answers.',
	`rol_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Interpret (ROLEPLAY-INTERPRETA), percentage.',
	`gra_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. Deprecated',
	`gra_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), help requests. Deprecated',
	`gra_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), wrong answers. Deprecated',
	`wri_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), percentage.',
	`wri_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), help requests.',
	`wri_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), wrong answers.',
	`new_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Vocabulary (NEWWORDS,VOCABULARIO), percentage.',
	`spe_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`spe_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`time_aux` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`gra_vid` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. (0 or 100), If user has watched video class.',
	`rol_on` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated',
	`exercises` VARCHAR(25) NOT NULL DEFAULT '0,0,0,0,0,0,0,0,0,0' COMMENT 'Responses of the 10 exercises',
	`eva_por` INT(4) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Section Evaluation (MINITEST, TEST DE NIVEL), 1 to 10 (successful exercises)',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `learner_theme_index` (`userid`, `themeid`) USING BTREE,
	INDEX `lastchange` (`lastchange`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `followup4_3` (
	`id` INT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
	`themeid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`userid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`lastchange` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`all_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total percentage from the unit made.',
	`all_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total wrong answers.',
	`all_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total help requests.',
	`all_time` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total time dedicated to the unit',
	`sit_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Aba Film (SPEAKING), Situation video percentage watched, 0, 50 or 100. New Users (0 or100)',
	`stu_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Speak (STUDY-HABLA), percentage.',
	`dic_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), percentage.',
	`dic_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), help requests.',
	`dic_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), wrong answers.',
	`rol_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Interpret (ROLEPLAY-INTERPRETA), percentage.',
	`gra_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. Deprecated',
	`gra_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), help requests. Deprecated',
	`gra_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), wrong answers. Deprecated',
	`wri_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), percentage.',
	`wri_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), help requests.',
	`wri_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), wrong answers.',
	`new_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Vocabulary (NEWWORDS,VOCABULARIO), percentage.',
	`spe_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`spe_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`time_aux` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`gra_vid` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. (0 or 100), If user has watched video class.',
	`rol_on` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated',
	`exercises` VARCHAR(25) NOT NULL DEFAULT '0,0,0,0,0,0,0,0,0,0' COMMENT 'Responses of the 10 exercises',
	`eva_por` INT(4) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Section Evaluation (MINITEST, TEST DE NIVEL), 1 to 10 (successful exercises)',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `learner_theme_index` (`userid`, `themeid`) USING BTREE,
	INDEX `lastchange` (`lastchange`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `followup4_4` (
	`id` INT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
	`themeid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`userid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`lastchange` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`all_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total percentage from the unit made.',
	`all_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total wrong answers.',
	`all_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total help requests.',
	`all_time` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total time dedicated to the unit',
	`sit_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Aba Film (SPEAKING), Situation video percentage watched, 0, 50 or 100. New Users (0 or100)',
	`stu_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Speak (STUDY-HABLA), percentage.',
	`dic_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), percentage.',
	`dic_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), help requests.',
	`dic_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), wrong answers.',
	`rol_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Interpret (ROLEPLAY-INTERPRETA), percentage.',
	`gra_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. Deprecated',
	`gra_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), help requests. Deprecated',
	`gra_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), wrong answers. Deprecated',
	`wri_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), percentage.',
	`wri_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), help requests.',
	`wri_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), wrong answers.',
	`new_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Vocabulary (NEWWORDS,VOCABULARIO), percentage.',
	`spe_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`spe_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`time_aux` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`gra_vid` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. (0 or 100), If user has watched video class.',
	`rol_on` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated',
	`exercises` VARCHAR(25) NOT NULL DEFAULT '0,0,0,0,0,0,0,0,0,0' COMMENT 'Responses of the 10 exercises',
	`eva_por` INT(4) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Section Evaluation (MINITEST, TEST DE NIVEL), 1 to 10 (successful exercises)',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `learner_theme_index` (`userid`, `themeid`) USING BTREE,
	INDEX `lastchange` (`lastchange`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `followup4_5` (
	`id` INT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
	`themeid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`userid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`lastchange` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`all_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total percentage from the unit made.',
	`all_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total wrong answers.',
	`all_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total help requests.',
	`all_time` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total time dedicated to the unit',
	`sit_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Aba Film (SPEAKING), Situation video percentage watched, 0, 50 or 100. New Users (0 or100)',
	`stu_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Speak (STUDY-HABLA), percentage.',
	`dic_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), percentage.',
	`dic_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), help requests.',
	`dic_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), wrong answers.',
	`rol_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Interpret (ROLEPLAY-INTERPRETA), percentage.',
	`gra_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. Deprecated',
	`gra_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), help requests. Deprecated',
	`gra_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), wrong answers. Deprecated',
	`wri_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), percentage.',
	`wri_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), help requests.',
	`wri_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), wrong answers.',
	`new_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Vocabulary (NEWWORDS,VOCABULARIO), percentage.',
	`spe_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`spe_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`time_aux` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`gra_vid` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. (0 or 100), If user has watched video class.',
	`rol_on` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated',
	`exercises` VARCHAR(25) NOT NULL DEFAULT '0,0,0,0,0,0,0,0,0,0' COMMENT 'Responses of the 10 exercises',
	`eva_por` INT(4) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Section Evaluation (MINITEST, TEST DE NIVEL), 1 to 10 (successful exercises)',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `learner_theme_index` (`userid`, `themeid`) USING BTREE,
	INDEX `lastchange` (`lastchange`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `followup4_30` (
	`id` INT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
	`themeid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`userid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`lastchange` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`all_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total percentage from the unit made.',
	`all_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total wrong answers.',
	`all_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total help requests.',
	`all_time` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total time dedicated to the unit',
	`sit_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Aba Film (SPEAKING), Situation video percentage watched, 0, 50 or 100. New Users (0 or100)',
	`stu_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Speak (STUDY-HABLA), percentage.',
	`dic_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), percentage.',
	`dic_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), help requests.',
	`dic_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), wrong answers.',
	`rol_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Interpret (ROLEPLAY-INTERPRETA), percentage.',
	`gra_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. Deprecated',
	`gra_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), help requests. Deprecated',
	`gra_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), wrong answers. Deprecated',
	`wri_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), percentage.',
	`wri_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), help requests.',
	`wri_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), wrong answers.',
	`new_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Vocabulary (NEWWORDS,VOCABULARIO), percentage.',
	`spe_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`spe_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`time_aux` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`gra_vid` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. (0 or 100), If user has watched video class.',
	`rol_on` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated',
	`exercises` VARCHAR(25) NOT NULL DEFAULT '0,0,0,0,0,0,0,0,0,0' COMMENT 'Responses of the 10 exercises',
	`eva_por` INT(4) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Section Evaluation (MINITEST, TEST DE NIVEL), 1 to 10 (successful exercises)',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `learner_theme_index` (`userid`, `themeid`) USING BTREE,
	INDEX `lastchange` (`lastchange`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `followup4_31` (
	`id` INT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
	`themeid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`userid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`lastchange` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`all_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total percentage from the unit made.',
	`all_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total wrong answers.',
	`all_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total help requests.',
	`all_time` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total time dedicated to the unit',
	`sit_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Aba Film (SPEAKING), Situation video percentage watched, 0, 50 or 100. New Users (0 or100)',
	`stu_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Speak (STUDY-HABLA), percentage.',
	`dic_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), percentage.',
	`dic_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), help requests.',
	`dic_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), wrong answers.',
	`rol_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Interpret (ROLEPLAY-INTERPRETA), percentage.',
	`gra_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. Deprecated',
	`gra_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), help requests. Deprecated',
	`gra_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), wrong answers. Deprecated',
	`wri_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), percentage.',
	`wri_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), help requests.',
	`wri_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), wrong answers.',
	`new_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Vocabulary (NEWWORDS,VOCABULARIO), percentage.',
	`spe_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`spe_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`time_aux` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`gra_vid` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. (0 or 100), If user has watched video class.',
	`rol_on` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated',
	`exercises` VARCHAR(25) NOT NULL DEFAULT '0,0,0,0,0,0,0,0,0,0' COMMENT 'Responses of the 10 exercises',
	`eva_por` INT(4) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Section Evaluation (MINITEST, TEST DE NIVEL), 1 to 10 (successful exercises)',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `learner_theme_index` (`userid`, `themeid`) USING BTREE,
	INDEX `lastchange` (`lastchange`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `followup4_32` (
	`id` INT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
	`themeid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`userid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`lastchange` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`all_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total percentage from the unit made.',
	`all_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total wrong answers.',
	`all_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total help requests.',
	`all_time` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total time dedicated to the unit',
	`sit_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Aba Film (SPEAKING), Situation video percentage watched, 0, 50 or 100. New Users (0 or100)',
	`stu_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Speak (STUDY-HABLA), percentage.',
	`dic_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), percentage.',
	`dic_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), help requests.',
	`dic_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), wrong answers.',
	`rol_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Interpret (ROLEPLAY-INTERPRETA), percentage.',
	`gra_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. Deprecated',
	`gra_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), help requests. Deprecated',
	`gra_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), wrong answers. Deprecated',
	`wri_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), percentage.',
	`wri_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), help requests.',
	`wri_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), wrong answers.',
	`new_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Vocabulary (NEWWORDS,VOCABULARIO), percentage.',
	`spe_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`spe_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`time_aux` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`gra_vid` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. (0 or 100), If user has watched video class.',
	`rol_on` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated',
	`exercises` VARCHAR(25) NOT NULL DEFAULT '0,0,0,0,0,0,0,0,0,0' COMMENT 'Responses of the 10 exercises',
	`eva_por` INT(4) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Section Evaluation (MINITEST, TEST DE NIVEL), 1 to 10 (successful exercises)',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `learner_theme_index` (`userid`, `themeid`) USING BTREE,
	INDEX `lastchange` (`lastchange`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `followup4_33` (
	`id` INT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
	`themeid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`userid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`lastchange` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`all_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total percentage from the unit made.',
	`all_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total wrong answers.',
	`all_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total help requests.',
	`all_time` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total time dedicated to the unit',
	`sit_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Aba Film (SPEAKING), Situation video percentage watched, 0, 50 or 100. New Users (0 or100)',
	`stu_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Speak (STUDY-HABLA), percentage.',
	`dic_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), percentage.',
	`dic_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), help requests.',
	`dic_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), wrong answers.',
	`rol_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Interpret (ROLEPLAY-INTERPRETA), percentage.',
	`gra_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. Deprecated',
	`gra_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), help requests. Deprecated',
	`gra_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), wrong answers. Deprecated',
	`wri_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), percentage.',
	`wri_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), help requests.',
	`wri_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), wrong answers.',
	`new_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Vocabulary (NEWWORDS,VOCABULARIO), percentage.',
	`spe_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`spe_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`time_aux` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`gra_vid` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. (0 or 100), If user has watched video class.',
	`rol_on` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated',
	`exercises` VARCHAR(25) NOT NULL DEFAULT '0,0,0,0,0,0,0,0,0,0' COMMENT 'Responses of the 10 exercises',
	`eva_por` INT(4) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Section Evaluation (MINITEST, TEST DE NIVEL), 1 to 10 (successful exercises)',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `learner_theme_index` (`userid`, `themeid`) USING BTREE,
	INDEX `lastchange` (`lastchange`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `followup4_34` (
	`id` INT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
	`themeid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`userid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`lastchange` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`all_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total percentage from the unit made.',
	`all_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total wrong answers.',
	`all_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total help requests.',
	`all_time` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total time dedicated to the unit',
	`sit_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Aba Film (SPEAKING), Situation video percentage watched, 0, 50 or 100. New Users (0 or100)',
	`stu_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Speak (STUDY-HABLA), percentage.',
	`dic_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), percentage.',
	`dic_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), help requests.',
	`dic_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), wrong answers.',
	`rol_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Interpret (ROLEPLAY-INTERPRETA), percentage.',
	`gra_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. Deprecated',
	`gra_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), help requests. Deprecated',
	`gra_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), wrong answers. Deprecated',
	`wri_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), percentage.',
	`wri_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), help requests.',
	`wri_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), wrong answers.',
	`new_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Vocabulary (NEWWORDS,VOCABULARIO), percentage.',
	`spe_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`spe_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`time_aux` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`gra_vid` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. (0 or 100), If user has watched video class.',
	`rol_on` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated',
	`exercises` VARCHAR(25) NOT NULL DEFAULT '0,0,0,0,0,0,0,0,0,0' COMMENT 'Responses of the 10 exercises',
	`eva_por` INT(4) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Section Evaluation (MINITEST, TEST DE NIVEL), 1 to 10 (successful exercises)',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `learner_theme_index` (`userid`, `themeid`) USING BTREE,
	INDEX `lastchange` (`lastchange`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
CREATE TABLE `followup4_35` (
	`id` INT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
	`themeid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`userid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
	`lastchange` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`all_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total percentage from the unit made.',
	`all_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total wrong answers.',
	`all_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total help requests.',
	`all_time` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total time dedicated to the unit',
	`sit_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Aba Film (SPEAKING), Situation video percentage watched, 0, 50 or 100. New Users (0 or100)',
	`stu_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Speak (STUDY-HABLA), percentage.',
	`dic_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), percentage.',
	`dic_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), help requests.',
	`dic_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write (DICTATION-ESCRIBE), wrong answers.',
	`rol_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Interpret (ROLEPLAY-INTERPRETA), percentage.',
	`gra_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. Deprecated',
	`gra_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), help requests. Deprecated',
	`gra_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), wrong answers. Deprecated',
	`wri_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), percentage.',
	`wri_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), help requests.',
	`wri_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises (WRITING, EJERCICIOS), wrong answers.',
	`new_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Vocabulary (NEWWORDS,VOCABULARIO), percentage.',
	`spe_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`spe_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`time_aux` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section',
	`gra_vid` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Video Class (GRAMMAR,VIDEO CLASE), percentage. (0 or 100), If user has watched video class.',
	`rol_on` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated',
	`exercises` VARCHAR(25) NOT NULL DEFAULT '0,0,0,0,0,0,0,0,0,0' COMMENT 'Responses of the 10 exercises',
	`eva_por` INT(4) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Section Evaluation (MINITEST, TEST DE NIVEL), 1 to 10 (successful exercises)',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `learner_theme_index` (`userid`, `themeid`) USING BTREE,
	INDEX `lastchange` (`lastchange`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

--
-- Creamos las followup_html4
--

CREATE TABLE `followup_html4_1` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`userid` INT(10) UNSIGNED NOT NULL,
	`themeid` INT(10) UNSIGNED NOT NULL,
	`page` INT(10) UNSIGNED NOT NULL,
	`section` VARCHAR(20) NOT NULL,
	`action` VARCHAR(20) NOT NULL,
	`audio` VARCHAR(20) NOT NULL,
	`bien` TINYINT(1) NOT NULL,
	`mal` INT(10) UNSIGNED NOT NULL,
	`lastchange` DATETIME NOT NULL,
	`wtext` VARCHAR(100) NOT NULL,
	`difficulty` INT(10) UNSIGNED NOT NULL,
	`time_aux` INT(4) UNSIGNED NOT NULL,
	`contador` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `IDs_lastchange_index` (`userid`, `themeid`, `lastchange`) USING BTREE,
	INDEX `INX1` (`userid`, `themeid`, `section`, `audio`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
CREATE TABLE `followup_html4_2` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`userid` INT(10) UNSIGNED NOT NULL,
	`themeid` INT(10) UNSIGNED NOT NULL,
	`page` INT(10) UNSIGNED NOT NULL,
	`section` VARCHAR(20) NOT NULL,
	`action` VARCHAR(20) NOT NULL,
	`audio` VARCHAR(20) NOT NULL,
	`bien` TINYINT(1) NOT NULL,
	`mal` INT(10) UNSIGNED NOT NULL,
	`lastchange` DATETIME NOT NULL,
	`wtext` VARCHAR(100) NOT NULL,
	`difficulty` INT(10) UNSIGNED NOT NULL,
	`time_aux` INT(4) UNSIGNED NOT NULL,
	`contador` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `IDs_lastchange_index` (`userid`, `themeid`, `lastchange`) USING BTREE,
	INDEX `INX1` (`userid`, `themeid`, `section`, `audio`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
CREATE TABLE `followup_html4_3` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`userid` INT(10) UNSIGNED NOT NULL,
	`themeid` INT(10) UNSIGNED NOT NULL,
	`page` INT(10) UNSIGNED NOT NULL,
	`section` VARCHAR(20) NOT NULL,
	`action` VARCHAR(20) NOT NULL,
	`audio` VARCHAR(20) NOT NULL,
	`bien` TINYINT(1) NOT NULL,
	`mal` INT(10) UNSIGNED NOT NULL,
	`lastchange` DATETIME NOT NULL,
	`wtext` VARCHAR(100) NOT NULL,
	`difficulty` INT(10) UNSIGNED NOT NULL,
	`time_aux` INT(4) UNSIGNED NOT NULL,
	`contador` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `IDs_lastchange_index` (`userid`, `themeid`, `lastchange`) USING BTREE,
	INDEX `INX1` (`userid`, `themeid`, `section`, `audio`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
CREATE TABLE `followup_html4_4` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`userid` INT(10) UNSIGNED NOT NULL,
	`themeid` INT(10) UNSIGNED NOT NULL,
	`page` INT(10) UNSIGNED NOT NULL,
	`section` VARCHAR(20) NOT NULL,
	`action` VARCHAR(20) NOT NULL,
	`audio` VARCHAR(20) NOT NULL,
	`bien` TINYINT(1) NOT NULL,
	`mal` INT(10) UNSIGNED NOT NULL,
	`lastchange` DATETIME NOT NULL,
	`wtext` VARCHAR(100) NOT NULL,
	`difficulty` INT(10) UNSIGNED NOT NULL,
	`time_aux` INT(4) UNSIGNED NOT NULL,
	`contador` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `IDs_lastchange_index` (`userid`, `themeid`, `lastchange`) USING BTREE,
	INDEX `INX1` (`userid`, `themeid`, `section`, `audio`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
CREATE TABLE `followup_html4_5` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`userid` INT(10) UNSIGNED NOT NULL,
	`themeid` INT(10) UNSIGNED NOT NULL,
	`page` INT(10) UNSIGNED NOT NULL,
	`section` VARCHAR(20) NOT NULL,
	`action` VARCHAR(20) NOT NULL,
	`audio` VARCHAR(20) NOT NULL,
	`bien` TINYINT(1) NOT NULL,
	`mal` INT(10) UNSIGNED NOT NULL,
	`lastchange` DATETIME NOT NULL,
	`wtext` VARCHAR(100) NOT NULL,
	`difficulty` INT(10) UNSIGNED NOT NULL,
	`time_aux` INT(4) UNSIGNED NOT NULL,
	`contador` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `IDs_lastchange_index` (`userid`, `themeid`, `lastchange`) USING BTREE,
	INDEX `INX1` (`userid`, `themeid`, `section`, `audio`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
CREATE TABLE `followup_html4_30` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`userid` INT(10) UNSIGNED NOT NULL,
	`themeid` INT(10) UNSIGNED NOT NULL,
	`page` INT(10) UNSIGNED NOT NULL,
	`section` VARCHAR(20) NOT NULL,
	`action` VARCHAR(20) NOT NULL,
	`audio` VARCHAR(20) NOT NULL,
	`bien` TINYINT(1) NOT NULL,
	`mal` INT(10) UNSIGNED NOT NULL,
	`lastchange` DATETIME NOT NULL,
	`wtext` VARCHAR(100) NOT NULL,
	`difficulty` INT(10) UNSIGNED NOT NULL,
	`time_aux` INT(4) UNSIGNED NOT NULL,
	`contador` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `IDs_lastchange_index` (`userid`, `themeid`, `lastchange`) USING BTREE,
	INDEX `INX1` (`userid`, `themeid`, `section`, `audio`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
CREATE TABLE `followup_html4_31` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`userid` INT(10) UNSIGNED NOT NULL,
	`themeid` INT(10) UNSIGNED NOT NULL,
	`page` INT(10) UNSIGNED NOT NULL,
	`section` VARCHAR(20) NOT NULL,
	`action` VARCHAR(20) NOT NULL,
	`audio` VARCHAR(20) NOT NULL,
	`bien` TINYINT(1) NOT NULL,
	`mal` INT(10) UNSIGNED NOT NULL,
	`lastchange` DATETIME NOT NULL,
	`wtext` VARCHAR(100) NOT NULL,
	`difficulty` INT(10) UNSIGNED NOT NULL,
	`time_aux` INT(4) UNSIGNED NOT NULL,
	`contador` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `IDs_lastchange_index` (`userid`, `themeid`, `lastchange`) USING BTREE,
	INDEX `INX1` (`userid`, `themeid`, `section`, `audio`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
CREATE TABLE `followup_html4_32` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`userid` INT(10) UNSIGNED NOT NULL,
	`themeid` INT(10) UNSIGNED NOT NULL,
	`page` INT(10) UNSIGNED NOT NULL,
	`section` VARCHAR(20) NOT NULL,
	`action` VARCHAR(20) NOT NULL,
	`audio` VARCHAR(20) NOT NULL,
	`bien` TINYINT(1) NOT NULL,
	`mal` INT(10) UNSIGNED NOT NULL,
	`lastchange` DATETIME NOT NULL,
	`wtext` VARCHAR(100) NOT NULL,
	`difficulty` INT(10) UNSIGNED NOT NULL,
	`time_aux` INT(4) UNSIGNED NOT NULL,
	`contador` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `IDs_lastchange_index` (`userid`, `themeid`, `lastchange`) USING BTREE,
	INDEX `INX1` (`userid`, `themeid`, `section`, `audio`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
CREATE TABLE `followup_html4_33` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`userid` INT(10) UNSIGNED NOT NULL,
	`themeid` INT(10) UNSIGNED NOT NULL,
	`page` INT(10) UNSIGNED NOT NULL,
	`section` VARCHAR(20) NOT NULL,
	`action` VARCHAR(20) NOT NULL,
	`audio` VARCHAR(20) NOT NULL,
	`bien` TINYINT(1) NOT NULL,
	`mal` INT(10) UNSIGNED NOT NULL,
	`lastchange` DATETIME NOT NULL,
	`wtext` VARCHAR(100) NOT NULL,
	`difficulty` INT(10) UNSIGNED NOT NULL,
	`time_aux` INT(4) UNSIGNED NOT NULL,
	`contador` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `IDs_lastchange_index` (`userid`, `themeid`, `lastchange`) USING BTREE,
	INDEX `INX1` (`userid`, `themeid`, `section`, `audio`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
CREATE TABLE `followup_html4_34` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`userid` INT(10) UNSIGNED NOT NULL,
	`themeid` INT(10) UNSIGNED NOT NULL,
	`page` INT(10) UNSIGNED NOT NULL,
	`section` VARCHAR(20) NOT NULL,
	`action` VARCHAR(20) NOT NULL,
	`audio` VARCHAR(20) NOT NULL,
	`bien` TINYINT(1) NOT NULL,
	`mal` INT(10) UNSIGNED NOT NULL,
	`lastchange` DATETIME NOT NULL,
	`wtext` VARCHAR(100) NOT NULL,
	`difficulty` INT(10) UNSIGNED NOT NULL,
	`time_aux` INT(4) UNSIGNED NOT NULL,
	`contador` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `IDs_lastchange_index` (`userid`, `themeid`, `lastchange`) USING BTREE,
	INDEX `INX1` (`userid`, `themeid`, `section`, `audio`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
CREATE TABLE `followup_html4_35` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`userid` INT(10) UNSIGNED NOT NULL,
	`themeid` INT(10) UNSIGNED NOT NULL,
	`page` INT(10) UNSIGNED NOT NULL,
	`section` VARCHAR(20) NOT NULL,
	`action` VARCHAR(20) NOT NULL,
	`audio` VARCHAR(20) NOT NULL,
	`bien` TINYINT(1) NOT NULL,
	`mal` INT(10) UNSIGNED NOT NULL,
	`lastchange` DATETIME NOT NULL,
	`wtext` VARCHAR(100) NOT NULL,
	`difficulty` INT(10) UNSIGNED NOT NULL,
	`time_aux` INT(4) UNSIGNED NOT NULL,
	`contador` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `IDs_lastchange_index` (`userid`, `themeid`, `lastchange`) USING BTREE,
	INDEX `INX1` (`userid`, `themeid`, `section`, `audio`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
