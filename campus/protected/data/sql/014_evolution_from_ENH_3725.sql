--Progreso

/* Aquí consolidamos el progreso aplicando las reglas antiguas solo una vez. */

update `aba_b2c`.followup4 set all_ans= all_ans-spe_ans where all_ans>0;

update `aba_b2c`.`followup4`
SET sit_por=IF(sit_por>0 and sit_por<=50,50, IF(sit_por > 50,100, 0)),
    stu_por=IF(stu_por*2>100,100,stu_por*2),
    wri_por=IF(dic_por>100,100,dic_por),
    rol_por=IF(rol_por>100,100,rol_por),
    gra_vid=IF(gra_vid>0,100,0),
    wri_por=IF(wri_por>100,100,wri_por),
    new_por=IF(new_por*2>100,100,new_por*2);

-- Recalculo del all_por sin contar el examen como el transaction.
Update followup4 set all_por= floor((sit_por+stu_por+dic_por+rol_por+gra_vid+wri_por+new_por)/7) where all_por<>floor((sit_por+stu_por+dic_por+rol_por+gra_vid+wri_por+new_por)/7);