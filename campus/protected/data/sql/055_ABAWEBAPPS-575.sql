
CREATE TABLE `aba_b2c`.`pay_gateway_allpago` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto Id',
  `idPayment` varchar(15) DEFAULT NULL COMMENT 'FK to payments table.',
  `userId` int(4) unsigned NOT NULL COMMENT 'FK to users table',
  `operation` varchar(20) NOT NULL COMMENT 'Name of operation, Credit, Debit, registration, etc.',
  `dateRequest` datetime NOT NULL COMMENT 'Request date',
  `dateResponse` datetime DEFAULT NULL COMMENT 'Response time date',
  `xmlRequest` text NOT NULL COMMENT 'Content sent to AllPago',
  `xmlResponse` text COMMENT 'Content sent to AllPago',
  `errorCode` varchar(12) DEFAULT NULL COMMENT 'In case of error, which error. Straight from AllPago codes.',
  `success` tinyint(1) DEFAULT '0' COMMENT 'If 0 is error, otherwise success.',
  `idPaySupplier` int(3) unsigned DEFAULT NULL COMMENT 'FK to pay_suppliers, to know which gateway supplier is.',
  PRIMARY KEY (`id`),
  KEY `byDateRequest` (`dateRequest`),
  KEY `byIdPayment` (`idPayment`),
  KEY `byUserId` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=70000 DEFAULT CHARSET=utf8;

CREATE TABLE `aba_b2c`.`pay_gateway_appstore` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto Id',
  `idPaymentIos` int(11) DEFAULT NULL COMMENT 'FK to payments_appstore table.',
  `userId` int(11) unsigned NOT NULL COMMENT 'FK to users table',
  `operation` varchar(20) NOT NULL COMMENT 'Name of operation...',
  `dateRequest` datetime NOT NULL COMMENT 'Request date',
  `dateResponse` datetime DEFAULT NULL COMMENT 'Response time date',
  `jsonRequest` mediumtext COMMENT 'Content sent to App Store',
  `jsonResponse` mediumtext COMMENT 'Response from App Store',
  `errorCode` varchar(12) DEFAULT NULL COMMENT 'In case of error, which error.',
  `success` tinyint(1) DEFAULT '0' COMMENT 'If 0 is error, otherwise success.',
  `idPaySupplier` int(3) unsigned DEFAULT NULL COMMENT 'FK to pay_suppliers, to know which gateway supplier is.',
  PRIMARY KEY (`id`),
  KEY `byDateRequest` (`dateRequest`),
  KEY `byIdPaymentIos` (`idPaymentIos`),
  KEY `byUserId` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8;

CREATE TABLE `aba_b2c`.`pay_gateway_paypal_ipn` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dateCreation` datetime NOT NULL,
  `header` text NOT NULL,
  `postFields` longtext,
  `transactionType` varchar(65) NOT NULL,
  `paymentStatus` varchar(45) NOT NULL,
  `userId` int(11) unsigned DEFAULT NULL COMMENT 'FK to table Users. Only since May-2014',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100000 DEFAULT CHARSET=utf8 COMMENT='To save every request we receive from IPN';

CREATE TABLE `aba_b2c`.`pay_gateway_paypal_express_co` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'auto Id',
  `userId` int(4) unsigned NOT NULL COMMENT 'FK to users table',
  `operation` varchar(45) NOT NULL COMMENT 'Name of operation, getExpress, DoExpress ,etc.',
  `dateRequest` datetime NOT NULL COMMENT 'Request date',
  `xmlRequest` text NOT NULL COMMENT 'Content sent to PayPal',
  `jsonResponse` text DEFAULT NULL COMMENT 'Reply from PayPal in json format.',
  `errorCode` varchar(6) DEFAULT NULL COMMENT 'In case of error, which error. Straight from paypal.',
  `success` tinyint(1) DEFAULT '0' COMMENT 'If 0 is error, otherwise success.',
  `idPayment` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `idPayment` (`idPayment`)
) ENGINE=InnoDB AUTO_INCREMENT=150000 DEFAULT CHARSET=utf8;

CREATE TABLE `aba_b2c`.`log_reconciliation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Auto id increment',
  `filePath` varchar(150) DEFAULT NULL COMMENT 'Name and Full filepath to the XML or CSV file.',
  `fileOrigin` varchar(40) DEFAULT NULL COMMENT 'IntranetCaixa, CampusAllPago, etc.',
  `lineNode` int(10) unsigned DEFAULT NULL COMMENT 'Line from CSV or node in the XML.',
  `paySuppExtId` int(2) unsigned NOT NULL COMMENT 'Which gateway payment the log belongs to.',
  `dateOperation` datetime NOT NULL COMMENT 'Date of the operation',
  `idPayment` varchar(15) DEFAULT NULL COMMENT 'FK to payments.',
  `refPaySuppOrderId` varchar(80) DEFAULT NULL COMMENT 'Id present in the XML file of reconciliation.',
  `refPaySuppStatus` int(10) unsigned DEFAULT NULL COMMENT 'Status in the reference file.',
  `refAmountPrice` varchar(20) DEFAULT NULL COMMENT 'Amount Prices in the reference file.',
  `refDateEndTransaction` datetime DEFAULT NULL COMMENT 'Date of transaction in the reference file.',
  `refCurrencyTrans` varchar(3) DEFAULT NULL COMMENT 'Currency code iso expected in the reference file.',
  `refPaySuppExtUniId` varchar(50) DEFAULT NULL COMMENT 'Internal id for the gateway.',
  `success` tinyint(1) DEFAULT NULL COMMENT 'If it was matched or not.',
  `matchType` varchar(20) DEFAULT NULL COMMENT 'MATCHED, NOT MATCHED, INCONSISTENT, ERROR, NOT FOUND.',
  PRIMARY KEY (`id`),
  KEY `byPaySuppExtUniId` (`refPaySuppExtUniId`),
  KEY `byPaySuppOrderId` (`refPaySuppOrderId`)
) ENGINE=InnoDB AUTO_INCREMENT=100000 DEFAULT CHARSET=utf8 COMMENT='To write down the reconciliation operations.';


UPDATE `config` SET `value` = 'log_web_service.dateRequest#30,log_user_level_change.dateModified#180,log_user_activity.time#30,log_sent_selligent.dateSent#90,pay_gateway_paypal_ipn.dateCreation#365,log_course_api.dateRequest#7' WHERE `key` = 'DELETION_LOGS_DURATION';


/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

INSERT INTO `aba_b2c`.`log_reconciliation`
  (id, filePath, fileOrigin, lineNode, paySuppExtId, dateOperation, idPayment, refPaySuppOrderId, refPaySuppStatus,
  refAmountPrice, refDateEndTransaction, refCurrencyTrans, refPaySuppExtUniId, success, matchType)
SELECT
  id, filePath, fileOrigin, lineNode, paySuppExtId, dateOperation, idPayment, refPaySuppOrderId, refPaySuppStatus,
  refAmountPrice, refDateEndTransaction, refCurrencyTrans, refPaySuppExtUniId, success, matchType
FROM `aba_b2c_logs`.log_reconciliation
;


INSERT INTO `aba_b2c`.`pay_gateway_allpago`
  ( id, idPayment, userId, operation, dateRequest, dateResponse, xmlRequest, xmlResponse, errorCode, success, idPaySupplier )
SELECT
  id, idPayment, userId, operation, dateRequest, dateResponse, xmlRequest, xmlResponse, errorCode, success, idPaySupplier
FROM `aba_b2c_logs`.log_allpago
;


INSERT INTO `aba_b2c`.`pay_gateway_appstore`
  ( id, idPaymentIos, userId, operation, dateRequest, dateResponse, jsonRequest, jsonResponse, errorCode, success, idPaySupplier )
SELECT
  id, idPaymentIos, userId, operation, dateRequest, dateResponse, jsonRequest, jsonResponse, errorCode, success, idPaySupplier
FROM `aba_b2c_logs`.log_appstore
;


INSERT INTO `aba_b2c`.`pay_gateway_paypal_ipn`
  ( id, dateCreation, header, postFields, transactionType, paymentStatus, userId )
SELECT
  id, dateCreation, header, postFields, transactionType, paymentStatus, userId
FROM `aba_b2c_logs`.log_ipn_paypal
;


INSERT INTO `aba_b2c`.`pay_gateway_paypal_express_co`
  ( id, userId, operation, dateRequest, xmlRequest, jsonResponse, errorCode, success, idPayment )
SELECT
  id, userId, operation, dateRequest, xmlRequest, jsonResponse, errorCode, success, idPayment
FROM `aba_b2c_logs`.log_paypal_express_co
;


/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

ALTER TABLE `payments_control_check` CHANGE `tokenPayPal` `tokenPayPal` VARCHAR(45)  CHARACTER SET utf8  NULL  DEFAULT NULL  COMMENT 'Token returned by PAYPAL after first checkout.';

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

