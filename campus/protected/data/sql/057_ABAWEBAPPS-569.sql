
CREATE TABLE `user_progress_queues` (
  `queueId` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Auto id increment',
  `userId` int(11) unsigned NOT NULL COMMENT 'FK to users table',
  `units` text COMMENT 'Units separated by commas',
  `dateAdd` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date created',
  `dateStart` datetime DEFAULT NULL COMMENT 'Process start date',
  `dateEnd` datetime DEFAULT NULL COMMENT 'Process end date',
  `status` tinyint(3) unsigned DEFAULT '0' COMMENT '0 = PENDING, 10 = IN PROGRESS, 20 = FAIL, 30 = PROCESSED',
  PRIMARY KEY (`queueId`),
  KEY `byUserId` (`userId`),
  KEY `byStatus` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Pending user units';


INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES
('USER_PROGRESS_QUEUES_CRON_PATH', ' . /home/rundeck/environment_vars;  /usr/bin/php  /var/app/abawebapps/intranet/public/script.php ', 'CRON PATH', '2016-03-09 16:16:16');


CREATE TABLE `user_progress_queues_details` (
  `queueDetailId` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Auto id increment',
  `queueId` int(11) unsigned NOT NULL COMMENT 'FK to user_progress_queues',
  `unitId` int(11) unsigned NOT NULL COMMENT 'FK to users table',
  `dateStart` datetime DEFAULT NULL COMMENT 'Process start date',
  `dateEnd` datetime DEFAULT NULL COMMENT 'Process end date',
  `status` tinyint(3) unsigned DEFAULT '0' COMMENT '0 = PENDING, 10 = IN PROGRESS, 20 = FAIL, 30 = PROCESSED',
  PRIMARY KEY (`queueDetailId`),
  KEY `byQueueId` (`queueId`),
  KEY `byUnitId` (`unitId`),
  KEY `byStatus` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Pending user units';


