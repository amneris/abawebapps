
/* ENH-ADYEN, Adien */

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_ENABLE_GENERAL', '1', 'enable adyen payments', '2015-02-19 11:11:11');
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_URL', 'https://pal-test.adyen.com/pal/Payment.wsdl', 'URL to execute payment by Adyen Soap. PROD:https://pal-live.adyen.com/pal/Payment.wsdl   TEST:https://pal-test.adyen.com/pal/Payment.wsdl', '2015-02-19 18:18:18');
-- PROD -- INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_URL', 'https://pal-live.adyen.com/pal/Payment.wsdl', 'URL to execute payment by Adyen Soap. PROD:https://pal-live.adyen.com/pal/Payment.wsdl   TEST:https://pal-test.adyen.com/pal/Payment.wsdl', '2015-02-19 18:18:18');

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_URL_RECURRING', 'https://pal-test.adyen.com/pal/Recurring.wsdl', 'URL to execute payment by Adyen Soap. PROD:https://pal-live.adyen.com/pal/Recurring.wsdl   TEST:https://pal-test.adyen.com/pal/Recurring.wsdl', '2015-02-19 18:18:18');
-- PROD -- INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_URL_RECURRING', 'https://pal-live.adyen.com/pal/Recurring.wsdl', 'URL to execute payment by Adyen Soap. PROD:https://pal-live.adyen.com/pal/Recurring.wsdl   TEST:https://pal-test.adyen.com/pal/Recurring.wsdl', '2015-02-19 18:18:18');

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_URL_REPORTS', 'https://ca-test.adyen.com/reports/download/MerchantAccount/', 'URL to download reports. PROD: ca-live.adyen.com TEST: ca-test.adyen.com ', '2015-03-16 16:16:16');
-- INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_URL_REPORTS', 'https://ca-live.adyen.com/reports/download/MerchantAccount/', 'URL to download reports. PROD: ca-live.adyen.com TEST: ca-test.adyen.com ', '2015-03-16 16:16:16');

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_SOAP_LOGIN', 'ws_248115@Company.ABAEnglish', 'adyen soap login', '2015-02-19 11:11:11');
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_SOAP_PASSWORD', 'EEz%nQFvBPeIV/M=]7AwxyxMM', 'adyen soap password', '2015-02-19 11:11:11');
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_SOAP_PUBLIC_KEY', '10001|98894F374E0D9FF7082D991954B98B5BB134A0A9C58DBC4B1BE65C233FBA91FBE6B6DA1FA756D22D368A3BFD7B2EB60BC0F14631C2C3FCE1B969C010FAA55739A20889CA6F32135470DEDA39FDB7D3EFC38EA1752DD7824FE210A7B19635FC26339A0165D963FA61DCE563B91B41DC2E2238AC8605C05B741649F6FFDBA4E8BED1117BDE7DABEADEFEB0BAF26B2D39FDD28094727F07067020F3B93CBAFC34AE7F05DFFA17507A8E77308732EA9A2E1DDE20AF127A598FE8094D1352DB819E469090B6086BEFC82942E717C21193EBA31E16DC4F5C8C85FCE1397C6E8F617D18C1434A159EE325D2B085A0BCA92435DF4D0DCC54A940ACA412AAC5D5A70933C7', 'Client Encryption Public Key', '2015-02-19 11:11:11');

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_MERCHANT_ACCOUNT', 'ABAEnglishCOM', 'Aba Merchant Account Adyen', '2015-02-23 17:17:17');
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_SOAP_ADDITIONALDATA_URL', 'http://payment.services.adyen.com', 'Url in soap request for additionalData field', '2015-02-23 17:17:17');

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('ENABLE_ADYEN_PAY_RECURRENT', '1', '1', '2015-02-26 16:16:16');

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_REPORT_LOGIN', 'report_724763@Company.ABAEnglish', 'adyen reports login', '2015-03-16 16:16:16');
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_REPORT_PASSWORD', 'AbAenglishPerortUser1qa2ws', 'adyen reports password', '2015-03-16 16:16:16');

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_NOTIFICATIONS_LOGIN', 'AbAenglish', 'adyen notifications login', '2015-03-19 11:11:11');
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES ('PAY_ADYEN_NOTIFICATIONS_PASSWORD', 'AbAenglsh1qa2ws', 'adyen notifications login', '2015-03-19 11:11:11');

--
INSERT INTO `aba_b2c`.`pay_suppliers` ( `id`, `name`, `invoice` ) VALUES( 10, 'Adyen', 1);

CREATE TABLE `pay_gateway_adyen_sent` (
`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
`idPayment` VARCHAR(15) NOT NULL COMMENT 'FK to payments control check',
`dateSent` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Sent date',
`merchantAccount` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Merchant account: ABAEnglishCOM',
`amountValue` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Amount value',
`amountCurrency` VARCHAR(3) NULL DEFAULT NULL COMMENT 'Currency',
`reference` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Unique aba reference',
`originalReference` VARCHAR(45) NULL DEFAULT NULL COMMENT 'This is the pspReference that was assigned to the authorisation',
`shopperIP` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Shopper (user) IP',
`userId` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'FK to userId',
`shopperEmail` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Shopper email',
`shopperReference` VARCHAR(45) NULL DEFAULT NULL COMMENT 'MD5 userId',
`fraudOffset` VARCHAR(45) NULL DEFAULT NULL COMMENT '"0"',
`additionalData` TEXT NULL COMMENT 'Encoded user credit card form data for AUTHORIZATION transaction',
`selectedRecurringDetailReference` VARCHAR(255) NULL DEFAULT '',
`recurringContract` VARCHAR(45) NULL DEFAULT '' COMMENT '"RECURRING" - force recurring payments',
`typeGateway` TINYINT(3) UNSIGNED NULL DEFAULT '10' COMMENT '10=AUTORISATION PAYMENT, 20=AUTHORISATION RECURRING, 30=REFUND, 40=LIST RECURRING DETAILS, 50=DISABLE RECURRING PAYMENT',
PRIMARY KEY (`id`),
INDEX `byPaymentDate` (`idPayment`, `dateSent`),
INDEX `reference` (`reference`),
INDEX `typeGateway` (`typeGateway`)
)
COLLATE='utf8_general_ci' ENGINE=InnoDB ;


CREATE TABLE `pay_gateway_adyen_response` (
`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
`idPayment` VARCHAR(15) NOT NULL COMMENT 'FK to payments control check',
`dateSent` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Sent date',
`authCode` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Authorisation code',
`dccAmount` VARCHAR(45) NULL DEFAULT NULL,
`dccSignature` VARCHAR(45) NULL DEFAULT NULL,
`fraudResult` VARCHAR(45) NULL DEFAULT NULL,
`issuerUrl` VARCHAR(255) NULL DEFAULT NULL,
`md` VARCHAR(45) NULL DEFAULT NULL,
`paRequest` VARCHAR(45) NULL DEFAULT NULL COMMENT 'pspReference',
`Order_number` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Unique adyen identifier',
`refusalReason` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Refusal reason',
`resultCode` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Refused, Authorised, [refund-received], [detail-successfully-disabled], visa, mc,...',
`additionalData` TEXT NULL,
`XML` TEXT NULL COMMENT '"Object" response',
`numNotifications` TINYINT(3) UNSIGNED NULL DEFAULT '0' COMMENT 'Notifications number',
PRIMARY KEY (`id`),
INDEX `byIdPayDate` (`idPayment`, `dateSent`),
INDEX `Order_number` (`Order_number`)
)
COLLATE='utf8_general_ci' ENGINE=InnoDB ;


CREATE TABLE `payments_adyen_notifications` (
`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'AutoId, PK',
`live` TINYINT(1) UNSIGNED NULL DEFAULT '0' COMMENT '0 OR NOT 1 = TEST, 1 = PROD',
`amountCurrency` VARCHAR(4) NULL DEFAULT NULL COMMENT 'Currency',
`amountValue` FLOAT UNSIGNED NULL DEFAULT '0' COMMENT 'Amount value',
`eventCode` VARCHAR(45) NULL DEFAULT '' COMMENT 'Notification code: AUTHORISATION, REPORT_AVAILABLE, REFUND,..',
`eventDate` VARCHAR(45) NULL DEFAULT '' COMMENT 'Event date (adyen)',
`merchantAccountCode` VARCHAR(45) NULL DEFAULT '' COMMENT 'Merchant account: ABAEnglishCOM',
`merchantReference` VARCHAR(45) NULL DEFAULT '' COMMENT 'Merchant unique referente',
`originalReference` VARCHAR(45) NULL DEFAULT '' COMMENT 'This is the pspReference that was assigned to the authorisation',
`pspReference` VARCHAR(45) NULL DEFAULT '' COMMENT 'Adyen unique identifier',
`reason` VARCHAR(510) NULL DEFAULT '' COMMENT 'Reason code OR  URL of available report file',
`success` TINYINT(2) NULL DEFAULT '0' COMMENT 'Result: 0, 1',
`paymentMethod` VARCHAR(45) NULL DEFAULT '' COMMENT 'Payment method code: visa, directdebit.NL,..',
`operations` VARCHAR(255) NULL DEFAULT '' COMMENT 'Operations  ["CANCEL","CAPTURE","REFUND"]',
`additionalData` TEXT NULL COMMENT 'Additional data',
`dateAdd` DATETIME NULL DEFAULT NULL COMMENT 'Created data (aba)',
PRIMARY KEY (`id`),
INDEX `eventCode` (`eventCode`)
)
COMMENT='Adyen notificaitons' COLLATE='utf8_general_ci' ENGINE=InnoDB ;


-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --


ALTER TABLE `aba_b2c`.`payments_control_check` ALTER `kind` DROP DEFAULT;
ALTER TABLE `aba_b2c`.`payments_control_check` CHANGE COLUMN `kind` `kind` INT(2) UNSIGNED NULL COMMENT 'KIND_CC_VISA=0;KIND_CC_MASTERCARD=1;KIND_CC_AMERICAN_EXPRESS=2;KIND_CC_DINERS_CLUB=3;KIND_PAYPAL=10' AFTER `amountPrice`;

ALTER TABLE `aba_b2c`.`user_credit_forms` ALTER `kind` DROP DEFAULT;
ALTER TABLE `aba_b2c`.`user_credit_forms` CHANGE COLUMN `kind` `kind` INT(2) UNSIGNED NULL COMMENT 'KIND_CC_VISA=0;KIND_CC_MASTERCARD=1;KIND_CC_AMERICAN_EXPRESS=2;KIND_CC_DINERS_CLUB=3;KIND_PAYPAL=10' AFTER `id`;


-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --


ALTER TABLE `pay_gateway_adyen_sent` CHANGE COLUMN `idPayment` `idPayment` VARCHAR(15) NULL DEFAULT NULL COMMENT 'FK to payments control check' AFTER `id`;
ALTER TABLE `pay_gateway_adyen_response` CHANGE COLUMN `idPayment` `idPayment` VARCHAR(15) NULL DEFAULT NULL COMMENT 'FK to payments control check' AFTER `id`;


-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

--
-- ! OJO ! COMENTAR ANTES !
--
-- UPDATE `aba_b2c`.`country_paymethod_paysupplier` SET `idPaySupplier`=10 WHERE  `idCountry`=47 AND `idPayMethod`=1 AND `idPaySupplier`=1 LIMIT 1;
--
--
--

/* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --  */

DROP PROCEDURE IF EXISTS `aba_b2c`.`resetUserFromCampus`;

DELIMITER //
BEGIN

    DECLARE varUserId INT;

    SELECT u.`id` INTO varUserId FROM `user` u WHERE u.`email`=argUserEmail;

  	DELETE f.* FROM followup4 f WHERE f.`userid` = varUserId;

		DELETE f.* FROM followup_html4 f WHERE f.`userid`=varUserId;

    DELETE p.* FROM user_dictionary p WHERE p.`idUser`=varUserId;

		DELETE p.* FROM pay_gateway_response p WHERE p.`idPayment` IN (SELECT p.id FROM payments p WHERE p.`userId`=varUserId);
		DELETE p.* FROM pay_gateway_sent p WHERE p.`idPayment` IN (SELECT p.id FROM payments p WHERE p.`userId`=varUserId);

		DELETE p.* FROM pay_gateway_adyen_response p WHERE p.`idPayment` IN (SELECT p.id FROM payments_control_check p WHERE p.`userId`=varUserId);
		DELETE p.* FROM pay_gateway_adyen_sent p WHERE p.`idPayment` IN (SELECT p.id FROM payments_control_check p WHERE p.`userId`=varUserId);
-- 		DELETE p.* FROM payments_adyen_notifications p WHERE p.`idPayment` IN (SELECT p.id FROM payments_control_check p WHERE p.`userId`=varUserId);
-- 		DELETE p.* FROM payments_adyen p WHERE p.`idPayment` IN (SELECT p.id FROM payments p WHERE p.`userId`=varUserId);

		DELETE p.* FROM payments p WHERE p.`userId`=varUserId;
		DELETE p.* FROM payments_control_check p WHERE p.`userId`=varUserId;
    DELETE p.* FROM payments_appstore p WHERE p.`userId`=varUserId;

		DELETE u.* FROM user_credit_forms u WHERE u.`userId`=varUserId;

		DELETE u.* FROM user_objective u WHERE u.`userId`=varUserId;

		DELETE u.* FROM user_devices u WHERE u.`userId`=varUserId;

		DELETE u.* FROM user_videoclasses_unlocks u WHERE u.`userId`=varUserId;

		DELETE u.* FROM user_address_invoice u WHERE u.`userId`=varUserId;
		DELETE u.* FROM user_locations u WHERE u.`userId`=varUserId;


		DELETE c.* FROM course_var4 c WHERE c.`userId`=varUserId;

    DELETE m.* FROM messages m WHERE m.receiver_id=varUserId;

    DELETE m.* FROM messages m WHERE m.sender_id=varUserId;

    DELETE FROM `aba_b2c_logs`.`log_user_level_change` WHERE `userId`=varUserId;

    DELETE u.* FROM `user` u WHERE u.id=varUserId;

/* followup_X */

    DELETE IGNORE f.* FROM followup4_30 f WHERE f.`userid`=varUserId;
    DELETE IGNORE f.* FROM followup4_31 f WHERE f.`userid`=varUserId;
    DELETE IGNORE f.* FROM followup4_32 f WHERE f.`userid`=varUserId;
    DELETE IGNORE f.* FROM followup4_33 f WHERE f.`userid`=varUserId;
    DELETE IGNORE f.* FROM followup4_34 f WHERE f.`userid`=varUserId;
    DELETE IGNORE f.* FROM followup4_35 f WHERE f.`userid`=varUserId;

/* followup_html4_X */

    DELETE IGNORE f.* FROM followup_html4_30 f WHERE f.`userid`=varUserId;
    DELETE IGNORE f.* FROM followup_html4_31 f WHERE f.`userid`=varUserId;
    DELETE IGNORE f.* FROM followup_html4_32 f WHERE f.`userid`=varUserId;
    DELETE IGNORE f.* FROM followup_html4_33 f WHERE f.`userid`=varUserId;
    DELETE IGNORE f.* FROM followup_html4_34 f WHERE f.`userid`=varUserId;
    DELETE IGNORE f.* FROM followup_html4_35 f WHERE f.`userid`=varUserId;

-- DELETE IGNORE f.* FROM aba_b2c_summary.followup4_summary f WHERE f.`userid`=varUserId;

    SELECT varUserId INTO userId;

END//
DELIMITER ;



ALTER TABLE `aba_b2c`.`payments_adyen_notifications` ADD COLUMN `notification` TEXT NULL COMMENT 'Notification' AFTER `additionalData`;







