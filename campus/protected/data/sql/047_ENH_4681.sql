
/* START  ENH-4681, no guardar datos de las tarjetas */

ALTER TABLE `pay_gateway_sent` ADD COLUMN `MerchantIdentifier` VARCHAR(45) NULL COMMENT 'Referencia a los datos de la tarjeta' AFTER `Cvv2`;

ALTER TABLE `pay_gateway_sent` ADD INDEX `byIdentifier` (`MerchantIdentifier`);

--

ALTER TABLE `pay_gateway_response`
ADD COLUMN `MerchantIdentifier` VARCHAR(45) NULL COMMENT 'Referencia asociada a los datos...' AFTER `MerchantData`,
ADD COLUMN `ExpiryDate` VARCHAR(20) NULL COMMENT 'Ds_ExpiryDate=2010' AFTER `MerchantIdentifier`;

ALTER TABLE `pay_gateway_response` ADD INDEX `byIdentifier` (`MerchantIdentifier`);

--
-- INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES
-- ('PAY_CAIXA_PAYMENT_MODE', '2', '1=PCI (no card data), 2=HYBRID, 3=NORMAL + save reference, 4=OLD (Without reference)', '2015-09-18 13:13:13');

/* END  ENH-4681, no guardar datos de las tarjetas */
