

DROP PROCEDURE IF EXISTS `aba_b2c`.`resetUserFromCampus`;


DELIMITER //
CREATE PROCEDURE `resetUserFromCampus`(IN `argUserEmail` VARCHAR(65), OUT `userId` INT)
BEGIN

	DECLARE varUserId INT;
	DECLARE progressVersion INT;
	DECLARE progressVersionHtml INT;

	SELECT u.`id` INTO varUserId FROM `user` u WHERE u.`email`=argUserEmail;
	SELECT ud.`progressVersion` INTO progressVersion FROM `user_dictionary` ud WHERE ud.`idUser`=varUserId;
	SELECT ud.`progressVersionHtml` INTO progressVersionHtml FROM `user_dictionary` ud WHERE ud.`idUser`=varUserId;


	/** aba_b2c **/ 

	DELETE p.* FROM pay_gateway_response p WHERE p.`idPayment` IN (SELECT p.id FROM payments p WHERE p.`userId`=varUserId);
	DELETE p.* FROM pay_gateway_sent p WHERE p.`idPayment` IN (SELECT p.id FROM payments p WHERE p.`userId`=varUserId);
	DELETE p.* FROM pay_gateway_adyen_response p WHERE p.`idPayment` IN (SELECT p.id FROM payments_control_check p WHERE p.`userId`=varUserId);
	DELETE p.* FROM pay_gateway_adyen_sent p WHERE p.`idPayment` IN (SELECT p.id FROM payments_control_check p WHERE p.`userId`=varUserId);

	DELETE p.* FROM payments_appstore p WHERE p.`userId`=varUserId;
	DELETE p.* FROM payments_playstore p WHERE p.`userId`=varUserId;
	DELETE p.* FROM payments p WHERE p.`userId`=varUserId;
	DELETE p.* FROM payments_control_check p WHERE p.`userId`=varUserId;

	DELETE u.* FROM user_credit_forms u WHERE u.`userId`=varUserId;
	DELETE u.* FROM user_objective u WHERE u.`userId`=varUserId;
	DELETE u.* FROM user_devices u WHERE u.`userId`=varUserId;
	DELETE u.* FROM user_address_invoice u WHERE u.`userId`=varUserId;
	DELETE u.* FROM user_locations u WHERE u.`userId`=varUserId;
	DELETE up.* FROM user_promos up WHERE up.`userId`=varUserId;
	DELETE up.* FROM user_experiments_variations up WHERE up.`userId`=varUserId;

	DELETE c.* FROM course_var4 c WHERE c.`userId`=varUserId;

	DELETE m.* FROM messages m WHERE m.receiver_id=varUserId;
	DELETE m.* FROM messages m WHERE m.sender_id=varUserId;

	CASE progressVersion 
		WHEN 30 THEN 
			DELETE f.* FROM followup4_30 f WHERE f.`userid`=varUserId;
		WHEN 31 THEN 
			DELETE f.* FROM followup4_31 f WHERE f.`userid`=varUserId;
		WHEN 32 THEN 
			DELETE f.* FROM followup4_32 f WHERE f.`userid`=varUserId;
		WHEN 33 THEN 
			DELETE f.* FROM followup4_33 f WHERE f.`userid`=varUserId;
		WHEN 34 THEN 
			DELETE f.* FROM followup4_34 f WHERE f.`userid`=varUserId;
		WHEN 35 THEN 
			DELETE f.* FROM followup4_35 f WHERE f.`userid`=varUserId;
		WHEN 36 THEN 
			DELETE f.* FROM followup4_36 f WHERE f.`userid`=varUserId;
		WHEN 37 THEN 
			DELETE f.* FROM followup4_37 f WHERE f.`userid`=varUserId;
		WHEN 38 THEN 
			DELETE f.* FROM followup4_38 f WHERE f.`userid`=varUserId;
		WHEN 39 THEN 
			DELETE f.* FROM followup4_39 f WHERE f.`userid`=varUserId;
		WHEN 40 THEN 
			DELETE f.* FROM followup4_40 f WHERE f.`userid`=varUserId;
		WHEN 41 THEN 
			DELETE f.* FROM followup4_41 f WHERE f.`userid`=varUserId;
		WHEN 42 THEN 
			DELETE f.* FROM followup4_42 f WHERE f.`userid`=varUserId;
		WHEN 43 THEN 
			DELETE f.* FROM followup4_43 f WHERE f.`userid`=varUserId;
		WHEN 44 THEN 
			DELETE f.* FROM followup4_44 f WHERE f.`userid`=varUserId;
		WHEN 45 THEN 
			DELETE f.* FROM followup4_45 f WHERE f.`userid`=varUserId;
		ELSE 
			DELETE f.* FROM followup4 f WHERE f.`userid` = varUserId;
	END CASE; 

	CASE progressVersionHtml 
		WHEN 30 THEN 
			DELETE f.* FROM followup_html4_30 f WHERE f.`userid`=varUserId;
		WHEN 31 THEN 
			DELETE f.* FROM followup_html4_31 f WHERE f.`userid`=varUserId;
		WHEN 32 THEN 
			DELETE f.* FROM followup_html4_32 f WHERE f.`userid`=varUserId;
		WHEN 33 THEN 
			DELETE f.* FROM followup_html4_33 f WHERE f.`userid`=varUserId;
		WHEN 34 THEN 
			DELETE f.* FROM followup_html4_34 f WHERE f.`userid`=varUserId;
		WHEN 35 THEN 
			DELETE f.* FROM followup_html4_35 f WHERE f.`userid`=varUserId;
		WHEN 36 THEN 
			DELETE f.* FROM followup_html4_36 f WHERE f.`userid`=varUserId;
		WHEN 37 THEN 
			DELETE f.* FROM followup_html4_37 f WHERE f.`userid`=varUserId;
		WHEN 38 THEN 
			DELETE f.* FROM followup_html4_38 f WHERE f.`userid`=varUserId;
		WHEN 39 THEN 
			DELETE f.* FROM followup_html4_39 f WHERE f.`userid`=varUserId;
		WHEN 40 THEN 
			DELETE f.* FROM followup_html4_40 f WHERE f.`userid`=varUserId;
		WHEN 41 THEN 
			DELETE f.* FROM followup_html4_41 f WHERE f.`userid`=varUserId;
		WHEN 42 THEN 
			DELETE f.* FROM followup_html4_42 f WHERE f.`userid`=varUserId;
		WHEN 43 THEN 
			DELETE f.* FROM followup_html4_43 f WHERE f.`userid`=varUserId;
		WHEN 44 THEN 
			DELETE f.* FROM followup_html4_44 f WHERE f.`userid`=varUserId;
		WHEN 45 THEN 
			DELETE f.* FROM followup_html4_45 f WHERE f.`userid`=varUserId;
		ELSE 
			DELETE f.* FROM followup_html4 f WHERE f.`userid`=varUserId;
	END CASE; 


	DELETE u.* FROM `user` u WHERE u.id=varUserId;


	/** aba_b2c_logs **/ 

	DELETE FROM `aba_b2c_logs`.`log_user_level_change` WHERE `userId`=varUserId;


	SELECT varUserId INTO userId;

END//
DELIMITER ;
