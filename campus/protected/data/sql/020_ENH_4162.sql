/* Fixing incorrect configuration of extend sunbscription payments
*
* Problem is fixed in code (files OnlinePayCommon.php, Payment.php and PaymentsplusController.php (all in Campus))
*
* This SQL is to fix DB entries that are already in a wrong state.
*/

/* First we update those boleto payments in DB that has been set as isExtend */

UPDATE aba_b2c.payments p
SET
  p.isExtend = 0
WHERE
  p.paySuppExtId = 7 AND p.isRecurring = 0;

/* Secondly we update payments that are actual subscription extensions with proper idPartner and isRecurring value */

UPDATE aba_b2c.payments p
SET
  p.idPartner = 300006,
  p.isRecurring = 1
WHERE
  p.isExtend = 1;

/* Thirdly we update Boleto payments that are actual subscription extensions that was "wrongly" set as not isExtend in step 1 */

UPDATE aba_b2c.payments p
SET
  p.isExtend = 1
WHERE
  paySuppExtId = 7
  AND idPromoCode LIKE '%fp%'
  AND idPromoCode LIKE '%up%'
  AND status = 30;