
CREATE TABLE `payments_fails` (
  `idFail` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'AutoId, PK',
  `paymentId` varchar(15) NOT NULL,
  `dateAdd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create date',
  PRIMARY KEY (`idFail`),
  UNIQUE KEY `paymentId` (`paymentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Temporal FAILS payments IDs';

