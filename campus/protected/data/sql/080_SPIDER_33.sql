

CREATE TABLE `log_user_progress_removed` (
  `removedProgressId` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Auto id increment',
  `userId` int(11) unsigned NOT NULL COMMENT 'FK to users table',
  `units` text COMMENT 'Units separated by commas',
  `dateAdd` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date created',
  PRIMARY KEY (`removedProgressId`),
  KEY `byUserId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Removed user units';
