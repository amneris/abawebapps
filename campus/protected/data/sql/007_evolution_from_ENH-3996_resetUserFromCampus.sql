
/* START  ENH-3996, web service free2premium */


DROP PROCEDURE IF EXISTS `aba_b2c`.`resetUserFromCampus`;

DELIMITER //
CREATE PROCEDURE `aba_b2c`.`resetUserFromCampus`(IN `argUserEmail` VARCHAR(65), OUT `userId` INT)
BEGIN

    DECLARE varUserId INT;

    SELECT u.`id` INTO varUserId FROM `user` u WHERE u.`email`=argUserEmail;

  	DELETE f.* FROM followup4 f WHERE f.`userid` = varUserId;

		DELETE f.* FROM followup_html4 f WHERE f.`userid`=varUserId;

DELETE p.* FROM user_dictionary p WHERE p.`idUser`=varUserId;


		DELETE p.* FROM pay_gateway_response p WHERE p.`idPayment` IN (SELECT p.id FROM payments p WHERE p.`userId`=varUserId);

		DELETE p.* FROM pay_gateway_sent p WHERE p.`idPayment` IN (SELECT p.id FROM payments p WHERE p.`userId`=varUserId);

		DELETE p.* FROM payments p WHERE p.`userId`=varUserId;

		DELETE p.* FROM payments_control_check p WHERE p.`userId`=varUserId;

DELETE p.* FROM payments_appstore p WHERE p.`userId`=varUserId;


		DELETE u.* FROM user_credit_forms u WHERE u.`userId`=varUserId;

		DELETE u.* FROM user_objective u WHERE u.`userId`=varUserId;

		DELETE u.* FROM user_devices u WHERE u.`userId`=varUserId;

		DELETE u.* FROM user_videoclasses_unlocks u WHERE u.`userId`=varUserId;

		DELETE u.* FROM user_address_invoice u WHERE u.`userId`=varUserId;
		DELETE u.* FROM user_locations u WHERE u.`userId`=varUserId;


		DELETE c.* FROM course_var4 c WHERE c.`userId`=varUserId;

    DELETE m.* FROM messages m WHERE m.receiver_id=varUserId;

    DELETE m.* FROM messages m WHERE m.sender_id=varUserId;

    DELETE FROM `aba_b2c_logs`.`log_user_level_change` WHERE `userId`=varUserId;


    DELETE u.* FROM `user` u WHERE u.id=varUserId;


/* followup_X */

DELETE IGNORE f.* FROM followup4_30 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_31 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_32 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_33 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_34 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup4_35 f WHERE f.`userid`=varUserId;

/* followup_html4_X */

DELETE IGNORE f.* FROM followup_html4_30 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_31 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_32 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_33 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_34 f WHERE f.`userid`=varUserId;
DELETE IGNORE f.* FROM followup_html4_35 f WHERE f.`userid`=varUserId;

-- DELETE IGNORE f.* FROM aba_b2c_summary.followup4_summary f WHERE f.`userid`=varUserId;

    SELECT varUserId INTO userId;

END//
DELIMITER ;

/* END  ENH-3996 */


