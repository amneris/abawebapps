
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES
('PROGRESS_REGISTER_THROTTLE', '2501', 'ABAWEBAPPS-623 - max amount of events allowed in one call', '2016-03-21 11:00:00');

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES
('PROGRESS_REGISTER_DELETE_EXCESS_PROGRESS_FROM_CLIENTS', '0', 'ABAWEBAPPS-623 - send the previous progress to the client and delete the current progress', '2016-03-21 11:00:00');
