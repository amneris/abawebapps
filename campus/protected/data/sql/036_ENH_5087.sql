
/* START ENH-5087, FAMILY PLAN */

CREATE TABLE `user_promos` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`userId` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to userId',
	`idPromoCode` VARCHAR(50) NULL DEFAULT '' COMMENT 'Name Promo',
	`email` VARCHAR(200) NULL DEFAULT '' COMMENT 'Related user email',
	`idProduct` VARCHAR(15) NULL DEFAULT '' COMMENT 'FK Product ID',
	`used` SMALLINT(3) UNSIGNED NULL DEFAULT '0' COMMENT 'Used by related user',
	`dateAdd` DATETIME NULL DEFAULT NULL COMMENT 'Date add',
	PRIMARY KEY (`id`),
	INDEX `byUserPromoUserId` (`userId`),
	INDEX `byUserPromoIdPromoCode` (`idPromoCode`),
	INDEX `byUserPromoEmail` (`email`),
	INDEX `byUresPromoIdProduct` (`idProduct`),
	INDEX `byUserPromoUsed` (`used`)
)
COMMENT='related user licenses' COLLATE='utf8_general_ci' ENGINE=InnoDB;


ALTER TABLE `products_prices` ADD COLUMN `isPlanPrice` TINYINT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Family plan...' AFTER `enabledExtranet`;


ALTER TABLE `products_prices`
CHANGE COLUMN `descriptionText` `descriptionText` VARCHAR(30) NULL DEFAULT NULL COMMENT 'It should be as same as product description with periodicity.' AFTER `modified`;


INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES
('OP_URL_SELLIGENT_PAGOSPLAN', '/optiext/optiextension.dll?ID=s6Xs1b1l6x9S_5038kigBbT1w6NKmQWXL20GU9f9Y1MjS7ScJZtCirQTYcsmhd0CZHCj%2BNpQfpNryEdpMF', 'Operation: Pagos Family Plan In Selligent', '2013-01-15 15:51:28');


INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`, `dateUpdate`) VALUES
('OP_URL_SELLIGENT_RENEWALSPLANNOTIFICATIONUPGRADE', '/optiext/optiextension.dll?ID=7ty7DUKrmP7DOnNrCDJJglKQ%2BW2Xjkd5dWPu024H_M_t9rTRllwGvcA_xPmCKxwh7P%2B4jeQyn27EoP41Do', 'Operation: Pagos Family Plan In Selligent', '2013-01-15 15:51:28');


-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
--

DROP PROCEDURE IF EXISTS `aba_b2c`.`resetUserFromCampus`;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para procedimiento aba_b2c.resetUserFromCampus
DELIMITER //
CREATE PROCEDURE `resetUserFromCampus`(IN argUserEmail VARCHAR(65), OUT userId INT)
BEGIN

  DECLARE varUserId INT;

  SELECT u.`id` INTO varUserId FROM `user` u WHERE u.`email`=argUserEmail;

  DELETE f.* FROM followup4 f WHERE f.`userid` = varUserId;
  DELETE f.* FROM followup_html4 f WHERE f.`userid`=varUserId;

  DELETE p.* FROM pay_gateway_response p WHERE p.`idPayment` IN (SELECT p.id FROM payments p WHERE p.`userId`=varUserId);
  DELETE p.* FROM pay_gateway_sent p WHERE p.`idPayment` IN (SELECT p.id FROM payments p WHERE p.`userId`=varUserId);
  DELETE p.* FROM pay_gateway_adyen_response p WHERE p.`idPayment` IN (SELECT p.id FROM payments_control_check p WHERE p.`userId`=varUserId);
  DELETE p.* FROM pay_gateway_adyen_sent p WHERE p.`idPayment` IN (SELECT p.id FROM payments_control_check p WHERE p.`userId`=varUserId);

  DELETE p.* FROM payments p WHERE p.`userId`=varUserId;
  DELETE p.* FROM payments_control_check p WHERE p.`userId`=varUserId;
  DELETE p.* FROM payments_appstore p WHERE p.`userId`=varUserId;

  DELETE u.* FROM user_credit_forms u WHERE u.`userId`=varUserId;
  DELETE u.* FROM user_objective u WHERE u.`userId`=varUserId;
  DELETE u.* FROM user_devices u WHERE u.`userId`=varUserId;
  DELETE u.* FROM user_address_invoice u WHERE u.`userId`=varUserId;
  DELETE u.* FROM user_locations u WHERE u.`userId`=varUserId;
  DELETE up.* FROM user_promos up WHERE up.`userId`=varUserId;

  DELETE c.* FROM course_var4 c WHERE c.`userId`=varUserId;

  DELETE m.* FROM messages m WHERE m.receiver_id=varUserId;
  DELETE m.* FROM messages m WHERE m.sender_id=varUserId;

  DELETE FROM `aba_b2c_logs`.`log_user_level_change` WHERE `userId`=varUserId;

  DELETE IGNORE f.* FROM followup4_30 f WHERE f.`userid`=varUserId;
  DELETE IGNORE f.* FROM followup4_31 f WHERE f.`userid`=varUserId;
  DELETE IGNORE f.* FROM followup4_32 f WHERE f.`userid`=varUserId;
  DELETE IGNORE f.* FROM followup4_33 f WHERE f.`userid`=varUserId;
  DELETE IGNORE f.* FROM followup4_34 f WHERE f.`userid`=varUserId;
  DELETE IGNORE f.* FROM followup4_35 f WHERE f.`userid`=varUserId;

  DELETE IGNORE f.* FROM followup_html4_30 f WHERE f.`userid`=varUserId;
  DELETE IGNORE f.* FROM followup_html4_31 f WHERE f.`userid`=varUserId;
  DELETE IGNORE f.* FROM followup_html4_32 f WHERE f.`userid`=varUserId;
  DELETE IGNORE f.* FROM followup_html4_33 f WHERE f.`userid`=varUserId;
  DELETE IGNORE f.* FROM followup_html4_34 f WHERE f.`userid`=varUserId;
  DELETE IGNORE f.* FROM followup_html4_35 f WHERE f.`userid`=varUserId;

  DELETE u.* FROM `user` u WHERE u.id=varUserId;

  SELECT varUserId INTO userId;

END//
DELIMITER ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;


/* END ENH-5087, FAMILY PLAN */

