
CREATE TABLE aba_b2c.`aba_partners_groups` (
  `idGroup` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'AutoId, PK',
  `nameGroup` varchar(75) NOT NULL DEFAULT '' COMMENT 'Unique group identifier',
  `dateAdd` datetime DEFAULT NULL COMMENT 'Create date',
  PRIMARY KEY (`idGroup`),
  UNIQUE KEY `uniqueByNameGroup` (`nameGroup`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE aba_b2c.`aba_partners_categories` (
  `idCategory` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'AutoId, PK',
  `nameCategory` varchar(75) NOT NULL DEFAULT '' COMMENT 'Unique category identifier',
  `dateAdd` datetime DEFAULT NULL COMMENT 'Create date',
  PRIMARY KEY (`idCategory`),
  UNIQUE KEY `uniqueByNameCategory` (`nameCategory`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE aba_b2c.`aba_partners_business_areas` (
  `idBusinessArea` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'AutoId, PK',
  `nameBusinessArea` varchar(75) NOT NULL DEFAULT '' COMMENT 'Unique Business Area identifier',
  `dateAdd` datetime DEFAULT NULL COMMENT 'Create date',
  PRIMARY KEY (`idBusinessArea`),
  UNIQUE KEY `uniqueByBusinessArea` (`nameBusinessArea`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE aba_b2c.`aba_partners_types` (
  `idType` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'AutoId, PK',
  `nameType` varchar(75) NOT NULL DEFAULT '' COMMENT 'Unique Type identifier',
  `dateAdd` datetime DEFAULT NULL COMMENT 'Create date',
  PRIMARY KEY (`idType`),
  UNIQUE KEY `uniqueByNameType` (`nameType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE aba_b2c.`aba_partners_channels` (
  `idChannel` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'AutoId, PK',
  `nameChannel` varchar(75) NOT NULL DEFAULT '' COMMENT 'Unique Channel identifier',
  `dateAdd` datetime DEFAULT NULL COMMENT 'Create date',
  PRIMARY KEY (`idChannel`),
  UNIQUE KEY `uniqueByNameChannel` (`nameChannel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE aba_b2c.`aba_partners_list` ADD `idCategory` INT(11)
  UNSIGNED  NULL  DEFAULT '2'  COMMENT '2 - Paid (default)' AFTER `nameGroup`;

ALTER TABLE aba_b2c.`aba_partners_list` ADD `idBusinessArea` INT(11)
  UNSIGNED  NULL  DEFAULT '2'  COMMENT '2 - B2C (default)' AFTER `idCategory`;

ALTER TABLE aba_b2c.`aba_partners_list` ADD `idType` INT(11)
  UNSIGNED  NULL  DEFAULT '1'  COMMENT '1 - Not classified (default)' AFTER `idBusinessArea`;

ALTER TABLE aba_b2c.`aba_partners_list` ADD `idGroup` INT(11)
  UNSIGNED  NULL  DEFAULT '1'  COMMENT '1 - Not classified (default)' AFTER `idType`;

ALTER TABLE aba_b2c.`aba_partners_list` ADD `idChannel` INT(11)
  UNSIGNED  NULL  DEFAULT '1'  COMMENT '1 - Not classified (default)' AFTER `idGroup`;

INSERT INTO aba_b2c.`aba_partners_business_areas` (`idBusinessArea`, `nameBusinessArea`, `dateAdd`)
VALUES
(1, 'Not classified', '2016-05-10 11:58:05'),
(2, 'B2C', '2016-05-10 11:58:24'),
(3, 'B2B', '2016-05-10 11:58:38'),
(4, 'Flashsaleszzz', '2016-05-10 11:58:52');

INSERT INTO aba_b2c.`aba_partners_categories` (`idCategory`, `nameCategory`, `dateAdd`)
VALUES
(1, 'Not classified', '2016-05-10 11:25:03'),
(2, 'Paid', '2016-05-10 11:25:24'),
(3, 'Not paid', '2016-05-10 11:25:56');

INSERT INTO aba_b2c.`aba_partners_channels` (`idChannel`, `nameChannel`, `dateAdd`)
VALUES
(1, 'Not classified', '2016-05-10 12:38:40'),
(2, 'Web', '2016-05-10 12:38:58'),
(3, 'iOS', '2016-05-10 12:39:14'),
(4, 'Android', '2016-05-10 12:39:31');

INSERT INTO aba_b2c.`aba_partners_groups` (`idGroup`, `nameGroup`, `dateAdd`)
VALUES
(1, 'Not classified', '2016-05-10 10:26:17'),
(2, 'Organic', '2016-05-10 10:26:17'),
(3, 'Google', '2016-05-10 10:26:17'),
(4, 'Bing', '2016-05-10 10:26:17'),
(5, 'Yandex', '2016-05-10 10:26:17'),
(6, 'Facebook', '2016-05-10 10:26:17'),
(7, 'Tweeter', '2016-05-10 10:26:17'),
(8, 'Criteo', '2016-05-10 10:26:17'),
(9, 'DAUAP', '2016-05-10 10:26:17');

INSERT INTO aba_b2c.`aba_partners_types` (`idType`, `nameType`, `dateAdd`)
VALUES
(1, 'Not classified', '2016-05-10 12:21:30'),
(2, 'Organic', '2016-05-10 12:23:16'),
(3, 'SEM (Search engine marketing)', '2016-05-10 12:23:42'),
(4, 'SMM (Social media marketing)', '2016-05-10 12:24:01'),
(5, 'Affiliates', '2016-05-10 12:24:31'),
(6, 'Remarkering', '2016-05-10 12:24:49');

ALTER TABLE aba_b2c.`aba_partners_list` ADD INDEX `byIdCategory` (`idCategory`);
ALTER TABLE aba_b2c.`aba_partners_list` ADD INDEX `byIdBusinessArea` (`idBusinessArea`);
ALTER TABLE aba_b2c.`aba_partners_list` ADD INDEX `byIdType` (`idType`);
ALTER TABLE aba_b2c.`aba_partners_list` ADD INDEX `byIdGroup` (`idGroup`);
ALTER TABLE aba_b2c.`aba_partners_list` ADD INDEX `byIdChannel` (`idChannel`);


ALTER TABLE aba_b2c.`country` ADD `continent` VARCHAR(75)  NULL  DEFAULT 'Unclassified'  COMMENT 'Geo Continent'  AFTER `invoice`;
ALTER TABLE aba_b2c.`country` ADD `region` VARCHAR(75)  NULL  DEFAULT 'Unclassified'  COMMENT 'Geo Region'  AFTER `continent`;
ALTER TABLE aba_b2c.`country` ADD `countryGroup` VARCHAR(75)  NULL  DEFAULT 'Unclassified'  COMMENT 'Geo Country group'  AFTER `region`;


UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 1;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'Other', countryGroup = 'Other' WHERE id = 2;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 3;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 4;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'Other', countryGroup = 'Other' WHERE id = 5;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 6;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 7;
UPDATE aba_b2c.country SET continent = 'Antarctica', region = 'Other', countryGroup = 'Other' WHERE id = 8;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 9;
UPDATE aba_b2c.country SET continent = 'South America', region = 'LATAM', countryGroup = 'Argentina' WHERE id = 10;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'Other', countryGroup = 'Other' WHERE id = 11;
UPDATE aba_b2c.country SET continent = 'South America', region = 'Other', countryGroup = 'Other' WHERE id = 12;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 13;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 14;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'Other', countryGroup = 'Other' WHERE id = 15;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 16;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 17;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 18;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 19;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'Other', countryGroup = 'Other' WHERE id = 20;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 21;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 22;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 23;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 24;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 25;
UPDATE aba_b2c.country SET continent = 'South America', region = 'LATAM', countryGroup = 'Other' WHERE id = 26;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'Other', countryGroup = 'Other' WHERE id = 27;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 28;
UPDATE aba_b2c.country SET continent = 'Antarctica', region = 'Other', countryGroup = 'Other' WHERE id = 29;
UPDATE aba_b2c.country SET continent = 'South America', region = 'LATAM', countryGroup = 'Brazil' WHERE id = 30;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 31;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 32;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 33;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 34;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 35;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 36;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 37;
UPDATE aba_b2c.country SET continent = 'North America', region = 'North America', countryGroup = 'Canada' WHERE id = 38;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 39;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 40;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 41;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 42;
UPDATE aba_b2c.country SET continent = 'South America', region = 'LATAM', countryGroup = 'Chile' WHERE id = 43;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 44;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 45;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 46;
UPDATE aba_b2c.country SET continent = 'South America', region = 'LATAM', countryGroup = 'Colombia' WHERE id = 47;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 48;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 49;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 50;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 51;
UPDATE aba_b2c.country SET continent = 'North America', region = 'LATAM', countryGroup = 'Other' WHERE id = 52;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 53;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 54;
UPDATE aba_b2c.country SET continent = 'North America', region = 'LATAM', countryGroup = 'Other' WHERE id = 55;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 56;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 57;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 58;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 59;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 60;
UPDATE aba_b2c.country SET continent = 'North America', region = 'LATAM', countryGroup = 'Other' WHERE id = 61;
UPDATE aba_b2c.country SET continent = 'South America', region = 'LATAM', countryGroup = 'Ecuador' WHERE id = 62;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 63;
UPDATE aba_b2c.country SET continent = 'North America', region = 'LATAM', countryGroup = 'Other' WHERE id = 64;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 65;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 66;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 67;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 68;
UPDATE aba_b2c.country SET continent = 'South America', region = 'Other', countryGroup = 'Other' WHERE id = 69;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'Other', countryGroup = 'Other' WHERE id = 70;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 71;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 72;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'France' WHERE id = 73;
UPDATE aba_b2c.country SET continent = 'South America', region = 'Other', countryGroup = 'Other' WHERE id = 74;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 75;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 76;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 77;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 78;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'Other', countryGroup = 'Other' WHERE id = 79;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Germany' WHERE id = 80;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 81;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 82;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 83;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'Other', countryGroup = 'Other' WHERE id = 84;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 85;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 86;
UPDATE aba_b2c.country SET continent = 'oceania', region = 'Other', countryGroup = 'Other' WHERE id = 87;
UPDATE aba_b2c.country SET continent = 'North America', region = 'LATAM', countryGroup = 'Other' WHERE id = 88;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 89;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 90;
UPDATE aba_b2c.country SET continent = 'South America', region = 'Other', countryGroup = 'Other' WHERE id = 91;
UPDATE aba_b2c.country SET continent = 'North America', region = 'LATAM', countryGroup = 'Other' WHERE id = 92;
UPDATE aba_b2c.country SET continent = 'Antarctica', region = 'Other', countryGroup = 'Other' WHERE id = 93;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'Other', countryGroup = 'Other' WHERE id = 94;
UPDATE aba_b2c.country SET continent = 'North America', region = 'LATAM', countryGroup = 'Other' WHERE id = 95;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 96;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 97;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'Other', countryGroup = 'Other' WHERE id = 98;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 99;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 100;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 101;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 102;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 103;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 104;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Italy' WHERE id = 105;
UPDATE aba_b2c.country SET continent = 'North America', region = 'LATAM', countryGroup = 'Other' WHERE id = 106;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 107;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 108;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 109;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 110;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 111;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 112;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 113;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 114;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 115;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 116;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 117;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 118;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 119;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 120;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 121;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 122;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 123;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 124;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 125;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'Other', countryGroup = 'Other' WHERE id = 126;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 127;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 128;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 129;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 130;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 131;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'Other', countryGroup = 'Other' WHERE id = 132;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 133;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 134;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 135;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 136;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 137;
UPDATE aba_b2c.country SET continent = 'North America', region = 'LATAM', countryGroup = 'Mexico' WHERE id = 138;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 139;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'Other', countryGroup = 'Other' WHERE id = 140;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 141;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 142;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 143;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 144;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 145;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 146;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 147;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 148;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 149;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 150;
UPDATE aba_b2c.country SET continent = 'South America', region = 'Other', countryGroup = 'Other' WHERE id = 151;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 152;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 153;
UPDATE aba_b2c.country SET continent = 'North America', region = 'LATAM', countryGroup = 'Other' WHERE id = 154;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 155;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 156;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 157;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 158;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 159;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 160;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 161;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 162;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 163;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 164;
UPDATE aba_b2c.country SET continent = 'North America', region = 'LATAM', countryGroup = 'Other' WHERE id = 165;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 166;
UPDATE aba_b2c.country SET continent = 'South America', region = 'LATAM', countryGroup = 'Other' WHERE id = 167;
UPDATE aba_b2c.country SET continent = 'South America', region = 'LATAM', countryGroup = 'Peru' WHERE id = 168;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 169;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 170;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 171;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Portugal' WHERE id = 172;
UPDATE aba_b2c.country SET continent = 'North America', region = 'LATAM', countryGroup = 'Other' WHERE id = 173;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 174;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 175;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 176;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Russian Federation' WHERE id = 177;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 178;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 179;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 180;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 181;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 182;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 183;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 184;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'Other', countryGroup = 'Other' WHERE id = 185;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 186;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 187;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 188;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'Other', countryGroup = 'Other' WHERE id = 189;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 190;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 191;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 192;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 193;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 194;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 195;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 196;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 197;
UPDATE aba_b2c.country SET continent = 'Antarctica', region = 'Other', countryGroup = 'Other' WHERE id = 198;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Spain' WHERE id = 199;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 200;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 201;
UPDATE aba_b2c.country SET continent = 'South America', region = 'Other', countryGroup = 'Other' WHERE id = 202;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'Other', countryGroup = 'Other' WHERE id = 203;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 204;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 205;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'Other', countryGroup = 'Other' WHERE id = 206;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 207;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 208;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 209;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 210;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 211;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 212;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 213;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 214;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 215;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 216;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 217;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 218;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 219;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 220;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 221;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 222;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'Other', countryGroup = 'Other' WHERE id = 223;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 224;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'UE', countryGroup = 'Other' WHERE id = 225;
UPDATE aba_b2c.country SET continent = 'North America', region = 'North America', countryGroup = 'United States' WHERE id = 226;
UPDATE aba_b2c.country SET continent = 'South America', region = 'LATAM', countryGroup = 'Other' WHERE id = 228;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 229;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 230;
UPDATE aba_b2c.country SET continent = 'South America', region = 'LATAM', countryGroup = 'Other' WHERE id = 231;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 232;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 233;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Other', countryGroup = 'Other' WHERE id = 234;
UPDATE aba_b2c.country SET continent = 'Oceania', region = 'Other', countryGroup = 'Other' WHERE id = 235;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 236;
UPDATE aba_b2c.country SET continent = 'Asia', region = 'Other', countryGroup = 'Other' WHERE id = 237;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 238;
UPDATE aba_b2c.country SET continent = 'Africa', region = 'Other', countryGroup = 'Other' WHERE id = 239;
UPDATE aba_b2c.country SET continent = 'Unclassified', region = 'Unclassified', countryGroup = 'Unclassified' WHERE id = 300;
UPDATE aba_b2c.country SET continent = 'Unclassified', region = 'Unclassified', countryGroup = 'Unclassified' WHERE id = 301;
UPDATE aba_b2c.country SET continent = 'Unclassified', region = 'Unclassified', countryGroup = 'Unclassified' WHERE id = 302;
UPDATE aba_b2c.country SET continent = 'Unclassified', region = 'Unclassified', countryGroup = 'Unclassified' WHERE id = 303;
UPDATE aba_b2c.country SET continent = 'Unclassified', region = 'Unclassified', countryGroup = 'Unclassified' WHERE id = 304;
UPDATE aba_b2c.country SET continent = 'North America', region = 'Unclassified', countryGroup = 'Unclassified' WHERE id = 305;
UPDATE aba_b2c.country SET continent = 'Europe', region = 'Other', countryGroup = 'Other' WHERE id = 306;
UPDATE aba_b2c.country SET continent = 'Unclassified', region = 'Unclassified', countryGroup = 'Unclassified' WHERE id = 307;