// La base aba_course se ha de sibir completa, con lo que este sql no será necesario.
	ALTER TABLE `aba_course`.`section_items`
	CHANGE COLUMN `exercises_old` `exercises_old_deprecated` INT(11) NOT NULL DEFAULT '0' COMMENT 'Number of exercises old system (Deprecated)' AFTER `locale`,
	CHANGE COLUMN `exercises_new` `exercises` INT(11) NOT NULL DEFAULT '0' COMMENT 'Number of exercises new system' AFTER `exercises_old_deprecated`,
	CHANGE COLUMN `weight` `weight_deprecated` INT(11) NOT NULL DEFAULT '0' COMMENT 'Deviation from new system  (Deprecated)' AFTER `exercises`;