/* ENH-3909 */
INSERT INTO `aba_b2c`.config (`key`, `value`,`description`)
    VALUES( 'ENABLE_PREMIUM_PAY_EXTEND', '0', 'Enables the access for PREMIUM users to pay again.')
    ON DUPLICATE KEY UPDATE `value` = '0';

 ALTER TABLE `aba_b2c`.`payments` ADD COLUMN `isExtend` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'If is an extra payment from a Premium' AFTER `amountPriceWithoutTax`;