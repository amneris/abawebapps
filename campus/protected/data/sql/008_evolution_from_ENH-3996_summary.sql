
/* START  ENH- */

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`) VALUES ('FOLLOWUP_CRON_PROCESS_SUMMARY_HOURS', '2', 'Hours process followups cron');

CREATE TABLE `aba_b2c_summary`.`followup4_summary` (
`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
`progressVersion` INT(11) UNSIGNED NOT NULL DEFAULT '0',
`idFollowup` INT(11) UNSIGNED NOT NULL DEFAULT '0',
`themeid` INT(4) UNSIGNED NOT NULL DEFAULT '0',
`userid` INT(11) UNSIGNED NOT NULL DEFAULT '0',
`lastchange` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`all_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Total percentage, not used at the moment',
`all_err` INT(4) UNSIGNED NULL DEFAULT '0',
`all_ans` INT(4) UNSIGNED NULL DEFAULT '0',
`all_time` INT(4) UNSIGNED NULL DEFAULT '0',
`sit_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Situation video percentage watched, 0, 50 or 100',
`stu_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Speak',
`dic_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Sectoin Write (dictation)',
`dic_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write, help requests',
`dic_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Write wrong answers',
`rol_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Role Play percentage',
`gra_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Grammar exercises percentage done. Kind of deprecated',
`gra_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Grammar help requests',
`gra_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Grammer wrong answers',
`wri_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises percetnage',
`wri_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises help requests',
`wri_err` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Exercises wrong answers',
`new_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Vocab, new words, percentage',
`spe_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section, older users have data',
`spe_ans` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated section, older users have data',
`time_aux` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Deprecated, old uses',
`gra_vid` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'If user has watched videoclass or not. 0 or 100',
`rol_on` INT(1) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Role Play, linked to rol_por.',
`exercises` VARCHAR(25) NULL DEFAULT '0,0,0,0,0,0,0,0,0,0' COMMENT 'Exam results, always 10 exercises. Linked to eva_por',
`eva_por` INT(4) UNSIGNED NULL DEFAULT '0' COMMENT 'Section Evaluation, 0/1, to be able to dwload certificate',
PRIMARY KEY (`id`),
UNIQUE INDEX `byFollowup` (`idFollowup`, `themeid`, `userid`),
INDEX `byUserid` (`userid`),
INDEX `byProgressVersion` (`progressVersion`),
INDEX `byLastchange` (`lastchange`)
)
COLLATE='utf8_general_ci' ENGINE=InnoDB ;

/* END  ENH- */




