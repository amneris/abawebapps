/* ENH-3448, Mexico payments */
CREATE TABLE `aba_b2c`.`pay_methods` (
    `id` INTEGER(2) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Unique id',
    `name` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Name of the method payment: Paypal',
    `visibleUser` INTEGER(1) UNSIGNED NOT NULL COMMENT 'If is displayed to the user.',
    PRIMARY KEY (`id`)
  )
  ENGINE = MyISAM
  CHARACTER SET utf8 COLLATE utf8_general_ci
  COMMENT = 'Methods of payment available: paypal, card, e-money.';

INSERT INTO pay_suppliers ( id, name)
VALUES( 6, 'AllPago-MX');

INSERT INTO pay_suppliers ( id, name)
VALUES( 7, 'AllPago-BR-Boletos');

UPDATE pay_suppliers ps SET ps.name='AllPago-BR' WHERE ps.id=4;

INSERT INTO pay_methods (`id`, `name`, `visibleUser`)
  VALUES ( 1, 'Credit card', 1),
          ( 2, 'Wallet Paypal', 1),
          ( 3, 'Voucher/FREE', 1);

DROP TABLE IF EXISTS `aba_b2c`.`country_paymethod_paysupplier`;

CREATE TABLE  `aba_b2c`.`country_paymethod_paysupplier` (
  `idCountry` int(4) unsigned NOT NULL COMMENT 'FK to Country',
  `idPayMethod` int(2) unsigned NOT NULL COMMENT 'FK to pay_methods',
  `idPaySupplier` int(2) unsigned NOT NULL COMMENT 'FK to pay_suppliers',
  PRIMARY KEY  (`idCountry`,`idPayMethod`,`idPaySupplier`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Association between country-paymethod-paysupplier';

INSERT INTO `aba_b2c`.`country_paymethod_paysupplier`
   (`idCountry`, `idPayMethod`, `idPaySupplier`)
SELECT c.`id`, 1, 1 FROM country c;

INSERT INTO `aba_b2c`.`country_paymethod_paysupplier`
   (`idCountry`, `idPayMethod`, `idPaySupplier`)
SELECT c.`id`, 2, 2 FROM `aba_b2c`.`country` c;

UPDATE  country_paymethod_paysupplier cpp SET cpp.idPaySupplier = 4 WHERE cpp.idCountry=30 AND cpp.idPayMethod=1;
UPDATE  country_paymethod_paysupplier cpp SET cpp.idPaySupplier = 6 WHERE cpp.idCountry=138 AND cpp.idPayMethod=1;

ALTER TABLE `aba_b2c`.`country_paymethod_paysupplier` DROP PRIMARY KEY,
 ADD PRIMARY KEY  USING BTREE(`idCountry`, `idPayMethod`);

 INSERT INTO config (`key`, `value`,`description`)
    VALUES( 'PAY_ALLPAGO_CHANNEL_INITIAL_MX', '8a82941748ac83630148c6e271341228', 'Chanel for AllPago Mexico recurring transactions.')
    ON DUPLICATE KEY UPDATE `value`='8a82941748ac83630148c6e271341228';

INSERT INTO config (`key`, `value`,`description`)
    VALUES( 'PAY_ALLPAGO_CHANNEL_RECURRING_MX', '8a82941748ac83630148c6e39dfc1230', 'Chanel for AllPago Mexico recurring transactions.')
    ON DUPLICATE KEY UPDATE `value`='8a82941748ac83630148c6e39dfc1230';

INSERT INTO config (`key`, `value`,`description`)
    VALUES( 'PAY_ALLPAGO_CHANNEL_RG_MX', '8a82941748ac83630148c6e15cf21224', 'Chanel for AllPago Mexico RG transactions.')
    ON DUPLICATE KEY UPDATE `value`='8a82941748ac83630148c6e15cf21224';

UPDATE country c SET c.officialIdCurrency = 'MXN' WHERE c.`id`=138;
UPDATE country c SET c.ABAIdCurrency = 'MXN' WHERE c.`id`=138;
UPDATE country c SET c.decimalPoint = '.' WHERE c.`id`=138;
UPDATE currencies c SET c.decimalPoint = '.' WHERE c.`id`='MXN';
UPDATE currencies c SET c.symbol = 'MXN$' WHERE c.`id`='MXN';

ALTER TABLE `aba_b2c_logs`.`log_allpago` ADD COLUMN `idPaySupplier` INTEGER(3) UNSIGNED DEFAULT NULL COMMENT 'FK to pay_suppliers, to know which gateway supplier is.' AFTER `success`
, ROW_FORMAT = FIXED;

UPDATE aba_b2c_logs.log_allpago l SET l.idPaySupplier = 4 ;

UPDATE products_prices p SET p.priceOfficialCry=399.99, p.priceExtranet=399.99 WHERE p.idProduct = '138_30';
UPDATE products_prices p SET p.priceOfficialCry=1199.97, p.priceExtranet=1199.97  WHERE p.idProduct = '138_90';
UPDATE products_prices p SET p.priceOfficialCry=1999.99, p.priceExtranet=1999.99 WHERE p.idProduct = '138_180';
UPDATE products_prices p SET p.priceOfficialCry=3499.99, p.priceExtranet=3499.99 WHERE p.idProduct = '138_360';
UPDATE products_prices p SET p.priceOfficialCry=4099.99, p.priceExtranet=4099.99 WHERE p.idProduct = '138_540';
UPDATE products_prices p SET p.priceOfficialCry=4499.99, p.priceExtranet=4499.99 WHERE p.idProduct = '138_720';

INSERT INTO config (`key`, `value`,`description`)
        VALUES( 'ENABLE_PAY_ALLPAGO_MX', '0', 'If we enable payments with AllPago for Mexican Users.')
        ON DUPLICATE KEY UPDATE `value`='0';

DELETE c.* FROM aba_b2c.`config` c WHERE c.`key`='PAY_ALLPAGO_COUNTRIES';

ALTER TABLE `aba_b2c`.`user_address_invoice` MODIFY COLUMN `zipCode` VARCHAR(14) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Postal code of the user';
