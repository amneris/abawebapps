
-- gatoamarilloverdoso1
-- TEST EUR
-- INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`) VALUES ('PAY_KEY_LACAIXA_GW_EUR_SHA256', 'sq7HjrUOBfKmC576ILgskD5srU870gJ7', 'Test. Nueva clave de comercio SHA-256 EUR');
-- PROD EUR
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`) VALUES ('PAY_KEY_LACAIXA_GW_EUR_SHA256', '6halQGyl7B8+cvc3JRobo2NLGQbwIFcR', 'Prod. Nueva clave de comercio SHA-256 EUR');

-- 221N20N08U006175
-- TEST USD
-- INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`) VALUES ('PAY_KEY_LACAIXA_GW_USD_SHA256', 'sq7HjrUOBfKmC576ILgskD5srU870gJ7', 'Test. Nueva clave de comercio SHA-256 USD');
-- PROD USD
INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`) VALUES ('PAY_KEY_LACAIXA_GW_USD_SHA256', 'uCczTwn8hkHtYLK0O+fGnWf9PFR3hT6b', 'Prod. Nueva clave de comercio SHA-256 USD');

INSERT INTO `aba_b2c`.`config` (`key`, `value`, `description`) VALUES ('PAY_LACAIXA_SIGNATUREVERSION', 'HMAC_SHA256_V1', 'Versión de la firma');


