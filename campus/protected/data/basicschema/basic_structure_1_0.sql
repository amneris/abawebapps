CREATE DATABASE `aba_b2c` /*!40100 DEFAULT CHARACTER SET latin1 */;

DROP TABLE IF EXISTS `aba_b2c`.`aba_partners_list`;
CREATE TABLE  `aba_b2c`.`aba_partners_list` (
  `idPartner` int(3) unsigned NOT NULL auto_increment COMMENT 'internal id',
  `name` varchar(60) NOT NULL,
  `idPartnerGroup` int(3) unsigned NOT NULL default '0' COMMENT '0-Aba English., 1-Ventas Flash, 2-Others.',
  PRIMARY KEY  (`idPartner`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COMMENT='List of groupones and partners that sells courses';

DROP TABLE IF EXISTS `aba_b2c`.`config`;
CREATE TABLE  `aba_b2c`.`config` (
  `key` varchar(100) collate utf8_bin NOT NULL,
  `value` text collate utf8_bin,
  `description` varchar(250) collate utf8_bin default NULL,
  `dateUpdate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='Tabla creada por Quino. Para configurar la aplicación.';

DROP TABLE IF EXISTS `aba_b2c`.`country`;
CREATE TABLE  `aba_b2c`.`country` (
  `id` int(4) NOT NULL auto_increment,
  `days` varchar(7) NOT NULL default '',
  `ID_month` int(4) NOT NULL default '0',
  `iso` char(2) NOT NULL default '',
  `name_m` varchar(100) NOT NULL default '',
  `name` varchar(100) NOT NULL default '',
  `iso3` char(3) default NULL,
  `numcode` smallint(6) default '0',
  `officialIdCurrency` varchar(3) NOT NULL COMMENT 'In near future should be replaced by the other one(2012-12-31)',
  `officialIdCurrency_tmpDisabled` varchar(3) NOT NULL default 'EUR' COMMENT 'Original currency for this country. Default for products_prices',
  `ABAIdCurrency` varchar(3) NOT NULL default 'EUR' COMMENT 'Default currency for transactions and payments',
  `ABALanguage` varchar(3) NOT NULL default 'en' COMMENT '639-1,Default Language for users for web',
  PRIMARY KEY  USING BTREE (`id`),
  KEY `By_name` (`name`),
  KEY `By_name_m` (`name_m`)
) ENGINE=MyISAM AUTO_INCREMENT=241 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `aba_b2c`.`course_var4`;
CREATE TABLE  `aba_b2c`.`course_var4` (
  `ID_var` int(4) unsigned NOT NULL auto_increment,
  `userid` int(4) unsigned NOT NULL default '0',
  `name` varchar(25) NOT NULL default '',
  `value` varchar(200) NOT NULL default '',
  PRIMARY KEY  (`ID_var`),
  KEY `index_var` USING BTREE (`userid`,`name`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `aba_b2c`.`currencies`;
CREATE TABLE  `aba_b2c`.`currencies` (
  `id` varchar(3) NOT NULL COMMENT 'ISO 4217',
  `extCode` varchar(3) NOT NULL COMMENT 'Supplier codes, usually ISO same as Google',
  `description` varchar(100) default NULL,
  `symbol` varchar(5) default NULL,
  `unitEur` decimal(14,10) NOT NULL,
  `outEur` decimal(14,10) NOT NULL,
  `unitUsd` decimal(14,10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='List of available world currencies and exchange rates versus';

DROP TABLE IF EXISTS `aba_b2c`.`followup_html4`;
CREATE TABLE  `aba_b2c`.`followup_html4` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `userid` int(10) unsigned NOT NULL,
  `themeid` int(10) unsigned NOT NULL,
  `page` int(10) unsigned NOT NULL,
  `section` varchar(20) NOT NULL,
  `action` varchar(20) NOT NULL,
  `audio` varchar(20) NOT NULL,
  `bien` tinyint(1) NOT NULL,
  `mal` int(10) unsigned NOT NULL,
  `lastchange` datetime NOT NULL,
  `wtext` varchar(100) NOT NULL,
  `difficulty` int(10) unsigned NOT NULL,
  `time_aux` int(4) unsigned NOT NULL,
  `contador` int(10) unsigned NOT NULL,
  PRIMARY KEY  USING BTREE (`id`),
  KEY `lastchange_index` USING BTREE (`lastchange`),
  KEY `INX1` USING BTREE (`userid`,`themeid`,`section`,`audio`),
  KEY `INX_learner` USING BTREE (`userid`,`themeid`),
  KEY `IDs_lastchange_index` USING BTREE (`userid`,`themeid`,`lastchange`)
) ENGINE=MyISAM AUTO_INCREMENT=2084 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `aba_b2c`.`followup4`;
CREATE TABLE  `aba_b2c`.`followup4` (
  `id` int(4) unsigned NOT NULL auto_increment,
  `themeid` int(4) unsigned NOT NULL default '0',
  `userid` int(4) unsigned NOT NULL default '0',
  `lastchange` datetime NOT NULL default '0000-00-00 00:00:00',
  `all_por` int(4) unsigned default '0',
  `all_err` int(4) unsigned default '0',
  `all_ans` int(4) unsigned default '0',
  `all_time` int(4) unsigned default '0',
  `sit_por` int(4) unsigned default '0',
  `stu_por` int(4) unsigned default '0',
  `dic_por` int(4) unsigned default '0',
  `dic_ans` int(4) unsigned default '0',
  `dic_err` int(4) unsigned default '0',
  `rol_por` int(4) unsigned default '0',
  `gra_por` int(4) unsigned default '0',
  `gra_ans` int(4) unsigned default '0',
  `gra_err` int(4) unsigned default '0',
  `wri_por` int(4) unsigned default '0',
  `wri_ans` int(4) unsigned default '0',
  `wri_err` int(4) unsigned default '0',
  `new_por` int(4) unsigned default '0',
  `spe_por` int(4) unsigned default '0',
  `spe_ans` int(4) unsigned default '0',
  `time_aux` int(4) unsigned default '0',
  `gra_vid` int(1) unsigned default '0',
  `rol_on` int(1) unsigned default '0',
  `exercises` varchar(25) NOT NULL default '0,0,0,0,0,0,0,0,0,0',
  `eva_por` int(4) unsigned NOT NULL default '0',
  PRIMARY KEY  USING BTREE (`id`),
  KEY `learner_theme_index` USING BTREE (`userid`,`themeid`)
) ENGINE=MyISAM AUTO_INCREMENT=128 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `aba_b2c`.`log_user_activity`;
CREATE TABLE  `aba_b2c`.`log_user_activity` (
  `userId` int(10) unsigned NOT NULL COMMENT 'FK to users',
  `id` int(10) unsigned NOT NULL auto_increment,
  `time` datetime NOT NULL,
  `table` varchar(45) NOT NULL COMMENT 'LIKE a FK to a table name',
  `fields` text COMMENT 'LIKE a FK to a fields name',
  `description` varchar(45) NOT NULL,
  `action` varchar(45) NOT NULL COMMENT 'Operation on Campus, payment, Crons, etc.',
  `ip` varchar(45) default NULL COMMENT 'REMOTE IP',
  PRIMARY KEY  (`id`),
  KEY `byTime` (`time`),
  KEY `byUserId` (`userId`)
) ENGINE=MyISAM AUTO_INCREMENT=708 DEFAULT CHARSET=utf8 COMMENT='Logs fields related with user, states, levels, payments, etc';

DROP TABLE IF EXISTS `aba_b2c`.`messages`;
CREATE TABLE  `aba_b2c`.`messages` (
  `id` int(11) NOT NULL auto_increment,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `subject` varchar(256) NOT NULL default '',
  `body` text,
  `is_read` enum('0','1') NOT NULL default '0',
  `deleted_by` enum('sender','receiver') default NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `sender` (`sender_id`),
  KEY `reciever` (`receiver_id`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `aba_b2c`.`pay_gateway_response`;
CREATE TABLE  `aba_b2c`.`pay_gateway_response` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `idPayment` varchar(15) NOT NULL,
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `Order_number` varchar(15) NOT NULL COMMENT 'FK to the supplier order id. It has a suffix',
  `Codigo` varchar(45) NOT NULL,
  `Amount` varchar(45) NOT NULL,
  `Currency` varchar(45) NOT NULL,
  `Signature` varchar(45) NOT NULL,
  `MerchantCode` varchar(45) NOT NULL,
  `Terminal` varchar(45) NOT NULL,
  `Response` varchar(45) NOT NULL,
  `AuthorisationCode` varchar(45) NOT NULL,
  `TransactionType` varchar(45) NOT NULL,
  `SecurePayment` varchar(45) NOT NULL,
  `MerchantData` varchar(45) NOT NULL,
  `XML` text NOT NULL,
  PRIMARY KEY  USING BTREE (`id`),
  KEY `byIdPayDate` (`idPayment`,`fecha`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `aba_b2c`.`pay_gateway_sent`;
CREATE TABLE  `aba_b2c`.`pay_gateway_sent` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `idPayment` varchar(15) NOT NULL,
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `Order_number` varchar(15) NOT NULL,
  `Currency` int(10) unsigned NOT NULL,
  `TransactionType` varchar(45) NOT NULL,
  `MerchantData` varchar(45) NOT NULL,
  `Amount` varchar(45) NOT NULL,
  `MerchantName` varchar(45) NOT NULL,
  `MerchantSignature` varchar(45) NOT NULL,
  `MerchantCode` varchar(45) NOT NULL,
  `CreditTarget` varchar(45) NOT NULL,
  `ExpiryDate` varchar(45) NOT NULL,
  `Cvv2` varchar(45) NOT NULL,
  `XML` text NOT NULL,
  PRIMARY KEY  USING BTREE (`id`),
  KEY `byPaymentDate` (`idPayment`,`fecha`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `aba_b2c`.`pay_suppliers`;
CREATE TABLE  `aba_b2c`.`pay_suppliers` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='List of suppliers to execute payments transactions';

DROP TABLE IF EXISTS `aba_b2c`.`payments`;
CREATE TABLE  `aba_b2c`.`payments` (
  `id` varchar(15) NOT NULL,
  `idUserProdStamp` int(10) unsigned default NULL COMMENT 'FK to user_prodcuts_stamp.',
  `userId` int(4) unsigned NOT NULL COMMENT 'FK to users',
  `idProduct` varchar(15) NOT NULL COMMENT 'FK mixed to user_products_stamp',
  `idCountry` int(4) unsigned NOT NULL COMMENT 'FK mixed to user_products_stamp',
  `idPeriodPay` int(5) unsigned NOT NULL COMMENT 'FK mixed to user_products_stamp',
  `idUserCreditForm` int(10) unsigned NOT NULL COMMENT 'FK linked to user_credit_forms',
  `paySuppExtId` int(2) unsigned NOT NULL COMMENT 'Link to Services payments suppliers 1 La Caixa, 2 PayPal',
  `paySuppOrderId` varchar(60) default NULL COMMENT 'Id transaction of external gateway payment',
  `status` int(2) unsigned NOT NULL default '0' COMMENT '0-PENDING,10-FAIL, 30-SUCCESS, 40-REFUND',
  `dateStartTransaction` datetime NOT NULL COMMENT 'Time of INSERT, creation of the transaction',
  `dateEndTransaction` datetime default NULL COMMENT 'Time of the end of execution of this Transaction',
  `dateToPay` datetime default NULL COMMENT 'Future date for PENDING STATUS only',
  `xRateToEUR` decimal(18,9) NOT NULL COMMENT 'Exchange rate of  EUR cur transaction ',
  `xRateToUSD` decimal(18,9) NOT NULL COMMENT 'Exchange rate of  USD cur transaction ',
  `currencyTrans` varchar(3) NOT NULL default 'EUR' COMMENT 'FK to currencies',
  `foreignCurrencyTrans` varchar(3) NOT NULL COMMENT 'FK to currencies, the currency of the country',
  `idPromoCode` varchar(50) default NULL COMMENT 'FK to products_promo',
  `amountOriginal` decimal(18,9) default NULL COMMENT 'Original price before discount.',
  `amountDiscount` decimal(18,9) default NULL COMMENT 'Amount discounted. NO PERCENTAGE',
  `amountPrice` decimal(18,9) NOT NULL COMMENT 'Total final amount.',
  `idPayControlCheck` varchar(15) NOT NULL COMMENT 'FK to payments control check',
  `idPartner` int(3) unsigned default NULL COMMENT 'FK to aba_partner_list, to know which partner was involved in the transaction.',
  `paySuppExtProfId` varchar(45) default NULL COMMENT 'Id of the profile ni the payment gateway. PayPal recurring Profile Id.',
  PRIMARY KEY  (`id`),
  KEY `byProduct` (`idProduct`),
  KEY `byProductPrices` (`idProduct`,`idCountry`,`idPeriodPay`),
  KEY `byStatus` (`status`),
  KEY `byDateToPay` (`dateToPay`),
  KEY `byIdPromoCode` (`idPromoCode`),
  KEY `byUserProduct` USING BTREE (`idUserProdStamp`),
  KEY `byIdPayControlCheck` (`idPayControlCheck`),
  KEY `byPaySuppOrderId` (`paySuppOrderId`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Transactions for each payment or cancellation';

DROP TABLE IF EXISTS `aba_b2c`.`payments_control_check`;
CREATE TABLE  `aba_b2c`.`payments_control_check` (
  `id` varchar(15) NOT NULL COMMENT 'UserId + IdProduct + IdPeriodPay + timeYYYYMMDDHHNNSS',
  `idUserProdStamp` int(10) unsigned default NULL COMMENT 'FK to user_prodcuts_stamp.',
  `userId` int(4) unsigned NOT NULL COMMENT 'FK to users',
  `idProduct` varchar(15) NOT NULL COMMENT 'FK mixed to user_products_stamp',
  `idCountry` int(4) unsigned NOT NULL COMMENT 'FK mixed to user_products_stamp',
  `idPeriodPay` int(5) unsigned NOT NULL COMMENT 'FK mixed to user_products_stamp',
  `paySuppExtId` int(2) unsigned NOT NULL COMMENT 'Link to Services payments suppliers',
  `paySuppOrderId` varchar(60) default NULL COMMENT 'Id transaction of external gateway payment',
  `status` int(2) unsigned NOT NULL default '0' COMMENT '5-INITIATED,10-FAIL, 30-SUCCESS',
  `dateStartTransaction` datetime NOT NULL COMMENT 'Time of INSERT, creation of the transaction',
  `dateEndTransaction` datetime default NULL COMMENT 'Time of the end of execution of this Transaction',
  `dateToPay` datetime default NULL COMMENT 'Future date for PENDING STATUS only',
  `xRateToEUR` decimal(18,9) NOT NULL COMMENT 'Exchange rate of  EUR cur transaction ',
  `xRateToUSD` decimal(18,9) NOT NULL COMMENT 'Exchange rate of  USD cur transaction ',
  `currencyTrans` varchar(3) NOT NULL default 'EUR' COMMENT 'FK to currencies',
  `foreignCurrencyTrans` varchar(3) NOT NULL COMMENT 'FK to currencies, the currency of the country',
  `idPromoCode` varchar(50) default NULL COMMENT 'FK to products_promo',
  `amountOriginal` decimal(18,9) default NULL COMMENT 'Original price before discount.',
  `amountDiscount` decimal(18,9) default NULL COMMENT 'Amount discounted. NO PERCENTAGE',
  `amountPrice` decimal(18,9) NOT NULL COMMENT 'Total final amount.',
  `kind` int(2) unsigned NOT NULL COMMENT 'KIND_CC_VISA=0;KIND_CC_MASTERCARD=1;KIND_CC_AMERICAN_EXPRESS=2;KIND_CC_DINERS_CLUB=3;KIND_PAYPAL=10',
  `cardNumber` blob,
  `cardYear` blob,
  `cardMonth` blob,
  `cardCvc` blob,
  `cardName` varchar(45) default NULL,
  `tokenPayPal` varchar(45) NOT NULL default '' COMMENT 'Token returned by PAYPAL after first checkout.',
  `payerPayPalId` varchar(45) NOT NULL default '' COMMENT 'Id of the payer returned by PAYPAL after first checkout',
  `lastAction` text,
  PRIMARY KEY  (`id`),
  KEY `byProduct` (`idProduct`),
  KEY `byProductPrices` (`idProduct`,`idCountry`,`idPeriodPay`),
  KEY `byStatus` (`status`),
  KEY `byDateToPay` (`dateToPay`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='controls transactions for each payment';

DROP TABLE IF EXISTS `aba_b2c`.`paypal_log`;
CREATE TABLE  `aba_b2c`.`paypal_log` (
  `a` int(10) unsigned NOT NULL auto_increment,
  `status` varchar(255) character set latin1 NOT NULL,
  `receiver_email` varchar(255) character set latin1 NOT NULL,
  `payer_email` varchar(255) character set latin1 NOT NULL,
  `txn_type` varchar(255) character set latin1 NOT NULL,
  `subscr_id` varchar(255) character set latin1 NOT NULL,
  `username` varchar(255) character set latin1 NOT NULL,
  `tipopago` varchar(255) character set latin1 NOT NULL,
  `recomendacion` varchar(255) character set latin1 NOT NULL,
  `cp` varchar(255) character set latin1 NOT NULL,
  `producto` varchar(15) NOT NULL,
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`a`)
) ENGINE=MyISAM AUTO_INCREMENT=3751 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS `aba_b2c`.`paypal_subscription_info`;
CREATE TABLE  `aba_b2c`.`paypal_subscription_info` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `status` varchar(10) character set latin1 NOT NULL,
  `subscr_id` varchar(255) NOT NULL COMMENT 'PROFILE ID of PAYPAL',
  `sub_event` varchar(50) character set latin1 NOT NULL default '',
  `subscr_date` varchar(255) character set latin1 NOT NULL default '',
  `subscr_effective` varchar(255) character set latin1 NOT NULL default '',
  `period1` varchar(255) character set latin1 NOT NULL default '',
  `period2` varchar(255) character set latin1 NOT NULL default '',
  `period3` varchar(255) character set latin1 NOT NULL default '',
  `amount1` varchar(255) character set latin1 NOT NULL default '',
  `amount2` varchar(255) character set latin1 NOT NULL default '',
  `amount3` varchar(255) character set latin1 NOT NULL default '',
  `mc_amount1` varchar(255) character set latin1 NOT NULL default '',
  `mc_amount2` varchar(255) character set latin1 NOT NULL default '',
  `mc_amount3` varchar(255) character set latin1 NOT NULL default '',
  `currency` varchar(10) character set latin1 NOT NULL,
  `recurring` varchar(255) character set latin1 NOT NULL default '',
  `reattempt` varchar(255) character set latin1 NOT NULL default '',
  `retry_at` varchar(255) character set latin1 NOT NULL default '',
  `recur_times` varchar(255) character set latin1 NOT NULL default '',
  `username` varchar(255) character set latin1 NOT NULL default '',
  `password` varchar(255) character set latin1 default NULL,
  `payment_txn_id` varchar(50) character set latin1 NOT NULL default '',
  `subscriber_emailaddress` varchar(255) character set latin1 NOT NULL default '',
  `datecreation` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `aba_b2c`.`prod_periodicity`;
CREATE TABLE  `aba_b2c`.`prod_periodicity` (
  `id` int(4) unsigned NOT NULL auto_increment,
  `days` int(4) unsigned NOT NULL COMMENT 'Quantity of days between payments(it is IGNORED).',
  `months` int(2) unsigned NOT NULL COMMENT 'possible values: 1,3,6,9,12 (IS USED)',
  `descriptionText` varchar(20) NOT NULL COMMENT 'MONTHLY, YEAR, 3months, etc',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1001 DEFAULT CHARSET=utf8 COMMENT='Frequency or Methods of payment';

DROP TABLE IF EXISTS `aba_b2c`.`products_list_20121023`;
CREATE TABLE  `aba_b2c`.`products_list_20121023` (
  `id` varchar(10) NOT NULL COMMENT 'PREMIUM, PLUS, FOREVER, etc.',
  `descriptionText` varchar(20) NOT NULL COMMENT 'description in English. Links to translations',
  `userType` int(2) unsigned default NULL COMMENT 'Correspondence with UserAccess:0-lead,Free-1,Premium-2,Plus-3, Teacher-4',
  `hierarchy` int(2) unsigned NOT NULL COMMENT 'Just to set an order quality, precedence',
  `visibleWeb` tinyint(1) default '1' COMMENT 'If it is a public product or only for special purposes.',
  PRIMARY KEY  (`id`),
  KEY `byUserType` (`userType`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Generic list of products availableand its details';

DROP TABLE IF EXISTS `aba_b2c`.`products_prices`;
CREATE TABLE  `aba_b2c`.`products_prices` (
  `idProduct` varchar(15) NOT NULL COMMENT 'foreign key to products_list',
  `idCountry` int(4) unsigned NOT NULL COMMENT 'foreign key to countries. Default country 0',
  `idPeriodPay` int(5) unsigned NOT NULL COMMENT 'foreign keys to periodicities(also quantity of days)',
  `priceOfficialCry` decimal(18,4) NOT NULL COMMENT 'Price in the original currency of idCountry:countries.officialIdCurrency',
  `enabled` tinyint(1) NOT NULL default '1' COMMENT 'If is active or not this price',
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP COMMENT 'Date this record is accessed and modified.',
  `descriptionText` varchar(20) default NULL COMMENT 'It should be as same as product description with periodicity.',
  `userType` int(2) unsigned default NULL COMMENT 'Correspondence with UserAccess:0-lead,Free-1,Premium-2,Plus-3, Teacher-4',
  `hierarchy` int(2) unsigned NOT NULL COMMENT 'Just to set an order quality, precedence',
  `visibleWeb` tinyint(1) default '1' COMMENT 'If it is a public product or only for special purposes.',
  PRIMARY KEY  USING BTREE (`idProduct`),
  KEY `byCountry` (`idCountry`),
  KEY `byIdPeriodPay` (`idPeriodPay`),
  KEY `byVisibleWeb` (`visibleWeb`),
  KEY `byHierarchy` (`hierarchy`),
  KEY `byEnabled` (`enabled`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Linked to Product. List of all plans of prices';

DROP TABLE IF EXISTS `aba_b2c`.`products_promos`;
CREATE TABLE  `aba_b2c`.`products_promos` (
  `idPromoCode` varchar(50) character set utf8 NOT NULL COMMENT 'Name Promo',
  `idProduct` varchar(15) character set utf8 NOT NULL COMMENT 'foreign key to products_list',
  `idPartner` int(3) unsigned default NULL COMMENT 'FK to aba_partners,In case it belongs to a Groupon',
  `descriptionText` varchar(20) character set utf8 default NULL COMMENT 'Link to text_link, it s a translation id',
  `type` enum('PERCENTAGE','FINALPRICE','AMOUNT') NOT NULL default 'PERCENTAGE' COMMENT 'Type of discount',
  `value` decimal(18,5) NOT NULL,
  `dateStart` date default NULL COMMENT 'Date since will be active',
  `dateEnd` date default NULL COMMENT 'Date since will be no valid anymore',
  `enabled` tinyint(1) NOT NULL default '1' COMMENT 'If it''s active or not',
  `visibleWeb` tinyint(1) NOT NULL default '0' COMMENT 'If it will be displayed for any user in the payment form',
  PRIMARY KEY  (`idPromoCode`),
  KEY `byPartner` (`idPartner`),
  KEY `byDateStart` (`dateStart`),
  KEY `byEnabled` (`enabled`),
  KEY `byProduct` (`idProduct`),
  KEY `byVisibility` (`visibleWeb`),
  KEY `byDateEnd` (`dateEnd`),
  KEY `byUniEnabledProd` USING BTREE (`enabled`,`idProduct`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `aba_b2c`.`texts_list`;
CREATE TABLE  `aba_b2c`.`texts_list` (
  `id` varchar(20) NOT NULL COMMENT 'key Text, linked to all descriptions in this database',
  `category` varchar(32) default NULL COMMENT 'Grammar, Messages, My profile, etc.',
  `message` text COMMENT 'Default translation',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `aba_b2c`.`texts_translations`;
CREATE TABLE  `aba_b2c`.`texts_translations` (
  `idText` varchar(20) NOT NULL default '0' COMMENT 'key text linked to text_list.',
  `language` varchar(3) NOT NULL COMMENT 'ISO language code of 2 digits',
  `translation` text COMMENT 'Final text value.',
  PRIMARY KEY  USING BTREE (`idText`,`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Translations for each text_list row, linked to texts';

DROP TABLE IF EXISTS `aba_b2c`.`user`;
CREATE TABLE  `aba_b2c`.`user` (
  `id` int(4) NOT NULL auto_increment,
  `name` varchar(200) NOT NULL default '',
  `surnames` varchar(200) NOT NULL default '',
  `email` varchar(200) NOT NULL default '',
  `password` varchar(50) NOT NULL default '',
  `passwordFlashMedia` varchar(50) default NULL COMMENT 'Password to access the Media server',
  `complete` char(3) NOT NULL default 'NO',
  `city` varchar(200) default NULL COMMENT 'literal of the name of the City',
  `countryId` int(4) NOT NULL default '0',
  `birthDate` datetime default NULL COMMENT 'date of birth',
  `telephone` varchar(20) default NULL COMMENT 'telephone of the user',
  `langEnv` varchar(5) NOT NULL default 'es' COMMENT 'Language of the Campus.',
  `langCourse` varchar(5) NOT NULL default 'en' COMMENT 'Always English at the moment',
  `userType` int(2) unsigned NOT NULL COMMENT '0-lead,Free-1,Premium-2,Plus-3, Teacher-4 ',
  `currentLevel` int(5) unsigned NOT NULL default '1' COMMENT '1-Beginners,2-Lower intermediate, 3-Intermediate,  4-Upper intermediate,5-Advanced, 6-Bussiness',
  `entryDate` datetime default NULL COMMENT 'Fecha de alta en ABA English',
  `expirationDate` datetime NOT NULL default '0000-00-00 00:00:00' COMMENT 'When last payment grants access to campus.',
  `teacherId` int(4) unsigned NOT NULL default '0' COMMENT 'Its Teacher',
  `idPartnerFirstPay` int(3) unsigned default NULL COMMENT 'FK to aba_partners_list, through which partner came the payment.',
  `firstLoginDone` tinyint(1) NOT NULL default '0' COMMENT 'Just to inform that first login has been done at some time.',
  `releaseCampus` int(10) unsigned default '4' COMMENT 'To identify if user comes from older campus releases.',
  `idPartnerSource` int(3) unsigned default NULL COMMENT 'The source from where the user was created (LEAD)',
  `idPartnerCurrent` int(3) unsigned default NULL COMMENT 'The active partner associated with this user. Data duplicated in payments.idPartner',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `byTeacher` (`teacherId`),
  KEY `byExpiration` (`expirationDate`),
  KEY `byCountryId` (`countryId`),
  KEY `byIdPartner` USING BTREE (`idPartnerFirstPay`)
) ENGINE=MyISAM AUTO_INCREMENT=300009 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `aba_b2c`.`user_access_20121005`;
CREATE TABLE  `aba_b2c`.`user_access_20121005` (
  `userId` int(4) NOT NULL COMMENT 'FK to user',
  `langEnv` varchar(5) NOT NULL default 'es' COMMENT 'Language of Campus',
  `langCourse` varchar(5) NOT NULL COMMENT 'Always English at the moment',
  `userType` int(2) unsigned NOT NULL COMMENT '0-lead,Free-1,Premium-2,Plus-3, Teacher-4 ',
  `currentLevel` int(10) unsigned NOT NULL default '0' COMMENT 'Level of English',
  `expirationDate` datetime NOT NULL default '0000-00-00 00:00:00' COMMENT 'When last payment grants access to campus.',
  `teacherId` int(4) unsigned NOT NULL default '0' COMMENT 'Its Teacher',
  `entryDate` datetime default '0000-00-00 00:00:00' COMMENT 'Fecha de alta en ABA English',
  `idPartnerFirstPay` int(3) unsigned NOT NULL COMMENT 'FK to aba_partners_list',
  PRIMARY KEY  (`userId`),
  UNIQUE KEY `ID_learner` USING BTREE (`userId`),
  KEY `byTeacher` (`teacherId`),
  KEY `byFirstPaySeller` USING BTREE (`idPartnerFirstPay`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `aba_b2c`.`user_credit_forms`;
CREATE TABLE  `aba_b2c`.`user_credit_forms` (
  `userId` int(4) unsigned NOT NULL COMMENT 'FK to user',
  `id` int(10) unsigned NOT NULL auto_increment COMMENT 'old ID CT',
  `kind` int(2) unsigned NOT NULL COMMENT 'KIND_CC_VISA=0;KIND_CC_MASTERCARD=1;KIND_CC_AMERICAN_EXPRESS=2;KIND_CC_DINERS_CLUB=3;KIND_PAYPAL=10',
  `cardNumber` blob,
  `cardYear` blob,
  `cardMonth` blob,
  `cardCvc` blob,
  `cardName` varchar(45) default NULL,
  `default` tinyint(1) default '1' COMMENT 'If the user has many, this one is the active one',
  PRIMARY KEY  (`id`),
  KEY `byUserId` (`userId`),
  KEY `byUniUserIdDef` USING BTREE (`userId`,`default`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COMMENT='Credit cards or payment methods of a user';

DROP TABLE IF EXISTS `aba_b2c`.`user_history_state`;
CREATE TABLE  `aba_b2c`.`user_history_state` (
  `userId` int(4) NOT NULL,
  `userType` int(2) unsigned NOT NULL COMMENT '0-lead,Free-1,Premium-2,',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `idPartner` int(4) unsigned NOT NULL COMMENT 'FK to aba_partners_list, which partner was involved in the change status.',
  PRIMARY KEY  USING BTREE (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `aba_b2c`.`user_objective`;
CREATE TABLE  `aba_b2c`.`user_objective` (
  `userId` int(4) unsigned NOT NULL COMMENT 'user id FK to users',
  `id` int(10) unsigned NOT NULL auto_increment,
  `travel` tinyint(1) unsigned default NULL,
  `estudy` tinyint(1) unsigned default NULL,
  `work` tinyint(1) unsigned default NULL,
  `level` tinyint(2) unsigned default NULL,
  `selEngPrev` varchar(50) default NULL,
  `engPrev` tinyint(2) unsigned default NULL,
  `dedication` varchar(50) default NULL,
  `birthDate` varchar(50) default NULL,
  `gender` char(1) default NULL,
  `others` tinyint(1) default NULL,
  PRIMARY KEY  USING BTREE (`id`),
  UNIQUE KEY `byUniUserIdObjec` USING BTREE (`userId`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='answers to objectives for each user';

DROP TABLE IF EXISTS `aba_b2c`.`user_products_stamp`;
CREATE TABLE  `aba_b2c`.`user_products_stamp` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `userId` int(4) default NULL,
  `idProduct` varchar(15) NOT NULL COMMENT 'FK to product_list',
  `idCountry` int(4) unsigned NOT NULL COMMENT 'FK to countries',
  `idPeriodPay` int(5) unsigned NOT NULL COMMENT 'foreign keys to prod_modes_pay(quantity of days)',
  `priceOfficialCry` decimal(10,4) NOT NULL COMMENT 'Price in the original currency of idCountry',
  `idCurrency` varchar(3) NOT NULL COMMENT 'FK to currencies',
  `dateCreation` datetime NOT NULL COMMENT 'Date of creation',
  `idPromoCode` varchar(50) default NULL COMMENT 'FK to products_promotion applied',
  `upToDatePaid` tinyint(1) default '0' COMMENT 'It says if it up to Date with the payment',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `ByUniPurchase` (`userId`,`idProduct`,`idCountry`,`idPeriodPay`,`priceOfficialCry`,`idCurrency`),
  KEY `byIdProduct` (`idProduct`),
  KEY `byCountry` (`idCountry`),
  KEY `byProductPrices` (`idProduct`,`idCountry`,`idPeriodPay`),
  KEY `ByUserProduct` (`userId`,`idProduct`,`idCountry`,`idPeriodPay`),
  KEY `byActiveProduct` USING BTREE (`idProduct`,`upToDatePaid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Prices at the moment of the first purchase of the user';

DROP TABLE IF EXISTS `aba_b2c`.`user_teacher`;
CREATE TABLE  `aba_b2c`.`user_teacher` (
  `userid` int(4) NOT NULL default '0',
  `nationality` varchar(100) default NULL,
  `language` varchar(200) NOT NULL default '',
  `photo` varchar(200) NOT NULL default '',
  PRIMARY KEY  (`userid`),
  KEY `byLanguage` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;