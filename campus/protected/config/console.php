<?php
/*
 * Override the configuration of Web, the one in main.php. It deletes some elements because otherwise it raises errors.
 *
 * */

$aFixArrayForConsole = CMap::mergeArray(
    require(dirname(__FILE__).'/main.php'),
    array(
        'commandPath'=> ROOT_DIR.DIRECTORY_SEPARATOR.'campus'.DIRECTORY_SEPARATOR.'protected'.DIRECTORY_SEPARATOR.'commands'.DIRECTORY_SEPARATOR.'crons',
    )
);

unset($aFixArrayForConsole["theme"]);
unset($aFixArrayForConsole["behaviors"]);

$aFixArrayForConsole['import'][]='application.commands.crons.*';
$aFixArrayForConsole['import'][]='common.protected.models.reconciliationParser.*';
$aFixArrayForConsole['import'][]='common.protected.rules.reconciliationParser.*';

$aFixArrayForConsole['import'][]='application.extlibraries.vendors.emma.*';

$aFixArrayForConsole["components"]['log']=array(
                                            'class'=>'CLogRouter',
                                            'routes'=>array(
                                                array(
                                                    'class'=>'CFileLogRoute',
                                                    'logFile'=>'cron.log',
                                                    'levels'=>'error, warning',
                                                ),
                                                array(
                                                    'class'=>'CFileLogRoute',
                                                    'logFile'=>'cron_trace.log',
                                                    'levels'=>'error, warning',
                                                ),
                                            ),
                                        );

return $aFixArrayForConsole;