<?php

/**
 * Enhancement #5443 - http://redmine.abaenglish.com/issues/5443
 *
 * Todas las configuraciones de config que no se necesiten recargar en caliente deberían estar aquí y deberíamos usar
 * Yii::app()->params en lugar de Yii::app()->config
 *
 */
return array(

  'components' => array(
    'user' => array(
      /**
       * WARN: RFC 2109
       *
       * Host A's name domain-matches host B's if
       *
       * A is a FQDN string and has the form NB, where N is a non-empty name string, B has the form .B', and B'
       * is a FQDN string.  (So, x.y.com domain-matches .y.com but not y.com.)
       *
       * https://www.ietf.org/rfc/rfc2109.txt
       */
      'identityCookie' => array(
        'domain' => '.abaenglish.com' //note dot before domain name
      ),
    ),
    'cache' => array(
      'class' => 'system.caching.CMemCache',
    ),
    'config' => array(
      'class' => 'common.protected.extensions.econfig.EConfig',
      'cacheID' => 'cache'
    ),
  ),
  'params' => array(
    'glossaryUrl' => '//api.abaenglish.com/glossary/search',
    'transactionsUrl' => '//trx.abaenglish.com',
    'googleAnalytics_trackingId' => 'UA-1201726-1',
    'googleAnalytics_trackingDomain' => '\'auto\'',

    'NEWRELIC_APPLICATIONID' => '19471376',
    'NEWRELIC_LICENSEKEY' => 'cb654f737f',

    'ABA_MOMENTS_URL' => 'https://abamoments.abaenglish.com',
    'ABA_API_URL' => 'https://gateway.abaenglish.com',
  ),

);
