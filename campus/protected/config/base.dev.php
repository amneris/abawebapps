<?php

/**
 * Enhancement #5443 - http://redmine.abaenglish.com/issues/5443
 *
 * Todas las configuraciones de config que no se necesiten recargar en caliente deberían estar aquí y deberíamos usar
 * Yii::app()->params en lugar de Yii::app()->config
 *
 */
return array(

  'components' => array(
    'cache' => array(
      'class' => 'system.caching.CMemCache',
    ),
    'config' => array(
      'class' => 'common.protected.extensions.econfig.EConfig',
      'cacheID' => 'cache'
    ),
    'user' => array(
        /**
         * WARN: RFC 2109
         *
         * Host A's name domain-matches host B's if
         *
         * A is a FQDN string and has the form NB, where N is a non-empty name string, B has the form .B', and B'
         * is a FQDN string.  (So, x.y.com domain-matches .y.com but not y.com.)
         *
         * https://www.ietf.org/rfc/rfc2109.txt
         */
      'identityCookie' => array(
        'domain' => '.dev.aba.land' //note dot before domain name
      ),
    )
  ),

  'params' => array(
    'glossaryUrl' => '//api.dev.aba.land/glossary/search',
    'transactionsUrl' => '//trx.dev.aba.land',
    'googleAnalytics_trackingId' => 'UA-68373237-10',
    'googleAnalytics_trackingDomain' => '\'auto\'',

    'ABA_MOMENTS_URL' => 'https://abamoments.dev.aba.land',
     'ABA_API_URL' => 'https://gateway.dev.aba.land',
  ),

);
