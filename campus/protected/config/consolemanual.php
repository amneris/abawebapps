<?php

$aFixArrayForConsole = CMap::mergeArray(
	require(dirname(__FILE__).'/console.php'),
	array(
        'commandPath'=> ROOT_DIR.DIRECTORY_SEPARATOR.'campus'.DIRECTORY_SEPARATOR.'protected'.DIRECTORY_SEPARATOR.'commands'.DIRECTORY_SEPARATOR.'manuals',
	    )
);


$aFixArrayForConsole['import'][]='application.extensions.phpexcel.*';
$aFixArrayForConsole['import'][]='application.extensions.excelReader.*';
$aFixArrayForConsole['import'][]='application.modules.reconciliationParser.models.*';
$aFixArrayForConsole['import'][]='application.modules.reconciliationParser.rules.*';



/*$aFixArrayForConsole["components"]['db']=array(
    'class'=>'CDbConnection',
    'connectionString' => 'mysql:host=172.16.1.20;dbname=aba_b2c',
    'emulatePrepare' => true,
    'username' => 'abaread',
    'password' => 'AbaWebReadEnglish',
    'charset' => 'utf8',
);
*/


return $aFixArrayForConsole;