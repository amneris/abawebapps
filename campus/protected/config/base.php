<?php
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

return array(
    // basePath is path to campus/protected/*, which is 'application'
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',

    'name' => 'ABA English - The best way to learn English Online',
    'theme' => 'ABAenglish',
    'preload' => array('log'),

    // for abawebapps the sourceLanguage should never match the real language or YII would display our keys instead of
    // their values. Some pages may work because they use real words as keys, other pages won't work beacuse they are
    // using f*king keys as f*cking keys. Way to go YII!!
    'sourceLanguage' => '00',

    // Auto-loading model and component classes
    'import' => array(
        /* All models and rules */
        'application.models.geo.*',
        'application.models.forms.*',
        'application.models.payment.*',
        'application.models.user.*',
        'application.models.course.*',
        'application.models.product.*',
        'application.models.summary.*',
        'application.models.logs.*',
        'application.models.wssoap.*',
        'application.models.experiments.*',
        'application.models.leveltest.*',
        'application.modules.message.models.*',
        /* Extending Yii and Widgets for views */
        // none, only at common at the moment
        /* web services and common code, rules , standard behaviours, etc.*/
        //'application.rules.*',
        'application.rules.paySuppliers.*',
        /* Customized libraries, semi-standard PHP libraries and Helpers */
        //'application.helpers.*',
        'application.extlibraries.*',
        'application.extlibraries.vendors.nusoap.*',
        /* Customized filters */
        'application.filters.*',
        /* RestFul extension, should be in components */
//        'ext.restfullyii.components.*',

    ),

    'behaviors' => array(
        'onBeginRequest' => array(
            'class' => 'application.components.RequireLogin'
        )
    ),
    'modules' => array(
        'message' => array( // Extension message
            'userModel' => 'AbaUser',// Model User,
            'getNameMethod' => 'getFullName',
            'getSuggestMethod' => 'getSuggest',
            'viewPath' => '//messages',
        ),
        'reconciliationParser' => array(
            'fileType' => 'XML',
            'urlAccess' => false,
        ),
    ),

    // application components
    'components' => array(
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'autoRenewCookie' => true,
            'loginUrl' => array('site/login'),
            'class' => "AbaWebUser",
            'stateKeyPrefix' => 'campusaba_skp',
            'authTimeout' => 31557600, // a year
        ),
        'session' => array(
            'sessionName' => 'campusaba_sn',
            'class' => 'CDbHttpSession',
            'connectionID' => 'db',
            'autoCreateSessionTable' => false,
            'cookieMode' => 'only',
            'timeout' => 1800, // CHttpSession defaults to 1440 seconds. 1440 Seconds = 24 minutes
        ),
        'dataLayer' => array(
            // we add this component to enable us to read/write information to a common place throughout app lifecycle
            // it's main purpose is to provide a memory datastore for information that will be written
            // to Google Tag Manager Data Layer (a simple javascript array found on every page of the app)
            'class' => 'application.components.AbaCDataLayer'
        ),
        // uncomment the following to enable URLs in path-format
        // Example of URLs admitted by Yii currently:
        // 'http://abaenglish-local/es/payments/reviewpayment/hello/bye/payerId/28434729'
        // 'http://abaenglish-local/es/payments/reviewpayment/?token=adios&payid=28434729'
        'urlManager' => array(
            'class' => 'application.components.UrlManager',
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(

                '<language:(es|en|pt|it|fr|de|ru)>/acceso-directo-aba-premium/*' => 'site/products',
                '<language:(es|en|pt|it|fr|de|ru)>/accesso-diretto-aba-premium/*' => 'site/products',
                '<language:(es|en|pt|it|fr|de|ru)>/acesso-direto-aba-premium/*' => 'site/products',
                '<language:(es|en|pt|it|fr|de|ru)>/direct-access-aba-premium/*' => 'site/products',
                '<language:(es|en|pt|it|fr|de|ru)>/acces-direct-aba-premium/*' => 'site/products',
                '<language:(es|en|pt|it|fr|de|ru)>/direkt-zugang-aba-premium/*' => 'site/products',
                '<language:(es|en|pt|it|fr|de|ru)>/pryamoy-dostup-aba-premium/*' => 'site/products',
                'acceso-directo-aba-premium/' => 'site/products',
                'accesso-diretto-aba-premium/' => 'site/products',
                'acesso-direto-aba-premium/' => 'site/products',
                'direct-access-aba-premium/' => 'site/products',
                'acces-direct-aba-premium/' => 'site/products',
                'direkt-zugang-aba-premium/' => 'site/products',
                'pryamoy-dostup-aba-premium/' => 'site/products',

                '<language:(es|en|pt|it|fr|de|ru)>/direct-payment/*' => 'site/directpayment',
                'direct-payment/' => 'site/directpayment',

                'recover-password/' => 'site/recoverpassword',
                'reset-password/' => 'site/resetpassword',
                'pages/<action:\w+>'   => 'pages/<action>',
                '<language:(es|en|pt|it|fr|de|ru)>/recover-password/' => 'site/recoverpassword',
                '<language:(es|en|pt|it|fr|de|ru)>/reset-password/' => 'site/resetpassword',

                '<language:(es|en|pt|it|fr|de|ru)>/' => 'site/index',
                '<language:(es|en|pt|it|fr|de|ru)>/<action:(login|logout)>/*' => 'site/<action>',
                '<action:(login|logout)>/*' => 'site/<action>',
                '<language:(es|en|pt|it|fr|de|ru)>/<controller:\w+>/<action:\w+>/unit/<unit:\w+>' => '<controller>/<action>', // kept for backward compatibility & Selligent
                '<language:(es|en|pt|it|fr|de|ru)>/<controller:\w+>/<action:\w+>/unit/<unit:\w+>/section/<section:\w+>' => '<controller>/<action>', // kept for backward compatibility & Selligent
                '<language:(es|en|pt|it|fr|de|ru)>/<controller:\w+>/<action:\w+>/unit/<unit:\w+>/section/<section:\w+>/page/<page:\w+>' => '<controller>/<action>', // kept for backward compatibility & Selligent
                '<language:(es|en|pt|it|fr|de|ru)>/<controller:\w+>/<action:\w+>/<unit:\w+>' => '<controller>/<action>', // @ascandroli: this is the one I like but it's not backward-compatible
                '<language:(es|en|pt|it|fr|de|ru)>/<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<language:(es|en|pt|it|fr|de|ru)>/<_c>/<_a>' => '<_c>/<_a>',
                '<language:(es|en|pt|it|fr|de|ru)>/<_c>/<_a>/*' => '<_c>/<_a>' // kept for backward compatibility
            ),
        ),
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'abaFacebook' => array(
            'class' => 'application.extensions.facebook.AbaFacebook',
        ),
//
//@TODO - test functional
//        'mailer' => array(
//            'class' => 'application.extensions.mailer.EMailer',
//            'pathViews' => 'application.views.email',
//            'pathLayouts' => 'application.views.email.layouts'
//        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'itbugs@abaenglish.com',
        'languages' => array('es' => 'Español', 'en' => 'English', 'pt' => 'Português',
            'it' => 'Italiano', 'fr' => 'Français', 'de' => 'Deutsch',
            'ru' => 'Русский'),
        'levels' => array(1 => "Beginners",
            2 => "Lower intermediate",
            3 => "Intermediate",
            4 => "Upper intermediate",
            5 => "Advanced",
            6 => "Business"),
        'userType' => array(DELETED => "Deleted",
            FREE => "Free",
            PREMIUM => "Premium",
            TEACHER => "Teacher"),
        'DbTransactional' => false,
        'dbCampus' => 'aba_b2c',
        'dbVentasFlash' => 'db_ventasflash',
        'dbCampusLogs' => 'aba_b2c_logs',
        'dbCampusSummary' => 'aba_b2c_summary',
        'dbExtranet' => 'aba_b2c_extranet',

        'ZENDESK_DOMAIN' => 'abaenglish.zendesk.com',   // help.abaenglish.com
        'ZENDESK_URLLOCALES' => 'https://abaenglish.zendesk.com/api/v2/locales/public.json',
        'ZENDESK_DEFAULTLANGUAGE' => 'en',
        'ZENDESK_PRIVATEKEY' => 'wO0OwCCTohmBa0VbvhAzkh8Lwc7gMmuGy1A7NyFBHmWIj3Ik',
        'ZENDESK_LANGUAGETABLE' => array(
          'en' => 'en-gb',
          'pt' => 'pt-br',
          'fr' => 'fr',
          'it' => 'it',
          'zh' => 'zh-tw',
          'de' => 'de',
          'ru' => 'ru',
          'es' => 'es',
        ),

        'urlLinkedins' => array(1 => "https://lnkd.in/dz2jezJ",
          2 => "https://lnkd.in/eEr_zQx",
          3 => "https://lnkd.in/eZvtbdd",
          4 => "https://lnkd.in/eJwtcjH",
          5 => "https://lnkd.in/eF9rNjb",
          6 => "https://lnkd.in/eUJyeP8"),

            // ** Configuracion niveles del Test de nivel.
        'LevelTest config' => array(
          '1' => array(
            'levelName' => 'Beginner',
            'textQuestions' => 4,
            'audioQuestions' => 0,
            'redirectUrl' => '',
          ),
          '2' => array(
            'levelName' => 'Low intermediate',
            'textQuestions' => 6,
            'audioQuestions' => 2,
            'redirectUrl' => '',
          ),
          '3' => array(
            'levelName' => 'Intermediate',
            'textQuestions' => 6,
            'audioQuestions' => 1,
            'redirectUrl' => '',
          ),
          '4' => array(
            'levelName' => 'Upper intermediate',
            'textQuestions' => 4,
            'audioQuestions' => 1,
            'redirectUrl' => '',
          ),
          '5' => array(
            'levelName' => 'Advanced',
            'textQuestions' => 4,
            'audioQuestions' => 2,
            'redirectUrl' => '',
          ),
          'logFacility' => (defined('LOG_LOCAL7') ? LOG_LOCAL7 : 8),
          'logGroup' => "campus",
          'logName' => "[ABAWEBAPPS]",
        ),

    ),
);
