<?php

/**
 * Enhancement #5443 - http://redmine.abaenglish.com/issues/5443
 *
 * Todas las configuraciones de config que no se necesiten recargar en caliente deberían estar aquí y deberíamos usar
 * Yii::app()->params en lugar de Yii::app()->config
 *
 */
return array(

  'components' => array(
    'cache' => array(
//      'class' => 'system.caching.CMemCache',
      'class' => 'system.caching.CDummyCache',
    ),
    'config' => array(
      'class' => 'common.protected.extensions.econfig.EConfig',
      'cacheID' => 'cache'
    ),
    'user' => array(
      /**
       * WARN: RFC 2109
       *
       * Host A's name domain-matches host B's if
       *
       * A is a FQDN string and has the form NB, where N is a non-empty name string, B has the form .B', and B'
       * is a FQDN string.  (So, x.y.com domain-matches .y.com but not y.com.)
       *
       * https://www.ietf.org/rfc/rfc2109.txt
       */
      'identityCookie' => array(
          'domain' => '.aba.local' //note dot before domain name
      ),
    )
  ),

  'params' => array(
    'glossaryUrl' => '//api.aba.local/glossary/search',
    'transactionsUrl' => '//trx.aba.local',
    'googleAnalytics_trackingId' => 'UA-68373237-2',
    'googleAnalytics_trackingDomain' => '{\'cookieDomain\':\'none\'}',

    'ZENDESK_DOMAIN' => 'abaenglish1450429490.zendesk.com',
    'ZENDESK_URLLOCALES' => 'https://abaenglish1450429490.zendesk.com/api/v2/locales/public.json',
    'ZENDESK_DEFAULTLANGUAGE' => 'en',
    'ZENDESK_PRIVATEKEY' => '92HxfFMJXCii2xzeL155kPVOtqp1UoLrValDX5glJO27PoKu',
    'ZENDESK_LANGUAGETABLE' => array(
      'en' => 'en-gb',
      'pt' => 'pt-br',
      'fr' => 'fr',
      'it' => 'it',
      'zh' => 'zh-tw',
      'de' => 'de',
      'ru' => 'ru',
      'es' => 'es',
    ),

    'NEWRELIC_APPLICATIONID' => '',
    'NEWRELIC_LICENSEKEY' => '',

    'MOCK_SELLIGENT' => true,

    'ABA_MOMENTS_URL' => 'https://abamoments.qa.aba.land',
    'ABA_API_URL' => 'https://gateway.qa.aba.land',

  ),

);

