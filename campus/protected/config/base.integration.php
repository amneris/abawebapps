<?php

/**
 * Enhancement #5443 - http://redmine.abaenglish.com/issues/5443
 *
 * Todas las configuraciones de config que no se necesiten recargar en caliente deberían estar aquí y deberíamos usar
 * Yii::app()->params en lugar de Yii::app()->config
 *
 */
return array(

  'params' => array(

    'glossaryUrl' => '//api.int.aba.land/glossary/search',
    'transactionsUrl' => '//trx.int.aba.land',

    'googleAnalytics_trackingId' => 'UA-68373237-2',
    'googleAnalytics_trackingDomain' => '{\'cookieDomain\':\'none\'}',

    'ZENDESK_DOMAIN' => 'abaenglish.zendesk.com',
    'ZENDESK_URLLOCALES' => 'https://abaenglish.zendesk.com/api/v2/locales/public.json',
    'ZENDESK_DEFAULTLANGUAGE' => 'en',
    'ZENDESK_PRIVATEKEY' => 'wO0OwCCTohmBa0VbvhAzkh8Lwc7gMmuGy1A7NyFBHmWIj3Ik',

    'MOCK_SELLIGENT' => true,

    'ABA_MOMENTS_URL' => 'https://abamoments.dev.aba.land'

  ),

);



