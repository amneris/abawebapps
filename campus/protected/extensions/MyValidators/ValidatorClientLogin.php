<?php
/**
 * Created by PhpStorm.
 * User: Ismael
 * Date: 1/28/2015
 * Time: 11:44 AM
 */
class ValidatorClientLogin extends CRequiredValidator
{
    public function clientValidateAttribute($object,$attribute)
    {
        $message=$this->message;
        if($this->requiredValue!==null)
        {
            if($message===null)
                $message=Yii::t('yii','{attribute} must be {value}.');
            $message=strtr($message, array(
                '{value}'=>$this->requiredValue,
                '{attribute}'=>$object->getAttributeLabel($attribute),
            ));
            return "
if(value!=" . CJSON::encode($this->requiredValue) . ") {
	messages.push(".CJSON::encode($message).");
}
";
        }
        else
        {

            if($message===null)
                if ($attribute == "name"){
                    $message=Yii::t('mainApp','nullUser');
                }
                elseif ($attribute == "email"){
                    $message=Yii::t('mainApp','emptyUser');
                }elseif($attribute == "password"){
                    $message=Yii::t('mainApp','emptyPassword');
                }


            $message=strtr($message, array(
                '{attribute}'=>$object->getAttributeLabel($attribute),
            ));
            if($this->trim)
                $emptyCondition = "jQuery.trim(value)==''";
            else
                $emptyCondition = "value==''";
            return "
if({$emptyCondition}) {
	messages.push(".CJSON::encode($message).");
}
";
        }
    }
}