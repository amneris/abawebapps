<?php
/**
 * Created by PhpStorm.
 * User: Ismael
 * Date: 1/29/2015
 * Time: 12:29 PM
 */
class ValidatorEmailCorrect extends CEmailValidator
{


    public function clientValidateAttribute($object,$attribute)
    {
        if($this->validateIDN)
        {
            Yii::app()->getClientScript()->registerCoreScript('punycode');
            // punycode.js works only with the domains - so we have to extract it before punycoding
            $validateIDN='
var info = value.match(/^(.[^@]+)@(.+)$/);
if (info)
	value = info[1] + "@" + punycode.toASCII(info[2]);
';
        }
        else
            $validateIDN='';

        $message=$this->message!==null ? $this->message : Yii::t('mainApp','emailValid');
        $message=strtr($message, array(
            '{attribute}'=>$object->getAttributeLabel($attribute),
        ));

        $condition="!value.match({$this->pattern})";
        if($this->allowName)
            $condition.=" && !value.match({$this->fullPattern})";

        return "
$validateIDN
if(".($this->allowEmpty ? "jQuery.trim(value)!='' && " : '').$condition.") {
	messages.push(".CJSON::encode($message).");
}
";
    }

}