<?php
/**
 * Description of codeReader
 *
 * @author Luis Assandri
 */
class codeReader 
{
    private $dataSheet;
    private $moreElements; 
    private $lastCode;
    private $inxRow;
    private $inxPage;
    private $pages;
    private $productsLabel = array(1=>'MENSUAL', 2=>'BIMENSUAL', 3=>'TRIMESTRAL', 6=>'SEMESTRAL', 12=>'ANUAL', 18=>'18 MESES', 24=>'24 MESES');
    private $detail;
    private $error="OK";

    public function __constructor()
    { 
         $this->dataSheet = null;
         $this->moreElements = false; 
         $this->lastCode ="";
         $this->inxRow = 0;
         $this->inxPage = 0;
         $this->pages = 0;

    }
    
    public function init($xTempPath)
    {
        $this->dataSheet = new Spreadsheet_Excel_Reader();
        $this->dataSheet->setOutputEncoding('CP1251');
        $this->dataSheet->read($xTempPath); 
    }
    
    public function processCodes($xProducts, $xProdTables, $xInsertModel, $xAgrupador, $xNoMatchProduct)
    {
        $this->detail .= " Nombre del fichero: ".$xInsertModel->fileName." *** ";
        foreach($xNoMatchProduct as $noProduct)
        {
            $this->detail .= " Has seleccionado el producto: ".$this->productsLabel[$noProduct].", pero no existe para este agrupador. *** ";
            $this->error .= "ERROR";
        }
        
        foreach($xProducts as $product)
        {
            $selected=0;
            
            foreach($this->dataSheet->boundsheets as $sheetsName)
            {
                
                if($sheetsName['name'] == $product)
                {  
                    $table = $xProdTables[$product];
                    echo "<hr>Insertando producto <b>".  $this->productsLabel[$product]."</b><br><br><hr>";
                    
                    foreach($this->dataSheet->sheets[$selected]['cells'] as $row)
                    {
                        foreach($row as $cell)
                        {	
                            $cell = trim($cell);
                            if($this->checkCodeExist($cell, $table))
                            {
                                //muestra mensaje de que ya existe el código.
                                echo "<i><span style=\"color:red\"><b>".$cell."</b> ya existe para este producto.</span></i><br>";
                            }
                            else
                            {
                                 //llama a la funcion que inserta el codigo. 
                                if($this->insertCode($cell, $table, $xInsertModel))
                                    echo "<i><span style=\"color:green\"><b>".$cell."</b> se ha insertado con exito.</span></i><br>";
                                else
                                    echo "<i><span style=\"color:red\"><b>".$cell."</b> ocurrio un problema al tratar de insertar el código.</span></i><br>";
                            }
                        }  
                    } 
                    echo "<br>";
                }
                $selected++;
            }
        }
      
        $this->printMissingExcelProducts($xProducts);
        if($this->insertLogs($xAgrupador, $this->error, $this->detail, $xInsertModel))
            echo "<br><i><span style=\"color:green\">Se insertaron los logs correctamente.</span></i><br>";
        else
            echo "<br><i><span style=\"color:red\">No se pudierón insertar logs.</span></i><br>";
    }
    
    public function printMissingExcelProducts($xProducts)
    { 
        foreach($xProducts as $product)
        {
            $found = 0;
            foreach($this->dataSheet->boundsheets as $sheetsName)
            {
                if($sheetsName['name']== $product)
                {
                    $found = 1;
                    break;
                }    
            }
            if(!$found)
            {
                echo "<span style=\"color:red\"> No existe el producto <b>".$this->productsLabel[$product]."</b> en el fichero exel proporcionado.</span><br>";
                $this->detail .= "No existe el producto ".$this->productsLabel[$product]." en el fichero exel proporcionado.";
                $this->error = "ERROR";
            }
                
        }
    }
    
    public function insertCode($xCode, $xProdTable, $xInsertModel)
    {
        $query = "INSERT INTO db_ventasflash.`$xProdTable` (code1, used, date, campaign, count) VALUES ('$xCode', 0, '0000-00-00 00:00:00', $xInsertModel->deal, $xInsertModel->count);";
        
        $connection = Yii::app()->db2;
        $command = $connection->createCommand($query);		
        $result = $command->query();
        
        return $result;
    }
    
    public function checkCodeExist($xCode, $xProdTable)
    {
        $query = "SELECT count(*) as registro FROM db_ventasflash.`$xProdTable` WHERE code1 = '$xCode';";
        $connection = Yii::app()->db2;
        $command = $connection->createCommand($query);		
        $dataReader = $command->query();
        $rows = $dataReader->read();
        
        if($rows['registro']<>0)
            return 1;
        else
            return 0;   
    } 
    
    public function insertLogs($xAgrupador, $xError, $xDetail, $xInsertModel)
    {
        $query = "INSERT INTO db_ventasflash.insertionLogs (date, agrupador, error, detail) VALUES ('$xInsertModel->fecha', '$xAgrupador->agrupadorName', '$xError', '$xDetail');";
  
        $connection = Yii::app()->db2;
        $command = $connection->createCommand($query);		
        $result = $command->query();

        return $result; 
    }
    
}
?>
