<?php
/**
 * Created by PhpStorm.
 * User: akorotkov
 * Date: 5/11/14
 * Time: 13:38
 */

if (!defined('_FACEBOOK_PATH')) define('_FACEBOOK_PATH', dirname(preg_replace('/\\\\/','/',__FILE__)) . '/');

require_once(_FACEBOOK_PATH . 'sdk-3-2-3/src/facebook.php');
// require_once "sdk-3-2-3/src/facebook.php";
// Yii::import('application.extensions.facebook.sdk-3-2-3.src.Facebook');

class AbaFacebook extends CApplicationComponent {

    /**
     * @param $appId
     * @return array
     */
    protected function getAppsData($appId) {

        $appsData = array(
            Yii::app()->config->get("FB_APP_CAMPUS_LOGIN_APPID") => array(
                'appId' => Yii::app()->config->get("FB_APP_CAMPUS_LOGIN_APPID"),
                'secret' => Yii::app()->config->get("FB_APP_CAMPUS_LOGIN_SECRET"),
//                'scope' => Yii::app()->config->get("FB_APP_CAMPUS_LOGIN_SCOPE"),
                'cookie' => true,
            ),
        );
        if(isset($appsData[$appId])) {
            return $appsData[$appId];
        }
        return array();
    }

    /**
     * @param $accessToken
     * @param $appId
     *
     * @return array|bool
     */
    public function getUser($accessToken, $appId)
    {
        $facebook = new Facebook($this->getAppsData($appId));

        $facebook->setAccessToken($accessToken);

        $fbUser = $facebook->getUser();

        $aUserData = array();

        if ($fbUser) {
            try {
                $aUserProfile = $facebook->api('/me');
            }
            catch (Exception $e) {
                return false;
            }

            $aUserData['id'] =          $aUserProfile["id"];
            $aUserData['email'] =       $aUserProfile["email"];
            $aUserData['first_name'] =  $aUserProfile["first_name"];
            $aUserData['last_name'] =   $aUserProfile["last_name"];
            $aUserData['gender'] =      $aUserProfile["gender"];
        }

        return $aUserData;
    }

}
