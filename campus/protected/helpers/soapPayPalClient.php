<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 7/11/12
 * Time: 18:19
 * To change this template use File | Settings | File Templates.
 */
class soapPayPalClient extends nusoap_client
{
    private $strHeader;

    private $aResultado;
    private $tokenPayPal;
    private $payerId;
    private $ack;
    private $profileId;
    private $errores;
    private $errorCode;

    private $success;

    /* @var PayGatewayLogPayPal $moLogPaypalExpressCo */
    public $moLogPaypalExpressCo;

    function __destruct() {
        ini_set("memory_limit","42M");
    }

    /**
     *
     */
    public function initPayPalConfig()
    {
        // Quino: It looks like the parsing of WSDL from PAYPAL needs a lot of memory since 2014-06-01
        ini_set("memory_limit","128M");

        $this->setUseCurl(0);

        $this->decode_utf8 = false;

        $this->setEndpoint(Yii::app()->config->get("PAY_PAL_API_URL_ENDPOINT"));

        $header = '<RequesterCredentials xmlns="urn:ebay:api:PayPalAPI" xsi:type="ebl:CustomSecurityHeaderType">' .
            '<Credentials xmlns="urn:ebay:apis:eBLBaseComponents" xsi:type="ebl:UserIdPasswordType">' .
            '<Username>' . Yii::app()->config->get("PAY_PAL_API_USER") . '</Username>' .
            '<Password>' . Yii::app()->config->get("PAY_PAL_API_PWD") . '</Password>' .
            '<Signature>' . Yii::app()->config->get("PAY_PAL_API_SIGNATURE") . '</Signature>' .
            '</Credentials>' .
            '</RequesterCredentials>';

        $this->strHeader = $header;
        $this->setHeaders($header);

        $this->moLogPaypalExpressCo = new PayGatewayLogPayPal( PAY_SUPPLIER_PAYPAL );
    }


    /**
     * @param string $operation
     * @param array|mixed $bodyRequest
     * @param integer $userId Optional
     * @param string $idPayment
     *
     * @return mixed
     */
    public function call($operation, $bodyRequest=array(), $userId = NULL, $idPayment = NULL,$headers=false,$rpcParams=null,$style='rpc',$use='encoded')
//    public function call($operation, $bodyRequest, $userId = NULL, $idPayment = NULL)
    {
        if (empty($userId)) {
            $this->moLogPaypalExpressCo->userId = Yii::app()->user->getId();
        } else {
            $this->moLogPaypalExpressCo->userId = $userId;
        }
        $this->moLogPaypalExpressCo->operation = $operation;
        $this->moLogPaypalExpressCo->idPayment = $idPayment;
        $this->moLogPaypalExpressCo->dateRequest = HeDate::todaySQL(true);
        $this->moLogPaypalExpressCo->xmlRequest = $bodyRequest;

        $this->aResultado = parent::call($operation, $bodyRequest);
        if(is_array($this->aResultado) && $this->aResultado!==false){
            try {
                $this->moLogPaypalExpressCo->jsonResponse = json_encode($this->aResultado);
            } catch (Exception $e) {
                $this->moLogPaypalExpressCo->jsonResponse = "JsonEncode did not work with theresponse of PayPal: " .
                    $this->moLogPaypalExpressCo->jsonResponse;
            }
        }

        $this->success = $this->processResponse();
        $this->moLogPaypalExpressCo->errorCode = NULL;
        $this->moLogPaypalExpressCo->success = 0;
        if ($this->success) {
            $this->moLogPaypalExpressCo->success = 1;
            $this->moLogPaypalExpressCo->aFullResponse = $this->aResultado;
        } else {
            $this->moLogPaypalExpressCo->errorCode = $this->errorCode;
        }

        $this->moLogPaypalExpressCo->insertLog();

        return $this->aResultado;
    }


    /** Is set from processResponse()
     * @return bool
     */
    public function isSuccess()
    {
        return $this->success;
    }


    /** Analyse the content of the response from Paypal.
     *
     * @return bool return true if success. Otherwise false.
     */
    private function processResponse()
    {
        $this->errores = $this->getError();
        if (!$this->errores || (is_array($this->aResultado) && array_key_exists('Ack', $this->aResultado))) {
            switch (strtoupper($this->aResultado['Ack'])) {
                case 'SUCCESS':
                case 'SUCCESSWITHWARNING':
                    if (array_key_exists("Token", $this->aResultado)) {
                        $this->tokenPayPal = $this->aResultado['Token'];
                    } elseif (array_key_exists("GetExpressCheckoutDetailsResponseDetails", $this->aResultado)) {
                        $this->tokenPayPal = $this->aResultado['GetExpressCheckoutDetailsResponseDetails']['Token'];
                    } elseif (array_key_exists("DoExpressCheckoutPaymentResponseDetails", $this->aResultado)) {
                        $this->tokenPayPal = $this->aResultado['DoExpressCheckoutPaymentResponseDetails']['Token'];
                    } elseif (array_key_exists("CreateRecurringPaymentsProfileResponseDetails", $this->aResultado)) {
                        $this->profileId = $this->aResultado['CreateRecurringPaymentsProfileResponseDetails']["ProfileID"];
                    } elseif (array_key_exists("ManageRecurringPaymentsProfileStatusResponseDetails", $this->aResultado)) {
                        $this->profileId = $this->aResultado['ManageRecurringPaymentsProfileStatusResponseDetails']["ProfileID"];
                    } elseif (array_key_exists("GetRecurringPaymentsProfileDetailsResponseDetails", $this->aResultado)) {
                        $this->profileId = $this->aResultado['GetRecurringPaymentsProfileDetailsResponseDetails']["ProfileID"];
                    }
                    return true;
                    break;
                case 'FAILURE':
                    if (array_key_exists("Errors", $this->aResultado)) {
                        $this->errores = $this->aResultado['Errors']['LongMessage'];
                        $this->errorCode = $this->aResultado['Errors']['ErrorCode'];
                    } else {
                        $this->errores = json_encode($this->aResultado);
                    }
                    return false;
                    break;
                default:
                    HeLogger::sendLog(
                      HeLogger::PREFIX_ERR_UNKNOWN_H .
                        'We do not collect the Ack Code of a Paypal operation correctly',
                        HeLogger::IT_BUGS,
                        HeLogger::CRITICAL,
                        ' The response from Paypal has been : ' . json_encode($this->aResultado)
                    );
                    $this->ack = $this->aResultado['Ack'];
                    if (is_array($this->aResultado) && array_key_exists("Errors", $this->aResultado)) {
                        $this->errores = $this->aResultado['Errors']['LongMessage'];
                        $this->errorCode = $this->aResultado['Errors']['ErrorCode'];
                    } else {
                        $this->errores = json_encode($this->aResultado);
                    }
                    return false;
                    break;
            }
            return false;
        } else {
            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_UNKNOWN_H .
                'Response from Paypal for operation '.$this->moLogPaypalExpressCo->operation." is error.",
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                ' The response from Paypal has been : ' . $this->errores
            );
            return false;
        }
    }

    /**
     * @return string
     */
    public function getTokenPayPal()
    {
        return $this->tokenPayPal;
    }

    /**
     * @return string
     */
    public function getAck()
    {
        return $this->ack;
    }

    /**
     * @return mixed
     */
    public function getErrores()
    {
        if (is_array($this->errores)) {
            return implode(", ", $this->errores);
        }

        return $this->errores;
    }

    /**
     * @return mixed
     */
    public function getAResultado()
    {
        return $this->aResultado;
    }

    /**
     * @return string
     */
    public function getProfileId()
    {
        return $this->profileId;
    }

    public function getStrHeader()
    {
        return $this->strHeader;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }
}
