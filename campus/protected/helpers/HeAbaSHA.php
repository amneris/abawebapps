<?php
/**
 * User: Quino
 * Date: 29/10/12
 * Time: 16:26
 * To change this template use File | Settings | File Templates.
 */
class HeAbaSHA
{
    static function sha1_str2blks_SHA1($str)
    {
        $strlen_str = strlen($str);

        $nblk = (($strlen_str + 8) >> 6) + 1;

        for ($i=0; $i < $nblk * 16; $i++) {
            $blks[$i] = 0;
        }

        for ($i=0; $i < $strlen_str; $i++) {
            $blks[$i >> 2] |= ord(substr($str, $i, 1)) << (24 - ($i % 4) * 8);
        }

        $blks[$i >> 2] |= (0xFFFFFFFF & (0x80 << (24 - ($i % 4) * 8)));
        $blks[$nblk * 16 - 1] = $strlen_str * 8;

        return $blks;
    }

    static function sha1_safe_add($x, $y)
    {
        $lsw = ($x & 0xFFFF) + ($y & 0xFFFF);
        $msw = ($x >> 16) + ($y >> 16) + ($lsw >> 16);

        return (0xFFFFFFFF & ($msw << 16)) | ($lsw & 0xFFFF);
    }

    static function sha1_rol($num, $cnt)
    {
        return (0xFFFFFFFF &($num << $cnt) | self::sha1_zeroFill($num, 32 - $cnt));
    }

    static function sha1_zeroFill($a, $b)
    {
        $bin = decbin($a);

        $strlen_bin = strlen($bin);

        $bin = $strlen_bin < $b ? 0 : substr($bin, 0, $strlen_bin - $b);

        for ($i=0; $i < $b; $i++) $bin = '0'.$bin;

        return bindec($bin);
    }

    static function sha1_ft($t, $b, $c, $d)
    {
        if ($t < 20) return ($b & $c) | ((~$b) & $d);
        if ($t < 40) return $b ^ $c ^ $d;
        if ($t < 60) return ($b & $c) | ($b & $d) | ($c & $d);

        return $b ^ $c ^ $d;
    }

    static function sha1_kt($t)
    {
        if ($t < 20) return 1518500249;
        if ($t < 40) return 1859775393;
        if ($t < 60) return 0x8f1bbcdc;//-1894007588;

        //return -899497514;
        return 0xca62c1d6;
    }

    static function sha_1($str, $raw_output = false)
    {
        if ( $raw_output === true ) return pack('H*', self::sha_1($str, false));

        $x = self::sha1_str2blks_SHA1($str);
        /* $a =  1732584193;
         $b = -271733879;
         $c = -1732584194;
         $d =  271733878;
         $e = -1009589776;*/
        $a = 0x67452301;
        $b = 0xefcdab89;
        $c = 0x98badcfe;
        $d = 0x10325476;
        $e = 0xc3d2e1f0;

        $x_count = count($x);

        for ($i = 0; $i < $x_count; $i += 16) {
            $olda = $a;
            $oldb = $b;
            $oldc = $c;
            $oldd = $d;
            $olde = $e;
            $w = array();
            for ($j = 0; $j < 80; $j++) {
                $w[$j] = ($j < 16) ? $x[$i + $j] : self::sha1_rol(
                    $w[$j - 3] ^ $w[$j - 8] ^ $w[$j - 14] ^ $w[$j - 16],
                    1
                );

                $t = self::sha1_safe_add(
                    self::sha1_safe_add(self::sha1_rol($a, 5), self::sha1_ft($j, $b, $c, $d)),
                    self::sha1_safe_add(self::sha1_safe_add($e, $w[$j]), self::sha1_kt($j))
                );
                $e = $d;
                $d = $c;
                $c = self::sha1_rol($b, 30);
                $b = $a;
                $a = $t;
            }

            $a = self::sha1_safe_add($a, $olda);
            $b = self::sha1_safe_add($b, $oldb);
            $c = self::sha1_safe_add($c, $oldc);
            $d = self::sha1_safe_add($d, $oldd);
            $e = self::sha1_safe_add($e, $olde);
        }

        return sprintf('%08x%08x%08x%08x%08x', $a, $b, $c, $d, $e);
    }

    /**
     * It returns  crc32(UserId + Email + Time now) + timeMMDD;
     * @param $user
     * @param string $dateToPay
     *
     * @return string
     */
    public static function generateIdPayment($user, $dateToPay = null)
    {
        if (!isset($dateToPay)) {
            $timeNow = time();
            $timeNow = date('YmdHis', $timeNow);
        } else {
            $dateToPay = substr($dateToPay, 0, 10);
            $timeNow = new DateTime($dateToPay.date('H:i:s', time()));
            $timeNow = date('YmdHis', $timeNow->format("YmdHis"));
        }
        $theString=$user->id.$user->email.$timeNow ;
        return hash("crc32b", $theString);
    }

    /** Generates a new token based on the email, device and operative system.
     * Returns a string.
     *
     * @param string $sysOper
     * @param string $deviceId
     * @param string $email
     * @param string $password
     *
     * @return string
     */
    public static function genTokenForMobile( $sysOper, $deviceId, $email, $password )
    {
        return HeAbaSHA::_md5( WORD4MOBILEWS.$sysOper.$deviceId.$email.$password );
    }

    /**
     * @static
     *
     * @param $text
     *
     * @return string
     */
    public static function _md5($text)
    {
        return md5($text);
    }

    /**
     * @param AbaUser $user
     *
     * @return string
     */
    public static function generateIdPromo( AbaUser $user, $sPlan, $i ) {

        $timeNow =      time();
        $timeDate =     date('Ymd', $timeNow );
        $timeTime =     date('His', $timeNow );
        $theString =    $user->id . $user->email . $timeDate . $timeTime . $i ;
        $sHash =        $timeDate . $sPlan . hash( "crc32b", $theString );

        return $sHash;
    }

}
