<?php
/**
 * http://www.livechatinc.com, it includes the chat through Javascript into the Campus.
 * It is used from the layouts, append at the end, just before end body tag
 *
 * https://abaenglish.zendesk.com/agent/apps/zopim-chat
 *
 */
class HeChatSupport
{
    public static function includeChatSupport ()
    {
        $script = '<!-- NO CHAT AVAILABLE -->';

        if (intval(Yii::app()->config->get('ENABLE_CHAT')) === 1) {

            $moUser = new AbaUser();

            if (isset(Yii::app()->user->id) && $moUser->getUserById( Yii::app()->user->id )) {

                if (($moUser->userType == FREE) || strpos(Yii::app()->request->getUrl(), 'payments/confirmedPayment') >= 1) {

                    if(HeMixed::filterCountry($moUser->langEnv, trim(Yii::app()->config->get('ZENDESK_CHAT_COUNTRIES')))) {

                        $script = Yii::app()->config->get('ZENDESK_CHAT_JS_SCRIPT');
                    }
                }
            }
        }

        echo '<script type="text/javascript">' . $script . '</script>';

        return true;
    }
}