<?php

/**
 *
 */
class HeAnalytics
{

    private static $aExceptions = array("payments/dopaymentpaypal");

    /** Returns http or https depending on Yii request.
     *
     * @return string
     */
    private static function httpPrefix()
    {
        $httpProt = 'https';
        if (Yii::app()->getRequest()->isSecureConnection) {
            $httpProt = 'https';
        }

        return $httpProt;
    }

    /** Controls conversion of registration of User To Free n) FOR DOUBLE OPT-IN
     * from any landing.
     * This pixel is always printed as long as the parameter  "dbloptin" is 1.
     *
     * @static
     * @return bool
     */
    public static function scriptRegisterUserConversionFooterDoubleOptIn()
    {
        return true;
    }

    /**
     * ALL CONVERSION AND TRACKING FOR REGISTRATION OF FREE USERS FOR SINGLE OPT-IN
     * @static
     * @return bool
     */
    public static function scriptRegisterUserConversionSingleOptIn()
    {
        // Check if scenario is a user that just landed to /index coming from the registration process:
        $comesFromRegistration = Yii::app()->user->getState('comesFromRegisterUserFirstTime');
        if ($comesFromRegistration == 0 || is_null($comesFromRegistration)) {
            return true;
        }

        // Reset the registrationUser session variable. ------------------------------
        Yii::app()->user->setState('comesFromRegisterUserFirstTime', 0);

        return true;
    }

    /** CALLED FROM payments/payment, when a selection of product is done all these tracking are executed on client side
     * @static
     *
     * @param array $aProductSelected
     */
    public static function scriptProductSelection(array $aProductSelected)
    {
    }

    /** CALLED FROM payments/paymentMethod, when a selection of paymentMethod is done all these tracking are executed on client side
     * @static
     *
     * @param array $aProductSelected
     */
    public static function scriptPaymentMethod(array $aProductSelected)
    {
        $moUser = new AbaUser();
        $moUser->getUserById(Yii::app()->user->id);

        if ($moUser->userType == FREE) {
            $planKey = $aProductSelected['productPricePK'];

            $moCountry = new AbaCountry();
            $moCountry->getCountryById(Yii::app()->user->getCountryId());
            $countryIso = $moCountry->iso;

            echo "<script>
               trackOpenCheckoutPage('$planKey', '$countryIso');
            </script>";
        }
    }

    /** CALLED FROM payments/confirmedPayment, when a purchase is done all these tracking are executed on client side
     * @static
     *
     * @param Payment|PaymentControlCheck $payment
     */
    public static function scriptPurchaseUserConversion($payment)
    {
        /*------------------------ Conversion track for GOOGLE E-Commerce---------------------*/
        self::retScriptGoogleECommerceConversionPurchase( $payment->id, $payment->idPartner, $payment->idCountry, $payment->amountPrice,
          $payment->currencyTrans, $payment->xRateToEUR, $payment->idProduct, $payment->idPromoCode ) ;
        /*--------------------------------------------------------------------------*/
    }

    /** It inserts Javascript code for analytics, included in all pages: index, login, payments, units, videos, etc.
     * @static
     * @return bool
     */
    public static function includeScriptsCommonAnalyitics()
    {
        /* Google generic analytics----------------------------------------------*/
        echo self::retScriptCommonGoogleA();
        /*------------  ----------------------------------Google generic analytics*/

        return true;
    }

    /**
     * This script only used for Mobile Analytics. It should be only tracked in mainMobile layout
     */
    public static function includeCommonMobileAnalytics()
    {
        /* Google generic analytics----------------------------------------------*/
        echo self::retScriptCommonGoogleA();
    }

    /**
     * @static
     * @return string
     */
    private static function retScriptCommonGoogleA()
    {
        $script = "
            <!-- Google Analytics -->
            <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', '" . Yii::app()->params["googleAnalytics_trackingId"] . "', " . Yii::app()->params["googleAnalytics_trackingDomain"] . ");
            ga('send', 'pageview');
        ";

        // Send user id when logged in
        $userId = Yii::app()->user->getId();
        if (!empty($userId)) {
            $script .= "
                ga('set', '&uid', '$userId');
            ";
        }
        // Tracking of new free users
        if (Yii::app()->user->getState('isUserFirstLogin') && !Yii::app()->user->getState('registeredGA')) {
            $moUser = new AbaUser();
            if ($moUser->getUserById($userId)) {
                if ($moUser->userType == FREE && isset($_COOKIE['__utmz'])) {
                    $utm_source = substr($_COOKIE['__utmz'], strpos($_COOKIE['__utmz'], 'utmcsr='));
                    $utm_source = substr($utm_source, 0, strpos($utm_source, '|'));
                    $utm_source = substr($utm_source, 7);

                    $script .= "
                        ga('send', 'event', 'Register', 'NewUser', '$utm_source');
                    ";

                    Yii::app()->user->setState('registeredGA', true);
                }
            }
        }

        $script .= "
            </script>
            <!-- End Google Analytics -->
        ";

        return $script;

    }

    /** NOT USED ANYWHERE, anyway it should be private.
     * @static
     * @return bool
     */
    private static function getAnalyticsExperimentTag()
    {

        //@todo config value in database to activate deactivate

        $moUser = new AbaUser();
        if (isset(Yii::app()->user->id) && $moUser->getUserById(Yii::app()->user->id)) {

            if ((strpos(Yii::app()->request->getUrl(),
                  'payments/paymentBversion') >= 1) || (strpos(Yii::app()->request->getUrl(),
                  'payments/paymentmethod') >= 1)
            ) {
                return true;
            } else {
                if (($moUser->userType == FREE && $moUser->langEnv == 'es' && strpos(Yii::app()->request->getUrl(),
                    'payments/payment') >= 1)
                ) {
                    if ((Yii::app()->config->get("SELF_DOMAIN") !== DOMAIN_LIVE)) {
                        $script = '<!-- Google Analytics Content Experiment code -->';
                        echo $script;
                        return true;
                    }

                    $script = <<<QUOTES
                        <!-- Google Analytics Content Experiment code -->
                        <script>function utmx_section(){}function utmx(){}(function(){var
                        k='70039636-1',d=document,l=d.location,c=d.cookie;
                        if(l.search.indexOf('utm_expid='+k)>0)return;
                        function f(n){if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.
                        indexOf(';',i);return escape(c.substring(i+n.length+1,j<0?c.
                        length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;d.write(
                        '<sc'+'ript src="'+'http'+(l.protocol=='https:'?'s://ssl':
                        '://www')+'.google-analytics.com/ga_exp.js?'+'utmxkey='+k+
                        '&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='+new Date().
                        valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
                        '" type="text/javascript" charset="utf-8"><\/sc'+'ript>')})();
                        </script><script>utmx('url','A/B');</script>
                        <!-- End of Google Analytics Content Experiment code -->

QUOTES;

                    echo $script;
                }
            }
        }
        return true;
    }

    /** Echoes the tracking for Visual Web Optimizer
     * @static
     * @return bool
     */

    public static function getVWOCode()
    {
        if (strpos(Yii::app()->request->getUrl(), 'site/indexRegister') >= 1 || strpos(Yii::app()->request->getUrl(),
            'site/indexregister') >= 1
        ) {

            if ((Yii::app()->config->get("SELF_DOMAIN") !== DOMAIN_LIVE)) {
                $script = '<!-- Start Visual Website Optimizer Code --> <!-- End Visual Website Optimizer Code -->';
                echo $script;
                return true;
            }

            $script = <<<QUOTES
                   <!-- Start Visual Website Optimizer Asynchronous Code -->
                    <script type='text/javascript'>
                    var _vwo_code=(function(){
                    var account_id=49559,
                    settings_tolerance=2000,
                    library_tolerance=2500,
                    use_existing_jquery=false,
                    // DO NOT EDIT BELOW THIS LINE
                    f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
                    </script>
                    <!-- End Visual Website Optimizer Asynchronous Code -->
QUOTES;

            echo $script;

        }
        return true;
    }

    public static function includeScriptVWO()
    {
        $moUser = new AbaUser();
        if (isset(Yii::app()->user->id) && $moUser->getUserById(Yii::app()->user->id)) {
            if ($moUser->userType == FREE || strpos(Yii::app()->request->getUrl(), 'payments/confirmedPayment') >= 1) {
                $script = "<!-- Start Visual Website Optimizer Asynchronous Code -->
                    <link rel='stylesheet' href='" . Yii::app()->theme->baseUrl . '/css/ab-testing.css' . "' />
                    <script type='text/javascript' src='" . Yii::app()->theme->baseUrl . '/js/ab-testing.js' . "'></script>

                    <script type='text/javascript'>
                        var _vwo_code = (function () {
                            var account_id = 49559,
                                settings_tolerance = 2000,
                                library_tolerance = 2500,
                                use_existing_jquery = false,
                                // DO NOT EDIT BELOW THIS LINE
                                f = false, d = document;
                            return {
                                use_existing_jquery: function () {
                                    return use_existing_jquery;
                                }, library_tolerance: function () {
                                    return library_tolerance;
                                }, finish: function () {
                                    if (!f) {
                                        f = true;
                                        var a = d.getElementById('_vis_opt_path_hides');
                                        if (a)a.parentNode.removeChild(a);
                                    }
                                }, finished: function () {
                                    return f;
                                }, load: function (a) {
                                    var b = d.createElement('script');
                                    b.src = a;
                                    b.type = 'text/javascript';
                                    b.innerText;
                                    b.onerror = function () {
                                        _vwo_code.finish();
                                    };
                                    d.getElementsByTagName('head')[0].appendChild(b);
                                }, init: function () {
                                    settings_timer = setTimeout('_vwo_code.finish()', settings_tolerance);
                                    this.load('//dev.visualwebsiteoptimizer.com/j.php?a=' + account_id + '&u=' + encodeURIComponent(d.URL) + '&r=' + Math.random());
                                    var a = d.createElement('style'), b = 'body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}', h = d.getElementsByTagName('head')[0];
                                    a.setAttribute('id', '_vis_opt_path_hides');
                                    a.setAttribute('type', 'text/css');
                                    if (a.styleSheet)a.styleSheet.cssText = b; else a.appendChild(d.createTextNode(b));
                                    h.appendChild(a);
                                    return settings_timer;
                                }
                            };
                        }());
                        _vwo_settings_timer = _vwo_code.init();
                    </script>
                    <!-- End Visual Website Optimizer Asynchronous Code -->";

                echo $script;

                return true;
            }
        }
    }


    /** Script to track conversion for purchases in Google Analytics ECommerce
     * @static
     *
     * @param $id
     * @param $idPartner
     * @param $idCountry
     * @param $amountPrice
     * @param $currencyTrans
     * @param $xRateToEUR
     * @param $idProduct
     * @param $idPromoCode
     *
     * @return bool
     */
    private static function retScriptGoogleECommerceConversionPurchase( $id, $idPartner, $idCountry, $amountPrice, $currencyTrans, $xRateToEUR, $idProduct, $idPromoCode )
    {

        if($currencyTrans!==CCY_DEFAULT)
        {
            $amountPrice = $amountPrice / $xRateToEUR;
        }

        $descProduct = "";
        $descCountry = "";
        try{
            $moProduct = new ProductPrice();
            $moProduct->getProductById($idProduct);
            $descProduct =strip_tags(Yii::t( "mainApp", $moProduct->descriptionText, array(), NULL, "en" )) ;

            $moCountry = new AbaCountry($idCountry);
            $descCountry = strip_tags($moCountry->name) ;
        }
        catch(Exception $e)
        {
            if($descCountry ==""){  $descCountry = strval($idCountry);     }
            if($descProduct ==""){  $descProduct = strval($idProduct);   }
        }

        $script = <<<QUOTES
            <script type="text/javascript">

                    ga('require', 'ecommerce');

                    ga('ecommerce:addTransaction', {
                        'id': '$id',                   // Transaction ID. Required
                        'affiliation': '$idPartner',   // Affiliation or store name
                        'revenue': '$amountPrice',     // Grand Total
                        'shipping': '0',               // Shipping
                        'tax': '0'                     // Tax
                    });

                    ga('ecommerce:addItem', {
                        'id': '$id',                   // Transaction ID. Required
                        'name': '$descProduct',        // Product name. Required
                        'sku': '$idProduct',           // SKU/code
                        'category': '$idPromoCode',    // Category or variation
                        'price': '$amountPrice',       // Unit price
                        'quantity': '1'                // Quantity
                    });

                    ga('ecommerce:send');

            </script>
QUOTES;

        echo $script;

        return true;

    }



}

