<?php
/**
 * http://www.livechatinc.com, it includes the chat through Javascript into the Campus.
 * It is used from the layouts, append at the end, just before end body tag
 */
class HeContactSupportTel
{
    public static function includeContactSupportTel()
    {
        $userType = Yii::app()->user->role;
        $currentPartnerId = Yii::app()->user->currentPartnerId;

        if (Yii::app()->user->idPartnerSource == PARTNER_JUNTAEXTREMADURA) {
            return false;
        }
        if (($userType== PREMIUM) && ($currentPartnerId == BCSPARTNERID)) {
            // user has come through AB testing path where they were told premium users have access to free support, so
            // thus we must return true.
            return true;
        }
        if (($userType == PREMIUM) && (intval(Yii::app()->config->get("ENABLE_CONTACT_SUPPORT_PREMIUM")) == 0)) {
            return false;
        }

        $langEnv = Yii::app()->user->getLanguage();
        switch ($langEnv) {
            case 'en':
                if (intval(Yii::app()->config->get("ENABLE_CONTACT_SUPPORT_EN")) == 1)
                    return true;
                break;
            case 'es':
                if (intval(Yii::app()->config->get("ENABLE_CONTACT_SUPPORT_ES")) == 1)
                    return true;
                break;
            case 'it':
                if (intval(Yii::app()->config->get("ENABLE_CONTACT_SUPPORT_IT")) == 1)
                    return true;
                break;
            case 'pt':
                if (intval(Yii::app()->config->get("ENABLE_CONTACT_SUPPORT_PT")) == 1)
                    return true;
                break;
            case 'fr':
                if (intval(Yii::app()->config->get("ENABLE_CONTACT_SUPPORT_FR")) == 1)
                    return true;
                break;
            case 'de':
                if (intval(Yii::app()->config->get("ENABLE_CONTACT_SUPPORT_DE")) == 1)
                    return true;
                break;
            case 'ru':
                if (intval(Yii::app()->config->get("ENABLE_CONTACT_SUPPORT_RU")) == 1)
                    return true;
                break;
            default:
                if (intval(Yii::app()->config->get("ENABLE_CONTACT_SUPPORT_DEFAULT")) == 1)
                    return true;
                break;
        }
        return false;
    }
}