<?php

class HeCooladata
{
    /**
     * navigation events
     */
    const EVENT_NAME_LOGGED_IN = "LOGGED_IN";
    const EVENT_NAME_ENTERED_CAMPUS_HOME = "ENTERED_CAMPUS_HOME";
    const EVENT_NAME_ENTERED_COURSE_INDEX = "ENTERED_COURSE_INDEX";
    const EVENT_NAME_ENTERED_UNIT = "ENTERED_UNIT";
    const EVENT_NAME_ENTERED_SECTION = "ENTERED_SECTION";
    const EVENT_NAME_OPENED_VIDEOCLASSES_INDEX = "OPENED_VIDEOCLASSES_INDEX";
    const EVENT_NAME_OPENED_PRICES = "OPENED_PRICES";
    const EVENT_NAME_OPENED_CHECKOUT = "OPENED_CHECKOUT_PAGE";
    const EVENT_NAME_OPENED_CONFIRMATION = "OPENED_CONFIRMATION_PAGE";
    const EVENT_NAME_OPENED_HELP = "OPENED_HELP";

    /**
     * study events
     */
    const EVENT_NAME_WATCHED_FILM = "WATCHED_FILM";
    const EVENT_NAME_LISTENED_AUDIO = "LISTENED_AUDIO";
    const EVENT_NAME_RECORDED_AUDIO = "RECORDED_AUDIO";
    const EVENT_NAME_COMPARED_AUDIO = "COMPARED_AUDIO";
    const EVENT_NAME_VERIFIED_TEXT = "VERIFIED_TEXT";
    const EVENT_NAME_GOT_TEXT_SUGGESTION = "GOT_TEXT_SUGGESTION";
    const EVENT_NAME_STARTED_INTERPRETATION = "STARTED_INTERPRETATION";
    const EVENT_NAME_REVIEWED_TEST_RESULTS = "REVIEWED_TEST_RESULTS";

    /**
     * profile events
     */
    const EVENT_NAME_USER_REGISTERED = "USER_REGISTERED";
    const EVENT_NAME_CHANGED_PERSONAL_DETAILS = "CHANGED_PERSONAL_DETAILS";
    const EVENT_NAME_CHANGED_LEVEL = "CHANGED_LEVEL";
    const EVENT_NAME_ADDED_LINKEDIN = "ADDED_LINKEDIN";
        const EVENT_NAME_LIKED_FACEBOOK = "LIKED_FACEBOOK";

    /**
     * other events
     */
    const EVENT_NAME_REVIEWED_INSTRUCTIONS = "REVIEWED_INSTRUCTIONS";
    const EVENT_NAME_SENT_PROFESOR_MESSAGE = "SENT_PROFESOR_MESSAGE";
    const EVENT_NAME_DOWNLOADED_CERTIFICATE = "DOWNLOADED_CERTIFICATE";
    const EVENT_NAME_ENTERED_PROFESSOR_MESSAGE_SECTION = "ENTERED_PROFESSOR_MESSAGE_SECTION";
    const EVENT_NAME_ENTERED_INTERACTIVE_GRAMMAR = "ENTERED_INTERACTIVE_GRAMMAR";
    const EVENT_NAME_SELECTED_PRICE_PLAN = "SELECTED_PRICE_PLAN";
    const EVENT_NAME_PAID = "PAID";


    public static function init() {
        return 'cooladata.init({
          "app_key": "'.Yii::app()->config->get("COOLADATA_SDK_API_KEY").'",
          "track_pageload": false,
          "img_src_get_request": true 
        })';
    }

    /**
     * @return bool
     */
    public static function checkUserData()
    {

        //@TODO
        if (!Yii::app()->user) {
            return false;
        }

        return true;
    }

    /**
     * CREATE BY Yii::app()->clientScript->registerScript
     *
     * @param $sEventName
     * @param bool|true $bCheckUserData
     * @param array $stParams
     *
     * @return string
     */
    public static function createDefaultEvent($sEventName, $bCheckUserData = true, $stParams = array())
    {
        $sJavaScriptParams = '';

        if ($bCheckUserData AND !self::checkUserData()) {

            $sJavaScript = '
                console.log("::COOLADATA::101::NOUSER::");
            ';

            return $sJavaScript;
        }

        try {

            $sJavaScript = '
                    try {
            ';

            foreach ($stParams as $sPropertyName => $sPropertyValue) {
                if(!isset($sPropertyValue["type"])) {
                    $sPropertyValue["type"] = "default";
                }
                switch ($sPropertyValue["type"]) {
                    case "js":
                        if (trim($sPropertyValue["value"]) <> "") {
                            $sJavaScript .= '
                        if (typeof ' . $sPropertyValue["value"] . ' === "undefined") {
                            ' . $sPropertyValue["value"] . ' = "";
                        }
            ';
                        }
                        break;
                }
            }

            $sJavaScript .= '
                    }
                    catch (e) {
                        console.log("::COOLADATA::001::" + e.message + "::");
                    }
            ';

            if (count($stParams) > 0) {

                $iParamsCounter = 0;

                foreach ($stParams as $sPropertyName => $sPropertyValue) {
                    if(!isset($sPropertyValue["type"])) {
                        $sPropertyValue["type"] = "default";
                    }

                    if ($iParamsCounter > 0) {
                        $sJavaScriptParams .= ',
                        ';
                    }

                    switch ($sPropertyValue["type"]) {
                        case "js":
                            $sJavaScriptParams .= '
                        "' . $sPropertyName . '":' . $sPropertyValue["value"];
                            break;
                        case "phpInteger":
                            $sJavaScriptParams .= '
                        "' . $sPropertyName . '":' . $sPropertyValue["value"];
                            break;
                        default :
                            $sJavaScriptParams .= '
                        "' . $sPropertyName . '":"' . $sPropertyValue["value"] . '"';
                            break;
                    }

                    ++$iParamsCounter;
                }
            }

            $iUserId = Yii::app()->user->getId();

            $sJavaScript .= '

                $(function() {

                    try {
                        var eventParameters = {';

            if (!empty($iUserId)) {
                $sJavaScript .= '
                            "customerUserId":"' . $iUserId . '"';

                if (trim($sJavaScriptParams) <> '') {

                    $sJavaScript .= ',
                        ' . $sJavaScriptParams;
                }
            } else {
                $sJavaScript .= $sJavaScriptParams;
            }

            $sJavaScript .= '
                        };

                        var eventProperties = new abaUserEventProperties("' . trim($sEventName) . '", eventParameters);
                        sendEventToTracker(eventActions.' . trim($sEventName) . '.name, eventProperties);
                    }
                    catch (e) {
                        console.log("::COOLADATA::002::" + e.message + "::");
                    }
                });
        ';

        } catch (Exception $e) {
            $sJavaScript = '
                console.log("::COOLADATA::003::ERROR::");
        ';
        }

        return $sJavaScript;
    }

    /**
     * @param $sEventName
     *
     * @return string
     */
    public static function createEventLoggedOrRegisterIn($sEventName)
    {

        $sJavaScript = '
            <script type="text/javascript">
                console.log("::COOLADATA::101::NOUSER::");
            </script>
        ';

        if (!self::checkUserData()) {
            return $sJavaScript;
        }

        try {
            $userId = Yii::app()->user->getId();

            $sJavaScript = '
            <script type="text/javascript">

                $(function() {

                    try {

                        var eventParameters = {
                            "customerUserId":"' . Yii::app()->user->getId() . '",

                            "userExpirationDate":"' . Yii::app()->user->expirationDate . '",
                            "userUserType":"' . Yii::app()->user->role . '",
                            "userLangId":"' . (isset(Yii::app()->user->language) ? Yii::app()->user->language : Yii::app()->user->langCourse) . '",
                            "birthDate":"' . Yii::app()->user->birthDate . '",
                            "userLevel":"' . Yii::app()->user->currentLevel . '",
                            "userCountryId":"' . Yii::app()->user->countryId . '",

                            "userPartnerFirstPayId":"' . Yii::app()->user->idPartnerFirstPay . '",
                            "userPartnerSourceId":"' . Yii::app()->user->idPartnerSource . '",
                            "userPartnerCurrentId":"' . Yii::app()->user->idPartnerCurrent . '",

                            "userSourceId":"' . Yii::app()->user->idSourceList . '",
                            "userDeviceType":"' . Yii::app()->user->deviceTypeSource . '",
                            "userEntryDate":"' . Yii::app()->user->entryDate . '",
                            "userProduct":"",
                            "userConversionDate":""
                        };

                        var eventProperties = new abaUserEventProperties("' . $sEventName . '", eventParameters);

                        sendEventToTracker(eventActions.' . $sEventName . '.name, eventProperties);
                    }
                    catch (e) {
                        console.log("::COOLADATA::004::" + e.message + "::");
                    }
                });
            </script>
        ';

        } catch (Exception $e) {
            $sJavaScript = '
            <script type="text/javascript">
                console.log("::COOLADATA::005::ERROR::");
            </script>
        ';
        }

        return $sJavaScript;
    }

    /**
     * CREATE BY Yii::app()->clientScript->registerScript IN mainColumn2
     *
     * @return string
     */
    public static function createDefaultEventForSendToTracker()
    {
        $sJavaScript = ' console.log("::COOLADATA::101::NOUSER::"); ';

        if (!self::checkUserData()) {
            return $sJavaScript;
        }

        try {
            $sJavaScript = ' var sendDefaultEventToTracker = function(sType, stParams) {
                try { 
                    var eventParameters = { "customerUserId":"' . Yii::app()->user->getId() . '" };
                    
                    if(stParams) { 
                        $.each(stParams, function (sKey, sValue) { 
                            eventParameters[sKey] = sValue; 
                        }); 
                    } 
                    
                    var eventProperties = new abaUserEventProperties(sType, eventParameters); 
                    sendEventToTracker(eventActions[sType].name, eventProperties); 
                } catch (e) { 
                    console.log("::COOLADATA::007::" + e.message + "::"); 
                } 
            } ';
        } catch (Exception $e) {
            $sJavaScript = ' console.log("::COOLADATA::008::ERROR::"); ';
        }

        return $sJavaScript;
    }

}
