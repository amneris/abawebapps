<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 18/09/12
 * Time: 17:48
 * Class to manage permissions over operations and profiles of a user.
 */
class HeRoles
{

    /**
     * At the moment due to minimal operation to be distinguished
     *  we have them listed here. Later on maybe we put them in a TXT file
     * or even in the database.
     * @return array
     */
    private static function listOperationsRoles()
    {
        return array(
            DELETED =>     array(""),
            FREE =>     array(OP_CAMPUSACCESS, OP_COURSE, OP_GRAMATICA, OP_VIDEOCLASSES, OP_INCOMPLETEUNIT, OP_MESSAGES_SECTION),
            PREMIUM =>  array(OP_CAMPUSACCESS, OP_COURSE, OP_GRAMATICA, OP_VIDEOCLASSES, OP_MESSAGES_SECTION, OP_MESSAGES_COMPOSE_TEACHER, OP_FULLUNIT) ,
            TEACHER =>  array(OP_CAMPUSACCESS, OP_COURSE, OP_GRAMATICA, OP_VIDEOCLASSES, OP_MESSAGES_SECTION, OP_MESSAGES_COMPOSE_TEACHER, OP_MESSAGES_COMPOSE_ANY, OP_FULLUNIT) ,
        );
    }

    /**
     * @static
     *
     * @param $operation
     * @param $role
     *
     * @return bool
     */
    public static function allowOperation($operation, $role="")
    {
        if ($role == "" || is_null($role)) {
            $role = Yii::app()->user->refreshAndGetUserType();
        }

        $operations = self::listOperationsRoles();
        $operations = $operations[$role];

        if (array_search($operation, $operations) !== false) {
            // Has permission over operation based on its role:
            return true;
        }

        return false;
    }

    public static function typeOfUnitCourseAccess($role=null, $unitId=null)
    {
        return self::typeOfUnitGeneralAccess($role, OP_COURSE, $unitId);
    }

    public static function typeOfVideoClassAccess($role=null, $videoId=null)
    {
        return self::typeOfUnitGeneralAccess($role, OP_VIDEOCLASSES, $videoId);
    }

    /** What kind of access to the Unit the user will have
     * @param integer $role
     * @param integer $operation
     * @param integer $id
     *
     * @return bool|int
     */
    private static function typeOfUnitGeneralAccess( $role=null, $operation, $id )
    {
        if( $role=="" || is_null($role) )
        {
            $role = Yii::app()->user->role;
        }

        if( !self::allowOperation($operation,$role))
        {
            return false;
        }

        // ENH-1124, Big exception for the case of some users:
        if( HeRoles::isWeirdPremiumAccessToUnit()  )
        {
            return self::typeOfUnitGeneralAccessForWeirds($id);
        }

        if( HeUnits::isFirstIdLevel($id) )
        {
            return OP_FULLUNIT;
        }

        if($role>=PREMIUM)
        {
            return OP_FULLUNIT;
        }
        elseif($role==FREE){
            // Is Premium Friend? which means he has gone under member get member:
            $commUser = new UserCommon();
            if ($commUser->isPremiumFriend(Yii::app()->user->getId())) {
                return OP_FULLUNIT;
            }
            return OP_INCOMPLETEUNIT;
        }
        else{
            return false;
        }
    }

    /**
     * @static
     *
     * @param null $role
     *
     * @return int
     */
    public static function getUpperRole($role=null)
    {
        if($role==null){
            $role = Yii::app()->user->role;
        }

        if($role>=MAX_ALLOWED_LEVEL)
        {
            return $role;
        }

        if($role==DELETED){
            return FREE;
        }
        if($role==FREE){
            return PREMIUM;
        }
        if($role==PREMIUM){
            return PLUS;
        }
        if($role==PLUS){
            return PLUS;
        }
        if($role==TEACHER){
            return PLUS;
        }

        return (Yii::app()->user->role + 1);
    }

    /**
     * @static
     *
     * @param null $role
     *
     * @return int
     */
    public static function getLowerRole($role=null)
    {
        if($role==null){
            $role = Yii::app()->user->role;
        }
        if($role==PLUS){
            return PREMIUM;
        }
        if($role==PREMIUM){
            return FREE;
        }
        if($role==FREE){
            return DELETED;
        }
        if($role==DELETED){
            return DELETED;
        }

        return (Yii::app()->user->role - 1);
    }

    public static function getLitRole($role=null)
    {
        if($role==null){
            $role = Yii::app()->user->role;
        }
        return Yii::app()->params['userType'][($role)];
    }

    /** Respond to special case dictated by ENH-1121
     *  This function and all its surrounding logic is to be removed on the 15/05/2013 approximately.
     * @static
     * @return bool
     */
    private static function isWeirdPremiumAccessToUnit(  )
    {
        $aUsersWeirdIds = array( );
        if( !Yii::app()->config->get("USERS_PREMIUM_WEIRD_ACCESS")=='' && !Yii::app()->config->get("UNITS_PREMIUM_WEIRD_ACCESS")=='' )
        {
            $USERS_PREMIUM_WEIRD_ACCESS = Yii::app()->config->get("USERS_PREMIUM_WEIRD_ACCESS");
            if( strpos($USERS_PREMIUM_WEIRD_ACCESS,",")>=1 )
            {
                $aUsersWeirdIds = explode(",", $USERS_PREMIUM_WEIRD_ACCESS);
                $aUsersWeirdIdsTmp = $aUsersWeirdIds;
                $i = 0;
                foreach ( $aUsersWeirdIdsTmp as $userId)
                {
                    $aUsersWeirdIds[$i] = intval( $userId );
                    $i = $i+1;
                }
            }
            else
            {
                $aUsersWeirdIds = array( intval($USERS_PREMIUM_WEIRD_ACCESS) );
            }

            if( in_array( intval(Yii::app()->user->getId()), $aUsersWeirdIds, true ) )
            {
                return true;
            }
        }
        else
        {
            return false;
        }
        return false;
    }

    /**  This function and all its surrounding logic is to be removed on the 15/05/2013 approximately.
     * @static
     *
     * @param $unitId
     *
     * @return bool|int
     */
    private static function typeOfUnitGeneralAccessForWeirds( $unitId)
    {
        $aUnitsWeirdIds = array();
        if( Yii::app()->config->get("USERS_PREMIUM_WEIRD_ACCESS")!=='' && Yii::app()->config->get("UNITS_PREMIUM_WEIRD_ACCESS")!=='' )
        {
            $UNITS_PREMIUM_WEIRD_ACCESS = Yii::app()->config->get("UNITS_PREMIUM_WEIRD_ACCESS");

            if( strpos($UNITS_PREMIUM_WEIRD_ACCESS,",")>=0 )
            {
                $aUnitsWeirdIds = explode(",", $UNITS_PREMIUM_WEIRD_ACCESS);
                $aUnitsWeirdIdsTmp = $aUnitsWeirdIds;
                $i = 0;
                foreach ( $aUnitsWeirdIdsTmp as $id)
                {
                    $aUnitsWeirdIds[$i] = intval( $id );
                    $i = $i+1;
                }
            }
            else
            {
                $aUnitsWeirdIds = array( intval($UNITS_PREMIUM_WEIRD_ACCESS) );
            }

            if( in_array( intval($unitId), $aUnitsWeirdIds, true) )
            {
                return OP_FULLUNIT;
            }
            else
            {
                return OP_NOACTIONACCESSUNIT;
            }

        }
        else{
            return false;
        }

        return false;
    }
}
