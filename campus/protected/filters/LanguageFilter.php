<?php

class LanguageFilter extends CFilter
{

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function preFilter($filterChain)
    {
        if (Yii::app()->user->isLogged() && Yii::app()->user->language !== Yii::app()->language) {
            $route = Yii::app()->getUrlManager()->parseUrl(Yii::app()->request);
            $params = $_GET;
            $params['language'] = Yii::app()->user->language;
            Yii::app()->request->redirect(
              Yii::app()->getUrlManager()->createUrl($route,$params),
              true
            );
            return false;
        }
        return true;
    }

}
