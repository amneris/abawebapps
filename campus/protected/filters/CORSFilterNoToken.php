<?php

class CORSFilterNoToken extends CFilter
{

	/**
	 * @var array Basic headers handled for the CORS requests.
	 */
	public $cors = [
		'Origin' => ['*'],
		'Access-Control-Allow-Origin' => ['*'],
		'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
		'Access-Control-Request-Headers' => [],
		'Access-Control-Allow-Credentials' => null,
		'Access-Control-Max-Age' => 86400,
		'Access-Control-Expose-Headers' => [],

	];

	/**
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	protected function preFilter($filterChain)
	{
		/*if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		} else {
			header("Access-Control-Allow-Origin: *");
		}*/

		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Methods: " . implode(', ', $this->cors['Access-Control-Request-Method']));
		header("Access-Control-Allow-Headers: " . implode(', ', $this->cors['Access-Control-Request-Headers']));

		return true;
	}
}