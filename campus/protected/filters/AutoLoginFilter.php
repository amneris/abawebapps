<?php

class AutoLoginFilter extends CFilter
{

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function preFilter($filterChain)
    {
//        if ( $filterChain->controller->id == 'site' && $filterChain->controller->action->id == 'login' ) return true;

        $autol = HeMixed::getGetArgs(MAGIC_LOGIN_KEY);

        if ($autol && Yii::app()->user->isLogged() && Yii::app()->user->getKeyExternalLogin() !== $autol) {
            Yii::app()->user->logout();

            $form = new LoginForm();
            if (!$form->autoLogin($autol)) {
                Yii::app()->request->redirect(
                  Yii::app()->createUrl('site/login', array(), true)
                );

                return false;
            }
        }

        return true;
    }

}
