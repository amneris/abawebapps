<?php

return array(
    'Try_Premium_1' => 'El curso de ',
    'ABA English' => 'ABA English',
    'Try_Premium_2' => ' es mucho más.',
    'Try_Premium_3' => '¿Quieres probar el curso completo?',
    'Try_Button' => 'Pruébalo gratis',
    'Email_notif' => 'Te enviamos un acceso a tu email',
    'Aba_Films' => '1. ABA Films',
    'Aba_Films_desc' => 'Verás el cortometraje de una situación actual de la vida real.',
    'Habla' => '2. Habla',
    'Habla_desc' => 'Estudiarás el diálogo del ABA Film',
    'Escribe' => '3. Escribe',
    'Escribe_desc' => 'Realizarás un dictado con todas las frases del ABA Film',
    'Interpreta' => '4. Interpreta',
    'Interpreta_desc' => 'Vivirás una situación real del ABA Film interpretando a los personajes',
    'Videoclase' => '5. Videoclase',
    'Videoclase_desc' => 'Aprenderás la gramática con ejemplos y podrás consultarla por escrito',
    'Ejercicios' => '6. Ejercicios',
    'Ejercicios_desc' => 'Practicarás todo lo aprendido con ejercicios escritos.',
    'Vocabulario' => '7. Vocabulario',
    'Vocabulario_desc' => 'Consolidarás y aprenderás a pronunciar correctamente el nuevo vocabulario',
    'Evaluacion' => '8. Evaluación',
    'Evaluacion_desc' => 'Realizarás una prueba final para comprobar todo lo aprendido',
    'Email_sent' => 'Email enviado',
    'Email_sent_desc_1' => 'Te hemos enviado un  email.',
    'Email_sent_desc_2'=> 'Por favor, consúltalo desde un ordenador con Flash Player.',
    'Working_Full_App' => '¡Estamos trabajando para que puedas acceder también al curso completo desde la app!'
);

?>