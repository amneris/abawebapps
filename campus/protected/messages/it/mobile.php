<?php

return array(
    'Try_Premium_1' => 'Il corso di ',
    'ABA English' => 'ABA English',
    'Try_Premium_2' => ' è molto di più…',
    'Try_Premium_3' => 'Vuoi provare il corso completo?',
    'Try_Button' => 'Provalo gratis',
    'Email_notif' => 'Ti inviamo un accesso alla tua email',
    'Aba_Films' => '1. ABA Films',
    'Aba_Films_desc' => 'Vedrai il cortometraggio di una situazione attuale della vita reale.',
    'Habla' => '2. Parla',
    'Habla_desc' => 'Studierai il dialogo dell’ABA Film',
    'Escribe' => '3. Scrivi',
    'Escribe_desc' => 'Realizzerai un dettato con tutte le frasi dell’ABA Film',
    'Interpreta' => '4. Interpreta',
    'Interpreta_desc' => 'Vivrai una situazione reale dell’ABA Film interpretando i personaggi',
    'Videoclase' => '5. Videolezione',
    'Videoclase_desc' => 'Imparerai la grammatica con esempi e potrai consultarla per iscritto',
    'Ejercicios' => '6. Esercizi',
    'Ejercicios_desc' => 'Ti eserciterai su tutto quello che hai imparato con esercizi scritti.',
    'Vocabulario' => '7. Vocabolario',
    'Vocabulario_desc' => 'Consoliderai e imparerai a pronunciare correttamente il nuovo vocabolario',
    'Evaluacion' => '8. Verifica',
    'Evaluacion_desc' => 'Realizzerai una prova finale per verificare tutto quello che hai imparato',
    'Email_sent' => 'Email inviata',
    'Email_sent_desc_1' => 'Ti abbiamo mandato un’email.',
    'Email_sent_desc_2'=> 'Per favore, consultala da un computer con Flash Player.',
    'Working_Full_App' => 'A breve sarà anche possibile accedere al corso completo da quest\'app!'
);

?>