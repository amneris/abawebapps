<?php

return array(
    'Try_Premium_1' => ' ',
    'ABA English' => 'ABA English\'s',
    'Try_Premium_2' => ' course is much more…',
    'Try_Premium_3' => 'Do you want to try out the complete course?',
    'Try_Button' => 'Test it for free',
    'Email_notif' => 'We will send you an email with your login details',
    'Aba_Films' => '1. ABA Films',
    'Aba_Films_desc' => 'You will watch a short film about a current real-life situation.',
    'Habla' => '2. Speak',
    'Habla_desc' => 'You will study the dialogue from the ABA Film.',
    'Escribe' => '3. Write',
    'Escribe_desc' => 'You will do a dictation with all the sentences from the ABA Film.',
    'Interpreta' => '4. Interpret',
    'Interpreta_desc' => 'You will experience a real situation from the ABA Film by interpreting the characters.',
    'Videoclase' => '5. Video Class',
    'Videoclase_desc' => 'You will learn grammar with examples and you will be able to consult the written version.',
    'Ejercicios' => '6. Exercises',
    'Ejercicios_desc' => 'You will practice what you have learnt with written exercises.',
    'Vocabulario' => '7. Vocabulary',
    'Vocabulario_desc' => 'You will consolidate and learn to pronounce the new vocabulary correctly.',
    'Evaluacion' => '8. Assessment',
    'Evaluacion_desc' => 'You will do an end of unit test to check what you have learnt.',
    'Email_sent' => 'Email sent',
    'Email_sent_desc_1' => 'We have sent you an email.',
    'Email_sent_desc_2'=> 'Please check your email from a computer with Flash Player.',
    'Working_Full_App' => 'We are working towards making the complete course available to you from the app!'
);

?>