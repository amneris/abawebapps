<?php

return array(
    'Try_Premium_1' => ' ',
    'ABA English' => 'Der ABA English Kurs',
    'Try_Premium_2' => ' ist viel mehr…',
    'Try_Premium_3' => 'Willst Du den gesamten Kurs ausprobieren?',
    'Try_Button' => 'Kostenlos testen',
    'Email_notif' => 'Wir schicken dir Zugang zu deinem E-Mail',
    'Aba_Films' => '1. ABA Films',
    'Aba_Films_desc' => 'Hier siehst du einen Kurzfilm von einer tatsächlichen realen Situation.',
    'Habla' => '2. Sprechen',
    'Habla_desc' => 'Hier studierst du den Dialog des ABA Films.',
    'Escribe' => '3. Schreiben',
    'Escribe_desc' => 'Hier schreibst du die Sätze des ABA Films wie bei einem Diktat.',
    'Interpreta' => '4. Interpretieren',
    'Interpreta_desc' => 'Hier erlebst du eine reale Situation indem du jeden Charakter des Videos interpretierst.',
    'Videoclase' => '5. Video-Lektion',
    'Videoclase_desc' => 'Hier wirst du die Grammatik Beispielen lernen und kannst sie schriftlich konsultieren.',
    'Ejercicios' => '6. Übungen',
    'Ejercicios_desc' => 'Hier kannst du alles, was du gelernt hast, schriftlich üben.',
    'Vocabulario' => '7. Wortschatz',
    'Vocabulario_desc' => 'Hier konsolidierst du den neuen Wortschazt und lernst es richtig auszusprechen.',
    'Evaluacion' => '8. Prüfung',
    'Evaluacion_desc' => 'Hier machst du einen Abschlusstest, um alles was du gelernt hast zu überprüfen.',
    'Email_sent' => 'Gesendete E-Mail',
    'Email_sent_desc_1' => 'Wir haben dir eine E-Mail geschickt.',
    'Email_sent_desc_2'=> 'Bitte öffne sie auf einem Computer mit Flash Player.',
    'Working_Full_App' => 'Wir arbeiten daran, damit du Zugang zu dem kompletten Kurs durch die Anwendung hast!'
);

?>