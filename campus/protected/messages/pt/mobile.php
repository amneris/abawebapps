<?php

return array(
    'Try_Premium_1' => 'O curso da ',
    'ABA English' => 'ABA English',
    'Try_Premium_2' => ' é muito mais…',
    'Try_Premium_3' => 'Quer provar o curso completo?',
    'Try_Button' => 'Prove-o  grátis',
    'Email_notif' => 'Enviamos-lhe  o acesso ao seu email',
    'Aba_Films' => '1. ABA Films',
    'Aba_Films_desc' => 'Verá o corto-metragem de uma situação actual da vida real.',
    'Habla' => '2. Fale',
    'Habla_desc' => 'Estudará o diálogo do ABA Film',
    'Escribe' => '3. Escreva ',
    'Escribe_desc' => 'Realizará um ditado com todas as frases do  ABA Film',
    'Interpreta' => '4. Interprete',
    'Interpreta_desc' => 'Viverá uma situação real da ABA Film interpretando os personagens',
    'Videoclase' => '5. Vídeo-aula',
    'Videoclase_desc' => ' A gramática com exemplos e poderá consulta-la  por escrito',
    'Ejercicios' => '6. Exercícios',
    'Ejercicios_desc' => 'Practicará tudo o que aprendeu com  exercícios escritos.',
    'Vocabulario' => '7. Vocabulário',
    'Vocabulario_desc' => 'Consolidará a sua pronuncia correctamente ao novo vocabulário',
    'Evaluacion' => '8. Avaliação',
    'Evaluacion_desc' => 'Realizará uma prova final para comprovar tudo o que aprendeu.',
    'Email_sent' => 'Email enviado',
    'Email_sent_desc_1' => 'Enviámos-lhe um  email.',
    'Email_sent_desc_2'=> 'Por favor, consulte desde um computador com Flash Player.',
    'Working_Full_App' => 'Estamos trabalhando para que você possa aceder também ao curso completo desde o app!'
);

?>