<?php
/* @var $this LogReconciliationController */
/* @var $moLogReconc LogReconciliation */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
    'id'=>'formLogReconcSearch',
)); ?>


	<div class="row">
		<?php echo $form->label($moLogReconc,'filePath'); ?>
		<?php echo $form->textField($moLogReconc,'filePath',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label( $moLogReconc,'paySuppExtId');
        echo $form->DropDownList( $moLogReconc,'paySuppExtId',
                                HeList::supplierExtGatewayList(), array('empty'=>'Method payment')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($moLogReconc,'dateOperation');
             echo $form->textField($moLogReconc,'dateOperation');
             echo CHtml::image("images/calendar.jpg","calendar1", array("id"=>"c_buttonStart","class"=>"pointer"));
                $this->widget('application.extensions.calendar.SCalendar',
                        array(
                            'inputField'=>'LogReconciliation_dateOperation',
                            'button'=>'c_buttonStart',
                            'ifFormat'=>'%Y-%m-%d 00:00:00',
                        ));
                ?>
	</div>

	<div class="row">
		<?php echo $form->label($moLogReconc,'idPayment'); ?>
		<?php echo $form->textField($moLogReconc,'idPayment',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($moLogReconc,'refPaySuppOrderId'); ?>
		<?php echo $form->textField($moLogReconc,'refPaySuppOrderId', array('size'=>60,'maxlength'=>80)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($moLogReconc,'refPaySuppStatus');
              echo $form->DropDownList( $moLogReconc,'refPaySuppStatus',
                        HeList::statusDescriptionFull(), array('empty'=>'Select status'));
        ?>
	</div>

	<div class="row">
		<?php echo $form->label($moLogReconc,'refAmountPrice'); ?>
		<?php echo $form->textField($moLogReconc,'refAmountPrice',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($moLogReconc,'refDateEndTransaction');
		echo $form->textField($moLogReconc,'refDateEndTransaction');
        echo CHtml::image("images/calendar.jpg","calendar1", array("id"=>"c_buttonStart","class"=>"pointer"));
        $this->widget('application.extensions.calendar.SCalendar',
            array(
                'inputField'=>'LogReconciliation_refDateEndTransaction',
                'button'=>'c_buttonStart',
                'ifFormat'=>'%Y-%m-%d 00:00:00',
            ));
        ?>
	</div>

	<div class="row">
		<?php echo $form->label($moLogReconc,'refCurrencyTrans');
              echo $form->DropDownList( $moLogReconc,'refCurrencyTrans',
                  array('EUR'=>'EUR', 'USD'=>'USD', 'BRL'=>'BRL', 'MXN'=>'MXN'), array('maxlength'=>3,'empty'=>'Select currency code'));
        ?>
	</div>

	<div class="row">
		<?php echo $form->label($moLogReconc,'refPaySuppExtUniId'); ?>
		<?php echo $form->textField($moLogReconc,'refPaySuppExtUniId',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($moLogReconc,'success');
              echo $form->DropDownList( $moLogReconc,'success',
            HeList::getYesNoAliasBool(), array('empty'=>'YES or NO'));
        ?>
	</div>

	<div class="row">

		<?php echo $form->label($moLogReconc,'matchType');
              echo $form->DropDownList( $moLogReconc,'matchType',
                                        HeList::getFileLogReconciliationStatus(),
                                        array('empty'=>'select line status'));
        ?>


	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton( 'Search');
              echo CHtml::button( "Export", array('onclick' => 'exportLogReconcToExcel();',
                                                  'title'=>"Export",
                                                  'id'=>'exportLogReconcButton') );
        ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->