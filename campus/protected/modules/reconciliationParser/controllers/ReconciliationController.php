<?php
/**
 * Class ReconciliationController
 */
class ReconciliationController extends AbaController
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column1';


    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(  'allow',
                'actions'=>array('ImportFile','DownloadFile' ),
                'users'=>array('root'),
            ),

            array(  'allow',
                'actions'=>array('ImportFile','DownloadFile' ),
                'users'=>array('support'),
            ),

            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

	public function actionIndex()
	{
		$this->render('index');
	}


    /**
     * Form to import and process reconciliation files.
     */
    public function actionImportFile()
    {
        $tempPath = "";
        $frmImport = $this->getFrmDefaultImp();
        if (empty($_POST)) {
            $this->render('importFile', array('frmImport' => $frmImport, 'fileLink' => $tempPath));
            return;

        } else {
            $frmImport->attributes = $_POST['ImportReconciliationForm'];
            /* @var $frmImport->fileName CUploadedFile */
            $frmImport->fileName = CUploadedFile::getInstance($frmImport, 'fileName');
            if (!$frmImport->validate()) {
                $this->render('importFile', array('frmImport' => $frmImport, 'fileLink' => $tempPath));
                return;
            }

            $commParserXml = new FileParserLaCaixa($frmImport->paySupplierExtId);
            if ($commParserXml->validateFile()) {
                if($commParserXml->saveFile($frmImport->fileName)){
                    $aMoLogsReconc = $commParserXml->parseToModels();
                    if(!$aMoLogsReconc){
                        $frmImport->addError("fileName", "File has not the right FORMAT: ".$commParserXml->getErrorMsg());
                    }else{
                        $commMatching = new ReconcMatchingLaCaixa($frmImport->paySupplierExtId);
                        $isSuccess = $commMatching->processPackTransactions($aMoLogsReconc, $commParserXml->getFilePath());
                        $this->render('importFile', array('frmImport' => $frmImport,
                                                          'fileLink' => $commParserXml->getFileName(),
                                                          'isSuccess'=> $isSuccess));
                        return;
                    }
                } else{
                    $frmImport->addError("fileName", "File could not be saved: ".$commParserXml->getErrorMsg());
                }
            }
        }

        $this->render('importFile', array('frmImport' => $frmImport, 'fileLink' => $tempPath));
        return;
    }

    /** It is called after Reconciliation file has been processed
     *
     * @param $fileName
     */
    public function actionDownloadFile( $fileName )
    {
        $commParserXml = new FileParserLaCaixa();
        $pathToFile = $commParserXml->getFilePath($fileName);
        if( !$pathToFile ){
            $frmImport = $this->getFrmDefaultImp();
            $frmImport->addError("fileName", "File ".$fileName." was no found in the hard drive.");
            $this->render('importFile', array('frmImport' => $frmImport, 'fileLink' => ''));
            return;
        }

        Yii::app()->getRequest()->sendFile( $pathToFile, file_get_contents( $pathToFile ) );
        return;
    }


    /** Form model default with default attributes.
     *
     * @return ImportReconciliationForm
     */
    protected function getFrmDefaultImp()
    {
        $frmImport = new ImportReconciliationForm();
        $frmImport->dateOperation = HeDate::SQLToEuropean(HeDate::todaySQL(false));
        $frmImport->paySupplierExtId = PAY_SUPPLIER_CAIXA;

        return $frmImport;
    }

}