<?php

class LogReconciliationController extends AbaController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin','index','view','search','export'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin','create','update','search','export'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','search','export'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}


	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$moLogReconc=new LogReconciliation('search');
		$moLogReconc->unsetAttributes();  // clear any default values
		if(isset($_GET['LogReconciliation'])){
			$moLogReconc->attributes=$_GET['LogReconciliation'];
        }

		$this->render('admin',array(
			'moLogReconc'=>$moLogReconc,
		));
	}

    /**
     *
     */
    public function actionExport()
    {
        $moLogReconc = new LogReconciliation('search');
        $moLogReconc->unsetAttributes(); // clear any default values
        if (isset($_GET['LogReconciliation'])) {
            $moLogReconc->attributes = $_GET['LogReconciliation'];
        }
        $cProvider = $moLogReconc->search(false);
        /* @var $moLog LogReconciliation[] */
        $aMoLogs = $cProvider->getData();

        $xls = new JPhpCsv('UTF-8', false, 'Log files reconciliation');
        $data = array();
        $flagFirst = true;
        foreach ($aMoLogs as $moLog) {
            $row = $moLog->getAttributes();
            if ($flagFirst) {
                $flagFirst = false;
                $theRow = array();
                foreach ($row as $key => $value) {
                    array_push($theRow, $key);
                }
                array_push($data, $theRow);
            }
            $theRow = array();
            foreach ($row as $key => $value) {
                array_push($theRow, $value);
            }
            array_push($data, $theRow);
        }
        if ($flagFirst == true) {
            $theRow = array();
            array_push($theRow, "DATA NOT FOUND(Data not exported)");
            array_push($data, $theRow);
        }
        $xls->addArray($data);
        $xls->generateXML("logReconciliation_" . HeDate::todaySQL(true));

    }


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
     *
     * @param $id
     * @return CActiveRecord
     * @throws CHttpException
	 */
    public function loadModel($id)
	{
		$model=LogReconciliation::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='log-reconciliation-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
