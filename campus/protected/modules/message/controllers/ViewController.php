<?php

class ViewController extends AbaController {

	public $defaultAction = 'view';

	public function actionView()
    {
        $accessCompose = true;
        if( !HeRoles::allowOperation( OP_MESSAGES_SECTION) )
        {
            $this->redirect( $this->createUrl('site/noaccess') );
        }
        elseif(!HeRoles::allowOperation( OP_MESSAGES_COMPOSE_TEACHER) && !Yii::app()->user->isPremiumFriend() )
        {
            $accessCompose = false;
        }

		$messageId = (int)Yii::app()->request->getParam('message_id');
		$viewedMessage = Message::model()->findByPk($messageId);

		if(!$viewedMessage){
			 throw new CHttpException(404, MessageModule::t('Message not found'));
		}

		$userId = Yii::app()->user->getId();

		if($viewedMessage->sender_id != $userId && $viewedMessage->receiver_id != $userId){
		    throw new CHttpException(403, MessageModule::t('You can not view this message'));
		}
		if(($viewedMessage->sender_id == $userId && $viewedMessage->deleted_by == Message::DELETED_BY_SENDER)
		    || $viewedMessage->receiver_id == $userId && $viewedMessage->deleted_by == Message::DELETED_BY_RECEIVER){
		    throw new CHttpException(404, MessageModule::t('Message not found'));
		}
		$message = new Message();

        $isIncomeMessage = $viewedMessage->receiver_id == $userId;
		if($isIncomeMessage){
		    $message->subject = preg_match('/^Re:/',$viewedMessage->subject) ? $viewedMessage->subject : 'Re: ' . $viewedMessage->subject;
			$message->receiver_id = $viewedMessage->sender_id;
        }else{
			$message->receiver_id = $viewedMessage->receiver_id;
        }

		if(Yii::app()->request->getPost('Message')){
            $formMess = Yii::app()->request->getPost('Message');
			$message->attributes = $formMess;
            $message->body = nl2br($formMess['body']);
			$message->sender_id = $userId;

            if(Yii::app()->user->role == TEACHER) {
                $message->sendFromRole = TEACHER;
            }

            if ($message->save()) {
				Yii::app()->user->setFlash('success', MessageModule::t('Message has been sent'));
			    if ($isIncomeMessage) {
					$this->redirect($this->createUrl('inbox/'));
			    } else {
					$this->redirect($this->createUrl('sent/'));
				}
			}
		}

		if($isIncomeMessage){
			$viewedMessage->markAsRead();
		}
        $user = new AbaUser();
        $user->getUserById($viewedMessage->sender_id);
        $NameComplete = $user->attributes['name'].' '.$user->attributes['surnames'];
        $receiverName = $message->getReceiverName();

        $user = new AbaUser();
        $user->getUserById(Yii::app()->user->getId());
        if(isset($user->teacherId)) {
            $moTeacher = new AbaTeacher();
            $moTeacher = $moTeacher->getTeacherById($user->teacherId);
            if(isset($moTeacher->photo)) {
                $teacherPicture = $moTeacher->photo;
            }
        }
        if(Yii::app()->user->role==TEACHER){
            $HistoryMessages = Message::getAdapterForHistory(Yii::app()->user->getId(), $viewedMessage->sender_id);
            $this->render(Yii::app()->getModule('message')->viewPath . '/view', array('accessCompose' => $accessCompose,
                                                                                    'viewedMessage' => $viewedMessage,
                                                                                    'message' => $message, 'HistoryMessages' => $HistoryMessages,
                                                                                    'receiverName' => $receiverName, 'NameComplete' => $NameComplete,
                                                                                    'teacherPicture' => $teacherPicture));
        }
        else{
            $this->render(Yii::app()->getModule('message')->viewPath . '/view', array('accessCompose' => $accessCompose,
                                                                                    'viewedMessage' => $viewedMessage,
                                                                                    'message' => $message,
                                                                                    'receiverName' => $receiverName, 'NameComplete' => $NameComplete,'teacherPicture' => $teacherPicture));
        }
	}

}
