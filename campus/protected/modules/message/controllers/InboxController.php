<?php

class InboxController extends AbaController
{
    public $defaultAction = 'inbox';

    public function actionInbox()
    {
        $accessCompose = true;
        if( !HeRoles::allowOperation( OP_MESSAGES_SECTION ) )
        {
            $this->redirect( $this->createUrl('site/noaccess') );
        }
        elseif(!HeRoles::allowOperation( OP_MESSAGES_COMPOSE_TEACHER ) && !Yii::app()->user->isPremiumFriend())
        {
            $accessCompose = false;
        }
        $user = new AbaUser();
        $user->getUserById(Yii::app()->user->getId());

        if(isset($user->teacherId)) {
            $moTeacher = new AbaTeacher();
            $moTeacher = $moTeacher->getTeacherById($user->teacherId);
            if(isset($moTeacher->photo)) {
                $teacherPicture = $moTeacher->photo;
            }
        }

        $notRead = Message::model()->getCountUnreaded(Yii::app()->user->getId());
        $messagesAdapter = Message::getAdapterForInbox(Yii::app()->user->getId());
        $pager = new CPagination($messagesAdapter->totalItemCount);
        $pager->pageSize = 10;
        $messagesAdapter->setPagination($pager);
        $receiverName = '';
        $teacherId= (Yii::app()->user->role==TEACHER)? Yii::app()->user->getId(): Yii::app()->user->getTeacherId();
        $theTeacher = new AbaTeacher( $teacherId );
        if(get_class($theTeacher)=='AbaTeacher')
        {
            $receiverName = $theTeacher->email;
            $receiverVisibleName = $theTeacher->name." ".$theTeacher->surnames ;
        }

        $this->render(Yii::app()->getModule('message')->viewPath . '/inbox', array(
                'messagesAdapter' => $messagesAdapter,
                'notRead' => $notRead,
                'receiverName' => $receiverName,
                'accessCompose' => $accessCompose,
                'receiverVisibleName' => $receiverVisibleName,
                'teacherPicture' => $teacherPicture
            )
        );
    }
}
