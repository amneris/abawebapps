<?php

class HistoryController extends AbaController
{
    public $defaultAction = 'history';

    public function actionHistory()
    {
        Yii::app()->getModule('message');

        $accessCompose = true;
        if(!HeRoles::allowOperation( OP_MESSAGES_SECTION )){
            $this->redirect($this->createUrl('site/noaccess'));
        }
        elseif(!HeRoles::allowOperation(OP_MESSAGES_COMPOSE_TEACHER  && !Yii::app()->user->isPremiumFriend())){
            $accessCompose = false;
        }
        (!is_null(Yii::app()->request->getParam('idTeacher')) && !is_null(Yii::app()->request->getParam('idStudent'))) ? '' : $this->redirect(Yii::app()->createUrl('site/index'));
        $idTeacher = Yii::app()->request->getParam('idTeacher');
        $idStudent = Yii::app()->request->getParam('idStudent');
        (Yii::app()->user->getId() != $idTeacher) ? $this->redirect($this->createUrl('site/noaccess')) : '';
        $notRead = Message::model()->getCountUnreadedHistory($idTeacher, $idStudent);
        //die("not read: ".$notRead." idteacher: ".$idTeacher." idstudent: ".$idStudent);
        $messagesAdapter = Message::getAdapterForHistory($idTeacher, $idStudent);
        $pager = new CPagination($messagesAdapter->totalItemCount);
        $pager->pageSize = 10;
        $messagesAdapter->setPagination($pager);
        $receiverName = '';
        $teacherId = (Yii::app()->user->role==TEACHER) ? Yii::app()->user->getId() : Yii::app()->user->getTeacherId();
        $theTeacher = new AbaTeacher($teacherId);
        if(get_class($theTeacher)=='AbaTeacher')
        {
            $receiverName = $theTeacher->email;
            $receiverVisibleName = $theTeacher->name." ".$theTeacher->surnames;
        }

        $user = new AbaUser();
        $user->getUserById(Yii::app()->user->getId());
        if(isset($user->teacherId)) {
            $moTeacher = new AbaTeacher();
            $moTeacher = $moTeacher->getTeacherById($user->teacherId);
            if(isset($moTeacher->photo)) {
                $teacherPicture = $moTeacher->photo;
            }
        }

        $this->render(Yii::app()->getModule('message')->viewPath . '/history', array('messagesAdapter' => $messagesAdapter,
            'notRead' => $notRead,
            'receiverName' => $receiverName,
            'accessCompose' => $accessCompose,
            'receiverVisibleName' => $receiverVisibleName,'teacherPicture' => $teacherPicture));
    }
}