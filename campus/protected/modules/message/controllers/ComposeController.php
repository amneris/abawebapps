<?php

class ComposeController extends AbaController
{

	public $defaultAction = 'compose';

    public function actionCompose($id = NULL)
    {
        $accesCompose = true;
        if( !HeRoles::allowOperation( OP_MESSAGES_SECTION )  || (!HeRoles::allowOperation( OP_MESSAGES_COMPOSE_TEACHER ) && !Yii::app()->user->isPremiumFriend()) )
        {
            $this->redirect( $this->createUrl('site/noaccess') );
        }
        elseif(!HeRoles::allowOperation( OP_MESSAGES_COMPOSE_TEACHER ) && !Yii::app()->user->isPremiumFriend() )
        {
            $accesCompose = false;
        }

        $message = new Message();
        $receiverName = '';
        $receiverVisibleName = '';
        // Sending the message:
        if (Yii::app()->request->getPost('Message'))
        {
            $receiverName = Yii::app()->request->getPost('receiver');
            $formMess = Yii::app()->request->getPost('Message');
        		$message->attributes = $formMess;
            $message->body = nl2br($formMess['body']);
            $message->receiver_id = $formMess['receiver_id'];
            $message->sender_id = Yii::app()->user->getId();

            if(Yii::app()->user->role == TEACHER) {
                $message->sendFromRole = TEACHER;
            }

            if($message->save())
            {
                Yii::app()->user->setFlash('messageModule', Yii::t('mainApp', 'El mensaje ha sido enviado'));
                $this->redirect($this->createUrl('inbox/'));
            }
            else if ($message->hasErrors('receiver_id'))
            {
                $message->receiver_id = NULL;
                $receiverName = "";
            }
        }
        // Preparing data for the COMPOSE FORM:
        else
        {
            //get the idUser of teacher;
            if( intval(Yii::app()->user->role)!==TEACHER )
            {
                $user = new AbaUser();
                $user->getUserById(Yii::app()->user->getId());
                $theTeacher = new AbaTeacher( Yii::app()->user->getTeacherId() );
                if( get_class( $theTeacher )=='AbaTeacher' )
                {
                    $message->receiver_id=$theTeacher->id;
                    $receiverName = $theTeacher->email;
                    $receiverVisibleName = $theTeacher->name." ".$theTeacher->surnames ;
                }
            }
            else
            {
                $message->receiver_id="";
            }
        }

        $user = new AbaUser();
        $user->getUserById(Yii::app()->user->getId());

        if(isset($user->teacherId)) {
            $moTeacher = new AbaTeacher();
            $moTeacher = $moTeacher->getTeacherById($user->teacherId);
            if(isset($moTeacher->photo)) {
                $teacherPicture = $moTeacher->photo;
            }
        }

        $this->render(Yii::app()->getModule('message')->viewPath . '/compose',
                    array('model' => $message,
                        'receiverName' => $receiverName,
                        'accessCompose' => $accesCompose,
                        'receiverVisibleName' => $receiverVisibleName,
                        'teacherPicture' => $teacherPicture ));
    }
}
