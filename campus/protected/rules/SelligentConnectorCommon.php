<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 18/12/12
 * Time: 15:37
 * CHECK file SelligentLoad.sql in this very same folder.
 */
class SelligentConnectorCommon
{
    protected $nameOperation;
    protected $aFieldsToBeSent;
    protected $response;
    protected $errMessage;
    /* @var LogSentSelligent $logSentSell */

    protected $logSentSell;

    protected static $yes='si';
    protected static $no='no';

    // user, user_objective, payments, aba_partners_list, followup4
    protected static $aFieldsSelligent = array(
        'IDUSER_WEB' => 'user.id',
        'MAIL' => 'user.email',
        'NAME' => 'user.name',
        'SURNAME' => 'user.surnames',
        'COUNTRY_IP' => 'user.countryId',
        'COUNTRY_USER' => 'user.countryIdCustom',
        'PASS_TMP' => 'user.password',
        'PASSWORD' => 'user.keyExternalLogin',
        'BIRTHDAY_DT' => 'user.birthDate',
        'SEX' => 'user.gender',
        'TELEPHONE' => 'user.telephone',
        'CANCEL_REASON' => 'user.cancelReason',
        'WHY_STUDY' => 'user_objective.whyStudy',
        'OBJETIVES_LEVEL' => 'user_objective.levelToReach',
        'PREVIOUS_EXPERIENCE' => 'user_objective.previousEngExperience',
        'OCCUPATION' => 'user_objective.occupationText',
        'PREF_LANG' => 'user.langEnv',
        'ENTRY_DATE' => 'user.entryDate',
        'USER_TYPE' => 'user.userType',
        'EXPIRATION_DATE' => 'user.expirationDate',
        'PRODUCT_TYPE' => 'payments.idPeriodPay', // payments.getLastPaymentByUserId()
        'PROMOCODE' => '.',
        'LEVEL_COURSE' => 'user.currentLevel',
        'FLAG_CAMPUS_LOGIN' => 'user.firstLoginDone',
        'FLAG_CAMPUS_WORK' => 'user.hasWorkedCourse',
        'PREMIUM_HISTORIC' => 'payments.LastSuccessPayment', //payments.getLastSuccessPayment
        'PARTNERT_SOURCE' => 'user.idPartnerSource',
        'PARTNERT_SOURCE_GROUP' => 'aba_partners_list.idPartnerGroup_Source', // AbaPartner.getPartnerByIdPartner()
        'PARTNERT_FIRSTPAY' => 'user.idPartnerFirstPay',
        'PARTNERT_FIRSTPAY_GROUP' => 'aba_partners_list.idPartnerGroup_FirstPay', // AbaPartner.getPartnerByIdPartner()
        'PARTNERT_CURRENT' => 'user.idPartnerCurrent',
        'PARTNERT_CURRENT_GROUP' => 'aba_partners_list.idPartnerGroup_Current', // AbaPartner.getPartnerByIdPartner()
        'DEVICE_NAME'=> 'user_devices.deviceName',
        'SYSOPER'=> 'user_devices.sysOper',
        'APP_VERSION'=> 'user_devices.appVersion',
        'NUM_UNITS'=> '',
        'RENOV_DT'=> 'payments.dateToPay',
        'PRICE'=> 'payments.amountPrice',
        'CURRENCY '=> 'payments.currencyTrans',
        'PRODUCT'=> 'payments.idProduct',
        'CARD_CAD'=> '',
        'CARD_NUMBER'=> '',
        'NEW_PRODUCT' =>'payments.idPeriodPay',
        'OLD_PRODUCT' =>'payments.idPeriodPay',
        'URL' => 'boleto.url',
        'PID' => 'boleto.paySuppExtUniId',
        'REQUESTED_DATE' => 'boleto.requestDate',
        'DUE_DATE' => 'boleto.dueDate',
        'ISEXTEND' => 'payments.isExtend',
        'TEACHERID' => 'user.teacherId',

        'MONTH_PERIOD' => 'payments.monthPeriod',
        'PAY_DATE_START' => 'payments.dateStart',
        'PAY_DATE_END' => 'payments.dateEnd',
        'AMOUNT_PRICE' => 'payments.amountPrice',
        'PRICE_CURR' => '',
        'TRANSID' => 'payments.id',
    );

    const registroWeb = "registroWeb";
    const registroAgrupadores = "registroAgrupadores";
    const registroTestNivel = "registroTestNivel";
    const cambioPassword = "cambioPassword";
    const perfil = "perfil";
    const login = "login";
    const haTrabajadoCampus = "haTrabajadoCampus";
    const pagos = "pagos";
    const boletoRequest = "boletoRequest";
    const pagosRecurrentes = "pagosRecurrentes";
    const cancelaciones = "cancelaciones";
    const userObjectives = "userObjectives";
    const synchroUser = "synchroUser";
    const unsubscribe = "unsubscribe";
    const tryForfFee = "tryForFree";
    const registerDevice = "registerDevice";
    const infoUnitsCompleted = "infoUnitsCompleted";
    const infoNextPayment = "infoNextPayment";
    const inviteFriends = "inviteFriends";
    const invitationRegister = "invitationRegister";
    const startPremiumWeek = "startPremiumWeek";
    const changeProduct = "changeProduct";
    const registroAgrupadoresExtranet = "registroAgrupadoresExtranet";
    const freeToB2b = "freeToB2b";
    const registerTeacher = "registerTeacher";
    const checkIfUserExists =    "checkIfUserExists";
    const pagosPlan = "pagosPlan";

    const renewalsPlanNotification =        "renewalsPlanNotification";
    const renewalsPlanPayment =             "renewalsPlanPayment";
    const pagosPlanNotification =           "pagosPlanNotification";
    const renewalsPlanNotificationUpgrade = "renewalsPlanNotificationUpgrade";
    const recoverPassword =                 "recoverPassword";
    const partnerSource = "partnerSource";

    const errorIdUserNotExist = "ERR: DON'T EXIST IDUSER_WEB" ;

    protected static $aOperationsSelligent = array(
                        'registroWeb'=>
                            array('IDUSER_WEB'=>'',
                                    'MAIL'=>'',
                                    'NAME'=>'',
                                    'SURNAME'=>'',
                                    'COUNTRY_IP'=>'',
                                    'COUNTRY_USER'=>'',
                                    'PASS_TMP'=>'',
                                    'PASSWORD'=>'',
                                    'PREF_LANG'=>'',
                                    'ENTRY_DATE'=>'',
                                    'USER_TYPE'=>'',
                                    'EXPIRATION_DATE'=>'',
                                    'PRODUCT_TYPE'=>'',
                                    'PROMOCODE'=>'',
                                    'LEVEL_COURSE'=>'',
                                    'FLAG_CAMPUS_LOGIN'=>'',
                                    'FLAG_CAMPUS_WORK'=>'',
                                    'PREMIUM_HISTORIC'=>'',
                                    'PARTNERT_SOURCE'=>'',
                                    'PARTNERT_SOURCE_GROUP'=>'',
                                    'PARTNERT_FIRSTPAY'=>'',
                                    'PARTNERT_FIRSTPAY_GROUP'=>'',
                                    'PARTNERT_CURRENT'=>'',
                                    'PARTNERT_CURRENT_GROUP'=>'',
                                    'TEACHERID'=>'',),
                        'registroAgrupadores'=> array(
                                    'IDUSER_WEB'=>'',
                                    'MAIL'=>'',
                                    'NAME'=>'',
                                    'SURNAME'=>'',
                                    'COUNTRY_IP'=>'',
                                    'PASS_TMP'=>'',
                                    'PASSWORD'=>'',
                                    'TELEPHONE'=>'',
                                    'PREF_LANG'=>'',
                                    'ENTRY_DATE'=>'',
                                    'USER_TYPE'=>'',
                                    'EXPIRATION_DATE'=>'',
                                    'PRODUCT_TYPE'=>'',
                                    'PROMOCODE'=>'',
                                    'LEVEL_COURSE'=>'',
                                    'FLAG_CAMPUS_LOGIN'=>'',
                                    'FLAG_CAMPUS_WORK'=>'',
                                    'PREMIUM_HISTORIC'=>'',
                                    'PARTNERT_SOURCE'=>'',
                                    'PARTNERT_SOURCE_GROUP'=>'',
                                    'PARTNERT_FIRSTPAY'=>'',
                                    'PARTNERT_FIRSTPAY_GROUP'=>'',
                                    'PARTNERT_CURRENT'=>'',
                                    'PARTNERT_CURRENT_GROUP'=>'',
                                    'TEACHERID'=>'',

                                    'MONTH_PERIOD' =>   '',
                                    'PAY_DATE_START' => '',
                                    'PAY_DATE_END' =>   '',
                                    'AMOUNT_PRICE' =>   '',
                                    'CURRENCY' =>       '',
                                    'PRICE_CURR' =>     '',
                                    'TRANSID' =>       '',
                        ),
                        'registroTestNivel'=> array(
                                    'IDUSER_WEB'=>'',
                                    'LEVEL_COURSE'=>'',
                                    'PREF_LANG'=>'',
                                    'MAIL'=>'',
                                    'PASS_TMP'=>'',
                                    'PASSWORD'=>'',
                                    'COUNTRY_IP'=>'',
                                    'COUNTRY_USER'=>'',
                                    'PARTNERT_SOURCE'=>'',
                                    'PARTNERT_SOURCE_GROUP'=>'',
                                    'USER_TYPE'=>'',
                                    'TEACHERID'=>'',),
                        'cambioPassword'=> array(
                                    'IDUSER_WEB'=>'',
                                    'MAIL'=>'',
                                    'PASSWORD'=>'',
                                    'PASS_TMP'=>'',),
                        'perfil'=> array(
                                    'IDUSER_WEB'=>'',
                                    'NAME'=>'',
                                    'SURNAME'=>'',
                                    'BIRTHDAY_DT'=>'',
                                    'LEVEL_COURSE'=>'',
                                    'COUNTRY_USER'=>'',
                                    'TELEPHONE'=>'',
                                    'PREF_LANG' => '',),
                        'login'=> array(
                                    'IDUSER_WEB'=>'',
                                    'FLAG_CAMPUS_LOGIN'=>'',),
                        'haTrabajadoCampus'=> array(
                                    'IDUSER_WEB'=>'',
                                    'FLAG_CAMPUS_WORK'=>'',),
                        'pagos'=> array(
                                    'IDUSER_WEB'=>'',
                                    'USER_TYPE'=>'',
                                    'NAME'=>'',
                                    'SURNAME'=>'',
                                    'PRODUCT_TYPE'=>'',
                                    'PREMIUM_HISTORIC'=>'',
                                    'EXPIRATION_DATE'=>'',
                                    'PARTNERT_CURRENT'=>'',
                                    'PARTNERT_CURRENT_GROUP'=>'',
                                    'PARTNERT_FIRSTPAY'=>'',
                                    'PARTNERT_FIRSTPAY_GROUP'=>'',
                                    'PROMOCODE'=>'',
                                    'ISEXTEND'=>'',

                                    'MONTH_PERIOD' =>   '',
                                    'PAY_DATE_START' => '',
                                    'PAY_DATE_END' =>   '',
                                    'AMOUNT_PRICE' =>   '',
                                    'CURRENCY' =>       '',
                                    'PRICE_CURR' =>     '',
                                    'TRANSID' =>       '',
                                    ),
                        'pagosRecurrentes'=> array(
                                    'IDUSER_WEB'=>'',
                                    'USER_TYPE'=>'',
                                    'NAME'=>'',
                                    'SURNAME'=>'',
                                    'PRODUCT_TYPE'=>'',
                                    'PREMIUM_HISTORIC'=>'',
                                    'EXPIRATION_DATE'=>'',
                                    'PARTNERT_CURRENT'=>'',
                                    'PARTNERT_CURRENT_GROUP'=>'',
                                    'PARTNERT_FIRSTPAY'=>'',
                                    'PARTNERT_FIRSTPAY_GROUP'=>'',
                                    'PROMOCODE'=>'',),
                        'cancelaciones'=> array(
                                    'IDUSER_WEB'=>'',
                                    'USER_TYPE'=>'',
                                    'EXPIRATION_DATE'=>'',
                                    'CANCEL_REASON'=>'',),
                        'userObjectives'=> array(
                                    'IDUSER_WEB'=>'',
                                    'WHY_STUDY'=>'',
                                    'OBJETIVES_LEVEL'=>'',
                                    'PREVIOUS_EXPERIENCE'=>'',
                                    'OCCUPATION'=>'',
                                    'BIRTHDAY_DT'=>'',
                                    'SEX'=>'',
                                    'LEVEL_COURSE'=>''),
                        'synchroUser'=> array(
                                'IDUSER_WEB'=>'',
                                'PREF_LANG'=>'',
                                'MAIL'=>'',
                                'NAME'=>'',
                                'SURNAME'=>'',
                                'BIRTHDAY_DT'=>'',
                                'SEX'=>'',
                                'COUNTRY_USER'=>'',
                                'WHY_STUDY'=>'',
                                'PREVIOUS_EXPERIENCE'=>'',
                                'OCCUPATION'=>'',
                                'PRODUCT_TYPE'=>'',
                                'PASSWORD'=>'',
                                'TELEPHONE'=>'',
                                'USER_TYPE'=>'',
                                'EXPIRATION_DATE'=>'',
                                'CANCEL_REASON'=>'',
                                'PROMOCODE'=>'',
                                'LEVEL_COURSE'=>'',
                                'PREMIUM_HISTORIC'=>'',
                                'PARTNERT_SOURCE'=>'',
                                'PARTNERT_SOURCE_GROUP'=>'',
                                'PARTNERT_FIRSTPAY'=>'',
                                'PARTNERT_FIRSTPAY_GROUP'=>'',
                                'PARTNERT_CURRENT'=>'',
                                'PARTNERT_CURRENT_GROUP'=>'',
                                'TEACHERID'=>'',
                                'COUNTRY_IP'=>'',
                                ),
                        'unsubscribe'=> array(
                                'IDUSER_WEB'=>'',
                                'MAIL'=>''),
                        'tryForFree'=> array(
                                'IDUSER_WEB'=>''),
                        'registerDevice'=>array(
                                'IDUSER_WEB'=>'',
                                'MAIL' => '',
                                'DEVICE_NAME'=>'',
                                'SYSOPER'=>'',
                                'APP_VERSION'=>''
                                 ),
                        'infoUnitsCompleted'=> array(
                                'IDUSER_WEB'=>'',
                                'NUM_UNITS'=>''),
                        'infoNextPayment'=> array(
                                'IDUSER_WEB'=>'',
                                'RENOV_DT'=>'',
                                'PRICE'=>'',
                                'CURRENCY'=>'',
                                'PRODUCT'=>'',
                                'CARD_CAD'=>'',
                                'CARD_NUMBER'=>'',),
                        'inviteFriends' => array(
                                'IDUSER_WEB' => '',
                                'EMAIL1' => '', 'EMAIL2' => '', 'EMAIL3' => '', 'EMAIL4' => '', 'EMAIL5' => '',
                                'EMAIL6' => '', 'EMAIL7' => '', 'EMAIL8' => '', 'EMAIL9' => '', 'EMAIL10' => '',
                                'EMAIL11' => '', 'EMAIL12' => '', 'EMAIL13' => '', 'EMAIL14' => '', 'EMAIL15' => '',
                                'EMAIL16' => '', 'EMAIL17' => '', 'EMAIL18' => '', 'EMAIL19' => '', 'EMAIL20' => ''
                            ),
                        'invitationRegister' => array(
                                'IDUSER_WEB' => '',
                                'MAIL' => '',
                                'NAME' => ''
                            ),
                        'startPremiumWeek' => array(
                                'IDUSER_WEB' => '',
                                'FECHA_FIN' => ''
                            ),
                        'changeProduct' => array(
                                'IDUSER_WEB'=>'',
                                'NEW_PRODUCT' =>'',
                                'OLD_PRODUCT' =>'',
                                ),
                        'boletoRequest'=> array(
                            'IDUSER_WEB'=>'',
                            'USER_TYPE'=>'',
                            'NAME'=>'',
                            'SURNAME'=>'',
                            'PRODUCT_TYPE'=>'',
                            'EXPIRATION_DATE'=>'',
                            'PARTNERT_CURRENT'=>'',
                            'PARTNERT_CURRENT_GROUP'=>'',
                            'PARTNERT_FIRSTPAY'=>'',
                            'PARTNERT_FIRSTPAY_GROUP'=>'',
                            'PROMOCODE'=>'',
                            'URL' => '',
                            'PID' => '',
                            'REQUESTED_DATE' => '',
                            'DUE_DATE' => ''),
                        'registroAgrupadoresExtranet'=> array(
                            'IDUSER_WEB'=>'',
                            'MAIL'=>'',
                            'NAME'=>'',
                            'SURNAME'=>'',
                            'COUNTRY_IP'=>'',
                            'PASS_TMP'=>'',
                            'PASSWORD'=>'',
                            'TELEPHONE'=>'',
                            'PREF_LANG'=>'',
                            'ENTRY_DATE'=>'',
                            'USER_TYPE'=>'',
                            'EXPIRATION_DATE'=>'',
                            'PRODUCT_TYPE'=>'',
                            'PROMOCODE'=>'',
                            'LEVEL_COURSE'=>'',
                            'FLAG_CAMPUS_LOGIN'=>'',
                            'FLAG_CAMPUS_WORK'=>'',
                            'PREMIUM_HISTORIC'=>'',
                            'PARTNERT_SOURCE'=>'',
                            'PARTNERT_SOURCE_GROUP'=>'',
                            'PARTNERT_FIRSTPAY'=>'',
                            'PARTNERT_FIRSTPAY_GROUP'=>'',
                            'PARTNERT_CURRENT'=>'',
                            'PARTNERT_CURRENT_GROUP'=>'',
                            'TEACHERID'=>'',

                            'MONTH_PERIOD' =>   '',
                            'PAY_DATE_START' => '',
                            'PAY_DATE_END' =>   '',
                            'AMOUNT_PRICE' =>   '',
                            'CURRENCY' =>       '',
                            'PRICE_CURR' =>     '',
                            'TRANSID' =>       '',
                        ),
                        'registerTeacher'=> array(
                            'TEACHERID' =>  '',
                            'SEX' =>        '',
                            'NAME' =>       '',
                            'SURNAME' =>    '',
                            'MAIL' =>       '',
                            'PREF_LANG' =>  '',
                        ),
                        'checkIfUserExists'=> array(
                            'IDUSER_WEB' =>  '',
                        ),
                        'freeToB2b'=> array(
                            'IDUSER_WEB'=>'',
                            'MAIL'=>'',
                            'NAME'=>'',
                            'SURNAME'=>'',
                            'COUNTRY_IP'=>'',
                            'PASS_TMP'=>'',
                            'PASSWORD'=>'',
                            'TELEPHONE'=>'',
                            'PREF_LANG'=>'',
                            'ENTRY_DATE'=>'',
                            'USER_TYPE'=>'',
                            'EXPIRATION_DATE'=>'',
                            'PRODUCT_TYPE'=>'',
                            'PROMOCODE'=>'',
                            'LEVEL_COURSE'=>'',
                            'FLAG_CAMPUS_LOGIN'=>'',
                            'FLAG_CAMPUS_WORK'=>'',
                            'PREMIUM_HISTORIC'=>'',
                            'PARTNERT_SOURCE'=>'',
                            'PARTNERT_SOURCE_GROUP'=>'',
                            'PARTNERT_FIRSTPAY'=>'',
                            'PARTNERT_FIRSTPAY_GROUP'=>'',
                            'PARTNERT_CURRENT'=>'',
                            'PARTNERT_CURRENT_GROUP'=>'',

                            'MONTH_PERIOD' =>   '',
                            'PAY_DATE_START' => '',
                            'PAY_DATE_END' =>   '',
                            'AMOUNT_PRICE' =>   '',
                            'CURRENCY' =>       '',
                            'PRICE_CURR' =>     '',
                            'TRANSID' =>       '',
                        ),
                        'renewalsPlanNotification'=> array(
                            'IDUSER_WEB' =>     '',
                            'PREF_LANG' =>      '',
                            'COUNTRY_IP' =>     '',
                            'PRICE' =>          '',
                            'CURRENCY' =>       '',
                            'PRODUCT' =>        '',
                            'RENOV_DT' =>       '',
                        ),
                        'renewalsPlanPayment'=> array(
                            'IDUSER_WEB' =>     '',
                            'PREF_LANG' =>      '',
                            'COUNTRY_IP' =>     '',
                            'PRICE' =>          '',
                            'CURRENCY' =>       '',
                            'PRODUCT' =>        '',
                            'RENOV_DT' =>       '',
                        ),
                        'pagosPlanNotification'=> array(
                            'IDUSER_WEB' =>     '',
                            'COUNTRY_IP' =>     '',
                        ),
                        'pagosPlan'=> array(
                            'IDUSER_WEB' =>     '',
                            'PRODUCT_TYPE' =>   '',
                        ),
                        'recoverPassword'=> array(
                            'IDUSER_WEB'=>'',
                        ),
                        'partnerSource'=> array(
                            'ID_PARTNER'=>'',
                            'PARTNER_NAME'=>'',
                            'ID_PARTNER_GROUP'=>'',
                            'NAME_GROUP'=>'',
                            'ID_CATEGORY'=>'',
                            'ID_BUSINESS_AREA'=>'',
                            'ID_TYPE'=>'',
                            'ID_GROUP'=>'',
                            'ID_CHANNEL'=>'',
                            'ID_COUNTRY'=>'',
                        ),
                    );
    protected static $aOperSimulateEmail = array(
        'registroWeb'=>true,
        'registroAgrupadores'=> true,
        'registroTestNivel'=> true,
        'perfil' => true,
        'cambioPassword'=> true,
        'login'=> false,
        'haTrabajadoCampus'=> false,
        'pagos'=> false,
        'pagosRecurrentes'=> true,
        'cancelaciones'=> false,
        'userObjectives'=> false,
        'synchroUser'=> false,
        'unsubscribe'=> false,
        'tryForFree'=> true,
        'registerDevice'=>true,
        'infoUnitsCompleted'=>true,
        'infoNextPayment'=>true,
        'inviteFriends'=>true,
        'invitationRegister'=>false,
        'startPremiumWeek'=>true,
        'changeProduct' => false,
        'registroAgrupadoresExtranet'=> true,
        'boletoRequest' => true,
        'freeToB2b' => true,
        'registerTeacher' => true,
        'checkIfUserExists' => true,
        'renewalsPlanNotification' => true,
        'renewalsPlanPayment' => true,
        'pagosPlanNotification' => true,
        'pagosPlan' => true,
        'recoverPassword' => true,
        'partnerSource' => true,
    );

    protected $aOperationsUrls ;

    public function __construct()
    {
        $this->logSentSell = new LogSentSelligent();

        if (Yii::app()->params["MOCK_SELLIGENT"])
        {
            $this->aOperationsUrls = array(
                'registroWeb' =>                    "/partnersimulators/simulateselligent/oper/registroWeb",
                'registroAgrupadores' =>            "/partnersimulators/simulateselligent/oper/registroAgrupadores",
                'registroTestNivel' =>              "/partnersimulators/simulateselligent/oper/registroTestNivel",
                'cambioPassword' =>                 "/partnersimulators/simulateselligent/oper/cambioPassword",
                'perfil' =>                         "/partnersimulators/simulateselligent/oper/perfil",
                'login' =>                          "/partnersimulators/simulateselligent/oper/login",
                'haTrabajadoCampus' =>              "/partnersimulators/simulateselligent/oper/haTrabajadoCampus",
                'pagos' =>                          "/partnersimulators/simulateselligent/oper/pagos",
                'pagosRecurrentes' =>               "/partnersimulators/simulateselligent/oper/pagosRecurrentes",
                'cancelaciones' =>                  "/partnersimulators/simulateselligent/oper/cancelaciones",
                'userObjectives' =>                 "/partnersimulators/simulateselligent/oper/userObjectives",
                'synchroUser' =>                    "/partnersimulators/simulateselligent/oper/synchroUser",
                'unsubscribe' =>                    "/partnersimulators/simulateselligent/oper/unsubscribe",
                'tryForFree' =>                     "/partnersimulators/simulateselligent/oper/tryForFree",
                'registerDevice' =>                 "/partnersimulators/simulateselligent/oper/registerDevice",
                'infoUnitsCompleted' =>             "/partnersimulators/simulateselligent/oper/infoUnitsCompleted",
                'infoNextPayment' =>                "/partnersimulators/simulateselligent/oper/infoNextPayment",
                'inviteFriends' =>                  "/partnersimulators/simulateselligent/oper/inviteFriends",
                'invitationRegister' =>             "/partnersimulators/simulateselligent/oper/invitationRegister",
                'startPremiumWeek' =>               "/partnersimulators/simulateselligent/oper/startPremiumWeek",
                'changeProduct' =>                  "/partnersimulators/simulateselligent/oper/changeProduct",
                'registroAgrupadoresExtranet' =>    "/partnersimulators/simulateselligent/oper/registroAgrupadoresExtranet",
                'boletoRequest' =>                  "/partnersimulators/simulateselligent/oper/boletoRequest",
                'freeToB2b' =>                      "/partnersimulators/simulateselligent/oper/registroAgrupadores",
                'registerTeacher' =>                "/partnersimulators/simulateselligent/oper/registerTeacher",
                'checkIfUserExists' =>              "/partnersimulators/simulateselligent/oper/checkIfUserExists",
                'renewalsPlanNotification' =>       "/partnersimulators/simulateselligent/oper/renewalsPlanNotification",
                'renewalsPlanPayment' =>            "/partnersimulators/simulateselligent/oper/renewalsPlanPayment",
                'pagosPlanNotification' =>          "/partnersimulators/simulateselligent/oper/pagosPlanNotification",
                'pagosPlan' =>                      "/partnersimulators/simulateselligent/oper/pagosPlan",
                'recoverPassword' =>                "/partnersimulators/simulateselligent/oper/recoverPassword",
                'partnerSource' =>                  "/partnersimulators/simulateselligent/oper/partnerSource",
            );
        } else {
            $this->aOperationsUrls = array(
                'registroWeb' =>                    Yii::app()->config->get("OP_URL_SELLIGENT_REGISTROWEB"),
                'registroAgrupadores' =>            Yii::app()->config->get("OP_URL_SELLIGENT_REGISTROAGRUPADORES"),
                'registroTestNivel' =>              Yii::app()->config->get("OP_URL_SELLIGENT_REGISTROTESTNIVEL"),
                'cambioPassword' =>                 Yii::app()->config->get("OP_URL_SELLIGENT_CAMBIOPASSWORD"),
                'perfil' =>                         Yii::app()->config->get("OP_URL_SELLIGENT_PERFIL"),
                'login' =>                          Yii::app()->config->get("OP_URL_SELLIGENT_LOGIN"),
                'haTrabajadoCampus' =>              Yii::app()->config->get("OP_URL_SELLIGENT_TRABAJADOCAMPUS"),
                'pagos' =>                          Yii::app()->config->get("OP_URL_SELLIGENT_PAGOS"),
                'pagosRecurrentes' =>               Yii::app()->config->get("OP_URL_SELLIGENT_PAGOSRECURRENTES"),
                'cancelaciones' =>                  Yii::app()->config->get("OP_URL_SELLIGENT_CANCELACIONES"),
                'userObjectives' =>                 Yii::app()->config->get("OP_URL_SELLIGENT_PRIMERLOGIN"),
                'synchroUser' =>                    Yii::app()->config->get("OP_URL_SELLIGENT_SINCROUSER"),
                'unsubscribe' =>                    Yii::app()->config->get("OP_URL_SELLIGENT_UNSUBSCRIBE"),
                'tryForFree' =>                     Yii::app()->config->get("OP_URL_SELLIGENT_TRYFORFREE"),
                'registerDevice' =>                 Yii::app()->config->get("OP_URL_SELLIGENT_REGISTERDEVICE"),
                'infoUnitsCompleted' =>             Yii::app()->config->get("OP_URL_SELLIGENT_INFOUNITSCOMPLETED"),
                'infoNextPayment' =>                Yii::app()->config->get("OP_URL_SELLIGENT_INFONEXTPAY"),
                'changeProduct' =>                  Yii::app()->config->get("OP_URL_SELLIGENT_CHANGEPRODUCT"),
                'inviteFriends' =>                  Yii::app()->config->get("OP_URL_SELLIGENT_INVITEFRIENDS"),
                'invitationRegister' =>             Yii::app()->config->get("OP_URL_SELLIGENT_INVITATIONREGISTER"),
                'startPremiumWeek' =>               Yii::app()->config->get("OP_URL_SELLIGENT_STARTPREMIUMWEEK"),
                'registroAgrupadoresExtranet' =>    Yii::app()->config->get("OP_URL_SELLIGENT_EXTRANET_REGISTROAGRUPADORES"),
                'boletoRequest' =>                  Yii::app()->config->get("OP_URL_SELLIGENT_BOLETO"),
                'freeToB2b' =>                      Yii::app()->config->get("OP_URL_SELLIGENT_REGISTROAGRUPADORES"),
                'registerTeacher' =>                Yii::app()->config->get("OP_URL_SELLIGENT_TEACHERS"),
                'checkIfUserExists' =>              Yii::app()->config->get("OP_URL_SELLIGENT_CHECKUSEREXISTS"),
                'renewalsPlanNotification' =>       Yii::app()->config->get("OP_URL_SELLIGENT_RENEWALSPLANNOTIFICATION"),
                'renewalsPlanPayment' =>            Yii::app()->config->get("OP_URL_SELLIGENT_RENEWALSPLANPAYMENT"),
                'pagosPlanNotification' =>          Yii::app()->config->get("OP_URL_SELLIGENT_RENEWALSPLANNOTIFICATIONUPGRADE"),
                'pagosPlan' =>                      Yii::app()->config->get("OP_URL_SELLIGENT_PAGOSPLAN"),
                'recoverPassword' =>                Yii::app()->config->get("OP_URL_SELLIGENT_RECOVERPASSWORD"),
                'partnerSource' =>                  Yii::app()->config->get("OP_URL_SELLIGENT_PARTNERSOURCE"),
            );
        }
    }

    public function getErrMessage()
    {
        return $this->errMessage;
    }

    public function sendOperationToSelligent($nameOperation, AbaUser $user, $extraFields=array(), $from="" )
    {
        // We refresh all values from database, but $extraFields will overwrite any values coming from database:
        $user->getUserById( $user->getId() );

        // We only send information to Selligent for FREE and/or PREMIUM users. Ignore LEADS, TEACHERS and PLUS
        if($nameOperation == self::registerTeacher) {
            if (intval($user->userType) !== TEACHER ) {
                return true;
            }
        }
        else {
            if (intval($user->userType) !== PREMIUM &&
                intval($user->userType) !== FREE &&
                intval($user->userType) !== DELETED
            ) {
                return true;
            }
        }

        $this->nameOperation=$nameOperation;
        if ($this->buildData($user, $extraFields)) {

            $url = Yii::app()->config->get("URL_SELLIGENT") . $this->aOperationsUrls[$this->nameOperation];
            if (!$this->sendData($url, $user)) {
                $this->errMessage = " The data for the web service call for Selligent could not be sent to $url. " .
                    "Operation = " . $this->nameOperation . " , detailed error: " . $this->errMessage;

                if(Yii::app()->config->get("ENABLE_LOG_SELLIGENT") == 1) {
                    $this->logSentSell->insertLog($url, $nameOperation, serialize($this->aFieldsToBeSent), 'false: ' .
                        $this->response, $user->getId(), $from, $this->errMessage);
                }

                $user->updateDateLastSentSelligent(null);
                return false;
            }

            $user->updateDateLastSentSelligent(HeDate::todaySQL(true));
        } else {

            $this->errMessage = " The data for the web service call for Selligent could not be resolved. Operation = " .
                $this->nameOperation . " , detailed error: " . $this->errMessage;
            HeLogger::sendLog(" WARNING=Error when building data to Selligent from user " . $user->email,
                HeLogger::IT_BUGS_PAYMENTS, HeLogger::CRITICAL,
                " For user=" . $user->email . " in the operation $nameOperation (from action $from) the" .
                "  data build syncronization with Selligent Failed for some reason: " . $this->errMessage);

            if(Yii::app()->config->get("ENABLE_LOG_SELLIGENT") == 1) {
                $this->logSentSell->insertLog('', $nameOperation, serialize($this->aFieldsToBeSent), 'false',
                    $user->getId(), $from, $this->errMessage);
            }
            return false;
        }

        // Save in log_sent_selligent all request
        if(Yii::app()->config->get("ENABLE_LOG_SELLIGENT") == 1) {
            $this->logSentSell->insertLog( $url, $nameOperation, serialize($this->aFieldsToBeSent),"OK, ".$this->response,
                $user->getId(), $from, "");
        }
        return true;
    }


    private function calculateField( AbaUser $user, $fieldSelligent, $staticValue, $extraFields=array() )
    {
        if ($staticValue !== "") {
            return $staticValue;
        } elseif (is_array($extraFields) && array_key_exists($fieldSelligent, $extraFields)) {
            $finalValue = $extraFields[$fieldSelligent];
        } else {
            if (!array_key_exists($fieldSelligent, self::$aFieldsSelligent)) {
                $this->errMessage = " Field  = " . $fieldSelligent . " does not appear in Selligent Fields list.";
                return false;
            }

            $aFieldCampus = explode(".", self::$aFieldsSelligent[$fieldSelligent]);
            if(count($aFieldCampus)<1)
            {
                $this->errMessage = " The format of field correspondence ".$fieldSelligent.
                                        " is incorrect. Check self::aFieldsSelligent list please.";
                return false;
            }

            $finalValue = null;
            $tableCampus = $aFieldCampus[0];
            $fieldCampus = $aFieldCampus[1];
            switch( $tableCampus )
            {
                // user, user_objective, payments, aba_partners_list, followup4
                case "user":
                    if(isset($user->$fieldCampus) )
                    {
                        $finalValue= $user->$fieldCampus;
                        if($fieldCampus=='firstLoginDone'){
                            $finalValue= (!$finalValue)? self::$no : self::$yes;
                        }
                        // All DATES fields are within the user TABLE. The are named with Date suffix.
                        if( strpos($fieldCampus, "Date")>0 && HeDate::validDate($finalValue, false, "-") ){
                            $finalValue = HeDate::europeanToSQL($finalValue,false,"-");
                        }
                    }
                    break;
                case "user_objective":
                    $finalValue = "";
                    $moUserObjective = new AbaUserObjective();
                    if ($moUserObjective->getObjectivesByUserId($user->getId())) {
                        if (isset($moUserObjective->$fieldCampus)) {
                            $finalValue = $moUserObjective->$fieldCampus;
                        }
                    }
                    break;
                case "payments":
                case "aba_partners_list":
                case "followup4":
                case "user_devices":
                    switch( $fieldCampus )
                    {
                        case 'deviceName':
                            $finalValue = $this->getUserDevice_DeviceName( $user );
                            break;
                        case 'sysOper':
                            $finalValue = $this->getUserDevice_SysOper( $user );
                            break;
                        case 'idPeriodPay':
                            $finalValue = $this->getPayments_IdPeriodPay( $user );
                            break;
                        case 'getHasStartedCourse':
                            $finalValue = $this->getFollowUp_HasWorked( $user );
                            break;
                        case 'LastSuccessPayment':
                            $finalValue = $this->getPayments_LastSuccessPayment($user);
                            break;
                        case 'idPartnerGroup_Source':
                            $finalValue = $this->getPartnerGoupName($user->idPartnerSource);
                            break;
                        case 'idPartnerGroup_FirstPay':
                            $finalValue = $this->getPartnerGoupName($user->idPartnerFirstPay);
                            break;
                        case 'idPartnerGroup_Current':
                            $finalValue = $this->getPartnerGoupName($user->idPartnerCurrent);
                            break;
                        default:
                            $this->errMessage = " Value ".$fieldCampus." not implemented.";
                            return false;
                            break;
                    }
                    break;
                default:
                    if( array_key_exists( $fieldSelligent, $extraFields) )
                    {
                        $finalValue =  $extraFields[$fieldSelligent];
                    }
                    break;
            }
        }

        return $finalValue;
    }

    /**
     * @param AbaUser $user
     * @param array   $extraFields
     *
     * @return bool
     */
    private function buildData( AbaUser $user, $extraFields=array() )
    {
        if( !array_key_exists($this->nameOperation, self::$aOperationsSelligent))
        {
            $this->errMessage=" There is no web service in Selligent called operation = ".$this->nameOperation;
            return false;
        }

        // We find out the fields for this web service and fill them with the proper values:
        $this->aFieldsToBeSent = array();
        foreach( self::$aOperationsSelligent[$this->nameOperation] as $fieldSelligent=>$staticValue )
        {

            $finalValue = $this->calculateField($user, $fieldSelligent, $staticValue, $extraFields);
            if( $finalValue===false )
            {
                $this->errMessage = " Calculating value for selligent field($fieldSelligent=".$finalValue.") failed: ".$this->errMessage;
                return false;
            }

            if($finalValue!=="" && (!empty($finalValue) ||  $finalValue===0))
            {
                $this->aFieldsToBeSent[$fieldSelligent] = $finalValue;
            }
        }


        return true;
    }


    private function sendData( $url, AbaUser $user)
    {
        if( Yii::app()->config->get("ENABLE_SELLIGENT")=='1' )
        {
            $response = HeComm::sendHTTPPostCurl($url, null, $this->aFieldsToBeSent);
            if( is_array($response) )
            {
                $this->errMessage = $this->errMessage." // Sending POST fields to Selligent failed: CURL error details: ".$response["description"];
                $this->response = $this->errMessage;
                return false;
            }
        }
        else{
            $this->response = "OK,bcoz disabled by config";
            return true;
        }

        // It connects and we have a RESPONSE, but we have to analyse the content of the response: 3 cases:
        if( trim(strip_tags($response))=='KO')
        {
            $this->errMessage = $this->errMessage." Sending POST fields to Selligent was successful but SELLIGENT returned K.O.";
            $this->response = $response;
            return false;
        }
        // The string could be = ERR: Don't exist ID_USER_WEB: 300000
        elseif( is_string($response) && substr(trim(strip_tags($response)),0,3)=='ERR'  )
        {
            // Exceptional case, in case of a FREE user it does not exist in SELLIGENT, then we force its automatic creation through registerUser operation by ourselves.
            if(($this->nameOperation==self::login || $this->nameOperation==self::cambioPassword) && strpos( strtoupper(trim($response)), self::errorIdUserNotExist)>0 )
            {
                $aPriorityFields = array();
                if( array_key_exists("PASS_TMP",$this->aFieldsToBeSent) )
                {
                    $aPriorityFields = array("PASS_TMP" => $this->aFieldsToBeSent["PASS_TMP"] );
                }
                $ret = $this->sendOperationToSelligent(self::registroWeb, $user, $aPriorityFields, "sendData in Selligent, auto-register" );
                if( $ret ){
                    return true;
                }
            }

            $this->errMessage = $this->errMessage." Sending POST fields to Selligent was successful but some fields were missing or went wrong, details by SELLIGENT= ".$response;
            $this->response = $response;
            return false;
        }
        elseif( substr(trim(strip_tags($response)),0,2)!=='OK' && substr(trim($response),0,2)!=='OK')
        {
            $this->errMessage = $this->errMessage." Sending POST fields to Selligent was successful but SELLIGENT server response was a new error, details by SELLIGENT= ".$response;
            $this->response = $response;
            return false;
        }

        $this->response = $response;
        return true;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * payments.idPeriodPay
     * @param AbaUser $user
     *
     * @return string
     */
    private function getPayments_IdPeriodPay( AbaUser $user )
    {
        $moPayment = new Payment();
        if( !$moPayment->getLastPaymentByUserId( $user->getId()) )
        {
            return '0';
        }

        return strval($moPayment->idPeriodPay);
    }

    //payments.getLastSuccessPayment
    /**
     * Returns if the user has ever paid at least once, meaning he's been at least once a PREMIUM user.
     * @param AbaUser $user
     *
     * @return bool
     */
    private function getPayments_LastSuccessPayment(AbaUser $user)
    {
        if( $user->idPartnerFirstPay !==0 && !is_null($user->idPartnerFirstPay) && $user->idPartnerFirstPay!=='' )
        {
            return self::$yes;
        }
        return self::$no;
    }

    /**
     * Based on an Id Partner return the partner group name
     * @param $idPartner
     *
     * @return string
     */
    private function getPartnerGoupName($idPartner)
    {
        $moPartner = new Partners();
        if( !$moPartner->getPartnerByIdPartner($idPartner) )
        {
            return "NO PARTNER INFORMED";
        }

        return $moPartner->nameGroup;
    }

    /** Returns if user has ever worked in Campus
     *
     * @param AbaUser $moUser
     * @return bool|int
     */
    private function getFollowUp_HasWorked(AbaUser $moUser)
    {
        $moFollowUp = new AbaFollowup();

        return $moFollowUp->getHasStartedCourse($moUser->id );
    }

    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return array
     */
    public static function getAOperSimulateEmail()
    {
        return self::$aOperSimulateEmail;
    }

    /**
     * @param AbaUser $moUser
     *
     * @return string
     */
    private function getUserDevice_DeviceName($moUser)
    {
        $moUserDevice = new AbaUserDevice();
        if( $moUserDevice->getDeviceByUserId($moUser->id) )
        {
            return $moUserDevice->deviceName;
        }

        return "";
    }

    /**
     * @param AbaUser $moUser
     *
     * @return string
     */
    private function getUserDevice_SysOper($moUser)
    {
        $moUserDevice = new AbaUserDevice();
        if( $moUserDevice->getDeviceByUserId($moUser->id) )
        {
            return $moUserDevice->sysOper;
        }

        return "";
    }

}
