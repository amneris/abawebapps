<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 21/11/12
 * Time: 15:57
  */
class ProductsPricesCommon
{
    private $errorMessage;

    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param AbaUser $user
     *
     * @return int
     */
    public function getCountryIdByUser(AbaUser $user) {

        $countryId = 0;

        if( is_null($user) || $user->countryId == "" || $user->countryId == '0' ) {
            $oCountry = new AbaCountry();
            if($oCountry->getCountryByIsoCode( Yii::app()->config->get("DefaultIPCountryCode") )) {
                $countryId = $oCountry->id;
            }
            else {
                $countryId = Yii::app()->config->get("ABACountryId");
            }
        }
        else {
            $countryId = $user->countryId;
        }

        return $countryId;
    }

    /**
     * Collects all the products the user can purchase
     * with the right prices and payments subscriptions periods
     * @param AbaUser $user
     * @param string $userPromoCode
     *
     * @return array|null|ProductPrice[]
     */
    public function getListProductsByUser(AbaUser $user, $userPromoCode = "")
    {

        $countryId = $this->getCountryIdByUser($user);

        if (isset($userPromoCode) && $userPromoCode !== "") {
            if (!$this->checkExistsPromocode($userPromoCode)) {
                $userPromoCode = null;
            }
        }

        //#5739
        $stProdsPrices = $this->getAllAvailableExperimentsVariationsPrices($user);

        $moProdsPrices =    new ProductsPrices();
        $aProds =           $moProdsPrices->getAllProductsByCountryId( $countryId, $userPromoCode, true, $stProdsPrices );

        if(empty($aProds)) {
            return null;
        }

        return $this->retUniqueProducts($aProds);
    }

    /** Returns all products available for a PREMIUM user, which are different
     * than the ones for FREE.
     * @TODO: We must do some refactor about Products + Country + Extranet + Groupon + FREE + PREMIUM
     *
     * @param AbaUser $user
     * @param string $userPromoCode
     * @return bool|null|ProductsPrices[]
     */
    public function getListProductsForPremium( AbaUser $user, $userPromoCode="", $bPlanPrice=false)
    {
        $countryId = $user->countryId;
        if(isset($userPromoCode) && $userPromoCode !== "") {
            if( !$this->checkExistsPromocode($userPromoCode) ) {
                $userPromoCode = NULL;
            }
        }

        $moProdsPrices = new ProductsPrices();
        $aProds = $moProdsPrices->getAllProductsByCountryId( $countryId, $userPromoCode, false );
        if(empty($aProds)) {
            return NULL;
        }

        $aProds = $this->retUniqueProducts($aProds);
        foreach ($aProds as $key => $productPrice) {

            if ((
                $productPrice->idPeriodPay != ProdPeriodicity::MONTH_3 &&
                $productPrice->idPeriodPay != ProdPeriodicity::MONTH_6 &&
                $productPrice->idPeriodPay != ProdPeriodicity::MONTH_12)
                OR ($productPrice->isPlanPrice == 1 AND $productPrice->visibleWeb == 0)
            ) {
                $aKeys2Delete[] = $key;
            }
        }

        //
        //#5087
        if(!$bPlanPrice) {
            foreach ($aProds as $key => $productPrice){
                if ($productPrice->isPlanPrice == 1) {
                    $aKeys2Delete[] = $key;
                }
            }
        }
        //

        foreach ($aKeys2Delete as $key) {
            if(isset($aProds[$key])) {
                unset($aProds[$key]);
            }
        }

        return $aProds;
    }

    /**
     * @param integer $countryId
     * @param string $language
     * @param null   $promoCode
     *
     * @return bool|ProductsPrices[]
     */
    public function getListProductsByCountry( $countryId, $language="en", $promoCode=NULL, $stProdsPrices=array())
    {
        $moProductsPrices = new ProductsPrices();
        /*
        Name of the product
        Currency Code
        Original price
        Final Price\
        Discount percentage
        Periodicity price
         */

        if(isset($promoCode))
        {
            if( !$this->checkExistsPromocode($promoCode) )
            {
                $promoCode = NULL;
            }
        }


        $aProducts = array();
        /* @var ProductPrice[] $aMoProducts */
        $aMoProducts = $moProductsPrices->getAllProductsByCountryId($countryId, $promoCode, true, $stProdsPrices);
        if( !$aMoProducts )
        {
            $this->errorMessage=" There is no products available for this country ".$countryId;
            return false;
        }

        if( $countryId!==Yii::app()->config->get("ABACountryId") )
        {
            $aMoProducts = $this->retUniqueProducts( $aMoProducts );
        }

        return $aMoProducts;
    }

    /**
     * @param ProductPrice[] $aProds
     *
     * @return boolean|ProductsPrices[]
     */
    public function retUniqueProducts($aProds)
    {
        if(is_null($aProds) || !$aProds)
        {
            return false;
        }

        // Combination of userType + idPeriodPay + Idcountry
        // to avoid repetition of same products
        /**
         * @var ProductPrice $productPrice
         * @var ProductPrice $prevProductPrice
         */
        $aKeys2Delete = array();
        $aPrevProducts = array();
        foreach ($aProds as $key=>$productPrice)
        {
            $auxKey = $productPrice->userType.$productPrice->idPeriodPay;
            if(  array_key_exists($auxKey,$aPrevProducts) )
            {
                /* $aPrevProducts[$auxKey][1] = idCountry, we remove the default country because we want the specific one. */
                if( $aPrevProducts[$auxKey][1] == Yii::app()->config->get("ABACountryId") )
                {
                    $aKeys2Delete[] = $aPrevProducts[$auxKey][0];
                }
                elseif($productPrice->idCountry == Yii::app()->config->get("ABACountryId") )
                {
                    $aKeys2Delete[] = $key;
                }
            }
            $aPrevProducts[$auxKey] = array($key, $productPrice->idCountry);
        }


        foreach ($aKeys2Delete as $key)
        {
            unset($aProds[$key]);
        }

        return $aProds;
    }


    /**
     * Returns true if the promocode is availble and enable.
     * @param $promoCode
     *
     * @return bool
     */
    public function checkExistsPromocode( $promoCode, $idProduct=NULL, $enable=1, $visibleWeb=1, $dateValid=true )
    {
        // @todo We might link promocodes to be available only for a certain products. So we should check in case $idProduct is passed by.

        // The user will have a promoCode associated for some scenarios, because from some places he will have the option to introducie one
        // this is one of the cases
        if( isset($promoCode) && $promoCode!=='')
        {
            // Promocode has to be validated otherwise it will raise several errors in queries.
            $moPromo = new ProductPromo();
            if( $moPromo->getProdPromoById($promoCode, $enable, $visibleWeb, $dateValid) )
            {
                return true;
            }
            else{
                return false;
            }
        }

        return false;
    }


    public function checkExistsIdProduct( $idProduct, $enabled=0 )
    {
        if($idProduct=="")
        {
            return false;
        }

        $moProduct = new ProductPrice();
        if( !$moProduct->getProductById( $idProduct, $enabled ) )
        {
            return false;
        }

        return true;

    }

    /** Returns true if the idProduct exists and also if the country of this idproduct
     * matches user's country.
     * @param     $idProduct
     * @param int $enabled
     *
     * @return bool
     */
    public function checkIdProductAndUserCountryId($idProduct, $enabled=0, $checkUser=true)
    {
        if($idProduct=="")
        {
            return false;
        }

        $moProduct = new ProductPrice();
        if( !$moProduct->getProductById( $idProduct, $enabled ) )
        {
            return false;
        }

        if($checkUser) {
            if( intval($moProduct->idCountry) !== intval(Yii::app()->user->getCountryId()) &&
              intval($moProduct->idCountry) !== intval(Yii::app()->config->get("ABACountryId")) &&
              intval($moProduct->idCountry) !== 300 )
            {
                return false;
            }
        }

        return true;
    }

    /**
     * @param AbaUser $moUser
     *
     * @return array|bool
     */
    public function getProductsTiersByUserCurrency( AbaUser $moUser)
    {
        $productsPricesAppstore = new ProductsPricesAppstore();

        $aProds = $productsPricesAppstore->getProductsTiersByUserCurrency($moUser);

        return $aProds;
    }

    /**
     * @param AbaUser $moUser
     *
     * @return array|bool
     */
    public function getProductsTiersByUserCountry( AbaUser $moUser)
    {
        $productsPricesAppstore = new ProductsPricesAppstore();

        $aProds = $productsPricesAppstore->getProductsTiersByUserCountry($moUser);

        if(count($aProds) == 0) {
            $aProds = $productsPricesAppstore->getDefaultProductsTiersByUser($moUser);
        }
        return $aProds;
    }


    /**
     * @param $userId
     * @return string
     */
    public function getUserProductPeriodPay(AbaUser $moUser) {

        $oUser = new AbaUser();
        $oUser->getUserById($moUser->id);

        $idPeriodPay = '';

        if($oUser->userType == PREMIUM){

            $moLastPay = new Payment();

            if ($moLastPay->getLastPayPendingByUserId($moUser->id)){

                $productPrice =     new ProductPrice();
                $oProductPrice =    $productPrice->getProductById($moLastPay->idProduct);
                $idPeriodPay =      $oProductPrice->idPeriodPay;

//                $iMonthPeriod = HePayments::periodToMonth($idPeriodPay);
            }
        }

        return $idPeriodPay;
    }

    /**
     * @param $idProduct
     *
     * @return string
     */
    public function getProductPricePkById( $idProduct ) {

        if($idProduct == "") {
            return "";
        }

        $productPrice = new ProductPrice();

        if($productPrice->getProductById( $idProduct, true )) {
            return HePayments::getProductPricePK($productPrice->idProduct, $productPrice->idCountry, $productPrice->idPeriodPay);
        }
        return "";
    }

    /**
     * @param $idProduct
     *
     * @return bool|ProductPrice
     */
    public function getProductPriceById( $idProduct ) {
        $productPrice = new ProductPrice();
        return $productPrice->getProductById( $idProduct );
    }

    /**
     * @param $idProduct
     *
     * @return bool
     */
    public function isPlanProduct( $idProduct ) {
        $prodSelected = new ProductPrice();
        return $prodSelected->isPlanProduct($idProduct);
    }

    /**
     * @param AbaUser $moUser
     * @param string $idProduct
     *
     * @return float|int
     */
    public function getDiscountPlan( AbaUser $moUser, $idProduct="") {
        $oProductPrice = new ProductPrice();
        return $oProductPrice->getDiscountPlan($moUser, $idProduct);
    }

    /**
     * @param $iDiscount
     * @param $productPrice
     *
     * @return array
     */
    public function setPaymentDiscount( $iDiscount, $productPrice) {
        $oProductPrice = new ProductPrice();
        return $oProductPrice->setPaymentDiscount($iDiscount, $productPrice);
    }


    /**
     * @param $idCountry
     * @param $sLang
     * @param $idPromoCode
     * @param $idProduct
     *
     * @return array
     */
    public function getProductByCountry($idCountry, $sLang, $idPromoCode, $idProduct)
    {

        $stProduct = array();

        /* @var ProductPrice[] $aMoProducts */
        $aMoProducts = $this->getListProductsByCountry($idCountry, $sLang, $idPromoCode);
        foreach ($aMoProducts as $key => $productPrice) {
            // if ($productPrice->isPlanPrice != 1) {
            if ($productPrice->idProduct == $idProduct) {
                $discountPercentage = round($productPrice->discount);
                if ($productPrice->type !== PERCENTAGE) {
                    $discountPercentage = round(($productPrice->getAmountDiscounted() * 100) / $productPrice->priceOfficialCry,
                      2);
                }
                $displayCur = $productPrice->getCcyDisplaySymbol();
                if ($displayCur == 'US $') {
                    $displayCur = '$';
                }
                $stProduct = array(
                  "productId" => $productPrice->idProduct,
                  "productName" => $productPrice->getPackageTitleKey(),
                  "currencyCode" => $displayCur,
                  "originalPrice" => $productPrice->getOriginalPrice(),
                  "finalPrice" => $productPrice->getFinalPrice(),
                  "discountPercentage" => $discountPercentage,
                  "monthlyPrice" => $productPrice->getPriceMonthly(),
                  "periodMonths" => $productPrice->periodInMonths,
                  "ccyDisplaySymbol" => $productPrice->getCcyDisplaySymbol(),
                  "ccyIsoCode" => $productPrice->officialIdCurrency,
                );
            }
        }

        return $stProduct;
    }


    /**
     * @param $products
     *
     * @return array
     */
    public function collectAndSelectProducts($products)
    {
        $userIdProduct = null;
        if (!is_null(Yii::app()->user->getState("idProduct")) && Yii::app()->user->getState("idProduct") !== '') {
            // $commProducts = new ProductsPricesCommon();
            if ($this->checkIdProductAndUserCountryId(Yii::app()->user->getState("idProduct"), 1)) {
                $userIdProduct = Yii::app()->user->getState("idProduct");
            }
        }

        //If any of the products available has a discount, we mark the flag for the view to know:
        $anyDiscount = false;//Apart from being used as a flag, it also has the value discounted to be displayed in view
        $typeDiscount = false;//PERCENTAGE, FINAL PRICE OR AMOUNT
        $validationTypePromo = false;
        foreach ($products as $aProd) {
            if ($aProd['amountDiscounted'] > 0.00) {
                $anyDiscount = $aProd['discountTitle'];
                $typeDiscount = $aProd['typePromo'];
                $validationTypePromo = $aProd['validationTypePromo'];
            }
        }
        // Promocode sent by user but not valid, we have to display information about why not:
        if (!$anyDiscount && !is_null(Yii::app()->user->getState("promoCode")) && Yii::app()->user->getState("promoCode") !== '') {
            $userPromoCode = Yii::app()->user->getState("promoCode");
            $moPromo = new ProductPromo();
            if ($moPromo->getProdPromoById($userPromoCode, 1, 1, false)) {
                $validationTypePromo = $aProd['validationTypePromo'] = $moPromo->dateEnd;
            } else {
                $validationTypePromo = Yii::t('mainApp', 'Promotion not valid');
            }
        }
        // Select the product for the Front-End, default it's annual:
        $posSessionIdProduct = 0;
        foreach ($products as $aProd) {
            if ($userIdProduct !== '' && !empty($userIdProduct)) {
                if (trim(strtolower($userIdProduct)) == trim(strtolower($aProd["idProduct"]))) {
                    break;
                }
            } elseif (intval(trim($aProd["monthsPeriod"])) == 12) {
                break;
            }
            $posSessionIdProduct++;
        }

        $packageSelected = "";
        if (isset($products[$posSessionIdProduct]["productPricePK"])) {
            $packageSelected = $products[$posSessionIdProduct]["productPricePK"];
        }
        return array($products, $packageSelected, $anyDiscount, $typeDiscount, $validationTypePromo);
    }

    /**
     * @param AbaUser $user
     * @param string $userPromoCode
     * @param bool|false $bPlanPrice
     * @param string $packageSelected
     * @param bool|false $bPaymentPlus
     *
     * @return array
     */
    public function getFormattedListProducts(
      AbaUser $user,
      $userPromoCode = "",
      $bPlanPrice = false,
      $packageSelected = "",
      $bPaymentPlus = false
    ) {

        if($bPaymentPlus) {
            $aProdsPrices = $this->getListProductsForPremium($user, $userPromoCode, $bPlanPrice);
        }
        else {
            $aProdsPrices = $this->getListProductsByUser($user, $userPromoCode);
        }

        if (!$aProdsPrices) {
            $aPackagesProps[] = array(
              "idProduct" => "",
              "productPricePK" => "",
              "packageTitleKey" => "If you wish to purchase a superior product please contact our support team. See the contact details in the footer of the web site.",
              "discountTitle" => "100%",
              "validationTypePromo" => "",
              "typePromo" => '',
              "originalPrice" => "0.00",
              "amountDiscounted" => "0.00",
              "finalPrice" => "0.00",
              "ccyDisplaySymbol" => " ",
              "finalPriceTransaction" => "0.0000",
              "ccyTransactionSymbol" => " ",
              "monthlyPriceOriginal" => 0.00,
              "monthlyPrice" => 0.00,
              "monthsPeriod" => 1,
              "ccyIsoCode" => "",
              "promocodeTranslationKey" => "",
              "isPlanPrice" => false,
              "visibleWeb" => true,
            );
        } else {
            $aPackagesProps = array();
            foreach ($aProdsPrices as $productPrice) {
                $moPeriod = new ProdPeriodicity();
                $moPeriod->getProdPeriodById($productPrice->idPeriodPay);
                $aPackagesProps[] = array(
                  "idProduct" => $productPrice->idProduct,
                  "productPricePK" => HePayments::getProductPricePK($productPrice->idProduct, $productPrice->idCountry,
                    $productPrice->idPeriodPay),
                  "packageTitleKey" => $productPrice->getPackageTitleKey(),
                  "discountTitle" => $productPrice->getDiscountTitle(),
                  "validationTypePromo" => $productPrice->getTxtShowValidationTypePromo(),
                  "typePromo" => '',
                  "originalPrice" => $productPrice->getOriginalPrice(),
                  "amountDiscounted" => $productPrice->getAmountDiscounted(),
                  "finalPrice" => $productPrice->getFinalPrice(),
                  "ccyDisplaySymbol" => $productPrice->getCcyDisplaySymbol(),
                  "finalPriceTransaction" => $productPrice->getFinalPriceTransaction(),
                  "ccyTransactionSymbol" => $productPrice->getCcyTransactionSymbol(),
                  "monthlyPriceOriginal" => $productPrice->getPriceMonthlyOriginalTrans(),
                  "monthlyPrice" => $productPrice->getPriceMonthly(),
                  "monthsPeriod" => $moPeriod->months,
                  "ccyIsoCode" => $productPrice->officialIdCurrency,
                  "promocodeTranslationKey" => "",
                  "isPlanPrice" => $productPrice->isPlanPrice,
                  "visibleWeb" => $productPrice->visibleWeb,
                );

                // We have 5 possible scenarios for displaying status for promocodes. Check ENH-1076 for more info:
                if ($userPromoCode !== '' && $productPrice->getDiscountTitle() !== '') {
                    $moPromo = new ProductPromo();
                    if ($moPromo->getProdPromoById($userPromoCode, 1, 1, false)) {
                        $aPackagesProps[count($aPackagesProps) - 1]["typePromo"] = $moPromo->type;
                        //
                        //#5342
                        if (isset($moPromo->idDescription) AND is_numeric($moPromo->idDescription)) {
                            $oProductsPromosDescription = new ProductsPromosDescription();
                            if ($oProductsPromosDescription->getDescriptionById($moPromo->idDescription)) {
                                $aPackagesProps[count($aPackagesProps) - 1]["promocodeTranslationKey"] = $oProductsPromosDescription->translationKey;
                            }
                        }
                        //
                    } else {
                        $aPackagesProps[count($aPackagesProps) - 1]["typePromo"] = '';
                    }
                }
            }
        }

        //
        //#5087
        if ($bPlanPrice AND $bPaymentPlus) {
            foreach ($aPackagesProps as $key => $productPrice) {

                if ($packageSelected == $productPrice["productPricePK"]) {

                    $iDiscount = $this->getDiscountPlan($user, $productPrice["idProduct"]);
                    $productPriceSelected = $this->setPaymentDiscount($iDiscount, $productPrice);

                    $aPackagesProps[$key]['amountDiscounted'] = $productPriceSelected["amountDiscounted"];
                    $aPackagesProps[$key]['finalPrice'] = $productPriceSelected["finalPrice"];
                }
            }
        }

        return $aPackagesProps;
    }

    /**
     * @param $countryId
     */
    public function getCountryAndCurrencyData($countryId) {
        $oAbaCounties = new AbaCountries();
        return $oAbaCounties->getCountryAndCurrencyData($countryId);
    }

    /**
     * Specific scenarios payments function
     *
     * @param $experimentVariationId
     * @param $countryId
     *
     * @return array
     */
    public function getAllAvailableUserExperimentsVariationsPrices($experimentVariationId, $countryId)
    {

        $stUserProdsPrices = array();

        $iEnabledExperiments = Yii::app()->config->get("ENABLED_EXPERIMENTS");

        if ($iEnabledExperiments == 1 AND is_numeric($experimentVariationId) AND is_numeric($countryId)) {
            $oAbaUserExperimentsVariations = new ExperimentsVariationsAttributes();
            $stUserProdsPrices = $oAbaUserExperimentsVariations->getUserExperimentsVariationsByCountryId($experimentVariationId,
              $countryId);
        }

        return $stUserProdsPrices;
    }
    /**
     * Specific scenarios payments function
     *
     * @param AbaUser $oUser
     *
     * @return array
     */
    public function getAllAvailableExperimentsVariationsPrices(AbaUser $oUser)
    {

        $stUserProdsPrices = array();

        $iEnabledExperiments = Yii::app()->config->get("ENABLED_EXPERIMENTS");

        if ($iEnabledExperiments == 1 AND $oUser->userType == FREE) {
            $oAbaUserExperimentsVariations = new AbaUserExperimentsVariations();
            $stUserProdsPrices = $oAbaUserExperimentsVariations->getAllUserExperimentsVariationsForScenariosByUserId($oUser->id,
              $oUser->countryId, ExperimentsTypes::EXPERIMENT_TYPE_SCENARIOS_PAYMENTS);

            $stUserProdsPrices = self::filterExperimentsAvailablesUsers($stUserProdsPrices, $oUser);
            $stUserProdsPrices = self::filterExperimentsAvailablesProfiles($stUserProdsPrices, $oUser);
        }

        return $stUserProdsPrices;
    }

    /**
     * @param $stAllProfiles
     * @param $oAbaUser
     *
     * @return array
     */
    protected static function filterExperimentsAvailablesProfiles($stAllProfiles, $oAbaUser)
    {
        $stProdsPrices = array();

        foreach ($stAllProfiles as $iKey => $stProdPrice) {
            if ($stProdPrice["availableExperiment"] == 1) {
                switch ($stProdPrice["availableModeId"]) {
                    case ExperimentsAvailabilityModes::EXPERIMENT_AVAILABLE_MODE_WHILE_EXPERIMENT:
                    case ExperimentsAvailabilityModes::EXPERIMENT_AVAILABLE_MODE_ALWAYS:
                        $stProdsPrices[] = $stProdPrice;
                        break;
                }
            } else {
                switch ($stProdPrice["availableModeId"]) {
                    case ExperimentsAvailabilityModes::EXPERIMENT_AVAILABLE_MODE_ALWAYS:
                    case ExperimentsAvailabilityModes::EXPERIMENT_AVAILABLE_MODE_AFTER:
                        $stProdsPrices[] = $stProdPrice;
                        break;
                }
            }
        }

        return $stProdsPrices;
    }

    /**
     * @param $stAllProfiles
     * @param $oAbaUser
     *
     * @return array
     */
    protected static function filterExperimentsAvailablesUsers($stAllProdsPrices, $oAbaUser)
    {

        $stProdsPrices = array();

        if(isset($oAbaUser->userType)) {

            // filter availables user profiles
            foreach ($stAllProdsPrices as $iKey => $stProdPrice) {

                if(HeMixed::filterCountry($oAbaUser->langEnv, $stProdPrice["languages"])) {
                    switch($oAbaUser->userType) {
                        case FREE :
                            switch ($stProdPrice["userCategoryId"]) {
                                case CmAbaExperimentsUserCategories::EXPERIMENT_CATEGORY_FREE_PREMIUM:
                                case CmAbaExperimentsUserCategories::EXPERIMENT_CATEGORY_FREE:
                                    $stProdsPrices[$iKey] = $stProdPrice;
                                    break;
                            }
                            break;
                        case PREMIUM :
                            switch ($stProdPrice["userCategoryId"]) {
                                case CmAbaExperimentsUserCategories::EXPERIMENT_CATEGORY_FREE_PREMIUM:
                                case CmAbaExperimentsUserCategories::EXPERIMENT_CATEGORY_PREMIUM:
                                    $stProdsPrices[$iKey] = $stProdPrice;
                                    break;
                            }
                            break;
                    }
                }
            }
        }

        return $stProdsPrices;
    }


    /**
     * @param $scenario
     * @param $countryId
     * @param $experimentTypeId
     * @param string $language
     *
     * @return bool
     */
    public function getRandomExperimentVariationId($scenario, $countryId, $experimentTypeId, $language='en') {

        $oAbaProductsGroup = new ExperimentsVariations();
        if (!is_numeric($scenario) OR $scenario == 0) {
            $scenario = $oAbaProductsGroup->getRandomExperimentVariationId($countryId, $experimentTypeId, $language);
        }
        return $scenario;
    }

    /**
     * @param $stAdyenHppPaymentMethods
     * @param array $stParams
     *
     * @return mixed
     */
    public function addAdyenHppPrices($stAdyenHppPaymentMethods, $oAbaUser, $stParams = array())
    {
        foreach ($stAdyenHppPaymentMethods as $sKey => $adyenPaymentMethod) {
            $stAdyenHppPaymentMethods[$sKey]["prices"] = self::getAdyenHppPrice($adyenPaymentMethod["brandCode"],
              (isset($stParams["idProduct"]) ? $stParams["idProduct"] : ''), $oAbaUser);
        }
        return $stAdyenHppPaymentMethods;
    }

//        Array ( [paymentAmount] => 30 [currencyCode] => CAD [skinCode] => Jvizcgco [countryCode] => CA [sessionValidity] => [idPayMethod] => 6 [idProduct] => 38_30 )

    /**
     * @param array $stParams
     *
     * @return array|bool
     */
    public function getAdyenHppCurrenciesByProduct($stParams = array())
    {
        $stAdyenHppPaymentMethods = array();

        if(isset($stParams["idProduct"])) {

            $oProductsPricesAdyenHpp = new ProductsPricesAdyenHpp();

            $stAdyenHppPaymentMethods = $oProductsPricesAdyenHpp->getAdyenHppCurrenciesByProduct($stParams["idProduct"]);
        }
        return $stAdyenHppPaymentMethods;
    }

    /**
     * #abawebapps-158
     *
     * @param $brandCode
     * @param $idProduct
     *
     * @return array|null
     */
    public function getAdyenHppPrice($brandCode, $idProduct, $oAbaUser)
    {

        $stPrices = null;

        $oProductsPricesAdyenHpp = new ProductsPricesAdyenHpp();

        $stAdyenHppPrice = $oProductsPricesAdyenHpp->getAdyenHppProductPrice($brandCode, $idProduct);

        if ($stAdyenHppPrice) {

            $idPromoCode = "";
            if (!is_null(Yii::app()->user->getState("promoCode")) && Yii::app()->user->getState("promoCode") !== '') {

                $idPromoCode = Yii::app()->user->getState("promoCode");

                $oPromoCode = new ProductPromo($idPromoCode);

                if(trim($oPromoCode->idPromoCode) <> "") {

                    $isPlanPrice = 0;
                    if(isset($stAdyenHppPrice["isPlanPrice"])) {
                        $isPlanPrice = $stAdyenHppPrice["isPlanPrice"];
                    }
                    //
                    //#4.101.1
                    $idPromoCode = ProductPrice::adjustPromoCode($idPromoCode, $isPlanPrice, $oPromoCode->idPromoType);
                }
            }

            $stIdProduct = HePayments::parseProductId($stAdyenHppPrice["idProduct"]);

            $prod = new ProductPrice();
            if ($prod->getProductByMixedId($stAdyenHppPrice["idProduct"], $stIdProduct[0], $stIdProduct[1],
              $idPromoCode, $stIdProduct[0], $stAdyenHppPrice["priceAdyenHpp"], $stAdyenHppPrice["currencyAdyenHpp"])
            ) {
                $stAdyenHppPrice["finalPriceAdyenHpp"] = $prod->finalPrice;


                if(!$oAbaUser OR !isset($oAbaUser->id) OR !is_numeric($oAbaUser->id)) {
                    $oAbaUser = new AbaUser();
                    $oAbaUser->getUserById(Yii::app()->user->getId());
                }

                $iDiscount = $this->getDiscountPlan($oAbaUser, $idProduct);

                $productPrice = array(
                    "finalPrice" => $prod->getFinalPrice(),
                    "amountDiscounted" => $prod->getAmountDiscounted(),
                    "originalPrice" => $prod->getOriginalPrice(),
                );

                $productPriceSelected = $this->setPaymentDiscount($iDiscount, $productPrice);

                $stAdyenHppPrice["finalPriceAdyenHpp"] = $productPriceSelected["finalPrice"];
            }

            $stPrices = array(
              "currency" => $stAdyenHppPrice["currencyAdyenHpp"],
              "price" => $stAdyenHppPrice["priceAdyenHpp"],
              "finalPrice" => $stAdyenHppPrice["finalPriceAdyenHpp"],
              "symbol" => $stAdyenHppPrice["symbol"],
            );
        }

        return $stPrices;
    }

}
