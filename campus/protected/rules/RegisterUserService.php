<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 13/11/12
 * Time: 15:45
 * To change this template use File | Settings | File Templates.
 */
class RegisterUserService implements IAbaService
{
    private $errorCode ;
    private $errorMessage ;
    static private $redirectUrl = "site/login";

    /**
     * Only creation of a new user into our database
     *
     * @param $email
     * @param $pwd
     * @param $langEnv
     * @param integer $idCountry
     * @param string $name
     * @param string $surnames
     * @param integer $currentLevel
     * @param integer $idPartner
     * @param integer $idSourceList Optional
     * @param string $deviceTypeSource
     *
     * @return AbaUser|bool
     */
    public function registerUser( $email, $pwd, $langEnv, $idCountry, $name="", $surnames="", $currentLevel=NULL,
      $idPartner=NULL, $idSourceList=NULL, $deviceTypeSource=NULL,
      $registerSource=REGISTER_USER, $scenario=null
    )
    {
        if( !array_key_exists( $langEnv, Yii::app()->params['languages'])  )
        {
            $langEnv = HeMixed::getAutoLangDecision();
        }

        $commUserRegistration = new RegisterUsersCommon();
        /* @var AbaUser $user */
        $user = $commUserRegistration->registerNewStandardUser($email, $pwd, $langEnv, $name, $surnames, $currentLevel,
                    $idPartner, $idCountry, $registerSource, $idSourceList, $deviceTypeSource, $scenario );
        if(!$user)
        {
           $this->errorCode = "506" ;
           $this->errorMessage = $commUserRegistration->getErrorMessage() ;
           return false;
        }

        $moTeacher = new AbaTeacher(  );
        $moTeacher->getTeacherById($user->teacherId);
        // Sends an internal message within the Campus, using Messages of Yii. A welcome message basically.
        $genero = $moTeacher->attributes['gender'];
        HeAbaMail::sendMessageCampus( $user->teacherId, $user->getId(),$user->email,
            Yii::t('mainApp', 'subjectwelcome_key', array(), null, $langEnv),
            Yii::t('mainApp','welcomeemailcampus_key'.$genero,
                array("{user.name}"=>$user->name,
                      "{tag_br}"=>"<br/>",
                      "{teacher.name}"=>$moTeacher->name ),null,$langEnv ) );

        //  Send data to Selligent
        $commConnSelligent = new SelligentConnectorCommon();
        $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::registroWeb,
                                                    $user,
                                                    array("PROMOCODE"=>"NOSENSE", "PASS_TMP"=>$pwd) ,
                                                    "registerUser web service" );

        // check if we have a member get member registration
        if(($idPartner == PARTNER_MGM_EMAIL || $idPartner == PARTNER_MGM_FACEBOOK) && $idSourceList > 0)
        {
            $mgm = new AbaMemberGetMember();
            $mgm->addRegister($idSourceList);

            // send invitation register to selligent
            $sourceUser = new AbaUser();
            $sourceUser->getUserById($idSourceList);
            $params = array('MAIL' => $email, 'NAME' => $name);
            $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::invitationRegister, $sourceUser, $params, "Invitation register");
        }


        return $user;
    }

    /**
     * Can create or update an user
     *
     * @param         $email
     * @param         $langEnv
     * @param         $currentLevel
     * @param         $insideCampus
     * @param integer $idCountry
     * @param integer $idPartner     *
     * @param integer $idSourceList
     * @param string  $deviceTypeSource
     *
     * @return AbaUser|bool
     */
    public function registerUserFromLevelTest( $email, $langEnv, $currentLevel, $insideCampus, $idCountry,
                                                               $idPartner, $idSourceList=NULL, $deviceTypeSource=NULL)
    {
        if( $email=="" )
        {
            $this->errorCode = "507";
            $this->errorMessage = " Email not valid.";
            return false;
        }

        // In case the user exists, we only update his current English level
        $user = new AbaUser();
        if( $user->getUserByEmail($email) )
        {
            // We mark the user as an existing record, it's a magic property assignment.
            $user->isNewUser = 0;
             // We only update the current level if empty or if user is LEAD
             if( empty($user->currentLevel) )
             {
                 $oldLevel = null;
                 $user->currentLevel = $currentLevel;
                 $user->registerLevel = $currentLevel;
                 if( !$user->update(array("currentLevel")) )
                 {
                     $this->errorCode = "511";
                     $this->errorMessage = " Failing updating the user level on registering user from level test.";
                     return false;
                 }
                 // We log the first level in logLevelChange
                 $commUser = new UserCommon();
                 $commUser->changeUserLevel( $user, $oldLevel, $user->currentLevel, 'Register user level test');

             }

            if( $user->userType==DELETED )
            {
                $user->currentLevel = $currentLevel;
                $user->userType = FREE;
                $user->langEnv = $langEnv;
                $user->langCourse = $langEnv;
                $user->countryId = $idCountry;
                $user->countryIdCustom = $idCountry;
                $teacher = new AbaTeacher();
                $teacher->getTeacherDefaultByLang( $langEnv );
                $user->teacherId = $teacher->id;
                $user->expirationDate = HeDate::todaySQL( true );
                $user->entryDate = HeDate::todaySQL( true );
                $user->registerUserSource = REGISTER_USER_FROM_LEVELTEST;
                $user->idPartnerSource = $idPartner;
//                $user->idPartnerCurrent = $idPartner;
                if( !$user->updateFromRegistration() )
                {
                    $this->errorCode = "511";
                    $this->errorMessage = " Failing updating the user level and converting to FREE type on registering user from level test.";
                    return false;
                }
                // We log the first level in logLevelChange
                $commUser = new UserCommon();
                $commUser->changeUserLevel( $user, $user->currentLevel, $user->currentLevel, 'Register DELETED to FREE user');
            }

             $user->password = "";
             return $user;

        }
        // Otherwise, we create the user through the standard registration process, a common function
        else
        {
            $commUserRegistration = new RegisterUsersCommon();
            $pwd = $commUserRegistration->genRandomPasswordNewUser($email,"LT");
            /* @var AbaUser $user */
            $user = $commUserRegistration->registerNewStandardUser($email, $pwd, $langEnv, "", "", $currentLevel,
                    $idPartner, $idCountry, REGISTER_USER_FROM_LEVELTEST, $idSourceList, $deviceTypeSource );
            if(!$user)
            {
                $this->errorCode = "506";
                $this->errorMessage = $commUserRegistration->getErrorMessage();
                return false;
            }
            else
            {
                // We mark the user as new record, it's a magic property.
                $user->isNewUser = 1;
                // Envia un mensaje a través de Yii, sólo dentro del Campus:
                $moTeacher = new AbaTeacher(  );
                $moTeacher->getTeacherById($user->teacherId);
                // Envia un mensaje a través de Yii, sólo dentro del Campus:
                $fSendMessage = true;
                $genero = $moTeacher->attributes['gender'];
                $moPartner = new Partners();
                if($moPartner->getPartnerById($user->idPartnerSource)){
                    if( intval($moPartner->idPartnerGroup)==PARTNER_B2B_G ){
                        $fSendMessage = false;
                    }
                }

                if( $fSendMessage ){
                    HeAbaMail::sendMessageCampus($user->teacherId, $user->getId(), $user->email,
                        Yii::t('mainApp','subjectwelcome_key', array(), null, $langEnv),
                        Yii::t('mainApp', 'welcomeemailcampus_key'.$genero,
                            array("{user.name}"=>$user->name,
                                  "{tag_br}" => "<br/>",
                                  "{teacher.name}" => $moTeacher->name), null, $langEnv));
                }
                //  Send data to Selligent
                $commConnSelligent = new SelligentConnectorCommon();
                $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::registroTestNivel, $user, array("PASS_TMP"=>$pwd), "registerUserFromLevelTest web service" );

                // We copy the password WITHOUT encryption because "BE_Republic" wants the password.
                $user->password = $pwd;
                return $user;
            }
        }

        return false;

    }

    /**
     * @param $email
     * @param $pwd
     * @param $langEnv
     * @param $name
     * @param $surnames
     * @param $telephone
     * @param $voucherCode
     * @param $securityCode
     * @param $partnerId
     * @param $periodId
     * @param $idCountry
     * @param string $deviceTypeSource
     *
     * @return AbaUser|bool
     */
    public function registerUserPartner( $email, $pwd, $langEnv, $name, $surnames, $telephone, $voucherCode,
                                         $securityCode, $partnerId, $periodId, $idCountry, $deviceTypeSource=NULL )
    {
        $this->errorMessage = "";

        if( $email == "" ) {
            $this->errorCode =      "507";
            $this->errorMessage =   "The user email is empty or is not valid.";
            return false;
        }

        // Validate User status and existence:
        $moUser =       new AbaUser();
        $userExists =   $moUser->getUserByEmail( $email );

        //
        // Validate user used coupons
        //#5087
        if($userExists) {
            //#6119
            // if(($partnerId == PARTNER_ID_2X1_MKT_ABAENGLISH) OR ($partnerId == PARTNER_ID_ABA_FAMILYPLAN AND $moUser->idPartnerCurrent == PARTNER_ID_ABA_FAMILYPLAN)) {
            if(($partnerId == PARTNER_ID_2X1_MKT_ABAENGLISH) OR $partnerId == PARTNER_ID_ABA_FAMILYPLAN) {
                switch($moUser->userType) {
                    case DELETED:
                    case PREMIUM:
                    case PLUS:
                    case TEACHER:
                        $this->errorCode =      "524";
                        $this->errorMessage =   "User already exists. Gift codes can only be activated by unregistered users.";
                        return false;
                        break;
                    default:
                        break;
                }
            }
        }

        //
        // Validate user used coupons
        if($userExists && $partnerId == PARTNER_ID_2X1_RENEWALS_FAMILY_PLAN) {
            switch($moUser->userType) {
                case DELETED:
                case PREMIUM:
                case PLUS:
                case TEACHER:
                    $this->errorCode =      "524";
                    $this->errorMessage =   "User already exists. Gift codes can only be activated by unregistered users.";
                    return false;
                    break;
                default:
                    break;
            }

            //
            $cmAbaRenewalsPlans = new CmAbaRenewalsPlans();

            if($cmAbaRenewalsPlans->getRenewalByUser($moUser->id, CmAbaRenewalsPlans::RENEWALS_STATUS_RENEWED)) {
                $this->errorCode =      "524";
                $this->errorMessage =   "User already exists. Gift codes can only be activated by unregistered users.";
                return false;
            }
        }


        //
        //#ABAWEBAPPS-557
        if(!$userExists) {
            if (HeMixed::checkEmailForFraud($email)) {
                $this->errorMessage = " Email not valid.";
                try {
                    HeLogger::sendLog(
                      HeLogger::PREFIX_ERR_UNKNOWN_H . ': Suspected fraud',
                      HeLogger::PAYMENTS,
                      HeLogger::CRITICAL,
                      " The user with the email " . $email . " could not register for suspected fraud"
                    );
                } catch (Exception $e) { }
                return false;
            }
        }
        //


        //
        $moPeriod = new ProdPeriodicity();
        if (!$moPeriod->getProdPeriodByMonths($periodId)) {
            $this->errorCode =      "508";
            $this->errorMessage =   "The period for this product is not valid. There is no such periodicity in the database with ID " . $periodId;
            return false;
        }

        //Validate Product and periodicity's:
        $moProduct = new ProductPrice();
        if( !$moProduct->getProductByCountryIdPeriodId( $idCountry, $moPeriod->id, PREMIUM) )
        {
            $this->errorCode =      "508";
            $this->errorMessage =   "The product is not valid. There is no product in the database for period with Id = ".$periodId;
            return false;
        }

        $commRegisterUsers = new RegisterUsersCommon();
        $tableGroupones = $commRegisterUsers->verifyCodesFromPartner( $moUser->id, $email, $partnerId, $voucherCode, $securityCode, $moPeriod);
        // Validate Codes of Groupon
        if( !$tableGroupones ) {
            $this->errorCode =      $commRegisterUsers->getCodeError();
            $this->errorMessage =   " The code of the partner or partner itself is not correct ".$voucherCode." , details:".$commRegisterUsers->getErrorMessage();
            return false;
        }

        // Update/create User in database:
        $moUser = $commRegisterUsers->registerUserPremiumFromPartner($moUser, $userExists, $email, $pwd, $langEnv, $name, $surnames,
                                $telephone, $voucherCode, $securityCode, $partnerId, $moProduct, $moPeriod, PREMIUM, $idCountry, $deviceTypeSource);
        if( !$moUser ) {
            $this->errorCode =      "506";
            $this->errorMessage =   " The user could not be registered. Failed on registerUserPremiumFromPartner(): ".$commRegisterUsers->getErrorMessage();
            return false;
        }

        // Update groupon table:
        if( !$commRegisterUsers->validateGrouponCodeUsed( $moUser->id, $tableGroupones, $voucherCode, $securityCode, $email, $partnerId) )
        {
            $this->errorCode =      "510";
            $this->errorMessage =   "User registered but the codes for partner could NOT be saved partner ".$partnerId." , code1= ".$voucherCode." , code2= ".$securityCode. " used by email ".$email;

            HeLogger::sendLog("Registering user from Partner ".$partnerId.", email ".$email." success but with one failure.",
                HeLogger::IT_BUGS_PAYMENTS, HeLogger::CRITICAL, $this->errorMessage);
        }

        $moPartner = new Partners();


        //
        //#5635
        $oPayment = new Payment();
        $oPayment->getLastSuccessPayment($moUser->id);

        $stPaymentInfo = $oPayment->getPaymentInfoForSelligent();

        //  Send data to Selligent
        $commConnSelligent = new SelligentConnectorCommon();
        $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::registroAgrupadores, $moUser,
            array(
                "PASS_TMP" =>       $pwd,
                "PROMOCODE" =>      $moPartner->getCampaignByCode1($tableGroupones, $voucherCode, $partnerId),

                "MONTH_PERIOD" =>   $stPaymentInfo['MONTH_PERIOD'],
                "PAY_DATE_START" => $stPaymentInfo['PAY_DATE_START'],
                "PAY_DATE_END" =>   $stPaymentInfo['PAY_DATE_END'],
                "AMOUNT_PRICE" =>   $stPaymentInfo['AMOUNT_PRICE'],
                "CURRENCY" =>       $stPaymentInfo['CURRENCY'],
                "PRICE_CURR" =>     $stPaymentInfo['PRICE_CURR'],
                "TRANSID" =>        $stPaymentInfo['TRANSID'],
            ), "registerUserPartner web service" );

        

        return $moUser;
    }


    /**
     * Returns a complete URL to access the Campus without login, means it will allow
     * the user enter straightforwardly to the user.
     *
     * @param AbaUser $user
     * @param string  $idProduct If present, it will appear in the url to access to payment page with product selected.
     * @param string  $promocode If present, it will appear in the URL and user will have a promocode in session.
     * @param string  $landingPage Could be HOME or PAYMENT only.
     * @param bool    $autoLogin If true, returns url that allows skipping the login page.
     * @param string  $idPartner
     *
     * @return string
     */
    public function getRedirectUrl(AbaUser $user, $idProduct = '', $promocode = '', $landingPage = 'HOME', $autoLogin = false, $idPartner = '', $idPay = '', $isDirectPayment = false, $extraParams = null)
    {
        $aAddExtraUrlParam = array('language' => $user->langEnv);

        if($autoLogin) $aAddExtraUrlParam[MAGIC_LOGIN_KEY] = $user->keyExternalLogin;
        if($idProduct!=='') $aAddExtraUrlParam['idproduct'] = $idProduct;
        if($promocode!=='') $aAddExtraUrlParam['promocode'] = $promocode;
        if($idPartner!=='') $aAddExtraUrlParam['idpartner'] = $idPartner;
        if($idPay!=='') $aAddExtraUrlParam['idPay'] = $idPay;
        if($isDirectPayment) $aAddExtraUrlParam['dPay'] = 1;
        // if($idCountryDirect!=='') $aAddExtraUrlParam['idcountrydirect'] = $idCountryDirect;

        if (!is_null($extraParams) && is_array($extraParams)) {
            foreach ($extraParams as $name => $value) {
                $aAddExtraUrlParam[$name] = $value;
            }
        }

        $aAddExtraUrlParam['1'] = 1;

        switch($landingPage) {
            case 'HOME_REGISTER':
                return Yii::app()->createAbsoluteUrl('site/indexregister', $aAddExtraUrlParam);
                break;

            case 'PAYMENT':
                return Yii::app()->createAbsoluteUrl('payments/payment', $aAddExtraUrlParam);
                break;

            case 'HOME':
                return Yii::app()->createAbsoluteUrl('site/index', $aAddExtraUrlParam);
                break;

            case 'PAYMENT_CONFIRMED':
                return Yii::app()->createAbsoluteUrl('/payments/confirmedPayment', $aAddExtraUrlParam);
                break;

            case 'PAYMENT_DIRECT':
                return Yii::app()->createAbsoluteUrl('/direct-payment', $aAddExtraUrlParam);
                break;

            case 'PAYMENT_DIRECT_METHOD':
                return Yii::app()->createAbsoluteUrl('/payments/paymentmethod', $aAddExtraUrlParam);
                break;

            case 'OBJECTIVES':
                $units  = new UnitsCourses();
                $moUnit = new UnitCourse($units->getStartUnitByLevel($user->currentLevel));
                $unitId = $moUnit->getFormatUnitId($moUnit->getUnitId());

                $aAddExtraUrlParam['unit'] = $unitId;
                return Yii::app()->createAbsoluteUrl('course/AllUnits', $aAddExtraUrlParam);

                break;

            default:
                return Yii::app()->createAbsoluteUrl(self::$redirectUrl, $aAddExtraUrlParam);
                break;
        }
    }

    public function getLastError()
    {
        return $this->errorMessage;
    }

    public function getErrorCode()
    {
        if(!isset($this->errorCode) || $this->errorCode == "") {
            return "503";
        }
        return $this->errorCode;
    }

    /**
     * @param $email
     * @param $pwd
     * @param $langEnv
     * @param $name
     * @param $surnames
     * @param $currentLevel
     * @param $teacherEmail
     * @param $codeDeal
     * @param $partnerId
     * @param $periodId
     * @param $idCountry
     * @param $device
     *
     * @return AbaUser|bool
     */
    public function registerUserPremiumB2bExtranet($email, $pwd, $langEnv, $name, $surnames, $currentLevel, $teacherEmail, $codeDeal, $partnerId, $periodId, $idCountry, $device)
    {
        $this->errorMessage = "";
        if ($email == "") {
            $this->errorCode = '507';
            $this->errorMessage = "The user email is empty or is not valid.";
            return false;
        }

        // Validate User status and existence:
        $moUser =       new AbaUser();
        $userExists =   $moUser->getUserByEmail($email);

        //Validate product
        $moPeriod = new ProdPeriodicity();
        if (!$moPeriod->getProdPeriodByMonths($periodId)) {
            $this->errorCode = '508';
            $this->errorMessage = "The period for this product is not valid. There is no such periodicity in the database with ID " . $periodId;
            return false;
        }

        //Validate Product and periodicity's:
        $moProduct = new ProductPrice();
        if (!$moProduct->getProductByCountryIdPeriodId($idCountry, $moPeriod->id, PREMIUM, true)) {
            $this->errorCode = '508';
            $this->errorMessage = "The product is not valid. There is no product in the database for period with Id = " . $periodId;
            return false;
        }

        //Validate discount
        $discountPercent = 0;
        if(is_numeric($codeDeal)) {
            $discountPercent = $codeDeal;
        }


        //
        // Check Teacher
        $moTeacher = new AbaTeacher();

        if(trim($teacherEmail) != "") {
            //
            if (!$moTeacher->getUserByEmail($teacherEmail)) {
                $this->errorCode =      '520';
                $this->errorMessage =   'Teacher email not found in Campus database.';
                return false;
            }
        }
        else {
            $moTeacher->getTeacherDefaultByLang($langEnv);
        }


        //
        $backUserData = array('oldTeacherEmail' => $moTeacher->email, 'oldIdPartnerCurrent' => $partnerId);
        if($userExists) {
            $moOldTeacher = new AbaTeacher();
            if($moOldTeacher->getTeacherById($moUser->teacherId)) {
                $backUserData = array('oldTeacherEmail' => $moOldTeacher->email, 'oldIdPartnerCurrent' => $moUser->idPartnerCurrent);
            }
        }

        //
        // Register user and payments
        $commRegisterUsers = new RegisterUsersCommon();

        $moUser = $commRegisterUsers->registerUserPremiumFromB2bExtranet(
            $userExists, $moUser, $email, $pwd, $langEnv, $name, $surnames, $currentLevel,
            $moTeacher->id, $partnerId, $idCountry, $device, $moPeriod, $moProduct, $discountPercent
        );

        if (!$moUser) {
            $this->errorCode = "506";
            $this->errorMessage = " The user could not be registered. Failed on registerUserPremiumB2bExtranet(): " . $commRegisterUsers->getErrorMessage();
            return false;
        }

        //
        //#5635
        $oPayment = new Payment();
        $oPayment->getLastSuccessPayment($moUser->id);

        $stPaymentInfo = $oPayment->getPaymentInfoForSelligent();


        //  Send data to Selligent ???
        $commConnSelligent = new SelligentConnectorCommon();
        $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::registroAgrupadoresExtranet, $moUser,
            array(
                "PASS_TMP" =>       $pwd,

                "MONTH_PERIOD" =>   $stPaymentInfo['MONTH_PERIOD'],
                "PAY_DATE_START" => $stPaymentInfo['PAY_DATE_START'],
                "PAY_DATE_END" =>   $stPaymentInfo['PAY_DATE_END'],
                "AMOUNT_PRICE" =>   $stPaymentInfo['AMOUNT_PRICE'],
                "CURRENCY" =>       $stPaymentInfo['CURRENCY'],
                "PRICE_CURR" =>     $stPaymentInfo['PRICE_CURR'],
                "TRANSID" =>        $stPaymentInfo['TRANSID'],
            ), "RegisterUserB2Bextranet web service" );

        $moUser->refresh();

//        return $moUser;
        return array('moUser' => $moUser, 'extraData' => $backUserData);
    }

    /**
     * @param $params
     *
     * @return AbaUser|bool
     */
    public function registerTeacher($params)
    {
        $this->errorMessage = "";
        if ($params['email'] == "") {
            $this->errorCode =      '507';
            $this->errorMessage =   "The user email is empty or is not valid.";
            return false;
        }

        // Validate User status and existence:
        $moUser =       new AbaUser();
        $userExists =   $moUser->getUserByEmail($params['email']);

        // Register user and payments
        $commRegisterUsers =    new RegisterUsersCommon();
        $moUser =               $commRegisterUsers->registerUserTeacher($userExists, $moUser, $params);

        if (!$moUser) {
            $this->errorCode =      "506";
            $this->errorMessage =   " The teacher could not be registered. Failed on registerTeacher(): " . $commRegisterUsers->getErrorMessage();
            return false;
        }

        //  Send data to Selligent
        $commConnSelligent = new SelligentConnectorCommon();
        $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::registerTeacher, $moUser, array(), "RegisterTeacher web service" );
        $moUser->refresh();

        return $moUser;
    }

    /**
     * @param $params
     *
     * @return AbaUser|bool
     */
    public function finaliceLicense($params)
    {
        $this->errorMessage = "";
        if ($params['email'] == "") {
            $this->errorCode =      '507';
            $this->errorMessage =   "The user email is empty or is not valid.";
            return false;
        }

        // Validate User status and existence:
        $moUser =       new AbaUser();
        $userExists =   $moUser->getUserByEmail($params['email']);

        if(!$userExists) {
            $this->codeError =      '506';
            $this->errorMessage =   'This user does not exists in the database.';
            return false;
        }

        // Register user and payments
        $commRegisterUsers =    new RegisterUsersCommon();
        $moUser =               $commRegisterUsers->changeLicense($moUser, $params);

        //
        if (!$moUser) {
            $this->errorCode =      "506";
            $this->errorMessage =   " User License could not be changed. Failed on finaliceLicense(): " . $commRegisterUsers->getErrorMessage();
            return false;
        }


        //  Send data to Selligent
        $commConnSelligent = new SelligentConnectorCommon();
        $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::synchroUser, $moUser, array(), "finaliceLicense web service" );
        $moUser->refresh();

        return $moUser;
    }

    /**
     * @param $sAction
     * @param string $sEmail
     * @param string $sName
     * @param string $sSurname
     * @param string $sGender
     * @param string $langEnv
     * @param string $idCountry
     * @param int $idPartner
     * @param int $idSourceList
     * @param string $device
     * @param int $currentLevel
     *
     * @return AbaUser|bool
     */
    public function registerUserFacebook($sEmail='', $sName='', $sSurname='', $iFbid=null, $sGender='',
         $langEnv='', $idCountry='', $idPartner=null, $idSourceList=null, $device='', $currentLevel=NULL, $scenario=null
    ) {
        $this->errorMessage = "";

        $sUserGender = (trim($sGender) == 'female' ? 'F' : (trim($sGender) == 'male' ? 'M' : ''));

        if($idCountry == '') {
            $idCountry = HeMixed::getCountryId(Yii::app()->request->getUserHostAddress());
        }

        if ($sEmail == "") {
            $this->errorCode =      '507';
            $this->errorMessage =   "The user email is empty or is not valid.";
            return false;
        }

        $abaUser =      new AbaUser();
        $userExists =   $abaUser->getUserByEmail($sEmail);

        $abaUserFb =    new AbaUser();
        $fbUserExists = $abaUserFb->getUserByFacebookId($iFbid);

        //
        // Add user, if no exist
        //
        if(!$userExists && !$fbUserExists) {

            $commUserRegistration = new RegisterUsersCommon();
            $sPwd = $commUserRegistration->genRandomPasswordNewUser($sEmail, "LT");

            //
            // Register user
            //
            $newUser = $this->registerUser (
                $sEmail,
                $sPwd,
                $langEnv,
                $idCountry,
                trim($sName),
                trim($sSurname),
                $currentLevel,
                $idPartner,
                $idSourceList,
                $device,
                REGISTER_USER_FROM_FACEBOOK,
                $scenario
            );

            if($newUser) {

                $newUser->fbId = $iFbid;

                $aUpdate = array("fbId");

                if($sUserGender != '') {

                    array_push($aUpdate, "gender");

                    $newUser->gender = $sUserGender;
                }
                $newUser->update($aUpdate);
            }

            return $newUser;
        }

        //
        // Update user, if exists
        //
        if($userExists && !$fbUserExists) {

            $commUser = new UserCommon();
            return $commUser->changeUserFbData( $abaUser, $sName, $sSurname, $iFbid, $sUserGender);
        }

        return false;
    }
    /**
     * @param string $sEmail
     * @param string $sName
     * @param string $sSurname
     * @param string $sGender
     * @param string $langEnv
     * @param string $idCountry
     * @param int $idPartner
     * @param int $idSourceList
     * @param string $device
     * @param int $currentLevel
     *
     * @return AbaUser|bool
     */
    public function registerUserLinkedin($sEmail='', $sName='', $sSurname='', $iinid=null, $sGender='',
        $langEnv='', $idCountry='', $idPartner=null, $idSourceList=null, $device='', $currentLevel=NULL, $scenario=null
    ) {
        $this->errorMessage = "";

        $sUserGender = (trim($sGender) == 'female' ? 'F' : (trim($sGender) == 'male' ? 'M' : ''));

        if($idCountry == '') {
            $idCountry = Yii::app()->config->get("ABACountryId");
        }

        if ($sEmail == "") {
            $this->errorCode =      '507';
            $this->errorMessage =   "The user email is empty or is not valid.";
            return false;
        }

        $abaUser =      new AbaUser();
        $userExists =   $abaUser->getUserByEmail($sEmail);

        $abaUserin =    new AbaUser();
        $inUserExists = $abaUserin->getUserByLinkedinId($iinid);

        //
        // Add user, if no exist
        //
        if(!$userExists && !$inUserExists) {

            $commUserRegistration = new RegisterUsersCommon();
            $sPwd = $commUserRegistration->genRandomPasswordNewUser($sEmail, "LT");

            //
            // Register user
            //
            $newUser = $this->registerUser (
                $sEmail,
                $sPwd,
                $langEnv,
                $idCountry,
                trim($sName),
                trim($sSurname),
                $currentLevel,
                $idPartner,
                $idSourceList,
                $device,
                REGISTER_USER_FROM_FACEBOOK,
                $scenario
            );

            if($newUser) {

                $newUser->inId = $iinid;

                $aUpdate = array("inId");
                if($sUserGender != '') {

                    array_push($aUpdate, "gender");

                    $newUser->gender = $sUserGender;
                }
                $newUser->update($aUpdate);
            }

            return $newUser;
        }

        //
        // Update user, if exists
        //
        if($userExists && !$inUserExists) {

            $commUser = new UserCommon();
            return $commUser->changeUserinData( $abaUser, $sName, $sSurname, $iinid, $sUserGender);
        }

        return false;
    }


}
