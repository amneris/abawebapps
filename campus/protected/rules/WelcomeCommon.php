<?php

/**
 * Created by PhpStorm.
 * User: Anna
 * Date: 13/06/14
 * Time: 11:03
 */
class WelcomeCommon
{
    private $workflow = null;

    /**
     * Function to choose if we show the welcome pop-up and which kind of
     * @return array
     */
    public function showWelcome($case = null)
    {
        $user = new AbaUser();
        if ($user->getUserById(Yii::app()->user->getId())){
            $this->workflow = (!empty($case)) ? $case : $this->chooseWelcomeFlow($user);
            $data = $this->welcomeWorkflow();

            return $data;
        }

        return false;
    }

    /**
     * Function to choose if we show the welcome pop-up and which kind of
     * @return integer //
     */
    private function welcomeWorkflow()
    {
        $data = array();
        $data['welcomeUrl'] = null;
        $data['welcomeData'] = null;
        $data['submitUrl'] = null;
        $data['submitData'] = null;
        $data['submitRedirect'] = false;
        switch ($this->workflow) {
            case WELCOME_USER_DATA:
                // show user data form and after validate redirects to home page
                // 3443: if user has saved SOME objectives in the register, then we fill the form
                $moFormObjectives = $this->fillObjectives();
                $data['welcomeUrl'] = '//welcome/userData';

                $user = Yii::app()->user;
                $is2ndLogin = isset($_COOKIE['loginTimes']) && $_COOKIE['loginTimes'] == 2;
                $hidePopup = isset($_COOKIE['hidePopup']) && $_COOKIE['hidePopup'] == true;
                if ($user->role == FREE && $is2ndLogin && !$hidePopup) { /* 5377 */
                    $data['submitUrl'] = '//welcome/memberGetMember';
                    $data['welcomeData'] = array('model' => $moFormObjectives,
                        'name' => Yii::app()->user->getName(),
                        'header' => 'default',
                        'closeBtn' => "render");
                } else {
                    $data['submitUrl'] = Yii::app()->createAbsoluteUrl('site/index');
                    $data['submitRedirect'] = true;
                    $data['submitData'] = array('redirect' => $data['submitRedirect']);
                    $data['welcomeData'] = array('model' => $moFormObjectives,
                        'name' => Yii::app()->user->getName(),
                        'header' => 'default',
                        'closeBtn' => 'true');
                }

                break;

            case WELCOME_LEVEL_SELECT:
                // show user level select form and after validate redirects to first unit
                $data['welcomeUrl'] = '//welcome/levelSelect';
                $data['welcomeData'] = array('header' => 'default',
                    'name' => Yii::app()->user->getName());
                $data['submitUrl'] = 'getStartUnitByLevel'; // use that key to tell the controller recalculate the path depending on the level
                $data['submitRedirect'] = true;
                $data['submitData'] = array('redirect' => $data['submitRedirect']);

                break;

            case WELCOME_MGM:
                $data['welcomeUrl'] = '//welcome/memberGetMember';
                break;
            case WELCOME_TESTLEVEL:
                Yii::app()->getModule('leveltest');
                $data['welcomeUrl'] = 'leveltest.views.main.main';
                break;
        }

        return $data;
    }

    /** Once we know the flow is the special case of the previous partners
     * we show the level select popup if it was not saved before
     * @param AbaUser
     * @return bool
     */
    private function chooseWelcomeFlow($user)
    {
        $show = false;

        // first login shows select level popup
        if (Yii::app()->user->getState('isUserFirstLogin')) {
            if (!$user->registerLevel) {
                $show = WELCOME_LEVEL_SELECT;
            }
        } else if (!Yii::app()->user->getSavedUserObjective()) {
            // if user did not fill the objectives before
            $show = WELCOME_USER_DATA;
        }

        return $show;
    }

    /**
     * Fill the view form in case of objectives has already some data
     * @return Paso1Form
     */
    private function fillObjectives()
    {
        $moFormObjectives = new Paso1Form();

        $moUserObjectives = new AbaUserObjective();
        if ($objectives = $moUserObjectives->getObjectivesByUserId(Yii::app()->user->getId())) {
            $moFormObjectives->setidUser($objectives->getAttribute('userId'));
            $moFormObjectives->setviajar($objectives->getAttribute('travel'));
            $moFormObjectives->settrabajar($objectives->getAttribute('work'));
            $moFormObjectives->setestudiar($objectives->getAttribute('estudy'));
            $moFormObjectives->setnivel($objectives->getAttribute('level'));
            $moFormObjectives->setselEngPrev($objectives->getAttribute('selEngPrev'));
            $moFormObjectives->setdedicacion($objectives->getAttribute('dedication'));
            $moFormObjectives->setengPrev($objectives->getAttribute('engPrev'));
            $moFormObjectives->setotros($objectives->getAttribute('others'));
        }

        return $moFormObjectives;
    }
}