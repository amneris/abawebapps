/* --------------- PARTIAL LOAD, UPDATE --------------------*/
 /* ------------BELOW THE FULL COLUMNS SQL---------------- */
SELECT u.`id` as `IDUSER_WEB`,
/*u.`email` as `MAIL`,*/
/*
u.`name` as `NAME`,
u.`surnames` as  `SURNAMES`,
*/
u.`countryIdCustom` as `COUNTRY_IP`,
u.`countryId` as `COUNTRY_USER`,
/*
u.`password` as `PASS_TMP`,
u.`keyExternalLogin` as `PASSWORD`,
IF( u.`birthDate` is null OR u.`birthDate`='0000-00-00 00:00:00','NULL', u.`birthDate`) as `BIRTHDAY_DT`,
u.`gender`  as `SEX`,
u.`telephone` as `TELEPHONE`,
*/
u.`cancelReason` as `CANCEL_REASON`,
/*CONCAT(IF(uo.`travel`=1,'travel,',''),IF(uo.`estudy`=1,'study,',''),IF(uo.`work`=1,'work,',''),IF(uo.`others`=1,'other,','') ) as `WHY_STUDY`,
CASE uo.`level` WHEN '1' THEN 'medium' WHEN '2' THEN 'high' WHEN '3' THEN 'native' END as `OBJECTIVES_LEVEL`,
CASE uo.`selEngPrev` WHEN 1 THEN 'school'  WHEN 2 THEN 'academy'  WHEN 3 THEN 'online'  WHEN 4 THEN 'personal class'  WHEN 5 THEN 'other'  ELSE 'no'  END as `PREVIOUS_EXPERIENCE`,
CASE uo.`dedication` WHEN 1 THEN 'study'  WHEN 2 THEN 'work'  WHEN 3 THEN 'looking for a job'  END as `OCCUPATION`,
u.`langEnv` as `PREF_LANG`,
*/
IF( u.`entryDate` is null OR u.`entryDate`='0000-00-00 00:00:00','NULL', u.`entryDate`) as `ENTRY_DATE`,
CASE u.`userType` WHEN 0 THEN 'lead'
  WHEN 1 THEN '1'
  WHEN 2 THEN '2'
  WHEN 3 THEN 'plus'
  ELSE 'teacher'
  END as `USER_TYPE`,
IF( u.`expirationDate` is null OR u.`expirationDate`='0000-00-00 00:00:00','NULL', u.`expirationDate`) as `EXPIRATION_DATE`,
/*p.`idPeriodPay` as `PRODUCT_TYPE`,
p.`idPromoCode` as `PROMOCODE`,
u.currentLevel as `LEVELCOURSE`,
-- uvu.videoId as `ID_VIDEOCLASS`,
*/
IF(u.firstLoginDone=1, '1', '0' ) as `FLAG_CAMPUS_LOGIN`,
IF(count(u.hasWorkedCourse)>=1, '1', '0' ) as `FLAG_CAMPUS_WORK`,
/*IF( u.idPartnerFirstPay is null,'no','si' ) as `PREMIUM_HISTORIC`,*/
u.idPartnerSource as `PARTNERT_SOURCE`,
CASE aplSource.idPartnerGroup WHEN 0 THEN 'abaenglish' WHEN 1 THEN 'flashsales' WHEN 2 THEN 'other' ELSE '' END as `PARTNERT_SOURCE_GROUP`,
u.idPartnerFirstPay as `PARTNERT_FIRSTPAY`,
CASE aplFirst.idPartnerGroup WHEN 0 THEN 'abaenglish' WHEN 1 THEN 'flashsales' WHEN 2 THEN 'other' ELSE '' END as `PARTNERT_FIRSTPAY_GROUP`,
u.idPartnerCurrent as `PARTNERT_CURRENT`,
CASE aplCurrent.idPartnerGroup WHEN 0 THEN 'abaenglish' WHEN 1 THEN 'flashsales' WHEN 2 THEN 'other' ELSE '' END as `PARTNERT_CURRENT_GROUP`


FROM `user` u
LEFT JOIN `user_objective` uo ON u.`id` = uo.`userId`
-- LEFT JOIN (SELECT userId, `idPeriodPay`, `status`, `idPromoCode` FROM `payments` WHERE `status`='30' ORDER BY `dateStartTransaction` DESC ) p ON u.`id`=p.`userId`
-- LEFT JOIN `user_videoclasses_unlocks` uvu ON u.`id` = uvu.`userId` AND u.currentLevel=uvu.userLevel
LEFT JOIN `aba_partners_list` aplSource ON u.idPartnerSource=aplSource.idPartner
LEFT JOIN `aba_partners_list` aplFirst ON u.idPartnerFirstPay=aplFirst.idPartner
LEFT JOIN `aba_partners_list` aplCurrent ON u.idPartnerCurrent=aplCurrent.idPartner

WHERE u.`userType`=2

-- AND u.entryDate>'2013-06-01' /* OR u.`userType`=1*/

GROUP BY u.id;

/*--------------- COMPLETE LOAD---------------------------*/
SELECT u.`id` as `IDUSER_WEB`,
u.`email` as `MAIL`,
u.`name` as `NAME`,
u.`surnames` as  `SURNAMES`,
u.`countryIdCustom` as `COUNTRY_IP`,
u.`countryId` as `COUNTRY_USER`,
u.`password` as `PASS_TMP`,
u.`keyExternalLogin` as `PASSWORD`,
IF( u.`birthDate` is null OR u.`birthDate`='0000-00-00 00:00:00','NULL', u.`birthDate`) as `BIRTHDAY_DT`,
u.`gender`  as `SEX`,
u.`telephone` as `TELEPHONE`,
u.`cancelReason` as `CANCEL_REASON`,
CONCAT(IF(uo.`travel`=1,'travel,',''),IF(uo.`estudy`=1,'study,',''),IF(uo.`work`=1,'work,',''),IF(uo.`others`=1,'other,','') ) as `WHY_STUDY`,
CASE uo.`level` WHEN '1' THEN 'medium' WHEN '2' THEN 'high' WHEN '3' THEN 'native' END as `OBJECTIVES_LEVEL`,
CASE uo.`selEngPrev` WHEN 1 THEN 'school'  WHEN 2 THEN 'academy'  WHEN 3 THEN 'online'  WHEN 4 THEN 'personal class'  WHEN 5 THEN 'other'  ELSE 'no'  END as `PREVIOUS_EXPERIENCE`,
CASE uo.`dedication` WHEN 1 THEN 'study'  WHEN 2 THEN 'work'  WHEN 3 THEN 'looking for a job'  END as `OCCUPATION`,
u.`langEnv` as `PREF_LANG`,
IF( u.`entryDate` is null OR u.`entryDate`='0000-00-00 00:00:00','NULL', u.`entryDate`) as `ENTRY_DATE`,
CASE u.`userType` WHEN 0 THEN 'lead'
  WHEN 1 THEN '1'
  WHEN 2 THEN '2'
  WHEN 3 THEN 'plus'
  ELSE 'teacher'
  END as `USER_TYPE`,
IF( u.`expirationDate` is null OR u.`expirationDate`='0000-00-00 00:00:00','NULL', u.`expirationDate`) as `EXPIRATION_DATE`,
p.`idPeriodPay` as `PRODUCT_TYPE`,
p.`idPromoCode` as `PROMOCODE`,
u.currentLevel as `LEVELCOURSE`,
-- uvu.videoId as `ID_VIDEOCLASS`,
IF(u.firstLoginDone=1, '1', '0' ) as `FLAG_CAMPUS_LOGIN`,
IF(count(u.hasWorkedCourse)>=1, '1', '0' ) as `FLAG_CAMPUS_WORK`,
IF( u.idPartnerFirstPay is null,'no','si' ) as `PREMIUM_HISTORIC`,
u.idPartnerSource as `PARTNERT_SOURCE`,
CASE aplSource.idPartnerGroup WHEN 0 THEN 'abaenglish' WHEN 1 THEN 'flashsales' WHEN 2 THEN 'other' ELSE '' END as `PARTNERT_SOURCE_GROUP`,
u.idPartnerFirstPay as `PARTNERT_FIRSTPAY`,
CASE aplFirst.idPartnerGroup WHEN 0 THEN 'abaenglish' WHEN 1 THEN 'flashsales' WHEN 2 THEN 'other' ELSE '' END as `PARTNERT_FIRSTPAY_GROUP`,
u.idPartnerCurrent as `PARTNERT_CURRENT`,
CASE aplCurrent.idPartnerGroup WHEN 0 THEN 'abaenglish' WHEN 1 THEN 'flashsales' WHEN 2 THEN 'other' ELSE '' END as `PARTNERT_CURRENT_GROUP`

FROM `user` u
LEFT JOIN `user_objective` uo ON u.`id` = uo.`userId`
LEFT JOIN (SELECT userId, `idPeriodPay`, `status`, `idPromoCode` FROM `payments` WHERE `status`='30' ORDER BY `dateStartTransaction` DESC ) p ON u.`id`=p.`userId`
-- LEFT JOIN `user_videoclasses_unlocks` uvu ON u.`id` = uvu.`userId` AND u.currentLevel=uvu.userLevel
LEFT JOIN `aba_partners_list` aplSource ON u.idPartnerSource=aplSource.idPartner
LEFT JOIN `aba_partners_list` aplFirst ON u.idPartnerFirstPay=aplFirst.idPartner
LEFT JOIN `aba_partners_list` aplCurrent ON u.idPartnerCurrent=aplCurrent.idPartner

WHERE u.`userType`=2 OR u.`userType`=1

GROUP BY u.id;

