<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 18/12/2013
 * Time: 15:57
 * To be used by La Caixa, PayPal or whatever other Gateways Suppliers we ve got in Aba.
 * The idea is to unify all logic from here.
 */
class BoletoCommon
{
    private $errorCode;
    private $errorMessage;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /** It gets off all all Boleto that have expired
     *
     * @param string $dateLimit ; YYYY-MM-DD
     * @return bool
     */
    public function processOff($dateLimit)
    {
        $allSuccess = true;
        $aBolSuccess = array();
        $aBolErrors = array();

        $moBoleto = new AbaPaymentsBoleto();
        $aIdsBoletosExpired = $moBoleto->getAllPending($dateLimit);
        foreach ($aIdsBoletosExpired as $aRowBoleto) {
            $moBoleto = new AbaPaymentsBoleto();
            if ($moBoleto->getBoletoById($aRowBoleto['id'])) {
                $allSuccess = $moBoleto->updateOffDate();
                $aBolSuccess[$aRowBoleto['paySuppExtUniId']] = $aRowBoleto['idPayment'];
            } else {
                $aBolErrors[$aRowBoleto['paySuppExtUniId']] = $aBolErrors['idPayment'];
                $allSuccess = false;
            }
        }

        foreach ($aBolSuccess as $key => $idPayment) {
            $moPayCtrlCheck = new PaymentControlCheck();
            $moPayCtrlCheck->getPaymentById($idPayment);
            $moPayCtrlCheck->updateStatusAfterPayment(PAY_FAIL, PAY_SUPPLIER_ALLPAGO_BR_BOL,
                $idPayment, 'Boleto is off', '', '');
        }

        if (!$allSuccess) {
            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_LOGIC_L . ' Error during Boleto processed off dates.',
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                'During the Off Boleto processing some Boletos could not be updated.'
            );
        }
        return $allSuccess;
    }
}
