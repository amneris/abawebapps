<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 21/11/12
 * Time: 15:57
 * To be used by La Caixa, PayPal or whatever other Gateways Suppliers we ve got in Aba.
 * The idea is to unify all logic from here.
 */
class RecurrentPayCommon
{
    private $errorMessage;

    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param string $email
     * @param string $paySuppOrderId Identifier for Gateway supplier
     * @param null $dateStart YYYY-mmm-dd
     * @param string $operation
     * @param integer $paySupplier
     * @param string $idPayment
     * @param array $aPaypalInfo
     *
     * @return bool
     */
    public function renewPaymentByPaySuppOrderId($email, $paySuppOrderId, $dateStart = NULL, $operation = '',
      $paySupplier = PAY_SUPPLIER_PAYPAL, $idPayment = NULL, $aPaypalInfo = NULL, $paySuppExtProfId='')
    {
        // <<$paySuppOrderId=str_replace("&amp;","&",$paySuppOrderId);>>
        $user = new AbaUser();
        if (!$user->getUserByEmail($email)) {
            $this->errorMessage = "Not found user account delivered by email= " . $email;
            // Must found always a pending payment, except if the user has cancelled maybe.
            HeLogger::sendLog("$operation, User email not found. ", HeLogger::IT_BUGS, HeLogger::CRITICAL,
              $this->errorMessage);
            return false;
        }

        $moPayment = new Payment();
        if (isset($idPayment)) {
            // La Caixa case, we have then the id payment:
            $isFoundPending = $moPayment->getPaymentById($idPayment);
        } else {
            // PAYPAL case, we do not have id payment:
            $isFoundPending = $moPayment->getPaymentByPaySuppOrderId($email, $paySuppOrderId, PAY_PENDING, true);
        }

        if (!$isFoundPending) {
            $this->errorMessage = "Not found payment with order id = " . $paySuppOrderId;
            // Must found always a pending payment, except if the user has cancelled maybe.
            HeLogger::sendLog(HeLogger::PREFIX_ERR_UNKNOWN_H . ":Review RecurrentPayCommon.php, suscription payment not found:" . $paySuppOrderId,
              HeLogger::IT_BUGS, HeLogger::CRITICAL, $this->errorMessage);
            return false;
        }

        $moProduct = new ProductPrice();
        if (!$moProduct->getProductById($moPayment->idProduct)) {
            $this->errorMessage = "Not found product  with id = " . $moPayment->idProduct;
            // Must found always a product, except if the user has cancelled maybe.
            HeLogger::sendLog(HeLogger::PREFIX_ERR_LOGIC_L . ":Recurring payment, product not found.",
              HeLogger::IT_BUGS, HeLogger::CRITICAL,
              " Check payments of this user please:" . $user->email . " ; " . $this->errorMessage);
            // In case of TPV, we must have the product in our database.
            if ($paySupplier !== PAY_SUPPLIER_GROUPON) {
                return false;
            } // In case of PAYPAL, we never REJECT the payment, but as long as the product is not available, we make it up the kind of access
            else {
                $moProduct->userType = PREMIUM;
            }
        }

        // We update as if payment has been successfully carried.
        // status, dateStartTransaction, dateEndTransaction, xRateToEUR, xRateToUSD
        $moCurrency = new AbaCurrency($moPayment->foreignCurrencyTrans);
        $xRateToEUR = $moCurrency->getConversionRateTo(CCY_DEFAULT);
        $xRateToUSD = $moCurrency->getConversionRateTo(CCY_2DEF_USD);
        $paySuppExtUnid = null;

        if (is_array($aPaypalInfo) AND array_key_exists("txn_id", $aPaypalInfo)) {
            $paySuppExtUnid = $aPaypalInfo["txn_id"];
        }

        //#4681
        if($paySupplier == PAY_SUPPLIER_CAIXA AND trim($moPayment->paySuppExtProfId) == '' AND trim($paySuppExtProfId) <> '') {
            $fSuccUpd = $moPayment->recurringPaymentProcess($paySupplier, $paySuppOrderId, PAY_SUCCESS, $dateStart, HeDate::todaySQL(true), $xRateToEUR, $xRateToUSD, $paySuppExtUnid, $paySuppExtProfId);

            $moPayment->paySuppExtProfId = $paySuppExtProfId;
        }
        else {
            $fSuccUpd = $moPayment->recurringPaymentProcess($paySupplier, $paySuppOrderId, PAY_SUCCESS, $dateStart, HeDate::todaySQL(true), $xRateToEUR, $xRateToUSD, $paySuppExtUnid);
        }

        if (!$fSuccUpd) {
            $this->errorMessage = "ABA: Could not be updated SQL for old payment = " . $moPayment->id;
            // Must found always a pending payment, except if the user has cancelled maybe.
            HeLogger::sendLog(HeLogger::PREFIX_ERR_UNKNOWN_H . ": Recurring payment , old payment could not be updated. ",
              HeLogger::IT_BUGS, HeLogger::CRITICAL, $this->errorMessage);
            return false;
        }

        // BUG-1205, we choose the option to send an email every time it happens instead of automatized the operation.
        // If we see there are many cases we would implement the proper modifications:
        if (isset($aPaypalInfo) && !empty($aPaypalInfo)) {
            if (trim(strtoupper($moPayment->currencyTrans)) !== trim(strtoupper($aPaypalInfo["currency_code"]))) {
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_LOGIC_L . ": Due to bug-1205, we recommend check and correct all " .
                    "currencies codes in this payment.",
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    " IPN PAYPAL notified us with a renewal of a subscription. Due to a bug at the beginning of the ".
                    "Campus Project, some payments were imported form old system with wrong currencies codes.".
                    " Most of them were USD, and were imported as EUR. Details of this very case: <br/>" .
                    "<br/>user Id= " . $moPayment->userId .
                    "<br/>id payment= " . $moPayment->id .
                    "<br/>Current payment currency code= " . $moPayment->currencyTrans .
                    "<br/>Paypal currency code notified= " . $aPaypalInfo["currency_code"] .
                    "<br/>Exchange rates: xRateToEUR=" . $xRateToEUR
                );
            }
            if (floatval($aPaypalInfo["amount"]) !== floatval($moPayment->amountPrice)) {
                HeLogger::sendLog(
                    HeLogger::PREFIX_NOTIF_L . ": PayPal renewal processed but amounts don't match. User " .
                    $user->email,
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    " IPN PAYPAL notified us with a renewal of a subscription. And everything looks fine, but the ".
                    "amounts in PAYPAL and in our system do not match" .
                    "<br/>user Id= " . $moPayment->userId .
                    "<br/>id payment= " . $moPayment->id .
                    "<br/>Current payment currency code= " . $moPayment->currencyTrans .
                    "<br/>Amount= " . $moPayment->amountPrice .
                    "<br/>PayPal Amount= " . $aPaypalInfo["amount"]
                );
            }
        }

        // We copy from all data fields from current payment in order to create a new one.
        $moNextPayment = $moPayment->createDuplicateNextPayment();
        $moNextPayment->id = HeAbaSHA::generateIdPayment($user, $moNextPayment->dateToPay);
        $moNextPayment->idPayControlCheck = $moPayment->id;
        $moNextPayment->xRateToEUR = $xRateToEUR;
        $moNextPayment->xRateToUSD = $xRateToUSD;
        $moNextPayment->isPeriodPayChange = null;

        if ($paySupplier == PAY_SUPPLIER_APP_STORE) {
            $moNextPayment->idPartner = PaySupplierAppstore::getAppstoreRecurringPartner();
        }

        if ($paySupplier == PAY_SUPPLIER_ANDROID_PLAY_STORE or $moPayment->idPartner == PARTNER_ID_MOBILE_ANDROID OR $moPayment->idPartner == PARTNER_ID_ANDROID_RECURRING){
            $moNextPayment->idPartner = PARTNER_ID_ANDROID_RECURRING;
        }

        // Insert next payment expected from PayPal
        if (!$moNextPayment->paymentProcess($moNextPayment->userId)) {
            $this->errorMessage = "ABA: Recurring payment. Next payment could not be created into database. = " . $moNextPayment->idPayControlCheck;
            // Must found always a pending payment, except if the user has cancelled maybe.
            HeLogger::sendLog("Recurring payment , Next Payment could not be created. ",HeLogger::IT_BUGS,HeLogger::CRITICAL,$this->errorMessage);
            return false;
        }

        // Check for IT
        if ($moProduct->userType !== $user->userType) {
            HeLogger::sendLog(HeLogger::PREFIX_ERR_LOGIC_L . ": User Types are not the same ",
              HeLogger::IT_BUGS, HeLogger::CRITICAL,
              " The product " . $moProduct->idProduct . " has a userType assigend different that the one the user " . $user->email . " has. There is an inconsistency. Review payments, user and products for this product.");
        }

        // Now we have to update the info user: expirationDate, userType, idPartnerActual.
        if (!$user->updateUserRenewal($moProduct->userType, $moNextPayment->dateToPay, PARTNER_ID_RECURRINGPAY)) {
            $this->errorMessage = " Save user Expiration date  " . $moNextPayment->dateToPay . " has failed.";
            HeLogger::sendLog("Recurring payment user could not be updated. ",HeLogger::IT_BUGS,HeLogger::CRITICAL, $this->errorMessage);
            return false;
        }

        //  Send data to Selligent
        $commConnSelligent = new SelligentConnectorCommon();
        $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::pagosRecurrentes, $user,
          array("PROMOCODE" => $moNextPayment->idPromoCode), "renewPaymentByPaySuppOrderId method=" . $paySupplier);


        //
        // #5222
        // Send data to Selligent - Family Plan e-mail
        //
        try{
            $oRenewalsPlans = new RenewalsPlans();

            if($oRenewalsPlans->isValidRenewal($user, $moPayment)) {

                $stWsData = array(
                  'IDUSER_WEB' => $user->id,
                  'PREF_LANG' =>  $user->langEnv,
                  'COUNTRY_IP' => $user->countryId,
                  'PRICE' =>      $moPayment->amountPrice,
                  'CURRENCY' =>   $moPayment->currencyTrans,
                  'PRODUCT' =>    $moPayment->idPeriodPay,
                  'RENOV_DT' =>   $moPayment->dateToPay,
                );

                $commConnSelligent = new SelligentConnectorCommon();
                if($commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::renewalsPlanPayment, $user, $stWsData, "renewPaymentByPaySuppOrderId method=" . $paySupplier)) {

                    $oRenewalsPlans->updateToRenewed();
                }
            }
        }
        catch(Exception $e){
            HeLogger::sendLog("Recurring payment, Renewals Plan. ",HeLogger::IT_BUGS, HeLogger::CRITICAL, $e->getMessage());
        }
        //

        if(Yii::app()->config->get("ENABLE_LOG_USERACTIVITY") == 1) {
            $user->abaUserLogUserActivity->insertLog("payments", "", " User has been renovated successfully.",
              "(AUTO)recurrent payment");
        }

        return true;
    }


    /**
     * @param AbaUser $moUser
     * @param Payment $moLastPayment
     * @param string $paySuppOrderId
     * @param int $cancelReason
     *
     * @return bool
     */
    public function insertCancelPayment(AbaUser $moUser, $moLastPayment = null, $paySuppOrderId, $cancelReason = 1)
    {
        $moPendingPayment = new Payment();
        $moPendingPayment->getPaymentById();

        if (is_null($moLastPayment)) {
            $moPendingPayment->getPaymentById();
            $moPendingPayment->id = HeAbaSHA::generateIdPayment($moUser);
            // Product by default
            $commProducts = new ProductsPricesCommon();
            $aProdUsers =   $commProducts->getListProductsByUser($moUser);

            $moPendingPayment->userId = $moUser->id;
            $moPendingPayment->idProduct = $aProdUsers[0]->idProduct;
            $moPendingPayment->idCountry = $aProdUsers[0]->idCountry;
            $moPendingPayment->idPeriodPay = $aProdUsers[0]->idPeriodPay;
            $moPendingPayment->amountOriginal = $aProdUsers[0]->getOriginalPriceTransaction();
            $moPendingPayment->amountDiscount = 0.00;
            $moPendingPayment->amountPrice = $aProdUsers[0]->getOriginalPriceTransaction();
            $moPendingPayment->currencyTrans = $aProdUsers[0]->ABAIdCurrency;
            $moPendingPayment->foreignCurrencyTrans = $aProdUsers[0]->officialIdCurrency;

        } else {
            $moPendingPayment = clone($moLastPayment);
        }

        $moPendingPayment->id = HeAbaSHA::generateIdPayment($moUser);
        $moPendingPayment->paySuppExtId = PAY_SUPPLIER_PAYPAL;
        $moPendingPayment->paySuppOrderId = $paySuppOrderId;
        $moPendingPayment->status = PAY_CANCEL;
        $moPendingPayment->dateStartTransaction = HeDate::todaySQL(true);
        $moPendingPayment->dateEndTransaction = HeDate::todaySQL(true);
        $moPendingPayment->dateToPay = HeDate::todaySQL(true);
        $moPendingPayment->lastAction = HeDate::todaySQL(true) . " [Payment cancellation inserted from IPN automatic cancellation process. There was no PENDING LINE, so we simply make it up]";
        $moPendingPayment->isRecurring = 1;

        $moUser->cancelReason = $cancelReason;
        $moUser->update(array("cancelReason"));

        if (!$moPendingPayment->paymentProcess()) {
            return false;
        }
        return true;
    }

    /** Cancels a payment and a subscription of a user
     *
     * @param        $paymentId
     * @param        $dateStart
     * @param string $operationSource
     * @param AbaUser $moUser
     * @param int $cancelReason It can be false in case of upgrade plan.
     * @param int $cancelOrigin It means the action that cause the cancellation
     *
     * @return bool
     */
    public function cancelPayment($paymentId, $dateStart, $operationSource = "Payment cancelled from Campus",
                                  AbaUser $moUser, $cancelReason = 1, $cancelOrigin = 1)
    {
        $moPayment = new Payment();
        if (!$moPayment->getPaymentById($paymentId)) {
            $this->errorMessage = "Not found payment with id =" . $paymentId;
            return false;
        }

        if (!$moPayment->updateCancelRenewPay($dateStart, $operationSource, $cancelOrigin)) {
            return false;
        }

        if ($cancelReason !== false) {
            $moUser->cancelReason = $cancelReason;
            $moUser->update(array("cancelReason"));
        }

        return true;
    }


    /**
     * @param $paymentId
     * @param $dateStart
     * @param string $operationSource
     * @param AbaUser $moUser
     * @param int $cancelReason
     * @return bool
     */
    public function failRecurring($paymentId, $dateStart, $operationSource = "Payment failure from Campus",
                                  AbaUser $moUser, $cancelReason = 3)
    {
        $moPayment = new Payment();
        if (!$moPayment->getPaymentById($paymentId)) {
            $this->errorMessage = "Not found payment with id =" . $paymentId;
            return false;
        }

        if (!$moPayment->updateFailedRenewPay($dateStart, $operationSource)) {
            return false;
        }

        $moUser->cancelReason = $cancelReason;
        $moUser->update(array("cancelReason"));

        return true;
    }

    /**
     * @param $paymentId
     * @param $newIdUserCreditForm
     *
     * @return bool
     */
    public function changeCreditInPayment($paymentId, $newIdUserCreditForm)
    {
        // Validate PENDING payment:
        $moPayment = new Payment();
        if (!$moPayment->getPaymentById($paymentId)) {
            $this->errorMessage = "Payment id " . $paymentId . " does not exist.";
            return false;
        }
        /*elseif ($moPayment->status != PAY_PENDING) {
            $this->errorMessage = "There is no subscription active.";
            return false;
        }*/

        // Validate $newIdUserCreditForm exists and if there is a change of method Payment
        /* @var $moUserCreditCurrent AbaUserCreditForms */
        $moUserCreditCurrent = new AbaUserCreditForms();
        $moUserCreditCurrent = $moUserCreditCurrent->getUserCreditFormById($moPayment->idUserCreditForm);
        if (!$moUserCreditCurrent) {
            $this->errorMessage = "There is no credit form active.";
            return false;
        }
        $moUserCreditNew = new AbaUserCreditForms();
        if (!$moUserCreditNew->getUserCreditFormById($newIdUserCreditForm)) {
            $this->errorMessage = "New credit form is not found or could not be saved.";
            return false;
        }

        // NO CHANGE of METHOD OF PAYMENT IS ALLOWED YET
        if (($moUserCreditNew->kind == KIND_CC_PAYPAL && $moUserCreditCurrent->kind != KIND_CC_PAYPAL) ||
            ($moUserCreditNew->kind != KIND_CC_PAYPAL && $moUserCreditCurrent->kind == KIND_CC_PAYPAL)
        ) {
            $this->errorMessage = "It is not allowed to change the method of payment from PAYPAL to credit card
                                                                                        or viceversa";
            return false;
        }

        // Inform payment gateway to change credit details
        $comPaySupplier = PaymentFactory::createPayment($moPayment);
        if (!$comPaySupplier->runChangeCreditDetails($moUserCreditCurrent, $moUserCreditNew)) {
            $this->errorMessage = "Sending new credit form to our payment gateway failed " .
                "(gateway=" . $moPayment->paySuppExtId . ").";
            return false;
        }

        // Update payment.idUserCreditForm
        if (!$moPayment->updateCreditForm($moUserCreditNew->id)) {
            $this->errorMessage = "Updating id user credit form in subscription failed.";
            return false;
        }

        // Saves the registration Id, which should be the same as the previous one!:
        if ($moPayment->paySuppExtId == PAY_SUPPLIER_ALLPAGO_BR) {
            $moUserCreditNew->registrationId = $moUserCreditCurrent->registrationId;
            $moUserCreditNew->update(array("registrationId"));
        }

        $moUser = new AbaUser();
        $moUser->getUserById($moPayment->userId);
        $logUserActivity = new AbaLogUserActivity($moUser);
        $logUserActivity->saveUserLogActivity("payments", "Credit Form " . $moUserCreditNew->id .
            " has changed for payment " . $moPayment->id,
            "ChangeCreditForm");

        return true;
    }

    /** Returns  the user status for the current or last subscription. It can only be one of these status:
     *   SUBS_PREMIUM_A_CARD_A, SUBS_PREMIUM_C_PAYPAL, etc.
     *
     * @param AbaUser $moUser
     * @param Payment $moLastPaySubs Last payment regarding the last subscription, can only be PENDING or CANCEL.
     *
     * @return int
     */
    public function getTypeSubscriptionStatus(AbaUser $moUser, &$moLastPaySubs = null)
    {
        if (empty($moUser->id)) {
            return SUBS_NULL;
        }
        // we compose PREMIUM(2) + ACTIVE (0-PENDING) + CARD (ALLPAGO)
        $userType = intval($moUser->userType);

        if (empty($moLastPaySubs) || empty($moLastPaySubs->id)) {
            $moLastPaySubs = new Payment();
            if (!$moLastPaySubs->getLastPayLastSubscription($moUser->id)) {
                if (!$moLastPaySubs->getLastPaymentByUserId($moUser->id, true)) {
                    // If there is no PAYMENT is obviously FREE;
                    $moLastPaySubs = NULL;
                    if ($userType == FREE) {
                        return SUBS_FREE;
                    } else {
                        if ($userType == DELETED) {
                            return SUBS_FREE;
                        } else {
                            return false;
                        }
                    }
                }
            }
        }

        if ($moLastPaySubs->paySuppExtId == PAY_SUPPLIER_GROUPON ||
            $moLastPaySubs->paySuppExtId == PAY_SUPPLIER_B2B
        ) {
            $paySuppExtId = intval($moLastPaySubs->paySuppExtId);
            $payStatus = str_pad(PAY_CANCEL, 2, '00', STR_PAD_LEFT);
        } else {
            $paySuppExtId = intval($moLastPaySubs->paySuppExtId);
            $payStatus = $moLastPaySubs->status;
            if ($payStatus == PAY_FAIL) {
                $payStatus = PAY_CANCEL;
            } elseif ($payStatus == PAY_SUCCESS) {
                // Case PAYPAL not creation of RECURRING:
                $payStatus = PAY_CANCEL;
            }
            $payStatus = str_pad($payStatus, 2, '00', STR_PAD_LEFT);
        }
        if ( ($paySuppExtId == PAY_SUPPLIER_ALLPAGO_BR || $paySuppExtId == PAY_SUPPLIER_ALLPAGO_MX ) &&
            $userType == FREE) {
            // In theory this case could not happen:
            $paySuppExtId = PAY_METHOD_CARD;
        }

        $typeSubsStatus = '' . $userType . $payStatus . $paySuppExtId;
        switch ($typeSubsStatus) {
            case SUBS_PREMIUM_A_CARD_A:
            case SUBS_PREMIUM_A_CARD_C:
            case SUBS_PREMIUM_A_CARD_M:
            case SUBS_PREMIUM_A_CARD_AD:
            case SUBS_PREMIUM_A_CARD_IOS:
            case SUBS_PREMIUM_A_PAYPAL:
            case SUBS_PREMIUM_C_CARD_A:
            case SUBS_PREMIUM_C_CARD_M:
            case SUBS_PREMIUM_C_CARD_C:
            case SUBS_PREMIUM_C_CARD_AD:
            case SUBS_PREMIUM_C_CARD_IOS:
            case SUBS_PREMIUM_C_PAYPAL:
            case SUBS_EXPREMIUM_CARD:
            case SUBS_EXPREMIUM_PAYPAL:
            case SUBS_PREMIUM_THROUGHPARTNER:
            case SUBS_PREMIUM_THROUGHB2B:
            case SUBS_EXPREMIUM_THROUGHPARTNER:
            case SUBS_PREMIUM_C_BOLETO_BR:
            case SUBS_PREMIUM_A_ZUORA:
            case SUBS_PREMIUM_A_ANDROID:
            case SUBS_PREMIUM_C_ANDROID:
            case SUBS_FREE:
                //continue, we have correctly matched a scenario.
                break;
            default:

                $typeSubsStatus = SUBS_FREE;
                if ($userType == PREMIUM) {
                    $typeSubsStatus = SUBS_PREMIUM_C_CARD_A;
                }
                break;
        }

        return $typeSubsStatus;
    }

    /** If the tab subscription in My Account should be displayed for the current user
     *
     * @param integer $typeSubscriptionStatus
     *
     * @return boolean
     */
    public static function isDisplaySubscription($typeSubscriptionStatus)
    {
        if (SUBS_PREMIUM_THROUGHPARTNER != $typeSubscriptionStatus &&
            SUBS_PREMIUM_THROUGHB2B != $typeSubscriptionStatus
        ) {
            return true;
        }
        return false;
    }


    /** Function to downgrade or upgrade an active  subscription for a PREMIUM user
     *
     * @param AbaUser $user
     * @param PaymentControlCheck $PayControlDraft
     * @param integer $idPartner
     *
     * @return bool|Payment
     */
    public function changeProduct(AbaUser $user, PaymentControlCheck $PayControlDraft, $idPartner=null)
    {
        if (!intval(Yii::app()->config->get('ENABLE_CHANGE_PRODUCT'))) {
            $this->errorMessage = 'Currently Aba English does not allow the change plan feature.';
            return false;
        }

        if (empty($idPartner)){
            $idPartner = Yii::app()->config->get('ABA_PARTNERID');
        }
        $this->errorMessage = '';
        // Collect all information on this subscription and user:
        $moLastPaySubs = new Payment();
        $typeSubs = $this->getTypeSubscriptionStatus($user, $moLastPaySubs);
        if (empty($moLastPaySubs) || $typeSubs == SUBS_FREE) {
            $this->errorMessage = ' We can not change a product to a FREE user, makes no sense mate.';
            return false;
        }

        $moLastPayPending = new Payment();
        if (!$moLastPayPending->getLastPayPendingByUserId($user->id)) {
            $this->errorMessage = 'There is no active subscription, there is no PENDING PAYMENT.';
            return false;
        }

        if ($moLastPaySubs->id !== $moLastPayPending->id) {
            $this->errorMessage = 'There are several PENDING payments or there is an inconsistency ' .
                'finding the last payment for the user ' . $user->id;
            return false;
        }
        $moLastPaySubs = null;

        if (HeDate::getDifferenceDateSQL($moLastPayPending->dateToPay, HeDate::todaySQL(true), HeDate::DAY_SECS) <= 1) {
            $this->errorMessage = 'key_before2days';
            return false;
        }

        // $productPrice->idProduct.DELIMITER_KEYS.$productPrice->idCountry.DELIMITER_KEYS.$productPrice->idPeriodPay
        $idNewProduct = trim($PayControlDraft->idProduct);
        $idOldProduct = $moLastPayPending->idProduct;
        // Let's warn about a student that has changed its country since the initial payment.
        if ($moLastPayPending->idCountry != $user->countryId) {
            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_LOGIC_H . ', Change product inconsistency',
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                ' User '.$user->id.' made initial payment with country '.$moLastPayPending->idCountry.' and product ' .
                $idOldProduct.' but now tries to change to country ' . $user->countryId . ', problems will arise with '.
                'prices and currencies. Please review or correct case ' . $moLastPayPending->id
            );
        }

        // DETERMINE if it is downgrade or upgrade, compare products.idPeriodPay
        $idNewPeriod = intval($PayControlDraft->idPeriodPay);
        $idOldPeriod = intval($moLastPayPending->idPeriodPay);

        //
        //#5087
        $commProducts = new ProductsPricesCommon();

        $idNewPeriodIsPlanProduct = $commProducts->isPlanProduct($PayControlDraft->idProduct);
        $idOldPeriodIsPlanProduct = $commProducts->isPlanProduct($moLastPayPending->idProduct);

        if ($idNewPeriod == $idOldPeriod AND $idNewPeriodIsPlanProduct == $idOldPeriodIsPlanProduct) {
            // SAME PRODUCT MAKES NO SENSE
            $this->errorMessage = 'There is no change in the current subscription, the product ' .
                $moLastPayPending->idProduct . ' is exactly the same.';
            return false;
        } else {
            if ($idNewPeriod > $idOldPeriod) {
                // Upgrade
                $casePlan = $idOldPeriod;
            } else {
                // Downgrade
                $casePlan = -1 * $idOldPeriod;
            }
        }

        // Validation of subscription, should be: PREMIUM+ACTIVE+PENDING in future + NO PAYPAL NOR B2B/GROUPON
        if ( $typeSubs != SUBS_PREMIUM_A_CARD_A &&
            $typeSubs != SUBS_PREMIUM_A_CARD_C  &&
            $typeSubs != SUBS_PREMIUM_A_CARD_M  &&
            $typeSubs != SUBS_PREMIUM_A_CARD_AD
        ) {
            $this->errorMessage = 'This operation is only permitted to users with credit card and with ' .
                ' an active subscription';
            return false;
        }

        // Create PENDING PAYMENT RECURRING:
        $moPayNewPlan = new Payment();
        if (!$moPayNewPlan->copyFromCancelPayToPending(HeAbaSHA::generateIdPayment($user), $PayControlDraft,
            $moLastPayPending, $idPartner, $casePlan)
        ) {
            $this->errorMessage = 'Payment PENDING could not be REPLACED';
            return false;
        }

        if (!$moPayNewPlan->paymentProcess($moPayNewPlan->userId)) {
            $this->errorMessage = 'Payment could not be inserted into database.';
            return false;
        }

        // Cancel ACTIVE payment:
        if (!$this->cancelPayment($moLastPayPending->id, HeDate::todaySQL(true), 'Cancelled and changed period from ' . abs($casePlan) .
            ' to ' . $idNewPeriod, $user, false, STATUS_CANCEL_CHANGEPROD)
        ) {
            return false;
        }

        $moLastPayPending->isPeriodPayChange = $moPayNewPlan->idPeriodPay;
        $moLastPayPending->update(array('isPeriodPayChange'));

        // INSERT LOGS:
        $user->abaUserLogUserActivity->saveUserLogActivity('Payments', 'Has changeProduct from ' . $idOldProduct .
            ' to ' . $idNewProduct, 'ChangeProduct', $user->id);

        //DELETE payment_control_check:
        if (!$PayControlDraft->delete()) {
            $this->errorMessage = 'The draft payment could not be deleted. Check out payment ' . $PayControlDraft->id;
        }

        return $moPayNewPlan;
    }

    /**
     * @TODO
     *
     * @param AbaUser $moUser
     * @param null $dateStart
     * @param null $monthsPeriod
     *
     * @return array|bool
     */
    public function getDatesSubscription(AbaUser $moUser, $dateStart = null, $monthsPeriod = null, $packageSelected="") {

        $stProduct = explode("|", $packageSelected);

        if(!empty($dateStart) && !empty($monthsPeriod)) {

            $commProducts = new ProductsPricesCommon();
            if($commProducts->isPlanProduct($stProduct[0])) {

                //
                // $dateStart = HeDate::today(true);
                $sDateStart = explode(" ", trim($dateStart));

                if(!isset($sDateStart[1])) {
                    $dateStart = $dateStart . " 00:00:00";
                }

//                $dateNow =  HeDate::today(true);
//                $dateDiff = HeDate::getDifferenceDateSQL($dateStart, $dateNow, (3600*24));
//
//                if($dateDiff >= 0) {
//                    $dateStart = HeDate::getDateTimeAdd($dateStart, (-$dateDiff), HeDate::DAY_SECS);
//                }
            }

            // if we recieve a date to apply we simulate the pending payment dates
            $dateToPay = HeDate::getDateAdd($dateStart, 0, $monthsPeriod);
        }
        else {
            $moPay =        new Payment();
            $moPendingPay = $moPay->getLastPayPendingByUserId($moUser->getId());
            $dateToPay =    $moPendingPay->dateToPay;
            $moSuccessPay = $moPay->getLastSuccessPaymentNoFree($moUser->getId());
            $dateStart =    $moSuccessPay->dateStartTransaction;

//@TODO !?! SIEMPRE ES FALSE

            if($moUser->expirationDate != $dateToPay) {
                return false;
            }
        }

        return array(
            'dateStartSubscription' =>  $dateStart, // moSuccessPay->dateStart
            'dateEndSubscription' =>    $dateToPay, // moPendingPay->dateToPay
            'dateToPay' =>              $dateToPay, // moPendingPay->dateToPay
        );
    }


    /**#5087
     *
     * @param AbaUser $user
     * @param PaymentControlCheck $PayControlDraft
     *
     * @return bool
     */
    public function cancelPendingPayment(AbaUser $user, PaymentControlCheck $PayControlDraft, $idPartner=null) {

        $moLastPayPending = new Payment();
        if (!$moLastPayPending->getLastPayPendingByUserId($user->id)) {
            $this->errorMessage = 'There is no active subscription, there is no PENDING PAYMENT.';
            return false;
        }

        // DETERMINE if it is downgrade or upgrade, compare products.idPeriodPay
        $idNewPeriod = intval($PayControlDraft->idPeriodPay);
        $idOldPeriod = intval($moLastPayPending->idPeriodPay);

        if ($idNewPeriod == $idOldPeriod) {
            $casePlan = $idOldPeriod;
        } else {
            if ($idNewPeriod > $idOldPeriod) {
                // Upgrade
                $casePlan = $idOldPeriod;
            } else {
                // Downgrade
                $casePlan = -1 * $idOldPeriod;
            }
        }

        // Cancel ACTIVE payment:
        if (!$this->cancelPayment($moLastPayPending->id, HeDate::todaySQL(true), 'Cancelled and changed period from ' . abs($casePlan) . ' to ' . $idNewPeriod,
            $user, false, STATUS_CANCEL_CONTRACT_NEW_PLAN)) {
            return false;
        }

        //
        //#5818
        $stDataToUpdate =                       array('isPeriodPayChange');
        $moLastPayPending->isPeriodPayChange =  $PayControlDraft->idPeriodPay;

        if(is_numeric($idPartner)) {
            $stDataToUpdate =               array('isPeriodPayChange', 'idPartner');
            $moLastPayPending->idPartner =  $idPartner;
        }

        $moLastPayPending->update($stDataToUpdate);
    }

    //
    //@TODO @TODO @TODO @TODO @TODO
    //

    /**#5087
     *
     * @param AbaUser $user
     */
    public function createUserPromos(AbaUser $user, Payment $pyment) {

        $allSuccess =   true;
        $sPlan =        "P001";

        for($i = 0; $i < FAMILYPLAN_NUM_USERS_360; $i++) {

            $oAbaUserPromos = new AbaUserPromos();

            $oAbaUserPromos->userId =       $user->id;
            $oAbaUserPromos->idProduct =    $pyment->idProduct;
//            $oAbaUserPromos->idPromoCode =  HeAbaSHA::generateIdPromo($user, $sPlan, $i);
            $oAbaUserPromos->idPromoCode =  "";
            $oAbaUserPromos->used =         0;

            if(!$oAbaUserPromos->insertUserPromo()) {
                $allSuccess = false;
            }
        }

        $res = $user->abaUserLogUserActivity->saveUserLogActivity( AbaUserPromos::getTableName(), "New family plan users created for user id => " . $user->id . (!$allSuccess ? " EITH ERRORS " : ""), "Payment Family Plan");
    }

    /**
     * @param $userId
     *
     * @return bool
     */
    public function userHasActiveFamilyPlan( $userId ) {
        $oPayment = new Payment();
        return $oPayment->userHasActiveFamilyPlan($userId);
    }

}
