<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abaenglish
 * Date: 19/11/12
 * Time: 12:40
 *  To change this template use File | Settings | File Templates.
 */
 /*
 * amount=24.90 ; initial_payment_amount=0.00 ; profile_status=Expired ; payer_id=X3GPAK24BSJPJ ; product_type=1 ; ipn_track_id=a2b81ca23511 ;
 * outstanding_balance=0.00 ; shipping=0.00 ; charset=windows-1252 ; period_type= Regular ; payment_gross= ;
 * currency_code=EUR ; verify_sign=AFcWxV21C7fd0v3bYYYRCpSSRl31AePnG2LnCLcdlwJ0krhmHuUz-wXs ; payment_cycle=Monthly ;
 * txn_type=recurring_payment ; receiver_id=38FJ47NHMEL38 ; payment_fee= ; mc_currency=EUR ; transaction_subject=ABA recurring payment ;
 * protection_eligibility=Ineligible ; payer_status=unverified ; first_name=LILIAN ; product_name=ABA recurring payment ;
 * amount_per_cycle=24.90 ; mc_gross=24.90 ; payment_date=04:09:13 Feb 11, 2013 PST ; rp_invoice_id=usernamePP=lpfernandes@oi.com.brx%x%producto=165 ;
 * payment_status=Completed ; business=payments@abaenglish.com ; last_name=FERNANDES ; txn_id=0NK27925S3798072Y ; mc_fee=1.57 ; time_created=05:26:30 Sep 11, 2012 PDT ;
 * resend=true ; payment_type=instant ; notify_version=3.7 ; recurring_payment_id=I-01LB1B6012RC ; payer_email=lpfernandes@oi.com.br ;
 * receiver_email=payments@abaenglish.com ; next_payment_date=N/A ; tax=0.00 ; residence_country=BR
 */
class IpnPaypalListenerService implements IAbaService
{
    const KEY_COMMAND = 'cmd=_notify-validate';

    const PP_COMPLETED = 'completed';
    const PP_FAILED = 'failed';
    const PP_DENIED = 'denied';
    const PP_ACTIVE = 'active';
    const PP_VERIFIED = 'verified';
    const PP_REFUNDED = 'refunded';
    const PP_REVERSED = 'reversed';
    const PP_PENDING = 'pending';
    const USER_NOT_FOUND = "NOT_USER_FOUND";
    const PAY_NOT_FOUND = "NOT_PAYMENT_FOUND";

    private $dateStart;
    private $errorMessage ;
    private $successMessage ;

    private $operation;
    private $userEmail; // Aba English subscription User name
    private $payerEmail; // User account from Paypal that pays this subscription
    private $profileId; // Subscription profile Id
    /* @var $moUser AbaUser */
    private $moUser; // Current user affected by this IPN notification.
    private $ipnPaySuppOrderId; // Invoice Id (custom field / rp_invoice)
    /* @var $moIpnLog PayPalLogIpn */
    private $moIpnLog;

    private static $aFieldsExpected = array('item_name',
        'address_city',
        'address_country',
        'address_state',
        'address_status',
        'address_street',
        'address_zip',
        'amount',
        'amount_per_cycle',
        'amount1',
        'amount2',
        'amount3',
        'auction_buyer_id',
        'auction_closing_date',
        'auction_multi_item',
        'business',
        'currency_code',
        'custom',
        'exchange_rate',
        'first_name',
        'for_auction',
        'for_auction',
        'initial_payment_amount',
        'invoice',
        'item_number',
        'last_name',
        'mc_amount1',
        'mc_amount2',
        'mc_amount3',
        'mc_currency',
        'mc_fee',
        'mc_gross',
        'memo',
        'next_payment_date',
        'notify_version',
        'num_cart_items',
        'option_name1',
        'option_name2',
        'option_selection1',
        'option_selection2',
        'outstanding_balance',
        'parent_txn_id',
        'password',
        'payer_business_name',
        'payer_email',
        'payer_id',
        'payer_status',
        'payment_cycle',
        'payment_date',
        'payment_fee',
        'payment_gross',
        'payment_status',
        'payment_type',
        'pending_reason',
        'period_type',
        'period1',
        'period2',
        'period3',
        'product_name',
        'product_type',
        'profile_status',
        'quantity',
        'reason_code',
        'reattempt',
        'receiver_email',
        'receiver_id',
        'recur_times',
        'recurring',
        'recurring_payment_id',
        'retry_at',
        'rp_invoice_id',
        'settle_amount',
        'settle_currency',
        'subscr_date',
        'subscr_effective',
        'subscr_id',
        'tax',
        'time_created',
        'txn_id',
        'txn_type',
        'username',
        'verify_sign');

    private static $aRelevantFields = array(
        'custom',  // For operations -cart- it is the invoice.
        'amount',
        'amount1',
        'amount2',
        'amount3',
        'currency_code',
        'mc_amount1',
        'mc_amount2',
        'mc_amount3',
        'mc_currency',
        'mc_gross', // For operation -cart-, it is the [payment.amountPrice]
        "mc_gross_1",
        'txn_id',
        'password',
        'payer_email',
        'payment_cycle',
        "payment_date" ,
        "payment_type" ,
        "payment_status", // For -cart- operations they tell us COMPLETED, PENDING, FAILED, eTC. [payment.status]
        'payer_status', // Is a validation field for -recurring_payment_profile_created-  VALUE=VERIFIED
        'period1',
        'period2',
        'period3',
        'profile_status',// Is a validation field for -recurring_payment_profile_created-  VALUE=VERIFIED
        'reattempt',
        'receiver_email',
        'recur_times',
        'recurring',
        'recurring_payment_id', // It must be coincident with field [payments.paySuppProfExtId], -recurring_payment_profile_created-
        'retry_at',
        'rp_invoice_id', // Is [custom] field(INVOICE, product and username) in -recurring_payment_profile_created-
        'subscr_date',
        'subscr_effective',
        'subscr_id',
        'time_created',
        'txn_type', // KEY field for all transactions
        'username');

    public function __construct()
    {
        $this->dateStart = HeDate::todaySQL(true);
    }

    public function getLastError()
    {
        return $this->errorMessage ;
    }

    /**
     * @param array $aPostValues
     *
     * @return bool|string
     */
    public function processRequestPayPal($aPostValues)
    {
        $logIPNId = $this->saveRequestIPN($aPostValues);
        $strPostParsedFormFields = $this->parsePostFields($aPostValues);
        if (!$strPostParsedFormFields) {
            $this->errorMessage = "ABA: Fields sent by PayPal couldn't be parsed or collected.";
            return false;
        }
        // From all fields that PayPal send to us we are only interested in a few:
        $aPaypalInfo = $this->extractRelevantFields($aPostValues);
        if (!$aPaypalInfo) {
            $this->errorMessage = " ABA: function extractRelevantFields  not all fields expected were received. ".
                " Function extractRelevantFields failed. ".
                " Transaction type= ".$aPostValues['txn_type']." Invoice=".$aPostValues['custom'];
            return false;
        }
        $isAuthentic = $this->checkAuthenticity($aPaypalInfo);
        if (is_string($isAuthentic)) {
            $this->operation = $this->checkUpOperation($aPaypalInfo);
            // It is a notification for a really old payment. Neither the email, nor the profile
            // has been found anywhere:
            if ($isAuthentic==self::PAY_NOT_FOUND) {
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_LOGIC_H.": UNKNOWN/MISSPELLED INVOICE, PAYMENT NOT FOUND ".
                    $aPaypalInfo["usernamePP"]." from IPN request(".$this->operation.") received",
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    "Check the table of logs IPN, the id is ".$logIPNId.". The operation is ".$this->operation.
                    ".<br/>Check thoroughly the field custom(invoice or rp_invoice or custom),".
                    " the invoice was not in our PAYMENTS LIST.".
                    ".<br/>Maybe is a case of an old subscription."
                );
                return false;
            } elseif ($isAuthentic==self::USER_NOT_FOUND) {
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_LOGIC_H.": UNKNOWN/MISSPELLED INVOICE field, USER NOT EVEN FOUND.".
                    $aPaypalInfo["usernamePP"]." from IPN request(".$this->operation.") received",
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    "Check the table of logs IPN, the id is ".$logIPNId.". The operation is ".$this->operation.
                    ".<br/>Check thoroughly the field custom(invoice or rp_invoice or custom),".
                    " the user was not found, maybe the e-mail has changed."
                );
                return false;
            }
        }
        // We send confirmation of RECEPTION to PAYPAL, they should reply VERIFIED
        $resp = $this->replyAuthenticity($strPostParsedFormFields);
        if ($resp == 'INVALID' || !$resp) {
            $this->errorMessage = " ABA: PayPal replied in the authenticity message that our confirmation message ".
                " was not well built: " . $this->errorMessage;
            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_UNKNOWN_H.": IPN verification message replied to PAYPAL has replied -INVALID-",
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                $this->errorMessage." Operation type=".$aPostValues['txn_type'].", log IPN=".$logIPNId
            );
            return false;
        } else {
            $retProc = true;
            $isAutomatized = true;
            // As long as the field txn_type is probably not informed, we analyse several fields to find out which
            // operation is being notified
            $this->operation = $this->checkUpOperation($aPaypalInfo);
            switch (strtolower(trim($this->operation)))
            {
                case 'cart':
                        // notification only, it does not trigger any process in the campus payment actions
                    if (strtolower(trim($aPaypalInfo["payment_status"]))!==self::PP_COMPLETED) {
                            $retProc = true;
                        if (strtolower(trim($aPaypalInfo["payment_status"]))!==self::PP_PENDING) {
                            HeLogger::sendLog(
                              HeLogger::PREFIX_ERR_LOGIC_L.": PAYPAL, -CART- IPN, EXPRESS CHECKOUT ".
                                strtoupper($aPaypalInfo["payment_status"])." USER PROBABLY HAS NOT PAID THROUGH PAYPAL",
                                HeLogger::IT_BUGS,
                                HeLogger::CRITICAL,
                                " Payment considered successful PayPAL suddenly told us is [ ".
                                $aPaypalInfo["payment_status"]." ] User and Invoice= ".
                                $aPostValues['custom'].". Recommend to turn him FREE user."
                            );
                                $this->processCartFailedIPN($aPaypalInfo);
                                $retProc = false;
                        }
                    }
                    break;

                case 'recurring_payment_profile_created':
                    if (strtolower(trim($aPaypalInfo["profile_status"]))!==self::PP_ACTIVE) {
                        HeLogger::sendLog(
                            HeLogger::PREFIX_NOTIF_H . ": PAYPAL, -RECURRING PAYMENT PROFILE-, ".
                            "NOTIFICATION OF NOT ACCEPTANCE THROUGH IPN",
                            HeLogger::IT_BUGS,
                            HeLogger::CRITICAL,
                            " Subscription profile considered successful PayPAL suddenly told us is ".
                            $aPaypalInfo["profile_status"] . " User and Invoice= ".$aPaypalInfo['rp_invoice_id']
                        );
                        $retProc = true;
                    }
                    break;

                case 'recurring_payment_profile_cancel':
                    $retProc = $this->processProfileCancellationIPN($aPaypalInfo);
                    if (!$retProc) {
                        HeLogger::sendLog(
                            HeLogger::PREFIX_ERR_UNKNOWN_H . ": PAYPAL, -RECURRING PAYMENT PROFILE CANCELLED-, ".
                            "NOTIFICATION OF CANCELLATION THROUGH IPN",
                            HeLogger::IT_BUGS,
                            HeLogger::CRITICAL,
                            " Subscription profile suddenly cancelled " . $aPaypalInfo["profile_status"] .
                            " User and Invoice= " . $this->ipnPaySuppOrderId .
                            " NOT ABLE TO PROCESS IT AUTOMATICALLY, because : " . $this->errorMessage .
                            "</br>" . " Check Log IPN " . $logIPNId
                        );
                        $retProc = true;
                    }
                    break;

                case 'recurring_payment_expired':
                case 'recurring_payment_failed':
                case 'recurring_payment_suspended':
                case 'recurring_payment_suspended_due_to_max_failed':
                case 'recurring_payment_suspended_due_to_max_failed_payment':
                    $retProc = $this->processProfileFailure($aPaypalInfo);
                    if (!$retProc) {
                        HeLogger::sendLog(
                            HeLogger::PREFIX_ERR_UNKNOWN_H .
                            ": PAYPAL, RECURRING PROFILE EXPIRED/FAILED/SUSPENDED-, NOTIFICATION IPN " . $logIPNId,
                            HeLogger::IT_BUGS,
                            HeLogger::CRITICAL,
                            " Subscription profile suddenly stopped operation = " . $this->operation . ", " .
                            $aPaypalInfo["profile_status"] . " User and Invoice= " . $aPaypalInfo['rp_invoice_id'] .
                            " NOT ABLE TO PROCESS IT AUTOMATICALLY, because : </br>" . $this->errorMessage .
                            "</br>" . " Check Log IPN " . $logIPNId
                        );
                        $retProc = true;
                    }
                    break;
                case 'recurring_payment':
                    if (trim(strtolower($aPaypalInfo["payment_status"])) !== self::PP_COMPLETED) {
                        HeLogger::sendLog(
                            HeLogger::PREFIX_NOTIF_H .
                            ": PAYPAL, -RECURRING PAYMENT-, NOTIFICATION OF CANCELLATION OR FAILURE THROUGH IPN",
                            HeLogger::IT_BUGS,
                            HeLogger::CRITICAL,
                            "Name -processRecurringPayIPN- informs us that payment_status= " .
                            $aPaypalInfo["payment_status"] . " User and Invoice= " . $aPaypalInfo['rp_invoice_id']
                        );
                        $retProc = true;
                    }
                    $retProc = $retProc && $this->processRecurringPayIPN($aPaypalInfo);
                    if (!$retProc) {
                        $this->errorMessage = " Name -processRecurringPayIPN- failed updating values for transaction ".
                            " type " . $aPaypalInfo["txn_type"] . " ; specifica message=" . $this->errorMessage;
                        $retProc = false;
                    }
                    break;
                case 'subscr_payment':
                    $retProc = $this->processRecurringPayIPN($aPaypalInfo, "playPayPalPayment");
                    break;

                case 'send_money':

                    HeLogger::sendLog(HeLogger::PREFIX_NOTIF_L.": PAYPAL, operation SEND_MONEY received in our account. Check it our in paypal.com please.",
                      HeLogger::FINANCE, HeLogger::INFO,
                      " We have apparently received a money transaction in our PAYPAL account. Probably from a GROUPON or PARTNER and it would be of significant amount.".
                      ". <br/>PayPal sent these values: ".implode(" ; ", $aPostValues).
                      " <br />For more information check log with Id=".$logIPNId." (Ask IT department)");

                case 'recurring_payment_skipped':
                    // On 2014-08-04, we decide to not notify anymore this operation, we just simply ignore them.
                    // Later we can decide to extend the period of the user to PREMIUM, right now we, by default,
                    // give away 15 days to PAYPAL users.
                    $isAutomatized = false;
                    $retProc = true;
                    break;
                    break;
                case 'recurring_payment_refund(**)':
                case 'recurring_payment_reversed(**)':
                case 'canceled_reversal(**)':
                case 'cart_reversed(**)':
                case 'cart_refund(**)':
                case 'unknown(**)':
                case 'new_case':
                case 'adjustment':
                default:
                HeLogger::sendLog(HeLogger::PREFIX_NOTIF_L.": PAYPAL, operation from paypal not expected or NOT AUTOMATIZED, it might be ".$this->operation,
                    HeLogger::PAYMENTS, HeLogger::CRITICAL,
                    "Recommend a review or advise from SUPPORT DPT. on this notification from PAYPAL. We have not automatized this operation ".$this->operation.
                    ". <br/>PayPal sent these values: ".implode(" ; ", $aPostValues).
                    " <br />For more information check log with Id=".$logIPNId." (Ask IT department)");
                    $isAutomatized = false;
                    $retProc = false;
                    break;
            }

            if( !$retProc && $isAutomatized ) {
                HeLogger::sendLog(HeLogger::PREFIX_ERR_UNKNOWN_H.": PAYPAL CALL RECEIVED AND PROCESSED WITH ERRORS, OPERATION = ".$this->operation,
                  HeLogger::PAYMENTS,HeLogger::CRITICAL,
                  " POST FIELDS SENT from PayPal= ".implode(" ; ", $aPaypalInfo).". These requests values are available in logs in database as well.");
            }

            $this->successMessage = ' transaction type is ' . $aPaypalInfo["txn_type"] .
                '(' . strtolower(trim($this->operation)) . ') for user ' . $aPaypalInfo["usernamePP"] .
                ' received successfully.';
            return $retProc;
        }

    }

    /**
     * Returns only the relevant fields important to negotiate with PAYPAL
     * IT DETECTS AND EXTRACTS EMAIL AND PRODUCT OF THE TRANSACTION.++++++++++++++++++++
     * @param $aPostValues Array with all fields sent by PAyPAl
     *
     * @return array|bool
     */
    public  function extractRelevantFields( $aPostValues )
    {
        $aIntersected =  array_intersect_key(
            $aPostValues,
            array_combine(self::$aRelevantFields, self::$aRelevantFields)
        );
        $invoiceFieldName = "custom";
        if (!array_key_exists("custom",$aIntersected)) {
            $invoiceFieldName = "rp_invoice_id";
        }
        $this->ipnPaySuppOrderId = $aIntersected[$invoiceFieldName];
        $this->payerEmail = "";
        if (!array_key_exists("payer_email", $aIntersected)) {
            $this->payerEmail = $aIntersected["payer_email"];
        }
        $this->profileId = "";
        if (array_key_exists("recurring_payment_id", $aIntersected)) {
            $this->profileId = $aIntersected["recurring_payment_id"];
        }
        // "usernamePP=".Yii::app()->user->getEmail()."&amp;producto=".$idProduct;
        // Example: usernamePP=ecole.votuquy@free.fr&amp;producto=73_360
        if (strpos($aIntersected[$invoiceFieldName], "&")>=1 && strpos($aIntersected[$invoiceFieldName], "@")>=1) {
            if (strpos($aIntersected[$invoiceFieldName], "&amp;")>=1) {
                $aCustom = explode("&amp;", $aIntersected[$invoiceFieldName]);
            } else {
                $aCustom = explode("&", $aIntersected[$invoiceFieldName]);
            }
            $aUser = explode("=", $aCustom[0]);
            $aProd = explode("=", $aCustom[1]);
            $aIntersected["usernamePP"] = $aUser[1];
            $aIntersected["producto"] = $aProd[1];
            // Due to an error during the period between 30/01/2013 and 14/02/2013, some invoices are wrong spelled.
            if (count($aUser)>2) {
                $aIntersected["usernamePP"] = $aUser[2];
                $aIntersected[$invoiceFieldName] = "usernamePP=".$aIntersected["usernamePP"]."&"."producto=" .
                    $aIntersected["producto"] = $aProd[1];
            }
            $this->userEmail = $aIntersected["usernamePP"];
            return $aIntersected;
        } elseif (strpos($aIntersected[$invoiceFieldName], "username")!==false  &&
            strpos($aIntersected[$invoiceFieldName], "product")!==false) {
            $aCustom = explode(PAY_PAL_SEPARATOR, $aIntersected[$invoiceFieldName]);
            $aUser = explode("=", $aCustom[0]);
            $aProd = explode("=", $aCustom[1]);
            //----
            $aIntersected["usernamePP"] = $aUser[1];
            $aIntersected["producto"] = $aProd[1];
            $this->userEmail = $aIntersected["usernamePP"];
            return $aIntersected;
        } else {
            $this->userEmail = "";
            return false;
        }
    }

    /** Returns the IPN operation name. There are some
     * that we have to make it up based on several fields bcoz
     * PAYPAL folks are a bunch of idiots and they are not a very
     * structured minded people.
     * @param $aPaypalInfo
     *
     * @return string
     */
    private function checkUpOperation($aPaypalInfo)
    {
        if (array_key_exists("txn_type", $aPaypalInfo)) {
            return $aPaypalInfo["txn_type"];
        }

        if (array_key_exists("payment_status", $aPaypalInfo) &&
            array_key_exists("recurring_payment_id", $aPaypalInfo) &&
            array_key_exists("rp_invoice_id", $aPaypalInfo)) {
            if (strtolower($aPaypalInfo["payment_status"])==self::PP_REFUNDED) {
                return "recurring_payment_refund(**)";
            }
        }
        if (array_key_exists("profile_status", $aPaypalInfo) &&
            array_key_exists("payment_status", $aPaypalInfo) &&
            array_key_exists("rp_invoice_id", $aPaypalInfo) &&
            array_key_exists("recurring_payment_id", $aPaypalInfo)) {
            if (strtolower($aPaypalInfo["payment_status"])== self::PP_REVERSED) {
                return "recurring_payment_reversed(**)";
            }
        }
        if (array_key_exists("payment_status", $aPaypalInfo) &&
           array_key_exists("custom", $aPaypalInfo)) {
            if (strtolower($aPaypalInfo["payment_status"])== "canceled_reversal") {
                return "canceled_reversal(**)";
            }
            if (strtolower($aPaypalInfo["payment_status"])== "reversed") {
                return "cart_reversed(**)";
            }
            if (strtolower($aPaypalInfo['payment_type'])=='instant' &&
                strtolower($aPaypalInfo["payment_status"])==self::PP_REFUNDED) {
                return "cart_refund(**)";
            }
        }
        return "unknown(**)";
    }

    /**
     * @param        $aPaypalInfo
     * @param string $operation
     *
     * @return bool
     */
    private function processRecurringPayIPN($aPaypalInfo, $operation = 'recurring_payment')
    {
        // @TODO Check more details about the recurring payment information like Amount, currency, next Payment date:
        // To avoid cases of renewal of newer payments because of cases of suspended profiles:
        // FIELDS: payer_email, payment_date, next_payment_date, recurring_payment_id,   mc_gross, currency_code
        $comRecurrentPay = new RecurrentPayCommon();
        $res = $comRecurrentPay->renewPaymentByPaySuppOrderId(
            $aPaypalInfo["usernamePP"],
            $aPaypalInfo["rp_invoice_id"],
            $this->dateStart,
            $operation,
            PAY_SUPPLIER_PAYPAL,
            null,
            $aPaypalInfo
        );
        if (!$res) {
            $this->errorMessage = $comRecurrentPay->getErrorMessage();
            HeLogger::sendLog(HeLogger::PREFIX_ERR_UNKNOWN_H . ": Recurring Payment from Paypal with ERRORS",
              HeLogger::PAYMENTS, HeLogger::CRITICAL,
              "Recurring payment received from PayPal but processed with errors for user " . $aPaypalInfo["usernamePP"] . ": " . $this->errorMessage);

            return false;
        } else {
            return true;
        }
    }

    /**
     * @param $aPaypalInfo
     * @return bool
     */
    private function processCartFailedIPN( /** @noinspection PhpUnusedParameterInspection */$aPaypalInfo )
    {
        // @todo We should update payment to FAIL. But PAYPAL did not clearly specify how this transaction is delimited , so we do not want to automatize this yet.
        return true;

    }

    /**
     * @param $aPaypalInfo
     *
     * @return bool
     */
    private function processProfileCancellationIPN( $aPaypalInfo )
    {
        $moUser = $this->getUserByInvoiceId($aPaypalInfo);
        if (!$moUser){
            $this->errorMessage = " User ".$aPaypalInfo["usernamePP"]." not found to be able to process cancellation.";
            return false;
        }

        $moPayment = $this->getPaymentByIPNReference($moUser, $this->ipnPaySuppOrderId, $this->profileId, PAY_PENDING, true);
        if (!$moPayment){
            $moPayment = new Payment();
            if (!$moPayment->getLastByPaySuppExtProfId( $moUser->id, PAY_SUPPLIER_PAYPAL, $this->profileId)){
                $moPayment->id = "NONE";
                $moPayment->status = self::PAY_NOT_FOUND;
            }
        }

        // Double Check security, see days passed between notification and last operation on payment.
        if (HeDate::validDateSQL($moPayment->dateEndTransaction)) {
            $diffDays = HeDate::getDifferenceDateSQL( $moPayment->dateToPay, $this->dateStart, HeDate::DAY_SECS);
            if (abs($diffDays)>2) {
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_LOGIC_L." Last Payment Paypal for profile ".$this->profileId.
                    " delayed several days.",
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    " Notification received on day ".$this->dateStart." from Paypal regarding payment on date " .
                    $moPayment->dateEndTransaction
                );
            }
        }

        /*++++ Scenarios that we may encounter when cancelling a user subscription:++++++++*/
        $commPayment = new RecurrentPayCommon();
        // Case PREMIUM´s
        if( intval($moUser->userType) == PREMIUM){
            switch ($moPayment->status){
                case PAY_PENDING:
                    if (!$commPayment->cancelPayment($moPayment->id, $this->dateStart,
                                                "IPN recurring_payment_profile_cancel", $moUser,
                                                PAY_CANCEL_USER, STATUS_CANCEL_IPN)){
                        $this->errorMessage = "Payment PENDING could not be cancelled in ".
                                        "IPN recurring_payemnt_profile_cancel auto process";
                        return false;
                    }
                    return true;
                    break;
                case PAY_CANCEL:
                case PAY_FAIL:
                    HeLogger::sendLog(HeAbaMail::PREFIX_NOTIF_INTRA . " Review paypal notification " .
                        $this->operation . " for " . $moUser->email . ", already is CANCEL/FAIL",
                        HeLogger::SUPPORT, HeLogger::INFO,
                        "Paypal says that user " . $moUser->email . "(PREMIUM) just cancel his recurring profile " .
                        $this->profileId . ". In our Intranet was already cancelled in payment id " .
                        $moPayment->id . " on date " . $moPayment->dateEndTransaction);
                    return true;
                    break;
                case self::PAY_NOT_FOUND:
                    $this->errorMessage = "Payment PENDING NOT FOUND for this recurring profile(PREMIUM) ".$this->profileId.
                                        "(IPN recurring_payemnt_profile_cancel auto process)";
                    return false;
                    break;
                case PAY_SUCCESS:
                    $this->errorMessage = "Payment SUCCESS found instead of PENDING payment for this recurring profile(PREMIUM)".
                        $this->profileId."(IPN recurring_payemnt_profile_cancel auto process)";
                    return false;
                    break;
                default:
                    $this->errorMessage = " This case is unexpected: user type PREMIUM + payment status is ".
                        $moPayment->status." + and the profile Paypal is ".$this->profileId;
                    return false;
                    break;
            }

        } elseif( intval($moUser->userType) == FREE){
            switch ($moPayment->status){
                case PAY_CANCEL:
                case PAY_FAIL:
                    HeLogger::sendLog(HeAbaMail::PREFIX_NOTIF_INTRA." Review paypal notification ".
                      $this->operation." for ".$moUser->email."(FREE), already is CANCELLED/FAIL",
                      HeLogger::SUPPORT, HeLogger::INFO,
                      "Paypal says that user ".$moUser->email."(FREE) just cancel his recurring profile ".
                      $this->profileId.". In our Intranet was already cancelled in payment id ".
                      $moPayment->id." on date ".$moPayment->dateEndTransaction);
                    return true;
                    break;
                case PAY_SUCCESS:
                    // Send ERROR MAIL to review
                    $this->errorMessage = "Paypal says that user ".$moUser->email."(FREE) just cancel his recurring profile ".
                        $this->profileId.". In our Intranet the last payment is SUCCESS in payment id ".
                        $moPayment->id." on date ".$moPayment->dateEndTransaction;
                    return false;
                    break;
                case self::PAY_NOT_FOUND:
                    $this->errorMessage = "Payment PENDING NOT FOUND for this recurring profile(FREE) ".$this->profileId.
                        "(IPN recurring_payemnt_profile_cancel auto process)";
                    return false;
                    break;
                default:
                    $this->errorMessage = " This case is unexpected: user type FREE + payment status is ".
                        $moPayment->status." + and the profile Paypal is ".$this->profileId;
                    return false;
                    break;
            }
        } else{
            // Maybe a teacher? a DELETED USER?
            $this->errorMessage = "For recurring_profile_cancel Campus did not find any matching scenario. ".
                "We are not able to cancel user ".$moUser->email." user type is ".$moUser->userType;
            return false;
        }
    }

    /** It processes the operations:  'recurring_payment_expired', 'recurring_payment_failed',
     *     'recurring_payment_suspended_due_to_max_failed'
     * @param $aPaypalInfo
     *
     * @return bool
     */
    private function processProfileFailure( $aPaypalInfo )
    {
        $moUser = $this->getUserByInvoiceId($aPaypalInfo);
        if (!$moUser){
            $this->errorMessage = " User ".$aPaypalInfo["usernamePP"]." not found to be able to process ".
                    $this->operation. ". We DID NOT SEND AUTO-CANCELLATION TO PAYPAL.";
            return false;
        }

        $moPayment = $this->getPaymentByIPNReference($moUser, $this->ipnPaySuppOrderId, $this->profileId, PAY_PENDING, true);
        if (!$moPayment){
            $moPayment = new Payment();
            if (!$moPayment->getLastByPaySuppExtProfId( $moUser->id, PAY_SUPPLIER_PAYPAL, $this->profileId)){
                $moPayment->id = "NONE";
                $moPayment->status = self::PAY_NOT_FOUND;
            }
        }

        /* @var $paySupplier PaySupplierPayPal */
        $paySupplier = PaymentFactory::createPayment($moPayment, PAY_SUPPLIER_PAYPAL);
        // @TODO send cancellation to PAYPAL:
        $moUserCredit = new AbaUserCreditForms();
        if (!$moUserCredit->getUserCreditFormById($moPayment->idUserCreditForm)){
            $this->errorMessage = "We did not find any credit form associated with this profile.";
            return false;
        }

        $commRecurring = new RecurrentPayCommon();
        if (!$commRecurring->failRecurring($moPayment->id, $this->dateStart, "IPN ".$this->operation, $moUser, PAY_CANCEL_FAILED_RENEW)){
            $this->errorMessage = " Marking the payment ".$moPayment->id." as FAILED has failed. Review case.";
            return false;
        }

        if (!$paySupplier->runCancelRecurring($moPayment, $moUserCredit)) {
            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_LOGIC_H." Send Auto Cancellation to Paypal has failed. User ".$moUser->email,
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                " Forcing the auto-cacenllation with runCancelRecurring ".
                " has failed, check subscription case ".$this->profileId. "."
            );
        }
        return true;
    }

    /**
     * @param $aPostFields
     *
     * @return AbaUser|bool
     */
    private function getUserByInvoiceId($aPostFields)
    {
        $moUser = new AbaUser();
        if (!$moUser->getUserByEmail($aPostFields["usernamePP"])) {
            $moCreditForms = new AbaUserCreditForms();
            // Search e-mail in the userCreditForms.cardName and get AbaUser()
            if (!$moCreditForms->getUserCreditFormByCardName($aPostFields["usernamePP"])) {
                return false;
            } else {
                $moUser = new AbaUser();
                if (!$moUser->getUserById($moCreditForms->userId)) {
                    return false;
                }
            }
        }

        $this->moUser = $moUser;
        return $moUser;
    }

    /** Return the payment referenced by the IPN notification.
     * @param AbaUSer $moUser
     * @param $invoiceId
     * @param $profileId
     * @param string $status  If none indicated then any status.
     * @param bool $exactMatch If we have to check also invoice and profile.
     *
     * @return bool|Payment
     */
    private function getPaymentByIPNReference(AbaUSer $moUser, $invoiceId, $profileId, $status = "", $exactMatch=false)
    {
        $moPayment = new Payment();
        if (!$moPayment->getPaymentByPaySuppOrderId($moUser->email, $invoiceId, $status, false)) {
            // Search last payment by profile ID (recurring_payment_id)
            if (!empty($profileId) &&
                $moPayment->getLastByPaySuppExtProfId($moUser->id, PAY_SUPPLIER_PAYPAL, $profileId, $status)){
                    return $moPayment;
            }
            return false;
        } elseif ($exactMatch && !empty($profileId)){
            // Check profile:
            if ($moPayment->paySuppExtProfId != $profileId){
                return false;
            }
        }

        return $moPayment;
    }

    /** Checks if the IPN notification is really from any user in our database.
     *
     * @param $aPostFields
     * @return bool|string
     */
    private function checkAuthenticity($aPostFields)
    {
        $moUser = $this->getUserByInvoiceId($aPostFields);
        if (!$moUser) {
            return self::USER_NOT_FOUND;
        }

        // At this point we know is autentic and we ve got user name; so we update the user Id affected by this call:
        if ($this->moUser){
            $this->moIpnLog->userId = $this->moUser->id;
            $this->moIpnLog->update(array("userId"));
        }

        $moPayment = $this->getPaymentByIPNReference($moUser, $this->ipnPaySuppOrderId, $this->profileId);
        if (!$moPayment) {
            return self::PAY_NOT_FOUND;
        }

        return true;
    }


    /** We auto-reply to IPN PayPal in order to give authenticity to the callback.
     * Then back to process the details of the initial request.
     * @param $strParsedFields
     *
     * @return bool|string
     */
    private function replyAuthenticity( $strParsedFields )
    {
        //"http://prod2.abaenglish.com/wspostaba/simulatepaypalverification";
        $urlAbaPayPalWeb = Yii::app()->config->get("PAY_PAL_URL_IPN_SERVICE");
        $urlAbaPayPalWebPort = Yii::app()->config->get("PAY_PAL_URL_IPN_PORT");
        if($urlAbaPayPalWebPort=='' || is_null($urlAbaPayPalWebPort) ){
            $urlAbaPayPalWebPort = "443";
        }

        $header = "";
        if($urlAbaPayPalWeb=='abaenglish-local')
        {
            $header .= "POST /wspostaba/simulatepaypalverification HTTP/1.0\r\n";
        }
        else{
            $header .= "POST /cgi-bin/webscr HTTP/1.0\r\n";
        }
        $header .= "Host: ".$urlAbaPayPalWeb.":".$urlAbaPayPalWebPort."\r\n";
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";

        $response = HeComm::sendRawPostSocket(Yii::app()->config->get("PAY_PAL_URL_IPN_SERVICE"), $header, $strParsedFields, $urlAbaPayPalWebPort );
        if( is_string($response) )
        {
            switch( trim( strtoupper($response ) ) )
            {
                case 'FALSE':
                    return false;
                    break;
                case PAY_PAL_INVALID:
                    $this->errorMessage= " PAYPAL or simulator replied INVALID ";
                    return PAY_PAL_INVALID;
                    break;
                case PAY_PAL_VERIFIED:
                    $this->successMessage= " PAYPAL or simulator replied VERIFIED " ;
                    return PAY_PAL_VERIFIED;
                    break;
                default:
                    $this->errorMessage= " Simulador replied sth else = ".$response;
                    return false;
                    break;
            }
        }
        elseif(is_array($response))
        {
            return false;
        }
        return false;
    }


    private function parsePostFields( $postValues )
    {
        $listParsedFields = "";
        foreach ($postValues as $fieldName => $fieldValue)
        {
            $value = HeComm::encodeValuesHTTP( stripslashes($fieldValue) );
            $listParsedFields .= "&" . $fieldName . "=" . $value;
        }

        if($listParsedFields!=='')
        {
            $listParsedFields = self::KEY_COMMAND.$listParsedFields;
            return $listParsedFields;
        }

        return false;

    }

    /**
     * @param array $postValues
     *
     * @return bool
     */
    private function saveRequestIPN( $postValues)
    {
        try{
            $this->moIpnLog = new PayPalLogIpn();
            // `dateCreation`, l.`header`, l.`postFields`, l.`transactionType`, l.`paymentStatus
            $this->moIpnLog->dateCreation = HeDate::todaySQL(true);
            $Headers = Yii::app()->getRequest()->getBaseUrl(true);
            $this->moIpnLog->header = "  HTTP URI=".$_SERVER["REQUEST_URI"]." # ".$Headers;
                /*"  HTTP Header=".$_SERVER["REQUEST_METHOD"]." # ".
                "  HTTP URI=".$_SERVER["REQUEST_URI"]." # ".
                "  REMOTE HOST=".$_SERVER["REMOTE_ADDR"]." # ".
                "  REQUEST TIME=".$_SERVER["REQUEST_TIME"]." # ";*/
            $strPostValues = "";
            foreach( $postValues as $key=>$value)
                { $strPostValues .=" ; ".$key."=".utf8_encode($value);    }

            $this->moIpnLog->postFields = $strPostValues;
            $this->moIpnLog->transactionType = (array_key_exists( "txn_type", $postValues))? $postValues["txn_type"]: "";
            $this->moIpnLog->paymentStatus = (array_key_exists( "payment_status", $postValues))? $postValues["payment_status"]: "";
            $logIPNId = $this->moIpnLog->insertLog();
        }
        catch(Exception $efc){
            return false;
        }

        return $logIPNId;
    }

    public function getSuccessMessage()
    {
        return $this->successMessage;
    }
}
