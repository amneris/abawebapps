<?php
/**
 * Class PaySupplierAdyen
 */
class PaySupplierAdyen extends PaySupplierCommon
{
    const ADYEN_SOAP_REQUEST_TYPE_AUTHORISE =               'authorise';
    const ADYEN_SOAP_REQUEST_TYPE_RECURRING =               'recurring';
    const ADYEN_SOAP_REQUEST_TYPE_REFUND =                  'refund';
    const ADYEN_SOAP_REQUEST_TYPE_LIST_RECURRING_DETAILS =  'listRecurringDetails';
    const ADYEN_SOAP_REQUEST_TYPE_RECURRING_DISABLE =       'disable';

    // states
    const ADYEN_SOAP_SEND_TYPE_AUTHORIZATION_PAYMENT =      10;
    const ADYEN_SOAP_SEND_TYPE_AUTHORIZATION_RECURRING =    20;
    const ADYEN_SOAP_SEND_TYPE_REFUND =                     30;
    const ADYEN_SOAP_SEND_TYPE_LIST_RECURRING_DETAILS =     40;
    const ADYEN_SOAP_SEND_TYPE_RECURRING_DISABLE =          50;
    const ADYEN_SOAP_SEND_TYPE_RECONCILIATION_FILE =        60;

    // hpp states
    const ADYEN_TYPE_HPP_PAYMENT_AUTHORIZATION =            500;
    const ADYEN_TYPE_HPP_PAYMENT_GET_PAYMENT_METHODS =      570;

    //#ABAWEBAPPS-601
    const ADYEN_SOAP_ONECLICK_CONTRACT =                    "ONECLICK";
    const ADYEN_SOAP_RECURRING_CONTRACT =                   "RECURRING";
    const ADYEN_SOAP_ONECLICK_RECURRING_CONTRACT =          "ONECLICK,RECURRING";

    const ADYEN_SOAP_SHOPPER_INTERACTION_CONTAUTH =         "ContAuth";
    const ADYEN_SOAP_SHOPPER_INTERACTION_ECOMMERCE =        "Ecommerce";
    //#ABAWEBAPPS-601

    const ADYEN_SOAP_ERROR_CODE_REFUND =                    "ABA_REFUND";
    const ADYEN_SOAP_ERROR_CODE_LIST_RECURRING_DETAILS =    "ABA_LIST_RECURRING_DETAILS";
    const ADYEN_SOAP_ERROR_CODE_RECURRING_DISABLE =         "ABA_LIST_RECURRING_DISABLE";

    const ADYEN_REPORT_TYPE_PAYMENTS_ACCOUNTING =           "payments_accounting_report";

    const ADYEN_REPORT_PAYMENTS_ACCOUNTING_STATE_SUCCESS =  "SentForSettle";
    const ADYEN_REPORT_PAYMENTS_ACCOUNTING_STATE_REFUND =   "SentForRefund";
//    const ADYEN_REPORT_PAYMENTS_ACCOUNTING_STATE_SUCCESS =  "Settled";
//    const ADYEN_REPORT_PAYMENTS_ACCOUNTING_STATE_REFUND =   "Refunded";


    //
    //  The result of the payment. The possible values are Authorised, Refused, Error or Received (In case with a Dutch DirectDebit).
    const ADYEN_SOAP_RESULT_CODE_AUTHORISED =       'Authorised';
    const ADYEN_SOAP_RESULT_CODE_REFUSED =          'Refused';
    const ADYEN_SOAP_RESULT_CODE_ERROR =            'Error';
    const ADYEN_SOAP_RESULT_CODE_RECEIVED =         'Received';
    const ADYEN_SOAP_RESULT_SUCCESS_REFUND =        '[refund-received]';
    const ADYEN_SOAP_RESULT_SUCCESS_CANCEL =        '[detail-successfully-disabled]';
    const ADYEN_SOAP_RESULT_SUCCESS_ALL_CANCEL =    '[all-details-successfully-disabled]';
    const ADYEN_SOAP_REQUEST_XMLSCHEMA =            'http://www.w3.org/2001/XMLSchema';
    const ERROR_ABADB_LOGPAY =                      1002;
    const ERROR_XML_PARSE =                         1003;
    const ERROR_SOAP =                              1004;
    const ERROR_EMPRTY_USER =                       1005;
    const ADYEN_HTTP_RESULT_CODE_EMPTY_METHODS =    'NoPaymentMethods';

    //
    // HPP url result codes
    const ADYEN_HPP_RESULT_CODE_AUTHORISED =    'AUTHORISED';
    const ADYEN_HPP_RESULT_CODE_PENDING =       'PENDING';
    const ADYEN_HPP_RESULT_CODE_REFUSED =       'REFUSED';
    const ADYEN_HPP_RESULT_CODE_ERROR =         'ERROR';
    const ADYEN_HPP_RESULT_CODE_CANCELLED =     'CANCELLED';

    //
    // All payment methods
    const T_PREFIX = "adyen_payment_method_";

    const HPP_RECUPPING_PAYMENT_METHOD_DIRECTEBANKING =     'directEbanking';
    const HPP_RECUPPING_PAYMENT_METHOD_GIROPAY =            'giropay';
    const HPP_RECUPPING_PAYMENT_METHOD_IDEAL =              'ideal';
    const HPP_RECUPPING_PAYMENT_METHOD_SEPADIRECTDEBIT =    'sepadirectdebit';

    const HPP_RECUPPING_PAYMENT_METHOD_ALIPAY =             'alipay';
    const HPP_RECUPPING_PAYMENT_METHOD_BANKTRANSFER =       'bankTransfer';
    const HPP_RECUPPING_PAYMENT_METHOD_BANKTRANSFERIBAN =   'bankTransfer_IBAN';
    const HPP_RECUPPING_PAYMENT_METHOD_BANKTRANSFERNL =     'bankTransfer_NL';
    const HPP_RECUPPING_PAYMENT_METHOD_BANKRU =             'bank_ru';
    const HPP_RECUPPING_PAYMENT_METHOD_BOLETO =             'boleto';
    const HPP_RECUPPING_PAYMENT_METHOD_DIRECTDEBITNL =      'directdebit_NL';
    const HPP_RECUPPING_PAYMENT_METHOD_DOTPAY =             'dotpay';
    const HPP_RECUPPING_PAYMENT_METHOD_EBANKINGFI =         'ebanking_FI';
    const HPP_RECUPPING_PAYMENT_METHOD_ELV =                'elv';
    const HPP_RECUPPING_PAYMENT_METHOD_INTERAC =            'interac';
    const HPP_RECUPPING_PAYMENT_METHOD_ONLINERU =           'online_RU';
    const HPP_RECUPPING_PAYMENT_METHOD_TERMINALRU =         'terminal_RU';
    const HPP_RECUPPING_PAYMENT_METHOD_TRUSTLY =            'trustly';
    const HPP_RECUPPING_PAYMENT_METHOD_TRUSTPAY =           'trustpay';
    const HPP_RECUPPING_PAYMENT_METHOD_UNIONPAY =           'unionpay';
    const HPP_RECUPPING_PAYMENT_METHOD_WALLETRU =           'wallet_RU';

    const HPP_RECUPPING_PAYMENT_METHOD_AMEX =               'amex';
    const HPP_RECUPPING_PAYMENT_METHOD_BCMC =               'bcmc';
    const HPP_RECUPPING_PAYMENT_METHOD_CUP =                'cup';
    const HPP_RECUPPING_PAYMENT_METHOD_DINERS =             'diners';
    const HPP_RECUPPING_PAYMENT_METHOD_DISCOVER =           'discover';
    const HPP_RECUPPING_PAYMENT_METHOD_ELO =                'elo';
    const HPP_RECUPPING_PAYMENT_METHOD_JCB =                'jcb';
    const HPP_RECUPPING_PAYMENT_METHOD_MAESTRO =            'maestro';
    const HPP_RECUPPING_PAYMENT_METHOD_MC =                 'mc';
    const HPP_RECUPPING_PAYMENT_METHOD_VISA =               'visa';
    const HPP_RECUPPING_PAYMENT_METHOD_VISADANKORT =        'visadankort';
    const HPP_RECUPPING_PAYMENT_METHOD_VIAS =               'vias';
    const HPP_RECUPPING_PAYMENT_METHOD_MAESTROUK =          'maestrouk';
    const HPP_RECUPPING_PAYMENT_METHOD_SOLO =               'solo';
    const HPP_RECUPPING_PAYMENT_METHOD_LASER =              'laser';
    const HPP_RECUPPING_PAYMENT_METHOD_BIJCARD =            'bijcard';
    const HPP_RECUPPING_PAYMENT_METHOD_DANKORT =            'dankort';
    const HPP_RECUPPING_PAYMENT_METHOD_HIPERCARD =          'hipercard';
    const HPP_RECUPPING_PAYMENT_METHOD_UATP =               'uatp';
    const HPP_RECUPPING_PAYMENT_METHOD_CARTENBANCAIRE =     'cartebancaire';
    const HPP_RECUPPING_PAYMENT_METHOD_VISAALPHABANKBONUS = 'visaalphabankbonus';
    const HPP_RECUPPING_PAYMENT_METHOD_MCALPHABANKBONUS =   'mcalphabankbonus';
    const HPP_RECUPPING_PAYMENT_METHOD_QIWIWALLET =         'qiwiwallet';
    const HPP_RECUPPING_PAYMENT_METHOD_SAFETYPAY =          'safetypay';

    const HPP_RECUPPING_PAYMENT_METHOD_MANDRY_CLICKPAY =    'mandiri_clickpay';
    const HPP_RECUPPING_PAYMENT_METHOD_DOKU_WALLET =        'doku_wallet';

    const HPP_RECUPPING_PAYMENT_METHOD_OXXO =               'oxxo';
    const HPP_RECUPPING_PAYMENT_METHOD_SEVENELEVEN =        'seveneleven';


//    const HPP_MERCHANT_REFERENCE_PREFIX = 'SK:';
    const HPP_MERCHANT_REFERENCE_PREFIX = 'HPP:';

    protected $connectToGateway;
    protected $orderNumber;
    protected $errorCode;

    /* @var PayGatewayLogAdyenResRec $moLogAllpago */
    protected $moLogResponse;

    protected $arrayRequest;
    protected $recurringDetailReference;

    protected $csvResponse =    null;
    protected $fileReport =     '';
    protected $reportPath =     "";
    protected $infoPayment;

    protected $allowedMethods = array();

    protected $errorMsg = array
    (
    );

    const RECURRING_PAYMENT_TYPE_INITIAL = 1;
    const RECURRING_PAYMENT_TYPE_RECURRING = 2;

    /**
     * @return array
     */
    public static function getRecurringPaymentTypeData($iRecurringPaymentType) {

        $stTypeData = array(
          "contract" => self::ADYEN_SOAP_RECURRING_CONTRACT,
          "shopperInteraction" => self::ADYEN_SOAP_SHOPPER_INTERACTION_CONTAUTH,
        );

        switch($iRecurringPaymentType) {
            case self::RECURRING_PAYMENT_TYPE_INITIAL:

                $sRecurringInitialPaymentType = Yii::app()->config->get("PAY_ADYEN_RECURRING_INITIAL_PAYMENT_TYPE");
                $sRecurringInitialPaymentType = str_replace(" ", "", trim($sRecurringInitialPaymentType));

                $stTypeData["contract"] = $sRecurringInitialPaymentType;

                switch($sRecurringInitialPaymentType) {
                    case self::ADYEN_SOAP_ONECLICK_CONTRACT:
                    case self::ADYEN_SOAP_ONECLICK_RECURRING_CONTRACT:
                        $stTypeData["shopperInteraction"] = self::ADYEN_SOAP_SHOPPER_INTERACTION_ECOMMERCE;
                        break;
                    case self::ADYEN_SOAP_RECURRING_CONTRACT:
                        $stTypeData["shopperInteraction"] = self::ADYEN_SOAP_SHOPPER_INTERACTION_CONTAUTH;
                        break;
                }
                break;

            case self::RECURRING_PAYMENT_TYPE_RECURRING:

                $sRecurringRecurringPaymentType = Yii::app()->config->get("PAY_ADYEN_RECURRING_RECURRING_PAYMENT_TYPE");
                $sRecurringRecurringPaymentType = str_replace(" ", "", trim($sRecurringRecurringPaymentType));

                $stTypeData["contract"] = $sRecurringRecurringPaymentType;

                switch($sRecurringRecurringPaymentType) {
                    //! DONT WORK !
                    case self::ADYEN_SOAP_ONECLICK_CONTRACT:
                    case self::ADYEN_SOAP_ONECLICK_RECURRING_CONTRACT:
                        $stTypeData["shopperInteraction"] = self::ADYEN_SOAP_SHOPPER_INTERACTION_ECOMMERCE;
                        break;
                    //! WORK !
                    case self::ADYEN_SOAP_RECURRING_CONTRACT:
                        $stTypeData["shopperInteraction"] = self::ADYEN_SOAP_SHOPPER_INTERACTION_CONTAUTH;
                        break;
                }
                break;
        }

        return $stTypeData;
    }

    /**
     * @return array
     */
    public static function getRecurringHppPaymentMethods() {
        return array(
            self::HPP_RECUPPING_PAYMENT_METHOD_DIRECTEBANKING =>    self::HPP_RECUPPING_PAYMENT_METHOD_DIRECTEBANKING,
            self::HPP_RECUPPING_PAYMENT_METHOD_GIROPAY =>           self::HPP_RECUPPING_PAYMENT_METHOD_GIROPAY,
            self::HPP_RECUPPING_PAYMENT_METHOD_IDEAL =>             self::HPP_RECUPPING_PAYMENT_METHOD_IDEAL,
            self::HPP_RECUPPING_PAYMENT_METHOD_SEPADIRECTDEBIT =>   self::HPP_RECUPPING_PAYMENT_METHOD_SEPADIRECTDEBIT,
//            "cards" =>              "",           // * cards (with the exception of maestro and mister cash)
//            "paypal" =>             "PayPal",
        );
    }

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->arrayRequest =   array();
        $this->errorCode =      0;
        $this->currentElement = "";
        $this->fileReport =     self::ADYEN_REPORT_TYPE_PAYMENTS_ACCOUNTING;
    }

    /**
     * @return string
     */
    public function getFileReport() {
        return $this->fileReport;
    }

    /** Returns an array of integer as a key, and string as a value. Each of implementations for this definition class
     * should have at least one element within the array.
     *
     * @return array
     */
    public function getListOfCards()
    {
        return array(
            KIND_CC_VISA =>             Yii::t('mainApp', 'VISA_w'),
            KIND_CC_VISA_ELECTRON =>    Yii::t('mainApp', 'VISA_ELECTRON_w'),
            KIND_CC_MASTERCARD =>       Yii::t('mainApp', 'MASTERCARD_w'),
            KIND_CC_AMERICAN_EXPRESS => Yii::t('mainApp', 'AMERICAN_EXPRESS_w'),
            KIND_CC_DINERS_CLUB =>      Yii::t('mainApp', 'DINERS_CLUB_w'),
            KIND_CC_MAESTRO =>          Yii::t('mainApp', 'MAESTRO_w'),
            // !AK! - PENDIENTE !!!
        );
    }

    /**
     * @return int
     */
    public function getErrorCode() {
        return $this->errorCode;
    }

    /**
     * @return string
     */
    public function getErrorString () {
        if(isset($this->errorMsg[$this->errorCode])) {
            return $this->errorMsg[$this->errorCode];
        }
        return "ErrorCode ".$this->errorCode;
    }

    /**
     * @param $refusalReason
     *
     * @return bool
     */
    public function getDescByRefusalReason($refusalReason) {

        $aFaultCodes = array(
            "000" => "Unknown",
            "010" => "Not allowed",
            "100" => "No amount specified",
            "101" => "Invalid card number",
            "102" => "Unable to determine variant",
            "103" => "CVC is not the right length",
            "104" => "Billing address problem",
            "105" => "Invalid paRes from issuer",
            "106" => "This session was already used previously",
            "107" => "Recurring is not enabled",
            "108" => "Invalid bankaccount number",
            "109" => "Invalid variant",
            "110" => "BankDetails missing",
            "111" => "Invalid BankCountryCode specified",
            "112" => "This bank country is not supported",
            "113" => "No InvoiceLines provided",
            "114" => "Received a incorrect InvoiceLine",
            "115" => "Total amount is not the same as the sum of the lines",
            "116" => "Invalid date of birth",
            "117" => "Invalid billing addresss",
            "118" => "Invalid delivery address",
            "119" => "Invalid shopper name",
            "120" => "ShopperEmail is missing",
            "121" => "ShopperReference is missing",
            "122" => "PhoneNumber is missing",
            "123" => "The PhoneNumber should be mobile",
            "124" => "Invalid PhoneNumber",
            "125" => "Invalid recurring contract specified",
            "126" => "Bank Account or Bank Location Id not valid or missing",
            "127" => "Account holder missing",
            "128" => "Card Holder Missing",
            "129" => "Expiry Date Invalid",
            "130" => "Reference Missing",
            "131" => "Billing address problem (City)",
            "132" => "Billing address problem (Street)",
            "133" => "Billing address problem (HouseNumberOrName)",
            "134" => "Billing address problem (Country)",
            "135" => "Billing address problem (StateOrProvince)",
            "136" => "Failed to retrieve OpenInvoiceLines",
            "137" => "Invalid amount specified",
            "138" => "Unsupported currency specified",
            "139" => "Recurring requires shopperEmail and shopperReference",
            "140" => "Invalid expiryMonth[1..12] / expiryYear[>2000], or before now",
            "141" => "Invalid expiryMonth[1..12] / expiryYear[>2000]",
            "142" => "Bank Name or Bank Location not valid or missing",
            "143" => "Submitted total iDeal merchantReturnUrl length is {0}, but max size is {1} for this request",
            "144" => "Invalid startMonth[1..12] / startYear[>2000], or in the future",
            "145" => "Invalid issuer countrycode",
            "146" => "Invalid social security number",
            "147" => "Delivery address problem (City)",
            "148" => "Delivery address problem (Street)",
            "149" => "Delivery address problem (HouseNumberOrName)",
            "150" => "Delivery address problem (Country)",
            "151" => "Delivery address problem (StateOrProvince)",
            "152" => "Invalid number of installments",
            "153" => "Invalid CVC",
            "154" => "No additional data specified",
            "155" => "No acquirer specified",
            "156" => "No authorisation mid specified",
            "157" => "No fields specified",
            "158" => "Required field {0} not specified",
            "159" => "Invalid number of requests",
            "160" => "Not allowed to store Payout Details",
            "161" => "Invalid iban",
            "162" => "Inconsistent iban",
            "163" => "Invalid bic",
            "170" => "Generation Date required but missing",
            "171" => "Unable to parse Generation Date",
            "172" => "Encrypted data used outside of valid time period",
            "173" => "Unable to load Private Key for decryption",
            "174" => "Unable to decrypt data",
            "175" => "Unable to parse JSON data",
            "180" => "Invalid shopperReference",
            "181" => "Invalid shopperEmail",
            "182" => "Invalid selected brand",
            "183" => "Invalid recurring contract",
            "184" => "Invalid recurring detail name",
            "185" => "Invalid additionalData",
            "186" => "Missing additionalData field",
            "187" => "Invalid additionalData field",
            "188" => "Invalid pspEchoData",
            "600" => "No InvoiceProject provided",
            "601" => "No InvoiceBatch provided",
            "602" => "No creditorAccount specified",
            "603" => "No projectCode specified",
            "604" => "No creditorAccount found",
            "605" => "No project found",
            "606" => "Unable to create InvoiceProject",
            "607" => "InvoiceBatch already exists",
            "608" => "Unable to create InvoiceBatch",
            "609" => "InvoiceBatch validity period exceeded",
            "690" => "Error while storing debtor",
            "691" => "Error while storing invoice",
            "692" => "Error while checking if invoice already exists for creditorAccount",
            "693" => "Error while searching invoices",
            "694" => "No Invoice Configuration configured for creditAccount",
            "695" => "Invalid Invoice Configuration configured for creditAccount",
            "800" => "Contract not found",
            "801" => "Too many PaymentDetails defined",
            "802" => "Invalid contract",
            "803" => "PaymentDetail not found",
            "804" => "Failed to disable",
            "805" => "RecurringDetailReference not available for provided recurring-contract",
            "806" => "No applicable contractTypes left for this payment-method",
            "901" => "Invalid Merchant Account",
            "902" => "Shouldn't have gotten here without a request!",
            "903" => "Internal error",
            "904" => "Unable To Process",
            "905" => "Payment details are not supported",
            "906" => "Invalid Request: Original pspReference is invalid for this environment!",
            "950" => "Invalid AcquirerAccount",
            "951" => "Configuration Error (acquirerIdentification)",
            "952" => "Configuration Error (acquirerPassword)",
            "953" => "Configuration Error (apiKey)",
            "954" => "Configuration Error (redirectUrl)",
            "955" => "Configuration Error (AcquirerAccountData)",
            "956" => "Configuration Error (currencyCode)",
            "957" => "Configuration Error (terminalId)",
            "958" => "Configuration Error (serialNumber)",
            "959" => "Configuration Error (password)",
            "960" => "Configuration Error (projectId)",
            "961" => "Configuration Error (merchantCategoryCode)",
            "962" => "Configuration Error (merchantName)",
            //!ak! @TODO
            self::ADYEN_SOAP_ERROR_CODE_REFUND =>                   "Error refund",
            self::ADYEN_SOAP_ERROR_CODE_LIST_RECURRING_DETAILS =>   "Error list recurring details",
            self::ADYEN_SOAP_ERROR_CODE_RECURRING_DISABLE =>        "Error recurring disable",
        );

        $refusalReasonCode = explode(" ", trim($refusalReason));
        $refusalReasonCode = $refusalReasonCode[0];

        if(array_key_exists($refusalReasonCode, $aFaultCodes)) {
            return $aFaultCodes[$refusalReasonCode];
        }
        return false;
    }

    /**
     * @param $refusalReason
     *
     * @return mixed
     */
    public function getErrorCodeByRefusalReason($refusalReason) {
        $refusalReasonCode = explode(" ", trim($refusalReason));
        return $refusalReasonCode[0];
    }

    /**
     * It must match the integer in the database
     *
     * @return int|mixed
     */
    public function getPaySupplierId() {
        return PAY_SUPPLIER_ADYEN;
    }

//    /**
//     * It must match the integer in the database
//     *
//     * @return int|mixed
//     */
//    public function getPaySupplierIdHpp() {
//        return PAY_SUPPLIER_ADYEN_HPP;
//    }

    /**
     * @param $aRequest
     *
     * @return string   XML response Adyen
     */
    public function getSimulateResponse($type, $aRequest )
    {
        $simulateData = array();

        switch($type) {
            case self::ADYEN_SOAP_REQUEST_TYPE_AUTHORISE:
            case self::ADYEN_SOAP_REQUEST_TYPE_RECURRING:
                $simulateData = json_decode('{"paymentResult":{"additionalData":null,"authCode":"' . mt_rand(1000, 99999) . '","dccAmount":null,"dccSignature":null,"fraudResult":null,"issuerUrl":null,"md":null,"paRequest":null,"pspReference":"' . mt_rand(1000000000000000, 9999999999999999) . '","refusalReason":null,"resultCode":"Authorised"}}');
                break;
            case self::ADYEN_SOAP_REQUEST_TYPE_REFUND:
                $simulateData = json_decode('{"refundResult":{"additionalData":,"pspReference":"' . mt_rand(1000000000000000, 9999999999999999) . '","response":"[refund-received]"}}');
                break;
        }

        return $simulateData;
    }

    /**
     * @param $oSoapResponse
     *
     * @return mixed
     */
// !AK! - PENDIENTE !!!
    public static function decodeSoapXmlResponse($oSoapResponse) {
        return json_decode(json_encode($oSoapResponse), true);
    }

    /**
     * @param $oHttpResponse
     *
     * @return mixed
     */
    public static function decodeHttpResponse($oHttpResponse) {
        return json_decode($oHttpResponse, true);
    }


    /**
     * Adyen SOAP response PAYMENT, REFUND
     *
     * @param $type
     * @param $oSoapResponseXml
     * @param $curPayment
     * @param $reference
     * @param $adyenEncryptedData
     *
     * @return PayGatewayLogAdyenResRec
     */
    private function copySoapResponseToPayGatewayResponse($type, $oSoapResponseXml, $curPayment, $reference, $adyenEncryptedData, $paySupplierId)
    {
//        $oSoapResponse =    self::decodeSoapXmlResponse($oSoapResponseXml);
        $oSoapResponse =    $oSoapResponseXml;

        $oGwPayResponse =   new PayGatewayLogAdyenResRec($paySupplierId);

        $aFieldsXML =               array();
        $aFieldsXML["idPayment"] =  $curPayment->id;
        $aFieldsXML["dateSent"] =   HeDate::todaySQL(true);

        switch($type) {
            case self::ADYEN_SOAP_REQUEST_TYPE_AUTHORISE: // PAYMENTS, RECURRING PAYMENTS
                if(isset($oSoapResponse->paymentResult)) {
                    $aFieldsXML["authCode"] =           (isset($oSoapResponse->paymentResult->authCode) ?       strval($oSoapResponse->paymentResult->authCode) : null);
                    $aFieldsXML["dccAmount"] =          (isset($oSoapResponse->paymentResult->dccAmount) ?      strval($oSoapResponse->paymentResult->dccAmount) : null);
                    $aFieldsXML["dccSignature"] =       (isset($oSoapResponse->paymentResult->dccSignature) ?   strval($oSoapResponse->paymentResult->dccSignature) : null);
                    $aFieldsXML["fraudResult"] =        (isset($oSoapResponse->paymentResult->fraudResult) ?    strval($oSoapResponse->paymentResult->fraudResult) : null);
                    $aFieldsXML["issuerUrl"] =          (isset($oSoapResponse->paymentResult->issuerUrl) ?      strval($oSoapResponse->paymentResult->issuerUrl) : null);
                    $aFieldsXML["md"] =                 (isset($oSoapResponse->paymentResult->md) ?             strval($oSoapResponse->paymentResult->md) : null);
                    $aFieldsXML["paRequest"] =          (isset($oSoapResponse->paymentResult->paRequest) ?      strval($oSoapResponse->paymentResult->paRequest) : null);
                    $aFieldsXML["Order_number"] =       (isset($oSoapResponse->paymentResult->pspReference) ?   strval($oSoapResponse->paymentResult->pspReference) : null);
                    $aFieldsXML["refusalReason"] =      (isset($oSoapResponse->paymentResult->refusalReason) ?  strval($oSoapResponse->paymentResult->refusalReason) : null);
                    $aFieldsXML["resultCode"] =         (isset($oSoapResponse->paymentResult->resultCode) ?     strval($oSoapResponse->paymentResult->resultCode) : null);
//                    $aFieldsXML["additionalData"] =     (isset($oSoapResponse->paymentResult->additionalData) ? (is_array($oSoapResponse->paymentResult->additionalData) ? json_encode($oSoapResponse->paymentResult->additionalData) : strval($oSoapResponse->paymentResult->additionalData)) : null);
                    $aFieldsXML["additionalData"] =     (isset($oSoapResponse->paymentResult->additionalData) ? (is_array($oSoapResponse->paymentResult->additionalData) ? json_encode($oSoapResponse->paymentResult->additionalData) : json_encode(json_decode(json_encode($oSoapResponse->paymentResult->additionalData), true))) : null);
                }
                break;
            case self::ADYEN_SOAP_REQUEST_TYPE_REFUND: // REFUNDS
                if(isset($oSoapResponse->refundResult)) {
                    //
                    // REFUNDS
                    //
                    $aFieldsXML["Order_number"] =   (isset($oSoapResponse->refundResult->pspReference) ?    strval($oSoapResponse->refundResult->pspReference) : null);
                    $aFieldsXML["resultCode"] =     (isset($oSoapResponse->refundResult->response) ?        strval($oSoapResponse->refundResult->response) : null);
//                    $aFieldsXML["additionalData"] = (isset($oSoapResponse->refundResult->additionalData) ? (is_array($oSoapResponse->refundResult->additionalData) ? json_encode($oSoapResponse->refundResult->additionalData) : strval($oSoapResponse->refundResult->additionalData)) : null);
                    $aFieldsXML["additionalData"] = (isset($oSoapResponse->refundResult->additionalData) ? (is_array($oSoapResponse->refundResult->additionalData) ? json_encode($oSoapResponse->refundResult->additionalData) : json_encode(json_decode(json_encode($oSoapResponse->refundResult->additionalData), true))) : null);
                }
                break;

            case self::ADYEN_SOAP_REQUEST_TYPE_LIST_RECURRING_DETAILS: // LIST RECURRING DETAILS
                if(isset($oSoapResponse->result)) {
                    //
                    // LIST RECURRING DETAILS
                    //
                    $aFieldsXML["Order_number"] =   (isset($oSoapResponse->result->details->RecurringDetail->recurringDetailReference) ?  strval($oSoapResponse->result->details->RecurringDetail->recurringDetailReference) : null);
                    $aFieldsXML["resultCode"] =     (isset($oSoapResponse->result->details->RecurringDetail->variant) ?                   strval($oSoapResponse->result->details->RecurringDetail->variant) : null);
                }
                break;

            case self::ADYEN_SOAP_REQUEST_TYPE_RECURRING_DISABLE: // RECURRING CANCEL
                if(isset($oSoapResponse->result)) {
                    //
                    // RECURRING CANCEL
                    //
                    $aFieldsXML["resultCode"] = (isset($oSoapResponse->result->response) ? strval($oSoapResponse->result->response) : null);
                }
                break;
        }

        $aFieldsXML["XML"] =                json_encode($oSoapResponse);
        $aFieldsXML["reference"] =          $reference;
        $aFieldsXML["adyenEncryptedData"] = $adyenEncryptedData;
        $aFieldsXML["numNotifications"] =   0;

        $oGwPayResponse->setXMLFieldsToProperties( $aFieldsXML );

        return $oGwPayResponse;
    }


    /**
     * @param $type
     * @param $oHttpResponse
     * @param $skinCode
     *
     * @return PayGatewayLogAdyenResRec
     */
    private function copyHttpResponseToPayGatewayResponse($type, $curPayment, $oHttpResponse, $reference, $paySupplierId)
    {
//        $oHttpResponseDecoded = self::decodeHttpResponse($oHttpResponse);

        $oGwPayResponse =   new PayGatewayLogAdyenResRec($paySupplierId);

        $aFieldsXML =               array();
        $aFieldsXML["idPayment"] =  "";
        $aFieldsXML["dateSent"] =   HeDate::todaySQL(true);

        switch($type) {
            case self::ADYEN_TYPE_HPP_PAYMENT_GET_PAYMENT_METHODS:
                if(isset($oHttpResponseDecoded['paymentMethods'])) {
//                    $aFieldsXML["authCode"] =           (isset($oSoapResponse->paymentResult->authCode) ?       strval($oSoapResponse->paymentResult->authCode) : null);
//                    $aFieldsXML["additionalData"] =     (isset($oSoapResponse->paymentResult->additionalData) ? (is_array($oSoapResponse->paymentResult->additionalData) ? json_encode($oSoapResponse->paymentResult->additionalData) : strval($oSoapResponse->paymentResult->additionalData)) : null);
                }
                break;

            case self::ADYEN_SOAP_REQUEST_TYPE_AUTHORISE:
                $aFieldsXML["idPayment"] =          $curPayment->id;
                $aFieldsXML["resultCode"] =         (isset($oHttpResponse['authResult']) ?      self::ADYEN_SOAP_RESULT_CODE_AUTHORISED : null);
                $aFieldsXML["Order_number"] =       (isset($oHttpResponse['pspReference']) ?    strval($oHttpResponse['pspReference']) : null);
                $aFieldsXML["numNotifications"] =   1;
                break;
        }

        $aFieldsXML["XML"] =                json_encode($oHttpResponse);
        $aFieldsXML["reference"] =          $reference;
        $aFieldsXML["adyenEncryptedData"] = "";
        $aFieldsXML["numNotifications"] =   0;

        $oGwPayResponse->setXMLFieldsToProperties( $aFieldsXML );

        return $oGwPayResponse;
    }



    /**
     * @param $action
     * @param $sendData
     *
     * @return null
     */
    public function sendSoapRequest($action, $sendData) {

//
// @TODO
//
//        //!ak! URL TEST OR LIVE
//        if( ENTORNO == TEST) {
//        }
//        else {
//        }

        $urlAdyen = Yii::app()->config->get("PAY_ADYEN_URL");

        switch($action) {
            case self::ADYEN_SOAP_REQUEST_TYPE_AUTHORISE:
            case self::ADYEN_SOAP_REQUEST_TYPE_RECURRING:
            case self::ADYEN_SOAP_REQUEST_TYPE_REFUND:
                $urlAdyen = Yii::app()->config->get("PAY_ADYEN_URL");
                break;

            case self::ADYEN_SOAP_REQUEST_TYPE_LIST_RECURRING_DETAILS:
            case self::ADYEN_SOAP_REQUEST_TYPE_RECURRING_DISABLE:
                $urlAdyen = Yii::app()->config->get("PAY_ADYEN_URL_RECURRING");
                break;
        }

        $configData =   array(
            "login" =>      Yii::app()->config->get("PAY_ADYEN_SOAP_LOGIN"),
            "password" =>   Yii::app()->config->get("PAY_ADYEN_SOAP_PASSWORD"),
            "style" =>      SOAP_DOCUMENT,
            "encoding" =>   SOAP_LITERAL,
            "cache_wsdl" => WSDL_CACHE_BOTH,
            "trace" =>      1,
        );

        return HeComm::sendSoapRequest($urlAdyen, $action, $configData, $sendData);
    }

    /**
     * @param $amountPrice
     *
     * @return mixed
     */
    public static function formatAmount($amountPrice) {
        return $amountPrice * 100;
    }

    /**
     * @param $amountPrice
     *
     * @return float
     */
    public static function getFormatAmount($amountPrice) {
        return (float) ($amountPrice / 100);
    }

    /**
     * @param $moUser
     *
     * @return string
     */
    public static function getReference($moUser) {
        return md5($moUser->id);
    }

    /**
     * @param $curPayment
     *
     * @return string
     */
    public static function createReference($curPayment) {
        return $curPayment->id . ":" . substr("" . date("Hms"), 3);
    }

    /** Executes the transaction through TPV of Adyen
     *  Create an initial recurring payment
     *
     * 2016-03-16 - Ecommers & ONECLICK,RECURRING for initial payment
     *
     * @param Payment|PaymentControlCheck $curPayment
     * @param string $transactionType
     * @param bool $isRecurringPay
     *
     * @return bool|PayGatewayLogAdyenResRec
     */
    public function runDebiting( $curPayment, $transactionType='', $isRecurringPay=false )
    {
        $paySupplierId = $this->getPaySupplierId();

        $moUser = new AbaUser();
        $moUser->getUserById( $curPayment->userId );

        $this->errorCode=0;

        $amount =   HeMixed::roundAmount(self::formatAmount(HeMixed::getRoundAmount($curPayment->amountPrice)), 0);
        $currency = $this->getPaySupplierCodes($curPayment->currencyTrans);

        $optionalData = $this->getOptionalData();

        $adyenEncryptedData = '';
        if(!$isRecurringPay AND isset($optionalData["adyen-encrypted-data"])) {
            $adyenEncryptedData = $optionalData["adyen-encrypted-data"];
        }

        $machine =              "";
        $merchantAccount =      Yii::app()->config->get("PAY_ADYEN_MERCHANT_ACCOUNT");
        $reference =            self::createReference($curPayment);
        $shopperReference =     self::getReference($moUser);
        $fraudOffset =          "0";


        //#ABAWEBAPPS-601
        $stTypeData = self::getRecurringPaymentTypeData(self::RECURRING_PAYMENT_TYPE_INITIAL);

        $recurringContract = $stTypeData["contract"];
        $shopperInteraction = $stTypeData["shopperInteraction"];


        $this->orderNumber =    $reference;

        $this->arrayRequest["idPayment"] =          $curPayment->id;
        $this->arrayRequest["dateSent"] =           strval(HeDate::todaySQL(true));
        $this->arrayRequest["merchantAccount"] =    $merchantAccount;
        $this->arrayRequest["amountValue"] =        $amount;
        $this->arrayRequest["amountCurrency"] =     $currency;
        $this->arrayRequest["reference"] =          $reference;
        $this->arrayRequest["shopperIP"] =          $machine;
        $this->arrayRequest["fraudOffset"] =        $fraudOffset;
        $this->arrayRequest["additionalData"] =     $adyenEncryptedData;

        //
        // OBLIGATORIOS PARA RECURRINGS
        //
        $this->arrayRequest["userId"] =             $moUser->id;
        $this->arrayRequest["shopperEmail"] =       $moUser->email;
        $this->arrayRequest["shopperReference"] =   $shopperReference;
        $this->arrayRequest["recurringContract"] =  $recurringContract;
        $this->arrayRequest["shopperInteraction"] = $shopperInteraction;

        $this->arrayRequest["typeGateway"] =        self::ADYEN_SOAP_SEND_TYPE_AUTHORIZATION_PAYMENT;

        //
        // Log SEND
        //
        $aLogRequest = new PayGatewayLogAdyenReqSent($paySupplierId);

        $aLogRequest->setXMLFieldsToProperties( $this->arrayRequest ) ;

        if (!$aLogRequest->insertRequestSent()) {
            $this->errorCode = self::ERROR_ABADB_LOGPAY;
            return false;
        }

        //
        $sendData = array (
            "paymentRequest" => array (
                "merchantAccount" =>    $merchantAccount,
                "amount" =>             array (
                    "currency" =>   $currency,
                    "value" =>      $amount,
                ),
                "reference" =>          $reference,
                "shopperIP" =>          $machine,
                "recurring" =>  array(
                    "contract" => $recurringContract
                ),
                "shopperEmail" =>       $moUser->email,
                "shopperReference" =>   $shopperReference,
                "shopperInteraction" => $shopperInteraction,
                "fraudOffset" =>        $fraudOffset,
                "additionalData" =>     array (
                    "entry" => new SoapVar ( array
                            (
                                "key" =>    new SoapVar("card.encrypted.json",  XSD_STRING, "string", self::ADYEN_SOAP_REQUEST_XMLSCHEMA, "key",    Yii::app()->config->get("PAY_ADYEN_SOAP_ADDITIONALDATA_URL")),
                                "value" =>  new SoapVar($adyenEncryptedData,    XSD_STRING, "string", self::ADYEN_SOAP_REQUEST_XMLSCHEMA, "value",  Yii::app()->config->get("PAY_ADYEN_SOAP_ADDITIONALDATA_URL"))
                            ), SOAP_ENC_OBJECT, ""
                        )
                ),
            )
        );

        if ($this->connectToGateway) {
            try {
                $oSoapResponse = $this->sendSoapRequest(self::ADYEN_SOAP_REQUEST_TYPE_AUTHORISE, $sendData);
            }
            catch (Exception $e) {
                HeLogger::sendLog(" A communication to pay in Adyen has failed for payment control check: " . $curPayment->id,
                  HeLogger::IT_BUGS, HeLogger::CRITICAL,
                  " An user was not able to finish his transaction, user Id=" . $curPayment->userId . " error curl probably= " . strval($e->getMessage()));
                $this->errorCode = self::ERROR_SOAP;
                return false;
            }
        } else {
            $oSoapResponse = $this->getSimulateResponse(self::ADYEN_SOAP_REQUEST_TYPE_AUTHORISE, $this->arrayRequest);
        }

        //
        $this->moLogResponse = $this->copySoapResponseToPayGatewayResponse(self::ADYEN_SOAP_REQUEST_TYPE_AUTHORISE, $oSoapResponse, $curPayment, $reference, $adyenEncryptedData, $paySupplierId);

        if (!$this->moLogResponse->insertResponseReceived()) {
            $this->errorCode = self::ERROR_ABADB_LOGPAY;
            return false;
        }

        if (!$this->interpretResponse($oSoapResponse, $moUser, $curPayment)) {
            return false;
        }

        if (!$isRecurringPay) {
            HeLogger::sendLog(HeLogger::PREFIX_NOTIF_L . ": A new purchase with Adyen has been CONFIRMED, the payment id is " .
              $curPayment->id . " from user " . $moUser->email, HeLogger::PAYMENTS, HeLogger::CRITICAL,
              " A purchase with Credit Card has ocurred, we want to follow the details of this purchase, user Id=" .
              $curPayment->userId);
        }

        return $this->moLogResponse;
    }


    /**
     * @param $curPayment
     * @param $moUser
     *
     * @return array
     */
    public function getMerchantSigData($curPayment, $moUser) {

        $sendData = array();

        $sendData['paymentAmount'] =        self::formatAmount(HeMixed::getRoundAmount($curPayment->amountPrice));
        $sendData['currencyCode'] =         $this->getPaySupplierCodes($curPayment->currencyTrans);
        $sendData['merchantAccount'] =      Yii::app()->config->get("PAY_ADYEN_MERCHANT_ACCOUNT");
        $sendData['shopperEmail'] =         $moUser->email;
        $sendData['shopperReference'] =     self::getReference($moUser);
        $sendData['recurringContract'] =    self::ADYEN_SOAP_RECURRING_CONTRACT;
        $sendData['allowedMethods'] =       "";
        $sendData['blockedMethods'] =       "";
        $sendData['offset'] =               "0";
        $sendData['hmacKey'] =              Yii::app()->config->get("PAY_ADYEN_HPP_SKIN_DEFAULT_KEY");

        return $sendData;
    }

    /**
     * @param $curPayment
     *
     * @return array|bool
     */
    public function runDebitingHpp( $curPayment )
    {
        $paySupplierId = $this->getPaySupplierId();

        $moUser = new AbaUser();
        $moUser->getUserById( $curPayment->userId );

        $this->errorCode = 0;

        //
        $sendData = $this->getMerchantSigData($curPayment, $moUser);

        //
        $optionalData = $this->getOptionalData();

        $brandCode = "";
        if (isset($optionalData['paymentBrandCode']) AND trim($optionalData['paymentBrandCode']) != '') {
            $brandCode = trim($optionalData['paymentBrandCode']);
        }

        $machine =              "";
        $reference =            self::HPP_MERCHANT_REFERENCE_PREFIX . self::createReference($curPayment);
        $shopperInteraction =   self::ADYEN_SOAP_SHOPPER_INTERACTION_CONTAUTH;
        $skinCode =             Yii::app()->config->get("PAY_ADYEN_HPP_SKIN_DEFAULT_NAME");

        $this->orderNumber = $reference;

        $this->arrayRequest["idPayment"] = $curPayment->id;
        $this->arrayRequest["dateSent"] = strval(HeDate::todaySQL(true));
        $this->arrayRequest["merchantAccount"] = $sendData['merchantAccount'];
        $this->arrayRequest["amountValue"] = $sendData['paymentAmount'];
        $this->arrayRequest["amountCurrency"] = $sendData['currencyCode'];
        $this->arrayRequest["reference"] = $reference; // merchantReference
        $this->arrayRequest["shopperIP"] = $machine;
        $this->arrayRequest["fraudOffset"] = $sendData['offset'];

        //
        // OBLIGATORIOS PARA RECURRINGS
        //
        $this->arrayRequest["userId"] = $moUser->id;
        $this->arrayRequest["shopperEmail"] = $sendData['shopperEmail'];
        $this->arrayRequest["shopperReference"] = $sendData['shopperReference'];
        $this->arrayRequest["recurringContract"] = $sendData['recurringContract'];
        $this->arrayRequest["shopperInteraction"] = $shopperInteraction;
        $this->arrayRequest["typeGateway"] = self::ADYEN_TYPE_HPP_PAYMENT_AUTHORIZATION;

        //
        $abaCountry = new AbaCountry();
        $abaCountry->getCountryById($curPayment->idCountry);

        $shopperLocale = $moUser->langEnv;

        $countryCode = HeMixed::getCountryCodeIso2ForAdyen($abaCountry->iso); // RU, ES,..

        //
        $sendData['shipBeforeDate'] = self::getShipBeforeDate();
        $sendData['merchantReference'] = $reference;
        $sendData['skinCode'] = $skinCode;
        $sendData['sessionValidity'] = self::getSessionValidity();

        $sendData['shopperLocale'] = $shopperLocale;
        $sendData['countryCode'] = $countryCode;

        $sendData['orderData'] = base64_encode(gzencode("Orderdata to display on the HPP can be put here"));

        $sendData['shopperInteraction'] = $shopperInteraction;

        $sendData['adyenPaymentUrl'] = Yii::app()->config->get("PAY_ADYEN_URL_HPP_PAYMENT");

        if ($brandCode != '') {
            $sendData['brandCode'] = $brandCode;
            $sendData['adyenPaymentUrl'] = Yii::app()->config->get("PAY_ADYEN_URL_HPP_PAYMENT_METHOD");
        }

        //
        $merchantSig = base64_encode(pack("H*", hash_hmac('sha1',
          $sendData['paymentAmount'] . $sendData['currencyCode'] . $sendData['shipBeforeDate'] . $sendData['merchantReference'] . $sendData['skinCode'] .
          $sendData['merchantAccount'] . $sendData['sessionValidity'] . $sendData['shopperEmail'] . $sendData['shopperReference'] . $sendData['recurringContract'] .
          $sendData['allowedMethods'] . $sendData['blockedMethods'] . $sendData['offset'],
          $sendData['hmacKey']
        )));

        $sendData['merchantSig'] = $merchantSig;

        //
        $this->arrayRequest["additionalData"] = $merchantSig;
        $this->arrayRequest["originalReference"] = $skinCode;
        $this->arrayRequest["selectedRecurringDetailReference"] = $brandCode;

        //
        // Log SEND
        //
        $aLogRequest = new PayGatewayLogAdyenReqSent($paySupplierId);

        $aLogRequest->setXMLFieldsToProperties($this->arrayRequest);

        if (!$aLogRequest->insertRequestSent()) {
            $this->errorCode = self::ERROR_ABADB_LOGPAY;
            return false;
        }

        return $sendData;
    }

    /**
     * Create an recurring payment
     *
     * 2016-03-16 ContAuth & RECURRING for recurring payments
     *
     * @param $moPayCtrlCheck
     * @param AbaUser $moUser
     * @param $moUserCredit
     *
     * @return bool|PayGatewayLogAdyenResRec
     */
    public function runDebitingRenewal( $moPayCtrlCheck, AbaUser $moUser, $moUserCredit )
    {
        $paySupplierId = $this->getPaySupplierId();

        $this->errorCode = 0;

        $amount =       HeMixed::roundAmount(self::formatAmount(HeMixed::getRoundAmount($moPayCtrlCheck->amountPrice)), 0);
        $currency =     $this->getPaySupplierCodes($moPayCtrlCheck->currencyTrans);
        $reference =    self::createReference($moPayCtrlCheck);

        //
        $bIsHpp = false;
        if($moPayCtrlCheck->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP) {
            $reference = self::HPP_MERCHANT_REFERENCE_PREFIX . $reference;
            $bIsHpp = true;
        }


        //#ABAWEBAPPS-601
        $stTypeData = self::getRecurringPaymentTypeData(self::RECURRING_PAYMENT_TYPE_RECURRING);

        $recurringContract = $stTypeData["contract"];
        $shopperInteraction = $stTypeData["shopperInteraction"];


        $selectedRecurringDetailReference = $moUserCredit->paySuppExtUniId;
        $machine =                          "";
        $merchantAccount =                  Yii::app()->config->get("PAY_ADYEN_MERCHANT_ACCOUNT");

        // An ID that uniquely identifies the shopper (e.g. a customer id in a shopping cart system).
        $shopperReference =     self::getReference($moUser);
        $fraudOffset =          "0";
        $shopperStatement =     "";

        $this->orderNumber =    $reference;

        $this->arrayRequest["idPayment"] =          $moPayCtrlCheck->id;
        $this->arrayRequest["dateSent"] =           strval(HeDate::todaySQL(true));
        $this->arrayRequest["merchantAccount"] =    $merchantAccount;
        $this->arrayRequest["amountValue"] =        $amount;
        $this->arrayRequest["amountCurrency"] =     $currency;
        $this->arrayRequest["reference"] =          $reference;
        $this->arrayRequest["shopperIP"] =          $machine;
        $this->arrayRequest["fraudOffset"] =        $fraudOffset;

        $this->arrayRequest["selectedRecurringDetailReference"] =   $selectedRecurringDetailReference;

        $this->arrayRequest["userId"] =             $moUser->id;
        $this->arrayRequest["shopperEmail"] =       $moUser->email;
        $this->arrayRequest["shopperReference"] =   $shopperReference;
        $this->arrayRequest["recurringContract"] =  $recurringContract;
        $this->arrayRequest["additionalData"] =     null;

        $this->arrayRequest["typeGateway"] =        self::ADYEN_SOAP_SEND_TYPE_AUTHORIZATION_RECURRING;

        //
        // Log SEND
        //
        $aLogRequest = new PayGatewayLogAdyenReqSent($paySupplierId);

        $aLogRequest->setXMLFieldsToProperties( $this->arrayRequest ) ;

        if (!$aLogRequest->insertRequestSent()) {
            $this->errorCode = self::ERROR_ABADB_LOGPAY;
            return false;
        }

        //
        $sendData = array (
            "paymentRequest" => array (
                "selectedRecurringDetailReference" => "LATEST",
                "recurring" => array(
                    "contract" => $recurringContract
                ),
                "shopperEmail" =>       $moUser->email,
                "shopperReference" =>   $shopperReference,
                "shopperInteraction" => $shopperInteraction,
                "merchantAccount" =>    $merchantAccount,
                "amount" =>             array (
                    "currency" =>   $currency,
                    "value" =>      $amount,
                ),
//                "card" =>               array (
//                    "cvc" =>   "7373",
//                ),
                "reference" =>          $reference,
                "fraudOffset" =>        $fraudOffset,
                "shopperIP" =>          $machine,
                "shopperStatement" =>   $shopperStatement,
            )
        );

        if ($this->connectToGateway) {
            try {
                $oSoapResponse = $this->sendSoapRequest(self::ADYEN_SOAP_REQUEST_TYPE_AUTHORISE, $sendData);
            }
            catch (Exception $e) {
                HeLogger::sendLog(" A communication to pay in Adyen has failed for payment control check: " . $moPayCtrlCheck->id,
                  HeLogger::IT_BUGS, HeLogger::CRITICAL,
                  " An user was not able to finish his transaction, user Id=" . $moPayCtrlCheck->userId . " error curl probably= " . strval($e->getMessage()));
                $this->errorCode = self::ERROR_SOAP;
                return false;
            }
        } else {
            $oSoapResponse = $this->getSimulateResponse(self::ADYEN_SOAP_REQUEST_TYPE_RECURRING, $this->arrayRequest);
        }

        //
        $this->moLogResponse = $this->copySoapResponseToPayGatewayResponse(self::ADYEN_SOAP_REQUEST_TYPE_AUTHORISE, $oSoapResponse, $moPayCtrlCheck, $reference, "", $paySupplierId);

        if (!$this->moLogResponse->insertResponseReceived()) {
            $this->errorCode = self::ERROR_ABADB_LOGPAY;
            return false;
        }

        if (!$this->interpretResponse($oSoapResponse, $moUser, $moPayCtrlCheck, $bIsHpp)) {
            return false;
        }

        HeLogger::sendLog(HeLogger::PREFIX_NOTIF_L . ": A new purchase with Adyen has been CONFIRMED, the payment id is " .
          $moPayCtrlCheck->id . " from user " . $moUser->email, HeLogger::PAYMENTS, HeLogger::INFO,
          " A purchase with Credit Card has ocurred, we want to follow the details of this purchase, user Id=" .
          $moPayCtrlCheck->userId);

        return $this->moLogResponse;
    }

    /**
     * @param Payment $curPayment
     * @param Payment $moPayOrigin
     *
     * @return bool|PayGatewayLogAdyenResRec
     */
    public function runRefund(Payment $curPayment, Payment $moPayOrigin) {

        $paySupplierId = $this->getPaySupplierId();

        $moUser = new AbaUser();
        $moUser->getUserById( $curPayment->userId );

        $this->errorCode = 0;

        $amount =   HeMixed::roundAmount(self::formatAmount(HeMixed::getRoundAmount($curPayment->amountPrice)), 0);
        $currency = $this->getPaySupplierCodes($curPayment->currencyTrans);

        $machine =          '';
        $merchantAccount =  Yii::app()->config->get("PAY_ADYEN_MERCHANT_ACCOUNT");
        $reference =        self::createReference($curPayment);
        $fraudOffset =      "0";

        $this->orderNumber = $reference;

        $originalReference = $curPayment->paySuppExtUniId;

        $this->arrayRequest["idPayment"] =          $curPayment->id;
        $this->arrayRequest["dateSent"] =           strval(HeDate::todaySQL(true));
        $this->arrayRequest["merchantAccount"] =    $merchantAccount;
        $this->arrayRequest["amountValue"] =        $amount;
        $this->arrayRequest["amountCurrency"] =     $currency;
        $this->arrayRequest["reference"] =          $reference;
        $this->arrayRequest["shopperIP"] =          $machine;
        $this->arrayRequest["fraudOffset"] =        $fraudOffset;
        $this->arrayRequest["additionalData"] =     null;
        $this->arrayRequest["originalReference"] =  $originalReference;
        $this->arrayRequest["typeGateway"] =        self::ADYEN_SOAP_SEND_TYPE_REFUND;

        //
        // Log SEND
        //
        $aLogRequest = new PayGatewayLogAdyenReqSent($paySupplierId);

        $aLogRequest->setXMLFieldsToProperties( $this->arrayRequest ) ;

        if (!$aLogRequest->insertRequestSent()) {
            $this->errorCode = self::ERROR_ABADB_LOGPAY;
            return false;
        }

        $this->orderNumber = $reference;

        //
        $sendData = array (
            "modificationRequest" => array (
                "merchantAccount" =>    $merchantAccount,
                "modificationAmount" => array (
                    "currency" =>       $currency,
                    "value" =>          $amount,
                ),
                "reference" =>          $reference,
                "originalReference" =>  $originalReference,
            )
        );

        if ($this->connectToGateway) {
            try {
                $oSoapResponse = $this->sendSoapRequest(self::ADYEN_SOAP_REQUEST_TYPE_REFUND, $sendData);
            }
            catch (Exception $e) {
                HeLogger::sendLog(" A communication to pay in Adyen has failed for payment control check: " . $curPayment->id,
                  HeLogger::IT_BUGS, HeLogger::CRITICAL,
                  " An user was not able to finish his transaction, user Id=" . $curPayment->userId . " error curl probably= " . strval($e->getMessage()));

                $this->errorCode = self::ERROR_SOAP;
                return false;
            }
        } else {
            $oSoapResponse = $this->getSimulateResponse(self::ADYEN_SOAP_REQUEST_TYPE_REFUND, $this->arrayRequest);
        }

        //
        $this->moLogResponse = $this->copySoapResponseToPayGatewayResponse(self::ADYEN_SOAP_REQUEST_TYPE_REFUND, $oSoapResponse, $curPayment, $reference, "", $paySupplierId);

        if (!$this->moLogResponse->insertResponseReceived()) {
            $this->errorCode = self::ERROR_ABADB_LOGPAY;
            return false;
        }

        if (!$this->interpretResponseRefund($oSoapResponse, $moUser, $curPayment)) {
            return false;
        }

        HeLogger::sendLog(HeLogger::PREFIX_NOTIF_L . ": A new REFUND with Adyen has been CONFIRMED, the refund id is " .
          $curPayment->id . " from user " . $moUser->email, HeLogger::PAYMENTS, HeLogger::INFO,
          " A purchase with Credit Card has ocurred, we want to follow the details of this refund, user Id=" .
          $curPayment->userId);
        return $this->getMoLogResponse();
    }

    /** Cancel recurring contract Adyen
     *
     * @param Payment $moPayment
     * @param AbaUserCreditForms $moUserCredit
     *
     * @return bool|PayGatewayLogAdyenResRec
     */
    public function runCancelRecurring(Payment $moPayment, AbaUserCreditForms $moUserCredit)
    {
        // @TODO
        // $paySupplierId = $this->getPaySupplierId();
        $paySupplierId = $moPayment->paySuppExtId;

        if (!$this->connectToGateway) {
            $this->infoPayment = "TEST_FICTICIO";
            return true;
        }

        $moUser = new AbaUser();
        $moUser->getUserById( $moPayment->userId );

        $this->errorCode=0;

        $merchantAccount =      Yii::app()->config->get("PAY_ADYEN_MERCHANT_ACCOUNT");
        $shopperReference =     self::getReference($moUser);

        //
        // 1. List recurring details
        //
        $this->arrayRequest["idPayment"] =          $moPayment->id;
        $this->arrayRequest["dateSent"] =           strval(HeDate::todaySQL(true));
        $this->arrayRequest["merchantAccount"] =    $merchantAccount;
        $this->arrayRequest["amountValue"] =        null;
        $this->arrayRequest["amountCurrency"] =     null;
        $this->arrayRequest["reference"] =          null;
        $this->arrayRequest["userId"] =             $moUser->id;
        $this->arrayRequest["shopperEmail"] =       null;
        $this->arrayRequest["shopperIP"] =          null;
        $this->arrayRequest["fraudOffset"] =        null;
        $this->arrayRequest["selectedRecurringDetailReference"] =   null;
        $this->arrayRequest["shopperReference"] =   $shopperReference;
        $this->arrayRequest["originalReference"] =  null;
        $this->arrayRequest["recurringContract"] =  null;
        $this->arrayRequest["additionalData"] =     null;
        $this->arrayRequest["typeGateway"] =        self::ADYEN_SOAP_SEND_TYPE_LIST_RECURRING_DETAILS;

        //
        // Log SEND
        //
        $aLogRequest = new PayGatewayLogAdyenReqSent($paySupplierId);

        $aLogRequest->setXMLFieldsToProperties( $this->arrayRequest ) ;

        if (!$aLogRequest->insertRequestSent()) {
            $this->errorCode = self::ERROR_ABADB_LOGPAY;
            return false;
        }

        //
        $sendData = array (
            "request" => array (
                "merchantAccount" =>    $merchantAccount,
                "shopperReference" =>   $shopperReference,
                "recurring" =>          array( "contract" => self::ADYEN_SOAP_RECURRING_CONTRACT )
//                "recurringContract" => "RECURRING"
            )
        );

        if ($this->connectToGateway) {
            try {
                $oSoapResponse = $this->sendSoapRequest(self::ADYEN_SOAP_REQUEST_TYPE_LIST_RECURRING_DETAILS, $sendData);
            }
            catch (Exception $e) {
                HeLogger::sendLog(" A communication to pay in Adyen has failed for payment control check: " . $moPayment->id,
                  HeLogger::IT_BUGS, HeLogger::CRITICAL,
                  " An user was not able to finish his transaction, user Id=" . $moPayment->userId . " error curl probably= " . strval($e->getMessage()));
                $this->errorCode = self::ERROR_SOAP;
                return false;
            }
        } else {
            $oSoapResponse = $this->getSimulateResponse(self::ADYEN_SOAP_REQUEST_TYPE_LIST_RECURRING_DETAILS, $this->arrayRequest);
        }

        //
        $this->moLogResponse = $this->copySoapResponseToPayGatewayResponse(self::ADYEN_SOAP_REQUEST_TYPE_LIST_RECURRING_DETAILS, $oSoapResponse, $moPayment, "", "", $paySupplierId);

        if (!$this->moLogResponse->insertResponseReceived()) {
            $this->errorCode = self::ERROR_ABADB_LOGPAY;
            return false;
        }

        if (!$this->interpretResponseListRecurringDetails($oSoapResponse, $moUser, $moPayment)) {
            return false;
        }

        //
        // 2. Cancel recurring detail
        //
        $recurringDetailReference = $this->getRecurringDetailReference();

        if($recurringDetailReference != '') {

            $this->arrayRequest = array();

            $this->arrayRequest["idPayment"] =          $moPayment->id;
            $this->arrayRequest["dateSent"] =           strval(HeDate::todaySQL(true));
            $this->arrayRequest["merchantAccount"] =    $merchantAccount;
            $this->arrayRequest["amountValue"] =        null;
            $this->arrayRequest["amountCurrency"] =     null;
            $this->arrayRequest["reference"] =          null;
            $this->arrayRequest["userId"] =             $moUser->id;
            $this->arrayRequest["shopperEmail"] =       null;
            $this->arrayRequest["shopperIP"] =          null;
            $this->arrayRequest["fraudOffset"] =        null;
            $this->arrayRequest["selectedRecurringDetailReference"] =   $recurringDetailReference;
            $this->arrayRequest["shopperReference"] =   $shopperReference;
            $this->arrayRequest["originalReference"] =  null;
            $this->arrayRequest["recurringContract"] =  null;
            $this->arrayRequest["additionalData"] =     null;
            $this->arrayRequest["typeGateway"] =        self::ADYEN_SOAP_SEND_TYPE_RECURRING_DISABLE;

            //
            // Log SEND
            //
            $aLogRequest = new PayGatewayLogAdyenReqSent($paySupplierId);

            $aLogRequest->setXMLFieldsToProperties( $this->arrayRequest ) ;

            if (!$aLogRequest->insertRequestSent()) {
                $this->errorCode = self::ERROR_ABADB_LOGPAY;
                return false;
            }

            //
            $sendData = array (
                "request" => array (
                    "merchantAccount" =>            $merchantAccount,
                    "shopperReference" =>           $shopperReference,
                    "recurringDetailReference" =>   $recurringDetailReference,
                )
            );

            if ($this->connectToGateway) {
                try {
                    $oSoapResponse = $this->sendSoapRequest(self::ADYEN_SOAP_REQUEST_TYPE_RECURRING_DISABLE, $sendData);
                }
                catch (Exception $e) {
                    HeLogger::sendLog(" A communication to pay in Adyen has failed for payment control check: " . $moPayment->id,
                      HeLogger::IT_BUGS, HeLogger::CRITICAL,
                      " An user was not able to finish his transaction, user Id=" . $moPayment->userId . " error curl probably= " . strval($e->getMessage()));

                    $this->errorCode = self::ERROR_SOAP;
                    return false;
                }
            } else {
                $oSoapResponse = $this->getSimulateResponse(self::ADYEN_SOAP_REQUEST_TYPE_RECURRING_DISABLE, $this->arrayRequest);
            }

            //
            $this->moLogResponse = $this->copySoapResponseToPayGatewayResponse(self::ADYEN_SOAP_REQUEST_TYPE_RECURRING_DISABLE, $oSoapResponse, $moPayment, "", "", $paySupplierId);

            if (!$this->moLogResponse->insertResponseReceived()) {
                $this->errorCode = self::ERROR_ABADB_LOGPAY;
                return false;
            }

            if (!$this->interpretResponseListRecurringDisable($oSoapResponse, $moUser, $moPayment)) {
                return false;
            }

            HeLogger::sendLog(HeLogger::PREFIX_NOTIF_L . ": A new CANCEL with Adyen has been CONFIRMED, the refund id is " .
              $moPayment->id . " from user " . $moUser->email, HeLogger::PAYMENTS, HeLogger::INFO,
              " A payment cancel has ocurred, we want to follow the details of this cancel, user Id=" .
              $moPayment->userId);

            return $this->getMoLogResponse();
        }
        else {
            HeLogger::sendLog(" Recurring detail reference not found in Adyen : " . $moPayment->id,
              HeLogger::IT_BUGS, HeLogger::CRITICAL,
              " An user was not able to finish his transaction, user Id=" . $moPayment->userId);

            return false;
        }

        return false;
    }

    /**
     * @param $allowedMethods
     */
    public function setAllowedMethods($allowedMethods) {
        $this->allowedMethods = $allowedMethods;
    }

    /**
     * @return array
     */
    public function getAllowedMethods($bDefaultMethods=false) {

        // @TODO - get default allowed methods (without api)
        if($bDefaultMethods) {
            return array();
        }

        return $this->allowedMethods;
    }

//
// @TODO
// @TODO
// @TODO
//
    /**
     * @param bool $bFormatted
     *
     * @return array
     */
    public static function getAllPaymentMethods($bFormatted=false) {

        return array(

            self::HPP_RECUPPING_PAYMENT_METHOD_DIRECTEBANKING =>     self::HPP_RECUPPING_PAYMENT_METHOD_DIRECTEBANKING,
            self::HPP_RECUPPING_PAYMENT_METHOD_GIROPAY =>            self::HPP_RECUPPING_PAYMENT_METHOD_GIROPAY,
            self::HPP_RECUPPING_PAYMENT_METHOD_IDEAL =>              self::HPP_RECUPPING_PAYMENT_METHOD_IDEAL,
            self::HPP_RECUPPING_PAYMENT_METHOD_SEPADIRECTDEBIT =>    self::HPP_RECUPPING_PAYMENT_METHOD_SEPADIRECTDEBIT,

            self::HPP_RECUPPING_PAYMENT_METHOD_ALIPAY =>             self::HPP_RECUPPING_PAYMENT_METHOD_ALIPAY,
            self::HPP_RECUPPING_PAYMENT_METHOD_BANKTRANSFER =>       self::HPP_RECUPPING_PAYMENT_METHOD_BANKTRANSFER,
            self::HPP_RECUPPING_PAYMENT_METHOD_BANKTRANSFERIBAN =>   self::HPP_RECUPPING_PAYMENT_METHOD_BANKTRANSFERIBAN,
            self::HPP_RECUPPING_PAYMENT_METHOD_BANKTRANSFERNL =>     self::HPP_RECUPPING_PAYMENT_METHOD_BANKTRANSFERNL,
            self::HPP_RECUPPING_PAYMENT_METHOD_BANKRU =>             self::HPP_RECUPPING_PAYMENT_METHOD_BANKRU,
            self::HPP_RECUPPING_PAYMENT_METHOD_BOLETO =>             self::HPP_RECUPPING_PAYMENT_METHOD_BOLETO,
            self::HPP_RECUPPING_PAYMENT_METHOD_DIRECTDEBITNL =>      self::HPP_RECUPPING_PAYMENT_METHOD_DIRECTDEBITNL,
            self::HPP_RECUPPING_PAYMENT_METHOD_DOTPAY =>             self::HPP_RECUPPING_PAYMENT_METHOD_DOTPAY,
            self::HPP_RECUPPING_PAYMENT_METHOD_EBANKINGFI =>         self::HPP_RECUPPING_PAYMENT_METHOD_EBANKINGFI,
            self::HPP_RECUPPING_PAYMENT_METHOD_ELV =>                self::HPP_RECUPPING_PAYMENT_METHOD_ELV,
            self::HPP_RECUPPING_PAYMENT_METHOD_INTERAC =>            self::HPP_RECUPPING_PAYMENT_METHOD_INTERAC,
            self::HPP_RECUPPING_PAYMENT_METHOD_ONLINERU =>           self::HPP_RECUPPING_PAYMENT_METHOD_ONLINERU,
            self::HPP_RECUPPING_PAYMENT_METHOD_TERMINALRU =>         self::HPP_RECUPPING_PAYMENT_METHOD_TERMINALRU,
            self::HPP_RECUPPING_PAYMENT_METHOD_TRUSTLY =>            self::HPP_RECUPPING_PAYMENT_METHOD_TRUSTLY,
            self::HPP_RECUPPING_PAYMENT_METHOD_TRUSTPAY =>           self::HPP_RECUPPING_PAYMENT_METHOD_TRUSTPAY,
            self::HPP_RECUPPING_PAYMENT_METHOD_UNIONPAY =>           self::HPP_RECUPPING_PAYMENT_METHOD_UNIONPAY,
            self::HPP_RECUPPING_PAYMENT_METHOD_WALLETRU =>           self::HPP_RECUPPING_PAYMENT_METHOD_WALLETRU,

            self::HPP_RECUPPING_PAYMENT_METHOD_AMEX =>               self::HPP_RECUPPING_PAYMENT_METHOD_AMEX,
            self::HPP_RECUPPING_PAYMENT_METHOD_BCMC =>               self::HPP_RECUPPING_PAYMENT_METHOD_BCMC,
            self::HPP_RECUPPING_PAYMENT_METHOD_CUP =>                self::HPP_RECUPPING_PAYMENT_METHOD_CUP,
            self::HPP_RECUPPING_PAYMENT_METHOD_DINERS =>             self::HPP_RECUPPING_PAYMENT_METHOD_DINERS,
            self::HPP_RECUPPING_PAYMENT_METHOD_DISCOVER =>           self::HPP_RECUPPING_PAYMENT_METHOD_DISCOVER,
            self::HPP_RECUPPING_PAYMENT_METHOD_ELO =>                self::HPP_RECUPPING_PAYMENT_METHOD_ELO,
            self::HPP_RECUPPING_PAYMENT_METHOD_JCB =>                self::HPP_RECUPPING_PAYMENT_METHOD_JCB,
            self::HPP_RECUPPING_PAYMENT_METHOD_MAESTRO =>            self::HPP_RECUPPING_PAYMENT_METHOD_MAESTRO,
            self::HPP_RECUPPING_PAYMENT_METHOD_MC =>                 self::HPP_RECUPPING_PAYMENT_METHOD_MC,
            self::HPP_RECUPPING_PAYMENT_METHOD_VISA =>               self::HPP_RECUPPING_PAYMENT_METHOD_VISA,
            self::HPP_RECUPPING_PAYMENT_METHOD_VISADANKORT =>        self::HPP_RECUPPING_PAYMENT_METHOD_VISADANKORT,
            self::HPP_RECUPPING_PAYMENT_METHOD_VIAS =>               self::HPP_RECUPPING_PAYMENT_METHOD_VIAS,
            self::HPP_RECUPPING_PAYMENT_METHOD_MAESTROUK =>          self::HPP_RECUPPING_PAYMENT_METHOD_MAESTROUK,
            self::HPP_RECUPPING_PAYMENT_METHOD_SOLO =>               self::HPP_RECUPPING_PAYMENT_METHOD_SOLO,
            self::HPP_RECUPPING_PAYMENT_METHOD_LASER =>              self::HPP_RECUPPING_PAYMENT_METHOD_LASER,
            self::HPP_RECUPPING_PAYMENT_METHOD_BIJCARD =>            self::HPP_RECUPPING_PAYMENT_METHOD_BIJCARD,
            self::HPP_RECUPPING_PAYMENT_METHOD_DANKORT =>            self::HPP_RECUPPING_PAYMENT_METHOD_DANKORT,
            self::HPP_RECUPPING_PAYMENT_METHOD_HIPERCARD =>          self::HPP_RECUPPING_PAYMENT_METHOD_HIPERCARD,
            self::HPP_RECUPPING_PAYMENT_METHOD_UATP =>               self::HPP_RECUPPING_PAYMENT_METHOD_UATP,
            self::HPP_RECUPPING_PAYMENT_METHOD_CARTENBANCAIRE =>     self::HPP_RECUPPING_PAYMENT_METHOD_CARTENBANCAIRE,
            self::HPP_RECUPPING_PAYMENT_METHOD_VISAALPHABANKBONUS => self::HPP_RECUPPING_PAYMENT_METHOD_VISAALPHABANKBONUS,
            self::HPP_RECUPPING_PAYMENT_METHOD_MCALPHABANKBONUS =>   self::HPP_RECUPPING_PAYMENT_METHOD_MCALPHABANKBONUS,
            self::HPP_RECUPPING_PAYMENT_METHOD_QIWIWALLET =>         self::HPP_RECUPPING_PAYMENT_METHOD_QIWIWALLET,
            self::HPP_RECUPPING_PAYMENT_METHOD_SAFETYPAY =>          self::HPP_RECUPPING_PAYMENT_METHOD_SAFETYPAY,
            self::HPP_RECUPPING_PAYMENT_METHOD_MANDRY_CLICKPAY =>    self::HPP_RECUPPING_PAYMENT_METHOD_MANDRY_CLICKPAY,
            self::HPP_RECUPPING_PAYMENT_METHOD_DOKU_WALLET =>        self::HPP_RECUPPING_PAYMENT_METHOD_DOKU_WALLET,
            self::HPP_RECUPPING_PAYMENT_METHOD_OXXO =>               self::HPP_RECUPPING_PAYMENT_METHOD_OXXO,
            self::HPP_RECUPPING_PAYMENT_METHOD_SEVENELEVEN =>        self::HPP_RECUPPING_PAYMENT_METHOD_SEVENELEVEN,
        );
    }

    /**
     * @return bool|string
     */
    public static function getSessionValidity() {
        return date("c", strtotime("+1 days"));
    }

    /**
     * @return bool|string
     */
    public static function getShipBeforeDate() {
        return date("Y-m-d", strtotime("+3 days"));
    }

    /** Get all available payment methods
     *
     * @param $hppData
     *
     * @return bool|PayGatewayLogAdyenResRec
     */
    public function getPaymentMethods( $hppData )
    {
//        $paySupplierId = $this->getPaySupplierIdHpp();
        $paySupplierId = $this->getPaySupplierId();

        $this->errorCode = 0;

        $optionalData = $this->getOptionalData();

        //
        $amount =               HeMixed::roundAmount(self::formatAmount(HeMixed::getRoundAmount($hppData['paymentAmount'])), 0);
        $currency =             $this->getPaySupplierCodes($hppData['currencyCode']);
        $skinCode =             $hppData['skinCode'];
        $skinKey =              Yii::app()->config->get("PAY_ADYEN_HPP_SKIN_DEFAULT_KEY");
        $countryCode =          HeMixed::getCountryCodeIso2ForAdyen($hppData['countryCode']); // RU, ES,..

        $sessionValidity =      self::getSessionValidity();

        $reference =            "Request payment methods";
        $machine =              "";
        $fraudOffset =          "";
        $additionalData =       "";
        $merchantAccount =      Yii::app()->config->get("PAY_ADYEN_MERCHANT_ACCOUNT");

        $this->orderNumber =    "";

        $this->arrayRequest["idPayment"] =          null;
        $this->arrayRequest["dateSent"] =           strval(HeDate::todaySQL(true));
        $this->arrayRequest["merchantAccount"] =    $merchantAccount;
        $this->arrayRequest["amountValue"] =        $amount;
        $this->arrayRequest["amountCurrency"] =     $currency;
        $this->arrayRequest["reference"] =          $reference;
        $this->arrayRequest["shopperIP"] =          $machine;
        $this->arrayRequest["fraudOffset"] =        $fraudOffset;
        $this->arrayRequest["additionalData"] =     $additionalData;

        //
        // OBLIGATORIOS PARA RECURRINGS
        //
        $userId =               (isset(Yii::app()->user) ? Yii::app()->user->getId() : null);
        $shopperEmail =         "";
        $shopperReference =     "";
        $recurringContract =    "";
        $shopperInteraction =   "";


        $this->arrayRequest["userId"] =             $userId;
        $this->arrayRequest["shopperEmail"] =       $shopperEmail;
        $this->arrayRequest["shopperReference"] =   $shopperReference;
        $this->arrayRequest["recurringContract"] =  $recurringContract;
        $this->arrayRequest["shopperInteraction"] = $shopperInteraction;

        $this->arrayRequest["typeGateway"] =        self::ADYEN_TYPE_HPP_PAYMENT_GET_PAYMENT_METHODS;

        //
        $sendData['paymentAmount'] =        $amount;
        $sendData['currencyCode'] =         $currency;
        $sendData['merchantReference'] =    $reference;
        $sendData['skinCode'] =             $skinCode;
        $sendData['merchantAccount'] =      $merchantAccount;
        $sendData['sessionValidity'] =      $sessionValidity;
        $sendData['countryCode'] =          $countryCode;

        $sendData['hmacKey'] =              $skinKey;

        $sendData["merchantSig"] = base64_encode(pack("H*", hash_hmac(
            'sha1',
            $sendData["paymentAmount"] . $sendData["currencyCode"] . $sendData["merchantReference"] .
            $sendData["skinCode"] .  $sendData["merchantAccount"] . $sendData["sessionValidity"],
            $sendData["hmacKey"]
        )));

        //
        // Log SEND
        //
        $aLogRequest = new PayGatewayLogAdyenReqSent($paySupplierId);

        $aLogRequest->setXMLFieldsToProperties( $this->arrayRequest ) ;

        if (!$aLogRequest->insertRequestSent()) {
            $this->errorCode = self::ERROR_ABADB_LOGPAY;
            return false;
        }

        //
        $oHttpResponse = false;

        //
        // @TODO - get default allowed methods (without api)
        //
//        if ($this->connectToGateway) {
        try {
            $oHttpResponse = HeComm::sendHTTPPostCurlSimpleAdyen(Yii::app()->config->get("PAY_ADYEN_URL_PAYMENTMETHODS"), $sendData);
        }
        catch (Exception $e) {
            HeLogger::sendLog( " A communication to pay in Adyen has failed for get payment methods. ",
              HeLogger::PAYMENTS, HeLogger::CRITICAL,
              " An user was not able to finish his transaction, error curl probably= " . strval($e->getMessage()));

            $this->errorCode = self::ERROR_SOAP;
            return false;
        }
//        } else {
//            $oSoapResponse = $this->getSimulateResponse(self::ADYEN_SOAP_REQUEST_TYPE_AUTHORISE, $this->arrayRequest);
//        }

        if(!$oHttpResponse) {
            HeLogger::sendLog(" A communication to pay in Adyen has failed for get payment methods. ",
              HeLogger::IT_BUGS, HeLogger::CRITICAL,
              " An user was not able to finish his transaction, NO PAYMENT METHODS DATA");
            $this->errorCode = self::ERROR_SOAP;
            return false;
        }

        //
        $this->moLogResponse = $this->copyHttpResponseToPayGatewayResponse(self::ADYEN_TYPE_HPP_PAYMENT_GET_PAYMENT_METHODS, null, $oHttpResponse, $reference, $paySupplierId);

        if (!$this->moLogResponse->insertResponseReceived()) {
            $this->errorCode = self::ERROR_ABADB_LOGPAY;
            return false;
        }

        if (!$this->interpretHttpResponse($oHttpResponse)) {
            return false;
        }

        return $this->moLogResponse;
    }


    /**
     * @param $oHttpResponse
     *
     * @return bool
     */
    public function interpretHttpResponse($oHttpResponse) {

        $oHttpResponseDecoded = self::decodeHttpResponse($oHttpResponse);

        if(isset($oHttpResponseDecoded['paymentMethods']) AND is_array($oHttpResponseDecoded['paymentMethods'])) {

            $stPaymentMethods = array();

            foreach($oHttpResponseDecoded['paymentMethods'] as $iPmKey => $stPaymentMethod) {

                $stIssuers = array();

                if(isset($stPaymentMethod['issuers']) AND is_array($stPaymentMethod['issuers'])) {
                    foreach($stPaymentMethod['issuers'] as $iIsKey => $stIssuer) {
                        $stIssuers[$stIssuer['issuerId']] = array(
                            "issuerId" =>     $stIssuer['issuerId'],
                            "issuerName" =>   $stIssuer['name']
                        );
                    }
                }

                $stPaymentMethods[$stPaymentMethod['brandCode']] = array(
                    "brandCode" =>      $stPaymentMethod['brandCode'],
                    "brandName" =>      $stPaymentMethod['name'],
                    "brandIssuers" =>   $stIssuers,
                    "prices" =>         null,
                );
            }

            ksort($stPaymentMethods);

            $this->setAllowedMethods($stPaymentMethods);

            return true;
        }
        else {
            $this->errorCode =                  self::ADYEN_HTTP_RESULT_CODE_EMPTY_METHODS;
            $this->errorMsg[$this->errorCode] = self::ADYEN_HTTP_RESULT_CODE_EMPTY_METHODS;

            return false;
        }

        return false;
    }

    /** Interpret the SOAP response from Adyen, error codes basically. Send a warning in case of error.
     *
     * @param $oSoapResponse
     * @param AbaUser $moUser
     * @param $curPayment
     *
     * @return bool
     */
    public function interpretResponse($oSoapResponse, AbaUser $moUser, $curPayment, $bIsHpp=false) {

//        $oSoapResponse = self::decodeSoapXmlResponse($oSoapResponse);

        $refusalReason = (isset($oSoapResponse->paymentResult->refusalReason) ? trim(strval($oSoapResponse->paymentResult->refusalReason)): "");
        $resultCode =    (isset($oSoapResponse->paymentResult->resultCode) ? trim(strval($oSoapResponse->paymentResult->resultCode)): "");

        $sDescErrAdyen =    $this->getDescByRefusalReason($refusalReason);
        $sErrorCode =       $this->getErrorCodeByRefusalReason($refusalReason) . ' - ' . $resultCode;

        // The result of the payment. The possible values are Authorised, Refused, Error or Received (In case with a Dutch DirectDebit).
        if($resultCode == self::ADYEN_SOAP_RESULT_CODE_AUTHORISED) {
            return true;
        }
        elseif($resultCode == self::ADYEN_SOAP_RESULT_CODE_REFUSED) {
            $this->errorCode =                  $sErrorCode;
            $this->errorMsg[$this->errorCode] = $refusalReason;

            HeLogger::sendLog(HeLogger::PREFIX_NOTIF_L . ": User " . $moUser->email . " CREDIT CARD NOT AUTHORIZED ".
              "BY LA ADYEN, payment " . $curPayment->id."( ENVIRONMENT ".Yii::app()->config->get("SELF_DOMAIN").") ",
              HeLogger::PAYMENTS, HeLogger::INFO,
              " A user has tried to purchase a subscription with a CREDIT CARD NOT AUTHORIZED BY LA ADYEN, ".
              "payment id=" . $curPayment->id . " ( ENVIRONMENT " . Yii::app()->config->get("SELF_DOMAIN") . ") ".
              " ; user email=" . $moUser->email . "; id=" . $curPayment->userId .
              ", Reason=" . $this->errorCode . "-" . $this->errorMsg[$this->errorCode]);

            return false;
        }
        elseif($resultCode == self::ADYEN_SOAP_RESULT_CODE_ERROR) {
            $this->errorCode =                  $resultCode;
            $this->errorMsg[$this->errorCode] = $refusalReason;

            return false;
        }
        elseif($resultCode == self::ADYEN_SOAP_RESULT_CODE_RECEIVED) {

            if($bIsHpp) {
                return true;
            }

        }

        return false;
    }

    /**
     * @param $oSoapResponse
     * @param AbaUser $moUser
     * @param $curPayment
     *
     * @return bool
     */
    public function interpretResponseRefund($oSoapResponse, AbaUser $moUser, $curPayment) {

//        $oSoapResponse = self::decodeSoapXmlResponse($oSoapResponse);

        $additionalData =   (isset($oSoapResponse->refundResult->additionalData) ? trim(strval($oSoapResponse->refundResult->additionalData)): "");
        $pspReference =     (isset($oSoapResponse->refundResult->pspReference) ? trim(strval($oSoapResponse->refundResult->pspReference)): "");
        $response =         (isset($oSoapResponse->refundResult->response) ? trim(strval($oSoapResponse->refundResult->response)): "");

        if($pspReference != '' AND $response == self::ADYEN_SOAP_RESULT_SUCCESS_REFUND) {
            return true;
        }

        $sErrorCode =       self::ADYEN_SOAP_ERROR_CODE_REFUND;
        $sDescErrAdyen =    $this->getDescByRefusalReason($sErrorCode);

        $this->errorCode =                  $sErrorCode;
        $this->errorMsg[$this->errorCode] = $sDescErrAdyen;

        return false;
    }

    /**
     * @param $oSoapResponse
     * @param AbaUser $moUser
     * @param $curPayment
     *
     * @return bool
     */
    public function interpretResponseListRecurringDetails($oSoapResponse, AbaUser $moUser, $curPayment) {

//        $oSoapResponse = self::decodeSoapXmlResponse($oSoapResponse);

        $recurringDetailReference = (isset($oSoapResponse->result->details->RecurringDetail->recurringDetailReference) ? trim(strval($oSoapResponse->result->details->RecurringDetail->recurringDetailReference)) : "");

        $this->recurringDetailReference = $recurringDetailReference;

        if($recurringDetailReference != '') {
            return true;
        }

        $sErrorCode =       self::ADYEN_SOAP_ERROR_CODE_LIST_RECURRING_DETAILS;
        $sDescErrAdyen =    $this->getDescByRefusalReason($sErrorCode);

        $this->errorCode =                  $sErrorCode;
        $this->errorMsg[$this->errorCode] = $sDescErrAdyen;

        return false;
    }

    /**
     * @param $oSoapResponse
     * @param AbaUser $moUser
     * @param $curPayment
     *
     * @return bool
     */
    public function interpretResponseListRecurringDisable($oSoapResponse, AbaUser $moUser, $curPayment) {

//        $oSoapResponse = self::decodeSoapXmlResponse($oSoapResponse);

        if(isset($oSoapResponse->result->response) AND trim($oSoapResponse->result->response) == self::ADYEN_SOAP_RESULT_SUCCESS_CANCEL) {
            return true;
        }
        elseif(isset($oSoapResponse->result->response) AND trim($oSoapResponse->result->response) == self::ADYEN_SOAP_RESULT_SUCCESS_ALL_CANCEL) {
            return true;
        }

        $sErrorCode =       self::ADYEN_SOAP_ERROR_CODE_RECURRING_DISABLE;
        $sDescErrAdyen =    $this->getDescByRefusalReason($sErrorCode);

        $this->errorCode =                  $sErrorCode;
        $this->errorMsg[$this->errorCode] = $sDescErrAdyen;

        return false;
    }

    /**
     * @return mixed
     */
    public function getRecurringDetailReference() {
        return $this->recurringDetailReference;
    }

    /**
     * @return mixed
     */
    public function getOrderNumber() {
        return $this->orderNumber;
    }

    /** Returns id of translation specific for every payment gateway
     *
     * @return string
     */
    public function getListSpecificConditions() {
        return "";
    }

    /**
     * @return PayGatewayLogAdyenResRec
     */
    public function getMoLogResponse() {
        return $this->moLogResponse;
    }

    /** It is not necessary to change anything in the remote platform of Adyen. So we return true.
     *
     * @param AbaUserCreditForms $moOldUserCredit
     * @param AbaUserCreditForms $moNewUserCredit
     *
     * @return bool
     */
    public function runChangeCreditDetails(AbaUserCreditForms $moOldUserCredit, AbaUserCreditForms $moNewUserCredit) {
        return true;
    }

// !ak! - @TODO
// !ak! - @TODO - que currencies se permiten ?
// !ak! - @TODO
    protected function getPaySupplierCodes($ccyCode)
    {
        if ($ccyCode == "USD") {
            return $ccyCode;
        } elseif ($ccyCode == "EUR") {
            return $ccyCode;
        } else {
//            throw new CException("ABA, you can not pay with this currency from this country. Allowed currencies are USD and EUR only. Please contact support for more information.");
            return $ccyCode;
        }
        return $ccyCode;
    }

    public function setCsvResponsePath()
    {
        $this->csvResponse =    null;
        $this->reportPath =     Yii::app()->runtimePath . '/tmpAdyen_' . $this->fileReport . '_' . HeDate::todayNoSeparators(true) . '.csv';
    }

    /**
     * @return string
     */
    public function getCsvResponsePath()
    {
        return $this->reportPath;
    }

    /**
     * @TODO
     *
     * @return string
     */
    public function getCsvResponse()
    {
        $sUrlCsv = $this->getCsvResponsePath();

        return $sUrlCsv;
    }

    /**
     * @param string $dateRequest
     * @param string $dateRequestTo
     * @param string $transType
     * @param string $transMethod
     * @param bool|true $onlySuccess
     *
     * @return bool|PayGatewayLogAdyenResRec|string
     */
    public function runRequestReconciliation($dateRequest='', $dateRequestTo='', $transType='PAYMENT', $transMethod='CC', $onlySuccess=true) {
        return $this->opRunQuery($dateRequest);
    }

    /**
     * @param $idPayment
     * @param $operation
     * @param string $additionalData
     * @param string $selectedRecurringDetailReference
     *
     * @return bool|PayGatewayLogAdyenReqSent
     */
    protected function startSentLogOperation($idPayment, $operation, $additionalData='', $selectedRecurringDetailReference='')
    {
        $paySupplierId = $this->getPaySupplierId();

        $aLogRequest = new PayGatewayLogAdyenReqSent($paySupplierId);

        $aFieldsXML = array(
            "idPayment" =>                          $idPayment,
            "dateSent" =>                           HeDate::todaySQL(true),
            "typeGateway" =>                        $operation,
            "additionalData" =>                     $additionalData,
            "selectedRecurringDetailReference" =>   $selectedRecurringDetailReference,
        );
        $aLogRequest->setXMLFieldsToProperties( $aFieldsXML ) ;

        if (!$aLogRequest->insertRequestSent()) {
            $this->errorCode = self::ERROR_ABADB_LOGPAY;
            return false;
        }
        return $aLogRequest;
    }

    /**
     * @param $idPayment
     * @param $operation
     * @param $additionalData
     *
     * @return bool
     */
    protected function startResponseLogOperation($idPayment, $operation, $additionalData)
    {
        $paySupplierId = $this->getPaySupplierId();

        $oGwPayResponse = new PayGatewayLogAdyenResRec($paySupplierId);

        $aFieldsXML = array(
            "idPayment" =>          $idPayment,
            "dateSent" =>           HeDate::todaySQL(true),
            "resultCode" =>         $operation,
            "additionalData" =>     $additionalData,
        );

        $oGwPayResponse->setXMLFieldsToProperties( $aFieldsXML );

        $this->moLogResponse = $oGwPayResponse;

        //
        if (!$this->moLogResponse->insertResponseReceived()) {
            $this->errorCode = self::ERROR_ABADB_LOGPAY;
            return false;
        }
        return true;
    }

    /**
     * @param $periodFrom
     * @param $periodTo
     * @param string $transType
     * @param string $transMethod
     * @param bool $onlySuccess
     *
     * @return string|PayGatewayLogAdyenResRec
     */
    public function opRunQuery( $dateRequest, $transType='PAYMENT', $transMethod='CC', $onlySuccess=true)
    {
        if( !$this->connectToGateway ){
//            $aLogRequest = $this->startSentLogOperation('-', self::ADYEN_SOAP_SEND_TYPE_RECONCILIATION_FILE);
//            $this->errorCode = PayAllPagoCodes::ABAERR_DISABLED_COMMS;
//            $this->endLogOperation(false);
            return false;
        }

        $headers = array(
            'Content-Type: application/csv',
            'quiet',
            'no-check-certificate',
        );

        if( $this->connectToGateway ){

            $this->setCsvResponsePath();

            $merchantAccount =  Yii::app()->config->get("PAY_ADYEN_MERCHANT_ACCOUNT");
            $urlReports =       Yii::app()->config->get("PAY_ADYEN_URL_REPORTS");
            $reportsLogin =     Yii::app()->config->get("PAY_ADYEN_REPORT_LOGIN");
            $reportsPassword =  Yii::app()->config->get("PAY_ADYEN_REPORT_PASSWORD");
            $sUrl =             $urlReports . $merchantAccount . "/" . $this->fileReport . "_" . str_replace("-", "_", $dateRequest) . ".csv";

            $aLogRequest = $this->startSentLogOperation('-', self::ADYEN_SOAP_SEND_TYPE_RECONCILIATION_FILE, $sUrl, $this->getCsvResponsePath());

            //
            // guardar fichero temporal
            //
            $aResultComm = HeComm::sendHTTPCurlSimpleFile(
                $sUrl,
                $headers,
                $reportsLogin . ":" . $reportsPassword,
                $this->getCsvResponsePath()
            );

            if(isset($aResultComm['status'])) {
                $this->startResponseLogOperation("-", "[reconciliation-file-" . ($aResultComm['status'] === true ? "success" : "fail") . "]", $aResultComm['description']);
            }

        } else{
//
// @TODO - TEST FILE
//            $aResultComm = HeComm::sendHTTPPostCurlRetAllQuery("http://".Yii::app()->config->get("SELF_DOMAIN"), "/partnersimulators/paymentallpago/ctpe/1", $this->xmlRequest, $this->userAgent);
        }
//        $this->resultURL = $aResultComm["resultUrl"];
//        $this->info = $aResultComm["info"];

        $resultURL = $this->getCsvResponse();

        return $this->moLogResponse;
    }

    /** Parser CSV
     *
     * @param $resultURL
     * @param bool $isQuery
     *
     * @return string
     */
    protected function parserResult($resultURL, $isQuery = false)
    {
        //
        // @TODO - FICHERO CORRECTO !!!
        //
        // $resultXML = urldecode($resultURL);
        $this->xmlResponse = $resultURL;

        return true;
    }


    /** Executes the transaction through TPV of Adyen
     *  Create an initial recurring payment
     *
     * @param $curPayment
     * @param $hppData
     *
     * @return bool|PayGatewayLogAdyenResRec
     */
    public function processDebitingHpp($curPayment, $hppData)
    {
        $paySupplierId = $this->getPaySupplierId();

        //
        $this->errorCode = 0;

        //
        $moUser = new AbaUser();
        $moUser->getUserById($curPayment->userId);

        if (!is_numeric($moUser->id)) {
            $this->errorCode = self::ERROR_EMPRTY_USER;
            return false;
        }

        $optionalData = $this->getOptionalData();

        $this->moLogResponse = $this->copyHttpResponseToPayGatewayResponse(self::ADYEN_SOAP_REQUEST_TYPE_AUTHORISE,
          $curPayment, $hppData, $hppData['merchantReference'], $paySupplierId);

        //
        // update/insert response log
        if ($this->moLogResponse->getAdyenPaymentByUniqueIdAndPaymentId()) {
            $this->moLogResponse->updateNotifications();
        } else {
            if (!$this->moLogResponse->insertResponseReceived()) {
                $this->errorCode = self::ERROR_ABADB_LOGPAY;
                return false;
            }
        }

        //
        // PAYMENT
        //
        // Pre-Payment Adyen
        $oPaymentAdyen = new AbaPaymentsAdyen();
        $oPaymentAdyen->getAdyenPaymentByPaymentIdAndMerchantReference($curPayment->id, $hppData['merchantReference']);
        if (is_numeric($oPaymentAdyen->id) AND $oPaymentAdyen->id > 0) {
            $oPaymentAdyen->updateSuccess();
        }

        //
        // Aba Payment
        //
        $moPayment = new Payment();
        if ($moPayment->getPaymentById($curPayment->id)) {
            $errMsgAdyenConversion = ' Adyen HPP payment found but the payment draft(' . $curPayment->id . ') was already converted to SUCCESS OR WAS NOT FOUND. ' .
              'Payment real and with status =' . $curPayment->status . ' already in database. REVIEW IT ALL!';

            HeLogger::sendLog(" HPP Adyen Payment FAILED : " . $curPayment->id, HeLogger::IT_BUGS,
              HeLogger::CRITICAL, $errMsgAdyenConversion);

            return false;
        }

        //
        $foPayForm = new PaymentForm();
        $foPayForm->idPayMethod = PAY_METHOD_ADYEN_HPP;
        $foPayForm->mail = $moUser->email;
        $foPayForm->typeCpf = $curPayment->typeCpf;
        $foPayForm->cpfBrasil = $curPayment->cpfBrasil;
        $foPayForm->creditCardType = $curPayment->kind;
        $foPayForm->creditCardName = $curPayment->cardName;

        if (trim($curPayment->cardName) != '') {
            $foPayForm->firstName = $curPayment->cardName;
        } else {
            $foPayForm->firstName = $moUser->name;
        }
        $foPayForm->secondName = $moUser->surnames;


        //#
        $isExtend = 0;
        if(isset($oPaymentAdyen->isExtend) AND is_numeric($oPaymentAdyen->isExtend)) {
            $isExtend = $oPaymentAdyen->isExtend;
        }

        $commOnlPay = new OnlinePayCommon();
        $paySuccess = $commOnlPay->savePaymentCtrlCheckToUserPayment($foPayForm, $curPayment, $this->moLogResponse,
          $curPayment->idPartner, 1, $isExtend);

        if (!$paySuccess) {
            $errMsgAdyenConversion = 'Transformation from payment control check ' . $curPayment->id . ' to real payment and save credit card failed. ' .
              'Please review all data related with this adyen HPP payments. UserId=' . $curPayment->userId;

            HeLogger::sendLog(" HPP Adyen Payment FAILED : " . $curPayment->id, HeLogger::IT_BUGS,
              HeLogger::CRITICAL, $errMsgAdyenConversion);

            return false;
        }


        //
        $commUser = new UserCommon();
        $moPeriodicity = new ProdPeriodicity();
        $moPeriodicity->getProdPeriodById($curPayment->idPeriodPay);

        //
        $expirationDate = $commUser->getNextExpirationDate($moUser, $moPeriodicity);

//        if($oPaymentAdyen->isExtend == 1) {
//            $moPeriodicitySuccess = new ProdPeriodicity();
//            $moPeriodicitySuccess->getProdPeriodById($paySuccess->idPeriodPay);
//            $iMonthPeriod = $moPeriodicitySuccess->getPeriodicityMonth($paySuccess->idPeriodPay);
//            $expirationDate = HeDate::getDateAdd($expirationDate, 0, $iMonthPeriod);
//        }

        //
        // Recurring payment
        //
        $bCreateRecurring = false;

        if (isset($hppData['paymentMethod']) AND in_array(trim($hppData['paymentMethod']),
            self::getRecurringHppPaymentMethods())
        ) {
            if($oPaymentAdyen->isExtend == 0) {
                $bCreateRecurring = true;
            }
        }

        //
        $errMsgAdyen = '';

        //
        // Create next payment 1
        //
        if ($bCreateRecurring) {

            $nextPayment = $paySuccess->createDuplicateNextPayment();

            if (!$nextPayment) {
                $errMsgAdyen = 'Error on RECURRING ADYEN HPP payment PREPARED. PaymentID=' . $paySuccess->id . '. ' .
                  'Please review all data related with this adyen HPP payments. UserId=' . $paySuccess->userId;
            }

            $nextPayment->paySuppExtUniId = $paySuccess->paySuppExtUniId;
        }

        //
        // To Premium
        //
        $moUser = $commUser->convertToPremiumCron($moUser, $curPayment->idPartner, $expirationDate, PREMIUM);
        if (!$moUser) {
            $errMsgAdyenConversion = 'Conversion to Premium with this Adyen failed. ' .
              'Conversion to Premium with this Adyen failed. Please review all data related with this adyen HPP payment=' . $paySuccess->id . '. UserId=' . $moUser->id;

            HeLogger::sendLog(" HPP Adyen Payment FAILED : " . $curPayment->id, HeLogger::IT_BUGS,
              HeLogger::CRITICAL, $errMsgAdyenConversion);

            return false;
        }

        //
        // Create next payment 2
        //
        if ($bCreateRecurring) {

            $nextPayment->id = HeAbaSHA::generateIdPayment($moUser, $nextPayment->dateToPay);

            if (!$nextPayment->paymentProcess()) {
                $errMsgAdyen = 'Error on RECURRING ADYEN HPP payment CREATED. PaymentID=' . $paySuccess->id . '. ' .
                  'Please review all data related with this adyen HPP payments. UserId=' . $paySuccess->userId;
            } else {

                $moUserUpdate = new AbaUser();
                $moUserUpdate = clone($moUser);

                $moUserUpdate->expirationDate = $nextPayment->dateToPay;
                $aColumns2Upd = array('expirationDate');

                if (!$moUserUpdate->update($aColumns2Upd)) {
                    $errMsgAdyen = 'Error on expirationDate UPDATE. ExpirationDate=' . $nextPayment->dateToPay . '. ' .
                      'Please review all data related with this adyen HPP payments. UserId=' . $moUser->id;
                }
            }
        }

        //
        if(trim($errMsgAdyen) != '') {
            HeLogger::sendLog(" HPP Adyen Payment INCONSISTENCY : " . $paySuccess->id, HeLogger::IT_BUGS,
              HeLogger::CRITICAL, $errMsgAdyen);
        }


        //#5635
        $stPaymentInfo = $paySuccess->getPaymentInfoForSelligent();

        //
        $commConnSelligent = new SelligentConnectorCommon();
        $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::pagos,
          $moUser, array(
            "PROMOCODE" => $paySuccess->idPromoCode,
            "ISEXTEND" => $paySuccess->isExtend,
            "MONTH_PERIOD" => $stPaymentInfo['MONTH_PERIOD'],
            "PAY_DATE_START" => $stPaymentInfo['PAY_DATE_START'],
            "PAY_DATE_END" => $stPaymentInfo['PAY_DATE_END'],
            "AMOUNT_PRICE" => $stPaymentInfo['AMOUNT_PRICE'],
            "CURRENCY" => $stPaymentInfo['CURRENCY'],
            "PRICE_CURR" => $stPaymentInfo['PRICE_CURR'],
            "TRANSID" => $stPaymentInfo['TRANSID'],
          ),
          "processDebitingHpp");

        //#5087
        $commProducts = new ProductsPricesCommon();
        if ($commProducts->isPlanProduct($paySuccess->idProduct)) {
            $commConnSelligent = new SelligentConnectorCommon();
            // IDUSER_WEB, PRODUCT_TYPE
            $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::pagosPlan,
              $moUser, array(), "processDebitingHpp - adyen success payment notification");
        } else {
            $commConnSelligent = new SelligentConnectorCommon();
            $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::pagosPlanNotification,
              $moUser, array(), "processDebitingHpp - adyen success payment notification");
        }

        //
        $curPayment->updateStatusAfterPayment(PAY_SUCCESS, PAY_SUPPLIER_ADYEN_HPP, $this->moLogResponse->getUniqueId(),
          HeDate::today(true) . '-Offline payment through Adyen HPP confirmed');

        return $this->moLogResponse;
    }

}

