<?php
/**
 * Class PaymentFactory
 *
 * Decision maker class about payment gateway.
 *
 */
class PaymentFactory
{
    /** BALANCES for CREDIT CARDS PAYMENTS: *************************************************************
     *     Decides which processing payment will execute based on several criteria, only for Credit Cards.
     * In case ENABLE_PAY_ALLPAGO_GENERAL is FALSE, it means that we redirect all this platform to TPV CAIXA.
     * In case PAY_CONNECT_GATEWAY is FALSE, it means that we redirect all this platform to TPV CAIXA for simulation.
     *
     * @param AbaUser $moUser
     * @return int
     */
    public static function whichCardGatewayPay(AbaUser $moUser)
    {
        $idSupplier = PAY_SUPPLIER_CAIXA;
        $idCountry = intval(trim($moUser->countryId));
        $moCountrySupplier = new CountryPaymethodPaysupplier();
        if ($moCountrySupplier->getPaySupplierByMethodAndCountry($idCountry, PAY_METHOD_CARD)) {
            switch (intval($moCountrySupplier->idPaySupplier)) {
                case PAY_SUPPLIER_PAYPAL:
                    HeLogger::sendLog(
                      HeLogger::PREFIX_ERR_LOGIC_H . 'Country pays cards with PAYPAL',
                        HeLogger::IT_BUGS,
                        HeLogger::CRITICAL,
                        'There is a country ' . $idCountry .
                        ' that for a card payment is linked to PAYPAL supplier. That is impossible. ' .
                        'Please check it out. At the moment it will be redirected to LA CAIXA'
                    );
                    $idSupplier = PAY_SUPPLIER_CAIXA;
                    break;
                case PAY_SUPPLIER_ALLPAGO_BR:
                    if (intval(Yii::app()->config->get('ENABLE_PAY_ALLPAGO_GENERAL'))) {
                        $idSupplier = intval($moCountrySupplier->idPaySupplier);
                    }
                    break;
                case PAY_SUPPLIER_ALLPAGO_MX:
                    if (intval(Yii::app()->config->get('ENABLE_PAY_ALLPAGO_MX'))) {
                        $idSupplier = intval($moCountrySupplier->idPaySupplier);
                    }
                    break;

                case PAY_SUPPLIER_ADYEN:
                case PAY_SUPPLIER_ADYEN_HPP:
                    if (intval(Yii::app()->config->get('PAY_ADYEN_ENABLE_GENERAL'))) {
                        $idSupplier = intval($moCountrySupplier->idPaySupplier);
                    }
                    break;

                case PAY_SUPPLIER_CAIXA:
                default:
                    break;
            }

        }

        return $idSupplier;
    }

    /** For a single payment checks its supplier and returns the corresponding class: method + supplier
     *
     * @param PaymentControlCheck|Payment $moPayCtrlCheck
     * @param integer $paySupplierId
     *
     * @return PaySupplierLaCaixa|PaySupplierPayPal|PaySupplierAllPagoBr|PaySupplierAllPagoMx|PaySupplierAppstore|PaySupplierAdyen|PaySupplierAdyenHpp
     */
    public static function createPayment( $moPayCtrlCheck=NULL, $paySupplierId = NULL) {

        if (is_null($paySupplierId)) {
            $paySupplierId = $moPayCtrlCheck->paySuppExtId;
        }

        switch (intval($paySupplierId)) {
            case PAY_SUPPLIER_PAYPAL:
                return new PaySupplierPayPal();
                break;
            case PAY_SUPPLIER_ALLPAGO_BR:
                return new PaySupplierAllPagoBr();
                break;
            case PAY_SUPPLIER_ALLPAGO_MX:
                return new PaySupplierAllPagoMx();
                break;
            case PAY_SUPPLIER_CAIXA:
                return new PaySupplierLaCaixa();
                break;
            case PAY_SUPPLIER_ALLPAGO_BR_BOL:
                return new PaySupplierAllPagoBoleto();
                break;
            case PAY_SUPPLIER_APP_STORE:
                return new PaySupplierAppstore();
                break;
            case PAY_SUPPLIER_ADYEN:
                return new PaySupplierAdyen();
                break;
            case PAY_SUPPLIER_ADYEN_HPP:
                return new PaySupplierAdyenHpp();
                break;
            case PAY_SUPPLIER_ANDROID_PLAY_STORE:
                return new CmPaySupplierPlayStore();
                break;
            default:
                return new PaySupplierLaCaixa();
                break;
        }
    }

    /** Returns an array with list of cards available for the user
     * @param AbaUser $moUser
     * @param string $currency
     * @return array
     */
    public static function listCardsAvailable(AbaUser $moUser, $currency = NULL)
    {
        $payGateway = PaymentFactory::whichCardGatewayPay($moUser);
        $paySupplier = PaymentFactory::createPayment( NULL, $payGateway);

        return $paySupplier->getListOfCards($currency);
    }


    /** Returns an absolute array containing ids for all suppliers available for a certain method of payment.
     *
     * @param integer $idPayMethod Points to values in table [pay_methods]
     *
     * @return array|bool
     */
    public static function getAllSuppliersByMethod( $idPayMethod )
    {
        $moCountriesSuppliers = new CountryPaymethodPaysupplier();
        $aListSuppliers = $moCountriesSuppliers->getPaySuppliersByMethod( $idPayMethod );

        if(count($aListSuppliers)>0){
            return $aListSuppliers;
        }

        return false;
    }

    /** Return the id supplier for a method and country. It can only return ONE.
     * @param integer $idPayMethod
     * @param integer $idCountry
     *
     * @return bool|int
     */
    public static function getSupplierByMethodAndCountry( $idPayMethod, $idCountry )
    {
        $moSupplier = new CountryPaymethodPaysupplier();
        if ($moSupplier->getPaySupplierByMethodAndCountry( $idCountry, $idPayMethod )){
            return $moSupplier->idPaySupplier;
        }

        return false;
    }

    /** For a certain user -usually his country Id- returns the list of methods of payment available
     *
     * @param AbaUser $moUser
     *
     * @return $this|array|bool
     */
    public static function whichMethodsAvailable( AbaUser $moUser, $isPaymentPlus=true )
    {
        $moCountriesSuppliers = new CountryPaymethodPaysupplier();
        $aPayMethods =          $moCountriesSuppliers->getPayMethodsByCountryId( $moUser->countryId );

        if ($moUser->userType == PREMIUM AND $isPaymentPlus) {
            $paypalPos = array_search(PAY_METHOD_PAYPAL, $aPayMethods, true);
            if ($paypalPos >= 0){
                unset($aPayMethods[$paypalPos + 1]);
            }
        }

        if(count($aPayMethods) > 0){
            return $aPayMethods;
        }

        return false;
    }

    /** Returns multi-dimensional array with relationship between Methods of payment and fields to be displayed.
     *
     * @param array $aIdsPaysMethods
     * @param integer $countryId
     * @return array
     */
    public static function getAllFieldsByMethodByCountry( $aIdsPaysMethods, $countryId )
    {
        $aFields = array();
        if ( in_array( PAY_METHOD_CARD, $aIdsPaysMethods ) ){
            $aFields[''.PAY_METHOD_CARD] = array( 'creditCardType', 'creditCardName', 'creditCardNumber',
                                                  'creditCardMonth', 'creditCardYear', 'CVC' );
            if ($countryId==COUNTRY_BRAZIL){
                array_push( $aFields[''.PAY_METHOD_CARD], 'firstName', 'secondName', 'cpfBrasil', 'typeCpf' );
            }
            if ($countryId==COUNTRY_MEXICO){
                array_push( $aFields[''.PAY_METHOD_CARD], 'firstName', 'secondName', 'street', 'zipCode', 'city', 'state' );
            }
        }

        if ( in_array( PAY_METHOD_PAYPAL, $aIdsPaysMethods ) ){
            $aFields[''.PAY_METHOD_PAYPAL] = array();
            if ($countryId==COUNTRY_BRAZIL){
//                $aFields[''.PAY_METHOD_PAYPAL] = array( 'cpfBrasil', 'typeCpf', 'firstName', 'secondName' );
            }
        }

        if ( in_array( PAY_METHOD_BOLETO, $aIdsPaysMethods ) ){
            $aFields[''.PAY_METHOD_BOLETO] = array( 'firstName', 'secondName', 'street','zipCode', 'city', 'state' );
            if ($countryId==COUNTRY_BRAZIL){
                array_push( $aFields[''.PAY_METHOD_BOLETO], 'cpfBrasil', 'typeCpf' );
            }
        }

        return $aFields ;
    }

}

