<?php
/**
 *  Credit Card RG ff80808143d35c8f0143d8c780b00609
Credit Card DB(recurring) ff80808143d35c8f0143d8c875770611
Credit Card DB(initial) ff80808142faebd6014301b291890bef
 */
/**
 * Class PaySupplierAllPago
 *
 * PayAllPago - PHP class to integrate with Allpagos.com
 * NOTE: Requires PHP version 5 or later
 * @package AllPagos
 * @author Joaquim Forcada Jiménez
 */
class PaySupplierAllPagoMx extends PaySupplierAllPago
{
    public function __construct()
    {
        parent::__construct();

        $this->idPaySupplier = PAY_SUPPLIER_ALLPAGO_MX;
        $this->channel = Yii::app()->config->get("PAY_ALLPAGO_CHANNEL_INITIAL_MX");
        $this->channelInitial = Yii::app()->config->get("PAY_ALLPAGO_CHANNEL_INITIAL_MX");
        $this->channelRG = Yii::app()->config->get("PAY_ALLPAGO_CHANNEL_RG_MX");
        $this->channelRecurring = Yii::app()->config->get("PAY_ALLPAGO_CHANNEL_RECURRING_MX");
    }


    /** Returns an array of integer as a key, and string as a value. Each of implementations for this definition class
     * should have at least one element within the array.
     *
     * @return array
     */
    public function getListOfCards()
    {
        return array( KIND_CC_VISA => Yii::t('mainApp', 'VISA_w'),
            KIND_CC_MASTERCARD => Yii::t('mainApp', 'MASTERCARD_w'),
        );
    }

    /**
     * @param Payment|PaymentControlCheck|AbaUserCreditForms $objSpecialFields
     *
     * @return bool
     */
    public function setSpecialFields($objSpecialFields)
    {
        if(!is_array($this->specialFields)){
            $this->specialFields = array();
        }

        $this->specialFields['userId'] = $objSpecialFields->userId;
        return true;
    }

    /** These fields need to be filled in by the user.
     * @return array
     */
    public function getSpecialDisplayFields()
    {
        return array( 'firstName', 'secondName', 'street', 'zipCode', 'city', 'state' );
    }

    /**
     * @param float $payment_amount
     * @param string $payment_usage
     * @param string $identification_transactionid
     * @param string $payment_currency
     * @param string $reference_id
     * @internal param string $shopper_id
     */
    protected function setPaymentInformation($payment_amount, $payment_usage, $identification_transactionid,
                                             $payment_currency, $reference_id = '')
    {
        $this->payment_amount = $payment_amount;
        $this->payment_usage = $payment_usage;
        $this->identification_transactionid = $identification_transactionid;
        $this->shopper_id = $this->specialFields['userId'];
        $this->payment_currency = $payment_currency;
        if ($reference_id != '') {
            $this->reference_id = $reference_id;
        }
    }


    /** Sets the registration tag values for the XML that will be sent to ALLPAGO.
     *
     * @param string $identification_transactionid
     *
     * @return void
     */
    protected function setRegistrationId($identification_transactionid)
    {
        $this->identification_transactionid = $identification_transactionid;
        $this->shopper_id = $this->specialFields['userId'];
    }

    /** Special behavious for MEXICO payments.
     *
     * @param AbaUser $moUser
     * @param string $address_country
     * @return AbaUserAddressInvoice|bool
     */
    protected function setCustomerAddress(AbaUser $moUser, $address_country)
    {
        if( !parent::setCustomerAddress($moUser, $address_country) ) {
            $this->address_street="No street informed";
            $this->address_zip="No zip code informed";
            $this->address_city="No city informed";
            $this->address_state="No state informed";
        }

        return true;
    }


}

