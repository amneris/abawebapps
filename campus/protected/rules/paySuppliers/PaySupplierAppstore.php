<?php
/**
 * Class PaySupplierAppstore
 */
class PaySupplierAppstore extends PaySupplierCommon
{
    const NOERROR = 0;
    const ERROR_HTTP = 1001;
    const ERROR_ABADB_LOGPAY = 1002;
    const ERROR_DISABLED_COMMS = 1003;
    const ERROR_EMPTY_RESPONSE = 1004;
    const ERROR_NOT_VALID_RESPONSE = 1005;
    const ERROR_JSON_NOT_READ = 21000; /* The App Store could not read the JSON object you provided. */
    const ERROR_MALFORMED_DATA = 21002; /* The data in the receipt-data property was malformed. */
    const ERROR_RECEIPT_NOT_BE_AUTH = 21003; /* The receipt could not be authenticated. */
    const ERROR_SECRET = 21004; /* The shared secret you provided does not match the shared secret on file for your account. */
    const ERROR_SERVER_NOT_AVAILABLE = 21005; /* The receipt server is not currently available. */
    const ERROR_EXPIRED_RECEIPT = 21006; /* This receipt is valid but the subscription has expired. */
    const ERROR_SEND_SANDBOX_TO_PROD = 21007; /* This receipt is a sandbox receipt, but it was sent to the production server. */
    const ERROR_SEND_PROD_TO_SERVER = 21008; /* This receipt is a production receipt, but it was sent to the sandbox server. */

    const PAY_NOT_FOUND = "NOT_PAYMENT_FOUND";
    const LOG_PURCHASES_VERIFICATION = "RECEIPT-VERIFICATION";

    protected $connectToGateway;
    protected $url;
    protected $version;

    protected $jsonRequest;
    protected $jsonResponse;
    protected $errorCode;

//    /* @var $this->moLogResponse PayGatewayLogAppstore */
    protected $moLogResponse;

    protected $originalTransactionId;
    protected $orderNumber;
    protected $transactionId;
    protected $originalProductId;
    protected $productId;

    protected $errorMsg = array
    (
        PaySupplierAppstore::NOERROR => "Sin error",
        PaySupplierAppstore::ERROR_HTTP => "Error Http",
        PaySupplierAppstore::ERROR_ABADB_LOGPAY => "Registering payment in database raised an error",
        PaySupplierAppstore::ERROR_DISABLED_COMMS => "CONFIGURATION COMMUNICATIONS DISABLED",
        PaySupplierAppstore::ERROR_EMPTY_RESPONSE => "Empty response",
        PaySupplierAppstore::ERROR_JSON_NOT_READ => "The App Store could not read the JSON object you provided.",
        PaySupplierAppstore::ERROR_MALFORMED_DATA => "The data in the receipt-data property was malformed.",
        PaySupplierAppstore::ERROR_RECEIPT_NOT_BE_AUTH => "The receipt could not be authenticated.",
        PaySupplierAppstore::ERROR_SECRET => "The shared secret you provided does not match the shared secret on file for your account.",
        PaySupplierAppstore::ERROR_SERVER_NOT_AVAILABLE => "The receipt server is not currently available.",
        PaySupplierAppstore::ERROR_EXPIRED_RECEIPT => "This receipt is valid but the subscription has expired.",
        PaySupplierAppstore::ERROR_SEND_SANDBOX_TO_PROD => "This receipt is a sandbox receipt, but it was sent to the production server.",
        PaySupplierAppstore::ERROR_SEND_PROD_TO_SERVER => "This receipt is a production receipt, but it was sent to the sandbox server.",
    );

    const CURRENCY_EUR = 'EUR';
    const CURRENCY_USD = 'USD';
    const CURRENCY_MXN = 'MXN';
    const CURRENCY_BRL = 'BRL';

    protected $availableCurrenciesDefault = array(
        PaySupplierAppstore::CURRENCY_EUR => PaySupplierAppstore::CURRENCY_EUR,
        PaySupplierAppstore::CURRENCY_USD => PaySupplierAppstore::CURRENCY_USD,
        PaySupplierAppstore::CURRENCY_MXN => PaySupplierAppstore::CURRENCY_MXN,
        PaySupplierAppstore::CURRENCY_BRL => PaySupplierAppstore::CURRENCY_BRL,
    );

    public function __construct()
    {
        parent::__construct();

        $this->connectToGateway = (intval(Yii::app()->config->get("PAY_CONNECT_GATEWAY")) && intval(
              Yii::app()->config->get("PAY_APP_STORE_ENABLE_GENERAL")
          ) && intval(Yii::app()->config->get("PAY_APP_STORE_CONNECT_GATEWAY")));
        $this->url = Yii::app()->config->get("PAY_APP_STORE_API_URL");
        $this->version = Yii::app()->config->get("PAY_APP_STORE_API_VERSION");
    }

    /**
     * @return array
     */
    public function getAvailableCurrencies()
    {

        $availableCurrencies = explode(",", Yii::app()->config->get("PAY_APP_STORE_AVAILABLE_CURRENCIES"));
        $stAvailableCurrencies = array();

        if (count($availableCurrencies) > 0) {
            foreach ($availableCurrencies as $iKey => $stCurrency) {
                $stCurrency = trim($stCurrency);
                $stAvailableCurrencies[$stCurrency] = $stCurrency;
            }
            return $stAvailableCurrencies;
        }

        return $this->availableCurrenciesDefault;
    }

    /** Returns an array of integer as a key, and string as a value. Each of implementations for this definition class
     * should have at least one element within the array.
     *
     * @return array
     */
    public function getListOfCards()
    {
        return array();
    }

    /**
     * @return int
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return string
     */
    public function getErrorString()
    {
        if (isset($this->errorMsg[$this->errorCode])) {
            return $this->errorMsg[$this->errorCode];
        } else {
            return "ErrorCode " . $this->errorCode;
        }
    }

    /**
     * "https://sandbox.itunes.apple.com/verifyReceipt/";
     * "https://buy.itunes.apple.com/verifyReceipt";
     *
     * @param string $stAppStorePayment
     * @param string $dateRequestTo
     * @param string $transType
     * @param string $transMethod
     * @param bool|true $onlySuccess
     *
     * @return bool
     * @throws CException
     */
    public function runRequestReconciliation($stAppStorePayment='', $dateRequestTo='', $transType='PAYMENT', $transMethod='CC', $onlySuccess=true)
    {

        $receiptDecoded = json_decode($stAppStorePayment->purchaseReceipt, true);
        $latestReceipt = $receiptDecoded['latest_receipt'];
        $originalProductId = $receiptDecoded['latest_receipt_info']['product_id'];

        $this->setJsonRequest($latestReceipt);
        $this->setOriginalProductId($originalProductId);

        /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
        $this->startLogOperation($stAppStorePayment->id, $stAppStorePayment->userId, self::LOG_PURCHASES_VERIFICATION);

        if (!$this->connectToGateway) {
            $this->errorCode = self::ERROR_DISABLED_COMMS;
            $this->endLogOperation(false);
            return false;
        }

        $dataString = array(
            'receipt-data' => $latestReceipt,
            'password' => HeMixed::getApiPwdAppStore(),
        );


        //#hotfix-4.141.3
        $stJsonResponse = HeComm::sendHTTPPostCurlSimple(
          $this->url,
          array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen(json_encode($dataString))
          ),
          $dataString,
          true
        );

        if(is_array($stJsonResponse) AND isset($stJsonResponse["status"]) AND isset($stJsonResponse["description"])) {
            $stJsonResponse = json_encode($stJsonResponse);
        }


        $this->setJsonResponse($stJsonResponse);

        //
        // There is a parsing problem
        if (!$this->isValidReceipt()) {
            $this->errorMsg = (isset($this->errorMsg[$this->errorCode]) ? $this->errorMsg[$this->errorCode] : 'error');
            $this->endLogOperation(false);
//            echo "\n+++++++++++NOT VALID RECEIPT+++++++++++\n";
            return false;
        }

        $this->endLogOperation(true);

        return true;
    }

    /**
     * @param $jsonRequest
     */
    public function setJsonRequest($jsonRequest)
    {
        $this->jsonRequest = $jsonRequest;
    }

    /**
     * @param $jsonResponse
     */
    public function setJsonResponse($jsonResponse)
    {
        $this->jsonResponse = $jsonResponse;
    }

    /**
     * @param $jsonResponse
     */
    public function getJsonResponse()
    {
        return $this->jsonResponse;
    }

    /**
     * @return mixed
     */
    public function getOutput()
    {
        return $this->stOutput;
    }

    /**
     * @return int|mixed|string
     */
    public function getPaySupplierId()
    {
        if (!is_numeric($this->idPaySupplier)) {
            return PAY_SUPPLIER_APP_STORE;
        }
        return $this->idPaySupplier;
    }

    /**
     * @param int $idPaySupplier
     */
    public function setPaySupplierId($idPaySupplier = PAY_SUPPLIER_APP_STORE)
    {
        $this->idPaySupplier = $idPaySupplier;
    }

    /**
     * @param Payment|PaymentControlCheck $curPayment
     * @param string $transactionType
     * @param bool $isRecurringPay
     *
     * @return bool|PayGatewayLogAppstore
     */
    public function runDebiting($curPayment, $transactionType = 'A', $isRecurringPay = false)
    {
        $this->orderNumber = $curPayment->id . ":" . substr("" . date("Hms"), 3);

        return $this->getMoLogResponse();
    }

    /**
     * @return mixed
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @return mixed
     */
    public function getOriginalTransactionId()
    {
        return $this->originalTransactionId;
    }

    /**
     * @return mixed
     */
    public function getFirstTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @return mixed
     */
    public function getOriginalProductId()
    {
        return $this->originalProductId;
    }

    /**
     * @param $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @param $originalTransactionId
     */
    public function setOriginalTransactionId($originalTransactionId)
    {
        $this->originalTransactionId = $originalTransactionId;
    }

    /**
     * @param $transactionId
     */
    public function setFirstTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;
    }

    /**
     * @param $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @param $originalProductId
     */
    public function setOriginalProductId($originalProductId)
    {
        $this->originalProductId = $originalProductId;
    }

    /**
     * @param $oResponse
     *
     * @return mixed
     */
    public static function decodeResponse($oResponse)
    {
        return json_decode($oResponse, true);
    }

    /**
     * @return bool
     */
    public function loadInitialPaymentJson()
    {

        if (!$this->isValidReceipt()) {
            return false;
        }

//        $stResponseDecoded = self::decodeResponse($this->jsonResponse);
        $stResponseDecoded = self::decodeResponse($this->getJsonResponse());

        if ($stResponseDecoded['status'] != 0) {
            $this->errorCode = (isset($stResponseDecoded['status']) ? $stResponseDecoded['status'] : self::ERROR_EMPTY_RESPONSE);
            return false;
        }

        $this->errorCode = $stResponseDecoded['status'];

        if (!isset($stResponseDecoded['receipt']['original_transaction_id']) OR !isset($stResponseDecoded['receipt']['transaction_id']) OR !isset($stResponseDecoded['latest_receipt_info']['transaction_id'])) {
            return false;
        }

        // ALERT
        if ($stResponseDecoded['receipt']['transaction_id'] != $stResponseDecoded['latest_receipt_info']['transaction_id']) {

             HeLogger::sendLog("AppStore Receipt " . $stResponseDecoded['latest_receipt_info']['transaction_id'] . " MUST BE ORIGINAL RECEIPT " . $stResponseDecoded['receipt']['transaction_id'] . " AND IT IS A RENEVAL RECEIPT",
               HeLogger::PAYMENTS, HeLogger::CRITICAL, "");// INFOPROC_LAYOUT
        }

        $this->setOriginalTransactionId($stResponseDecoded['receipt']['original_transaction_id']);
        $this->setOrderNumber($stResponseDecoded['latest_receipt_info']['transaction_id']);
        $this->setFirstTransactionId($stResponseDecoded['receipt']['transaction_id']);
        $this->setProductId($stResponseDecoded['receipt']['product_id']);

        return true;
    }

    /**
     * @param $user
     *
     * @return bool
     */
    public function loadAppstorePendingPaymentJson($user)
    {

        $stResponseDecoded = self::decodeResponse($this->getJsonResponse());

        $this->errorCode = $stResponseDecoded['status'];

        if (!isset($stResponseDecoded['receipt']['original_transaction_id']) OR !isset($stResponseDecoded['receipt']['transaction_id']) OR !isset($stResponseDecoded['latest_receipt_info']['transaction_id'])) {
            return false;
        }

        $this->setOriginalTransactionId($stResponseDecoded['receipt']['original_transaction_id']);
        $this->setOrderNumber($stResponseDecoded['latest_receipt_info']['transaction_id']);
        $this->setFirstTransactionId($stResponseDecoded['receipt']['transaction_id']);
        $this->setProductId($stResponseDecoded['receipt']['product_id']);

        /*
            The values of the latest_receipt and latest_receipt_info keys are useful when checking whether an auto-renewable subscription is currently active.
            By providing any transaction receipt for the subscription and checking these values, you can get information about the currently-active subscription period.
            If the receipt being validated is for the latest renewal, the value for latest_receipt is the same as receipt-data (in the request)
                and the value for latest_receipt_info is the same as receipt.
        */
        if ($this->getOrderNumber() == $this->getFirstTransactionId()) {
            return false;
        }

        //
        // Check pending payment
        //
        $moPayment = new Payment();
        if (!$moPayment->getLastByPaySuppExtProfId($user->id, PAY_SUPPLIER_APP_STORE, $this->getOrderNumber())) {
            $moPayment->status = self::PAY_NOT_FOUND;
            return false;
        }

        if ($moPayment->status != PAY_PENDING) {
            return false;
        }

        return true;
    }

    /**
     * @param $originalTransactionId
     *
     * @return bool
     */
    public function validateAllReceiptsByOriginalTransactionId($originalTransactionId)
    {
        //
        // AL MENOS TIENE QUE HAVER UNO VALIDO (NO EXPIRADO)
        $isExpiredReceipt = true;

        //
        $abaPaymentsAppstore = new AbaPaymentsAppstore();
        $stAllPayments = $abaPaymentsAppstore->getPaymentsAppstoreByOriginalTransactionId(
            $originalTransactionId,
            PAY_APP_STORE_PAY_PENDING
        );

        if ($stAllPayments) {

            foreach ($stAllPayments as $stAppstorePayment) {

                $oAbaPaymentsAppstore = new AbaPaymentsAppstore();

                if (trim(
                      $stAppstorePayment["purchaseReceipt"]
                  ) != '' AND strpos(
                      $stAppstorePayment["purchaseReceipt"],
                      '{'
                  ) !== false AND $oAbaPaymentsAppstore->getPaymentAppstoreById($stAppstorePayment["id"])
                ) {
                    if ($this->runRequestReconciliation($oAbaPaymentsAppstore)) {
                        //
                        // Cancelled / Expired Receipt
                        //
                        if ($this->isExpiredReceipt()) {
                            //@TODO
                            // $oAbaPaymentsAppstore->getPaymentAppstoreById($stAppstorePayment["id"]);
                            // $oAbaPaymentsAppstore->updateToCancel();
                        } elseif ($this->isValidRenewalReceipt()) {

                            $isExpiredReceipt = false;
                            break;
                        }
                    }
                }
            }
        }

        return $isExpiredReceipt;
    }


    /**
     * @param AbaUser $moUser
     *
     * @return bool
     */
    public function validatePurchaseReceipt(AbaUser $moUser)
    {

        if (!$this->isValidReceipt()) {
            return false;
        }

        $this->loadInitialPaymentJson();

        $abaPaymentsAppstore = new AbaPaymentsAppstore();

        if ($abaPaymentsAppstore->checkPaymentAppstoreByOriginalTransactionId(
            $this->getOriginalTransactionId(),
            $moUser->id
        )
        ) {

            //#5692
            if (!$this->validateAllReceiptsByOriginalTransactionId($this->getOriginalTransactionId())) {

                HeLogger::sendLog("EXISTING AppStore receipt. There is already a user with this original_transaction_id: " . $this->getOriginalTransactionId(),
                  HeLogger::PAYMENTS, HeLogger::CRITICAL, "");

                //#4973#5501
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @return bool
     */
    public function isValidReceipt()
    {

        $stResponseDecoded = self::decodeResponse($this->getJsonResponse());

        if (!is_array($stResponseDecoded) OR count($stResponseDecoded) == 0 OR !isset($stResponseDecoded['status'])) {
            $this->errorCode = self::ERROR_EMPTY_RESPONSE;
            return false;
        }

        $this->errorCode = $stResponseDecoded['status'];

        if ($stResponseDecoded['status'] != self::NOERROR AND $stResponseDecoded['status'] != self::ERROR_EXPIRED_RECEIPT) {
            $this->errorCode = self::ERROR_NOT_VALID_RESPONSE . ":" . $stResponseDecoded['status'];
            return false;
        }
        if (!isset($stResponseDecoded['receipt'])) {
            $this->errorCode = self::ERROR_NOT_VALID_RESPONSE . ":" . $stResponseDecoded['status'];
            return false;
        }
        if ($stResponseDecoded['status'] == self::NOERROR AND !isset($stResponseDecoded['latest_receipt'])) {
            $this->errorCode = self::ERROR_NOT_VALID_RESPONSE . ":" . $stResponseDecoded['status'];
            return false;
        }
        if ($stResponseDecoded['status'] == self::ERROR_EXPIRED_RECEIPT AND !isset($stResponseDecoded['latest_expired_receipt_info'])) {
            $this->errorCode = self::ERROR_NOT_VALID_RESPONSE . ":" . $stResponseDecoded['status'];
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isExpiredReceipt()
    {

        if (!$this->isValidReceipt()) {
            return false;
        }

        $stResponseDecoded = self::decodeResponse($this->getJsonResponse());

        if (!isset($stResponseDecoded['latest_expired_receipt_info'])) {
            return false;
        }

        // 21006
        if ($stResponseDecoded['status'] == self::ERROR_EXPIRED_RECEIPT) {
            $this->errorCode = self::ERROR_EXPIRED_RECEIPT;
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isValidRenewalReceipt()
    {

        if (!$this->isValidReceipt()) {
            return false;
        }

        $stResponseDecoded = self::decodeResponse($this->getJsonResponse());

        if ($stResponseDecoded['status'] != self::NOERROR AND $stResponseDecoded['status'] != self::ERROR_EXPIRED_RECEIPT) {
            $this->errorCode = $stResponseDecoded['status'] . ":" . self::ERROR_NOT_VALID_RESPONSE;
            return false;
        }

        if (!isset($stResponseDecoded['latest_receipt_info']) OR !isset($stResponseDecoded['latest_receipt'])) {
            $this->errorCode = $stResponseDecoded['status'] . ":" . self::ERROR_NOT_VALID_RESPONSE;
            return false;
        }

        $this->errorCode = $stResponseDecoded['status'];

        return true;
    }


    /** Returns id of translation specific for every payment gateway
     *
     * @return string
     */
    public function getListSpecificConditions()
    {
        return "";
    }

    /**
     * @param Payment $curPayment
     * @param Payment $moPayOrigin
     *
     * @return bool|PayGatewayLogAppstore
     */
    public function runRefund(Payment $curPayment, Payment $moPayOrigin)
    {
        return $this->getMoLogResponse();
    }

    /** ++++++++++++Pay supplier log++++++++++++ */

    /**
     * @param $idPayment
     * @param $userId
     * @param $operation
     *
     * @return bool|int
     */
    protected function startLogOperation($idPayment, $userId, $operation)
    {
        $this->moLogResponse = new PayGatewayLogAppstore($this->idPaySupplier);

        $this->moLogResponse->idPaymentIos = $idPayment;
        $this->moLogResponse->userId = $userId;
        $this->moLogResponse->operation = $operation;
        $this->moLogResponse->dateRequest = HeDate::todaySQL(true);
        $this->moLogResponse->jsonRequest = $this->jsonRequest;
        $this->moLogResponse->errorCode = null;
        $this->moLogResponse->success = 0;
        $this->moLogResponse->idPaySupplier = $this->getPaySupplierId();

        return $this->moLogResponse->insertLog();
    }

    /**
     * @param $success
     * @return bool
     */
    protected function endLogOperation($success)
    {
        $this->moLogResponse->jsonResponse = $this->getJsonResponse();
        $this->moLogResponse->dateResponse = HeDate::todaySQL(true);
        $this->moLogResponse->errorCode = $this->errorCode;
        $this->moLogResponse->success = $success;

        return $this->moLogResponse->update(array("jsonResponse", "dateResponse", "errorCode", "success"));
    }

    /**
     * @return PayGatewayLogAppstore
     */
    public function getMoLogResponse()
    {
        return $this->moLogResponse;
    }

    /**
     * @return int
     */
    public static function getAppstoreRecurringPartner () {
        return PARTNER_ID_IOS_RECURRING;
    }


}
