<?php
/**
 * Class PaySupplierAdyenHpp
 */
class PaySupplierAdyenHpp extends PaySupplierAdyen
{
    /**
     * It must match the integer in the database
     *
     * @return int|mixed
     */
    public function getPaySupplierId() {
        return PAY_SUPPLIER_ADYEN_HPP;
    }

}

