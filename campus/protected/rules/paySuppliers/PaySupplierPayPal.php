<?php
/*
 * Primer redireccionamiento:
 * https://www.paypal.com/es/cgi-bin/merchantpaymentweb?cmd=_flow
 * &SESSION=krPUQ8mI0wV-smnfVBXw8MwPuLNxW36jSEQIZkq0-4Qxw1VTK7g_3_YpaXW&dispatch=50a222a57771920b6a3d7b606239e4d529b525e0b7e69bf0224adecfb0124e9b61f737ba21b0819882a9058c69cf92dcdac469a145272506#pageState=review
 * &pageDispatch=50a222a57771920b6a3d7b606239e4d529b525e0b7e69bf0224adecfb0124e9b61f737ba21b0819882a9058c69cf92dcdac469a145272506&pageSession=aEpO6qfKnRpZ5EeVdPH4cJlXI38OIJ5WfirFFs93bbWlwxQL1MSuYYpZSXG
 *
 * Confirmación de pago:
 * http://www.abaenglish.com/index.php?
 *  option=com_abacompra&
 *  task=paypalconfirm&
 *  Itemid=100091&
 *  lang=es&
 *  token=EC-6P238920LX033705K&
 *  PayerID=4KY82DY8VS4X2
 *
 * Ejecución de pago
 * http://www.abaenglish.com/index.php?option=com_abacompra&task=finalizarpayment&token=EC-6P238920LX033705K&payid=4KY82DY8VS4X2
 *
 *  @var EConfig Yii::app()->config
 *
 * */
/**
 *

<SOAP-ENV:Header>
    <RequesterCredentials xmlns=”urn:ebay:api:PayPalAPI” xsi:type=”ebl:CustomSecurityHeaderType”>
        <Credentials xmlns=”urn:ebay:apis:eBLBaseComponents” xsi:type=”ebl:UserIdPasswordType”>
            <Username>api_username</Username>
            <Password>api_password</Password>
            <Signature>api_signature</Signature>
            <Subject>authorizing_account_emailaddress</Subject>
        </Credentials>
    </RequesterCredentials>
</SOAP-ENV:Header>

 *
 */
class PaySupplierPayPal extends PaySupplierCommon
{
    const ERROR_HTTP =1001;
    const ERROR_ABADB_LOGPAY =1002;
    const ERROR_XML_PARSE =1003;

    const PURCHASE_CONCEPT = "English Course of ABA English";
    const PURCHASE_RECURRENT_CONCEPT = "English Course of ABA English";
    const RECURRENT_REPETITIONS = 0; //0 means unlimited, anyway PAYPAL for the Express Checkout mode only allows a maximum of 10 reptitions

    protected $orderNumber;
    protected $errorCode;

    /* @var soapPayPalClient $soapPayPalClient */
    protected $soapPayPalClient;
    protected $tokenPayPal;
    protected $payerIdPayPal;
    protected $infoPayPalUser;
    protected $infoPayment;
    protected $payLog;

    protected $errorMsg = array
    (
        // https://cms.paypal.com/es/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_errorcodes
        "Failure" => " Error communication with PayPal.",
        0=> "Sin error",
        101=>"Tarjeta caducada",
        1001=>"Error Http",
        1002=>"Registering payment in database raised an error",
        1003=>"XML response with our supplier of payments is not well formed",
        10417=>"The transaction cannot complete successfully. Instruct the customer to use an alternative payment method.",
    );
    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->arrayRequest = array();
        $this->errorCode = 0;
        $this->currentElement="";
    }

    /** Returns an array of integer as a key, and string as a value. Each of implementations for this definition class
     * should have at least one element within the array.
     *
     * @return array
     */
    public function getListOfCards()
    {
        return array(KIND_CC_PAYPAL=>Yii::t('mainApp', 'PAYPAL_w'));
    }


    /**
     * @return int
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return string
     */
    public function getErrorString ()
    {
        if(isset($this->errorMsg[$this->errorCode]))
        {
            return $this->errorMsg[$this->errorCode];
        }
        else{
            return "ErrorCode ".$this->errorCode;
        }
    }

    /**
     * It must match the integer in the database
     * Currently only there is La Caixa(1) and PayPal(2)
     * @return int|mixed
     */
    public function getPaySupplierId()
    {
        return PAY_SUPPLIER_PAYPAL;
    }

    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    public function getTokenPayPal()
    {
        return $this->tokenPayPal;
    }

    public function setTokenPayPal($tokenPayPal)
    {
        $this->tokenPayPal = $tokenPayPal;
    }

    /**
     * @param Payment|PaymentControlCheck $curPayment
     * @param string                      $transactionType
     * @param bool                        $isRecurringPay
     *
     * @return bool|PayGatewayLogPayPal
     */
    public function runDebiting( $curPayment, $transactionType='A', $isRecurringPay = false)
    {
        if ($this->connectToGateway) {
            if ($this->doExpressCheckout($curPayment)) {
                $this->payLog = $this->soapPayPalClient->moLogPaypalExpressCo;
                return $this->payLog;
            } else {
                return false;
            }
        } else {
            $this->payLog = new PayGatewayLogPayPal($this->getPaySupplierId());
            $this->payLog->setOrderNumber($this->getCustomField($curPayment->idProduct));
            return $this->payLog;
        }
    }

    /**
     * @param PaymentControlCheck $curPayment
     *
     * @return bool
     */
    public function doExpressCheckout( PaymentControlCheck $curPayment)
    {
        // $this->soapPayPalClient = new soapPayPalClient( Yii::app()->config->get("PAY_PAL_API_WSDL"), true, false, false, false, false, 50, 45 );
        $this->soapPayPalClient = new soapPayPalClient( Yii::app()->config->get("PAY_PAL_API_WSDL"), true );
        $this->soapPayPalClient->initPayPalConfig();
        $this->orderNumber = $this->getCustomField( $curPayment->idProduct );
        // @todo    Customize the order number to be able to track down the transaction later in PayPal
        //.'&amp;country='.$curPayment->idCountry.'&amp;periodicity='.$curPayment->idPeriodPay
        $bodyReq = '<DoExpressCheckoutPaymentReq xmlns="urn:ebay:api:PayPalAPI">
                        <DoExpressCheckoutPaymentRequest>
                          <Version xmlns="urn:ebay:apis:eBLBaseComponents">'.Yii::app()->config->get("PAY_PAL_API_VERSION").'</Version>
                            <DoExpressCheckoutPaymentRequestDetails xmlns="urn:ebay:apis:eBLBaseComponents">
                                <PaymentAction>Sale</PaymentAction>
                                <Token>'.$curPayment->tokenPayPal.'</Token>
                                <PayerID>'.$curPayment->payerPayPalId.'</PayerID>
                                <PaymentDetails>
                                    <OrderTotal currencyID="'.$curPayment->currencyTrans.'">'.$curPayment->amountPrice.'</OrderTotal>
                                    <Custom>'.$this->orderNumber.'</Custom>
                                    <PaymentDetailsItem>
                                        <Name>'.self::PURCHASE_CONCEPT.'</Name>
                                        <Quantity>1</Quantity>
                                        <Amount currencyID="'.$curPayment->currencyTrans.'">'.$curPayment->amountPrice.'</Amount>
                                    </PaymentDetailsItem>
                                </PaymentDetails>
                            </DoExpressCheckoutPaymentRequestDetails>
                        </DoExpressCheckoutPaymentRequest>
                    </DoExpressCheckoutPaymentReq>';

        $result = $this->soapPayPalClient->call("DoExpressCheckoutPayment", $bodyReq,
                                                                    $curPayment->userId, $curPayment->id);
        if( $this->soapPayPalClient->isSuccess()==true )
        {
            HeLogger::sendLog(HeLogger::PREFIX_NOTIF_L.": A new purchase with PAYPAL has been CONFIRMED, the payment id is ".$curPayment->id." from user ".Yii::app()->user->getEmail(),
              HeLogger::IT_BUGS, HeLogger::INFO,
              " A purchase with Paypal has ocurred, we want to follow the details of Express Checkout : ".$bodyReq);
            $this->infoPayment = $this->soapPayPalClient->getAResultado();
            $this->soapPayPalClient->moLogPaypalExpressCo->setOrderNumber($this->orderNumber);
            $this->infoPayment = $this->infoPayPalUser['DoExpressCheckoutPaymentResponseDetails']['PaymentInfo'];
            return true;
        }


        $this->errorCode = $this->soapPayPalClient->getAck();
        try{
            $errsPayPal = $this->soapPayPalClient->getErrores();
            if (strpos($errsPayPal, ",") > 0) {
                $errsPayPal = implode(", ", $errsPayPal);
            }

            $paypalErrCodes =array(10422, 10486);
            foreach($paypalErrCodes as $knownError){
                // The user has no credit or not enough balance in his credit card:
                if( intval($this->soapPayPalClient->getErrorCode())==$knownError ||
                    (   strpos(serialize( $result ), strval($knownError))>=1 &&
                        strpos(serialize( $result ), "ErrorCode")>=1) ) {
                    return false;
                }
            }
        }
        catch(Exception $e){
            $errsPayPal = " this->soapPayPalClient->getErrores, There has been a problem in DoExpressCheckout in PAypal";
        }

        $this->errorMsg[$this->errorCode]= $errsPayPal;
        HeLogger::sendLog(HeLogger::PREFIX_NOTIF_L." PayPal  do Express Checkout Failed connection for payment ".$curPayment->id,
          HeLogger::IT_BUGS, HeLogger::CRITICAL, " Details of Express Checkout : ". $bodyReq. ". Details of errors: ".serialize( $result ).
          " <br/> Check error code in https://cms.paypal.com/es/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_errorcodes ");

        return false;
    }

    /**
     * @param PaymentControlCheck $curPayment
     * @param string $urlReview
     * @param string $urlCancel
     *
     * @return bool
     */
    public function setExpressCheckout( PaymentControlCheck $curPayment, $urlReview = 'payments/reviewpayment', $urlCancel = 'payments/landcancelpaypal' )
    {
        $this->soapPayPalClient = new soapPayPalClient( Yii::app()->config->get("PAY_PAL_API_WSDL"), true );
        $this->soapPayPalClient->initPayPalConfig();

        $urlOkRedir = Yii::app()->createAbsoluteUrl($urlReview, array("paycontrolcheckid"=>$curPayment->id ));
        $urlCancelRedir = Yii::app()->createAbsoluteUrl($urlCancel, array("paycontrolcheckid"=>$curPayment->id ));

        // -->>>>>>> Description of the product to be shown detailed in the PAYPAL confirmation page ----
        $conceptRecurrentPayment = self::PURCHASE_RECURRENT_CONCEPT;
        $productDetail = new ProductPrice();
        $succ = $productDetail->getProductById( $curPayment->idProduct , 1) ;
        if($succ)
        {
            $prodPeriodicity = new ProdPeriodicity();
            $prodPeriodicity->getProdPeriodById( $curPayment->idPeriodPay );
        }
        /* Decide which country Landing Page to show in PayPal */
        /* check on
         https://developer.paypal.com/webapps/developer/docs/classic/api/merchant/SetExpressCheckout_API_Operation_SOAP/  */
        switch( strtoupper(Yii::app()->user->getLanguage()) )
        {
            case 'ES':
                $countryPaypal = 'ES' ;
                break;
            case 'EN':
                $countryPaypal = 'US' ;
                break;
            case 'PT':
                $countryPaypal = 'PT' ;
                break;
            case 'IT':
                $countryPaypal = 'IT' ;
                break;
            case 'FR':
                $countryPaypal = 'FR' ;
                break;
            case 'DE':
                $countryPaypal = 'DE' ;
                break;
            default:
                $countryPaypal = 'US';
                break;
        }
        // ----------------<<<<<

        $bodyReq = '<SetExpressCheckoutReq xmlns="urn:ebay:api:PayPalAPI">
                        <SetExpressCheckoutRequest>
                          <Version xmlns="urn:ebay:apis:eBLBaseComponents">'.Yii::app()->config->get("PAY_PAL_API_VERSION").'</Version>
                            <SetExpressCheckoutRequestDetails xmlns="urn:ebay:apis:eBLBaseComponents">
                                <ReturnURL>'.$urlOkRedir.'</ReturnURL>
                                <CancelURL>'.$urlCancelRedir.'</CancelURL>
                                <NoShipping>1</NoShipping>
                                <LocaleCode>'.$countryPaypal.'</LocaleCode>
                                <cpp-header-image>'.Yii::app()->config->get("PAY_PAL_URL_LOGO_ABA").'</cpp-header-image>
                                <cpp-header-border-color>FFFFFF</cpp-header-border-color>
                                <cpp-header-back-color>FFFFFF</cpp-header-back-color>
                                <cpp-payflow-color>FFFFFF</cpp-payflow-color>
                                <BillingAgreementDetails>
                                    <BillingType>RecurringPayments</BillingType>
                                    <BillingAgreementDescription>'.$conceptRecurrentPayment.'</BillingAgreementDescription>
                                </BillingAgreementDetails>
                                <PaymentDetails>
                                    <OrderTotal currencyID="'.$curPayment->currencyTrans.'">'.$curPayment->amountPrice.'</OrderTotal>
                                    <PaymentDetailsItem>
                                      <Name>'.self::PURCHASE_CONCEPT.'</Name>
                                      <Quantity>1</Quantity>
                                      <Amount currencyID="'.$curPayment->currencyTrans.'">'.$curPayment->amountPrice.'</Amount>
                                    </PaymentDetailsItem>
                                </PaymentDetails>
                            </SetExpressCheckoutRequestDetails>
                        </SetExpressCheckoutRequest>
				    </SetExpressCheckoutReq>';

        $result = $this->soapPayPalClient->call("SetExpressCheckout", $bodyReq, $curPayment->userId, $curPayment->id);
        if( $this->soapPayPalClient->isSuccess()==true )
        {
            $this->tokenPayPal = $this->soapPayPalClient->getTokenPayPal();
            return true;
        }

        // Communication error with PayPal:
        $this->errorCode = $this->soapPayPalClient->getAck(); // FAILURE
        $this->errorMsg[$this->errorCode]= $this->soapPayPalClient->getErrores();
        HeLogger::sendLog(" PayPal  setExpressCheckout Failed creation for payment ".$curPayment->id,
          HeLogger::IT_BUGS, HeLogger::CRITICAL,
          " Details of Express Checkout request: ".htmlentities($this->soapPayPalClient->getStrHeader())." ".htmlentities($bodyReq). " Response error= ".$this->soapPayPalClient->getErrores());
        return false;
    }

    /**
     * @param PaymentControlCheck $curPayment
     *
     * @return bool
     */
    public function getExpressCheckout( PaymentControlCheck $curPayment )
    {
        $this->soapPayPalClient = new soapPayPalClient( Yii::app()->config->get("PAY_PAL_API_WSDL"), true );
        $this->soapPayPalClient->initPayPalConfig();

        $bodyGetReq = '<GetExpressCheckoutDetailsReq xmlns="urn:ebay:api:PayPalAPI">
                        <GetExpressCheckoutDetailsRequest>
                                <Version xmlns="urn:ebay:apis:eBLBaseComponents">'.Yii::app()->config->get("PAY_PAL_API_VERSION").'</Version>
                                <Token>'.$curPayment->tokenPayPal.'</Token>
			  	            </GetExpressCheckoutDetailsRequest>
			            </GetExpressCheckoutDetailsReq>';

        // Llamamos al GetExpressCheckout para recuperar los datos de la compra:
        $aResult =  $this->soapPayPalClient->call("GetExpressCheckoutDetails", $bodyGetReq,
                                                                $curPayment->userId, $curPayment->id);
        if( $this->soapPayPalClient->isSuccess()==true )
        {
            $this->infoPayPalUser = $this->soapPayPalClient->getAResultado();
            $this->infoPayPalUser = $this->infoPayPalUser['GetExpressCheckoutDetailsResponseDetails']['PayerInfo'];
            /**
             * ['GetExpressCheckoutDetailsResponseDetails']['PayerInfo']['Payer']
             *                                                            PayerID
             *                                                            PayerStatus
                                                                          PayerName
                                                                          PayerCountry
             * ['GetExpressCheckoutDetailsResponseDetails']['Token']
             * ['GetExpressCheckoutDetailsResponseDetails']['BillingAgreementAcceptedStatus']
             *
             */
            return true;
        }

        $this->errorCode = $this->soapPayPalClient->getAck();
        $this->errorMsg[$this->errorCode]= $this->soapPayPalClient->getErrores();
        return false;

    }

    /** Alias of function createRecurringPayments()
     *
     * @param Payment $nextPayment
     * @param Payment|PaymentControlCheck $curPayment
     * @param AbaUser $moUser
     *
     * @return bool|PayGatewayLogPayPal
     */
    public function runCreateRecurring(Payment $nextPayment, $curPayment, AbaUser $moUser)
    {
        if($this->createRecurringPayments($nextPayment, $curPayment)){
            return $this->soapPayPalClient->moLogPaypalExpressCo;
        } else{
            return false;
        }
    }


    /** Recurring payments with Express Checkout also has the following limitations:
    - To be able to create a recurring payments profile for the buyer, the buyer’s PayPal account must include an active credit card.
    - At most ten recurring payments profiles can be created during checkout.
    - You can only increase the profile amount by 20% in each 180-day interval after the profile is created.     *
     * @param Payment             $nextPayment
     * @param PaymentControlCheck $curPayment
     *
     * @return bool
     */
    public function createRecurringPayments( Payment $nextPayment, PaymentControlCheck $curPayment )
    {
        if (!$this->connectToGateway) {
            $this->infoPayment = "TEST_FICTICIO";
            return true;
        }

        $this->soapPayPalClient = new soapPayPalClient( Yii::app()->config->get("PAY_PAL_API_WSDL"), true );
        $this->soapPayPalClient->initPayPalConfig();
        $this->orderNumber = $this->getCustomField( $curPayment->idProduct );

        $prodPeriodicity = new ProdPeriodicity();
        $prodPeriodicity->getProdPeriodById( $nextPayment->idPeriodPay );

        $billingPeriodUnit = 'Month';
        if($prodPeriodicity->months==0 && $prodPeriodicity->days>=1){
            $billingPeriodUnit = 'Day';
        }
        $BillingFrequency = $prodPeriodicity->months;
        if($prodPeriodicity->months==0 && $prodPeriodicity->days>=1){
            $BillingFrequency = ''.$prodPeriodicity->days;
        }

        $dateNextPayment = $nextPayment->dateToPay." 11:00:00";
        $bodyReq = '<CreateRecurringPaymentsProfileReq xmlns="urn:ebay:api:PayPalAPI">
					<CreateRecurringPaymentsProfileRequest>
					  	<Version xmlns="urn:ebay:apis:eBLBaseComponents">'.Yii::app()->config->get("PAY_PAL_API_VERSION").'</Version>
					  	<CreateRecurringPaymentsProfileRequestDetails xmlns="urn:ebay:apis:eBLBaseComponents">
							<Token>'.$curPayment->tokenPayPal.'</Token>
							<RecurringPaymentsProfileDetails>
								<BillingStartDate>'.$dateNextPayment.'</BillingStartDate>
								<ProfileReference>'.$this->orderNumber.'</ProfileReference>
								<FailedInitAmountAction>CancelOnFailure</FailedInitAmountAction>
							</RecurringPaymentsProfileDetails>
							<ScheduleDetails>
								<Description>'.self::PURCHASE_RECURRENT_CONCEPT.'</Description>
								<ActivationDetails>
								    <FailedInitAmountAction>CancelOnFailure</FailedInitAmountAction>
								</ActivationDetails>
								<MaxFailedPayments>1</MaxFailedPayments>
								<PaymentPeriod>
									<BillingPeriod>'.$billingPeriodUnit.'</BillingPeriod>
									<BillingFrequency>'.$BillingFrequency.'</BillingFrequency>
									<Amount currencyID="'.$nextPayment->currencyTrans.'">'.$nextPayment->amountPrice.'</Amount>
									<TotalBillingCycles>'.self::RECURRENT_REPETITIONS.'</TotalBillingCycles>
								</PaymentPeriod>
							</ScheduleDetails>
						</CreateRecurringPaymentsProfileRequestDetails>
				  	</CreateRecurringPaymentsProfileRequest>
				</CreateRecurringPaymentsProfileReq>';

        $aResult =  $this->soapPayPalClient->call( "CreateRecurringPaymentsProfile", $bodyReq,
                                                                        $curPayment->userId, $nextPayment->id );
        if( $this->soapPayPalClient->isSuccess()==true )
        {
            $this->infoPayment = $this->soapPayPalClient->getProfileId();
            $this->soapPayPalClient->moLogPaypalExpressCo->setOrderNumber($this->orderNumber);
//            $this->infoPayment = $this->infoPayPalUser['DoExpressCheckoutPaymentResponseDetails']['PaymentInfo'];
            return true;
        }
        else{
            HeLogger::sendLog(" PayPal  CreateRecurringPaymentsProfileReq Failed creation for payment ".$curPayment->id,
              HeLogger::IT_BUGS, HeLogger::CRITICAL, " Details of Express Checkout : ".$bodyReq);
        }
        $this->errorCode = $this->soapPayPalClient->getAck();
        $this->errorMsg[$this->errorCode]= $this->soapPayPalClient->getErrores();
        return false;
    }

    /**
     * @return mixed
     */
    public function getURLPayPalRedir()
    {
        $payPalURL = Yii::app()->config->get("PAY_PAL_URL_FRONT_END");
        $payPalURL = str_replace( "@TOKEN@", $this->tokenPayPal, $payPalURL ) ;

        return $payPalURL;
    }

    public function getInfoPayPalUser()
    {
        return $this->infoPayPalUser;
    }

    public function getInfoPayment()
    {
        return $this->infoPayment;
    }

    public function getCustomField( $idProduct )
    {
        return "usernamePP=".Yii::app()->user->getEmail().PAY_PAL_SEPARATOR."product=".$idProduct;
    }

    /** Returns id of translation specific for every payment gateway
     *
     * @return string
     */
    public function getListSpecificConditions()
    {
        return "";
    }

    /**
     * @param Payment $curPayment
     * @param Payment $moPayOrigin
     *
     * @return bool|PayGatewayLogPayPal
     */
    public function runRefund(Payment $curPayment, Payment $moPayOrigin)
    {
        // TODO: Implement runRefund() method.
        return false;
    }

    /**
     * @param Payment $moPayment
     * @param AbaUserCreditForms $moUserCredit
     * @return bool
     */
    public function runCancelRecurring(Payment $moPayment, AbaUserCreditForms $moUserCredit)
    {
        if (!$this->connectToGateway) {
            $this->infoPayment = "TEST_FICTICIO";
            return true;
        }

        $this->soapPayPalClient = new soapPayPalClient(Yii::app()->config->get("PAY_PAL_API_WSDL"), true);
        $this->soapPayPalClient->initPayPalConfig();
        $bodyReq = '<ManageRecurringPaymentsProfileStatusReq xmlns="urn:ebay:api:PayPalAPI">'.
					'<ManageRecurringPaymentsProfileStatusRequest>
					  	<Version xmlns="urn:ebay:apis:eBLBaseComponents">' .
                            Yii::app()->config->get("PAY_PAL_API_VERSION") .
                        '</Version>
                        <ManageRecurringPaymentsProfileStatusRequestDetails xmlns="urn:ebay:apis:eBLBaseComponents">
                            <ProfileID>' . $moUserCredit->registrationId . '</ProfileID>
                            <ManageRecurringPaymentsProfileStatusRequestType>
                                    <Action>Cancel</Action>
                                    <Note>Forced cancellation from Aba English.</Note>
                            </ManageRecurringPaymentsProfileStatusRequestType>
						</ManageRecurringPaymentsProfileStatusRequestDetails>
				  	 </ManageRecurringPaymentsProfileStatusRequest>
				    </ManageRecurringPaymentsProfileStatusReq>';

        $aResult = $this->soapPayPalClient->call("ManageRecurringPaymentsProfileStatus", $bodyReq,
                                                 $moUserCredit->userId, $moPayment->id);
        if ($this->soapPayPalClient->isSuccess()) {
            $this->infoPayment = $this->soapPayPalClient->getProfileId();
            return true;
        } else {
            HeLogger::sendLog(" PayPal  ManageRecurringPaymentsProfileStatus Failed updating subscription for payment " . $moPayment->id,
              HeLogger::IT_BUGS, HeLogger::CRITICAL,
              " Details error response = ". $this->soapPayPalClient->getErrores().". </br>".
              " Details of Express Checkout Request: </br>" . $bodyReq);
        }

        $this->errorCode = $this->soapPayPalClient->getErrorCode();
        $this->errorMsg[$this->errorCode] = $this->soapPayPalClient->getErrores();
        return false;
    }

    /**
     * @param int $userId
     * @param string $registrationId
     * @return bool|Payment
     */
    public function runGetSubscriptionInfo($userId, $registrationId)
    {
        if (!$this->connectToGateway) {
            $this->infoPayment = "TEST_FICTICIO";
            return true;
        }

        $this->soapPayPalClient = new soapPayPalClient(Yii::app()->config->get("PAY_PAL_API_WSDL"), true);
        $this->soapPayPalClient->initPayPalConfig();
        $bodyReq = '<GetRecurringPaymentsProfileDetailsReq xmlns="urn:ebay:api:PayPalAPI">'.
                     '<GetRecurringPaymentsProfileDetailsRequest>
                        <Version xmlns="urn:ebay:apis:eBLBaseComponents">' .
                            Yii::app()->config->get("PAY_PAL_API_VERSION") .
                        '</Version>
                        <ProfileID>' . $registrationId . '</ProfileID>
                        <GetRecurringPaymentsProfileDetailsRequestType>
                            <ProfileID>' . $registrationId . '</ProfileID>
						</GetRecurringPaymentsProfileDetailsRequestType>
				  	  </GetRecurringPaymentsProfileDetailsRequest>
				    </GetRecurringPaymentsProfileDetailsReq>';

        $remotePayment = new Payment();
        $aResult = $this->soapPayPalClient->call("GetRecurringPaymentsProfileDetails", $bodyReq, $userId);
        if ($this->soapPayPalClient->isSuccess()) {
            try{
            $remotePayment->userId = $userId;
            $remotePayment->paySuppExtProfId = $this->soapPayPalClient->getProfileId();
            switch(strtoupper($aResult["GetRecurringPaymentsProfileDetailsResponseDetails"]["ProfileStatus"])){
                case 'ACTIVEPROFILE':
                case 'ACTIVE':
                case 'PENDINGPROFILE':
                case 'ACTIVE':
                    $remotePayment->status = PAY_PENDING;
                    break;
                case 'CANCELLEDPROFILE':
                case 'CANCELLED':
                case 'EXPIREDPROFILE':
                case 'EXPIRED':
                    $remotePayment->status = PAY_CANCEL;
                    break;
                case 'SUSPENDEDPROFILE':
                case 'SUSPENDED':
                    $remotePayment->status = PAY_FAIL;
                    break;
            }
            $remotePayment->paySuppOrderId = $aResult["GetRecurringPaymentsProfileDetailsResponseDetails"]["RecurringPaymentsProfileDetails"]["ProfileReference"];
            if(array_key_exists("NextBillingDate", $aResult["GetRecurringPaymentsProfileDetailsResponseDetails"]["RecurringPaymentsSummary"])){
                $remotePayment->dateToPay = $aResult["GetRecurringPaymentsProfileDetailsResponseDetails"]["RecurringPaymentsSummary"]["NextBillingDate"];
                $remotePayment->dateToPay = HeDate::removeTimeFromSQL($remotePayment->dateToPay);
            }
            $remotePayment->currencyTrans = $aResult["GetRecurringPaymentsProfileDetailsResponseDetails"]["CurrentRecurringPaymentsPeriod"]["Amount"]["!currencyID"];
            $remotePayment->amountPrice = $aResult["GetRecurringPaymentsProfileDetailsResponseDetails"]["CurrentRecurringPaymentsPeriod"]["Amount"]["!"];
            } catch(Exception $e){
                echo " Parsing values from Paypal raise an error ".$e->getMessage();
            }

            return $remotePayment;
        } else {
            $remotePayment->status = 'NOT_FOUND';
        }

        $this->errorCode = $this->soapPayPalClient->getAck();
        $this->errorMsg[$this->errorCode] = $this->soapPayPalClient->getErrores();
        return false;
    }

    /**
     * @return PayGatewayLogPayPal
     */
    public function getMoLogResponse()
    {
        return $this->payLog;
    }
}
