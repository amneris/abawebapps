<?php
/**
 * Created by Joaquim Forcada (Quino) in Aba English with PHP Storm
 * User: Joaquim Forcada (Quino)
 * Date: 2/12/13
 * Time: 18:22
 
 ---Please brief description here---- 
 * @see http://support.ctpe.net/faq/doku.php
 *
 * PayAllPago - PHP class to integrate with Allpagos.com
 * NOTE: Requires PHP version 5 or later
 * @package AllPagos
 * @author Joaquim Forcada Jiménez

 */

class PayAllPagoCodes
{
    CONST PAY_CARD_DEBIT = "DC";
    CONST PAY_CARD_CREDIT = "CC";
    CONST PAY_BOLETO = "PP";
    
    CONST OP_DEBIT="DB";
    CONST OP_SCHEDULE="SD";
    CONST OP_PREAUTHORIZATION="PA";
    CONST OP_CHARGEBACKREVERSAL="CR";
    CONST OP_REREGISTER="RR";
    CONST OP_CONFIRMATION="CF";
    CONST OP_CREDIT="CD";
    CONST OP_RISKMANAGEMENT="RM";
    CONST OP_CHARGEBACK="CB";
    CONST OP_RECONCILE="RL";
    CONST OP_REGISTER="RG";
    CONST OP_REFUND="RF";
    CONST OP_RECEIPT="RC";
    CONST OP_THREATMETRIX="TM";
    CONST OP_DESCHEDULE="DS";
    CONST OP_DEREGISTER="DR";

    private static $aErrorDisplayable = array( '100.100.101'=>'invalid creditcard, bank account number or bank name',
                                        '100.100.700'=>'invalid cc number/brand combination',
                                        '600.200.500'=>'Invalid payment data. You are not configured for this currency or sub type (country or brand)',
                                        '100.100.304'=>'card not yet valid',
                                        '100.100.501'=>'invalid credit card brand',
                                        '800.100.151'=>'transaction declined (invalid card)',
                                        '800.100.152'=>'transaction declined by authorization system',
                                        '800.100.*'=>'transaction declined' );

    private static $aEquivalence = array(   'VISADEBIT'=> KIND_CC_VISA_ELECTRON,
                                    'POSTEPAY'=>  NULL,
                                    'PAYFAIR'=> NULL ,
                                    'CARDFINANS'=> NULL ,
                                    'VISA'=>  KIND_CC_VISA,
                                    'VISAELECTRON'=> KIND_CC_VISA_ELECTRON ,
                                    'WORLD'=> NULL ,
                                    'LASER'=> NULL ,
                                    'DISCOVER'=> KIND_CC_DISCOVER ,
                                    'SOLO'=> NULL ,
                                    'VPAY'=> KIND_CC_VISA ,
                                    '4B'=> NULL ,
                                    'SWITCH'=> NULL ,
                                    'JCB'=> KIND_CC_JCB ,
                                    'ELO'=> KIND_CC_ELO ,
                                    'MAXIMUM'=> NULL ,
                                    'PF_KARTE_DIRECT'=> NULL ,
                                    'DELTA'=> NULL ,
                                    'CHINAUNIONPAY'=> NULL ,
                                    'DANKORT'=> NULL ,
                                    'EURO6000'=> NULL ,
                                    'DINERS'=>  KIND_CC_DINERS_CLUB,
                                    'CARTEBLEUE'=> NULL ,
                                    'SERVIRED'=> NULL ,
                                    'ADVANTAGE'=> NULL ,
                                    'BONUS'=> NULL ,
                                    'CARTEBANCAIRE'=> NULL ,
                                    'AXESS'=> NULL,
                                    'AMEX'=> KIND_CC_AMERICAN_EXPRESS,
                                    'MAESTRO'=> NULL,
                                    'MASTER'=> KIND_CC_MASTERCARD,
                                    'BOLETO'=> KIND_CC_BOLETO,
                                    );

    CONST SAL_MR = "MR"; //=mister
    CONST SAL_MS = "MR"; //=miss
    CONST SAL_MRS = "MRS"; //=mistress
    CONST SAL_NONE = ""; //=NONE

    CONST ABAERR_ADDRESSDB = "001";
    CONST ABAERR_COMMXMLVALID = "002";
    CONST ABAERR_COMMTIMEOUT = "003";
    CONST ABAERR_DISABLED_COMMS = "004";
    CONST ABAERR_CREDIT_NOVALID = "005";

    public static $aErrors = array(self::ABAERR_ADDRESSDB=>"Address not found in database.",
                            self::ABAERR_COMMXMLVALID=>"XML parser could not parse or response is not XML valid.",
                            self::ABAERR_COMMTIMEOUT=>"Time out when connecting allpago.com",
                            self::ABAERR_DISABLED_COMMS=>"CONFIGURATION COMMUNICATIONS DISABLED",
                            self::ABAERR_CREDIT_NOVALID=>"Last payment no associated with this credit card details",);

    public static $aBrazilStates = array( 'AC'=>'Acre', 'AL'=>'Alagoas', 'AP'=>'Amapá',
    'AM'=>'Amazonas', 'BA'=>'Bahia','CE'=>'Ceará','DF'=>'Distrito Federal',
    'GO'=>'Goiás', 'ES'=>'Espírito Santo', 'MA'=>'Maranhão', 'MT'=>'Mato Grosso',
    'MS'=>'Mato Grosso do Sul', 'MG'=>'Minas Gerais', 'PA'=>'Pará','PB'=>'Paraíba','PR'=>'Paraná',
    'PE'=>'Pernambuco', 'PI'=>'Piauí','RJ'=>'Rio de Janeiro',
    'RN'=>'Rio Grande do Norte', 'RS'=>'Rio Grande do Sul', 'RO'=>'Rondônia',
    'RM'=>'Roraima', 'SC'=>'Santa Catarina', 'SP'=>'São Paulo',
    'SE'=>'Sergipe', 'TO'=>'Tocantins');

    /**
     * @param $kind
     * @return mixed|string
     */
    public static function retKindNameCard($kind)
    {
        $posNameCard = array_search(intval($kind), PayAllPagoCodes::$aEquivalence, true);
        if (!$posNameCard) {
            HeLogger::sendLog(
                HeLogger::PREFIX_NOTIF_H." Credit Card not found for AllPago",
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                "Card type $kind not found in list of credit cards available"
            );
            return "";
        }
        return $posNameCard;

    }

    /** Returns description error based on AllPago list of strings: https://test.ctpe.io/payment/codes/resultCodes.jsp
     * @param string $errorCode Format XXX.YYY.ZZZ
     * @return bool
     */
    public static function getErrorDescription($errorCode)
    {
        if ($errorCode=='600.200.500') {
            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_LOGIC_H.": Currency code wrong for ALLPAGO",
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                "The currency code for AllPago is only BRL. Please review logs in AllPagoLogs for this error code ".
                $errorCode
            );
        }

        if (array_key_exists($errorCode, self::$aErrorDisplayable)){
            return self::$aErrorDisplayable[$errorCode];
        } else {
            $supErrorCode = substr($errorCode, 0, 8).'*';
            if (array_key_exists($supErrorCode, self::$aErrorDisplayable)){
                return self::$aErrorDisplayable[$supErrorCode];
            }
            return false;
        }
    }

    /** Validates that the initials are in the official list of states of Brazil.
     *
     * @param string $stateInitials
     * @param string $stateName
     *
     * @return bool
     */
    public static function validBrazilState($stateInitials, $stateName='')
    {
        $allInitials = array_keys(self::$aBrazilStates);
        if (in_array($stateInitials, $allInitials) && $stateInitials!=='XX'){
            if($stateName==''){
                return true;
            }
        } else {
            return false;
        }

        if (in_array($stateName, self::$aBrazilStates)){
            return true;
        }

        return false;

    }
}