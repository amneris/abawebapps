<?php
/**
 *  Credit Card RG ff80808143d35c8f0143d8c780b00609
Credit Card DB(recurring) ff80808143d35c8f0143d8c875770611
Credit Card DB(initial) ff80808142faebd6014301b291890bef
 */
/**
 * Class PaySupplierAllPago
 *
 * PayAllPago - PHP class to integrate with Allpagos.com
 * NOTE: Requires PHP version 5 or later
 * @package AllPagos
 * @author Joaquim Forcada Jiménez

 * @property string $account_holder
 * @property string $account_brand
 * @property string $account_number
 * @property string $account_bankname
 * @property string $account_country
 * @property string $account_authorisation
 * @property string $account_verification
 * @property string $account_year
 * @property string $account_month
 *
 * @property string $payment_code
 * @property string $payment_amount
 * @property string $payment_usage
 * @property string $payment_currency
 *
 * @property string $idPayment
 * @property string $identification_transactionid
 * @property string $shopper_id
 * @property string $reference_id
 *
 * @property $account_registration After a RG operation we obtain this number to allow CB operations.
 *
 * @property string $contact_email
 * @property string $contact_mobile
 * @property string $contact_ip
 * @property string $contact_phone
 *
 * @property string $identity
 *
 * @property string $address_street
 * @property string $address_zip
 * @property string $address_city
 * @property string $address_state
 * @property string $address_country
 *
 * @property string $name_salutation
 * @property string $name_title
 * @property string $name_give
 * @property string $name_family
 * @property string $name_company
 */
abstract class PaySupplierAllPago extends PaySupplierCommon
{

	protected $server ;
    protected $path ;
    protected $queryPath;
    protected $sender ;
    protected $channel;

    protected $channelInitial ;
    protected $channelRecurring ;
    protected $channelRG ;

    protected $userid ;
    protected $userpwd ;

    protected $transaction_mode = "CONNECTOR_TEST";
    protected $transaction_response = "SYNC";

    protected $userAgent="php ctpepost";
    protected $request_version="1.0";

    /* @var PayGatewayLogAllpago $moLogAllpago */
    public $moLogAllpago;
    /* @var integer $idPaySupplier; Pay_supplier: Caixa, AllPago, Paypal, B2b, etc. */
    protected $idPaySupplier;

    protected $errorCode;
    protected $errorMsg;
    protected $resultURL=false;
    protected $info;

    public $xmlRequest;
    public $xmlResponse;

    protected $dateQueryFrom;
    protected $dateQueryUntil;

    /**
     * We read the configuration to access the platform from config database
     */
    public function __construct ()
    {
        parent::__construct();

        $this->connectToGateway = ( intval( Yii::app()->config->get("PAY_CONNECT_GATEWAY") ) &&
                                            intval( Yii::app()->config->get("PAY_ALLPAGO_CONNECT_GATEWAY") ));

        $this->server = Yii::app()->config->get("PAY_ALLPAGO_SERVER");
        $this->path = Yii::app()->config->get("PAY_ALLPAGO_PATH");
        $this->queryPath = Yii::app()->config->get("PAY_ALLPAGO_QUERYPATH");
        $this->sender = Yii::app()->config->get("PAY_ALLPAGO_SENDER");
        $this->userid = Yii::app()->config->get("PAY_ALLPAGO_USERID");
        $this->userpwd = Yii::app()->config->get("PAY_ALLPAGO_PWD");
        $this->transaction_mode = Yii::app()->config->get("PAY_ALLPAGO_TRANSTYPE");
        $this->transaction_response = "SYNC";
    }


    /** Returns one of the error codes listed in allpagos.com
     * @see https://test.ctpe.io/payment/codes/resultCodes.jsp
     *
     * @return string
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return string
     */
    public function getErrorString()
    {
        return $this->errorMsg;
    }

    /** Returns the id for this payment gateway. For allPago it is called Transaction.
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->identification_transactionid;
    }

    /**
     * @param Payment|PaymentControlCheck $curPayment
     * @param string $transactionType
     * @param bool $isRecurringPay
     *
     * @return bool|PayGatewayLogAllpago
     */
    public function runDebiting($curPayment, $transactionType = "CC.DB", $isRecurringPay = false)
    {
        $this->idPayment = $curPayment->id;

        $moUser = new AbaUser();
        $moUser->getUserById($curPayment->userId);

        $this->channel = $this->channelInitial;
        $retRun = $this->opRunDebit($curPayment, $moUser, $transactionType);

        if (!$isRecurringPay && $retRun) {
        } elseif(!$retRun){
        }

        return $retRun;
    }

    /** Alias of opRunRefund
     * @param Payment $curPayment
     * @param Payment $moPayOrigin
     *
     * @return bool|PayGatewayLogAllpago
     */
    public function runRefund(Payment $curPayment, Payment $moPayOrigin)
    {
        $this->idPayment = $curPayment->id;

        $retRun = $this->opRunRefund($curPayment, $moPayOrigin);
        if ($retRun) {
            return $retRun;
        } else{
            return false;
        }
    }

    /**
     * @param AbaUserCreditForms $moOldUserCredit
     * @param AbaUserCreditForms $moNewUserCredit
     *
     * @return bool
     */
    public function runChangeCreditDetails(AbaUserCreditForms $moOldUserCredit, AbaUserCreditForms $moNewUserCredit)
    {
        $retRun = $this->opRunReRegistration($moOldUserCredit, $moNewUserCredit);
        if ($retRun) {
            // SUCCESS changing credit card
            return $retRun;
        } else{
            return false;
        }

    }

    /**
     * @param Payment $moPayment
     * @param AbaUserCreditForms $moUserCredit
     *
     * @return bool
     */
    public function runCancelRecurring(Payment $moPayment, AbaUserCreditForms $moUserCredit)
    {
        $retRun = $this->opRunDeRegistration();
        if ($retRun) {
            return $retRun;
        } else{
            return false;
        }
    }


    /** Alias of opRunCredit, except payment code and type nothing else differs.
     *
     * @param PaymentControlCheck $curPayment
     * @param AbaUser $moUser
     * @param string $transactionType
     *
     * @return bool|PayGatewayLogAllpago
     */
    public function opRunDebit(PaymentControlCheck $curPayment, AbaUser $moUser, $transactionType = "CC.DB")
    {
        //#4848
        $tempCardData = $curPayment->getTempCardData();

        //
        $this->idPayment = $curPayment->id;
        $identityUser="";

        $moCountry = new AbaCountry($moUser->countryId);

        $moProduct = new ProductPrice();
        $moProduct->getProductById( $curPayment->idProduct, true);
        $prodDesc = strip_tags(Yii::t("mainApp", "Curso", array(), null, $moUser->langEnv)." ".
            Yii::t("mainApp", $moProduct->descriptionText, array(), null, $moUser->langEnv)." ".
            Yii::t("mainApp", "companyname_key") );

        $this->setSpecialFields($curPayment);

        // Credit card (uncomment if you use CC)
        /** @var $curPayment TYPE_NAME */
        $this->setAccountInformation(
            $tempCardData['cardName'],
            PayAllPagoCodes::retKindNameCard($curPayment->kind),
            $tempCardData['cardNumber'],
            '',
            $moCountry->iso,
            '',
            $tempCardData['cardCvc'],
            $tempCardData['cardYear'],
            $tempCardData['cardMonth']
        );

        $this->setPaymentCode($transactionType);

        $this->setPaymentInformation($curPayment->amountPrice, $prodDesc, $this->createTransactionId($curPayment->id, $curPayment->attempts), $curPayment->currencyTrans);

        $this->setCustomerContact($moUser->email, $moUser->telephone, $this->getIPLocation($moUser),
            $moUser->telephone);
        $this->setCustomerDetails($identityUser);
        $this->setCustomerAddress($moUser, $moCountry->iso);

        $firstName = $curPayment->getFirstNameTmp();
        if ( $firstName==''){
            $firstName = $moUser->name;
        }
        $secondName = $curPayment->getSecondNameTmp();
        if ( $secondName==''){
            $secondName = $moUser->name;
        }

        $this->setCustomerName(PayAllPagoCodes::SAL_MR, '', $firstName, $secondName, '');

        return $this->opRunComms($moUser);
    }

    /** Alias of opRunDebitOnRG
     *
     * @param $moPayCtrlCheck
     * @param AbaUser $moUser
     * @param $moUserCredit
     * @return bool|PayGatewayLogAllpago
     */
    public function runDebitingRenewal( $moPayCtrlCheck, AbaUser $moUser, $moUserCredit )
    {
        return $this->opRunDebitOnRG( $moPayCtrlCheck, $moUser, $moUserCredit);
    }

    /** Sends recurring payments to AllPago. Based on the uniqueId of Registration operation.
     * @param PaymentControlCheck $curPayment
     * @param AbaUser $moUser
     * @param AbaUserCreditForms $moUserCredit
     * @param string $transactionType
     * @return bool|PayGatewayLogAllpago
     */
    public function opRunDebitOnRG(PaymentControlCheck $curPayment,  AbaUser $moUser,
                                   AbaUserCreditForms $moUserCredit, $transactionType = "CC.DB")
    {
        $this->idPayment = $curPayment->id;

        $moProduct = new ProductPrice();
        $moProduct->getProductById( $curPayment->idProduct, true);
        $prodDesc = strip_tags( Yii::t("mainApp", "Pago Recurrente", array(), null, $moUser->langEnv)." ".
                                Yii::t("mainApp", "Curso", array(), null, $moUser->langEnv)." ".
                                Yii::t("mainApp", $moProduct->descriptionText, array(), null, $moUser->langEnv)." ".
                                Yii::t("mainApp", "companyname_key") );

        $this->setSpecialFields($curPayment);

        // Credit card (uncomment if you use CC)
        $this->setAccountRegistration($moUserCredit->registrationId);

        $this->setPaymentCode($transactionType);

        $this->setPaymentInformation($curPayment->amountPrice, $prodDesc, $this->createTransactionId($curPayment->id), $curPayment->currencyTrans);

        $this->channel = $this->channelRecurring;
        return $this->opRunComms($moUser);
    }

    /** Alias of function opRunRegistration()
     *
     * @param Payment $nextPayment
     * @param PaymentControlCheck $curPayment
     * @param AbaUser $moUser
     * @return bool|PayGatewayLogAllpago
     */
    public function runCreateRecurring(Payment $nextPayment, $curPayment, AbaUser $moUser)
    {
        return $this->opRunRegistration( $nextPayment->id, $curPayment, $moUser);
    }


    /**
     * @param string $idNext
     * @param PaymentControlCheck $curPayment
     * @param AbaUser $moUser
     * @return bool|PayGatewayLogAllpago
     */
    public function opRunRegistration( $idNext, $curPayment, AbaUser $moUser)
    {
        $this->idPayment = $curPayment->id;

        $tempCardData = $curPayment->getTempCardData();

        $moUser->refresh();
        $moCountry = new AbaCountry($moUser->countryId);
        $this->setSpecialFields($curPayment);
        $this->setPaymentCode(PayAllPagoCodes::PAY_CARD_CREDIT.".".PayAllPagoCodes::OP_REGISTER);

//        $this->setAccountInformation($curPayment->cardName, PayAllPagoCodes::retKindNameCard($curPayment->kind), $curPayment->cardNumber, '', $moCountry->iso,'',$curPayment->cardCvc, $curPayment->cardYear, $curPayment->cardMonth);
        $this->setAccountInformation(
            $tempCardData['cardName'],
            PayAllPagoCodes::retKindNameCard($curPayment->kind),
            $tempCardData['cardNumber'],
            '',
            $moCountry->iso,
            '',
            $tempCardData['cardCvc'],
            $tempCardData['cardYear'],
            $tempCardData['cardMonth']
        );

        $this->setRegistrationId( $idNext );
        $this->setCustomerContact($moUser->email,$moUser->telephone, $this->getIPLocation($moUser), $moUser->telephone);
        $this->setCustomerAddress($moUser, $moCountry->iso);
        $this->setCustomerName(PayAllPagoCodes::SAL_MR, '', $moUser->name, $moUser->surnames, '');
        $this->channel = $this->channelRG;
        return $this->opRunComms($moUser);
    }

    /**
     * Alias runCancelRecurring
     *
     * @return bool
    */
    public function opRunDeRegistration()
    {
        // @TODO: To be executed when executing a cancellation
        // Probably will be called from Intranet through a web service
        return false;
    }

    /**
     * @param AbaUserCreditForms $moOldUserCredit
     * @param AbaUserCreditForms $moNewUserCredit
     * @param string $transactionType
     *
     * @return bool|PayGatewayLogAllpago
     */
    public function opRunReRegistration(AbaUserCreditForms $moOldUserCredit, AbaUserCreditForms $moNewUserCredit,
                                                                    $transactionType = "CC.RR")
    {



//
//
// @TODO - CHANGE CARD DATA ALLPAGO - FUERA CARD DATA !!!
// @TODO - CHANGE CARD DATA ALLPAGO - FUERA CARD DATA !!!
// @TODO - CHANGE CARD DATA ALLPAGO - FUERA CARD DATA !!!
//
//


        $this->setSpecialFields($moNewUserCredit);

        $moUser = new AbaUser();
        $moUser->getUserById($moOldUserCredit->userId);
        $moCountry = new AbaCountry($moUser->countryId);

        // Set XML tags:
        $this->setAccountInformation($moNewUserCredit->cardName,
                                     PayAllPagoCodes::retKindNameCard($moNewUserCredit->kind),
                                     $moNewUserCredit->cardNumber, '', $moCountry->iso, '', $moNewUserCredit->cardCvc,
                                     $moNewUserCredit->cardYear, $moNewUserCredit->cardMonth);

        $this->idPayment = $this->createTransactionId(HeAbaSHA::generateIdPayment($moUser));
        // PENDING PAYMENT
        $moLastPayment = new Payment();
        if ( $moLastPayment->getLastPayPendingByUserId($moUser->id, false) ){
            $this->idPayment = $moLastPayment->id;
        }

        $this->setSpecialFields( $moNewUserCredit );
        $this->setRegistrationId( $this->idPayment );
        $this->reference_id = $moOldUserCredit->registrationId;

        $this->setPaymentCode($transactionType);
        $this->channel = $this->channelRG;
        return $this->opRunComms($moUser);
    }

    /**
     * @param Payment $curPayment
     * @param Payment $moPayOrigin
     * @param string $transactionType
     *
     * @return bool|PayGatewayLogAllpago
     */
    public function opRunRefund(Payment $curPayment, Payment $moPayOrigin, $transactionType = "CC.RF")
    {
        $this->idPayment = $curPayment->id;

        $moUser = new AbaUser();
        $moUser->getUserById($moPayOrigin->userId);

        $moProduct = new ProductPrice();
        $moProduct->getProductById( $curPayment->idProduct, true);
        $prodDesc = strip_tags( Yii::t("mainApp", "w_abono", array(), null, $moUser->langEnv)." ".
                                Yii::t("mainApp", "Curso", array(), null, $moUser->langEnv)." ".
                                Yii::t("mainApp", $moProduct->descriptionText, array(), null, $moUser->langEnv)." ".
                                Yii::t("mainApp", "companyname_key") );
        $moUserCredit = new AbaUserCreditForms($moUser->id);
        $this->setSpecialFields($moUserCredit);

        $this->setPaymentCode($transactionType);
        $this->setPaymentInformation($curPayment->amountPrice, $prodDesc,
                                                $this->createTransactionId($curPayment->paySuppOrderId),
                                                $curPayment->currencyTrans, $moPayOrigin->paySuppExtUniId);
        if ($moPayOrigin->isRecurring ){
            $this->channel = $this->channelRecurring;
        }else{
            $this->channel = $this->channelInitial;
        }

        return $this->opRunComms($moUser);

    }

    /**
     * Request group of transactions from allPago. The goal is to reconcile payments.
     */

    /**
     * @param $periodFrom
     * @param $periodTo
     * @param string $transType
     * @param string $transMethod
     * @param bool $onlySuccess
     *
     * @return string|PayGatewayLogAllpago
     */
    public function opRunQuery( $periodFrom, $periodTo, $transType='PAYMENT', $transMethod='CC', $onlySuccess=true)
    {
        // @TODO To request transactions between date range
        $this->dateQueryFrom = $periodFrom;
        $this->dateQueryUntil = $periodTo;
        $this->commitXMLQuery();

        $this->startLogOperation('', 'QUERY', '1');
        if( !$this->connectToGateway ){
            $this->errorCode = PayAllPagoCodes::ABAERR_DISABLED_COMMS;
            $this->endLogOperation(false);
            return false;
        }

        if( $this->connectToGateway ){
            $aResultComm = HeComm::sendHTTPPostCurlRetAllQuery($this->server, $this->queryPath,
                                                          $this->xmlRequest, $this->userAgent);
        } else{
            $aResultComm = HeComm::sendHTTPPostCurlRetAllQuery("http://".Yii::app()->config->get("SELF_DOMAIN"),
                "/partnersimulators/paymentallpago/ctpe/1",
                $this->xmlRequest, $this->userAgent);
        }
        $this->resultURL = $aResultComm["resultUrl"];
        $this->info = $aResultComm["info"];

        if ($this->resultURL) {
            $output = $this->parserResult($this->resultURL, true);
            if ($output) {
                if ($output == "ACK") {
                    // Everything is OK
                    $this->endLogOperation(true);
                    return $this->moLogAllpago;
                } else {
                    $this->endLogOperation(false);
                    return false;
                }
            } else {
                // There is a parsing problem
                $this->errorCode = PayAllPagoCodes::ABAERR_COMMXMLVALID;
                $this->errorMsg = PayAllPagoCodes::$aErrors[$this->errorCode];
                $this->endLogOperation(false);
                return false;
            }
        } else {
            // There is a connection-problem
            $this->errorCode = PayAllPagoCodes::ABAERR_COMMTIMEOUT;
            $this->errorMsg = $aResultComm["error"]." or maybe there was a ".PayAllPagoCodes::$aErrors[$this->errorCode];
            $this->xmlResponse = $aResultComm["error"];
            $this->endLogOperation(false);
            return false;
        }
    }

    /** Sets the payment header tag for the XML sent to ALLPAGO.
     * It contains small differences between Brazil, Mexico and probably BOLETO BRAZIL.
     *
     * @param float $payment_amount
     * @param string $payment_usage
     * @param string $identification_transactionid
     * @param string $payment_currency
     * @param string $shopper_id
     * @param string $reference_id
     *
     */
    protected abstract function setPaymentInformation($payment_amount, $payment_usage, $identification_transactionid,
                                             $payment_currency, $reference_id = '');

    /**
     * @param AbaUser $moUser
     *
     * @return bool|PayGatewayLogAllpago
     */
    protected function opRunComms(AbaUser $moUser)
    {
        $this->commitXMLPayment();

        $this->startLogOperation($this->idPayment, $this->payment_code, $moUser->getId());
        if ( !$this->connectToGateway ){
            $this->errorCode = PayAllPagoCodes::ABAERR_DISABLED_COMMS;
            $this->errorMsg = PayAllPagoCodes::$aErrors[$this->errorCode];
            $this->endLogOperation(false);
            return false;
        }

        if ( $this->connectToGateway ){
            $aResultComm = HeComm::sendHTTPPostCurlRetAll($this->server,$this->path, $this->xmlRequest, $this->userAgent);
        } else{
            // @TODO: ABA ALL PAGO Simulator which is not implemented YET
            $aResultComm = HeComm::sendHTTPPostCurlRetAll("http://".Yii::app()->config->get("SELF_DOMAIN"),
                                                                            "/partnersimulators/paymentallpago/ctpe/1",
                                                                            $this->xmlRequest, $this->userAgent);
        }
        $this->resultURL = $aResultComm["resultUrl"];
        $this->info = $aResultComm["info"];

        if ($this->resultURL) {
            $output = $this->parserResult($this->resultURL);
            if ($output) {
                if ($output == "ACK") {
                    // Everything is OK
                    $this->endLogOperation(true);
                    return $this->moLogAllpago;
                } else {
                    $this->endLogOperation(false);
                    // There is an error (check $output for error code ... e.g. print $output to logfile
                    return false;
                }
            } else {
                // There is a parsing problem
                $this->errorCode = PayAllPagoCodes::ABAERR_COMMXMLVALID;
                $this->errorMsg = PayAllPagoCodes::$aErrors[$this->errorCode];
                $this->endLogOperation(false);
                return false;
            }
        } else {
            // There is a connection-problem
            $this->errorCode = PayAllPagoCodes::ABAERR_COMMTIMEOUT;
            $this->errorMsg = $aResultComm["error"]." or maybe there was a ".PayAllPagoCodes::$aErrors[$this->errorCode];
            $this->xmlResponse = $aResultComm["error"];
            $this->endLogOperation(false);
            return false;
        }
    }

    /**
     * It must match the integer in the database. Returns 4.
     *
     *
     * @return int|mixed
     */
    public function getPaySupplierId()
    {
        return $this->idPaySupplier;
    }

    /** Alias of function opRunQuery(), overwrite from PaySupplierCommon
     *
     * @param string $periodFrom
     * @param string $periodTo
     * @param string $transType
     * @param string $transMethod
     * @param bool|true $onlySuccess
     *
     * @return bool|PayGatewayLogAllpago|string
     */
    public function runRequestReconciliation($periodFrom='', $periodTo='', $transType='PAYMENT', $transMethod='CC', $onlySuccess=true) {
        return $this->opRunQuery($periodFrom, $periodTo);
    }


    /** Returns id of translation specific for every payment gateway
     *
     * @return string
     */
    public function getListSpecificConditions()
    {
        return "key_allpago_conditions_privacy";
    }

    /**
     * @param string $curPaymentId
     * @param integer $attempts
     *
     * @return string
     */
    protected function createTransactionId($curPaymentId, $attempts=NULL)
    {
        $sessionAttemptVar = $attempts;
        if(!empty($sessionAttemptVar) && !is_null($sessionAttemptVar)){
            return $curPaymentId.":".$sessionAttemptVar;
        }

        return $curPaymentId;
    }

    /**
     * @param $account_holder
     * @param $account_brand
     * @param $account_number
     * @param $account_bankname
     * @param $account_country
     * @param $account_authorisation
     * @param $account_verification
     * @param $account_year
     * @param $account_month
     */
    protected function setAccountInformation($account_holder,$account_brand,$account_number,$account_bankname,
                           $account_country, $account_authorisation,$account_verification,$account_year,$account_month)
	{
		$this->account_holder=$account_holder;
		$this->account_brand=$account_brand;
		$this->account_number=$account_number;
		$this->account_bankname=$account_bankname;
		$this->account_country=$account_country;
		$this->account_authorisation=$account_authorisation;
		$this->account_verification=$account_verification;
		$this->account_year=$account_year;
		$this->account_month=$account_month;
	}

    /** Reference to the id of the registration. In order to execute a Recurrent payment
     *
     * @param $account_registration
     */
    protected function setAccountRegistration($account_registration)
    {
        $this->account_registration = $account_registration;
    }

    /** establishes the type of transaction we want to execute. The most important one.
     * @see http://support.ctpe.net/faq/doku.php?id=start#request_codes_return_codes_and_properties
     *
     * @param $payment_code
     */
    protected function setPaymentCode($payment_code)
	{
		$this->payment_code=$payment_code;
	}


    /** Sets the registration tag values for the XML that will be sent to ALLPAGO. Small differences between
     * XML tags among the countries used for ALLPAGO.
     *
     * @param string $identification_transactionid
     *
     * @return mixed
     */
    protected abstract function setRegistrationId( $identification_transactionid);


    protected function setCustomerContact($contact_email,$contact_mobile,$contact_ip,$contact_phone)
	{
		$this->contact_email=$contact_email;
		$this->contact_mobile=$contact_mobile;
		$this->contact_ip=$contact_ip;
		$this->contact_phone=$contact_phone;
	}

    /** Id of the shopper, empty by default for Mexico and Brazil.
     *
     * @param string $identityCard
     */
    protected function setCustomerDetails($identityCard)
    {
        $this->identity = $identityCard;
    }

    /**  In general the address is not compulsory, but at the beginning for Brazil it was.
     *
     * @param AbaUser $moUser
     * @param string $address_country
     *
     * @return bool|AbaUserAddressInvoice
     */
    protected function setCustomerAddress( AbaUser $moUser, $address_country)
	{
        /* $address_street,$address_zip,$address_city,$address_state,$address_country */
        $this->address_country=$address_country;
        $moUserAddress = new AbaUserAddressInvoice();
        if ($moUserAddress->getUserAddressByUserId($moUser->getId())){
            $this->address_street=$moUserAddress->street;
            $this->address_zip=$moUserAddress->zipCode;
            $this->address_city=$moUserAddress->city;
            $this->address_state=$moUserAddress->state;
            return true;
        } else {
            $this->address_street="";
            $this->address_zip="";
            $this->address_city="";
            $this->address_state="";
            return false;
        }
	}

    /**
     * @param string $name_salutation
     * @param string $name_title
     * @param string $name_give
     * @param string $name_family
     * @param string $name_company
     */
    protected function setCustomerName($name_salutation,$name_title,$name_give,$name_family,$name_company)
	{
		$this->name_salutation=$name_salutation;
		$this->name_title=$name_title;
		$this->name_give=$name_give;
		$this->name_family=$name_family;
		$this->name_company=$name_company;
	}

    /** Prepares the XML to be sent to allpagos.com platform.
     *
     * @return bool
     */
    protected function commitXMLPayment()
    {
        $strXML = '<?xml version="1.0" encoding="UTF-8" ?>';

        // set account and user information.
        $strXML .= "<Request version=\"$this->request_version\">";

        $strXML .= "<Header>";
        $strXML .= "<Security sender=\"$this->sender\" token=\"token\" />";
        $strXML .= "</Header>";

        $strXML .= "<Transaction response=\"$this->transaction_response\" channel=\"$this->channel\" mode=\"$this->transaction_mode\">";
        $strXML .= "<User pwd=\"$this->userpwd\" login=\"$this->userid\" />";
        $strXML .= "<Identification>";
        $strXML .= "<TransactionID>$this->identification_transactionid</TransactionID>";
        $strXML .= "<ShopperID>$this->shopper_id</ShopperID>";
        if( property_exists($this, "reference_id") && $this->reference_id!=='' ){
            $strXML .= "<ReferenceID>$this->reference_id</ReferenceID>";
        }
        $strXML .= "</Identification>";

        // set account information++++++++++++++++++++++++++++++++
        if(property_exists($this, "account_registration") && $this->account_registration!==''){
            $strXML .= "<Account registration=\"".$this->account_registration."\">";
        }else if($this->payment_code!=='PP.PA') {
            // For Boleto this XML TAG is not written:
            $strXML .= "<Account>";
        }
        if(property_exists($this, "account_number") && $this->account_number!==''){
            $strXML .= "<Holder>$this->account_holder</Holder>";
            $strXML .= "<Brand>$this->account_brand</Brand>";
            $strXML .= "<Number>$this->account_number</Number>";
            $strXML .= "<Bank>$this->account_bankname</Bank>";
            $strXML .= "<Country>$this->account_country</Country>";
            $strXML .= "<Authorisation>$this->account_authorisation</Authorisation>";
            $strXML .= "<Verification>$this->account_verification</Verification>";
            $strXML .= "<Year>$this->account_year</Year>";
            $strXML .= "<Month>$this->account_month</Month>";
        }
        if($this->payment_code!=='PP.PA') {
            // For Boleto this XML TAG is not written:
            $strXML .= "</Account>";
        }
        // ------------------------------- End Account Group Tag

        $strXML .= "<Payment code=\"$this->payment_code\">";
        if(property_exists($this, "payment_amount") && $this->payment_amount!==''){
            // set payment information----------------------------
            $strXML .= "<Presentation>";
            $strXML .= "<Amount>$this->payment_amount</Amount>";
            $strXML .= "<Usage>$this->payment_usage</Usage>";
            $strXML .= "<Currency>$this->payment_currency</Currency>";
            $strXML .= "</Presentation>";
        } else{
            // set payment information for registration only-------
            $strXML .= "<Presentation>";
            $strXML .= "<Amount></Amount>";
            $strXML .= "<Usage>Registration for subscription in Aba English</Usage>";
            $strXML .= "<Currency></Currency>";
            $strXML .= "</Presentation>";
        }
        $strXML .= "</Payment>";

        //Start customer information+++++++++++++++
        if( property_exists($this, "contact_email") && $this->contact_email!=='' ){
            $strXML .= "<Customer>";
                $strXML .= "<Contact>";
                $strXML .= "<Email>$this->contact_email</Email>";
                $strXML .= "<Mobile>$this->contact_mobile</Mobile>";
                $strXML .= "<Ip>$this->contact_ip</Ip>";
                $strXML .= "<Phone>$this->contact_phone</Phone>";
            $strXML .= "</Contact>";

            if (( property_exists($this, "identity") && $this->identity!=='' ) &&
                $this->payment_code==PayAllPagoCodes::PAY_CARD_CREDIT.".".PayAllPagoCodes::OP_DEBIT){
                $strXML .= "<Details>";
                $strXML .= '<Identity paper="IDCARD">'.$this->identity.'</Identity>';
                $strXML .= "</Details>";
            }

            $strXML .= "<Address>";
            if (( property_exists($this, "address_street") && $this->address_street!=='' )){
                $strXML .= "<Street>$this->address_street</Street>";
                $strXML .= "<Zip>$this->address_zip</Zip>";
                $strXML .= "<City>$this->address_city</City>";
                $strXML .= "<State>$this->address_state</State>";
            }
            $strXML .= "<Country>$this->address_country</Country>";
            $strXML .= "</Address>";
            $strXML .= "<Name>";
            $strXML .= "<Salutation>$this->name_salutation</Salutation>";
            $strXML .= "<Title>$this->name_title</Title>";
            $strXML .= "<Given>$this->name_give</Given>";
            $strXML .= "<Family>$this->name_family</Family>";
            $strXML .= "<Company>$this->name_company</Company>";
            $strXML .= "</Name>";
            $strXML .= "</Customer>";
        }
        //------------End Customer information

        // Start criterion tags, compliance++++++++++
        if ( $this->payment_code==PayAllPagoCodes::PAY_CARD_CREDIT.".".PayAllPagoCodes::OP_DEBIT ||
            $this->payment_code=='PP.PA' ){
            $strXML .= "<Analysis>";
                $strXML .= "<Criterion name=\"merchant_sitename\">".urlencode(Yii::app()->config->get("SELF_DOMAIN"))."</Criterion>";
                $strXML .= "<Criterion name=\"product\">".$this->payment_usage."</Criterion>";
            $strXML .= "</Analysis>";
        }
        // -----------------------End Criterion tags

        $strXML .= "</Transaction>";
        $strXML .= "</Request>";

        $this->xmlRequest = $strXML;

        return true;
	}

    /** Prepares the XML query request in order to retrieve
     * a list of transactions
     *
     * @return bool
     */
    protected function commitXMLQuery()
    {
        $xmlFile = '<?xml version="1.0" encoding="UTF-8" ?>';
        $xmlFile .= '<Request version="1.0"  encoding="UTF-8"><Header><Security sender="'.$this->sender.'"/></Header>';
        $xmlFile .= '<Query mode="'.$this->transaction_mode.'" level="MERCHANT" entity="'.$this->sender.'" type="STANDARD" maxCount="400">';
//        $xmlFile .= '<Query mode="'.$this->transaction_mode.'" level="CHANNEL" entity="'.$this->channelInitial.'" type="STANDARD" maxCount="400">';
        $xmlFile .= '<User login="'.$this->userid.'" pwd="'.$this->userpwd.'"/>';
        //$xmlFile .= '<TransactionType>PAYMENT</TransactionType>';
        $xmlFile .= '<ProcessingResult>ACK</ProcessingResult>';
        $xmlFile .= '<Period from="'.$this->dateQueryFrom.'" to="'.$this->dateQueryUntil.'"/>';
        $xmlFile .= '<Methods><Method code="CC"></Method></Methods>';// We only deal with credit operations with Brazil.
        // We exclude REGISTRATIONS, RG. ?
        $xmlFile .= '<Types>
                            <Type code="DB"/>
                            <Type code="RF"/>
                            <!--<Type code="RG"/>-->
                     </Types>';

        $xmlFile .= '</Query></Request>';

        $this->xmlRequest = $xmlFile;
        return true;
    }

    /**   Parser XML message returned by CTPE server.
     *
     * @param $resultURL
     * @param bool $isQuery
     *
     * @return string
     */
    protected function parserResult($resultURL, $isQuery = false)
	{
        $resultXML = urldecode($resultURL);
        $this->xmlResponse = $resultXML;
        $processingResult = substr($resultXML, strpos($resultXML, "<Result>") + strlen("<Result>"),
            strpos($resultXML, "</Result>") - strlen("<Result>") - strpos($resultXML, "<Result>"));

        if ($processingResult == "ACK") {
            return $processingResult;
        } elseif ($processingResult == "NOK") {
            $this->errorMsg = substr($resultXML,
                strpos($resultXML, "<Return code=") + strlen('<Return code="000.000.000">'),
                strpos($resultXML, "</Return>") - strlen('<Return code="000.000.000">') - strpos($resultXML, "<Return code"));

            $this->errorCode = substr($resultXML, strpos($resultXML, "<Return code=") + 1 + strlen("<Return code="), 11);

            return $processingResult;
        } elseif ($isQuery) {
            $xml = new SimpleXMLElement($resultXML);
            $countTransactions = $xml->xpath("/Response/Result/@count");
            if (intval($countTransactions[0]) >= 0) {
                return "ACK";
            }
        }
        $this->errorCode = PayAllPagoCodes::ABAERR_COMMXMLVALID;
        $this->errorMsg = substr($resultXML,
            strpos($resultXML, "<Return code=") + strlen('<Return code="000.000.000">'),
            strpos($resultXML, "</Return>") - strlen('<Return code="000.000.000">') - strpos($resultXML, "<Return code"));

        return $processingResult;

    }

    /** Returns the ip location of the user. It depends on model userLocation.
     * @param AbaUser $moUser
     *
     * @return bool|mixed|string
     */
    protected function getIPLocation( AbaUser $moUser )
    {
        $moLoc = new AbaUserLocation();
        $lastIp = $moLoc->getLastIp($moUser->getId());
        if (!$lastIp || $lastIp == '') {
            $lastIp = Yii::app()->request->getUserHostAddress();
        }
        return $lastIp;
    }

    /**
     * @param string $idPayment
     * @param string $operation
     * @param integer $userId
     *
     * @return bool|int
     */
    protected function startLogOperation($idPayment, $operation, $userId)
    {
        $this->moLogAllpago = new PayGatewayLogAllpago($this->idPaySupplier);
        $this->moLogAllpago->idPayment = $idPayment;
        $this->moLogAllpago->operation = $operation;
        $this->moLogAllpago->userId = $userId;
        $this->moLogAllpago->dateRequest = HeDate::todaySQL(true);
        $this->moLogAllpago->xmlRequest = $this->xmlRequest;
        $this->moLogAllpago->errorCode = NULL;
        $this->moLogAllpago->success = 0;

        $this->stripCardData();

        return $this->moLogAllpago->insertLog();
    }

    /**
     * @return bool
     */
    protected function stripCardData () {
        try {
            if($this->moLogAllpago->idPaySupplier <> PAY_SUPPLIER_ALLPAGO_BR_BOL) {
                $this->moLogAllpago->xmlRequest = preg_replace('/<Account\b[^>]*>(.*?)<\/Account>/i', '', $this->moLogAllpago->xmlRequest);
/*  //            $this->xmlRequest = preg_replace('/<Account\b[^>]*?>.*?<\/*?Account>/i', '', $this->xmlRequest);  */
            }
        }
        catch(Exception $e) {
            $this->moLogAllpago->xmlRequest = '';
        }

        return true;
    }

    /**
     * @param $success
     * @return bool
     */
    protected function endLogOperation($success)
    {
        $this->moLogAllpago->xmlResponse = $this->xmlResponse;
        $this->moLogAllpago->dateResponse = HeDate::todaySQL(true);
        $this->moLogAllpago->errorCode = $this->errorCode;
        $this->moLogAllpago->success = $success;
        return $this->moLogAllpago->update(array("xmlResponse","dateResponse","errorCode","success"));
    }

    /**
     * @return PayGatewayLogAllpago
     */
    public function getMoLogResponse()
    {
        return $this->moLogAllpago;
    }

}

