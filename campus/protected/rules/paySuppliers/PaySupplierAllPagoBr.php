<?php

/**
 * Class PaySupplierAllPagoBr
 *
 */
class PaySupplierAllPagoBr extends PaySupplierAllPago
{

    public function __construct()
    {
        parent::__construct();

        $this->idPaySupplier = PAY_SUPPLIER_ALLPAGO_BR;
        $this->channel = Yii::app()->config->get("PAY_ALLPAGO_CHANNEL_INITIAL");
        $this->channelInitial = Yii::app()->config->get("PAY_ALLPAGO_CHANNEL_INITIAL");
        $this->channelRG = Yii::app()->config->get("PAY_ALLPAGO_CHANNEL_RG");
        $this->channelRecurring = Yii::app()->config->get("PAY_ALLPAGO_CHANNEL_RECURRING");
    }

    /** Returns an array of integer as a key, and string as a value. Each of implementations for this definition class
     * should have at least one element within the array.
     *
     * @return array
     */
    public function getListOfCards()
    {
        return array(KIND_CC_VISA => Yii::t('mainApp', 'VISA_w'),
            KIND_CC_MASTERCARD => Yii::t('mainApp', 'MASTERCARD_w'),
            KIND_CC_AMERICAN_EXPRESS => Yii::t('mainApp', 'AMERICAN_EXPRESS_w'),
            KIND_CC_DINERS_CLUB => Yii::t('mainApp', 'DINERS_CLUB_w'),
            KIND_CC_JCB => Yii::t('mainApp', 'JCB_w'),
            KIND_CC_ELO => Yii::t('mainApp', 'ELO_w'),
            KIND_CC_DISCOVER => Yii::t('mainApp', 'DISCOVER_w')
        );
    }

    /**
     * @param Payment|PaymentControlCheck|AbaUserCreditForms $objSpecialFields
     *
     * @return bool
     */
    public function setSpecialFields($objSpecialFields)
    {
        $this->specialFields['cpfBrasil'] = $objSpecialFields->cpfBrasil;
        return true;
    }

    /** These fields need to be filled in by the user. These FIELDS would be displayed to the user
     * in the payment Method page. Not to be confused with array ->specialFields. Nothing to do with that.
     * @return array
     */
    public function getSpecialDisplayFields()
    {
        return array( 'cpfBrasil', 'firstName', 'secondName');
    }


    /**
     * @param float $payment_amount
     * @param string $payment_usage
     * @param string $identification_transactionid
     * @param string $payment_currency
     * @param string $reference_id
     * @internal param string $shopper_id
     */
    protected function setPaymentInformation($payment_amount, $payment_usage, $identification_transactionid,
                                             $payment_currency, $reference_id = '')
    {
        $this->payment_amount = $payment_amount;
        $this->payment_usage = $payment_usage;
        $this->identification_transactionid = $identification_transactionid;
        $this->shopper_id = $this->specialFields['cpfBrasil'];
        $this->payment_currency = $payment_currency;
        if ($reference_id != '') {
            $this->reference_id = $reference_id;
        }
    }


    /** Sets the registration tag values for the XML that will be sent to ALLPAGO.
     *
     * @param string $identification_transactionid
     *
     * @return void
     */
    protected function setRegistrationId($identification_transactionid)
    {
        $this->identification_transactionid = $identification_transactionid;
        $this->shopper_id = $this->specialFields['cpfBrasil'];
    }

}