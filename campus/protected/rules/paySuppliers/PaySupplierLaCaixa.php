<?php

/**
 *
 *
 */
class PaySupplierLaCaixa extends PaySupplierCommon
{
    const ERROR_HTTP = 1001;
    const ERROR_ABADB_LOGPAY = 1002;
    const ERROR_XML_PARSE = 1003;
    const EUR_CAIXA_CODE = 978;
    const USD_CAIXA_CODE = 840;

    protected $connectToGateway;
    protected $orderNumber;
    protected $errorCode;
    protected $arrayRequest;
    /* @var $this->moLogResponse PayGatewayLogLaCaixaResRec */
    protected $moLogResponse;

    protected $ds_merchant_identifier = "REQUIRED";

    protected $errorMsg = array
    (
      0 => "Sin error",
      101 => "Tarjeta caducada",
      102 => "Tarjeta en excepción transitoria o bajo sospecha de fraude",
      104 => "Operación no permitida para esa tarjeta o terminal",
      116 => "Disponible insuficiente",
      118 => "Tarjeta no registrada",
      129 => "Código de seguridad (CVV2/CVC2) incorrecto",
      180 => "Tarjeta ajena al servicio",
      184 => "Error en la autenticación del titular",
      190 => "Denegación sin especificar Motivo",
      191 => "Fecha de caducidad errónea",
      202 => "Tarjeta en excepción transitoria o bajo sospecha de fraude con retirada de tarjeta",
      1001 => "Error Http",
      1002 => "Registering payment in database raised an error",
      1003 => "XML response with our supplier of payments is not well formed"
    );

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->arrayRequest = array();
        $this->errorCode = 0;
        $this->currentElement = "";
    }

    /** Returns an array of integer as a key, and string as a value. Each of implementations for this definition class
     * should have at least one element within the array.
     *
     * @param string $currency
     *
     * @return array
     */
    public function getListOfCards($currency = null)
    {
        $cardsList = array(
          KIND_CC_VISA => Yii::t('mainApp', 'VISA_w'),
          KIND_CC_VISA_ELECTRON => Yii::t('mainApp', 'VISA_ELECTRON_w'),
          KIND_CC_MASTERCARD => Yii::t('mainApp', 'MASTERCARD_w'),
          KIND_CC_DINERS_CLUB => Yii::t('mainApp', 'DINERS_CLUB_w'),
          KIND_CC_MAESTRO => Yii::t('mainApp', 'MAESTRO_w')
        );

        if ($currency !== 'USD') {
            $cardsList[KIND_CC_AMERICAN_EXPRESS] = Yii::t('mainApp', 'AMERICAN_EXPRESS_w');
        }

        return $cardsList;
    }

    /**
     * @return int
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return string
     */
    public function getErrorString()
    {
        if (isset($this->errorMsg[$this->errorCode])) {
            return $this->errorMsg[$this->errorCode];
        } else {
            return "ErrorCode " . $this->errorCode;
        }
    }

    /**
     * @param string $ccyCode
     * @return array
     *
     * @throws CException
     */
    protected function getPaySupplierCodes($ccyCode)
    {
        if ($ccyCode == "USD") {
            return array(
              "code" => Yii::app()->config->get("PAY_CAIXA_USDCODE_TPV"),
              "terminal" => "001",
              "password" => Yii::app()->config->get("PAY_CONTRA_LACAIXA_GW_USD"),
              "currency" => PaySupplierLaCaixa::USD_CAIXA_CODE
            );
        } elseif ($ccyCode == "EUR") {
            return array(
              "code" => Yii::app()->config->get("PAY_CAIXA_EURCODE_TPV"),
              "terminal" => "002",
              "password" => Yii::app()->config->get("PAY_CONTRA_LACAIXA_GW_EUR"),
              "currency" => PaySupplierLaCaixa::EUR_CAIXA_CODE
            );
        } else {
            throw new CException("ABA, you can not pay with this currency from this country." .
              " Allowed currencies are USD and EUR only. Please contact support for more information.");
        }
    }

    /**
     * @param $amount
     * @param $order
     * @param $code
     * @param $currency
     * @param $curPayment
     * @param $transactionType
     * @param string $password
     *
     * @return string
     */
    protected function getSignature_old_20150331(
      $amount,
      $order,
      $code,
      $currency,
      $curPayment,
      $transactionType,
      $password
    ) {
        if ($transactionType == 'A') {
            $phraseToSign = $amount . $order . $code . $currency . $curPayment->cardNumber . $curPayment->cardCvc .
              $transactionType . $password;
        } else {
            $phraseToSign = $amount . $order . $code . $currency . $transactionType . $password;
        }

        return HeAbaSHA::sha_1($phraseToSign);
    }

    /**
     * #abawebapps-170  -  Deprecated
     *
     * @param $sParams
     * @param bool $withReference
     *
     * @return string
     */
    protected function getSignature($sParams, $withReference = false)
    {
        //
        $iAmount = $sParams['amount'];
        $sOrder = $sParams['order'];
        $sCode = $sParams['code'];
        $sCurrency = $sParams['currency'];
        $sTransactionType = $sParams['transactionType'];
        $sPassword = $sParams['password'];

        $sMerchantIdentifier = (isset($sParams['merchantIdentifier']) ? $sParams['merchantIdentifier'] : '');
        $sMerchantDirectpayment = (isset($sParams['merchantDirectpayment']) ? $sParams['merchantDirectpayment'] : '');

        $sCardNumber = (isset($sParams['cardNumber']) ? $sParams['cardNumber'] : '');
        $sCardCvc = (isset($sParams['cardCvc']) ? $sParams['cardCvc'] : '');

        if ($sTransactionType == 'A') {
            if ($withReference AND trim($sParams['paySuppExtProfId']) != '') {
                //
                // recurring payment
                $phraseToSign = $iAmount . $sOrder . $sCode . $sCurrency . $sTransactionType . $sMerchantIdentifier . $sMerchantDirectpayment . $sPassword;
            } else {
                //
                // initial payment OR recurring (initial) payment with card data
                $phraseToSign = $iAmount . $sOrder . $sCode . $sCurrency . $sCardNumber . $sCardCvc . $sTransactionType . $sMerchantIdentifier . $sMerchantDirectpayment . $sPassword;
            }
        } else {
            //
            // refund
            if ($withReference) {
                $phraseToSign = $iAmount . $sOrder . $sCode . $sCurrency . $sTransactionType . $sMerchantIdentifier . $sMerchantDirectpayment . $sPassword;
            } else {
                $phraseToSign = $iAmount . $sOrder . $sCode . $sCurrency . $sTransactionType . $sPassword;
            }
        }

        return HeAbaSHA::sha_1($phraseToSign);
    }


    /**
     * List of all La Caixa error codes and descriptions
     * @param $sisCodigo
     *
     * @return string
     */
    public function getDescBySISCodigo($sisCodigo)
    {
        $aLaCaixaCodes = array(
          "SIS0007" => " Error al desmontar el XML de entrada ",
          "SIS0008" => " Error falta Ds_Merchant_MerchantCode ",
          "SIS0009" => " Error de formato en Ds_Merchant_MerchantCode ",
          "SIS0010" => " Error falta Ds_Merchant_Terminal ",
          "SIS0011" => " Error de formato en Ds_Merchant_Terminal ",
          "SIS0014" => " Error de formato en Ds_Merchant_Order ",
          "SIS0015" => " Error falta Ds_Merchant_Currency ",
          "SIS0016" => " Error de formato en Ds_Merchant_Currency ",
          "SIS0017" => " Error no se admiten operaciones en pesetas ",
          "SIS0018" => " Error falta Ds_Merchant_Amount ",
          "SIS0019" => " Error de formato en Ds_Merchant_Amount ",
          "SIS0020" => " Error falta Ds_Merchant_MerchantSignature ",
          "SIS0021" => " Error la Ds_Merchant_MerchantSignature viene vacía ",
          "SIS0022" => " Error de formato en Ds_Merchant_TransactionType ",
          "SIS0023" => " Error Ds_Merchant_TransactionType desconocido ",
          "SIS0024" => " Error Ds_Merchant_ConsumerLanguage tiene mas de 3 posiciones ",
          "SIS0025" => " Error de formato en Ds_Merchant_ConsumerLanguage ",
          "SIS0026" => " Error No existe el comercio / terminal enviado ",
          "SIS0027" => " Error Moneda enviada por el comercio es diferente a la que tieneasignada para ese terminal",
          "SIS0028" => " Error Comercio / terminal está dado de baja ",
          "SIS0030" => " Error en un pago con tarjeta ha llegado un tipo de operación queno es ni pago ni preautorización",
          "SIS0031" => " Método de pago no definido ",
          "SIS0033" => " Error en un pago con móvil ha llegado un tipo de operación que noes ni pago ni preautorización ",
          "SIS0034" => " Error de acceso a la Base de Datos ",
          "SIS0037" => " El número de teléfono no es válido ",
          "SIS0038" => " Error en java ",
          "SIS0040" => " Error el comercio / terminal no tiene ningún método de pagoasignado ",
          "SIS0041" => " Error en el cálculo de la HASH de datos del comercio. ",
          "SIS0042" => " La firma enviada no es correcta ",
          "SIS0043" => " Error al realizar la notificación on-line ",
          "SIS0046" => " El bin de la tarjeta no está dado de alta ",
          "SIS0051" => " Error número de pedido repetido ",
          "SIS0054" => " Error no existe operación sobre la que realizar la devolución ",
          "SIS0055" => " Error existe más de un pago con el mismo número de pedido ",
          "SIS0056" => " La operación sobre la que se desea devolver no está autorizada ",
          "SIS0057" => " El importe a devolver supera el permitido ",
          "SIS0058" => " Inconsistencia de datos, en la validación de una confirmación ",
          "SIS0059" => " Error no existe operación sobre la que realizar la confirmación ",
          "SIS0060" => " Ya existe una confirmación asociada a la preautorización ",
          "SIS0061" => " La preautorización sobre la que se desea confirmar no estáautorizada ",
          "SIS0062" => " El importe a confirmar supera el permitido ",
          "SIS0063" => " Error. Número de tarjeta no disponible ",
          "SIS0064" => " Error. El número de tarjeta no puede tener más de 19 posiciones ",
          "SIS0065" => " Error. El número de tarjeta no es numérico ",
          "SIS0066" => " Error. Mes de caducidad no disponible ",
          "SIS0067" => " Error. El mes de la caducidad no es numérico ",
          "SIS0068" => " Error. El mes de la caducidad no es válido ",
          "SIS0069" => " Error. Año de caducidad no disponible ",
          "SIS0070" => " Error. El Año de la caducidad no es numérico ",
          "SIS0071" => " Tarjeta caducada ",
          "SIS0072" => " Operación no anulable ",
          "SIS0074" => " Error falta Ds_Merchant_Order ",
          "SIS0075" => " Error el Ds_Merchant_Order tiene menos de 4 posiciones o más de ",
          "SIS0076" => " Error el Ds_Merchant_Order no tiene las cuatro primerasposiciones numéricas ",
          "SIS0077" => " Error el Ds_Merchant_Order no tiene las cuatro primerasposiciones numéricas. No se utiliza ",
          "SIS0078" => " Método de pago no disponible ",
          "SIS0079" => " Error al realizar el pago con tarjeta ",
          "SIS0081" => " La sesión es nueva, se han perdido los datos almacenados ",
          "SIS0084" => " El valor de Ds_Merchant_Conciliation es nulo ",
          "SIS0085" => " El valor de Ds_Merchant_Conciliation no es numérico ",
          "SIS0086" => " El valor de Ds_Merchant_Conciliation no ocupa 6 posiciones ",
          "SIS0089" => " El valor de Ds_Merchant_ExpiryDate no ocupa 4 posiciones ",
          "SIS0092" => " El valor de Ds_Merchant_ExpiryDate es nulo ",
          "SIS0093" => " Tarjeta no encontrada en la tabla de rangos ",
          "SIS0094" => " La tarjeta no fue autenticada como 3D Secure ",
          "SIS0097" => " Valor del campo Ds_Merchant_CComercio no válido ",
          "SIS0098" => " Valor del campo Ds_Merchant_CVentana no válido ",
          "SIS0112" => " Error El tipo de transacción especificado en Ds_Merchant_Transaction_Type no esta permitido ",
          "SIS0113" => " Excepción producida en el servlet de operaciones ",
          "SIS0114" => " Error, se ha llamado con un GET en lugar de un POST ",
          "SIS0115" => " Error no existe operación sobre la que realizar el pago de la cuota ",
          "SIS0116" => " La operación sobre la que se desea pagar una cuota no es unaoperación válida ",
          "SIS0117" => " La operación sobre la que se desea pagar una cuota no estáautorizada ",
          "SIS0118" => " Se ha excedido el importe total de las cuotas ",
          "SIS0119" => " Valor del campo Ds_Merchant_DateFrecuency no válido ",
          "SIS0120" => " Valor del campo Ds_Merchant_ChargeExpiryDate no válido ",
          "SIS0121" => " Valor del campo Ds_Merchant_SumTotal no válido ",
          "SIS0122" => " Valor del campo Ds_Merchant_DateFrecuency o noDs_Merchant_SumTotal tiene formato incorrecto ",
          "SIS0123" => " Se ha excedido la fecha tope para realizar transacciones ",
          "SIS0124" => " No ha transcurrido la frecuencia mínima en un pago recurrente sucesivo ",
          "SIS0132" => " La fecha de Confirmación de Autorización no puede superar en mas de 7 días a la de Preautorización. ",
          "SIS0133" => " La fecha de Confirmación de Autenticación no puede superar en mas de 45 días a la de Autenticación Previa.",
          "SIS0139" => " Error el pago recurrente inicial está duplicado ",
          "SIS0142" => " Tiempo excedido para el pago ",
          "SIS0197" => " Error al obtener los datos de cesta de la compra en operación tipopasarela ",
          "SIS0198" => " Error el importe supera el límite permitido para el comercio ",
          "SIS0199" => " Error el número de operaciones supera el límite permitido para el comercio ",
          "SIS0200" => " Error el importe acumulado supera el límite permitido para el comercio ",
          "SIS0214" => " El comercio no admite devoluciones ",
          "SIS0216" => " Error Ds_Merchant_CVV2 tiene mas de 3 posiciones ",
          "SIS0217" => " Error de formato en Ds_Merchant_CVV2 ",
          "SIS0218" => " El comercio no permite operaciones seguras por la entrada /operaciones ",
          "SIS0219" => " Error el número de operaciones de la tarjeta supera el límite ",
          "SIS0220" => " Error el importe acumulado de la tarjeta supera el límite permitido para el comercio ",
          "SIS0221" => " Error el CVV2 es obligatorio ",
          "SIS0222" => " Ya existe una anulación asociada a la preautorización ",
          "SIS0223" => " La preautorización que se desea anular no está autorizada ",
          "SIS0224" => " El comercio no permite anulaciones por no tener firma ampliada ",
          "SIS0225" => " Error no existe operación sobre la que realizar la anulación ",
          "SIS0226" => " Inconsistencia de datos, en la validación de una anulación ",
          "SIS0227" => " Valor del campo Ds_Merchant_TransactionDate no válido ",
          "SIS0229" => " No existe el código de pago aplazado solicitado ",
          "SIS0252" => " El comercio no permite el envío de tarjeta ",
          "SIS0253" => " La tarjeta no cumple el check-digit ",
          "SIS0254" => " El número de operaciones de la IP supera el límite permitido por el comercio ",
          "SIS0255" => " El importe acumulado por la IP supera el límite permitido por el comercio ",
          "SIS0256" => " El comercio no puede realizar preautorizaciones ",
          "SIS0257" => " Esta tarjeta no permite operativa de preautorizaciones ",
          "SIS0258" => " Inconsistencia de datos, en la validación de una confirmación ",
          "SIS0261" => " Operación detenida por superar el control de restricciones en la entrada al SIS ",
          "SIS0270" => " El comercio no puede realizar autorizaciones en diferido ",
          "SIS0274" => " Tipo de operación desconocida o no permitida por esta entrada al SIS ",
          "SIS0298" => " El comercio no permite realizar operaciones de Tarjeta en Archivo ",
          "SIS0319" => " El comercio no pertenece al grupo especificado en Ds_Merchant_Group ",
          "SIS0321" => " La referencia indicada en Ds_Merchant_Identifier no está asociada al comercio ",
          "SIS0322" => " Error de formato en Ds_Merchant_Group ",
          "SIS0325" => " Se ha pedido no mostrar pantallas pero no se ha enviado ninguna referencia de tarjeta ",
        );

        if (array_key_exists($sisCodigo, $aLaCaixaCodes)) {
            return $aLaCaixaCodes[$sisCodigo];
        } else {
            return false;
        }
    }

    /**
     * It must match the integer in the database
     * Currently only there is La Caixa(1) and PayPal(2)
     * @return int|mixed
     */
    public function getPaySupplierId()
    {
        return PAY_SUPPLIER_CAIXA;
    }

    /**
     * @param $aRequest
     *
     * @return string   XML response La Caixa
     */
    public function getSimulateResponse($aRequest)
    {
        $fAcceptPay = false;

        $xmlResponseSimulated = "<RETORNOXML>";
        $xmlResponseSimulated .= "<CODIGO>0</CODIGO>";
        $xmlResponseSimulated .= "<Ds_Version>0.1</Ds_Version>";
        $xmlResponseSimulated .= "<OPERACION>";
        $xmlResponseSimulated .= "<Ds_Amount>" . $aRequest["Amount"] . "</Ds_Amount>";
        $xmlResponseSimulated .= "<Ds_Currency>" . $aRequest["Currency"] . "</Ds_Currency>";
        $xmlResponseSimulated .= "<Ds_Order>" . $aRequest["Order_number"] . "</Ds_Order>";
        $xmlResponseSimulated .= "<Ds_Signature>" . $aRequest["MerchantSignature"] . "</Ds_Signature>";
        $xmlResponseSimulated .= "<Ds_MerchantCode>" . $aRequest["MerchantCode"] . "</Ds_MerchantCode>";
        $xmlResponseSimulated .= "<Ds_Terminal>2</Ds_Terminal>";
        if ($fAcceptPay) {
            $xmlResponseSimulated .= "<Ds_Response>0000</Ds_Response>"; // Success case equals to 0000.
            $xmlResponseSimulated .= "<Ds_AuthorisationCode>123456</Ds_AuthorisationCode>"; // Success case any value.
        } else {
            $xmlResponseSimulated .= "<Ds_Response>190</Ds_Response>"; // 190, 182 A code error of three digits.
            $xmlResponseSimulated .= "<Ds_AuthorisationCode></Ds_AuthorisationCode>"; // Empty for error.
        }

        $xmlResponseSimulated .= "<Ds_TransactionType>A</Ds_TransactionType>";
        $xmlResponseSimulated .= "<Ds_SecurePayment>0</Ds_SecurePayment>";
        $xmlResponseSimulated .= "<Ds_Language>1</Ds_Language>";

        $xmlResponseSimulated .= "<Ds_MerchantData>ABA English</Ds_MerchantData>";
        $xmlResponseSimulated .= "<Ds_Card_Country>620</Ds_Card_Country>";
        $xmlResponseSimulated .= "</OPERACION></RETORNOXML>";

        return $xmlResponseSimulated;
    }

    /**
     * @param SimpleXMLElement $oXML
     * @param   $curPayment
     *
     * @return PayGatewayLogLaCaixaResRec
     */
    private function copyXmlResponseToPayGatewayResponse(SimpleXMLElement $oXML, $curPayment)
    {
        /*
         * `idPayment`,  `fecha`,  `Order_number`,`Codigo`,`Amount`, `Currency`,
                                `Signature`, `MerchantCode`, `Terminal`,  `Response`, `AuthorisationCode`,
                                `TransactionType`, `SecurePayment`, `MerchantData`, `XML` ) ".
         * */
        $oGwPayResponse = new PayGatewayLogLaCaixaResRec($this->getPaySupplierId());
        $aFieldsXML = array();
        $aFieldsXML["idPayment"] = $curPayment->id;
        $aFieldsXML["fecha"] = HeDate::todaySQL(true);
        $aFieldsXML["MerchantIdentifier"] = "";
        $aFieldsXML["ExpiryDate"] = "";

        //
        //#abawebapps-170
        if (isset($oXML->CODIGO)) {
            $aFieldsXML["Codigo"] = strval($oXML->CODIGO);
        }
        elseif (isset($oXML->CODIGO[0])) {
            $aFieldsXML["Codigo"] = strval($oXML->CODIGO[0]);
        }

        if (isset($oXML->RECIBIDO)) {
            if (isset($oXML->RECIBIDO[0]->REQUEST)) {

                $aFieldsXML["Order_number"] = strval($oXML->RECIBIDO[0]->REQUEST[0]->DATOSENTRADA[0]->DS_MERCHANT_ORDER);
//                $aFieldsXML["Codigo"] = strval($oXML->CODIGO);
                $aFieldsXML["Amount"] = strval($oXML->RECIBIDO[0]->REQUEST[0]->DATOSENTRADA[0]->DS_MERCHANT_AMOUNT);
                $aFieldsXML["Currency"] = strval($oXML->RECIBIDO[0]->REQUEST[0]->DATOSENTRADA[0]->DS_MERCHANT_CURRENCY);
                $aFieldsXML["Signature"] = strval($oXML->RECIBIDO[0]->REQUEST[0]->DATOSENTRADA[0]->DS_MERCHANT_MERCHANTSIGNATURE);
                $aFieldsXML["MerchantCode"] = strval($oXML->RECIBIDO[0]->REQUEST[0]->DATOSENTRADA[0]->DS_MERCHANT_MERCHANTCODE);
                $aFieldsXML["Terminal"] = strval($oXML->RECIBIDO[0]->REQUEST[0]->DATOSENTRADA[0]->DS_MERCHANT_TERMINAL);
                $aFieldsXML["TransactionType"] = strval($oXML->RECIBIDO[0]->REQUEST[0]->DATOSENTRADA[0]->DS_MERCHANT_TRANSACTIONTYPE);
                $aFieldsXML["MerchantData"] = strval($oXML->RECIBIDO[0]->REQUEST[0]->DATOSENTRADA[0]->DS_MERCHANT_MERCHANTDATA);

                //#4681
                if (isset($oXML->RECIBIDO[0]->REQUEST[0]->DATOSENTRADA[0]->DS_MERCHANT_IDENTIFIER)) {
                    $aFieldsXML["MerchantIdentifier"] = strval($oXML->RECIBIDO[0]->REQUEST[0]->DATOSENTRADA[0]->DS_MERCHANT_IDENTIFIER);
                }
                if (isset($oXML->RECIBIDO[0]->REQUEST[0]->DATOSENTRADA[0]->DS_EXPIRY_DATE)) {
                    $aFieldsXML["ExpiryDate"] = strval($oXML->RECIBIDO[0]->REQUEST[0]->DATOSENTRADA[0]->DS_EXPIRY_DATE);
                }

                $aFieldsXML["Response"] = "";
                $aFieldsXML["AuthorisationCode"] = "";
                $aFieldsXML["SecurePayment"] = "";
                $aFieldsXML["XML"] = $oXML->asXML();
            }
        }

        if (isset($oXML->OPERACION)) {

            $aFieldsXML["Order_number"] = strval($oXML->OPERACION[0]->Ds_Order);
//            $aFieldsXML["Codigo"] = strval($oXML->CODIGO);
            $aFieldsXML["Amount"] = strval($oXML->OPERACION[0]->Ds_Amount);
            $aFieldsXML["Currency"] = strval($oXML->OPERACION[0]->Ds_Currency);
            $aFieldsXML["Signature"] = strval($oXML->OPERACION[0]->Ds_Signature);
            $aFieldsXML["MerchantCode"] = strval($oXML->OPERACION[0]->Ds_MerchantCode);
            $aFieldsXML["Terminal"] = strval($oXML->OPERACION[0]->Ds_Terminal);
            $aFieldsXML["TransactionType"] = strval($oXML->OPERACION[0]->Ds_TransactionType);
            $aFieldsXML["MerchantData"] = strval($oXML->OPERACION[0]->Ds_MerchantData);

            //#4681
            if (isset($oXML->OPERACION[0]->Ds_Merchant_Identifier)) {
                $aFieldsXML["MerchantIdentifier"] = strval($oXML->OPERACION[0]->Ds_Merchant_Identifier);
            }
            if (isset($oXML->OPERACION[0]->Ds_ExpiryDate)) {
                $aFieldsXML["ExpiryDate"] = strval($oXML->OPERACION[0]->Ds_ExpiryDate);
            }

            $aFieldsXML["Response"] = strval($oXML->OPERACION[0]->Ds_Response);
            $aFieldsXML["AuthorisationCode"] = strval($oXML->OPERACION[0]->Ds_AuthorisationCode);
            $aFieldsXML["SecurePayment"] = strval($oXML->OPERACION[0]->Ds_SecurePayment);
            $aFieldsXML["XML"] = $oXML->asXML();
            $this->orderNumber = strval($oXML->OPERACION[0]->Ds_Order);
        }

        $oGwPayResponse->setXMLFieldsToProperties($aFieldsXML);

        return $oGwPayResponse;
    }


    /** Executes the transaction through TPV of La Caixa
     * @param Payment|PaymentControlCheck $curPayment
     * @param string $transactionType
     * @param bool $isRecurringPay
     *
     * @return bool|PayGatewayLogLaCaixaResRec
     */
    public function runDebiting($curPayment, $transactionType = 'A', $isRecurringPay = false)
    {

        //#4681
        $tempCardData = $curPayment->getTempCardData();

        $moUser = new AbaUser();
        $moUser->getUserById($curPayment->userId);

        $this->errorCode = 0;
        $urlSermepa = Yii::app()->config->get("PAY_URL_LACAIXA");
        // Prepare the values we ll send to La Caixa:
        $aSupplierCcyCodes = $this->getPaySupplierCodes($curPayment->currencyTrans);
        $currency = $aSupplierCcyCodes["currency"];
        $code = $aSupplierCcyCodes["code"];
        $terminal = $aSupplierCcyCodes["terminal"];
        $password = $aSupplierCcyCodes["password"];
        $expirationDate = substr($tempCardData['cardYear'], -2) . $tempCardData['cardMonth'];
        $amount = HeMixed::getRoundAmount($curPayment->amountPrice) * 100;
        $order = $curPayment->id . ":" . substr("" . date("Hms"), 3);
        $this->orderNumber = $order;

        //
        $merchantIdentifier = $this->ds_merchant_identifier;
        $merchantDirectpayment = "";

        //
        //#abawebapps-170
//        $withReference = false;
        if ($isRecurringPay AND isset($curPayment->paySuppExtProfId) AND trim($curPayment->paySuppExtProfId) != '') {
            $merchantIdentifier = $curPayment->paySuppExtProfId;
            $merchantDirectpayment = "true";
//            $withReference = true;
        }

        //#4681
        // First we will generate the signature to use
        $sParams = array(
          "amount" => $amount,
          "order" => $order,
          "code" => $code,
          "currency" => $currency,
          "curPayment" => $curPayment,
          "transactionType" => $transactionType,
          "password" => $password,
          "merchantIdentifier" => $merchantIdentifier,
          "merchantDirectpayment" => $merchantDirectpayment,
          "cardNumber" => $tempCardData['cardNumber'],
          "cardCvc" => $tempCardData['cardCvc'],
          "paySuppExtProfId" => "",
        );

        if($isRecurringPay AND isset($curPayment->paySuppExtProfId) AND trim($curPayment->paySuppExtProfId) != '') {
            $sParams["paySuppExtProfId"] = $curPayment->paySuppExtProfId;
        }

        //
        //#abawebapps-170
//        $signature = $this->getSignature($sParams, $withReference);

        //
        // Now we are going to build the XML message
        $xmlSent = "";
        $xmlSentDatosEntrada = "";

        //
        $xmlSent .= "<REQUEST>";

        //
        $xmlSentDatosEntrada .= "<DATOSENTRADA>";
        $xmlSentDatosEntrada .= "<DS_MERCHANT_AMOUNT>" . $amount . "</DS_MERCHANT_AMOUNT>";
        $xmlSentDatosEntrada .= "<DS_MERCHANT_ORDER>" . $order . "</DS_MERCHANT_ORDER>";
        $xmlSentDatosEntrada .= "<DS_MERCHANT_MERCHANTCODE>" . $code . "</DS_MERCHANT_MERCHANTCODE>";
        $xmlSentDatosEntrada .= "<DS_MERCHANT_TERMINAL>" . $terminal . "</DS_MERCHANT_TERMINAL>";
        $xmlSentDatosEntrada .= "<DS_MERCHANT_CURRENCY>" . $currency . "</DS_MERCHANT_CURRENCY>";

        //
        //#abawebapps-170
//         $xmlSentDatosEntrada .= "<DS_MERCHANT_MERCHANTSIGNATURE>$signature</DS_MERCHANT_MERCHANTSIGNATURE>";

        //#4681
        if ($transactionType == 'A') {
            //
            // recurring payment with reference
            if ($isRecurringPay AND isset($curPayment->paySuppExtProfId) AND trim($curPayment->paySuppExtProfId) != '') {
                $xmlSentDatosEntrada .= "<DS_MERCHANT_IDENTIFIER>" . $curPayment->paySuppExtProfId . "</DS_MERCHANT_IDENTIFIER>";
                $xmlSentDatosEntrada .= "<DS_MERCHANT_DIRECTPAYMENT>" . $merchantDirectpayment . "</DS_MERCHANT_DIRECTPAYMENT>";
            }
            else {
                //
                // initial payment OR recurring payment without reference
                $xmlSentDatosEntrada .= "<DS_MERCHANT_PAN>" . trim($tempCardData['cardNumber']) . "</DS_MERCHANT_PAN>";
                $xmlSentDatosEntrada .= "<DS_MERCHANT_EXPIRYDATE>" . trim($expirationDate) . "</DS_MERCHANT_EXPIRYDATE>";
                $xmlSentDatosEntrada .= "<DS_MERCHANT_CVV2>" . trim($tempCardData['cardCvc']) . "</DS_MERCHANT_CVV2>";

                $xmlSentDatosEntrada .= "<DS_MERCHANT_IDENTIFIER>" . $this->ds_merchant_identifier . "</DS_MERCHANT_IDENTIFIER>";
            }
        }

        $xmlSentDatosEntrada .= "<DS_MERCHANT_TRANSACTIONTYPE>" . $transactionType . "</DS_MERCHANT_TRANSACTIONTYPE>";

        //
        //#abawebapps-170
//        $xmlSentDatosEntrada .= "	<DS_VERSION>0.1</DS_VERSION>";
        //$xmlSentDatosEntrada.=	"	<DS_MERCHANT_MERCHANTURL>http://80.25.201.6:91/tpvvirtual/notificacion.php</DS_MERCHANT_MERCHANTURL>";
        $xmlSentDatosEntrada .= "<DS_MERCHANT_MERCHANTDATA>" . trim(Yii::t('mainApp', 'conceptPayment_key')) . "</DS_MERCHANT_MERCHANTDATA>";
        $xmlSentDatosEntrada .= "<DS_MERCHANT_MERCHANTNAME>" . trim(Yii::t('mainApp', 'companyname_key')) . "</DS_MERCHANT_MERCHANTNAME>";

        $xmlSentDatosEntrada .= "</DATOSENTRADA>";

        //
        //#abawebapps-170
//
//        $xmlSent .= "<DATOSENTRADA>";
//        $xmlSent .= "	<DS_VERSION>0.1</DS_VERSION>";
//        $xmlSent .= "	<DS_MERCHANT_CURRENCY>$currency</DS_MERCHANT_CURRENCY>";
//        //$xmlSent.=	"	<DS_MERCHANT_MERCHANTURL>http://80.25.201.6:91/tpvvirtual/notificacion.php</DS_MERCHANT_MERCHANTURL>";
//        $xmlSent .= "	<DS_MERCHANT_TRANSACTIONTYPE>$transactionType</DS_MERCHANT_TRANSACTIONTYPE>";
//        $xmlSent .= "	<DS_MERCHANT_MERCHANTDATA>" . Yii::t('mainApp',
//            'conceptPayment_key') . "</DS_MERCHANT_MERCHANTDATA>";
//        $xmlSent .= "	<DS_MERCHANT_AMOUNT>$amount</DS_MERCHANT_AMOUNT>";
//        $xmlSent .= "	<DS_MERCHANT_MERCHANTNAME>" . Yii::t('mainApp',
//            'companyname_key') . "</DS_MERCHANT_MERCHANTNAME>";
//        $xmlSent .= "	<DS_MERCHANT_MERCHANTSIGNATURE>$signature</DS_MERCHANT_MERCHANTSIGNATURE>";
//        $xmlSent .= "	<DS_MERCHANT_TERMINAL>" . $terminal . "</DS_MERCHANT_TERMINAL>";
//        $xmlSent .= "	<DS_MERCHANT_MERCHANTCODE>$code</DS_MERCHANT_MERCHANTCODE>";
//
//        //#4681
//        if ($transactionType == 'A') {
//
//            //
//            // recurring payment with reference
//            if ($isRecurringPay AND isset($curPayment->paySuppExtProfId) AND trim($curPayment->paySuppExtProfId) != '') {
//                $xmlSent .= "	<DS_MERCHANT_IDENTIFIER>" . $curPayment->paySuppExtProfId . "</DS_MERCHANT_IDENTIFIER>";
//                $xmlSent .= "	<DS_MERCHANT_DIRECTPAYMENT>" . $merchantDirectpayment . "</DS_MERCHANT_DIRECTPAYMENT>";
//            }
//            else {
//                //
//                // initial payment OR recurring payment without reference
//                $xmlSent .= "	<DS_MERCHANT_PAN>" . $tempCardData['cardNumber'] . "</DS_MERCHANT_PAN>";
//                $xmlSent .= "	<DS_MERCHANT_EXPIRYDATE>$expirationDate</DS_MERCHANT_EXPIRYDATE>";
//                $xmlSent .= "	<DS_MERCHANT_CVV2>" . $tempCardData['cardCvc'] . "</DS_MERCHANT_CVV2>";
//
//                $xmlSent .= "	<DS_MERCHANT_IDENTIFIER>" . $this->ds_merchant_identifier . "</DS_MERCHANT_IDENTIFIER>";
//            }
//        }
//
//        $xmlSent .= "	<DS_MERCHANT_ORDER>$order</DS_MERCHANT_ORDER>";
//
//        $xmlSent .= "</DATOSENTRADA>";

        /**
         * #abawebapps-170
         *
         */
        //
        //#abawebapps-170
        $oRedsysAPIWs = new RedsysAPIWs();

        $PAY_KEY_LACAIXA_GW_EUR_SHA256 =    Yii::app()->config->get("PAY_KEY_LACAIXA_GW_EUR_SHA256");
        $PAY_KEY_LACAIXA_GW_USD_SHA256 =    Yii::app()->config->get("PAY_KEY_LACAIXA_GW_USD_SHA256");
        $PAY_LACAIXA_SIGNATUREVERSION =     Yii::app()->config->get("PAY_LACAIXA_SIGNATUREVERSION");

        $sSignature = "";

        switch($currency) {
            case PaySupplierLaCaixa::EUR_CAIXA_CODE:  // EUR
                $sSignature = $oRedsysAPIWs->createMerchantSignatureHostToHost($PAY_KEY_LACAIXA_GW_EUR_SHA256, $xmlSentDatosEntrada);
                break;
            case PaySupplierLaCaixa::USD_CAIXA_CODE: // USD
                $sSignature = $oRedsysAPIWs->createMerchantSignatureHostToHost($PAY_KEY_LACAIXA_GW_USD_SHA256, $xmlSentDatosEntrada);
                break;
        }

        $xmlSent .= $xmlSentDatosEntrada;
        $xmlSent .= "<DS_SIGNATUREVERSION>" . $PAY_LACAIXA_SIGNATUREVERSION . "</DS_SIGNATUREVERSION>";
        $xmlSent .= "<DS_SIGNATURE>" . $sSignature . "</DS_SIGNATURE>";
        $xmlSent .= "</REQUEST>";

        // We send the XML data in a synchronous transaction
        $this->arrayRequest["idPayment"] = $curPayment->id;
        $this->arrayRequest["fecha"] = strval(HeDate::todaySQL(true));
        $this->arrayRequest["Order_number"] = $order;
        $this->arrayRequest["Currency"] = $currency;
        $this->arrayRequest["TransactionType"] = $transactionType;
        $this->arrayRequest["MerchantData"] = Yii::t('mainApp', 'conceptPayment_key');
        $this->arrayRequest["Amount"] = $amount;
        $this->arrayRequest["MerchantName"] = Yii::t('mainApp', 'companyname_key');

        //
        //#abawebapps-170
//        $this->arrayRequest["MerchantSignature"] = $signature;
        $this->arrayRequest["MerchantSignature"] = $sSignature;

        $this->arrayRequest["MerchantCode"] = $code;
        $this->arrayRequest["CreditTarget"] = $tempCardData['cardNumber'];
        $this->arrayRequest["ExpiryDate"] = $expirationDate;
        $this->arrayRequest["Cvv2"] = $tempCardData['cardCvc'];

        //#4681
        $this->arrayRequest["MerchantIdentifier"] = $this->ds_merchant_identifier;

        $this->arrayRequest["XML"] = str_replace($tempCardData['cardNumber'], "****", $xmlSent);

        $aLogRequest = new PayGatewayLogLaCaixaReqSent($this->getPaySupplierId());

        $aLogRequest->setXMLFieldsToProperties($this->arrayRequest);

        if (!$aLogRequest->insertRequestSent()) {
            $this->errorCode = self::ERROR_ABADB_LOGPAY;
            return false;
        }

        if ($this->connectToGateway) {

            try {
                //
                //#abawebapps-170
                $httpResponse = HeComm::sendHTTPPost($urlSermepa, null, 'entrada=' . urlencode($xmlSent));
//                $httpResponse = HeComm::sendHTTPPostRedsys($urlSermepa, $xmlSent);

            } catch (Exception $e) // si no error
            {
                HeLogger::sendLog(" A communication to pay in La Caixa has failed for payment control check: " . $curPayment->id,
                  HeLogger::IT_BUGS, HeLogger::CRITICAL,
                  " An user was not able to finish his transaction, user Id=" . $curPayment->userId . " error curl probably= " . strval($e->getCode()) . "-" . strval($e->getMessage()));
                $this->errorCode = self::ERROR_HTTP;
                return false;
            }
        } else {
            $httpResponse = $this->getSimulateResponse($this->arrayRequest);
        }

        if (!$httpResponse) {
            return false;
        }

        $xmlResponse = $httpResponse;
        $oXmlResponse = new SimpleXMLElement($xmlResponse);
        if (!isset($oXmlResponse->CODIGO)) {
            $this->errorCode = self::ERROR_XML_PARSE;
            return false;
        }

        $this->moLogResponse = $this->copyXmlResponseToPayGatewayResponse($oXmlResponse, $curPayment);
        if (!$this->moLogResponse->insertResponseReceived()) {
            $this->errorCode = self::ERROR_ABADB_LOGPAY;
            return false;
        }

        if (!$this->isSecureResponse($oXmlResponse, $password, $aLogRequest->MerchantSignature)) {
            return false;
        }

        if (!$this->interpretResponse($oXmlResponse, $moUser, $curPayment)) {
            return false;
        }

        if (!$isRecurringPay) {
            HeLogger::sendLog(HeLogger::PREFIX_NOTIF_L . ": A new purchase with La Caixa has been CONFIRMED, the payment id is " .
              $curPayment->id . " from user " . $moUser->email, HeLogger::IT_BUGS, HeLogger::INFO,
              " A purchase with Credit Card has ocurred, we want to follow the details of this purchase, user Id=" .
              $curPayment->userId);
        }

        return $this->moLogResponse;
    }


    /** Alias of self::RunDebitting()
     *
     * @param $moPayCtrlCheck
     * @param AbaUser $moUser
     * @param $moUserCredit
     * @return bool|PayGatewayLogLaCaixaResRec
     */
    public function runDebitingRenewal($moPayCtrlCheck, AbaUser $moUser, $moUserCredit)
    {
        return $this->runDebiting($moPayCtrlCheck, "A", true);
    }

    /** Interpret the XML response from La Caixa, error codes basically. Send a warning in case of error.
     * @param SimpleXMLElement $oXmlResponse
     * @param AbaUser $moUser
     * @param PaymentControlCheck|Payment $curPayment
     *
     * @return bool
     */
    public function interpretResponse(SimpleXMLElement $oXmlResponse, AbaUser $moUser, $curPayment)
    {
        $descErrLaCaixa = $this->getDescBySISCodigo(strval($oXmlResponse->CODIGO));

        if (strval($oXmlResponse->CODIGO) !== '0') {
            $this->errorCode = strval($oXmlResponse->CODIGO);
            $this->errorMsg[$this->errorCode] = $descErrLaCaixa;
            HeLogger::sendLog(HeLogger::PREFIX_NOTIF_L . ": User " . $moUser->email . " CREDIT CARD NOT AUTHORIZED ".
              "BY LA CAIXA, payment " . $curPayment->id."( ENVIRONMENT ".Yii::app()->config->get("SELF_DOMAIN").") ",
              HeLogger::IT_BUGS, HeLogger::CRITICAL,
              " A user has tried to purchase a subscription with a CREDIT CARD NOT AUTHORIZED BY LA CAIXA, ".
              "payment id=" . $curPayment->id . " ( ENVIRONMENT " . Yii::app()->config->get("SELF_DOMAIN") . ") ".
              " ; user email=" . $moUser->email . "; id=" . $curPayment->userId .
              ", Reason=" . $this->errorMsg[$this->errorCode]);

            return false;

        } elseif (intval($curPayment->status) == PAY_REFUND &&
          intval(strval($oXmlResponse->OPERACION[0]->Ds_Response)) == 900
        ) {
            return true;
        } elseif (intval(strval($oXmlResponse->OPERACION[0]->Ds_Response)) >= 99 ||
          trim(strval($oXmlResponse->OPERACION[0]->Ds_AuthorisationCode)) == ""
        ) {
            $this->errorCode = intval(strval($oXmlResponse->OPERACION[0]->Ds_Response));
            return false;
        }

        return true;
    }

    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /** Returns id of translation specific for every payment gateway
     *
     * @return string
     */
    public function getListSpecificConditions()
    {
        return "";
    }

    /**
     * @return PayGatewayLogLaCaixaResRec
     */
    public function getMoLogResponse()
    {
        return $this->moLogResponse;
    }

    /**
     * @param Payment $curPayment
     * @param Payment $moPayOrigin
     *
     * @return bool|PayGatewayLogLaCaixaResRec
     */
    public function runRefund(Payment $curPayment, Payment $moPayOrigin)
    {
        $moUser = new AbaUser();
        $moUser->getUserById($curPayment->userId);
        $transactionType = "3";
        $this->errorCode = 0;
        $urlSermepa = Yii::app()->config->get("PAY_URL_LACAIXA");
        $conceptProduct = strip_tags(Yii::t("mainApp", "w_abono", array(), null,
            $moUser->langEnv) . " " . Yii::t('mainApp', 'conceptPayment_key'));

        // Prepare the values we ll send to La Caixa:
        $aSupplierCcyCodes = $this->getPaySupplierCodes($curPayment->currencyTrans);
        $currency = $aSupplierCcyCodes["currency"];
        $code = $aSupplierCcyCodes["code"];
        $terminal = $aSupplierCcyCodes["terminal"];
        $password = $aSupplierCcyCodes["password"];
//        $expirationDate =       substr($curPayment->cardYear,-2).$curPayment->cardMonth ;
        $amount = HeMixed::getRoundAmount($curPayment->amountPrice) * 100;
        $order = $moPayOrigin->paySuppOrderId;
        $this->orderNumber = $order;

        //
        //#4681 - @TODO $merchantIdentifier ?!?!? NO HACE FALTA EN PRINCIPIO
        //
        // First we will generate the signature to use
        $sParams = array(
          "amount" => $amount,
          "order" => $order,
          "code" => $code,
          "currency" => $currency,
          "curPayment" => $curPayment,
          "transactionType" => $transactionType,
          "password" => $password,
          "merchantIdentifier" => "",
          "merchantDirectpayment" => "",
          "cardNumber" => "",
          "cardCvc" => "",
          "paySuppExtProfId" => "",
        );

        if(isset($curPayment->paySuppExtProfId) AND trim($curPayment->paySuppExtProfId) != '') {
            $sParams["paySuppExtProfId"] = $curPayment->paySuppExtProfId;
        }

        //
        //#abawebapps-170
//        $signature = $this->getSignature($sParams, false);

        //
        // Now we are going to build the XML message
        $xmlSent = "";
        $xmlSentDatosEntrada = "";

        //
        $xmlSent .= "<REQUEST>";

        //
        $xmlSentDatosEntrada .= "<DATOSENTRADA>";

        //
        //#abawebapps-170
//        $xmlSentDatosEntrada .= "<DS_VERSION>0.1</DS_VERSION>";
        $xmlSentDatosEntrada .= "<DS_MERCHANT_CURRENCY>" . $currency . "</DS_MERCHANT_CURRENCY>";
        $xmlSentDatosEntrada .= "<DS_MERCHANT_TRANSACTIONTYPE>" . $transactionType . "</DS_MERCHANT_TRANSACTIONTYPE>";
        $xmlSentDatosEntrada .= "<DS_MERCHANT_MERCHANTDATA>" . $conceptProduct . "</DS_MERCHANT_MERCHANTDATA>";
        $xmlSentDatosEntrada .= "<DS_MERCHANT_AMOUNT>" . $amount . "</DS_MERCHANT_AMOUNT>";
        $xmlSentDatosEntrada .= "<DS_MERCHANT_MERCHANTNAME>" . Yii::t('mainApp', 'companyname_key') . "</DS_MERCHANT_MERCHANTNAME>";
//        $xmlSentDatosEntrada .= "<DS_MERCHANT_MERCHANTSIGNATURE>" . $signature . "</DS_MERCHANT_MERCHANTSIGNATURE>";
        $xmlSentDatosEntrada .= "<DS_MERCHANT_TERMINAL>" . $terminal . "</DS_MERCHANT_TERMINAL>";
        $xmlSentDatosEntrada .= "<DS_MERCHANT_MERCHANTCODE>" . $code . "</DS_MERCHANT_MERCHANTCODE>";
        $xmlSentDatosEntrada .= "<DS_MERCHANT_ORDER>" . $order . "</DS_MERCHANT_ORDER>";

        $xmlSentDatosEntrada .= "</DATOSENTRADA>";

        /**
         * #abawebapps-170
         *
         */

        //
        //#abawebapps-170
        $oRedsysAPIWs = new RedsysAPIWs();

        $PAY_KEY_LACAIXA_GW_EUR_SHA256 =    Yii::app()->config->get("PAY_KEY_LACAIXA_GW_EUR_SHA256");
        $PAY_KEY_LACAIXA_GW_USD_SHA256 =    Yii::app()->config->get("PAY_KEY_LACAIXA_GW_USD_SHA256");
        $PAY_LACAIXA_SIGNATUREVERSION =     Yii::app()->config->get("PAY_LACAIXA_SIGNATUREVERSION");

        $sSignature = "";

        switch($currency) {
            case PaySupplierLaCaixa::EUR_CAIXA_CODE: // EUR
                $sSignature = $oRedsysAPIWs->createMerchantSignatureHostToHost($PAY_KEY_LACAIXA_GW_EUR_SHA256, $xmlSentDatosEntrada);
                break;
            case PaySupplierLaCaixa::USD_CAIXA_CODE: // USD
                $sSignature = $oRedsysAPIWs->createMerchantSignatureHostToHost($PAY_KEY_LACAIXA_GW_USD_SHA256, $xmlSentDatosEntrada);
                break;
        }

        $xmlSent .= $xmlSentDatosEntrada;
        $xmlSent .= "<DS_SIGNATUREVERSION>" . $PAY_LACAIXA_SIGNATUREVERSION . "</DS_SIGNATUREVERSION>";
        $xmlSent .= "<DS_SIGNATURE>" . $sSignature . "</DS_SIGNATURE>";
        $xmlSent .= "</REQUEST>";


        //
        //#abawebapps-170
//        // Now we are going to build the XML message
//        $xmlSent = "";
//        $xmlSent .= "<DATOSENTRADA>";
//        $xmlSent .= "	<DS_VERSION>0.1</DS_VERSION>";
//        $xmlSent .= "	<DS_MERCHANT_CURRENCY>$currency</DS_MERCHANT_CURRENCY>";
//        $xmlSent .= "	<DS_MERCHANT_TRANSACTIONTYPE>$transactionType</DS_MERCHANT_TRANSACTIONTYPE>";
//        $xmlSent .= "	<DS_MERCHANT_MERCHANTDATA>" . $conceptProduct . "</DS_MERCHANT_MERCHANTDATA>";
//        $xmlSent .= "	<DS_MERCHANT_AMOUNT>$amount</DS_MERCHANT_AMOUNT>";
//        $xmlSent .= "	<DS_MERCHANT_MERCHANTNAME>" . Yii::t('mainApp',
//            'companyname_key') . "</DS_MERCHANT_MERCHANTNAME>";
//        $xmlSent .= "	<DS_MERCHANT_MERCHANTSIGNATURE>$signature</DS_MERCHANT_MERCHANTSIGNATURE>";
//        $xmlSent .= "	<DS_MERCHANT_TERMINAL>" . $terminal . "</DS_MERCHANT_TERMINAL>";
//        $xmlSent .= "	<DS_MERCHANT_MERCHANTCODE>$code</DS_MERCHANT_MERCHANTCODE>";
//        $xmlSent .= "	<DS_MERCHANT_ORDER>$order</DS_MERCHANT_ORDER>";
//        $xmlSent .= "</DATOSENTRADA>";

        // We send the XML data in a synchronous transaction
        $this->arrayRequest["idPayment"] = $curPayment->id;
        $this->arrayRequest["fecha"] = strval(HeDate::todaySQL(true));
        $this->arrayRequest["Order_number"] = $order;
        $this->arrayRequest["Currency"] = $currency;
        $this->arrayRequest["TransactionType"] = $transactionType;
        $this->arrayRequest["MerchantData"] = $conceptProduct;
        $this->arrayRequest["Amount"] = $amount;
        $this->arrayRequest["MerchantName"] = Yii::t('mainApp', 'companyname_key');

        //
        //#abawebapps-170
//        $this->arrayRequest["MerchantSignature"] = $signature;
        $this->arrayRequest["MerchantSignature"] = $sSignature;

        $this->arrayRequest["MerchantCode"] = $code;
        $this->arrayRequest["CreditTarget"] = "";
        $this->arrayRequest["ExpiryDate"] = "";
        $this->arrayRequest["Cvv2"] = "";
        $this->arrayRequest["XML"] = $xmlSent;

        $aLogRequest = new PayGatewayLogLaCaixaReqSent($this->getPaySupplierId());
        $aLogRequest->setXMLFieldsToProperties($this->arrayRequest);
        if (!$aLogRequest->insertRequestSent()) {
            $this->errorCode = self::ERROR_ABADB_LOGPAY;
            return false;
        }

        if ($this->connectToGateway) {
            try {
                //
                //#abawebapps-170
                $httpResponse = HeComm::sendHTTPPost($urlSermepa, null, "entrada=" . urlencode($xmlSent));
//                $httpResponse = HeComm::sendHTTPPostRedsys($urlSermepa, $xmlSent);
            } catch (Exception $e) // si no error
            {
                HeLogger::sendLog(" A communication to REFUND in La Caixa has failed for payment: " . $curPayment->id,
                  HeLogger::IT_BUGS, HeLogger::CRITICAL,
                  " A REFUND has not been finished right, user Id=" . $curPayment->userId . " error curl probably= " .
                  strval($e->getCode()) . "-" . strval($e->getMessage()));
                $this->errorCode = self::ERROR_HTTP;
                return false;
            }
        } else {
            $httpResponse = $this->getSimulateResponse($this->arrayRequest);
        }

        if (!$httpResponse) {
            return false;
        }

        $xmlResponse = $httpResponse;
        $oXmlResponse = new SimpleXMLElement($xmlResponse);
        if (!isset($oXmlResponse->CODIGO)) {
            $this->errorCode = self::ERROR_XML_PARSE;
            return false;
        }

        $this->moLogResponse = $this->copyXmlResponseToPayGatewayResponse($oXmlResponse, $curPayment);
        if (!$this->moLogResponse->insertResponseReceived()) {
            $this->errorCode = self::ERROR_ABADB_LOGPAY;
            return false;
        }

        if (!$this->isSecureResponse($oXmlResponse, $password, $aLogRequest->MerchantSignature)) {
            return false;
        }

        if (!$this->interpretResponse($oXmlResponse, $moUser, $curPayment)) {
            return false;
        }

        return $this->moLogResponse;
    }


    /** It is not necessary to change anything in the remote platform of La Caixa. So we return true.
     *
     * @param AbaUserCreditForms $moOldUserCredit
     * @param AbaUserCreditForms $moNewUserCredit
     *
     * @return bool
     */
    public function runChangeCreditDetails(AbaUserCreditForms $moOldUserCredit, AbaUserCreditForms $moNewUserCredit)
    {
        return true;
    }

    /**
     * @param $oXmlResponse
     * @param string $password
     * @param string $requestSignature
     *
     * @return bool
     */
    protected function isSecureResponse($oXmlResponse, $password, $requestSignature)
    {
        //
        // SimpleXMLElement Object ( [CODIGO] => SIS0042 [RECIBIDO] => SimpleXMLElement Object ( [REQUEST] => SimpleXMLElement Object ( [DATOSENTRADA] => SimpleXMLElement Object ( [DS_MERCHANT_AMOUNT] => 13999 [DS_MERCHANT_ORDER] => 66efbf44:133 [DS_MERCHANT_MERCHANTCODE] => 010454155 [DS_MERCHANT_CURRENCY] => 978 [DS_MERCHANT_PAN] => 4548812049400004 [DS_MERCHANT_CVV2] => 285 [DS_MERCHANT_EXPIRYDATE] => 1810 [DS_MERCHANT_IDENTIFIER] => REQUIRED [DS_MERCHANT_TRANSACTIONTYPE] => A [DS_MERCHANT_TERMINAL] => 002 [DS_MERCHANT_MERCHANTDATA] => ABA English cuota mensual [DS_MERCHANT_MERCHANTNAME] => ABA English S.L ) [DS_SIGNATUREVERSION] => HMAC_SHA256_V1 [DS_SIGNATURE] => 4Me9Kcij970o98LeM j5Gg190dOYJ85miVfRS9zJIeQ= ) ) )
        // SimpleXMLElement Object ( [CODIGO] => 0 [OPERACION] => SimpleXMLElement Object ( [Ds_Amount] => 13999 [Ds_Currency] => 978 [Ds_Order] => 66efbf44:144 [Ds_Signature] => v1vfR1sCxytt1AuETR1ulB8z7jx+myGKqQ4jtN9xBnU= [Ds_MerchantCode] => 010454155 [Ds_Terminal] => 2 [Ds_Response] => 0000 [Ds_AuthorisationCode] => 290827 [Ds_TransactionType] => A [Ds_SecurePayment] => 0 [Ds_Language] => 1 [Ds_ExpiryDate] => 1810 [Ds_Merchant_Identifier] => fcb2ed18fee747a470c710dfd37566f51d9ef339 [Ds_MerchantData] => ABA English cuota mensual [Ds_Card_Country] => 724 ) )
        //
        //#abawebapps-170
        if (property_exists($oXmlResponse, "OPERACION")) {

            //
            // Cadena = Ds_Amount + Ds_Order + Ds_MerchantCode + Ds_Currency + Ds_Response + Ds_TransactionType + Ds_SecurePayment

            $phrase = trim(strval($oXmlResponse->OPERACION[0]->Ds_Amount)) .
              trim(strval($oXmlResponse->OPERACION[0]->Ds_Order)) .
              trim(strval($oXmlResponse->OPERACION[0]->Ds_MerchantCode)) .
              trim(strval($oXmlResponse->OPERACION[0]->Ds_Currency)) .
              trim(strval($oXmlResponse->OPERACION[0]->Ds_Response)) .
//              trim(strval($oXmlResponse->OPERACION[0]->Ds_CardNumber)) .
              trim(strval($oXmlResponse->OPERACION[0]->Ds_TransactionType)) .
              trim(strval($oXmlResponse->OPERACION[0]->Ds_SecurePayment))
//              . $password
            ;

            $dsSignature = strtoupper(trim(strval($oXmlResponse->OPERACION[0]->Ds_Signature)));

            $PAY_KEY_LACAIXA_GW_EUR_SHA256 =    Yii::app()->config->get("PAY_KEY_LACAIXA_GW_EUR_SHA256");
            $PAY_KEY_LACAIXA_GW_USD_SHA256 =    Yii::app()->config->get("PAY_KEY_LACAIXA_GW_USD_SHA256");

            /**
             * #abawebaps-170
             *
             */
            $oRedsysAPIWs = new RedsysAPIWs();

            $localSignature = strtoupper(trim($oRedsysAPIWs->createMerchantSignatureResponseHostToHost($PAY_KEY_LACAIXA_GW_EUR_SHA256, $phrase, trim(strval($oXmlResponse->OPERACION[0]->Ds_Order)))));
//            $localSignature = "";

            $currency = trim(strval($oXmlResponse->OPERACION[0]->Ds_Currency));

            switch($currency) {
                case PaySupplierLaCaixa::EUR_CAIXA_CODE: // EUR
                    $localSignature = strtoupper(trim($oRedsysAPIWs->createMerchantSignatureResponseHostToHost($PAY_KEY_LACAIXA_GW_EUR_SHA256, $phrase, trim(strval($oXmlResponse->OPERACION[0]->Ds_Order)))));
                    break;
                case PaySupplierLaCaixa::USD_CAIXA_CODE: // USD
                    $localSignature = strtoupper(trim($oRedsysAPIWs->createMerchantSignatureResponseHostToHost($PAY_KEY_LACAIXA_GW_USD_SHA256, $phrase, trim(strval($oXmlResponse->OPERACION[0]->Ds_Order)))));
                    break;
            }

            //
            //#abawebapps-170
//            $localSignature = strtoupper(trim(HeAbaSHA::sha_1($phrase)));

            if ($dsSignature != $localSignature) {
                // Send notification warning to IT
                HeLogger::sendLog(
                  HeLogger::PREFIX_NOTIF_L." Warning on signature from response Caixa paymentId ".$this->orderNumber,
                  HeLogger::IT_BUGS,
                  HeLogger::CRITICAL,
                  "The signature from the XML response is wrong, this situation is suspicious, please review logs ".
                  "for this payment id ".$this->orderNumber.". The Caixa signature is = ".$dsSignature.
                  " and the local one is ".$localSignature
                );
                return false; //true; // false
            } else {
                return true;
            }
        } else {

            if (property_exists($oXmlResponse, "RECIBIDO")) {
                if ($requestSignature != strval($oXmlResponse->RECIBIDO[0]->REQUEST[0]->DATOSENTRADA[0]->DS_MERCHANT_MERCHANTSIGNATURE)) {
                    // Send notification warning to IT
                    HeLogger::sendLog(
                      HeLogger::PREFIX_NOTIF_L." Warning on signature from response Caixa paymentId ".$this->orderNumber,
                      HeLogger::IT_BUGS,
                      HeLogger::CRITICAL,
                      "The signature from the XML response is wrong, this situation is suspicious, please review logs ".
                      "for this payment id ".$this->orderNumber
                    );
                    return false; //return true; //false
                } else {
                    return true;
                }
            }
        }

        return false;
    }

    /** It always returns true because it has nothing to do in the remote environment from La Caixa.
     * @param Payment $moPayment
     * @param AbaUserCreditForms $moUserCredit
     *
     * @return bool
     */
    public function runCancelRecurring(Payment $moPayment, AbaUserCreditForms $moUserCredit)
    {
        return true;
    }

    /**
     * @param $moPay
     *
     * @return bool
     */
    public static function validateCardDataBypaySuppExtProfId($moPay) {

        if($moPay->paySuppExtId == PAY_SUPPLIER_CAIXA) {
            if(trim($moPay->paySuppExtProfId) == '') {
                return true;
            }
        }
        return false;
    }

}

/**
 * Class RedsysAPIWs
 */
class RedsysAPIWs{

    /******  Array de DatosEntrada ******/
    var $vars_pay = array();

    /******  Set parameter ******/
    function setParameter($key, $value) {
        $this->vars_pay[$key] = $value;
    }

    /******  Get parameter ******/
    function getParameter($key) {
        return $this->vars_pay[$key];
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    ////////////					FUNCIONES AUXILIARES:							  ////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////

    /******  3DES Function  ******/
    function encrypt_3DES($message, $key) {
        // Se establece un IV por defecto
        $bytes = array(0,0,0,0,0,0,0,0); //byte [] IV = {0, 0, 0, 0, 0, 0, 0, 0}
        $iv = implode(array_map("chr", $bytes)); //PHP 4 >= 4.0.2

        // Se cifra
        $ciphertext = mcrypt_encrypt(MCRYPT_3DES, $key, $message, MCRYPT_MODE_CBC, $iv); //PHP 4 >= 4.0.2
        return $ciphertext;
    }

    /******  Base64 Functions  ******/
    function base64_url_encode($input) {
        return strtr(base64_encode($input), '+/', '-_');
    }

    function encodeBase64($data) {
        $data = base64_encode($data);
        return $data;
    }

    function base64_url_decode($input) {
        return base64_decode(strtr($input, '-_', '+/'));
    }

    function decodeBase64($data) {
        $data = base64_decode($data);
        return $data;
    }

    /******  MAC Function ******/
    function mac256($ent,$key) {
        $res = hash_hmac('sha256', $ent, $key, true);//(PHP 5 >= 5.1.2)
        return $res;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    ////////////	   FUNCIONES PARA LA GENERACIÓN DE LA PETICIÓN DE PAGO:			  ////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////

    /******  Obtener Número de pedido ******/
    function getOrder($datos) {
        $posPedidoIni = strrpos($datos, "<DS_MERCHANT_ORDER>");
        $tamPedidoIni = strlen("<DS_MERCHANT_ORDER>");
        $posPedidoFin = strrpos($datos, "</DS_MERCHANT_ORDER>");

        return substr($datos, $posPedidoIni + $tamPedidoIni, $posPedidoFin - ($posPedidoIni + $tamPedidoIni));
    }

    function createMerchantSignatureHostToHost($key, $ent) {
        // Se decodifica la clave Base64
        $key = $this->decodeBase64($key);
        // Se diversifica la clave con el Número de Pedido
        $key = $this->encrypt_3DES($this->getOrder($ent), $key);
        // MAC256 del parámetro Ds_MerchantParameters
        $res = $this->mac256($ent, $key);
        // Se codifican los datos Base64
        return $this->encodeBase64($res);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////// FUNCIONES PARA LA RECEPCIÓN DE DATOS DE PAGO (Respuesta HOST to HOST) ///////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////

    function createMerchantSignatureResponseHostToHost($key, $datos, $numPedido) {
        // Se decodifica la clave Base64
        $key = $this->decodeBase64($key);
        // Se diversifica la clave con el Número de Pedido
        $key = $this->encrypt_3DES($numPedido, $key);
        // MAC256 del parámetro Ds_Parameters que envía Redsys
        $res = $this->mac256($datos, $key);
        // Se codifican los datos Base64
        return $this->encodeBase64($res);
    }
}

