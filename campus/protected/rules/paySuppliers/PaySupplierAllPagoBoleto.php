<?php
/**
 * Class PaySupplierAllPagoBoleto
 *
 * PayAllPago - PHP class to integrate with Allpagos.com
 * NOTE: Requires PHP version 5 or later
 * @package AllPagos
 * @author Joaquim Forcada Jiménez
 */
class PaySupplierAllPagoBoleto extends PaySupplierAllPagoBr
{

    public function __construct()
    {
        parent::__construct();
        $this->idPaySupplier = PAY_SUPPLIER_ALLPAGO_BR_BOL;
        $this->channel = Yii::app()->config->get("PAY_ALLPAGO_CHANNEL_INITIAL_BR_BOLETO");
        $this->channelInitial = Yii::app()->config->get("PAY_ALLPAGO_CHANNEL_INITIAL_BR_BOLETO");
    }

    /** Basically it only overrides the argument transactionType.
     *
     * @param PaymentControlCheck $curPayment
     * @param AbaUser $moUser
     * @param string $transactionType
     * @return bool|PayGatewayLogAllpago
     */
    public function opRunDebit(PaymentControlCheck $curPayment, AbaUser $moUser, $transactionType = "PP.PA")
    {
        return parent::opRunDebit($curPayment, $moUser, $transactionType);
    }


    /** Returns an array of integer as a key, and string as a value. Each of implementations for this definition class
     * should have at least one element within the array.
     *
     * @return array
     */
    public function getListOfCards()
    {
        return array();
    }

    /** Special behaviour for MEXICO and Boletos payments.
     *
     * @param AbaUser $moUser
     * @param string $address_country
     * @return AbaUserAddressInvoice|bool
     */
    protected function setCustomerAddress(AbaUser $moUser, $address_country)
    {
        if( !parent::setCustomerAddress($moUser, $address_country) ) {
            $this->address_street="No street informed";
            $this->address_zip="No zip code informed";
            $this->address_city="No city informed";
            $this->address_state="No state informed";
        }

        return true;
    }

    /**
     * @param PaymentControlCheck $objSpecialFields
     *
     * @return bool
     */
    public function setSpecialFields($objSpecialFields)
    {
        $this->specialFields['cpfBrasil'] = $objSpecialFields->cpfBrasil;
        $this->specialFields['requestDate'] = $objSpecialFields->dateStartTransaction;
        $this->specialFields['dueDate'] = $objSpecialFields->dateToPay;
        return true;
    }

    /** These fields need to be filled in by the user.
     * @return array
     */
    public function getSpecialDisplayFields()
    {
        return array( 'firstName', 'secondName', 'street', 'city', 'state', 'zipCode', 'cpfBrasil' );
    }

    /**
     * @param float $payment_amount
     * @param string $payment_usage
     * @param string $identification_transactionid
     * @param string $payment_currency
     * @param string $reference_id
     * @internal param string $shopper_id
     */
    protected function setPaymentInformation($payment_amount, $payment_usage, $identification_transactionid,
                                             $payment_currency, $reference_id = '')
    {
        $this->payment_amount = $payment_amount;
        $this->payment_usage = $payment_usage;
        $this->identification_transactionid = $identification_transactionid;
        $this->shopper_id = $this->specialFields['cpfBrasil'];
        $this->payment_currency = $payment_currency;
        if ($reference_id != '') {
            $this->reference_id = $reference_id;
        }
    }

    /** Sets the registration tag values for the XML that will be sent to ALLPAGO.
     *
     * @param string $identification_transactionid
     *
     * @return void
     */
    protected function setRegistrationId($identification_transactionid)
    {
        $this->identification_transactionid = $identification_transactionid;
        $this->shopper_id = $this->specialFields['cpfBrasil'];
    }

    /** We override this function in order to avoid credit card details in the XML.
     *
     * @param $account_holder
     * @param $account_brand
     * @param $account_number
     * @param $account_bankname
     * @param $account_country
     * @param $account_authorisation
     * @param $account_verification
     * @param $account_year
     * @param $account_month
     *
     * @return bool|void
     */
    protected function setAccountInformation($account_holder,$account_brand,$account_number,$account_bankname,
                                             $account_country, $account_authorisation,$account_verification,$account_year,$account_month)
    {
       return false;
    }


    /**
     * @param Payment $curPayment
     * @param Payment $moPayOrigin
     * @param string $transactionType
     * @return bool
     */
    public function opRunRefund(Payment $curPayment, Payment $moPayOrigin, $transactionType = "PP.RF")
    {
        return false;
    }

    /** We override the header, beacuase of f. Bradesco specifications.
     * Add payment usage to the boleto description.
     *
     * @return bool
     */
    protected function commitXMLPayment()
    {
        parent::commitXMLPayment();

        $strXML = $this->xmlRequest ;

        $strToReplace = "</Analysis>";
        $strNewText = '<Criterion name="BRADESCO_cpfsacado">'.$this->specialFields['cpfBrasil'].'</Criterion>'.
                        '<Criterion name="BRADESCO_datavencimento">'.str_replace( '-', '/', HeDate::SQLToEuropean($this->specialFields['dueDate'], '-', 'dd/mm/yyyy')).'</Criterion>'.
                        '<Criterion name="BRADESCO_instrucao1">'.urlencode(Yii::app()->config->get('SELF_DOMAIN')).'</Criterion>'.
                        '<Criterion name="BRADESCO_instrucao2">'.$this->payment_usage.'</Criterion>'.
                        '<Criterion name="BRADESCO_instrucao3">'.Yii::t('mainApp','key_dueDate', array( '{dueDate}'=> $this->specialFields['dueDate'])).'</Criterion>'.
                        '</Analysis>';        
        $strXML = str_replace($strToReplace, $strNewText, $strXML);
        $this->xmlRequest = $strXML;        
        return true;
    }


    /** Prepares the XML query request in order to retrieve
     * a list of transactions
     *
     * @return bool
     */
    protected function commitXMLQuery()
    {
        $xmlFile = '<?xml version="1.0" encoding="UTF-8" ?>';
        $xmlFile .= '<Request version="1.0"  encoding="UTF-8"><Header><Security sender="'.$this->sender.'"/></Header>';
        $xmlFile .= '<Query mode="'.$this->transaction_mode.'" level="CHANNEL" entity="'.$this->channelInitial.'" type="STANDARD" maxCount="400">';
        $xmlFile .= '<User login="'.$this->userid.'" pwd="'.$this->userpwd.'"/>';
        $xmlFile .= '<ProcessingResult>ACK</ProcessingResult>';
        $xmlFile .= '<Period from="'.$this->dateQueryFrom.'" to="'.$this->dateQueryUntil.'"/>';
        // $xmlFile .= '<Methods><Method code="CC"></Method></Methods>';// We only deal with credit operations with Brazil.
        $xmlFile .= '<Types>
                            <Type code="RC"/>
                     </Types>';

        $xmlFile .= '</Query></Request>';
        $this->xmlRequest = $xmlFile;
        return true;
    }

    /** Returns id of translation specific for every payment gateway
     *
     * @return string
     */
    public function getListSpecificConditions()
    {
        return 'key_allpago_conditions_privacy_boleto';
    }

    /**
     * @param $contact_email
     * @param $contact_mobile
     * @param $contact_ip
     * @param $contact_phone
     */
    protected function setCustomerContact($contact_email,$contact_mobile,$contact_ip,$contact_phone)
    {
        $this->contact_email=$contact_email;
        $this->contact_mobile='';
        $this->contact_ip=$contact_ip;
        $this->contact_phone='';
    }

}

