<?php
/**
 * Created by Joaquim Forcada (Quino)
 * User: Joaquim Forcada (Quino)
 * Date: 16/04/14
 * Time: 13:50
 
 ---Please brief description here---- 
 */

class InvoicesCommon
{
    /** It returns if the user should see all sections associated with requesting invoices.
     * If user is PREMIUM and has any payment valid for generating invoice or has any address availvable.
     *
     * @param $userId
     *
     * @return bool
     */
    public function isDisplayInvoices($userId)
    {
        $fDisplayInvoices = false;
        $moLastSuccessPay = new Payment();
        if( $moLastSuccessPay->getLastSuccessPayment($userId) ){
            $fDisplayInvoices = true;
        }
        $moUserAddress = new AbaUserAddressInvoice();
        if ($moUserAddress->getUserAddressByUserId($userId)){
            $fDisplayInvoices = true;
        }

        return $fDisplayInvoices;
    }

    /**
     * @param integer $userId
     * @param ProfileForm $modProfileForm
     *
     * @return bool|int|string
     */
    public function saveInvoice( $userId, ProfileForm $modProfileForm)
    {
        $moAddress = new AbaUserAddressInvoice();
        $exists =  false;
        if($moAddress->getUserAddressByUserId($userId)){
            $exists =  true;
        }

        $moAddress->userId = $userId;
        $moAddress->name = $modProfileForm->invoicesName;
        $moAddress->lastName = $modProfileForm->invoicesSurname;
        $moAddress->city = $modProfileForm->invoicesCity;
        $moAddress->street = $modProfileForm->invoicesStreet ;
        $moAddress->streetMore = $modProfileForm->invoicesStreetMore ;
        $moAddress->zipCode= $modProfileForm->invoicesPostalCode ;
        $moAddress->city = $modProfileForm->invoicesCity;
        $moAddress->state = $modProfileForm->invoicesProvince;
        $moAddress->country = $modProfileForm->invoicesCountry;
        $moAddress->vatNumber = $modProfileForm->invoicesTaxIdNumber;
        $moAddress->default = 1;
        if(!$moAddress->validate()){
            return false;
        }

        if($exists){
            $moAddress->update();
            return true;
        } else{
            return $moAddress->insertUserAddress();
        }


    }

    public function setFromInvoiceToForm( AbaUserAddressInvoice $moAddress, ProfileForm $modProfileForm)
    {
        $modProfileForm->invoicesId = $moAddress->id;
        $modProfileForm->invoicesName = $moAddress->name ;
        $modProfileForm->invoicesSurname = $moAddress->lastName;
        $modProfileForm->invoicesCity = $moAddress->city;
        $modProfileForm->invoicesStreet  = $moAddress->street;
        $modProfileForm->invoicesStreetMore = $moAddress->streetMore;
        $modProfileForm->invoicesPostalCode = $moAddress->zipCode ;
        $modProfileForm->invoicesCity = $moAddress->city;
        $modProfileForm->invoicesProvince = $moAddress->state;
        $modProfileForm->invoicesCountry = $moAddress->country;
        $modProfileForm->invoicesTaxIdNumber = $moAddress->vatNumber;

        return $modProfileForm;
    }


}