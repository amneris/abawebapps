<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 18/12/2013
 * Time: 15:57
 * To be used by La Caixa, PayPal or whatever other Gateways Suppliers we ve got in Aba.
 * The idea is to unify all logic from here.
 */
class OnlinePayCommon
{
    private $errorCode ;
    private $errorMessage ;

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param PaymentForm $modFormPayment
     * @param             $paySuppExtId
     * @param AbaUser $user
     * @param int $idPartner
     *
     * @return bool|PaymentControlCheck
     */
    public function savePaymentBeforeGateway( PaymentForm &$modFormPayment, $paySuppExtId, AbaUser $user,
                                              $idPartner=300001, $validate=true, $bPlanPrice=false, $stAdyenHppData=array() )
    {

        $idPaymentCtrl = HeAbaSHA::generateIdPayment($user);

        if (isset(Yii::app()->user->paymentControlCheckId)) {
            //
            //#abawebapps-190
            $oPayment = new Payment();
            if(!$oPayment->getPaymentById(Yii::app()->user->paymentControlCheckId)) {
                $idPaymentCtrl = Yii::app()->user->paymentControlCheckId;
            }
        }

        /* @TODO: We must control if in the last 1 minute/seconds (parametrized) there has been
         * any communication with any payment gateway and user did not wait response.
         */

        $payControl =       new PaymentControlCheck();
        $aPKProductPrice =  HePayments::parseProductPricePK($modFormPayment->packageSelected);

        // Validate the promocode again in case of an older promocode in session is still active.
        // In that case we simply ignore the promocode.
        $userPromoCode = "";
        if (!is_null(Yii::app()->user->getState("promoCode")) && Yii::app()->user->getState("promoCode") !== '') {
            $userPromoCode = Yii::app()->user->getState("promoCode");
            $moPromocode = new ProductPromo();
            if (!$moPromocode->getProdPromoById($userPromoCode, 1, 1, true)) {
                $userPromoCode = "";
            }
            else {

                if(isset($aPKProductPrice[0])) {

                    $oProductPrice = new ProductPrice();
                    $oProductPrice->getProductById($aPKProductPrice[0]);

                    //
                    //#4.101.1
                    $userPromoCode = ProductPrice::adjustPromoCode($userPromoCode, $oProductPrice->isPlanPrice, $moPromocode->idPromoType);
                }
            }
        }

        //
        //#5739
        $iProductPrice = null;
        $experimentVariationAttributeId = null;
        $idSession =Yii::app()->getSession()->getSessionId();
        $oProductsPricesCommon = new ProductsPricesCommon();
        $stUserProdsPrices = $oProductsPricesCommon->getAllAvailableExperimentsVariationsPrices($user);
        foreach ($stUserProdsPrices as $stUserProdsPrice) {
            if ($stUserProdsPrice['idProduct'] == $aPKProductPrice[0]) {
                $iProductPrice = $stUserProdsPrice["price"];
                $experimentVariationAttributeId = $stUserProdsPrice["experimentVariationAttributeId"];
                break;
            }
        }

        //
        //#abawebapps-158
        $sCurrency = "";

        if ($modFormPayment->idPayMethod == PAY_METHOD_ADYEN_HPP AND isset($stAdyenHppData["paymentBrandCode"]) AND isset($aPKProductPrice[0])) {

            //
            //#abawebapps-158
            $commProducts = new ProductsPricesCommon();
            $adyenHppPaymentMethod = $commProducts->getAdyenHppPrice($stAdyenHppData["paymentBrandCode"], $aPKProductPrice[0], $user);

            if(isset($adyenHppPaymentMethod["price"])) {
                $iProductPrice = $adyenHppPaymentMethod["price"];
                $sCurrency = $adyenHppPaymentMethod["currency"];
            }
        }

        if ($payControl->copyPaymentFormToControlCheck( $user->id, 1, $aPKProductPrice[0],
            $aPKProductPrice[1], $aPKProductPrice[2], $userPromoCode, $modFormPayment->creditCardType,
            $modFormPayment->creditCardName, $modFormPayment->creditCardNumber,
            $modFormPayment->creditCardYear, $modFormPayment->creditCardMonth, $modFormPayment->CVC, $paySuppExtId,
            $modFormPayment->cpfBrasil, $modFormPayment->typeCpf, $idPartner, $validate, $bPlanPrice, $iProductPrice, $experimentVariationAttributeId,
            $idSession, $sCurrency)
        ) {

            // #4400
            $payControl->detectUnknownCreditCardType($modFormPayment->creditCardNumber);

            if ($payControl->insertPayment($idPaymentCtrl)) {
                $payControl->id = $idPaymentCtrl;
                Yii::app()->user->setState("paymentControlCheckId", $payControl->id);
                Yii::app()->user->setState("paymentControlStatus", $payControl->status);
                $payControl->setFirstNameTmp($modFormPayment->firstName);
                $payControl->setSecondNameTmp($modFormPayment->secondName);

                /* In case of AllPago-Mexico at the moment (2014-11-11) */
                if ($modFormPayment->street != '' && !$modFormPayment->isChangeProd) {
                    $moUserAddress = new AbaUserAddressInvoice();
                    if ($moUserAddress->setUserAddressForm($user->id, $modFormPayment->firstName, $modFormPayment->secondName,
                            '', $modFormPayment->street, '', $modFormPayment->zipCode,
                            $modFormPayment->city, $modFormPayment->state, $user->countryId)
                    ) {
                        $moUserAddress->insertUserAddress();
                    }
                }

                return $payControl;
            }
        }

        $modFormPayment->addError("PaymentControlProcess", Yii::t('mainApp', 'errorprepayment_key') );
        $modFormPayment->addErrors($payControl->getErrors());
        return false;
    }

    /** After Gateway confirmation saves everything to database because everything is confirmed.
     * @param PaymentForm $modFormPayment
     * @param PaymentControlCheck $paymentControlCheck
     * @param PayGatewayLogAllpago|PayGatewayLogLaCaixaResRec|PayGatewayLogPayPal|PayGatewayLogAdyenResRec $payGwLog
     * @param integer $idPartner
     * @param int $defaultToRenew ; This payment method will be used as the default one for this user.
     * @param int $isExtend ; It is an extra payment
     * @param int $isRecurring ; It is a recurring payment
     *
     * @return bool|Payment
     */
    public function savePaymentCtrlCheckToUserPayment( PaymentForm &$modFormPayment,
                                                        PaymentControlCheck $paymentControlCheck,
                                                        $payGwLog, $idPartner, $defaultToRenew=1, $isExtend=0, $isRecurring=0)
    {
        /** 3.Update AbaUser*/
        $moUser = $this->saveUserData($modFormPayment);
        if (!$moUser) {
            $this->errorMessage = 'You user name and last name could not be updated.';
            return false;
        }
        /* 3.1. Insert user_credit_forms  */
        $errorMsg =         "";
        $aDataPayMethod =   array();

        $aDataPayMethod["cpfBrasil"] =          $modFormPayment->cpfBrasil;
        $aDataPayMethod["typeCpf"] =            $modFormPayment->typeCpf;
        $aDataPayMethod["creditCardType"] =     HePayments::getCreditCardType($modFormPayment->creditCardNumber);
        $aDataPayMethod["creditCardNumber"] =   "";
        $aDataPayMethod["creditCardYear"] =     "";
        $aDataPayMethod["creditCardMonth"] =    "";
        $aDataPayMethod["CVC"] =                "";
        $aDataPayMethod["creditCardName"] =     "";

        $userCreditCard = $this->saveCreditFormDataFromPayment($aDataPayMethod, $errorMsg, $moUser, $defaultToRenew);

        //
        if (!$userCreditCard) {
            $modFormPayment->addError("credit", $errorMsg);
            return false;
        }

        // If this is a boleto payment we set it as not isExtended.
        if ($defaultToRenew == 0 && $modFormPayment->idPayMethod == PAY_METHOD_BOLETO){
            $isExtend = 0;
        }
        /* 3.3.  Insert in Payments PENDING,*/
        $payment = $this->savePayment($modFormPayment, $paymentControlCheck, $userCreditCard, $payGwLog,
                                                                                        $idPartner, $isExtend, $isRecurring);
        if (!$payment) {
            return false;
        }

        return $payment;
    }

    /**
     * @param PaymentControlCheck $paymentControlCheck
     * @param $payGwLog
     * @param AbaUserCreditForms $moUserCredit
     * @param $idPartner
     *
     * @return bool|Payment
     */
    public function savePaymentCtrlCheckToUserDirectPayment(
      PaymentControlCheck $paymentControlCheck,
      $payGwLog,
      AbaUserCreditForms $moUserCredit,
      $idPartner
    ) {
        $modFormPayment = new PaymentForm();

        $paymentControlCheck->id = "DP" . $paymentControlCheck->id;

        $payment = $this->savePayment($modFormPayment, $paymentControlCheck, $moUserCredit, $payGwLog, $idPartner);
        if (!$payment) {
            return false;
        }
        return $payment;
    }

    /** Saves credit Card into database and returns the model.
     * @param array $aDataPayMethod
     * @param string $errorMsg By reference
     * @param AbaUser $moUser
     * @param integer $defaultToRenew; it would be used to renew payments
     *
     * @return AbaUserCreditForms|bool
     */
    public function saveCreditFormData($aDataPayMethod, &$errorMsg, $moUser=null, $defaultToRenew = 1)
    {
        if (isset($moUser)) {
            $userId = $moUser->getId();
        } else {
            $userId = Yii::app()->user->getId();
        }

        if ($userId) {
            $userCreditCard = new AbaUserCreditForms($userId);

            if (!$userCreditCard->setUserCreditForm($userId, $aDataPayMethod["creditCardType"],
                $aDataPayMethod["creditCardNumber"], $aDataPayMethod["creditCardYear"],
                $aDataPayMethod["creditCardMonth"], $aDataPayMethod["CVC"],
                $aDataPayMethod["creditCardName"], $defaultToRenew, $aDataPayMethod["cpfBrasil"], "", $aDataPayMethod["typeCpf"])
            ) {
                $aErrors =  $userCreditCard->getErrors();
                $lastKey =  key( array_slice( $aErrors, -1, 1, TRUE ) );

                $sError = '';
                if(is_array($aErrors[$lastKey])) {
                    $sError = print_r($aErrors[$lastKey], true);
                }
                else {
                    $sError = $aErrors[$lastKey];
                }


                $errorMsg = Yii::t('mainApp', 'errorsavepayment_key') . " " . $sError;
                return false;
            }

            if (!$userCreditCard->insertUserCreditFormByUserId()) {
                $errorMsg = Yii::t('mainApp', 'errorsavepayment_key');
                return false;
            }

            return $userCreditCard;
        }

        return false;
    }


    /** Saves credit Card into database and returns the model.
     * @param array $aDataPayMethod
     * @param string $errorMsg By reference
     * @param AbaUser $moUser
     * @param integer $defaultToRenew; it would be used to renew payments
     *
     * @return AbaUserCreditForms|bool
     */
    public function saveCreditFormDataFromPayment($aDataPayMethod, &$errorMsg, $moUser=null, $defaultToRenew = 1)
    {
        if (isset($moUser)) {
            $userId = $moUser->getId();
        } else {
            $userId = Yii::app()->user->getId();
        }

        if ($userId) {
            $userCreditCard = new AbaUserCreditForms($userId);

            if (!$userCreditCard->setUserCreditFormFromPayment($userId, $aDataPayMethod["creditCardType"],
                $aDataPayMethod["creditCardNumber"], $aDataPayMethod["creditCardYear"],
                $aDataPayMethod["creditCardMonth"], $aDataPayMethod["CVC"],
                $aDataPayMethod["creditCardName"], $defaultToRenew, $aDataPayMethod["cpfBrasil"], "", $aDataPayMethod["typeCpf"])
            ) {
                $aErrors =  $userCreditCard->getErrors();
                $lastKey =  key( array_slice( $aErrors, -1, 1, TRUE ) );
                $errorMsg = Yii::t('mainApp', 'errorsavepayment_key') . " " . $aErrors[$lastKey];
                return false;
            }

            if (!$userCreditCard->insertUserCreditFormByUserId()) {
                $errorMsg = Yii::t('mainApp', 'errorsavepayment_key');
                return false;
            }

            return $userCreditCard;
        }

        return false;
    }

    /** Save user data from Payment Form
     * @param PaymentForm $modFormPayment
     *
     * @return bool|AbaUser
     */
    public function saveUserData(PaymentForm &$modFormPayment)
    {
        $moUser = new AbaUser();
        // User Exists, is registered so we update the data
        if ($moUser->getUserByEmail($modFormPayment->mail)) {
            $moUser->name = $modFormPayment->firstName;
            $moUser->surnames = $modFormPayment->secondName;
            if (!$moUser->updateUserFromPayment()) {
                $modFormPayment->addError("firstName", "errorsavepayment_key");
                return false;
            }

            return $moUser;
        } else {
            // Email has been changed from the original. It could be an attack.
            $modFormPayment->addError("mail", "erroremail_key");
            return false;
        }
    }

    /** Save the payment transaction after transaction has been executed in the proper gateway.
     * @param PaymentForm $modFormPayment
     * @param PaymentControlCheck $paymentControlCheck
     * @param AbaUserCreditForms $userCreditCard
     * @param PayGatewayLog $payGwLog
     * @param integer $idPartner
     * @param integer $isExtend
     *
     * @return bool|Payment
     */
    public function savePayment(PaymentForm &$modFormPayment, PaymentControlCheck $paymentControlCheck,
                                 AbaUserCreditForms $userCreditCard, PayGatewayLog $payGwLog, $idPartner, $isExtend=0, $isRecurring=0)
    {
        $payment = new Payment();
        // Copies draft payment to a SUCCESS payment :
        if ($payment->copyFromPayControlCheckToPayment($paymentControlCheck, $userCreditCard, $payGwLog, $idPartner, $isExtend, $isRecurring)) {
            if ($payment->paymentProcess($paymentControlCheck->userId)) {
                if (!Yii::app() instanceof CConsoleApplication) {
                    Yii::app()->user->setState("paymentControlStatus", $payment->status);
                }
                return $payment;
            }
        }
        $modFormPayment->addError("paymentProcess", Yii::t('mainApp', 'errorsavepayment_key'));
        $modFormPayment->addErrors($payment->getErrors());
        return false;
    }

    /**
     * @param $paySuppExtId
     * @param AbaUser $user
     * @param PayGatewayLogAdyenReqSent $sentLog
     *
     * @return bool|PaymentControlCheck
     */
    public function getAdyenPaymentBeforeGateway( $paySuppExtId, AbaUser $user, PayGatewayLogAdyenReqSent $sentLog )
    {
        //
        // @TODO - get PCH a base de idPayment del log LIMIT 1 ORDER BY id DESC  ==  ya viene en $sentLog
        //
        $payControl = new PaymentControlCheck();

        if($payControl->getPaymentById($sentLog->idPayment)) {
            return $payControl;
        }
        return false;
    }
}
