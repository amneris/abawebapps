<?php
/** Common operations on User.
 * Should be called from controller, rules, crons. Never from Models.
 * Also from web services.
 *
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 22/11/12
 * Time: 13:01
 * To change this template use File | Settings | File Templates.
 */
class UserCommon
{
    private $errMessage;

    /** Returns the first Unit that user has not finished yet, or even has not started.
     * @param integer $userId
     * @param integer $userLevel
     *
     * @return UnitCourse
     * @throws CDbException
     */
    public function getFirstUnitNotProgressCompleted($userId=NULL, $userLevel=NULL)
    {
        if( !isset($userId) )
        {  $userId= Yii::app()->user->getId();  }
        if( !isset($userLevel) )
        {  $userLevel= Yii::app()->user->role;  }

        $moFollow = new AbaFollowup();
        $iFirstUnitId = $moFollow->getFirstUnitNotProgressCompleted($userId, $userLevel) ;
        if( !$iFirstUnitId )
        {
            $moUnits = new UnitsCourses();
            $iFirstUnitId = $moUnits->getStartUnitByLevel($userLevel);
        }

        $moUnit = new UnitCourse();
        if( !$moUnit->getUnit($iFirstUnitId) ) {
            throw new CDbException("ABA Error, unit $iFirstUnitId could not be loaded ");
        }

        return $moUnit;

    }

    /**
     * @param integer $userId
     * @param integer $userLevel
     *
     * @return bool
     */
    public function hasFinishedLevel($userId, $userLevel)
    {
        $moUnits = new UnitsCourses();
        $moFollowUp = new AbaFollowup();
        $lastUnit100 = $moFollowUp->getLastUnitProgressCompleted($userId, $userLevel);
        if ($moUnits->getLevelByUnitId($lastUnit100) > $userLevel) {
            return true;
        }

        return false;
    }

    /** Searches next Unit is not completed just above the user current level.
     * In case of 6 and everything is completed it returns 6.
     * @param AbaUser $moUser
     *
     * @return integer
     */
    public function nextLevelNotCompleted(AbaUser $moUser)
    {
        $moUnits = new UnitsCourses();
        $moFollowUp = new AbaFollowup();

        $nextLevel = $moUser->currentLevel;
        while ($nextLevel <= MAX_ENGLISH_LEVEL) {
            $nextLevel = $nextLevel + 1;

            $unitIdNextLevel = $moFollowUp->getFirstUnitNotProgressCompleted($moUser->id, $nextLevel);
            if ($moUnits->getLevelByUnitId($unitIdNextLevel) == $nextLevel) {
                break;
            }
        }

        return $nextLevel;
    }


    /**
     * Returns first Video Id that user is pending for watching.
     * @param integer $userId
     * @param integer $userLevel
     *
     * @return VideoClass
     * @throws CDbException
     */
    public function getFirstVideoclassNotWatched($userId = NULL, $userLevel = NULL)
    {
        if (!isset($userId)) {
            $userId = Yii::app()->user->getId();
        }
        if (!isset($userLevel)) {
            $userLevel = Yii::app()->user->role;
        }

        $moFollow = new AbaFollowup();
        $iFirstVideoId = $moFollow->getFirstVideoclassNotWatched($userId, $userLevel);
        if (!$iFirstVideoId) {
            $moUnits = new UnitsCourses();
            $iFirstVideoId = $moUnits->getStartUnitByLevel($userLevel);
        }

        $moVideo = new VideoClass();
        if (!$moVideo->getUnit($iFirstVideoId)) {
            throw new CDbException("ABA Error, videoId $iFirstVideoId could not be loaded ");
        }

        return $moVideo;

    }

    /**  Returns all payments of a user sorted by dateToPay
     * @param AbaUser $moUser
     *
     * @return bool|Payments
     */
    public function getAllPayments(AbaUser $moUser)
    {
        $moPayments = new Payments();

        if (!$moPayments->getAllPaymentsByUserId($moUser->id)) {
            return false;
        } else {
            return $moPayments;
        }

    }

    /**
     * Set userType=FREE for the user.
     * @param     $userId
     * @param int $cancelReason
     *
     * @return bool
     */
    public function cancelUser($userId, $cancelReason = 3)
    {
        $moUser = new AbaUser();
        if (!$moUser->getUserById($userId)) {
            return false;
        }
        if (!$moUser->updateUserToCancelled($cancelReason)) {
            return false;
        }
        // We have to search for any PENDING payment:
        $moPossiblePending = new Payment();
        if ($moPossiblePending->getLastPaymentByUserId($moUser->id, false)) {
            if (intval($moPossiblePending->status) == PAY_PENDING) {
                /** @noinspection PhpParamsInspection */
                $fSuccUpd = $moPossiblePending->updateFailedRenewPay(
                    HeDate::todaySQL(true),
                    'Process expiration, assume that last payment was failed.'
                );
                if (!$fSuccUpd) {
                    HeLogger::sendLog(
                        "Process Expired Premium Users failed trying to update the last PENDING payment to FAILED.",
                        HeLogger::IT_BUGS,
                        HeLogger::CRITICAL,
                        "Process Expired Premium Users failed trying to update the last PENDING payment to FAILED. " .
                        "Recommend to turn [status] to FAILED." . " IDENTIFICATION:  User " . $moUser->email .
                        " payment should be considered failure=" . $moPossiblePending->id
                    );
                }
            }
        }
        // Sends confirmation to Selligent:
        $commSelligent = new SelligentConnectorCommon();
        $commSelligent->sendOperationToSelligent(
            SelligentConnectorCommon::cancelaciones,
            $moUser,
            array(),
            "Campus-Cancellation"
        );
        return true;
    }

    /**
     * @param $paymentPendingId
     * @param int $cancelReason
     *
     * @return bool
     */
    public function cancelPaymentPending($paymentPendingId, $cancelReason = 3)
    {
        $moPossiblePending = new Payment();
        if ($moPossiblePending->getPaymentPendingById($paymentPendingId, false)) {
            if (intval($moPossiblePending->status) == PAY_PENDING) {
                $fSuccUpd = $moPossiblePending->updateFailedRenewPay(HeDate::todaySQL(true), 'Process expiration, assume that last payment was failed.');
                if (!$fSuccUpd) {
                    HeLogger::sendLog(
                        "Process Expired Premium Users failed trying to update the last PENDING payment to FAILED.",
                        HeLogger::IT_BUGS,
                        HeLogger::CRITICAL,
                        "Process Expired Premium Users failed trying to update the last PENDING payment to FAILED. " .
                        "Recommend to turn [status] to FAILED."." IDENTIFICATION:  UserId ".$moPossiblePending->userId .
                        " payment should be considered failure=" . $moPossiblePending->id
                    );
                }
            }
            $moUser = new AbaUser();
            $moUser->getUserById($moPossiblePending->userId);
            $commSelligent = new SelligentConnectorCommon();
            $commSelligent->sendOperationToSelligent(
                SelligentConnectorCommon::cancelaciones,
                $moUser,
                array(),
                "Appstore-Cancellation"
            );
        }

        return true;
    }


    /** Returns the user's country name in format ISO.
     * @param AbaUser $moUser
     *
     * @return bool|string
     */
    public function getCountryName(AbaUser $moUser)
    {
        if (!$moUser) {
            return false;
        }

        $moCountry = new AbaCountry($moUser->countryId);
        if ($moCountry) {
            return $moCountry->name;
        }

        return false;
    }

    /** Logs the change level in logs and also updates current Session value
     * @param AbaUser $moUser
     * @param integer $oldLevel Current and previous level of English
     * @param integer $newLevel New level to be updated
     * @param string $sourceAction = Campus, My Profile, Register User.
     *
     * @return bool
     */
    public function changeUserLevel(AbaUser $moUser, $oldLevel, $newLevel, $sourceAction = 'Campus')
    {
        $moLogLevelUserChange = new LogUserLevelChange();
        $moLogLevelUserChange->getLogById();

        $moLastLevelChange = new LogUserLevelChange();
        if ($moLastLevelChange->getLogByUserId($moUser->id, true, true)) {
            if ($moLastLevelChange->newUserLevel <> $oldLevel) {
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_LOGIC_H . ' User with email '.$moUser->email.' has a gap in log change level',
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    ' Last User Level Log equals to '.$moLastLevelChange->newUserLevel . ' but expected ' . $oldLevel .
                    '. Probably change from Intranet or maybe an IT member from database?!'
                );
            }
        }
        $moLogLevelUserChange->userId = $moUser->id;
        $moLogLevelUserChange->oldUserLevel = $oldLevel;
        $moLogLevelUserChange->newUserLevel = $newLevel;
        $moLogLevelUserChange->sourceAction = $sourceAction;
        $moLogLevelUserChange->insertLog();
        if (isset(Yii::app()->user)) {
            if (Yii::app()->user->getState("currentLevel")) {
                Yii::app()->user->setState("currentLevel", $newLevel);
            }
        }
        return true;
    }

    /** Updates user Type, usually to PREMIUM only.
     * @param AbaUser $moUser
     * @param integer $userType
     * @param string $expirationDate
     * @param integer $idPartner
     *
     * @return AbaUser|bool
     */
    private function updateUserType(AbaUser $moUser, $userType, $expirationDate, $idPartner = NULL)
    {
        $errMessage = false;

        if (!$moUser->updateUserType($userType, $idPartner)) {
            $errMessage = true;
        }

        $moUser->expirationDate = $expirationDate;
        /** @var $aColumns2Upd array */
        $aColumns2Upd = array('expirationDate');

        if (empty($moUser->idPartnerFirstPay)) {
            $moUser->idPartnerFirstPay = $idPartner;
            $aColumns2Upd[] = 'idPartnerFirstPay';
        }

        if (!$moUser->update($aColumns2Upd)) {
            $errMessage = true;
        }

        if (empty($moUser->idPartnerCurrent)) {
            $moUser->idPartnerCurrent = $idPartner;
            $aColumns2Upd[] = 'idPartnerCurrent';
        }

        if (!$moUser->update($aColumns2Upd)) {
            $errMessage = true;
        }

        //ENH-1324, Update field followUpAtFirstPay = AbaFollowUp->all_por
        $moUnits = new UnitsCourses();
        $aUnits = $moUnits->getArrayStartUnits(1, 6);
        $moFollowUp = new AbaFollowup();
        $maxProgress = $moFollowUp->getMaxFollowUp($moUser->id, $aUnits);
        if (!$moUser->updateFollowUpAtFirstPay($maxProgress)) {
            $errMessage = true;
        }

        // ENH-1558, how many videoclasses user has watched:
        $moProgresses = new AbaFollowups();
        $watchedVideos = $moProgresses->getNumberOfVideosWatched($moUser->id);
        if (!$moUser->updateWatchedVideos($watchedVideos)) {
            $errMessage = true;
        }

        if ($errMessage) {
            $this->errMessage = ' User ' . $moUser->email . ' could not be converted to Premium. Review fields ' .
                " userType($userType), expirationDate($expirationDate), idPartnerSource&FirstPay($idPartner)." .
                " Their values should be the ones between parenthesis";

            return false;
        }

        return $moUser;
    }

    /** Alias of function updateUserType
     * Should update these columns: user.idPartnerFirstPay, user.userType, user.ExpirationDate
     * @param AbaUser $moUser
     * @param integer $idPartner
     * @param string $expirationDate
     * @param integer|null $userType
     *
     * @return bool|AbaUser
     */
    public function convertToPremium(AbaUser $moUser, $idPartner, $expirationDate, $userType = NULL)
    {
        if (empty($userType)) {
            $userType = PREMIUM;
        }

        $moPayPending = new Payment();
        if ($moPayPending->getLastPayPendingByUserId($moUser->id)){
            /*if ( abs(HeDate::getDifferenceDateSQL(HeDate::todaySQL(false),
                HeDate::removeTimeFromSQL($moPayPending->dateStartTransaction),
                HeDate::DAY_SECS, false))>=1 ){*/
              $moPayPending->updateDateToPayInPending($idPartner, $expirationDate);
//            }
        }

        return $this->updateUserType($moUser, $userType, $expirationDate, $idPartner);
    }

    /**
     * @param AbaUser $moUser
     * @param $idPartner
     * @param $expirationDate
     * @param null $userType
     *
     * @return AbaUser|bool
     */
    public function convertToPremiumCron(AbaUser $moUser, $idPartner, $expirationDate, $userType = NULL)
    {
        if (empty($userType)) {
            $userType = PREMIUM;
        }
        $moPayPending = new Payment();
        if ($moPayPending->getLastPayPendingByUserId($moUser->id)){
            $moPayPending->updateDateToPayInPending($idPartner, $expirationDate);
        }
        return $this->updateUserTypeCron($moUser, $userType, $expirationDate, $idPartner);
    }

    /**
     * @param AbaUser $moUser
     * @param $userType
     * @param $expirationDate
     * @param null $idPartner
     *
     * @return AbaUser|bool
     */
    private function updateUserTypeCron(AbaUser $moUser, $userType, $expirationDate, $idPartner = NULL)
    {
        $errMessage = false;

        if (!$moUser->updateUserType($userType, $idPartner)) {
            $errMessage = true;
        }

        $moUser->expirationDate = $expirationDate;
        /** @var $aColumns2Upd array */
        $aColumns2Upd = array('expirationDate');

        if (empty($moUser->idPartnerFirstPay)) {
            $moUser->idPartnerFirstPay = $idPartner;
            $aColumns2Upd[] = 'idPartnerFirstPay';
        }

        if (!$moUser->update($aColumns2Upd)) {
            $errMessage = true;
        }

        //ENH-1324, Update field followUpAtFirstPay = AbaFollowUp->all_por
        $moUnits = new UnitsCourses();
        $aUnits = $moUnits->getArrayStartUnits(1, 6);
        $moFollowUp = new AbaFollowup();
        $maxProgress = $moFollowUp->getMaxFollowUpCron($moUser->id, $aUnits);
        if (!$moUser->updateFollowUpAtFirstPay($maxProgress)) {
            $errMessage = true;
        }

        // ENH-1558, how many videoclasses user has watched:
        $moProgresses = new AbaFollowups();
        $watchedVideos = $moProgresses->getNumberOfVideosWatched($moUser->id);
        if (!$moUser->updateWatchedVideos($watchedVideos)) {
            $errMessage = true;
        }

        if ($errMessage) {
            $this->errMessage = ' User ' . $moUser->email . ' could not be converted to Premium. Review fields ' .
                " userType($userType), expirationDate($expirationDate), idPartnerSource&FirstPay($idPartner)." .
                " Their values should be the ones between parenthesis";
            return false;
        }
        return $moUser;
    }


    /**
     *  Set country Id in case user country has not been properly registered or informed:
     * @param AbaUser $moUser
     * @param string $action
     * @param array $aCountriesLikeEmpty
     *
     * @return AbaUser|bool False if it has not changed country
     */
    public function checkAndUpdateCountryId(AbaUser $moUser, $action = "FirstLogin", $aCountriesLikeEmpty = NULL)
    {
        $changed = false;

        if(trim($moUser->dateLastSentSelligent) != '') {

            $ipCgi = Yii::app()->request->getUserHostAddress();
            $moCountry =    new AbaCountry();

            // First, case for an IP valid and through the GEOIP we find the region, country ,etc.
            if (HeMixed::isCountryNoReal(intval($moUser->countryId), $aCountriesLikeEmpty)) {
                if (!$moCountry->getCountryCodeByIP($ipCgi)) {
                    $moCountry->getCountryByIsoCode(Yii::app()->config->get("DefaultIPCountryCode"));
                }
                $moUser->countryId =        $moCountry->getId();
                $moUser->countryIdCustom =  $moCountry->getId();
                $moUser->update(array("countryId", "countryIdCustom"));
                $moUser->abaUserLogUserActivity->saveUserLogActivity("user", " assigned country based on IP " . $ipCgi, $action);

                $geoUser = $moCountry->geoLocation;
                $moUserLoc = new AbaUserLocation();
                $moUserLoc->insertNewLocation($moUser->getId(), $ipCgi, $geoUser->longitude, $geoUser->latitude,
                    $geoUser->countryCode, $geoUser->areaCode,
                    $geoUser->city, 1);
                $changed = $moUser;
            } else {
                if (HeMixed::isCountryNoReal(intval($moUser->countryId), $aCountriesLikeEmpty)) {
                    $moCountry->getCountryByIsoCode(Yii::app()->config->get("DefaultIPCountryCode"));
                    $moUser->countryId =        $moCountry->getId();
                    $moUser->countryIdCustom =  $moCountry->getId();
                    $moUser->update(array("countryId", "countryIdCustom"));
                    $moUser->abaUserLogUserActivity->saveUserLogActivity("user", " assigned country based on IP " . $ipCgi, $action);
                    $changed = $moUser;
                }
            }

            if ($changed) {
                //  Send data to Selligent
                $commConnSelligent = new SelligentConnectorCommon();
                $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::registroWeb, $moUser, array(),
                    "Change country auto web service");
            }
        }

        return $changed;
    }


    /** In case of error of any of this class functions, this method returns the detailed message
     * @return mixed
     */
    public function getErrMessage()
    {
        return $this->errMessage;
    }

    /**
     * @param $sEmail
     *
     * @return bool
     */
    public function sendRecoverPasswordEmail($sEmail)
    {
        $oUser = new AbaUser();
        if (!$oUser->getUserByEmail($sEmail)) {
            $this->errMessage = Yii::t('mainApp', 'email_not_exists_key');
            return false;
        }

        $commConnSelligent = new SelligentConnectorCommon();
        $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::recoverPassword, $oUser, array(), "sendRecoverPasswordEmail");

        return true;
    }


    /** Generates a new random password and changes it.
     * @param        $email
     * @param string $source
     *
     * @return bool
     */
    public function recoverPassword($email, $source="RP", $newPassword="", $fromRecover=true)
    {
        $user = new AbaUser();
        if (!$user->getUserByEmail($email)) {
            $this->errMessage = Yii::t('mainApp', 'email_not_exists_key');
            return false;
        }

        if(trim($newPassword) == "") {
            $commUserRegistration = new RegisterUsersCommon();
            $newPassword = $commUserRegistration->genRandomPasswordNewUser($email, $source);
        }

        $commUser = new UserCommon();
//        if (!$commUser->changePassword($user, $newPassword, "RecoverPasswordProcess $source", true, false)) {
        if (!$commUser->changePassword($user, $newPassword, "RecoverPasswordProcess $source", true, $fromRecover)) {
            $this->errMessage = Yii::t('mainApp', 'newpws_notcreated_key');
            return false;
        }

        return true;
    }

    /** Changes password , updates to database and informs Selligent.
     *
     * @param AbaUser $moUser
     * @param string $newPassword
     * @param string $actionSource
     * @param bool $updateSQL
     * @param bool $fromRecover
     *
     * @return bool
     */
    public function changePassword(AbaUser &$moUser, $newPassword, $actionSource, $updateSQL = true, $fromRecover = true)
    {
        $moUser->setPassword($newPassword);
        if ($updateSQL) {
            if (!$moUser->update(array("password", "keyExternalLogin"))) {
                $this->errMessage = "User password could not be updated.";
                return false;
            }
        }

        // Get all devices and recreate tokens and change their tokens
        $moDevices = new AbaUserDevicesCol();
        $aMoUserDevices = $moDevices->getAllUserDevicesByUserId($moUser->id);
        if (is_array($aMoUserDevices) && count($aMoUserDevices) >= 1) {
            /* @var AbaUserDevice $moUserDevice */
            foreach ($aMoUserDevices as $moUserDevice) {
                // Right now, we decide to change the tokens of all devices if password changes. If
                // we do not want this, we only want to users to keep entering mobile devices without asking for
                // password then just change password by Email!
                $tmpToken = HeAbaSHA::genTokenForMobile($moUserDevice->sysOper, $moUserDevice->id,
                    $moUser->email, $newPassword);
                $moUserDevice->updateToken($tmpToken);
            }
        }


        //  Send data to Selligent only when user is recovering password, not changing manually
        $commConnSelligent = new SelligentConnectorCommon();
        if($fromRecover) {
            $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::cambioPassword, $moUser,
                array("PASS_TMP" => $newPassword), $actionSource);
        } else {
            // send operation to selligent
            $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::synchroUser, $moUser,
                array(), 'profile-changePassword');
        }

        return true;
    }

    /** Renames, downgrade user type and sends update information to Selligent.
     * It is as if the user would be deleted.
     *
     * @param $userId
     * @param $cancelReason
     *
     * @return bool
     */
    public function unsubscribe($userId, $cancelReason)
    {

        $moUser = new AbaUser();
        if ($moUser->getUserById($userId)) {
            $moUser->unsubscribe($cancelReason);
        } else {
            return false;
        }

        $commSelligent = new SelligentConnectorCommon();
        $commSelligent->sendOperationToSelligent(SelligentConnectorCommon::unsubscribe,
            $moUser, array(), "Unsubscribe");


        return true;

    }

    /** Returns true/false, true if $lastDays
     * has passed by since last time user changed his level
     *
     * @param $userId
     * @param int $lastDays
     *
     * @return bool
     */
    public function hasRecentlyChangedLevel($userId, $lastDays = 2)
    {
        $fLevelRecentlyChange = false;
        $moLogUserLevel = new LogUserLevelChange();
        if ($moLogUserLevel->getLogByUserId($userId, true)) {
            if (HeDate::isGreaterThan(HeDate::removeTimeFromSQL($moLogUserLevel->dateModified),
                HeDate::getDateAdd(HeDate::todaySQL(false), -($lastDays)), true)
            ) {
                $fLevelRecentlyChange = true;
            }
        }

        return $fLevelRecentlyChange;
    }


    /**
     * Checks if geo location is available, and in that case we save all possible data into database.
     * @param AbaUser $moUser
     * @param $ipCgi
     *
     * @return bool
     */
    public function registerGeoLocation(AbaUser $moUser, $ipCgi)
    {
        if (!ip2long($ipCgi)) {
            return false;
        }

        $moUserLoc = new AbaUserLocation();
        $geoUserLoc = Yii::app()->geoip->lookupLocation($ipCgi);
        if ($geoUserLoc) {
            if ($moUserLoc->getLocationByIdUserIdIp($moUser->getId(), '', 0)) {
                $moUserLoc->updateRestNoDefault();
            }
            $moUserLoc->insertNewLocation($moUser->getId(), $ipCgi, $geoUserLoc->longitude, $geoUserLoc->latitude,
              $geoUserLoc->countryCode, $geoUserLoc->region,
              $geoUserLoc->city, 1);
            return true;
        } else {
            return false;
        }
    }


    /** If it's possible returns the country according the geo-localization, otherwise returns the default user country
     * @return int
     */
    public function getCountryForPhonesByIp($ipCgi)
    {
        $moCountry = new AbaCountry();
        if (!empty($ipCgi)) {
            if ($moCountry->getCountryCodeByIP($ipCgi)) {
                return $moCountry->id;
            }
        }
        return Yii::app()->user->getCountryId();
    }

    /**
     * @param $userId
     * @return array|bool|null
     */
    public function getMemberGetMember($userId) {
        $moUser = new AbaUser();
        $moUser->getUserById($userId);
        $moLastPaySubs = new Payment();
        $commRecurring = new RecurrentPayCommon();
        $typeSubscriptionStatus = $commRecurring->getTypeSubscriptionStatus($moUser, $moLastPaySubs);

        // don't allow member get member to premium and ex-premium users
        if ($moUser->userType == PREMIUM
            || $typeSubscriptionStatus == SUBS_EXPREMIUM_CARD
            || $typeSubscriptionStatus == SUBS_EXPREMIUM_PAYPAL
            || $typeSubscriptionStatus == SUBS_EXPREMIUM_THROUGHPARTNER ) {

                return false;
        }

        // instance the model
        $mgm = new AbaMemberGetMember();
        $mgm_userExists = $mgm->getMGMByUserId($userId);
        if ($mgm_userExists) {
            if ($mgm->expireDate === NULL && $mgm->registered >= MGM_REQUIRED_USERS) {
                if($mgm->premiumActivate($userId))
                {
                    // notify to selligent that the user has activated its premium period
                    $params = array('FECHA_FIN' => $mgm->expireDate);
                    $commConnSelligent = new SelligentConnectorCommon();
                    $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::startPremiumWeek, $moUser,
                                                                                    $params, "Start free premium week");
                    if (!Yii::app() instanceof CConsoleApplication) {
                        Yii::app()->user->setState('showModal_popupMgmActivatePremium', true);
                    }
                }
            }
        }

        return $mgm->toArray();
    }

    /**
     * @param integer $userId
     * @param array $mgm 3 elements: expirationDate,userId and registeredDate
     *
     * @return bool
     */
    public function isPremiumFriend($userId, $mgm = NULL)
    {
        if (empty($mgm)){
            $mgm = $this->getMemberGetMember($userId);
        }

        if($mgm !== false && $mgm['expireDate'] !== NULL &&
            HeDate::isGreaterThan($mgm['expireDate'], date('Y-m-d'))) {
            return true;
        }

        return false;
    }


    /**
     * @param $sEmail
     * @param $sEmailFacebook
     *
     * @return bool
     */
    public function checkIfChangedEmailExists($sEmail, $sEmailFacebook)
    {
        if ($sEmail != $sEmailFacebook) {

            $abaUser =      new AbaUser();
            $userExists =   $abaUser->getUserByEmail($sEmail);

            if($userExists) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $iFbid
     *
     * @return AbaUser|bool
     */
    public function checkIfFbUserExists($iFbid)
    {
        $abaUserFb =    new AbaUser();
        $fbUserExists = $abaUserFb->getUserByFacebookId($iFbid);

        if ($fbUserExists) {
            return $abaUserFb;
        }
        return false;
    }

    /**
     * @param AbaUser $abaUser
     * @param $sName
     * @param $sSurname
     * @param $iFbid
     * @param $sUserGender
     *
     * @return AbaUser
     */
    public function changeUserFbData(AbaUser $abaUser, $sName, $sSurname, $iFbid=null, $sUserGender)
    {
        if(trim($abaUser->fbId) == '') {

            $fbUser = $this->checkIfFbUserExists($iFbid);

            if(!$fbUser) {

                $aUpdate = array();

                if($iFbid != '' || $iFbid != 0) {
                    $aUpdate = array("fbId");
                    $abaUser->fbId = $iFbid;
                }
                if(trim($abaUser->name) == '' && trim($sName) != '') {
                    $abaUser->name = $sName;
                    array_push($aUpdate, "name");
                }
                if(trim($abaUser->surnames) == '' && trim($sSurname) != '') {
                    $abaUser->surnames = $sSurname;
                    array_push($aUpdate, "surnames");
                }
                if(trim($abaUser->gender) == '' && $sUserGender != '') {
                    $abaUser->gender = $sUserGender;
                    array_push($aUpdate, "gender");
                }
                if(count($aUpdate) > 0) {
                    $abaUser->update($aUpdate);
                }
            }
        }

        return $abaUser;
    }
    /**
     * @param $iINid
     *
     * @return AbaUser|bool
     */
    public function checkILinkedinUserExists($iINid)
    {
        $abaUserIN =    new AbaUser();
        $INUserExists = $abaUserIN->getUserByLinkedinId($iINid);

        if ($INUserExists) {
            return $abaUserIN;
        }
        return false;
    }
    /**
     * @param AbaUser $abaUser
     * @param $sName
     * @param $sSurname
     * @param $iFbid
     * @param $sUserGender
     *
     * @return AbaUser
     */
    public function changeUserinData(AbaUser $abaUser, $sName, $sSurname, $iINid=null, $sUserGender)
    {
        if(trim($abaUser->inId) == '') {

            $inUser = $this->checkILinkedinUserExists($iINid);

            if(!$inUser) {

                $aUpdate = array();

                if($iINid != '' || $iINid != 0) {
                    $aUpdate = array("inId");
                    $abaUser->inId = $iINid;
                }
                if(trim($abaUser->name) == '' && trim($sName) != '') {
                    $abaUser->name = $sName;
                    array_push($aUpdate, "name");
                }
                if(trim($abaUser->surnames) == '' && trim($sSurname) != '') {
                    $abaUser->surnames = $sSurname;
                    array_push($aUpdate, "surnames");
                }
                if(trim($abaUser->gender) == '' && $sUserGender != '') {
                    $abaUser->gender = $sUserGender;
                    array_push($aUpdate, "gender");
                }
                if(count($aUpdate) > 0) {
                    $abaUser->update($aUpdate);
                }
            }
        }

        return $abaUser;
    }

    /** Returns the new expiration date for the user.
     *
     * @param AbaUser $moUser
     * @param AbaProdPeriodicity $moPeriodicity
     *
     * @return bool|string; YYYY-MM-DD
     */
    public function getNextExpirationDate( AbaUser $moUser, ProdPeriodicity $moPeriodicity )
    {
        if (HeDate::validDateSQL(HeDate::europeanToSQL($moUser->expirationDate)) &&
            HeDate::isGreaterThanToday(HeDate::europeanToSQL($moUser->expirationDate), true)
        ) {
            $expirationDate = HeDate::europeanToSQL($moUser->expirationDate);
        } else {
            $expirationDate = HeDate::todaySQL(true);
        }

        $expirationDate = $moPeriodicity->getNextDateBasedOnDate($expirationDate);

        // Case FREE|LEAD|DELETED-----------------------------------------------------------------------******
        if ($moUser->userType == FREE || $moUser->userType == DELETED) {
            $expirationDate = $moPeriodicity->getNextDateBasedOnDate(HeDate::todaySQL(true));
        }

        return $expirationDate;
    }

    /**
     * @param $eMail
     * @param $sPass
     * @param bool $usePassword
     *
     * @return AbaUser|bool
     */
    public function checkIfUserExists($eMail, $sPass, $usePassword=true)
    {
        $abaUser =      new AbaUser();
        $fbUserExists = $abaUser->getUserByEmail($eMail, $sPass, $usePassword);
        if ($fbUserExists) {
            return $abaUser;
        }
        return false;
    }

    /**
     * @param AbaUser $moUser
     * @param $periodId
     * @param $currency
     * @param $priceAppStore
     * @param $actionSource
     *
     * @return bool
     */
    public function free2premium(AbaUser $moUser, $periodId, $currency, $priceAppStore, $countryId, $userId, $purchaseReceipt, $actionSource)
    {
        // Validate user type
        if ($moUser->userType != FREE) {
            $this->codeError =  '410';
            $this->errMessage = 'User id ' . $moUser->id . ' already PREMIUM ';
            return false;
        }

        // Validate product
        $moPeriodicity = new ProdPeriodicity();
        if (!$moPeriodicity->getProdPeriodByMonths($periodId)) {
            $this->errorCode = '508';
            $this->errMessage = "The period for this product is not valid. There is no such periodicity in the database with ID " . $periodId;
            return false;
        }

        $errorEmail = $moUser->email;

        //
        if(!is_numeric($countryId)) {

            $oCountry = new AbaCountry();

            if($oCountry->getCountryByIsoCode($countryId)) {
                $countryId = $oCountry->id;
            }
            else {
                $countryId = $moUser->countryId;

                if(!is_numeric($countryId)) {
                    if($oCountry->getCountryByIsoCode( Yii::app()->config->get("DefaultIPCountryCode") )) {
                        $countryId = $oCountry->id;
                    }
                    else {
                        $countryId = Yii::app()->config->get("ABACountryId");
                    }
                }
            }
        }

        //
        // AppStore payment receipt
        //
        $oAbaPaymentsAppstore = new AbaPaymentsAppstore();
        $oAbaPaymentsAppstore->idCountry =       $countryId;
        $oAbaPaymentsAppstore->userId =          $userId;
        $oAbaPaymentsAppstore->purchaseReceipt = $purchaseReceipt;

        //
        /* @var PaySupplierAppstore $paySupplier */
        $paySupplier = PaymentFactory::createPayment(null, PAY_SUPPLIER_APP_STORE);
        //
        // set appstore json receipt
        $paySupplier->setJsonResponse($oAbaPaymentsAppstore->purchaseReceipt);

        //
        // Validate json appstore receipt
        //
        if(!$paySupplier->validatePurchaseReceipt($moUser)) {
            $this->codeError =    '410';
            $this->errMessage =   'The purchase receipt is not valid. There is already a user with this original_transaction_id.';
            return false;
        }

        $oAbaPaymentsAppstore->originalTransactionId = $paySupplier->getOriginalTransactionId();

        //@TODO
//        //
//        //#5533
//        $availableCurrencies = $paySupplier->getAvailableCurrencies();
//
//        if(!in_array($currency, $availableCurrencies)) {
//
//            $oProductsPricesAppstore = new ProductsPricesAppstore();
//
//            if($oProductsPricesAppstore->getProductTierByIdProductApp($paySupplier->getProductId())) {
//                $currency = $oProductsPricesAppstore->getCurrencyAppStore();
//            }
//        }

        // Validate Product and periodicity's:
        $moProduct = new ProductPrice();
        if (!$moProduct->getProductByCountryIdPeriodId($countryId, $moPeriodicity->id, PREMIUM)) {
            $this->errorCode =  '508';
            $this->errMessage = "The product is not valid. There is no product in the database for period with Id = " . $periodId;
            return false;
        }

        //
        // SUBSCRIPTION
        //
        $moLastPay =        new Payment();
        $commRecurring =    new RecurrentPayCommon();
        $typeSubsStatus =   $commRecurring->getTypeSubscriptionStatus($moUser, $moLastPay);

        $expirationDate = $this->getNextExpirationDate($moUser, $moPeriodicity);

        $moUser = $this->convertToPremium($moUser, $moUser->idPartnerCurrent, $expirationDate, $moProduct->userType);
        if (!$moUser) {
            $this->errMessage = "Converting  moUser " . $errorEmail . " to PREMIUM has failed.";
            return false;
        }

        $sNow = HeDate::todaySQL(true);


        // Insert a mock payment row ---------------------------------------------------------------------------
        $moPayment = new Payment();

        $moPayment->id =                    HeAbaSHA::generateIdPayment($moUser);
        $moPayment->idUserProdStamp =       1;
        $moPayment->userId =                $moUser->id;
        $moPayment->idProduct =             $moProduct->idProduct;
        $moPayment->idCountry =             $countryId;
        $moPayment->idCountry =             $countryId;
        $moPayment->idPeriodPay =           $moPeriodicity->id;
        $moPayment->idUserCreditForm =      NULL;
        $moPayment->paySuppExtId =          PAY_SUPPLIER_APP_STORE;

        $moPayment->status =                PAY_SUCCESS;
        $moPayment->dateStartTransaction =  $sNow;
        $moPayment->dateEndTransaction =    $sNow;
        $moPayment->dateToPay =             $sNow;

        // get app store currency
        $ABAIdCurrency =                    $moProduct->getAppStoreCurrency($currency);
        $xRates =                           new AbaCurrency($currency);

        $moPayment->xRateToEUR =            $xRates->getConversionRateTo(CCY_DEFAULT);
        $moPayment->xRateToUSD =            $xRates->getConversionRateTo(CCY_2DEF_USD);

        $moPayment->currencyTrans =         $ABAIdCurrency;
        $moPayment->foreignCurrencyTrans =  $ABAIdCurrency;

        $moPayment->amountOriginal =        $priceAppStore;
        $moPayment->amountDiscount =        0;
        $moPayment->amountPrice =           $priceAppStore;

        $moPayment->idPromoCode =           '';
        $moPayment->idPartner =             PARTNER_ID_MOBILE;

        if (!empty($moLastPay)) {
            $moPayment->idPartnerPrePay = $moLastPay->idPartner;
        }
        $moPayment->isRecurring =           0;
        $moPayment->paySuppLinkStatus =     PAY_RECONC_ST_MATCHED;
        $moPayment->idSession =             Yii::app()->getSession()->getSessionId();

        //
        $moPayment->paySuppOrderId =        $moPayment->id . ":" . substr("" . date("Hms"), 3);

        if($paySupplier->loadInitialPaymentJson()) {

            $initialTransactionId = $paySupplier->getOrderNumber();

// ?!?!?
// ?!?!?
            $moPayment->paySuppExtUniId =   $initialTransactionId;
            $moPayment->paySuppExtProfId =  $initialTransactionId;
        }

        //
        // tax rate
        //
        $moPayment->setAmountWithoutTax($moPayment->dateEndTransaction);

        // --------------------------------------
        if (!$moPayment->paymentProcess($moPayment->userId)) {
            $this->errMessage = "Payment could not be successfully inserted for " . $moProduct->idProduct . $countryId . $moPeriodicity->id;
            return false;
        }

        //
        //#5739
        $amountOriginal = $moPayment->amountOriginal;

        $nextPayment = $moPayment->createDuplicateNextPayment();

        $nextPayment->id =                  HeAbaSHA::generateIdPayment($moUser, $nextPayment->dateToPay);
        $nextPayment->idPayControlCheck =   $moPayment->id;
        $nextPayment->isPeriodPayChange =   NULL;
        $nextPayment->paySuppExtUniId =     NULL;
        $nextPayment->paySuppExtProfId =    NULL;
        // $nextPayment->idPartner =           PaySupplierAppstore::getAppstoreRecurringPartner();
        $nextPayment->idPartner =           PaySupplierAppstore::getAppstoreRecurringPartner();

        //
        //#5739
        $nextPayment->amountOriginal =  $amountOriginal;
        $nextPayment->amountPrice =     $amountOriginal;

        // --------------------------------------
        if (!$nextPayment->insertPayment()) {
            $this->errMessage = "Payment could not be successfully inserted for " . $moProduct->idProduct . $countryId . $moPeriodicity->id;
            return false;
        }

        $moUser->abaUserLogUserActivity->saveUserLogActivity('user', 'Registration from type ' . $typeSubsStatus . ' to Premium AppStore', 'free2premium', $moUser->id);

        //
        $oAbaPaymentsAppstore->idPayment = $moPayment->id;
        if(!$oAbaPaymentsAppstore->insertPaymentAppstore()) {
            HeLogger::sendLog("Payment AppStore could not be successfully inserted for payment " . $moPayment->id . " and " . $moProduct->idProduct . $countryId . $moPeriodicity->id,HeLogger::PAYMENTS,HeLogger::CRITICAL,  'free2premium');
        }


        //#5635
        $stPaymentInfo = $moPayment->getPaymentInfoForSelligent();

        //  Send data to Selligent only when user is converted to premium
        $commConnSelligent = new SelligentConnectorCommon();
        // send operation to selligent
        $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::freeToB2b, $moUser,
            array(
                "MONTH_PERIOD" =>   $stPaymentInfo['MONTH_PERIOD'],
                "PAY_DATE_START" => $stPaymentInfo['PAY_DATE_START'],
                "PAY_DATE_END" =>   $stPaymentInfo['PAY_DATE_END'],
                "AMOUNT_PRICE" =>   $stPaymentInfo['AMOUNT_PRICE'],
                "CURRENCY" =>       $stPaymentInfo['CURRENCY'],
                "PRICE_CURR" =>     $stPaymentInfo['PRICE_CURR'],
                "TRANSID" =>        $stPaymentInfo['TRANSID'],
            ), $actionSource . ' - free2premium');

        return true;
    }

    /**
     * Function to update partnerSource and sourceList if current partnerSource is 300031 or 400002
     * @param AbaUser $user
     * @param integer $idPartner
     * @param integer $idSource
     * @return bool
     */
    public function updateAttribution($user, $idPartner, $idSource){
        // We only update attribution data if current partner source is iOS (iPhone(400004) or iPad(400005))
        if($user->idPartnerSource == 400004 || $user->idPartnerSource == 400005 || $user->idPartnerSource == 400089 || $user->idPartnerSource == 0){
            $user->idPartnerSource = $idPartner;
            $user->idSourceList = $idSource;
            $user->save(false);
            return true;
        } else {
            return false;
        }
    }


    /**
     * @param AbaUser $abaUser
     * @param array $stUserData
     *
     * @return AbaUser
     * @throws CDbException
     */
    public function changeUserFromDirectPayment(AbaUser $abaUser, $stUserData=array())
    {

        if($abaUser) {

            $aUpdate = array();

            if(isset($stUserData["name"])) {
                $abaUser->name = $stUserData["name"];
                array_push($aUpdate, "name");
            }

            if(isset($stUserData["surnames"])) {
                $abaUser->surnames = $stUserData["surnames"];
                array_push($aUpdate, "surnames");
            }

            if(count($aUpdate) > 0) {
                return($abaUser->update($aUpdate));
            }

        }

        return $abaUser;
    }

    /**
     * @return int to indicate if the user has access to the aba moments section
     * if the returned value is -1, it means that the user has no access to the option
     */
    public function hasAbaMomentsVisible($user) {
        $abaMomentsResponse = -1;

        if (!isset($_COOKIE['abamoments'])) {
            $helperCommon = new HeComm();
            $url = Yii::app()->params['ABA_API_URL'] . '/api/vabawebapps/moments/progress/' . $user->id;

            $response = $helperCommon->sendHTTPPostCurl($url);

            if (is_string($response)) {
                $jsonResponse = json_decode($response);
                // status is ok when it fits 200 - 399 range and it is different than 204 (no access to abamoments)
                if (!isset($jsonResponse->status) || ($jsonResponse->status >= 200 && $jsonResponse->status < 400 && $jsonResponse != 204)) {
                    if (is_array($jsonResponse)) {
                        $abaMomentsResponse = count($jsonResponse); // return the quantity of NEW aba moments
                    }
                    else {
                        $abaMomentsResponse = 0;
                    }
                }
            }

            // cookie that stores the endpoint result and expires in 6 hours
            setcookie('abamoments', $abaMomentsResponse, time()+60*60*6, '/');
        }
        else {
            $abaMomentsResponse = $_COOKIE['abamoments'];
        }
        
        return $abaMomentsResponse;
    }

}
