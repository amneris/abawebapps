<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 27/11/12
 * Time: 10:15
 * To change this template use File | Settings | File Templates.
 */
class RegisterUsersCommon
{
    private $codeError ;
    private $errorMessage;

    /** Returns text associated with last error.
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /** Returns code number error
     * @return mixed
     */
    public function getCodeError()
    {
        return $this->codeError;
    }

    /**
     * @param AbaUser         $user
     * @param boolean         $userExists
     * @param string          $email
     * @param string          $pwd
     * @param string          $langEnv
     * @param string          $name
     * @param string          $surnames
     * @param  string         $telephone
     * @param string          $voucherCode
     * @param string          $securityCode
     * @param integer         $partnerId
     * @param ProductPrice    $moProduct
     * @param ProdPeriodicity $moPeriodicity
     * @param int             $userType
     * @param integer|null    $idCountry
     * @param string|null     $deviceTypeSource
     *
     * @return AbaUser|bool
     */
    public function registerUserPremiumFromPartner(AbaUser $user, $userExists, $email, $pwd, $langEnv, $name, $surnames,
                                                         $telephone, $voucherCode, $securityCode, $partnerId,
                                                         ProductPrice $moProduct, ProdPeriodicity $moPeriodicity,
                                                         $userType=PREMIUM, $idCountry=null, $deviceTypeSource=null )
    {
        $isUnderSubscription = false;
        $moPaymentPending = new Payment();
        // Assign Country if null
        if ($idCountry == null) {
            $idCountry = Yii::app()->config->get("ABACountryId");
        }
        // ASSIGN TEACHER ID if NULL
        if (($user->teacherId == '' || $user->teacherId == '0' || intval($user->teacherId) == 0)) {
            $teacher = new AbaTeacher();
            $teacher->getTeacherDefaultByLang($langEnv);
            $user->teacherId = $teacher->id;
        }
        // ASSIGN ID PARTNERS FIELDS
        // If idPartnerFirstPay or Source are empty, we must update them with the value of the current Partner:
        if (empty($user->idPartnerFirstPay) || trim($user->idPartnerFirstPay) == '') {
            $user->idPartnerFirstPay = $partnerId;
        }
        if (empty($user->idPartnerSource) || trim($user->idPartnerSource) == '') {
            $user->idPartnerSource = $partnerId;
            $user->registerUserSource = REGISTER_USER_FROM_PARTNER;
        }

        $commUser = new UserCommon();
        $expirationDate = null;
        //Cases : Exists, Exists && DELETED|FREE, Exists && PREMIUM, No Exists--------------********************
        if ($userExists) {
            // Case PREMIUM--------------------------------------------------------------------------********
            if ($user->userType == PREMIUM) {
                // Check for current payment subscription:
                if ($moPaymentPending->getLastPaymentByUserId($user->id, false)) {
                    if (intval($moPaymentPending->status) == PAY_PENDING) {
                        $isUnderSubscription = true;
                        if ($moPaymentPending->paySuppExtId == PAY_SUPPLIER_PAYPAL) {
                            HeLogger::sendLog(" A registration from partner " . $partnerId . " with a Groupon Code " . $voucherCode . " is likely to cause problems in near future.",
                                HeLogger::PAYMENTS, HeLogger::INFO,
                                " A registration from partner " . $partnerId . " with a Groupon Code " . $voucherCode . " for user " . $user->email . " is likely to cause problems in near future because " .
                                " this user is already an active PREMIUM user with active subscription through PAYPAL, and now is trying to pay with a -CUPON-.");
                        }
                    }
                }
            }
            // El expirationDate se suma días/meses gratis a la expirationDate siempre que
            // expirationDate sea mayor que hoy. Si no, se debe poner Hoy + PeriodMonths.
            $expirationDate = $commUser->getNextExpirationDate($user, $moPeriodicity) ;

        } else {
            // Case New User ------------------------------------------------------------------------------******
            $user->entryDate = HeDate::todaySQL(true);
            $user->email = $email;
            $user->userType = $userType;
            $user->langEnv = $langEnv;
            $user->currentLevel = 1;
            $user->countryId = $idCountry;
            $expirationDate = $commUser->getNextExpirationDate($user, $moPeriodicity) ;
            $user->idPartnerSource = $partnerId;
            $user->idPartnerFirstPay = $partnerId;
            $user->registerUserSource = REGISTER_USER_FROM_PARTNER;
        }


        $user->expirationDate = $expirationDate;
        $user->setPassword($pwd, true);
        $user->name = $name;
        $user->surnames = $surnames;
        $user->telephone = $telephone;
        $idPreviousPartner = $user->idPartnerCurrent;
        $user->idPartnerCurrent = $partnerId;
        $user->deviceTypeSource = $deviceTypeSource;
        if (!$user->validate(array("email", "password", "langEnv"))) {
            $msgErrores = "";
            $errores = $user->getErrors();
            foreach ($errores as $field => $message) {
                $msgErrores .= " Parameter " . $field . " is incorrect " . $message[0];
            }
            $this->errorMessage = " Some parameters were not correct for the registration of the user " . $msgErrores;
            return false;
        }


        // Send to DATABASE
        if (!$user->insertUserPremiumForRegistration()) {
            $this->errorMessage = " Updating/Inserting user $email has failed.";
            return false;
        }

        if (!$userExists) {
            // User dictionary
            $oAbaUserDictionary = new AbaUserDictionary();
            if (!$oAbaUserDictionary->insertUserDictionaryForRegistration($user)) {
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_LOGIC_L . ' Create (Insert) user dictionary inconsistency.',
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    ' Function insertUserPremiumForRegistration / insertUserDictionaryForRegistration'
                );
            }
        }
        $user = $commUser->convertToPremium($user, $partnerId, $expirationDate);
        if (!$user) {
            $this->errorMessage = " Converting  user $email to PREMIUM has failed.";
            return false;
        }
        // MOCK PAYMENT, IT IS A PAYMENT TO REFLECT GROUPON PURHCASE
        $ret = $moProduct->getProductById($moProduct->idProduct);
        if (!$ret) {
            $this->errorMessage = "Not found idProduct to generate currencies, and aliases for " .
                $moProduct->idProduct . $moProduct->idCountry . $moPeriodicity->id;
            return false;
        }
        // Insert a mock payment row -----------------------------------------------------------------------------------
        $moPayment = new Payment();
        $moPayment->id = HeAbaSHA::generateIdPayment($user);
        $moPayment->idUserProdStamp = 1;
        $moPayment->userId = $user->id;
        $moPayment->idProduct = $moProduct->idProduct;
        $moPayment->idCountry = $idCountry;
        $moPayment->idPeriodPay = $moPeriodicity->id;
        $moPayment->idUserCreditForm = null;
        $moPayment->paySuppExtId = PAY_SUPPLIER_GROUPON;
        $moPayment->paySuppOrderId = $voucherCode;
        $moPayment->status = PAY_SUCCESS;
        $moPayment->dateStartTransaction = HeDate::todaySQL(true);
        $moPayment->dateEndTransaction = HeDate::todaySQL(true);
        $moPayment->dateToPay = HeDate::todaySQL(true);
        $moCountry = new AbaCountry($moProduct->idCountry);
        $xRates = new AbaCurrency($moCountry->ABAIdCurrency);
        $moPayment->xRateToEUR = $xRates->getConversionRateTo(CCY_DEFAULT);
        $moPayment->xRateToUSD = $xRates->getConversionRateTo(CCY_2DEF_USD);
        $moPayment->currencyTrans = $moCountry->ABAIdCurrency;
        $moPayment->foreignCurrencyTrans = $moCountry->ABAIdCurrency;
        $moPayment->amountOriginal = $moProduct->priceOfficialCry;
        $moPayment->idPromoCode = $voucherCode;
        $moPayment->amountDiscount = $moPayment->amountPrice;
        $moPayment->amountPrice = 0.00;
        $moPayment->idPartner = $partnerId;
        $moPayment->idPartnerPrePay = $idPreviousPartner;
        $moPayment->isRecurring = 0;
        $moPayment->paySuppLinkStatus = PAY_RECONC_ST_MATCHED;

        //
        // tax rate
        //
        $moPayment->setAmountWithoutTax();

        // --------------------------------------
        if (!$moPayment->paymentProcess($moPayment->userId)) {
            $this->errorMessage = "Payment could not be successfully inserted for " . $moProduct->idProduct . $moProduct->idCountry . $moPeriodicity->id;
            return false;
        }

        return $user;
    }

    /** Checks on dbVentasFlash the Vouchers are free and available.
     * @param integer $userId
     * @param string $email
     * @param integer $partnerId
     * @param string $voucherCode
     * @param string $securityCode
     * @param ProdPeriodicity $moProdPeriod
     *
     * @return bool|int
     */
    public function verifyCodesFromPartner($userId, $email, $partnerId, $voucherCode, $securityCode, ProdPeriodicity $moProdPeriod )
    {
        $monthsPeriod = $moProdPeriod->months;
        $columnReturn = "";
        switch (intval($monthsPeriod)) {
            case 1:
                $columnReturn = "prefijoMensual";
                break;
            case 2:
                $columnReturn = "prefijoBimensual";
                break;
            case 3:
                $columnReturn = "prefijoTrimestral";
                break;
            // @todo: Bug, due to ENH-2012, have to change for 4 months
            case 4:
                $columnReturn = "prefijoTrimestral";
                break;
            case 6:
                $columnReturn = "prefijoSemestral";
                break;
            // @todo: Bug, due to ENH-2012, have to change for 9 months
            case 9:
                $columnReturn = "prefijoAnual";
                break;
            case 12:
                $columnReturn = "prefijoAnual";
                break;
            case 18:
                $columnReturn = "prefijo18Meses";
                break;
            case 24:
                $columnReturn = "prefijo24Meses";
                break;
            default:
                $this->errorMessage = "It is a period that has no equivalent on the old tables of groupon";
                return false;
                break;
        }

        $moPartner = new Partners();
        $tableGroupones = $moPartner->getGrouponTableByIdAndPeriod($partnerId, $columnReturn);
        if (!$tableGroupones) {
            $this->codeError = 509;
            $this->errorMessage = " Groupon or Partner $partnerId not found in database ";
            return false;
        }

        $retRow = $moPartner->getGrouponCodeFreeByCode($tableGroupones, $voucherCode);


        if (!$retRow) {

            $retRow = $moPartner->getGrouponCodeBusyByCode( $tableGroupones, $voucherCode);

            if (!$retRow) {
                $this->codeError = 509;
                $this->errorMessage = " Code $voucherCode is neither busy nor free in -ventas flash.$tableGroupones-. It is not avilable";
                return false;
            }

            $this->codeError = 514;
            $this->errorMessage = " Voucher $voucherCode has been validated with emails ".$retRow["email"]." on ".
                                $retRow["date"];

            return false;
        }

        return $tableGroupones;

    }


    /** Validates Groupon code in database db_ventasflash
     * @param integer $userId
     * @param $tableGroupones
     * @param $voucherCode
     * @param $securityCode
     * @param $email
     * @param integer $partnerId
     *
     * @return bool
     */
    public function validateGrouponCodeUsed($userId, $tableGroupones, $voucherCode, $securityCode, $email, $partnerId)
    {

        $moPartner = new Partners();
        if (!$moPartner->updateGrouponCodeUsed($tableGroupones, $voucherCode, $securityCode, $email)) {
            $this->errorMessage = " Update in $tableGroupones for $email has not worked. ";
            return false;
        } else {
            return true;
        }


        return false;
    }

    /**
     * @param string $email a valid email account
     * @param string $source source of the creation of the user, just descriptive
     *
     * @return string
     */
    public function genRandomPasswordNewUser($email, $source)
    {
        $email = substr($email, 0, (strpos($email, "@")-1));
        $str = '2014abaenglish'."welcome".$email.'14201209'.strval(rand(100, 999));
        $shuffled = $source.substr(str_shuffle($str), 0, 8);
        return strtolower($shuffled);
    }

    /**
     * Registers a new device used by an user and communicates this to Selligent. If user/device combination is already
     * registered the existing token is returned otherwise the newly created token is returned. If it is not possible
     * to create a token or the device false is returned.
     *
     * @param integer $deviceId
     * @param string $sysOper
     * @param string $deviceName
     * @param AbaUser $moUser
     * @param string $appVersion
     *
     * @return bool|string False or token is returned
     */
    public function registerNewDevice($deviceId, $sysOper, $deviceName, AbaUser $moUser, $appVersion = 'oldApp')
    {
        // Check if device already exist for this user:
        $moUserDevice = new AbaUserDevice();
        if ($moUserDevice->getDeviceByIdUserId($deviceId, $moUser->id)) {
            return $moUserDevice->token;
        }
        $token = HeAbaSHA::genTokenForMobile($sysOper, $deviceId, $moUser->email, $moUser->password);
        if (!$token) {
            return false;
        }
        if ($moUserDevice->insertNewDevice($deviceId, $moUser->id, $deviceName, $sysOper, $token, $appVersion)) {
            $commSelligent = new SelligentConnectorCommon();
            $commSelligent->sendOperationToSelligent(
                SelligentConnectorCommon::registerDevice,
                $moUser,
                array("DEVICE_NAME" => $deviceName, "SYSOPER" => $sysOper, "APP_VERSION" => $appVersion),
                "registerNewDevice"
            );
            return $token;
        }
        return false;
    }

    /**
     * @param string $email
     * @param string $pwd
     * @param string $langEnv
     * @param string $name
     * @param string $surnames
     * @param int    $currentLevel
     * @param null   $idPartnerSource
     * @param integer $idCountry
     * @param integer $registerSource
     * @param integer $idSourceList
     * @param string $deviceTypeSource
     *
     * @return AbaUser|bool
     */
    public function registerNewStandardUser(
        $email,
        $pwd,
        $langEnv,
        $name = "",
        $surnames = "",
        $currentLevel = null,
        $idPartnerSource = null,
        $idCountry = null,
        $registerSource = null,
        $idSourceList = null,
        $deviceTypeSource = null,
        $scenario = null
    ) {
        $updateUser = 0;
        if ($email=="") {
            $this->errorMessage = " Email not valid.";
            return false;
        }
        $registerLevel = $currentLevel;
        if (empty($currentLevel) || !is_numeric($currentLevel)) {
            $currentLevel = 1;
        }
        $user = new AbaUser();
        if ($user->getUserByEmail($email)) {
            // Let's check if user is DELETED, on that case we have to update it anyway.
            if ($user->userType==DELETED) {
                $updateUser = 1;
            } else {
                $this->errorMessage = Yii::t('mainApp', 'emailExist');
                return false;
            }
        }


        //
        //#ABAWEBAPPS-557
        if (HeMixed::checkEmailForFraud($email)) {
            $this->errorMessage = " Email not valid.";

            try {
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_UNKNOWN_H . ': Suspected fraud',
                  HeLogger::PAYMENTS,
                  HeLogger::CRITICAL,
                  " The user with the email " . $email . " could not register for suspected fraud"
                );
            } catch (Exception $e) { }

            return false;
        }
        //


        $user->scenario = "registerUser"; //Scenarios property from Yii Models.
        $user->email= $email;
        $user->setPassword($pwd, true);
        $user->langEnv = $langEnv;
        $user->langCourse = $langEnv;
        $user->name = $name;
        $user->surnames = $surnames;
        $user->currentLevel = $currentLevel;
        $user->registerLevel = $registerLevel;
        // u.`countryId`, u.`countryIdCustom`
        $user->countryId = $idCountry;
        $user->countryIdCustom = $idCountry;
        $user->idSourceList = $idSourceList;
        $user->deviceTypeSource = $deviceTypeSource;

        $teacher = new AbaTeacher();
        $teacher->getTeacherDefaultByLang($langEnv);
        $user->teacherId = $teacher->id;
        // 2.10.Assign [userType]=FREE y [expirationDate]=Today, we set Today, for FREE user have no use this field.
        $user->userType = FREE;
        $user->expirationDate = HeDate::todaySQL(true);
        // 2.11. [EntryDate] current date.
        $user->entryDate = HeDate::todaySQL(true);

        // From which service web user has been registered:
        $user->registerUserSource = REGISTER_USER;
        if (isset($registerSource)) {
            $user->registerUserSource = $registerSource;
        }
        // Id de Level Test Partner or maybe Aba Web site:
        if(!isset($idPartnerSource)) {
            $user->idPartnerSource = "". Yii::app()->config->get("ABA_PARTNERID");
            $user->idPartnerCurrent = $user->idPartnerSource;
        } else {
            $user->idPartnerSource = "". $idPartnerSource;
            $user->idPartnerCurrent = $user->idPartnerSource;
        }

        if (!$user->validate(array("email", "password", "langEnv"))) {
            $msgErrores = "";
            $errores = $user->getErrors();
            foreach ($errores as $field=>$message) {
                $msgErrores .= " Parameter ".$field." is incorrect ".$message[0];
            }
            $this->errorMessage = " Some parameters were not correct ".$msgErrores;
            return false;
        }
        if (!$this->insertUserForRegistration($user, $updateUser, $scenario)) {
            $this->errorMessage = " Insert into database failed.";
            return false;
        }

        // We log the first level in logLevelChange
        $commUser = new UserCommon();
        $commUser->changeUserLevel($user, $user->currentLevel, $user->currentLevel, 'Register new user');
        if (!$user->getUserByEmail($email, "", false)) {
            return false;
        }
        return $user;
    }

    /**
     * It returns the URL to create an account in the website www.abaenglish.com
     * According to the user selected language
     */
    public static function getCreateAccountUrl()
    {
        $language = Yii::app()->language;

        switch ($language) {
            case 'es':
                $createAccountUrl = 'http://'.Yii::app()->config->get("URL_DOMAIN_WEB").'/es/planes-curso-ingles/aba-free/';
                break;
            case 'en':
                $createAccountUrl = 'http://'.Yii::app()->config->get("URL_DOMAIN_WEB").'/en/english-courses/aba-free/';
                break;
            case 'it':
                $createAccountUrl = 'http://'.Yii::app()->config->get("URL_DOMAIN_WEB").'/it/modalita-corsi-inglese/aba-free/';
                break;
            case 'fr':
                $createAccountUrl = 'http://'.Yii::app()->config->get("URL_DOMAIN_WEB").'/fr/offres-courses-anglais/aba-free/';
                break;
            case 'pt':
                $createAccountUrl = 'http://'.Yii::app()->config->get("URL_DOMAIN_WEB").'/pt/planos-curso-ingles/aba-free/';
                break;
            default:
                $createAccountUrl = 'http://'.Yii::app()->config->get("URL_DOMAIN_WEB").'/en/english-courses/aba-free/';
        }

        return $createAccountUrl;
    }




    /**
     * @param $userExists
     * @param AbaUser $moUser
     * @param $email
     * @param $pwd
     * @param $langEnv
     * @param $name
     * @param $surnames
     * @param $currentLevel
     * @param $teacherId
     * @param $partnerId
     * @param $idCountry
     * @param $deviceTypeSource
     * @param ProdPeriodicity $moPeriodicity
     * @param ProductPrice $moProduct
     * @param int $discountPercent
     *
     * @return AbaUser|bool
     */
    public function registerUserPremiumFromB2bExtranet (
        $userExists, AbaUser $moUser, $email, $pwd, $langEnv, $name, $surnames, $currentLevel, $teacherId, $partnerId,
        $idCountry, $deviceTypeSource, ProdPeriodicity $moPeriodicity, ProductPrice $moProduct, $discountPercent = 0
    ) {
        $iUserType = null;
        if ($userExists) {
            $iUserType = $moUser->userType;
            if (!in_array($moUser->userType, array(LEAD, DELETED, FREE, PREMIUM))) {
                $this->codeError = '506';
                $this->errorMessage = 'Only DELETED, FREE and PREMIUM conversions are allowed';
                return false;
            }
        }

        // ASSIGN TEACHER ID if null
        if (($moUser->teacherId == '' || $moUser->teacherId == '0' || intval($moUser->teacherId) == 0)) {
            $teacher = new AbaTeacher();
            $teacher->getTeacherDefaultByLang($langEnv);
            $moUser->teacherId = $teacher->id;
        }
        // ASSIGN ID PARTNERS FIELDS
        // If idPartnerFirstPay or Source are empty, we must update them with the value of the current Partner:
        if (empty($moUser->idPartnerFirstPay) || trim($moUser->idPartnerFirstPay) == '') {
            $moUser->idPartnerFirstPay = $partnerId;
        }
        if (empty($moUser->idPartnerSource) || trim($moUser->idPartnerSource) == '') {
            $moUser->idPartnerSource =    $partnerId;
            $moUser->registerUserSource = REGISTER_USER_FROM_B2B_EXTRANET;
        }
        // Assign Country if null
        if ($idCountry == null) {
            $idCountry = Yii::app()->config->get("ABACountryId");
        }
        $commUser =         new UserCommon();
        $moPaymentPending = new Payment();
        $isUnderSubscription =  false;
        $idPreviousPartner =    $moUser->idPartnerCurrent;

        if ($userExists) {
            // Case PREMIUM--------------------------------------------------------------------------********
            if ($moUser->userType == PREMIUM) {
                // Check for current payment subscription:
                if ($moPaymentPending->getLastPaymentByUserId($moUser->id, false)) {
                    if (intval($moPaymentPending->status) == PAY_PENDING) {
                        $isUnderSubscription = true;
                        if ($moPaymentPending->paySuppExtId == PAY_SUPPLIER_PAYPAL) {
                            HeLogger::sendLog(" A registration from B2B Extranet " . $partnerId . " is likely to cause problems in near future.",
                                HeLogger::PAYMENTS, HeLogger::INFO,
                                " A registration from B2B Extranet  " . $partnerId . " is likely to cause problems in near future because " .
                                " this user is already an active PREMIUM user with active subscription through PAYPAL, and now is trying to pay with a -DEAL B2B-.");
                        }
                    }
                }
            }

            if($moUser->countryId != $idCountry){
                HeLogger::sendLog(
                    "The country (" . $moUser->countryId . ") of existing user does not match with the new country (" .
                    $idCountry . ") ",
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    " Please check user " . $moUser->email
                );
            }
        }
        else{

            $moUser->isNewUser = true;
            $moUser->isNewRecord = true;
            $moUser->entryDate = HeDate::todaySQL(true);
            $moUser->email = $email;
            $moUser->idPartnerSource = $partnerId;
            $moUser->idPartnerFirstPay = $partnerId;
            $moUser->registerUserSource = REGISTER_USER_FROM_B2B_EXTRANET;
            $moUser->userType = $moProduct->userType;

            if (!$moUser->validate(array("email", "password", "langEnv"))) {
                $msgErrores = "";
                $errores = $moUser->getErrors();
                foreach ($errores as $field => $message) {
                    $msgErrores .= " Parameter " . $field . " is incorrect " . $message[0];
                }
                $this->codeError = '506';
                $this->errorMessage = " Some parameters were not correct for the registration of the moUser " . $msgErrores;
                return false;
            }
        }

        $moUser->name = $name;
        $moUser->surnames = $surnames;
        $moUser->teacherId = $teacherId;
        $moUser->idPartnerCurrent = $partnerId;
        $moUser->expirationDate = $commUser->getNextExpirationDate($moUser, $moPeriodicity);

        $moUser->langEnv = $langEnv;
        $moUser->langCourse = $langEnv;
        $moUser->currentLevel = $currentLevel;
        $moUser->registerLevel = $currentLevel;
        $moUser->countryId = $idCountry;
        $moUser->countryIdCustom = $idCountry;
        $moUser->deviceTypeSource = $deviceTypeSource;

        $moUser->setPassword($pwd, true);

        // Send to DATABASE
        if (!$moUser->insertUserPremiumForRegistration()) {
            $this->errorMessage = " Updating/Inserting moUser $email has failed.";
            return false;
        }

        if (!$userExists) {
            // User dictionary
            $oAbaUserDictionary = new AbaUserDictionary();
            if (!$oAbaUserDictionary->insertUserDictionaryForRegistration($moUser)) {
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_LOGIC_L . ' Create (Insert) user dictionary inconsistency.',
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    ' Function insertUserPremiumForRegistration / insertUserDictionaryForRegistration '
                );
            }
        }

        $moUser = $commUser->convertToPremium($moUser, $partnerId, $moUser->expirationDate, $moProduct->userType);
        if (!$moUser) {
            $this->errorMessage = " Converting  moUser $email to PREMIUM has failed.";
            return false;
        }

        // Insert a mock payment row ---------------------------------------------------------------------------
        $moPayment = new Payment();

        $moPayment->id =                    HeAbaSHA::generateIdPayment($moUser);
        $moPayment->idUserProdStamp =       1;
        $moPayment->userId =                $moUser->id;
        $moPayment->idProduct =             $moProduct->idProduct;
        $moPayment->idCountry =             $idCountry;
        $moPayment->idPeriodPay =           $moPeriodicity->id;
        $moPayment->idUserCreditForm =      null;
        $moPayment->paySuppExtId =          PAY_SUPPLIER_B2B;
        $moPayment->status =                PAY_SUCCESS;
        $moPayment->dateStartTransaction =  HeDate::todaySQL(true);
        $moPayment->dateEndTransaction =    HeDate::todaySQL(true);
        $moPayment->dateToPay =             HeDate::todaySQL(true);

        $moCountry =    new AbaCountry($moProduct->idCountry);
        $xRates =       new AbaCurrency($moCountry->ABAIdCurrency);

        $moPayment->xRateToEUR =            $xRates->getConversionRateTo(CCY_DEFAULT);
        $moPayment->xRateToUSD =            $xRates->getConversionRateTo(CCY_2DEF_USD);
        $moPayment->currencyTrans =         $moCountry->ABAIdCurrency;
        $moPayment->foreignCurrencyTrans =  $moCountry->ABAIdCurrency;

        $moPayment->idPartner =             $partnerId;
        $moPayment->idPartnerPrePay =       $idPreviousPartner;
        $moPayment->isRecurring =           0;
        $moPayment->paySuppLinkStatus =     PAY_RECONC_ST_MATCHED;
        $moPayment->idSession =             Yii::app()->getSession()->getSessionId();
        // --------------------------------------

        // Extranet promocode
        $moDeal =       new PartnerDealsB2b();
        $idPromoCode =  $moDeal->createB2bExtranetPromoCode($discountPercent);

        $moPayment->paySuppOrderId = $idPromoCode;
        $moPayment->idPromoCode =    $idPromoCode;

        // tax discount
        $moPayment->setExtranetAmounts($moProduct->priceExtranet, $discountPercent);

        // tax rate
        $moPayment->setAmountWithoutTax();
        if (!$moPayment->insertPayment()) {
            $this->errorMessage = "Payment could not be successfully inserted for " . $moProduct->idProduct . $moProduct->idCountry . $moPeriodicity->id;
            return false;
        }
        $moUser->abaUserLogUserActivity->saveUserLogActivity('user', 'Registration from type '.$iUserType.' to Premium B2B','registerUserPremiumFromB2bExtranet', $moUser->id);
        return $moUser;
    }


    /**
     * @param $userExists
     * @param AbaUser $moUser
     * @param $params
     *
     * @return AbaUser|bool
     */
    public function registerUserTeacher ($userExists, AbaUser $moUser, $params) {

        if(!isset($params['registerUserSource']) OR !is_numeric($params['registerUserSource'])) {
            $params['registerUserSource'] = REGISTER_USER;
        }

        if (!isset($params['idCountry']) OR !is_numeric($params['idCountry'])) {
            $params['idCountry'] = Yii::app()->config->get("ABACountryId");
        }

        if(!isset($params['device']) OR trim($params['device']) == '') {
            $params['device'] = null;
        }

        if(!isset($params['registerTeacherSource']) OR !is_numeric($params['registerTeacherSource'])) {
            $params['registerTeacherSource'] = TEACHER_REGISTER_SOURCE_ABA;
        }

        if(!isset($params['langTeacher']) OR trim($params['langTeacher']) == '') {
            $params['langTeacher'] = ' ';
        }

        if(!isset($params['nationality']) OR trim($params['nationality']) == '') {
            $params['nationality'] = null;
        }

        if(!isset($params['photo']) OR trim($params['photo']) == '') {
            $params['photo'] = '';
        }

        if(!isset($params['mobileImage2x']) OR trim($params['mobileImage2x']) == '') {
            $params['mobileImage2x'] = null;
        }

        if(!isset($params['mobileImage3x']) OR trim($params['mobileImage3x']) == '') {
            $params['mobileImage3x'] = null;
        }

        $commUser = new UserCommon();
        $moUser->name =             $params['name'];
        $moUser->surnames =         $params['surnames'];
        $moUser->langEnv =          $params['langEnv'];
        $moUser->langCourse =       $params['langEnv'];
        $moUser->countryId =        $params['idCountry'];
        $moUser->countryIdCustom =  $params['idCountry'];
        $moUser->deviceTypeSource = $params['device'];
        $moUser->email =            $params['email'];
        $moUser->setPassword($params['pwd'], true);
        $moTeacher = new AbaTeacher();
        if ($userExists) {
            // Update
            if ($moUser->userType == TEACHER) {
                if (!$moUser->updateUserTeacher()) {
                    HeLogger::sendLog(
                      HeLogger::PREFIX_ERR_LOGIC_L . ' Teacher inconsistency. User has not been updated.',
                        HeLogger::IT_BUGS,
                        HeLogger::CRITICAL,
                        ' Function registerUserTeacher '
                    );
                }
            } else {
                $this->codeError =      '506';
                $this->errorMessage =   'This user already exists in the database.';
                return false;
            }
            $currentTeacher =   $moTeacher->existsTeacher($moUser->id);
            if ($currentTeacher AND $currentTeacher['registerTeacherSource'] != $params['registerTeacherSource']) {
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_LOGIC_L . ' Teacher inconsistency. This user already exists in the database'.
                    ' but registerTeacherSource of teacher is different from the original registerTeacherSource. ID=' .
                    $moUser->id,
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    ' Function registerUserTeacher '
                );
                $this->codeError =      '506';
                $this->errorMessage =   'This teacher already exists in the database but registerTeacherSource of teacher is different from the original registerTeacherSource.';
                return false;
            }
        } else {
            $moUser->isNewUser =            true;
            $moUser->isNewRecord =          true;
            $moUser->expirationDate =       HeDate::todaySQL(true);
            $moUser->entryDate =            HeDate::todaySQL(true);
            $moUser->idPartnerSource =      null;
            $moUser->idPartnerCurrent =     null;
            $moUser->idPartnerFirstPay =    null;
            $moUser->registerUserSource =   $params['registerUserSource'];
            $moUser->currentLevel =         1;
            $moUser->userType =             TEACHER;
            $moUser->teacherId =            0;

            if (!$moUser->validate(array("email", "password", "langEnv"))) {
                $msgErrores =   "";
                $errores =      $moUser->getErrors();
                foreach ($errores as $field => $message) {
                    $msgErrores .= " Parameter " . $field . " is incorrect " . $message[0];
                }
                $this->codeError =      '506';
                $this->errorMessage =   " Some parameters were not correct for the registration of the moUser " . $msgErrores;
                return false;
            }
            if (!$this->insertUserForRegistration($moUser)) {
                $this->errorMessage = " Updating/Inserting moUser " . $params['email'] . " has failed.";
                return false;
            }
            $moUser->teacherId = $moUser->id;

            if(!$moUser->updateUserTeacherId()) {
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_LOGIC_L . ' Teacher inconsistency. User-teacher has beeen created but the '.
                    'teacherId field has not been updated.',
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    ' Function registerUserTeacher '
                );
            }
        }
        // TEACHER
        $teacherData = array(
            'userid' =>                 $moUser->id,
            'nationality' =>            $params['nationality'],
            'language' =>               $params['langTeacher'],
            'photo' =>                  ($params['photo'] != '' ? $params['photo'] : $moUser->id . ".png"),
            'mobileImage2x' =>          $params['mobileImage2x'],
            'mobileImage3x' =>          $params['mobileImage3x'],
            'registerTeacherSource' =>  $params['registerTeacherSource'],
        );
        if(!$moTeacher->insertTeacher($teacherData)) {
            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_LOGIC_L . ' Teacher inconsistency. This user already exists in the database but Updating/Inserting teacher has failed OR not changed data. ID=' . $moUser->id,
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                ' Function registerUserTeacher '
            );
            $this->codeError =      '506';
            $this->errorMessage =   " Some parameters were not correct for the registration of the moUser ";
            return false;
        }
        $moUser->abaUserLogUserActivity->saveUserLogActivity('user', 'New teacher registration: ID=' . $moUser->teacherId . '.', 'registerUserTeacher', $moUser->id);
        return $moUser;
    }

    /**
     * @param AbaUser $moUser
     * @param $params
     *
     * @return AbaUser|bool
     */
    public function changeLicense (AbaUser $moUser, $params) {
        if(!isset($params['teacherId']) OR !is_numeric($params['teacherId']) OR intval($params['teacherId']) == 0) {
            $teacher = new AbaTeacher();
            $teacher->getTeacherDefaultByLang($moUser->langEnv);
            $params['teacherId'] = $teacher->id;
        }
        else {
            $abaTeacher = new AbaTeacher();
            $teacherExists = $abaTeacher->existsTeacher($params['teacherId']);
            if(!$teacherExists) {
                $this->codeError =      '506';
                $this->errorMessage =   'Teacher does not exists in the database.';
                return false;
            }
        }
        if (!isset($params['idPartnerCurrent']) OR !is_numeric($params['idPartnerCurrent'])) {
            $params['idPartnerCurrent'] = null;
        }
        $commUser = new UserCommon();
        $moUser->teacherId =        $params['teacherId'];
        $moUser->idPartnerCurrent = $params['idPartnerCurrent'];
        if(!$moUser->changeUserLicense()) {
            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_LOGIC_L . ' Updating user teacher has failed OR not changed data. User ID=' .
                $moUser->id . ". ",
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                ' Function changeLicense '
            );
            $this->codeError =      '506';
            $this->errorMessage =   " Updating user teacher has failed OR not changed data. ";
            return false;
        }
        $moUser->abaUserLogUserActivity->saveUserLogActivity('user', 'Updating user teacher and current partner', 'changeLicense', $moUser->id);
        return $moUser;
    }


    /**
     * @param AbaUser $oAbaUser
     * @param $experimentTypeId
     *
     * @param null $experimentVariationId
     * @return bool
     */
    public function insertUserExperimentVariationForRegistration(
      AbaUser $oAbaUser,
      $experimentTypeId,
      $experimentVariationId = null
    ) {
        $iEnabledExperiments = Yii::app()->config->get("ENABLED_EXPERIMENTS");
        if ($iEnabledExperiments == 1) {
            $oAbaUserExperimentsVariations = new AbaUserExperimentsVariations();
            return $oAbaUserExperimentsVariations->insertUserExperimentVariationForRegistration($oAbaUser,
              $experimentTypeId, $experimentVariationId);
        }
        return true;
    }

    /**
     * @param $oAbaUser
     * @param int $updateUser
     * @param null $scenario
     *
     * @return mixed
     */
    public function insertUserForRegistration($oAbaUser, $updateUser = 0, $scenario = null)
    {
        $result = $oAbaUser->insertUser($updateUser);
        //
        // User dictionary
        if ($result) {
            $oAbaUserDictionary = new AbaUserDictionary();
            if (!$oAbaUserDictionary->insertUserDictionaryForRegistration($oAbaUser)) {
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_LOGIC_L . ' Create (Insert) user dictionary inconsistency.',
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    ' Function insertUserForRegistration / insertUserDictionaryForRegistration '
                );
            } else {
                //Check if we need to apply a test
                $clsAbaCouseTest = new CmAbaCourseTest();
                $tmpidTest = $clsAbaCouseTest->retrieveTest($oAbaUser);
            }

            //#6254
            $stProfiles = CmUserRules::getOrCreateUserExperimentProfilesWithType($oAbaUser, CmAbaExperimentsTypes::EXPERIMENT_TYPE_SCENARIOS_PAYMENTS, true, $scenario);
        }

        return $result;
    }

}
