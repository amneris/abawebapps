<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 5/10/12
 * Time: 12:34
 * To change this template use File | Settings | File Templates.
 */
/**
 * @property integer $role
 * @property integer $currentLevel
 * @property PaymentControlCheck $paymentControlCheck
 * @property integer $paymentControlCheckId
 * @property integer $paymentControlStatus
 */
class AbaWebUser extends CWebUser
{
    // Store model to not repeat query.
    private $_model;
    private $_mgm; // member get member

    /** Return first name.
     * @return int
     */
    public function getTeacherId()
    {
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->teacherId;
    }

    public function getCountryId()
    {
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->countryId;
    }

    /** Return the current session id partner associated with the navigation of the user.
     * It has nothing to do with user.idPartnerCurrent, which is the last payment partner id.
     * @return integer
     */
    public function getIdPartnerCurNavigation()
    {
        $idPartner = Yii::app()->config->get("ABA_PARTNERID");
        if( !is_null(Yii::app()->user->getState("idPartnerCurNavigation")) &&
            Yii::app()->user->getState("idPartnerCurNavigation")!=='')
        {
            $idPartner = Yii::app()->user->getState("idPartnerCurNavigation");
        }

        return $idPartner;
    }

    /**
     * @return string
     */
    public function getSourceId()
    {
        $idSource = "";
        if( !is_null(Yii::app()->user->getState("idSource")) &&
          Yii::app()->user->getState("idSource") !== '')
        {
            $idSource = Yii::app()->user->getState("idSource");
        }

        return $idSource;
    }

    /**
     * @return string
     */
    public function getIdPromoCode()
    {
        $idPromocode = "";
        if( !is_null(Yii::app()->user->getState("promoCode")) &&
          Yii::app()->user->getState("promoCode") !== '')
        {
            $idPromocode = Yii::app()->user->getState("promoCode");
        }

        return $idPromocode;
    }

    /**
     * @return int
     */
    public function getIdPartnerSource()
    {
        $user = $this->loadUser(Yii::app()->user->id);
        $idPartnerSource = $user->idPartnerSource;
        return $idPartnerSource;
    }

    /**
     * @return int
     */
    public function getIdSourceList()
    {
        $user = $this->loadUser(Yii::app()->user->id);
        $idSourceList = $user->idSourceList;
        return $idSourceList;
    }

    /** Gets language of the Campus for the current User
     * @return string
     */
    public function getLanguage()
    {
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->langEnv;
    }

    /** Gets Language of the Course for current User, not the Campus language
     * @return string
     */
    public function getLangCourse()
    {
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->langCourse;
    }

    /** Returns current Email
     * @return string
     */
    public function getEmail()
    {
        /**
         * @var AbaUser $user
         */
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->email;
    }

    public function getPassword()
    {
        /**
         * @var AbaUser $user
         */
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->password;
    }

    public function getKeyExternalLogin()
    {
        /**
         * @var AbaUser $user
         */
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->keyExternalLogin;
    }

    public function getReleaseCampus()
    {
        /**
         * @var AbaUser $user
         */
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->releaseCampus;
    }

    public function getSavedUserObjective()
    {
        /**
         * @var AbaUser $user
         */
        $user = $this->loadUser(Yii::app()->user->id);
        return intval($user->savedUserObjective);
    }

    public function getMemberGetMember()
    {
        if(!$this->hasState('memberGetMember')) {
            $commUser = new UserCommon();
            $this->_mgm = $commUser->getMemberGetMember(Yii::app()->user->id);
            $this->setState('memberGetMember', $this->_mgm);
        }
        else {
            $this->_mgm = $this->getState('memberGetMember');
        }

        return $this->_mgm;
    }

    public function isPremiumFriend()
    {
        if ($this->role == FREE) {
            if ($this->hasState('memberGetMember')) {
                $this->_mgm = $this->getState('memberGetMember');
            } else {
                $this->getMemberGetMember();
            }
            $commUser = new UserCommon();
            return $commUser->isPremiumFriend(Yii::app()->user->id, $this->_mgm);
        }
        return false;
    }

    /**
     * Returns session variable flag to indicate if user is B2B
     * @return mixed
     */
    public function isB2B()
    {
        if(!$this->hasState('isB2B')) {
            /**
             * @var AbaUser $user
             */
            $user = $this->loadUser(Yii::app()->user->id);
            $moPartner = new Partners();
            if($moPartner = $moPartner->getPartnerById($user->idPartnerCurrent)) {
                $this->setState('currentPartnerId', $user->idPartnerCurrent);
                $this->setState('isB2B', $moPartner->idPartnerGroup == PARTNER_B2B_G);
            }
        }

        return $this->getState('isB2B');
    }

    /**
     * Returns session variable flag to indicate if user is B2B
     * @return mixed
     */
    public function getCurrentPartnerId()
    {
        if(!$this->hasState('currentPartnerId')) {
            /**
             * @var AbaUser $user
             */
            $user = $this->loadUser(Yii::app()->user->id);
            $this->setState('currentPartnerId', $user->idPartnerCurrent);

        }

        return $this->getState('currentPartnerId');
    }

    /** Returns current Email
     * @return string
     */
    public function getIdPartnerCurrent()
    {
        /**
         * @var AbaUser $user
         */
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->idPartnerCurrent;
    }

    /**
     *  Load user model.
     * @param integer $id
     *
     * @return AbaUser
     */
    protected function loadUser($id=NULL)
    {
        if($this->_model===NULL)
        {
            if($id!==NULL)
                $this->_model=AbaUser::model("AbaUser")->findByPk($id);
        }
        return $this->_model;
    }


    /**
     * Redirects the user browser to the login page.
     * Before the redirection, the current URL (if it's not an AJAX url) will be
     * kept in {@link returnUrl} so that the user browser may be redirected back
     * to the current page after successful login. Make sure you set {@link loginUrl}
     * so that the user browser can be redirected to the specified login URL after
     * calling this method.
     * After calling this method, the current request processing will be terminated.
     */
    public function loginRequired()
    {
        $app=Yii::app();
        $request=$app->getRequest();

        if(!$request->getIsAjaxRequest()){
            $retUrl_tmp = $request->getUrl();
            $arUrl = parse_url($retUrl_tmp);
            if (isset($arUrl['query'])) {
                parse_str($arUrl['query'], $output);
                if ( isset($output[MAGIC_LOGIN_KEY]) ) unset($output[MAGIC_LOGIN_KEY]);
                $arUrl['query'] = http_build_query($output);
                $arUrl['query'] = ( !empty($arUrl['query']) ) ? '?'.$arUrl['query'] : $arUrl['query'];
            }
            if (isset($arUrl['path'])) {
                $arTmp = explode('/', $arUrl['path']);
                foreach( $arTmp as $k=>$value ) {
                    if ( $value == MAGIC_LOGIN_KEY && isset($arTmp[$k+1])  ) {
                        unset($arTmp[$k]);
                        unset($arTmp[$k+1]);
                    }
                }
                $arUrl['path'] = implode('/', $arTmp);
            }
            $retUrl_tmp = implode('', $arUrl);
            $this->setReturnUrl($retUrl_tmp);
        }
        elseif(isset($this->loginRequiredAjaxResponse))
        {
            echo $this->loginRequiredAjaxResponse;
            Yii::app()->end();
        }

        if(($url=$this->loginUrl)!==NULL)
        {
            if(is_array($url))
            {
                $route=isset($url[0]) ? $url[0] : $app->defaultController;
                $url=$app->createUrl($route,array_splice($url,1));
            }

            $autoLoginUrl = "";
            $tmpValueKeyExternalLogin = HeMixed::getGetArgsRawMethod(MAGIC_LOGIN_KEY);
            if( !is_null($tmpValueKeyExternalLogin) && $tmpValueKeyExternalLogin!=='')
            {
//                $autoLoginUrl = "?".MAGIC_LOGIN_KEY."=".$tmpValueKeyExternalLogin;
                $autoLoginUrl = "/".MAGIC_LOGIN_KEY."/".$tmpValueKeyExternalLogin;
            }

            $request->redirect($url.$autoLoginUrl);
        }
        else
            throw new CHttpException(403,Yii::t('yii','Login Required'));
    }

    /** Used to refresh the user access type to the campus , every time the user access to the payment page, message, etc..
     * @param AbaUser $user
     *
     * @return integer
     */
    public function refreshAndGetUserType($user=NULL)
    {
        if(is_null($user)){
            $user = $this->loadUser(Yii::app()->user->id);
        }

        if( Yii::app()->user->role!==$user->userType )
        {
//            Yii::app()->user->setState("role", $user->userType);
            $this->setState("role", $user->userType);
        }

        return $this->role;
    }

    /**
     * @param AbaUser $user
     * @return bool
     */
    public function refresh($user=NULL)
    {
        $this->_model=AbaUser::model("AbaUser")->findByPk($user->id);
        return true;
    }

    /**
     * #ABAWEBAPPS-597
     *
     * @param bool $fromCookie
     */
    protected function afterLogin($fromCookie)
    {
        parent::afterLogin($fromCookie);

        Yii::app()->session['cooladataUserLogin'] = COOLDATA_USER_LOGIN;
    }

    protected function beforeLogin($id,$states,$fromCookie) {

        $bParent = parent::beforeLogin($id,$states,$fromCookie);
        $bLocal = false;
        $mUser = new AbaUser;

        if ($mUser->getUserById($id)) {
            $bLocal = $mUser->limiteHorario();
        }

        return ($bParent && $bLocal);
    }

    public function login($identity, $duration = 0)
    {
        return parent::login($identity, $this->authTimeout);
    }

    public function logout($destroySession = true)
    {
        parent::logout($destroySession);

        $this->_model = null;
        $this->_mgm = null;
    }

    /**
     * Checks if user has role, which means that the user has passed through authentication.
     * Otherwise maybe some flow has failed.
     *
     * @return bool
     */
    public function isLogged()
    {
        // @ascandroli: #todo implement isAuthenticated(), isRemembered() and isGuest()

        if (!isset($this->role)) {
            return false;
        }

        return (intval($this->role) >= 1);
    }

    public function getFullname()
    {
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->name.' '.$user->surnames;
    }

    /**
     * #ABAWEBAPPS-597
     *
     * @return string
     */
    public function getExpirationDate()
    {
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->expirationDate;
    }

    public function getBirthDate()
    {
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->birthDate;
    }

    public function getDeviceTypeSource()
    {
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->deviceTypeSource;
    }

    public function getEntryDate()
    {
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->entryDate;
    }

    public function getIdPartnerFirstPay()
    {
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->idPartnerFirstPay;
    }

}

