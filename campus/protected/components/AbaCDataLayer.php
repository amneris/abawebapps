<?php

/**
 * Class AbaCDataLayer
 *
 * class to store a variety of information gathered through the app lifecycle, such as user and payment details
 */
class AbaCDataLayer
{
    private $userId;
    private $partnerSource;
    private $email;
    private $device;
    private $paymentValue;
    private $currency;
    private $idProduct;
    private $paySuppExtId;
    private $idPayment;
    private $quantity;
    private $promoCode;
    private $discount;
    private $productPlan;
    private $comesFromRegisterUserFirstTime;
    private $idSite;

    public function init()
    {
        $this->idSite = 1;      // ** Campus es el idSite 1.
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    public function getPartnerSource()
    {
        return $this->partnerSource;
    }

    public function setPartnerSource($partnerSource)
    {
        $this->partnerSource = $partnerSource;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getDevice()
    {
        return $this->device;
    }

    public function setDevice($device)
    {
        $this->device = $device;
    }

    public function getPaymentValue()
    {
        return $this->paymentValue;
    }

    public function setPaymentValue($paymentValue)
    {
        $this->paymentValue = $paymentValue;
    }

    public function getPromoCode()
{
    return $this->promoCode;
}

    public function setPromoCode($promoCode)
    {
        $this->promoCode = $promoCode;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    public function setUser($user)
    {
        $this->setUserId($user->id);
        $this->setEmail($user->email);
        $this->setPartnerSource($user->idPartnerSource);
    }

    public function getIdProduct()
    {
        return $this->idProduct;
    }

    public function setIdProduct($idProduct)
    {
        $this->idProduct = $idProduct;
    }

    public function getPaySuppExtId()
    {
        return $this->paySuppExtId;
    }

    public function setPaySuppExtId($paySuppExtId)
    {
        $this->paySuppExtId = $paySuppExtId;
    }

    public function getIdPayment()
    {
        return $this->idPayment;
    }

    public function setIdPayment($idPayment)
    {
        $this->idPayment = $idPayment;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function getComesFromRegisterUserFirstTime()
    {
        return $this->comesFromRegisterUserFirstTime;
    }

    public function setComesFromRegisterUserFirstTime($comesFromRegisterUserFirstTime)
    {
        $this->comesFromRegisterUserFirstTime = $comesFromRegisterUserFirstTime;
    }

    public function getProductPlan() {
        return $this->productPlan;
    }

    public function setProductPlan($productPlan) {
        $this->productPlan = $productPlan;
    }

    public function getIdSite() {
        return $this->idSite;
    }

    public function setIdSite($idSite) {
        $this->idSite = $idSite;
    }

}