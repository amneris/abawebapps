<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 22/11/12
 * Time: 17:47
 * EXTENDS The standard class for CRON executions in Yii. All crons should inherit from here
 */
class AbaConsoleCommand extends CConsoleCommand
{

    protected $errorMessage;

    public function __construct($name, $runner)
    {
        parent::__construct($name, $runner);
    }

    public function getLastError()
    {
        return $this->errorMessage;
    }

    function __destruct()
    {
    }

    /** Tracks the end of execution into database for all crons.
     * @param $aIdLog
     */
    public static function logsEndExecution($aIdLog)
    {
    }

    /** Checks if is enabled the debugEcho for Crons in table cron.
     * @param $text
     */
    protected function echoCron($text)
    {
    }

}
