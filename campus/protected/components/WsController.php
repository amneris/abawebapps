<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 12/11/12
 * Time: 16:27
 * To change this template use File | Settings | File Templates.
 */
class WsController extends CController implements IWebServiceProvider
{
    protected static $aErrorCodes = array(
        "410"=> "The purchase receipt is not valid",
        "555"=> "Unidentified error",
        "501"=> "Invalid Parameter Value",
        "502"=> "Invalid Signature",
        "503"=> "Operation not completed",
        // Detailed and customized errors, referenced from outside this class:
        "504"=> "User does not exist",
        "505"=> "Selligent communications error",
        "506"=> "User could not be created",
        "507"=> "User email is empty",
        "508"=> "Product not valid",
        "509"=> "Voucher is not valid. Probably it does not exist, or maybe it applies to another product",
        "510"=> "User registered, but error on GROUPON CODE update.",
        "511"=> "User could not be updated",
        "512"=> "Promocode not valid",
        "513"=> "Country Id not valid. Check its value on your call please.",
        "514"=> "Voucher already used. The code probably has been validated: ",
        "515"=> "Payment not valid.",
        "516"=> "Payment gateway did not allow transaction.",
        "517"=> "Payment is too old to be refund. Ask IT.",
        "518"=> "Change of credit form in subscription not allowed. ",
        "519"=> "User already exists and is Premium",
        "520"=> "teacher email not valid",
        "521"=> "Invalid Level",
        "522"=> "Password is not valid.",
        "523"=> "The facebook id is empty or is not valid.",
        "524"=> "User already exists. Gift codes can only be activated by unregistered users.",
        "556"=> "All not valid products",
        "557"=> "Operation completed with errors",
    );

    /* @var LogWebService $moLogWs */
    protected $moLogWs;
    protected $nameService;
    protected $aResponse;
    protected $isSuccess;
    protected $errorCode;

    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
        $this->nameService = NULL;
        Yii::app()->params["isTransRunning"] = false;

    }

    /**
     * @param CWebService $service
     *
     * @return bool
     */
    public function beforeWebMethod($service)
    {
        $this->logStart( $service);
        return true;
    }

    /**
     * @param CWebService $service
     *
     * @return bool
     */
    public function afterWebMethod($service)
    {
        $this->logEnd( $this->aResponse, $this->isSuccess, $this->errorCode);
        return true;
    }

    /**
     * @param string $signature
     * @param array $params
     *
     * @return bool
     */
    protected function isValidSignature( $signature, $params )
    {
        if( substr($signature,-3) == "123"  && Yii::app()->config->get("SELF_DOMAIN")!==DOMAIN_LIVE )
        {
            return true ;
        }
        // We have to a check on signature. For example a hash with all arguments
        if( $signature === $this->getSignature($params) ){
            return true;
        }

        return false;
    }

    /**
     * @param string $signature
     * @param array $params
     *
     * @return bool
     */
    protected function isValidSignatureAffiliate( $signature, $params )
    {
        if( substr($signature,-3) == "123"  && Yii::app()->config->get("SELF_DOMAIN")!==DOMAIN_LIVE )
        {
            return true ;
        }
        // We have to a check on signature. For example a hash with all arguments
        if( $signature === $this->getSignatureAffiliate($params) ){
            return true;
        }

        return false;
    }

    /**
     * @param $signature
     * @param $params
     * @return bool
     */
    protected function isValidSignatureExtranet( $signature, $params )
    {
        if( substr($signature,-3) == "123"  && Yii::app()->config->get("SELF_DOMAIN")!==DOMAIN_LIVE )
        {
            return true ;
        }
        // We have to a check on signature. For example a hash with all arguments
        if( $signature === $this->getSignatureExtranet($params) ){
            return true;
        }

        return false;
    }


    /**
     * @param $signature
     * @param $params
     * @return bool
     */
    protected function isValidSignatureExternal( $signature, $params )
    {
        if( substr($signature, -3) == "123" && Yii::app()->config->get("SELF_DOMAIN") !== DOMAIN_LIVE ) {
            return true ;
        }
        // We have to a check on signature. For example a hash with all arguments
        if( $signature === $this->getSignatureExternal($params) ){
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    protected function isValidHttpAdyenPassword()
    {
        // TEST / STAGE
        if(Yii::app()->config->get("SELF_DOMAIN") !== DOMAIN_LIVE) {
            return true;
        }

        $notificationUser = (isset($_SERVER['PHP_AUTH_USER']) ? trim($_SERVER['PHP_AUTH_USER']) : '');
        $notificationPass = (isset($_SERVER['PHP_AUTH_PW']) ? trim($_SERVER['PHP_AUTH_PW']) : '');

        $PAY_ADYEN_NOTIFICATIONS_LOGIN =    Yii::app()->config->get("PAY_ADYEN_NOTIFICATIONS_LOGIN");
        $PAY_ADYEN_NOTIFICATIONS_PASSWORD = Yii::app()->config->get("PAY_ADYEN_NOTIFICATIONS_PASSWORD");

        if( $PAY_ADYEN_NOTIFICATIONS_LOGIN == $notificationUser AND $PAY_ADYEN_NOTIFICATIONS_PASSWORD == $notificationPass ){
            return true;
        }
        return false;
    }

    /**
     * @param $params
     *
     * @return string
     */
    protected function getSignature( $params )
    {
        $joinParams = implode("", $params);
        $joinParams = HeMixed::getWord4Ws().$joinParams;
        $signature = md5( $joinParams );

        return $signature;
    }

    /**
     * @param $params
     *
     * @return string
     */
    protected function getSignatureAffiliate( $params )
    {
        $joinParams = implode("", $params);
        $joinParams = HeMixed::getWord4WsPartners().$joinParams;
        $signature = md5( $joinParams );

        return $signature;
    }

    /**
     * @param $params
     *
     * @return string
     */
    protected function getSignatureExtranet( $params )
    {
        $joinParams = implode("", $params);
        $joinParams = HeMixed::getWord4WsExtranet().$joinParams;
        $signature = md5( $joinParams );

        return $signature;
    }

    /**
     * @param $params
     *
     * @return string
     */
    protected function getSignatureExternal( $params ) {
        $joinParams =   implode("", $params);
        $joinParams =   HeMixed::getWord4WsExternal() . $joinParams;
        $signature =    md5( $joinParams );

        return $signature;
    }

    /**
     * @param null $code
     * @param string $additionalErrorMsg
     *
     * @return array
     */
    protected function retErrorWs( $code=NULL, $additionalErrorMsg="" )
    {
        if (!isset($code)) {
            $code = "555"; // generic error code
        }

        $aResponse = array($code => self::$aErrorCodes[$code] . " - " . $additionalErrorMsg);

        $this->aResponse = $aResponse;
        $this->isSuccess = 0;
        $this->errorCode = $code;

        return $aResponse;
    }

    /**
     * @param null $code
     * @param string $additionalErrorMsg
     * @return array
     */
    protected function retWarningWs($code=null, $additionalErrorMsg="")
    {
        if(!isset($code)) {
            $code = "555"; // generic warning code
        }

        $aResponse = array( $code => self::$aErrorCodes[$code]." - ".$additionalErrorMsg );

        $this->aResponse = $aResponse;
        $this->isSuccess = 1;
        $this->errorCode = null;

        return $aResponse;
    }


    protected function retSuccessWs( $aResponse )
    {
        $this->aResponse = $aResponse;
        $this->isSuccess = 1;

        return $aResponse;
    }


    /**
     * @param array $aResponse
     * @param integer $success
     * @param int  $errorCode
     *
     */
    final protected function logEnd( $aResponse, $success = false, $errorCode=null)
    {
        if (Yii::app()->config->get("ENABLE_LOG_WEBSERVICES")) {
            if (!is_null($this->nameService) && is_subclass_of($this->moLogWs, 'CmLogWebService')) {
                $this->moLogWs->dateResponse = HeDate::todaySQL(true);
                $this->moLogWs->errorCode = $errorCode;
                $this->moLogWs->paramsResponse = HeMixed::serializeToStoreInDb($aResponse);
                $this->moLogWs->successResponse = $success;
                if (!$this->moLogWs->updateLogEndRequest()) {
                    HeLogger::sendLog(
                        "Model Error SQL update LogWebservice",
                        HeLogger::IT_BUGS,
                        HeLogger::CRITICAL,
                        "Function moLogWs->updateEndRequest() has raised an error. Please review."
                    );
                }
            }
        }
    }

    /**
     * @param CWebService $service
     */
    protected function logStart($service=null)
    {
        if (Yii::app()->config->get("ENABLE_LOG_WEBSERVICES")) {
            $this->moLogWs = new LogWebService();
            $this->moLogWs->getLogById();

            $this->moLogWs->dateRequest = HeDate::todaySQL(true);
            $this->moLogWs->ipFromRequest = Yii::app()->request->getUserHostAddress();
            $this->moLogWs->appNameFromRequest = HeMixed::getNameKnownHost(Yii::app()->request->getUserHostAddress());
            $this->moLogWs->urlRequest = Yii::app()->request->getHostInfo() . Yii::app()->request->getUrl();

            $postdata = file_get_contents("php://input");
            $this->moLogWs->paramsRequest = $postdata;

            if (!is_null($service)) {
                $nameService = $service->getMethodName();
            } else {
                $nameService = $this->retServiceName();
            }
            $this->nameService = $nameService;
            $this->moLogWs->nameService = $this->nameService;

            $this->moLogWs->successResponse = false;

            $this->moLogWs->insertLog();
        }
    }

    /**
     * It extracts the name of the web service from one of the params of CGI variables
     * @return string
     */
    private function retServiceName( )
    {
        $nameService = $_SERVER["HTTP_SOAPACTION"];
        if( strpos( $nameService, "#") )
        {
            $aNameService = explode("#", $_SERVER["HTTP_SOAPACTION"]);
            $nameService = str_replace('"',"", $aNameService[1]);
        }

        return $nameService;
    }

    /**
     * @param null $code
     * @param string $additionalErrorMsg
     * @return array
     */
    public static function getErrorNoWs($code=NULL, $additionalErrorMsg="") {
        if (!isset($code)) {
            $code = "555"; // generic error code
        }
        return array($code => self::$aErrorCodes[$code] . " - " . $additionalErrorMsg);
    }

}