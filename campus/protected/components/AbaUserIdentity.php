<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */

class AbaUserIdentity extends CUserIdentity
{
    const ERROR_NO_ACCESS = 101;
    const ERROR_INACTIVE_USER = 999;
    const ERROR_USER_LIMITEHORARIO = 998;

    private $_id;
    private $_name;
    private $_role;
    private $_currentLevel;
    private $_rememberMe;
    private $_lastLoginTime;

    public function __construct($username, $password, $rememberMe=0)
    {
        $password = trim($password);
        parent::__construct($username, $password);
        $this->_rememberMe = $rememberMe;
    }

    /**
     * Originally defined in the parent class
     * @param bool $pwdEncrypted
     *
     * @return bool
     */
    public function authenticate($pwdEncrypted = false)
    {
        $user = new AbaUser();
        $this->errorCode = "";

        if (!$user->getUserByEmail(array('email' => $this->username))) //Miguel: overrinding $this->username with email information
        {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } elseif (!$user->getUserByEmail(array('email' => $this->username), $this->password, true, $pwdEncrypted)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
            //TODO: Refactor! From a performance perspective it is bad to check schedule for all users when this is only an
            //TODO: issue for B2B users.
        } elseif (($errorMsg = $user->limiteHorario()) !== true) {
            $this->errorCode = self::ERROR_USER_LIMITEHORARIO;
            $this->errorMessage = $errorMsg;
        }

        if ($this->errorCode == "") {
            $this->errorCode = self::ERROR_NONE;
            // Please set all session and yii global variables depending on the user in this function
            $this->initSessionUserVariables($user);
            if (!HeRoles::allowOperation(OP_CAMPUSACCESS, $user->userType)) {
                $this->errorCode = self::ERROR_NO_ACCESS;
            } else {
                // Success on login:
                $user = $this->traceLogin($user);
            }
        }

        return !$this->errorCode;
    }

	public function getId()
	{
		return $this->_id;

	}

	public function getName()
	{
		return $this->_name;
	}

    /**
     * @param AbaUser $user
     *
     * @return AbaUser
     */
    private function traceLogin(AbaUser $user)
    {
        $logUserActivity = new AbaLogUserActivity($user);
        // Login successful so we check if it is first login
        // and proceed with special features:
        if( $user->firstLoginDone==0 )
        {

            //#ABAWEBAPPS-701
            Yii::app()->session['cooladataUserRegister'] = COOLDATA_USER_REGISTER;

            $user->updateFirstLoginDone(1);
            $this->setState('comesFromRegisterUserFirstTime', 1);
            $this->setState('isUserFirstLogin', 1);

            /* 5377 */
            setcookie('loginTimes', 1, 0, '/');

            $logUserActivity->saveUserLogActivity("user","First login successful of ".$user->email,"Login");

            //  Send data to Selligent
            $commConnSelligent = new SelligentConnectorCommon();
            $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::login, $user, array(), "traceLogin web service" );
        }
        else
        {
            $logUserActivity->saveUserLogActivity("user","Login successful of ".$user->email,"Login");
            /* 5377 */
            $loginTimes = isset($_COOKIE['loginTimes']) ? (int)$_COOKIE['loginTimes'] + 1 : 2;
            setcookie('loginTimes', $loginTimes, 0, '/');
        }
        setcookie('language', Yii::app()->language, 0, '/');

        // remove abamoments cookie every time a user logs in
        if (isset($_COOKIE['abamoments'])) {
            setcookie('abamoments', '', -1, '/');
        }

        $user->updateLastLoginTime();

        if( $user->hasWorkedCourse==0 )
        {
            $moFollowUp = new AbaFollowup();
            if($moFollowUp->getHasStartedCourse( $user->id ))
            {
                $user->updateHasWorkedCourse( 1 );
                $commConnSelligent = new SelligentConnectorCommon();
                $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::haTrabajadoCampus, $user, array(), "actionLogin first time has detected he worked" );
            }
        }

        return $user;
    }



    /**
     * @param AbaUser $user
     * @param bool    $throughLogin
     *
     * @return bool
     */
    public function initSessionUserVariables(AbaUser $user, $throughLogin = true)
    {
        // Once the login is successful we are about to restart the public counter of attempts to the public Campus URLs:
        Yii::app()->user->setState("attemptPublicUrl", 0 );

        $this->_id = $user->getId();
        $this->_name = $user->name;
        $this->_role = $user->userType;

        $this->_lastLoginTime = $user->lastLoginTime;

        // Access level:
        $this->setState('role', $user->userType);
        // Language of the course:
        $this->setState('langCourse', $user->langEnv);
        // The one is displayed in the main page
        $this->setState('currentLevel', 1);
        // Last Login DateTime
        $this->setState('lastLoginTime', $user->lastLoginTime);

        // Last Login DateTime
        $this->setState('fbId', $user->fbId);

        // Reset any promocode:
        $this->setState('promoCode', NULL);
        // Reset any idProduct:
        $this->setState('idProduct', NULL);
        // Reset any Partner:
        $this->setState('idPartnerCurNavigation', NULL);

        // Reset payment control check:
//        $this->setState('paymentControlCheckId', NULL);
        $this->initPaymentControlCheckId();

        // Reset source:
        //#5191
        $this->setState('idSource', NULL);

        // Reset this variable used in Google Analytics for Adwords:
        $this->setState('comesFromRegisterUserFirstTime', 0);


        if( isset($user->currentLevel) && intval($user->currentLevel)>=1 )
        {
            $this->setState('currentLevel', $user->currentLevel);
        }
        // If we want the user to have a popup on the home page after Login action.
        $this->setState( "showObjectives" , "0");
        // We save if the user wants to be remembered, used in sessionTimeout management:
        if($throughLogin)
        {
            $this->setState( "rememberMe" , $this->_rememberMe);
        }

        // To fake IP in order to test the application:
        if( strpos(Yii::app()->user->returnUrl, "IP_FALSE_TEST=")>=1 ) {
            $aIP = explode("IP_FALSE_TEST=",Yii::app()->user->returnUrl);
            $_SERVER["REMOTE_ADDR"] = $aIP[1];
        }

        // If user data has been loaded in the session data:
        Yii::app()->params['sessionVarsLoaded'] = 1;
        // In case the database would be Transactional InnoDB
        Yii::app()->params["isTransRunning"] = false;

        //#ABAWEBAPPS-597
        if(isset(Yii::app()->session['cooladataUserLogin']) AND Yii::app()->session['cooladataUserLogin'] != "") {
            Yii::app()->user->setState("cooladataUserLogin", Yii::app()->session['cooladataUserLogin']);
            unset(Yii::app()->session['cooladataUserLogin']);
        }
        if(isset(Yii::app()->session['cooladataUserRegister']) AND Yii::app()->session['cooladataUserRegister'] != "") {
            Yii::app()->user->setState("cooladataUserRegister", Yii::app()->session['cooladataUserRegister']);
            unset(Yii::app()->session['cooladataUserRegister']);
            unset(Yii::app()->session['cooladataUserLogin']); //!important
        }

        return true;
    }

    /**
     *
     * #abawebapps-190
     *
     * @return bool
     */
    public function initPaymentControlCheckId()
    {
        // Reset payment control check:
        $this->setState('paymentControlCheckId', null);

        return true;
    }

    public function initUserLogin()
    {
        $this->setState('cooladataUserLogin', null);

        return true;
    }

    public function initUserRegister()
    {
        $this->setState('cooladataUserRegister', null);

        return true;
    }

}
?>