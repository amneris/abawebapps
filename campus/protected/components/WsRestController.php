<?php
/**
 * WsRestController is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class WsRestController extends WsRestComController
{
    protected $_format = 'json';

    protected static $aErrorCodes = array(
        200 => 'OK',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Bad signature',
        410 => 'The purchase receipt is not valid',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        506 => 'User already exists',
    );

    static protected $version = "3.4";

    static protected $aHistoryVersion = array("1.0"=> "First release of REST SERVICES",
                    "1.1"=> "Creation of 2 sub-groups of web services, user and register.",
                    "1.2"=> "Creation of new web service to change password.",
                    "1.3"=> "Change password works only with POST method.",
                    "1.4"=> "Parameters name for register and login are used. Order it is used only for signature.",
                    "1.5"=> "Parameters order in signature validation.",
                    "1.6"=> "Change response messages for error and success. Always JSON.",
                    "1.7"=> "Login returns name and last name.",
                    "1.8"=> "New web service recover password.",
                    "1.9"=> "New mobile page for concept Try for Free.",
                    "2.0"=> "New params in register user w.s. Partner id and source id.",
                    "2.1"=> "New returned value in register user, countryNameIso",
                    "2.2"=> "New returned value in login user, countryNameIso",
                    "2.3"=> "ListVideoClass service only returns video-classes above the current level of user",
                    "2.4"=> "New web service ipRecognition. Which will be called from mobiles to find out ip",
                    "2.5"=> "New parameter in the web service register, parameter deviceTypeSource.",
                    "3.2"=> "Added update user FreeToPremium",
                    "3.3"=> "New values returned in register and login methods",
                    "3.4"=> "Added Update Attribution WS"
    );

    /* @var array $reqDataDecoded*/
    protected $reqDataDecoded;
    protected $reqMethod;
    /* @var LogWebService $moLogWs */
    protected $moLogWs;
    /* @var AbaUser $moUser */
    protected $moUser;
    /* @var AbaUserDevice $moDevice */
    protected $moDevice;
    protected $nameService;
    protected $aResponse;
    protected $isSuccess;
    /****************************************************************/
                /*  CUSTOM METHODS, OUTSIDE YII  */
    /****************************************************************/

    /* ^^^^^END OF CUSTOM METHODS, OUTSIDE YII^^^^^ */


    /**
     * @param string $signature
     * @param array $params
     *
     * @return bool
     */
    protected function isValidSignature( $signature, $params )
    {
        if( substr($signature,-3) == "123"  && Yii::app()->config->get("SELF_DOMAIN")!==DOMAIN_LIVE )
        {
            return true ;
        }
        // We have to a check on signature. For example a hash with all arguments
        if( $signature === $this->getSignature($params) ){
            return true;
        }

        return false;
    }

    /** Checks Signature in params received.
     * @param $params
     *
     * @return string
     */
    protected function getSignature( $params )
    {
        $joinParams = implode("", $params);
        $joinParams = HeMixed::getWord4MobileWs().$joinParams;
        $signature = md5( $joinParams );

        return $signature;
    }

    /**
     * Checks authentication, at the moment BASIC DIGEST SIMPLE HTTP
     *
     * @return bool
     */
    protected function checkIfLogined()
    {
        Yii::app()->language = 'en';
        if(intval(Yii::app()->config->get("ENABLE_AUTH_BASIC_RESTSERVICES")) == 1){
            // Check if we have the USERNAME and PASSWORD HTTP headers set?
            if(!(isset($_SERVER['PHP_AUTH_USER']) and isset($_SERVER['PHP_AUTH_PW']))) {
                // Error: Unauthorized
                $this->sendResponse(401, array("status"=>'Unauthorized', 'message'=>Yii::t('yii','Login Required')));
            }

            $username = $_SERVER['PHP_AUTH_USER'];
            $password = $_SERVER['PHP_AUTH_PW'];

            // Find the user
            $moUser = new AbaUser();
            $moUser->getUserByEmail( $username, $password, true, false);
            if($moUser===NULL) {
                // Error: Unauthorized
                $this->sendResponse(401, array( 'status'=>'Error', 'message'=>Yii::t('mainApp','user_unknown')));
                return false;
            }
            else if(!$moUser->validatePassword($password)) {
                // Error: Unauthorized
                $this->sendResponse(401, array( 'status'=>'Error',
                                                'message'=>Yii::t('mainApp', 'La contraseña no es correcta')));
                return false;
            }

            $this->moUser = $moUser;
            return true;
        } else {
            $token = HeUtils::getServerValue('HTTP_PHP_AUTH_TOKEN');
            if (!$token) {
                $this->sendResponse(401, array( 'status'=>'Error',
                                                'message'=>'Token missing'));
                return false;
            }
            // We search the device based on the Token, and from that we obtain the user as well.
            $moDevice = new AbaUserDevice();
            $moUser = new AbaUser();
            if (!$moDevice->getDeviceByToken($token)) {
                if (!$moUser->getUserByKeyExternalLogin($token)) {
                    $this->sendResponse(400, array( 'status'=>'Error', 'message'=>'User or device not found.'));

                    return false;
                } else {
                    $moDevice->getDeviceByUserId($moUser->getId());
                    Yii::app()->user->id = $moUser->getId();
                    $this->moUser = $moUser;
                    $this->moDevice = $moDevice;

                    return true;
                }
            }
            if (!$moUser->getUserById($moDevice->userId)) {
                $this->sendResponse(401, array( 'status'=>'Error', 'message'=>Yii::t('mainApp', 'user_unknown')));
                return false;
            }

            Yii::app()->user->id = $moUser->getId();
            $this->moUser = $moUser;
            $this->moDevice = $moDevice;

            return true;
        }

    }

    /**
     * @param int    $statusCode
     * @param string $body
     * @param string $contentType
     *
     * @return bool
     */
    protected function sendResponse($statusCode = 200, $body = '', $contentType = 'json')
    {
        return parent::sendResponse($statusCode, $body, $contentType);
    }
}