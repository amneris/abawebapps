<?php
/**
 * AbaController is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class AbaPayController extends AbaController
{
    /**
     * @param PaymentForm $modFormPayment
     * @param AbaUser $user
     * @param bool $isChangeProd To find out if user is changing of product or paying.
     * @return PaymentForm
     */
    protected function fillPaymentForm(PaymentForm $modFormPayment, AbaUser $user=NULL, $isChangeProd=false)
    {
        $postPaymentForm = HeMixed::getPostArgs("PaymentForm");

        if ((empty($postPaymentForm)) && !is_null($user)) {
            $modFormPayment->firstName =        $user->name;
            $modFormPayment->secondName =       $user->surnames;
            $modFormPayment->mail =             $user->email;
            $modFormPayment->creditCardName =   $modFormPayment->firstName . " " . $modFormPayment->secondName;
            $modFormPayment->setIdPaySupplier( $user );
            if (intval(Yii::app()->config->get("PAY_DISPLAY_VISA_TEST")) == 1) {
                $modFormPayment->creditCardNumber = Yii::app()->config->get("PAY_VISA_NUMBER_TEST");
            }
            $aRetProdsAndPrices =                   $this->collectAndSelectProducts($user);
            $modFormPayment->productsPricesPlans =  $aRetProdsAndPrices[0];
            $modFormPayment->packageSelected =      $aRetProdsAndPrices[1];
        } else {
            if (!empty($postPaymentForm)) {
                $modFormPayment->attributes = $postPaymentForm;
                $modFormPayment->setIdPaySupplier($user);
            }
        }

        if ($isChangeProd) {
            $moUserCreditForm = new AbaUserCreditForms();
            $moUserCreditForm->getUserCreditFormByUserId($user->id);
            if ($moUserCreditForm->kind == KIND_CC_PAYPAL ||
                $moUserCreditForm->kind == KIND_CC_BOLETO ||
                $moUserCreditForm->kind == KIND_CC_APPSTORE ) {
                // Inconsistency can never happen:
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_LOGIC_L. ' Method not available to change product',
                    HeLogger::IT_BUGS_PAYMENTS,
                    HeLogger::CRITICAL,
                    'A user with PAYPAL active subscription tried to change the plan which is not allowed.'
                );
            }
            $modFormPayment->idPayMethod =      PAY_METHOD_CARD;
            $modFormPayment->creditCardType =   intval($moUserCreditForm->kind);
            $modFormPayment->creditCardNumber = $moUserCreditForm->cardNumber;
            $modFormPayment->cpfBrasil =        $moUserCreditForm->cpfBrasil;
            $modFormPayment->creditCardYear =   $moUserCreditForm->cardYear;
            $modFormPayment->creditCardMonth =  intval($moUserCreditForm->cardMonth);
            $moUserAddress = new AbaUserAddressInvoice();
            if (!$moUserAddress->getUserAddressByUserId( $user->id )){
                $modFormPayment->street =   '-';
                $modFormPayment->state =    '-';
                $modFormPayment->city =     '-';
                $modFormPayment->zipCode =  '-';
            } else {
                $modFormPayment->street =   $moUserAddress->street;
                $modFormPayment->state =    $moUserAddress->state;
                $modFormPayment->city =     $moUserAddress->city;
                $modFormPayment->zipCode =  $moUserAddress->zipCode;
            }
        }

        $modFormPayment->isChangeProd = $isChangeProd;
        return $modFormPayment;
    }

    /** Returns de current Id Partner navigation
     *
     * @return integer
     */
    protected function getIdPartner()
    {
        $idPartner = Yii::app()->config->get("ABA_PARTNERID");
        if (!is_null(Yii::app()->user->getState("idPartnerCurNavigation")) &&
            Yii::app()->user->getState("idPartnerCurNavigation") !== ''
        ) {
            $idPartner = Yii::app()->user->getState("idPartnerCurNavigation");
        }

        return $idPartner;
    }

    /**
     * function to write payment details to data layer class
     * @param $payment object holding payment details
     */
    protected function updateDataLayerPaymentDetails($payment){

        try {
            Yii::app()->dataLayer->setPaymentValue($payment->amountPrice);
            Yii::app()->dataLayer->setCurrency($payment->currencyTrans);
            Yii::app()->dataLayer->setIdProduct($payment->idProduct);
            Yii::app()->dataLayer->setPaySuppExtId($payment->paySuppExtId);
            Yii::app()->dataLayer->setIdPayment($payment->id);
            Yii::app()->dataLayer->setPromoCode($payment->idPromoCode);
            Yii::app()->dataLayer->setDiscount($payment->amountDiscount);
            Yii::app()->dataLayer->setQuantity(1);
            Yii::app()->dataLayer->setProductPlan($payment->idPeriodPay);

        }catch(Exception $e){
            echo 'Excepcion caught writing payment details to data layer: ',  $e->getMessage(), "\n";
        }
    }

    /** Resets the form after a submit and before replying to the client
     * @param PaymentForm $modFormPayment
     * @return PaymentForm
     */
    protected function emptyCardDetailsForPaypal( PaymentForm $modFormPayment)
    {
        $modFormPayment->creditCardType = KIND_CC_PAYPAL;
        $modFormPayment->creditCardNumber = '';
        $modFormPayment->creditCardYear = '';
        $modFormPayment->creditCardMonth = '';
        $modFormPayment->CVC = '';
        $modFormPayment->cpfBrasil = '';
        $modFormPayment->typeCpf = '';
        return $modFormPayment;
    }

    /** Resets the form after a submit and before replying to the client
     * @param PaymentForm $modFormPayment
     * @return PaymentForm
     */
    protected function emptyCardDetailsForBoleto( PaymentForm $modFormPayment)
    {
        $modFormPayment->creditCardType = KIND_CC_BOLETO;
        $modFormPayment->creditCardName = '';
        $modFormPayment->creditCardNumber = '';
        $modFormPayment->creditCardYear = '';
        $modFormPayment->creditCardMonth = '';
        $modFormPayment->CVC = '';
        return $modFormPayment;
    }

    /** Resets the form after a submit and before replying to the client
     * @param PaymentForm $modFormPayment
     * @return PaymentForm
     */
    protected function emptyCardDetailsForCard( PaymentForm $modFormPayment)
    {
        if ($modFormPayment->creditCardType==KIND_CC_BOLETO || $modFormPayment->creditCardType==KIND_CC_PAYPAL || $modFormPayment->creditCardType==KIND_CC_APPSTORE){
            // Just to skip validation. Should be refactored.
            $modFormPayment->creditCardType = KIND_CC_VISA;
        }

        return $modFormPayment;
    }

    /** Return all data related with Boletos requested and valid for the user.
     * @return array
     */
    protected function getAnyLastBoletoPending()
    {
        $boletoPrice = NULL;
        $productBoleto = '';
        $moBoleto = new AbaPaymentsBoleto();
        $moBoleto = $moBoleto->getLastPendingByUserId( $this->user->id );
        if($moBoleto){
            $moPayDraftBoleto = new PaymentControlCheck();
            $moPayDraftBoleto->getPaymentById($moBoleto->idPayment);
            $boletoPrice = HeViews::formatAmountInCurrency( HeMixed::getRoundAmount($moPayDraftBoleto->amountPrice),
                $moPayDraftBoleto->foreignCurrencyTrans) ;
            $moProd = new ProdPeriodicity();
            $moProd = $moProd->getProdPeriodById($moPayDraftBoleto->idPeriodPay);
            $productBoleto = Yii::t('mainApp', $moProd->descriptionText);
            $moProd = NULL;
            $moPayDraftBoleto = NULL;
        }

        return array('moBoleto' => $moBoleto,
            'boletoPrice'=> $boletoPrice,
            'productBoleto'=> $productBoleto);
    }

    /** Whatever the pay supplier for credit card/country, they all should be called through here.
     *
     * @param PaymentForm $modFormPayment
     * @param PaymentControlCheck $paymentControlCheck
     * @return bool
     */
    protected function runCardTransaction( PaymentForm $modFormPayment, PaymentControlCheck $paymentControlCheck, $params=array())
    {
        /*
       * GATEWAY PAYMENT SERVICE CALLS
       * 4. Build HTTP request,
        * 4.1. Insert log request into database.
        * 4.2. Inserts reply From La Caixa/AllPago-BR/AllPago-MX/Adyen into log database
       */
        /* @var PaySupplierLaCaixa|PaySupplierAllPagoBr|PaySupplierAllPagoMx|PaySupplierAdyen|PaySupplierAdyenHpp $paySupplier */
        $paySupplier = PaymentFactory::createPayment($paymentControlCheck);
        $paymentControlCheck->attempts = intval(Yii::app()->user->getState('paymentMethodAttempt'));

        $paySupplier->setOptionalData($params);

        /* @var PayGatewayLogLaCaixaResRec|PayGatewayLogAllpago|PayGatewayLogAdyenResRec $retPaySupplierLog */
        $retPaySupplierLog = $paySupplier->runDebiting($paymentControlCheck);
        if (!$retPaySupplierLog) {
            $paySuppOrderId = $paySupplier->getOrderNumber();
            $errPaymentGateway = strip_tags(Yii::t('mainApp', 'gateway_failure_key')) .
                " (" . $paySupplier->getErrorCode() . "-" . $paySupplier->getErrorString() . ")";
            $paymentControlCheck->updateStatusAfterPayment(PAY_FAIL, $paySupplier->getPaySupplierId(), $paySuppOrderId,
                                                                                $errPaymentGateway);
            $errPaymentGateway = Yii::t('mainApp', 'gateway_failure_key');
            $modFormPayment->addError("PaymentControlProcess", $errPaymentGateway);
            $this->redirPaymentMethod($modFormPayment, false, $this->user);
            return false;
        }

        //#5635
        $payment = $this->runAfterCardTransactionConfirmed( $modFormPayment, $paymentControlCheck, $retPaySupplierLog, $paySupplier);

        if( $payment && $payment instanceof Payment ) {

            //
            //#5635
            $stPaymentInfo = $payment->getPaymentInfoForSelligent();

            //
            //  Send data to Selligent
            $stSelligentDataToSend = array(
              "PROMOCODE" =>      $payment->idPromoCode,
              "ISEXTEND" =>       $payment->isExtend,

              "MONTH_PERIOD" =>   $stPaymentInfo['MONTH_PERIOD'],
              "PAY_DATE_START" => $stPaymentInfo['PAY_DATE_START'],
              "PAY_DATE_END" =>   $stPaymentInfo['PAY_DATE_END'],
              "AMOUNT_PRICE" =>   $stPaymentInfo['AMOUNT_PRICE'],
              "CURRENCY" =>       $stPaymentInfo['CURRENCY'],
              "PRICE_CURR" =>     $stPaymentInfo['PRICE_CURR'],
              "TRANSID" =>        $stPaymentInfo['TRANSID'],
            );

            //
            //#abawebapps-227-4.129.4
            if($payment->isExtend == 1) {
                $stSelligentDataToSend["PRODUCT_TYPE"] = $payment->idPeriodPay;
            }

            $commConnSelligent = new SelligentConnectorCommon();
            $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::pagos, $this->user,
              $stSelligentDataToSend,
                "actionProcessPayment controller");

            //#5087
            $commProducts = new ProductsPricesCommon();
            if($commProducts->isPlanProduct($payment->idProduct)) {
                $commConnSelligent = new SelligentConnectorCommon();
                // IDUSER_WEB, PRODUCT_TYPE
                $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::pagosPlan,
                $this->user, array(), "actionProcessPayment controller");
            }
            else {
                $commConnSelligent = new SelligentConnectorCommon();
                $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::pagosPlanNotification,
                    $this->user, array(), "actionProcessPayment controller");
            }

            // Redirects to welcome Premium page:
            $this->redirConfirmedPayment($this->user, $payment, array("email" => $this->user->email));
            return true;
        }

        $this->redirPaymentMethod($modFormPayment, false, $this->user);
        return false;
    }

    /**
     * @param PaymentForm $modFormPayment
     * @param PaymentControlCheck $paymentControlCheck
     * @param $retPaySupplierLog
     * @param $paySupplier
     * @return mixed
     */
    protected function runAfterCardTransactionConfirmed( PaymentForm $modFormPayment,
                                                         PaymentControlCheck $paymentControlCheck,
                                                         $retPaySupplierLog, $paySupplier ){
        return false;
    }


    /** Whatever the pay supplier for credit card/country, they all should be called through here.
     *
     * @param PaymentForm $modFormPayment
     * @param PaymentControlCheck $paymentControlCheck
     * @return bool|Payment
     */
    protected function runBoletoTransaction( PaymentForm $modFormPayment, PaymentControlCheck $paymentControlCheck)
    {
        // We use DateToPay as a DueDate.
        $boleto = new AbaPaymentsBoleto();
        $paymentControlCheck->dateToPay = $boleto->getDueDate($paymentControlCheck->dateStartTransaction);
        /* @var PaySupplierAllPagoBoleto $paySupplier */
        $paySupplier = PaymentFactory::createPayment($paymentControlCheck);
        $paymentControlCheck->attempts = intval(Yii::app()->user->getState('paymentMethodAttempt'));
        /* @var PayGatewayLogAllpago $retPaySupplierLog */
        $retPaySupplierLog = $paySupplier->runDebiting( $paymentControlCheck,
            PayAllPagoCodes::PAY_BOLETO.'.'.PayAllPagoCodes::OP_PREAUTHORIZATION, false);
        if (!$retPaySupplierLog) {
            $paySuppOrderId = $paySupplier->getOrderNumber();
            $errPaymentGateway = strip_tags(Yii::t('mainApp', 'key_gateway_failure_boleto')) .
                " (" . $paySupplier->getErrorCode() . "-" . $paySupplier->getErrorString() . ")";
            $paymentControlCheck->updateStatusAfterPayment( PAY_FAIL, $paySupplier->getPaySupplierId(), $paySuppOrderId,
                $errPaymentGateway);
            $errPaymentGateway = Yii::t('mainApp', 'key_gateway_failure_boleto');
            $modFormPayment->addError("PaymentControlProcess", $errPaymentGateway);
            $this->redirPaymentMethod($modFormPayment, false, $this->user);
            return false;
        }

        // After Boleto has been requested successfully:
        // Insert Boleto

        $urlBoleto = urldecode($retPaySupplierLog->getUrlBoleto());
        $boleto = $boleto->insertRequest( $paymentControlCheck->id, $paymentControlCheck->paySuppExtId,
            $retPaySupplierLog->getUniqueId(), $paymentControlCheck->dateStartTransaction,
            $paymentControlCheck->dateToPay, $urlBoleto, $retPaySupplierLog->getXmlOrderId() );

        $paymentControlCheck->updateStatusAfterPayment(PAY_PENDING, $retPaySupplierLog->getPaySuppExtId(),
            $retPaySupplierLog->getOrderNumber(), 'Boleto has been requested successfully');
        $paymentControlCheck->updateReviewedLastWeekAttempts($this->user->getId());

        if( $boleto && $boleto instanceof AbaPaymentsBoleto ){
            $moPartner = new Partners();
            $nameGroup = PARTNER_ABA_G;
            if ($moPartner->getPartnerByIdPartner( $paymentControlCheck->idPartner )){
                $nameGroup = $moPartner->nameGroup;
            }

            //  Send data to Selligent
            $commConnSelligent = new SelligentConnectorCommon();
            $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::boletoRequest, $this->user,
                array("PROMOCODE" => $paymentControlCheck->idPromoCode,
                    'URL' => urlencode($boleto->url),
                    'PID' => $boleto->paySuppExtUniId,
                    'REQUESTED_DATE' => $boleto->requestDate,
                    'PRODUCT_TYPE' => $paymentControlCheck->idPeriodPay,
                    'PARTNERT_FIRSTYPE' => $paymentControlCheck->idPartner,
                    'PARTNERT_FIRSTPAY_GROUP' => $nameGroup,
                    'DUE_DATE' => $boleto->dueDate), "actionProcessPayment(Boleto) controller");

            // Redirects to welcome Premium page:
            $this->redirConfirmedBoleto( $paymentControlCheck, $boleto);
            return true;
        }

        return false;
    }


    /** Prepare and start adyen hpp transaction and submit to adyen hosted payment page
     *
     * @param PaymentForm $modFormPayment
     * @param PaymentControlCheck $paymentControlCheck
     *
     * @return bool|Payment
     */
    protected function runAdyenHppTransaction(
      PaymentForm $modFormPayment,
      PaymentControlCheck $paymentControlCheck,
      $optionalParams = array()
    ) {
        /* @var PaySupplierAdyenHpp $paySupplier */
        $paySupplier = PaymentFactory::createPayment($paymentControlCheck);
        $paymentControlCheck->attempts = intval(Yii::app()->user->getState('paymentMethodAttempt'));

        $paySupplier->setOptionalData($optionalParams);

        $adyenSendData = $paySupplier->runDebitingHpp($paymentControlCheck);

        $paySuppExtId = PAY_SUPPLIER_ADYEN_HPP;

        //
        // Pre-Payment Adyen
        $oPaymentAdyen = new AbaPaymentsAdyen();

        $oPaymentAdyen->idPayment = $paymentControlCheck->id;
        $oPaymentAdyen->merchantReference = $adyenSendData['merchantReference'];
        $oPaymentAdyen->status = PAY_ADYEN_PENDING;

        $oPaymentAdyen->isExtend = 0;
        if (isset($optionalParams['isPaymentPlus']) AND $optionalParams['isPaymentPlus'] == 1) {
            $oPaymentAdyen->isExtend = 1;
        }

        $oPaymentAdyen->requestDate = $paymentControlCheck->dateStartTransaction;

        //
        $oPaymentAdyen = $oPaymentAdyen->insertPaymentAdyen();

        if ($oPaymentAdyen && $oPaymentAdyen instanceof AbaPaymentsAdyen) {

            unset(Yii::app()->user->paymentControlCheck);
            unset(Yii::app()->session['paymentControlCheck']);

            Yii::app()->user->setState("paymentMethodAttempt", null);
            Yii::app()->user->setState("paymentControlCheckId", null);
            Yii::app()->user->setState("paymentControlStatus", null);
        }

        //
        $data = array(
          "adyenPostData" => $adyenSendData,
          "paySuppExtId" => $paySuppExtId
        );

        $this->render('processPaymentAdyen', $data);
    }

    /**
     *  Get adyen payment methods for country & currency
     *
     * @param $hppData
     *
     * @return array
     */
    protected function runAdyenHppPaymentMethodsTransaction( $hppData ) {

        $paySupplier = PaymentFactory::createPayment(null, PAY_SUPPLIER_ADYEN_HPP);

        $paySupplier->setOptionalData($hppData);

        /* @var PayGatewayLogAdyenResRec $retPaySupplierLog */
        $retPaySupplierLog =        $paySupplier->getPaymentMethods($hppData);
        $adyenHppPaymentMethods =   $paySupplier->getAllowedMethods();

        return $adyenHppPaymentMethods;
    }

    /** Process adyen HPP response
     *
     * @param PaymentControlCheck $paymentControlCheck
     * @param array $hppData
     *
     * @return bool|Payment
     */
    protected function processAdyenHppTransaction( PaymentControlCheck $paymentControlCheck, $hppData=array() )
    {
        /* @var PaySupplierAdyenHpp $paySupplier */
        $paySupplier =                      PaymentFactory::createPayment(null, PAY_SUPPLIER_ADYEN_HPP);
        $paymentControlCheck->attempts =    intval(Yii::app()->user->getState('paymentMethodAttempt'));

        $paySupplier->setOptionalData($hppData);

        $oPayment = new Payment();

        if($oPayment->getAdyenHppSuccessPayment($paymentControlCheck->userId, $hppData['pspReference'], $paymentControlCheck->id)) {
            return $oPayment;
        }

        return false;
    }

    /**
     * @param $moUser
     * @param $payment
     * @param $isChangePlan
     *
     * @return array
     */
    protected function getConfirmPaymentData(AbaUser $moUser, $payment, $isChangePlan=false) {

        //
        //#5635
        $moPay = new Payment();
        $moPendingPay = false;

        if ($isChangePlan) {
            $moPendingPay = $moPay->getLastPayPendingByUserId($moUser->getId());
        }

        $productsPricesPlans = $this->getAllAvailableProductsPrices($moUser, true);

        $sProductName =     '';
        $sProductMonths =     '';
        $iFinalPrice =      '';

        foreach($productsPricesPlans as $productPrice) {

            if ($isChangePlan AND $moPendingPay) {
                if($productPrice["idProduct"] == $moPendingPay->idProduct) {
                    $sProductName = Yii::t('mainApp', $productPrice['packageTitleKey']);
                    $sProductMonths = $productPrice['monthsPeriod'];
                }
            }            else {
                if($productPrice["idProduct"] == $payment->idProduct) {
                    $sProductName = Yii::t('mainApp', $productPrice['packageTitleKey']);
                    $sProductMonths = $productPrice['monthsPeriod'];
                }
            }

            if($productPrice["idProduct"] == $payment->idProduct) {
                $iFinalPrice = HeViews::formatAmountInCurrency( HeMixed::getRoundAmount($payment->amountPrice), $productPrice['ccyIsoCode'], $productPrice["ccyDisplaySymbol"]);
            }
        }

        //
        if($isChangePlan AND $iFinalPrice == '') {

            if($payment->idPeriodPay == ProdPeriodicity::MONTH_3) {

                $commProducts = new ProductsPricesCommon();
                $productPrice = $commProducts->getProductPriceById($payment->idProduct);

                if($productPrice) {
                    $stCountryData = $commProducts->getCountryAndCurrencyData($payment->idCountry);
                    $iFinalPrice = HeViews::formatAmountInCurrency( HeMixed::getRoundAmount($payment->amountPrice), $stCountryData["ABAIdCurrency"], $stCountryData["symbol"]);
                }
            }
        }

        $dateToPay =    HeDate::europeanToSQL($moUser->expirationDate);
        $sDateToPay =   HeDate::SQLToUserRegionalDate($dateToPay, '-', Yii::app()->user->getCountryId());

        if ($sProductMonths == 1) {
            $sProductMonths = Yii::t('mainApp', 'paymentConfirm_paySummary_type_1month');
        }
        else {
            $sProductMonths = Yii::t('mainApp', 'paymentConfirm_paySummary_type_Xmonth', array('{months}' => $sProductMonths));
        }

        return array(
            'sPaymentConfirmMonths' =>          $sProductMonths,
            'sPaymentConfirmSuscription' =>     $sProductName,
            'sPaymentConfirmDateToRenewal' =>   Yii::t('mainApp', 'payments_resumeRenew', array('{dateToPay}' => $sDateToPay)),
            'sPaymentConfirmPrice' =>           $iFinalPrice,
        );
    }


    /** --------------------------------------------- **/
    /** --------------------------------------------- **/
    /**
     * @param AbaUser $user
     * @param bool|false $bPlanPrice
     * @param string $packageSelected
     * @param bool|false $bPaymentPlus
     *
     * @return array
     */
    protected function getAllAvailableProductsPrices(
      AbaUser $user,
      $bPlanPrice = false,
      $packageSelected = "",
      $bPaymentPlus = false
    ) {

        $userPromoCode = "";
        if (!is_null(Yii::app()->user->getState("promoCode")) && Yii::app()->user->getState("promoCode") !== '') {
            $userPromoCode = Yii::app()->user->getState("promoCode");
        }

        //#5191
        $commProducts = new ProductsPricesCommon();
        $aPackagesProps = $commProducts->getFormattedListProducts($user, $userPromoCode, $bPlanPrice, $packageSelected,
          $bPaymentPlus);

        return $aPackagesProps;
    }

    /**
     * It should be the only Logic for products in the whole controller.
     *
     * @param AbaUser $user
     * @param bool|false $bPlanPrice
     * @param bool|false $bPaymentPlus
     *
     * @return array returns always the list of products for user and also the selected one in the front-end.
     */
    protected function collectAndSelectProducts(AbaUser $user, $bPlanPrice = false, $bPaymentPlus=false)
    {
        $products = $this->getAllAvailableProductsPrices($user, $bPlanPrice, "", $bPaymentPlus);

        $commProducts = new ProductsPricesCommon();
        return $commProducts->collectAndSelectProducts($products);
    }

}
