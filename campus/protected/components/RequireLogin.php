<?php

class RequireLogin extends CBehavior
{

    public function attach($owner)
    {
        $owner->attachEventHandler('onBeginRequest', array($this, 'handleBeginRequest'));
    }


    public function handleBeginRequest($event)
    {
        // We exclude from login some URLs, specially the web service requests that require not front - end Login.
        if( isset($_SERVER['REQUEST_URI']) )
        {
            $aExcludedURLNoLogin = explode('@', URL_WS_NO_LOGIN);
            foreach($aExcludedURLNoLogin as $excludedValue)
            {
                $strPosIn = strpos( strtolower($_SERVER['REQUEST_URI']), strtolower($excludedValue) );
                if( $strPosIn>=1 && $strPosIn<=5 )
                {
                    return true;
                }
            }
        }

        $autol = HeMixed::getGetArgsRawMethod(MAGIC_LOGIN_KEY);
        if ($autol) {
            // not a guest
            if (!Yii::app()->user->isGuest) {
                // same autol than logged in user
                if (Yii::app()->user->getKeyExternalLogin() == $autol) {
                    return true;
                } else {
                    // different autol than logged in user, we logout the current user
                    Yii::app()->user->logout(false);
                }
            }

            // process auto login
            $form = new LoginForm();
            $form->autoLogin($autol);
        }

        if (Yii::app()->user->isGuest) {
            /* @var AbaWebUser Yii::app()->user */
            Yii::app()->user->loginRequired();
        }
    }
}

