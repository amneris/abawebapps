
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 26/09/12
 * Time: 13:11
 * To change this template use File | Settings | File Templates.
 */
class BreadcrumbHeader extends CWidget
{

    public $curMenuSectionTab = "";
    public $msgRight = "";
    public $uppercase = true;

    public function run()
    {
        $this->render('breadcrumbHeader');
    }
}

?>