<?php

class LevelSelector extends CWidget
{
    public $currentLevel = "";
    public $levels = array();

    public function run()
    {
        $this->levels = Yii::app()->params['levels'];
        if (isset($this->currentLevel) && $this->currentLevel != "" && (HeMixed::isValidLevel($this->currentLevel))) {
            Yii::app()->user->setState("currentLevel", $this->currentLevel);
        } elseif (isset(Yii::app()->user->currentLevel)) {
            $this->currentLevel = Yii::app()->user->currentLevel;
        }
        $actionForm = (Yii::app()->getController()->id == 'videoclass') ? Yii::app()->createUrl('/videoclass/index') : Yii::app()->createUrl('/course/index');
        $moAllProgress = new AbaFollowups();
        $porcentageFollowUpCurrentLevel = $moAllProgress->getCurrentLevelProgress(Yii::app()->user->getId(), Yii::app()->user->currentLevel);
        $this->render('levelSelector', array('porcentageFollowUpCurrentLevel' => $porcentageFollowUpCurrentLevel, 'actionForm' => $actionForm));

        //#ABAWEBAPPS-693
        $sCooladata = " var cooladataLevelId = '" . $this->currentLevel . "'; ";
        Yii::app()->clientScript->registerScript(md5($sCooladata), $sCooladata, CClientScript::POS_HEAD);
    }
}
