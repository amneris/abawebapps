<?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/modal.css'); ?>
<span class="cmp_nivelLabel left"><?php echo Yii::t('mainApp', Yii::t('mainApp', 'Estás en el nivel: ')); ?></span>

<?php  echo CHtml::form($actionForm, "post"); ?>
    <div id="level-select" class="left">
        <?php
        echo CHtml::dropDownList('level', $this->currentLevel, $this->levels,
                array(
                    /*'submit' => "",*/
                    'onchange' => "popUpChangeLevel('".Yii::app()->createUrl('modals/CambioDeNivel')."');"
                    /*'onchange' => "popUpChangeLevel('".Yii::app()->createUrl('modals/CambioDeNivel')."')"*/
                )
            );
        ?>
    </div>
<?php echo CHtml::endForm(); ?>

<span class="cmp_progresoLabel left"><?php echo Yii::t('mainApp', Yii::t('mainApp', 'Tu Progreso:')); ?></span>
<div class="containerProgressInLayout left">
    <span class="cmp_progresBarNivel left"></span>
    <span class="cmp_progresBarNivelColor left" style="width: <?php echo (($porcentageFollowUpCurrentLevel*1.11) > 111) ? 111 : ($porcentageFollowUpCurrentLevel*1.11) ?>px"></span>
</div>
<span class="cmp_progresoNumber left"><?php echo ($porcentageFollowUpCurrentLevel > 100) ? $porcentageFollowUpCurrentLevel : $porcentageFollowUpCurrentLevel ?>%</span>
