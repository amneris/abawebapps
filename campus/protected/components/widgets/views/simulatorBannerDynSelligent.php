<?php
$role=Yii::app()->user->refreshAndGetUserType();
?>
<div class="selligent_wrapper">
<?php
if(MAX_ALLOWED_LEVEL > $role)
{
    ?>
    <div class="cmp_anuncioPremium">
        <div class="cmp_TextanuncioPremium">
            <span class="cmp_pasateap1"><?php echo Yii::t('mainApp', 'Desbloquea ABA Premium') ?></span><br />
            <span class="cmp_pasateap2"><?php echo Yii::t('mainApp', 'y disfrutarás de todos los contenidos') ?></span>
        </div>
        <?php
        $divGotoPremium = '<div id="idDivGoPremium" class="cmp_haztePremium">
                            <span class="cmp_pasateaNoBold">'.Yii::t('mainApp', 'head_pasate').'</span>'.
                           '<span class="cmp_pasateaBold">ABA </span>
                            <span class="cmp_pasateaNoBold">'.Yii::app()->params["userType"][HeRoles::getUpperRole($role)].'</span>
                           </div>';
        echo CHtml::link($divGotoPremium, Yii::app()->createUrl(LAND_PAYMENT), array('id'=>'divBtnGoPremium', 'class'=>'bn-pasate'));
        ?>
    </div>
<?php
}
else
{
    ?>
    <div id="divBtnFree" class="cmp_anuncioFree">
        <p class="cmp_PanuncioFree">
            <span class="grey"><?php echo Yii::t('mainApp', '4 de cada 5 empresas piden un ') ?></span><span class="red"><?php echo Yii::t('mainApp', 'nivel alto de inglés') ?></span>
        </p>
    </div>
<?php
}
?>
</div>