<?php  echo CHtml::form(); ?>
    <div id="premiumButtonContainer">
        Por Solo <span class="highlight">9 €</span> al mes accede <span class="highlight">ya</span> a todos los contenidos
            <?php $urlRedirect = Yii::app()->createUrl(LAND_PAYMENT); ?>
            <?php 
                $this->widget('zii.widgets.jui.CJuiButton', array(
                    'buttonType'=>'link',
                    'name'=>'btnABA',
                    'caption'=>'Pasate a ABA '.HeRoles::getLitRole(HeRoles::getUpperRole()),
                    'options'=>array('icons'=>'js:{primary:"ui-icon-newwin"}'),
                    'url' => $urlRedirect
                ));
            ?>
     </div>
<?php echo CHtml::endForm(); ?>
