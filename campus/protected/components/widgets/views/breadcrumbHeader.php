<div class="clear"></div>
<div class="menuCampus left">
    <div class="NombreSeccion left"
         style="<?= $this->uppercase ? 'text-transform:uppercase' : '' ; ?>">
        <?php echo Yii::t('mainApp', $this->curMenuSectionTab) ?>
    </div>
    <div class="flechaUnidad left"></div>
    <span class="breadcrumb-msgRight right"><?php echo $this->msgRight; ?></span>
</div>
<div class="clear"></div>
