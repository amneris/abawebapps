<?php 
class LanguageSelector extends CWidget{
    
    public $currentLanguage;
	
    public function run()
    {
        $availableLanguages = Yii::app()->params->languages;

        $currentLang = "";
        if( Yii::app()->user->hasState('language') )
        {
            $currentLang = Yii::app()->user->getState('language');
        }
        else
        {
    	    $currentLang = Yii::app()->language;
        }



    	if(isset($this->currentLanguage) && $this->currentLanguage!="")
        {
            Yii::app()->user->setState("language", $this->currentLanguage);
    	}
        elseif( $currentLang!=="" )
        {
            $this->currentLanguage = $currentLang;
    	}
        
        $this->render('languageSelector', array('currentLang' => $this->currentLanguage, 'languages'=>$availableLanguages));
        
    }
    
}
?>