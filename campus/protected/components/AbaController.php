<?php

/**
 * AbaController is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class AbaController extends CController
{
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column2';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    /**
     * @var int to specify how many NEW aba moments should the menu display (-1 when the user has no access)
     */
    public $hasAbaMoments = 0;

    public function filters()
    {
        return array(
          array('application.filters.HttpsFilter'),
//          array('application.filters.AutoLoginFilter'),
          array('application.filters.LanguageFilter'),
	        array('application.filters.CORSFilterNoToken')

        );
    }

    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);

        $currentLang = HeMixed::getAutoLangDecision();
        Yii::app()->setLanguage($currentLang);
        //-------------------------------------------------------------------------------------------------------------
    }

    /**
     * function to write user and device details to data layer class
     * @param $user object holding user details
     */
    private function updateDataLayer($user){

        try {
            //get device details
            $device = HeDetectDevice::getDeviceType();
            $device = ($device == 'c') ? 'd' : $device;
            Yii::app()->dataLayer->setDevice($device);
            Yii::app()->dataLayer->setUser($user);
        }catch(Exception $e){
            echo 'Excepcion caught writing user details to data layer: ',  $e->getMessage(), "\n";
        }
    }

    /**
     * function of the Yii controllers lifecycle
     * it is executed before the view is rendered
     */
    protected function beforeAction($action)
    {
        $this->routeRequest();
        // First of all we process all generic params
        if (!$this->processLandingParams()) {
            return false;
        }

        if (Yii::app()->user->isLogged()) {

            // send user details to data layer object
            $this->updateDataLayer(Yii::app()->user);

            Yii::app()->params["isTransRunning"] = false;
            if (!array_key_exists("sessionVarsLoaded",
                Yii::app()->params) || Yii::app()->params['sessionVarsLoaded'] == 0
            ) {
                $user = new AbaUser();
                if (!$user->getUserByEmail(Yii::app()->user->getEmail())) {
                    // Redir to login
                    $this->redirect(array('/site/login'));
                    return;
                }
                $identityUser = new AbaUserIdentity(Yii::app()->user->getEmail(), "");
                $identityUser->initSessionUserVariables($user, false);

                // different splash pages priority
                if (!array_key_exists(Yii::app()->language, Yii::app()->params['languages'])
                    && $action->id !== 'errorLang' && $action->id !== 'logout') {
                    header('Location: /site/errorLang');
                    return;
                } elseif (HeDetectDevice::isAppPromo()) {
                    if (HeMixed::getGetArgs(MAGIC_LOGIN_KEY) || isset($_COOKIE['splash'])) {
                        Yii::app()->user->setState('splashShown', true);
                    }

                    // pages where we cannot show the promo
                    $withoutPromoPages = array('recoverpassword', 'resetpassword');
                    $withoutPromoControllers = array('payments', 'paymentsplus');
                    $withoutPromo = in_array($action->id, $withoutPromoPages) || in_array($action->controller->id,
                        $withoutPromoControllers);
                    if ($withoutPromo) { // when a forced case, remove the session var to show it again later
                        Yii::app()->user->setState('splashShown', false);
                    }
                    if (!Yii::app()->user->getState('splashShown') && !$withoutPromo) {
                        Yii::app()->user->setState('splashShown', true);
                        $splashUri = '/splash/campus/out/app_' . Yii::app()->language . '.html';
                        header('Location: ' . STATIC_PAGES_SITE . $splashUri);
                        return;
                    }
                }
            }

            if (isset($_COOKIE['__utmz'])){

                $pattern = '/^(\d*\.*)*(utm.*)$/';
                preg_match($pattern, $_COOKIE['__utmz'], $matches);
                $utm_str_vars = explode("|",$matches[2]);
                $utm_vars = array();
                foreach ($utm_str_vars AS $utm_var) {
                    list ($utm_key, $utm_val) = explode ("=", $utm_var);
                    $utm_vars[$utm_key] = str_replace("'", '\'', $utm_val);
                }

            }

            $userCommon = new UserCommon();
            $abaMomentsResponse = $userCommon->hasAbaMomentsVisible($user);
            $this->hasAbaMoments = $abaMomentsResponse;

            return true;
        } else {

            return true;
        }
    }

    /**
     * It funnels all generic params the campus web can receive in any landing page and sets it into
     * session variables.
     * @return bool
     */
    protected function processLandingParams()
    {
        /* ANY NEW VARIABLE HERE MUST BE RESET IN initSessionUserVariables() function  */
        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

        // For any URL requested, if a param called "promocode" is set,
        //  then we assign that promocode for the current session and user:
        if (array_key_exists("promocode", $this->actionParams)) {
            $commProducts = new ProductsPricesCommon();
            if ($commProducts->checkExistsPromocode($this->actionParams["promocode"], null, 1, 1, false)) {
                Yii::app()->user->setState("promoCode", $this->actionParams["promocode"]);
            } else {
                // We let it pass through even it does not exist, because we want to inform to user that
                // the promocode is not valid at all.
                Yii::app()->user->setState("promoCode", $this->actionParams["promocode"]);
            }
        }

        if (array_key_exists("idproduct", $this->actionParams)) {
            $commProducts = new ProductsPricesCommon();
            if ($commProducts->checkExistsIdProduct($this->actionParams["idproduct"], 1)) {
                Yii::app()->user->setState("idProduct", $this->actionParams["idproduct"]);
            } else {
                Yii::app()->user->setState("idProduct", null);
            }
        }

        // If idpartner is present is very likely that the request comes from a registration user process. Meaning it
        // comes from the web site.
        if (array_key_exists("idpartner", $this->actionParams)) {
            $moPartner = new Partners();
            if ($moPartner->getPartnerByIdPartner(intval($this->actionParams["idpartner"]))) {
                Yii::app()->user->setState("idPartnerCurNavigation", $this->actionParams["idpartner"]);
            } else {
                Yii::app()->user->setState("idPartnerCurNavigation", null);
            }
        }

        //#5191
        if (array_key_exists("idsource", $this->actionParams)) {
            Yii::app()->user->setState("idSource", $this->actionParams["idsource"]);
        }

        return true;
    }

    /**
     * Function used to redirect user to app if installed on iPhone, otherwise redirects user to page referenced by
     * $_SERVER['REQUEST_URI'] (minus controller/action part).
     */
    private function routeRequest()
    {
        $openApp = isset($_GET['openApp']) ? $_GET['openApp'] : false;
        if ($openApp) {
            $isIOS = HeDetectDevice::isIOS();
            $request_uri = $_SERVER['REQUEST_URI'];
            $originalUrl = $_SERVER['SERVER_NAME'] . $request_uri;
            $redirectUrl = str_replace('&openApp=1', '', $originalUrl);
            if ($isIOS) {
                //   $this->render('//mobile/route', array('url'=>$redirectUrl));
                $this->redirect($this->createUrl('mobile/route?page=' . $redirectUrl));
            }
        }
    }

    /**
     * @param array $params
     *
     * @return bool
     */
    protected function setLandingParams($params = array())
    {
        /* ANY NEW VARIABLE HERE MUST BE RESET IN initSessionUserVariables() function  */
        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

        if (isset($params["promocode"])) {
            Yii::app()->user->setState("promoCode", $params["promocode"]);
        }

        if (isset($params["idsource"])) {
            Yii::app()->user->setState("idSource", $params["idsource"]);
        }

        return true;
    }

    /** Redirects to the confirmation view page.
     * @param AbaUser $user
     */
    protected function redirPremiumTryPayAgain(AbaUser $user)
    {
        $user->refresh();
        $commSelligent = new SelligentConnectorCommon();
        $commSelligent->sendOperationToSelligent(SelligentConnectorCommon::synchroUser, $user,
          array(), "redirPaymentMethod");

        unset(Yii::app()->session['paymentControlCheck']);
        Yii::app()->user->setState("paymentMethodAttempt", null);

        $this->redirect(Yii::app()->createUrl("/site/index"));
        return;
    }
}