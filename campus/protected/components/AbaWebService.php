<?php
class AbaWebService extends CWebService
{

    /**
     * Generates the WSDL as defined by the provider.
     * The cached version may be used if the WSDL is found valid in cache.
     * @return string the generated WSDL
     * @see wsdlCacheDuration
     */
    public function generateWsdl()
    {
        $providerClass=is_object($this->provider) ? get_class($this->provider) : Yii::import($this->provider,true);
        if($this->wsdlCacheDuration>0 && $this->cacheID!==false && ($cache=Yii::app()->getComponent($this->cacheID))!==null)
        {
            $key='Yii.CWebService.'.$providerClass.$this->serviceUrl.$this->encoding;
            if(($wsdl=$cache->get($key))!==false)
                return $wsdl;
        }
        $generator=new AbaWsdlGenerator;
        $wsdl=$generator->generateWsdl($providerClass,$this->serviceUrl,$this->encoding);
        if(isset($key))
            $cache->set($key,$wsdl,$this->wsdlCacheDuration);
        return $wsdl;
    }

}