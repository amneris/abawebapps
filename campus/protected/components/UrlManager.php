<?php

class UrlManager extends CUrlManager
{
    public function createUrl($route, $params = array(), $ampersand = '&')
    {
        /**
         * when asking for "/" Yii::app()->language; is not yet set when we get to this point. As a workaround instead
         * of using Yii::app()->language; we use HeMixed::getAutoLangDecision();
         */
        $appLanguage = HeMixed::getAutoLangDecision(); // Yii::app()->language;
        if (array_key_exists("language", $params)) {
            $appLanguage = $params["language"];
        } elseif (array_key_exists("lang", $params)) {
            $appLanguage = $params["lang"];
        }
        $params["language"] = $appLanguage;

        return parent::createUrl($route, $params, $ampersand);
    }
}

?>
