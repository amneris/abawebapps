<?php
/**
 * CWebServiceAction class file.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright Copyright &copy; 2008-2011 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

/**
 * CWebServiceAction implements an action that provides Web services.
 *
 * CWebServiceAction serves for two purposes. On the one hand, it displays
 * the WSDL content specifying the Web service APIs. On the other hand, it
 * invokes the requested Web service API. A GET parameter named <code>ws</code>
 * is used to differentiate these two aspects: the existence of the GET parameter
 * indicates performing the latter action.
 *
 * By default, CWebServiceAction will use the current controller as
 * the Web service provider. See {@link CWsdlGenerator} on how to declare
 * methods that can be remotely invoked.
 *
 * Note, PHP SOAP extension is required for this action.
 *
 * @property CWebService $service The Web service instance.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @version $Id: CWebServiceAction.php 3426 2011-10-25 00:01:09Z alexander.makarow $
 * @package system.web.services
 * @since 1.0
 */
class AbaWebServiceAction extends CWebServiceAction
{

    /**
     * Creates a {@link CWebService} instance.
     * You may override this method to customize the created instance.
     * @param mixed $provider the web service provider class name or object
     * @param string $wsdlUrl the URL for WSDL.
     * @param string $serviceUrl the URL for the Web service.
     * @return CWebService the Web service instance
     */
    protected function createWebService($provider,$wsdlUrl,$serviceUrl)
    {
        return new AbaWebService($provider,$wsdlUrl,$serviceUrl);
    }
}