<?php
/**
 * AbaMobController is the customized base controller class based on AbaController but we make it public.
 */
class AbaMobController extends AbaController {
    /**
     * Mobile layout only
     */
    public $layout = 'mainMobile';

    public function __construct($id, $module = null) {
        CController::__construct($id, $module);

        /**
         * Este controlador no llama a "super" y entonces la detección del idioma se hace desde el filtro, demasiado tarde.
         * No hemos tenido tiempo para comprobar si llamando a super desde aquí no afectamos al resto de los métodos del
         * controlador.
         *
         * https://abaenglish.atlassian.net/browse/ABAWEBAPPS-599
         */
        $lang = HeMixed::getAutoLangDecision();
        Yii::app()->setLanguage($lang);
    }

    protected function beforeAction($action) {
        if(array_key_exists("lang", $this->actionParams)) {
            $lang = $this->actionParams["lang"];
            if ($lang !== "") {
                Yii::app()->setLanguage($lang);
            }
        }
        return true;
    }

    /**
     * @param string $signature
     * @param array $params
     *
     * @return bool
     */
    protected function isValidSignature($signature, $params) {
        if(substr($signature, -3) == "123" && Yii::app()->config->get("SELF_DOMAIN") !== DOMAIN_LIVE) {
            return true;
        }
        // We have to a check on signature. For example a hash with all arguments
        if($signature === $this->getSignature($params)) {
            return true;
        }
        return false;
    }

    /**
     * Checks Signature in params received.
     * @param $params
     *
     * @return string
     */
    protected function getSignature($params) {
        $joinParams = implode("", $params);
        $joinParams = HeMixed::getWord4MobileWs() . $joinParams;
        $signature = md5($joinParams);
        return $signature;
    }
}