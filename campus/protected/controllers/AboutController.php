<?php

class AboutController extends AbaController
{

    public $data = array();

    public function actionIndex()
    {
        $this->render('index');
    }
    
    public function actionTest()
    {
        Yii::app()->user->getId() === '779' || Yii::app()->user->getId() === '681297' ? '' : die(Yii::t('mainApp', 'no'));
        if(array_key_exists(Yii::app()->request->getParam('idioma'), Yii::app()->params->languages))
            $language2 = Yii::app()->request->getParam('idioma');
        else
            $language2 = Yii::app()->user->getLanguage();
        if($language2 == 'es')
            $language2 = 'fr';
        $this->render('test', array("language2" => $language2));
    }
    
    public function actionTestinverso()
    {
        Yii::app()->user->getId() === '779' || Yii::app()->user->getId() === '681297' ? '' : die(Yii::t('mainApp', 'no'));
        if(array_key_exists(Yii::app()->request->getParam('idioma'), Yii::app()->params->languages))
            $language2 = Yii::app()->request->getParam('idioma');
        else
            $language2 = Yii::app()->user->getLanguage();
        if($language2 == 'fr')
            $language2 = 'es';
        $this->render('testinverso', array("language2" => $language2));
    }
    
    public function actionTestdeduplicaciondeclaves()
    {
        Yii::app()->user->getId() === '779' || Yii::app()->user->getId() === '681297' ? '' : die(Yii::t('mainApp', 'no'));
        if(array_key_exists(Yii::app()->request->getParam('idioma'), Yii::app()->params->languages))
            $language2 = Yii::app()->request->getParam('idioma');
        else
            $language2 = Yii::app()->user->getLanguage();
        $this->render('testdeduplicaciondeclaves', array("language2" => $language2));
    }
    
    public function actionTraduccionesJS()
    {
        if(Yii::app()->request->getParam('string'))
            $string = Yii::app()->request->getParam('string');
        else
            die();
        $this->renderPartial('traduccionesJS', array("string" => $string));
    }

    public function actionExportarTraducciones(){
        Yii::app()->user->getId() === '779' || Yii::app()->user->getId() === '681297' ? '' : die(Yii::t('mainApp', 'no'));
        $language2 = "menu";
        if(array_key_exists(Yii::app()->request->getParam('idioma'), Yii::app()->params->languages)){
            $language2 = Yii::app()->request->getParam('idioma');
            $this->renderPartial("exportarTraducciones", array("language2" => $language2));
        }
        else{
            $this->render("exportarTraducciones", array("language2" => $language2));
        }
    }

    public function actionTestdenotraduccidos()
    {
        Yii::app()->user->getId() === '779' || Yii::app()->user->getId() === '681297' ? '' : die(Yii::t('mainApp', 'no'));
        if(array_key_exists(Yii::app()->request->getParam('idioma'), Yii::app()->params->languages))
            $language2 = Yii::app()->request->getParam('idioma');
        else
            $language2 = Yii::app()->user->getLanguage();
        if($language2 == 'es')
            $language2 = 'fr';
        $this->render('testdenotraduccidos', array("language2" => $language2));
    }
}