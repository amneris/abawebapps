<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 28/06/13
 * Time: 11:03
 * To change this template use File | Settings | File Templates.
 *
 * We allow public access to all web services in this class, It is expected that this services are
 * called from mobile devices.
 *
 */
class WsrestregisterController extends WsRestController
{
    /**
     * Default response format
     * either 'json' or 'xml'
     */
    protected $_format = 'json';

    /** We allow public access to all web services in this class.
     * Even so, it is true that Login service can be called with token authentication, but it is encapsulated
     * within Login function scope.
     *
     * @return bool
     */
    protected function checkIfLogined()
    {
        return true;
    }

    /** Public controller of Rest View type calls. It distributes to the rest of the
     *  sub views.
     *
     * @return bool
     */
    public function actionView()
    {
        switch (strtoupper($this->reqDataDecoded['object'])) {
            case 'VERSION':
                $this->viewVersion();

                return true;
                break;
            case 'IPRECOGNITION':
                $this->viewIpRecognition();

                return true;
                break;
            default:
                return parent::actionView();
                break;
        }
    }

    /** http://wcampus.local/wsrestregister/view/object/version
     *  Just to obtain the version of the Rest services and associated history.
     */
    protected function viewVersion()
    {
        $this->sendResponse(
            200,
            array('versionNumber' => self::$version, "history" => self::$aHistoryVersion),
            $this->_format
        );
    }

    /**
     * Method used by Mobile Apps to identify IP of device
     *
     * URI: wsrestregister/view/object/ipRecognition
     */
    protected function viewIpRecognition()
    {
        $ipClient = Yii::app()->request->getUserHostAddress();
        $this->sendResponse(200, array('publicIp' => $ipClient), $this->_format);
    }

    /** Checks login of user, and also will generate the Token for temporary use
     * in the mobile device. Also used to refresh sensitive info of the mobile user.
     * The call to this web service is accepted with or without parameters by POST. If no params
     * are received, then a TOKEN by HTTP HEADER will be expected.
     * Parameters expected by POST are:
     *  string $signature
     *  string $deviceId
     *  string $sysOper
     *  string $deviceName
     *  string $email
     *  string $password
     *
     * @return bool
     */
    public function actionLogin()
    {
        Yii::app()->language = 'en';
        // Light security measure to avoid DOS attacks.
        HeMixed::securePublicUrlRequest("wsrestregister/login");
        $wToken = false;
        $signature = '';
        try {
            /*  Validation of parameters received or TOKEN */
            $validArray = array_intersect_key(array(
                'signature'  => 1,
                'deviceId'   => 2,
                'sysOper'    => 3,
                'deviceName' => 4,
                'email'      => 5,
                'password'   => 6
            ), $this->reqDataDecoded);
            if (!is_array($this->reqDataDecoded) || sizeof($validArray) !== 6) {
                // It means that no params are received, so we expect a token to be delivered instead.
                if (!parent::checkIfLogined()) {
                    return false;
                } else {
                    $wToken = $this->moDevice->token;
                }
            }
            if (!$wToken) {
                $signature = $this->reqDataDecoded["signature"];
                $deviceId = $this->reqDataDecoded["deviceId"];
                $sysOper = $this->reqDataDecoded["sysOper"];
                $deviceName = $this->reqDataDecoded["deviceName"];
                $email = $this->reqDataDecoded["email"];
                $password = $this->reqDataDecoded["password"];
                $appVersion = null;
                if (array_key_exists("appVersion", $this->reqDataDecoded)) {
                    $appVersion = $this->reqDataDecoded["appVersion"];
                }
                if (!$this->isValidSignature(
                    $signature,
                    array($deviceId, $sysOper, $deviceName, $email, $password, $appVersion)
                )
                ) {
                    $this->sendResponse(405);

                    return false;
                }
                $userIdentity = new AbaUserIdentity($email, $password);
                $userIdentity->authenticate();
                switch ($userIdentity->errorCode) {
                    case AbaUserIdentity::ERROR_NONE:
                        // Continues right
                        break;
                    case AbaUserIdentity::ERROR_UNKNOWN_IDENTITY:
                    case AbaUserIdentity::ERROR_NO_ACCESS:
                    case AbaUserIdentity::ERROR_USERNAME_INVALID:
                        $this->sendResponse(401, array(
                            'status'  => 'Error',
                            'message' => Yii::t('mainApp', 'user_unknown')
                        ));

                        return false;
                        break;
                    case AbaUserIdentity::ERROR_PASSWORD_INVALID:
                        $this->sendResponse(401, array(
                            'status'  => 'Error',
                            'message' => Yii::t('mainApp', 'La contraseña no es correcta')
                        ));

                        return false;
                        break;
                    default:
                        $this->sendResponse(401, array(
                            'status'  => 'Error',
                            'message' => Yii::t('mainApp', 'user_unknown')
                        ));

                        return false;
                        break;
                }
                // Find the user
                $moUser = new AbaUser();
                $moUser->getUserByEmail($email, $password, true, false);
                $this->moUser = $moUser;
                $this->moDevice = new AbaUserDevice();
                if (!$this->moDevice->getDeviceByIdUserId($deviceId, $moUser->id)) {
                    $commUser = new RegisterUsersCommon();
                    $wToken = $commUser->registerNewDevice($deviceId, $sysOper, $deviceName, $moUser, $appVersion);
                    if (!$wToken) {
                        $this->sendResponse(500, array(
                            'status'  => 'Error',
                            'message' => 'Your user was created, ' .
                                'but your device could not be registered.'
                        ));

                        return false;
                    }
                } else {
                    $wToken = $this->moDevice->token;
                }
            }
            if ($this->moUser === null) {
                // Error: Unauthorized
                $this->sendResponse(401, array('status' => 'Error', 'message' => Yii::t('mainApp', 'user_unknown')));

                return false;
            }
            $moCountry = new CmAbaCountry($this->moUser->countryId);
            $isoCodeCountry = $moCountry->iso;
            $teacher = new AbaTeacher($this->moUser->teacherId);
            $teacherMobileImage = '';
            $headerDevice = HeUtils::getServerValue('DEVICE');
            if ($headerDevice && $headerDevice !== '') {
                $deviceRule = new DeviceRules();
                $teacherMobileImage = $deviceRule->getMobileImageByDevice($headerDevice, $teacher);
            }
            //#5315
            $idPartnerSource = $this->moUser->idPartnerSource;
            $sPartner = "";

            $moPartner = new Partners();
            if ($moPartner->getPartnerById($idPartnerSource)) {
                $sPartner = $moPartner->name;
            }

            //
            // Return profiles
            $stProfiles = CmUserRules::getOrCreateUserExperimentProfilesWithType($this->moUser,
              ExperimentsTypes::EXPERIMENT_TYPE_MOBILE_STYLE_CSS, false);

            /** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
            $productsPricesCommon = new ProductsPricesCommon();
            $idPeriodPay = $productsPricesCommon->getUserProductPeriodPay($this->moUser);
            $this->sendResponse(
                200,
                array(
                    'userId'         => $this->moUser->getId(),
                    'userType'       => $this->moUser->userType,
                    'userLevel'      => $this->moUser->currentLevel,
                    'token'          => $wToken,
                    'name'           => $this->moUser->name,
                    'surnames'       => is_null($this->moUser->surnames) ? '' : $this->moUser->surnames,
                    'email'          => $this->moUser->email,
                    // TODO: HACK to fix Bug#4979, remove when iOS app can handle null values
                    'countryNameIso' => $isoCodeCountry,
                    'userLang'       => $this->moUser->langEnv,
                    'teacherId'      => $this->moUser->teacherId,
                    'teacherName'    => $teacher->name,
                    'teacherImage'   => $teacherMobileImage == '' ? $teacher->mobileImage2x : $teacherMobileImage,
                    'expirationDate' => $this->moUser->expirationDate,
                    'idPeriod'       => $idPeriodPay,
                    'idPartner'      => $sPartner,
                    'idSource'       => is_null($this->moUser->idSourceList) ? "0" : $this->moUser->idSourceList,
                    'gender'         => (trim($this->moUser->gender) != '' ? AbaUser::getGenderDescription($this->moUser->gender) : ''),
                    'birthdate'      => (trim($this->moUser->birthDate) != '' ? HeDate::europeanToSQL($this->moUser->birthDate, false, "-") : ''),
                    'phone'          => (trim($this->moUser->telephone) != '' ? $this->moUser->telephone : ''),
                    'externalkey'    => (trim($this->moUser->keyExternalLogin) != '' ? $this->moUser->keyExternalLogin : ''),
                    'profiles'       => $stProfiles,
                ),
                $this->_format
            );

            return true;
        } catch (Exception $ex) {
            error_log($signature . " error=> " . $ex->getMessage());

            return $this->sendResponse(
                500,
                array(
                    'status'  => 'Exception',
//                    'message' => "Internal error. Check with Aba IT Team and see logs Apache for " . $signature
                    'message' => $ex->getMessage()
                )
            );
        }
    }

    /**
     * Parameters expected by POST
     * $signature, $deviceId, $sysOper, $deviceName, $email, $password, $langEnv, $name, $surnames, $ip,
     *      $idPartner, $idSourceList, $deviceTypeSource, $appVersion
     *
     * @return bool/JSON array
     */
    public function actionRegister()
    {
        Yii::app()->language = 'en';
        // Light security measure to avoid DOS attacks.
        HeMixed::securePublicUrlRequest("wsrestregister/register");
        $signature = '';
        try {
            /*  Validation of parameters received */
            $validArray = array_intersect_key(
                array(
                'signature'        => 1,
                'deviceId'         => 2,
                'sysOper'          => 3,
                'deviceName'       => 4,
                'email'            => 5,
                'password'         => 6,
                'langEnv'          => 7,
                'name'             => 8,
                'surnames'         => 9,
                'ipMobile'         => 10,
                'idPartner'        => 11,
                'idSourceList'     => 12,
                'deviceTypeSource' => 13,
                'appVersion'       => 14
                ),
                $this->reqDataDecoded
            );
            if (!is_array($this->reqDataDecoded) || sizeof($validArray) < 10 || sizeof($validArray) > 14) {
                $this->sendResponse(400, array(
                    'status'  => 'Error',
                    'message' => 'Bad params received. Try again with the proper params.
                                                                Bear in mind that parameters are case sensitive.'
                ));

                return false;
            }
            $signature = $this->reqDataDecoded["signature"];
            $deviceId = $this->reqDataDecoded["deviceId"];
            $sysOper = $this->reqDataDecoded["sysOper"];
            $deviceName = $this->reqDataDecoded["deviceName"];
            $email = $this->reqDataDecoded["email"];
            $password = $this->reqDataDecoded["password"];
            $langEnv = $this->reqDataDecoded["langEnv"];
            $name = $this->reqDataDecoded["name"];
            $surnames = $this->reqDataDecoded["surnames"];
            $ipMobile = $this->reqDataDecoded["ipMobile"];
            $idPartner = null;
            $idSourceList = null;
            $deviceTypeSource = null;
            $appVersion = null;
            if (array_key_exists("idPartner", $this->reqDataDecoded)) {
                $idPartner = $this->reqDataDecoded["idPartner"];
            }
            if (array_key_exists("idSourceList", $this->reqDataDecoded)) {
                $idSourceList = $this->reqDataDecoded["idSourceList"];
            }
            if (array_key_exists("deviceTypeSource", $this->reqDataDecoded)) {
                $deviceTypeSource = $this->reqDataDecoded["deviceTypeSource"];
            }
            if (array_key_exists("appVersion", $this->reqDataDecoded)) {
                $appVersion = $this->reqDataDecoded["appVersion"];
            }
            /* deviceId, sysOper, deviceName, email, password langEnv name surnames ipMobile: */
            if (!$this->isValidSignature($signature, array(
                $deviceId,
                $sysOper,
                $deviceName,
                $email,
                $password,
                $langEnv,
                $name,
                $surnames,
                $ipMobile,
                $idPartner,
                $idSourceList,
                $deviceTypeSource,
                $appVersion
            ))
            ) {
                $this->sendResponse(405);

                return false;
            }
            if (ip2long($ipMobile) === false) {
                $this->sendResponse(400, array("status" => 'Error', 'message' => 'IP not valid.'));

                return false;
            }
            $idCountry = Yii::app()->config->get('ABACountryId');
            $moCountry = new CmAbaCountry();
            if ($moCountry->getCountryCodeByIP($ipMobile)) {
                $idCountry = $moCountry->getId();
            }
            if (empty($idPartner)) {
                $idPartner = PARTNER_ID_MOBILE;
            }
            if (empty($deviceTypeSource)) {
                $deviceTypeSource = DEVICE_SOURCE_MOBILE;
            }
            $rulerWs = new RegisterUserService();
            $moUser = $rulerWs->registerUser(
                $email,
                $password,
                $langEnv,
                $idCountry,
                $name,
                $surnames,
                1,
                $idPartner,
                $idSourceList,
                $deviceTypeSource
            );
            if ($moUser) {
                $commUser = new UserCommon();
                $commUser->registerGeoLocation($moUser, $ipMobile);
                $commUser = new RegisterUsersCommon();
                $token = $commUser->registerNewDevice($deviceId, $sysOper, $deviceName, $moUser, $appVersion);
                if (!$token) {
                    $this->sendResponse(500, array(
                        'status'  => 'Error',
                        'message' => 'Your user was registered, ' .
                            'but your device could not be registered.'
                    ));

                    return false;
                }
                $teacher = new AbaTeacher($moUser->teacherId);
                $teacherMobileImage = '';
                $headerDevice = HeUtils::getServerValue('DEVICE');
                if($headerDevice) {
                    $deviceRule = new DeviceRules();
                    $teacherMobileImage = $deviceRule->getMobileImageByDevice($headerDevice, $teacher);
                }

                //
                // Create & return profiles
                $stProfiles = CmUserRules::getOrCreateUserExperimentProfilesWithType($moUser,
                  ExperimentsTypes::EXPERIMENT_TYPE_MOBILE_STYLE_CSS, true);

                $this->sendResponse(
                    200,
                    array(
                        'userId'         => $moUser->getId(),
                        'userType'       => $moUser->userType,
                        'userLevel'      => $moUser->currentLevel,
                        'token'          => $token,
                        'countryNameIso' => $moCountry->iso,
                        'userLang'       => $moUser->langEnv,
                        'teacherId'      => $moUser->teacherId,
                        'teacherName'    => $teacher->name,
                        'teacherImage'   => $teacherMobileImage == '' ? $teacher->mobileImage2x : $teacherMobileImage,
                        'expirationDate' => $moUser->expirationDate,
                        'idPartner'      => $moUser->idPartnerSource,
                        'idSource'       => is_null($moUser->idSourceList) ? "0" : $moUser->idSourceList,
                        'profiles'       => $stProfiles,
                    ),
                    $this->_format
                );

                return true;
            } else {
                $this->sendResponse(500, array(
                    'status'  => 'Error',
                    'message' => 'Your user could not be registered successfully.'
                ));

                return false;
            }
        } catch (Exception $ex) {
            error_log($signature . " error=> " . $ex->getMessage());

            return $this->sendResponse(
                500,
                array(
                    'status'  => 'Exception',
                    'message' => "Internal error at register. Check with Aba IT Team and see logs Apache for " .
                        $signature
                )
            );
        }
    }

    /** By POST, expect email and language. Changes password and sends email
     * @return bool
     */
    public function actionRecoverPassword()
    {
        Yii::app()->language = 'en';
        // Light security measure to avoid DOS attacks.
        HeMixed::securePublicUrlRequest("wsrestregister/recoverpassword");
        $signature = '';
        try {
            /*  Validation of parameters received */
            $validArray = array_intersect_key(
                array('signature' => 1, 'email' => 2, 'langEnv' => 3),
                $this->reqDataDecoded
            );
            if (!is_array($this->reqDataDecoded) || sizeof($validArray) !== 3) {
                $this->sendResponse(400, array(
                    'status'  => 'Error',
                    'message' => 'Bad params received. Try again with the proper params.
                                                                Bear in mind that parameters are case sensitive.'
                ));

                return false;
            }
            $signature = $this->reqDataDecoded["signature"];
            $email = $this->reqDataDecoded["email"];
            $langEnv = $this->reqDataDecoded["langEnv"];
            if (!$this->isValidSignature($signature, array($email, $langEnv))) {
                $this->sendResponse(405);

                return false;
            }
            Yii::app()->language = $langEnv;

            //
            //#5766
            //
            $commUser = new UserCommon();
            if($commUser->sendRecoverPasswordEmail($email)) {
                $this->sendResponse(
                    200,
                    array('status' => "success"),
                    $this->_format
                );
                return true;
            }
            else {
                $this->sendResponse(500, array(
                        'status'  => 'Error',
                        'message' => $commUser->getErrMessage()
                    ));
                return false;
            }

//            $commUser = new UserCommon();
//            if (!$commUser->recoverPassword($email, "WS", "", true)) {
//                $this->sendResponse(500, array(
//                    'status'  => 'Error',
//                    'message' => $commUser->getErrMessage()
//                ));
//
//                return false;
//            } else {
//                $this->sendResponse(
//                    200,
//                    array('status' => "success"),
//                    $this->_format
//                );
//
//                return true;
//            }
        } catch (Exception $ex) {
            error_log($signature . " error=> " . $ex->getMessage());

            return $this->sendResponse(
                500,
                array(
                    'status'  => 'Exception',
                    'message' => "Internal error non identified at actionRecoverPassword.
                                    Check with Aba IT Team and see logs Apache for " . $signature
                )
            );
        }
    }

    /**
     * @return bool
     */
    public function actionRegisterFacebook()
    {
        Yii::app()->language = 'en';
        // Light security measure to avoid DOS attacks.
        HeMixed::securePublicUrlRequest("wsrestregister/registerfacebook");

        if (Yii::app()->config->get("FB_APP_LOGIN_ENABLED") == 0) {
            return $this->sendResponse(500, array(
                'status'  => 'Exception',
                'message' => "Internal error non identified at actionRegister. Check with Aba IT Team"
            ));
        }

        $wToken = false;
        $signature = '';
        $isnewuser = 0;
        $stProfiles = array();

        try {
            /*  Validation of parameters received */
            $validArray = array_intersect_key(
                array(
                'signature'        => 1,
                'email'            => 2,
                'name'             => 3,
                'surnames'         => 4,
                'fbId'             => 5,
                'gender'           => 6,
                'langEnv'          => 7,
                'deviceId'         => 8,
                'sysOper'          => 9,
                'deviceName'       => 10,
                'ipMobile'         => 11,
                'idPartner'        => 12,
                'idSourceList'     => 13,
                'deviceTypeSource' => 14,
                'appVersion'       => 15
                ),
                $this->reqDataDecoded
            );

            if (!is_array($this->reqDataDecoded) || sizeof($validArray) < 11 || sizeof($validArray) > 15) {
                // It means that no params are received, so we expect a token to be delivered instead.
                if (!parent::checkIfLogined()) {
                    $this->sendResponse(
                        400,
                        array(
                        'status'  => 'Error',
                        'message' => 'Bad params received. Try again with the proper params.
                                                                Bear in mind that parameters are case sensitive.'
                        )
                    );

                    return false;
                } else {
                    $wToken = $this->moDevice->token;
                }
            }
            $ipMobile = $this->reqDataDecoded["ipMobile"];
            $idCountry = HeMixed::getCountryId($ipMobile);
            if (!$wToken) {
                $signature = $this->reqDataDecoded["signature"];
                $email = $this->reqDataDecoded["email"];
                $name = $this->reqDataDecoded["name"];
                $surnames = $this->reqDataDecoded["surnames"];
                $fbId = $this->reqDataDecoded["fbId"];
                $gender = $this->reqDataDecoded["gender"];
                $langEnv = $this->reqDataDecoded["langEnv"];
                $deviceId = $this->reqDataDecoded["deviceId"];
                $sysOper = $this->reqDataDecoded["sysOper"];
                $deviceName = $this->reqDataDecoded["deviceName"];
                $idPartner = null;
                $idSourceList = null;
                $deviceTypeSource = null;
                $appVersion = null;
                if (array_key_exists("idPartner", $this->reqDataDecoded)) {
                    $idPartner = $this->reqDataDecoded["idPartner"];
                }
                if (array_key_exists("idSourceList", $this->reqDataDecoded)) {
                    $idSourceList = $this->reqDataDecoded["idSourceList"];
                }
                if (array_key_exists("deviceTypeSource", $this->reqDataDecoded)) {
                    $deviceTypeSource = $this->reqDataDecoded["deviceTypeSource"];
                }
                if (array_key_exists("appVersion", $this->reqDataDecoded)) {
                    $appVersion = $this->reqDataDecoded["appVersion"];
                }
                if (empty($fbId) || trim($fbId) == '' || $fbId == 0 || !is_numeric($fbId)) {
                    $this->sendResponse(400, array("status" => 'Error', 'message' => 'Facebook ID not valid.'));

                    return false;
                }

                /* deviceId, sysOper, deviceName, email, password langEnv name surnames ipMobile: */
                if (!$this->isValidSignature($signature, array(
                    $email,
                    $name,
                    $surnames,
                    $fbId,
                    $gender,
                    $langEnv,
                    $deviceId,
                    $sysOper,
                    $deviceName,
                    $ipMobile,
                    $idPartner,
                    $idSourceList,
                    $deviceTypeSource,
                    $appVersion
                ))
                ) {
                    $this->sendResponse(405);

                    return false;
                }
                if (ip2long($ipMobile) === false) {
                    $this->sendResponse(400, array("status" => 'Error', 'message' => 'IP not valid.'));

                    return false;
                }
                if (empty($idPartner)) {
                    $idPartner = PARTNER_ID_MOBILE;
                }
                if (empty($deviceTypeSource)) {
                    $deviceTypeSource = DEVICE_SOURCE_MOBILE;
                }
                $userCommon = new UserCommon();
                $rulerWs = new RegisterUserService();
                $commUser = new RegisterUsersCommon();
                $this->moUser = $userCommon->checkIfFbUserExists($fbId);
                if ($this->moUser) {
                    $this->moDevice = new AbaUserDevice();
                    if (!$this->moDevice->getDeviceByIdUserId($deviceId, $this->moUser->id)) {
                        $wToken = $commUser->registerNewDevice(
                            $deviceId,
                            $sysOper,
                            $deviceName,
                            $this->moUser,
                            $appVersion
                        );
                        if (!$wToken) {
                            $this->sendResponse(
                                500,
                                array(
                                    'status'  => 'Error',
                                    'message' => 'Your user was created, but your device could not be registered.'
                                )
                            );

                            return false;
                        }
                    } else {
                        $wToken = $this->moDevice->token;
                    }
                } else {
                    $currentLevel = 1;
                    $abaExistingUser = new AbaUser();
                    $bUserExists = $abaExistingUser->getUserByEmail($email);
                    $this->moUser = $rulerWs->registerUserFacebook(
                        $email,
                        $name,
                        $surnames,
                        $fbId,
                        $gender,
                        $langEnv,
                        $idCountry,
                        $idPartner,
                        $idSourceList,
                        $deviceTypeSource,
                        $currentLevel
                    );
                    if ($this->moUser) {
                        if (!$bUserExists) {
                            $isnewuser = 1;

                            //
                            // Create & Return profiles
                            $stProfiles = CmUserRules::getOrCreateUserExperimentProfilesWithType($this->moUser,
                              ExperimentsTypes::EXPERIMENT_TYPE_MOBILE_STYLE_CSS, true);
                        }
                        $commUser = new UserCommon();
                        $commUser->registerGeoLocation($this->moUser, $ipMobile);
                        $commUser = new RegisterUsersCommon();
                        $wToken = $commUser->registerNewDevice(
                            $deviceId,
                            $sysOper,
                            $deviceName,
                            $this->moUser,
                            $appVersion
                        );
                        if (!$wToken) {
                            $this->sendResponse(
                                500,
                                array(
                                    'status'  => 'Error',
                                    'message' => 'Your user was registered, but your device could not be registered.'
                                )
                            );

                            return false;
                        }
                    } else {
                        $this->sendResponse(
                            500,
                            array('status' => 'Error', 'message' => 'Your user could not be registered succesfully.')
                        );

                        return false;
                    }
                }
            }

            if ($this->moUser === null) {
                // Error: Unauthorized
                $this->sendResponse(401, array('status' => 'Error', 'message' => Yii::t('mainApp', 'user_unknown')));

                return false;
            }
            $moCountry =        new AbaCountry($this->moUser->countryId);
            $isoCodeCountry =   $moCountry->iso;
            $teacher =              new AbaTeacher($this->moUser->teacherId);
            $teacherMobileImage =   '';
            $headerDevice = $headerDevice = HeUtils::getServerValue('DEVICE');
            if($headerDevice) {
                $deviceRule = new DeviceRules();
                $teacherMobileImage = $deviceRule->getMobileImageByDevice($headerDevice, $teacher);
            }
            $productsPricesCommon = new ProductsPricesCommon();
            $idPeriodPay = $productsPricesCommon->getUserProductPeriodPay($this->moUser);

            //
            // Return profiles
            $stProfiles = CmUserRules::getOrCreateUserExperimentProfilesWithType($this->moUser,
              ExperimentsTypes::EXPERIMENT_TYPE_MOBILE_STYLE_CSS, false);

            $this->sendResponse(
                200,
                array(
                    'status'         => 'OK',
                    'token'          => $wToken,
                    'userId'         => $this->moUser->getId(),
                    'userType'       => $this->moUser->userType,
                    'userLevel'      => $this->moUser->currentLevel,
                    'name'           => $this->moUser->name,
                    'surnames'       => $this->moUser->surnames,
                    'countryNameIso' => $isoCodeCountry,
                    'userLang'       => $this->moUser->langEnv,
                    'teacherId'      => $this->moUser->teacherId,
                    'teacherName'    => $teacher->name,
                    'teacherImage'   => $teacherMobileImage == '' ? $teacher->mobileImage2x : $teacherMobileImage,
                    'expirationDate' => $this->moUser->expirationDate,
                    'idPeriod'       => $idPeriodPay,
                    'isnewuser'      => $isnewuser,
                    'idPartner'      => $this->moUser->idPartnerSource,
                    'idSource'       => is_null($this->moUser->idSourceList) ? "0" : $this->moUser->idSourceList,
                    'profiles'       => $stProfiles,
                ),
                $this->_format
            );

            return true;
        } catch (Exception $ex) {
            error_log($signature . " error=> " . $ex->getMessage());

            return $this->sendResponse(
                500,
                array(
                    'status'  => 'Exception',
                    'message' => "Internal error non identified at actionRegister. " .
                        "Check with Aba IT Team and see logs Apache for " . $signature
                )
            );
        }
    }

    /**
     * Webservice called by Adjust to update Attribution data (idPartnerSource & idSourceList). Will only update if
     * existing idPartnerSource is either 400004/400005(iOS phone/tablet) or 400087(Android)
     *
     * URI: /wsrestregister/updateattribution/?idUser=VALUE&idPartner=VALUE&idSource=VALUE&signature=VALUE
     * (Optional URI parameter: &idSource=VALUE)
     * @return bool/json response
     */
    public function actionUpdateattribution()
    {
        HeMixed::securePublicUrlRequest("wsrestregister/updateattribution");
        $idUser = isset($_GET['user_id']) ? $_GET['user_id'] : null;
        $idPartner = isset($_GET['idPartner']) ? $_GET['idPartner'] : null;
        $idSource = isset($_GET['idSource']) ? $_GET['idSource'] : null;
        $signature = isset($_GET['signature']) ? $_GET['signature'] : null;
        if (is_null($idUser) || intval($idPartner) == 0 || is_null($idPartner) || is_null($signature)) {
            $this->sendResponse(
                400,
                array(
                    'status'  => 'Error',
                    'message' => "Param missing or null. Request URI must be on the form: ".
                        "/wsrestregister/updateattribution/?idUser=VALUE&idPartner=VALUE&signature=VALUE".
                        "(With optional &idSource=VALUE)"
                ),
                $this->_format
            );

            return false;
        }
        if ($signature !== Yii::app()->config->get("ADJUST_ATTRIBUTION_SIGNATURE")) {
            $this->sendResponse(405, array('status' => 'Error', 'message' => "Incorrect signature"), $this->_format);

            return false;
        }
        $user = AbaUser::model('AbaUser')->findByPk($idUser);
        $comUser = new UserCommon();
        if ($comUser->updateAttribution($user, $idPartner, $idSource)) {
            $commSelligent = new SelligentConnectorCommon();
            $commSelligent->sendOperationToSelligent(
                SelligentConnectorCommon::synchroUser,
                $user,
                array(),
                "Wsrestregister Update attribution"
            );
            $this->sendResponse(200, array('status' => 'OK', "Message" => "Attribution updated"), $this->_format);
        } else {
            $this->sendResponse(
                400,
                array('status' => 'Error', "Message" => "Attribution could not be updated"),
                $this->_format
            );
        }
    }
}