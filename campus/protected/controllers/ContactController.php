<?php

class ContactController extends AbaController
{

    public $data = array();

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $user = new AbaUser();
        $user->getUserById(Yii::app()->user->id);

        $isB2B = Yii::app()->user->isB2B();

        if($isB2B){
            $formContact = new ContactForm(true);
        }
        else {
            $formContact = new ContactForm();
        }
        $this->renderForm($formContact,"",array('typeUser'=>$isB2B));
    }


    public function actionProcessContactInfo()
    {
        $msgConfirmation="";

        $user=new AbaUser();
        if( !$user->getUserById(Yii::app()->user->id) )
        {
            // Redirect to HOME PAGE at the moment:
            $this->redirect("/site/index");
        }

        $isB2B = Yii::app()->user->isB2B();

        $formContact = new ContactForm();
        $postValues = HeMixed::getPostArgs("ContactForm");
        if( !empty($postValues) && trim($user->email)!=="" )
        {
            $formContact->attributes = $postValues;
            if( $formContact->validate() )
            {
                // The subject contains the email address:
                if (Yii::app()->user->getCurrentPartnerId() == PARTNER_JUNTAEXTREMADURA) {
                    $email = 'miriamsanchez@linkup-learning.es';
                } else if ($isB2B) {
                    $email = 'support_corporate@abaenglish.com';
                }
                else {
                    $email = $formContact->getEmailFromSubject();
                }

                $subject = $formContact->subject." from user ".$user->email;
                $body = "<span style='font-weight:bold;'>User ".$user->name." ".$user->surnames." wrote this: </span><br/>".$formContact->body;

                HeAbaMail::sendEmailInternal( $email, "", $subject, $body,
                                true, $this, HeAbaMail::FROM_CAMPUS_USER, $user->email, "", null, $user->email);

                $msgConfirmation= "mailsent_key";
            }
        }

        $this->renderForm($formContact,$msgConfirmation,array('typeUser'=>$isB2B));
    }

    /**
     * @param ContactForm $formContact
     */
    private function renderForm( ContactForm $formContact, $msgConfirmation="", $extraparams = array())
    {
        $params = array_merge(array('modelContact' => $formContact,'msgConfirmation' => $msgConfirmation),$extraparams);
        $this->render('index', $params );
    }

}