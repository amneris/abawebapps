<?php
/**
 * Member Get Member Controller
 */

class InviteController extends AbaController
{
    public function actionIndex() {
        $user = Yii::app()->user;
        $userId = $user->getId();
        $userLang = $user->getLanguage();

        $this->layout = "mainEmpty";
        $absoluteUrl = Yii::app()->createAbsoluteUrl('invite/index');

        $units = new UnitsCourses();
        $level = $user->currentLevel;
        $moUnit = new UnitCourse($units->getStartUnitByLevel($level));
        $successUrl = Yii::app()->createAbsoluteUrl('course/AllUnits/unit/' . $moUnit->getFormatUnitId($moUnit->getUnitId()));

        $this->render('index', array('absoluteUrl' => $absoluteUrl, 'userId' => $userId, 'userLang' => $userLang,
                                    'successUrl' => $successUrl, '_GET' => HeMixed::getGetArgs(), '_POST' => HeMixed::getPostArgs()));
    }

    public function actionOauth() {
        $user = Yii::app()->user;
        $userId = $user->getId();
        $userLang = $user->getLanguage();

        $this->render('oauth', array('userId' => $userId, 'userLang' => $userLang, '_GET' => HeMixed::getGetArgs(), '_POST' => HeMixed::getPostArgs()));
    }

    public function actionHotmailoauth() {
        $user = Yii::app()->user;
        $userId = $user->getId();
        $userLang = $user->getLanguage();

        $this->render('hotmailoauth', array('userId' => $userId, 'userLang' => $userLang, '_GET' => HeMixed::getGetArgs(), '_POST' => HeMixed::getPostArgs()));
    }

    public function actionInvite()
    {
        $user = new AbaUser();
        $user->getUserById(Yii::app()->user->getId());

        $emails = explode(',', HeMixed::getPostArgs('emails'));
        //var_dump($emails);
        if(count($emails) > 0) {
            // every call to selligent is limited to 20 emails (because we use static number of params)
            $emails = array_chunk($emails, 20);
            $commSelligent = new SelligentConnectorCommon();
            foreach($emails as $pack) {
                $params = array();
                $i = 1;
                foreach($pack as $email) {
                    $params['EMAIL'.$i] = $email;
                    $i ++;
                }
                while($i <= 20) {
                    $params['EMAIL'.$i] = '';
                    $i ++;
                }
                $commSelligent->sendOperationToSelligent(SelligentConnectorCommon::inviteFriends, $user, $params, "Invite friends - Send invitations");
            }
        }
    }


}