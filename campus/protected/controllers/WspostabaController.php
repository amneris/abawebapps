<?php

/**
 * This is used for simulations and to receive public notifications like IPN from Paypal
 */
class WspostabaController extends WsController
{

    public function beforeWebMethod($service)
    {
        return true;
    }

    public function afterWebMethod($service)
    {
        return true;
    }


    /**
     * IPN entry point for IPN PAYPAL calls.
     *
     */
    public function actionPaypal()
    {

        $ipnPayPalServ = new IpnPaypalListenerService();
        $retListener = $ipnPayPalServ->processRequestPayPal(HeMixed::getPostArgs());
        if ($retListener) {
            echo "OK";
        } else {
            HeLogger::sendLog(HeLogger::PREFIX_ERR_LOGIC_L . ": PAYPAL IPN Listener ABA service received request and was WRONG PROCESSED",
              HeLogger::IT_BUGS_PAYMENTS, HeLogger::INFO,
              ", details= " . $ipnPayPalServ->getSuccessMessage());
            echo "WRONG";
            error_log("WRONG IPN call:" . $ipnPayPalServ->getLastError());
        }

        return true;
    }

    /**
     * Simulation of the verifying process from PayPal when we answer them.
     */
    public function actionSimulatePayPalVerification()
    {
        if (is_array($_POST)) {
            echo PAY_PAL_VERIFIED;
            error_log(" Simulador de PayPal verificacion, cmapos recibidos" . implode(", ", $_POST));
        } else {
            echo PAY_PAL_INVALID;
            error_log(" No hay POST ");
        }
    }

    /** Simulator for development and pre-production environments to assess that tracks are working. It is called
     * from HeAnalytics.
     *
     * @param $userId
     * @param $userEmail
     * @param $tracking
     */
    public function actionSimulatorTrackings($userId, $userEmail, $tracking)
    {
        error_log(" Simulator of Tracking " . $tracking . " received for user " . $userEmail . " " . date("Y-m-d H:m:s"));
        echo "OK, recevied tracking from " . $tracking . " url referer= " . htmlentities($_SERVER["HTTP_REFERER"]);
        die();
    }


    /**
     * @param integer $userId
     * @param string $urlPage
     * @param integer $status
     * @param string $srcHrefSellg
     */
    public function actionSimulatorAnalyticsSelligent($userId, $urlPage, $status, $srcHrefSellg = null)
    {
        $urlPage = base64_decode($urlPage);

        error_log(' Simulator for Selligent Analytics received a page landing: userId=' . $userId . ', Page=' . $urlPage .
          ', Status=' . $status . ', srcHrefSellg=' . $srcHrefSellg);
        echo ' Simulator for Selligent Analytics received a page landing: userId=' . $userId . ', Page=' . $urlPage .
          ', Status=' . $status;
        die();
    }

    /** Simulator of Selligent Banner generator. In simulation configuration the actionProxyBannerDynSelligent
     * is calling this function.
     *
     * @param $userId
     */
    public function actionSimulatorBannerDynSelligent($userId)
    {
        $this->layout = "";
        $this->render('bannerlocal');

        return;
    }

    /** Connects to Selligent to obtain HTML data and the returns it to the client through AJAX calls requests.
     *
     * @param $userId
     */
    public function actionProxyBannerDynSelligent($userId)
    {
        $this->layout = "";
        $data = file_get_contents(Yii::app()->config->get('URL_BANNER_DYNAMIC') . $userId);
        echo $data;

        return;
    }

    /**
     * ADYEN Notifications "Server"
     *
     * @return bool
     */
    public function actionSendnotification()
    {
        if (!$this->isValidHttpAdyenPassword()) {
            return false;
        }

        $notificationRequestItem = HeMixed::getPostArgs();

        if (isset($notificationRequestItem['eventCode'])) {

            $abaPaymentsAdyenNotifications = new AbaPaymentsAdyenNotifications();

            $eventCode = mb_strtoupper($notificationRequestItem['eventCode']);

            switch ($eventCode) {

                case AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_AUTHORISATION:

                    $this->saveNoStandardNotification($abaPaymentsAdyenNotifications, $notificationRequestItem,
                      $eventCode);

                    //
                    // 1. eventCode == AUTHORISATION && success = 1
                    if ($abaPaymentsAdyenNotifications->success == 1) {

                        //
                        // RENEWALS
                        //
                        // Pre-Payment Adyen Renewal
                        //
                        $oPaymentAdyenHpp = new AbaPaymentsAdyenRenewals();
                        $oPaymentAdyenHpp->getAdyenPaymentByMerchantReference($notificationRequestItem['merchantReference'],
                          AbaPaymentsAdyenRenewals::STATUS_HPP_RENEWALS_PENDING);

                        if (is_numeric($oPaymentAdyenHpp->id) AND $oPaymentAdyenHpp->id > 0) {

                            try {

                                $oPaymentAdyenHpp->updateSuccess();

                                //
                                $moPendingPay = new Payment();
                                if ($moPendingPay->getPaymentById($oPaymentAdyenHpp->idPayment)) {
                                }

                                $moUser = new AbaUser();
                                if ($moUser->getUserById($moPendingPay->userId)) {
                                }

                                $dateStart = HeDate::todaySQL(true);

                                // 1 - Actualizar pago viejo.
                                // 2 - Crear siguiente pago.
                                // 3 - Actualizar Usuario (Renovar)
                                $commRecurrPayment = new RecurrentPayCommon();
                                $retSucc = $commRecurrPayment->renewPaymentByPaySuppOrderId(
                                  $moUser->email,
                                  $notificationRequestItem['merchantReference'],
                                  $dateStart,
                                  "success_payment_ADYEN_HPP",
                                  PAY_SUPPLIER_ADYEN_HPP,
                                  $moPendingPay->id,
                                  array());

                                if (!$retSucc) {
                                    try {
                                        HeLogger::sendLog(HeLogger::PREFIX_NOTIF_L.": CRON User Recurring Payment Adyen successful finished with errors",
                                          HeLogger::IT_BUGS, HeLogger::CRITICAL,
                                          " User Recurring Payment Adyen successful but finished with errors " . $moUser->email . ", payment id=" . $moPendingPay->id);

                                        echo ">> XXX" . " Error processing, RENEWED in database but with failures. It MUST be REVIEWED: payment id " . $moPendingPay->id;
                                    } catch (Exception $ex) {
                                    }
                                } else {

                                    if (isset($notificationRequestItem['pspReference'])) {
                                        $moPendingPay->paySuppExtUniId = $notificationRequestItem['pspReference'];
                                        $moPendingPay->update(array("paySuppExtUniId"));
                                    }
                                }
                            } catch (Exception $e) { }

                            $logResponse = true;
                        }
                        else {

                            //
                            // HPP Adyen payments
                            if ($abaPaymentsAdyenNotifications->isHppNotification()) {

                                //
                                // 2. Validate payment method
                                $payGatewayLogAdyenReqSent = new PayGatewayLogAdyenReqSent();

                                if ($payGatewayLogAdyenReqSent->getLogByReference($abaPaymentsAdyenNotifications->merchantReference)) {

                                    $payControl = new PaymentControlCheck();

                                    //
                                    if (!$payControl->getPaymentById($payGatewayLogAdyenReqSent->idPayment)) {
                                        HeLogger::sendLog(HeLogger::PREFIX_NOTIF_L.": HPP Adyen Payment NOT FOUND",
                                          HeLogger::IT_BUGS, HeLogger::CRITICAL,
                                          " HPP Adyen Payment NOT FOUND. ID paymentControlChech=" . $payGatewayLogAdyenReqSent->idPayment);
                                    }

                                    $paySupplier = PaymentFactory::createPayment(null, PAY_SUPPLIER_ADYEN_HPP);

                                    $paySupplier->setOptionalData($notificationRequestItem);

                                    $logResponse = $paySupplier->processDebitingHpp($payControl, $notificationRequestItem);
                                }

                            }

                        }

                    }

                    /**
                     * The following payment method support recurring:
                     *
                     * cards (with the exception of maestro and mister cash)
                     * directEbanking
                     * giropay
                     * iDEAL
                     * PayPal
                     * SEPA Direct Debit
                     */
                    //
                    // @TODO
                    //
                    switch ($notificationRequestItem['paymentMethod']) {

                        case "alipay":
                        case "bankTransfer":
                        case "bankTransfer_IBAN":
                        case "bankTransfer_NL":
                        case "bank_ru":
                        case "boleto":
                        case "directEbanking":
                        case "directdebit_NL":
                        case "dotpay":
                        case "ebanking_FI":
                        case "elv":
                        case "giropay":
                        case "ideal":
                        case "interac":
                        case "online_RU":
                        case "sepadirectdebit":
                        case "terminal_RU":
                        case "trustly":
                        case "trustpay":
                        case "unionpay":
                        case "wallet_RU":

//                        case "amex":
//                        case "bcmc":
//                        case "cup":
//                        case "diners":
//                        case "discover":
//                        case "elo":
//                        case "jcb":
//                        case "maestro":
//                        case "mc":
//                        case "visa":

//                        case "visadankort":
//                        case "vias":
//                        case "maestrouk":
//                        case "solo":
//                        case "laser":
//                        case "bijcard":
//                        case "dankort":
//                        case "hipercard":
//                        case "uatp":
//                        case "cartebancaire":
//                        case "visaalphabankbonus":
//                        case "mcalphabankbonus":

                            break;

                        default:
                            // * Consultar primer per si de cas que no existeixi el PAYMENT  !!!
                            break;
                    }

                    /**
                     * This field displays the modification operations supported by this payment as a list of strings, this is only populated for
                     * AUTHORISATION notifications. The operations will inform you whether you need to capture the payment (if you don't have
                     * auto-capture set up), whether you can cancel the payment (before capture) or if you can refund the payment (after it has
                     * been captured).
                     *
                     * Handle AUTHORISATION notification.
                     * Confirms whether the payment was authorised successfully.
                     * The authorisation is successful if the "success" field has the value true.
                     * In case of an error or a refusal, it will be false and the "reason" field
                     * should be consulted for the cause of the authorisation failure.
                     *
                     */
                    break;
                /**
                 * If the message was syntactically valid and merchantAccount is correct you will
                 * receive a refundReceived response with the following fields:
                 * - pspReference:  A new reference to uniquely identify this modification request.
                 * - response:      A confirmation indicating we receievd the request: [refund-received].
                 *
                 * Please note: The result of the refund is sent via a notification with eventCode REFUND.
                 */
                case AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_REFUND:

                    $this->saveNoStandardNotification($abaPaymentsAdyenNotifications, $notificationRequestItem,
                      $eventCode);

                    //
                    // eventCode == REFUND
                    if (isset($notificationRequestItem['pspReference']) AND trim($notificationRequestItem['pspReference']) <> ''
                      AND isset($notificationRequestItem['merchantReference']) AND trim($notificationRequestItem['merchantReference']) <> ''
                    ) {
                        $adyenPendingRefunds = new AdyenPendingRefunds();

                        if ($adyenPendingRefunds->getAdyenPaymentByReferences($notificationRequestItem['pspReference'],
                          $notificationRequestItem['merchantReference'])
                        ) {
                            if ($adyenPendingRefunds->status == PAY_ADYEN_PENDING_REFUND_PENDING) {
                                if ($adyenPendingRefunds->getAdyenPaymentById($adyenPendingRefunds->id)) {
                                    if ($abaPaymentsAdyenNotifications->success == 1) {
                                        $adyenPendingRefunds->updateToSuccessNotification();
                                    } else {
                                        $adyenPendingRefunds->updateToFail();
                                    }
                                }
                            }
                        }
                    }
                    break;

                case AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_REPORT_AVAILABLE:
                case AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_CANCELLATION:
                case AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_CANCEL_OR_REFUND:
                case AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_CAPTURE:
                case AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_CAPTURE_FAILED:
                    // $abaPaymentsAdyenNotifications->saveDefaultNotification(AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_REQUEST_FOR_INFORMATION, $notificationRequestItem);
                    $abaPaymentsAdyenNotifications->saveDefaultNotification($eventCode, $notificationRequestItem);
                    break;

                //#6084
                case AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_NOTIFICATION_OF_FRAUD:
                //
                case AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_REQUEST_FOR_INFORMATION:
                case AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_NOTIFICATION_OF_CHARGEBACK:
                case AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_ADVICE_OF_DEBIT:
                case AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_CHARGEBACK:
                case AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_CHARGEBACK_REVERSED:

                    $this->saveNoStandardNotification($abaPaymentsAdyenNotifications, $notificationRequestItem,
                      $eventCode);

                    if (isset($notificationRequestItem['originalReference']) AND trim($notificationRequestItem['originalReference']) <> ''
                      AND isset($notificationRequestItem['merchantReference']) AND trim($notificationRequestItem['merchantReference']) <> ''
                    ) {

                        $oPayment = new Payment();
                        if ($oPayment->getPaymentBySuppOrderIdAndSuppExtUniId($notificationRequestItem['merchantReference'],
                          $notificationRequestItem['originalReference'], PAY_SUCCESS)
                        ) {
                            $oAdyenFraud = new AdyenFraud();

                            $oAdyenFraud->userId = $oPayment->userId;
                            $oAdyenFraud->idPayment = $oPayment->id;
                            $oAdyenFraud->amountCurrency = $abaPaymentsAdyenNotifications->amountCurrency;
                            $oAdyenFraud->amountValue = $abaPaymentsAdyenNotifications->amountValue;
                            $oAdyenFraud->eventCode = $eventCode;
                            $oAdyenFraud->merchantReference = $abaPaymentsAdyenNotifications->merchantReference;
                            $oAdyenFraud->originalReference = $abaPaymentsAdyenNotifications->originalReference;
                            $oAdyenFraud->pspReference = $abaPaymentsAdyenNotifications->pspReference;
                            $oAdyenFraud->reason = $abaPaymentsAdyenNotifications->reason;
                            $oAdyenFraud->paymentMethod = $abaPaymentsAdyenNotifications->paymentMethod;
                            $oAdyenFraud->status = AdyenFraud::STATUS_PENDING;
                            $oAdyenFraud->dateAdd = HeDate::todaySQL(true);

                            if (!$oAdyenFraud->insertFraudAdyen()) {

                                HeLogger::sendLog(HeLogger::PREFIX_ERR_LOGIC_L . ": ADYEN FRAUD NOTIFICATIONS Listener ABA service received " .
                                    $eventCode . " request received WITHOUT errors. InsertFraudAdyen inconsistency. Fraud has not been inserted.",
                                    HeLogger::IT_BUGS_PAYMENTS, HeLogger::INFO,
                                    ", details=" . $eventCode . ":" . $abaPaymentsAdyenNotifications->merchantReference . ":" . $abaPaymentsAdyenNotifications->originalReference);
                            } else {

                                $oAbaPaymentsAdyenFraud = new AdyenFraud();
                                switch($eventCode) {
                                    case AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_CHARGEBACK:
                                        if ($oAbaPaymentsAdyenFraud->getAdyenFraudById($oAdyenFraud->idFraud)) {
                                            $oAbaPaymentsAdyenFraud->updateToInitiatedDispute();
                                        }
                                        break;
                                    case AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_CHARGEBACK_REVERSED:
                                        if ($oAbaPaymentsAdyenFraud->getAdyenFraudById($oAdyenFraud->idFraud)) {
                                            $oAbaPaymentsAdyenFraud->updateToSuccessAfterDispute();
                                        }
                                        break;
                                }
                            }
                        }
                    }
                    break;

                default :
//                    AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_REFUND_FAILED;
//                    AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_REFUND_WITH_DATA;
//                    AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_PAYOUT_DECLINE;
//                    AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_ORDER_OPENED;
//                    AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_ORDER_CLOSED;

                    $abaPaymentsAdyenNotifications->saveDefaultNotification("UNKNOWN - " . $eventCode,
                      $notificationRequestItem);

                    HeLogger::sendLog(HeLogger::PREFIX_ERR_LOGIC_L . ": ADYEN NOTIFICATIONS Listener ABA service received UNKNOWN request",
                        HeLogger::IT_BUGS_PAYMENTS, HeLogger::INFO,
                        ", details= " . $eventCode);

                    break;
            }
        }

        /**
         * !!! IMPORTANT - always print "[accepted]"
         *
         */
        echo "[accepted]";
        return true;
    }

    /**
     * @param $abaPaymentsAdyenNotifications
     * @param $notificationRequestItem
     * @param $eventCode
     *
     * @return bool
     */
    protected function saveNoStandardNotification(AbaPaymentsAdyenNotifications $abaPaymentsAdyenNotifications, $notificationRequestItem, $eventCode)
    {

        $abaPaymentsAdyenNotifications->setNotificationData($notificationRequestItem);

        if ($abaPaymentsAdyenNotifications->insertPaymentAdyenNotification()) {
            if (isset($notificationRequestItem['pspReference']) AND trim($notificationRequestItem['pspReference']) <> '') {
                $payGatewayLogAdyenResRec = new PayGatewayLogAdyenResRec();
                $payGatewayLogAdyenResRec->getAdyenPaymentByUniqueId($notificationRequestItem['pspReference']);
                if (is_numeric($payGatewayLogAdyenResRec->id) AND $payGatewayLogAdyenResRec->id > 0) {
                    $payGatewayLogAdyenResRec->updateNotifications();
                }
            }
        } else {
            HeLogger::sendLog(HeLogger::PREFIX_ERR_LOGIC_L . ": ADYEN NOTIFICATIONS Listener ABA service received " . $eventCode . " request received with errors",
                HeLogger::IT_BUGS_PAYMENTS, HeLogger::INFO,
                ", details= " . $eventCode);
        }

        return true;
    }

}
