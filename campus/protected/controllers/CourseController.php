<?php

class CourseController extends AbaController
{
    public $data = array();

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        if (!HeRoles::allowOperation(OP_COURSE)) {
            $this->redirect($this->createUrl('site/noaccess'));
        }

        $units = new UnitsCourses();
        $tmpLevel = (!is_null(HeMixed::getPostArgs('level')) && (HeMixed::isValidLevel(
              HeMixed::getPostArgs('level')
          ))) ? HeMixed::getPostArgs('level') : Yii::app()->user->currentLevel;
        /* @var UnitCourse[] $aUnitsObject */
        $aUnitsObject = $units->getAllUnitsByLevel($tmpLevel);

        // Find out which is the last video-class the user has access to:
        $userId = Yii::app()->user->getId();
        $user = new AbaUser();
        $user->getUserById($userId);

        $aUnits = array();
        /*  @var UnitCourse $unitCourse */
        /*  @var integer $unitId */
        foreach ($aUnitsObject as $unitId => $unitCourse) {
            /**  We have to collect all these info
             * Curso:
             * 1. Número
             * 2. Título No traducible (Helper)
             * 3. Título traducible (mainApp.php, implmemetnar función en el Helper)
             * 5. Mirar el getUrl dinámico, debería estar en el modelo este cálculo.
             * 6. Thumbnails mirar de donde c. está.(????)
             */
            $aUnits[$unitId] = array();
            $aUnits[$unitId]["unitId"] = $unitCourse->getFormatUnitId($unitId);
            $aUnits[$unitId]["mainNameNoTrans"] = $unitCourse->getMainNameNoTrans();
            $aUnits[$unitId]["titleTrans"] = $unitCourse->getTitleTrans();
            $aUnits[$unitId]["thumbUrlLink"] = $unitCourse->getThumbUrlLink();
            $aUnits[$unitId]["thumbPath"] = $unitCourse->getThumbPath();
            /*
             - Progreso (abaFollowUp)
                4. Progreso de cada unidad (Función de Jose Antonio)
             */
            $followUpCourse = new AbaFollowup();
            $aUnits[$unitId]["unitProgress"] = 0;
            if ($followUpCourse->getFollowupByIds(Yii::app()->user->getId(), $unitId)) {
                $aUnits[$unitId]["unitProgress"] = $followUpCourse->getUnitProgress();
            }
        }

        $firstUnit = $units->getStartUnitByLevel($tmpLevel);
        $lastUnit = $units->getEndUnitByLevel($tmpLevel);

        $this->render('index', array("aUnits" => $aUnits, "firstUnit" => $firstUnit, "lastUnit" => $lastUnit));
    }


    /** This action is a kind of REST web service. the reason it is located here, and not in the wsrestservice
     * is because you need CAMPUS AUTHENTICATION.
     *
     * @param integer $userId
     * @param integer $unit
     *
     * @return string
     */
    public function actionCheckLevelAfterExam($userId, $unit)
    {
        if (Yii::app()->user->getId() == false ||
          intval($userId) !== intval(Yii::app()->user->getId())
        ) {
            $body = CJSON::encode("Your request is invalid. Please make sure to send proper parameters.");
            WsRestController::genericSendResponse($body);
            return false;
        }

        $moUser = new AbaUser();
        $moUser->getUserById(Yii::app()->user->getId());
        $hasChanged = false;
        $oldLevel = $moUser->currentLevel;
        $newLevel = $moUser->currentLevel;

        $moFollowUp = new AbaFollowup();
        $moFollowUp = $moFollowUp->getFollowupByIds($moUser->id, $unit);
        if (!$moFollowUp) {
            $this->returnLevelAfterExam($oldLevel, $newLevel, $hasChanged);
            return false;
        }

        $unitProgress = $moFollowUp->getUnitProgress(true);
        // If user has not finished this UNIT, then there is no need to continue, there will be not level upgrade:
        if (intval($unitProgress) < 100) {
            $this->returnLevelAfterExam($oldLevel, $newLevel, $hasChanged);
            return false;
        }

        $commUser = new UserCommon();
        if ($commUser->hasFinishedLevel($moUser->id, $moUser->currentLevel)) {
            if ($oldLevel == MAX_ENGLISH_LEVEL) {
                $this->returnLevelAfterExam($oldLevel, $newLevel, $hasChanged);
                return true;
            }

            $newLevel = $commUser->nextLevelNotCompleted($moUser);
            if ($newLevel > MAX_ENGLISH_LEVEL) {
                HeLogger::sendLog(
                    HeLogger::PREFIX_NOTIF_H. ": User has reached maximum level" . $oldLevel . " to $newLevel",
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    "User id " . $moUser->id . " with level $oldLevel has completed full course. It should not tried " .
                    " to change level."
                );
                $hasChanged = "error";
                $this->returnLevelAfterExam($oldLevel, $newLevel, $hasChanged);
                return true;
            }
            $hasChanged = $moUser->updateUserCourseLevel($newLevel);
            if ($hasChanged) {
                $hasChanged = $commUser->changeUserLevel($moUser, $oldLevel, $newLevel, "Course Exam finish");
            }
            if (!$hasChanged) {
                $newLevel = $oldLevel;
                HeLogger::sendLog(
                    HeLogger::PREFIX_NOTIF_H . ": Could not change user level from " . $oldLevel . " to $newLevel",
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    "User id " . $moUser->id . " with level $oldLevel has completed an " .
                    " exam and Campus could not change his level."
                );
                $hasChanged = "error";
            }
        }
        $this->returnLevelAfterExam($oldLevel, $newLevel, $hasChanged);
        return true;
    }

    /**
     * @param integer $oldLevel
     * @param integer $newLevel
     * @param bool $hasChanged
     * @return bool
     */
    protected function returnLevelAfterExam($oldLevel, $newLevel, $hasChanged = false)
    {
        $aReturnLevels = array("oldLevel" => $oldLevel, "newLevel" => $newLevel, "hasChanged" => $hasChanged);
        WsRestController::genericSendResponse($aReturnLevels);
        return true;
    }

    /**
     * Gives access to a unit of the course
     * @param $unit
     * @param int $section In case $section comes then it opens up the unit by this tab.
     * @param int $page In case $page comes then it opens up the page inside the tab.
     */
    public function actionAllUnits($unit, $section = 1, $page = 0)
    {
        if (!HeRoles::allowOperation(OP_COURSE)) {
            $this->redirect($this->createUrl('site/noaccess'));
        }

        if (trim($unit) == '') {
            $this->redirect($this->createUrl('course/index'));
        }

        $userId = Yii::app()->user->getId();
        $user = new AbaUser();
        $user->getUserById($userId);
        $userLang = $user->langEnv;
        $isB2B = Yii::app()->user->isB2B();

        $userTypeAccessUnit = HeRoles::typeOfUnitCourseAccess(null, intval($unit));

        /* special logic for A / B / C testing */
        $firstUnits = HeUnits::getArrayStartUnitsByLevel();
        if (is_array($firstUnits) && in_array($unit, $firstUnits)) {
            $moCourse = new UnitsCourses();
            if ($course = $moCourse->getUserCourse($userId, $userLang)) {
                if (!empty($course['description'])) {
                    $unit = $unit . '_' . $course['description'];
                }
            }
        }

        switch ($userTypeAccessUnit) {

            case OP_INCOMPLETEUNIT:

                $this->render(
                    'allUnits/index',
                    array(
                        'unit' => $unit,
                        'userTypeAccessUnit' => OP_INCOMPLETEUNIT,
                        'type' => 'video',
                        'section' => 5,
                        'page' => $page,
                        'isB2B' => $isB2B
                    )
                );

                break;

            case OP_FULLUNIT:

                $teacherImg = '';
                if (isset($user->teacherId)) {
                    $moTeacher = new AbaTeacher();
                    $moTeacher = $moTeacher->getTeacherById($user->teacherId);
                    if (isset($moTeacher->photo)) {
                        $teacherImg = $moTeacher->photo;
                    }
                }

                $this->render(
                    'allUnits/index',
                    array(
                        'unit' => $unit,
                        'userTypeAccessUnit' => OP_FULLUNIT,
                        'type' => 'video',
                        'section' => $section,
                        'page' => $page,
                        'isB2B' => $isB2B,
                        'teacherImg' => $teacherImg
                    )
                );

                break;

            case OP_NOACTIONACCESSUNIT:

                $this->redirect($this->createUrl('course/index'));

                break;

            default:

                $this->render(
                    'allUnits/index',
                    array(
                        'unit' => $unit,
                        'userTypeAccessUnit' => 0,
                        'type' => 'unit'
                    )
                );

                break;
        }

        return;
    }

    public function actionRefresh()
    {
        return true;
    }

    // todo this has to be moved to a new microservice of course
    public function actionWsGetUserData($autol) {
        if ($autol) {
            $user = new AbaUser();
            $user->getUserByKeyExternalLogin($autol);

            if ($user) {
                // copied from campus site index
                 $userLevel = $user->currentLevel;
                 $commUser = new UserCommon();

                 $moFirstUnit = $commUser->getFirstUnitNotProgressCompleted($user->getId(), $userLevel);
                 // If is 144 (last unit of last level) and progress = 100% we return 121 (first unit of last level)
                 if ($moFirstUnit->getUnitId() >= HeUnits::getMaxUnits()) {
                     $moFollow144 = new AbaFollowup();
                     $moFollow144->getFollowupByIds(Yii::app()->user->getId(), $moFirstUnit->getUnitId());
                     if ($moFollow144->getUnitProgress(true) == 100) {
                         $moFirstUnit = new UnitCourse(HeUnits::getStartUnitByLevel($userLevel));
                     }
                 }

                 $nextUnitId = $moFirstUnit->getFormatUnitId($moFirstUnit->getUnitId());

                 if (isset($user->teacherId)) {
                     $moTeacher = new AbaTeacher();
                     $moTeacher = $moTeacher->getTeacherById($user->teacherId);
                     if (isset($moTeacher->photo)) {
                         $teacherImage = Yii::app()->getBaseUrl(true).Yii::app()->theme->baseUrl.'/media/images/teachers/'.$moTeacher->photo;
                     }
                 }

                 $response = array(
                     'teacherImage' => $teacherImage,
                     'nextUnitId' => (int)$nextUnitId
                 );

                 $this->sendResponse(200, $response);
            }
        }

        $this->sendResponse(401, 'Unauthorized');
    }

    protected function sendResponse($status = 200, $body = '', $contentType = 'application/json')
    {
        // set the header fields (Status and Content-type)
        $statusHeader = 'HTTP/1.1 ' . $status;
        header($statusHeader);
        header('Content-Type: ' . $contentType);
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
        }

        if (is_array($body)) {
            $response = CJSON::encode($body);
        } else {
            $response = CJSON::encode(array('httpCode'=>$status, 'message'=>$body));
        }

        echo $response;

        Yii::app()->end();
    }

}
