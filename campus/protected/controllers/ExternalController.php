<?php

class ExternalController extends AbaController
{
    CONST SECRET_WSDL_KEY = 'Yii ABA English Key 2';

    protected $stFieldsNamesAliases = array(
      "name" => array("name"),
      "email" => array("email"),
      "password" => array("password", "pass", "sPass"),
      "surname" => array("surname"),
      "langEnv" => array("langEnv", "lang_env"),
      "idProduct" => array("product_id", "idProduct", "productID", "productId"),
      "promocode" => array("promocode", "code1"),
      "code2" => array("code2"),
      "idCountry" => array("idCountry", "country_id", "countryCode"),
      "partnerId" => array("partnerId", "partner_id", "idPartner"),
      "idSource" => array("idSource", "idSourceList"),
      "device" => array("device"),
      "currentLevel" => array("currentLevel"),
      "scenario" => array("scenario"),
      "viajar" => array("viajar"),
      "estudiar" => array("estudiar"),
      "trabajar" => array("trabajar"),
      "otros" => array("otros"),
      "nivel" => array("nivel"),
      "selEngPrev" => array("selEngPrev"),
      "engPrev" => array("engPrev"),
      "dedicacion" => array("dedicacion"),
      "sAction" => array("sAction"),
      "sEmail" => array("sEmail"),
      "sPass" => array("sPass"),
      "sName" => array("sName"),
      "sSurname" => array("sSurname"),
      "sGender" => array("sGender"),
      "sFbid" => array("sFbid"),
      "sEmailFacebook" => array("sEmailFacebook"),
      "sINid" => array("sINid"),
      "sEmailLinkedin" => array("sEmailLinkedin"),
      "telephone" => array("telephone"),
    );

    /**
     * @param $sAbaFieldName
     * @param array $stPostArgs
     * @param string $stDefaultValue
     *
     * @return string
     */
    private function getValueByAlias(
      $sAbaFieldName,
      $stPostArgs = array(),
      $stDefaultValue = '',
      $bCheckEmptyValue = true
    ) {
        foreach ($this->stFieldsNamesAliases as $sFieldName => $stFieldNameAliases) {

            if ($sAbaFieldName == $sFieldName) {

                foreach ($stPostArgs as $sPostArgName => $sPostArgValue) {

                    if (in_array($sPostArgName, $stFieldNameAliases)) {

                        if ($bCheckEmptyValue) {
                            if (empty($sPostArgValue)) {
                                return $stDefaultValue;
                            }
                        } else {
                            if (trim($sPostArgValue) == '') {
                                return $stDefaultValue;
                            }
                        }
                        return $sPostArgValue;
                    }
                }
            }
        }
        return $stDefaultValue;
    }

    /**
     * Replaces Website service Aba_Wsdl::registerUser()
     */
    public function actionSignup()
    {
        //TODO: @abaenglish: Hides irritating PHP notice
        error_reporting(E_ALL ^ E_NOTICE);

        HeMixed::securePublicUrlRequest("external/signup");
        $this->layout = "none";

        $postArgs = HeMixed::getPostArgs();

        // TODO: @abaenglish: Implement com security issues with signature
        //$signature = $postArgs['signature'];
        $name = $this->getValueByAlias('name', $postArgs, '', false);
        $email = $this->getValueByAlias('email', $postArgs, '', false);
        $pass = $this->getValueByAlias('password', $postArgs, '', false);
        $surname = $this->getValueByAlias('surname', $postArgs);
        $langEnv = $this->getValueByAlias('langEnv', $postArgs);
        $idProduct = $this->getValueByAlias('idProduct', $postArgs); // product_id
        $promocode = $this->getValueByAlias('promocode', $postArgs);
        $landingPage = (trim($idProduct) == '' ? 'HOME' : 'PAYMENT');
        $idCountry = $this->getValueByAlias('idCountry', $postArgs, null); // null instead of DEFAULT_COUNTRY_ID for ABAWEBAPPS-971
        $partnerId = $this->getValueByAlias('partnerId', $postArgs, DEFAULT_PARTNER_ID);
        $idSourceList = $this->getValueByAlias('idSource', $postArgs, null);
        $device = $this->getValueByAlias('device', $postArgs);
        $currentLevel = $this->getValueByAlias('currentLevel', $postArgs, null);
        $scenario = $this->getValueByAlias('scenario', $postArgs);

        $stLandingParams = array(
          'server' => $_SERVER,
          'post' => $postArgs,
          'identifier' => "actionSignup",
        );
        HeMixed::trackLandings($stLandingParams);

        try {
            if (!isset($partnerId)) {
                $partnerId = Yii::app()->config->get("ABA_PARTNERID");
            }

            $commProducts = new ProductsPricesCommon();
            if (trim($idProduct) !== "" && !$commProducts->checkExistsIdProduct($idProduct)) {
                echo $this->retErrorWs("508", " Id product " . $idProduct);
                return true;
            }
            if ($landingPage !== 'HOME' && $landingPage !== 'PAYMENT') {
                echo $this->retErrorWs("501", " LadingPage " . $landingPage . " should be HOME or PAYMENT.");
                return true;
            }
            $moCountry = new AbaCountry();
            if ($idCountry == '' ) {
                // ABAWEBAPPS-971 autodetect countryID by IP.
                $idCountry = HeMixed::getCountryId(Yii::app()->request->getUserHostAddress());
            } elseif (!$moCountry->getCountryById($idCountry)) {
                echo $this->retErrorWs("513", " Country Id($idCountry) in registerUser " . "should contain a valid value.");
                return true;
            }
            if (trim($promocode) !== "" && !$commProducts->checkExistsPromocode($promocode)) {
                $promocode = "";
            }

            $rulerWs = new RegisterUserService();
            $retUser = $rulerWs->registerUser($email, $pass, $langEnv, $idCountry, $name, $surname, $currentLevel,
              $partnerId, $idSourceList, $device, null, $scenario);

            if ($retUser) {
                if ($retUser->langEnv == '') {
                    echo $this->retErrorWs("503", "Language not set.");
                    return true;
                }

                if ($landingPage == 'HOME') {
                    $landingPage = 'HOME_REGISTER';
                }

                $extraParams = array();
                foreach ($postArgs as $name => $value) { // ABAWEBAPPS-1008 - cleanup the parameters to re-send only unknown parameters
                    if (!$this->isAKnownParameterOrAlias($name)) {
                        $extraParams[$name] = $value;
                    }
                }

                $extraParams['idSource'] = $this->getValueByAlias('idSource', $postArgs, null); // ABAWEBAPPS-1008

                echo $this->retSuccessWs(array(
                  "userId" => $retUser->getId(),
                  "result" => "success",
                  "redirectUrl" => $rulerWs->getRedirectUrl($retUser, $idProduct, $promocode,
                    $landingPage, true, $partnerId, '', false, $extraParams)
                ));
                return true;

            } else {
                echo $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError());
                return true;
            }

        } catch (Exception $e) {
            echo $this->retErrorWs("555", $e->getMessage());
            return true;
        }
    }

    /**
     * Replaces Website service Aba_Wsdl::registerUserObjectives()
     */
    public function actionSignupObjectives()
    {
        //TODO: @abaenglish: Hides irritating PHP notice
        error_reporting(E_ALL ^ E_NOTICE);

        HeMixed::securePublicUrlRequest("external/signupobjectives");
        $this->layout = "none";

        $postArgs = HeMixed::getPostArgs();

        // TODO: @abaenglish: Implement com security issues with signature
        //$signature = $postArgs['signature'];
        $email = $this->getValueByAlias('email', $postArgs, '', false);
        $pass = $this->getValueByAlias('password', $postArgs, '', false);
        $langEnv = $this->getValueByAlias('langEnv', $postArgs);
        $name = $this->getValueByAlias('name', $postArgs, '', false);
        $surname = $this->getValueByAlias('surname', $postArgs, '', false);
        $idProduct = $this->getValueByAlias('idProduct', $postArgs);
        $promocode = $this->getValueByAlias('promocode', $postArgs);
        $landingPage = (trim($idProduct) == '' ? 'HOME' : 'PAYMENT');
        $idCountry = $this->getValueByAlias('idCountry', $postArgs, DEFAULT_COUNTRY_ID);
        $partnerId = $this->getValueByAlias('partnerId', $postArgs, DEFAULT_PARTNER_ID);
        $idSourceList = $this->getValueByAlias('idSource', $postArgs, null);
        $device = $this->getValueByAlias('device', $postArgs);
        $currentLevel = $this->getValueByAlias('currentLevel', $postArgs, null);
        $scenario = $this->getValueByAlias('scenario', $postArgs);

        $objective['viajar'] = $this->getValueByAlias('viajar', $postArgs, null, false);
        $objective['trabajar'] = $this->getValueByAlias('trabajar', $postArgs, null, false);
        $objective['estudiar'] = $this->getValueByAlias('estudiar', $postArgs, null, false);
        $objective['nivel'] = $this->getValueByAlias('nivel', $postArgs, null, false);
        $objective['otros'] = $this->getValueByAlias('otros', $postArgs, null, false);
        $objective['dedicacion'] = $this->getValueByAlias('dedicacion', $postArgs, null, false);
        $objective['engPrev'] = $this->getValueByAlias('engPrev', $postArgs, null, false);
        $objective['selEngPrev'] = $this->getValueByAlias('selEngPrev', $postArgs, null, false);

        $objectives = json_encode($objective);

        $stLandingParams = array(
          'server' => $_SERVER,
          'post' => $postArgs,
          'identifier' => "actionSignupObjectives",
        );
        HeMixed::trackLandings($stLandingParams);

        try {
            if (!isset($partnerId)) {
                $partnerId = Yii::app()->config->get("ABA_PARTNERID");
            }

            $commProducts = new ProductsPricesCommon();
            if (trim($idProduct) !== "" && !$commProducts->checkExistsIdProduct($idProduct)) {
                echo $this->retErrorWs("508", " Id product " . $idProduct);
                return true;
            }
            if ($landingPage !== 'HOME' && $landingPage !== 'PAYMENT') {
                echo $this->retErrorWs("501", " LadingPage " . $landingPage . " should be HOME or PAYMENT.");
                return true;
            }
            $moCountry = new AbaCountry();
            if ($idCountry == '' || !$moCountry->getCountryById($idCountry)) {
                echo $this->retErrorWs("513", " Country Id($idCountry) in registerUser " .
                  "should contain a valid value.");
                return true;
            }

            $rulerWs = new RegisterUserService();
            $retUser = $rulerWs->registerUser($email, $pass, $langEnv, $idCountry, $name, $surname, $currentLevel,
              $partnerId, $idSourceList, $device, null, $scenario);

            if ($retUser) {
                if ($retUser->langEnv == '') {
                    echo $this->retErrorWs("503", "Language not set.");
                    return true;
                }

                $bObjectivesSuccess = true;

                if (trim($objectives) != '') {

                    $aObjectives = json_decode($objectives, true);

                    $userObjectives = new AbaUserObjective();

                    if (!$userObjectives->saveFormObjectivesWs($retUser, $aObjectives)) {
                        $bObjectivesSuccess = false;
                    }
                }

                $sRedirectUrl = $rulerWs->getRedirectUrl($retUser, $idProduct, $promocode, 'OBJECTIVES', true,
                  $partnerId);

                if (!HeMixed::isValidLevel($currentLevel)) {
                    echo $this->retSuccessWs(array(
                      "userId" => $retUser->getId(),
                      "result" => "success with errors: " . "Selected level is invalid",
                      "redirectUrl" => $sRedirectUrl,
                      "resultObjectives" => ($bObjectivesSuccess ? "success" : "Some error has ocurred while inserting/updating your questionnaire.")
                    ));
                } else {
                    echo $this->retSuccessWs(array(
                      "userId" => $retUser->getId(),
                      "result" => "success",
                      "redirectUrl" => $sRedirectUrl,
                      "resultObjectives" => ($bObjectivesSuccess ? "success" : "Some error has ocurred while inserting/updating your questionnaire.")
                    ));
                }
            } else {
                echo $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError());
            }
            return true;

        } catch (Exception $e) {
            echo $this->retErrorWs("555", $e->getMessage());
            return true;
        }
    }

    /**
     * Replaces Website service Aba_Wsdl::registerUserFacebook()
     */
    public function actionSignupFacebook()
    {
        //TODO: @abaenglish: Hides irritating PHP notice
        error_reporting(E_ALL ^ E_NOTICE);

        HeMixed::securePublicUrlRequest("external/signupfacebook");
        $this->layout = "none";

        $postArgs = HeMixed::getPostArgs();

        // TODO: abaenglish: Implement com security issues with signature
        //$signature = $postArgs['signature'];
        $sAction = $this->getValueByAlias('sAction', $postArgs, '', false);
        $sEmail = $this->getValueByAlias('sEmail', $postArgs, '', false);
        $sPass = $this->getValueByAlias('sPass', $postArgs, '', false);
        $sName = $this->getValueByAlias('sName', $postArgs, '', false);
        $sSurname = $this->getValueByAlias('sSurname', $postArgs, '', false);
        $sFbid = $this->getValueByAlias('sFbid', $postArgs, '', false);
        $sGender = $this->getValueByAlias('sGender', $postArgs, '', false);
        $sEmailFacebook = $this->getValueByAlias('sEmailFacebook', $postArgs, '', false);
        $langEnv = $this->getValueByAlias('langEnv', $postArgs);
        $idProduct = $this->getValueByAlias('idProduct', $postArgs);
        $promocode = $this->getValueByAlias('promocode', $postArgs);
        $landingPage = (trim($idProduct) == '' ? 'HOME' : 'PAYMENT');
        $idCountry = $this->getValueByAlias('idCountry', $postArgs, DEFAULT_COUNTRY_ID);
        $partnerId = $this->getValueByAlias('partnerId', $postArgs, DEFAULT_PARTNER_ID);
        $idSourceList = $this->getValueByAlias('idSource', $postArgs, null);
        $device = $this->getValueByAlias('device', $postArgs);
        $currentLevel = $this->getValueByAlias('currentLevel', $postArgs, null);
        $scenario = $this->getValueByAlias('scenario', $postArgs);

        $stLandingParams = array(
          'server' => $_SERVER,
          'post' => $postArgs,
          'identifier' => "actionSignupFacebook",
        );
        HeMixed::trackLandings($stLandingParams);

        try {
            // Validation of web service action
            if ($sAction !== 'exists' && $sAction !== 'check' && $sAction !== 'register') {
                echo $this->retErrorWs("503", "Invalid action");
                return true;
            }

            $userCommon = new UserCommon();
            $rulerWs = new RegisterUserService();

            $sResult = '';
            $retUser = false;

            if ($sAction == 'exists') { // existing user

                $retUser = $userCommon->checkIfFbUserExists($sFbid);

                if ($retUser) {
                    $sResult = "successexists";
                }
            } elseif ($sAction == 'check' && $userCommon->checkIfChangedEmailExists($sEmail,
                $sEmailFacebook)
            ) { // new registered user
                $sResult = "successcheck";
            } elseif ($sAction == 'register') { // check email

                $form = new LoginForm();
                $form->attributes = array('email' => $sEmail, 'password' => $sPass, 'rememberMe' => 0);

                if ($form->validate()) {
                    //
                    // Update any user data
                    $retUser = new AbaUser();
                    $retUser->getUserByEmail($sEmail);

                    $sResult = "successregister";
                    $userCommon->changeUserFbData($retUser, $sName, $sSurname, $sFbid, $sGender);
                } else {
                    echo $this->retErrorWs("504", '');
                    return true;
                }
            } else {

                if (!isset($partnerId)) {
                    $partnerId = Yii::app()->config->get("ABA_PARTNERID");
                }

                $commProducts = new ProductsPricesCommon();
                if (trim($idProduct) !== "" && !$commProducts->checkExistsIdProduct($idProduct)) {
                    echo $this->retErrorWs("508", " Id product " . $idProduct);
                    return true;
                }
                if ($landingPage !== 'HOME' && $landingPage !== 'PAYMENT') {
                    echo $this->retErrorWs("501", " LadingPage " . $landingPage . " should be HOME or PAYMENT.");
                    return true;
                }
                $moCountry = new AbaCountry();
                if ($idCountry == '' || !$moCountry->getCountryById($idCountry)) {
                    echo $this->retErrorWs("513",
                      " Country Id($idCountry) in registerUser should contain a valid value.");
                    return true;
                }

                if ($landingPage == 'HOME') {
                    $landingPage = 'HOME_REGISTER';
                }

                $sResult = "successregister";

                $retUser = $rulerWs->registerUserFacebook($sEmail, $sName, $sSurname, $sFbid, $sGender, $langEnv,
                  $idCountry, $partnerId, $idSourceList, $device, $currentLevel, $scenario);
            }

            if ($retUser) {

                // ---------------------------------------------
                // Transform some parameters: +++++++++++++++++++++++++++++++++++++++
                if (trim($promocode) == "undefined") {
                    $promocode = "";
                }
                $commProducts = new ProductsPricesCommon();
                if (trim($promocode) !== "" && !$commProducts->checkExistsPromocode($promocode)) {
                    $promocode = "";
                }
                //-----------------------------------------------

                echo $this->retSuccessWs(array(
                  "fbId" => $sFbid,
                  "userId" => $retUser->getId(),
                  "email" => $retUser->email,
                  "result" => $sResult,
                  "redirectUrl" => $rulerWs->getRedirectUrl($retUser, $idProduct, $promocode, $landingPage, true,
                    $partnerId),
                ));
                return true;
            } else {

                if ($rulerWs->getLastError() !== "" && $rulerWs->getLastError() !== null) {
                    echo $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError());
                    return true;
                } else {
                    if ($sAction == 'exists') { // USER NO EXISTS
                        echo $this->retSuccessWs(array(
                          "result" => "noexists",
                        ));
                        return true;
                    } elseif ($sAction == 'check') { //
                        echo $this->retSuccessWs(array(
                          "result" => ($sResult == "successcheck" ? "successcheck" : "nocheck"),
                        ));
                        return true;
                    } elseif ($sAction == 'register') { //
                        echo $this->retSuccessWs(array(
                          "result" => "noregister",
                        ));
                        return true;
                    }
                }
                echo $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError());
                return true;
            }

        } catch (Exception $e) {
            echo $this->retErrorWs("555", $e->getMessage());
        }
    }

    /**
     * Replaces Website service Aba_Wsdl::registerUserLinkedin()
     */
    public function actionSignupLinkedin()
    {
        //TODO: @abaenglish: Hides irritating PHP notice
        error_reporting(E_ALL ^ E_NOTICE);

        HeMixed::securePublicUrlRequest("external/signuplinkedin");
        $this->layout = "none";

        $postArgs = HeMixed::getPostArgs();

        // TODO: abaenglish: Implement com security issues with signature
        //$signature = $postArgs['signature'];
        $sAction = $this->getValueByAlias('sAction', $postArgs, '', false);
        $sEmail = $this->getValueByAlias('sEmail', $postArgs, '', false);
        $sPass = $this->getValueByAlias('sPass', $postArgs, '', false);
        $sName = $this->getValueByAlias('sName', $postArgs, '', false);
        $sSurname = $this->getValueByAlias('sSurname', $postArgs, '', false);
        $sINid = $this->getValueByAlias('sINid', $postArgs, '', false);
        $sGender = $this->getValueByAlias('sGender', $postArgs, '', false);
        $sEmailLinkedin = $this->getValueByAlias('sEmailLinkedin', $postArgs, '', false);
        $langEnv = $this->getValueByAlias('langEnv', $postArgs);
        $idCountry = $this->getValueByAlias('idCountry', $postArgs, DEFAULT_COUNTRY_ID);
        $promocode = $this->getValueByAlias('promocode', $postArgs);
        $idProduct = $this->getValueByAlias('idProduct', $postArgs);
        $landingPage = (trim($idProduct) == '' ? 'HOME' : 'PAYMENT');
        $partnerId = $this->getValueByAlias('partnerId', $postArgs, DEFAULT_PARTNER_ID);
        $idSourceList = $this->getValueByAlias('idSource', $postArgs, null);
        $device = $this->getValueByAlias('device', $postArgs);
        $currentLevel = $this->getValueByAlias('currentLevel', $postArgs, null);
        $scenario = $this->getValueByAlias('scenario', $postArgs);

        $stLandingParams = array(
          'server' => $_SERVER,
          'post' => $postArgs,
          'identifier' => "actionSignupLinkedin",
        );
        HeMixed::trackLandings($stLandingParams);

        try {
            // Validation of web service action
            if ($sAction !== 'exists' && $sAction !== 'check' && $sAction !== 'register') {
                echo $this->retErrorWs("503", "Invalid action");
                return true;
            }

            $userCommon = new UserCommon();
            $rulerWs = new RegisterUserService();

            $sResult = '';
            $retUser = false;

            if ($sAction == 'exists') { // existing user

                $retUser = $userCommon->checkILinkedinUserExists($sINid);

                if ($retUser) {
                    $sResult = "successexists";
                }
            } elseif ($sAction == 'check' && $userCommon->checkIfChangedEmailExists($sEmail,
                $sEmailLinkedin)
            ) { // new registered user
                $sResult = "successcheck";
            } elseif ($sAction == 'register') { // check email

                $form = new LoginForm();
                $form->attributes = array('email' => $sEmail, 'password' => $sPass, 'rememberMe' => 0);

                if ($form->validate()) {
                    //
                    // Update any user data
                    $retUser = new AbaUser();
                    $retUser->getUserByEmail($sEmail);

                    $sResult = "successregister";
                    $userCommon->changeUserinData($retUser, $sName, $sSurname, $sINid, $sGender);
                } else {
                    echo $this->retErrorWs("504", '');
                    return true;
                }
            } else {

                if (!isset($partnerId)) {
                    $partnerId = Yii::app()->config->get("ABA_PARTNERID");
                }

                $commProducts = new ProductsPricesCommon();
                if (trim($idProduct) !== "" && !$commProducts->checkExistsIdProduct($idProduct)) {
                    echo $this->retErrorWs("508", " Id product " . $idProduct);
                    return true;
                }
                if ($landingPage !== 'HOME' && $landingPage !== 'PAYMENT') {
                    echo $this->retErrorWs("501", " LadingPage " . $landingPage . " should be HOME or PAYMENT.");
                    return true;
                }
                $moCountry = new AbaCountry();
                if ($idCountry == '' || !$moCountry->getCountryById($idCountry)) {
                    echo $this->retErrorWs("513",
                      " Country Id($idCountry) in registerUser should contain a valid value.");
                    return true;
                }

                if ($landingPage == 'HOME') {
                    $landingPage = 'HOME_REGISTER';
                }

                $sResult = "successregister";

                $retUser = $rulerWs->registerUserLinkedin($sEmail, $sName, $sSurname, $sINid, $sGender, $langEnv,
                  $idCountry, $partnerId, $idSourceList, $device, $currentLevel, $scenario);
            }

            if ($retUser) {

                // ---------------------------------------------
                // Transform some parameters: +++++++++++++++++++++++++++++++++++++++
                if (trim($promocode) == "undefined") {
                    $promocode = "";
                }
                $commProducts = new ProductsPricesCommon();
                if (trim($promocode) !== "" && !$commProducts->checkExistsPromocode($promocode)) {
                    $promocode = "";
                }
                //-----------------------------------------------

                echo $this->retSuccessWs(array(
                  "sINid" => $sINid,
                  "userId" => $retUser->getId(),
                  "email" => $retUser->email,
                  "result" => $sResult,
                  "redirectUrl" => $rulerWs->getRedirectUrl($retUser, $idProduct, $promocode, $landingPage, true,
                    $partnerId),
                ));
                return true;
            } else {

                if ($rulerWs->getLastError() !== "" && $rulerWs->getLastError() !== null) {
                    echo $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError());
                    return true;
                } else {
                    if ($sAction == 'exists') { // USER NO EXISTS
                        echo $this->retSuccessWs(array(
                          "result" => "noexists",
                        ));
                        return true;
                    } elseif ($sAction == 'check') { //
                        echo $this->retSuccessWs(array(
                          "result" => ($sResult == "successcheck" ? "successcheck" : "nocheck"),
                        ));
                        return true;
                    } elseif ($sAction == 'register') { //
                        echo $this->retSuccessWs(array(
                          "result" => "noregister",
                        ));
                        return true;
                    }
                }
                echo $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError());
                return true;
            }

        } catch (Exception $e) {
            echo $this->retErrorWs("555", $e->getMessage());
            return true;
        }
    }

    /**
     * Replaces Website service Aba_Wsdl::registerUserPartner()
     */
    public function actionSignupPartner()
    {
        //TODO: @abaenglish: Hides irritating PHP notice
        error_reporting(E_ALL ^ E_NOTICE);

        HeMixed::securePublicUrlRequest("external/signuppartner");
        $this->layout = "none";

        $postArgs = HeMixed::getPostArgs();

        // TODO: @abaenglish: Implement com security issues with signature
        //$signature = $postArgs['signature'];
        $name = $this->getValueByAlias('name', $postArgs, '', false);
        $surname = $this->getValueByAlias('surname', $postArgs, '', false);
        $email = $this->getValueByAlias('email', $postArgs, '', false);
        $password = $this->getValueByAlias('password', $postArgs, '', false);
        $telephone = $this->getValueByAlias('telephone', $postArgs, '', false);
        $voucherCode = $this->getValueByAlias('promocode', $postArgs, '', false); // code1
        $securityCode = $this->getValueByAlias('code2', $postArgs);
        $partnerId = $this->getValueByAlias('partnerId', $postArgs, DEFAULT_PARTNER_ID);
        $periodId = $this->getValueByAlias('idProduct', $postArgs, '', false);  // months from prod_periodicity
        $idCountry = $this->getValueByAlias('idCountry', $postArgs, DEFAULT_COUNTRY_ID);
        $device = $this->getValueByAlias('device', $postArgs);
        $langEnv = $this->getValueByAlias('langEnv', $postArgs);

        $stLandingParams = array(
          'server' => $_SERVER,
          'post' => $postArgs,
          'identifier' => "actionSignupPartner",
        );
        HeMixed::trackLandings($stLandingParams);

        try {
            $rulerWs = new RegisterUserService();
            $retUser = $rulerWs->registerUserPartner($email, $password, $langEnv, $name, $surname,
              $telephone, $voucherCode, $securityCode, $partnerId, $periodId, $idCountry, $device);
            if (!$retUser) {
                echo $this->retErrorWs($rulerWs->getErrorCode(),
                  $rulerWs->getLastError());
                return true;
            } elseif ($rulerWs->getLastError() !== "") {
                echo $this->retSuccessWs(array(
                  "userId" => $retUser->getId(),
                  "result" => "success with warning: " . $rulerWs->getLastError(),
                  "redirectUrl" => $rulerWs->getRedirectUrl($retUser, "", "", "HOME", true, $partnerId)
                ));
                return true;
            } else {
                echo $this->retSuccessWs(array(
                  "userId" => $retUser->getId(),
                  "result" => "success",
                  "redirectUrl" => $rulerWs->getRedirectUrl($retUser, "", "", "HOME", true, $partnerId)
                ));
                return true;
            }

        } catch (Exception $e) {
            echo $this->retErrorWs("555", $e->getMessage());
        }
    }

    protected function retSuccessWs($aResponse)
    {
        return json_encode($aResponse);
    }

    protected function retErrorWs($code = null, $additionalErrorMsg = "")
    {
        if (!isset($code)) {
            $code = "555"; // generic error code
        }

        $aResponse = array("result" => $code, "details" => self::$aErrorCodes[$code] . " - " . $additionalErrorMsg);
        return json_encode($aResponse);
    }

    protected static $aErrorCodes = array(
      "410" => "The purchase receipt is not valid",
      "555" => "Unidentified error",
      "501" => "Invalid Parameter Value",
      "502" => "Invalid Signature",
      "503" => "Operation not completed",
        // Detailed and customized errors, referenced from outside this class:
      "504" => "User does not exist",
      "505" => "Selligent communications error",
      "506" => "User could not be created",
      "507" => "User email is empty",
      "508" => "Product not valid",
      "509" => "Voucher is not valid. Probably it does not exist, or maybe it applies to another product",
      "510" => "User registered, but error on GROUPON CODE update.",
      "511" => "User could not be updated",
      "512" => "Promocode not valid",
      "513" => "Country Id not valid. Check its value on your call please.",
      "514" => "Voucher already used. The code probably has been validated: ",
      "515" => "Payment not valid.",
      "516" => "Payment gateway did not allow transaction.",
      "517" => "Payment is too old to be refund. Ask IT.",
      "518" => "Change of credit form in subscription not allowed. ",
      "519" => "User already exists and is Premium",
      "520" => "teacher email not valid",
      "521" => "Invalid Level",
      "522" => "Password is not valid.",
      "523" => "The facebook id is empty or is not valid.",
      "524" => "User already exists. Gift codes can only be activated by unregistered users.",
      "556" => "All not valid products",
      "557" => "Operation completed with errors",
    );

    public function filters()
    {
        return array(
          array('application.filters.CORSFilterNoToken')
        );
    }

    public function isAKnownParameterOrAlias($name)
    {
        foreach ($this->stFieldsNamesAliases as $sFieldName => $stFieldNameAliases) {
            if (in_array($name, $stFieldNameAliases)) {
                return true;
            }
        }
        return false;
    }
}
