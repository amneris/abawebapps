<?php

class PlansController extends AbaController
{
    /**
     * Declares class-based actions.
     */

    public $data = array();

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */

    public function actionIndex()
    {
        $this->redirect(Yii::app()->createUrl("site/index"));
    }
    
    public function actionAbaPremium()
    {
        $this->layout = "mainColumn1";
        $this->render('abapremium');
    }
    
    public function actionAbaForever()
    {
        $this->layout = "mainColumn1";
        $this->render('abaforever');        
    }
    
    public function actionPlans()
    {
        $this->render('plans');  
    }
}

