<?php
class ExternalController extends AbaController
{
    CONST SECRET_WSDL_KEY       = 'Yii ABA English Key 2';

    /**
     * Replaces Website service Aba_Wsdl::registerUser()
     */
    public function actionSignup()
    {
        //TODO: @abaenglish: Hides irritating PHP notice
        error_reporting(E_ALL ^ E_NOTICE);

        HeMixed::securePublicUrlRequest("external/signup");
        $this->layout = "none";

        $postArgs = HeMixed::getPostArgs();

        // TODO: @abaenglish: Implement com security issues with signature
        //$signature = $postArgs['signature'];
        $name = $postArgs['name'];
        $email = $postArgs['email'];
        $pass = $postArgs['password'];
        $surname = empty($postArgs['surname'])? '' : $postArgs['surname'];
        $langEnv = empty($postArgs['langEnv'])? '' : $postArgs['langEnv'];
        $idProduct = empty($postArgs['product_id'])? '' : $postArgs['product_id'];
        $promocode = empty($postArgs['promocode'])? '' : $postArgs['promocode'];
        $landingPage = empty($idProduct) ? 'HOME' : 'PAYMENT';
        $idCountry = empty($postArgs['idCountry']) ? '301': $postArgs['idCountry'];
        $partnerId = empty($postArgs['partnerId']) ? '300001' : $postArgs['partnerId'];
        $idSourceList = empty($postArgs['idSource'])? NULL : $postArgs['idSource'];
        $device = empty($postArgs['device'])? '' : $postArgs['device'];
        $currentLevel = empty($postArgs['currentLevel']) ? NULL : $postArgs['currentLevel'];
        $scenario = empty($postArgs['scenario'])? '' : $postArgs['scenario'];

        try
        {
            if (!isset($partnerId)) {
                $partnerId = Yii::app()->config->get("ABA_PARTNERID");
            }

            $commProducts = new ProductsPricesCommon();
            if (trim($idProduct) !== "" && !$commProducts->checkExistsIdProduct($idProduct)) {
                echo $this->retErrorWs("508", " Id product " . $idProduct);
            }
            if ($landingPage !== 'HOME' && $landingPage !== 'PAYMENT') {
                echo $this->retErrorWs("501", " LadingPage " . $landingPage . " should be HOME or PAYMENT.");
            }
            $moCountry = new AbaCountry();
            if ($idCountry == '' || !$moCountry->getCountryById($idCountry)) {
                echo $this->retErrorWs("513", " Country Id($idCountry) in registerUser ".
                  "should contain a valid value.");
            }
            if (trim($promocode) !== "" && !$commProducts->checkExistsPromocode($promocode)) {
                $promocode = "";
            }

            $rulerWs = new RegisterUserService();
            $retUser = $rulerWs->registerUser($email, $pass, $langEnv, $idCountry, $name, $surname, $currentLevel,
              $partnerId, $idSourceList, $device, null, $scenario);

            if ($retUser) {
                if ($retUser->langEnv == '') {
                    echo $this->retErrorWs("503", "Language not set.");
                }

                if ($landingPage == 'HOME') {
                    $landingPage = 'HOME_REGISTER';
                }

                echo $this->retSuccessWs(array("userId" => $retUser->getId(),
                  "result" => "success",
                  "redirectUrl" => $rulerWs->getRedirectUrl($retUser, $idProduct, $promocode,
                    $landingPage, true, $partnerId)));

            } else {
                echo $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError());
            }


        }catch(Exception $e)
        {
            echo $e->getMessage();
        }
    }

    /**
     * Replaces Website service Aba_Wsdl::registerUserObjectives()
     */
    public function actionSignupObjectives()
    {
        //TODO: @abaenglish: Hides irritating PHP notice
        error_reporting(E_ALL ^ E_NOTICE);

        HeMixed::securePublicUrlRequest("external/signupobjectives");
        $this->layout = "none";

        $postArgs = HeMixed::getPostArgs();

        // TODO: @abaenglish: Implement com security issues with signature
        //$signature = $postArgs['signature'];
        $email = $postArgs['email'];
        $pass = $postArgs['password'];
        $langEnv = empty($postArgs['langEnv'])? '' : $postArgs['langEnv'];
        $name = $postArgs['name'];
        $surname = $postArgs['surname'];
        $idProduct = empty($postArgs['idProduct'])? '' : $postArgs['idProduct'];
        $promocode = empty($postArgs['promocode'])? '' : $postArgs['promocode'];
        $landingPage = empty($idProduct) ? 'HOME' : 'PAYMENT';
        $idCountry = empty($postArgs['idCountry']) ? '301': $postArgs['idCountry'];
        $partnerId = empty($postArgs['partnerId']) ? '300001' : $postArgs['partnerId'];
        $idSourceList = empty($postArgs['idSourceList'])? NULL : $postArgs['idSourceList'];
        $device = empty($postArgs['device'])? '' : $postArgs['device'];
        $currentLevel = empty($postArgs['currentLevel']) ? NULL : $postArgs['currentLevel'];
        $scenario = empty($postArgs['scenario'])? '' : $postArgs['scenario'];
        $objective['viajar'] = (isset($postArgs['viajar']) ? $postArgs['viajar'] : null);
        $objective['estudiar'] = (isset($postArgs['estudiar']) ? $postArgs['estudiar'] : null);
        $objective['trabajar'] = (isset($postArgs['trabajar']) ? $postArgs['trabajar'] : null);
        $objective['otros'] = (isset($postArgs['otros']) ? $postArgs['otros'] : null);
        $objective['nivel'] = (isset($postArgs['nivel']) ? $postArgs['nivel'] : null);
        $objective['selEngPrev'] = (isset($postArgs['selEngPrev']) ? $postArgs['selEngPrev'] : null);
        $objective['engPrev'] = (isset($postArgs['engPrev']) ? $postArgs['engPrev'] : null);
        $objective['dedicacion'] = (isset($postArgs['dedicacion']) ? $postArgs['dedicacion'] : null);
        $objectives = json_encode($objective);

        try
        {
            if (!isset($partnerId)) {
                $partnerId = Yii::app()->config->get("ABA_PARTNERID");
            }

            $commProducts = new ProductsPricesCommon();
            if (trim($idProduct) !== "" && !$commProducts->checkExistsIdProduct($idProduct)) {
                echo $this->retErrorWs("508", " Id product " . $idProduct);
            }
            if ($landingPage !== 'HOME' && $landingPage !== 'PAYMENT') {
                echo $this->retErrorWs("501", " LadingPage " . $landingPage . " should be HOME or PAYMENT.");
            }
            $moCountry = new AbaCountry();
            if ($idCountry == '' || !$moCountry->getCountryById($idCountry)) {
                echo $this->retErrorWs("513", " Country Id($idCountry) in registerUser ".
                  "should contain a valid value.");
            }
            // TODO: abaenglish: Why they don't check promocode here
            /*if (trim($promocode) !== "" && !$commProducts->checkExistsPromocode($promocode)) {
              $promocode = "";
            }*/

            $rulerWs = new RegisterUserService();
            $retUser = $rulerWs->registerUser($email, $pass, $langEnv, $idCountry, $name, $surname, $currentLevel,
              $partnerId, $idSourceList, $device, null, $scenario);

            if ($retUser) {
                if ($retUser->langEnv == '') {
                    echo $this->retErrorWs("503", "Language not set.");
                }

                /*if ($landingPage == 'HOME_LEVEL_UNIT') {
                  $landingPage = 'HOME_LEVEL_UNIT';
                }*/

                $bObjectivesSuccess = true;

                if(trim($objectives) != '') {

                    $aObjectives = json_decode($objectives, true);

                    $userObjectives = new AbaUserObjective();

                    if(!$userObjectives->saveFormObjectivesWs($retUser, $aObjectives)) {
                        $bObjectivesSuccess = false;
                    }
                }

                $sRedirectUrl = $rulerWs->getRedirectUrl($retUser, $idProduct, $promocode, 'OBJECTIVES', true, $partnerId);

                if(!HeMixed::isValidLevel($currentLevel)) {
                    echo $this->retSuccessWs(array(
                      "userId" =>             $retUser->getId(),
                      "result" =>             "success with errors: " . "Selected level is invalid",
                      "redirectUrl" =>        $sRedirectUrl,
                      "resultObjectives" =>   ($bObjectivesSuccess ? "success" : "Some error has ocurred while inserting/updating your questionnaire.")
                    ));
                }
                else{
                    echo $this->retSuccessWs(array(
                      "userId" =>             $retUser->getId(),
                      "result" =>             "success",
                      "redirectUrl" =>        $sRedirectUrl,
                      "resultObjectives" =>   ($bObjectivesSuccess ? "success" : "Some error has ocurred while inserting/updating your questionnaire.")
                    ));
                }

            } else {
                echo $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError());
            }


        }catch(Exception $e)
        {
            echo $e->getMessage();
        }
    }

    /**
     * Replaces Website service Aba_Wsdl::registerUserFacebook()
     */
    public function actionSignupFacebook()
    {
        //TODO: @abaenglish: Hides irritating PHP notice
        error_reporting(E_ALL ^ E_NOTICE);

        HeMixed::securePublicUrlRequest("external/signupfacebook");
        $this->layout = "none";

        $postArgs = HeMixed::getPostArgs();

        // TODO: abaenglish: Implement com security issues with signature
        //$signature = $postArgs['signature'];
        $sAction = $postArgs['sAction'];
        $sEmail = $postArgs['sEmail'];
        $sPass = $postArgs['sPass'];
        $sName = $postArgs['sName'];
        $sSurname = $postArgs['sSurname'];
        $sFbid = $postArgs['sFbid'];
        $sGender = $postArgs['sGender'];
        $sEmailFacebook = $postArgs['sEmailFacebook'];
        $langEnv = empty($postArgs['langEnv'])? '' : $postArgs['langEnv'];
        $idProduct = empty($postArgs['idProduct'])? '' : $postArgs['idProduct'];
        $promocode = empty($postArgs['promocode'])? '' : $postArgs['promocode'];
        $landingPage = empty($idProduct) ? 'HOME' : 'PAYMENT';
        $idCountry = empty($postArgs['idCountry']) ? '301': $postArgs['idCountry'];
        $partnerId = empty($postArgs['partnerId']) ? '300001' : $postArgs['partnerId'];
        $idSourceList = empty($postArgs['idSourceList'])? NULL : $postArgs['idSourceList'];
        $device = empty($postArgs['device'])? '' : $postArgs['device'];
        $currentLevel = empty($postArgs['currentLevel']) ? NULL : $postArgs['currentLevel'];
        $scenario = empty($postArgs['scenario'])? '' : $postArgs['scenario'];

        try
        {
            // Validation of web service action
            if ($sAction !== 'exists' && $sAction !== 'check' && $sAction !== 'register') {
                echo $this->retErrorWs("503", "Invalid action");
            }

            $userCommon =   new UserCommon();
            $rulerWs =      new RegisterUserService();

            $sResult = '';
            $retUser = false;

            if($sAction == 'exists') { // existing user

                $retUser = $userCommon->checkIfFbUserExists($sFbid);

                if($retUser) {
                    $sResult = "successexists";
                }
            }
            elseif($sAction == 'check' && $userCommon->checkIfChangedEmailExists($sEmail, $sEmailFacebook)) { // new registered user
                $sResult = "successcheck";
            }
            elseif($sAction == 'register') { // check email

                $form = new LoginForm();
                $form->attributes = array('email' => $sEmail, 'password' => $sPass, 'rememberMe' => 0);

                if ($form->validate()) {
                    //
                    // Update any user data
                    $retUser = new AbaUser();
                    $retUser->getUserByEmail($sEmail);

                    $sResult = "successregister";
                    $userCommon->changeUserFbData( $retUser, $sName, $sSurname, $sFbid, $sGender);
                }
                else {
                    echo $this->retErrorWs( "504", ''); // 522
                }
            }
            else {

                if (!isset($partnerId)) {
                    $partnerId = Yii::app()->config->get("ABA_PARTNERID");
                }

                $commProducts = new ProductsPricesCommon();
                if (trim($idProduct) !== "" && !$commProducts->checkExistsIdProduct($idProduct)) {
                    echo $this->retErrorWs("508", " Id product " . $idProduct);
                }
                if ($landingPage !== 'HOME' && $landingPage !== 'PAYMENT') {
                    echo $this->retErrorWs("501", " LadingPage " . $landingPage . " should be HOME or PAYMENT.");
                }
                $moCountry = new AbaCountry();
                if ($idCountry == '' || !$moCountry->getCountryById($idCountry)) {
                    echo $this->retErrorWs("513", " Country Id($idCountry) in registerUser should contain a valid value.");
                }

                if ($landingPage == 'HOME') {
                    $landingPage = 'HOME_REGISTER';
                }

                $sResult = "successregister";

                $retUser = $rulerWs->registerUserFacebook($sEmail, $sName, $sSurname, $sFbid, $sGender, $langEnv, $idCountry, $partnerId, $idSourceList, $device, $currentLevel, $scenario);
            }

            if ($retUser) {

                // ---------------------------------------------
                // Transform some parameters: +++++++++++++++++++++++++++++++++++++++
                if (trim($promocode) == "undefined") {
                    $promocode = "";
                }
                if (trim($promocode) !== "" && !$commProducts->checkExistsPromocode($promocode)) {
                    $promocode = "";
                }
                //-----------------------------------------------

                echo $this->retSuccessWs(array(
                  "fbId" =>           $sFbid,
                  "userId" =>         $retUser->getId(),
                  "email" =>          $retUser->email,
                  "result" =>         $sResult,
                  "redirectUrl" =>    $rulerWs->getRedirectUrl($retUser, $idProduct, $promocode, $landingPage, true, $partnerId),
                ));
            } else {

                if ($rulerWs->getLastError() !== "" && $rulerWs->getLastError() !== null) {
                    echo $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError());
                }
                else {
                    if($sAction == 'exists') { // USER NO EXISTS
                        echo $this->retSuccessWs(array(
                          "result" => "noexists",
                        ));
                    }
                    elseif($sAction == 'check') { //
                        echo $this->retSuccessWs(array(
                          "result" => ($sResult == "successcheck" ? "successcheck" : "nocheck"),
                        ));
                    }
                    elseif($sAction == 'register') { //
                        echo $this->retSuccessWs(array(
                          "result" => "noregister",
                        ));
                    }
                }
                echo $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError());
            }

        }catch(Exception $e)
        {
            echo $this->retErrorWs( "555", $e);
        }
    }

    /**
     * Replaces Website service Aba_Wsdl::registerUserLinkedin()
     */
    public function actionSignupLinkedin()
    {
        //TODO: @abaenglish: Hides irritating PHP notice
        error_reporting(E_ALL ^ E_NOTICE);

        HeMixed::securePublicUrlRequest("external/signuplinkedin");
        $this->layout = "none";

        $postArgs = HeMixed::getPostArgs();

        // TODO: abaenglish: Implement com security issues with signature
        //$signature = $postArgs['signature'];
        $sAction = $postArgs['sAction'];
        $sEmail = $postArgs['sEmail'];
        $sPass = $postArgs['sPass'];
        $sName = $postArgs['sName'];
        $sSurname = $postArgs['sSurname'];
        $sINid = $postArgs['sINid'];
        $sGender = $postArgs['sGender'];
        $sEmailLinkedin = $postArgs['sEmailLinkedin'];
        $langEnv = empty($postArgs['langEnv'])? '' : $postArgs['langEnv'];
        $idCountry = empty($postArgs['idCountry']) ? '301': $postArgs['idCountry'];
        $promocode = empty($postArgs['promocode'])? '' : $postArgs['promocode'];
        $landingPage = empty($idProduct) ? 'HOME' : 'PAYMENT';
        $idCountry = empty($postArgs['idCountry']) ? '301': $postArgs['idCountry'];
        $partnerId = empty($postArgs['partnerId']) ? '300001' : $postArgs['partnerId'];
        $idSourceList = empty($postArgs['idSourceList'])? NULL : $postArgs['idSourceList'];
        $device = empty($postArgs['device'])? '' : $postArgs['device'];
        $currentLevel = empty($postArgs['currentLevel']) ? NULL : $postArgs['currentLevel'];
        $scenario = empty($postArgs['scenario'])? '' : $postArgs['scenario'];

        try
        {
            // Validation of web service action
            if ($sAction !== 'exists' && $sAction !== 'check' && $sAction !== 'register') {
                echo $this->retErrorWs("503", "Invalid action");
            }

            $userCommon =   new UserCommon();
            $rulerWs =      new RegisterUserService();

            $sResult = '';
            $retUser = false;

            if($sAction == 'exists') { // existing user

                $retUser = $userCommon->checkILinkedinUserExists($sINid);

                if($retUser) {
                    $sResult = "successexists";
                }
            }
            elseif($sAction == 'check' && $userCommon->checkIfChangedEmailExists($sEmail, $sEmailLinkedin)) { // new registered user
                $sResult = "successcheck";
            }
            elseif($sAction == 'register') { // check email

                $form = new LoginForm();
                $form->attributes = array('email' => $sEmail, 'password' => $sPass, 'rememberMe' => 0);

                if ($form->validate()) {
                    //
                    // Update any user data
                    $retUser = new AbaUser();
                    $retUser->getUserByEmail($sEmail);

                    $sResult = "successregister";
                    $userCommon->changeUserinData( $retUser, $sName, $sSurname, $sINid, $sGender);
                }
                else {
                    echo $this->retErrorWs( "504", ''); // 522
                }
            }
            else {

                if (!isset($partnerId)) {
                    $partnerId = Yii::app()->config->get("ABA_PARTNERID");
                }

                $commProducts = new ProductsPricesCommon();
                if (trim($idProduct) !== "" && !$commProducts->checkExistsIdProduct($idProduct)) {
                    echo $this->retErrorWs("508", " Id product " . $idProduct);
                }
                if ($landingPage !== 'HOME' && $landingPage !== 'PAYMENT') {
                    echo $this->retErrorWs("501", " LadingPage " . $landingPage . " should be HOME or PAYMENT.");
                }
                $moCountry = new AbaCountry();
                if ($idCountry == '' || !$moCountry->getCountryById($idCountry)) {
                    echo $this->retErrorWs("513", " Country Id($idCountry) in registerUser should contain a valid value.");
                }

                if ($landingPage == 'HOME') {
                    $landingPage = 'HOME_REGISTER';
                }

                $sResult = "successregister";

                $retUser = $rulerWs->registerUserLinkedin($sEmail, $sName, $sSurname, $sINid, $sGender, $langEnv,
                  $idCountry, $partnerId, $idSourceList, $device, $currentLevel, $scenario);
            }

            if ($retUser) {

                // ---------------------------------------------
                // Transform some parameters: +++++++++++++++++++++++++++++++++++++++
                if (trim($promocode) == "undefined") {
                    $promocode = "";
                }
                if (trim($promocode) !== "" && !$commProducts->checkExistsPromocode($promocode)) {
                    $promocode = "";
                }
                //-----------------------------------------------

                echo $this->retSuccessWs(array(
                  "sINid" =>          $sINid,
                  "userId" =>         $retUser->getId(),
                  "email" =>          $retUser->email,
                  "result" =>         $sResult,
                  "redirectUrl" =>    $rulerWs->getRedirectUrl($retUser, $idProduct, $promocode, $landingPage, true, $partnerId),
                ));
            } else {

                if ($rulerWs->getLastError() !== "" && $rulerWs->getLastError() !== null) {
                    echo $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError());
                }
                else {
                    if($sAction == 'exists') { // USER NO EXISTS
                        echo $this->retSuccessWs(array(
                          "result" => "noexists",
                        ));
                    }
                    elseif($sAction == 'check') { //
                        echo $this->retSuccessWs(array(
                          "result" => ($sResult == "successcheck" ? "successcheck" : "nocheck"),
                        ));
                    }
                    elseif($sAction == 'register') { //
                        echo $this->retSuccessWs(array(
                          "result" => "noregister",
                        ));
                    }
                }
                echo $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError());
            }

        }catch(Exception $e)
        {
            $this->retErrorWs( "555", $e);
        }
    }

    /**
     * Replaces Website service Aba_Wsdl::registerUserPartner()
     */
    public function actionSignupPartner()
    {
        //TODO: @abaenglish: Hides irritating PHP notice
        error_reporting(E_ALL ^ E_NOTICE);

        HeMixed::securePublicUrlRequest("external/signuppartner");
        $this->layout = "none";

        $postArgs = HeMixed::getPostArgs();

        // TODO: @abaenglish: Implement com security issues with signature
        //$signature = $postArgs['signature'];
        $name = $postArgs['name'];
        $surname = $postArgs['surname'];
        $email = $postArgs['email'];
        $password = $postArgs['password'];
        $telephone = $postArgs['telephone'];
        $voucherCode = $postArgs['code1'];
        $securityCode = empty($postArgs['code2']) ? '' : $postArgs['code2'];
        $partnerId = empty($postArgs['idPartner']) ? '300001' : $postArgs['idPartner'];
        $periodId = $postArgs['productID'];
        $idCountry = empty($postArgs['countryCode']) ? '301': $postArgs['countryCode'];
        $device = empty($postArgs['device'])? '' : $postArgs['device'];
        $langEnv = empty($postArgs['langEnv'])? '' : $postArgs['langEnv'];

        try
        {
            $rulerWs = new RegisterUserService();
            $retUser = $rulerWs->registerUserPartner($email, $password, $langEnv, $name, $surname,
              $telephone, $voucherCode, $securityCode, $partnerId, $periodId, $idCountry, $device);
            if (!$retUser) {
                echo $this->retErrorWs($rulerWs->getErrorCode(),
                  $rulerWs->getLastError());
            } elseif ($rulerWs->getLastError() !== "") {
                echo $this->retSuccessWs(array("userId" => $retUser->getId(),
                  "result" => "success with warning: " . $rulerWs->getLastError(),
                  "redirectUrl" => $rulerWs->getRedirectUrl($retUser, "", "", "HOME", true, $partnerId)));
            } else {
                echo $this->retSuccessWs(array("userId" => $retUser->getId(),
                  "result" => "success",
                  "redirectUrl" => $rulerWs->getRedirectUrl($retUser, "", "", "HOME", true, $partnerId)));
            }

        }catch(Exception $e)
        {
            echo $this->retErrorWs( "555", $e);
        }
    }

    protected function retSuccessWs ($aResponse)
    {
        return json_encode($aResponse);
    }

    protected function retErrorWs( $code=NULL, $additionalErrorMsg="" )
    {
        if (!isset($code)) {
            $code = "555"; // generic error code
        }

        $aResponse = array("result"=>$code, "details"=> self::$aErrorCodes[$code] . " - " . $additionalErrorMsg);
        return json_encode($aResponse);
    }

    protected static $aErrorCodes = array(
      "410"=> "The purchase receipt is not valid",
      "555"=> "Unidentified error",
      "501"=> "Invalid Parameter Value",
      "502"=> "Invalid Signature",
      "503"=> "Operation not completed",
        // Detailed and customized errors, referenced from outside this class:
      "504"=> "User does not exist",
      "505"=> "Selligent communications error",
      "506"=> "User could not be created",
      "507"=> "User email is empty",
      "508"=> "Product not valid",
      "509"=> "Voucher is not valid. Probably it does not exist, or maybe it applies to another product",
      "510"=> "User registered, but error on GROUPON CODE update.",
      "511"=> "User could not be updated",
      "512"=> "Promocode not valid",
      "513"=> "Country Id not valid. Check its value on your call please.",
      "514"=> "Voucher already used. The code probably has been validated: ",
      "515"=> "Payment not valid.",
      "516"=> "Payment gateway did not allow transaction.",
      "517"=> "Payment is too old to be refund. Ask IT.",
      "518"=> "Change of credit form in subscription not allowed. ",
      "519"=> "User already exists and is Premium",
      "520"=> "teacher email not valid",
      "521"=> "Invalid Level",
      "522"=> "Password is not valid.",
      "523"=> "The facebook id is empty or is not valid.",
      "524"=> "User already exists. Gift codes can only be activated by unregistered users.",
      "556"=> "All not valid products",
      "557"=> "Operation completed with errors",
    );

    public function filters()
    {
        return array(
          array('application.filters.CORSFilterNoToken')
        );
    }
}