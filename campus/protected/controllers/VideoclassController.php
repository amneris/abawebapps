<?php

class VideoclassController extends AbaController
{
    /**
     * Declares class-based actions.
     */

    public function actions()
    {
        return array(
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function actionIndex()
    {
        if( !HeRoles::allowOperation( OP_VIDEOCLASSES ) )
        {
            $this->redirect( $this->createUrl('site/noaccess') );
            return;
        }

        $units = new VideoClasses();
        $tmpLevel = ( !is_null(HeMixed::getPostArgs('level')) && (HeMixed::isValidLevel(HeMixed::getPostArgs('level')))) ? HeMixed::getPostArgs('level') : Yii::app()->user->currentLevel;
        /* @var VideoClass[] $aUnitsObject */
        $aUnitsObject = $units->getAllUnitsByLevel($tmpLevel);
        $aUnits = array();

        // Find out which is the last video-class the user has access to:
        $userId = Yii::app()->user->getId();
        $user = new AbaUser();
        $user->getUserById($userId);

        $aTeachers = HeUnits::getTeachersCountries();

        /*  @var VideoClass $unitCourse */
        /*  @var integer $unitId */
        foreach( $aUnitsObject as  $unitId=>$unitCourse )
        {
            $aUnits['unit'][$unitId] = $unitCourse->getFormatUnitId($unitId);
            $aUnits['title'][$unitId] = $unitCourse->getTitleTrans();
            $aUnits['teacherName'][$unitId] = $unitCourse->getTeacherVideo();
            $followUpCourse = new AbaFollowup();
            $aUnits["VideoClassProgress"][$unitId] = 0;
            if($followUpCourse->getFollowupByIds( Yii::app()->user->getId(),  $unitId ))
            {
                $aUnits["VideoClassProgress"][$unitId] = $followUpCourse->gra_vid;
            }
        }
        // Find out bounds for this level
        $firstUnit = $units->getStartUnitByLevel($tmpLevel);
        $lastUnit = $units->getEndUnitByLevel($tmpLevel);


        $this->render('index', array("aUnits" => $aUnits, "firstUnit" => $firstUnit, "lastUnit" => $lastUnit, 'aTeachers' => $aTeachers));
    }

    /**
     * @param integer $unit
     */
    public function actionAllUnits($unit)
    {
        if( !HeRoles::allowOperation( OP_VIDEOCLASSES ) )
        {
            $this->redirect( $this->createUrl('site/noaccess') );
        }

        if (trim($unit) == '') {
            $this->redirect($this->createUrl('course/index'));
        }

        $userId = Yii::app()->user->getId();
        $user = new AbaUser();
        $user->getUserById($userId);
        $userLang = $user->langEnv;
        $isB2B = Yii::app()->user->isB2B();

        $userTypeAccessUnit = HeRoles::typeOfVideoClassAccess(null, intval($unit));

        /* special logic for A / B / C testing */
        $firstUnits = HeUnits::getArrayStartUnitsByLevel();
        if(is_array($firstUnits) && in_array($unit, $firstUnits)) {
            $moCourse = new UnitsCourses();
            if ($course = $moCourse->getUserCourse($userId, $userLang)) {
                if (!empty($course['description'])) {
                    $unit = $unit . '_' . $course['description'];
                }
            }
        }

        switch($userTypeAccessUnit)
        {
            case OP_INCOMPLETEUNIT:

                $this->render('allUnits/index', array('unit' => $unit,
                    'userTypeAccessUnit' => OP_INCOMPLETEUNIT,
                    'type' => 'video',
                    'isB2B' => $isB2B)
                );

                break;

            case OP_FULLUNIT:

                $teacherImg = '';
                if(isset($user->teacherId)) {
                    $moTeacher = new AbaTeacher();
                    $moTeacher = $moTeacher->getTeacherById($user->teacherId);
                    if(isset($moTeacher->photo)) {
                        $teacherImg = $moTeacher->photo;
                    }
                }

                $this->render('allUnits/index', array('unit' => $unit,
                    'userTypeAccessUnit'=> OP_FULLUNIT,
                    'type'=>'video',
                    'isB2B' => $isB2B,
                    'teacherImg' => $teacherImg)
                );

                break;

            case OP_NOACTIONACCESSUNIT:
                $this->redirect( $this->createUrl('videoclass/index') );
                break;

            default:

                $this->render('allUnits/index', array('unit' => $unit,
                    'userTypeAccessUnit'=> 0,
                    'type' => 'video')
                );

                break;
        }

        return;
    }

}