<?php
/**
 * IMPORTANT, DO NOT MODIFY THE PHP DOC COMMENTS ON THE HEADER OF EVERY FUNCTION.
 * IT IS USED BY Yii IN ORDER TO CREATE WSDL.FORBIDDEN!
 */
class WsoapabaController extends WsController
{
   static private $_version = "1.29";

   static private $_aHistoryVersion = array(
      "1.0"=> "Creation of RegisterUser",
      "1.1"=> "Creation of RegisterUserPremiumPartners",
      "1.2"=> "Version control added",
      "1.3"=> "Return a new element in all web services called redirectUrl",
      "1.4"=> "New web service registerUserFromLevelTest",
      "1.5"=> "New param in service registerUserFromLevelTest",
      "1.6"=> "Change param productId for periodId in registerUserPremiumPartners",
      "1.7"=> "Encryption of all passwords, the ones received and the ones auto-created",
      "1.8"=> "Added 3 new params IdProduct, PromoCode and Landing Page to registerUser.",
      "1.9"=> "For registerUserLevelTest the element -returnURL- returns an URL redirecting to the home page.",
      "1.10"=> "For registerUserLevelTest returns a a new element password.",
      "1.11"=> "For registerUserLevelTest adds a new parameter called idCountry.",
      "1.12"=> "For registerUser adds a new parameter called idCountry.",
      "1.13"=> "For registerUser Promocode if present, we do not return error if it is not valid.",
      "1.14"=> "For registerUser and registerUserLevelTest we add idPartner",
      "1.15"=> "Url returned to client has a new param called idpartner for registerUser and registerUserFromLevelTest",
      "1.16"=> "For registerUserLevelTest LEAD users are turned into PREMIUM users.",
      "1.17"=> "Url returned to client has a new param called idpartner for registerUserFromPartner",
      "1.18"=> "New optional param called [referenceCodeBr] for registerUserFromPartner. Just for Brasil.",
      "1.19"=> "New element returned for registerUserFromLevelTest called IsNewUser, possible values 0/1",
      "1.20"=> "New periodicity in groupones tables for 2 months.",
      "1.21"=> "Remove referenceCode for Brasil Groupon. Special behaviour was not used.",
      "1.22"=> "More error codes for registerUserFromPartner.",
      "1.23"=> "New web service registerUserAffiliate.",
      "1.24"=> "New parameter in all webservices: device",
      "1.25"=> "New web service registerUserPremiumB2b",
      "1.26"=> "New web service registerUserFacebook",
      "1.27"=> "New web service registerUserObjectives",
      "1.28"=> "New web service loginUserExternal",
      "1.29"=> "New web service recoverPassword"
   );

    public function actions()
    {
        return array(
            'usersapi' => array(
                'class' => 'AbaWebServiceAction',
            ),
        );
    }

    /**
     * Returns the version of this API. element one is [version] and second is [description].
     * @return array The array has format [version,description]
     * @soap
     */
    public function version()
    {
        return $this->retSuccessWs( array("version"=>self::$_version,
                    "description"=>self::$_aHistoryVersion[self::$_version] ) );
    }

    /* !! IMPORTANT, DO NOT MODIFY THE PHP DOC COMMENTS ON THE HEADER OF EVERY FUNCTION. ******************************
       * IT IS USED BY Yii IN ORDER TO CREATE WSDL. FORBIDDEN! ********************************************************
    */

    /**
     * Returns array of 2 elements: [userId] and [result] in case of success.
     *
     * @param string $signature
     * @param string $email
     * @param string $pwd
     * @param string $langEnv
     * @param string $name
     * @param string $surnames
     * @param string $idProduct
     * @param string $promocode
     * @param string $landingPage
     * @param integer $idCountry
     * @param integer $idPartner
     * @param integer $idSourceList Optional
     * @param string $device
     * @param integer $currentLevel
     * @param string $scenario
     *
     * @return array The array has the following format array("userId","result","redirectUrl")
     *
     * @soap
     */
    public function registerUser( $signature, $email, $pwd, $langEnv, $name, $surnames, $idProduct, $promocode,
      $landingPage, $idCountry, $idPartner=null, $idSourceList=NULL, $device=NULL, $currentLevel=NULL,
      $scenario='' )
    {

        try
        {
            // Validation of params for this web service  ++++++++++++++++++++++
            if (!$this->isValidSignature($signature, array($email, $pwd, $langEnv, $name, $surnames, $idProduct,
                $promocode, $landingPage, $idCountry, $idPartner, $idSourceList, $device, $currentLevel, $scenario))
            ) {
                return $this->retErrorWs("502");
            }
            if (!isset($idPartner)) {
                $idPartner = Yii::app()->config->get("ABA_PARTNERID");
            }

            $commProducts = new ProductsPricesCommon();
            if (trim($idProduct) !== "" && !$commProducts->checkExistsIdProduct($idProduct)) {
                return $this->retErrorWs("508", " Id product " . $idProduct);
            }
            if ($landingPage !== 'HOME' && $landingPage !== 'PAYMENT') {
                return $this->retErrorWs("501", " LadingPage " . $landingPage . " should be HOME or PAYMENT.");
            }
            $moCountry = new AbaCountry();
            if ($idCountry == '' || !$moCountry->getCountryById($idCountry)) {
                return $this->retErrorWs("513", " Country Id($idCountry) in registerUser ".
                                                                                "should contain a valid value.");
            }
            // ---------------------------------------------
            // Transform some parameters: +++++++++++++++++++++++++++++++++++++++
            if (trim($promocode) !== "" && !$commProducts->checkExistsPromocode($promocode)) {
                $promocode = "";
            }
            //-----------------------------------------------

            $rulerWs = new RegisterUserService();
            $retUser = $rulerWs->registerUser($email, $pwd, $langEnv, $idCountry, $name, $surnames, $currentLevel, $idPartner,
                $idSourceList, $device, null, $scenario);
            if ($retUser) {
                if ($retUser->langEnv == '') {
                    return $this->retErrorWs("503", "Language not set.");
                }

                if ($landingPage == 'HOME') {
                    $landingPage = 'HOME_REGISTER';
                }

                return $this->retSuccessWs(array("userId" => $retUser->getId(),
                    "result" => "success",
                    "redirectUrl" => $rulerWs->getRedirectUrl($retUser, $idProduct, $promocode,
                            $landingPage, true, $idPartner)));

            } else {
                return $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError());
            }
        }
        catch(Exception $ex)
        {
            error_log( $signature." error=> ".$ex->getMessage() );
            return $this->retErrorWs( "555", $ex/*"Internal error non identified. ".
                                        "Check with Aba IT Team and see logs Apache for ".$signature */);
        }
    }

    /* !! IMPORTANT, DO NOT MODIFY THE PHP DOC COMMENTS ON THE HEADER OF EVERY FUNCTION. ********************
    * IT IS USED BY Yii IN ORDER TO CREATE WSDL. FORBIDDEN! *************************************************
    */

    /** Registers a PREMIUM USER with a voucher from a partner.
     * Expects a new or existing user and returns an array with an id and success string in case of success;
     * or an array error if it fails. Please be sure to send the signature properly codified. Error codes:
     * "555"=> "Unidentified error", "502"=> "Invalid Signature", "503"=> "Operation not completed"
     *
     * @param string $signature
     * @param string $email
     * @param string $pwd
     * @param string $langEnv
     * @param string $name
     * @param string $surnames
     * @param string $telephone
     * @param string $voucherCode
     * @param string $securityCode
     * @param integer $partnerId
     * @param integer $periodId
     * @param integer $idCountry
     * @param string $device
     *
     * @return array The array when success has 3 elements: userId,result, redirectUrl
     * @soap
     */
    public function registerUserPartner( $signature, $email, $pwd, $langEnv, $name, $surnames, $telephone,
                                         $voucherCode, $securityCode, $partnerId, $periodId, $idCountry, $device=NULL)
    {
        try
        {
            if (!$this->isValidSignature($signature, array($email, $pwd, $langEnv, $name, $surnames,
                        $telephone, $voucherCode, $securityCode, strval($partnerId), $periodId, strval($idCountry),
                        $device))) {
                return $this->retErrorWs("502");
            }

            $rulerWs = new RegisterUserService();
            $retUser = $rulerWs->registerUserPartner($email, $pwd, $langEnv, $name, $surnames,
                                   $telephone, $voucherCode, $securityCode, $partnerId, $periodId, $idCountry, $device);
            if (!$retUser) {
                return $this->retErrorWs($rulerWs->getErrorCode(),
                    $rulerWs->getLastError());
            } elseif ($rulerWs->getLastError() !== "") {
                return $this->retSuccessWs(array("userId" => $retUser->getId(),
                    "result" => "success with warning: " . $rulerWs->getLastError(),
                    "redirectUrl" => $rulerWs->getRedirectUrl($retUser, "", "", "HOME", true, $partnerId)));
            } else {
                return $this->retSuccessWs(array("userId" => $retUser->getId(),
                    "result" => "success",
                    "redirectUrl" => $rulerWs->getRedirectUrl($retUser, "", "", "HOME", true, $partnerId)));
            }
        }
        catch(Exception $ex)
        {
            error_log( $signature." error=> ".$ex->getMessage() );
            return $this->retErrorWs( "555", "Internal error non identified for registerUserPartner.".
                                                    " Check with Aba IT Team and see logs Apache for ".$signature );
        }
    }

    /**
     * Expects a valid email, a language(en,es,fr,it,pt,de) and a level between 1 and 6. Returns an array of 5 elements
     * if successful.
     *
     * @param string $signature
     * @param string $email
     * @param string $langEnv
     * @param integer $currentLevel
     * @param bool $insideCampus
     * @param integer $idCountry
     * @param integer $idPartner
     * @param integer $idSourceList Optional
     * @param string $device Optional
     *
     * @return array The array when success has 4 elements: userId, password, result, redirectUrl, isNewUser
     * @soap
     */
    public function registerUserFromLevelTest( $signature, $email, $langEnv, $currentLevel, $insideCampus, $idCountry,
                                                                    $idPartner=NULL, $idSourceList=NULL, $device=NULL)
    {
        try
        {
            // Validations of parameters: +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            if (!$this->isValidSignature($signature, array($email, $langEnv, $currentLevel, $insideCampus, $idCountry,
                $idPartner, $idSourceList, $device))
            ) {
                return $this->retErrorWs("502");
            }
            if (!isset($idPartner)) {
                $idPartner = Yii::app()->config->get("LEVELTEST_PARTNERID");
            }
            $moCountry = new AbaCountry();
            if ($idCountry == '' || !$moCountry->getCountryById($idCountry)) {
                return $this->retErrorWs("513", " Country Id($idCountry) in ".
                                                            "registerUserFromLevelTest should contain a valid value.");
            }
            // --------------------------------------------------------------------------------------------------------

            $rulerWs = new RegisterUserService();
            $retUser = $rulerWs->registerUserFromLevelTest($email, $langEnv, $currentLevel, $insideCampus, $idCountry,
                $idPartner, $idSourceList, $device);
            if ($retUser) {
                return $this->retSuccessWs(array("userId" => $retUser->getId(),
                    "password" => $retUser->password,
                    "result" => "success",
                    "redirectUrl" => $rulerWs->getRedirectUrl($retUser, "", "", "HOME", true, $idPartner),
                    "isNewUser" => $retUser->isNewUser));
            } else {
                return $this->retErrorWs("503", $rulerWs->getLastError());
            }
        }
        catch(Exception $ex)
        {
            error_log( $signature." error=> ".$ex->getMessage() );
            return $this->retErrorWs( "555", "Internal error non identified. ".
                                        "Check with Aba IT Team and see logs Apache for ".$signature );
        }
    }


    /**
     * Returns array of 2 elements: [userId] and [result] in case of success.
     *
     * @param string  $signature
     * @param string  $email
     * @param string  $pwd
     * @param string  $langEnv
     * @param string  $name
     * @param string  $surnames
     * @param string  $landingPage
     * @param string  $ipFrom
     * @param integer $idPartner
     * @param integer $idSourceList Optional
     *
     * @return array The array has the following format array("userId","result","redirectUrl")
     * @soap
     */
    public function registerUserAffiliate( $signature, $email, $pwd, $langEnv, $name, $surnames, $landingPage, $ipFrom,
                                                                   $idPartner=null, $idSourceList=NULL )
    {
        try
        {
            // Validation of params for this web service  ++++++++++++++++++++++
            if (!$this->isValidSignatureAffiliate($signature, array($email, $pwd, $langEnv, $name, $surnames,
                                                            $landingPage, $ipFrom, $idPartner, $idSourceList))
            ) {
                return $this->retErrorWs("502");
            }

            if (!isset($idPartner)) {
                $idPartner = Yii::app()->config->get("ABA_PARTNERID");
            }

            if ($landingPage !== 'HOME' && $landingPage !== 'PAYMENT') {
                return $this->retErrorWs("501", " LadingPage " . $landingPage . " should be HOME or PAYMENT.");
            }

            if ( ip2long($ipFrom)=== false) {
                return $this->retErrorWs("501", "IP not valid.");
            }

            $idCountry = Yii::app()->config->get('ABACountryId');
            $moCountry = new AbaCountry();
            if ($moCountry->getCountryCodeByIP($ipFrom)) {
                $idCountry = $moCountry->getId();
            }

            $rulerWs = new RegisterUserService();
            $retUser = $rulerWs->registerUser($email, $pwd, $langEnv, $idCountry, $name, $surnames, 1, $idPartner,
                                                                                                     $idSourceList);
            if ($retUser) {
                if ($landingPage == 'HOME') {
                    $landingPage = 'HOME_REGISTER';
                }

                return $this->retSuccessWs(array("userId" => $retUser->getId(),
                    "result" => "success",
                    "redirectUrl" => $rulerWs->getRedirectUrl($retUser, "", "", $landingPage, true, $idPartner)));
            } else {
                return $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError());
            }

        }
        catch(Exception $ex)
        {
            error_log( $signature." error=> ".$ex->getMessage() );
            return $this->retErrorWs( "555", "Internal error non identified. ".
                "Check with Aba IT Team and see logs Apache for ".$signature );
        }
    }


    /**
     * @param string $signature
     * @param string $users
     *
     * @return array The array when success has 3 elements: userId, result, redirectUrl
     * @soap
     */
    public function registerUserPremiumB2bExtranet($signature, $users)
    {

        try
        {
            if (!$this->isValidSignatureExtranet($signature, array($users))) {
                return $this->retErrorWs("502");
            }

            $aUsers =           json_decode($users, true);
            $aUsersResponse =   array();
            $iTotalErrors =     0;

            foreach($aUsers as $iKey => $aUser) {

                $rulerWs = new RegisterUserService();

//                $retUser = $rulerWs->registerUserPremiumB2bExtranet(
                $resultUser = $rulerWs->registerUserPremiumB2bExtranet(
                    $aUser['email'],
                    $aUser['pwd'],
                    $aUser['langEnv'],
                    $aUser['name'],
                    $aUser['surnames'],
                    $aUser['currentLevel'],
                    $aUser['teacherEmail'],
                    $aUser['codeDeal'],
                    $aUser['partnerId'],
                    $aUser['periodId'],
                    $aUser['idCountry'],
                    $aUser['device']
                );

                $retUser =      false;
                $extraData =    array('oldTeacherEmail' => '', 'oldIdPartnerCurrent' => '');
                if(isset($resultUser['moUser'])) {
                    $retUser =      $resultUser['moUser'];
                    $extraData =    $resultUser['extraData'];
                }

                if (!$retUser) {
                    $aUsersResponse[] = array(
                        'email' =>      $aUser['email'],
                        'response' =>   $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError())
                    );

                    ++$iTotalErrors;

                } elseif ($rulerWs->getLastError() !== "") {
                    $aUsersResponse[] = array(
                        'email' =>      $aUser['email'],
                        'response' =>   $this->retSuccessWs(
                            array(
                                "userId" =>                 $retUser->getId(),
                                "result" =>                 "success with warning: " . $rulerWs->getLastError(),
                                "oldTeacherEmail" =>        (isset($extraData['oldTeacherEmail']) ? $extraData['oldTeacherEmail'] : ''),
                                "oldIdPartnerCurrent" =>    (isset($extraData['oldIdPartnerCurrent']) ? $extraData['oldIdPartnerCurrent'] : ''),
                        ))
                    );
                } else {
                    $aUsersResponse[] = array(
                        'email' =>      $aUser['email'],
                        'response' =>   $this->retSuccessWs(
                            array(
                                "userId" =>                 $retUser->getId(),
                                "result" =>                 "success",
                                "oldTeacherEmail" =>        (isset($extraData['oldTeacherEmail']) ? $extraData['oldTeacherEmail'] : ''),
                                "oldIdPartnerCurrent" =>    (isset($extraData['oldIdPartnerCurrent']) ? $extraData['oldIdPartnerCurrent'] : ''),
                        ))
                    );
                }
            }

            if($iTotalErrors == count($aUsersResponse)) {
                $this->retErrorWs("556");
            }
            else if($iTotalErrors > 0 && $iTotalErrors < count($aUsersResponse)) {
                $this->retWarningWs("557");
            }

            return $aUsersResponse;
        }
        catch(Exception $ex)
        {
            error_log( $signature . " error=> " . $ex->getMessage() );
            return $this->retErrorWs( "555", "Internal error non identified for registerUserPremiumB2b." . " Check with Aba IT Team and see logs Apache for " . $signature );
        }
    }


    /**
     * @param string $signature
     * @param string $users
     *
     * @return array The array when success has 3 elements: userId, result, redirectUrl
     * @soap
     */
    public function registerTeacher($signature, $users)
    {
        try
        {
            if (!$this->isValidSignatureExternal($signature, array($users))) {
                return $this->retErrorWs("502");
            }

            $aUsers =           json_decode($users, true);
            $aUsersResponse =   array();
            $iTotalErrors =     0;

            foreach($aUsers as $iKey => $aUser) {

                $rulerWs = new RegisterUserService();
                $retUser = $rulerWs->registerTeacher($aUser);

                if (!$retUser) {
                    $aUsersResponse[] = array(
                        'email' =>      $aUser['email'],
                        'response' =>   $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError())
                    );

                    ++$iTotalErrors;

                } elseif ($rulerWs->getLastError() !== "") {
                    $aUsersResponse[] = array(
                        'email' =>      $aUser['email'],
                        'response' =>   $this->retSuccessWs(
                                array(
                                    "userId" => $retUser->getId(),
                                    "email" =>  $retUser->email,
                                    "result" => "success with warning: " . $rulerWs->getLastError()
                                ))
                    );
                } else {
                    $aUsersResponse[] = array(
                        'email' =>      $aUser['email'],
                        'response' =>   $this->retSuccessWs(
                                array(
                                    "userId" => $retUser->getId(),
                                    "email" =>  $retUser->email,
                                    "result" => "success"
                                ))
                    );
                }
            }

            if($iTotalErrors == count($aUsersResponse)) {
                $this->retErrorWs("556");
            }
            else if($iTotalErrors > 0 && $iTotalErrors < count($aUsersResponse)) {
                $this->retWarningWs("557");
            }

            return $aUsersResponse;
        }
        catch(Exception $ex)
        {
            error_log( $signature . " error=> " . $ex->getMessage() );
            return $this->retErrorWs( "555", "Internal error non identified for registerTeacher." . " Check with Aba IT Team and see logs Apache for " . $signature );
        }
    }


    /**
     * @param string $signature
     * @param string $users
     *
     * @return array The array when success has 3 elements: userId, result, redirectUrl
     * @soap
     */
    public function finaliceLicense($signature, $users)
    {
        try
        {
            if (!$this->isValidSignatureExternal($signature, array($users))) {
                return $this->retErrorWs("502");
            }

            $aUsers =           json_decode($users, true);
            $aUsersResponse =   array();
            $iTotalErrors =     0;

            foreach($aUsers as $iKey => $aUser) {

                $rulerWs = new RegisterUserService();
                $retUser = $rulerWs->finaliceLicense($aUser);

                if (!$retUser) {
                    $aUsersResponse[] = array(
                        'email' =>      $aUser['email'],
                        'response' =>   $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError())
                    );

                    ++$iTotalErrors;

                } elseif ($rulerWs->getLastError() !== "") {
                    $aUsersResponse[] = array(
                        'email' =>      $aUser['email'],
                        'response' =>   $this->retSuccessWs(
                                array(
                                    "userId" => $retUser->getId(),
                                    "result" => "success with warning: " . $rulerWs->getLastError()
                                ))
                    );
                } else {
                    $aUsersResponse[] = array(
                        'email' =>      $aUser['email'],
                        'response' =>   $this->retSuccessWs(
                                array(
                                    "userId" => $retUser->getId(),
                                    "result" => "success"
                                ))
                    );
                }
            }

            if($iTotalErrors == count($aUsersResponse)) {
                $this->retErrorWs("556");
            }
            else if($iTotalErrors > 0 && $iTotalErrors < count($aUsersResponse)) {
                $this->retWarningWs("557");
            }

            return $aUsersResponse;
        }
        catch(Exception $ex)
        {
            error_log( $signature . " error=> " . $ex->getMessage() );
            return $this->retErrorWs( "555", "Internal error non identified for registerTeacher." . " Check with Aba IT Team and see logs Apache for " . $signature );
        }
    }


    /** Register FREE users with facebook id
     *
     * @param string $signature
     * @param string $sAction
     * @param string $sEmail
     * @param string $sName
     * @param string $sSurname
     * @param string $sFbid
     * @param string $sGender
     * @param string $sEmailFacebook
     * @param string $sPass
     *
     * @param string $langEnv
     * @param string $idProduct
     * @param string $promocode
     * @param string $idCountry
     * @param string $idPartner
     * @param string $idSourceList
     * @param string $device
     * @param string $currentLevel
     * @param string $landingPage
     * @param string $scenario
     *
     * @return array
     *
     * @soap
     */
    public function registerUserFacebook( $signature, $sAction, $sEmail='', $sName='', $sSurname='', $sFbid=null, $sGender='', $sEmailFacebook='', $sPass='',
        $langEnv='', $idProduct='', $promocode='', $idCountry='', $idPartner=null, $idSourceList=null, $device=null, $currentLevel=NULL, $landingPage='', $scenario='')
    {
        try
        {
            // Validation of params for this web service  ++++++++++++++++++++++
            if (!$this->isValidSignature($signature, array($sAction, $sEmail, $sName, $sSurname, $sFbid, $sGender, $sEmailFacebook, $sPass
                , $langEnv, $idProduct, $promocode, $idCountry, $idPartner, $idSourceList, $device, $currentLevel, $landingPage, $scenario))) {
                return $this->retErrorWs("502");
            }

            // Validation of web service action
            if ($sAction !== 'exists' && $sAction !== 'check' && $sAction !== 'register') {
                return $this->retErrorWs("503", "Invalid action");
            }

            $userCommon =   new UserCommon();
            $rulerWs =      new RegisterUserService();

            $sResult = '';
            $retUser = false;

            if($sAction == 'exists') { // existing user

                $retUser = $userCommon->checkIfFbUserExists($sFbid);

                if($retUser) {
                    $sResult = "successexists";
                }
            }
            elseif($sAction == 'check' && $userCommon->checkIfChangedEmailExists($sEmail, $sEmailFacebook)) { // new registered user
                $sResult = "successcheck";
            }
            elseif($sAction == 'register') { // check email

                $form = new LoginForm();
                $form->attributes = array('email' => $sEmail, 'password' => $sPass, 'rememberMe' => 0);

                if ($form->validate()) {
                    //
                    // Update any user data
                    $retUser = new AbaUser();
                    $retUser->getUserByEmail($sEmail);

                    $sResult = "successregister";
                    $userCommon->changeUserFbData( $retUser, $sName, $sSurname, $sFbid, $sGender);
                }
                else {
                    return $this->retErrorWs( "504", ''); // 522
                }
            }
            else {

                if (!isset($idPartner)) {
                    $idPartner = Yii::app()->config->get("ABA_PARTNERID");
                }

                $commProducts = new ProductsPricesCommon();
                if (trim($idProduct) !== "" && !$commProducts->checkExistsIdProduct($idProduct)) {
                    return $this->retErrorWs("508", " Id product " . $idProduct);
                }
                if ($landingPage !== 'HOME' && $landingPage !== 'PAYMENT') {
                    return $this->retErrorWs("501", " LadingPage " . $landingPage . " should be HOME or PAYMENT.");
                }
                $moCountry = new AbaCountry();
                if ($idCountry == '' || !$moCountry->getCountryById($idCountry)) {
                    return $this->retErrorWs("513", " Country Id($idCountry) in registerUser should contain a valid value.");
                }

                if ($landingPage == 'HOME') {
                    $landingPage = 'HOME_REGISTER';
                }

                $sResult = "successregister";

                $retUser = $rulerWs->registerUserFacebook($sEmail, $sName, $sSurname, $sFbid, $sGender, $langEnv, $idCountry, $idPartner, $idSourceList, $device, $currentLevel, $scenario);
            }

            if ($retUser) {

                // ---------------------------------------------
                // Transform some parameters: +++++++++++++++++++++++++++++++++++++++
                if (trim($promocode) == "undefined") {
                    $promocode = "";
                }
                if (trim($promocode) !== "" && !$commProducts->checkExistsPromocode($promocode)) {
                    $promocode = "";
                }
                //-----------------------------------------------

                return $this->retSuccessWs(array(
                    "fbId" =>           $sFbid,
                    "userId" =>         $retUser->getId(),
                    "email" =>          $retUser->email,
                    "result" =>         $sResult,
                    "redirectUrl" =>    $rulerWs->getRedirectUrl($retUser, $idProduct, $promocode, $landingPage, true, $idPartner),
                ));
            } else {

                if ($rulerWs->getLastError() !== "" && $rulerWs->getLastError() !== null) {
                    return $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError());
                }
                else {
                    if($sAction == 'exists') { // USER NO EXISTS
                        return $this->retSuccessWs(array(
                            "result" => "noexists",
                        ));
                    }
                    elseif($sAction == 'check') { //
                        return $this->retSuccessWs(array(
                            "result" => ($sResult == "successcheck" ? "successcheck" : "nocheck"),
                        ));
                    }
                    elseif($sAction == 'register') { //
                        return $this->retSuccessWs(array(
                            "result" => "noregister",
                        ));
                    }
                }
                return $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError());
            }
        }
        catch(Exception $ex)
        {
            error_log( $signature." error=> ".$ex->getMessage() );
            return $this->retErrorWs( "555", $ex);
        }
    }
    /** Register FREE users with Linkedin id
     *
     * @param string $signature
     * @param string $sAction
     * @param string $sEmail
     * @param string $sName
     * @param string $sSurname
     * @param string $sINid
     * @param string $sGender
     * @param string $sEmailLinkedin
     * @param string $sPass
     *
     * @param string $langEnv
     * @param string $idProduct
     * @param string $promocode
     * @param string $idCountry
     * @param string $idPartner
     * @param string $idSourceList
     * @param string $device
     * @param string $currentLevel
     * @param string $landingPage
     * @param string $scenario
     *
     * @return array
     *
     * @soap
     */
    public function registerUserLinkedin( $signature, $sAction, $sEmail='', $sName='', $sSurname='', $sINid=null, $sGender='', $sEmailLinkedin='', $sPass='',
      $langEnv='', $idProduct='', $promocode='', $idCountry='', $idPartner=null, $idSourceList=null, $device=null, $currentLevel=NULL, $landingPage='', $scenario='')
    {
        try
        {
            // Validation of params for this web service  ++++++++++++++++++++++
            if (!$this->isValidSignature($signature, array($sAction, $sEmail, $sName, $sSurname, $sINid, $sGender, $sEmailLinkedin, $sPass
            , $langEnv, $idProduct, $promocode, $idCountry, $idPartner, $idSourceList, $device, $currentLevel, $landingPage, $scenario))) {
                return $this->retErrorWs("502");
            }

            // Validation of web service action
            if ($sAction !== 'exists' && $sAction !== 'check' && $sAction !== 'register') {
                return $this->retErrorWs("503", "Invalid action");
            }

            $userCommon =   new UserCommon();
            $rulerWs =      new RegisterUserService();

            $sResult = '';
            $retUser = false;

            if($sAction == 'exists') { // existing user

                $retUser = $userCommon->checkILinkedinUserExists($sINid);

                if($retUser) {
                    $sResult = "successexists";
                }
            }
            elseif($sAction == 'check' && $userCommon->checkIfChangedEmailExists($sEmail, $sEmailLinkedin)) { // new registered user
                $sResult = "successcheck";
            }
            elseif($sAction == 'register') { // check email

                $form = new LoginForm();
                $form->attributes = array('email' => $sEmail, 'password' => $sPass, 'rememberMe' => 0);

                if ($form->validate()) {
                    //
                    // Update any user data
                    $retUser = new AbaUser();
                    $retUser->getUserByEmail($sEmail);

                    $sResult = "successregister";
                    $userCommon->changeUserinData( $retUser, $sName, $sSurname, $sINid, $sGender);
                }
                else {
                    return $this->retErrorWs( "504", ''); // 522
                }
            }
            else {

                if (!isset($idPartner)) {
                    $idPartner = Yii::app()->config->get("ABA_PARTNERID");
                }

                $commProducts = new ProductsPricesCommon();
                if (trim($idProduct) !== "" && !$commProducts->checkExistsIdProduct($idProduct)) {
                    return $this->retErrorWs("508", " Id product " . $idProduct);
                }
                if ($landingPage !== 'HOME' && $landingPage !== 'PAYMENT') {
                    return $this->retErrorWs("501", " LadingPage " . $landingPage . " should be HOME or PAYMENT.");
                }
                $moCountry = new AbaCountry();
                if ($idCountry == '' || !$moCountry->getCountryById($idCountry)) {
                    return $this->retErrorWs("513", " Country Id($idCountry) in registerUser should contain a valid value.");
                }

                if ($landingPage == 'HOME') {
                    $landingPage = 'HOME_REGISTER';
                }

                $sResult = "successregister";

                $retUser = $rulerWs->registerUserLinkedin($sEmail, $sName, $sSurname, $sINid, $sGender, $langEnv,
                  $idCountry, $idPartner, $idSourceList, $device, $currentLevel, $scenario);
            }

            if ($retUser) {

                // ---------------------------------------------
                // Transform some parameters: +++++++++++++++++++++++++++++++++++++++
                if (trim($promocode) == "undefined") {
                    $promocode = "";
                }
                if (trim($promocode) !== "" && !$commProducts->checkExistsPromocode($promocode)) {
                    $promocode = "";
                }
                //-----------------------------------------------

                return $this->retSuccessWs(array(
                    "sINid" =>          $sINid,
                    "userId" =>         $retUser->getId(),
                    "email" =>          $retUser->email,
                    "result" =>         $sResult,
                    "redirectUrl" =>    $rulerWs->getRedirectUrl($retUser, $idProduct, $promocode, $landingPage, true, $idPartner),
                ));
            } else {

                if ($rulerWs->getLastError() !== "" && $rulerWs->getLastError() !== null) {
                    return $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError());
                }
                else {
                    if($sAction == 'exists') { // USER NO EXISTS
                        return $this->retSuccessWs(array(
                            "result" => "noexists",
                        ));
                    }
                    elseif($sAction == 'check') { //
                        return $this->retSuccessWs(array(
                            "result" => ($sResult == "successcheck" ? "successcheck" : "nocheck"),
                        ));
                    }
                    elseif($sAction == 'register') { //
                        return $this->retSuccessWs(array(
                            "result" => "noregister",
                        ));
                    }
                }
                return $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError());
            }
        }
        catch(Exception $ex)
        {
            error_log( $signature." error=> ".$ex->getMessage() );
            return $this->retErrorWs( "555", $ex);
        }
    }


    /**
     * Returns array of 4 elements: [userId] and [result], [redirectUrl] and [resultObjectives] in case of success.
     *
     * @param string $signature
     * @param string $email
     * @param string $pwd
     * @param string $langEnv
     * @param string $name
     * @param string $surnames
     * @param string $idProduct
     * @param string $promocode
     * @param string $landingPage
     * @param integer $idCountry
     * @param integer $idPartner
     * @param integer $idSourceList Optional
     * @param string $device
     * @param integer $currentLevel
     * @param string $objectives
     * @param string $scenario
     *
     * @return array The array has the following format array("userId","result","redirectUrl","resultObjectives)
     *
     * @soap
     */
    public function registerUserObjectives( $signature, $email, $pwd, $langEnv, $name, $surnames, $idProduct, $promocode,
      $landingPage, $idCountry, $idPartner=null, $idSourceList=NULL, $device=NULL, $currentLevel=NULL, $objectives='',
      $scenario='' )
    {
        try
        {
            // Validation of params for this web service  ++++++++++++++++++++++
            if (!$this->isValidSignature($signature, array($email, $pwd, $langEnv, $name, $surnames, $idProduct,
                $promocode, $landingPage, $idCountry, $idPartner, $idSourceList, $device, $currentLevel, $objectives,
                $scenario))
            ) {
                return $this->retErrorWs("502");
            }
            if (!isset($idPartner)) {
                $idPartner = Yii::app()->config->get("ABA_PARTNERID");
            }

            $commProducts = new ProductsPricesCommon();
            if (trim($idProduct) !== "" && !$commProducts->checkExistsIdProduct($idProduct)) {
                return $this->retErrorWs("508", " Id product " . $idProduct);
            }
            if ($landingPage !== 'HOME' && $landingPage !== 'HOME_LEVEL_UNIT' && $landingPage !== 'PAYMENT') {
                return $this->retErrorWs("501", " LadingPage " . $landingPage . " should be HOME, HOME_LEVEL_UNIT or PAYMENT.");
            }
            $moCountry = new AbaCountry();
            if ($idCountry == '' || !$moCountry->getCountryById($idCountry)) {
                return $this->retErrorWs("513", " Country Id($idCountry) in registerUser ".
                    "should contain a valid value.");
            }

            $rulerWs = new RegisterUserService();
            $retUser = $rulerWs->registerUser($email, $pwd, $langEnv, $idCountry, $name, $surnames, $currentLevel,
              $idPartner, $idSourceList, $device, null, $scenario);

            if ($retUser) {
                if ($retUser->langEnv == '') {
                    return $this->retErrorWs("503", "Language not set.");
                }

                $bObjectivesSuccess = true;

                if(trim($objectives) != '') {

                    $aObjectives = json_decode($objectives, true);

                    $userObjectives = new AbaUserObjective();

                    if(!$userObjectives->saveFormObjectivesWs($retUser, $aObjectives)) {
                        $bObjectivesSuccess = false;
                    }
                }

                $sRedirectUrl = $rulerWs->getRedirectUrl($retUser, $idProduct, $promocode, 'OBJECTIVES', true, $idPartner);

                if(!HeMixed::isValidLevel($currentLevel)) {
                    return $this->retSuccessWs(array(
                        "userId" =>             $retUser->getId(),
                        "result" =>             "success with errors: " . "Selected level is invalid",
                        "redirectUrl" =>        $sRedirectUrl,
                        "resultObjectives" =>   ($bObjectivesSuccess ? "success" : "Some error has ocurred while inserting/updating your questionnaire.")
                    ));
                }
                else{
                    return $this->retSuccessWs(array(
                        "userId" =>             $retUser->getId(),
                        "result" =>             "success",
                        "redirectUrl" =>        $sRedirectUrl,
                        "resultObjectives" =>   ($bObjectivesSuccess ? "success" : "Some error has ocurred while inserting/updating your questionnaire.")
                    ));
                }
            } else {
                return $this->retErrorWs($rulerWs->getErrorCode(), $rulerWs->getLastError());
            }
        }
        catch(Exception $ex)
        {
            error_log( $signature . " error=> " . $ex->getMessage() );
            return $this->retErrorWs( "555", $ex);
        }
    }


    /** Login Aba Users from external web apps
     *
     * @param string $signature
     * @param string $sEmail
     * @param string $sPass
     *
     * @return array
     *
     * @soap
     */
    public function loginUserExternal( $signature, $sEmail='', $sPass='')
    {
        try
        {
            // Validation of params for this web service  ++++++++++++++++++++++
            if (!$this->isValidSignatureExternal($signature, array($sEmail, $sPass))) {
                return $this->retErrorWs("502");
            }

            $userCommon =   new UserCommon();
            $rulerWs =      new RegisterUserService();

            $retUser =      false;

            $form = new LoginForm();
            $form->attributes = array('email' => $sEmail, 'password' => $sPass, 'rememberMe' => 0);

            if ($form->validate()) {
                $retUser = $userCommon->checkIfUserExists($sEmail, $sPass);
            }
            else {
                return $this->retErrorWs("504", "User does not exist");
            }

            if ($retUser) {
                return $this->retSuccessWs(array(
                    "email" =>          $retUser->email,
                    "result" =>         "success",
                    "redirectUrl" =>    $rulerWs->getRedirectUrl($retUser, "", "", "HOME", true, ""),
                ));
            } else {
                return $this->retErrorWs("504", "User email or password are empty or not valid");
            }
        }
        catch(Exception $ex)
        {
            error_log( $signature." error=> ".$ex->getMessage() );
            return $this->retErrorWs( "555", $ex);
        }
    }


    /** Recover Aba User passowrd from external web apps
     *
     * @param string $signature
     * @param string $sEmail
     *
     * @return array
     *
     * @soap
     */
    public function recoverPassword( $signature, $sEmail='')
    {
        try
        {
            // Validation of params for this web service  ++++++++++++++++++++++
            if (!$this->isValidSignatureExternal($signature, array($sEmail))) {
                return $this->retErrorWs("502");
            }

            //
            //#5766
            //
            $commUser = new UserCommon();
            if($commUser->sendRecoverPasswordEmail($sEmail)) {
                return $this->retSuccessWs(array(
                        "email" =>  $sEmail,
                        "result" => "success",
                    ));
            }
            else {
                return $this->retErrorWs("504", "User does not exist OR user email is empty or not valid");
            }
        }
        catch(Exception $ex)
        {
            error_log( $signature." error=> ".$ex->getMessage() );
            return $this->retErrorWs( "555", $ex);
        }
    }
    /**-----------------------------------------------------------------------------------------------------------------**/
/**-----------------------------------------------------------------------------------------------------------------**/
/**-----------------------------------------------------------------------------------------------------------------**/
}