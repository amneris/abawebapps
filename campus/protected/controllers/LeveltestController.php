<?php

use appCampus\models\leveltest\LevelTestData;
use appCampus\models\leveltest\LevelTestQuestions;

class LeveltestController extends CController
{
    /**
     *
     */
    public function init()
    {
        parent::init();

        header("Access-Control-Allow-Origin: *");
        if ( !isset(Yii::app()->session['_ABASession_LevelTest']) ) $this->resetSession();
    }

    /**
     *
     */
    protected function resetSession()
    {
        Yii::app()->session['_ABASession_LevelTest'] = new stdClass();
    }

    /**
     *
     */
    static public function getSession( ) {
        return Yii::app()->session['_ABASession_LevelTest'];
    }

    /**
     *
     */
    protected function setSessionVar( $varName, $val )
    {
        Yii::app()->session['_ABASession_LevelTest']->$varName = $val;

        return $val;
    }

    /**
     *
     */
    protected function getSessionVar($varName, $defaultValue = false)
    {
        if ( isset(Yii::app()->session['_ABASession_LevelTest']->$varName) ) $val = Yii::app()->session['_ABASession_LevelTest']->$varName;
        else $val = $defaultValue;

        return $val;
    }

    /**
     *
     */
    public function actionIndex()
    {
            // ** Clear data from another test.
        $this->resetSession();
            // ** Code
        $code = Yii::app()->request->getParam('code', 0);
        $isoLanguage = Yii::app()->request->getParam('language', Yii::app()->language);
        $forceNew = ($code == 'new');
        if($forceNew) {
            $mLevelTest = LevelTestData::getNewInstance();
            $mLevelTest->save();
        } else {
            $criteria = new CDbCriteria();
            $criteria->addCondition('t.code = :code');
            $criteria->addCondition('t.isdeleted = :isdeleted');
            $criteria->params = array_merge($criteria->params,
              array(':isdeleted' => 0, ':code' => $code));
            $mLevelTest = LevelTestData::model()->find($criteria);

            if (!$mLevelTest) {
                echo json_encode(array('ok' => false, 'html' => '', 'errorMsg' => Yii::t('leveltest', 'lbl: Level test not found.'), 'errorCode' => -1, 'isoLanguage' => $this->getSessionVar('isoLanguage') ));
                Yii::app()->end();
            }
        }

        $this->setLevelTestValues($mLevelTest->jsonData);
        $this->setSessionVar('idLevelTestData', $mLevelTest->id);
        $this->setSessionVar('code', $mLevelTest->code);
        $this->setSessionVar('isoLanguage', $isoLanguage);

        switch ($mLevelTest->status) {
            case LevelTestData::_STATUS_DONE:
                echo json_encode(array('ok' => false, 'html' => '', 'errorMsg' => Yii::t('leveltest', 'lbl: Level test is already done.'), 'errorCode' => -2, 'level' => $mLevelTest->idLevel, 'isoLanguage' => $this->getSessionVar('isoLanguage') ));
                Yii::app()->end();
            case LevelTestData::_STATUS_NEW:
                $this->setSessionVar('status', LevelTestData::_STATUS_PENDING);
                break;
            case LevelTestData::_STATUS_PENDING:
            default:
                break;
        }

        $this->evalueNextQuestion();
    }

    /**
     *
     */
    protected function setLevelTestValues($jsonData = '')
    {

        if (!empty($jsonData)) {
            $arJsonData = unserialize($jsonData);

            foreach ($arJsonData as $key => $value) {
                $this->setSessionVar($key, $value);
            }
        } else {
            $this->setSessionVar('qCorrectAnswerOnActualLevel', false);
            $this->setSessionVar('idQuestion', 0);
            $this->setSessionVar('answer', 0);
            $this->setSessionVar('curQuestion', 0);
            $this->setSessionVar('lastAnsweredQuestion', 0);
            $this->setSessionVar('curQuestionList', array());
            $this->setSessionVar('curProgress', 0);
            $this->setSessionVar('curLevel', 1);
            $this->setSessionVar('qTextDone', 0);
            $this->setSessionVar('qAudioDone', 0);
            $this->setSessionVar('error', 0);
            $this->setSessionVar('code', 0);
            $this->setSessionVar('status', LevelTestData::_STATUS_NEW);
        }

    }

    protected function evalueNextQuestion($errorAnswer = false) {
        $idQuestion = $this->getSessionVar('idQuestion', 0 );
        if ( !empty($idQuestion) ) $mQuestion = LevelTestQuestions::model()->findByPk($idQuestion);
        else $mQuestion = $this->getRandomQuestion($this->getSessionVar('curLevel'));

        if ($mQuestion === false) {                         // ** Error al consultar la siguiente pregunta.
            $errorMsg = Yii::t('leveltest', 'lbl: Error with the next question.');
            echo json_encode(array('ok' => false, 'html' => $errorMsg, 'errorMsg' => $errorMsg, 'errorCode' => -3));
            Yii::app()->end();
        } else if ( $mQuestion === true ) {
            $this->saveActualState(LevelTestData::_STATUS_DONE);
            $endOfTest = true;
            $html =  '';
        } else {
            $endOfTest = false;
            $html = $this->generateNextQuestionHTML($mQuestion);        // ** Mostramos la siguiente pregunta.
        }

        echo json_encode(array('ok' => true, 'html' => $html, 'errorMsg' => '', 'errorCode' => 0, 'okAnswer' => !$errorAnswer, 'end' => $endOfTest, 'level' => $this->getSessionVar('curLevel'), 'isoLanguage' => $this->getSessionVar('isoLanguage') ));
        Yii::app()->end();
     }

    protected function generateNextQuestionHTML($mQuestion) {
        $this->saveActualState($this->getSessionVar('status', LevelTestData::_STATUS_NEW));

        $html = $this->renderPartial('main', array(
          'mQuestion' => $mQuestion,
        ), true);

        return $html;
    }

    protected function saveActualState($status = LevelTestData::_STATUS_NEW)
    {
        $arSession = $this->getSession();
        $idLevelTestData = $this->getSessionVar('idLevelTestData');
        if(empty($idLevelTestData)) {
            $model = new LevelTestData();
            $model->code = md5(time());
            $model->save();

            $idLevelTestData = $model->id;
        }
        $mLevelTestData = LevelTestData::model()->findByPk($idLevelTestData);
        if ($mLevelTestData) {
            $mLevelTestData->jsonData = serialize($arSession);
            $mLevelTestData->idLevel = $this->getSessionVar('curLevel');
            $mLevelTestData->numQuestions = $this->getSessionVar('curQuestion');
            $mLevelTestData->numErrors = $this->getSessionVar('error');
            $mLevelTestData->status = $status;
            if ($status == LevelTestData::_STATUS_DONE) {
                $mLevelTestData->completed = date("Y-m-d H:i:s");
                // ** Ej. Si contesto preguntas del nivel 1, nivel 2 y nivel 3 pero agoto mi última posibilidad en este nivel, el resultado es nivel 3 (a no ser que no haya respondido bien ninguna pregunta del nivel 3).
                if (!$this->getSessionVar('qCorrectAnswerOnActualLevel', false)) {
                    $mLevelTestData->idLevel -= 1;
                }
                if ($mLevelTestData->idLevel < 1) {
                    $mLevelTestData->idLevel = 1;
                }
//                $this->setSessionVar('curLevel', $mLevelTestData->idLevel);
            }
            $mLevelTestData->save();
        } else {
            // ** ERROR
        }

    }

    /**
     *
     */
    private function getRandomQuestion( $idLevel = 1 ) {

        if ( $this->getSessionVar('error', 0) >= 3 ) return true;

        // ** Escojo el tipo de pregunta a realizar (texto/audio).
        $selectedType = 0;
        if ( $this->getSessionVar('qTextDone') < Yii::app()->params['LevelTest config'][$idLevel]['textQuestions']) {
            $selectedType = LevelTestQuestions::_TYPE_TEXT;
        } else if ( $this->getSessionVar('qAudioDone') < Yii::app()->params['LevelTest config'][$idLevel]['audioQuestions']) {
            $selectedType = LevelTestQuestions::_TYPE_AUDIO;
        }

        // ** Ya hemos terminado las preguntas de este nivel, pasamos al siguiente.
        if ( empty($selectedType) ) {
            if ( isset(Yii::app()->params['LevelTest config'][$idLevel+1]) ) {
                $this->setSessionVar('qCorrectAnswerOnActualLevel', false);
                $this->setSessionVar('qTextDone', 0);
                $this->setSessionVar('qAudioDone', 0);

                return $this->getRandomQuestion( $idLevel + 1 );
            } else return true;  // ** Ya no hay mas niveles.
        }

        // ** Consulto todas las preguntas de este nivel.
        switch( $idLevel ) {
            case 1:
                // ** En el nivel 1 se hacen x preguntas escogidas aleatoriamente entre las del nivel 1 … sin ninguna pregunta de audio
                $criteria = new CDbCriteria();
                $criteria->addCondition( 't.idLevel = :idLevel' );
                $criteria->addCondition( 't.isdeleted = :isdeleted' );
                $criteria->params = array_merge( $criteria->params, array( ':idLevel' => $idLevel, ':isdeleted' => 0 ) );
                $mlQuestion = LevelTestQuestions::model()->findAll($criteria);
                if ( !$mlQuestion ) return false;   // ** Si no hay preguntas disponibles, no hay nada que hacer.

                break;
            case 2:
                // ** En el nivel 2 se hacen x preguntas escogidas aleatoriamente entre las del nivel 2 y 3 mas otras y preguntas de audio del nivel 2
                $criteria = new CDbCriteria();
                $criteria->addCondition( 't.idLevel = :idLevel1' );
                $criteria2 = new CDbCriteria();
                $criteria2->addCondition( 't.idLevel = :idLevel2' );
                $criteria->mergeWith($criteria2, 'or');
                $criteria->addCondition( 't.isdeleted = :isdeleted' );
                $criteria->params = array_merge( $criteria->params, array( ':idLevel1' => $idLevel, ':idLevel2' => $idLevel + 1, ':isdeleted' => 0 ) );
                $mlQuestion = LevelTestQuestions::model()->findAll($criteria);
                if ( !$mlQuestion ) return false;   // ** Si no hay preguntas disponibles, no hay nada que hacer.

                break;
            case 3: case 4: case 5:
            // ** En el nivel 3 se hacen x preguntas escogidas aleatoriamente entre las del nivel 4 mas otras y pregunta de audio del nivel 4
            // ** En el nivel 4 se hacen x preguntas escogidas aleatoriamente entre las del nivel 5 mas otras y pregunta de audio del nivel 5
            // ** En el nivel 5 se hacen x preguntas escogidas aleatoriamente entre las del nivel 6 mas otras y preguntas de audio del nivel 6
            $criteria = new CDbCriteria();
            $criteria->addCondition( 't.idLevel = :idLevel' );
            $criteria->addCondition( 't.isdeleted = :isdeleted' );
            $criteria->params = array_merge( $criteria->params, array( ':idLevel' => ($idLevel + 1), ':isdeleted' => 0 ) );
            $mlQuestion = LevelTestQuestions::model()->findAll($criteria);

            if ( !$mlQuestion ) return false;   // ** Si no hay preguntas disponibles, no hay nada que hacer.

            break;
            default: return true;   // ** No hay nivel 6.
        }

        // ** Filtro las preguntas ya realizadas.
        $arQuestions = array();
        $arCurQuestionList = $this->getSessionVar('curQuestionList');
        foreach( $mlQuestion as $mQuestion ) {
            if ( isset( $arCurQuestionList[$mQuestion->id] ) ) continue;
            $arQuestions[$mQuestion->type][] = $mQuestion;
        }

        // ** Vuelvo a escoger el tipo de pregunta, esta vez teniendo en cuenta las que hay disponibles.
        $selectedType = 0;
        if ( $this->getSessionVar('qTextDone') < Yii::app()->params['LevelTest config'][$idLevel]['textQuestions'] && isset($arQuestions[LevelTestQuestions::_TYPE_TEXT]) ) {
            $selectedType = LevelTestQuestions::_TYPE_TEXT;
        } else if ( $this->getSessionVar('qAudioDone') < Yii::app()->params['LevelTest config'][$idLevel]['audioQuestions'] && isset($arQuestions[LevelTestQuestions::_TYPE_AUDIO]) ) {
            $selectedType = LevelTestQuestions::_TYPE_AUDIO;
        }
        // ** Ya hemos terminado las preguntas de este nivel, pasamos al siguiente.
        if ( empty($selectedType) ) {
            if ( isset(Yii::app()->params['LevelTest config'][$idLevel+1]) ) {
                $this->setSessionVar('qCorrectAnswerOnActualLevel', false);
                $this->setSessionVar('qTextDone', 0);
                $this->setSessionVar('qAudioDone', 0);

                return $this->getRandomQuestion( $idLevel + 1 );
            } else return true;  // ** Ya no hay mas niveles.
        }

        // ** Escogemos una pregunta de las que nos quedan.
        $countQuestions = count($arQuestions[$selectedType]);
        $mQuestion = $arQuestions[$selectedType][ rand( 0, $countQuestions-1 ) ];

        // ** Actualizo el progreso.
        $arCurQuestionList[$mQuestion->id] = 1;
        $this->setSessionVar('idQuestion', $mQuestion->id);
        $this->setSessionVar('answer', $mQuestion->answer);
        $this->setSessionVar('curQuestion', $this->getSessionVar('curQuestion') + 1 );
        $this->setSessionVar('curQuestionList', $arCurQuestionList);
        $this->setSessionVar('curProgress', $this->getSessionVar('curProgress') + (int) (((100 - $this->getSessionVar('curProgress')) * 10 ) / 100) );
        $this->setSessionVar('curLevel', $idLevel);
        if ( $selectedType == LevelTestQuestions::_TYPE_TEXT ) $this->setSessionVar('qTextDone', $this->getSessionVar('qTextDone') + 1 );
        else if ( $selectedType == LevelTestQuestions::_TYPE_AUDIO ) $this->setSessionVar('qAudioDone', $this->getSessionVar('qAudioDone') + 1 );

        return $mQuestion;
    }

    function actionNext() {
        // ** Answer
        $answer = Yii::app()->request->getParam( 'answer', 0 );

        // ** Code
        $code = Yii::app()->request->getParam('code', 0);
        if (!empty($code)) {
            $criteria = new CDbCriteria();
            $criteria->addCondition('t.code = :code');
            $criteria->addCondition('t.isdeleted = :isdeleted');
            $criteria->params = array_merge($criteria->params,
              array(':isdeleted' => 0, ':code' => $code));
            $mLevelTest = LevelTestData::model()->find($criteria);
        }

        if (empty($code) || !$mLevelTest) {
            echo json_encode(array('ok' => false, 'html' => '', 'errorMsg' => Yii::t('leveltest', 'lbl: Level test not found.'), 'errorCode' => -1, 'isoLanguage' => $this->getSessionVar('isoLanguage') ));
            Yii::app()->end();
        }
        $this->setLevelTestValues($mLevelTest->jsonData);

        if ($this->getSessionVar('code') != $code) {
            echo json_encode(array('ok' => false, 'html' => '', 'errorMsg' => Yii::t('leveltest', 'lbl: Level test not found.'), 'errorCode' => -1, 'isoLanguage' => $this->getSessionVar('isoLanguage') ));
            Yii::app()->end();
        }

        $errorAnswer = false;
        if ( $answer != $this->getSessionVar('answer') ) {
            $this->setSessionVar('error', $this->getSessionVar('error') + 1 );
            $errorAnswer = true;
        } else {
            $this->setSessionVar('qCorrectAnswerOnActualLevel', true);
        }

        $this->setSessionVar('idQuestion', 0 );

        $this->evalueNextQuestion($errorAnswer);
    }

    function actionNewLeveltestData() {
        $mLeveltestData = LevelTestData::getNewInstance();
        $mLeveltestData->save();

        $code = $mLeveltestData->code;
        echo json_encode(array('code'=>$code));
    }
}