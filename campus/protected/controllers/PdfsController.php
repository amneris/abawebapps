<?php

class PdfsController extends AbaController
{
    public $data = array();

    public function actionIndex()
    {
        $level = Yii::app()->request->getParam('nivel');
        //tiene que recibirse un nivel entre el numero 1 al 6
        if(array_key_exists($level, Yii::app()->params['levels']))
        {
            $moFollowsUps = new AbaFollowups();
            if ( $moFollowsUps->isLevelCompleted( Yii::app()->user->getId(), $level) ) {
                $finalStringLevel = "";
                $words = "";
                $hours = "";
                $color = "";
                switch ($level){
                    case 1:
                        $finalStringLevel = "Beginners (A1)*";
                        $words = "1172";
                        $hours = "72";
                        $color = "0852a0";
                        $footerString = array('Student understands words and simple sentences in relation to his or her immediate surroundings.',
                            'Student is able to use simple sentences to describe where he or she lives and to describe people.',
                            'Student can write simple short messages and is able to fill out a personal data form (name, nationality, address).',
                            'Student understands basic sentences, for example on notices or in catalogues.');
                        break;
                    case 2:
                        $finalStringLevel = "Lower intermediate - A2*";
                        $words = "622";
                        $hours = "86";
                        $color = "0852a0";
                        $footerString = array('Student understands the core meaning of short, clear, and simple statements and announcements.',
                            'Student can use a number of phrases and expressions to describe his or her family or other people.',
                            'Student is able to produce simple texts, for example, a simple personal letter to thank someone.',
                            'Student is able to read short texts and to find expected information in daily materials such as flyers, menus and train timetables.');
                        break;
                    case 3:
                        $finalStringLevel = "Intermediate – B1*";
                        $words = "634";
                        $hours = "101";
                        $color = "0852a0";
                        $footerString = array('Student is able to understand the main message of a standard speech. He or she understands the main points of radio and TV programmes.',
                            'Student can connect phrases in a simple way to describe his or her experiences, dreams or hopes.',
                            'Student can produce a simple continuous text on a topic that is well known to him or her.',
                            'Student understands general text and description of events, feelings, and wishes often expressed in personal letters.');
                        break;
                    case 4:
                        $finalStringLevel = "Upper Intermediate – B2*";
                        $words = "568";
                        $hours = "115";
                        $color = "0852a0";
                        $footerString = array('Student understands most TV news and the majority of films in standard dialect.',
                            'Student can interact with a degree of fluency with native speakers and can explain his or her point of view.',
                            'Student is able to produce a detailed text about various topics as well as to write essays or compositions.',
                            'Student can read articles concerned with current problems and can also understand modern literary works.');
                        break;
                    case 5:
                        $finalStringLevel = "Advanced – B2-C1*";
                        $words = "584";
                        $hours = "130";
                        $color = "0852a0";
                        $footerString = array('Student can understand extended speech and lectures even if the speech is not clearly structured.',
                            'Student is able to describe clearly and finish the conversation with an appropriate conclusion.',
                            'Student can write well-structured text, expressing points of view in detail.',
                            'Student can understand long and complex literary texts, appreciating distinctions of style.');
                        break;
                    case 6:
                        $finalStringLevel = "Business – C1*";
                        $words = "938";
                        $hours = "144";
                        $color = "0852a0";
                        $footerString = array('Student can understand television programmes and films without too much effort.',
                            'Student can use language effectively for social and professional purposes.',
                            'Student can write essays or reports dealing with complex topics',
                            'Student understand specialized articles and longer technical instructions');
                        break;
                }
                $user = new AbaUser();
                $user->getUserById(Yii::app()->user->getId());
                $Name = $user->attributes['name'];
                $Surname = $user->attributes['surnames'];
                $this->render('index', array('name' => $Name, 'surnames' => $Surname, 'finalStringLevel' => $finalStringLevel, 'words' => $words, 'hours' => $hours, 'color' => $color, 'footerString' => $footerString));
            }
            else{
                die(Yii::t('mainApp', 'No tienes la puntuación suficiente para recibir el certificado'));
            }
        }
        else
        {
            die(Yii::t('mainApp', 'Este nivel no existe'));
        }
    }
}