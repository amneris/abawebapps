<?php

class GrammarController extends AbaController
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex($section = 1, $chapter = 1, $page = 1)
	{
        if( !HeRoles::allowOperation( OP_GRAMATICA ) )
        {
            $this->redirect( $this->createUrl('site/noaccess') );
        }

        $userId = Yii::app()->user->getId();
        $user = new AbaUser();
        $user->getUserById($userId);
        $isB2B = Yii::app()->user->isB2B();

		$this->render('index', array('section' => $section, 'chapter' => $chapter, 'page' => $page, "isB2B" => $isB2B));
	}

}