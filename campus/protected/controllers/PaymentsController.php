<?php

/**
 * UI controller for all payments related operations
 */
class PaymentsController extends AbaPayController
{
    /* @var AbaUser $user */
    protected $user;
    /* @var OnlinePayCommon $commOnlPay */
    protected $commOnlPay;
    protected $renderFooter = true;
    protected $headerTitle;

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // They can be accessed via: index.php?r=site/page&view=FileName
          'page' => array(
            'class' => 'CViewAction',
          ),
        );
    }

    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
        $this->commOnlPay = new OnlinePayCommon();
    }


    /** First payment URL access, product selection
     * @param string $promoCode
     *
     */
//    public function actionPayment($promoCode = "")
    public function actionPayment()
    {
        $this->user = new AbaUser();
        // Case it access to PAYMENT FROM INSIDE the CAMPUS
        if ($this->user->getUserById(Yii::app()->user->id)) {
            if (Yii::app()->user->refreshAndGetUserType($this->user) >= MAX_ALLOWED_LEVEL) {
                $this->redirect(Yii::app()->createUrl("/site/index"));
                return;
            }

            $this->redirPayProductSelection('payments/paymentmethod');
        } else {
            $this->redirect(Yii::app()->createUrl("/site/noaccess"));
        }
    }

    /** URL for PREMIUM users that want to change the product(plan prices).
     * @param string $promoCode
     */
    public function actionChangePlan($promoCode = "")
    {
        $this->user = new AbaUser();
        // Case it access to PAYMENT FROM INSIDE the CAMPUS
        if ($this->user->getUserById(Yii::app()->user->id)) {
            if (Yii::app()->user->refreshAndGetUserType($this->user) == FREE) {
                $this->redirect(Yii::app()->createUrl("/site/index"));
                return;
            }

            $this->redirPayProductSelection('payments/ReviewNewPlan');
        } else {
            $this->redirect(Yii::app()->createUrl("/site/noaccess"));
        }
    }


    /**  URL for PREMIUM users that want to review the new product selected
     *  and check out the payment method details.
     *
     * @param string $promoCode
     */
    public function actionReviewNewPlan($promoCode = '')
    {
        $this->layout = "mainColumn3";
        $success = false;
        $this->user = new AbaUser();
        $modFormPayment = new PaymentForm();
        // Case it access to PAYMENT FROM INSIDE the CAMPUS
        if ($this->user->getUserById(Yii::app()->user->id)) {
            if (Yii::app()->user->refreshAndGetUserType($this->user) == FREE) {
                $this->redirect(Yii::app()->createUrl("/payments/paymentMethod"));
                return;
            }
            $modFormPayment = $this->fillPaymentForm($modFormPayment, $this->user, true);
            $prodPricePKSelected = HeMixed::getPostArgs("productPricePKSelected");
            if (!is_null($prodPricePKSelected)) {
                $modFormPayment->packageSelected = $prodPricePKSelected;
            }

            $this->redirPaymentMethod($modFormPayment, $success, $this->user, true);
        } else {
            $this->redirect(Yii::app()->createUrl("/site/noaccess"));
        }
    }

    /**
     * Processes the upgrade or downgrade of current product for a PREMIUM user with an
     * ACTIVE subscription.
     *
     */
    public function actionProcessNewPlan()
    {
        $this->layout = "mainColumn3";

        $this->user = new AbaUser();
        $this->user->getUserById(Yii::app()->user->id);

        // In case payment have just been done, and user refreshes or tries
        // to reload same request, we check his userType value.
        if (Yii::app()->user->refreshAndGetUserType($this->user) == FREE) {
            $this->actionPayment('');
            return;
        }

        $modFormPayment = new PaymentForm();
        $modFormPayment = $this->fillPaymentForm($modFormPayment, $this->user, true);

        $commRecurring = new RecurrentPayCommon();

        //
        // no card data
        //
        $idPaySupplier = $modFormPayment->getIdPaySupplier();
        //
        if($idPaySupplier == PAY_SUPPLIER_ADYEN OR $idPaySupplier == PAY_SUPPLIER_ADYEN_HPP OR $idPaySupplier == PAY_SUPPLIER_ALLPAGO_BR OR $idPaySupplier == PAY_SUPPLIER_ALLPAGO_MX OR $modFormPayment->getIdPaySupplier() == PAY_SUPPLIER_CAIXA) {

            $paymentControlCheck = $this->commOnlPay->savePaymentBeforeGateway( $modFormPayment, $idPaySupplier, $this->user, $this->getIdPartner(), false );

            if (!$paymentControlCheck) {
                $this->redirPaymentMethod($modFormPayment, false, $this->user, true);
                return;
            }

            $payment = $commRecurring->changeProduct($this->user, $paymentControlCheck,
              Yii::app()->user->getIdPartnerCurNavigation());
        } else {

            // Validate user input, through standard validations of Yii Model Form. Both AllPago and Caixa:
            //$modFormPayment->idPayMethod = PaymentFactory::whichCardGatewayPay($this->user);
            if (!$modFormPayment->validate()) {
                $this->redirPaymentMethod($modFormPayment, false, $this->user, true);
                return;
            }

            $paymentControlCheck = $this->commOnlPay->savePaymentBeforeGateway($modFormPayment, $idPaySupplier,
              $this->user, $this->getIdPartner());

            if (!$paymentControlCheck) {
                $this->redirPaymentMethod($modFormPayment, false, $this->user, true);
                return;
            }

            $payment = $commRecurring->changeProduct($this->user, $paymentControlCheck,
              Yii::app()->user->getIdPartnerCurNavigation());
        }

        if ($payment && $payment instanceof Payment) {
            //  Send data to Selligent
            $commConnSelligent = new SelligentConnectorCommon();
            $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::changeProduct, $this->user,
              array('NEW_PRODUCT' => $payment->idPeriodPay, 'OLD_PRODUCT' => abs($payment->isPeriodPayChange),),
              "actionProcessNewPlan controller");
            // Redirects to Subscription page within the Profile:
            $this->redirect('/subscription/ChangePlanSuccessful');
            return;
        } else {
            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_LOGIC_L . ', change product',
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                $commRecurring->getErrorMessage()
            );
            $msgError = Yii::t('mainApp', 'errorsavepayment_key');
            if ($commRecurring->getErrorMessage() == 'key_before2days') {
                $msgError = Yii::t('mainApp', 'errorsavepayment_key') .
                    '(' . Yii::t('mainApp', 'key_before2days') . ')';
            }
            $modFormPayment->addError('idPayMethod', $msgError);
            $this->redirPaymentMethod($modFormPayment, false, $this->user, true);
        }
    }

    /**
     * @TODO
     */
    public function actionProcessNewPlanNocard()
    {
        $this->actionProcessNewPlan();
    }

    /**
     * For a FREE user the action URL to type in a new method of payment.
     * @param string $promoCode An existing promocode
     */
    public function actionPaymentMethod($promoCode = '', $segm_id = null, $oc = null)
    {
        // store segment id from zuora to allow go back to plans page
        if ($segm_id) {
            Yii::app()->user->setState('zuoraSegmentId', $segm_id);
        }
        // same with oc (old checkout)
        if ($oc) {
            Yii::app()->user->setState('zuoraOldCheckoutVersion', $oc);
        }

        $this->layout = "mainColumn3";
        $success = false;
        $user = new AbaUser();
        $modFormPayment = new PaymentForm();
        // Case it access to PAYMENT FROM INSIDE the CAMPUS
        if ($user->getUserById(Yii::app()->user->id)) {
            if (Yii::app()->user->refreshAndGetUserType($user) >= MAX_ALLOWED_LEVEL) {
                $this->redirect(Yii::app()->createUrl("/site/index"));
                return;
            }
            $modFormPayment = $this->fillPaymentForm($modFormPayment, $user);
            $prodPricePKSelected = HeMixed::getPostArgs("productPricePKSelected");
            if (!is_null($prodPricePKSelected)) {
                $modFormPayment->packageSelected = $prodPricePKSelected;
            }

            $this->redirPaymentMethod($modFormPayment, $success, $user);
        } else {
            $this->redirect(Yii::app()->createUrl("/site/noaccess"));
        }
    }

    /**
     * Called after user has enter all data in the payment form, and has selected PayPal method as a payment
     * Receives as POST all data necessary to carry out the whole process.
     *
     */
    public function actionProcessPaymentPayPal()
    {
        $this->layout = "mainColumn3";
        $user = new AbaUser();
        $user->getUserById(Yii::app()->user->id);
        $modFormPayment = new PaymentForm();
        $modFormPayment = $this->fillPaymentForm($modFormPayment, $user);
        $modFormPayment = $this->emptyCardDetailsForPaypal($modFormPayment);
        $this->user = $user;

        // In case the user refreshes the browser and the previous request finished then we redirect them
        // to the Home page.
        if (Yii::app()->user->refreshAndGetUserType($user) >= MAX_ALLOWED_LEVEL) {
            $this->redirPremiumTryPayAgain($user);
        }

        if ($modFormPayment->validate(null, true)) {
            $paymentControlCheck = $this->commOnlPay->savePaymentBeforeGateway($modFormPayment, PAY_SUPPLIER_PAYPAL,
              $this->user, $this->getIdPartner());
            if (!$paymentControlCheck) {
                $this->redirPaymentMethod($modFormPayment, false, $user);
                return;
            }

//            //
//            //#abawebapps-190
//            try {
//                $oPaymentControlCheck = new PaymentControlCheck();
//                if($oPaymentControlCheck->getPaymentControlCheckById($paymentControlCheck->id)) {
//                    $paymentControlCheck->lastAction = $oPaymentControlCheck->lastAction . " " . $paymentControlCheck->lastAction;
//                }
//            } catch (Exception $e) { }

            /*
             * We call to PAYPAL FACTORY to prepare the request to go to its FRONT-END page
             * */
            /* @var PaySupplierPayPal $paySupplier */
            $paySupplier = PaymentFactory::createPayment($paymentControlCheck);
            $paySuppExtId = strval($paySupplier->getPaySupplierId());
            $paySuppOrderId = $paymentControlCheck->id;
            $urlReview = 'payments/reviewpayment';
            $urlCancel = 'payments/landcancelpaypal';
            $retPaymentSupplier = $paySupplier->setExpressCheckout($paymentControlCheck, $urlReview, $urlCancel);
            if (!$retPaymentSupplier) {
                //'SetExpressCheckout failed: ' . print_r($result, true);
                $tokenPayPal = $paySupplier->getTokenPayPal();
                $errPaymentGateway = Yii::t('mainApp', 'gateway_failure_key_pp') .
                  " ( Response from PaymentSupplier PayPal = " .
                  $paySupplier->getErrorCode() . "-" . $paySupplier->getErrorString() . " )";
                $paymentControlCheck->updateStatusAfterPayment(PAY_FAIL, $paySuppExtId, $paySuppOrderId,
                  $errPaymentGateway, $tokenPayPal);

                $errPaymentGateway = Yii::t('mainApp', 'gateway_failure_key_pp');
                $modFormPayment->addError("idPayMethod", $errPaymentGateway);
                $this->redirPaymentMethod($modFormPayment, false, $user);
                return;
            }

            $paymentControlCheck->tokenPayPal = $paySupplier->getTokenPayPal();

            Yii::app()->user->setState("paymentControlCheck", serialize($paymentControlCheck));

            $this->redirect($paySupplier->getURLPayPalRedir());
        }

        $this->redirPaymentMethod($modFormPayment, false, $user);
    }

    /**
     * @param string $paycontrolcheckid
     * @param string $token
     * @param string $payId
     */
    public function actionLandCancelPayPal($paycontrolcheckid = "", $token = "", $payId = "")
    {
        $this->layout = "mainColumn3";

        $user = new AbaUser();
        if (!$user->getUserById(Yii::app()->user->id)) {
            // Redirect to HOME PAGE at the moment:
            $this->redirect("/site/index");
        }

        // @todo    PAYPAL: We should filter the HTTP REFERRER, to double check if it comes from
        // the PAYPAL url domain:
        // Retrieve back the paymentControlCheck:
        $paymentControlCheck = null;
        /* @var PaymentControlCheck $paymentControlCheck */
        if (!isset(Yii::app()->user->paymentControlCheck)) {
            $this->redirPaymentErrFromPayPal($paymentControlCheck, $user, Yii::t('mainApp', 'paypal_fail_timeout'));
            return;
        }
        $paymentControlCheck = unserialize(Yii::app()->user->paymentControlCheck);

        $this->redirPaymentErrFromPayPal($paymentControlCheck, $user, Yii::t('mainApp', 'paypal_cancelled_key'));
    }

    /**
     * http://abaenglish-local/es/payments/reviewpayment/paycontrolcheckid/580e838d?
     *  token=EC-5CL85741VC730622P&PayerID=TH7XDW8NGYPE8
     * @param string $paycontrolcheckid
     * @param string $token
     * @param string $PayerID
     */
    public function actionReviewPayment( $paycontrolcheckid="", $token="", $PayerID="" )
    {
        $this->layout = "mainColumn3";
        //-----
        $user = new AbaUser();
        if (!$user->getUserById(Yii::app()->user->id)) {
            // Redirect to HOME PAGE at the moment:
            $this->redirect("/site/index");
        }
        // Retrieve back the paymentControlCheck:
        $paymentControlCheck = null;
        /* @var PaymentControlCheck $paymentControlCheck */
        if (!isset(Yii::app()->user->paymentControlCheck)) {
            $this->redirPaymentErrFromPayPal($paymentControlCheck, $user, Yii::t('mainApp', 'paypal_fail_timeout'));
            return;
        }

        // In case the user refreshes the browser and the previous request finished then we redirect them
        // to the Home page.
        if (Yii::app()->user->refreshAndGetUserType($user) >= MAX_ALLOWED_LEVEL) {
            $this->redirPremiumTryPayAgain($user);
        }

        $paymentControlCheck = unserialize(Yii::app()->user->paymentControlCheck);

        if ($token == "") {
            $token = Yii::app()->getRequest()->getQuery('token');
        }
        if ($PayerID == "") {
            $PayerID = Yii::app()->getRequest()->getQuery('payid');
        }
        if ($token == "" || $PayerID == "") {
            $this->redirPaymentErrFromPayPal($paymentControlCheck, $user, Yii::t('mainApp', 'paypal_fail_reception'));
            return;
        }

        $paymentControlCheck->tokenPayPal = $token;
        $paymentControlCheck->payerPayPalId = $PayerID;

        /* @var PaySupplierPayPal $paySupplier */
        $paySupplier = PaymentFactory::createPayment($paymentControlCheck);
        $retPaymentSupplier = $paySupplier->getExpressCheckout($paymentControlCheck);
        if (!$retPaymentSupplier) {
            $paymentControlCheck->updateStatusAfterPayment(PAY_FAIL, PAY_SUPPLIER_PAYPAL,
              $paySupplier->getOrderNumber(),
              " CANCEL OR NOT ACCEPTED IN PAYPAL(ReviewExpressCheckout): " . " ABA could not connect to PayPal to collect your payment reference info. Please try again.",
              $token);
            $this->redirPaymentErrFromPayPal($paymentControlCheck, $user,
              Yii::t('mainApp', 'paypal_fail_collect_checkout'));
            return;
        }

        $InfoPayPalUser = $paySupplier->getInfoPayPalUser();
        $paymentControlCheck->lastAction = $paymentControlCheck->lastAction . " >>>[" . date("Y-m-d H:i:s") .
          "] Going through Review Page collected GetExpressCheckout successfully.";
        $paymentControlCheck->cardName = $InfoPayPalUser["Payer"];
        $paymentControlCheck->update(array("tokenPayPal", "payerPayPalId", "cardName", "lastAction"));
        Yii::app()->user->setState("paymentControlCheck", serialize($paymentControlCheck));
        /**
         * $user
         * $productPrice
         * $urlDoExpressCheckout
         */
        $urlDoExpressCheckout = $this->createUrl("payments/DoPaymentPayPal",
          array("paycontrolcheckid" => $paymentControlCheck->id, "tokenPayPal" => $paymentControlCheck->tokenPayPal));
        $productPrice = array();
        $productPrice["finalPrice"] = $paymentControlCheck->amountPrice;
        $productPrice["originalPrice"] = $paymentControlCheck->amountOriginal;
        $productPrice["ccyTransactionSymbol"] = $paymentControlCheck->currencyTrans;

        $prodDetails = new ProductPrice();
        $prodDetails->getProductByMixedId($paymentControlCheck->idProduct, $paymentControlCheck->idCountry,
          $paymentControlCheck->idPeriodPay, $paymentControlCheck->idPromoCode);

        $productPrice["packageTitleKey"] = $prodDetails->getPackageTitleKey();
        $productPrice['ccyTransactionSymbol'] = $prodDetails->getCcyDisplaySymbol();

        $userPayPal = array();
        $userPayPal["Payer"] = $InfoPayPalUser["Payer"];

        $paySupplier = PaymentFactory::createPayment($paymentControlCheck);
        try {
            $retPaymentSupplierLog = $paySupplier->runDebiting($paymentControlCheck);
        } catch (Exception $e) {
            $retPaymentSupplierLog = false;

            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_UNKNOWN_H . ': Exception in PayPal payment, in the DoExpressCheckout',
              HeLogger::IT_BUGS_PAYMENTS,
              HeLogger::CRITICAL,
              "Probably PayPal s fault. Even though check everything related with this payment."
            );
        }

        if (!$retPaymentSupplierLog) {
            $paySuppOrderId = $paySupplier->getOrderNumber();

            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_UNKNOWN_H . ': PAYPAL purchase has failed for some reason',
              HeLogger::IT_BUGS_PAYMENTS,
              HeLogger::CRITICAL,
              "User " . $user->email . " has tried to pay with paypal but for some reason we have received an error."
            );

            $tokenPayPal = "";
            try {
                $tokenPayPal = $paySupplier->getTokenPayPal();
            }
            catch(Exception $e) { }

            $paymentControlCheck->updateStatusAfterPayment(PAY_FAIL, PAY_SUPPLIER_PAYPAL, $paySuppOrderId,
              " CANCEL OR NOT ACCEPTED IN PAYPAL(DoExpressCheckout): " . $paySupplier->getErrorString(), $tokenPayPal);
            $this->redirPaymentErrFromPayPal($paymentControlCheck, $user,
              Yii::t('mainApp', 'paypal_process_fail_key'));
            return;
        }

        //---------------------------------------------------------
        $idPartner = $this->getIdPartner();
        $successAfterGateway = true;
        $lastAction = "";
        $paymentControlCheck->paySuppOrderId = $retPaymentSupplierLog->getOrderNumber();

        $modFormPayment = $this->resetFormToPayPal($paymentControlCheck, $user);
        $payment = $this->commOnlPay->savePaymentCtrlCheckToUserPayment($modFormPayment, $paymentControlCheck,
          $retPaymentSupplierLog, $idPartner, 1);
        if (!$payment) {
            /*  If Error 5. Update Payment FAILED */
            $lastAction = " savePaymentCtrlCheckToUserPayment: Saving paypal payment info details " .
              "or user or payment failed. Probably SQL failure. User " . $user->email . " , payment " . $paymentControlCheck->id;

            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_UNKNOWN_H . ': after paypal DoExpress failed database access for user ' . $user->email,
              HeLogger::IT_BUGS_PAYMENTS,
              HeLogger::CRITICAL,
              $lastAction . " You have to create manually de SUCCESS payment and convert to Premium manually based on " .
              " payment draft id = " . $paymentControlCheck->id
            );

            $this->redirPaymentErrFromPayPal($paymentControlCheck, $user, "");
            return;
        } else {
            /**********************************************************************************************************/
            /*****SUCCESS SO CONVERT USER TO PREMIUM******/
            /**********************************************************************************************************/
            /* 6. PayPal has confirmed payment successful, Do Express Checkout Success
             Update userType, expiration date and idPartners in AbaUser from Free to PREMIUM or WHATEVER */
            $aConfirmOpers = $this->convertPremiumCreateNextPay($payment, $user, $idPartner);
            /* @var Payment $nextPayment */
            $nextPayment = $aConfirmOpers["nextPayment"];
            /* @var ProductPrice $prodSelected */
            $prodSelected = $aConfirmOpers["prodSelected"];
            /* @var AbaUser $user */
            $user = $aConfirmOpers["user"];
            $lastAction .= $aConfirmOpers["errorDesc"];
            if (!$aConfirmOpers["success"]) {
                $successAfterGateway = $aConfirmOpers["success"];
            }


            //#5635
            $stPaymentInfo = $payment->getPaymentInfoForSelligent();

            //Send data to Selligent:
            $commConnSelligent = new SelligentConnectorCommon();
            $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::pagos, $user,
              array(
                "PROMOCODE" => $payment->idPromoCode,
                "ISEXTEND" => $payment->isExtend,
                "MONTH_PERIOD" => $stPaymentInfo['MONTH_PERIOD'],
                "PAY_DATE_START" => $stPaymentInfo['PAY_DATE_START'],
                "PAY_DATE_END" => $stPaymentInfo['PAY_DATE_END'],
                "AMOUNT_PRICE" => $stPaymentInfo['AMOUNT_PRICE'],
                "CURRENCY" => $stPaymentInfo['CURRENCY'],
                "PRICE_CURR" => $stPaymentInfo['PRICE_CURR'],
                "TRANSID" => $stPaymentInfo['TRANSID'],
              ), "actionDoPaymentPayPal controller");

            //#5087
            $commProducts = new ProductsPricesCommon();
            if ($commProducts->isPlanProduct($payment->idProduct)) {
                $commConnSelligent = new SelligentConnectorCommon();
                // IDUSER_WEB, PRODUCT_TYPE
                $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::pagosPlan,
                  $user, array(), "actionDoPaymentPayPal controller");
            } else {
                $commConnSelligent = new SelligentConnectorCommon();
                $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::pagosPlanNotification,
                  $user, array(), "actionDoPaymentPayPal controller");
            }

            /**********************************************************************************************************/
            /*****CREATION OF NEXT PAYMENT IN PAYPAL******/
            /**********************************************************************************************************/
            /*---->>>>>Now We deal with the next payment for PAYPAL -------------------------------------------------*/
            $nextPayment->id = HeAbaSHA::generateIdPayment($user, $nextPayment->dateToPay);
            $retRegistSupplierLog = $paySupplier->runCreateRecurring($nextPayment, $paymentControlCheck, $user);
            if (!$retRegistSupplierLog) {
                // Send an email to IT to inform that Paypal is not going to proceed with next Recurring Payment

                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_UNKNOWN_H . ': PayPal front payment, creation of Recurrent payment IN PAYPAL has failed',
                  HeLogger::IT_BUGS_PAYMENTS,
                  HeLogger::CRITICAL,
                  " PayPal front payment, creation of Recurrent payment IN PAYPAL has failed; user=" . $user->email .
                  ", payment control check id=" . $paymentControlCheck->id .
                  " user=" . $user->email . ", payment control check id=" . $paymentControlCheck->id
                );

                $lastAction .= " Failure creating recurring payment in PayPal. Function createRecurringPayments " .
                  "returned false.";
                $successAfterGateway = false;
                // As long as the subscription has not been created, we create it CANCELLED.
                $nextPayment->status = PAY_CANCEL;
            }
            // We collect and assign the registration id or profile Id from Paypal:
            $nextPayment->paySuppExtProfId = $paySupplier->getInfoPayment();
            $userCreditCard = new AbaUserCreditForms();
            $userCreditCard = $userCreditCard->getUserCreditFormById($payment->idUserCreditForm);
            $userCreditCard->registrationId = $nextPayment->paySuppExtProfId;
            $userCreditCard->update(array("registrationId"));

            if (!$nextPayment->paymentProcess()) {

                //  Send an email to IT to inform of this error

                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_UNKNOWN_H . ': PayPal front payment, Insert into database of next payment has failed',
                  HeLogger::IT_BUGS_PAYMENTS,
                  HeLogger::CRITICAL,
                  " PayPal front payment, Insert into database of next payment has failed; user=" . $user->email . ", payment control check id=" . $paymentControlCheck->id .
                  " user=" . $user->email . ", payment control check id=" . $paymentControlCheck->id
                );

                $lastAction .= "Failure inserting nextPayment into database. Check last payment for user " . $user->getId();
                $successAfterGateway = false;
            }
            /**********************************************************************************************************/
            /*****SUCCESS OR NOT, BUT FINISHED******/
            /**********************************************************************************************************/
            if ($successAfterGateway) {
                $lastAction = "[" . date("Y-m-d H:i:s") . "] DoExpresssCheckout, the payment, the recurrent payment, " .
                  "user credit card and user data was inserted properly into database for PAYPAL.";
                $paymentControlCheck->updateStatusAfterPayment(PAY_SUCCESS, $paySupplier->getPaySupplierId(),
                  $retPaymentSupplierLog->getOrderNumber(), $lastAction,
                  $paymentControlCheck->tokenPayPal,
                  $paymentControlCheck->payerPayPalId);
                $paymentControlCheck->updateReviewedLastWeekAttempts($user->getId());
            } else {
                // But PAYMENT HAS BEEN EXECUTED AND SUCCESSFULLY RECEIVED
                $lastAction .= "[" . date("Y-m-d H:i:s") . "] Payment has been accepted by the supplier " .
                  $retPaymentSupplierLog->getPaySuppExtId() .
                  " and the response of the supplier is within the table " .
                  $retPaymentSupplierLog->tableName() . " " . $paySupplier->getErrorString();
                // Send an email to our BackOffice to check this payment because it failed and it need to be reviewed

                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_UNKNOWN_H . ': Review line 368, after Payment confirmed, update failed',
                  HeLogger::IT_BUGS_PAYMENTS,
                  HeLogger::CRITICAL,
                  $lastAction
                );

                $paymentControlCheck->updateStatusAfterPayment(PAY_FAIL, $retPaymentSupplierLog->getPaySuppExtId(),
                  $retPaymentSupplierLog->getOrderNumber(), $lastAction, $paymentControlCheck->tokenPayPal,
                  $paymentControlCheck->payerPayPalId);
                $user->abaUserLogUserActivity->saveUserLogActivity("payment", $lastAction,
                  "payment done with paypal, fail to SQL updates");
            }
            // ---<<<<<<< End Next Payment
        }

        /**********************************************************************************************************/
        /*****LOGS******/
        /**********************************************************************************************************/
        // After Paypal payment and recurring payments created we redirect user to the confirmation page -----

        $productInfo = array();
        $productInfo['packageTitleKey'] = $prodSelected->getPackageTitleKey();
        $productInfo['finalPrice'] = $prodSelected->getFinalPrice();
        $productInfo['ccyTransactionSymbol'] = $prodSelected->getCcyTransactionSymbol();
        $this->redirConfirmedPayment($user, $payment, array("email" => $paymentControlCheck->cardName));
    }

    /**
     * It executes the real payment in PAYPAL, and it creates de Recurrent payment too.
     * http://abaenglish-local/es/payments/DoPaymentPayPal/paycontrolcheckid/580e838d/tokenPayPal/EC-5CL85741VC730622P
     * @param string $paycontrolcheckid
     * @param string $tokenPayPal
     */
    public function actionDoPaymentPayPal($paycontrolcheckid = "", $tokenPayPal = "")
    {
        ini_set('max_execution_time', 75);
        $this->layout = "mainColumn3";
        //-----
        $user = new AbaUser();
        if (!$user->getUserById(Yii::app()->user->id)) {
            // Redirect to HOME PAGE at the moment:
            $this->redirect("/site/index");
        }
        $this->user = $user;

        // In case the user refreshes the browser and the previous request finished then we redirect them
        // to the Home page.
        if (Yii::app()->user->refreshAndGetUserType($user) >= MAX_ALLOWED_LEVEL) {
            $this->redirPremiumTryPayAgain($user);
            return;
        }

        $paymentControlCheck = null;
        /* @var PaymentControlCheck $paymentControlCheck */
        if (isset(Yii::app()->user->paymentControlCheck)) {
            $paymentControlCheck = unserialize(Yii::app()->user->paymentControlCheck);
        }

        // Just to be sure is the same payment process initiated at the beginning:
        if ($paycontrolcheckid !== $paymentControlCheck->id || $tokenPayPal !== $paymentControlCheck->tokenPayPal) {
            $this->redirPaymentErrFromPayPal($paymentControlCheck, $user, Yii::t('mainApp', 'paypal_fail_cross_info'));
            return;
        }

        /* @var PaySupplierPayPal $paySupplier */
        /* @var PayGatewayLogPayPal $retPaymentSupplierLog */
        $paySupplier = PaymentFactory::createPayment($paymentControlCheck);
        try {
            $retPaymentSupplierLog = $paySupplier->runDebiting($paymentControlCheck);
        } catch (Exception $e) {
            $retPaymentSupplierLog = false;
            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_UNKNOWN_H . ': Exception in PayPal payment, in the DoExpressCheckout.',
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                'Probably PayPal s fault. Even though check everything related with this payment.'
            );
        }

        if (!$retPaymentSupplierLog) {
            $paySuppOrderId = $paySupplier->getOrderNumber();
            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_UNKNOWN_H . "PAYPAL purchase has failed for some reason email = " . $user->email,
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                "User " . $user->email . " has tried to pay with paypal but for some reason we have received an error."
            );
            $paymentControlCheck->updateStatusAfterPayment(PAY_FAIL, PAY_SUPPLIER_PAYPAL, $paySuppOrderId,
              " CANCEL OR NOT ACCEPTED IN PAYPAL(DoExpressCheckout): " . $paySupplier->getErrorString(), $tokenPayPal);
            $this->redirPaymentErrFromPayPal($paymentControlCheck, $user,
              Yii::t('mainApp', 'paypal_process_fail_key'));
            return;
        }

        //---------------------------------------------------------
        $idPartner = $this->getIdPartner();
        $successAfterGateway = true;
        $lastAction = "";
        $paymentControlCheck->paySuppOrderId = $retPaymentSupplierLog->getOrderNumber();

        $modFormPayment = $this->resetFormToPayPal($paymentControlCheck, $user);
        $payment = $this->commOnlPay->savePaymentCtrlCheckToUserPayment($modFormPayment, $paymentControlCheck,
          $retPaymentSupplierLog, $idPartner, 1);
        if (!$payment) {
            /*  If Error 5. Update Payment FAILED */
            $lastAction = " savePaymentCtrlCheckToUserPayment: Saving paypal payment info details " .
              "or user or payment failed. Probably SQL failure. User " . $user->email . " , payment " . $paymentControlCheck->id;
            HeLogger::sendLog(HeLogger::PREFIX_ERR_LOGIC_H . ": after paypal DoExpress failed database access for user " . $user->email,
              HeLogger::PAYMENTS, HeLogger::CRITICAL,
              $lastAction . " You have to create manually de SUCCESS payment and convert to Premium manually based on " .
              " payment draft id = " . $paymentControlCheck->id);
            $this->redirPaymentErrFromPayPal($paymentControlCheck, $user, "");
            return;
        } else {
            /**********************************************************************************************************/
            /*****SUCCESS SO CONVERT USER TO PREMIUM******/
            /**********************************************************************************************************/
            /* 6. PayPal has confirmed payment successful, Do Express Checkout Success
             Update userType, expiration date and idPartners in AbaUser from Free to PREMIUM or WHATEVER */
            $aConfirmOpers = $this->convertPremiumCreateNextPay($payment, $user, $idPartner);
            /* @var Payment $nextPayment */
            $nextPayment = $aConfirmOpers["nextPayment"];
            /* @var ProductPrice $prodSelected */
            $prodSelected = $aConfirmOpers["prodSelected"];
            /* @var AbaUser $user */
            $user = $aConfirmOpers["user"];
            $lastAction .= $aConfirmOpers["errorDesc"];
            if (!$aConfirmOpers["success"]) {
                $successAfterGateway = $aConfirmOpers["success"];
            }


            //#5635
            $stPaymentInfo = $payment->getPaymentInfoForSelligent();

            //Send data to Selligent:
            $commConnSelligent = new SelligentConnectorCommon();
            $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::pagos, $user,
              array(
                "PROMOCODE" => $payment->idPromoCode,
                "ISEXTEND" => $payment->isExtend,
                "MONTH_PERIOD" => $stPaymentInfo['MONTH_PERIOD'],
                "PAY_DATE_START" => $stPaymentInfo['PAY_DATE_START'],
                "PAY_DATE_END" => $stPaymentInfo['PAY_DATE_END'],
                "AMOUNT_PRICE" => $stPaymentInfo['AMOUNT_PRICE'],
                "CURRENCY" => $stPaymentInfo['CURRENCY'],
                "PRICE_CURR" => $stPaymentInfo['PRICE_CURR'],
                "TRANSID" => $stPaymentInfo['TRANSID'],
              ), "actionDoPaymentPayPal controller");

            //#5087
            $commProducts = new ProductsPricesCommon();
            if ($commProducts->isPlanProduct($payment->idProduct)) {
                $commConnSelligent = new SelligentConnectorCommon();
                // IDUSER_WEB, PRODUCT_TYPE
                $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::pagosPlan,
                  $user, array(), "actionDoPaymentPayPal controller");
            } else {
                $commConnSelligent = new SelligentConnectorCommon();
                $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::pagosPlanNotification,
                  $user, array(), "actionDoPaymentPayPal controller");
            }

            /**********************************************************************************************************/
            /*****CREATION OF NEXT PAYMENT IN PAYPAL******/
            /**********************************************************************************************************/
            /*---->>>>>Now We deal with the next payment for PAYPAL -------------------------------------------------*/
            $nextPayment->id = HeAbaSHA::generateIdPayment($user, $nextPayment->dateToPay);
            $retRegistSupplierLog = $paySupplier->runCreateRecurring($nextPayment, $paymentControlCheck, $user);
            if (!$retRegistSupplierLog) {
                // Send an email to IT to inform that Paypal is not going to proceed with next Recurring Payment
                $lastAction .= " Failure creating recurring payment in PayPal. Function createRecurringPayments " .
                  "returned false.";

                HeLogger::sendLog(" PayPal front payment, creation of Recurrent payment IN PAYPAL has failed; user=" . $user->email .
                  ", payment control check id=" . $paymentControlCheck->id, HeLogger::PAYMENTS, HeLogger::CRITICAL,
                  " user=" . $user->email . ", payment control check id=" . $paymentControlCheck->id);
                $successAfterGateway = false;
                // As long as the subscription has not been created, we create it CANCELLED.
                $nextPayment->status = PAY_CANCEL;
            }
            // We collect and assign the registration id or profile Id from Paypal:
            $nextPayment->paySuppExtProfId = $paySupplier->getInfoPayment();
            $userCreditCard = new AbaUserCreditForms();
            $userCreditCard = $userCreditCard->getUserCreditFormById($payment->idUserCreditForm);
            $userCreditCard->registrationId = $nextPayment->paySuppExtProfId;
            $userCreditCard->update(array("registrationId"));

            if (!$nextPayment->paymentProcess()) {
                //  Send an email to IT to inform of this error
                HeLogger::sendLog(" PayPal front payment, Insert into database of next payment has failed; user=" .
                  $user->email . ", payment control check id=" . $paymentControlCheck->id, HeLogger::PAYMENTS,
                  HeLogger::CRITICAL," user=" . $user->email . ", payment control check id=" . $paymentControlCheck->id);

                $lastAction .= "Failure inserting nextPayment into database. Check last payment for user " . $user->getId();
                $successAfterGateway = false;
            }
            /**********************************************************************************************************/
            /*****SUCCESS OR NOT, BUT FINISHED******/
            /**********************************************************************************************************/
            if ($successAfterGateway) {
                $lastAction = "[" . date("Y-m-d H:i:s") . "] DoExpresssCheckout, the payment, the recurrent payment, " .
                  "user credit card and user data was inserted properly into database for PAYPAL.";
                $paymentControlCheck->updateStatusAfterPayment(PAY_SUCCESS, $paySupplier->getPaySupplierId(),
                  $retPaymentSupplierLog->getOrderNumber(), $lastAction,
                  $paymentControlCheck->tokenPayPal,
                  $paymentControlCheck->payerPayPalId);
                $paymentControlCheck->updateReviewedLastWeekAttempts($user->getId());
            } else {
                // But PAYMENT HAS BEEN EXECUTED AND SUCCESSFULLY RECEIVED
                $lastAction .= "[" . date("Y-m-d H:i:s") . "] Payment has been accepted by the supplier " .
                  $retPaymentSupplierLog->getPaySuppExtId() .
                  " and the response of the supplier is within the table " .
                  $retPaymentSupplierLog->tableName() . " " . $paySupplier->getErrorString();
                // Send an email to our BackOffice to check this payment because it failed and it need to be reviewed
                HeLogger::sendLog("Review line 368, after Payment confirmed, update failed",
                  HeLogger::PAYMENTS, HeLogger::CRITICAL, $lastAction);
                $paymentControlCheck->updateStatusAfterPayment(PAY_FAIL, $retPaymentSupplierLog->getPaySuppExtId(),
                  $retPaymentSupplierLog->getOrderNumber(), $lastAction, $paymentControlCheck->tokenPayPal,
                  $paymentControlCheck->payerPayPalId);
                $user->abaUserLogUserActivity->saveUserLogActivity("payment", $lastAction,
                  "payment done with paypal, fail to SQL updates");
            }
            // ---<<<<<<< End Next Payment
        }

        /**********************************************************************************************************/
        /*****LOGS******/
        /**********************************************************************************************************/
        // After Paypal payment and recurring payments created we redirect user to the confirmation page -----

        $productInfo = array();
        $productInfo['packageTitleKey'] = $prodSelected->getPackageTitleKey();
        $productInfo['finalPrice'] = $prodSelected->getFinalPrice();
        $productInfo['ccyTransactionSymbol'] = $prodSelected->getCcyTransactionSymbol();
        $this->redirConfirmedPayment($user, $payment, array("email" => $paymentControlCheck->cardName));
    }

    /**
     * It processes the Credit card payments. Originally only with La Caixa.
     * @returns null It redirects to a Confirm page or again to the Payment Page
     */
    public function actionProcessPayment()
    {
        $this->layout = "mainColumn3";
        $user = new AbaUser();
        $user->getUserById(Yii::app()->user->id);
        $this->user = $user;

        // In case payment have just been done, and user refreshes or tries
        // to reload same request, we check his userType value.
        if (Yii::app()->user->refreshAndGetUserType($this->user) >= MAX_ALLOWED_LEVEL) {
            $this->redirPremiumTryPayAgain($user);
            return;
        }

        $modFormPayment = new PaymentForm();
        $modFormPayment = $this->fillPaymentForm($modFormPayment, $user);

        $this->emptyCardDetailsForCard($modFormPayment);

        // Validate user input, through standard validations of Yii Model Form. Both AllPago and Caixa:
        if (!$modFormPayment->validate()) {
            $this->redirPaymentMethod($modFormPayment, false, $this->user);
            return;
        }

        /*
         * It inserts all user form data into paymentControlCheck to avoid in case of error to lose the user input
         * */
        $paymentControlCheck = $this->commOnlPay->savePaymentBeforeGateway($modFormPayment,
          $modFormPayment->getIdPaySupplier(), $this->user,
          $this->getIdPartner());

        if (!$paymentControlCheck) {
            $this->redirPaymentMethod($modFormPayment, false, $this->user);
            return;
        }

        if ($modFormPayment->idPayMethod == PAY_METHOD_CARD) {

            $paramsPost = HeMixed::getPostArgs();

            if ($this->runCardTransaction($modFormPayment, $paymentControlCheck, $paramsPost)) {
                return;
            }
        }

        return;
    }

    public function actionProcessPaymentBoletoB()
    {
//        $this->version = 'B';
        $this->actionProcessPaymentBoleto();
    }


    /** Process the Boleto Request. If a Boleto has been recently requested it redirects to the subscription page.
     *
     */
    public function actionProcessPaymentBoleto()
    {
        // We check if a Boleto has been requested in the last 5 minutes, and in that case we redirect him
        // to the subscription page
        $boleto = new AbaPaymentsBoleto();
        if ($boleto->anyPendingLastTime(Yii::app()->user->id, 5)) {
            $this->redirect(Yii::app()->createUrl("/subscription/index"));
        }

        $this->layout = "mainColumn3";
        $user = new AbaUser();
        $user->getUserById(Yii::app()->user->id);
        $this->user = $user;

        // In case payment have just been done, and user refreshes or tries
        // to reload same request, we check his userType value.
        if (Yii::app()->user->refreshAndGetUserType($this->user) >= MAX_ALLOWED_LEVEL) {
            $this->redirPremiumTryPayAgain($user);
            return;
        }

        $modFormPayment = new PaymentForm();
        $modFormPayment = $this->fillPaymentForm($modFormPayment, $user);
        $this->emptyCardDetailsForBoleto($modFormPayment);
        // Validate user input, through standard validations of Yii Model Form. Both AllPago and Caixa:
        if (!$modFormPayment->validate()) {
            $this->redirPaymentMethod($modFormPayment, false, $this->user);
            return;
        }

        $paymentControlCheck = $this->commOnlPay->savePaymentBeforeGateway($modFormPayment,
          $modFormPayment->getIdPaySupplier(), $this->user, $this->getIdPartner());

        if (!$paymentControlCheck) {
            $this->redirPaymentMethod($modFormPayment, false, $this->user);
            return;
        }

        if ($modFormPayment->idPayMethod == PAY_METHOD_BOLETO) {
            $this->runBoletoTransaction($modFormPayment, $paymentControlCheck);
        }
    }

    /** To force URL display for tracking purposes...
     *
     * @param string $idPay
     */
    public function actionConfirmedPayment($idPay="")
    {
        $payment = new Payment();
        $payment->getPaymentById($idPay);

        $moUser = new AbaUser();
        $moUser->getUserById(Yii::app()->user->getId());

        //#ZOR-306
        try {
            if($idPay == "") {
                $payment->getLastSuccessPayment($moUser->id);

                if($payment->paySuppExtId <> PAY_SUPPLIER_Z) {
                    $payment = new Payment();
                }
            }
        }
        catch (Exception $e) { }

        if ($payment->status != PAY_SUCCESS) {
            $modFormPayment = $this->fillPaymentForm(new PaymentForm(), $moUser, false);
            $this->redirPaymentMethod($modFormPayment, false, $moUser);
            return;
        }

        unset(Yii::app()->user->paymentControlCheck);
        unset(Yii::app()->session['paymentControlCheck']);
        Yii::app()->user->setState("paymentMethodAttempt", null);
        Yii::app()->user->setState("paymentControlCheckId", null);
        Yii::app()->user->setState("paymentControlStatus", null);

        //
        //#5635
        $stConfirmedPaymentData = $this->getConfirmPaymentData($moUser, $payment);

        $this->layout = "mainResponsive";
        $data = array(
            'isPaymentPlus' => false,
            'payment' => $payment,
            'sPaymentConfirmMonths' => $stConfirmedPaymentData['sPaymentConfirmMonths'],
            'sPaymentConfirmDateToRenewal' => $stConfirmedPaymentData['sPaymentConfirmDateToRenewal'],
            'sPaymentConfirmPrice' => $stConfirmedPaymentData['sPaymentConfirmPrice'],
            'userData' => array(
                'email' => $moUser->email,
                'fullName' => $moUser->name.' '.$moUser->surnames
            )
        );
        parent::updateDataLayerPaymentDetails($payment);

        $this->headerTitle = Yii::t('mainApp', 'paymentConfirm_info1');

        $this->render('confirmedPayment', $data);
    }

    /**
     * @param PaymentForm $modFormPayment
     * @param PaymentControlCheck $paymentControlCheck
     * @param PayGatewayLogLaCaixaResRec|PayGatewayLogAllpago|PayGatewayLogAdyenResRec $retPaySupplierLog
     * @param PaySupplierLaCaixa|PaySupplierAllPagoBr|PaySupplierAllPagoMx|PaySupplierAdyen|PaySupplierAdyenHpp $paySupplier
     *
     * @return bool|Payment
     */

    protected function runAfterCardTransactionConfirmed(
      PaymentForm $modFormPayment,
      PaymentControlCheck $paymentControlCheck,
      $retPaySupplierLog,
      $paySupplier
    ) {
        /* +++++++++++++ Point of no return +++++++++++++++++*/
        $idPartner = $this->getIdPartner();

        $successAfterGateway = true;
        $lastAction = "";

        //
        // @TODO ?!
        //
        $payment = $this->commOnlPay->savePaymentCtrlCheckToUserPayment($modFormPayment, $paymentControlCheck,
          $retPaySupplierLog, $idPartner, 1);

        if (!$payment) {
            /*  If Error 5. Update Payment FAILED */
            $lastAction = " savePaymentCtrlCheckToUserPayment: Saving user_credit_card or user or payment failed. Probably SQL failure.";
            $successAfterGateway = false;
        } else {
            /**********************************************************************************************************/
            /*****SUCCESS SO CONVERT USER TO PREMIUM******/
            /**********************************************************************************************************/
            /* 6. Update userType, expiration date and idPartners in AbaUser from Free to PREMIUM or WHATEVER */
            $aConfirmOpers = $this->convertPremiumCreateNextPay($payment, $this->user, $idPartner);
            /* @var Payment $nextPayment */
            $nextPayment = $aConfirmOpers["nextPayment"];
            /* @var ProductPrice $prodSelected */
            $this->user = $aConfirmOpers["user"];
            $lastAction .= $aConfirmOpers["errorDesc"];
            if (!$aConfirmOpers["success"]) {
                $successAfterGateway = $aConfirmOpers["success"];
            }

            /* * 5.1. Insert Next Payment PENDING */
            $nextPayment->id = HeAbaSHA::generateIdPayment($this->user, $nextPayment->dateToPay);
            $userCreditCard = new AbaUserCreditForms();
            $userCreditCard = $userCreditCard->getUserCreditFormById($payment->idUserCreditForm);

            /* @var PayGatewayLogAllpago $retRegistSupplierLog */
            $retRegistSupplierLog = $paySupplier->runCreateRecurring($nextPayment, $paymentControlCheck, $this->user);

            if ($retRegistSupplierLog && $retRegistSupplierLog instanceof PayGatewayLog) {
                $registrationId = $retRegistSupplierLog->getUniqueId();
                $userCreditCard->registrationId = $registrationId;

                $userCreditCard->update(array("registrationId"));

                $nextPayment->paySuppExtProfId = $registrationId;
                $nextPayment->paySuppExtUniId = $registrationId;
                $nextPayment->paySuppOrderId = "";
            } elseif ($retRegistSupplierLog) {
                // No need to create profile nor anything else, success for this supplier. La Caixa basically until 2014-12-04
            } else {
                $lastAction .= $paySupplier->getSupplierName() .
                  '.runCreateRecurring(): Registering user credit card for Next Pending payment failed ' .
                  $paySupplier->getErrorCode() . " " . $paySupplier->getErrorString();
                $successAfterGateway = false;
            }

            if (!$nextPayment->paymentProcess()) {
                $lastAction .= " insertPayment(): Inserting next recurrent payment failed.";
                $successAfterGateway = false;
            }
        }

        if ($successAfterGateway) {
            $lastAction .= " The payment, user credit card and user data was inserted properly into database. ";
            $paymentControlCheck->updateStatusAfterPayment(PAY_SUCCESS, $retPaySupplierLog->getPaySuppExtId(),
              $retPaySupplierLog->getOrderNumber(), $lastAction);
            $paymentControlCheck->updateReviewedLastWeekAttempts($this->user->getId());
        } else {
            /* But PAYMENT HAS BEEN EXECUTED AND SUCCESSFULLY RECEIVED but errors have occurred after this,
            so our database is inconsistent. */
            $lastAction .= " Payment has been accepted by the supplier " . $retPaySupplierLog->getPaySuppExtId() .
              " and the response of the supplier is within the table " .
              $retPaySupplierLog->tableName() . ", id = " . $retPaySupplierLog->id;
            $paymentControlCheck->updateStatusAfterPayment(PAY_FAIL, $retPaySupplierLog->getPaySuppExtId(),
              $retPaySupplierLog->getOrderNumber(), $lastAction);
            $this->user->abaUserLogUserActivity->saveUserLogActivity("payment", $lastAction, "payment done");

            HeLogger::sendLog(HeLogger::PREFIX_ERR_UNKNOWN_H . ' Payment through ' . $paySupplier->getSupplierName() .
              ' successful, but finished with errors in our database; user=' .
              $this->user->email . ", payment id=" . $paymentControlCheck->id, HeLogger::IT_BUGS, HeLogger::CRITICAL,
              " user=" . $this->user->email . ", payment control check id=" . $paymentControlCheck->id);
        }

        // --------- After Supplier accepted payment
        return $payment;
    }

    /** Only function in charge to prepare and to render the final page of payment method selection
     *
     * @param PaymentForm $modFormPayment
     * @param $success
     * @param AbaUser $user
     * @param bool $isChangePlan
     */
    protected function redirPaymentMethod(PaymentForm $modFormPayment, $success, AbaUser $user, $isChangePlan = false)
    {
        $modFormPayment->productsPricesPlans = $this->getAllAvailableProductsPrices($user);

        $currency = null;
        if (!empty($modFormPayment->productsPricesPlans)) {
            $currency = $modFormPayment->productsPricesPlans[0]['ccyIsoCode'];
        }
        $listOfCreditCards = PaymentFactory::listCardsAvailable($user, $currency);

        $arrayErrors = $modFormPayment->getErrors();
        if (!empty($arrayErrors)) {
            $attempt = 1;
            $sessionAttemptVar = Yii::app()->user->getState('paymentMethodAttempt');
            if (isset($sessionAttemptVar)) {
                $attempt = intval($sessionAttemptVar) + 1;
            }
            Yii::app()->user->setState("paymentMethodAttempt", $attempt);
        }
        // If user is PREMIUM, redirect to his profile. This scenario can only happen if
        // the user has paid through Paypal and he did not wait til the end.
        if ($user->userType == PREMIUM) {
            $isChangePlan = true;
        }

        $this->layout = "mainResponsive";
        $urlReturn = 'payments/payment';

        $urlsFormProcess = array(
          PAY_METHOD_PAYPAL => $this->createURL('/payments/processPaymentPayPal'),
          PAY_METHOD_CARD => $this->createURL('/payments/processPayment'),
          PAY_METHOD_BOLETO => $this->createURL('/payments/ProcessPaymentBoleto'),
          PAY_METHOD_ADYEN_HPP => ""
        );

        if ($isChangePlan) {
            $urlReturn = 'payments/ChangePlan';

            $urlsFormProcess[PAY_METHOD_CARD] = $this->createURL('/payments/processNewPlan');
            $urlsFormProcess[PAY_METHOD_PAYPAL] = $this->createURL('/payments/processNewPlan');
        }

        $moLastPaySubs = null;
        if ($isChangePlan) {
            $moPay = new Payment();
            $moPendingPay = $moPay->getLastPayPendingByUserId($user->getId());
            $dateStart = (isset($moPendingPay->dateToPay) ? $moPendingPay->dateToPay : HeDate::todaySQL());
        } else {
            $dateStart = HeDate::todaySQL();
        }

        $monthsPeriod = '';
        foreach ($modFormPayment->productsPricesPlans as $key => $productPrice) {
            if ($modFormPayment->packageSelected == $productPrice["productPricePK"]) {
                $monthsPeriod = $productPrice['monthsPeriod'];
                break;
            }
        }

        $idPaySupplier = PaymentFactory::whichCardGatewayPay($user);
        $paymentControlCheck = new PaymentControlCheck();
        if (isset(Yii::app()->user->paymentControlCheck)) {
            /* @var PaymentControlCheck $paymentControlCheck */
            $paymentControlCheck = unserialize(Yii::app()->user->paymentControlCheck);
            $idPaySupplier = $paymentControlCheck->paySuppExtId;
        } else {
            $paymentControlCheck->paySuppExtId = $idPaySupplier;
        }

        $commRecurring = new RecurrentPayCommon();
        $typeSubsStatus = $commRecurring->getDatesSubscription($user, $dateStart, $monthsPeriod,
          $modFormPayment->packageSelected);
        $aIdsPaysMethods = PaymentFactory::whichMethodsAvailable($user);

        //
        //#4986
        $adyenPaymentData = array(
          "paymentAmount" => 0,
          "currencyCode" => "",
          "countryCode" => "",
          "skinCode" => Yii::app()->config->get("PAY_ADYEN_HPP_SKIN_DEFAULT_NAME"),
          "sessionValidity" => "",
          "idProduct" => "",
        );

        $isPlanPriceFree = false;

        foreach ($modFormPayment->productsPricesPlans as $iKey => $productPrice) {

            if ($modFormPayment->packageSelected == $productPrice["productPricePK"]) {

                $idProduct = explode("_", $productPrice['idProduct']);

                $abaCountry = new AbaCountry();
                $abaCountry->getCountryById($idProduct[0]);

                $adyenPaymentData["paymentAmount"] = $productPrice['finalPrice'];
                $adyenPaymentData["currencyCode"] = $productPrice['ccyIsoCode'];
                $adyenPaymentData["countryCode"] = HeMixed::getCountryCodeIso2ForAdyen($abaCountry->iso); // RU, ES,..

                //
                //#abawebapps-158
                $adyenPaymentData["idProduct"] = $productPrice['idProduct'];

                $isPlanPriceFree = $productPrice['isPlanPrice'];
                break;
            }
        }

        //#5191
        $dPay = HeMixed::getGetArgs("dPay");

        //terms and conditions data to render inline
        $payCommSupplier = PaymentFactory::createPayment( NULL, $idPaySupplier);
        $idTextSpecificConditions = $payCommSupplier->getListSpecificConditions();

        $data = array(
          'modelPayForm' => $modFormPayment,
          'listOfCreditCards' => $listOfCreditCards,
          'urlsFormProcess' => $urlsFormProcess,
          'urlReturn' => $urlReturn,
          'success' => $success,
          'user' => $user,
          'paySuppExtId' => $idPaySupplier,
          'aIdsPaysMethods' => $aIdsPaysMethods,
          'aSpecialFields' => $modFormPayment->getSpecialFieldsPaySupplier($paymentControlCheck),
          'typeSubsStatus' => $typeSubsStatus,
          'aAllFieldsByMethod' => PaymentFactory::getAllFieldsByMethodByCountry($aIdsPaysMethods, $user->countryId),
          'isPaymentPlus' => false,
          'isPlanPrice' => false,
          'isPlanPriceFree' => $isPlanPriceFree,
          'isUpgrade' => false,
          'adyenPublicKey' => Yii::app()->config->get("PAY_ADYEN_SOAP_PUBLIC_KEY"),
          'adyenPaymentData' => $adyenPaymentData,
          'dPay' => $dPay,
          'bDiscardCardData' => true,
          'idTextSpecificConditions' => $idTextSpecificConditions
        );

        $this->renderFooter = false;

        $this->render('paymentMethod', $data);
    }

    /** Unique function to render the selection product page whether for FREE or PREMIUM users.
     *
     * @param $urlContinue
     */
    protected function redirPayProductSelection($urlContinue)
    {
        $aRetProdsAndPrices = $this->collectAndSelectProducts($this->user, true);
        $products = $aRetProdsAndPrices[0];
        $packageSelected = $aRetProdsAndPrices[1];
        $anyDiscount = $aRetProdsAndPrices[2];
        $typeDiscount = $aRetProdsAndPrices[3];
        $validationTypePromo = $aRetProdsAndPrices[4];

        //
        //#5087
        $productsPlan = array();

        foreach ($products as $product) {
            if ($product['isPlanPrice'] == 1) {
                $productsPlan[] = $product;
            }
        }

        // Register locations every time we go through Home Page:
        $commUser = new UserCommon();
        $commUser->registerGeoLocation($this->user, Yii::app()->request->getUserHostAddress());

        // In case of a change of subscription we check last product, so current one:
        $currentProduct = false;
        $dateToPay = false;
        if ($this->user->userType == PREMIUM) {
            $moLastPay = new Payment();
            if ($moLastPay->getLastPayPendingByUserId($this->user->id)) {
                $currentProduct = $moLastPay->idProduct;
            }
        }

        $aInfoBoletos = $this->getAnyLastBoletoPending();
        $boletoPrice = $aInfoBoletos['boletoPrice'];
        $productBoleto = $aInfoBoletos['productBoleto'];
        $moBoleto = $aInfoBoletos['moBoleto'];

//        $this->layout = "mainColumn3";
        $this->layout = "mainResponsive";
        $data = array(
          'isPaymentPlus' => false,
          'products' => $products,
          'productsPlan' => $productsPlan,
          'packageSelected' => $packageSelected,
          'currentProduct' => $currentProduct,
          'anyDiscount' => $anyDiscount,
          'typeDiscount' => $typeDiscount,
          'validationTypePromo' => $validationTypePromo,
          'urlContinue' => $urlContinue,
          'urlWithPromo' => 'payments/' . $this->action->id,
          'moBoleto' => $moBoleto,
          'boletoPrice' => $boletoPrice,
          'productBoleto' => $productBoleto,
          'sPaymentLanguage' => strtolower(Yii::app()->user->getLanguage()),
          'iPaymentCountry' => Yii::app()->user->getCountryId(),
        );

        // In case of PREMIUM (Upgrade/Downgrade) flow, we display another view.
        // Almost identicall to the FREE version at 2014-11-11:
        if ($this->user->userType == PREMIUM && $moLastPay) {
            $data['dateToPay'] = $moLastPay->dateToPay;
            $data['isChangePlan'] = true;
//            $this->render('changePlan', $data);
            $this->render('payment', $data);
        } else {
            $data['isChangePlan'] = false;
            $this->render('payment', $data);
        }
    }

    /** Returns validations errors processes.
     * @param PaymentControlCheck $curPayment
     * @param AbaUser $user
     * @param         $error
     */
    protected function redirPaymentErrFromPayPal($curPayment, AbaUser $user, $error)
    {

        $modFormPayment = new PaymentForm();
        $modFormPayment = $this->fillPaymentForm($modFormPayment, $user);
        $modFormPayment = $this->emptyCardDetailsForPaypal($modFormPayment);
        $modFormPayment->idPayMethod = PAY_METHOD_PAYPAL;
        if (isset($curPayment)) {
            $curPayment->lastAction = $curPayment->lastAction . ">>>> [" . date("Y-m-d H:i:s") .
              "] The process in PayPal was cancelled or was unsuccessful: " . $error;
            $curPayment->update(array("lastAction"));
            $this->resetFormToPayPal($curPayment, $user);
            $modFormPayment->packageSelected = $curPayment->idProduct .
              DELIMITER_KEYS . $curPayment->idCountry . DELIMITER_KEYS . $curPayment->idPeriodPay;
        }

        if ($error == '') {
            $modFormPayment->addError("idPayMethod", Yii::t('mainApp', 'paypal_process_fail_key'));
        } else {
            $modFormPayment->addError("idPayMethod", $error);
        }

        $this->redirPaymentMethod($modFormPayment, false, $user);
    }

    /** Redirects to the confirmation view page.
     * @param AbaUser $user
     * @param Payment $payment
     * @param null $userPayPal
     */
    protected function redirConfirmedPayment(AbaUser $user, Payment $payment, $userPayPal = null)
    {
        //#5533
        $isMobile = HeDetectDevice::isMobile();

        if ($isMobile AND $payment->idPartner == PARTNER_ID_MOBILE_ANDROID) {
            //
            $rulerWs = new RegisterUserService();
            $sUrl = $rulerWs->getRedirectUrl($user, '', '', 'PAYMENT_CONFIRMED', false, '', $payment->id);

            //#6013
            $this->render('/mobile/mobileRedirect', array(
              'campusUrl' => $sUrl,
              'metaUrl' => HeComm::getMobileRedirectUrl('ANDROID_PAYMENT_CONFIRMATION', array(
                "paymentId" => $payment->id,
                "paymentAmountPrice" => $payment->amountPrice,
                "paymentProductiD" => $payment->idProduct,
                "paymentCurrencyTrans" => $payment->currencyTrans,
              ))
            ));
        } else {
            $this->redirect(Yii::app()->createUrl("/payments/confirmedPayment", array('idPay' => $payment->id)));
        }
    }

    /**
     * @param PaymentControlCheck $payDraft
     * @param AbaPaymentsBoleto $boleto
     */
    protected function redirConfirmedBoleto(PaymentControlCheck $payDraft, AbaPaymentsBoleto $boleto)
    {
        unset(Yii::app()->user->paymentControlCheck);
        unset(Yii::app()->session['paymentControlCheck']);
        Yii::app()->user->setState("paymentMethodAttempt", null);
        Yii::app()->user->setState("paymentControlCheckId", null);
        Yii::app()->user->setState("paymentControlStatus", null);

        $this->render("requestedBoleto", array(
          'payment' => $payDraft,
          'urlBoleto' => $boleto->url,
          'dueDateBoleto' => $boleto->dueDate
        ));
        return;
    }

    /** Convert To Premium and creates Next payment model in memory
     * @param Payment $payment
     * @param AbaUser $user
     * @param $idPartner
     * @return array
     */
    protected function convertPremiumCreateNextPay(Payment $payment, AbaUser $user, $idPartner)
    {
        $successAfterGateway = true;
        $lastAction = "";
        /* @var Payment $nextPayment */
        $nextPayment = $payment->createDuplicateNextPayment();
        if (!$nextPayment) {
            $lastAction = " createDuplicateNextPayment: Clonning payment failed. Check periodicity, product, amount.";
            $successAfterGateway = false;
        }

        //#4400
        if ($payment->paySuppExtId == PAY_SUPPLIER_ADYEN OR $payment->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP) {
            $nextPayment->paySuppExtUniId = $payment->paySuppExtUniId;
        }

        //#5825
        if ($payment->idPartner == PARTNER_ID_MOBILE_ANDROID){
            $nextPayment->idPartner = PARTNER_ID_ANDROID_RECURRING;
        }

        /* Double-Check product type again   */
        $prodSelected = new ProductPrice();
        $prodSelected = $prodSelected->getProductById($payment->idProduct);
        if (!$prodSelected) {
            $lastAction .= " getProductById/updateUserType: Assigning new access type to the user failed.";
            $successAfterGateway = false;
            $userTypeProd = PREMIUM;
        } else {
            $userTypeProd = $prodSelected->userType;
        }

        $commUser = new UserCommon();
        $user = $commUser->convertToPremium($user, $idPartner, $nextPayment->dateToPay, $userTypeProd);
        if (!$user) {
            $user = $this->user;
            $lastAction .= "Failed to convert to Premium. Review user " . $user->id;
            $successAfterGateway = false;
        }
        // For the current session we also have to update his userType to PREMIUM.
        Yii::app()->user->setState("role", $userTypeProd);

        // For the current session we also have to update the user's IdPartnerCurrent.
        //Yii::app()->user->setState("idPartnerCurNavigation", $idPartner);

        Yii::app()->user->setState('currentPartnerId',$idPartner);
        Yii::app()->user->setState("idPartnerCurrent", $idPartner);
        echo "";
        return array(
          "success" => $successAfterGateway,
          "errorDesc" => $lastAction,
          "nextPayment" => $nextPayment,
          "user" => $user,
          "prodSelected" => $prodSelected
        );
    }

    /** Resets UI form for PayPal access method.
     * @param PaymentControlCheck $paymentControlCheck
     * @param AbaUser $user
     *
     * @return PaymentForm
     */
    protected function resetFormToPayPal(PaymentControlCheck $paymentControlCheck, AbaUser $user)
    {
        $modFormPayment = new PaymentForm();
        $modFormPayment->mail = $user->email;
        $modFormPayment->firstName = $paymentControlCheck->getFirstNameTmp();
        $modFormPayment->secondName = $paymentControlCheck->getSecondNameTmp();
        $modFormPayment->creditCardName = $paymentControlCheck->cardName;
        $modFormPayment = $this->emptyCardDetailsForPaypal($modFormPayment);
        return $modFormPayment;
    }

    /**
     * Pre processes the Credit card and redirect to HPP Adyen payments page. Only with Adyen.
     * @returns null It redirects to a Confirm page or again to the Payment Page
     */
    public function actionProcessPaymentAdyen()
    {
        $this->layout = "mainAdyenSkin";
        $user = new AbaUser();
        $user->getUserById(Yii::app()->user->id);
        $this->user = $user;

        // Extend payments
//        // In case payment have just been done, and user refreshes or tries
//        // to reload same request, we check his userType value.
//        if (Yii::app()->user->refreshAndGetUserType($this->user) >= MAX_ALLOWED_LEVEL) {
//            $this->redirPremiumTryPayAgain($user);
//            return;
//        }

        $modFormPayment = new PaymentForm();
        $modFormPayment = $this->fillPaymentForm($modFormPayment, $user);
        $this->emptyCardDetailsForCard($modFormPayment);
        // Validate user input, through standard validations of Yii Model Form. Both AllPago and Caixa:
        if (!$modFormPayment->validate()) {
//            $this->redirPaymentMethod($modFormPayment, false, $this->user);
//            return;
        }

        //
        //#abawebapps-158
        $stAdyenHppData = array();
        if ($modFormPayment->idPayMethod == PAY_METHOD_ADYEN_HPP) {
            $stAdyenHppData = HeMixed::getPostArgs();
        }

        /**
         * It inserts all user form data into paymentControlCheck to avoid in case of error to lose the user input
         */
        $paymentControlCheck = $this->commOnlPay->savePaymentBeforeGateway($modFormPayment,
          $modFormPayment->getIdPaySupplier(), $this->user, $this->getIdPartner(), false, false, $stAdyenHppData);

        if (!$paymentControlCheck) {
//            $this->redirPaymentMethod($modFormPayment, false, $this->user);
//            return;
        }

        if ($modFormPayment->idPayMethod == PAY_METHOD_ADYEN_HPP) {
            $this->runAdyenHppTransaction($modFormPayment, $paymentControlCheck, $stAdyenHppData);
        }
        return;
    }

    /**
     * Get ADYEN payment methods by skin & country & currency
     *
     */
    public function actionPaymentMethodsAdyen()
    {

        //Light security measure to avoid DOS attacks.
        HeMixed::securePublicUrlRequest("payments/paymentmethodsadyen");
        $this->layout = "";

        // collect user input data
        $postValuesHpp = HeMixed::getPostArgs();

        $renderData = array();

        if (isset($postValuesHpp['idPayMethod']) AND $postValuesHpp['idPayMethod'] == PAY_METHOD_ADYEN_HPP) {

            //
            //#abawebapps-158
            $commProducts = new ProductsPricesCommon();

            //
            // 1. GET DEFAULT PAYMENT METHODS BY DEFAULT CURRENCY, COUNTRY & AMOUNT
            //
            $adyenHppPaymentMethods = $this->runAdyenHppPaymentMethodsTransaction($postValuesHpp);

            if (count($adyenHppPaymentMethods) > 0) {
                $adyenHppPaymentMethods = $commProducts->addAdyenHppPrices($adyenHppPaymentMethods, $this->user, $postValuesHpp);

                $renderData['adyenHppPaymentMethods'] = $adyenHppPaymentMethods;
            }

            //
            // 2. GET CUSTOM PAYMENT METHODS WITH SPECIFIC CURRENCY, COUNTRY & AMOUNT
            //
            $stAdyenHppCurrenciesByProduct = $commProducts->getAdyenHppCurrenciesByProduct($postValuesHpp);

            $stAllAdyenHppPaymentMethods = $adyenHppPaymentMethods;

            foreach($stAdyenHppCurrenciesByProduct as $sKey => $stPaymentMethodByProduct) {

                $bFoundBrand = false;

                foreach($adyenHppPaymentMethods as $sKeyBrandName => $stPaymentMethod) {
                    if ($stPaymentMethodByProduct["paymentMethod"] == $stPaymentMethod["brandCode"]) {
                        $bFoundBrand = true;
                        continue;
                    }
                }

                if(!$bFoundBrand) {

                    $postValuesHppByCurrency = $postValuesHpp;

                    $postValuesHppByCurrency["paymentAmount"] = $stPaymentMethodByProduct["priceAdyenHpp"];
                    $postValuesHppByCurrency["currencyCode"] = $stPaymentMethodByProduct["currencyAdyenHpp"];
                    $postValuesHppByCurrency["idProduct"] = $stPaymentMethodByProduct["idProduct"];

                    $adyenHppPaymentMethodsByCurrency = $this->runAdyenHppPaymentMethodsTransaction($postValuesHppByCurrency);

                    if (count($adyenHppPaymentMethodsByCurrency) > 0) {
                        $adyenHppPaymentMethodsByCurrency = $commProducts->addAdyenHppPrices($adyenHppPaymentMethodsByCurrency, $this->user, $postValuesHppByCurrency);

                        foreach($adyenHppPaymentMethodsByCurrency as $sKeyBrandCode => $stBrandData) {

                            if(!array_key_exists($sKeyBrandCode, $stAllAdyenHppPaymentMethods)) {
                                $stAllAdyenHppPaymentMethods[$sKeyBrandCode] = $stBrandData;
                            }
                        }
                    }
                }
            }

            if (count($stAllAdyenHppPaymentMethods) > 0) {
                $renderData['adyenHppPaymentMethods'] = $stAllAdyenHppPaymentMethods;

                $this->renderPartial("adyenAlternativePayments", $renderData);
            } else {
                $renderData['errMsg'] = Yii::t('mainApp', 'payments_adyen_err_001');

                $this->renderPartial("adyenAlternativePayments", $renderData);
            }

        } else {
            $renderData['errMsg'] = Yii::t('mainApp', 'payments_adyen_err_000');

            $this->renderPartial("adyenAlternativePayments", $renderData);
        }

        return;
    }

    /**
     *
     */
    public function actionConfirmedPaymentAdyen()
    {

        // 1. Recullir dades
        $getArgs = HeMixed::getGetArgs();

        $sRes = ADYEN_HPP_RESULT_INTERNAL_ERR;

        $user = new AbaUser();
        $user->getUserById(Yii::app()->user->id);
        $this->user = $user;

        // Extend payments
//        // In case payment have just been done, and user refreshes or tries
//        // to reload same request, we check his userType value.
//        if (Yii::app()->user->refreshAndGetUserType($this->user) >= MAX_ALLOWED_LEVEL) {
//            $this->redirPremiumTryPayAgain($user);
//            return;
//        }

        //
        $paymentControlCheck = new PaymentControlCheck();

        //
        $payGatewayLogAdyenReqSent = new PayGatewayLogAdyenReqSent();

        if (!$payGatewayLogAdyenReqSent->getLogByReference($getArgs['merchantReference'])) {
            //
            $sRes = ADYEN_HPP_RESULT_INTERNAL_ERR; // ERROR INTERNO
            //
            $this->redirConfirmedHpp($paymentControlCheck, $sRes, null);
            return;
        }

        $bValidateSig = true;
        if ($getArgs['authResult'] != PaySupplierAdyen::ADYEN_HPP_RESULT_CODE_AUTHORISED) {
            $bValidateSig = false;
        }

        if (!$payGatewayLogAdyenReqSent->validateLogByResponse($getArgs, $bValidateSig)) {

//! @TODO  - ERROR DESCOMUNAL !!!

            //
            $sRes = ADYEN_HPP_RESULT_NOT_VALID_RESPONSE; // NOT VALID RESPONSE
            //
            $this->redirConfirmedHpp($paymentControlCheck, $sRes, null);
            return;
        }

        //
        $paymentControlCheck = $this->commOnlPay->getAdyenPaymentBeforeGateway(PAY_SUPPLIER_ADYEN_HPP, $this->user,
          $payGatewayLogAdyenReqSent);

        /*
        * It inserts all user form data into paymentControlCheck to avoid in case of error to lose the user input
        */
        if (!$paymentControlCheck) {

//! @TODO  - ERROR DESCOMUNAL !!!

            //
            $sRes = ADYEN_HPP_RESULT_INTERNAL_ERR; // ERROR INTERNO
            //
            $this->redirConfirmedHpp($paymentControlCheck, $sRes, null);
            return;
        }

        $pspReference = "";
        $authResult = "";

        if (isset($getArgs['pspReference'])) {
            $pspReference = $getArgs['pspReference'];
        }
        if (isset($getArgs['authResult'])) {
            $authResult = $getArgs['authResult'];
        }

        //
        switch ($authResult) {

            //
            // Unique case where payment is created
            //
            case PaySupplierAdyen::ADYEN_HPP_RESULT_CODE_AUTHORISED:

                $sRes = ADYEN_HPP_RESULT_OK;
                //
                $this->redirConfirmedHpp($paymentControlCheck, $sRes, $pspReference);
                return;
                break;

            case PaySupplierAdyen::ADYEN_HPP_RESULT_CODE_PENDING:     // Final status of the payment attempt could not be established immediately.

                //
                $sRes = ADYEN_HPP_RESULT_PENDING;
                //
                $this->redirConfirmedHpp($paymentControlCheck, $sRes, $pspReference);
                return;
                break;

            case PaySupplierAdyen::ADYEN_HPP_RESULT_CODE_REFUSED:     // Payment was refused / payment authorisation was unsuccessful.
            case PaySupplierAdyen::ADYEN_HPP_RESULT_CODE_ERROR:       // An error occurred during the payment processing.

                //
                $sRes = ADYEN_HPP_RESULT_REFUSED;
                //
                $this->redirConfirmedHpp($paymentControlCheck, $sRes, $pspReference);
                return;
                break;

            case PaySupplierAdyen::ADYEN_HPP_RESULT_CODE_CANCELLED:   // Payment attempt was cancelled by the shopper or the shopper requested to return to the merchant by pressing the back button on the initial page.

                $modFormPayment = new PaymentForm();
                $modFormPayment = $this->fillPaymentForm($modFormPayment, $user);

                $this->emptyCardDetailsForCard($modFormPayment);

                $modFormPayment->packageSelected = $paymentControlCheck->idProduct . DELIMITER_KEYS . $paymentControlCheck->idCountry . DELIMITER_KEYS . $paymentControlCheck->idPeriodPay;

                $this->redirPaymentMethod($modFormPayment, false, $this->user, false);
                return;

//
//                //
//                $sRes = ADYEN_HPP_RESULT_CANCELLED;
//                //
//                $this->redirConfirmedHpp($paymentControlCheck, $sRes, $pspReference);
//                return;
                break;

            default:
                break;
        }

        //
        $this->redirConfirmedHpp($paymentControlCheck, $sRes, $pspReference);

        return;
    }

    /**
     * @param PaymentControlCheck $paymentControlCheck
     * @param $sRes
     * @param $pspReference
     */
    protected function redirConfirmedHpp(PaymentControlCheck $paymentControlCheck, $sRes, $pspReference)
    {
        $this->redirect(Yii::app()->createUrl("/payments/confirmedPaymentHppAdyen",
          array('idPayCtrlCk' => $paymentControlCheck->id, 'pspReference' => $pspReference, 'sRes' => $sRes)));
    }

    /**
     * @param $idPayCtrlCk
     * @param $iRes
     */
    public function actionConfirmedPaymentHppAdyen($idPayCtrlCk = "", $sRes = null, $pspReference = null)
    {
        $this->layout = "mainResponsive";

        $paymentControlCheck = new PaymentControlCheck();
        $paymentControlCheck->getPaymentById($idPayCtrlCk);

        unset(Yii::app()->user->paymentControlCheck);
        unset(Yii::app()->session['paymentControlCheck']);

        Yii::app()->user->setState("paymentMethodAttempt", null);
        Yii::app()->user->setState("paymentControlCheckId", null);
        Yii::app()->user->setState("paymentControlStatus", null);

        $msgs = array(
          $sRes => array("errCode" => $sRes)
        );

        $this->render("confirmedPaymentHppAdyen", array(
          'paymentControlCheck' => $paymentControlCheck,
          'msgs' => $msgs,
          'idPayCtrlCk' => $idPayCtrlCk,
          'pspReference' => $pspReference,
          'attInterval' => ADYEN_HPP_ATTEMPTS_INTERVAL,
        ));
        return;
    }

    /**
     *
     */
    public function actionGetHppSuccess()
    {

        //Light security measure to avoid DOS attacks.
        HeMixed::securePublicUrlRequest("payments/gethppsuccess");
        $this->layout = "";

        $user = new AbaUser();
        $user->getUserById(Yii::app()->user->id);
        $this->user = $user;

        $sRedirectUrl = "";
        $bSuccess = false;

        // collect user input data
        $postValuesHpp = HeMixed::getPostArgs();

        //
        $paymentControlCheck = new PaymentControlCheck();
        if (isset($postValuesHpp['idPayCtrlCk'])) {
            $paymentControlCheck->getPaymentById($postValuesHpp['idPayCtrlCk']);
        }

        $payment = $this->processAdyenHppTransaction($paymentControlCheck, $postValuesHpp);

        if ($payment && $payment instanceof Payment) {

            //#5635
            $stPaymentInfo = $payment->getPaymentInfoForSelligent();

            //  Send data to Selligent
            $commConnSelligent = new SelligentConnectorCommon();
            $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::pagos, $this->user,
              array(
                "PROMOCODE" => $payment->idPromoCode,
                "ISEXTEND" => $payment->isExtend,
                "MONTH_PERIOD" => $stPaymentInfo['MONTH_PERIOD'],
                "PAY_DATE_START" => $stPaymentInfo['PAY_DATE_START'],
                "PAY_DATE_END" => $stPaymentInfo['PAY_DATE_END'],
                "AMOUNT_PRICE" => $stPaymentInfo['AMOUNT_PRICE'],
                "CURRENCY" => $stPaymentInfo['CURRENCY'],
                "PRICE_CURR" => $stPaymentInfo['PRICE_CURR'],
                "TRANSID" => $stPaymentInfo['TRANSID'],
              ),
              "actionGetHppSuccess controller");

            //#5087
            $commProducts = new ProductsPricesCommon();
            if($commProducts->isPlanProduct($payment->idProduct)) {
                $commConnSelligent = new SelligentConnectorCommon();
                // IDUSER_WEB, PRODUCT_TYPE
                $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::pagosPlan,
                  $this->user, array(), "actionGetHppSuccess controller");
            }
            else {
                $commConnSelligent = new SelligentConnectorCommon();
                $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::pagosPlanNotification,
                  $this->user, array(), "actionGetHppSuccess controller");
            }

            $bSuccess = true;
            $sRedirectUrl = Yii::app()->createUrl("/payments/confirmedPayment", array('idPay' => $payment->id));
        }

        echo json_encode(array("success" => $bSuccess, "redirectUrl" => $sRedirectUrl));

        return;
    }

    public function actionWsGetUserData() {
        $autol = HeMixed::getGetArgs(MAGIC_LOGIN_KEY);
        if ($autol) {
            if (Yii::app()->user->isLogged() && Yii::app()->user->getKeyExternalLogin() !== $autol) {
                Yii::app()->user->logout();
            }

            $form = new LoginForm();
            $form->autoLogin($autol);
        }

        if (Yii::app()->user->isLogged()) {
            $user = new AbaUser();
            $user->getUserById(Yii::app()->user->id);

            if ($user) {
                $userType = $user->userType;

                switch ($userType) {
                    case FREE:
                        $userType = 'free';
                        break;
                    case PREMIUM:
                        $userType = 'premium';
                        break;
                    default:
                        $userType = null;
                        break;
                }

                if ($userType) {

                    $currenciesBeforePrice = array('CAD', 'USD', 'BRL');

                    $country = AbaCountry::model('AbaCountry')->findByPk($user->countryId);
                    $currencyIsoCode = $country->ABAIdCurrency;
                    $currency = AbaCurrency::model('AbaCurrency')->getCurrencyById($currencyIsoCode);

                    $currencyData = array(
                        'iso' => $currencyIsoCode,
                        'symbol' => $currency->getSymbol(),
                        'decimalSeparator' => $currency->decimalPoint,
                        'symbolShowBeforePrice' => in_array($currencyIsoCode, $currenciesBeforePrice) ? 1 : 0
                    );

                    $response = array(
                        'id' => $user->id,
                        'type' => $userType,
                        'language' => $user->langEnv,
                        'name' => $user->name.' '.$user->surnames,
                        'email' => $user->email,
                        'country' => array(
                            'id' => (int)$country->id,
                            'iso' => $country->iso
                        ),
                        'currency' => $currencyData,
                        'autoLoginKey' => $user->keyExternalLogin
                    );

                    $this->sendResponse(200, $response);
                }
            }
        }

        $this->sendResponse(401, 'Unauthorized');
    }

    protected function sendResponse($status = 200, $body = '', $contentType = 'application/json')
    {
        // set the header fields (Status and Content-type)
        $statusHeader = 'HTTP/1.1 ' . $status;
        header($statusHeader);
        header('Content-Type: ' . $contentType);
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
        }

        if (is_array($body)) {
            $response = CJSON::encode($body);
        } else {
            $response = CJSON::encode(array('httpCode'=>$status, 'message'=>$body));
        }

        echo $response;

        Yii::app()->end();
    }
}
