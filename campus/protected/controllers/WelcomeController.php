<?php
/**
 * Popups and Display boxes called from views. Most of them called from AJAX.
 */
class WelcomeController extends AbaController
{
    public $data = array();

    /** Generic for Yii.
     * @return array
     */
    public function actions()
    {
        return array(
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }


    /** Show user objectives popup form in the Home page
     * @return bool
     * @throws CDbException
     */
    public function actionUserData()
    {
        //$name = Yii::app()->user->getName();
        //$model = new Paso1Form();
        $form = Yii::app()->request->getPost("Paso1Form");
        if( is_null($form) )
        {
            return false;
            //$this->renderPartial('userData', array('model' => $model, 'name' => $name));
        }
        else
        {
            $user = new AbaUser();
            if( !$user->getUserById( Yii::app()->user->getId()) )
            {
                throw new CDbException(" User could not be loaded in questionnaire of user objectives, user Id ".Yii::app()->user->getId() );
            }

            $aux = array();
            foreach($form as $key => $inputs)
            {
                $aux[$key] = $inputs;
            }

            $commWelcome = new WelcomeCommon();
            $data = $commWelcome->showWelcome();

            $model = $data['welcomeData']['model'];

            $model->setidUser(Yii::app()->user->getId());
            $model->setviajar($aux["viajar"]);
            $model->settrabajar($aux["trabajar"]);
            $model->setestudiar($aux["estudiar"]);
            $model->setnivel($aux["nivel"]);
            $model->setselEngPrev($aux["selEngPrev"]);
            $model->setgenero($aux["genero"]);
            $model->setdedicacion($aux["dedicacion"]);
            $model->setengPrev($aux["engPrev"]);
            $model->setday($aux["day"]);
            $model->setmonth($aux["month"]);
            $model->setyear($aux["year"]);
            $model->setbirthDate($model->getday()."-".$model->getmonth()."-".$model->getyear());
            $model->setotros($aux["otros"]);
            if ($model->validate()) {
                $moUserObjectives = new AbaUserObjective();
                if (!$moUserObjectives->getObjectivesByUserId($user->id)) {
                    if ($moUserObjectives->saveFormObjectives($model)===true) {
                        Yii::app()->user->setState("showObjectives", "1");
                        $user->birthDate = HeDate::europeanToSQL($model->getbirthDate(), false, "-");
                        $user->gender = $model->getgenero() ;
                        if (!$user->updateSaveUserObjectiveFields()) {
                            HeLogger::sendLog(
                                "Fields birthDate and Gender could not be saved during questionnaire.",
                                HeLogger::IT_BUGS,
                                HeLogger::CRITICAL,
                                "For some reason user ".$user->email .
                                " could not be saved some fields during questionnaire."
                            );
                            echo " Some error has ocurred while inserting your questionnaire." .
                                " Please close this dialog and try again.<br/> ";
                            $this->doRender($data['submitUrl'], $data['submitData'], $data['submitRedirect']);
                            return true;
                        }
                        $user->updateSavedUserObjective(1);
                        $commSelligent = new SelligentConnectorCommon();
                        $commSelligent->sendOperationToSelligent(
                            SelligentConnectorCommon::userObjectives,
                            $user,
                            array(),
                            "Bienvenido Paso 1"
                        );
                        $this->doRender($data['submitUrl'], $data['submitData'], $data['submitRedirect']);
                    } else {
                        echo " Some error has ocurred while inserting your questionnaire. Please try again or close this dialog.<br/> ";
                        $this->doRender($data['submitUrl'], $data['submitData'], $data['submitRedirect']);
                    }
                } else {
                    if ($moUserObjectives->updateFormObjectives($model)) {
                        Yii::app()->user->setState("showObjectives", "1");
                        $user->birthDate = HeDate::europeanToSQL($model->getbirthDate(), false, "-");
                        $user->gender = $model->getgenero() ;
                        if (!$user->updateSaveUserObjectiveFields()) {
                            HeLogger::sendLog(
                                "Fields birthDate and Gender could not be saved during questionnaire.",
                                HeLogger::IT_BUGS,
                                HeLogger::CRITICAL,
                                "For some reason user " . $user->email .
                                " could not be saved some fields during questionnaire."
                            );
                            echo " Some error has ocurred while inserting your questionnaire. Please close this dialog and try again.<br/> ";
                            $this->doRender($data['submitUrl'], $data['submitData'], $data['submitRedirect']);
                            return true;
                        }
                        $user->updateSavedUserObjective(1);
                        $commSelligent = new SelligentConnectorCommon();
                        $commSelligent->sendOperationToSelligent(SelligentConnectorCommon::userObjectives,$user,array(),"Bienvenido Paso 1");
                        $this->doRender($data['submitUrl'], $data['submitData'], $data['submitRedirect']);
                    } else {
                        echo " Some error has ocurred while updating your questionnaire. Please try again or close this dialog.<br/> ";
                        $this->doRender($data['submitUrl'], $data['submitData'], $data['submitRedirect']);
                    }
                }
            } else {
                Yii::app()->user->setState("showObjectives", "0");
                $user->updateSavedUserObjective(0);
                $this->renderPartial($data['welcomeUrl'], $data['welcomeData']);
            }
        }
    }

    /**
     * 2nd step for the User Objectives popup in the Home page.
     * It requests to select an user level.
     */
    public function actionLevelSelect()
    {
        $user = new AbaUser();
        $user->getUserById(Yii::app()->user->getId());

        $commWelcome = new WelcomeCommon();
        $data = $commWelcome->showWelcome();

        $level = Yii::app()->request->getPost('nivelActual', null);
        if (!is_null($level) && (HeMixed::isValidLevel($level))) {
            $oldLevel = $user->currentLevel;
            if ($user->updateUserCourseLevel($level)) {
                $commUser = new UserCommon();
                $commUser->changeUserLevel($user, $oldLevel, $level, 'User objetives');
                $commSelligent = new SelligentConnectorCommon();
                $commSelligent->sendOperationToSelligent(SelligentConnectorCommon::userObjectives, $user, array(), "Bienvenido Paso 2");
                Yii::app()->user->setState('welcomeLevelSelected', true);
                if($data['submitUrl'] == 'getStartUnitByLevel')
                {
                    $units = new UnitsCourses();
                    $level = Yii::app()->user->currentLevel;
                    $moUnit = new UnitCourse($units->getStartUnitByLevel($level));
                    $data['submitUrl'] = Yii::app()->createAbsoluteUrl('course/AllUnits/unit/' . $moUnit->getFormatUnitId($moUnit->getUnitId()));
                }
                $this->doRender($data['submitUrl'], $data['submitData'], $data['submitRedirect']);
            } else {
                echo " Some error has ocurred while updating your profile. Please try again or close this dialog. ";
                $this->renderPartial($data['welcomeUrl'], $data['welcomeData']);
            }
        } else {

            ob_start();
            Yii::app()->runController('leveltest/index');
            $jsonResponse = ob_get_contents();
            ob_end_clean();
            echo $jsonResponse;
            return true;
        }
    }

    public function actionPricePlans()
    {
        $this->renderPartial('pricePlans');
    }

    public function actionMemberGetMember($fromCourse = 0)
    {
        $this->renderPartial('memberGetMember', array('fromCourse' => $fromCourse));
    }

    private function doRender($url = NULL, $data = NULL, $redirect = FALSE)
    {
        if($redirect === FALSE)
        {
            $this->renderPartial($url, $data);
        }
        else
        {
            // return json to process the redirect with javascript
            echo json_encode(array('url' => $url));
        }
    }

    protected function showRenderResult($view,$idLevel, $url =null) {

        $testResultString = Yii::t('mainApp','Test result:');
        $analysisString = Yii::t('mainApp','Analysis');
        $objectivesString = Yii::t('mainApp','Objectives');
        $abaEnglishString = Yii::t('mainApp','ABA English');

        switch ($idLevel) {
            case 1:
                $levelTitle = Yii::t('mainApp','Beginners');
                $analysis = Yii::t('mainApp','In order to get the most out of the course start from the basics.');
                $abaEnglish = Yii::t('mainApp','The ABA English course consists of 6 levels which will take your English from the Beginners (A1) * level to Business (C1)*. In each level you will find 24 units. Our course will be available 24 hours a day throughout the duration of your subscription.');
                break;
            case 2:
                $levelTitle = Yii::t('mainApp','Lower intermediate');
                $analysis = Yii::t('mainApp','You can understand simple phrases and expressions.');
                $abaEnglish = Yii::t('mainApp','The ABA English course consists of 6 levels which will take your English from the Beginners (A1) * level to Business (C1)*. In each level you will find 24 units. Our course is available 24 hours a day throughout the duration of your subscription.');
                break;
            case 3:
                $levelTitle = Yii::t('mainApp','Intermediate');
                $analysis = Yii::t('mainApp','You have proved that you already have some experience in English.');
                $abaEnglish = Yii::t('mainApp','The ABA English course consists of 6 levels which will take your English from the Beginners (A1) * level to Business (C1)*. In each level you will find 24 units. Our course is available 24 hours a day throughout the duration of your subscription.');
                break;
            case 4:
                $levelTitle = Yii::t('mainApp','Upper intermediate');
                $analysis = Yii::t('mainApp','You have proved to have a good level of English.');
                $abaEnglish = Yii::t('mainApp','The ABA English course consists of 6 levels which will take your English from the Beginners (A1) * level to Business (C1)*. In each level you will find 24 units. Our course is available 24 hours a day throughout the duration of your subscription.');
                break;
            case 5:
                $levelTitle = Yii::t('mainApp','Advanced');
                $analysis = Yii::t('mainApp','You have proved to have a high level of English.');
                $abaEnglish = Yii::t('mainApp','The ABA English course consists of 6 levels which will take your English from the Beginners (A1) * level to Business (C1)*. In each level you will find 24 units. Our course is available 24 hours a day throughout the duration of your subscription.');
                break;
            case 6:
                $levelTitle = Yii::t('mainApp','Business');
                $analysis = Yii::t('mainApp','You have proved to have a high level of English.');
                $abaEnglish = Yii::t('mainApp','The ABA English course consists of 6 levels which will take your English from the Beginners (A1) * level to Business (C1)*. In each level you will find 24 units. Our course is available 24 hours a day throughout the duration of your subscription.');
                break;
        }

        $objectives = Yii::t('mainApp','textfinal', array('{levelTitle}' => $levelTitle));

        $html= $this->renderPartial($view, array(
          'testResultString' => $testResultString,
          'levelTitle' => $levelTitle,
          'analysisString' => $analysisString,
          'analysis' => $analysis,
          'objectivesString' => $objectivesString,
          'objectives' => $objectives,
          'abaEnglishString' => $abaEnglishString,
          'abaEnglish' => $abaEnglish,
          'idLevel' => $idLevel,
          'url' => $url),true);
        return $html;

    }
    public function actionFinishTestLevel()
    {
        $user = new AbaUser();
        $user->getUserById(Yii::app()->user->getId());
        // $idLevel = Yii::app()->request->getParam('level', $idLevel);

        $idLevel = Yii::app()->request->getPost("level",1);
        $code = Yii::app()->request->getPost("code",1);
        $user->updateUserCourseLevel($idLevel);
        $oldLevel = $user->currentLevel;

        $commUser = new UserCommon();
        $commUser->changeUserLevel($user, $oldLevel, $idLevel, 'User objetives');
        $commSelligent = new SelligentConnectorCommon();
        $commSelligent->sendOperationToSelligent(SelligentConnectorCommon::userObjectives, $user, array(), "Test de nivel Campus");
        $commWelcome = new WelcomeCommon();

        $data = $commWelcome->showWelcome();

        if($data['submitUrl'] == 'getStartUnitByLevel')
        {
            $units = new UnitsCourses();
            $level = Yii::app()->user->currentLevel;
            $moUnit = new UnitCourse($units->getStartUnitByLevel($level));
            $data['submitUrl'] = Yii::app()->createAbsoluteUrl('course/AllUnits/unit/' . $moUnit->getFormatUnitId($moUnit->getUnitId()));
        }

        $levelTestUser = new LevelTestUser();
        $levelTestUser->idUser = Yii::app()->user->id;
        $levelTestUser->code = $code;
        $levelTestUser->created = date("Y-m-d H:i:s");
        $levelTestUser->deleted = date("Y-m-d H:i:s");
        $levelTestUser->isdeleted = 0;
        $levelTestUser->typeleveltest = '0';
        $levelTestUser->save();

        $html = $this->showRenderResult('//leveltest/endTestResult',$idLevel,$data['submitUrl']);

        echo json_encode(array('html'=> $html));

    }

    public function actionThisIsDemo() {
        $idLevel = Yii::app()->request->getPost("level",1);
        $code = Yii::app()->request->getPost("code",1);

        $levelTestUser = new LevelTestUser();
        $levelTestUser->idUser = Yii::app()->user->id;
        $levelTestUser->code = $code;
        $levelTestUser->created = date("Y-m-d");
        $levelTestUser->deleted = date("Y-m-d H:i:s");
        $levelTestUser->isdeleted = 0;
        $levelTestUser->typeleveltest = '1';
        $levelTestUser->save();

        $html = $this->showRenderResult('//leveltest/endTestDemo',$idLevel);

        echo json_encode(array('html'=> $html));
    }
}
