<?php

/** @noinspection PhpUndefinedClassInspection */
class JwtController extends CController
{
    public function actionLogin() {

        if (!Yii::app()->user->isGuest) {
            $userId = Yii::app()->user->id;
            $fullName = Yii::app()->user->Fullname;
            $email = Yii::app()->user->Email;

            $mUser = new AbaUser();
            $mUser->getUserById($userId);
            $strLanguage = $mUser->langEnv;
        } else {
            $userId = 0;
            $fullName = '';
            $email = '';
            $strLanguage = Yii::app()->params['ZENDESK_DEFAULTLANGUAGE'];
        }

        if (!isset($_GET['locale_id'])) {
            $locale_id = $this->getLocaleId($strLanguage);
        } else {
            $locale_id = $_GET['locale_id'];
        }

        $key = Yii::app()->params['ZENDESK_PRIVATEKEY'];
        $domain = Yii::app()->params['ZENDESK_DOMAIN'];
        $now       = time();
        $token = array(
          'jti'   => md5($now . rand()),
          'iat'   => $now,
          'name'  => $fullName,
          'email' => $email,
          'external_id' => $userId,
          'locale' => $locale_id,
          'locale_id' => $locale_id,
          'user_fields' => array(
            'link_a_intranet' => Yii::app()->params['INTRANET_URL'].'/index.php?r=abaUser/admin_id&id='.$userId,
          )
        );
        $jwt = Firebase\JWT\JWT::encode($token, $key);
        $location = "https://" . $domain . "/access/jwt?jwt=" . $jwt;
        if(isset($_GET["return_to"])) {
            $location .= "&return_to=" . urlencode($_GET["return_to"]);
        }

        header("Location: " . $location);
        Yii::app()->end();
    }

    public function actionLogout() {

        $strLanguage = '';
        $userId = Yii::app()->request->getParam('external_id');
        if ( !empty($userId) ) {
            $mUser = new AbaUser();
            $mUser->getUserById($userId);
            $strLanguage = $mUser->langEnv;
        }
        $strLanguage = ( isset(Yii::app()->params['ZENDESK_LANGUAGETABLE'][$strLanguage]) ) ?
          Yii::app()->params['ZENDESK_LANGUAGETABLE'][$strLanguage] : $strLanguage;

        $domain = Yii::app()->params['ZENDESK_DOMAIN'];
        $location = "https://" . $domain . "/hc/".$strLanguage;
        if(isset($_GET["return_to"])) {
            if ( str_replace('?', '', $location) == $location ) $separador = '?';
            else $separador = '&';
            $location .= $separador."return_to=" . urlencode($_GET["return_to"]);
        }

        header("Location: " . $location);

        Yii::app()->end();
    }

    public function getLocaleId( $strLocale )
    {
        $strLocale = ( isset(Yii::app()->params['ZENDESK_LANGUAGETABLE']) && isset(Yii::app()->params['ZENDESK_LANGUAGETABLE'][$strLocale]) ) ?
          Yii::app()->params['ZENDESK_LANGUAGETABLE'][$strLocale] : $strLocale;
        $idLocale = 1;

        try {
            $jsonLocales = file_get_contents(Yii::app()->params['ZENDESK_URLLOCALES']);
            $arLocales = json_decode($jsonLocales);

            foreach ($arLocales->locales as $mLocale) {
                if ($mLocale && $mLocale->locale == $strLocale) {
                    $idLocale = $mLocale->id;
                    break;
                }
            }
        } catch (Exception $e) {

        }

        return $idLocale;
    }
}

?>