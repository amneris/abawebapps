<?php

class AbamomentsController extends AbaController
{
    public $data = array();

    public function actionIndex()
    {
        $abaMomentsUrl = Yii::app()->params['ABA_MOMENTS_URL'];

        $user = new AbaUser();
        $user->getUserById(Yii::app()->user->id);

        if ($user) {
            $language = $user->langEnv;
            $email = urlencode($user->email);
            $autoLoginKey = urlencode($user->keyExternalLogin);

            $abaMomentsUrl .= "/$language/dashboard";
            $abaMomentsUrl .= "?email=$email&autol=$autoLoginKey";
        }


        $this->render('index', array(
            'abaMomentsUrl' => $abaMomentsUrl
        ));
    }
}
