<?php
/**
 * !! IMPORTANT, DO NOT MODIFY THE PHP DOC COMMENTS ON THE HEADER OF EVERY FUNCTION.
 * IT IS USED BY Yii IN ORDER TO CREATE WSDL.FORBIDDEN!
 */
class WsoapabaselligentController extends WsController
{
    static private $_version = "1.8";
    static private $_aHistoryVersion = array(
        "1.0" => "Creation of cancellations service",
        "1.1" => "Creation of passwordChange service",
        "1.2" => "Creation of synchroUser service",
        "1.3" => "Creation of unsubscribe service",
        "1.4" => "Creation of refund service",
        "1.5" => "Creation of changeCreditInPayment",
        "1.6" => "Creation of createCreditForm",
        "1.7" => "Creation of cancelPayment",
        "1.8" => "Creation of partnerSource",
    );

    public function actions()
    {
        return array(
            'senddata' => array(
                'class' => 'AbaWebServiceAction',
            ),
        );
    }

    /** Returns the version of this API. element one is version and second is description
     *
     * @return array The array has format [version,description]
     * @soap
     */
    public function version()
    {
        return $this->retSuccessWs(array("version" => self::$_version,
            "description" => self::$_aHistoryVersion[self::$_version]));
    }


    /* !! IMPORTANT, DO NOT MODIFY THE PHP DOC COMMENTS ON THE HEADER OF EVERY FUNCTION. *************
       * IT IS USED BY Yii IN ORDER TO CREATE WSDL. FORBIDDEN! ***************************************
    */

    /** Sends data to selligent and returns true if success.
     *
     * @param string $signature
     * @param integer $userId
     * @param integer $userType
     * @param string $expirationDate
     *
     * @return array|bool
     * @soap
     */
    public function cancellations($signature, $userId, $userType, $expirationDate)
    {
        try {
            if (!$this->isValidSignature($signature, array($userId, $userType, $expirationDate))) {
                return $this->retErrorWs("502");
            }

            $user = new AbaUser();
            if (!$user->getUserById($userId)) {
                return $this->retErrorWs("504", " operation Cancellation needs user to exist.");
            }

            $commSelligent = new SelligentConnectorCommon();
            $res = $commSelligent->sendOperationToSelligent(SelligentConnectorCommon::cancelaciones, $user,
                array('IDUSER_WEB' => $user->getId(), 'USER_TYPE' => $userType, 'EXPIRATION_DATE' => $expirationDate),
                "Wsoapabaselligent-Cancellations");

            if (!$res) {
                return $this->retErrorWs("505", $commSelligent->getErrMessage());
            } else {
                return $this->retSuccessWs(array("result" => "success",
                    "details" => $commSelligent->getResponse()));
            }
        } catch (Exception $ex) {
            error_log($signature . " error=> " . $ex->getMessage());
            return $this->retErrorWs("555",
                "Internal error non identified. Check with Aba IT Team and see logs Apache for " . $signature);
        }
    }

    /* !! IMPORTANT, DO NOT MODIFY THE PHP DOC COMMENTS ON THE HEADER OF EVERY FUNCTION. *******************************
    * IT IS USED BY Yii IN ORDER TO CREATE WSDL. FORBIDDEN! ************************************************************
    */

    /**
     * Returns an array with result="success" and details="description", otherwise
     * it returns an array with error code and description
     *
     * @param string $signature
     * @param integer $userId
     * @param string $password
     *
     * @return array
     * @soap
     */
    public function passwordChange($signature, $userId, $password)
    {
        try {
            if (!$this->isValidSignature($signature, array($userId, $password))) {
                return $this->retErrorWs("502");
            }

            if( empty($userId) ){
                return $this->retErrorWs('504', ' operation passwordChange needs user to exist.');
            }


            $user = new AbaUser();
            if (!$user->getUserById($userId)) {
                return $this->retErrorWs("504", " operation passwordChange needs user to exist.");
            }

            $commUser = new UserCommon();
            // We leave the password blank in purpose:
            $res = $commUser->changePassword($user, $password, "Wsoapabaselligent-passwordChange", false);
            if (!$res) {
                return $this->retErrorWs("505", $commUser->getErrMessage());
            } else {
                return $this->retSuccessWs(array("result" => "success",
                    "details" => "OK"));
            }
        } catch (Exception $ex) {
            error_log($signature . " error=> " . $ex->getMessage());
            return $this->retErrorWs("555",
                "Internal error non identified. Check with Aba IT Team and see logs Apache for " . $signature);
        }
    }


    /** Used from external apps to update info for user only in Selligent.
     *
     * @param string $signature
     * @param integer $userId
     *
     * @return array
     * @soap
     */
    public function synchroUser($signature, $userId)
    {
        if (!$this->isValidSignature($signature, array($userId))) {
            return $this->retErrorWs("502");
        }

        $user = new AbaUser();
        if (!$user->getUserById($userId)) {
            return $this->retErrorWs("504", " operation Syncro needs user to exist.");
        }

        $commSelligent = new SelligentConnectorCommon();
        $res = $commSelligent->sendOperationToSelligent(SelligentConnectorCommon::synchroUser, $user,
            array(), "Wsoapabaselligent-synchroUser");
        if (!$res) {
            return $this->retErrorWs("505", $commSelligent->getErrMessage());
        } else {
            return $this->retSuccessWs(array("result" => "success",
                "details" => $commSelligent->getResponse()));
        }
    }

    /**  Used from external apps to delete info for user only in Selligent.
     * @param string $signature
     * @param integer $userId
     *
     * @return array
     * @soap
     */
    public function unsubscribe($signature, $userId)
    {
        if (!$this->isValidSignature($signature, array($userId))) {
            return $this->retErrorWs("502");
        }

        $user = new AbaUser();
        if (!$user->getUserById($userId)) {
            return $this->retErrorWs("504", " operation unsubscribe needs user to exist.");
        }

        $commSelligent = new SelligentConnectorCommon();
        $res = $commSelligent->sendOperationToSelligent(SelligentConnectorCommon::unsubscribe, $user,
            array(), "Wsoapabaselligent-unsubscribe");
        if (!$res) {
            return $this->retErrorWs("505", $commSelligent->getErrMessage());
        } else {
            return $this->retSuccessWs(array("result" => "success",
                "details" => $commSelligent->getResponse()));
        }
    }

    /** Executes some validations and send Refund to the payment gateway. It DOES NOT
     * save the refund into our database/models.
     *
     * @param string $signature
     * @param string $paymentId
     * @param float $amountPrice
     * @param string $paySuppOrderId
     *
     * @return array 3 elements
     * @soap
     */
    public function refundPayment($signature, $paymentId, $amountPrice, $paySuppOrderId)
    {
        try {
            if (!$this->isValidSignature($signature, array($paymentId, $amountPrice, $paySuppOrderId))) {
                return $this->retErrorWs("502");
            }
            /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
            /*++++++++These validations might be in the Intranet or encapsulated somewhere else++++*/
            //1. Identify payment
            $moPaySource = new Payment();
            if (!$moPaySource->getPaymentById($paymentId)) {
                return $this->retErrorWs("515");
            } elseif ($moPaySource->status !== PAY_SUCCESS) {
                return $this->retErrorWs("515", "Pay should be SUCCESS");
            } elseif (floatval($moPaySource->amountPrice) < floatval($amountPrice)) {
                return $this->retErrorWs("515", "Amount is over the original one.");
            }
            //2. Check user if exists?
            $moUser = new AbaUser();
            if (!$moUser->getUserById($moPaySource->userId)) {
                return $this->retErrorWs("504", " operation Refund needs an user to exist: " . $moPaySource->userId);
            }
            //3. Last transaction date, if > 1 month returns error.
            $dateToday = HeDate::todaySQL(true);
            $moLastPayForUser = new Payment();
            $moLastPayForUser->getLastSuccessPayment($moPaySource->userId);
            if (abs(HeDate::getDifferenceDateSQL($moLastPayForUser->dateEndTransaction,
                    $dateToday,
                    HeDate::DAY_SECS)) >= 90
            ) {
                return $this->retErrorWs("517", " Operation Refund is no longer supported for this user. " .
                    " Too long since last payment");
            }
            /* -------------------------------------------------------------------------------------*/
            //4. Create draft refund payment
            $moPayDraftRefund = clone($moPaySource);
            $moPayDraftRefund->dateToPay = HeDate::todaySQL(true);
            $moPayDraftRefund->id = HeAbaSHA::generateIdPayment($moUser, $moPayDraftRefund->dateToPay);
            if ($paySuppOrderId == '') {
                $paySuppOrderId = $moPayDraftRefund->id;
            }
            $moPayDraftRefund->paySuppOrderId = $paySuppOrderId;
            $moPayDraftRefund->amountPrice = $amountPrice;
            $moPayDraftRefund->status = PAY_REFUND;
            $moPayDraftRefund->idPayControlCheck = $moPaySource->id;

            //5. Execute Refund
            $paySupplier = PaymentFactory::createPayment($moPayDraftRefund);
            try {
                $retPaymentSupplier = $paySupplier->runRefund($moPayDraftRefund, $moPaySource);
            } catch (Exception $e) {
                $retPaymentSupplier = false;
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_UNKNOWN_H . ': Exception in Refund payment, in the web service from Campus.',
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    'Probably Gateway s fault. Check everything related with this payment.'
                );
            }
            if (!$retPaymentSupplier) {
                return $this->retErrorWs("516", $paySupplier->getErrorString());
            }

            // @TODO: 6. Count refunds from today, in case >10 or >1.000 send e-mail

            // 7. Success
            return $this->retSuccessWs(array("paySuppExtUnid" => $retPaymentSupplier->getUniqueId(),
                                            "paySuppOrderId" => $retPaymentSupplier->getOrderNumber(),
                                            "idPayment" => $moPayDraftRefund->id));

        } catch (Exception $ex) {
            error_log($signature . " error=> " . $ex->getMessage());
            return $this->retErrorWs(
                "555",
                "Internal error non identified. Check with Aba IT Team and see logs Apache for " . $signature
            );
        }
    }

    /** It searches for the new Id User Credit Form, and it replaces
     *
     * @param string $signature
     * @param string $paymentId
     * @param integer $newIdUserCreditForm
     *
     * @return array
     * @soap
     */
    public function changeCreditInPayment($signature, $paymentId, $newIdUserCreditForm)
    {
        try {
            if (!$this->isValidSignature($signature, array($paymentId, $newIdUserCreditForm))) {
                return $this->retErrorWs("502");
            }

            $comRecurring = new RecurrentPayCommon();
            if (!$comRecurring->changeCreditInPayment($paymentId, $newIdUserCreditForm)) {
                return $this->retErrorWs("518", " , Details are ".$comRecurring->getErrorMessage());
            }

            return $this->retSuccessWs(array("result" => true));

        } catch (Exception $ex) {
            error_log($signature . " error=> " . $ex->getMessage());
            return $this->retErrorWs("555",
                "Internal error non identified. Check with Aba IT Team and see logs Apache for " . $signature);
        }
    }

    /**
     * @param string $signature
     * @param integer $userId
     * @param integer $creditCardType
     * @param string $creditCardNumber
     * @param string $creditCardYear
     * @param string $creditCardMonth
     * @param string $creditCvc
     * @param string $creditCardName
     * @param string $cpfBrasil
     * @param string $typeCpf
     * @param string $paySuppExtId
     *
     * @return array
     *
     * @soap
     */
    public function createCreditForm( $signature, $userId, $creditCardType, $creditCardNumber, $creditCardYear,
                                                $creditCardMonth, $creditCvc, $creditCardName, $cpfBrasil, $typeCpf, $paySuppExtId)
    {
        try {
            if (!$this->isValidSignature($signature, array( $userId, $creditCardType, $creditCardNumber,
                                                                $creditCardYear, $creditCardMonth,
                                                                $creditCvc, $creditCardName, $cpfBrasil, $typeCpf, $paySuppExtId))) {
                return $this->retErrorWs("502");
            }

            $moUser = new AbaUser();
            if (!$moUser->getUserById($userId)){
                return $this->retErrorWs("504", "UserId not found in database.");
            }

            $aDataPayMethod = array();
            $aDataPayMethod["creditCardType"] =     $creditCardType;
            $aDataPayMethod["creditCardNumber"] =   $creditCardNumber;
            $aDataPayMethod["creditCardYear"] =     $creditCardYear;
            $aDataPayMethod["creditCardMonth"] =    $creditCardMonth;
            $aDataPayMethod["CVC"] =                $creditCvc;
            $aDataPayMethod["creditCardName"] =     $creditCardName;
            $aDataPayMethod["cpfBrasil"] =          $cpfBrasil;
            $aDataPayMethod["typeCpf"] =            $typeCpf;
            $comOnlPay = new OnlinePayCommon();
            $errMsg = "";

            $moUserCredit = $comOnlPay->saveCreditFormDataFromPayment($aDataPayMethod, $errMsg, $moUser);

            if ( !$moUserCredit ) {
                return $this->retErrorWs("518", $comOnlPay->getErrorMessage().":".$errMsg);
            }

            $moUser->abaUserLogUserActivity->saveUserLogActivity("user_credit_forms",
                                                                    "New credit card inserted default ".
                                                                    $moUserCredit->id,
                                                                    "createCreditForm Service");
            return $this->retSuccessWs(array("result" => true, "userCreditFormId"=>$moUserCredit->id));

        } catch (Exception $ex) {
            error_log($signature . " error=> " . $ex->getMessage());
            return $this->retErrorWs("555",
                "Internal error non identified. Check with Aba IT Team and see logs Apache for " . $signature);
        }
    }

    /** Executes some validations and send Refund to the payment gateway. It DOES NOT
     * save the refund into our database/models.
     *
     * @param string $signature
     * @param string $paymentId
     * @param string $paySuppOrderId
     *
     * @return array
     * @soap
     */
    public function cancelPayment($signature, $paymentId, $paySuppOrderId)
    {
        try {
            if (!$this->isValidSignature($signature, array($paymentId, $paySuppOrderId))) {
                return $this->retErrorWs("502");
            }
            /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
            /*++++++++These validations might be in the Intranet or encapsulated somewhere else++++*/

            //
            // 1. Identify payment
            $moPaySource = new Payment();
            if (!$moPaySource->getPaymentById($paymentId)) {
                return $this->retErrorWs("515");
            } elseif ($moPaySource->status !== PAY_PENDING) {
                return $this->retErrorWs("515", "Pay should be PENDING");
            }

            //
            // 2. Check user if exists?
            $moUser = new AbaUser();
            if (!$moUser->getUserById($moPaySource->userId)) {
                return $this->retErrorWs("504", " operation Refund needs an user to exist: " . $moPaySource->userId);
            }

            $moUserCredit = new AbaUserCreditForms();
            if (!$moUserCredit->getUserCreditFormById($moPaySource->idUserCreditForm)) {
                $this->processError(" Credit Form for user " . $moUser->id . " not found, id credit form equals to " . $moPaySource->idUserCreditForm);
                return $this->retErrorWs("515", "Credit Form for user " . $moUser->id . " not found, id credit form equals to " . $moPaySource->idUserCreditForm);
            }

            //
            // 3. Create cancel payment
            $moPayDraftCancel = new PaymentControlCheck();
            $moPayDraftCancel->getPaymentById($moPaySource->idPayControlCheck);

            //
            // 4. Execute Cancel
            $paySupplier = PaymentFactory::createPayment($moPayDraftCancel);

            try {
                $retPaymentSupplier = $paySupplier->runCancelRecurring($moPaySource, $moUserCredit);
            } catch (Exception $e) {
                $retPaymentSupplier = false;
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_UNKNOWN_H . ': Exception in Cancel payment, in the web service from Campus.',
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    'Probably Gateway s fault. Check everything related with this payment.'
                );
            }
            if (!$retPaymentSupplier) {
                return $this->retErrorWs("516", $paySupplier->getErrorString());
            }

            // 5. Success
            return $this->retSuccessWs(array(
                "paySuppExtUnid" => $moPaySource->paySuppExtUniId,
                "paySuppOrderId" => $retPaymentSupplier->getRecurringDetailReference(),
                "idPayment" =>      $moPayDraftCancel->id
            ));

        } catch (Exception $ex) {
            error_log($signature . " error=> " . $ex->getMessage());
            return $this->retErrorWs("555", "Internal error non identified. Check with Aba IT Team and see logs Apache for " . $signature);
        }
    }


    /** Executes some validations and send Payment (Direct) to the payment gateway. It DOES
     * SAVE the payment into our database/models.
     *
     * @param string $signature
     * @param string $paymentId
     * @param float $amountPrice
     * @param string $dateToPay
     *
     * @return array 3 elements
     * @soap
     */
    public function directPayment($signature, $paymentId, $amountPrice, $dateToPay)
    {

        try {
            if (!$this->isValidSignature($signature, array($paymentId, $amountPrice, $dateToPay))) {
                return $this->retErrorWs("502");
            }

            /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
            /*++++++++These validations might be in the Intranet or encapsulated somewhere else++++*/

            $moPaySource = new Payment();
            if (!$moPaySource->getPaymentById($paymentId)) {
                return $this->retErrorWs("515");
            } elseif ($moPaySource->status !== PAY_PENDING AND $moPaySource->status !== PAY_FAIL AND $moPaySource->status !== PAY_CANCEL) {
                return $this->retErrorWs("515", "Pay should be SUCCESS");
            } elseif (floatval($moPaySource->amountPrice) < floatval($amountPrice)) {
                return $this->retErrorWs("515", "Amount is over the original one.");
            }

            if ($moPaySource->paySuppExtId <> PAY_SUPPLIER_CAIXA) {
                return $this->retErrorWs("515", "Payment supplier must by only laCaixa.");
            }

            $moUser = new AbaUser();
            if (!$moUser->getUserById($moPaySource->userId)) {
                return $this->retErrorWs("504", " operation Direct Payment needs an user to exist: " . $moPaySource->userId);
            }

            $moUserCredit = new AbaUserCreditForms();
            if (!$moUserCredit->getUserCreditFormByUserId($moPaySource->userId)) {
                return $this->retErrorWs("515", "Credit card Details not found for the user " . $moUser->email);
            }

            $moProductUser = new ProductPrice();
            if (!$moProductUser->getProductById($moPaySource->idProduct)) {
                return $this->retErrorWs("515", "Product not Found for the user " . $moUser->email);
            }

            //
            // POST DATA
            //
            if (trim($dateToPay) != '') {
                $moPaySource->dateToPay = $dateToPay;
            }
            $moPaySource->amountOriginal = $amountPrice;
            $moPaySource->amountDiscount = 0;
            $moPaySource->amountPrice = $amountPrice;
            $moPaySource->idPartner = PARTNER_ID_DIRECT_PAYMENT_LACAIXA;

            //
            $moPaySource->id = HeAbaSHA::generateIdPayment($moUser, $moPaySource->dateToPay);

            //
            $moPaymentControlCheck = new PaymentControlCheck();

            if (trim($moPaySource->paySuppExtProfId) == '') {
                if (!$moPaymentControlCheck->createPayControlCheckFromPayment($moUser->id, 1, $moPaySource->idProduct,
                  $moPaySource->idCountry, $moPaySource->idPeriodPay,
                  $moPaySource->idPromoCode, $moUserCredit->kind, $moUserCredit->cardName, $moUserCredit->cardNumber,
                  $moUserCredit->cardYear, $moUserCredit->cardMonth,
                  $moUserCredit->cardCvc, $moPaySource->paySuppExtId, $moPaySource)
                ) {
                    return $this->retErrorWs("515", "Payment Control check could not be built " . $moUser->email);
                }
            } else {
                if (!$moPaymentControlCheck->createPayControlCheckFromPayment($moUser->id, 1, $moPaySource->idProduct,
                  $moPaySource->idCountry, $moPaySource->idPeriodPay,
                  $moPaySource->idPromoCode, $moUserCredit->kind, $moUserCredit->cardName, $moUserCredit->cardNumber,
                  $moUserCredit->cardYear, $moUserCredit->cardMonth,
                  $moUserCredit->cardCvc, $moPaySource->paySuppExtId, $moPaySource,
                  '', '', false, $moPaySource->paySuppExtProfId)
                ) {
                    return $this->retErrorWs("515", "Payment Control check could not be built " . $moUser->email);
                }
            }

            /* @var PaySupplierLaCaixa $paySupplier */
            $paySupplier = PaymentFactory::createPayment($moPaySource);
            /* @var PayGatewayLogLaCaixaResRec $retPaymentSupplier */
            $retPaymentSupplier = $paySupplier->runDebiting($moPaymentControlCheck, "A", true);
            if (!$retPaymentSupplier) {
                $errPaymentGateway = Yii::t('mainApp', 'gateway_recurrent_failure_key') .
                  " ( Response from PaymentSupplier = " . $paySupplier->getErrorCode() . "-" . $paySupplier->getErrorString() . " )";

                $moPaySource->paySuppOrderId = $paySupplier->getOrderNumber();

                return $this->retErrorWs("515",
                  "Direct Payment request through La Caixa for user " . $moUser->email . " payment= " . $moPaySource->id . " Details=" . $errPaymentGateway);
            }

            //
            $onlinePayCommon = new OnlinePayCommon();
            $payment = $onlinePayCommon->savePaymentCtrlCheckToUserDirectPayment($moPaymentControlCheck,
              $retPaymentSupplier, $moUserCredit, PARTNER_ID_DIRECT_PAYMENT_LACAIXA);

            if (!$payment) {
                return $this->retErrorWs("515",
                  "El pago se ha realizado con exito pero no se ha reflejado en la base de datos. User email=" . $moUser->email . " payment= " . $moPaySource->id);
            } else {
                // 7. Success
                return $this->retSuccessWs(array(
                  "paySuppExtUnid" => $retPaymentSupplier->getUniqueId(),
                  "paySuppOrderId" => $retPaymentSupplier->getOrderNumber(),
                  "idPayment" => $payment->id
                ));
            }

        } catch (Exception $ex) {
            error_log($signature . " error=> " . $ex->getMessage());
            return $this->retErrorWs("555",
              "Internal error non identified. Check with Aba IT Team and see logs Apache for " . $signature);
        }
    }

    /**
     * @param string $signature
     * @param array $dataSelligent
     *
     * @return array
     * @soap
     */
    public function partnerSource($signature, $dataSelligent)
    {
        try {
            $commSelligent = new SelligentConnectorCommon();

            if (!$this->isValidSignature($signature, $dataSelligent)) {
                return $this->retErrorWs("502");
            }

            $user = new AbaUser();
            if (!$user->getUserById(SELLIGENT_PARTNER_DEFAULT_USERID)) {
                return $this->retErrorWs("504", " operation Syncro needs user to exist.");
            }

            $res = $commSelligent->sendOperationToSelligent(SelligentConnectorCommon::partnerSource, $user,
                array(
                    'ID_PARTNER'=>      $dataSelligent['idPartner'],
                    'PARTNER_NAME'=>    $dataSelligent['partnerName'],
                    'ID_PARTNER_GROUP'=>$dataSelligent['idPartnerGroup'],
                    'NAME_GROUP'=>      $dataSelligent['nameGroup'],
                    'ID_CATEGORY'=>     $dataSelligent['idCategory'],
                    'ID_BUSINESS_AREA'=>$dataSelligent['idBusinessArea'],
                    'ID_TYPE'=>         $dataSelligent['idType'],
                    'ID_GROUP'=>        $dataSelligent['idGroup'],
                    'ID_CHANNEL'=>      $dataSelligent['idChannel'],
                    'ID_COUNTRY'=>      $dataSelligent['idCountry']), "Wsoapabaselligent-createPartner");
            if (!$res) {
                return $this->retErrorWs("505", $commSelligent->getErrMessage());
            } else {
                return $this->retSuccessWs(array("result" => "success",
                    "details" => $commSelligent->getResponse()));
            }

        } catch (Exception $ex) {
            error_log($signature . " error=> " . $ex->getMessage());
            return $this->retErrorWs("555", "Internal error non identified. Check with Aba IT Team and see logs Apache for " . $signature);
        }
    }

    /**--------------------------------------------------------------------------------------------------------------**/
    /**--------------------------------------------------------------------------------------------------------------**/
}