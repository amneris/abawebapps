<?php

class ProfileController extends AbaController
{

    public function actions()
    {
        return array(
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     * @param null $fDisplayInvoices
     */
    public function actionIndex($fDisplayInvoices = null)
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $modProfile = new ProfileForm();
        $user = new AbaUser();

        $userId = Yii::app()->user->getId();
        if ($userId) {
            if ($user->getUserById($userId)) {
                $modProfile->id = $user->id;
                $modProfile->name = $user->name;
                $modProfile->surnames = $user->surnames;
                $modProfile->email = $user->email;
                $modProfile->password = $user->password;
                $modProfile->birthDate = $user->birthDate;
                $modProfile->gender = $user->gender;
                $modProfile->countryId = $user->countryIdCustom;
                $modProfile->city = $user->city;
                $modProfile->telephone = $user->telephone;
                $modProfile->currentLevel = $user->currentLevel;
                $modProfile->canChangeLanguage = $this->canChangeLanguage($user);
                $modProfile->langEnv = $user->langEnv;

                if (isset($fDisplayInvoices)) {
                    $modProfile->fDisplayInvoices = intval($fDisplayInvoices);
                }

                $moUserAddress = new AbaUserAddressInvoice();
                if ($moUserAddress->getUserAddressByUserId($userId)) {
                    $modProfile->fDisplayInvoices = ( $modProfile->fDisplayInvoices === 1 ) ? $modProfile->fDisplayInvoices : true;
                    $comInvoice = new InvoicesCommon();
                    $modProfile = $comInvoice->setFromInvoiceToForm($moUserAddress, $modProfile);
                }
            } else {
                $this->redirect(array('site'));
            }
        }

        $this->redirIndex($modProfile, $user, "");

    }

    /**
     * Collects data from the ProfileForm model
     * and updates through SQL the data in MYSQL
     */
    public function actionUpdate()
    {
        //#ABAWEBAPPS-701
        $stCooladataParams = array(
            "bSendCooladataChangedPersonalDetailsEvent" => false,
            "bSendCooladataChangedLevelEvent" => false
        );

        $modProfileForm = new ProfileForm();
        $user = new AbaUser();
        $fChangeLevel = false;
        if (!$user->getUserById(Yii::app()->user->getId())) {
            $this->redirIndex($modProfileForm, $user, "No se han guardado los campos. Ha ocurrido algún error con su usuario.");
        }

        $postFormFields = HeMixed::getPostArgs('ProfileForm');
        if (intval(Yii::app()->user->getId()) !== intval($postFormFields["id"])) {
            $modProfileForm->addError("id", "The id is not correct.");
            $this->redirIndex($modProfileForm, $user, "The id is not correct.");
            return;
        }

        $oldLevel = $user->currentLevel;
        $oldLanguage = $user->langEnv;
        if (isset($postFormFields)) {
            $modProfileForm->attributes = $postFormFields;
            $modProfileForm->canChangeLanguage = $this->canChangeLanguage($user);
            $user->name = $postFormFields["name"];
            $user->surnames = $postFormFields["surnames"];
            $user->birthDate = $postFormFields["birthDate"];
            $user->gender = $postFormFields["gender"];
            $user->countryIdCustom = $postFormFields["countryId"];
            $user->city = $postFormFields["city"];
            $user->telephone = $postFormFields["telephone"];

            if (!isset($postFormFields["langEnv"]) || $postFormFields["langEnv"] == '') {
                $postFormFields["langEnv"] = $oldLanguage;
            }

            $user->langEnv = $postFormFields["langEnv"];
            $user->langCourse = $postFormFields["langEnv"];

            if ($user->currentLevel !== $postFormFields["currentLevel"]) {
                $fChangeLevel = true;
            }
            $user->currentLevel = $postFormFields["currentLevel"];
            if ($modProfileForm->validate()) {
                if (!$user->validate()) {
                    $this->redirIndex($modProfileForm, $user, "No se han guardado los campos. No ha superado la validación de datos.");
                    return;
                }
                if (!$user->updateUser()) {
                    $this->redirIndex($modProfileForm, $user, 'Ha ocurrido algún error al guardar los datos');
                    return;
                }

                //#ABAWEBAPPS-701
                $stCooladataParams["bSendCooladataChangedPersonalDetailsEvent"] = true;

                if ($oldLanguage !== $postFormFields["langEnv"]) {
                    Yii::app()->setLanguage($postFormFields["langEnv"]);
                    Yii::app()->user->refresh($user);
                }

                $commConnSelligent = new SelligentConnectorCommon();
                $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::perfil, $user, array(), "actionUpdate");
                if (intval($modProfileForm->fDisplayInvoices)) {
                    $comInvoice = new InvoicesCommon();
                    if (!$comInvoice->saveInvoice(Yii::app()->user->getId(), $modProfileForm)) {
                        $this->redirIndex($modProfileForm, $user, "No se han guardado los campos. Por favor revise los errores");
                        return;
                    }
                }
            } else {
                $msgErrores = "";
                $errores = $modProfileForm->getErrors();
                foreach ($errores as $field => $message) {
                    $msgErrores .= " El campo " . $field . " no es correcto " . $message[0];
                }
                $this->redirIndex($modProfileForm, $user, "No se han guardado los campos. Por favor revise los errores");
                return;
            }
        }

        if ($fChangeLevel) {
            //#ABAWEBAPPS-701
            $stCooladataParams["bSendCooladataChangedLevelEvent"] = true;

            // We log the first level in logLevelChange and sets the session level:
            $commUser = new UserCommon();
            $commUser->changeUserLevel($user, $oldLevel, $user->currentLevel, 'My account');
        }
        $this->redirIndex($modProfileForm, $user, 'Se ha guardado correctamente', $stCooladataParams);
    }

    /**
     * @param ProfileForm $modProfileForm
     * @param AbaUser     $user
     * @param             $msgConfirmation
     *
     */
    protected function redirIndex( ProfileForm $modProfileForm, AbaUser $user, $msgConfirmation="", $stCooladataParams=array() )
    {

        $userTypes = Yii::app()->params["userType"];
        $modProfileForm->userType = $userTypes[$user->userType];

        $objCountries = new AbaCountries();
        $listCountries = $objCountries->getListAllCountries();

        $userTypePartnerStatus = $user->getUserTypePartnerStatus();

        //#5232
        $oPayment =             new Payment();
        $bLastPendingPayment =  $oPayment->getLastPayPendingByUserId($user->id);

        $stRenderData = array(
          'model' => $modProfileForm,
          "currentLevel" => $user->currentLevel,
          "entryDate" => $user->entryDate,
          "expirationDate" => $user->expirationDate,
          "listOfCountries" => $listCountries,
          "userTypePartnerStatus" => $userTypePartnerStatus,
          "msgConfirmation" => $msgConfirmation,
          "bLastPendingPayment" => $bLastPendingPayment,
          //#ABAWEBAPPS-701
          "cooladataUserData" => $user
        );

        //#ABAWEBAPPS-701
        if (isset($stCooladataParams["bSendCooladataChangedPersonalDetailsEvent"])) {
            $stRenderData["bSendCooladataChangedPersonalDetailsEvent"] = $stCooladataParams["bSendCooladataChangedPersonalDetailsEvent"];
        }
        if (isset($stCooladataParams["bSendCooladataChangedLevelEvent"])) {
            $stRenderData["bSendCooladataChangedLevelEvent"] = $stCooladataParams["bSendCooladataChangedLevelEvent"];
        }

        $this->render('index', $stRenderData);
    }

    /**
     *
     */
    public function actionProgress()
    {
        $user = new AbaUser();
        $userId = Yii::app()->user->getId();
        if($user->getUserById($userId))
        {
            $moUnitsCourses = new UnitsCourses();
            $currentLevel = Yii::app()->user->currentLevel;
            if(is_null(HeMixed::getPostArgs('levelProgress')) && is_null(HeMixed::getPostArgs('unit'))) {
                /* ENH-3986, show user's last unit with some progress increase */
                $followup = new AbaFollowup();
                if($currentUnit = $followup->getLastUnitProgressIncreased($userId)) {
                    $currentLevel = $moUnitsCourses->getLevelByUnitId($currentUnit);
                }
            }
            else {
                if( !is_null(HeMixed::getPostArgs('levelProgress')) && (HeMixed::isValidLevel(HeMixed::getPostArgs('levelProgress'))) ) {
                    $currentLevel = HeMixed::getPostArgs('levelProgress');
                }
                $currentUnit = ( !is_null(HeMixed::getPostArgs('unit')) )? HeMixed::getPostArgs('unit') : $moUnitsCourses->getStartUnitByLevel($currentLevel);
            }


            // tiny fix for Bug #5921
            if($currentUnit === false OR !array_key_exists ($currentUnit,$moUnitsCourses->getUnitsNamesByLevel($currentLevel))) {
                $currentUnit = $moUnitsCourses->getStartUnitByLevel($currentLevel);
            }

            $followup = new AbaFollowup();
            if( !$followup->getFollowupByIds($userId, $currentUnit) )
            {
                // No hay seguimiento para esta unidad. Debemos hacer algo??
                // No, los valores están inicializados a 0.
            }

            $iAllErrorsOnUnit = $followup->all_err;
            $iAllAnswersOnUnit = $followup->all_ans;

            /* @var UnitCourse[] $aUnitsObject */
            $aUnitsObject = $moUnitsCourses->getAllUnitsByLevel($currentLevel);
            $aUnits = array();
            /*  @var UnitCourse $unitCourse */
            /*  @var integer $unitId */
            foreach( $aUnitsObject as  $unitId=>$unitCourse )
            {
                $aUnits[$unitId] = $unitId.". ".$unitCourse->getMainNameNoTrans();

                if ($unitId == $currentUnit) {
                    $urlUnit = $unitCourse->getThumbUrlLink();
                }

            }

            $this->render('progress', array("currentLevel" => $currentLevel,
                                            "entryDate" => $user->entryDate,
                                            "expirationDate" => $user->expirationDate,
                                            "currentUnit" => $currentUnit,
                                            "followup" => $followup,
                                            "nombresUnidades" => $aUnits,
                                            "iAllErrorsOnUnit" => $iAllErrorsOnUnit,
                                            "iAllAnswersOnUnit" => $iAllAnswersOnUnit,
                                            "urlUnit" => $urlUnit,)
                                            );
        }
    }

    /**
     *
     */
    public function actionCertificates()
    {
        $moUser = new AbaUser();
        $userId = Yii::app()->user->getId();
        $levels = Yii::app()->params['levels'];
        $urlsLinkedins = Yii::app()->params['urlLinkedins'];
        $certificados = null;
        $certificadosDisponibles = 0;
        // Obtain all percentages for every Level:
        foreach($levels as $levelId=>$strLevel) {
            $moAllProgress = new AbaFollowups();
            $percentageByLevel = $moAllProgress->getCurrentLevelProgress($userId, $levelId);
            $certificados['level'][$levelId-1] = $strLevel;
            $certificados['porcentage'][$levelId-1] = $percentageByLevel;
            $certificados['urlLinkedin'][$levelId-1] = $urlsLinkedins[$levelId];
            if ($certificados['porcentage'][$levelId-1] >= 100) {
                $certificadosDisponibles++;
            }
        }
        if ($moUser->getUserById($userId)) {
            $this->render('certificates', array("currentLevel" => $moUser->currentLevel,
                "entryDate" => $moUser->entryDate,
                "expirationDate" => $moUser->expirationDate,
                "certificados" => $certificados,
                "certificadosDisponibles" => $certificadosDisponibles,
                "levelsId" => array_keys($levels)));
        }
    }

    /** Displays the tab of invoices in the profile section of the user
     *
     * @param string $msgValidation
     */
    public function actionInvoices( $msgValidation="")
    {
        $aDataInvoices = array();
        $allDownloaded = true;
        $moUserAddressInvoice = new AbaUserAddressInvoice();
        if (!$moUserAddressInvoice->getUserAddressByUserId(Yii::app()->user->getId(), true)) {
            $msgValidation = "key_fillInvoiceFormClickHere{link}";
            $allDownloaded = false;
        } else {
            $oAbaUserAddressInvoiceForm = new AbaUserAddressInvoiceForm();

            if(!$oAbaUserAddressInvoiceForm->setUserAddressInvoiceForm($moUserAddressInvoice)) {
                $msgValidation = "key_fillInvoiceFormClickHere{link}";
                $allDownloaded = false;
            }
            else{

                $invoicesToPdf = new ColInvoices();
                $aDataInvoices = $invoicesToPdf->getAllInvoicesByUserId(Yii::app()->user->getId());

                if(is_array($aDataInvoices)) {
                    foreach ($aDataInvoices as &$dataInvoice) {
                        $aFields = array($dataInvoice['idPayment']);
                        $dataInvoice['signature'] = HeMixed::getSignatureUrlParams($aFields);
                        $moCCy = new AbaCurrency($dataInvoice["currencyTrans"]);
                        $dataInvoice['ccySymbol'] = $moCCy->getSymbol();
                        if( trim($dataInvoice["dateLastDownload"])=='' ||
                            trim($dataInvoice["dateLastDownload"])=='0000-00-00 00:00:00'){
                            $allDownloaded = false;
                        }
                    }
                }
                else {
                    $aDataInvoices = array();
                }
            }
        }
        $this->redirInvoices($aDataInvoices, $msgValidation, $allDownloaded);
        return;
    }

    /**
     * @param string $signature Encrypted parameter by Get
     * @param string $idPayment
     *
     * @return bool
     */
    public function actionInvoicePdf( $signature, $idPayment)
    {
        if (!HeMixed::isValidSignature($signature, array($idPayment))) {
            $this->actionInvoices("The invoice requested is not correct. Please contact our support department.");
            return false;
        }

        $moUserAddressInvoice = new AbaUserAddressInvoice();
        if (!$moUserAddressInvoice->getUserAddressByUserId(Yii::app()->user->getId(), true)) {
            $this->actionInvoices();
            return false;
        }
        else {
            $oAbaUserAddressInvoiceForm = new AbaUserAddressInvoiceForm();

            if(!$oAbaUserAddressInvoiceForm->setUserAddressInvoiceForm($moUserAddressInvoice)) {
                $this->actionInvoices();
                return false;
            }
        }

        $moPayment = new Payment();
        if (!$moPayment->getPaymentById($idPayment)) {
            $this->actionInvoices("The invoice from payment requested is not correct. Please contact our support department.");
            return false;
        }
        if (!$moPayment->validateInvoicePartner()) {
            $this->actionInvoices("An error has ocurred while generating the invoice. Try again or contact our support department.");
            return false;
        }

        $moInvoice = new AbaUserInvoices();

        if (!$moInvoice->setFromPaymentAddress($moPayment, $moUserAddressInvoice, 1)) {
//            $this->actionInvoices("An error has ocurred while generating the invoice. Try again or contact our support department.");
            $this->actionInvoices("");
            return false;
        }

        //
        //
        $moUser = new AbaUser();
        $moUser->getUserById($moPayment->userId);
        $moInvoice->productDesc = $moPayment->getProductPeriodDesc($moUser->langEnv, $moPayment->userId);
        //
        //

        //
        $moExistingInvoice =    new AbaUserInvoices();
        $iInvoiceId =           null;
        if($moExistingInvoice->getByPaymentId($moInvoice->idPayment) && $moExistingInvoice->usedByUser == 0) {
            $iInvoiceId = $moExistingInvoice->id;
        }
        $moInvoice->usedByUser = 1;

        // Generate Invoice, if already exists then we only update dateLastDownload
        $moInvoice->insertUpdateInvoice($iInvoiceId);

        $moInvoice->refresh();
        $moCCy = new AbaCurrency($moInvoice->currencyTrans);

        $this->render('invoicePdf', array("moInvoice" => $moInvoice, "ccySymbol"=> $moCCy->getSymbol() ));
        return true;
    }

    /**
     * Member get member
     */
    public function actionInvite()
    {
        // member get member
        $mgm = Yii::app()->user->getMemberGetMember();
        if($mgm !== false) {
            $daysDiff = 0;
            if($mgm['expireDate'] !== NULL) {
                $daysDiff = HeDate::getDifferenceDateSQL($mgm['expireDate'], date('Y-m-d'), 86400, false);
            }
            if($mgm['expireDate'] !== NULL && $daysDiff <= 0) {
                // the invite period expired, redirect to home
                $this->redirect(Yii::app()->createAbsoluteUrl('site/index'));
            }
            else {
                $absoluteUrl = Yii::app()->createAbsoluteUrl('profile/invite');
                $this->render('invite', array('absoluteUrl' => $absoluteUrl, 'mgm' => $mgm, '_GET' => HeMixed::getGetArgs(), '_POST' => HeMixed::getPostArgs()));
            }
        }
        else {
            $this->redirect(Yii::app()->createAbsoluteUrl('profile/index'));
        }
    }

    /**
     *
     */
    public function actionForever()
    {
        $user = new AbaUser();
        $userId = Yii::app()->user->getId();
        if($user->getUserById($userId))
        {
            $this->render('forever', array("currentLevel" => $user->currentLevel,
                "entryDate" => $user->entryDate,
                "expirationDate" => $user->expirationDate));
        }
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError(){

        if($error=Yii::app()->errorHandler->error){

            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }

    }
    
    public function actionChangepassword()
    {
        $passwordForm = new PasswordForm();
        $msgConfirmation = "";



        $this->render('changepassword',array('modelPassword'=> $passwordForm,
                                        "msgConfirmation"=> $msgConfirmation)
                    );
    }

    public function actionUpdatepwd()
    {
        $modelPassword = new PasswordForm();
        $msgConfirmation = "";
        $user = new AbaUser();
        if( !$user->getUserById(Yii::app()->user->getId()) )
        {
            $this->render('changepassword',array('modelPassword'=> $modelPassword,
                                             "msgConfirmation"=> "Su id de usuario tiene algún problema. Por favor vuelva a autentificarse." )
            );
            return;
        }

        $postFormFields = HeMixed::getPostArgs('PasswordForm');
        $modelPassword->attributes = $postFormFields;
        if( !$modelPassword->validate() )
        {
            $this->render('changepassword',array('modelPassword'=> $modelPassword,
                    "msgConfirmation"=> "validationnotsucc_k" )
            );
            return;
        }

        if( !$user->getUserByEmail($user->email, $modelPassword->oldPassword, true) )
        {
            $this->render('changepassword',array('modelPassword'=> $modelPassword,
                    "msgConfirmation"=> "La actual contraseña no es correcta." )
            );
            return;
        }

        if( $modelPassword->newPassword!==$modelPassword->confirmPassword ){
            $this->render('changepassword',array('modelPassword'=> $modelPassword,
                    "msgConfirmation"=> "La nueva contraseña no es correcta." )
            );
            return;
        }

        $commUser = new UserCommon();
        if( !$commUser->changePassword($user, $modelPassword->confirmPassword, "actionUpdatepwd controller", true, false) )
        {
            $this->render('changepassword',array('modelPassword'=> $modelPassword,
                        "msgConfirmation"=> "La nueva contraseña no ha podido ser guardada." )
            );
            return;
        }

        $modProfile = new ProfileForm();
        $modProfile->id = $user->id;
        $modProfile->name = $user->name;
        $modProfile->surnames = $user->surnames;
        $modProfile->email = $user->email;
        $modProfile->password = $user->password;
        $modProfile->birthDate = $user->birthDate;
        $modProfile->countryId = $user->countryId;
        $modProfile->city = $user->city;
        $modProfile->telephone = $user->telephone;
        $modProfile->currentLevel = $user->currentLevel;

        $this->redirIndex( $modProfile, $user, 'Se ha cambiado la contraseña correctamente');
    }


    /**
     * @param array $aDataInvoices
     * @param string $msgValidation
     * @param bool $allDownloaded
     *
     * @return bool
     */
    protected function redirInvoices($aDataInvoices, $msgValidation, $allDownloaded)
    {
        $this->render('invoices', array("aDataInvoices" => $aDataInvoices,
                                        "msgValidation"=>$msgValidation,
                                        "allDownloaded"=>$allDownloaded) );
        return true;
    }

    /**
     * @param $user
     * @return bool
     */
    protected function canChangeLanguage($user)
    {
        $abaFollowups = new AbaFollowups();
        $progress = $abaFollowups->getCurrentLevelProgress($user->id, Yii::app()->user->currentLevel);

        // user free (but no ex-premium) with progress < 10%
        return $user->userType == FREE && empty($user->cancelReason) && $progress < 10;
    }

}