<?php
/** static Pages controller
 *
 */
/** @noinspection PhpUndefinedClassInspection */
class PagesController extends CController
{
    /**
     * Declares class-based actions.
     */

    public $data = array();

    /** Yii configuration
     * @return array
     */
    public function actions()
    {
        return array(
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function actionOops()
    {
        $this->layout = '';
        $this->render('oops');
        return;
    }

    public function actionDelCo()
    {
        $this->layout = '';
        $this->render('cookieMonster');
        return;
    }
}