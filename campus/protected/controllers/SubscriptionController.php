<?php

class SubscriptionController extends AbaController
{

    public function actions()
    {
        return array(
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array('class' => 'CViewAction',),
        );
    }

    /** GET HTTP expected
     * First page of the tab Subscription on the Profile section. We collect
     * all info from the subscription, or ex-subscription of the current user.
     *
     */
    public function actionIndex()
    {
        $moUser = new AbaUser();
        $moUser->getUserById(Yii::app()->user->getId());
        $moLastPaySubs = new Payment();
        $commRecurring = new RecurrentPayCommon();
        $typeSubsStatus = $commRecurring->getTypeSubscriptionStatus($moUser, $moLastPaySubs);
        $aDataView = $this->getCreditFormData($moUser, $typeSubsStatus, $moLastPaySubs);

        $this->redirIndex($aDataView);
    }


    /**
     * Present blank form to create a new credit card abd also process data.
     * POST HTTPS method expected.
     */
    public function actionChangeCard()
    {
        $moUser = new AbaUser();
        $moUser->getUserById(Yii::app()->user->getId());
        $moLastPaySubs = new Payment();
        $commRecurring = new RecurrentPayCommon();
        $typeSubsStatus = $commRecurring->getTypeSubscriptionStatus($moUser, $moLastPaySubs);

        $aDataView = $this->getCreditFormData($moUser, $typeSubsStatus, $moLastPaySubs);

        // If user subscription is not via credit card nor his payment is PENDING, we do not
        // allow any change at the moment.
        if ($typeSubsStatus != SUBS_PREMIUM_A_CARD_A &&
            $typeSubsStatus != SUBS_PREMIUM_A_CARD_C &&
            $typeSubsStatus != SUBS_PREMIUM_A_CARD_M &&
            $typeSubsStatus != SUBS_PREMIUM_A_CARD_AD
        ) {
            $this->redirIndex($aDataView, "key_notallowedchangecreditnotpendingpay");
            return;
        }

        $frmMoChangeCredit = new CreditCardForm();
        $frmMoChangeCredit->setIdPaySupplier($moUser);
        $postFields = HeMixed::getPostArgs("CreditCardForm");
        if (empty($postFields)) {
            // New blank form displayed to the user.
            $this->redirChangeCardForm($moUser, $frmMoChangeCredit, '', $moLastPaySubs);
            return;
        } else {
            // *********** WE ARE going to create the new card and replace the PENDING PAYMENT ***************
            $frmMoChangeCredit->attributes = $postFields;
            if (!$frmMoChangeCredit->validate()) {
                $this->redirChangeCardForm($moUser, $frmMoChangeCredit, 'Please fix the following input errors:', $moLastPaySubs);
                return;
            }

            $errorMsg = "";
            $aDataPayMethod = array();
            $aDataPayMethod["creditCardType"] =     $frmMoChangeCredit->creditCardType;
            $aDataPayMethod["creditCardNumber"] =   $frmMoChangeCredit->creditCardNumber;
            $aDataPayMethod["creditCardYear"] =     $frmMoChangeCredit->creditCardYear;
            $aDataPayMethod["creditCardMonth"] =    $frmMoChangeCredit->creditCardMonth;
            $aDataPayMethod["CVC"] =                $frmMoChangeCredit->CVC;
            $aDataPayMethod["creditCardName"] =     $frmMoChangeCredit->creditCardName;
            $aDataPayMethod["cpfBrasil"] =          $frmMoChangeCredit->cpfBrasil;
            $aDataPayMethod["typeCpf"] =            $frmMoChangeCredit->typeCpf;

            $comOnlPayment = new OnlinePayCommon();

            $aDataView["moUserCreditForm"] = $comOnlPayment->saveCreditFormDataFromPayment($aDataPayMethod, $errorMsg, $moUser);

            if (!$aDataView["moUserCreditForm"]) {
                $frmMoChangeCredit->addError("creditCardNumber", $errorMsg);
                $this->redirChangeCardForm($moUser, $frmMoChangeCredit, 'errorsavepayment_key', $moLastPaySubs);
                return;
            }
            if (!$commRecurring->changeCreditInPayment($moLastPaySubs->id, $aDataView["moUserCreditForm"]->id)) {
                $aDataView["moUserCreditForm"]->delete();
                $frmMoChangeCredit->addError("creditCardNumber", $commRecurring->getErrorMessage());
                $this->redirChangeCardForm($moUser, $frmMoChangeCredit, "key_reviewfields_created_butnoreplaced", $moLastPaySubs);
                return;
            }

        }

        // Everything worked as expected, success saved and changed the credit card details:
        $this->redirIndex($aDataView, "Se ha guardado correctamente");
    }

    /**
     * IT is called from /payments/actionProcessNewPlan when the change of Product has been successful
     * It will display the subscription tab as in Index but with a special confirmation message.
     *
     */
    public function actionChangePlanSuccessful()
    {
        $moUser = new AbaUser();
        $moUser->getUserById(Yii::app()->user->getId());
        $moLastPaySubs = new Payment();
        $commRecurring = new RecurrentPayCommon();
        $typeSubsStatus = $commRecurring->getTypeSubscriptionStatus($moUser, $moLastPaySubs);
        $aDataView = $this->getCreditFormData($moUser, $typeSubsStatus, $moLastPaySubs);

        $this->redirIndex($aDataView, 'key_changeproductsuccess');
    }

    /**
     * Renders View with all sensitive data plus any possible special message
     */
    protected function redirIndex(array $aData, $specialMessage = "")
    {
        $aData["specialMessage"] = $specialMessage;

        $this->render('index', $aData);
    }

    /** Renders View with the Form of credit Card
     *
     * @param AbaUser $moUser
     * @param CreditCardForm $frmMoChangeCredit
     * @param string $msgValidation
     * @param Payment $moLastPaySubs
     */
    protected function redirChangeCardForm(AbaUser $moUser, CreditCardForm $frmMoChangeCredit, $msgValidation='', Payment $moLastPaySubs=NULL) {

        $listOfCreditCards = PaymentFactory::listCardsAvailable($moUser);

        $urlsFormProcess = array(
            PAY_METHOD_PAYPAL => $this->createURL('/payments/processPaymentPayPal'),
            PAY_METHOD_CARD => $this->createURL('/payments/processPayment'),
            PAY_METHOD_BOLETO => $this->createURL('/payments/ProcessPaymentBoleto'),
            PAY_METHOD_ADYEN_HPP => ""
        );

        $this->render('changeCard', array(
            'moUser' => $moUser,
            'frmMoChangeCredit' => $frmMoChangeCredit,
            'listOfCreditCards' => $listOfCreditCards,
            'urlCardFormProcess' => $this->createURL('/subscription/changecard'),
            'urlPayPalProcess' => $this->createURL('/subscription/changecard'),
            'msgValidation' => $msgValidation,
            'dateToPay' => $moLastPaySubs->dateToPay,
            'currency' => "",
            'urlsFormProcess' => $urlsFormProcess,
            ));
    }

    /** Get all details from the method and credit card details, or PAYPAL that later will be used
     * in the view of Subscription.
     *
     * @param AbaUser $moUser
     * @param integer $typeSubsStatus
     * @param Payment $moLastPaySubs
     *
     * @return array
     */
    protected function getCreditFormData(AbaUser $moUser, $typeSubsStatus, $moLastPaySubs)
    {
        $moUserCreditForm = new AbaUserCreditForms();
        $dateToPay = NULL;
        $dateCancel = NULL;
        $amountPrice = NULL;
        $kindMethod = NULL;
        $productDesc = NULL;
        $periodDescKey = NULL;
        $expirationDate = NULL;

        if (is_null($moLastPaySubs)) {
            $moLastPaySubs = new Payment();
        }

        if ($typeSubsStatus != SUBS_FREE &&
            $moLastPaySubs->paySuppExtId != PAY_SUPPLIER_GROUPON &&
            $moLastPaySubs->paySuppExtId != PAY_SUPPLIER_B2B ) {
            if (!empty($moLastPaySubs->idUserCreditForm) &&
                !$moUserCreditForm->getUserCreditFormById($moLastPaySubs->idUserCreditForm)) {
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_LOGIC_H . ", Missing credit form id " . $moLastPaySubs->idUserCreditForm,
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    " For user trying to change credit card, missing card " .
                    " from user id " . $moLastPaySubs->userId . " id card = " . $moLastPaySubs->idUserCreditForm
                );
            }
            $kindMethod = $moUserCreditForm->kind;
            $productDesc = $moLastPaySubs->getProductPeriodDesc($moUser->langEnv);
            $moPeriodicity = new ProdPeriodicity();
            $moPeriodicity->getProdPeriodById($moLastPaySubs->idPeriodPay);
            $periodDescKey = $moPeriodicity->descriptionText;
            $expirationDate = $moUser->expirationDate;
        }

        $moBoleto = false;
        switch ($typeSubsStatus) {
            case SUBS_PREMIUM_C_BOLETO_BR:
            case SUBS_PREMIUM_A_CARD_A:
            case SUBS_PREMIUM_A_CARD_M:
            case SUBS_PREMIUM_A_CARD_C:
            case SUBS_PREMIUM_A_CARD_AD:
            case SUBS_PREMIUM_A_CARD_IOS:
            case SUBS_PREMIUM_A_PAYPAL:
            case SUBS_PREMIUM_C_CARD_A:
            case SUBS_PREMIUM_C_CARD_M:
            case SUBS_PREMIUM_C_CARD_C:
            case SUBS_PREMIUM_C_CARD_AD:
            case SUBS_PREMIUM_C_CARD_IOS:
            case SUBS_PREMIUM_A_ZUORA:
            case SUBS_PREMIUM_C_PAYPAL:
            case SUBS_PREMIUM_A_ANDROID:
            case SUBS_PREMIUM_C_ANDROID:
                $dateToPay = $moLastPaySubs->dateToPay;
                $dateCancel = null;
                if ($moLastPaySubs->status == PAY_CANCEL) {
                    $dateCancel = $moLastPaySubs->dateEndTransaction;
                }
                $amountPrice = HeViews::formatAmountInCurrency(
                    HeMixed::getRoundAmount($moLastPaySubs->getAmountPriceInForeignCcy(),$moLastPaySubs->foreignCurrencyTrans),
                    $moLastPaySubs->foreignCurrencyTrans);
                break;
            case SUBS_EXPREMIUM_CARD:
            case SUBS_EXPREMIUM_PAYPAL:
                $dateToPay = null;
                $dateCancel = $moLastPaySubs->dateEndTransaction;
                $amountPrice = null;
                break;
            case SUBS_PREMIUM_THROUGHPARTNER:
            case SUBS_EXPREMIUM_THROUGHPARTNER:
            case SUBS_PREMIUM_THROUGHB2B:
                $dateToPay = null;
                $dateCancel = $moLastPaySubs->dateEndTransaction;
                $amountPrice = null;
                $moUserCreditForm = null;
                $kindMethod = null;
                break;
            case SUBS_FREE:
                $dateToPay = null;
                $dateCancel = null;
                $amountPrice = null;
                $moUserCreditForm = null;
                $kindMethod = null;
                $productDesc = null;
                $periodDescKey = null;
                $expirationDate = null;
                break;
            default:

                break;
        }

        if ($moUser->userType == FREE) {
            $moBoleto = new AbaPaymentsBoleto();
            $moBoleto = $moBoleto->getLastPendingByUserId($moUser->id);
            if ($moBoleto) {
                $moPayDraft = new PaymentControlCheck();
                $moPayDraft = $moPayDraft->getPaymentById($moBoleto->idPayment);
                $moPeriodicity = new ProdPeriodicity();
                $moPeriodicity->getProdPeriodById($moPayDraft->idPeriodPay);
                $periodDescKey = $moPeriodicity->descriptionText;
                $amountPrice = HeViews::formatAmountInCurrency(HeMixed::getRoundAmount($moPayDraft->getAmountPriceInForeignCcy(),
                  $moPayDraft->foreignCurrencyTrans), $moPayDraft->foreignCurrencyTrans);
            }
        }

        return array(
          'typeSubscriptionStatus' => $typeSubsStatus,
          'dateToPay' => $dateToPay,
          'dateCancel' => $dateCancel,
          'amountPrice' => $amountPrice,
          'moUserCreditForm' => $moUserCreditForm,
          'kindMethod' => $kindMethod,
          'productDesc' => $productDesc,
          'periodDescKey' => $periodDescKey,
          'expirationDate' => $expirationDate,
          'moBoleto' => $moBoleto,
          'bDiscardCardData' => true,
        );
    }

}