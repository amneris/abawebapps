<?php
/**
 * UI controller for all payments related operations
 */
class PaymentsplusController extends AbaPayController
{
    /* @var AbaUser $user */
    protected $user;
    /* @var OnlinePayCommon $commOnlPay */
    protected $commOnlPay;
    protected $renderFooter = true;
    protected $headerTitle;

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
        $this->commOnlPay = new OnlinePayCommon();
    }

    /** First payment URL access, product selection
     * @param string $promoCode
     *
     */
    public function actionPayment($promoCode = "")
    {
        if (!$this->accessToPlusFlow()){
            $this->redirect(Yii::app()->createUrl("/site/index"));
            return;
        } else {
            $this->redirPayProductSelection('/paymentsplus/paymentmethod');
            return;
        }
    }

    /**
     * For a PREMIUM user the action URL to type in a new method of payment.
     * @param string $promoCode An existing promocode
     */
    public function actionPaymentMethod($promoCode='')
    {
        $this->layout = "mainResponsive";
        $success = false;

        if (!$this->accessToPlusFlow()){
            $this->redirect(Yii::app()->createUrl("/site/noaccess"));
            return;
        } else {
            $modFormPayment = new PaymentForm();
            $modFormPayment = $this->fillPaymentForm($modFormPayment, $this->user);
            $prodPricePKSelected = HeMixed::getPostArgs("productPricePKSelected");
            if (!is_null($prodPricePKSelected)) {
                $modFormPayment->packageSelected = $prodPricePKSelected;
            }

            $this->redirPaymentMethod($modFormPayment, $success, $this->user, false);
        }
    }

    /**
     * Called after user has enter all data in the payment form, and has selected PayPal method as a payment
     * Receives as POST all data necessary to carry out the whole process.
     *
     */
    public function actionProcessPaymentPayPal()
    {
        $this->layout = "mainColumn3";

        $user = new AbaUser();
        $user->getUserById(Yii::app()->user->id);
        $modFormPayment = new PaymentForm();
        $modFormPayment = $this->fillPaymentForm($modFormPayment, $user);
        $modFormPayment = $this->emptyCardDetailsForPaypal($modFormPayment);
        $this->user = $user;

        $modFormPayment->addError("PaymentControlProcess",
            Yii::t('mainApp', 'It is not allowed to create a new subscription when you are PREMIUM already') );
        $this->redirPaymentMethod($modFormPayment, false, $user, false);
        return;
    }

    /**
     * It processes the Credit card payments. Originally only with La Caixa.
     * @returns null It redirects to a Confirm page or again to the Payment Page
     */
    public function actionProcessPayment()
    {
        $this->layout = "mainColumn3";

        if (!$this->accessToPlusFlow()){
            $this->redirect(Yii::app()->createUrl("/site/noaccess"));
            return;
        }

        $modFormPayment = new PaymentForm();
        $modFormPayment = $this->fillPaymentForm($modFormPayment, $this->user);
        $this->emptyCardDetailsForCard($modFormPayment);
        // Validate user input, through standard validations of Yii Model Form. Both AllPago and Caixa:
        if (!$modFormPayment->validate()) {
            $this->redirPaymentMethod($modFormPayment, false, $this->user, false);
            return;
        }

        //
        //#5087
        $bPlanPrice = true;

        /*
         * It inserts all user form data into paymentControlCheck to avoid in case of error to lose the user input
         * */
        $paymentControlCheck = $this->commOnlPay->savePaymentBeforeGateway($modFormPayment,
                                            $modFormPayment->getIdPaySupplier(), $this->user,
                                            $this->getIdPartner(), true, $bPlanPrice);
        if (!$paymentControlCheck) {
            $this->redirPaymentMethod($modFormPayment, false, $this->user, false);
            return;
        }

        if ( $modFormPayment->idPayMethod==PAY_METHOD_CARD ){

            $paramsPost = HeMixed::getPostArgs();

            if ($this->runCardTransaction( $modFormPayment, $paymentControlCheck, $paramsPost)){
                return;
            }
        }

        return;
    }

    /** Process the Boleto Request. If a Boleto has been recently requested it redirects to the subscription page.
     *
     */
    public function actionProcessPaymentBoleto()
    {
        $this->layout = "mainColumn3";

        // We check if a Boleto has been requested in the last 5 minutes, and in that case we redirect him
        // to the subscription page
        $boleto = new AbaPaymentsBoleto();
        if ( $boleto->anyPendingLastTime(Yii::app()->user->id, 5) ){
            $this->redirect(Yii::app()->createUrl("/subscription/index"));
        }

        if (!$this->accessToPlusFlow()){
            $this->redirect(Yii::app()->createUrl("/site/noaccess"));
            return;
        }

        $modFormPayment = new PaymentForm();
        $modFormPayment = $this->fillPaymentForm($modFormPayment, $this->user);
        $this->emptyCardDetailsForBoleto($modFormPayment);
        // Validate user input, through standard validations of Yii Model Form. Both AllPago and Caixa:
        if (!$modFormPayment->validate()) {
            $this->redirPaymentMethod($modFormPayment, false, $this->user, false);
            return;
        }

        //
        //#5087
        $bPlanPrice = true;

        $paymentControlCheck = $this->commOnlPay->savePaymentBeforeGateway($modFormPayment,
                                            $modFormPayment->getIdPaySupplier(), $this->user, $this->getIdPartner(), true, $bPlanPrice);
        if (!$paymentControlCheck) {
            $this->redirPaymentMethod($modFormPayment, false, $this->user, false);
            return;
        }

        if ( $modFormPayment->idPayMethod==PAY_METHOD_BOLETO ){
            if ($this->runBoletoTransaction( $modFormPayment, $paymentControlCheck)){
                return;
            } else {
                $modFormPayment->addError('paymentControlCheck', 'Boleto could not be requested fully successful. '.
                    'Please contact our support department' );
            }
        } else {
            // It should never happen:
            $modFormPayment->addError('paymentControlCheck', 'Method and Supplier for your payment does not match.'.
                'Please contact our Support department');
        }

        $this->redirPaymentMethod($modFormPayment, false, $this->user, false);
        return;
    }

    /** To force URL display for tracking purposes...
     *
     * @param string $idPay
     */
    public function actionConfirmedPayment($idPay)
    {
        $payment = new Payment();
        $payment->getPaymentById( $idPay );

        $moUser = new AbaUser() ;
        $moUser->getUserById(Yii::app()->user->getId());

        if ( $payment->status!=PAY_SUCCESS ){
            $modFormPayment=$this->fillPaymentForm(new PaymentForm(), $moUser, false);
            $this->redirPaymentMethod( $modFormPayment, false, $moUser, false);
            return;
        }

        unset(Yii::app()->user->paymentControlCheck);
        unset(Yii::app()->session['paymentControlCheck']);
        Yii::app()->user->setState("paymentMethodAttempt", NULL);
        Yii::app()->user->setState("paymentControlCheckId", NULL);
        Yii::app()->user->setState("paymentControlStatus", NULL);

        $modPeriod = new ProdPeriodicity();
        $modPeriod->getProdPeriodById($payment->idPeriodPay);

        $this->layout = "mainResponsive";

        //
        //#5635
        $stConfirmedPaymentData = $this->getConfirmPaymentData($moUser, $payment, true);

        $data = array(
            'isPaymentPlus' =>                  true,
            'payment' =>                        $payment,
            'months' =>                         $modPeriod->months,
            'sPaymentConfirmMonths' =>          $stConfirmedPaymentData['sPaymentConfirmMonths'],
            'sPaymentConfirmDateToRenewal' =>   $stConfirmedPaymentData['sPaymentConfirmDateToRenewal'],
            'sPaymentConfirmPrice' =>           $stConfirmedPaymentData['sPaymentConfirmPrice'],
            'userData' => array(
                'email' => $moUser->email,
                'fullName' => $moUser->name.' '.$moUser->surnames
            )
        );
        parent::updateDataLayerPaymentDetails($payment);

        $this->headerTitle = Yii::t('mainApp', 'paymentConfirm_info1_ext');

        $this->render('/payments/confirmedPayment', $data);
    }


    /**
     * @param PaymentForm $modFormPayment
     * @param PaymentControlCheck $paymentControlCheck
     * @param PayGatewayLogLaCaixaResRec|PayGatewayLogAllpago|PayGatewayLogAdyenResRec $retPaySupplierLog
     * @param  PaySupplierLaCaixa|PaySupplierAllPagoBr|PaySupplierAllPagoMx|PaySupplierAdyen $paySupplier
     *
     * @return bool|Payment
     */
    protected function runAfterCardTransactionConfirmed( PaymentForm $modFormPayment,
                                                         PaymentControlCheck $paymentControlCheck,
                                                         $retPaySupplierLog, $paySupplier )
    {
        /* +++++++++++++ Point of no return +++++++++++++++++*/
        //
        //#5818
        $idPartner = PARTNER_ID_RECURRINGPAY;
        $idPartnerPendingPayment = null;
        if($this->getIdPartner() == PARTNER_ID_UPGRADE) {
            $idPartner = PARTNER_ID_UPGRADE;
            $idPartnerPendingPayment = PARTNER_ID_UPGRADE;
        }

        $successAfterGateway =  true;
        $lastAction =           "";
        $isExtend =             1;
        $isRecurring =          1;
        $isPlanProduct =        false;

        //#5087
        $commProducts = new ProductsPricesCommon();
        if($commProducts->isPlanProduct($paymentControlCheck->idProduct)) {
            $isPlanProduct =    true;
            $isExtend =         0;
            $isRecurring =      0;
        }

        $payment = $this->commOnlPay->savePaymentCtrlCheckToUserPayment($modFormPayment, $paymentControlCheck, $retPaySupplierLog, $idPartner, 0, $isExtend, $isRecurring);

        if (!$payment) {
            /*  If Error 5. Update Payment FAILED */
            $lastAction = " savePaymentCtrlCheckToUserPayment: Saving user_credit_card or user or payment failed. " .
                "Probably SQL failure.";
            $successAfterGateway = false;
        } else {

            $commUser =         new UserCommon();
            $moPeriodicity =    new ProdPeriodicity();

            //
            //#5087
            // Family plan
            //
            if($isPlanProduct) {

                //
                // Cancel pending
                //
                $commRecurring = new RecurrentPayCommon();
                $commRecurring->cancelPendingPayment($this->user, $paymentControlCheck, $idPartnerPendingPayment);

                /**********************************************************************************************************/
                /*****SUCCESS SO CONVERT USER TO PREMIUM******/
                /**********************************************************************************************************/

                $moPeriodicity->getProdPeriodById($payment->idPeriodPay);

                //
                $dateStart = $paymentControlCheck->dateToPay;

                $oProdPeriodicity = new ProdPeriodicity();
                $iMonthPeriod =     $oProdPeriodicity->getPeriodicityMonth($paymentControlCheck->idPeriodPay);

                $expirationDate = HeDate::getDateAdd($dateStart, 0, $iMonthPeriod);


                /* 6. Update userType, expiration date and idPartners in AbaUser from Free to PREMIUM or WHATEVER */
                $aConfirmOpers =    $this->convertPremiumCreateNextPay($payment, $this->user, $idPartner, $expirationDate);
                /* @var Payment $nextPayment */
                $nextPayment =      $aConfirmOpers["nextPayment"];
                /* @var ProductPrice $prodSelected */
                $this->user =       $aConfirmOpers["user"];
                $lastAction .=      $aConfirmOpers["errorDesc"];
                if (!$aConfirmOpers["success"]) {
                    $successAfterGateway = $aConfirmOpers["success"];
                }

                /* * 5.1. Insert Next Payment PENDING */
                $nextPayment->id =  HeAbaSHA::generateIdPayment($this->user, $nextPayment->dateToPay);
                $userCreditCard =   new AbaUserCreditForms();
                $userCreditCard =   $userCreditCard->getUserCreditFormById($payment->idUserCreditForm);

                /* @var PayGatewayLogAllpago $retRegistSupplierLog */
                $retRegistSupplierLog = $paySupplier->runCreateRecurring( $nextPayment, $paymentControlCheck, $this->user);

                if ($retRegistSupplierLog && $retRegistSupplierLog instanceof PayGatewayLog) {
                    $registrationId =                   $retRegistSupplierLog->getUniqueId();
                    $userCreditCard->registrationId =   $registrationId;

                    $userCreditCard->update(array("registrationId"));

                    $nextPayment->paySuppExtProfId =    $registrationId;
                    $nextPayment->paySuppExtUniId =     $registrationId;
                    $nextPayment->paySuppOrderId =      "";
                } elseif($retRegistSupplierLog) {
                    // No need to create profile nor anything else, success for this supplier. La Caixa basically until 2014-12-04
                } else {
                    $lastAction .= $paySupplier->getSupplierName().
                        '.runCreateRecurring(): Registering user credit card for Next Pending payment failed '.
                        $paySupplier->getErrorCode() . " " . $paySupplier->getErrorString();
                    $successAfterGateway = false;
                }

                if (!$nextPayment->paymentProcess()) {
                    $lastAction .= " insertPayment(): Inserting next recurrent payment failed.";
                    $successAfterGateway = false;
                }
                else {

                    //
                    //@TODO @TODO @TODO @TODO @TODO
                    //
//                    $commRecurring = new RecurrentPayCommon();
//                    $commRecurring->createUserPromos($this->user, $nextPayment);

                }
            }
            else {
                /**********************************************************************************************************/
                /*****SUCCESS SO EXTEND PREMIUM PERIOD******/
                /**********************************************************************************************************/
                $moPeriodicity->getProdPeriodById($payment->idPeriodPay);
                $expirationDate = $commUser->getNextExpirationDate($this->user, $moPeriodicity) ;
                $moUser = $commUser->convertToPremium($this->user, $payment->idPartner, $expirationDate);
                if(!$moUser){
                    $lastAction = " convertToPremium: Extending period of PREMIUM failed. Please review PaymentPlus payments.";
                    $successAfterGateway = false;
                }
            }
        }

        if ($successAfterGateway) {
            $lastAction .= " The payment, user credit card and user data was inserted properly into database. ";
            $paymentControlCheck->updateStatusAfterPayment(PAY_SUCCESS, $retPaySupplierLog->getPaySuppExtId(), $retPaySupplierLog->getOrderNumber(), $lastAction);
            $paymentControlCheck->updateReviewedLastWeekAttempts($this->user->getId());
        } else {
            /* But PAYMENT HAS BEEN EXECUTED AND SUCCESSFULLY RECEIVED but errors have occurred after this,
            so our database is inconsistent. */
            $lastAction .= " Payment has been accepted by the supplier " . $retPaySupplierLog->getPaySuppExtId() .
                " and the response of the supplier is within the table " .
                $retPaySupplierLog->tableName() . ", id = " . $retPaySupplierLog->id;
            $paymentControlCheck->updateStatusAfterPayment(PAY_FAIL, $retPaySupplierLog->getPaySuppExtId(),
                $retPaySupplierLog->getOrderNumber(), $lastAction);
            $this->user->abaUserLogUserActivity->saveUserLogActivity("payment", $lastAction, "extended payment done");

            HeLogger::sendLog(HeLogger::PREFIX_ERR_UNKNOWN_H.' Payment through '.$paySupplier->getSupplierName().
              ' successful, but finished with errors in our database; user=' .
              $this->user->email . ", payment id=" . $paymentControlCheck->id, HeLogger::PAYMENTS, HeLogger::CRITICAL,
              " user=" . $this->user->email . ", payment control check id=" . $paymentControlCheck->id);
        }

        // --------- After Supplier accepted payment
        return $payment;
    }

    /** Only function in charge to prepare and to render the final page of payment method selection
     *
     * @param PaymentForm $modFormPayment
     * @param $success
     * @param AbaUser $user
     */
    protected function redirPaymentMethod(PaymentForm $modFormPayment, $success, AbaUser $user, $isUpgrade=false) {

        //#5087
        $stPackageSelected =    HePayments::parseProductPricePK($modFormPayment->packageSelected);
        $productId =            $stPackageSelected[0];

        //#5087
        $isPaymentPlus =    true;
        $commProducts =     new ProductsPricesCommon();
        if($commProducts->isPlanProduct($productId)) {
            $isPaymentPlus = false;
        }

        $listOfCreditCards = PaymentFactory::listCardsAvailable($user);

        if($isPaymentPlus) {
            $modFormPayment->productsPricesPlans = $this->getAllAvailableProductsPrices($user, false, $modFormPayment->packageSelected, true);
        }
        else {
            $modFormPayment->productsPricesPlans = $this->getAllAvailableProductsPrices($user, true, $modFormPayment->packageSelected, $isUpgrade);
        }

        $monthsPeriod = null;

        foreach ($modFormPayment->productsPricesPlans as $key => $productPrice) {
            if ($modFormPayment->packageSelected == $productPrice["productPricePK"]) {
                $monthsPeriod = $productPrice['monthsPeriod'];
                break;
            }
        }

        $commRecurring = new RecurrentPayCommon();

        $arrayErrors = $modFormPayment->getErrors();
        if (!empty($arrayErrors)) {
            $attempt = 1;
            $sessionAttemptVar = Yii::app()->user->getState('paymentMethodAttempt');
            if (isset($sessionAttemptVar)) {
                $attempt = intval($sessionAttemptVar) + 1;
            }
            Yii::app()->user->setState("paymentMethodAttempt", $attempt);
        }

        $isChangePlan = false;
        $this->layout = "mainResponsive";
        $urlReturn = 'paymentsplus/payment';
        $urlsFormProcess = array(
            PAY_METHOD_PAYPAL => $this->createURL('/paymentsplus/processPaymentPayPal'),
            PAY_METHOD_CARD => $this->createURL('/paymentsplus/processPayment'),
            PAY_METHOD_BOLETO => $this->createURL('/paymentsplus/ProcessPaymentBoleto'),
            PAY_METHOD_ADYEN_HPP => ""
        );

        if($isPaymentPlus) {
            $urlReturn =        'paymentsplus/payment';
            $urlsFormProcess =  array(
                PAY_METHOD_PAYPAL =>    $this->createURL('/paymentsplus/processPaymentPayPal'),
                PAY_METHOD_CARD =>      $this->createURL('/paymentsplus/processPayment'),
                PAY_METHOD_BOLETO =>    $this->createURL('/paymentsplus/ProcessPaymentBoleto'),
                PAY_METHOD_ADYEN_HPP => ""
            );
        }
        else {
            $urlReturn =        'paymentsplus/familypayment';
            $urlsFormProcess =  array(
                PAY_METHOD_PAYPAL =>    $this->createURL('/paymentsplus/processPaymentPayPal'),
                PAY_METHOD_CARD =>      $this->createURL('/paymentsplus/processPayment'),
                PAY_METHOD_BOLETO =>    $this->createURL('/paymentsplus/ProcessPaymentBoleto'),
                PAY_METHOD_ADYEN_HPP => ""
            );
        }

        $dateStart = $this->getDateStartNextPay($user, $isPaymentPlus);

        if($isPaymentPlus AND HeDate::removeTimeFromSQL($dateStart) != HeDate::europeanToSQL($user->expirationDate) ){
            $err = 'Premium paying, expiration date and date to Pay does not match('.$dateStart.', '.$user->expirationDate.'). '.
                'User '.$this->user->id;
            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_LOGIC_L.' Inconsistency between dates, userPREMIUM extending payment',
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                $err
            );
        }

        $idPaySupplier = PaymentFactory::whichCardGatewayPay($user);
        $paymentControlCheck = new PaymentControlCheck();
        if (isset(Yii::app()->user->paymentControlCheck)) {
            /* @var PaymentControlCheck $paymentControlCheck */
            $paymentControlCheck = unserialize(Yii::app()->user->paymentControlCheck);
            $idPaySupplier = $paymentControlCheck->paySuppExtId;
        } else {
            $paymentControlCheck->paySuppExtId = $idPaySupplier;
        }

        $typeSubsStatus =   $commRecurring->getDatesSubscription($user, $dateStart, $monthsPeriod, $modFormPayment->packageSelected);

        //
        //#4986
        $adyenPaymentData = array(
            "paymentAmount" =>    0,
            "currencyCode" =>     "",
            "countryCode" =>      "",
            "skinCode" =>         Yii::app()->config->get("PAY_ADYEN_HPP_SKIN_DEFAULT_NAME"),
            "sessionValidity" =>  "",
            "idProduct" =>        "",
        );

        $isPlanPriceFree = false;

        foreach ($modFormPayment->productsPricesPlans as $iKey => $productPrice) {

            if ($modFormPayment->packageSelected == $productPrice["productPricePK"]) {

                $idProduct = explode("_", $productPrice['idProduct']);

                $abaCountry = new AbaCountry();
                $abaCountry->getCountryById($idProduct[0]);

                $adyenPaymentData["paymentAmount"] = $productPrice['finalPrice'];
                $adyenPaymentData["currencyCode"] = $productPrice['ccyIsoCode'];
                $adyenPaymentData["countryCode"] = (isset($abaCountry->iso) ? $abaCountry->iso : ''); // RU, ES,..
                //
                //#abawebapps-158
                $adyenPaymentData["idProduct"] = $productPrice['idProduct'];

                $isPlanPriceFree = $productPrice['isPlanPrice'];
                break;
            }
        }

        //
        $aIdsPaysMethods = PaymentFactory::whichMethodsAvailable($user, true);

        //#5191
        $dPay = HeMixed::getGetArgs("dPay");

        //terms and conditions data to render inline
        $payCommSupplier = PaymentFactory::createPayment( NULL, $idPaySupplier);
        $idTextSpecificConditions = $payCommSupplier->getListSpecificConditions();

        $data = array(
            'modelPayForm' =>       $modFormPayment,
            'listOfCreditCards' =>  $listOfCreditCards,
            'urlsFormProcess' =>    $urlsFormProcess,
            'urlReturn' =>          $urlReturn,
            'success' =>            $success,
            'user' =>               $user,
            'paySuppExtId' =>       $idPaySupplier,
            'aIdsPaysMethods' =>    $aIdsPaysMethods,
            'aSpecialFields' =>     $modFormPayment->getSpecialFieldsPaySupplier($paymentControlCheck),
            'typeSubsStatus' =>     $typeSubsStatus,
            'aAllFieldsByMethod' => PaymentFactory::getAllFieldsByMethodByCountry( $aIdsPaysMethods, $user->countryId ),
            'isPaymentPlus' =>      $isPaymentPlus,
            'isPlanPrice' =>        ($isPaymentPlus ? false : true),
            'isPlanPriceFree' =>    $isPlanPriceFree,
            'isUpgrade' =>          $isUpgrade,
            'adyenPublicKey' =>     Yii::app()->config->get("PAY_ADYEN_SOAP_PUBLIC_KEY"),
            'adyenPaymentData' =>   $adyenPaymentData,
            'dPay' =>               $dPay,
            'bDiscardCardData' =>   true,
            'idTextSpecificConditions' => $idTextSpecificConditions
        );

        $this->renderFooter = false;

        $this->render('/payments/paymentMethod', $data);
    }

    /** Unique function to render the selection product page whether for FREE or PREMIUM users.
     *
     * @param $urlContinue
     */
    protected function redirPayProductSelection($urlContinue)
    {
        $aRetProdsAndPrices =   $this->collectAndSelectProducts($this->user, true, true);
        $products =             $aRetProdsAndPrices[0];
        $packageSelected =      $aRetProdsAndPrices[1];
        $anyDiscount =          $aRetProdsAndPrices[2];
        $typeDiscount =         $aRetProdsAndPrices[3];
        $validationTypePromo =  $aRetProdsAndPrices[4];

        //
        //#5087
        $productsPlan = array();

        foreach($products as $product) {
            if($product['isPlanPrice'] == 1) {
                $productsPlan[] = $product;
            }
        }

        // Register locations every time we go through Home Page:
        $commUser = new UserCommon();
        $commUser->registerGeoLocation($this->user, Yii::app()->request->getUserHostAddress());

        // In case of a change of subscription we check last product, so current one:
        $currentProduct = false;
        $dateToPay = $this->getDateStartNextPay($this->user);
        $moLastPay = new Payment();
        if ($moLastPay->getLastPayPendingByUserId($this->user->id)){
            $currentProduct = $moLastPay->idProduct;
            $dateToPay = $moLastPay->dateToPay;
        }

        $aInfoBoletos =     $this->getAnyLastBoletoPending();
        $boletoPrice =      $aInfoBoletos['boletoPrice'];
        $productBoleto =    $aInfoBoletos['productBoleto'];
        $moBoleto =         $aInfoBoletos['moBoleto'];

        $this->layout = "mainResponsive";
        $data = array(
            'isPaymentPlus' =>          true,
            'isChangePlan' =>           false,
            'products' =>               $products,
            'productsPlan' =>           $productsPlan,
            'packageSelected' =>        $packageSelected,
            'currentProduct' =>         $currentProduct,
            'anyDiscount' =>            $anyDiscount,
            'typeDiscount' =>           $typeDiscount,
            'validationTypePromo' =>    $validationTypePromo,
            'urlContinue' =>            $urlContinue,
            'urlWithPromo' =>           'paymentsplus/'.$this->action->id,
            'dateToPay' =>              $dateToPay,
            'moBoleto' =>               $moBoleto,
            'boletoPrice' =>            $boletoPrice,
            'productBoleto' =>          $productBoleto,
            'sPaymentLanguage' =>       strtolower(Yii::app()->user->getLanguage()),
            'iPaymentCountry' =>        Yii::app()->user->getCountryId(),
        );

        $this->render('/payments/payment', $data);
    }


    /** Redirects to the confirmation view page.
     * @param AbaUser $user
     * @param Payment $payment
     * @param null    $userPayPal
     */
    protected function redirConfirmedPayment( AbaUser $user, Payment $payment, $userPayPal=NULL )
    {
        $this->redirect(Yii::app()->createUrl("/paymentsplus/confirmedPayment", array('idPay' => $payment->id)));
    }

    /**
     * @param PaymentControlCheck $payDraft
     * @param AbaPaymentsBoleto $boleto
     */
    protected function redirConfirmedBoleto( PaymentControlCheck $payDraft, AbaPaymentsBoleto $boleto  )
    {
        unset(Yii::app()->user->paymentControlCheck);
        unset(Yii::app()->session['paymentControlCheck']);
        Yii::app()->user->setState("paymentMethodAttempt", NULL);
        Yii::app()->user->setState("paymentControlCheckId", NULL);
        Yii::app()->user->setState("paymentControlStatus", NULL);

        $this->render("/payments/requestedBoleto", array( 'payment' => $payDraft,
                                                'urlBoleto' => $boleto->url,
                                               'dueDateBoleto' => $boleto->dueDate ));
        return;
    }

    /** Returns true if it has access to this flow, if FREE it redirects to the normal version
     *  payment page.
     *
     * @return bool
     */
    protected function accessToPlusFlow( )
    {
        if ( intval(Yii::app()->config->get('ENABLE_PREMIUM_PAY_EXTEND'))==0 ){
            return false;
        } else {
            $this->user = new AbaUser();
            if ($this->user->getUserById(Yii::app()->user->id)) {
                if (Yii::app()->user->refreshAndGetUserType($this->user) == FREE) {
                    $this->redirect(Yii::app()->createUrl("/payments/payment"));
                    return false;
                }
            } else {
                $this->redirect(Yii::app()->createUrl("/site/noaccess"));
            }
        }

        return true;
    }

    /** Returns Date in which the user currently have to make next payment.
     *In case no Pending payment it will deliver Expiration date of the PREMIUM user.
     *
     * @param AbaUser $user
     * @param bool $isPaymentPlus
     *
     * @return string
     */
    private function getDateStartNextPay($user, $isPaymentPlus=false) {

        if($isPaymentPlus) {
            $moPendingPay = new Payment();
            if ($moPendingPay->getLastPayPendingByUserId($user->getId())){
                return $moPendingPay->dateToPay;
            } else {
                return HeDate::europeanToSQL($user->expirationDate);
            }
        }
        else {
            return HeDate::todaySQL(true);
        }
    }

    /**
     * UPGRADES
     *
     * For a PREMIUM user the action URL to type in a new method of payment.
     * @param string $promoCode An existing promocode
     */
    public function actionFamilyPayment($promoCode='')
    {
        $this->layout = "mainColumn3";
        $success =      false;

        if (!$this->accessToPlusFlow()){
            $this->redirect(Yii::app()->createUrl("/site/noaccess"));
            return;
        } else {

            //
            $commRecurring = new RecurrentPayCommon();

            if($commRecurring->userHasActiveFamilyPlan($this->user->id)) {
                $this->redirect(Yii::app()->createUrl("/site/index"));
                return;
            }

            //
            $modFormPayment =   new PaymentForm();
            $modFormPayment =   $this->fillPaymentForm($modFormPayment, $this->user);
            $productId =        HeMixed::getGetArgs("productId");
            $idPeriod =         HeMixed::getGetArgs("idPeriod");

            //
            //#5818 @TODO
            Yii::app()->user->setState( "idPartnerCurNavigation", PARTNER_ID_UPGRADE);

            $commProducts = new ProductsPricesCommon();

            if (trim($productId) != '') {
                $modFormPayment->packageSelected = $commProducts->getProductPricePkById($productId);
            }
            elseif (trim($idPeriod) != '') {
                //
                $countryId =                        $commProducts->getCountryIdByUser($this->user);
                $productId =                        HePayments::makeFamilyPlanProductId($countryId, $idPeriod);
                $modFormPayment->packageSelected =  $commProducts->getProductPricePkById($productId);
            }

            $this->redirPaymentMethod($modFormPayment, $success, $this->user, true);
        }
    }

    /**
     * #5087
     * Convert To Premium and creates Next payment model in memory
     * @param Payment $payment
     * @param AbaUser $user
     * @param $idPartner
     * @return array
     */
    protected function convertPremiumCreateNextPay(Payment $payment, AbaUser $user, $idPartner, $expirationDate)
    {
        $successAfterGateway = true;
        $lastAction = "";
        /* @var Payment $nextPayment */
        $nextPayment = $payment->createDuplicateNextPayment();
        if(!$nextPayment){
            $lastAction = " createDuplicateNextPayment: Clonning payment failed. Check periodicity, product, amount.";
            $successAfterGateway = false;
        }

        //#5087
        $nextPayment->dateToPay = $expirationDate;

        //#4400
        if($payment->paySuppExtId == PAY_SUPPLIER_ADYEN) {
            $nextPayment->paySuppExtUniId = $payment->paySuppExtUniId;
        }

        /* Double-Check product type again   */
        $prodSelected = new ProductPrice();
        $prodSelected = $prodSelected->getProductById($payment->idProduct);
        if (!$prodSelected) {
            $lastAction .= " getProductById/updateUserType: Assigning new access type to the user failed.";
            $successAfterGateway = false;
            $userTypeProd = PREMIUM;
        } else{
            $userTypeProd = $prodSelected->userType;
        }

        $commUser = new UserCommon();
        $user =     $commUser->convertToPremium($user, $idPartner, $nextPayment->dateToPay, $userTypeProd);
        if (!$user) {
            $user = $this->user;
            $lastAction .= "Failed to convert to Premium. Review user ".$user->id;
            $successAfterGateway = false;
        }
        // For the current session we also have to update his userType to PREMIUM.
        Yii::app()->user->setState("role", $userTypeProd);

        return array(
            "success" =>        $successAfterGateway,
            "errorDesc" =>      $lastAction,
            "nextPayment" =>    $nextPayment,
            "user" =>           $user,
            "prodSelected" =>   $prodSelected
        );
    }

}
