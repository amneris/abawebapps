<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abaenglish
 * Date: 28/06/13
 * Time: 11:03
 *
 *  It is expected that this services are
 * called from mobile devices.
 */
class WsrestuserController extends WsRestController
{
    /**
     * Default response format
     * either 'json' or 'xml'
     */
    protected $_format = 'json';

    /**
     * Once we have a simulator to send HTTP BASIC DIGEST, we can remove this function. For
     *this web services we want all authenticated
    */
    protected function checkIfLogined()
    {
        //return true;
        return parent::checkIfLogined();
    }

    /**
     * Generic controller for view for this group of web services
     */
    public function actionView()
    {
        if( array_key_exists('object',$this->reqDataDecoded)){
            $subView = 'view'.ucfirst($this->reqDataDecoded['object']) ;
            $idObj = $this->reqDataDecoded['id'];
            if( method_exists($this, $subView)){
                $this->$subView($idObj);
                return true;
            }
        }

        $this->sendResponse(400, 'Missing object parameter or invalid value', $this->_format);
        return false;
    }

    /**
     * Generic controller for view for this group of web services
     * Example: wsrestuser/list/object/videoclasses
     */
    public function actionList()
    {
        if( array_key_exists('object',$this->reqDataDecoded)){
            $subList = 'list'.ucfirst($this->reqDataDecoded['object']) ;
            if( method_exists($this, $subList)){
                $this->$subList();
                return true;
            }
            else{
                $this->sendResponse(400, 'Missing object parameter or invalid value', $this->_format);
            }
        }

        $this->sendResponse(400, 'Missing object parameter or invalid value', $this->_format);
    }

    /**
     * Generic controller in order to update one or several fields of an element.
     * It is expected from one to 4 fields.
     * Example: wsrestuser/update/object/userLevel
     */
    public function actionUpdate()
    {
        if( array_key_exists('object',$this->reqDataDecoded)){
            $subUpdate= 'update'.ucfirst($this->reqDataDecoded['object']) ;
            if( method_exists($this, $subUpdate)){
                $this->$subUpdate();
                return true;
            }
        }

        $this->sendResponse(400, 'Missing object parameter or invalid value', $this->_format);
    }

    /** wsrestuser/view/object/videoclass/id/idXXX
     * Returns info of a single videoclass
     * @param integer $idVideo
     *
     * @return bool
     */
    protected function viewVideoclass( $idVideo)
    {
        /*   @todo Unify Code in a Rule altogether with videoclasses/allUnits       */
        $idVideo = intval($idVideo);
        if( $idVideo>HeUnits::getMaxUnits() || !is_numeric($idVideo))
        {
            $this->sendResponse(400, array( 'status'=>'error', 'message'=>'Value videoclass not valid'));
            return false;
        }
        if( !HeRoles::allowOperation( OP_VIDEOCLASSES, $this->moUser->userType ) )
        {
            $this->sendResponse(403);
            return false;
        }

        $userTypeAccessUnit = HeRoles::typeOfVideoClassAccess($this->moUser->userType, $idVideo);

        switch($userTypeAccessUnit)
        {
            case OP_INCOMPLETEUNIT:
                $this->sendResponse( 200, array('unit' => $idVideo,
                                               "userTypeAccessUnit"=> OP_INCOMPLETEUNIT,
                                               "type"=>"video"),
                                            $this->_format);
                break;
            case OP_FULLUNIT:
                $this->sendResponse( 200, array('unit' => $idVideo,
                                                "userTypeAccessUnit"=> OP_FULLUNIT,
                                                "type"=>"video"),
                    $this->_format);
                break;
            case OP_NOACTIONACCESSUNIT:
                $this->sendResponse( 403, 'No access, go to PREMIUM' );
                break;
            case false:
            default:
                $this->sendResponse( 403, 'No access, go to PREMIUM' );
                break;
        }

        return true;
    }


    // * Example: wsrestuser/list/object/videoclasses
    /** Outputs an JSON array with information of all videoclasses
     * @return bool
     */
    protected function listVideoclasses()
    {
        /*   @todo Unify Code in a Rule altogether with videoclasses/index       */
        if( !HeRoles::allowOperation( OP_VIDEOCLASSES, $this->moUser->userType ) )
        {
            $this->sendResponse(403, array('status'=>'no access','message'=>'User neither FREE nor PREMIUM'));
            return false;
        }

        $units = null;
        $aUnits = array();
        for($tmpLevel=1; $tmpLevel<=MAX_ENGLISH_LEVEL; $tmpLevel++) {
            $units = new VideoClasses();
            /* @var VideoClass[] $aUnitsObject */
            $aUnitsObject = $units->getAllUnitsByLevel($tmpLevel);

            /*  @var VideoClass $unitCourse */
            /*  @var integer $unitId */
            foreach( $aUnitsObject as  $unitId=>$unitCourse )
            {
                $aUnits[$unitId]['level'] = $tmpLevel;
                $aUnits[$unitId]['unit'] = $unitCourse->getFormatUnitId($unitId);

                $followUpCourse = new AbaFollowup();
                $aUnits[$unitId]["VideoClassProgress"] = 0;
                if($followUpCourse->getFollowupByIds( Yii::app()->user->getId(),  $unitId ))
                {
                    $aUnits[$unitId]["VideoClassProgress"] = $followUpCourse->gra_vid;
                }
                $aUnits[$unitId]["userAccess"] = false;
                $typeOfAccessToVideo= HeRoles::typeOfVideoClassAccess($this->moUser->userType, $unitId);
                if( $typeOfAccessToVideo==OP_FULLUNIT || $typeOfAccessToVideo==OP_INCOMPLETEUNIT )
                {
                    $aUnits[$unitId]["userAccess"] = true;
                }

                $aUnits[$unitId]["lockInfoStatus"] = UNLOCKED_VC;
            }

        }

        $this->sendResponse( 200,array("userLevel"=>$this->moUser->currentLevel,
                                       "aUnits" => $aUnits),
                            $this->_format);
        return true;

    }

    /** Updates user Level, logs the change in the table log_user_level,
     * send to Selligent, etc.
     *
     * @return bool
     */
    protected function updateUserlevel()
    {
        $currentLevel = $this->reqDataDecoded['userLevel'];
        if( $currentLevel=='' ){
            $currentLevel = HeMixed::getGetArgs("userLevel");
            if (empty($currentLevel)){
                $this->sendResponse( 400, array('status'=>'Error', 'message'=>"Param user level missing."), $this->_format);
                return false;
            }
        }

        $oldLevel = $this->moUser->currentLevel;
        if ( intval($oldLevel)!==intval($currentLevel) ){
            if( $this->moUser->updateUserCourseLevel($currentLevel) ) {
                $commUser = new UserCommon();
                $commUser->changeUserLevel( $this->moUser, $oldLevel, $currentLevel, 'From Rest Services');
            } else{
                $this->sendResponse( 500, array('status'=>'Error', 'message'=>"Level could not be updated"), $this->_format);
                return false;
            }
        }
        else{
            $this->sendResponse( 400, array('status'=>'Error', 'message'=>"Level is the same"), $this->_format);
            return false;
        }

        $commConnSelligent = new SelligentConnectorCommon();
        $commConnSelligent->sendOperationToSelligent(SelligentConnectorCommon::perfil,
            $this->moUser, array(), "actionUpdate" );

        $this->sendResponse( 200, array("status"=>'OK', "userLevel"=>$currentLevel), $this->_format);
        return true;
    }

    /** Updates the videoclass progress from REST web services. By POST.
     * 
     * @return bool
     */
    protected function updateFollowupvideo()
    {
        $fProgressUpdated= false;

        $aParams = array();
        $aParams["id"] = $this->reqDataDecoded["id"];
        $aParams["followUpVideo"] = $this->reqDataDecoded["followUpVideo"];
        if( empty($aParams) ){
            $aParams["id"] = HeMixed::getGetArgs("id");
            $aParams["followUpVideo"] = HeMixed::getGetArgs("followUpVideo");
            if (empty($aParams["id"])){
                $this->sendResponse( 400, array('status'=>'Error', 'message'=>"Parameter video class missing."), $this->_format);
                return false;
            }
            if (empty($aParams["followUpVideo"])){
                $this->sendResponse( 400, array('status'=>'Error', 'message'=>"Parameter progress missing."), $this->_format);
                return false;
            }
        }

        if( !is_numeric(($aParams["followUpVideo"])) ||
            !is_numeric(($aParams["id"])) ||
            (   intval($aParams["followUpVideo"])!==100 &&
                intval($aParams["followUpVideo"])!==0 &&
                intval($aParams["followUpVideo"])!==50) )
        {
            $this->sendResponse( 400, array('status'=>'Error', 'message'=>"*Values are invalid, check params received:".implode(array_keys($aParams))), $this->_format);
            return false;
        }

        $moFollowUp = new AbaFollowup();
        if( !$moFollowUp->getFollowupByIds($this->moUser->id, $aParams['id']) ){
            $idFollow = $moFollowUp->insertVideoclassProgress($this->moUser->id, $aParams['id'],
                $aParams["followUpVideo"] );
            if( !$idFollow ){
                $this->sendResponse( 500,
                    array('status'=>'Error', 'message'=>"Video progress could not be created"), $this->_format);
                return false;
            }
            $fProgressUpdated= true;

        } else{
            $moFollowUp = new AbaFollowup();
            $moFollowUp->getFollowupByIds($this->moUser->id, $aParams["id"] );
            $moFollowUp->themeid = $aParams["id"];
            $moFollowUp->userid = $this->moUser->id;
            if( !$moFollowUp->updateVideoclassProgress( $aParams["followUpVideo"] )){
                $this->sendResponse( 500,
                    array('status'=>'Error', 'message'=>"Video progress could not be updated"), $this->_format);
                return false;
            }
            $fProgressUpdated= true;

        }

        $this->sendResponse( 200, array('status'=>'OK', 'message'=>'Updated to '.$aParams["followUpVideo"]), $this->_format);
        return true;
    }

    /** POsT METHOD: Updates user password, send to Selligent, etc.
     *
     * @return bool
     */
    protected function updateUserpassword()
    {
        $newPassword = $this->reqDataDecoded['password'];
        if( $newPassword=='' ){
            $newPassword = HeMixed::getGetArgs("password");
            if ( empty($newPassword) ){
                $this->sendResponse( 400, array('status'=>'Error', 'message'=>"Param password missing or invalid."),
                                                $this->_format);
                return false;
            }
        }

        $commUser = new UserCommon();
        if (!$commUser->changePassword( $this->moUser, $newPassword, 'Action Rest Web Service updateUserpassword.')){
            $this->sendResponse( 500, array('status'=>'Error', 'message'=>"Error while updating the password."),
                                                $this->_format);
            return false;
        }

        $this->moDevice->refresh();

        $this->sendResponse( 200, array('status'=>'OK',"token"=>$this->moDevice->token), $this->_format);
        return true;
    }


    /** POsT METHOD:
     *
     * @return bool
     */
    protected function updateFreetopremium()
    {
        $periodId =         $this->reqDataDecoded['periodid'];
        $currency =         $this->reqDataDecoded['currency'];
        $price =            $this->reqDataDecoded['price'];
        $countryId =        $this->reqDataDecoded['countryid'];
        $userId =           $this->reqDataDecoded['userid'];
        $purchaseReceipt =  $this->reqDataDecoded['purchasereceipt'];

        if(trim($periodId) == '') {
            $periodId = HeMixed::getGetArgs("periodid");
            if ( empty($periodId) ){
                self::sendEmailInternal("FREETOPREMIUM", $this->moUser->id, "Error", "Param periodId missing or invalid.");
                $this->sendResponse( 400, array('status'=>'Error', 'message'=>"Param periodId missing or invalid."), $this->_format);
                return false;
            }
        }
        if(trim($currency) == '') {
            $currency = HeMixed::getGetArgs("currency");
            if ( empty($currency) ){
                self::sendEmailInternal("FREETOPREMIUM", $this->moUser->id, "Error", "Param currency missing or invalid.");
                $this->sendResponse( 400, array('status'=>'Error', 'message'=>"Param currency missing or invalid."), $this->_format);
                return false;
            }
        }
        if(trim($price) == '') {
            $price = HeMixed::getGetArgs("price");
            if ( empty($price) ){
                self::sendEmailInternal("FREETOPREMIUM", $this->moUser->id, "Error", "Param price missing or invalid.");
                $this->sendResponse( 400, array('status'=>'Error', 'message'=>"Param price missing or invalid."), $this->_format);
                return false;
            }
        }
        if(trim($purchaseReceipt) == '') {
            $purchaseReceipt = HeMixed::getGetArgs("purchasereceipt");
            if ( empty($purchaseReceipt) ){
                self::sendEmailInternal("FREETOPREMIUM", $this->moUser->id, "Error", "Param purchasereceipt missing or invalid.");
                $this->sendResponse( 400, array('status'=>'Error', 'message'=>"Param purchasereceipt missing or invalid."), $this->_format);
                return false;
            }
        }

        if(trim($countryId) == '') {
            $countryId = HeMixed::getGetArgs("countryid");
            if ( empty($countryId) ){
                self::sendEmailInternal("FREETOPREMIUM", $this->moUser->id, "Warning", "Empty param countryid.");
            }
        }
        if(trim($userId) == '') {
            $userId = HeMixed::getGetArgs("userid");
            if ( empty($userId) ){
                self::sendEmailInternal("FREETOPREMIUM", $this->moUser->id, "Warning", "Empty param userid.");
            }
        }
        if ( $this->moUser->id != $userId ){
            self::sendEmailInternal("FREETOPREMIUM", $this->moUser->id, "Error", "Param userid missing or invalid.");
            $this->sendResponse( 401, array('status'=>'Error', 'message'=>"Param userid missing or invalid."), $this->_format);
            return false;
        }

        /** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
        $commUser = new UserCommon();
        if (!$commUser->free2premium( $this->moUser, $periodId, $currency, $price, $countryId, $userId, $purchaseReceipt, 'Action Rest Web Service updateFreetopremium')) {
            self::sendEmailInternal("FREETOPREMIUM", $this->moUser->id, "Error", "Error while updating user to premium. " . trim($commUser->getErrMessage()));
            if(isset($commUser->codeError) AND $commUser->codeError == 410) {
                $this->sendResponse( 410, array('status'=>'Error', 'message' => $commUser->getErrMessage()), $this->_format);
            }
            else {
                $this->sendResponse( 500, array('status'=>'Error', 'message' => "Error while updating user to premium."), $this->_format);
            }
            return false;
        }

        $this->moDevice->refresh();

        /** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
        $productsPricesCommon = new ProductsPricesCommon();

        $idPeriodPay = $productsPricesCommon->getUserProductPeriodPay($this->moUser);

        $oUser = new AbaUser();
        $oUser->getUserById($this->moUser->getId());

        $this->sendResponse( 200, array(
            'status' =>         'OK',
            'token' =>          $this->moDevice->token,
            'userType' =>       $oUser->userType,
            'userLevel' =>      $oUser->currentLevel,
            'expirationDate' => $oUser->expirationDate,
            'idPeriod' =>       $idPeriodPay
        ), $this->_format);
        return true;
    }

    /**
     * @param $userId
     * @param $status
     *
     * @param $msg
     */
    public static function sendEmailInternal($title, $userId, $status, $msg) {
        HeLogger::sendLog($title . ". User " . $userId . ". " . $status . ". ", HeLogger::PAYMENTS, HeLogger::CRITICAL, $msg);
    }

    /**
     * Returns list of prices for products in AppStore, with localized Price List Title
     *
     * @return bool/mixed returns bool for success/fail and json response to request
     */
    protected function listProductstiers()
    {
        $prodPricesCommon = new ProductsPricesCommon();

        $aTiers = $prodPricesCommon->getProductsTiersByUserCountry($this->moUser);

        if (count($aTiers) == 0) {
            self::sendEmailInternal("PRODUCTTIERS", $this->moUser->id, "Error", "Tiers list could not be generated.");
            $this->sendResponse(500, array('status' => 'Error', 'message' => "Tiers list could not be generated"),
              $this->_format);
            return false;
        }
        $this->sendResponse(200, array(
          "text" => Yii::t('mainApp', 'key_appStore_specialPrices', null, null,
            (isset($this->reqDataDecoded['lang']) ? $this->reqDataDecoded['lang'] : $this->moUser->langEnv)),
          "tiers" => $aTiers
        ), $this->_format);
        return true;
    }

    /**
     * Returns list of availables profiles
     *
     * @return bool/mixed returns bool for success/fail and json response to request
     */
    protected function listUserprofiles()
    {
        $experimentTypeIdentifier = (isset($this->reqDataDecoded['type']) ? $this->reqDataDecoded['type'] : null);
        if (trim($experimentTypeIdentifier) == "") {
            $experimentTypeIdentifier = HeMixed::getGetArgs("type");
        }

        $oCmAbaExperimentsTypes = new CmAbaExperimentsTypes();
        $oCmAbaExperimentsTypes->getExperimentTypeByIdentifier($experimentTypeIdentifier);

        $stAllProfiles = CmUserRules::getOrCreateUserExperimentProfilesWithType($this->moUser,
          (is_numeric($oCmAbaExperimentsTypes->experimentTypeId) ? $oCmAbaExperimentsTypes->experimentTypeId : null));

        $this->sendResponse(200, array(
          "profiles" => $stAllProfiles,
        ), $this->_format);

        return true;
    }

}
