<?php
/**
 * Popups and Display boxes called from views. Most of them called from AJAX.
 */
class ModalsController extends AbaController
{
    public $data = array();

    /** Generic for Yii.
     * @return array
     */
    public function actions()
    {
        return array(
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    public function actionCambioDeNivel()
    {
        $this->renderPartial('cambioDeNivel', array("level" => Yii::app()->getRequest()->getParam('level')));
    }

    public function actionEscuchaGrabaCompara()
    {
        $mail = '';
        $pass = '';
        $user = new AbaUser();
        if($user->getUserById(Yii::app()->user->getId()))
        {
            $mail = $user->email;
            $pass = $user->password;
        }
        $this->renderPartial("escuchaGrabaCompara", array("mail" => $mail, "pass" => $pass));
    }

    public function actionPopupPasateaPremiumCourse()
    {
        $name = Yii::app()->user->getName();
        $this->renderPartial("popupPasateaPremiumCourse", array("name" => $name));
    }

    public function actionPopupCreditCardIssues()
    {
        $name = Yii::app()->user->getName();
        $this->renderPartial("popupCreditCardIssues", array("name" => $name));
    }

    public function actionPopupPasteaPremiumVideoclass()
    {
        $name = Yii::app()->user->getName();
        $this->renderPartial("popupPasteaPremiumVideoclass", array("name" => $name));
    }

    public function actionPopupPasateaPremiumMessages()
    {
        $receiverName = 'no teacher';
        $name = Yii::app()->user->getName();
        $theTeacher = new AbaTeacher( Yii::app()->user->getTeacherId() );
        if(get_class($theTeacher)=='AbaTeacher')
        {
            $receiverName = $theTeacher->email;
            $teacherName = $theTeacher->name;
        }
        $this->renderPartial("popupPasateaPremiumMessages", array("name" => $name, 'receiverName' => $receiverName, 'teacherName' => $teacherName));
    }

    public function actionPopupPaypalProcessing()
    {
        $name = Yii::app()->user->getName();
        $this->renderPartial("popupPaypalProcessing", array("name" => $name));
    }

    public function actionPopupPaypalIssues()
    {
        $name = Yii::app()->user->getName();
        $this->renderPartial("popupPaypalIssues", array("name" => $name));
    }

    public function actionPopupMic()
    {
        $name = Yii::app()->user->getName();
        $this->renderPartial("popupMic", array("name" => $name));
    }

    public function actionPopupErrorDevice()
    {
        $name = Yii::app()->user->getName();
        $this->renderPartial("popupErrorDevice", array("name" => $name));
    }

    public function actionLevelTest()
    {
        Yii::app()->getModule("leveltest");

        ob_start();
        Yii::app()->runController("leveltest/main/startleveltest/demo/1");
        $jsonResponse = ob_get_contents();
        ob_end_clean();

        echo $jsonResponse;

        return true;
    }
}
