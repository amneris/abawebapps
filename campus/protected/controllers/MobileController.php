<?php
/** Main controller for the web-views of mobile
 *
 */
class MobileController extends AbaMobController
{
    /** Yii configuration
     * @return array
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /** Public URL to access mobile landing pages
     *  '/mobile/tryforfree/signature/15645674610546/userid/575757/lang/en/usertype/2'
     * And also used to reply AJAX request.
     *
     * @param null $signature
     * @param null $userId
     * @param null $lang
     * @param null $userType
     * @param int $sendEmail
     * @return bool
     */
    public function actionTryForFree($signature = null, $userId = null, $lang = null, $userType = null, $sendEmail = 0)
    {
        //Light security measure to avoid DOS attacks.
        HeMixed::securePublicUrlRequest("mobile/tryForFree");

        // Being a public URL, we check a field signature:
        if (!$this->isValidSignature($signature, array($userId, $lang, $userType))) {
            $this->redirect($this->createUrl('site/noaccess'));
            return false;
        }

        if($lang=='(null)')
        {
            $lang="de";
            Yii::app()->language = "de";
        }

        // If click on try for free button, then we send email:
        // AJAX CALL received>>>>
        if ($sendEmail > 0) {
            $this->emailSend( $userId );
            return true;
        }

        $this->render('tryForFree', array("signature" => $signature,
            "userId" => $userId,
            "lang" => $lang,
            "userType" => $userType,
            "sendEmail" => $sendEmail));

        return true;

    }

    /** To display confirmation mobile page in order to inform that
     * email has been sent.
     *
     * @return bool
     */
    public function actionTryComplete()
    {
        //Light security measure to avoid DOS attacks.
        HeMixed::securePublicUrlRequest("mobile/trycomplete");

        $this->render('tryForFree', array("complete" => 1));

        return true;
    }

    /**
     * Function used to redirect user to app if installed on iPhone, otherwise redirects user to page referenced by
     * $_SERVER['REQUEST_URI'] (minus controller/action part).
     */
    public function actionRoute()
    {
        $isIOS = HeDetectDevice::isIOS();
        $isAndroid = HeDetectDevice::isAndroid();

        $url = isset($_GET['page']) ? $_GET['page'] : 'campus.abaenglish.com';
        unset($_GET['page']);

        /**
         * WARN: Velociraptor CODE ahead (https://xkcd.com/292/)
         *
         * Acá me he dejado el estilo de lado, lo siento.
         * https://abaenglish.atlassian.net/browse/ABAWEBAPPS-599
         */
        if (!strpos($url, '?')) {
            $url = $url . '?1=1';
        }

        foreach ($_GET as $getParam => $paramValue) {
            $url = $url . '&' . $getParam . '=' . $paramValue;
        }

        if ($isIOS) {
            $this->render('mobileRedirect', array(
              'campusUrl' => 'http://' . $url,
              'metaUrl' => HeComm::getMobileRedirectUrl('IOS_REDIRECT')
            ));
        } elseif ($isAndroid) {
            $this->render('mobileRedirect', array(
              'campusUrl' => 'http://' . $url,
              'metaUrl' => HeComm::getMobileRedirectUrl('ANDROID_DEFAULT_REDIRECT')
            ));
        } else {
            Yii::app()->getRequest()->redirect('//' . $url);
        }
    }

    /**
     * @param null $userId
     *
     * @return bool
     */
    protected function emailSend($userId = null)
    {
        // This returns response for an Ajax call:
        $this->layout = "";

        $moUser = new AbaUser();
        if (!$moUser->getUserById($userId)) {
            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_UNKNOWN_H . ": userId unknown in mobile web page.",
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                " In actionEmailSend a user Id $userId unkown."
            );
            $this->redirect("site/error");
            return false;
        }

        $comSelligent = new  SelligentConnectorCommon();
        if (!$comSelligent->sendOperationToSelligent(
            SelligentConnectorCommon::tryForfFee,
            $moUser,
            null,
            "actionEmailSend"
        )) {
            // In case of error from Selligent or whatever then we'll show Try again in the front side.
            $this->render('errorConnection');
            return false;
        }

        $this->render('emailSend');

        return true;

    }
}