<?php

class ReadytoworkController extends AbaController
{

    public $data = array();

    public function actionIndex()
    {
        $isMobile = HeDetectDevice::isMobile();
        if($isMobile) {
            $this->redirect(array('site/index'));
            return;
        }

        $user = new AbaUser();
        if($user->getUserById(Yii::app()->user->getId()))
        {
            $mail = $user->email;
            $pass = $user->password;
        }
        $this->render('index', array("mail" => $mail, "pass" => $pass));
    }

}
