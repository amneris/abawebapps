<?php

class TermsController extends AbaController
{

    public $data = array();

    /** General conditions associated with a payment. It is displayed to the user in a different blank page.
     * It comes from a link in the /payments/paymentMethod controller/action.
     *
     * @param integer $paySuppExtId
     */
    public function actionIndex($paySuppExtId=null)
    {
        $moUser = new AbaUser();
        $moUser->getUserById(Yii::app()->user->getId());

        $idTextSpecificConditions = "";
        if(isset($paySuppExtId)){
            $payCommSupplier = PaymentFactory::createPayment( NULL, $paySuppExtId);
            $idTextSpecificConditions = $payCommSupplier->getListSpecificConditions();
        }

        $this->render('index', array('paySuppExtId' => $paySuppExtId,
                                    'idTextSpecificConditions' => $idTextSpecificConditions,
                                    'withBreadcrumb' => true)
        );
    }

}