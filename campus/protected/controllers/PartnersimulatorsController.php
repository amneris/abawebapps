<?php
/**
 * This is used for simulations emulating Partner behaviours of web services. Selligent, AllPago, Paypal, etc.
 */
class PartnersimulatorsController extends WsController
{

    public function beforeWebMethod($service)
    {
        return true;
    }

    public function afterWebMethod($service)
    {
        return true;
    }



    /**
     * Simulation of the verifying process from PayPal when we answer them.
     */
    public function actionSimulatePayPalVerification()
    {
        if (is_array($_POST)) {
            echo PAY_PAL_VERIFIED;
            error_log(" Simulador de PayPal verificacion, cmapos recibidos" . implode(", ", $_POST));
        } else {
            echo PAY_PAL_INVALID;
            error_log(" No hay POST ");
        }
    }

    /**
     * Simulator of Listener from Selligent that receives notifications with all POST fields.
     *
     * @param string $oper
     *
     */
    public function actionSimulateSelligent( $oper )
    {
        sleep(2);
        $listFields = "";
        $email = "";
        $operationWithEmail = SelligentConnectorCommon::getAOperSimulateEmail();

        if (is_array($_POST)) {
            $listFields = "";

            switch($oper) {
                /* special cases that don't have MAIL field */
                case 'inviteFriends':
                    foreach ($_POST as $fieldName => $value) {
                        if (strpos($fieldName, 'EMAIL') === 0) {
                            if($email == '') $email = $value;
                            else $email .= ','.$value;
                        }
                        $listFields .= " # " . $fieldName . "=" . $value;
                    }
                    break;
                case 'startPremiumWeek':
                    foreach ($_POST as $fieldName => $value) {
                        if ($fieldName == 'IDUSER_WEB') {
                            $moUser = new AbaUser();
                            $moUser->getUserById($value);

                            $email = $moUser->email;
                        }
                        $listFields .= " # " . $fieldName . "=" . $value;
                    }
                    break;
                default:
                    foreach ($_POST as $fieldName => $value) {
                        if ($fieldName == 'MAIL') {
                            $email = $value;
                        }
                        $listFields .= " # " . $fieldName . "=" . $value;
                    }
                    break;
            }

            error_log(" Simulador de SELLIGENT for operation $oper, POST fields received: ");
            error_log($listFields);
        } else {
            error_log(" There are no POST VALUES in this request");
        }

        if (intval(Yii::app()->config->get("ENABLE_EMAIL_SELLIGENT_SIMULATOR")) == 1) {
            if (array_key_exists($oper, $operationWithEmail)) {
                if ($operationWithEmail[$oper] && !empty($email)) {
                    HeLogger::sendLog(" Simulator of Selligent Emails for operation " . $oper, HeLogger::IT_BUGS,
                      HeLogger::CRITICAL,
                      " In this scenario Selligent would send an email to user $email for operation $oper with " .
                      " fields $listFields");
                }
            }
        }

        echo "OK , Simulation from campus itself";
    }

    /** Simulates AllPAgo payments gateway platform. Expects XML by POST.
     *
     * @param int $ctpe
     * @return string
     */
    public function actionPaymentAllPago($ctpe=1)
    {

        //$data = HeMixed::getPostArgs();
        $data = file_get_contents('php://input');
        $data = substr(urldecode($data), 5);
        error_log(" \n ".$data);
        $oXmlResponse = new SimpleXMLElement( $data );
        error_log("new Simple loaded");
        $aTransactionId = $oXmlResponse->xpath("/Request/Transaction/Identification/TransactionID");
        error_log(" \n -----------------");
        error_log(strval($aTransactionId[0]));

        $transactionId = "";
        if (count($aTransactionId) >= 1) {
            $transactionId = strval($aTransactionId[0]);
        }

        header ("Content-Type:text/xml");
        if ($ctpe == 1) {
            $xmlSuccess = '<?xml version="1.0" encoding="UTF-8"?>
            <Response version="1.0">
                <Transaction mode="INTEGRATOR_TEST" channel="8a829417281b8ee30128299067030cdc" response="SYNC">
                    <Identification>
                        <ShortID>4237.3709.0722</ShortID>
                        <UniqueID>ff80808142d6cfb90142d7e5c1511eb0</UniqueID>
                        <TransactionID>'.$transactionId.'</TransactionID>
                    </Identification>
                    <Payment code="CC.CD">
                        <Clearing>
                            <Amount>39.99</Amount>
                            <Currency>EUR</Currency>
                            <Descriptor>4237.3709.0722 DEFAULT Course Six-monthly ABA English S.L</Descriptor>
                            <FxRate>1.0</FxRate>
                            <FxSource>INTERN</FxSource>
                            <FxDate>2013-12-09 15:07:08</FxDate>
                        </Clearing>
                    </Payment>
                    <Processing code="CC.CD.90.00">
                        <Timestamp>2013-12-09 15:07:08</Timestamp>
                        <Result>ACK</Result>
                        <Status code="90">NEW</Status>
                        <Reason code="00">Successful Processing</Reason>
                        <Return code="000.100.110">Request successfully processed in simulator mode Aba english</Return>
                        <Risk score="0" />
                        <ConnectorDetails />
                    </Processing>
                </Transaction>
            </Response>';
            echo urlencode($xmlSuccess);
        } else {
            $xmlRejected = '<?xml version="1.0" encoding="UTF-8"?>
            <Response version="1.0">
                <Transaction mode="INTEGRATOR_TEST" channel="8a829417281b8ee30128299067030cdc" response="SYNC">
                    <Identification>
                        <ShortID>3778.1785.0530</ShortID>
                        <UniqueID>ff80808142d6cfb90142dc129edb58c6</UniqueID>
                        <TransactionID>'.$transactionId.'</TransactionID>
                    </Identification>
                    <Payment code="DC.DB" />
                    <Processing code="DC.DB.70.10">
                        <Timestamp>2013-12-10 10:34:37</Timestamp>
                        <Result>NOK</Result>
                        <Status code="70">REJECTED_VALIDATION</Status>
                        <Reason code="10">Configuration Validation</Reason>
                        <Return code="600.200.500">Invalid payment data. You are not
                            configured for this currency or sub type (country or brand)</Return>
                        <ConnectorDetails />
                    </Processing>
                </Transaction>
            </Response>';
            echo urlencode($xmlRejected);
        }

    }

}
