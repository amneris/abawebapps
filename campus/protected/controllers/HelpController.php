<?php

class HelpController extends AbaController
{

    public $data = array();

    public function actionIndex()
    {
        //$this->layout = "layout3";
        $this->render('index');
    }

    public function actionPrecios()
    {
        $this->layout = "layout3";
        $this->render('precios');
    }

    public function actionCertificados()
    {
        $this->layout = "layout3";
        $this->render('certificados');
    }

    public function actionPlanes()
    {
        $this->layout = "layout3";
        $this->render('planes');
    }

    public function actionCurso()
    {
        $this->layout = "layout3";
        $this->render('curso');
    }

    public function actionUnidades()
    {
        $this->layout = "layout3";
        $this->render('unidades');
    }

    public function actionGramatica()
    {
        $this->layout = "layout3";
        $this->render('gramatica');
    }

    public function actionTeachers()
    {
        $this->layout = "layout3";
        $this->render('teachers');
    }

    public function actionAyudatecnica()
    {
        //$this->layout = "layout3";
        $this->render('ayudatecnica');
    }

    public function actionMicuenta()
    {
        $this->layout = "layout3";
        $this->render('micuenta');
    }

    public function actionContacto()
    {
        $this->layout = "layout3";
        $this->render('contacto');
    }

}