<?php

/**
 * !! IMPORTANT, DO NOT MODIFY THE PHP DOC COMMENTS ON THE HEADER OF EVERY FUNCTION.
 * IT IS USED BY Yii IN ORDER TO CREATE WSDL.FORBIDDEN!
 */
class WsoapabafeedController extends WsController
{
    static private $version = "1.1";
    static private $aHistoryVersion = array(
      "1.0" => "Creation of listProductsByCountry service",
      "1.1" => "Return one more element periodInMonths."
    );

    /**
     * Overriding method in WsController to stop writing to the log
     * @param CWebService $service
     *
     * @return bool
     */
    public function beforeWebMethod($service)
    {
        return true;
    }

    /**
     * Overriding method in WsController to stop writing to the log
     * @param CWebService $service
     *
     * @return bool
     */
    public function afterWebMethod($service)
    {
        return true;
    }

    public function actions()
    {
        return array(
          'feed' => array(
            'class' => 'AbaWebServiceAction',
          ),
        );
    }

    /** Returns the version of this API. element one is version and second is description
     * @return array The array has format [version,description]
     * @soap
     */
    public function version()
    {
        return $this->retSuccessWs(array(
          "version" => self::$version,
          "description" => self::$aHistoryVersion[self::$version]
        ));
    }


    /* !! IMPORTANT, DO NOT MODIFY THE PHP DOC COMMENTS ON THE HEADER OF EVERY FUNCTION. **************************************************************
       * IT IS USED BY Yii IN ORDER TO CREATE WSDL. FORBIDDEN! *******************************************************************************************
    */


    /** Returns a multiple array with elements: Id Product, Name of the product, Currency Code,Original price,
     * Final Price, Discount percentage, final monthly price, periodicity in months. Items name are productId,
     * productName, currencyCode,originalPrice,finalPrice,discountPercentage,monthlyPrice, periodMonths
     *
     * @param string $signature
     * @param integer $countryId
     * @param string $language
     * @param string $promoCode
     * @param string $scenario
     *
     * @return array
     * @soap
     */
    public function listProductsByCountry($signature, $countryId, $language, $promoCode, $scenario)
    {
        try {
            if (!$this->isValidSignature($signature, array($countryId, $language, $promoCode, $scenario))) {
                return $this->retErrorWs("502");
            }

            //
            //#5739
            $commProdsPrices = new ProductsPricesCommon();
            $scenario = $commProdsPrices->getRandomExperimentVariationId($scenario, $countryId, ExperimentsTypes::EXPERIMENT_TYPE_SCENARIOS_PAYMENTS, $language);

            //
            if (trim($promoCode) == "") {
                $promoCode = null;
            }

            if (array_key_exists($language, Yii::app()->params["languages"])) {
                Yii::app()->setLanguage($language);
            } else {
                Yii::app()->setLanguage("en");
            }
            /*
            Name of the product
            Currency Code
            Original price
            Final Price
            Discount percentage
            Periodicity price
             */
            $aProducts = array();

            //
            //#5739
            $stProdsPrices = array();
            try {
                $stProdsPrices = $commProdsPrices->getAllAvailableUserExperimentsVariationsPrices($scenario, $countryId);
            }
            catch(Exception $e) { }

            /* @var ProductPrice[] $aMoProducts */
            $aMoProducts = $commProdsPrices->getListProductsByCountry($countryId, $language, $promoCode,
              $stProdsPrices);

            foreach ($aMoProducts as $key => $productPrice) {
                if ($productPrice->isPlanPrice != 1) {
                    $discountPercentage = round($productPrice->discount);
                    if ($productPrice->type !== PERCENTAGE) {
                        //($productPrice->getOriginalPrice()*X)/100 = $productPrice->getAmountDiscounted();
                        $discountPercentage = round(($productPrice->getAmountDiscounted() * 100) / $productPrice->priceOfficialCry,
                          2);
                    }
                    $displayCur = $productPrice->getCcyDisplaySymbol();
                    if ($displayCur == 'US $') {
                        $displayCur = '$';
                    }

                    $aProducts[] = array(
                      "productId" => $productPrice->idProduct,
                      "productName" => $productPrice->getPackageTitleKey(),
                      "currencyCode" => $displayCur,
                      "originalPrice" => $productPrice->getOriginalPrice(),
                      "finalPrice" => $productPrice->getFinalPrice(),
                      "discountPercentage" => $discountPercentage,
                      "monthlyPrice" => $productPrice->getPriceMonthly(),
                      "periodMonths" => $productPrice->periodInMonths
                    );
                }
            }

            if (count($aProducts) >= 1) {
                return $this->retSuccessWs(array(
                  "products" => $aProducts,
                  "scenario" => $scenario,
                  "result" => "success"
                ));
            } elseif (empty($aProducts)) {
                return $this->retSuccessWs(array(
                  "products" => $aProducts,
                  "scenario" => $scenario,
                  "result" => "success, no products available for this country."
                ));
            } else {
                return $this->retErrorWs("503", $commProdsPrices->getErrorMessage());
            }
        } catch (Exception $ex) {
            error_log($signature . " error=> " . $ex->getMessage());
            return $this->retErrorWs("555",
              "Internal error non identified. Check with Aba IT Team and see logs Apache for " . $signature);
        }
    }

    /* !! IMPORTANT, DO NOT MODIFY THE PHP DOC COMMENTS ON THE HEADER OF EVERY FUNCTION. **************************************************************
    * IT IS USED BY Yii IN ORDER TO CREATE WSDL. FORBIDDEN! *******************************************************************************************
    */

    /**-----------------------------------------------------------------------------------------------------------------------------**/
    /**-----------------------------------------------------------------------------------------------------------------------------**/
}