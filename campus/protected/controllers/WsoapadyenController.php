<?php
/**
 * IMPORTANT, DO NOT MODIFY THE PHP DOC COMMENTS ON THE HEADER OF EVERY FUNCTION.
 * IT IS USED BY Yii IN ORDER TO CREATE WSDL.FORBIDDEN!
 */
class WsoapadyenController extends WsController
{
   static private $_version = "1.0";

   static private $_aHistoryVersion = array(
      "1.0"=> "Creation of sendNotification",
   );

    public function actions()
    {
        return array(
            'usersapi' => array(
                'class' => 'AbaWebServiceAction',
            ),
        );
    }

    /**
     * Returns the version of this API. element one is [version] and second is [description].
     * @return array The array has format [version,description]
     * @soap
     */
    public function version() {
        return $this->retSuccessWs(array("version" => self::$_version, "description" => self::$_aHistoryVersion[self::$_version]));
    }

    /**
     * @param mixed $request
     * @return array
     * @soap
     */
    public function sendNotification($request)
    {

error_log("::ADYENNOTIFICATION::WsoapadyenController::");

return array("notificationResponse" => "[accepted]");

        if (!$this->isValidHttpAdyenPassword()) {
            return false;
        }

//!ak! - TEST
//        $request = json_decode($request);

        if(isset($request->notification->notificationItems) && count($request->notification->notificationItems) > 0) {

            foreach($request->notification->notificationItems AS $notificationRequestItem) {

                /**
                 * We reccomend you to handle the notifications based on the
                 * eventCode types available, please refer to the integration
                 * manual for a comprehensive list. For debug purposes we also
                 * recommend you to store the notification itself.
                 */

                if(isset($notificationRequestItem->eventCode)) {

                    $abaPaymentsAdyenNotifications = new AbaPaymentsAdyenNotifications();

                    switch(mb_strtoupper($notificationRequestItem->eventCode)){

                        case AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_AUTHORISATION:

//                            $abaPaymentsAdyenNotifications->setNotificationData($notificationRequestItem, false);
                            $abaPaymentsAdyenNotifications->setNotificationData($notificationRequestItem, true);

                            if($abaPaymentsAdyenNotifications->insertPaymentAdyenNotification()) {
                                if(isset($notificationRequestItem->pspReference) AND trim($notificationRequestItem->pspReference) <> '') {
                                    $payGatewayLogAdyenResRec = new PayGatewayLogAdyenResRec();
                                    $payGatewayLogAdyenResRec->getAdyenPaymentByUniqueId($notificationRequestItem->pspReference);
                                    $payGatewayLogAdyenResRec->updateNotifications();
                                }
                            }
                            else {
                                HeLogger::sendLog(HeLogger::PREFIX_ERR_LOGIC_L . ": ADYEN NOTIFICATIONS Listener ABA service received " . AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_AUTHORISATION . " request received with errors",
                                    HeLogger::IT_BUGS_PAYMENTS, HeLogger::INFO,
                                    ", details= " . AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_AUTHORISATION);
                            }

//This field displays the modification operations supported by this payment as a list of strings, this is only populated for
//AUTHORISATION notifications. The operations will inform you whether you need to capture the payment (if you don't have
//auto-capture set up), whether you can cancel the payment (before capture) or if you can refund the payment (after it has
//been captured).

                            // Handle AUTHORISATION notification.
                            // Confirms whether the payment was authorised successfully.
                            // The authorisation is successful if the "success" field has the value true.
                            // In case of an error or a refusal, it will be false and the "reason" field
                            // should be consulted for the cause of the authorisation failure.
                            break;

                        case 'CANCELLATION':

                            $abaPaymentsAdyenNotifications->saveDefaultNotification("CANCELLATION", $notificationRequestItem, true);

                            // Handle CANCELLATION notification.
                            // Confirms that the payment was cancelled successfully.
                            break;

                        case 'REFUND':

                            $abaPaymentsAdyenNotifications->saveDefaultNotification("REFUND", $notificationRequestItem, true);

                            // Handle REFUND notification.
                            // Confirms that the payment was refunded successfully.
                            break;

                        case 'CANCEL_OR_REFUND':

                            $abaPaymentsAdyenNotifications->saveDefaultNotification("CANCEL_OR_REFUND", $notificationRequestItem, true);

                            // Handle CANCEL_OR_REFUND notification.
                            // Confirms that the payment was refunded or cancelled successfully.
                            break;

                        case 'CAPTURE':

                            $abaPaymentsAdyenNotifications->saveDefaultNotification("CAPTURE", $notificationRequestItem, true);

                            // Handle CAPTURE notification.
                            // Confirms that the payment was successfully captured.
                            break;

                        case 'REFUNDED_REVERSED':

                            $abaPaymentsAdyenNotifications->saveDefaultNotification("REFUNDED_REVERSED", $notificationRequestItem, true);

                            // Handle REFUNDED_REVERSED notification.
                            // Tells you that the refund for this payment was successfully reversed.
                            break;

                        case 'CAPTURE_FAILED':

                            $abaPaymentsAdyenNotifications->saveDefaultNotification("CAPTURE_FAILED", $notificationRequestItem, true);

                            // Handle AUTHORISATION notification.
                            // Tells you that the capture on the authorised payment failed.
                            break;

                        case 'REQUEST_FOR_INFORMATION':

                            $abaPaymentsAdyenNotifications->saveDefaultNotification("REQUEST_FOR_INFORMATION", $notificationRequestItem, true);

                            // Handle REQUEST_FOR_INFORMATION notification.
                            // Information requested for this payment .
                            break;

                        case 'NOTIFICATION_OF_CHARGEBACK':

                            $abaPaymentsAdyenNotifications->saveDefaultNotification("NOTIFICATION_OF_CHARGEBACK", $notificationRequestItem, true);

                            // Handle NOTIFICATION_OF_CHARGEBACK notification.
                            // Chargeback is pending, but can still be defended
                            break;

                        case 'CHARGEBACK':

                            $abaPaymentsAdyenNotifications->saveDefaultNotification("CHARGEBACK", $notificationRequestItem, true);

                            // Handle CHARGEBACK notification.
                            // Payment was charged back. This is not sent if a REQUEST_FOR_INFORMATION or
                            // NOTIFICATION_OF_CHARGEBACK notification has already been sent.
                            break;

                        case 'CHARGEBACK_REVERSED':

                            $abaPaymentsAdyenNotifications->saveDefaultNotification("CHARGEBACK_REVERSED", $notificationRequestItem, true);

                            // Handle CHARGEBACK_REVERSED notification.
                            // Chargeback has been reversed (cancelled).
                            break;

                        case AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_REPORT_AVAILABLE:

                            $abaPaymentsAdyenNotifications->saveDefaultNotification(AbaPaymentsAdyenNotifications::ADYEN_NOTIFICATAION_TYPE_REPORT_AVAILABLE, $notificationRequestItem, true);

                            break;

                        default :

                            $abaPaymentsAdyenNotifications->saveDefaultNotification("UNKNOWN - " . $notificationRequestItem->eventCode, $notificationRequestItem, true);

                            HeLogger::sendLog(HeLogger::PREFIX_ERR_LOGIC_L.": ADYEN NOTIFICATIONS Listener ABA service received UNKNOWN request",
                                HeLogger::IT_BUGS_PAYMENTS, HeLogger::INFO,
                                ", details= " . $notificationRequestItem->eventCode);
                            break;
                    }
                }
            }
        }

        return $this->retSuccessWs(array("notificationResponse" => "[accepted]"));
    }

/**-----------------------------------------------------------------------------------------------------------------**/
/**-----------------------------------------------------------------------------------------------------------------**/
/**-----------------------------------------------------------------------------------------------------------------**/
}