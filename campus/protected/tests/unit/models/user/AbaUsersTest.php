<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 24/05/13
 * Time: 15:41
 * To change this template use File | Settings | File Templates.
 */
class AbaUsersTest extends AbaCDbTestCase
{
    public function setUp()
    {
        parent::setUp();

        /* USER TEST:  PREMIUM EXPIRED ---*/
        $testAbaTestCase = new AbaTestCase();
        $testAbaTestCase->createUsersIntoDb("unit.test.expired.premium@yopmail.com", PREMIUM,
            106, "UnitPremiumExpired", "Testing", true);
        //$moUser = new AbaUser();
        //$moUser->getUserByEmail("unit.test.expired.premium@yopmail.com");
        /*$testAbaTestCase->createTestPayment($moUser, PAY_SUCCESS, HeDate::getDateAdd(HeDate::todaySQL(true), 0, -1),
            106, Yii::app()->config->get("ABA_PARTNERID"), Yii::app()->config->get("ABA_PARTNERID"), 30, 'EUR', '106_30');
        */

        /* ---- END USER TEST */
    }

    public function tearDown()
    {
        parent::tearDown();

        $moUser = new AbaUser();
        if ($moUser->getUserByEmail("unit.test.expired.premium@yopmail.com", "", false)) {
            $moUser->deleteUserThroughResetCall();
        }
    }

    /** Tests same name function
     *
     * @return bool
     */
    public function testGetAllUsersPremiumExpired()
    {
        $aSuppliersCards = PaymentFactory::getAllSuppliersByMethod(PAY_METHOD_CARD);
        $moUsers = new AbaUsers(implode(',',$aSuppliersCards));
        $aExpiredUsers = $moUsers->getAllUsersPremiumExpired();
        $this->assertTrue(is_array($aExpiredUsers));
        $this->assertGreaterThanOrEqual(1, count($aExpiredUsers));

        $aEmailsExpired = array();
        foreach ($aExpiredUsers as $aUser) {
            $aEmailsExpired[] = $aUser["email"];
        }

        $this->assertTrue(array_search("unit.test.expired.premium@yopmail.com", $aEmailsExpired, false)!==false,
                                                'User test unit.test.expired.premium@yopmail.com should be expired');

        return true;
    }

}
