<?php
/**
 * Class AbaUsersAddressTest
 *
 *
 */
class AbaUserAddressInvoiceTest extends AbaCDbTestCase
{
    public function setUp()
    {
        parent::setUp();

        /* USER TEST:  PREMIUM EXPIRED ---*/
        $testAbaTestCase = new AbaTestCase();
        $testAbaTestCase->createUsersIntoDb("unit.test.useraddress@yopmail.com", FREE, 199, "Unit", "Testing", true);
        /* ---- END USER TEST */
    }

    public function tearDown()
    {
        parent::tearDown();

        $moUser = new AbaUser();
        if ($moUser->getUserByEmail("unit.test.useraddress@yopmail.com", "", false)) {
            $moUser->deleteUserThroughResetCall();
        }
    }

    /**
     * @test AbaUserAddressInvoice::insertUserAddress, AbaUserAddressInvoice::setUserAddressForm
     *
     * @return bool
     */
    public function testInsertUserAddressByUserId( )
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail("unit.test.useraddress@yopmail.com");

        $userId =$moUser->getId();
        $name = "Unit Test";
        $lastName = "Adress Invoice ";
        $street="c/ Aribau, 240 - 7º - 08006";
        $streetMore = "240 - 7º";
        $zipCode= "08006";
        $city = "Barcelona";
        $state = "BARCELONA";
        $country = "ES";
        $vatNumber = "82154965-G";
        $moUserAddress = new AbaUserAddressInvoice();
        $this->assertFalse($moUserAddress->setUserAddressForm("", $name, $lastName, $vatNumber, $street,$streetMore,
                                                                        $zipCode, $city, $state, $country, false));
        $this->assertTrue($moUserAddress->setUserAddressForm( $userId, $name, $lastName, $vatNumber, $street,
                                                                    $streetMore, $zipCode, $city, $state, $country));
        // Simple assertions after insert:
        $this->assertGreaterThan( 0, $moUserAddress->insertUserAddress());
        $moUserAddress->getUserAddressByUserId( $userId );
        $this->assertEquals( $city, $moUserAddress->city);
        $this->assertEquals( $street, $moUserAddress->street);
        $this->assertEquals( $streetMore, $moUserAddress->streetMore);
        $this->assertEquals( $zipCode, $moUserAddress->zipCode);

        $street="Carrer Bilbao ";
        $streetMore= "76, zona 22@";
        $zipCode = '08005';
        $this->assertTrue($moUserAddress->setUserAddressForm( $userId, $name, $lastName, $vatNumber, $street,
                                                                        $streetMore, $zipCode, $city, $state, $country));
        $this->assertTrue($moUserAddress->update());
        $this->assertTrue( $this->assertGetUserAddressByUserId($userId));

        return true;

    }


    /**
     * Tests AbaUserAddressInvoice::testGetUserAddressByUserId
     *
     * @depends testInsertUserAddressByUserId
     *
     * @param integer $userId
     *
     * @return bool
     */
    protected function assertGetUserAddressByUserId($userId)
    {
        $street="Carrer Bilbao ";
        $streetMore= "76, zona 22@";
        $zipCode = '08005';

        $moUserAddress = new AbaUserAddressInvoice();
        $moUserAddress->getUserAddressByUserId($userId, true);


        $this->assertEquals( "AbaUserAddressInvoice", get_class($moUserAddress));
        $this->assertEquals( $street, $moUserAddress->street, 'Address is not matching.
                                                                Check also insertUserAddress function behaviour.');
        $this->assertEquals( $streetMore, $moUserAddress->streetMore, 'Address 2 is not matching.
                                                                Check also insertUserAddress function behaviour.');
        $this->assertEquals( $zipCode, $moUserAddress->zipCode, 'Zip Code is not matching.
                                                                Check also insertUserAddress function behaviour.');
        return true;
    }

}
