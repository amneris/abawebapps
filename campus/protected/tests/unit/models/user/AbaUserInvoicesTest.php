<?php
/**
 * Created by Joaquim Forcada (Quino)
 * User: Joaquim Forcada (Quino)
 * Date: 14/04/14
 * Time: 16:05
 
 ---Please brief description here---- 
 */

class AbaUserInvoicesTest extends AbaTestCase
{

    public function setUp()
    {
        parent::setUp();

        /* USER TEST:  PREMIUM EXPIRED ---*/
        $this->createUsersExPremium("unit.test.ex.premium.user@yopmail.com", FREE, COUNTRY_FRANCE,
                                                                                            "UnitExPremium", "Testing");
        /* ---- END USER TEST */
    }

    public function tearDown()
    {
        parent::tearDown();

        $moUser = new AbaUser();
        if ($moUser->getUserByEmail("unit.test.ex.premium.user@yopmail.com", "", false)) {
            $moUser->deleteUserThroughResetCall();
        }
    }


    public function testNotSetFromPaymentAddress( )
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail("unit.test.ex.premium.user@yopmail.com");
        $moPayment = new Payment();
        $moPayment->getLastSuccessPaymentNoFree($moUser->id);
        $moUserAddress = new AbaUserAddressInvoice();
        $moUserAddress->getUserAddressByUserId($moUser->id, true);

        $moInvoice = new AbaUserInvoices();
        $moInvoice = $moInvoice->setFromPaymentAddress($moPayment, $moUserAddress);
        $this->assertFalse($moInvoice, 'Should return false because there is no address associated yet.');

        return true;

    }

    /**
     * @test setFromPaymentAddress, buildNumberInvoice, getNextNumInvoiceId
     * @return AbaUserInvoices
     */
    public function testSetFromPaymentAddress()
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail("unit.test.ex.premium.user@yopmail.com");

        $moPayment = new Payment();
        $moPayment->getLastSuccessPaymentNoFree($moUser->id);

        $userId =$moUser->getId();
        Yii::app()->user->setId($userId);
        $name = "Unit Test";
        $lastName = "Adress Invoice";
        $street="Rue del Percebe 13";
        $streetMore = "Ático 2ª";
        $zipCode= "08005";
        $city = "Barcelona";
        $state = "BARCELONA";
        $country = "ES";
        $vatNumber = "82154965-G";
        $moUserAddress = new AbaUserAddressInvoice();
        $this->assertTrue($moUserAddress->setUserAddressForm( $userId, $name, $lastName, $vatNumber, $street,
                                                                    $streetMore, $zipCode, $city, $state, $country));
        $this->assertGreaterThan( 0, $moUserAddress->insertUserAddress());


        $moInvoice = new AbaUserInvoices();
        $moInvoice = $moInvoice->setFromPaymentAddress($moPayment, $moUserAddress);
        $this->assertEquals("AbaUserInvoices", get_class($moInvoice),
                                                    'Should return true because there is an address associated.');
        $this->assertEquals( 1, intval(preg_match("/ABA\-[0-9]{7}/", $moInvoice->numberInvoice )) );
        $this->assertTrue( $this->assertInsertUpdateInvoice($moInvoice) , 'Inserting invoice has failed.');


        return true;
    }

    /**
     *
     * @param AbaUserInvoices $moInvoice
     * @return bool
     */
    protected  function assertInsertUpdateInvoice(AbaUserInvoices $moInvoice)
    {
        $retId = $moInvoice->insertUpdateInvoice();
        $this->assertGreaterThan(1, $retId);

        /* @var AbaUserInvoices $moInvoiceObt */
        $moInvoiceObt = new AbaUserInvoices();
        $moInvoiceObt = $moInvoiceObt->getByPaymentId($moInvoice->idPayment);
        $this->assertEquals( "AbaUserInvoices", get_class($moInvoiceObt));

        $this->assertGreaterThanOrEqual(FIRST_INVOICE, $moInvoiceObt->numInvoiceId,
                                                        'The invoice number is under FIRST INVOICE DEFAULT');

        $moProduct = new ProductPrice();
        $moProduct->getProductById("30_30", 0);
        $textProduct = Yii::t('mainApp', $moProduct->descriptionText, array(), NULL, LANG_CAMPUS_DEFAULT);
        $this->assertContains($textProduct, $moInvoiceObt->productDesc );

        return true;
    }


}
