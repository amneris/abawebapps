<?php
/**
 * Created by Joaquim Forcada (Quino) in Aba English with PHP Storm
 * User: Joaquim Forcada (Quino)
 * Date: 4/09/14
 * Time: 12:34
 
 ---Please brief description here---- 
 */

class AbaUserTest extends AbaCDbTestCase
{
    public function setUp()
    {
        parent::setUp();
        /* USER TEST:  PREMIUM EXPIRED ---*/
        $testAbaTestCase = new AbaTestCase();
        $testAbaTestCase->createUsersExPremium("unit.test.ex.premium.user@yopmail.com", FREE, COUNTRY_FRANCE,
            "UnitExPremium", "Testing");
        /* ---- END USER TEST */


    }

    public function tearDown()
    {
        parent::tearDown();

        $moUser = new AbaUser();
        if ($moUser->getUserByEmail("unit.test.ex.premium.user@yopmail.com", "", false)) {
            $moUser->deleteUserThroughResetCall();
        }

    }

    /**
     *  Tests function with the same name
     */
    public function testGetDaysSinceRegistration()
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail("unit.test.ex.premium.user@yopmail.com");
        $days = $moUser->getDaysSinceRegistration();

        $this->assertGreaterThan(100 /*>3 motnhs*/, $days);

        return true;
    }

}
 