<?php
/**
 * Created by Joaquim Forcada (Quino) in Aba English with PHP Storm
 * User: Joaquim Forcada (Quino)
 * Date: 8/11/13
 * Time: 12:31
 
 ---Please brief description here---- 
 */

class AbaFollowUpHtml4Test extends AbaCDbTestCase
{

    private $numUsers = 20;

    /**
     *
     */
    public function setUp()
    {

        parent::setUp();

        $testAbaTestCase = new AbaTestCase();
        $i = 0;
        for ($i = 1; $i <= $this->numUsers; $i++) {
            /* USER TEST:  PREMIUM ---*/
            $testAbaTestCase->createUsersIntoDb("unit.test.premiumStressI2_" . $i . "@yopmail.com", PREMIUM,
                106, "Unit Premium", "Testing", true);
            /* ---- END USER TEST */
        }
    }

    /**
     *
     */
    public function tearDown()
    {
        parent::tearDown();

        for ($i = 1; $i <= $this->numUsers; $i++) {
            $moUser = new AbaUser();
            if ($moUser->getUserByEmail("unit.test.premiumStressI2_" . $i . "@yopmail.com", "", false)) {
                $moUser->deleteUserThroughResetCall();
            }
        }
    }

    /** Test a simple function, just in order to check if table exist and is accessible.
     * @test AbaFollowUpHtml4::getLastId
     *
     * @return bool
     */
    public function testGetLastId()
    {
        $moAbaFollowUpHtml4 = new AbaFollowUpHtml4();
        $idRet = $moAbaFollowUpHtml4->getLastId();

        $this->assertGreaterThan(15000, $idRet);

        return true;
    }

    /** Excluded for general testing execution. It's a manual execution to check stress on table.
     *
     * @group STRESS
     *
     */
    public function exclude_testStressFullTable()
    {
        for ($i = 1; $i <= $this->numUsers; $i++) {
            $moUser = new AbaUser();
            $this->assertTrue( $moUser->getUserByEmail("unit.test.premiumStressI2_".$i."@yopmail.com") );
            echo " \n ".HeDate::todaySQL(true)." User "."unit.test.premiumStressI2_".$i."@yopmail.com ----------------- ";
            $moAbaFollowUpHtml = new AbaFollowUpHtml4();
            // Number of units to test stress with:
            $aUnits = array(3, 5, 10 , 45, 47, 48 ,49, 50, 51, 52, 60, 62,
                            70, 72, 75, 76, 87, 90 , 91 , 92,
                            100, 130, 131, 133, 134, 140
                            );
            $timeDiff = array();
            foreach ($aUnits as $unitId){
                $stressFollowUpHtml = new stressFollowUpHtml( $moUser->id, $unitId);
                $timeStart = microtime(true);
                $aSqls = explode(";", $stressFollowUpHtml->sqls);
                foreach($aSqls as $sql){
                    $startSql = " ".strtoupper(substr($sql, 0, 30));
                    if( strpos($startSql,"SELECT")>=1 ){
                        $dataReader = $moAbaFollowUpHtml->querySQL($sql);
                       if ( $dataReader->read()==false ) {
                          // echo "  \n Failing on ".$moUser->id." and unit ".$unitId." = ".$sql;
                       }
                    }
                    if( strpos($startSql,"INSERT")>=1 ){
                        if (!$moAbaFollowUpHtml->executeSQL($sql)) {
                           echo "  \n Failing on ".$moUser->id." and unit ".$unitId." = ".$sql;
                        }
                    }
                    if( strpos($startSql,"UPDATE")>=1 ){
                        if (!$moAbaFollowUpHtml->updateSQL($sql)) {
                            echo "  \n Failing on ".$moUser->id." and unit ".$unitId." = ".$sql;
                        }
                    }
                }
                $timeEnd = microtime(true);
                $tmpDiff = $timeEnd - $timeStart;
                echo " \n Time for execution of queries for user $i has last: ".$tmpDiff;
                $timeDiff[] = $tmpDiff;

                $stressFollowUpHtml = NULL;
            }
        }
        $timeAvgxUser = array_sum($timeDiff)/count($timeDiff);
        echo " \n Average time for each user= ".$timeAvgxUser;

        return true;
    }


}
