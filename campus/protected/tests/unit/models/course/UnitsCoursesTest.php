<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 24/05/13
 * Time: 15:41
 * To change this template use File | Settings | File Templates.
 */
class UnitsCoursesTest extends AbaCDbTestCase
{

    /**
     * @test
     *
     * @return bool
     */
    public function testGetArrayStartUnits()
    {
        $moUnits = new UnitsCourses();
        $aUnits = $moUnits->getArrayStartUnits( 1, 6);

        $this->assertEquals( array( 1=>1, 2=>25, 3=>49, 4=>73, 5=>97, 6=>121 ), $aUnits );


        $aUnits = $moUnits->getArrayStartUnits( 1, 4);

        $this->assertEquals( array( 1=>1, 2=>25, 3=>49, 4=>73 ), $aUnits );

        return true;
    }
}
