<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 24/05/13
 * Time: 15:41
 * To change this template use File | Settings | File Templates.
 */
class AbaFollowUpTest extends AbaCDbTestCase
{

    /** Test same function name
     *
     * @dataProvider dataUsersFollowUp
     */
    public function testGetMaxFollowUp( $userId )
    {
        $moAbaFollowUp = new AbaFollowup();
        $maxValue = $moAbaFollowUp->getMaxFollowUp( $userId, array( 1=>1, 2=>25, 3=>49, 4=>73, 5=>97, 6=>121 ) );

        $this->assertGreaterThanOrEqual( 0, $maxValue);

        return true;
    }


    /** Test same function name
     *
     * @dataProvider dataUsersFollowUpSummary
     */
    public function testFollowupSummary(
        $progressVersion, $USERID, $ABCDEF
    ) {

        $AbaFollowUpSummary = new AbaFollowUpSummary();

        // COPY FROM FOLLOW UP 4 !!!
        // COPY FROM FOLLOW UP 4 !!!
        // COPY FROM FOLLOW UP 4 !!!
//
//        $AbaFollowUpSummary->progressVersion = 30;
//        $AbaFollowUpSummary->idFollowup = 1;
//        $AbaFollowUpSummary->themeid = 1;
//        $AbaFollowUpSummary->userid = Yii::app()->user->getId();
//        $AbaFollowUpSummary->lastchange = HeDate::todaySQL();
//        $AbaFollowUpSummary->all_por = 0;
//        $AbaFollowUpSummary->all_err = 0;
//        $AbaFollowUpSummary->all_ans = 0;
//        $AbaFollowUpSummary->all_time = 0;
//        $AbaFollowUpSummary->sit_por = 2;
//        $AbaFollowUpSummary->stu_por = 2;
//        $AbaFollowUpSummary->dic_por = 0;
//        $AbaFollowUpSummary->dic_ans = 0;
//        $AbaFollowUpSummary->dic_err = 5;
//        $AbaFollowUpSummary->rol_por = 5;
//        $AbaFollowUpSummary->gra_por = 0;
//        $AbaFollowUpSummary->gra_ans = 0;
//        $AbaFollowUpSummary->gra_err = 7;
//        $AbaFollowUpSummary->wri_por = 7;
//        $AbaFollowUpSummary->wri_ans = 7;
//        $AbaFollowUpSummary->wri_err = 0;
//        $AbaFollowUpSummary->new_por = 0;
//        $AbaFollowUpSummary->spe_por = 0;
//        $AbaFollowUpSummary->spe_ans = 0;
//        $AbaFollowUpSummary->time_aux = 0;
//        $AbaFollowUpSummary->gra_vid = 0;
//        $AbaFollowUpSummary->rol_on = 0;
//        $AbaFollowUpSummary->exercises = 'A,B,C,D,E,F';
//        $AbaFollowUpSummary->eva_por = 0;

        $AbaFollowUpSummary->saveFollowup4summary();


        return true;
    }



    /**
     * @return array
     */
    public function dataUsersFollowUp()
    {
        return array(
                array(779),
                array(724856),
                array(725366),
            );
    }


    public function dataUsersFollowUpSummary()
    {
        return array(
            array(30, 'userid', 'A,B,C,D,E,F'),
        );
    }
}
