<?php
/**
 * Tests a model class PayGatewayLogPayPal that extends AbaActiveRecord
 * @covers PayGatewayLogPayPal
 */
class PayGatewayLogPayPalTest extends AbaCDbTestCase
{

    /** Test simply that the table and model of Yii is working for this "non existent" table
     * @return bool
     */
    public function testConstruct( )
    {
        $payLog = new PayGatewayLogPayPal( 58 );

        $this->assertEquals( 58 , $payLog->getPaySuppExtId() );

        $payLog->setOrderNumber( 'usernamePP=test@yopmail.com,product=199_300' );

        $this->assertEquals( 'usernamePP=test@yopmail.com,product=199_300', $payLog->getOrderNumber() );

        return true;

    }


}
