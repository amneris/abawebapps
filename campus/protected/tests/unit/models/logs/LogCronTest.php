<?php
/**
 * Tests a model class LogCron that extends AbaActiveRecord
 * @covers LogCron
 */
class LogCronTest extends AbaCDbTestCase
{

    /**
     *
     * @dataProvider dataLogExample
     */
    public function testGetLogById( $cronName, $ipFromRequest)
    {
        return true;
    }


    /** Fixture for testing usage of table
     * @return array
     */
    public function dataLogExample()
    {
        return array(
            /* NEW */
            array('test.unit.logcron','127.0.0.1'),
            array('test.unit.logcron2','127.0.0.1'),
        );
    }

}
