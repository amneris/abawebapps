<?php
/**
 * Tests a model class LogSentSelligent that extends AbaActiveRecord
 * @covers LogSentSelligent
 */
class LogSentSelligentTest extends AbaCDbTestCase
{

    public function testGetLogById(  )
    {
        $moLogSelligent = new LogSentSelligent();
        $logId = $moLogSelligent->insertLog("http://abaenglish.emsecure.net/optiext/optiextension.dll?ID=HpIIJkdeR8ZdWhKw7GeMKU1htKfvUH__WXFPrF",
                "login", 'a:2:{s:10:"IDUSER_WEB";s:5:"90150";s:17:"FLAG_CAMPUS_LOGIN";s:2:"no";}',
                'OK, OK,bcoz disabled by config', 76444, 'unit test', '');

        $this->assertInternalType('string', $logId);
        $this->assertLessThan( intval($logId), 1);

        $moLogSelligent = new LogSentSelligent();
        $moLogSelligent = $moLogSelligent->getLogById($logId);

        $this->assertEquals('LogSentSelligent', get_class($moLogSelligent) );
        $this->assertEquals('unit test', $moLogSelligent->sourceAction );

        $moLogSelligent->delete();

        return true;
    }



}
