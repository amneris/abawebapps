<?php
/**
 * Tests a model class LogUserLevelChange that extends AbaActiveRecord
 * @covers LogUserLevelChange
 */
class LogUserLevelChangeTest extends AbaCDbTestCase
{
    private $idTest;

    /**
     * @covers LogUserLevelChange::getLogByUserId
     * @dataProvider dataLogExample
     */
    public function testNotGetLogByUserId($userId, $dateModified, $oldUserLevel, $newUserLevel, $sourceAction)
    {
        $moLogUserLevelChange = new LogUserLevelChange();
        $this->assertFalse($moLogUserLevelChange->getLogByUserId($userId));

        $this->assertFalse($moLogUserLevelChange->dateModified == $dateModified);
        $this->assertFalse($moLogUserLevelChange->oldUserLevel == $oldUserLevel);
        $this->assertFalse($moLogUserLevelChange->newUserLevel == $newUserLevel);

        return true;

    }

    /** Tests a really simple method. Checks Magic properties creation.
     * @covers LogUserLevelChange::setUserChangeLevel
     * @dataProvider dataLogExample
     */
    public function testSetUserChangeLevel( $userId, $dateModified, $oldUserLevel, $newUserLevel, $sourceAction )
    {

        $moLogUserLevelChange = new LogUserLevelChange();
        $moLogUserLevelChange->setUserChangeLevel($userId, $oldUserLevel, $newUserLevel, $sourceAction);

        $this->assertEquals($moLogUserLevelChange->userId, $userId);
        $this->assertEquals($moLogUserLevelChange->oldUserLevel, $oldUserLevel);
        $this->assertEquals($moLogUserLevelChange->newUserLevel, $newUserLevel);
        $this->assertEquals($moLogUserLevelChange->sourceAction, $sourceAction);

        return true;
    }

    /**
     * @covers LogUserLevelChange::insertLog
     * @dataProvider dataLogExample
     */
    public function testInsertLog($userId, $dateModified, $oldUserLevel, $newUserLevel, $sourceAction, $delete)
    {
        $moLogUserLevelChange = new LogUserLevelChange();
        $moLogUserLevelChange->userId = $userId;
        $moLogUserLevelChange->dateModified = $dateModified;
        $moLogUserLevelChange->oldUserLevel = $oldUserLevel;
        $moLogUserLevelChange->newUserLevel = $newUserLevel;
        $moLogUserLevelChange->sourceAction = $sourceAction;

        $this->assertGreaterThanOrEqual(1, $moLogUserLevelChange->insertLog());
        $this->idTest = $moLogUserLevelChange->id;

        if ($delete) {
            $this->assertEquals(1, $moLogUserLevelChange->delete());
        }

        return $this->idTest;
    }


    /**
     * @covers LogUserLevelChange::getLogByUserId
     * @dataProvider dataLogExample
     */
    public function testGetLogByUserId($userId, $dateModified, $oldUserLevel, $newUserLevel, $sourceAction, $delete)
    {
        $moLogUserLevelChange = new LogUserLevelChange();

        $this->testInsertLog($userId, $dateModified, $oldUserLevel, $newUserLevel, $sourceAction, false);

        $this->assertTrue($moLogUserLevelChange->getLogByUserId($userId));

        $this->assertTrue(HeDate::validDateSQL( $moLogUserLevelChange->dateModified, true), 'Is not a valid SQL date='.$moLogUserLevelChange->dateModified );
        $this->assertTrue($moLogUserLevelChange->oldUserLevel == $oldUserLevel);
        $this->assertTrue($moLogUserLevelChange->newUserLevel == $newUserLevel);

        $this->assertEquals(1, $moLogUserLevelChange->delete());

        return true;

    }

    /**
     * @covers LogUserLevelChange::getLogById
     * @dataProvider dataLogExample
     */
    public function testGetLogById($userId, $dateModified, $oldUserLevel, $newUserLevel, $sourceAction, $delete)
    {
        $moLogUserLevelChange = new LogUserLevelChange();
        $this->testInsertLog($userId, $dateModified, $oldUserLevel, $newUserLevel, $sourceAction, false);

        $ret = $moLogUserLevelChange->getLogById($this->idTest);
        $this->assertEquals("LogUserLevelChange", get_class($ret), 'Type expected is LogUserLevelChange' );

        $this->assertEquals(1, $moLogUserLevelChange->delete());

        return true;
    }


    /** Fixture for testing usage of table
     * @return array
     */
    public function dataLogExample()
    {
        return array(
            /* NEW */
            array(rand(1, 90000), HeDate::todaySQL(true), rand(1, 5), rand(2, 6), 'test', true),
        );
    }

}
