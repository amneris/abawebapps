<?php
/**
 * Tests a model class PayPalLogIpn that extends AbaActiveRecord
 * @covers PayPalLogIpn
 */
class PayPalLogIpnTest extends AbaCDbTestCase
{

    /** tests simply creation and deletion of a log.
     *
     * @return bool
     */
    public function testGetLogById( )
    {
        $moIpnLog = new PayPalLogIpn();
        // `dateCreation`, l.`header`, l.`postFields`, l.`transactionType`, l.`paymentStatus
        $moIpnLog->dateCreation = HeDate::todaySQL(true);
        $moIpnLog->header = " HTTP URI=/wspostaba/paypal # https://unit.test.abaenglish.com ";
        $strPostValues = " ; payment_cycle=Monthly ; txn_type=recurring_payment_profile_created ;
        last_name=Barahona ; next_payment_date=03:00:00 Mar 31, 2013 PDT ;
        residence_country=ES ; initial_payment_amount=0.00 ;
        rp_invoice_id=usernamePP=homer.simpson.english@yopmail.com&producto=199_30 ; currency_code=EUR ;
        time_created=00:33:50 Jan 30, 2013 PST ; verify_sign=AmKT3kSchYOWJBKoZel9T0oW4ZiDAjyAbMYVdI8KTTWqpo7l-fyDdFMn ;
        period_type= Regular ; payer_status=unverified ; tax=0.00 ; payer_email=mbarahona@abaenglish.com ;
        first_name=Miguel ; receiver_email=payments@abaenglish.com ; payer_id=76C5MYUC6CMYN ; product_type=1 ;
        shipping=0.00 ; amount_per_cycle=14.99 ; profile_status=Active ; charset=windows-1252 ; notify_version=3.7 ;
        amount=14.99 ; outstanding_balance=0.00 ; recurring_payment_id=I-NBLUHNF9SGPC ;
        product_name=English Course of ABA English ; ipn_track_id=b171f884470eb";
        $moIpnLog->postFields = $strPostValues;
        $moIpnLog->transactionType = "recurring_payment_profile_created";
        $moIpnLog->paymentStatus = "Completed";

        $logIPNId = $moIpnLog->insertLog();

        $this->assertInternalType('string',$logIPNId);
        $this->assertLessThan( intval($logIPNId), 1);

        $moIpnLog = new PayPalLogIpn();
        $moIpnLog = $moIpnLog->findByPk($logIPNId);

        $this->assertNotEmpty($moIpnLog, 'Log should be found in database table PayPalLogIpn, but it is not.');

        $moIpnLog->delete();

        return true;
    }

}
