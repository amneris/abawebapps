<?php
/**
 * Tests a model class AbaLogUserActivity that extends AbaActiveRecord
 * @covers AbaLogUserActivity
 */
class AbaLogUserActivityTest extends AbaCDbTestCase
{
    /** Simple insertion test on database
     *
     * @test
     *
     * @return bool
     */
    public function testInsertLog(  )
    {
        $testAbaTestCase = new AbaTestCase();
        $testAbaTestCase->createUsersIntoDb("unit.test.log.user@yopmail.com", PREMIUM,
            106, "UnitTestLogUserActivity", "Testing", true);

        $moUser = new AbaUser();
        $moUser->getUserByEmail("unit.test.log.user@yopmail.com", "", false);

        $moLogUser = new AbaLogUserActivity( $moUser );
        $this->assertTrue($moLogUser->insertLog("user", ", [birthDate] changed from 23-10-1979 to 24-10-1985",
                                "Saved user UNIT TEST", "Form User Objectives"));
        $logId = $moLogUser->id ;
        $this->assertInternalType('string', $logId);
        $this->assertLessThan( intval($logId), 1);

        $moLogUser = new AbaLogUserActivity( $moUser );
        $moLogUser = $moLogUser->findByPk( $logId );

        $this->assertEquals('AbaLogUserActivity', get_class($moLogUser) );
        $this->assertEquals('Saved user UNIT TEST', $moLogUser->description );

        if ($moUser->getUserByEmail("unit.test.log.user@yopmail.com", "", false)) {
            $moUser->deleteUserThroughResetCall();
        }

        return true;
    }

    /**
     * @test AbaLogUserActivity::getLastIp
     *
     * @return bool
     */
    public function testGetLastIp()
    {
        $testAbaTestCase = new AbaTestCase();
        $testAbaTestCase->createUsersIntoDb("unit.test.log.user@yopmail.com", PREMIUM,
            73, "UnitTestLogUserActivity", "Testing", true);

        $moUser = new AbaUser();
        $this->assertTrue($moUser->getUserByEmail("unit.test.log.user@yopmail.com", "", false));

        $moLogUser = $moUser->abaUserLogUserActivity;
        $this->assertEquals('AbaLogUserActivity', get_class($moLogUser) );
        $this->assertTrue($moLogUser->insertLog("user", ", dummy test from testGetLastIp",
                                                    "Dummy test", "Connection from Unit Test"));

        $logId = $moLogUser->id ;
        $this->assertInternalType('string', $logId);

        $lastIp = $moLogUser->getLastIp();
        $this->assertInternalType('string', $lastIp);
        $this->assertGreaterThan(0, preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\z/', $lastIp));
        $this->assertInternalType('integer', ip2long($lastIp));


        if ($moUser->getUserByEmail("unit.test.log.user@yopmail.com", "", false)) {
            $moUser->deleteUserThroughResetCall();
        }

        return true;
    }


}
