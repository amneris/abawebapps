<?php
/**
 * Tests a model class SourcesList that extends AbaActiveRecord
 * @covers SourcesList
 */
class SourceListTest extends AbaCDbTestCase
{

    /** tests simply creation and deletion of a log.
     *
     * @return bool
     */
    public function testGetSourceById( )
    {
        $moSourceList = new SourcesList();
        $moSourceList->getSourceById('1');
        $this->assertEquals('SourcesList', get_class($moSourceList),
            'In the table source_list should be at least one record' );
        $this->assertEquals( 'ser-nobr-nogratis', $moSourceList->name, 'The name of source list 1 should be -Search Brand-' );
        // Negative testing:
        $this->assertFalse($moSourceList->getSourceById('564654643'), 'It should not find any Record.');


        return true;
    }

}
