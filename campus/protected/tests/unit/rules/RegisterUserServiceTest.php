<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 5/04/13
 * Time: 17:57
 * To change this template use File | Settings | File Templates.
 */
class RegisterUserServiceTest extends AbaTestCase
{
    public function tearDown()
    {
        parent::tearDown();

        $moUser = new AbaUser();
        if($moUser->getUserByEmail("unit.test.register.user.level.new@yopmail.com","",false))
        {
            $moUser->deleteUserThroughResetCall();
        }

        $moUser = new AbaUser();
        if($moUser->getUserByEmail("unit.test.register.user.new@yopmail.com","",false))
        {
            $moUser->deleteUserThroughResetCall();
        }

        $moUser = new AbaUser();
        if($moUser->getUserByEmail("test.fabcebook.user.1@facebookuserabatest.com")) {
            $moUser->deleteUserThroughResetCall();
        }

        $moUser = new AbaUser();
        if($moUser->getUserByEmail("test.fabcebook.user.2@facebookuserabatest.com")) {
            $moUser->deleteUserThroughResetCall();
        }
    }


    /** Tests the subject function with the same name
     * @dataProvider dataUsersRegistrationForUserLevelTest
     */
    public function testRegisterUserFromLevelTest( $email, $lang, $currentLevel, $insideCampus, $countryId, $idPartner)
    {
        $serviceRegister = new RegisterUserService();

        /* @var AbaUser $moUser */
        $moUser = $serviceRegister->registerUserFromLevelTest( $email, $lang, $currentLevel, $insideCampus, $countryId, $idPartner);

        $this->assertInstanceOf('AbaUser', $moUser);
        $this->assertEquals($currentLevel,$moUser->currentLevel, "Looks like current Level has not been updated for test on $email");
        $this->assertEquals($countryId, $moUser->countryId, "Looks like country has not been updated for test on $email");
        $this->assertObjectHasAttribute("isNewUser",$moUser);

        $this->assertEquals($idPartner, $moUser->idPartnerSource );
        //$this->assertEquals($idPartner, $moUser->idPartnerCurrent, 'Id partner is expected to be '.$idPartner );
        $this->assertEquals(REGISTER_USER_FROM_LEVELTEST, $moUser->registerUserSource );
        $this->assertEmpty($moUser->idPartnerFirstPay, 'Id Partner First Pay should be empty.');


    }

    /** Tests the subject function with the same name but in a negative way
     * @dataProvider dataUsersNotRegistrationForUserLevelTest
     */
    public function testNotRegisterUserFromLevelTest( $email, $lang, $currentLevel, $insideCampus,
                                                                                        $countryId, $idPartner)
    {
        $serviceRegister = new RegisterUserService();
        $moUserBeforeReg = new AbaUser();
        $moUserBeforeReg->getUserByEmail($email);


        /* @var AbaUser $moUser */
        $moUser = $serviceRegister->registerUserFromLevelTest( $email, $lang, $currentLevel, $insideCampus,
                                                                                        $countryId, $idPartner);

        $this->assertInstanceOf('AbaUser', $moUser);
        $this->assertObjectHasAttribute("isNewUser",$moUser);
        if($moUser->isNewUser==1)
        {
            $this->assertEquals($lang, $moUser->langCourse);
            $this->assertEquals($lang, $moUser->langEnv);
            $this->assertEquals($currentLevel, $moUser->currentLevel);
            $this->assertEquals($countryId, $moUser->countryIdCustom);

            /* Rules for Partnership control */
            $this->assertEquals($idPartner, $moUser->idPartnerSource);
            $this->assertEquals($idPartner, $moUser->idPartnerCurrent );
            $this->assertEquals(REGISTER_USER_FROM_LEVELTEST, $moUser->registerUserSource );
            $this->assertEquals('', $moUser->idPartnerFirstPay);
            /* --------------------------- */
            $this->assertEquals(FREE, $moUser->userType );
            $this->assertGreaterThanOrEqual(1, $moUser->teacherId );
            $this->assertEquals(null, $moUser->deviceTypeSource, 'Device should be null for
                                                        backwards compatiblilty');

            $moUser->deleteUserThroughResetCall();

        }
        else
        {
            $this->assertEquals("0", $moUser->isNewUser);
            if($moUserBeforeReg->currentLevel<>$currentLevel){
                $this->assertNotEquals($currentLevel, $moUser->currentLevel,
                    "Looks like current Level has been updated for test on $email, and it should not be.");
            }
            if($moUserBeforeReg->countryId<>$countryId){
                $this->assertNotEquals($countryId, $moUser->countryId,
                    "Looks like country has been updated for test on $email , and it should not be.");
            }
            if($moUserBeforeReg->idPartnerSource <> $idPartner){
                $this->assertNotEquals($idPartner, $moUser->idPartnerSource,
                    "Looks like Partner has been updated for test on $email , and it should not be.");
                $this->assertNotEquals($idPartner, $moUser->idPartnerCurrent );
            }
        }
    }

    /** Tests the subject function with the same name
     * @dataProvider dataUsersRegistrationForUser
     */
    public function testRegisterUser($email, $password, $lang, $countryId, $name, $lastName, $level,
                                     $partnerId, $deviceTypeSource)
    {
        $serviceRegister = new RegisterUserService();
        /* @var AbaUser $moUser */
        $moUser = $serviceRegister->registerUser($email, $password, $lang, $countryId, $name, $lastName,
            $level, $partnerId, 45, $deviceTypeSource);

        $this->assertNull($serviceRegister->getLastError(), "Register user on ".$email);

        $this->assertInstanceOf('AbaUser', $moUser);
        $moUser->refresh();
        // Checks values of certain sensitive fields, basically rules of registration:
        $this->assertEquals($level, $moUser->currentLevel );
        $this->assertEquals($lastName, $moUser->surnames );
        $this->assertGreaterThanOrEqual(1, $moUser->teacherId );
        /* Rules for Partnership control */
        $this->assertEquals($partnerId, $moUser->idPartnerSource );
        /*$this->assertEquals($partnerId, $moUser->idPartnerCurrent, 'Partner Id is expected to be '.$partnerId.
                                                                                    'bcoz registration source' );*/
        $this->assertEquals(REGISTER_USER, $moUser->registerUserSource );
        $this->assertEmpty($moUser->idPartnerFirstPay, 'Id Partner First Pay should be empty.');
        $this->assertEquals($deviceTypeSource, $moUser->deviceTypeSource, 'User should be considered as coming
                                                                                    from a device from PC');

        /* --------------------------- */

    }

    /** Tests the subject function with the same name in a NEGATIVE way
     * @dataProvider dataUsersNotRegistrationForUser
     */
    public function testNotRegisterUser($email, $password, $lang, $countryId, $name, $lastName, $level, $partnerId)
    {
        $serviceRegister = new RegisterUserService();

        /* @var AbaUser $moUser */
        $moUser = $serviceRegister->registerUser($email, $password, $lang, $countryId,
                                    $name, $lastName, $level, $partnerId);

        $this->assertNotNull( $serviceRegister->getLastError() );
        $this->assertFalse( $moUser );

        $moUser = new AbaUser();
        $this->assertTrue($moUser->getUserByEmail($email),
                                    'User must exist in database if it has not been allowed to be registered');
        $this->assertGreaterThanOrEqual(FREE, intval($moUser->userType),
                                    'User must be FREE or PREMIUM if it has not been allowed to be registered');


        return true;
    }

    /**
     *  Tests that when a voucher is validated already then it returns a customized 514 error.
     * @test
     *
     */
    public function testFailUsedRegisterUserPartner()
    {
        // Gu96487m12t
        $serviceRegister = new RegisterUserService();

        $email = "unit.test.partner.".rand(0,10000)."@yopmail.com";

        /* @var AbaUser $moUser */
        $moUser = $serviceRegister->registerUserPartner($email, '123456789', "en", "Unit", "Test", "999999999",
            'Gu96487m12t', 'SECURITEE_Gu96487m12t', 21, 3, 199);

        $this->assertFalse($moUser);

        $moUser = new AbaUser();
        $this->assertFalse($moUser->getUserByEmail($email,"",false), 'The user should not exist, not been registered');


        $this->assertEquals( 514, $serviceRegister->getErrorCode(),
            ' The error code should be that the voucher has been used and has been = '.$serviceRegister->getLastError());

        return true;
    }

    /** Tests the subject function with the same name in a very simple way.
     * Checks all the creation of the payment line for a Partner
     *
     */
    public function testRegisterUserPartner()
    {
        $serviceRegister = new RegisterUserService();

        $email = "unit.test.partner.".rand(0,10000)."@yopmail.com";

        /* @var AbaUser $moUser */
        $moUser = $serviceRegister->registerUserPartner($email, '123456789', "en", "Unit", "Test", "999999999",
                                        'Gu10010m696t', 'SECURITEE_Gu10010m696t', 21, 3, 199, DEVICE_SOURCE_TABLET);
        $this->assertInstanceOf('AbaUser', $moUser);
        $this->assertEquals( PREMIUM, $moUser->userType );
        $this->assertEquals( 21, $moUser->idPartnerFirstPay );
        $this->assertEquals( 21, $moUser->idPartnerSource );
        $this->assertEquals( 21, $moUser->idPartnerCurrent );
        $this->assertEquals( DEVICE_SOURCE_TABLET, $moUser->deviceTypeSource, 'User should be considered as coming
                                                                                    from a device from PC');

        // Let's assert the payment created
        $moPayUser = new Payment();
        $this->assertInstanceOf("Payment", $moPayUser->getLastPaymentByUserId($moUser->id, true),
                            'There would be at least a payment for this user');
        $this->assertEquals( 21, $moPayUser->idPartner);
        $this->assertEmpty( $moPayUser->idPartnerPrePay);
        $this->assertGreaterThan( 1.00, $moPayUser->amountOriginal);
        $this->assertEquals( 0.00, $moPayUser->amountPrice);
        $this->assertEquals( 'Gu10010m696t', $moPayUser->paySuppOrderId);
        $this->assertEquals( 90, $moPayUser->idPeriodPay);

        $moUser->refresh();
        $this->assertTrue($moUser->deleteUserThroughResetCall());
    }


    /** Tests same function as above but in a more thoroughly way for EXISTENT users.
     * @dataProvider dataUsersRegistrationForPartner
     */
    public function testRegisterUserPartnerComplex($email, $pwd, $langEnv, $name, $surnames, $telephone, $voucherCode,
                                  $securityCode, $partnerId, $periodId, $idCountry, $userType, $deviceTypeSource=NULL)
    {
        $moUserBeforeReg = new AbaUser();
        if($userType>=0){
            $this->assertTrue( $moUserBeforeReg->getUserByEmail($email) );
        }

        $serviceRegister = new RegisterUserService();
        /* @var AbaUser $moUser */
        $moUser = $serviceRegister->registerUserPartner( $email, $pwd, $langEnv, $name, $surnames,
                                                $telephone, $voucherCode, $securityCode, $partnerId,
                                                              $periodId, $idCountry, $deviceTypeSource);
        $this->assertInstanceOf('AbaUser', $moUser);
        $this->assertEquals( PREMIUM, $moUser->userType );
        if($userType>=0){
            $this->assertNotEquals( $moUserBeforeReg->keyExternalLogin, $moUser->keyExternalLogin );
        }

        $moLastPayment = new Payment();
        $this->assertInstanceOf('Payment', $moLastPayment->getLastPaymentByUserId( $moUser->id, true ) );
        $this->assertEquals(HeDate::todaySQL(false),HeDate::removeTimeFromSQL($moLastPayment->dateEndTransaction),
            'Date of last payment SUCCESS should be now');
        $this->assertEquals(HeDate::todaySQL(false),HeDate::europeanToSQL($moLastPayment->dateToPay),
            'Date of last payment SUCCESS should be now');


        $moUser->refresh();
        // Tests all columns for Partners columns:------------------------
        switch( $userType )
        {
            // PREMIUM TO PREMIUM
            case PREMIUM:
                $this->assertGreaterThan(HeDate::removeTimeFromSQL(HeDate::europeanToSQL($moUserBeforeReg->expirationDate)),
                    HeDate::removeTimeFromSQL($moUser->expirationDate),
                   'Expiration date has to be greater than previous expiration date.');
                $moProdPeriod = new ProdPeriodicity();
                $moProdPeriod->getProdPeriodByMonths($periodId);

                $this->assertEquals( HeDate::removeTimeFromSQL($moUser->expirationDate),
                    HeDate::removeTimeFromSQL( $moProdPeriod->getNextDateBasedOnDate(HeDate::europeanToSQL($moUserBeforeReg->expirationDate)) ),
                    'Expiration date has not been added' );
                $this->assertFalse($partnerId==$moUser->idPartnerFirstPay, 'Partner Id should not be updated');
                $this->assertFalse( REGISTER_USER_FROM_PARTNER==$moUser->registerUserSource,
                    'The source of registration is not Registration From Partner.');
                $this->assertFalse( $partnerId==$moUser->idPartnerSource,
                    'Partner source should not be updated, user existed before registration');
                $this->assertTrue( $partnerId==$moUser->idPartnerCurrent,
                    'Current partner id should be the last payment');
                $this->assertEquals($partnerId, $moLastPayment->idPartner);
                $this->assertEmpty($moUser->deviceTypeSource, 'User device source should be NULL');
                // Let's test the next payment PENDING, only in case of user was PREMIUM
                $moNextPayment = new Payment();
                $this->assertInstanceOf('Payment', $moNextPayment->getLastPaymentByUserId( $moUser->id, true ) );
                $this->assertEquals( $moNextPayment->status, PAY_SUCCESS );
                $this->assertEquals( $moNextPayment->paySuppOrderId, $voucherCode );

                break;
            // FREE TO PREMIUM
            case FREE:
                $this->assertTrue(HeDate::isGreaterThanToday( HeDate::removeTimeFromSQL($moUser->expirationDate) ),
                    'Expiration Date must be a value in the future');
                $this->assertTrue(HeDate::isGreaterThan( HeDate::removeTimeFromSQL($moUser->expirationDate),
                                                         HeDate::getDateAdd( HeDate::todaySQL(false),$periodId*28,0),
                                                        true ),
                                'Expiration date has not been correctly set');
                /* Id Partners verification */
                $this->assertFalse( REGISTER_USER_FROM_PARTNER==$moUser->registerUserSource,
                    'The source of registration is not Registration From Partner.');
                $this->assertFalse( $partnerId==$moUser->idPartnerSource,
                    'Partner source should not be updated, user existed before registration');
                if( empty($moUser->cancelReason) ){
                    $this->assertTrue($partnerId==$moUser->idPartnerFirstPay,
                        'Partner Id should be updated for FREE user');
                }else{
                    $this->assertFalse($partnerId==$moUser->idPartnerFirstPay,
                        'Partner Id Firat Pay should NOT be updated for user '.$email);
                }
                $this->assertTrue( $partnerId==$moUser->idPartnerCurrent,
                    'Current partner id should be the last payment '.$email.' id partner='.$partnerId);
                $this->assertEquals( $partnerId, $moLastPayment->idPartner,
                    'Current id Partner for Groupon payment should be '.$partnerId);
                $this->assertEquals( $moUserBeforeReg->idPartnerCurrent , $moLastPayment->idPartnerPrePay,
                    'idPartnerPrePay should be the partner before partner registration.');
                // Let's test the next payment PENDING, only in case of user was PREMIUM
                $moAbsLastPayment = new Payment();
                $this->assertInstanceOf('Payment', $moAbsLastPayment->getLastPaymentByUserId( $moUser->id, false ) );
                $this->assertEquals( $moAbsLastPayment->status, PAY_SUCCESS ,
                            'Last Payment is the one from Partner, so SUCCESS should be.');
                $this->assertEmpty($moUser->deviceTypeSource, 'User device source should be NULL');
                break;
            // DELETED TO PREMIUM
            case DELETED:
                $this->assertTrue(HeDate::isGreaterThanToday( HeDate::removeTimeFromSQL($moUser->expirationDate) ),
                    'Expiration Date must be a value in the future');
                $this->assertTrue(HeDate::isGreaterThan( HeDate::removeTimeFromSQL($moUser->expirationDate),
                        HeDate::getDateAdd( HeDate::todaySQL(false),$periodId*28,0),
                        true ),
                    'Expiration date has not been correctly set');
                /* Id Partners verification */
                $this->assertFalse( REGISTER_USER_FROM_PARTNER==$moUser->registerUserSource,
                    'The source of registration is not Registration From Partner.');
                $this->assertFalse( $partnerId==$moUser->idPartnerSource,
                    'Partner source should not be updated, user existed before registration');
                $this->assertTrue( $partnerId==$moUser->idPartnerCurrent,
                    'Current partner id should be the last payment');
                $this->assertTrue($partnerId==$moUser->idPartnerFirstPay,
                    'Partner Id should be updated for DELETED user');
                $this->assertEquals( $partnerId, $moLastPayment->idPartner,
                    'idPartner from Partner payment should be this partner='.$partnerId);
                $this->assertEmpty( $moLastPayment->idPartnerPrePay,
                    'idPartnerCurrent from user before this payment should be null and it values '.
                    $moLastPayment->idPartnerPrePay);
                $this->assertEquals(NULL, $moUser->deviceTypeSource, 'User should be considered as coming
                                                                                    from a device of this type');

                // Let's test the next payment PENDING, only in case of user was PREMIUM
                $moAbsLastPayment = new Payment();
                $this->assertInstanceOf('Payment', $moAbsLastPayment->getLastPaymentByUserId( $moUser->id, false ) );
                $this->assertEquals( $moAbsLastPayment->status, PAY_SUCCESS ,
                    'Last Payment is the one from Partner, so SUCCESS should be');
                break;
            // NEW USER
            case -1:
                $this->assertTrue(HeDate::isGreaterThanToday( HeDate::removeTimeFromSQL($moUser->expirationDate) ),
                    'Expiration Date must be a value in the future');
                $this->assertTrue(HeDate::isGreaterThan( HeDate::removeTimeFromSQL($moUser->expirationDate),
                        HeDate::getDateAdd( HeDate::todaySQL(false),$periodId*28,0),
                        true ),
                    'Expiration date has not been correctly set');
                /* Id Partners verification */
                $this->assertTrue( REGISTER_USER_FROM_PARTNER==intval($moUser->registerUserSource),
                    'The source of registration is not Registration From Partner.');
                $this->assertTrue( $partnerId==$moUser->idPartnerSource,
                    'Partner source should not be updated, user existed before registration');
                $this->assertTrue( $partnerId==$moUser->idPartnerCurrent,
                    'Current partner id should be the last payment');
                $this->assertTrue($partnerId==$moUser->idPartnerFirstPay,
                    'Partner Id should be updated for DELETED user');
                $this->assertEquals( $partnerId, $moLastPayment->idPartner);
                $this->assertEquals($deviceTypeSource, $moUser->deviceTypeSource, 'User should be considered as coming
                                                                                    from a device of this type');
                break;

        }

        return true;

    }



    /** FB users. Tests check if user exists
     * @dataProvider dataUsersRegistrationFacebookExists
     */
    public function testUserFacebookCheckExists($lang, $sFbid, $sEmail1, $sName, $sSurname, $sGender)
    {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
        $createResponse =   $this->createUsersIntoDb($sEmail1, FREE, Yii::app()->config->get("ABACountryId"), $sName, $sSurname, false, (trim($sGender) == 'female' ? 'F' : (trim($sGender) == 'male' ? 'M' : '')), $lang);
        $userIdFb =         $this->createUserFbidIntoDb($sEmail1, $sFbid);
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

        $userCommon = new UserCommon();

        $moUser = $userCommon->checkIfFbUserExists($sFbid);

        $this->assertFacebookUser($moUser, $sFbid, $sEmail1, $sName, $sSurname, $sGender, $lang);
    }

    /** FB users. Tests register user
     *
     * @dataProvider dataUsersRegistrationFacebookNoChangedEmailRegister
     */
    public function testRegisterUserChangedAndNoChangedEmailFacebook($sFbid, $sEmail1, $sEmail2, $sName, $sSurname, $sGender, $sEmailFacebook,
                                                           $langEnv, $idCountry, $idPartner, $idSourceList, $device, $currentLevel)
    {
        $userCommon =   new UserCommon();
        $rulerWs =      new RegisterUserService();

        // STEP 1
        $moUser1 = $userCommon->checkIfFbUserExists($sFbid);

        $this->assertInternalType('boolean', $moUser1);
        $this->assertEquals( false, $moUser1, "STEP 1: User exists with facebook ID " . $sFbid);


        // STEP 2
        // case no changed email
        $moUser2 = $userCommon->checkIfChangedEmailExists($sEmail1, $sEmailFacebook);

        $this->assertInternalType('boolean', $moUser2, "STEP 2 (no changed e-mail)");
        $this->assertEquals( false, $moUser2, "STEP 2 (no changed e-mail): User exists with facebook ID " . $sFbid);

        // STEP 2
        // case changed email
        $moUser3 = $userCommon->checkIfChangedEmailExists($sEmail2, $sEmailFacebook);

        $this->assertInternalType('boolean', $moUser3, "STEP 2 (changed e-mail)");
        $this->assertEquals( false, $moUser3, "STEP 2 (changed e-mail): User exists with facebook ID " . $sFbid);


        // REGISTER
        // case no changed email
        $moUser4 = $rulerWs->registerUserFacebook($sEmail1, $sName, $sSurname, $sFbid, $sGender, $langEnv, $idCountry, $idPartner, $idSourceList, $device, $currentLevel);

        $this->assertFacebookUser($moUser4, $sFbid, $sEmail1, $sName, $sSurname, $sGender, $langEnv);

        // case changed email
        $moUser5 = $rulerWs->registerUserFacebook($sEmail2, $sName, $sSurname, $sFbid, $sGender, $langEnv, $idCountry, $idPartner, $idSourceList, $device, $currentLevel);

        $this->assertFacebookUser($moUser5, $sFbid, $sEmail2, $sName, $sSurname, $sGender, $langEnv);

        return true;
    }

    /** FB users. Tests register user
     *
     * @dataProvider dataUsersRegistrationFacebookNoChangedEmailRegister2
     */
    public function testRegisterUserChangedEmailFacebook($sFbid, $sEmail1, $sEmail2, $sName, $sSurname, $sGender, $sEmailFacebook,
                                                           $langEnv, $idCountry, $idPartner, $idSourceList, $device, $currentLevel, $sPass)
    {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
        $createResponse = $this->createUsersIntoDb($sEmail1, FREE, Yii::app()->config->get("ABACountryId"), $sName, $sSurname, false, (trim($sGender) == 'female' ? 'F' : (trim($sGender) == 'male' ? 'M' : '')), $langEnv);
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

        $userCommon =   new UserCommon();
        $rulerWs =      new RegisterUserService();

        // STEP 1
        $moUser1 = $userCommon->checkIfFbUserExists($sFbid);

        $this->assertInternalType('boolean', $moUser1);
        $this->assertEquals( false, $moUser1, "STEP 1: User exists with facebook ID " . $sFbid);


        $abaUser =      new AbaUser();
        $userExists =   $abaUser->getUserByEmail($sEmail2);
        $this->assertInternalType('boolean', $userExists);
        $this->assertEquals( false, $userExists);

        // STEP 2
        // case changed email
        $moUser3 = $userCommon->checkIfChangedEmailExists($sEmail1, $sEmailFacebook);
        $this->assertInternalType('boolean', $moUser3, "STEP 2 (changed e-mail)");
        $this->assertEquals( true, $moUser3, "STEP 2 (changed e-mail): User exists with facebook ID " . $sFbid);


        // STEP 3
        // case changed email
        $moUser4 = new AbaUser();
        $moUser4->getUserByEmail($sEmail1, $sPass, true);

        $this->assertFacebookUser($moUser4, $sFbid, $sEmail1, $sName, $sSurname, $sGender, $langEnv, false);

        // STEP 3
        // case changed email
        // update any user data
        $moUser5 = $userCommon->changeUserFbData( $moUser4, $sName, $sSurname, $sFbid, $sGender);

        return true;
    }




//-----------------------------------------------------------------------------------------------------------------------------------

    /** Set of tests for function Register User Facebook Test   *
     * @return array
     */
    public function dataUsersRegistrationFacebookExists()
    {
        return array(
            array(HeMixed::getAutoLangDecision(), "1234567890", "test.fabcebook.user.1@facebookuserabatest.com", "Ivan", "Martinez", "male"),
            array("es", "1234567890", "test.fabcebook.user.1@facebookuserabatest.com", "Ivan", "Martinez", "male"),
            array("en", "1234567890", "test.fabcebook.user.1@facebookuserabatest.com", "Ivan", "Martinez", "female"),
            array("pt", "1234567890", "test.fabcebook.user.1@facebookuserabatest.com", "Ivan", "Martinez", "female"),
            array("it", "1234567890", "test.fabcebook.user.1@facebookuserabatest.com", "Ivan", "Martinez", "male"),
        );
    }

    /** Set of tests for function Register User Facebook Test   *
     * @return array
     */
    public function dataUsersRegistrationFacebookNoChangedEmailRegister()
    {
        return array(
            array("1234567890", "test.fabcebook.user.1@facebookuserabatest.com", "test.fabcebook.user.2@facebookuserabatest.com", "", "", "female", "test.fabcebook.user.1@facebookuserabatest.com", HeMixed::getAutoLangDecision(), "", "", "", "c", "1", "123456789"),
            array("1234567890", "test.fabcebook.user.1@facebookuserabatest.com", "test.fabcebook.user.2@facebookuserabatest.com", "", "", "female", "test.fabcebook.user.1@facebookuserabatest.com", "es", "", "", "", "c", "1", "123456789"),
            array("1234567890", "test.fabcebook.user.1@facebookuserabatest.com", "test.fabcebook.user.2@facebookuserabatest.com", "", "", "male", "test.fabcebook.user.1@facebookuserabatest.com", "en", "", "", "", "c", "1", "123456789"),
            array("1234567890", "test.fabcebook.user.1@facebookuserabatest.com", "test.fabcebook.user.2@facebookuserabatest.com", "", "", "male", "test.fabcebook.user.1@facebookuserabatest.com", "pt", "", "", "", "c", "1", "123456789"),
            array("1234567890", "test.fabcebook.user.1@facebookuserabatest.com", "test.fabcebook.user.2@facebookuserabatest.com", "", "", "female", "test.fabcebook.user.1@facebookuserabatest.com", "it", "", "", "", "c", "1", "123456789"),
        );
    }

    /** Set of tests for function Register User Facebook Test   *
     * @return array
     */
    public function dataUsersRegistrationFacebookNoChangedEmailRegister2()
    {
        return array(
            array("1234567890", "test.fabcebook.user.1@facebookuserabatest.com", "test.fabcebook.user.2@facebookuserabatest.com", "", "", "female", "test.fabcebook.user.2@facebookuserabatest.com", HeMixed::getAutoLangDecision(), "", "", "", "c", "1", "123456789"),
            array("1234567890", "test.fabcebook.user.1@facebookuserabatest.com", "test.fabcebook.user.2@facebookuserabatest.com", "", "", "female", "test.fabcebook.user.2@facebookuserabatest.com", "es", "", "", "", "c", "1", "123456789"),
            array("1234567890", "test.fabcebook.user.1@facebookuserabatest.com", "test.fabcebook.user.2@facebookuserabatest.com", "", "", "male", "test.fabcebook.user.2@facebookuserabatest.com", "en", "", "", "", "c", "1", "123456789"),
            array("1234567890", "test.fabcebook.user.1@facebookuserabatest.com", "test.fabcebook.user.2@facebookuserabatest.com", "", "", "male", "test.fabcebook.user.2@facebookuserabatest.com", "pt", "", "", "", "c", "1", "123456789"),
            array("1234567890", "test.fabcebook.user.1@facebookuserabatest.com", "test.fabcebook.user.2@facebookuserabatest.com", "", "", "female", "test.fabcebook.user.2@facebookuserabatest.com", "it", "", "", "", "c", "1", "123456789"),
        );
    }




    /** Set of tests for function Register User     *
     * @return array
     */
    public function dataUsersRegistrationForUser()
    {
        return array(
         /* NEW */   array("unit.test.register.user.new@yopmail.com", '123456789', "it", 199, "Unit", "Test New User",
                    3, 25, DEVICE_SOURCE_PC),
        /* NEW */   array("unit.test.register.user.new@yopmail.com", 'AbaEnglishóterquino', "pt",
                        COUNTRY_BRAZIL, "Unit", "Test New User", 3, 25, DEVICE_SOURCE_TABLET),
         /* DELETED */   array("unit.test.deleted.user@yopmail.com", '12345678902', "fr", 199, "Unit",
                    "Test DELETED User",5, 50, DEVICE_SOURCE_MOBILE)
        );
    }

    /** Set of NEGATIVE tests for function Register User     *
     * @return array
     */
    public function dataUsersNotRegistrationForUser()
    {
        return array(
            /* FREE */   array("unit.test.free.user@yopmail.com", '12345678902', "fr", 199, "Unit", "Test FREE User",5, 50)
        );
    }

    /** Set of tests for function Register User  Level Test   *
     * @return array
     */
    public function dataUsersRegistrationForUserLevelTest()
    {
        return array(
            /* NEW */   array("unit.test.register.user.level.new@yopmail.com", "it", 4, 1, 32, 75 ),
            /* DELETED */   array("unit.test.deleted.user@yopmail.com", "it", 2, 1, 48, 75)
        );
    }

    /** Set of tests for function Register User  Level Test   *
     * @return array
     */
    public function dataUsersNotRegistrationForUserLevelTest()
    {
        return array(
            /* PREMIUM */   array("unit.test.premium.user@yopmail.com", "en", 2, 1, 88, 120 ),
            /* FREE */      array("unit.test.free.user@yopmail.com", "it", 5, 1, 48, 75),
            /* NEW USER */  array("unit.test.new.user.leveltest@yopmail.com", "fr", 5, 1, 48, 75),
        );
    }

    /** Set of tests for registrationUserFromPartner
     * @return array
     */
    public function dataUsersRegistrationForPartner()
    {
        return array(
         /* PREMIUM */array("unit.test.premium.user@yopmail.com", '123456789ABC', 'en', 'UnitPremium RegPartner',
                'Test RegPartner', '93221111', 'UnitTest1', '', '72', 1, 5, PREMIUM, DEVICE_SOURCE_PC),
         /* FREE  */ array("unit.test.free.user@yopmail.com", '123456789ABC', 'en', 'UnitFree RegPartner',
              'Test RegPartner', '932222222', 'UnitTest2', 'UnitTest2Security', '72', 1, 5, FREE, DEVICE_SOURCE_MOBILE),
         /* EX-PREMIUM  */ array("unit.test.ex.premium.user@yopmail.com", '123456789ABC', 'en', 'UnitExPremium RegPartner',
                'Test RegPartner', '932223333', 'UnitTest2', 'UnitTest2Security', '72', 1, 5, FREE),
         /* DELETED  */ array("unit.test.deleted.user@yopmail.com", '123456789ABC', 'en', 'UnitLead RegPartner',
                'Test RegPartner', '932224444', 'UnitTest3', '', '72', 1, 5, DELETED, DEVICE_SOURCE_PC),
         /* NEW */array("unit.test.register.user.new@yopmail.com", '123456789', 'en', 'Unit Test New RegPartner',
                'Test RegPartner', '932224444', 'UnitTest3', '', '72', 1, 5, -1, DEVICE_SOURCE_TABLET),
        );
    }

}
