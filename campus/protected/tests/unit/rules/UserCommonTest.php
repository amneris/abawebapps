<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abaenglish
 * Date: 29/05/13
 * Time: 16:11
 * To change this template use File | Settings | File Templates.
 */
class UserCommonTest extends AbaTestCase
{
    public function setUp()
    {
        /* USER TEST:  PREMIUM EXPIRED ---*/
        $this->createUsersIntoDb("unit.test.expired.premium@yopmail.com", PREMIUM,
            106, "UnitPremiumExpired", "Testing", true);
        $moUser = new AbaUser();
        $moUser->getUserByEmail("unit.test.expired.premium@yopmail.com");
        $this->createTestPayment($moUser, PAY_SUCCESS, HeDate::getDateAdd(HeDate::todaySQL(true),0, -1),
            106,  Yii::app()->config->get("ABA_PARTNERID"), Yii::app()->config->get("ABA_PARTNERID"),30,'EUR','106_30');

        $this->createTestPayment($moUser, PAY_PENDING, HeDate::getDateAdd(HeDate::getDateAdd(HeDate::todaySQL(true), -3, 0),0,1),
            106,  PARTNER_ID_RECURRINGPAY, Yii::app()->config->get("ABA_PARTNERID"),30,'EUR','106_30');

        /* ---- END USER TEST */
        /* USER TEST:  FREE ---*/
        $this->createUsersIntoDb("unit.test.free.usercommon@yopmail.com", FREE,
            COUNTRY_ABACOUNTRY_OLD, "UnitFree", "Testing", true);
        /* -----    END FREE   */

        $wsAppStoreData = $this->wsAppStoreData();
        foreach($wsAppStoreData as $iKey => $user) {
            $this->createUsersIntoDb($user[0], FREE, $user[6], $user[4] . " - UnitFree", $user[5] . " - Testing", true);
        }
    }

    public function tearDown()
    {
        parent::tearDown();

        $moUser = new AbaUser();
        if($moUser->getUserByEmail("unit.test.expired.premium@yopmail.com","",false))
        {
            $moUser->deleteUserThroughResetCall();
        }
        $moUser = new AbaUser();
        if($moUser->getUserByEmail("unit.test.free.usercommon@yopmail.com","",false))
        {
            $moUser->deleteUserThroughResetCall();
        }

        $wsAppStoreData = $this->wsAppStoreData();
        foreach($wsAppStoreData as $iKey => $user) {
            $moUser = new AbaUser();
            if($moUser->getUserByEmail($user[0], "", false)) {
                $moUser->deleteUserThroughResetCall();
            }
        }
    }

    /** Test user for testCancelUser function
     * @return array
     */
    public function dataCancelUser()
    {
        return array(
            array('unit.test.expired.premium@yopmail.com', PAY_CANCEL_UNDEFINED),
        );
    }

    /** Tests same subject name function
     * @dataProvider dataCancelUser
     *
     * @param $email
     * @param $cancelReason
     *
     * @return bool
     */
    public function testCancelUser($email, $cancelReason)
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail($email);
        $userId= $moUser->getId();
        $this->assertEquals(PREMIUM, $moUser->userType);


        $commTestUser = new UserCommon();
        // Assert function finishes without errors
        $this->assertTrue( $commTestUser->cancelUser($userId, $cancelReason) );

        /* USER ASSERTIONS */
        $moUser->refresh();
        $this->assertEquals(FREE, $moUser->userType);
        $this->assertEquals($cancelReason, $moUser->cancelReason);
//        $this->assertEquals( PARTNER_ID_EXABAENGLISH, $moUser->idPartnerCurrent);
        /* LAST PAYMENT ASSERTIONS */
        $moLastPay = new Payment();
        $moLastPay->getLastPaymentByUserId($userId, true) ;
        $this->assertEquals(PAY_FAIL, $moLastPay->status, 'Status of payment should be CANCEL');
        $this->assertEquals(HeDate::todaySQL(false),HeDate::removeTimeFromSQL($moLastPay->dateEndTransaction),
            'Date End transaction should value today');
        $this->assertEquals( PARTNER_ID_RECURRINGPAY, $moLastPay->idPartner );
        $this->assertEquals( Yii::app()->config->get("ABA_PARTNERID"), $moLastPay->idPartnerPrePay );

        return true;

    }

    /** Test user for testChangePassword function
     * @return array
     */
    public function dataChangePassword()
    {
        return array(
            array('unit.test.expired.premium@yopmail.com', 'testSeleniumPassword'),
        );
    }

    /** Tests same subject name function
     * @dataProvider dataChangePassword
     *
     * @param $email
     * @param $password
     *
     * @return bool
     */
    public function testChangePassword( $email, $password)
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail($email);

        $oldAutoLogin = $moUser->keyExternalLogin;
        // AbaUser &$moUser, $newPassword, $actionSource, $updateSQL=true

        $commTestUser = new UserCommon();
        // Assert function finishes without errors
        $this->assertTrue( $commTestUser->changePassword( $moUser, $password, "Test Selenium") );

        $moUser->refresh();
        $this->assertNotEquals($oldAutoLogin, $moUser->keyExternalLogin);

        return true;

    }

    /**
     * @return bool
     */
    public function testCheckAndUpdateCountryId()
    {
        // Toscanna, 16, Italy, 10.31670000, 43.55000000
        $ipTest = "79.56.203.156";
        $_SERVER["REMOTE_ADDR"] = $ipTest;

        $moUser = new AbaUser();
        $moUser->getUserByEmail("unit.test.free.usercommon@yopmail.com");

        $commTestUser = new UserCommon();

        $moUser = $commTestUser->checkAndUpdateCountryId($moUser, "Unit Test");
        $this->assertEquals( "AbaUser", get_class($moUser),
            "Function checkAndUpdateCountryId should change the country and location and return user");
        $moLocation = new AbaUserLocation();
        $moLocation = $moLocation->getLocationByIdUserIdIp($moUser->getId(), $ipTest);
        // Assertions:
        $this->assertEquals(105, $moUser->countryIdCustom);
        $this->assertEquals(105, $moUser->countryId);
        $this->assertEquals( "AbaUserLocation", get_class($moLocation) );
        $this->assertEquals("IT", $moLocation->isoCountryCode,
            "The iso code should be Italy.");

        $moCountry = new AbaCountry();
        $this->assertTrue( $moCountry->getCountryByIsoCode($moLocation->isoCountryCode),
            "The country code delivered by Geo Ip Library should be a valid Iso Code in our table" );
        $this->assertEquals("ITALY", strtoupper($moCountry->name),
            "The name of the country is not ITALY but ".$moCountry->name );
        $this->assertEquals("ROCCASTRADA", strtoupper($moLocation->city),
            "City Should be ROCCASTRADA");
        $this->assertEquals("11.16670000", ($moLocation->longitude));
        $this->assertEquals("43.00000000", ($moLocation->latitude));

        return true;
    }

    /**
     * @return bool
     */
    public function testRegisterGeoLocation()
    {
        // 146.185.135.95, 30.26420000, 59.89440000, NL, Netherlands
        $ipTest = "62.140.160.0";

        $moUser = new AbaUser();
        $moUser->getUserByEmail("unit.test.free.usercommon@yopmail.com");

        $commTestUser = new UserCommon();
        $this->assertTrue($commTestUser->registerGeoLocation($moUser, $ipTest));


        $moLocation = new AbaUserLocation();
        $moLocation = $moLocation->getLocationByIdUserIdIp($moUser->getId(), '', 1);

        $this->assertEquals($ipTest, $moLocation->ip, " Ip is different");
        $this->assertEquals("NL", $moLocation->isoCountryCode,
            "The iso code should be Netherlands.");
        $moCountry = new AbaCountry();
        $this->assertTrue( $moCountry->getCountryByIsoCode($moLocation->isoCountryCode),
            "The country code delivered by Geo Ip Library should be a valid Iso Code in our table" );
        $this->assertEquals("NETHERLANDS", strtoupper($moCountry->name),
            "The name of the country is not NETHERLANDS but ".$moCountry->name );
        $this->assertEquals("Amsterdam", $moLocation->city,
            "City Should be Amsterdam");
        $this->assertEquals("4.91670000", ($moLocation->longitude));
        $this->assertEquals("52.35000000", ($moLocation->latitude));
        $this->assertEquals(1, $moLocation->default);

        return true;

    }

    /**
     * @test isDisplayInvoices
     *
     * @return bool
     */
    public function testIsDisplayInvoices()
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail("unit.test.expired.premium@yopmail.com");

        $comInvoice = new InvoicesCommon();
        $this->assertTrue( $comInvoice->isDisplayInvoices($moUser->id));

        $moUser = NULL;
        $moUser = new AbaUser();
        $moUser->getUserByEmail("unit.test.free.usercommon@yopmail.com");
        $this->assertFalse( $comInvoice->isDisplayInvoices($moUser->id));

        return true;
    }

    /**
     * @return array
     */
    public function wsAppStoreData()
    {
        return array(
            array("unit.test.free.usercommon1@yopmail.com", 1,  'EUR',  '19.98', 'Alek 1', 'Sander',   30, '{"em$ail":"unit.test.free.us\'ercommon1@yopmail.com", "price": "19.98"}'),
            array("unit.test.free.usercommon2@yopmail.com", 6,  'BRL', '129.97', 'Alek 6', 'Sander',  105, "{'e:USERIDm?ail':'unit.test.free.usercommon2@yopmail.com', 'price': '129.98'}"),
            array("unit.test.free.usercommon3@yopmail.com", 1,  'USD',  '19.96', 'Alek 12', 'Sander', 177, '{"email":"unit.test.free.usercommon3@yopmail.com", "price": "19.98"}'),
            array("unit.test.free.usercommon4@yopmail.com", 12, 'USD', '259.96', 'Alek 12', 'Sander', 177, '{"email":"unit.test.free.usercommon4@yopmail.com", "price": "259.98"}'),
            array("unit.test.free.usercommon5@yopmail.com", 24, 'EUR', '509.95', 'Alek 24', 'Sander', 199, '{"email":"unit.test.free.usercommon5@yopmail.com", "price": "509.98"}'),
            array("unit.test.free.usercommon6@yopmail.com", 12, '',    '259.96', 'Alek 12', 'Sander', 177, '{"email":"unit.test.free.usercommon6@yopmail.com", "price": "259.98"}'),
            array("unit.test.free.usercommon7@yopmail.com", 24, 'EUR', '',       'Alek 24', 'Sander', 199, '{"email":"unit.test.free.usercommon7@yopmail.com", "price": "0"}'),
            array("unit.test.free.usercommon8@yopmail.com", 12, '',    '',       'Alek 12', 'Sander', 177, '{"email":"unit.test.free.usercommon8@yopmail.com", "price": "0"}'),
        );
    }

    /**
     * @dataProvider wsAppStoreData
     *
     * @return bool
     */
    public function testFree2premium($tEmail, $periodId, $currency, $price, $name, $surnames, $country, $purchaseReceipt)
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail($tEmail);

        //
        $commUser = new UserCommon();
        if (!$commUser->free2premium($moUser, $periodId, $currency, $price, $country, $moUser->id, $purchaseReceipt, 'Action Rest Web Service TEST testFree2premium')) {
            $this->assertFalse(true);
        }

//        $commProductsPrices = new ProductsPricesCommon();
//        if(!$commProductsPrices->updateAppStoreData($moUser, $periodId, $currency, $price, $country, $moUser->id, $purchaseReceipt)) {
//            $this->assertFalse(true);
//        }

        return true;
    }


}
