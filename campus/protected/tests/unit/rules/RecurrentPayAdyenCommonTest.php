<?php
/**
 * Class RecurrentPayAdyenCommonTest
 */
class RecurrentPayAdyenCommonTest  extends AbaTestCase
{
    private $premiumUser = "unit.test.premium.changecredit@yopmail.com";

    public function setUp()
    {
        parent::setUpAdyen();

        /* USER TEST:  PREMIUM ACTIVE ---*/
//        $this->createUsersIntoDb($this->premiumUser, PREMIUM, COUNTRY_ITALY, "UnitPremiumChageCredit", "Testing", false, '', '', false, PAY_SUPPLIER_ADYEN);
        $this->createUsersIntoDb($this->premiumUser, PREMIUM, 47, "UnitPremiumChageCredit", "Testing", false, '', '', false, PAY_SUPPLIER_ADYEN);  // COUNTRY_FRANCE
        $moUser = new AbaUser();
        $moUser->getUserByEmail($this->premiumUser);

        $aCardMcOld = DataCollections::$aCreditFormsAdyen["MasterCard"];
        $aDataPayMethod = array("creditCardType"=> $aCardMcOld[1], "creditCardNumber"=> $aCardMcOld[2],
            "creditCardYear"=>$aCardMcOld[3], "creditCardMonth"=> $aCardMcOld[2], "CVC"=> $aCardMcOld[3],
            "creditCardName"=> $aCardMcOld[4], "cpfBrasil"=> $aCardMcOld[5] , "typeCpf" => $aCardMcOld[6]);
        $commOnlinePay = new OnlinePayCommon();
        $moUserCredit = $commOnlinePay->saveCreditFormData( $aDataPayMethod, $errorMsg, $moUser);

        $moLastPay = new Payment();
        $moLastPay->getLastPaymentByUserId($moUser->id);
        $moLastPay->idUserCreditForm = $moUserCredit->id;
        $moLastPay->update(array("idUserCreditForm"));
        /* ---- END USER TEST */
    }


    public function tearDown()
    {
        parent::tearDown();

        $moUser = new AbaUser();
        if ($moUser->getUserByEmail($this->premiumUser, "", false)) {
            $moUser->deleteUserThroughResetCall();
        }
        $moUser = NULL;
    }

    /**
     * @test
     *
     */
    public function testRenewPayments()
    {
        $abaUserPremium = new AbaUser();
        $abaUserPremium->getUserByEmail("unit.test.premium.user@yopmail.com");
        $abaUserFree = new AbaUser();
        $abaUserFree->getUserByEmail("unit.test.free.user@yopmail.com");
        $abaUserDeleted = new AbaUser();
        $abaUserDeleted->getUserByEmail("unit.test.deleted.user@yopmail.com");
        $abaUserExPremium = new AbaUser();
        $abaUserExPremium->getUserByEmail("unit.test.ex.premium.user@yopmail.com");
        $abaUserCancelled = new AbaUser();
        $abaUserCancelled->getUserByEmail("unit.test.premium.cancelled.user@yopmail.com");
        $abaUserB2B = new AbaUser();
        $abaUserB2B->getUserByEmail("unit.test.premium.b2b.active@yopmail.com");
        $moUser = new AbaUser();
        $moUser->getUserByEmail($this->premiumUser);

        $users = array(
            $abaUserPremium->id => $abaUserPremium->id,
            $abaUserFree->id => $abaUserFree->id,
            $abaUserDeleted->id => $abaUserDeleted->id,
            $abaUserExPremium->id => $abaUserExPremium->id,
            $abaUserCancelled->id => $abaUserCancelled->id,
            $abaUserB2B->id => $abaUserB2B->id,
            $moUser->id => $moUser->id,
        );

        $moPayments =   new Payments();
        $aPendingPays = $moPayments->getAllPaymentsPendingSupplierTest($users, PAY_SUPPLIER_ADYEN);

        if( $aPendingPays ) {

            foreach($aPendingPays as $moPendingPay)
            {
                $dateStart =    HeDate::todaySQL(true);
                $user =         new AbaUser( );

                if(is_numeric($moPendingPay->userId) ) {

                    if( !$user->getUserById( $moPendingPay->userId ) ) {
                        continue;
                    }

                    $moUserCredit = new AbaUserCreditForms();
                    if( !$moUserCredit->getUserCreditFormByUserId($moPendingPay->userId) ) {
                        continue;
                    }

                    $moProductUser = new ProductPrice();
                    if( !$moProductUser->getProductById( $moPendingPay->idProduct ) ) {
                        continue;
                    }

                    if( intval( Yii::app()->config->get("ENABLE_ADYEN_PAY_RECURRENT") ) == 1) {

                        $moPaymentControlCheck = new PaymentControlCheck();
                        if( !$moPaymentControlCheck->createPayControlCheckFromPayment(
                            $user->id, 1, $moPendingPay->idProduct, $moPendingPay->idCountry, $moPendingPay->idPeriodPay,
                            $moPendingPay->idPromoCode, $moUserCredit->kind, $moUserCredit->cardName, $moUserCredit->cardNumber , $moUserCredit->cardYear, $moUserCredit->cardMonth,
                            $moUserCredit->cardCvc, $moPendingPay->paySuppExtId, $moPendingPay, '', '', false ) ) {
                            continue;
                        }

                        $paySupplier = PaymentFactory::createPayment( $moPendingPay );

                        $retPaymentSupplier = $paySupplier->runDebiting( $moPaymentControlCheck,"A",true );

                        if( !$retPaymentSupplier ){
                            $moPendingPay->paySuppOrderId = $paySupplier->getOrderNumber();
                            continue;
                        }

                        $commRecurrPayment = new RecurrentPayCommon();
                        $retSucc = $commRecurrPayment->renewPaymentByPaySuppOrderId($user->email, $paySupplier->getOrderNumber(), $dateStart,"recurring_payment_ADYEN", PAY_SUPPLIER_ADYEN , $moPendingPay->id);
                        if ($retSucc) {
                            $moPendingPay->paySuppExtUniId = $retPaymentSupplier->getUniqueId();
                            $moPendingPay->update(array("paySuppExtUniId"));
                        }
                    }
                    else{}
                }
            }
        }
    }


    /**
     * @test
     *
     * @dataProvider dataAdyenCase
     *
     * @param $email
     * @return bool
     */
    public function testGetTypeSubscriptionStatus($email)
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail($email);

        $commRecurrent = new RecurrentPayCommon();
        /* @var Payment $moLastPay */
        $typeSubscription = $commRecurrent->getTypeSubscriptionStatus($moUser, $moLastPay);

        switch ($email) {
            case 'unit.test.premium.user@yopmail.com':
                $this->assertEquals(SUBS_PREMIUM_A_CARD_AD, $typeSubscription);
                $this->assertEquals("Payment", get_class($moLastPay));
                $this->assertEquals(PAY_SUPPLIER_ADYEN, $moLastPay->paySuppExtId);
                $this->assertEquals(PAY_PENDING, $moLastPay->status);
                break;
            case 'unit.test.free.user@yopmail.com':
                $this->assertEquals(SUBS_FREE, $typeSubscription);
                $this->assertEmpty($moLastPay);
                break;
            case 'unit.test.ex.premium.user@yopmail.com':
                $this->assertEquals(SUBS_EXPREMIUM_CARD, $typeSubscription);
                $this->assertEquals(PAY_SUPPLIER_ADYEN, $moLastPay->paySuppExtId);
                $this->assertEquals(PAY_CANCEL, $moLastPay->status);
                break;
            case 'unit.test.deleted.user@yopmail.com':
                $this->assertEquals(SUBS_FREE, $typeSubscription);
                break;
            case 'unit.test.premium.b2b.active@yopmail.com':
                $this->assertEquals(SUBS_PREMIUM_THROUGHB2B, $typeSubscription);
                $this->assertEquals("Payment", get_class($moLastPay));
                $this->assertEquals(PAY_SUCCESS, $moLastPay->status);
                $this->assertEquals(PAY_SUPPLIER_B2B, $moLastPay->paySuppExtId);
                break;
            case 'unit.test.premium.cancelled.user@yopmail.com':
                $this->assertEquals(SUBS_PREMIUM_C_CARD_C, $typeSubscription);
                $this->assertEquals("Payment", get_class($moLastPay));
                $this->assertEquals(PAY_SUPPLIER_ADYEN, $moLastPay->paySuppExtId);
                $this->assertEquals(PAY_CANCEL, $moLastPay->status);
                break;
        }

        return true;
    }

    /**
     * @test
     *
     * @dataProvider dataAdyenCase
     *
     * @param $email
     * @return bool
     */
    public function testIsDisplaySubscription($email)
    {
       $moUser = new AbaUser();
       $moUser->getUserByEmail($email);

        $this->assertTrue(RecurrentPayCommon::isDisplaySubscription($moUser->id));

        return true;
    }

    /**
     * @test
     *
     * @return bool
     */
    public function testChangeProductDowngrade()
    {
        $moUser = new AbaUser();
        $email = DataCollections::$aUsersAdyenCase['PREMIUM_ACTIVE_ADYEN'];
        $moUser->getUserByEmail($email[0]);
        $moUserCredit = new AbaUserCreditForms($moUser->id);

        $payDraft = $this->prepareMockPayment( $moUser, '', '', '', '', COUNTRY_SPAIN,
            '199_30', 30, $moUserCredit->kind,
            $moUserCredit->cardName, $moUserCredit->cardYear, $moUserCredit->cardMonth, $moUserCredit->cardNumber,
            14.99, 'EUR', $moUserCredit->cardCvc, '');


        $commRecurring = new RecurrentPayCommon();
        $moNewPay = $commRecurring->changeProduct( $moUser, $payDraft);
        $this->assertEquals('payment', strtolower(get_class($moNewPay)),
            'Instead of payment returned error='. $commRecurring->getErrorMessage());
        $this->assertEquals(PAY_PENDING, $moNewPay->status);

        $moPendPay = new Payment();
        $moPendPay->getLastPayPendingByUserId($moUser->id);
        $this->assertEquals($moNewPay->id, $moPendPay->id);
        $this->assertEquals(-180, intval($moPendPay->isPeriodPayChange),
                'It should say that comes from 6 months period product');
        $this->assertEquals(HeDate::europeanToSQL($moUser->expirationDate,false),
              HeDate::removeTimeFromSQL($moPendPay->dateToPay), 'Expiration date and date To Pay should be the same.');

        $moCancelPay = new Payment();
        $moCancelPay->getLastCancelPayment($moUser->id);
        $this->assertEquals( 30, intval($moCancelPay->isPeriodPayChange));
        $this->assertEquals($moPendPay->idUserCreditForm, $moCancelPay->idUserCreditForm, 'It should be same credit card');
        $this->assertEquals($moPendPay->currencyTrans, $moCancelPay->currencyTrans, 'It should be same currency');
        $this->assertLessThan( $moCancelPay->amountOriginal, $moPendPay->amountOriginal );
        $this->assertEquals(1, intval($moPendPay->isRecurring));
        $this->assertEquals(STATUS_CANCEL_CHANGEPROD, $moCancelPay->cancelOrigin);
        $this->assertEmpty($moUser->cancelReason);

        return true;
    }

    /**
     * @test
     *
     * Tests a successful Upgrade
     */
    public function testChangeProductUpgrade()
    {
        $moUser = new AbaUser();
        $email = DataCollections::$aUsersAdyenCase['PREMIUM_ACTIVE_ADYEN'];
        $moUser->getUserByEmail($email[0]);
        $moUserCredit = new AbaUserCreditForms($moUser->id);

        $payDraft = $this->prepareMockPayment( $moUser, '', '', '', '', COUNTRY_ITALY, '199_720',
            720, $moUserCredit->kind, $moUserCredit->cardName,
            $moUserCredit->cardYear, $moUserCredit->cardMonth, $moUserCredit->cardNumber,
            139.99, 'EUR', $moUserCredit->cardCvc, '');


        $commRecurring = new RecurrentPayCommon();
        $moNewPay = $commRecurring->changeProduct( $moUser, $payDraft);
        $this->assertEquals('payment', strtolower(get_class($moNewPay)),
            'Instead of payment returned error='. $commRecurring->getErrorMessage());
        $this->assertEquals(PAY_PENDING, $moNewPay->status);

        $moPendPay = new Payment();
        $moPendPay->getLastPayPendingByUserId($moUser->id);
        $this->assertEquals($moNewPay->id, $moPendPay->id);
        $this->assertEquals( 180, intval($moPendPay->isPeriodPayChange),
            'It should say that comes from 6 months period product');

        $this->assertEquals(HeDate::europeanToSQL($moUser->expirationDate,false),
            HeDate::removeTimeFromSQL($moPendPay->dateToPay), 'Expiration date and date To Pay should be the same.');

        $moCancelPay = new Payment();
        $moCancelPay->getLastCancelPayment($moUser->id);
        $this->assertEquals( 720, intval($moCancelPay->isPeriodPayChange));
        $this->assertEquals($moPendPay->idUserCreditForm, $moCancelPay->idUserCreditForm, 'It should be same credit card');
        $this->assertEquals($moPendPay->currencyTrans, $moCancelPay->currencyTrans, 'It should be same currency');
        $this->assertGreaterThan( $moPendPay->amountOriginal, $moCancelPay->amountOriginal );
        $this->assertEquals(1, intval($moPendPay->isRecurring));
        $this->assertEquals(STATUS_CANCEL_CHANGEPROD, $moCancelPay->cancelOrigin);
        $this->assertEmpty($moUser->cancelReason);

        return true;
    }

    /** It asserts that someone that is not on an active subscription can not change anything.
     *
     * @test
     *
     * @return bool
     */
    public function testNotChangeProduct()
    {
        $moUser = new AbaUser();
        $email = DataCollections::$aUsersAdyenCase['PREMIUM_CANCEL_ADYEN'];
        $moUser->getUserByEmail($email[0]);
        $moUserCredit = new AbaUserCreditForms($moUser->id);

        $payDraft = $this->prepareMockPayment( $moUser, '', '', '', '', COUNTRY_ITALY, '199_720',
            720, $moUserCredit->kind, $moUserCredit->cardName,
            $moUserCredit->cardYear, $moUserCredit->cardMonth, $moUserCredit->cardNumber,
            139.99, 'EUR', $moUserCredit->cardCvc, '');

        $moOldPay = new Payment();
        $moOldPay->getLastCancelPayment ( $moUser->id );

        $commRecurring = new RecurrentPayCommon();
        $moNewPay = $commRecurring->changeProduct( $moUser, $payDraft);
        $this->assertFalse($moNewPay, 'It should return an error because someone that is not active subscription '.
            'can not upgrade or downgrade it.');

        $this->assertEquals( PAY_CANCEL, $moOldPay->status);

        $moOldAfterPay = new Payment();
        $moOldAfterPay->getLastCancelPayment($moUser->id);

        $this->assertEquals( $moOldAfterPay->id, $moOldPay->id);
        $moOldPay->refresh();
        $this->assertEquals( PAY_CANCEL, $moOldAfterPay->status);

        return true;
    }


    /**
     * @return array
     */
    public function dataAdyenCase()
    {
        return DataCollections::$aUsersAdyenCase;
    }

}
