<?php
/**
 * Created by Joaquim Forcada (Quino) in Aba English with PHP Storm
 * User: Joaquim Forcada (Quino)
 * Date: 28/05/14
 * Time: 10:43
 
 ---Please brief description here---- 
 */

class PaySupplierLaCaixaTest extends AbaTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test PaySupplierLaCaixa::runDebiting
     *
     * @dataProvider dataUserPaymentDebit
     */
    public function testRunDebiting( $resultTest, $email, $idProduct, $idPeriodPay, $cardKind, $cardName,
                                     $cardYear,$cardMonth, $cardNumber, $amountPrice, $currency, $cardCvc )
    {
        $moUser = new AbaUser();
        $this->assertTrue($moUser->getUserByEmail($email));

        $testTransaction = $this->prepareMockPayment($moUser, "", "", "", "", COUNTRY_SPAIN, $idProduct, $idPeriodPay, $cardKind,
            $cardName, $cardYear, $cardMonth, $cardNumber, $amountPrice, $currency, $cardCvc, "");

        $paySupplier = new PaySupplierLaCaixa();
        /* @var $moLog PayGatewayLogLaCaixaResRec */
        $moLog = $paySupplier->runDebiting($testTransaction, "A", false);

        if ($resultTest) {
            $this->assertInstanceOf( PayGatewayLog, $moLog);
            $this->assertTrue(HeMixed::isXmlStructureValid($moLog->XML));
            $this->assertEmpty($paySupplier->getErrorCode());
            $this->assertEquals($testTransaction->id, $moLog->idPayment,
                "Id Payment from Log should be equal to payment");
            $this->assertNotEmpty($moLog->AuthorisationCode);
            $this->assertEquals( $moLog->AuthorisationCode, $moLog->getUniqueId());
            $this->assertEquals(2, $moLog->Terminal);
            $this->assertEquals('010454155', $moLog->MerchantCode);
            $this->assertEquals(0, $moLog->Codigo);

            $testTransaction->paySuppExtUniId =  $moLog->AuthorisationCode;
            return $this->assertRunRefund( $testTransaction, $moLog->getUniqueId(),
                                HeMixed::getRoundAmount(($amountPrice)), $moLog );

        } else {
            $this->assertFalse($moLog);
            $this->assertNotEmpty($paySupplier->getErrorCode());
            $moLog = $paySupplier->getMoLogResponse();
            $this->assertTrue(HeMixed::isXmlStructureValid($moLog->XML));
        }

        return true;
    }

    /**
     * @param PaymentControlCheck $moPayCtrlSuccess
     * @param $uniqueId
     * @param $amountPrice
     * @param PayGatewayLog $paymentSupplierLog
     * @return bool
     */
    public function assertRunRefund( PaymentControlCheck $moPayCtrlSuccess, $uniqueId, $amountPrice,
                                     PayGatewayLog $paymentSupplierLog )
    {
        $moPaySuccess = new Payment();
        $moPaySuccess->copyFromPayControlCheckToPayment($moPayCtrlSuccess, NULL, $paymentSupplierLog);
        $moPaySuccess->paySuppExtUniId = $uniqueId;

        $moUser = new AbaUser();
        $moUser->getUserById($moPaySuccess->userId);
        $moRefund = clone($moPaySuccess);
        $moRefund->status = PAY_REFUND;
        $moRefund->paySuppExtUniId = NULL;
        $moRefund->amountPrice = $amountPrice;
        $moRefund->id = HeAbaSHA::generateIdPayment($moUser);

        $payCaixa = new PaySupplierLaCaixa();
        $moLog = $payCaixa->runRefund($moRefund, $moPaySuccess);

        $this->assertEquals( "PayGatewayLogLaCaixaResRec", get_class($moLog), "Transaction was not successful");
        $this->assertTrue(HeMixed::isXmlStructureValid($moLog->XML));
        $this->assertEmpty( $payCaixa->getErrorCode(), 'There should be no error.');
        $this->assertEquals( $moRefund->paySuppOrderId, strval($moLog->getOrderNumber()),
                                    'Order number dows not match.' );
        $this->assertEquals( $moPayCtrlSuccess->paySuppExtUniId, strval($moLog->getUniqueId()),
                                    'ExtUnid should be the same os the payment referenced' );

        return true;
    }

    /** Data samples for La Caixa Tests
     *
     * @return array
     */
    public function dataUserPaymentDebit()
    {
        /** $resultTest, $email, $idProduct, $idPeriodPay, $cardKind, $cardName, $cardYear,
          $cardMonth, $cardNumber, $amountPrice, $currency */
        return array(
            /* POSITIVE */
            array(true,"unit.test.free.user@yopmail.com",
                "205_180",
                "180",
                KIND_CC_VISA,
                "Test Unit Free Credit Card Aba English",
                "2020",
                "12",
                "4548812049400004",
                "125.0000",
                "EUR",
                "285"),
            /* NEGATIVE */
            array(false,"unit.test.free.user@yopmail.com",
                "30_360",
                "360",
                KIND_CC_VISA,
                "Test Failure Unit Free Credit Card Aba English ",
                "2018",
                "05",
                "4012888888888888",
                "109",
                "EUR",
                "285"),
        );
    }

}
 