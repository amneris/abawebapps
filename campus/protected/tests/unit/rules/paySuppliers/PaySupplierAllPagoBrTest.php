<?php
/**
 * /**
 * Created by Joaquim Forcada (Quino) in Aba English with PHP Storm
 * User: Joaquim Forcada (Quino)
 * Date: 3/12/13
 * Time: 13:48
 *
 */
class PaySupplierAllPagoBrTest extends AbaTestCase
{
    /* @var $email string */
    protected $email;
    protected $emailN;

    public function setUp()
    {
        parent::setUp();
        // User FREE specific for this test only:

        $this->emailN = strval(intval(rand(1,1000)));
        $this->email = 'unit.test.free.user.br.'.$this->emailN.'@yopmail.com';
        $this->createUsersIntoDb( $this->email,
            FREE, COUNTRY_BRAZIL, "Unit Free", "Testing Brazil");
    }

    public function tearDown()
    {
        parent::tearDown();
        $moUser = new AbaUser();
        $moUser->getUserByEmail ( $this->email, '', false );
        $moUser->deleteUserThroughResetCall();
    }

    /**
     * @test PaySupplierAllPagoBr::opRunCredit
     *
     * @dataProvider dataUserPaymentDebit1
     */
    public function testOpRunDebit1($resultTest, $email, $street, $zipCode, $city, $state, $countryIso2, $idProduct,
                         $idPeriodPay, $cardKind, $cardName, $cardYear,$cardMonth, $cardNumber, $amountPrice, $currency,
                            $cardCvc, $cpfBrasil)
    {
        $email = str_replace('XXXX',$this->emailN, $email);
        $moUser = new AbaUser();
        $this->assertTrue($moUser->getUserByEmail($email));

        /** @var $testTransaction PaymentControlCheck */
        $testTransaction = $this->prepareMockPayment($moUser, $street, $zipCode, $city, $state, COUNTRY_BRAZIL,
           $idProduct, $idPeriodPay, $cardKind, $cardName, $cardYear, $cardMonth, $cardNumber, $amountPrice, $currency,
            $cardCvc, $cpfBrasil);

        $moUser->countryId = COUNTRY_BRAZIL;
        $moUser->update( array("countryId") );

        $payment = new PaySupplierAllPagoBr();
        $moLog = $payment->opRunDebit($testTransaction, $moUser);

        if ($resultTest) {
            $this->assertEquals( "PayGatewayLogAllpago", get_class($moLog));
            $this->assertTrue(HeMixed::isXmlStructureValid($payment->xmlRequest));
            $this->assertEmpty($payment->getErrorCode());
            $transId = $moLog->getXmlResponse()->xpath("/Response/Transaction/Identification/TransactionID");
            $this->assertEquals($testTransaction->id, strval($transId[0]), "The All Pago Transaction Id not ".
                "equals to our payment attempt.");
            $amountReturned = $moLog->getXmlResponse()->xpath("/Response/Transaction/Payment/Clearing/Amount");
            $this->assertEquals(HeMixed::getRoundAmount($amountPrice),
                                HeMixed::getRoundAmount((strval($amountReturned[0])) ));
            $fkIdAllpago = $moLog->getUniqueId();
            $this->assertInternalType("string", $fkIdAllpago);
            $this->assertGreaterThan(10, strlen($fkIdAllpago));
            // For DISCOVER card we randomly refund half the amount.
            if ($cardKind == KIND_CC_DISCOVER) {
                $amountPrice = $amountPrice / 2;
            }
            $this->assertOpRunRefund($testTransaction, $moLog->getUniqueId(),
                                            HeMixed::getRoundAmount(($amountPrice)), $moLog);

        } else if($cardNumber=='4012888888888888'){
            $this->assertFalse($moLog);
            $this->assertTrue(HeMixed::isXmlStructureValid($payment->xmlRequest));
            $this->assertEquals("100.100.101", $payment->getErrorCode(), 'Expected invalid creditcard, '.
                                                                        'bank account number '.
                                                                        'bank name 100.100.700.');
            $this->assertEquals("invalid creditcard, bank account number or bank name", $payment->getErrorString());
        } else{
            $this->assertFalse($moLog);
            $this->assertTrue(HeMixed::isXmlStructureValid($payment->xmlRequest));
            //$this->assertEquals("100.100.101", )
            $this->assertRegExp('/\b\d{1,3}\.\d{1,3}\.\d{1,3}/', trim($payment->getErrorCode()));
        }

        return true;
    }

    /**
     * @test PaySupplierAllPagoBr::OpRunDebit
     *
     * @dataProvider dataUserPaymentDebit2
     */
    public function testOpRunDebit2($resultTest, $email, $street, $zipCode, $city, $state, $countryIso2, $idProduct,
                        $idPeriodPay, $cardKind, $cardName, $cardYear,$cardMonth, $cardNumber, $amountPrice, $currency,
                        $cardCvc, $cpfBrasil)
    {
        $email = str_replace('XXXX',$this->emailN, $email);
        $moUser = new AbaUser();
        $this->assertTrue($moUser->getUserByEmail($email));

        $testTransaction = $this->prepareMockPayment($moUser, $street, $zipCode, $city, $state, COUNTRY_BRAZIL,
            $idProduct, $idPeriodPay, $cardKind, $cardName, $cardYear, $cardMonth, $cardNumber, $amountPrice, $currency,
            $cardCvc, $cpfBrasil);

        $moUser->countryId = COUNTRY_BRAZIL;
        $moUser->update(array("countryId"));

        $payment = new PaySupplierAllPagoBr();
        $moLog = $payment->opRunDebit($testTransaction, $moUser);

        if ($resultTest) {
            $this->assertEquals( "PayGatewayLogAllpago", get_class($moLog), "Transaction was not successful");
            $this->assertTrue(HeMixed::isXmlStructureValid($payment->xmlRequest));
            $this->assertEmpty($payment->getErrorCode());
            $transId = $moLog->getXmlResponse()->xpath("/Response/Transaction/Identification/TransactionID");
            $this->assertEquals($testTransaction->id, strval($transId[0]), "The All Pago Transaction Id not ".
                                                                "equals to our payment attempt.");

        } else {
            $this->assertFalse($moLog);
            $this->assertTrue(HeMixed::isXmlStructureValid($payment->xmlRequest));
            $this->assertEquals("100.100.700", $payment->getErrorCode(), 'Expected invalid card 100.100.700.');
            $this->assertEquals("invalid cc number/brand combination", $payment->getErrorString());
        }

        return true;
    }

    /**
     * @test PaySupplierAllPagoBr::opRunRegistration
     *
     * @dataProvider dataUserPaymentRegistration
     */
    public function testOpRunRegistration($resultTest, $email, $street, $zipCode, $city, $state, $countryIso2,
                                         $cardKind, $cardName, $cardYear,$cardMonth, $cardNumber, $cardCvc, $cpfBrasil)
    {
        $email = str_replace('XXXX',$this->emailN, $email);
        $moUser = new AbaUser();
        $this->assertTrue($moUser->getUserByEmail($email));

        $testTransaction = $this->prepareMockPayment($moUser, $street, $zipCode, $city, $state, COUNTRY_BRAZIL,
           "30_30", "30", $cardKind, $cardName, $cardYear, $cardMonth, $cardNumber, 16.00, "BRL", $cardCvc, $cpfBrasil);

        $moUser->countryId = COUNTRY_BRAZIL;
        $moUser->update(array("countryId"));

        $payment = new PaySupplierAllPagoBr();
        $moLog = $payment->opRunRegistration($testTransaction->id, $testTransaction, $moUser);

        if ($resultTest) {
            $this->assertEquals( "PayGatewayLogAllpago", get_class($moLog), "Transaction was not successful");
            $this->assertTrue(HeMixed::isXmlStructureValid($payment->xmlRequest));
            $this->assertEmpty($payment->getErrorCode());
            $transId = $moLog->getXmlResponse()->xpath("/Response/Transaction/Identification/TransactionID");
            $this->assertEquals($testTransaction->id, strval($transId[0]), "The All Pago Transaction Id not ".
                "equals to our payment attempt.");
            // Registration Id used later on to execute debit payment.
            $this->assertEquals(32, strlen($moLog->getUniqueId()), "The Unique ID from AllPago seems not valid");

            $ret = $this->assertOpRunDebitOnRegistration(array("registrationId"=>$moLog->getUniqueId(),
                                                                "moUser"=>$moUser,
                                                                "testTransaction"=>$testTransaction));
            $this->assertTrue($ret);

            $retReReg = $this->assertOpRunReRegistration(array("registrationId"=>$moLog->getUniqueId(),
                                                        "moUser"=>$moUser,
                                                        "testTransaction"=>$testTransaction));
            $this->assertTrue($retReReg);

        } else{
            $this->assertTrue(false);
        }

        return true;


    }


    /** Executes a debit operation without credit card details nor address or user data.
     * @param array $aRegistration
     *
     * @return bool
     */
    public function assertOpRunDebitOnRegistration( $aRegistration )
    {
        /* @var AbaUser $moUser */
        $moUser = $aRegistration["moUser"];

        /* @var PaymentControlCheck $testTransaction */
        $testTransaction = $aRegistration["testTransaction"];
        $testTransaction->id = HeAbaSHA::generateIdPayment( $moUser );

        $moUserCredit = new AbaUserCreditForms();
        $moUserCredit->setUserCreditForm($moUser->getId(), $testTransaction->kind,
            $testTransaction->cardNumber, $testTransaction->cardYear, $testTransaction->cardMonth,
            $testTransaction->cardCvc, $testTransaction->cardName, 1, $testTransaction->cpfBrasil,
            $aRegistration["registrationId"]);

        $payment = new PaySupplierAllPagoBr();
        $moLog = $payment->opRunDebitOnRG($testTransaction, $moUser, $moUserCredit);

        $this->assertEquals( "PayGatewayLogAllpago", get_class($moLog), "Transaction was not successful");
        $this->assertTrue(HeMixed::isXmlStructureValid($payment->xmlRequest));
        $this->assertEmpty($payment->getErrorCode());
        $transId = $moLog->getXmlResponse()->xpath("/Response/Transaction/Identification/TransactionID");
        $this->assertEquals($testTransaction->id, strval($transId[0]), "The All Pago Transaction Id not ".
            "equals to our payment attempt.");

        return true;

    }

    /** Tests the change of credit card in AllPago payment Gateway. ReRegistration operation.
     *
     * @param $aRegistration
     * @return bool
     */
    public function assertOpRunReRegistration( $aRegistration )
    {
        /* @var AbaUser $moUser */
        $moUser = $aRegistration["moUser"];
        /* @var PaymentControlCheck $testTransaction */
        $testTransaction = $aRegistration["testTransaction"];

        $moUserCredit = new AbaUserCreditForms();
        $moUserCredit->setUserCreditForm($moUser->getId(), $testTransaction->kind,
            $testTransaction->cardNumber, $testTransaction->cardYear, $testTransaction->cardMonth,
            $testTransaction->cardCvc, $testTransaction->cardName, 1, $testTransaction->cpfBrasil,
            $aRegistration["registrationId"]);

        $moNewCredit = new AbaUserCreditForms();
        $moNewCredit->setUserCreditForm($moUser->getId(), KIND_CC_DISCOVER,
            "6011020000245045", "2018", "05",
            "123", $testTransaction->cardName." bis", 1, $testTransaction->cpfBrasil,
            $aRegistration["registrationId"]);

        $paySupplier = new PaySupplierAllPagoBr();
        $moLog = $paySupplier->opRunReRegistration( $moUserCredit, $moNewCredit );

        $this->assertEquals( "PayGatewayLogAllpago", get_class($moLog), "Transaction was not successful");

        return true;
    }

    /**
     * @test PaySupplierAllPagoBr::opRunQuery()
     *
     * @return bool
     */
    public function testOpRunQuery()
    {
        $transQuery = new PaySupplierAllPagoBr();
        // Should retrieve 8 transactions from previous tests:
        $moLog = $transQuery->opRunQuery( HeDate::todaySQL(false).' 00:00:00', HeDate::todaySQL(false).' 23:59:59');

        $this->assertEquals( "PayGatewayLogAllpago", get_class($moLog), "Query was not successful");

        $fileXml = $moLog->getXmlResponse();

        $aNodesXML = $fileXml->xpath("/Response/Result"); ///Response/Result
        $nodeNumber = 0;
        foreach($aNodesXML[0]->Transaction as $nodeTransXML){
            $nodeNumber = $nodeNumber +1 ;
            $code = $nodeTransXML[0]->Processing[0]->xpath("@code");
            $code = strval($code[0]->code);
            $parseStatus = substr(trim(strval($code)),0,5);
            $this->assertTrue($parseStatus=='CC.RF' || $parseStatus=='CC.RG' || $parseStatus=='CC.DB') ;

        }

        return true;
    }


    /**
     * @param PaymentControlCheck $moPayCtrlSuccess
     * @param string $uniqueId
     * @param float $amountPrice
     * @param PayGatewayLog $paymentSupplierLog
     *
     * @return bool
     */
    protected function assertOpRunRefund( $moPayCtrlSuccess, $uniqueId, $amountPrice, PayGatewayLog $paymentSupplierLog )
    {
        $moPaySuccess = new Payment();
        $moPaySuccess->copyFromPayControlCheckToPayment($moPayCtrlSuccess, NULL, $paymentSupplierLog);
        $moPaySuccess->paySuppExtUniId = $uniqueId;

        $moUser = new AbaUser();
        $moUser->getUserById($moPaySuccess->userId);
        $moRefund = clone($moPaySuccess);
        $moRefund->status = PAY_REFUND;
        $moRefund->paySuppExtUniId = NULL;
        $moRefund->amountPrice = $amountPrice;
        $moRefund->id = HeAbaSHA::generateIdPayment($moUser);

        $payAllPago = new PaySupplierAllPagoBr();
        $moLog = $payAllPago->opRunRefund($moRefund, $moPaySuccess);


        $this->assertEquals( "PayGatewayLogAllpago", get_class($moLog), "Transaction was not successful");
        $this->assertTrue(HeMixed::isXmlStructureValid($payAllPago->xmlRequest));

        $this->assertEmpty($payAllPago->getErrorCode());
        $transId = $moLog->getXmlResponse()->xpath("/Response/Transaction/Identification/TransactionID");
        $this->assertEquals($moRefund->paySuppOrderId, strval($transId[0]), "The All Pago Transaction Id not ".
            "equals to our payment attempt.");

        return true;
    }

    /** Set of data for testing credit operations
     * @return array
     */
    public function dataUserPaymentDebit1()
    {
     /** $resultTest, $email, $street, $zipCode, $city, $state, $countryIso2, $idProduct,
        $idPeriodPay, $cardKind, $cardName, $cardYear,$cardMonth, $cardNumber, $amountPrice, $currency
     */
        return array(
            /* POSITIVE */
            '1_PaymentDebit1_unit.test.free.user.br' =>
                array(true,
                'unit.test.free.user.br.XXXX@yopmail.com',
                    'Rua de Foyerteo 458',
                    '08005',
                    'Rio de Janeiro',
                    'Rio de Janeiro',
                    'BR',
                   "30_180",
                   "180",
                   KIND_CC_MASTERCARD,
                   "Test Unit Free Credit Card Aba English",
                   "2018",
                   "05",
                   "5453010000066167",
                   "65",
                   "BRL",
                   "123",
                   "123.456.789-09"
                    ),
            '2_PaymentDebit1_unit.test.free.user.br' =>
                array(true,
                'unit.test.free.user.br.XXXX@yopmail.com',
                'Costa de blancaosao',
                '08005',
                'São Paulo',
                'São Paulo',
                'BR',
                "30_30",
                "30",
                KIND_CC_DISCOVER,
                "Test Unit Free Credit Card Aba English",
                "2018",
                "05",
                "6011020000245045",
                "15.00",
                "BRL",
                "123",
                "123.456.789-09"
            ),
            /* NEGATIVE */
            '3_PaymentDebit1_unit.test.free.user.br' =>
                array(false,
                'unit.test.free.user.br.XXXX@yopmail.com',
                    'Carrer de Guitard 45',
                    '08005',
                    'Barcelona',
                    'BARCELONA',
                    'ES',
                    "30_360",
                    "360",
                    KIND_CC_VISA,
                    "Test Failure Unit Free Credit Card Aba English ",
                    "2018",
                    "05",
                    "4012888888888888",
                    "109",
                    "EUR",
                    "123",
                     "123.456.789-09"
                  ),
            '4_PaymentDebit1_unit.test.free.user.br' =>
                array(false,
                'unit.test.free.user.br.XXXX@yopmail.com',
                    'Carrer de Guitard 45',
                    '08005',
                    'Barcelona',
                    'BARCELONA',
                    'ES',
                    "30_360",
                    "360",
                    KIND_CC_MASTERCARD,
                    "Test Failure Unit Free Credit Card Aba English ",
                    "2018",
                    "05",
                    "5453010000066167",
                    "100",
                    "EUR",
                    "123",
                     "123.456.789-09"
            )
            );
    }

    /** Set of data for testing debit operations
     * @return array
     */
    public function dataUserPaymentDebit2()
    {
        /** $resultTest, $email, $street, $zipCode, $city, $state, $countryIso2, $idProduct,
        $idPeriodPay, $cardKind, $cardName, $cardYear,$cardMonth, $cardNumber, $amountPrice, $currency
         */
        return array(
            /* POSITIVE */
            '1_PaymentDebit1_unit.test.free.user.br' =>
                array(true,
                'unit.test.free.user.br.XXXX@yopmail.com',
                'Rua de Foyerteo 458',
                '08005',
                'Rio de Janeiro',
                'Rio de Janeiro',
                'BR',
                "30_30",
                "30",
                KIND_CC_DINERS_CLUB,
                "Test Unit Free Debit Card  Aba English",
                "2018",
                "05",
                "36490102462661",
                "31",
                "BRL",
                "123",
                "123.456.789-09"
            ),
            '2_PaymentDebit1_unit.test.free.user.br' =>array(true,
                'unit.test.free.user.br.XXXX@yopmail.com',
                'Av. Cristiano Machado, 4000',
                '31110-230',
                'Belo Horizonte',
                'MG',
                'BR',
                "30_360",
                "30",
                KIND_CC_JCB,
                "Test Unit Free Debit Card  Aba English",
                "2018",
                "05",
                "3566007770004971",
                "112",
                "BRL",
                "123",
                "03.847.655/0001-98"
//                "284.763.786-96"
            ));
    }

    /** Set of data for testing registration operations
     * @return array
     */
    public function dataUserPaymentRegistration()
    {
        /** $resultTest, $email, $street, $zipCode, $city, $state, $countryIso2, $cardKind, $cardName,
          $cardYear,$cardMonth, $cardNumber
         */
        return array(
            /* POSITIVE */
            'RG_unit.test.free.user.br.2'=>array(true,
                'unit.test.free.user.br.XXXX@yopmail.com',
                'Rua de Foyerteo 458',
                '08005',
                'Rio de Janeiro',
                'Rio de Janeiro',
                'BR',
                KIND_CC_DISCOVER,
                "Test Unit Free Registration Aba English",
                "2018",
                "05",
                "6011020000245045",
                "123",
                "123.456.789-09"
                ));
    }


}