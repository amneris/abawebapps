<?php
/**
 *
 * Created by Joaquim Forcada (Quino) in Aba English with PHP Storm
 * User: Joaquim Forcada (Quino)
 * Date: 3/12/13
 * Time: 13:48
 *
 * MEXICO ALLPAGO GATEWAY PAYMENT SYSTEM
 * 30: 399,99 MXN
180: 1999,99 MXN
360: 3499,99 MXN
720: 4499,99 MXN

 *
 */
class PaySupplierAllPagoMxTest extends AbaTestCase
{

    /* @var $email string */
    protected $email;
    protected $emailN;

    public function setUp()
    {
        parent::setUp();
        // User FREE specific for this test only:

        $this->emailN = strval(intval(rand(1,1000)));
        $this->email = 'unit.test.free.user.mx.'.$this->emailN.'@yopmail.com';
        $this->createUsersIntoDb( $this->email,
            FREE, COUNTRY_MEXICO, "Unit Free", "Testing Mexico");
        /*
         * Simulate ip location
         */
        // @var GeoIP Yii::app()->geoip
        $ipCgi = '148.204.0.15';
        $moUser = new AbaUser();
        $moUser->getUserByEmail($this->email);
        $geoUserLoc = Yii::app()->geoip->lookupLocation( $ipCgi );
        if ($geoUserLoc) {
            $moUserLoc = new AbaUserLocation();
            $moUserLoc->insertNewLocation($moUser->getId(), $ipCgi, $geoUserLoc->longitude, $geoUserLoc->latitude,
                $geoUserLoc->countryCode, $geoUserLoc->region, $geoUserLoc->city, 1);
        }

    }

    public function tearDown()
    {
        parent::tearDown();
        $moUser = new AbaUser();
        $moUser->getUserByEmail ( $this->email, '', false );
        $moUser->deleteUserThroughResetCall();
    }
    /**
     * @test PaySupplierAllPagomx::opRunCredit
     *
     * @dataProvider dataUserPaymentDebit1
     */
    public function testOpRunDebit1($resultTest, $email, $street, $zipCode, $city, $idCountry, $idProduct,
                                    $idPeriodPay, $cardKind, $cardName, $cardYear,$cardMonth, $cardNumber, $amountPrice,
                                    $currency, $cardCvc)
    {
        $email = str_replace('XXXX',$this->emailN, $email);
        $moUser = new AbaUser();
        $this->assertTrue($moUser->getUserByEmail($email));

        /** @var $testTransaction PaymentControlCheck */
        $testTransaction = $this->prepareMockPayment($moUser, $street, $zipCode, $city, '', $idCountry,
            $idProduct, $idPeriodPay, $cardKind, $cardName, $cardYear, $cardMonth, $cardNumber,
            $amountPrice, $currency, $cardCvc);

        $moUser->countryId = COUNTRY_MEXICO;
        $moUser->update( array("countryId") );

        $payment = new PaySupplierAllPagoMx();
        $moLog = $payment->opRunDebit($testTransaction, $moUser);

        if ($resultTest) {
            $this->assertEquals( "PayGatewayLogAllpago", get_class($moLog));
            $this->assertTrue(HeMixed::isXmlStructureValid($payment->xmlRequest));
            $this->assertEmpty($payment->getErrorCode());
            $transId = $moLog->getXmlResponse()->xpath("/Response/Transaction/Identification/TransactionID");
            $this->assertEquals($testTransaction->id, strval($transId[0]), "The All Pago Transaction Id not ".
                "equals to our payment attempt.");
            $amountReturned = $moLog->getXmlResponse()->xpath("/Response/Transaction/Payment/Clearing/Amount");
            $this->assertEquals(HeMixed::getRoundAmount($amountPrice),
                HeMixed::getRoundAmount((strval($amountReturned[0])) ));
            $fkIdAllpago = $moLog->getUniqueId();
            $this->assertInternalType("string", $fkIdAllpago);
            $this->assertGreaterThan(10, strlen($fkIdAllpago));            
            $this->assertOpRunRefund($testTransaction, $moLog->getUniqueId(),
                HeMixed::getRoundAmount(($amountPrice)), $moLog);

        } else if($cardNumber=='4012888888888888'){
            $this->assertFalse($moLog);
            $this->assertTrue(HeMixed::isXmlStructureValid($payment->xmlRequest));
            $this->assertEquals("100.100.101", $payment->getErrorCode(), 'Expected invalid creditcard, '.
                'bank account number '.
                'bank name 100.100.700.');
            $this->assertEquals("invalid creditcard, bank account number or bank name", $payment->getErrorString());
        } else{
            $this->assertFalse($moLog);
            $this->assertTrue(HeMixed::isXmlStructureValid($payment->xmlRequest));
            //$this->assertEquals("100.100.101", )
            $this->assertRegExp('/\b\d{1,3}\.\d{1,3}\.\d{1,3}/', trim($payment->getErrorCode()));
        }

        return true;
    }

    /**
     * @test PaySupplierAllPagoMx::OpRunDebit
     *
     * @dataProvider dataUserPaymentDebit2
     */
    public function testOpRunDebit2($resultTest, $email, $street, $zipCode, $city, $idCountry, $idProduct,
                                    $idPeriodPay, $cardKind, $cardName, $cardYear,$cardMonth, $cardNumber, $amountPrice,
                                    $currency, $cardCvc)
    {
        $email = str_replace('XXXX',$this->emailN, $email);
        $moUser = new AbaUser();
        $this->assertTrue($moUser->getUserByEmail($email));

        $testTransaction = $this->prepareMockPayment($moUser, $street, $zipCode, $city, '', $idCountry,
            $idProduct, $idPeriodPay, $cardKind, $cardName, $cardYear, $cardMonth, $cardNumber, $amountPrice, $currency,
            $cardCvc, '');

        $moUser->countryId = COUNTRY_MEXICO;
        $moUser->update(array("countryId"));

        $payment = new PaySupplierAllPagoMx();
        $moLog = $payment->opRunDebit($testTransaction, $moUser);

        if ($resultTest) {
            $this->assertEquals( "PayGatewayLogAllpago", get_class($moLog), "Transaction was not successful");
            $this->assertTrue(HeMixed::isXmlStructureValid($payment->xmlRequest));
            $this->assertEmpty($payment->getErrorCode());
            $transId = $moLog->getXmlResponse()->xpath("/Response/Transaction/Identification/TransactionID");
            $this->assertEquals($testTransaction->id, strval($transId[0]), "The All Pago Transaction Id not ".
                "equals to our payment attempt.");

        } else {
            $this->assertFalse($moLog);
            $this->assertTrue(HeMixed::isXmlStructureValid($payment->xmlRequest));
            $this->assertEquals("100.100.700", $payment->getErrorCode(), 'Expected invalid card 100.100.700.');
            $this->assertEquals("invalid cc number/brand combination", $payment->getErrorString());
        }

        return true;
    }

    /**
     * @test PaySupplierAllPagoMx::opRunRegistration
     *
     * @dataProvider dataUserPaymentRegistration
     */
    public function testOpRunRegistration($resultTest, $email, $street, $zipCode, $city, $idCountry,
                                          $cardKind, $cardName, $cardYear,$cardMonth, $cardNumber, $cardCvc)
    {
        $email = str_replace('XXXX',$this->emailN, $email);
        $moUser = new AbaUser();
        $this->assertTrue($moUser->getUserByEmail($email));

        $testTransaction = $this->prepareMockPayment($moUser, $street, $zipCode, $city, '', $idCountry,
            $idCountry.'_30', '30', $cardKind, $cardName, $cardYear, $cardMonth, $cardNumber,  399.99, 'MXN', $cardCvc);

        $moUser->countryId = COUNTRY_MEXICO;
        $moUser->update( array("countryId") );

        $payment = new PaySupplierAllPagoMx();
        $moLog = $payment->opRunRegistration($testTransaction->id, $testTransaction, $moUser);

        if ($resultTest) {
            $this->assertEquals( "PayGatewayLogAllpago", get_class($moLog), "Transaction was not successful");
            $this->assertTrue(HeMixed::isXmlStructureValid($payment->xmlRequest));
            $this->assertEmpty($payment->getErrorCode());
            $transId = $moLog->getXmlResponse()->xpath("/Response/Transaction/Identification/TransactionID");
            $this->assertEquals($testTransaction->id, strval($transId[0]), "The All Pago Transaction Id not ".
                "equals to our payment attempt.");
            // Registration Id used later on to execute debit payment.
            $this->assertEquals(32, strlen($moLog->getUniqueId()), "The Unique ID from AllPago seems not valid");

            $ret = $this->assertOpRunDebitOnRegistration(array("registrationId"=>$moLog->getUniqueId(),
                "moUser"=>$moUser,
                "testTransaction"=>$testTransaction));
            $this->assertTrue($ret);

            $retReReg = $this->assertOpRunReRegistration(array("registrationId"=>$moLog->getUniqueId(),
                "moUser"=>$moUser,
                "testTransaction"=>$testTransaction));
            $this->assertTrue($retReReg);

        } else{
            $this->assertTrue(false);
        }

        return true;


    }


    /** Executes a debit operation without credit card details nor address or user data.
     * @param array $aRegistration
     *
     * @return bool
     */
    public function assertOpRunDebitOnRegistration( $aRegistration )
    {
        /* @var AbaUser $moUser */
        $moUser = $aRegistration["moUser"];

        /* @var PaymentControlCheck $testTransaction */
        $testTransaction = $aRegistration["testTransaction"];
        $testTransaction->id = HeAbaSHA::generateIdPayment( $moUser );

        $moUserCredit = new AbaUserCreditForms();
        $moUserCredit->setUserCreditForm($moUser->getId(), $testTransaction->kind,
            $testTransaction->cardNumber, $testTransaction->cardYear, $testTransaction->cardMonth,
            $testTransaction->cardCvc, $testTransaction->cardName, 1, '',
            $aRegistration["registrationId"]);

        $payment = new PaySupplierAllPagoMx();
        $moLog = $payment->opRunDebitOnRG($testTransaction, $moUser, $moUserCredit);

        $this->assertEquals( "PayGatewayLogAllpago", get_class($moLog), "Transaction was not successful");
        $this->assertTrue(HeMixed::isXmlStructureValid($payment->xmlRequest));
        $this->assertEmpty($payment->getErrorCode());
        $transId = $moLog->getXmlResponse()->xpath("/Response/Transaction/Identification/TransactionID");
        $this->assertEquals($testTransaction->id, strval($transId[0]), "The All Pago Transaction Id not ".
            "equals to our payment attempt.");

        return true;

    }

    /** Tests the change of credit card in AllPago payment Gateway. ReRegistration operation.
     *
     * @param $aRegistration
     * @return bool
     */
    public function assertOpRunReRegistration( $aRegistration )
    {
        /* @var AbaUser $moUser */
        $moUser = $aRegistration["moUser"];
        /* @var PaymentControlCheck $testTransaction */
        $testTransaction = $aRegistration["testTransaction"];

        $moUserCredit = new AbaUserCreditForms();
        $moUserCredit->setUserCreditForm($moUser->getId(), $testTransaction->kind,
            $testTransaction->cardNumber, $testTransaction->cardYear, $testTransaction->cardMonth,
            $testTransaction->cardCvc, $testTransaction->cardName, 1, '',
            $aRegistration["registrationId"]);

        $moNewCredit = new AbaUserCreditForms();
        $moNewCredit->setUserCreditForm($moUser->getId(), KIND_CC_MASTERCARD,
            DataCollections::$aCreditForms['MasterCard'][2], DataCollections::$aCreditForms['MasterCard'][3],
            DataCollections::$aCreditForms['MasterCard'][4], DataCollections::$aCreditForms['MasterCard'][5],
            $testTransaction->cardName." bis", 1, '',
            $aRegistration["registrationId"]);

        $paySupplier = new PaySupplierAllPagoMx();
        $moLog = $paySupplier->opRunReRegistration( $moUserCredit, $moNewCredit );

        $this->assertEquals( "PayGatewayLogAllpago", get_class($moLog), "Transaction was not successful");

        return true;
    }

    /**
     * @test PaySupplierAllPagoMx::opRunQuery()
     *
     * @return bool
     */
    public function testOpRunQuery()
    {
        $transQuery = new PaySupplierAllPagoMx();
        // Should retrieve 8 transactions from previous tests:
        $moLog = $transQuery->opRunQuery( HeDate::todaySQL(false).' 00:00:00', HeDate::todaySQL(false).' 23:59:59');

        $this->assertEquals( "PayGatewayLogAllpago", get_class($moLog), "Query was not successful");

        $fileXml = $moLog->getXmlResponse();

        $aNodesXML = $fileXml->xpath("/Response/Result"); ///Response/Result
        $nodeNumber = 0;
        foreach($aNodesXML[0]->Transaction as $nodeTransXML){
            $nodeNumber = $nodeNumber +1 ;
            $code = $nodeTransXML[0]->Processing[0]->xpath("@code");
            $code = strval($code[0]->code);
            $parseStatus = substr(trim(strval($code)),0,5);
            $this->assertTrue($parseStatus=='CC.RF' || $parseStatus=='CC.RG' || $parseStatus=='CC.DB') ;

        }

        return true;
    }

    /**
     * @param PaymentControlCheck $moPayCtrlSuccess
     * @param string $uniqueId
     * @param float $amountPrice
     * @param PayGatewayLog $paymentSupplierLog
     *
     * @return bool
     */
    protected function assertOpRunRefund( $moPayCtrlSuccess, $uniqueId, $amountPrice, PayGatewayLog $paymentSupplierLog )
    {
        $moPaySuccess = new Payment();
        $moPaySuccess->copyFromPayControlCheckToPayment($moPayCtrlSuccess, NULL, $paymentSupplierLog);
        $moPaySuccess->paySuppExtUniId = $uniqueId;

        $moUser = new AbaUser();
        $moUser->getUserById($moPaySuccess->userId);
        $moRefund = clone($moPaySuccess);
        $moRefund->status = PAY_REFUND;
        $moRefund->paySuppExtUniId = NULL;
        $moRefund->amountPrice = $amountPrice;
        $moRefund->id = HeAbaSHA::generateIdPayment($moUser);

        $payAllPago = new PaySupplierAllPagoMx();
        $moLog = $payAllPago->opRunRefund($moRefund, $moPaySuccess);


        $this->assertEquals( "PayGatewayLogAllpago", get_class($moLog), "Transaction was not successful");
        $this->assertTrue(HeMixed::isXmlStructureValid($payAllPago->xmlRequest));

        $this->assertEmpty($payAllPago->getErrorCode());
        $transId = $moLog->getXmlResponse()->xpath("/Response/Transaction/Identification/TransactionID");
        $this->assertEquals($moRefund->paySuppOrderId, strval($transId[0]), "The All Pago Transaction Id not ".
            "equals to our payment attempt.");

        return true;
    }

    /** Set of data for testing credit operations
     * @return array
     */
    public function dataUserPaymentDebit1()
    {
        /** $resultTest, $email, $street, $zipCode, $city, $state, $countryIso2, $idProduct,
        $idPeriodPay, $cardKind, $cardName, $cardYear,$cardMonth, $cardNumber, $amountPrice, $currency
         */
        return array(
            /* POSITIVE */
            'Debit_1_MX_AllPagoTest' =>
                array( true,
                    'unit.test.free.user.mx.XXXX@yopmail.com',
                    'Callejo Champatán',
                    '08005',
                    'Mexico City',
                    COUNTRY_MEXICO,
                    ''.COUNTRY_MEXICO.'_180',
                    '180',
                    KIND_CC_MASTERCARD,
                    'Test Unit Free  MX Credit Card Aba English Mexico',
                    "2018",
                    "05",
                    "KIND_CC_MASTERCARD",
                    1999.00,
                    'MXN',
                    "123"
                ),
            /* NEGATIVE */
            'Debit_2_MX_AllPagoTest' =>
                array( false,
                'unit.test.free.user.mx.XXXX@yopmail.com',
                'Costa de blancaosao',
                '08005',
                'Mexico City',
                COUNTRY_MEXICO,
                ''.COUNTRY_MEXICO.'_180',
                ''.COUNTRY_MEXICO,
                KIND_CC_DISCOVER,
                "Test Unit Free  MX Credit Card Aba English Mexico",
                "2018",
                "05",
                "6011020000245045",
                1999.00,
                "MXN",
                "123",
            ),
        );
    }

    /** Set of data for testing debit operations
     * @return array
     */
    public function dataUserPaymentDebit2()
    {
        /** $resultTest, $email, $street, $zipCode, $city, $countryIso2, $idProduct,
        $idPeriodPay, $cardKind, $cardName, $cardYear,$cardMonth, $cardNumber, $amountPrice, $currency
         */
        return array(
            /* POSITIVE */
            'Debit2_1_MX_AllPagoTest' =>
                array( true,
                'unit.test.free.user.mx.XXXX@yopmail.com',
                'Costa de blancaosao',
                '08005',
                'Mexico City',
                COUNTRY_MEXICO,
                ''.COUNTRY_MEXICO.'_30',
                '30',
                KIND_CC_VISA,
                'Test Unit Free MX Debit Card Aba English',
                "2018",
                "05",
                "4012001038443335",
                399.00,
                'MXN',
                "123"
            ),
            /* NEGATIVE */
            'Debit2_2_MX_AllPagoTest' =>
                array( false,
                'unit.test.free.user.mx.XXXX@yopmail.com',
                'Costa de blancaosao',
                '08005',
                'Mexico City',
                COUNTRY_MEXICO,
                ''.COUNTRY_MEXICO.'_30',
                '30',
                KIND_CC_DINERS_CLUB,
                'Test Unit Free MX Debit Card  Aba English MX',
                "2018",
                "05",
                "6011020000245045",
                399.00,
                'MXN',
                "123"
            ));
    }

    /** Set of data for testing registration operations
     * @return array
     */
    public function dataUserPaymentRegistration()
    {
        /** $resultTest, $email, $street, $zipCode, $city, $state, $countryIso2, $cardKind, $cardName,
        $cardYear,$cardMonth, $cardNumber
         */
        return array(
            /* POSITIVE */
            'Debit_1_MX_AllPagoRegistration' =>
                array( true,
                'unit.test.free.user.mx.XXXX@yopmail.com',
                'Costa de blancaosao',
                '08005',
                'Mexico City',
                COUNTRY_MEXICO,
                KIND_CC_VISA,
                'Test Unit Free MX Debit Card  Aba English MX',
                "2018",
                "05",
                "4012001038443335",
                "123"
            ));
    }


}