<?php
/**
 * Class PaySupplierAdyenTest
 *
 * * en caso de adyen no se puede hacer el test UNITARIO. se necesita el campo "adyen-encrypted-data" encriptado desde el front con datos de pago
 *
 */

class PaySupplierAdyenTest extends AbaTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test PaySupplierAdyen::runDebiting
     *
     * @dataProvider dataUserPaymentDebit
     */
    public function testRunDebiting( $resultTest, $email, $idProduct, $idPeriodPay, $cardKind, $cardName,
                                     $cardYear, $cardMonth, $cardNumber, $amountPrice, $currency, $cardCvc )
    {
        $moUser = new AbaUser();
        $this->assertTrue($moUser->getUserByEmail($email));

        $testTransaction = $this->prepareMockPayment($moUser, "", "", "", "", COUNTRY_SPAIN, $idProduct, $idPeriodPay, $cardKind,
            $cardName, $cardYear, $cardMonth, $cardNumber, $amountPrice, $currency, $cardCvc, "");

        $paySupplier = new PaySupplierAdyen();
        /* @var $moLog PayGatewayLogAdyenResRec */
        $moLog = $paySupplier->runDebiting($testTransaction, "A", false);

        if ($resultTest) {
            $this->assertInstanceOf( PayGatewayLog, $moLog);
//            $this->assertTrue(HeMixed::isXmlStructureValid($moLog->XML));
            $this->assertEmpty($paySupplier->getErrorCode());
            $this->assertEquals($testTransaction->id, $moLog->idPayment, "Id Payment from Log should be equal to payment");
            $this->assertNotEmpty($moLog->authCode);
            $this->assertEquals( $moLog->Order_number, $moLog->getUniqueId());
            $this->assertEquals( $moLog->reference, $moLog->getOrderNumber());
//            $this->assertEquals(2, $moLog->Terminal);
            $this->assertEquals('ABAEnglishCOM', $moLog->MerchantCode);
            $this->assertEquals('Authorised', $moLog->resultCode);

            //?!?!?
            return $this->assertRunRefund( $testTransaction, $moLog->getUniqueId(), HeMixed::getRoundAmount(($amountPrice)), $moLog );

        } else {
            $this->assertFalse($moLog);
            $this->assertNotEmpty($paySupplier->getErrorCode());
            $moLog = $paySupplier->getMoLogResponse();
//            $this->assertTrue(HeMixed::isXmlStructureValid($moLog->XML));
            $this->assertEquals('Refused', $moLog->resultCode);
            $this->assertEquals( $moLog->reference, $moLog->getOrderNumber());
            $this->assertEquals( $moLog->Order_number, $moLog->getUniqueId());
        }

        return true;
    }

    /**
     * @param PaymentControlCheck $moPayCtrlSuccess
     * @param $uniqueId
     * @param $amountPrice
     * @param PayGatewayLog $paymentSupplierLog
     * @return bool
     */
    public function assertRunRefund( PaymentControlCheck $moPayCtrlSuccess, $uniqueId, $amountPrice,
                                     PayGatewayLog $paymentSupplierLog )
    {
        $moPaySuccess = new Payment();
        $moPaySuccess->copyFromPayControlCheckToPayment($moPayCtrlSuccess, NULL, $paymentSupplierLog);
        $moPaySuccess->paySuppExtUniId = $uniqueId;

        $moUser = new AbaUser();
        $moUser->getUserById($moPaySuccess->userId);
        $moRefund = clone($moPaySuccess);
        $moRefund->status = PAY_REFUND;
        $moRefund->paySuppExtUniId = NULL;
        $moRefund->amountPrice = $amountPrice;
        $moRefund->id = HeAbaSHA::generateIdPayment($moUser);

        $payAdyen = new PaySupplierAdyen();
        $moLog =    $payAdyen->runRefund($moRefund, $moPaySuccess);

        $this->assertEquals( "PayGatewayLogAdyenResRec", get_class($moLog), "Transaction was not successful");
//        $this->assertTrue(HeMixed::isXmlStructureValid($moLog->XML));
        $this->assertEmpty(  $payAdyen->getErrorCode(), 'There should be no error.');
        $this->assertEquals( $moRefund->paySuppOrderId, strval($moLog->getOrderNumber()), 'Order number dows not match.' );
        $this->assertEquals( $moPayCtrlSuccess->paySuppExtUniId, strval($moLog->getUniqueId()), 'ExtUnid should be the same os the payment referenced' );

        return true;
    }

    /** Data samples for Adyen Tests
     *
     * @return array
     */
    public function dataUserPaymentDebit()
    {
        /** $resultTest, $email, $idProduct, $idPeriodPay, $cardKind, $cardName, $cardYear,
          $cardMonth, $cardNumber, $amountPrice, $currency */
        return array(
            /* NEGATIVE */
            array(false,"unit.test.premium.user@yopmail.com",
                "47_180",
                "180",
                KIND_CC_NO_CARD, // 4988 4388 4388 4305	Classic	ES
                "Test Unit Free Adyen Credit Card Aba English",
                "2016",
                "06",
                "4988438843884305",
                "99.99",
                "USD",
                "737"),
            /* NEGATIVE */
            array(false,"unit.test.free.user@yopmail.com",
                "73_180",
                "180",
                KIND_CC_NO_CARD, // 374251021090003	Amex
                "Test Unit Free Adyen Credit Card Aba English",
                "2016",
                "06",
                "374251021090003",
                "89.99",
                "USD",
                "737"),
            /* NEGATIVE */
            array(false,"unit.test.deleted.user@yopmail.com",
                "73_30",
                "30",
                KIND_CC_NO_CARD,
                "Test Failure Unit Free Adyen Credit Card Aba English ",
                "2026",
                "01",
                "4548812049400004",
                "79.88",
                "EUR",
                "285"),
        );
    }

}
 