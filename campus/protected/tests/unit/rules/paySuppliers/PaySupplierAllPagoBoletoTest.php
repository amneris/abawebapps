<?php
/**
 * /**
 * Created by Joaquim Forcada (Quino) in Aba English with PHP Storm
 * User: Joaquim Forcada (Quino)
 * Date: 02/12/2014
 *
 */
class PaySupplierAllPagoBoletoTest extends AbaTestCase
{
    /* @var $email string */
    protected $email;
    protected $emailN;

    public function setUp()
    {
        parent::setUp();
        // User FREE specific for this test only:

        $this->emailN = strval(intval(rand(1,1000)));
        $this->email = 'unit.test.free.user.boleto.'.$this->emailN.'@yopmail.com';
        $this->createUsersIntoDb( $this->email,
            FREE, COUNTRY_BRAZIL, "Unit Free", "Testing Brazil");
    }

    public function tearDown()
    {
        parent::tearDown();
        $moUser = new AbaUser();
        $moUser->getUserByEmail ( $this->email, '', false );
        $moUser->deleteUserThroughResetCall();
    }

    /**
     * @test PaySupplierAllPagoBoleto::opRunCredit
     *
     * @dataProvider dataUserPaymentDebit1
     */
    public function testOpRunDebit1( $resultTest, $email, $firstName, $secondName, $street, $zipCode, $city, $state,
                                    $countryIso2, $idProduct, $idPeriodPay, $amountPrice, $currency, $cpfBrasil)
    {
        $email = str_replace('XXXX',$this->emailN, $email);
        $moUser = new AbaUser();
        $this->assertTrue($moUser->getUserByEmail($email));

        /** @var $testTransaction PaymentControlCheck */
        $testTransaction = $this->prepareMockPayment($moUser, $street, $zipCode, $city, $state, COUNTRY_BRAZIL,
                                                $idProduct, $idPeriodPay, '', '', '', '', '', $amountPrice, $currency, '', $cpfBrasil);

        $moUser->countryId = COUNTRY_BRAZIL;
        $moUser->update( array("countryId") );

        $payment = new PaySupplierAllPagoBoleto();
        $moLog = $payment->opRunDebit($testTransaction, $moUser);

        if ($resultTest) {
            $this->assertEquals( "PayGatewayLogAllpago", get_class($moLog));
            $this->assertTrue(HeMixed::isXmlStructureValid($payment->xmlRequest));
            $this->assertEmpty($payment->getErrorCode());
            $transId = $moLog->getXmlResponse()->xpath("/Response/Transaction/Identification/TransactionID");
            $this->assertEquals($testTransaction->id, strval($transId[0]), "The All Pago Transaction Id not ".
                "equals to our payment attempt.");
            $amountReturned = $moLog->getXmlResponse()->xpath("/Response/Transaction/Payment/Clearing/Amount");
            $this->assertEquals(HeMixed::getRoundAmount($amountPrice),
                                HeMixed::getRoundAmount((strval($amountReturned[0])) ));
            $fkIdAllpago = $moLog->getUniqueId();
            $this->assertInternalType("string", $fkIdAllpago);
            $this->assertGreaterThan(10, strlen($fkIdAllpago));
            $this->assertOpRunRefund($testTransaction, $moLog->getUniqueId(),
                                            HeMixed::getRoundAmount(($amountPrice)), $moLog);

        } else {
            $this->assertFalse($moLog);
            $this->assertTrue(HeMixed::isXmlStructureValid($payment->xmlRequest));
            //$this->assertEquals("100.100.101", )
            $this->assertRegExp('/\b\d{1,3}\.\d{1,3}\.\d{1,3}/', trim($payment->getErrorCode()));
        }

        return true;
    }



    /**
     * @test PaySupplierAllPagoBoleto::opRunQuery()
     *
     * @return bool
     */
    public function testOpRunQuery()
    {
        $transQuery = new PaySupplierAllPagoBoleto();
        // Should retrieve 8 transactions from previous tests:
        $moLog = $transQuery->opRunQuery( HeDate::todaySQL(false).' 00:00:00', HeDate::todaySQL(false).' 23:59:59');

        $this->assertEquals( "PayGatewayLogAllpago", get_class($moLog), "Query was not successful");

        $fileXml = $moLog->getXmlResponse();

        $aResult = $fileXml->xpath("/Response/Result"); ///Response/Result
        $number = $aResult[0]->count;

        $aNodesXML = $fileXml->xpath("/Response/Result"); ///Response/Result
        $nodeNumber = 0;
        foreach($aNodesXML[0]->Transaction as $nodeTransXML){
            $nodeNumber = $nodeNumber +1 ;
            $code = $nodeTransXML[0]->Processing[0]->xpath("@code");
            $code = strval($code[0]->code);
            $parseStatus = substr(trim(strval($code)),0,5);
            $this->assertTrue( $parseStatus=='PP.RC' ) ;

            $descriptor = $nodeTransXML[0]->Payment[0]->xpath("Clearing/Descriptor");
            $strDesc = strval($descriptor[0]);

        }

        return true;
    }


    /**
     * @param PaymentControlCheck $moPayCtrlSuccess
     * @param string $uniqueId
     * @param float $amountPrice
     * @param PayGatewayLog $paymentSupplierLog
     *
     * @return bool
     */
    protected function assertOpRunRefund( $moPayCtrlSuccess, $uniqueId, $amountPrice, PayGatewayLog $paymentSupplierLog )
    {
        $moPaySuccess = new Payment();
        $moPaySuccess->copyFromPayControlCheckToPayment($moPayCtrlSuccess, NULL, $paymentSupplierLog);
        $moPaySuccess->paySuppExtUniId = $uniqueId;

        $moUser = new AbaUser();
        $moUser->getUserById($moPaySuccess->userId);
        $moRefund = clone($moPaySuccess);
        $moRefund->status = PAY_REFUND;
        $moRefund->paySuppExtUniId = NULL;
        $moRefund->amountPrice = $amountPrice;
        $moRefund->id = HeAbaSHA::generateIdPayment($moUser);

        $payAllPago = new PaySupplierAllPagoBoleto();
        $moLog = $payAllPago->opRunRefund($moRefund, $moPaySuccess);

        $this->assertFalse($moLog);

        return true;
    }

    /** Set of data for testing credit operations
     * @return array
     */
    public function dataUserPaymentDebit1()
    {
     /** $resultTest, $email, $firstName, $secondName, $street, $zipCode, $city, $state,
      * $countryIso2, $idProduct, $idPeriodPay, $amountPrice, $currency, $cpfBrasil
     */
        return array(
            /* POSITIVE */
            '1_PaymentBoleto1_unit.test.free.user.boleto' =>
                array(true,
                'unit.test.free.user.boleto.XXXX@yopmail.com',
                'Name Unit',
                'LastName Test',
                'Rua de Foyerteo 458',
                '01332000',
                'Rio de Janeiro',
                'SP',
                'BR',
               '30_180',
               '180',
               '68.00',
               'BRL',
               '123.456.789-09'
                    ),
            '2_PaymentBoleto1_unit.test.free.user.boleto' =>
                array(true,
                'unit.test.free.user.boleto.XXXX@yopmail.com',
                'Name Unit',
                'LastName Test',
                'Costa de blancaosao',
                '01332000',
                'São Paulo',
                'SP',
                'BR',
                '30_30',
                '30',                
                '15.00',
                'BRL',
                '123.456.789-09'
            ),
            /* NEGATIVE */
           /* '3_PaymentBoleto1_unit.test.free.user.boleto' =>
                array(false,
                    'unit.test.free.user.boleto.XXXX@yopmail.com',
                    'Name Unit',
                    'LastName Test',
                    'Carrer de Guitard 45',
                    '01332000',
                    'Barcelona',
                    'BARCELONA',
                    'ES',
                    '30_360',
                    '360',
                    '540.00',
                    'EUR',
                    '123.456.789-09'
                  ),*/
            /*'4_PaymentBoleto1_unit.test.free.user.boleto' =>
                array(false,
                    'unit.test.free.user.boleto.XXXX@yopmail.com',
                    'Name Unit',
                    'LastName Test',
                    'Carrer de Guitard 45',
                    '01332000',
                    'Barcelona',
                    'BARCELONA',
                    'ES',
                    "30_360",
                    "360",
                    '540.00',
                    "EUR",
                    "123.456.789-09"
            )*/
            );
    }


}