<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abaenglish
 * Date: 24/12/13
 * Time: 13:11
 * To change this template use File | Settings | File Templates.
 */
class OnlinePayCommonTest extends AbaTestCase
{
    public function setUp()
    {
        parent::setUp();

        /* USER TEST:  FREE ---*/
        $this->createUsersIntoDb("unit.test.free.onlinepay@yopmail.com", FREE,
            199, "UnitFree", "Testing", true);
        /* -----    END FREE   */
    }

    public function tearDown()
    {
        parent::tearDown();

        $moUser = new AbaUser();
        if ($moUser->getUserByEmail("unit.test.free.onlinepay@yopmail.com", "", false)) {
            $moUser->deleteUserThroughResetCall();
        }
    }


    /**
     *
     */
    public function testSavePaymentBeforeGateway()
    {
        // Arguments In : PaymentForm &$modFormPayment, $paySuppExtId, AbaUser $user
        $modFormPayment = new PaymentForm();
        $modFormPayment->firstName = "UnitTest";
        $modFormPayment->secondName = "Online Pay Test ";
        $modFormPayment->mail = "unit.test.free.onlinepay@yopmail.com";
        $modFormPayment->idPayMethod = PAY_METHOD_PAYPAL;
        $modFormPayment->creditCardType = KIND_CC_PAYPAL;
        $modFormPayment->acceptConditions = 1;
        // $productPrice->idProduct.DELIMITER_KEYS.$productPrice->idCountry.DELIMITER_KEYS.$productPrice->idPeriodPay,
        $modFormPayment->packageSelected = "199_30".DELIMITER_KEYS."199".DELIMITER_KEYS."30";
        $paySuppExtId = PAY_SUPPLIER_PAYPAL;
        $moUser = new AbaUser();
        $moUser->getUserByEmail("unit.test.free.onlinepay@yopmail.com");
        Yii::app()->user->id = $moUser->id;

        $commOnlinePay = new OnlinePayCommon();
        $moPayControl = $commOnlinePay->savePaymentBeforeGateway($modFormPayment, $paySuppExtId, $moUser, 300001);

        // Assertions
        $this->assertEquals( "PaymentControlCheck", get_class($moPayControl));
        $this->assertEquals(KIND_CC_PAYPAL, $moPayControl->kind);

        $moPayCtrlDraft = new PaymentControlCheck();
        $moPayCtrlDraft->getPaymentById($moPayControl->id);
        $moPayControl = null;
        $this->assertEquals("199_30", $moPayCtrlDraft->idProduct);
        $this->assertEmpty( $moPayCtrlDraft->idPromoCode );
        $this->assertEquals( PAY_INITIATED, $moPayCtrlDraft->status );

        $this->assertTrue(true);
    }


    /**
     * @dataProvider dataCardsSamples
     *
     * @test OnlinePayCommon::saveCreditFormData
     *
     * @param bool $success
     * @param $creditCardType
     * @param $creditCardNumber
     * @param $creditCardYear
     * @param $creditCardMonth
     * @param $CVC
     * @param $creditCardName
     * @param $cpfBrasil
     * @param $typeCpf
     */
    public function testSaveCreditCardData( $success, $creditCardType, $creditCardNumber, $creditCardYear,
                                            $creditCardMonth, $CVC, $creditCardName, $cpfBrasil, $typeCpf )
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail("unit.test.free.onlinepay@yopmail.com", "", false);

        $errorMsg = false;
        $commOnlinePay = new OnlinePayCommon();
        $aDataPayMethod = array("creditCardType"=> $creditCardType, "creditCardNumber"=> $creditCardNumber,
                        "creditCardYear"=>$creditCardYear, "creditCardMonth"=> $creditCardMonth, "CVC"=> $CVC,
                        "creditCardName"=> $creditCardName, "cpfBrasil"=> $cpfBrasil , "typeCpf" => $typeCpf);
        $userCredit = $commOnlinePay->saveCreditFormData( $aDataPayMethod, $errorMsg, $moUser);
        if ( $success ){
            $this->assertEquals("AbaUserCreditForms", get_class($userCredit));
            $this->assertEquals( $creditCardNumber, $userCredit->cardNumber  );
            $this->assertEquals( $cpfBrasil, $userCredit->cpfBrasil );
            $this->assertEquals( $creditCardType, $userCredit->kind);
            $this->assertFalse($errorMsg);
            if(KIND_CC_PAYPAL==$creditCardType){
                $this->assertEmpty($userCredit->cardCvc);
            }
        } else{
            $this->assertFalse($userCredit);
            $this->assertStringStartsWith(Yii::t('mainApp', 'errorsavepayment_key'), $errorMsg);
        }


    }


}
