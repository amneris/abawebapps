<?php
/**
 * Created by Joaquim Forcada (Quino) in Aba English
 * User: Joaquim Forcada (Quino)
 * Date: 14/03/14
 * Time: 13:51
 
 ---Please brief description here---- 
 */

class RegisterUsersCommonTest  extends AbaTestCase
{

    public function setUp()
    {
        parent::setUp();
    }

    public function tearDown()
    {
        parent::tearDown();
    }


    public function testRegisterNewStandardUser()
    {
        $this->assertTrue(true);
    }

    /**
     * @test
     *
     * @dataProvider dataEmails
     *
     * @param $email
     *
     * @return bool
     */
    public function testGenRandomPasswordNewUser($email)
    {
        $commRegisterUser = new RegisterUsersCommon();
        $newPassword = $commRegisterUser->genRandomPasswordNewUser($email, 'TT');

        $this->assertStringStartsWith("TT", $newPassword);
        $this->assertEquals(10, strlen($newPassword));


        return true;
    }

    /** Data for changes of password
     *
     * @return array
     */
    public function dataEmails()
    {
        return array(
            "usuario 1 test"=>array("usuario 1 test"=>"unit.test.free@yopmail.com"),
            "usuario 2 test"=>array("usuario 2 test"=>"unit.test.premium_A@yopmail.com"),
            "usuario 3 test"=>array("usuario 3 test"=>"unit@yop.com"),
        );
    }



}
