<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abaenglish
 * Date: 8/04/13
 * Time: 17:02
 * To change this template use File | Settings | File Templates.
 */
class HeUnitsTest extends AbaTestCase
{
    /**
     * @dataProvider levels
     */
    public function testGetStartUnitByLevel($level)
    {
        $retEnd = HeUnits::getStartUnitByLevel($level);

        $this->assertInternalType("integer", $retEnd, "Integer Type expected");

    }

    /**
     * @dataProvider levels
     */
    public function testGetEndUnitByLevel($level)
    {
        $retEnd = HeUnits::getEndUnitByLevel($level);

        $this->assertInternalType("integer", $retEnd, "Integer Type expected");
    }

    /**
     * @dataProvider keysStrings
     */
    public function testGetVideoClassTitleByUnit($key)
    {
        $trans = HeUnits::getVideoClassTitleByUnit($key);

        $this->assertInternalType("string", $trans);

        $this->assertNotEquals($trans, $key, "No translation for " . $key);
    }

    /** Tests same function name
     *
      */
    public function testGetArrayStartUnitsByLevel()
    {
        $aTestUnits = HeUnits::getArrayStartUnitsByLevel(1,3);

        $this->assertSameSize(array(1=>1,2=>25,3=>49), $aTestUnits);

        $this->assertEquals((3),count($aTestUnits));

    }

    public function levels()
    {
        return array(array(4), array(5), array(146));
    }

    public function keysStrings()
    {
        return array(array("key_title_subject_002"),
            array("key_title_subject_005"),
            array("key_title_subject_144"));
    }

}
