<?php
/**
 *
 */
class HeAnalyticsTest extends AbaTestCase
{

    /* @var ReflectionClass extends HeAnalytics $heAnalytics */
    public $heAnalytics;

    public function setUp()
    {
//        parent::setUp();
        // FREE
        $this->createUsersIntoDb("unit.test.free.zanox@yopmail.com", FREE, Yii::app()->config->get("ABACountryId"),
                                                                "UnitFree", "Testing");
        $moUser = new AbaUser();
        $moUser->getUserByEmail("unit.test.free.zanox@yopmail.com");
        $moUser->idPartnerSource = PARTNER_ID_ZANOX_CPL;
        $moUser->update( array("idPartnerSource") );

        Yii::app()->user->id = $moUser->id;
        parent::setUp();

        $this->heAnalytics = new ReflectionClass('HeAnalytics');
    }

    public function tearDown()
    {
        parent::tearDown();

        $moUser = new AbaUser();
        if($moUser->getUserByEmail("unit.test.free.zanox@yopmail.com","",false)){
            $moUser->deleteUserThroughResetCall();
        }
    }


    public function testRetScriptGoogleConversionPurchase()
    {
        $amountTest = "159.99";
        $currencyTrans = "EUR";
        $xRateToEUR = "1.00";
    }


    /**
     * @tests retScriptGoogleConversionRegistrationItalyReMark
     * @return bool
     */
    public function testRetScriptGoogleConversionRegistrationItalyReMark()
    {
        $_GET["idpartner"] = PARTNER_ID_ADWORDS_ITALY;

        return true;
    }


    public function testRetScriptGoogleECommerceConversionPurchase()
    {
        $id=111;
        $idPartner=300001;
        $idCountry=199;
        $amountPrice = "9.99";
        $idProduct = "199_30";
        $currencyTrans = "EUR";
        $xRateToEUR = "2.00";
        $idPromoCode = "";
    }

     /**
     * Tests function HeAnalytics::retPixelNetAffiliationConversionRegistration() when
     * it should not output any tag.
     */
    public function testNotRetPixelNetAffiliationConversionRegistration()
    {
        $_GET["idpartner"] = 60000;
    }

    /**
     * Test private method of HeAnalytics, Output string
     */
    public function testRetScriptZanoxCPLConversionRegistration()
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail("unit.test.free.zanox@yopmail.com");

        $method = $this->heAnalytics->getMethod('retScriptZanoxCPLConversionRegistration');
        $method->setAccessible(true);

        $this->assertEquals(PARTNER_ID_ZANOX_CPL, $moUser->idPartnerSource);
        $this->assertEquals( Yii::app()->user->id, $moUser->id);

        $this->expectOutputRegex("/(.*)(retScriptZanoxCPLConversionRegistration)(.*)/s");
        $this->assertTrue( $method->invokeArgs($this->heAnalytics,array()) );

    }

    /**
     * @tests retScriptGoogleConversionRegistrationGermany
     * @return bool
     */
    public function testRetScriptGoogleConversionRegistrationGermany()
    {
        $_GET["idpartner"] = PARTNER_ID_ADWORDS_GE;

        return true;
    }

}