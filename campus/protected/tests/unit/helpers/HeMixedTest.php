<?php
/**
 *
 */
class HeMixedTest extends AbaTestCase
{

    public function setUp()
    {
        parent::setUp();
    }

    public function tearDown()
    {
        parent::tearDown();
    }


    public function testGetNameKnownHost(  )
    {
        $this->assertEquals('www.abaenglish.com', HeMixed::getNameKnownHost('172.16.1.125') );


        return true;

    }

    /** Tests function same name
     * @test
     * @return bool
     */
    public function testIsValidCreditCard()
    {
        $typeOfValidation = Yii::app()->config->get("PAY_ENFORCE__LEVEL_CARD");

        $this->assertTrue( HeMixed::isValidCreditCard('5540500001000004') );

        if( $typeOfValidation==PAY_VALID_CARD_LEVEL_MEDIUM){
            $this->assertTrue( HeMixed::isValidCreditCard('4548812049400004') );
            $this->assertTrue( HeMixed::isValidCreditCard('4012001038443335') );
            $this->assertTrue( HeMixed::isValidCreditCard('5453010000066167') );
            $this->assertTrue( HeMixed::isValidCreditCard('5105105105105100') );
            $this->assertTrue( HeMixed::isValidCreditCard('36490102462661') );
            $this->assertTrue( HeMixed::isValidCreditCard('6011020000245045') );
            $this->assertTrue( HeMixed::isValidCreditCard('3566007770004971') );
            $this->assertTrue( HeMixed::isValidCreditCard('376449047333005') );
            $this->assertTrue( HeMixed::isValidCreditCard('378282246310005') ); // American Express
            $this->assertTrue( HeMixed::isValidCreditCard('371449635398431') );// American Express
            $this->assertTrue( HeMixed::isValidCreditCard('378734493671000') ); // American Express Corporate
            $this->assertTrue( HeMixed::isValidCreditCard('5610591081018250') ); // Australian BankCard
            $this->assertTrue( HeMixed::isValidCreditCard('3566002020360505') ); // JCB BankCard
            $this->assertTrue( HeMixed::isValidCreditCard('6011111111111117') );

        }

        if( $typeOfValidation==PAY_VALID_CARD_LEVEL_HIGH ){
            $this->assertEquals( 'visa', HeMixed::isValidCreditCard('4548812049400004') );
            $this->assertEquals( 'visa', HeMixed::isValidCreditCard('4012001038443335') );
            $this->assertEquals( 'mastercard', HeMixed::isValidCreditCard('5453010000066167') );
            $this->assertEquals( 'mastercard', HeMixed::isValidCreditCard('5105105105105100') );
            $this->assertEquals( 'diners', HeMixed::isValidCreditCard('36490102462661') );
            $this->assertEquals( 'discover', HeMixed::isValidCreditCard('6011020000245045') );
            $this->assertEquals( 'jcb', HeMixed::isValidCreditCard('3566007770004971') );
            $this->assertEquals( 'amex', HeMixed::isValidCreditCard('376449047333005') );
            $this->assertEquals( 'jcb', HeMixed::isValidCreditCard('3566002020360505') );
            $this->assertEquals( 'amex', HeMixed::isValidCreditCard('378734493671000') );
            $this->assertEquals( 'discover', HeMixed::isValidCreditCard('6011111111111117') );

        }

        return true;
    }

    /**
     *
     */
    public function testIsValidLuhn()
    {
        $this->assertTrue( HeMixed::isValidLuhn('5540500001000004'),
                                'Mastercard is expected to pass Luhn validation' );
        $this->assertTrue( HeMixed::isValidLuhn('6362970000457013'),
                                'ELO Brazilian card is expected to pass Luhn validation' );
        $this->assertTrue( HeMixed::isValidLuhn('376449047333005'),
                                'American Express is expected to pass Luhn validation' );
        return true;

    }
    /**  test negative function same name
     * @test
     *
     * @return bool
     */
    public function testNotIsValidCreditCard()
    {
        $this->assertFalse( HeMixed::isValidCreditCard('4540500001000004') );
        $this->assertFalse( HeMixed::isValidCreditCard('812049400004') );

        return true;
    }

}