<?php

class HeDateTest  extends CTestCase
{

    /**
     * @return bool
     */
    public function testGetDateTimeAdd( )
    {
        $dateToTest = '2014-02-28 09:00:01';
        $dateSubstracted = HeDate::getDateTimeAdd($dateToTest, -2, HeDate::HOURS);
        $this->assertEquals('2014-02-28 07:00:01', $dateSubstracted);

        $dateSubstracted = HeDate::getDateTimeAdd($dateToTest, 24, HeDate::HOURS);
        $this->assertEquals('2014-03-01 09:00:01', $dateSubstracted);

        return true;
    }


    /**
     * It tests the conversion of date times from zone to zone.
     */
    public function testChangeFromUTCTo()
    {
        // summer time
        $dateToTestUTC = '2014-04-28 07:00:01';
        $dateSubstracted = HeDate::changeFromUTCTo($dateToTestUTC);
        $this->assertEquals('2014-04-28 09:00:01', $dateSubstracted);

        // winter time
        $dateToTestUTC = '2014-11-30 23:00:00';
        $dateSubstracted = HeDate::changeFromUTCTo($dateToTestUTC, 'Europe/Madrid');
        $this->assertEquals('2014-12-01 00:00:00', $dateSubstracted);


        return true;
    }


}
