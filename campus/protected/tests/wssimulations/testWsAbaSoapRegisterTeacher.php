<?php

ini_set("soap.wsdl_cache_enabled",  0);
ini_set("default_socket_timeout",   600);

$client = new SoapClient( 'http://campus.aba.local/wsoapaba/usersapi/', array('soap_version' => SOAP_1_1, 'port' => 80, 'trace' => 1) );
//$client = new SoapClient( 'http://campus-stage.abaenglish.com/wsoapaba/usersapi/', array('soap_version' => SOAP_1_1, 'port' => 80, 'trace' => 1) );

$return = null;

try
{
    echo " ---------------------------------- VERISON INFO API----------------------------------------------------------------";
    /* @var WsoapabaController $client */
    echo "\n Version =  \n";
    $return = $client->version();
    var_dump($return);

    echo " ---------------------------------- EXAMPLE OF USAGE registerUserPartner----------------------------------------------";
    echo "\n Return registerTeacher =  \n";

    $sEmailPrefix = 'extranet.test.teacher';
    $sEmailSuffix = 'abaenglish.com';

    //
    // user template
    $sUserT = array
    (
        'registerUserSource' =>     '5',
        'idCountry' =>              '199',
        'device' =>                 'd',
        'registerTeacherSource' =>  '2',
        'langTeacher' =>            '',
        'nationality' =>            '',
        'photo' =>                  'graham.png',
        'mobileImage2x' =>          'http://dc1spakwio57v.cloudfront.net/images/mobile/ios/iphone/teacher/teacherImage-Graham@2x.png',
        'mobileImage3x' =>          'http://dc1spakwio57v.cloudfront.net/images/mobile/ios/iphone/teacher/teacherImage-Graham@3x.png',
        'email' =>                  'test.teacher.03@gmail.com',
        'name' =>                   'Test',
        'surnames' =>               'Teacher 1',
        'langEnv' =>                'es',
        'pwd' =>                    '123456789',
    );
    $aUsers[] =                 $sUserT;
//
//    $sUserT = array
//    (
//        'registerUserSource' =>     '5',
//        'idCountry' =>              '199',
//        'device' =>                 'd',
//        'registerTeacherSource' =>  '2',
//        'langTeacher' =>            '',
//        'nationality' =>            '',
//        'photo' =>                  'graham.png',
//        'mobileImage2x' =>          'http://dc1spakwio57v.cloudfront.net/images/mobile/ios/iphone/teacher/teacherImage-Graham@2x.png',
//        'mobileImage3x' =>          'http://dc1spakwio57v.cloudfront.net/images/mobile/ios/iphone/teacher/teacherImage-Graham@3x.png',
//        'email' =>                  'mlsanchez@fundaciongsr.es',
//        'name' =>                   'María Luisa',
//        'surnames' =>               'Sánchez Zaballos',
//        'langEnv' =>                'es',
//        'pwd' =>                    '123456789',
//    );
//    $aUsers[] =                 $sUserT;

    $sUsers =                   json_encode($aUsers);
    $signatureWsAbaEnglish =    md5('Aba English External Login Key' . $sUsers);

    /* @var WsoapabaController $client */
    $return = $client->registerTeacher( $signatureWsAbaEnglish, $sUsers);

    var_dump($return);
}
catch (Exception $exc) {
    echo "\n Mensaje de error para el servicio call = ". $exc->getMessage();
}


echo " ---------------------------------- REQUEST TRACE DEBUG INFO----------------------------------------------------------------";
echo "\n ------Request HEaders =  \n ";
var_dump( $client->__getLastRequestHeaders() );
echo "\n -----Last Request Body= \n ";
var_dump( $client->__getLastRequest() );


echo "\n ---------------------------------- RESPONSE TRACE DEBUG INFO----------------------------------------------------------------";
echo "\n Retorno =  \n";
var_dump($return);
echo "\n ------Response Headers = \n";
var_dump( $client->__getLastResponseHeaders() );


echo "\n ------Response Body= \n";
$response =  $client->__getLastResponse();
$fullResponse = var_export ( $response );
echo $fullResponse;


echo "\n ---------------------------------- COOKIES INVOLVED IN RESPONSE----------------------------------------------------------------";
echo "\n ------Cookies recommended by SERVER= \n";
var_dump($client->_cookies);
