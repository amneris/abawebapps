<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 12/11/12
 * Time: 16:36
 * To change this template use File | Settings | File Templates.
 */

ini_set("soap.wsdl_cache_enabled",  0);
ini_set("default_socket_timeout",   600);

$client = new SoapClient( 'http://campus.aba.local/wsoapaba/usersapi/', array('soap_version' => SOAP_1_1, 'port' => 80, 'trace' => 1) );

// $client->__setCookie('PHPSTORM', (isset($_COOKIE['PHPSTORM']) ? $_COOKIE['PHPSTORM'] : ''));

$return = null;

try
{
    echo " ---------------------------------- VERISON INFO API----------------------------------------------------------------";
    /* @var WsoapabaController $client */
    echo "\n Version =  \n";
    $return = $client->version();
    var_dump($return);

    echo " ---------------------------------- EXAMPLE OF USAGE registerUserPartner----------------------------------------------";
    echo "\n Return registerUserPartner =  \n";

    $sEmailPrefix = 'ak.escenario.';
    $sEmailSuffix = 'abaenglish.com';

    //
    // user template
    $sUserT = array
    (
//        'email' =>          $sEmailPrefix . '0@' . $sEmailSuffix,
        'email' =>          'akorotkov.07@abaenglish.com',
        'pwd' =>            '123456789',
        'langEnv' =>        'zh',
        'name' =>           'Test China',
        'surnames' =>       'ZH',
        'currentLevel' =>   '3',
//        'teacherEmail' =>   'miriamsanchez@linkup-learning.es',
        'teacherEmail' =>   '',
//        'codeDeal' =>       '40',
        'codeDeal' =>       '',
        'partnerId' =>      '7002',
        'periodId' =>       '12',
        'idCountry' =>      '44',
        'device' =>         'd',
        'error' =>          true
    );

    $iNumUsers = 40;

    $aUsers[] = $sUserT;

//    for($iCounter = 21; $iCounter <= $iNumUsers; $iCounter++) {
//        $sUserTmpl =            $sUserT;
//        $sUserTmpl['email'] =   $sEmailPrefix . $iCounter . '@' . $sEmailSuffix;
//
//        $aUsers[] = $sUserTmpl;
//    }

//
//    for($iCounter = 1; $iCounter <= $iNumUsers; $iCounter++) {
//
//        $sUserTmpl =            $sUserT;
//        $sUserTmpl['email'] =   $sEmailPrefix . $iCounter . '@' . $sEmailSuffix;
//
//        if($iCounter < 0) { $sUserTmpl['codeDeal'] = '40'; $sUserTmpl['periodId'] = '1'; }
//        elseif($iCounter < 2) { $sUserTmpl['codeDeal'] = '30'; $sUserTmpl['periodId'] = '3'; }
//        elseif($iCounter < 3) { $sUserTmpl['codeDeal'] = '75'; $sUserTmpl['periodId'] = '12'; }
//        elseif($iCounter < 4) { $sUserTmpl['codeDeal'] = '90'; $sUserTmpl['periodId'] = '24'; }
//        else{ $sUserTmpl['codeDeal'] = ''; $sUserTmpl['periodId'] = '3'; }
//
//        $aUsers[] = $sUserTmpl;
//    }

    $sUsers = json_encode($aUsers);

    $signatureWsAbaEnglish = md5('Aba English Extranet Key' . $sUsers);

    /* @var WsoapabaController $client */

    $return = $client->registerUserPremiumB2bExtranet( $signatureWsAbaEnglish, $sUsers);

    var_dump($return);
}
catch (Exception $exc)
{
    echo "\n Mensaje de error para el servicio call = ". $exc->getMessage();
}


echo " ---------------------------------- REQUEST TRACE DEBUG INFO----------------------------------------------------------------";
echo "\n ------Request HEaders =  \n ";
var_dump( $client->__getLastRequestHeaders() );
echo "\n -----Last Request Body= \n ";
var_dump( $client->__getLastRequest() );



echo "\n ---------------------------------- RESPONSE TRACE DEBUG INFO----------------------------------------------------------------";
echo "\n Retorno =  \n";
var_dump($return);
echo "\n ------Response Headers = \n";
var_dump( $client->__getLastResponseHeaders() );

echo "\n ------Response Body= \n";
$response =  $client->__getLastResponse();
$fullResponse = var_export ( $response );
echo $fullResponse;
//
//echo "\n ---------------------------------- COOKIES INVOLVED IN RESPONSE----------------------------------------------------------------";
//echo "\n ------Cookies recommended by SERVER= \n";
//var_dump($client->_cookies);
