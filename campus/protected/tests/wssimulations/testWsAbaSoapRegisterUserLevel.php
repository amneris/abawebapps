<?php
/**
 * this is only used to recreate registration users by command line
 * User: Quino
 * Date: 12/11/12
 * Time: 16:36
 * To change this template use File | Settings | File Templates.
 */

ini_set("soap.wsdl_cache_enabled", 0);

// campus.abaenglish.com
$client=new SoapClient('http://campus.aba.local/wsoapaba/usersapi/',
    array('soap_version'=>SOAP_1_2,'port'=> 80,'trace' => 1));

//var_dump($client->__getFunctions());
$currentLevel = 1;
//$eMail =        'wssimulator.user.level.2@yopmail.com';
$eMail =        'unit.test.functional.free.level1@yopmail.com';

/*
action:do_plan_suscription_adwords_objectives
name:alek sander
surname:
email:unit.test.functional.free.level1@yopmail.com
pass:123456789
lang_env:es
product_id:
promocode:
currentLevel:1
objectives:{"viajar":0,"estudiar":0,"trabajar":1,"otros":1,"nivel":1,"engPrev":4,"dedicacion":4}
*/

$aObjectives =   array(
    'viajar' => 0,
    'trabajar' => 1,
    'estudiar' => 0,
    'nivel' => 1,
////    'selEngPrev' => '1',
////    'genero' => 'M',
    'dedicacion' => 4,
    'engPrev' => 4,
////    'day' => '02',
////    'month' => '09',
////    'year' => '1976',
    'otros' => 1,
);
$sObjectives = json_encode($aObjectives);

$signatureWsAbaEnglish = md5( "Yii ABA English Key 2". $eMail.'123456789'. 'es'. 'Alek Sander'. ''. "". "". "HOME_LEVEL_UNIT". ''.'' . NULL . NULL . $currentLevel . $sObjectives);

$return = null;

try
{
    echo " ---------------------------------- VERSION INFO API----------------------------";
    $return = $client->version();
    echo "\n Version = ";
    echo $return['version'];
    echo "\n Description = ";
    echo $return['description'];

    /* @var WsoapabaController $client */
//    $return = $client->registerUserObjectives( $signatureWsAbaEnglish, $eMail, '123456789', 'es', 'Alek', 'Sander', '199_90', '', 'HOME_LEVEL_UNIT', '199', '300001', NULL, NULL, $currentLevel, $sObjectives);
    $return = $client->registerUserObjectives( $signatureWsAbaEnglish . "123", $eMail, '123456789', 'es', 'Alek Sander', '', '', '', 'HOME_LEVEL_UNIT', '', '', NULL, NULL, $currentLevel, $sObjectives);

    echo "\n Return registerUser =  \n";
    var_dump($return);
}
catch (Exception $exc)
{
    echo "\n Mensaje de error para el servicio call = ". $exc->getMessage();
}


echo " ---------------------------------- REQUEST TRACE DEBUG INFO----------------------------";
    echo "\n ------Request HEaders =  \n ";
    var_dump( $client->__getLastRequestHeaders() );
    echo "\n -----Last Request Body= \n ";
    var_dump( $client->__getLastRequest() );
echo "\n ---------------------------------- RESPONSE TRACE DEBUG INFO----------------------------";
    echo "\n Retorno =  \n";
    var_dump($return);
    echo "\n ------Response Headers = \n";
    var_dump( $client->__getLastResponseHeaders() );
echo "\n ------Response Body= \n";
    $response =  $client->__getLastResponse();
    var_dump( $response );
echo "\n ---------------------------------- COOKIES INVOLVED IN RESPONSE----------------------------";
    echo "\n ------Cookies recommended by SERVER= \n";
    var_dump($client->_cookies);


