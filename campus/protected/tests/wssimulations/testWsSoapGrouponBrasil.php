<?php

// https://parceirosgroupon.com.br/VouchersService.asmx
ini_set("soap.wsdl_cache_enabled", 0);
$client = new SoapClient( 'http://demo.parceirosgroupon.com.br/vouchersservice.asmx?wsdl' ,array('soap_version'   => SOAP_1_2, 'port'=> 80, 'trace' => 1)  );
                            //http://www.parceirosgroupon.com.br/vouchersservice.asmx?wsdl
$return = null;

try
{
    echo " ---------------------------------- LIST OF FUNCTIONS AVAILABLE----------------------------";
        var_dump($client->__getFunctions());
    /*----------------------------------------------------------------------------------------------*/


        $header = new SoapHeader(
            "http://www.parceirosgroupon.com.br/",
            "UserCredentials",
            array("userName" => "99011", "password" => "testeapi")
        );
        $client->__setSoapHeaders($header);
        $voucher = array('CodigosReferencia' => '999011', //Multiple Offers Code(REF)
                        'CodigoCupom'  	=> '181/567',
                        'CodigoSeguranca'   => '101BA37974DS');

        //$client->
        //Check Voucher
        $return = $client->VerificarVoucherMultiplasReferencias($voucher);
    echo " ---------------------------------- RETURN VALUE FOR VerificarVoucherMultiplasReferencias----------";
        var_dump($return);
        echo " Id=" . $return->VerificarVoucherMultiplasReferenciasResult;
    /*----------------------------------------------------------------------------------------------*/
}
catch (Exception $exc)
{
    echo "\n Mensaje de error para el servicio call = ". $exc->getMessage();
}



echo " ---------------------------------- REQUEST TRACE DEBUG INFO----------------------------";
    echo "\n ------Request HEaders =  \n ";
    var_dump( $client->__getLastRequestHeaders() );
    echo "\n -----Last Request Body= \n ";
    var_dump( $client->__getLastRequest() );

echo "\n ---------------------------------- RESPONSE TRACE DEBUG INFO----------------------------";
    echo "\n ------Response Headers = \n";
    var_dump( $client->__getLastResponseHeaders() );
    echo "\n ------Response Body= \n";
    $response =  $client->__getLastResponse();
    var_dump( $response );


