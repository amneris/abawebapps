<?php

ini_set("soap.wsdl_cache_enabled",  0);
ini_set("default_socket_timeout",   600);

$client = new SoapClient( 'http://campus.aba.local/wsoapaba/usersapi/', array('soap_version' => SOAP_1_1, 'port' => 80, 'trace' => 1) );

$return = null;

try
{
    echo " ---------------------------------- VERISON INFO API----------------------------------------------------------------";
    /* @var WsoapabaController $client */
    echo "\n Version =  \n";
    $return = $client->version();
    var_dump($return);

    echo " ---------------------------------- EXAMPLE OF USAGE loginUserExternal----------------------------------------------";
    echo "\n Return recoverPassword =  \n";

    $sEmailPrefix = 'ak.0';
    $sEmailSuffix = 'abaenglish.com';

    $aUsers[] = array ( 'email' => $sEmailPrefix . '1@' . $sEmailSuffix);
    $aUsers[] = array ( 'email' => $sEmailPrefix . '2@' . $sEmailSuffix);

    foreach($aUsers AS $aUser) {

        $signatureWsAbaEnglish = md5("Aba English External Login Key" . $aUser['email']);

        $return = $client->recoverPassword( $signatureWsAbaEnglish, $aUser['email']);

        echo "\n \n ";
        var_dump($return);
        echo "\n \n ";

        echo " ---------------------------------- REQUEST TRACE DEBUG INFO----------------------------------------------------------------";
        echo "\n ------Request HEaders =  \n ";
        var_dump( $client->__getLastRequestHeaders() );
        echo "\n -----Last Request Body= \n ";
        var_dump( $client->__getLastRequest() );

        echo "\n ---------------------------------- RESPONSE TRACE DEBUG INFO----------------------------------------------------------------";
        echo "\n Retorno =  \n";
        var_dump($return);
        echo "\n ------Response Headers = \n";
        var_dump( $client->__getLastResponseHeaders() );

        echo "\n ------Response Body= \n";
        $response =  $client->__getLastResponse();
        $fullResponse = var_export ( $response );
        echo $fullResponse;
    }
}
catch (Exception $exc)
{
    echo "\n Mensaje de error para el servicio call = ". $exc->getMessage();
}

echo "\n ---------------------------------- COOKIES INVOLVED IN RESPONSE----------------------------------------------------------------";
echo "\n ------Cookies recommended by SERVER= \n";
var_dump($client->_cookies);
