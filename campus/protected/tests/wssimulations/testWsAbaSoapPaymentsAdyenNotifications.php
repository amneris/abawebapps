<?php

ini_set("soap.wsdl_cache_enabled",  0);
//ini_set("default_socket_timeout",   600);

//$client = new SoapClient( 'http://campus-stage.abaenglish.com/wsoapadyen/usersapi/', array('soap_version' => SOAP_1_1, 'port' => 80, 'trace' => 1) );
$client = new SoapClient( 'http://campus.aba.local/wsoapadyen/usersapi/', array('soap_version' => SOAP_1_1, 'port' => 80, 'trace' => 1) );

$return = null;

try {

    /* @var WsoapabaController $client */
    echo "\n Version =  \n";
    $return = $client->version();
    var_dump($return);

    $dataAuthorization = array("notification" => array("notificationItems" => array(
        array(
            "live" =>                   false,
            "amount" =>                 array("currency" => 'USD', "value" => 35900),
            "eventCode" =>              'AUTHORISATION',
            "eventDate" =>              date("Y-m-d H:i:s"),
            "merchantAccountCode" =>    'TestMerchant',
            "merchantReference" =>      'TestMerchantReference',
            "originalReference" =>      'hola',
            "pspReference" =>           '7914266818738823', /* DEVELOPMENT */ // 7914266777456938, 8514266773984844
//            "pspReference" =>           '7914262679797164', /* STAGE */
            "reason" =>                 '01234:1111:12/2012',
            "success" =>                true,
            "paymentMethod" =>          'visa',
            "operations" =>             array("CANCEL", "CAPTURE", "REFUND"),
            "additionalData" =>         array("qq"=>"1", "ww"=>array("ww"=>"ss", "ss"=>22), "ee"=>array(array("123"), array(123), array(3, "44", array(1, "e"=>array(123))))),
        ),
        array(
            "live" =>                   false,
            "amount" =>                 false,
            "eventCode" =>              'DESCONOCIDO',
            "eventDate" =>              date("Y-m-d H:i:s"),
            "merchantAccountCode" =>    'TestMerchant',
            "merchantReference" =>      'TestMerchantReference',
            "originalReference" =>      'hola',
            "pspReference" =>           '',
            "reason" =>                 'UNKNOWN',
            "success" =>                true,
            "paymentMethod" =>          '',
            "operations" =>             "",
            "additionalData" =>         false,
        ),
        array(
            "live" =>                   false,
            "amount" =>                 array("currency" => 'EUR', "value" => 0),
            "eventCode" =>              'REPORT_AVAILABLE',
            "eventDate" =>              date("Y-m-d H:i:s"),
            "merchantAccountCode" =>    'TestMerchant',
            "merchantReference" =>      'TestMerchantReference',
            "originalReference" =>      true,
            "pspReference" =>           'received_payments_report_2015_03_17.csv',
            "reason" =>                 'https://ca-live.adyen.com/reports/download/MerchantAccount/TestMerchant/received_payments_report_2015_03_17.csv',
            "success" =>                true,
            "paymentMethod" =>          true,
            "operations" =>             true,
        ),
    )));

    // 1. Authorization
    $data = json_encode($dataAuthorization);
//    $data = $dataAuthorization;

    /* @var WsoapabaController $client */
    $return = $client->sendNotification( $data );

    echo "\n Return registerUser =  \n";
    print_r($return);

}
catch(SoapFault $exc) {
    echo "\n Mensaje de error para el servicio call = ". $exc->getMessage();
}


