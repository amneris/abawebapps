<?php
/**
 * this is only used to recreate registration users by command line
 * User: Quino
 * Date: 12/11/12
 * Time: 16:36
 * To change this template use File | Settings | File Templates.
 */

ini_set("soap.wsdl_cache_enabled", 0);

// campus.abaenglish.com
$client=new SoapClient('http://campus-stage.abaenglish.com/wsoapaba/usersapi/', array('soap_version'=>SOAP_1_2,'port'=> 80,'trace' => 1));

//var_dump($client->__getFunctions());

$signatureWsAbaEnglish = md5( "Yii ABA English Key 2". 'sellenium4@abaenglish.com'.'121212121'. 'es'. 'alek'. 'sander'. "199_180". "". "HOME". '199'.'300001');
$return = null;

$letras = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J');
$aTestsUsers = array(
            array('test.uat.mexico.XXX@yopmail.com', '123456789', 'es', 'Test Mexico XXX ', 'UAT', '138_180', 'HOME', '138', '300001'),
            array('test.uat.mexico.XXX@yopmail.com', '123456789', 'es', 'Test Mexico XXX', 'UAT', '138_180', 'HOME', '138', '300001'),
            array('test.uat.mexico.XXX@yopmail.com', '123456789', 'es', 'Test Mexico XXX', 'UAT', '138_180', 'HOME', '138', '300001'),
            array('test.uat.spain.XXX@yopmail.com', '123456789', 'es', 'Test Spain XXX', 'UAT', '199_180', 'HOME', '199', '300001'),
            array('test.uat.uk.XXX@yopmail.com', '123456789', 'en', 'Test United Kingdom XXX', 'UAT', '225_180', 'HOME', '225', '300001'),
            array('test.uat.brazil.XXX@yopmail.com', '123456789', 'pt', 'Test Brazil XXX', 'UAT', '30_180', 'HOME', '30', '300001'),
            array('test.uat.france.XXX@yopmail.com', '123456789', 'fr', 'Test France XXX', 'UAT', '73_180', 'HOME', '73', '300001'),
            array('test.uat.vietnam.XXX@yopmail.com', '123456789', 'fr', 'Test Vietnam XXX', 'UAT', '232_180', 'HOME', '232', '300001'),
                );

try
{
    echo " ---------------------------------- VERISON INFO API----------------------------";
    $return = $client->version();
    echo "\n Version = ";
    echo $return['version'];
    echo "\n Description = ";
    echo $return['description'];
    $iLetra = 0;
    foreach ($aTestsUsers as $aTestUser) {
        /* @var WsoapabaController $client */
        $return = $client->registerUser( $signatureWsAbaEnglish.'123', str_replace('XXX',$letras[$iLetra],$aTestUser[0]), $aTestUser[1],
                        $aTestUser[2], str_replace('XXX',$letras[$iLetra],$aTestUser[3]),
                        $aTestUser[4], $aTestUser[5], '', $aTestUser[6], $aTestUser[7], $aTestUser[8]);
        echo "\n Return registerUser =  \n";
        var_dump($return);
        $iLetra++;
    }


}
catch (Exception $exc)
{
    echo "\n Mensaje de error para el servicio call = ". $exc->getMessage();
}


echo " ---------------------------------- REQUEST TRACE DEBUG INFO----------------------------";
    echo "\n ------Request HEaders =  \n ";
    var_dump( $client->__getLastRequestHeaders() );
    echo "\n -----Last Request Body= \n ";
    var_dump( $client->__getLastRequest() );
echo "\n ---------------------------------- RESPONSE TRACE DEBUG INFO----------------------------";
    echo "\n Retorno =  \n";
    var_dump($return);
    echo "\n ------Response Headers = \n";
    var_dump( $client->__getLastResponseHeaders() );
echo "\n ------Response Body= \n";
    $response =  $client->__getLastResponse();
    var_dump( $response );
echo "\n ---------------------------------- COOKIES INVOLVED IN RESPONSE----------------------------";
    echo "\n ------Cookies recommended by SERVER= \n";
    var_dump($client->_cookies);


