<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 12/11/12
 * Time: 16:36
 * To change this template use File | Settings | File Templates.
 */

ini_set("soap.wsdl_cache_enabled", 0);
$client = new SoapClient( 'http://campus.aba.local/wsoapaba/usersapi/' ,array('soap_version'   => SOAP_1_1, 'port'=> 80, 'trace' => 1)  );
$signatureWsAbaEnglish = md5( 'Yii ABA English Key 2'.'pagos.paypal.landcancel.3@yopmail.com'.'123456789'.'fr'.'testeo'.'ids partner'."199_30".""."HOME".'211'.'14');
$return = null;

try
{
    echo " ---------------------------------- VERISON INFO API----------------------------------------------------------------";
    /* @var WsoapabaController $client */
    echo "\n Version =  \n";
    $return = $client->version();
    var_dump($return);

    echo " ---------------------------------- EXAMPLE OF USAGE registerUserPartner----------------------------------------------";
    echo "\n Return registerUserPartner =  \n";
    // 59/3894 9A89E41FC8
    // 20/5150 6072EEC4CE
    //25/4513             B4F496D814
    $signature = '161616123';
    /* @var WsoapabaController $client */
//    $return = $client->registerUserPremiumB2b( $signature , 'akorotkov6@abaenglish.com', '121212121','en','Test Ex-Premium B2b',
//        'Functional Selenium', '3', 'miriamsanchez@linkup-learning.es', 'COMPUTENSE180', '7002', '6', '199', 'c');

    $return = $client->registerUserPremiumB2b( $signature , 'akorotkov6@abaenglish.com', '121212121','es','Alek EX',
        'Sander EX', '3', 'miriamsanchez@linkup-learning.es', 'COMPUTENSE180', '7002', '6', '199', 'c');

    var_dump($return);
}
catch (Exception $exc)
{
    echo "\n Mensaje de error para el servicio call = ". $exc->getMessage();
}


echo " ---------------------------------- REQUEST TRACE DEBUG INFO----------------------------------------------------------------";
    echo "\n ------Request HEaders =  \n ";
    var_dump( $client->__getLastRequestHeaders() );
    echo "\n -----Last Request Body= \n ";
    var_dump( $client->__getLastRequest() );



echo "\n ---------------------------------- RESPONSE TRACE DEBUG INFO----------------------------------------------------------------";
    echo "\n Retorno =  \n";
    var_dump($return);
    echo "\n ------Response Headers = \n";
    var_dump( $client->__getLastResponseHeaders() );

echo "\n ------Response Body= \n";
    $response =  $client->__getLastResponse();
    $fullResponse = var_export ( $response );
echo $fullResponse;

echo "\n ---------------------------------- COOKIES INVOLVED IN RESPONSE----------------------------------------------------------------";
echo "\n ------Cookies recommended by SERVER= \n";
     var_dump($client->_cookies);


