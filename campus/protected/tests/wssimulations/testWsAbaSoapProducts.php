<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 12/11/12
 * Time: 16:36
 * To change this template use File | Settings | File Templates.
 */

ini_set("soap.wsdl_cache_enabled", 0);
//$client = new SoapClient( 'http://prod2.abaenglish.com/wsoapabafeed/feed/' ,array('soap_version'   => SOAP_1_1, 'port'=> 80, 'trace' => 1)  );
$client = new SoapClient( 'http://abaenglish-local/wsoapabafeed/feed/' ,array('soap_version'   => SOAP_1_1, 'port'=> 80, 'trace' => 1)  );

$return = null;

try
{
    echo " ---------------------------------- VERISON INFO API----------------------------";
    $return = $client->version();
    echo "\n Version =  \n";
    var_dump($return);
    $return = $client->listProductsByCountry( '089daacdd5381b9fbee40cb996516c3a123', 199, "en", '');
}
catch (Exception $exc)
{
    echo "\n Mensaje de error para el servicio call = ". $exc->getMessage();
}



echo " ---------------------------------- REQUEST TRACE DEBUG INFO----------------------------";
echo "\n ------Request HEaders =  \n ";
var_dump( $client->__getLastRequestHeaders() );
echo "\n -----Last Request Body= \n ";
var_dump( $client->__getLastRequest() );


echo "\n ---------------------------------- RESPONSE TRACE DEBUG INFO----------------------------";
echo "\n Retorno =  \n";
var_dump($return);
echo "\n ------Response Headers = \n";
var_dump( $client->__getLastResponseHeaders() );

echo "\n ------Response Body= \n";
var_dump( $client->__getLastResponse() );
echo "\n ---------------------------------- COOKIES INVOLVED IN RESPONSE----------------------------";
echo "\n ------Cookies recommended by SERVER= \n";
var_dump($client->_cookies);


function listProductsByCountry($country_id, $lang, $promoCode = '' )
{
    try{
        $client = new SoapClient( WSDL_PRICES_URL, array('soap_version' => SOAP_1_1, 'port'=> 80,'trace' => 1 ));

        $params = array(
            'countryId' => $country_id,
            'language' => $lang,
            'promoCode' => $promoCode);

        $params['signature'] = md5( SECRET_WSDL_KEY . $params['countryId'] . $params['language'] . $params['promoCode']);

        $r = $client->listProductsByCountry( $params['signature'], $params['countryId'], $params['language'], $params['promoCode'] );
        // $r = $client->version();
        //debug
//        if (is_soap_fault($r)) {
        //trigger_error("SOAP Fault: (faultcode: {$r->faultcode}, faultstring: {$r->faultstring})", E_USER_ERROR);
//        }
        var_export($r);
        //return true;
        // fin debug
        /*echo " ---------------------------------- REQUEST TRACE DEBUG INFO----------------------------";
      echo "\n ------Request HEaders =  \n ";
      var_dump( $client->__getLastRequestHeaders() );
      echo "\n -----Last Request Body= \n ";
      var_dump( $client->__getLastRequest() );*/

        if( empty($r['502']) && empty($r['503']) && empty($r['505']) )
        {
            return $r;
        }

    }catch(Exception $e)
    {
        //echo $e->getMessage();
        //var_dump($client->__getLastResponse());
    }
    return false;
}