<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 12/11/12
 * Time: 16:36
 * To change this template use File | Settings | File Templates.
 */

ini_set("soap.wsdl_cache_enabled", 0);
//$client = new SoapClient( 'http://abaenglish-local/wsoapaba/usersapi/' ,array('soap_version'   => SOAP_1_1, 'port'=> 80, 'trace' => 1)  );
//$signatureWsAbaEnglish = md5( 'Yii ABA English Key 2'.'pagos.paypal.landcancel.3@yopmail.com'.'123456789'.'fr'.'testeo'.'ids partner'."199_30".""."HOME".'211'.'14');

// $client = new SoapClient( 'http://campus-stage.abaenglish.com/wsoapaba/usersapi/' ,array('soap_version'   => SOAP_1_1, 'port'=> 80, 'trace' => 1)  );
$client = new SoapClient( 'http://campus.aba.local/wsoapaba/usersapi/' ,array('soap_version'   => SOAP_1_1, 'port'=> 80, 'trace' => 1)  );
//$signatureWsAbaEnglish = md5( 'Yii ABA English Key 2'.'usuarios1@test.com'.'123'.'es'.'test'.'test'."934915876"."199_360".""."HOME".'211'.'14');
//$signatureWsAbaEnglish = md5( 'Yii ABA English Key 2'.'akorotkov2@abaenglish.com'.'123456789'.'es'.'alek'.'sander'."934915876"."$voucherCode"."$securityCode"."$partnerId"."$periodId"."$device");
$signatureWsAbaEnglish = md5( 'Yii ABA English Key 2'.'akorotkov.adyen70@abaenglish.com'.'123456789'.'es'.'alek'.'sander'."934915876"."20150525PK002"."20150525PK002"."300247"."12"."199");
$return = null;

try
{
    echo " ---------------------------------- VERISON INFO API----------------------------------------------------------------";
    /* @var WsoapabaController $client */
    echo "\n Version =  \n";
    $return = $client->version();
    var_dump($return);

    echo " ---------------------------------- EXAMPLE OF USAGE registerUserPartner----------------------------------------------";
    echo "\n Return registerUserPartner =  \n";
    // 59/3894 9A89E41FC8
    // 20/5150 6072EEC4CE
    //25/4513             B4F496D814
//    $return = $client->registerUserPartner( '089daacdd5381b9fbee40cb996516c3a123', 'akorotkov2@abaenglish.com',
    $return = $client->registerUserPartner( $signatureWsAbaEnglish, 'akorotkov.adyen70@abaenglish.com',
        '123456789', 'es','alek', 'sander', '934915876',
        '20150525PK002', '20150525PK002', "300247", "12", "199");
    var_dump($return);

}
catch (Exception $exc)
{
    echo "\n Mensaje de error para el servicio call = ". $exc->getMessage();
}


echo " ---------------------------------- REQUEST TRACE DEBUG INFO----------------------------------------------------------------";
    echo "\n ------Request HEaders =  \n ";
    var_dump( $client->__getLastRequestHeaders() );
    echo "\n -----Last Request Body= \n ";
    var_dump( $client->__getLastRequest() );



echo "\n ---------------------------------- RESPONSE TRACE DEBUG INFO----------------------------------------------------------------";
    echo "\n Retorno =  \n";
    var_dump($return);
    echo "\n ------Response Headers = \n";
    var_dump( $client->__getLastResponseHeaders() );

echo "\n ------Response Body= \n";
    $response =  $client->__getLastResponse();
    var_dump( $response );

echo "\n ---------------------------------- COOKIES INVOLVED IN RESPONSE----------------------------------------------------------------";
echo "\n ------Cookies recommended by SERVER= \n";
     var_dump($client->_cookies);


