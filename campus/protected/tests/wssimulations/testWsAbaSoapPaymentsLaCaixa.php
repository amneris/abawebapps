<?php

ini_set("soap.wsdl_cache_enabled",  0);
//ini_set("default_socket_timeout",   600);

try {

//print_r(mcrypt_list_algorithms());
//return false;

    $miObj = new RedsysAPI();

    $fuc =      "010454155";
    $terminal = "002";
    $moneda =   "978"; // EUR
    $trans =    "0";

//    $url =      "https://sis-t.redsys.es:25443/sis/realizarPago";
//    $url =      "https://sis-d.redsys.es:25443/sis/realizarPago";
////    $url =      "https://sis-d.redsys.es/sis/realizarPago";
//    $url =      "https://sis-t.sermepa.es:25443/sis/operaciones";
//
////    $url =      "http://sis-d5.sermepa.es/sis/realizarPago";
//    $url =      "https://sis-t.sermepa.es:25443/sis/realizarPago";

$url =      "https://sis-t.redsys.es:25443/sis/realizarPago";
$url =      "http://sis-d.redsys.es/sis/realizarPago";


    $urlOKKO =  "";
    $id =       time() . ":" . mt_rand(100, 999);
    $amount =   "1999"; // 19,99

    $miObj->setParameter("DS_MERCHANT_AMOUNT",          $amount);
    $miObj->setParameter("DS_MERCHANT_ORDER",           strval($id));
    $miObj->setParameter("DS_MERCHANT_MERCHANTCODE",    $fuc);
    $miObj->setParameter("DS_MERCHANT_CURRENCY",        $moneda);
    $miObj->setParameter("DS_MERCHANT_TRANSACTIONTYPE", $trans);
    $miObj->setParameter("DS_MERCHANT_TERMINAL",        $terminal);
    $miObj->setParameter("DS_MERCHANT_MERCHANTURL",     $url);
    $miObj->setParameter("DS_MERCHANT_URLOK",           $urlOKKO);
    $miObj->setParameter("DS_MERCHANT_URLKO",           $urlOKKO);

    $version =      "HMAC_SHA256_V1";
    $kc =           'sq7HjrUOBfKmC576ILgskD5srU870gJ7'; //Clave recuperada de CANALES
    $request =      "";
    $params =       $miObj->createMerchantParameters();
    $signature =    $miObj->createMerchantSignature($kc);

    $aData = array(
        "Ds_Merchant_SignatureVersion" => $version,
        "Ds_Merchant_MerchantParameters" => $params,
        "Ds_Merchant_Signature" => $signature,
    );

// print_r($aData);

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_TIMEOUT,               (60) * 5);

    curl_setopt($ch, CURLOPT_URL,               $url);
    curl_setopt($ch, CURLOPT_HEADER,            false);
    curl_setopt($ch, CURLOPT_POST,              count($aData));
    curl_setopt($ch, CURLOPT_POSTFIELDS,        http_build_query($aData));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,    true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,    false);

//curl_setopt($ch, CURLOPT_ENCODING,          "");
//curl_setopt($ch, CURLOPT_ENCODING,          "UTF-8");

    $result = curl_exec($ch);


echo "RESPONSE: ";
print_r($result);
return false;


    if($result === false) {
        return false;
    }

    if($result === false) {
// echo "Error: " . curl_error($ch);
    }
    else{
//        parse_str($result, $result);
    }

    curl_close($ch);

}
catch(SoapFault $exc) {
    echo "\n Mensaje de error para el servicio call = ". $exc->getMessage();
}






class RedsysAPI {


    /**
     * NEW SH1256 DEVELOPMENT
     *
     * 2015-11-16
     *
     * Copy from apiRedsys.php
     *
     */

    /**
     ****  Array de DatosEntrada ****
     */
    protected $vars_pay = array();

    /**
     ****  Set parameter ****
     */
    public function setParameter($key, $value)
    {
        $this->vars_pay[$key] = $value;
    }

    /**
     ****  Get parameter ****
     */
    public function getParameter($key)
    {
        return $this->vars_pay[$key];
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    ////////////					FUNCIONES AUXILIARES:							  ////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////

    /**
     ****  3DES Function  ****
     */
    protected function encrypt_3DES($message, $key)
    {
        // Se establece un IV por defecto
        $bytes = array(0, 0, 0, 0, 0, 0, 0, 0); //byte [] IV = {0, 0, 0, 0, 0, 0, 0, 0}
        $iv = implode(array_map("chr", $bytes)); //PHP 4 >= 4.0.2

        // Se cifra
        $ciphertext = mcrypt_encrypt(MCRYPT_3DES, $key, $message, MCRYPT_MODE_CBC, $iv); //PHP 4 >= 4.0.2

        return $ciphertext;
    }

    /**
     ****  Base64 Functions  ****
     */
    protected function base64_url_encode($input)
    {
        return strtr(base64_encode($input), '+/', '-_');
    }

    protected function encodeBase64($data)
    {
        $data = base64_encode($data);
        return $data;
    }

    protected function base64_url_decode($input)
    {
        return base64_decode(strtr($input, '-_', '+/'));
    }

    protected function decodeBase64($data)
    {
        $data = base64_decode($data);
        return $data;
    }

    /**
     ****  MAC Function ****
     */
    protected function mac256($ent, $key)
    {
        $res = hash_hmac('sha256', $ent, $key, true);//(PHP 5 >= 5.1.2)
        return $res;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    ////////////	   FUNCIONES PARA LA GENERACIÓN DEL FORMULARIO DE PAGO:			  ////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////

    /**
     ****  Obtener Número de pedido ****
     */
    protected function getOrder()
    {
        $numPedido = "";
        if (empty($this->vars_pay['DS_MERCHANT_ORDER'])) {
            $numPedido = $this->vars_pay['Ds_Merchant_Order'];
        } else {
            $numPedido = $this->vars_pay['DS_MERCHANT_ORDER'];
        }
        return $numPedido;
    }

    /**
     ****  Convertir Array en Objeto JSON ****
     */
    protected function arrayToJson()
    {
        $json = json_encode($this->vars_pay); //(PHP 5 >= 5.2.0)
        return $json;
    }

    public function createMerchantParameters()
    {
        // Se transforma el array de datos en un objeto Json
        $json = $this->arrayToJson();
        // Se codifican los datos Base64
        return $this->encodeBase64($json);
    }

    public function createMerchantSignature($key)
    {
        // Se decodifica la clave Base64
        $key = $this->decodeBase64($key);
        // Se genera el parámetro Ds_MerchantParameters
        $ent = $this->createMerchantParameters();
        // Se diversifica la clave con el Número de Pedido
        $key = $this->encrypt_3DES($this->getOrder(), $key);
        // MAC256 del parámetro Ds_MerchantParameters
        $res = $this->mac256($ent, $key);
        // Se codifican los datos Base64
        return $this->encodeBase64($res);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////// FUNCIONES PARA LA RECEPCIÓN DE DATOS DE PAGO (Notif, URLOK y URLKO): ////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////

    /**
     ****  Obtener Número de pedido ****
     */
    protected function getOrderNotif()
    {
        $numPedido = "";
        if (empty($this->vars_pay['Ds_Order'])) {
            $numPedido = $this->vars_pay['DS_ORDER'];
        } else {
            $numPedido = $this->vars_pay['Ds_Order'];
        }
        return $numPedido;
    }

    protected function getOrderNotifSOAP($datos)
    {
        $posPedidoIni = strrpos($datos, "<Ds_Order>");
        $tamPedidoIni = strlen("<Ds_Order>");
        $posPedidoFin = strrpos($datos, "</Ds_Order>");
        return substr($datos, $posPedidoIni + $tamPedidoIni, $posPedidoFin - ($posPedidoIni + $tamPedidoIni));
    }

    protected function getRequestNotifSOAP($datos)
    {
        $posReqIni = strrpos($datos, "<Request");
        $posReqFin = strrpos($datos, "</Request>");
        $tamReqFin = strlen("</Request>");
        return substr($datos, $posReqIni, ($posReqFin + $tamReqFin) - $posReqIni);
    }

    protected function getResponseNotifSOAP($datos)
    {
        $posReqIni = strrpos($datos, "<Response");
        $posReqFin = strrpos($datos, "</Response>");
        $tamReqFin = strlen("</Response>");
        return substr($datos, $posReqIni, ($posReqFin + $tamReqFin) - $posReqIni);
    }

    /**
     ****  Convertir String en Array ****
     */
    protected function stringToArray($datosDecod)
    {
        $this->vars_pay = json_decode($datosDecod, true); //(PHP 5 >= 5.2.0)
    }

    protected function decodeMerchantParameters($datos)
    {
        // Se decodifican los datos Base64
        $decodec = $this->base64_url_decode($datos);
        return $decodec;
    }

    protected function createMerchantSignatureNotif($key, $datos)
    {
        // Se decodifica la clave Base64
        $key = $this->decodeBase64($key);
        // Se decodifican los datos Base64
        $decodec = $this->base64_url_decode($datos);
        // Los datos decodificados se pasan al array de datos
        $this->stringToArray($decodec);
        // Se diversifica la clave con el Número de Pedido
        $key = $this->encrypt_3DES($this->getOrderNotif(), $key);
        // MAC256 del parámetro Ds_Parameters que envía Redsys
        $res = $this->mac256($datos, $key);
        // Se codifican los datos Base64
        return $this->base64_url_encode($res);
    }

    /**
     ****  Notificaciones SOAP ENTRADA ****
     */
    protected function createMerchantSignatureNotifSOAPRequest($key, $datos)
    {
        // Se decodifica la clave Base64
        $key = $this->decodeBase64($key);
        // Se obtienen los datos del Request
        $datos = $this->getRequestNotifSOAP($datos);
        // Se diversifica la clave con el Número de Pedido
        $key = $this->encrypt_3DES($this->getOrderNotifSOAP($datos), $key);
        // MAC256 del parámetro Ds_Parameters que envía Redsys
        $res = $this->mac256($datos, $key);
        // Se codifican los datos Base64
        return $this->encodeBase64($res);
    }

    /**
     ****  Notificaciones SOAP SALIDA ****
     */
    protected function createMerchantSignatureNotifSOAPResponse($key, $datos, $numPedido)
    {
        // Se decodifica la clave Base64
        $key = $this->decodeBase64($key);
        // Se obtienen los datos del Request
        $datos = $this->getResponseNotifSOAP($datos);
        // Se diversifica la clave con el Número de Pedido
        $key = $this->encrypt_3DES($numPedido, $key);
        // MAC256 del parámetro Ds_Parameters que envía Redsys
        $res = $this->mac256($datos, $key);
        // Se codifican los datos Base64
        return $this->encodeBase64($res);
    }

}




//
//
// WS
//
//

//
//class RedsysAPIWs{
//
//    /******  Array de DatosEntrada ******/
//    var $vars_pay = array();
//
//    /******  Set parameter ******/
//    function setParameter($key,$value){
//        $this->vars_pay[$key]=$value;
//    }
//
//    /******  Get parameter ******/
//    function getParameter($key){
//        return $this->vars_pay[$key];
//    }
//
//
//    //////////////////////////////////////////////////////////////////////////////////////////////
//    //////////////////////////////////////////////////////////////////////////////////////////////
//    ////////////					FUNCIONES AUXILIARES:							  ////////////
//    //////////////////////////////////////////////////////////////////////////////////////////////
//    //////////////////////////////////////////////////////////////////////////////////////////////
//
//
//    /******  3DES Function  ******/
//    function encrypt_3DES($message, $key){
//        // Se establece un IV por defecto
//        $bytes = array(0,0,0,0,0,0,0,0); //byte [] IV = {0, 0, 0, 0, 0, 0, 0, 0}
//        $iv = implode(array_map("chr", $bytes)); //PHP 4 >= 4.0.2
//
//        // Se cifra
//        $ciphertext = mcrypt_encrypt(MCRYPT_3DES, $key, $message, MCRYPT_MODE_CBC, $iv); //PHP 4 >= 4.0.2
//        return $ciphertext;
//    }
//
//    /******  Base64 Functions  ******/
//    function base64_url_encode($input){
//        return strtr(base64_encode($input), '+/', '-_');
//    }
//    function encodeBase64($data){
//        $data = base64_encode($data);
//        return $data;
//    }
//    function base64_url_decode($input){
//        return base64_decode(strtr($input, '-_', '+/'));
//    }
//    function decodeBase64($data){
//        $data = base64_decode($data);
//        return $data;
//    }
//
//    /******  MAC Function ******/
//    function mac256($ent,$key){
//        $res = hash_hmac('sha256', $ent, $key, true);//(PHP 5 >= 5.1.2)
//        return $res;
//    }
//
//
//    //////////////////////////////////////////////////////////////////////////////////////////////
//    //////////////////////////////////////////////////////////////////////////////////////////////
//    ////////////	   FUNCIONES PARA LA GENERACIÓN DE LA PETICIÓN DE PAGO:			  ////////////
//    //////////////////////////////////////////////////////////////////////////////////////////////
//    //////////////////////////////////////////////////////////////////////////////////////////////
//
//    /******  Obtener Número de pedido ******/
//    function getOrder($datos){
//        $posPedidoIni = strrpos($datos, "<DS_MERCHANT_ORDER>");
//        $tamPedidoIni = strlen("<DS_MERCHANT_ORDER>");
//        $posPedidoFin = strrpos($datos, "</DS_MERCHANT_ORDER>");
//        return substr($datos,$posPedidoIni + $tamPedidoIni,$posPedidoFin - ($posPedidoIni + $tamPedidoIni));
//    }
//    function createMerchantSignatureHostToHost($key, $ent){
//        // Se decodifica la clave Base64
//        $key = $this->decodeBase64($key);
//        // Se diversifica la clave con el Número de Pedido
//        $key = $this->encrypt_3DES($this->getOrder($ent), $key);
//        // MAC256 del parámetro Ds_MerchantParameters
//        $res = $this->mac256($ent, $key);
//        // Se codifican los datos Base64
//        return $this->encodeBase64($res);
//    }
//
//
//    //////////////////////////////////////////////////////////////////////////////////////////////
//    //////////////////////////////////////////////////////////////////////////////////////////////
//    //////////// FUNCIONES PARA LA RECEPCIÓN DE DATOS DE PAGO (Respuesta HOST to HOST) ///////////
//    //////////////////////////////////////////////////////////////////////////////////////////////
//    //////////////////////////////////////////////////////////////////////////////////////////////
//
//    function createMerchantSignatureResponseHostToHost($key, $datos, $numPedido){
//        // Se decodifica la clave Base64
//        $key = $this->decodeBase64($key);
//        // Se diversifica la clave con el Número de Pedido
//        $key = $this->encrypt_3DES($numPedido, $key);
//        // MAC256 del parámetro Ds_Parameters que envía Redsys
//        $res = $this->mac256($datos, $key);
//        // Se codifican los datos Base64
//        return $this->encodeBase64($res);
//    }
//}




