<?php

ini_set("soap.wsdl_cache_enabled", 0);

try {
    $request = array(
        "reason" => "",
        "currency" => "USD",
        "value" => "6835",
        "eventCode" => "REQUEST_FOR_INFORMATION",
        "eventDate" => "2015-08-30T08:02:34.88Z",
        "merchantAccountCode" => "ABAEnglishCOM",
        "merchantReference" => "fe1ba028:759",
        "originalReference" => "4414367518407198",
        "pspReference" => "4414367518407198",
        // "operations" => false,
        "reason" => "No Cardholder Authorisation",
        "success" => true,
        "paymentMethod" => "visa",
        "live" => false,
    );

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "http://campus.aba.local/wspostaba/sendnotification");
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, "AbAenglish:AbAenglsh1qa2ws");
    curl_setopt($ch, CURLOPT_POST, count($request));
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    $result = curl_exec($ch);

    if ($result === false) {
        echo "Error: " . curl_error($ch);
    } else {
        print_r(($result));
    }

    curl_close($ch);
} catch (SoapFault $exc) {
    echo "\n Mensaje de error para el servicio call = " . $exc->getMessage();
}
