<?php

ini_set("soap.wsdl_cache_enabled",  0);
ini_set("default_socket_timeout",   600);

$client = new SoapClient( 'http://campus.aba.local/wsoapaba/usersapi/', array('soap_version' => SOAP_1_1, 'port' => 80, 'trace' => 1) );

$return = null;

try
{
    echo " ---------------------------------- VERISON INFO API----------------------------------------------------------------";
    /* @var WsoapabaController $client */
    echo "\n Version =  \n";
    $return = $client->version();
    var_dump($return);

    echo " ---------------------------------- EXAMPLE OF USAGE registerUserPartner----------------------------------------------";
    echo "\n Return registerTeacher =  \n";

    $sEmailPrefix = 'extranet.test.teacher';
    $sEmailSuffix = 'abaenglish.com';

    //
    // user template
    $sUserT = array
    (
        'email' =>              'akorotkov.adyen18@abaenglish.com',
        'teacherId' =>          '1503594',
        'idPartnerCurrent' =>   '300001',
    );
    $aUsers[] = $sUserT;

    $sUserT = array
    (
        'email' =>              'akorotkov.adyen19@abaenglish.com',
        'teacherId' =>          '1503599',
        'idPartnerCurrent' =>   '300001',
    );
    $aUsers[] = $sUserT;

    $sUserT = array
    (
        'email' =>              'akorotkov.extranet.teacher.05@abaenglish.com',
        'teacherId' =>          '1503600',
        'idPartnerCurrent' =>   '300001',
    );
    $aUsers[] = $sUserT;

    $sUsers =                   json_encode($aUsers);
    $signatureWsAbaEnglish =    md5('Aba English External Login Key' . $sUsers);

    /* @var WsoapabaController $client */
    $return = $client->finaliceLicense( $signatureWsAbaEnglish, $sUsers);

    var_dump($return);
}
catch (Exception $exc) {
    echo "\n Mensaje de error para el servicio call = ". $exc->getMessage();
}


echo " ---------------------------------- REQUEST TRACE DEBUG INFO----------------------------------------------------------------";
echo "\n ------Request HEaders =  \n ";
var_dump( $client->__getLastRequestHeaders() );
echo "\n -----Last Request Body= \n ";
var_dump( $client->__getLastRequest() );


echo "\n ---------------------------------- RESPONSE TRACE DEBUG INFO----------------------------------------------------------------";
echo "\n Retorno =  \n";
var_dump($return);
echo "\n ------Response Headers = \n";
var_dump( $client->__getLastResponseHeaders() );


echo "\n ------Response Body= \n";
$response =  $client->__getLastResponse();
$fullResponse = var_export ( $response );
echo $fullResponse;


echo "\n ---------------------------------- COOKIES INVOLVED IN RESPONSE----------------------------------------------------------------";
echo "\n ------Cookies recommended by SERVER= \n";
var_dump($client->_cookies);
