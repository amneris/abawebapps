<?php

ini_set("soap.wsdl_cache_enabled",  0);
//ini_set("default_socket_timeout",   600);

try {
    $request = array(
        "paymentAmount" =>      "10000",
        "currencyCode" =>       "USD",
//        "currencyCode" =>       "RUB",
        "merchantReference" =>  "Request payment methods",
        "skinCode" =>           "cEUElC17",
//        "skinCode" =>           "j8MT0vKI",
        "merchantAccount" =>    "ABAEnglishCOM",
        "sessionValidity" =>    date("c", strtotime("+1 days")),
        "countryCode" =>        "RU",
        "merchantSig" =>        "",
    );

    $hmacKey = "SKINTESTAbAenglish1qa2ws";

    $request["merchantSig"] = base64_encode(pack("H*",hash_hmac(
        'sha1',
        $request["paymentAmount"] . $request["currencyCode"] . $request["merchantReference"] .
        $request["skinCode"] .  $request["merchantAccount"] . $request["sessionValidity"],
        $hmacKey
    )));


    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://test.adyen.com/hpp/directory.shtml");
    curl_setopt($ch, CURLOPT_HEADER, false);
//    curl_setopt($ch, CURLOPT_HEADER,
//        array(
//            'Content-Type: application/json',
//            'Content-Length: ' . strlen(json_encode($request))
//        )
//    );
//    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC  );
//    curl_setopt($ch, CURLOPT_USERPWD, "AbAenglish:AbAenglsh1qa2ws");
    curl_setopt($ch, CURLOPT_POST, count($request));
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

//    curl_setopt($ch, CURLOPT_HEADER, false);
//    curl_setopt($ch, CURLOPT_POST, count($request));
//    curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($request));
////    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC  );
////    curl_setopt($ch, CURLOPT_USERPWD, "AbAenglish:AbAenglsh1qa2ws");
//    curl_setopt($ch, CURLOPT_POST, count($request));
//    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
////    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($ch);

    if($result === false)
        echo "Error: " . curl_error($ch);
    else{
//        parse_str($result, $result);
        print_r($result);
    }

    curl_close($ch);

}
catch(SoapFault $exc) {
    echo "\n Mensaje de error para el servicio call = ". $exc->getMessage();
}

