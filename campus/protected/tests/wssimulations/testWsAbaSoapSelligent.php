<?php
define ('URLCAMPUSWEBSOAP' , 'http://abaenglish-local/wsoapabaselligent/senddata/');

// Once everything about development is finished, please set to 1
ini_set("soap.wsdl_cache_enabled", 0);



/**
 * @param string $operation
 * @param array  $aFields
 *
 * @return array|bool
 */
function campusCallForSelligent( $operation="cancellations", $aFields)
{
    $return = null;

    // Composition of secure signature:
    $joinParams = implode("", $aFields);
    $joinParams = "Yii ABA English Key 2".$joinParams;
    $signature = md5( $joinParams );

    try{
        // Once everything about development is finished, please set trace to 0 ++++++++++
        $client = new SoapClient( URLCAMPUSWEBSOAP ,array('soap_version'   => SOAP_1_1, 'port'=> 80, 'trace' => 1)  );
        switch( $operation ){
            case 'cancellations':
                    //Parameters = signature, $userId, $userType, $expirationDate
                    /* @var array $response :
                            If success  array("result"=> "success",
                                                "details"=> "string" );
                           If error     array( "$code=503, etc." => "string" )
                      */
                    $response = $client->$operation( $signature, $aFields["userId"], $aFields["userType"], $aFields["expirationDate"] );
                    return $response;
                break;
            case 'passwordChange':
                $response = $client->$operation( $signature, $aFields["userId"], $aFields["password"] );
                return $response;
                break;
            default:
                 return false;
                break;
        }
    }


    catch (Exception $exc)
    {
        echo " Communications with Campus web service Selligent for operation $operation has failed. Details= \n ";
        echo $exc->getMessage();
        echo " ----------------------------------------------------------------- ";


        echo " ---------------------------------- REQUEST TRACE DEBUG INFO----------------------------";
        echo "\n ------Request HEaders =  \n ";
        var_dump( $client->__getLastRequestHeaders() );
        echo "\n -----Last Request Body= \n ";
        var_dump( $client->__getLastRequest() );


        echo "\n ---------------------------------- RESPONSE TRACE DEBUG INFO----------------------------";
        echo "\n Retorno =  \n";
        var_dump($return);
        echo "\n ------Response Headers = \n";
        var_dump( $client->__getLastResponseHeaders() );

        echo "\n ------Response Body= \n";
        var_dump( $client->__getLastResponse() );

        return false;
    }

}

/*  Example of Cancellations*/
//$return = campusCallForSelligent( "cancellations", array ("userId"=>2, "userType"=>1 /* FREE=1, PREMIUM=2*/ , "expirationDate"=>"2013-02-01" /*  YYYY-MM-DD */));

/* Example if passwordChange */
$return = campusCallForSelligent( "passwordChange", array ("userId"=>62451, "password"=>"987654321" ));


var_dump($return);