<?php
/**
 * this is only used to recreate registration users by command line
 * User: Alek Sander
 * Date: 4/11/14
 * Time: 16:55
 * To change this template use File | Settings | File Templates.
 */

ini_set("soap.wsdl_cache_enabled", 0);

// campus.abaenglish.com
$client=new SoapClient('http://campus.aba.local/wsoapaba/usersapi/',
    array('soap_version'=>SOAP_1_2,'port'=> 80,'trace' => 1));

$sAction1 =  'exists';
$sAction2 =  'register';
$sFbid =     '1234567890';
$sEmail1 =   'test.fabebook.user.1@facebookuserabatest.com';
$sEmail2 =   'test.fabebook.user.2@facebookuserabatest.com';

$sName =    'Dick';
$sSurname = 'Martinez';
$sGender =  'M';

$signatureWsAbaEnglish1 = md5( "Yii ABA English Key 2" . $sAction1 . $sFbid . $sEmail1 . $sName . $sSurname . $sGender . $sEmail1 . '123456789' .
    '' . '' . '' . '' . '' . '' . '' . '1' . ''
);
$signatureWsAbaEnglish2 = md5( "Yii ABA English Key 2" . $sAction2 . $sFbid . $sEmail2 . $sName . $sSurname . $sGender . $sEmail2 . '123456789' .
    '' . '' . '' . '' . '' . '' . '' . '1' . ''
);

try
{
    echo " ---------------------------------- VERSION INFO API----------------------------";
    $return = $client->version();
    echo "\n Version = ";
    echo $return['version'];
    echo "\n Description = ";
    echo $return['description'];

    /* @var WsoapabaController $client */
    $return1 = $client->registerUserFacebook( $signatureWsAbaEnglish1, $sAction1, $sEmail1, $sName, $sSurname, $sFbid, $sGender, $sEmail1, '123456789', '', '', '', '', '', '', '', '1', '');

    echo "\n Return registerUser1 =  \n";
    print_r($return1);

    if($return1['result'] == 'successexists') {
        echo "redirectUrl: " . $return1['redirectUrl'];
    }
    elseif($return1['result'] == 'noexists') {

        /* @var WsoapabaController $client */
        $return2 = $client->registerUserFacebook( $signatureWsAbaEnglish2, $sAction2, $sEmail2, $sName, $sSurname, $sFbid, $sGender, $sEmail2, '123456789', '', '', '', '', '', '', '', '1', '');

        echo "\n Return registerUser2 =  \n";
        print_r($return2);
    }

}
catch (Exception $exc)
{
    echo "\n Mensaje de error para el servicio call = ". $exc->getMessage();
}


echo " ---------------------------------- REQUEST TRACE DEBUG INFO----------------------------";
echo "\n ------Request HEaders =  \n ";
var_dump( $client->__getLastRequestHeaders() );
echo "\n -----Last Request Body= \n ";
var_dump( $client->__getLastRequest() );
echo "\n ---------------------------------- RESPONSE TRACE DEBUG INFO----------------------------";
echo "\n Retorno =  \n";
var_dump($return1);
echo "\n Retorno =  \n";
var_dump($return2);
echo "\n ------Response Headers = \n";
var_dump( $client->__getLastResponseHeaders() );
echo "\n ------Response Body= \n";
$response =  $client->__getLastResponse();
var_dump( $response );
echo "\n ---------------------------------- COOKIES INVOLVED IN RESPONSE----------------------------";
echo "\n ------Cookies recommended by SERVER= \n";
var_dump($client->_cookies);


