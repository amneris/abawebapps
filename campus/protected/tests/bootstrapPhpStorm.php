<?php

// It includes all common files and folders shared between different projects:
require dirname(__FILE__) . '/../../../common/bootstrapTest.php';


// These keys overwrite the ones in the table config in the database.
// It is useful for customize a local environment ignoring the development
// database and without annoying ur sensitive buddies.
if (file_exists(ROOT_DIR . '/campus/protected/config/ProgrammerConfigKeysTest.php')) {
    include_once(ROOT_DIR . '/campus/protected/config/ProgrammerConfigKeysTest.php');
}


$config = ROOT_DIR . '/campus/protected/config/test.php';
require_once(ROOT_DIR . '/campus/protected/tests/basetest'.DIRECTORY_SEPARATOR.'AbaWebTestCase.php');

// DEVELOPMENT, INTEGRATION and STAGE: ****
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

Yii::createWebApplication($config);
