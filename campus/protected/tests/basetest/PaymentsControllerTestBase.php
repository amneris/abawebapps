<?php

/**
 * To be able to run tests we should start the Selenium Server:
 * C:\Program Files (x86)\Java\jre7\bin>java.exe -jar C:\wamp\bin\selenium\selenium-server-standalone-2.31.0.jar
 */
class PaymentsControllerTestBase extends AbaWebTestCase
{
    /**
     * Creates all test users and any other test data used specific from this test.
     */
    public function setUp()
    {
        parent::setUp();
    }

    public function setUpNoCard()
    {
        parent::setUpNoCard();
    }

    /**
     * Destroy all test users and any other test data used specific from this test.
     */
    public function tearDown() {
        parent::tearDown();
    }

    public function tearDownNoCard() {
        parent::tearDownNoCard();
    }

    /************************************************/
    /*** REPETITIVE UI OPERATIONS AND ASSERTIONS ***/
    /************************************************/

    /*public function testPaymentMethodTPVAndBuy()
    {
        return true;
    }*/

    /**
     * Encapsulates the actions of login to Campus and go to Payment and select a product
     *
     * @param $email
     * @param integer
     * @param string $extraParams
     *
     * @return bool
     */
    protected function selectPaymentProductPrice($email, $monthId, $extraParams='' )
    {
        $this->loginUserByEmail($email, true, $extraParams);
        // Closes the dialog box of JQuery User Objectives Form
        if( $this->isElementPresent("id=CampusModals") )
        {
            $this->click("class=ui-dialog-titlebar-close ui-corner-all");
        }

        // We click on Go To Premium banner
//        $this->clickAndWait("id=divBtnGoPremium");
        $this->clickAndWait("id=idDivGoPremium");

        // Assert at least 4 products available:
        $this->assertProductSelectionPage();

        // Click on the selected product
        if( isset($monthId) && is_numeric($monthId)) {
            if($this->isElementPresent("id=BtnMonthContinue-".$monthId)) {
                $this->clickAndWait("id=BtnMonthContinue-".$monthId);
            }
        }
        else {
            // Default click on Annual product:
            $this->clickAndWait("id=BtnMonthContinue-12");
        }

        return true;
    }


    /** Asserts the method payment Page. Basic elements.
     *
     * @param string $langEnv
     * @param integer $monthPeriod
     * @return bool
     */
    protected function assertPaymentMethodPage( $langEnv, $monthPeriod )
    {
        $dateSpainNow =         HeDate::SQLToEuropean(HeDate::now());
        $datePurchasedPeriod =  HeDate::getDateAdd(HeDate::now(), 0, $monthPeriod);
        $datePurchasedPeriod =  HeDate::SQLToEuropean($datePurchasedPeriod);

        if ( $langEnv == 'es'){
            $this->assertEquals("Del $dateSpainNow al $datePurchasedPeriod", $this->getText("id=IdPFromtoPeriod"));
            $this->assertEquals("Se renovará el $datePurchasedPeriod", $this->getText("id=IdPDateToPay"));
            if ($monthPeriod==6){
                $this->assertEquals("Semestral", $this->getText("css=#IdPPeriodicity > span"));
                $this->assertEquals("Plazo: 6 meses", $this->getText("id=IdPPlanMonth"));
                $this->assertEquals("Precio total: 69,99 €", $this->getText("id=IdPFinalPrice"));
            }
            if ($monthPeriod==12){
                $this->assertEquals("Anual", $this->getText("css=#IdPPeriodicity > span"));
                $this->assertEquals("Plazo: 12 meses", $this->getText("id=IdPPlanMonth"));
                $this->assertEquals("Precio total: 99,99 €", $this->getText("id=IdPFinalPrice"));
            }
        }

        if ( $langEnv == 'en'){
            $this->assertEquals("From $dateSpainNow to $datePurchasedPeriod", $this->getText("id=IdPFromtoPeriod"));
            $this->assertEquals("To be renewed on $datePurchasedPeriod", $this->getText("id=IdPDateToPay"));
            if ($monthPeriod==6){
                $this->assertEquals("Six-monthly", $this->getText("css=#IdPPeriodicity > span"));
                $this->assertEquals("Duration: 6 months", $this->getText("id=IdPPlanMonth"));
                $this->assertEquals("Total price: US $150", $this->getText("id=IdPFinalPrice"));
            }
            if ($monthPeriod==12){
                $this->assertEquals("Annual", $this->getText("css=#IdPPeriodicity > span"));
                $this->assertEquals("Duration: 12 months", $this->getText("id=IdPPlanMonth"));
                $this->assertEquals("Total price: US $239", $this->getText("id=IdPFinalPrice"));
            }
        }

        if ( $langEnv == 'fr'){
            $this->assertEquals("Du $dateSpainNow au $datePurchasedPeriod", $this->getText("id=IdPFromtoPeriod"));
            $this->assertEquals("Renouvellement le $datePurchasedPeriod", $this->getText("id=IdPDateToPay"));
            if ($monthPeriod==6){
                $this->assertEquals("Semestriel", $this->getText("css=#IdPPeriodicity > span"));
                $this->assertEquals("Durée: 6 mois", $this->getText("id=IdPPlanMonth"));
                $this->assertEquals("Prix total: 125 € ", $this->getText("id=IdPFinalPrice"));
            }
            if ($monthPeriod==12){
                $this->assertEquals("Annuel", $this->getText("css=#IdPPeriodicity > span"));
                $this->assertEquals("Durée: 12 mois", $this->getText("id=IdPPlanMonth"));
                $this->assertEquals("Prix total: 199 €", $this->getText("id=IdPFinalPrice"));
            }
        }
        return true;
    }

    /** Opens payments/payment and logs into the Campus.
     * @param string $promoCode
     * @param integer $idPartner
     * @param string $email
     *
     * @return string Email of user logged in
     */
    protected  function gotoPayment( $promoCode, $idPartner=NULL, $email='unit.test.free.user@yopmail.com' )
    {
        $this->loginUserByEmail($email);

        $extraParams = '';
        if(isset($idPartner)){
            $extraParams = '/idpartner/'.$idPartner.'';
        }

        if($promoCode!=''){
            $this->open('payments/payment/promocode/'.$promoCode.$extraParams);
        } else {
            $this->open('payments/payment/promocode/'.'NONEXISTENPROMOCODE'.$extraParams);
        }

        // Closes the dialog box of JQuery User Objectives Form
        if( $this->isElementPresent("id=CampusModals") )
        {
            $this->click("class=ui-dialog-titlebar-close ui-corner-all");
        }

        return true;

    }

    /** Click on congratulations page and asserts everything related with PREMIUM privileges.
     *
     * @param string $langEnv
     * @return bool
     */
    protected function goAndAssertPremiumAfterPayment( $langEnv )
    {
        // Welcome page and confirmed payment:
        $this->assertCongratulationsPage($langEnv);

        // Click on Start Now!
        $this->clickAndWait("id=idDivBtnStartPremium");

        // Asserts that it goes  to index course units:
        $this->assertCourseIndexPage($langEnv, 6, PREMIUM);

        //Check the layout Premium:
        $this->goHomePage();
        $this->assertHomePage(PREMIUM, $langEnv);

        $this->assertElementNotPresent('id=idDivGoPremium');
        $this->assertElementPresent('id=divBtnFree');

        return true;
    }

    /** Checks that the data on REGISTRATION ALL PAGO is correct.
     * @param AbaUser $moUser
     * @param string $cpf
     *
     * @return bool
     */
    protected function assertRegitrationOnAllPago($moUser, $cpf)
    {
        $moUserCreditForm = new AbaUserCreditForms($moUser->id);
        /* @var Payment $moPayPend */
        $moPayPend = new Payment();
        $moPayPend = $moPayPend->getLastPayPendingByUserId($moUser->id);
        $this->assertInstanceOf('Payment', $moPayPend, 'Not found PENDING payment after paying successfully');

        $this->assertEquals($moUserCreditForm->registrationId, $moPayPend->paySuppExtProfId,
            'Registration/profile id does not match');
        $this->assertEquals( preg_replace('/[^0-9+]/', '', $cpf), $moUserCreditForm->cpfBrasil, 'cpfBrasil does not match.');

        return true;
    }

    /**
     * @param $moUser
     *
     * @return bool
     */
    public function downloadAllInvioces($moUser) {
        $counter = 0;
        $this->clickAndWait("id=aTabInvoices");
        $invoicesToPdf = new ColInvoices();
        $aDataInvoices = $invoicesToPdf->getAllInvoicesByUserIdTest($moUser->id);
        foreach($aDataInvoices as $iKey => $aDataInvoice) {
            if(is_numeric($aDataInvoice['id'])) {
                $this->click("id=" . $aDataInvoice['idPayment'] . "-" . $aDataInvoice['id']);
            }
            $counter += 10;
        }
        sleep($counter);

        return true;
    }

    /**
     * @param bool $oUser
     * @param string $email
     * @param bool $usedByUser
     * @param int $countryId
     *
     * @return bool
     */
    public function assertUserInvoices($oUser=false, $email='', $usedByUser=false, $countryId=301) {

        if(!$oUser) {
            $oUser = new AbaUser();
            $oUser->getUserByEmail($email);
        }

        $aUsersToCreate = $this->dataUsers();

        $invoicesToPdf = new ColInvoices();
        $aDataInvoices = $invoicesToPdf->getAllInvoicesByUserIdTest($oUser->id);

//        $this->assertTrue(count($aDataInvoices) == 1);
//        $this->assertEquals(count($aDataInvoices), 1);

        $dataCountries = $this->dataCountries();

        if(isset($dataCountries[$countryId])) {
            foreach($aDataInvoices as $iKey => $aDataInvoice) {

                /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
                if(is_numeric($aDataInvoice['id'])) {

                    if(!$usedByUser) {

                        $oTaxRate = new CmAbaTaxRates();
                        $oTaxRate->getTaxRateByCountry($aDataInvoice['idCountry']);

                        $amountPriceWithoutTax =    HeMixed::getRoundAmount($oTaxRate->getPriceWithoutTax($aDataInvoice['iAmountPrice']));
                        $taxRateValue =             $oTaxRate->getTaxRateValue();

                        $this->assertEquals($amountPriceWithoutTax, $aDataInvoice['iAmountPriceWithoutTax']);
                        $this->assertEquals($taxRateValue, $aDataInvoice['iTaxRateValue']);
                        $this->assertEquals(round($aDataInvoice['iAmountTax'] + $aDataInvoice['iAmountPriceWithoutTax'], 2), round($aDataInvoice['iAmountPrice'], 2));

                        /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
                        $this->assertEquals($aDataInvoice['usedByUser'], 0);
                        $this->assertEquals(trim($aDataInvoice['userStreet']),      '');
                        $this->assertEquals(trim($aDataInvoice['userVatNumber']),   '');
                        $this->assertEquals(trim($aDataInvoice['userZipCode']),     '');
                        $this->assertEquals(trim($aDataInvoice['userCityName']),    '');
                        $this->assertEquals(trim($aDataInvoice['userStateName']),   '');
                    }
                    else {
                        $this->assertEquals($aDataInvoice['usedByUser'], 1);
                        foreach ($aUsersToCreate as $userCols) {
                            if($userCols[0] == $oUser->email) {
                                $this->assertEquals(trim($aDataInvoice['userStreet']),       trim($userCols[9]));
                                $this->assertEquals(trim($aDataInvoice['userVatNumber']),    trim($userCols[13]));
                                $this->assertEquals(trim($aDataInvoice['userZipCode']),      trim($userCols[12]));
                                $this->assertEquals(trim($aDataInvoice['userCityName']),     trim($userCols[7]));
                                $this->assertEquals(trim($aDataInvoice['userStateName']),    trim($userCols[11]));
                            }
                        }
                    }
                    /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

                    /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
                    $oInvoice = new AbaUserInvoices();
                    $oInvoice->getByPaymentId($aDataInvoice['idPayment']);

                    $oCountry = new AbaCountry();
                    $stCountries = $oCountry->getFullUeList();

                    $prefixInvoice = DEFAULT_PREFIX_INVOICE;
                    if(array_key_exists($dataCountries[$countryId][0], $stCountries)) {
                        $prefixInvoice = $oInvoice->prefixInvoice;
                    }

                    $this->assertEquals($prefixInvoice, $oInvoice->prefixInvoice);
                    $this->assertEquals($prefixInvoice . '-' . str_pad(strval($oInvoice->numInvoiceId), FORMAT_NUMBER_INVOICE, "0", STR_PAD_LEFT), $oInvoice->numberInvoice);
                    /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
                }
            }
        }
        return true;
    }

    /**
     * @param $birthDate
     * @param $city
     * @param $telephone
     * @param $countryId
     * @param $street
     * @param $streetMore
     * @param $province
     * @param $cp
     * @param $ident
     *
     * @return bool
     */
    public function paymentSaveInvoiceData($birthDate, $city, $telephone, $countryId, $street, $streetMore, $province, $cp, $ident)
    {
//        if($this->isElementPresent("id=idBillingInfoLink")) {
        if($this->isElementPresent("id=headLinkMenuMyAccount")) {

//            $this->assertElementPresent("id=idBillingInfoLink");
//            $this->clickAndWait("id=idBillingInfoLink");
            $this->assertElementPresent("id=headLinkMenuMyAccount");
            $this->clickAndWait("id=headLinkMenuMyAccount");

            $this->type('id=ProfileForm_birthDate', $birthDate);
            $this->type('id=ProfileForm_city', $city);
            $this->type('id=ProfileForm_telephone', $telephone);

            $this->select('id=ProfileForm_countryId', 'value=' . $countryId);

            $this->click('id=copyData');

            $this->type('id=ProfileForm_invoicesStreet', $street);
            $this->type('id=ProfileForm_invoicesStreetMore', $streetMore);
            $this->type('id=ProfileForm_invoicesProvince', $province);
            $this->type('id=ProfileForm_invoicesPostalCode', $cp);
            $this->type('id=ProfileForm_invoicesTaxIdNumber', $ident);

            $this->clickAndWait("id=btnSaveMyAccount");
        }
        return true;
    }

    /**
     * @param bool $isBrazil
     * @param $email
     * @param $countryId
     * @param $name
     * @param $lastName
     * @param $langEnv
     * @param $creditType
     * @param $cardNumber
     * @param $cardMonth
     * @param $cardYear
     * @param $cardCvc
     * @param $cpf
     * @param $ipLocation
     *
     * @return bool
     */
    public function paymentMethodTPVAndBuyForInvoice($isBrazil=false, $email, $countryId, $name, $lastName, $langEnv, $creditType, $cardNumber, $cardMonth, $cardYear, $cardCvc, $cpf, $ipLocation )
    {
        $this->select('id=PaymentForm_creditCardType', 'value=' . $creditType);
        $this->type("id=PaymentForm_creditCardName", $name);
        $this->type("id=PaymentForm_creditCardNumber", $cardNumber);
        $this->select('id=PaymentForm_creditCardMonth', 'index=' . $cardMonth);
        $this->select('id=PaymentForm_creditCardYear', 'label=' . $cardYear);
        $this->type("id=PaymentForm_creditCardName", $name . " " . $lastName);

        if($isBrazil) {
            $this->type("id=PaymentForm_cpfBrasil", $cpf);
        }

        $this->type("id=PaymentForm_CVC", $cardCvc);
        $this->clickAndWait("id=btnABA");

        $this->assertElementPresent("id=idDivBtnStartPremium");
        $this->clickAndWait("id=idDivBtnStartPremium");

        return true;
    }


}
