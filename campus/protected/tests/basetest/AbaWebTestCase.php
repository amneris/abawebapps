<?php

/**
 * Change the following URL based on your server configuration
 * Make sure the URL ends with a slash so that we can use relative URLs in test cases
 */
define('TEST_BASE_URL','http://campus.aba.local/index-test.php');
/**
 * The base class for functional test cases.
 * In this class, we set the base URL for the test application.
 * We also provide some common methods to be used by concrete test classes.
 */
class AbaWebTestCase extends CWebTestCase
{
    protected $sendEmailOnFailure = FALSE;
    protected $captureScreenshotOnFailure = FALSE;
    protected $screenshotPath = '/var/www/abawebapps/campus/protected/tests/freport/screenshots';
    protected $screenshotUrl = 'http://campus.aba.local/screenshots';

    /**
     * Sets up before each test method runs.
     * This mainly sets the base URL for the test application.
     */
    public function setUp()
    {
        // PREMIUM
        $uId = $this->createUsersIntoDb(
            "unit.test.premium.user@yopmail.com",
            PREMIUM,
            Yii::app()->config->get("ABACountryId"),
            "UnitPremium",
            "Testing"
        );
        // FREE
        $uId = $this->createUsersIntoDb("unit.test.free.user@yopmail.com", FREE, Yii::app()->config->get("ABACountryId"),
            "UnitFree", "Testing");
        // FREE that was PREMIUM in the PAST
        $uId = $this->createUsersExPremium("unit.test.ex.premium.user@yopmail.com",
            FREE, COUNTRY_ITALY/*Italy*/, "UnitExPremium", "Testing");
        // PREMIUM CANCEL
        $uId = $this->createUsersPremiumCancel("unit.test.premium.cancelled.user@yopmail.com",
            PREMIUM, 138/*Mexico*/, "UnitPremium", "Testing Cancelled");
        // DELETED: unit.test.lead.user@yopmail.com
        $userId = $this->createUsersIntoDb("unit.test.deleted.user@yopmail.com", DELETED, Yii::app()->config->get("ABACountryId"),
            "UnitLead", "Testing");
        // PREMIUM B2B
        $uId = $this->createUserB2bIntoDb('unit.test.premium.b2b.active@yopmail.com',
            PREMIUM, 199, "UnitPremium", "B2B Testing Premium Active");
        Yii::app()->user->id = $userId;

        parent::setUp();

        $this->setBrowserUrl(TEST_BASE_URL);
    }

    public function setUpNoCard()
    {
        // PREMIUM
        $uId = $this->createUsersIntoDb("unit.test.premium.user@yopmail.com", PREMIUM, 47, "UnitPremium", "Testing");
        // FREE
        $uId = $this->createUsersIntoDb("unit.test.free.user@yopmail.com", FREE, 73, "UnitFree", "Testing");
        // FREE that was PREMIUM in the PAST
        $uId = $this->createUsersExPremium("unit.test.ex.premium.user@yopmail.com", FREE, 47, "UnitExPremium", "Testing");
        // PREMIUM CANCEL
        $uId = $this->createUsersPremiumCancel("unit.test.premium.cancelled.user@yopmail.com", PREMIUM, 73, "UnitPremium", "Testing Cancelled");
        // DELETED: unit.test.lead.user@yopmail.com
        $userId = $this->createUsersIntoDb("unit.test.deleted.user@yopmail.com", DELETED, 47, "UnitLead", "Testing");
        // PREMIUM B2B
        $uId = $this->createUserB2bIntoDb('unit.test.premium.b2b.active@yopmail.com', PREMIUM, 73, "UnitPremium", "B2B Testing Premium Active");

        Yii::app()->user->id = $userId;

        parent::setUp();

        $this->setBrowserUrl(TEST_BASE_URL);
    }


    /**
     * Overwrites Unit Yii Test case destructor
      */
    public function tearDown()
    {
        $this->resetDownUser("unit.test.premium.user@yopmail.com");
        $this->resetDownUser("unit.test.free.user@yopmail.com");
        $this->resetDownUser("unit.test.deleted.user@yopmail.com");
        $this->resetDownUser("unit.test.premium.cancelled.user@yopmail.com");
        $this->resetDownUser("unit.test.ex.premium.user@yopmail.com");
        $this->resetDownUser("unit.test.premium.b2b.active@yopmail.com");
    }

    /**
     *
     */
    public function tearDownNoCard() {
        $this->resetDownUser("unit.test.premium.user@yopmail.com");
        $this->resetDownUser("unit.test.free.user@yopmail.com");
        $this->resetDownUser("unit.test.deleted.user@yopmail.com");
        $this->resetDownUser("unit.test.premium.cancelled.user@yopmail.com");
        $this->resetDownUser("unit.test.ex.premium.user@yopmail.com");
        $this->resetDownUser("unit.test.premium.b2b.active@yopmail.com");
    }


    /** Login user in functional web tests.
     *
     * @param string $email Valid email in fixtures or in setUp() function
     * @param bool $closeDialog
     * @param array $extraParams
     *
     * @return bool
     */
    protected function loginUserByEmail( $email, $closeDialog=false, $extraParams=NULL )
    {
        if (!isset($extraParams)) {
            $this->open('');
        } else {
            $this->open('' . $extraParams);
        }

        $this->type('name=LoginForm[email]', $email);
        $this->type('name=LoginForm[password]', '123456789');

        $this->clickAndWait("id=btnStartSession");
//        $this->waitForPageToLoad("30000");

        if ("unit.test.deleted.user@yopmail.com" == $email) {

            return false;
        }

        if ($closeDialog && $this->isElementPresent("id=CampusModals")) {
            $this->click("class=ui-dialog-titlebar-close ui-corner-all");
        }

        return true;
    }

    /** Logout/Exits the Campus
     * @param $email
     *
     * @return bool
     */
    protected function logoutUserByEmail( $email )
    {
        if ($this->isElementPresent("id=CampusModals")) {
            $this->click("class=ui-dialog-titlebar-close ui-corner-all");
        }

        $this->clickAndWait("id=aLinkHeadLogout");

        $this->assertLoginPage();
        return true;
    }

    /** It checks that user is on the Login page from the Campus
     *
     * @return bool
     */
    protected function assertLoginPage()
    {
        $this->assertElementPresent( 'name=LoginForm[email]');
        $this->assertElementPresent( 'name=LoginForm[password]');
        $this->assertElementPresent( 'id=btnStartSession' );

        return true;
    }

    /**
     * @return bool
     */
    protected function selectLevelFromPopup($level=5)
    {
        $this->click("xpath=(//input[@name='nivelActual'])[".$level."]");
        $this->click("id=submitLevelSelect");
        return true;
    }

    /**
     * @return bool
     */
    protected function assertChooseToInvitePopup()
    {
        $this->waitForElementPresent("id=popup-registrerSendButton");

        $this->assertEquals("Not now, thanks!", $this->getText("id=popup-closeAction"));
        $this->assertEquals("OK!", $this->getText("id=popup-registrerSendButton"));

        return true;
    }

    /**
     * @return bool
     */
    protected function rejectInvitePopup()
    {
        $this->click("id=popup-closeAction");

        return true;
    }

    /**
     * @return bool
     */
    protected function openInvitePopup()
    {
        $this->click("id=popup-registrerSendButton");

        $this->assertTextPresent(Yii::t("mainApp", 'Practise English with your friends!', NULL, NULL, Yii::app()->params["langAssert"]));

        return true;
    }

    /** Used to Insert tests users in database.
     * @param string $email
     * @param integer $userType
     * @param integer $countryId
     * @param string $name
     * @param string $surnames
     * @param bool $expired
     * @param string $lang
     *
     * @return integer
     */
    protected function createUsersIntoDb( $email, $userType, $countryId, $name, $surnames, $expired=false, $lang='en', $gender='', $firstLoginDone=false )
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail($email,"",false);

        $moUser->email = $email;
        $moUser->userType = $userType;
        $moUser->countryId = $countryId;
        $moUser->countryIdCustom =$moUser->countryId;
        $moUser->currentLevel = 6;
        $moUser->entryDate = HeDate::todaySQL(true);
        if($userType==PREMIUM){
            if($expired){
                $moUser->expirationDate = HeDate::getDateAdd(HeDate::todaySQL(false),-4/*days*/);
            }else{
                $moUser->expirationDate = HeDate::getDateAdd(HeDate::todaySQL(false),5/*days*/);
            }
        }else{
            $moUser->expirationDate = HeDate::todaySQL(false);
        }
        $moUser->teacherId = 0;
        $moUser->name = $name;
        $moUser->surnames = $surnames;
        $moUser->langCourse = $lang;
        $moUser->langEnv = $lang;
        if($userType==PREMIUM){
            $moUser->idPartnerFirstPay = Yii::app()->config->get("ABA_PARTNERID");
            $moUser->idPartnerCurrent = Yii::app()->config->get("ABA_PARTNERID");
        }
        else{
            $moUser->idPartnerFirstPay = NULL;
            $moUser->idPartnerCurrent = NULL;
        }
        $moUser->idPartnerSource = Yii::app()->config->get("ABA_PARTNERID");
        $moUser->telephone = "931511515";
        $moUser->setPassword("123456789",true);

        $moUser->insertUserPremiumForRegistration();

        if($userType==PREMIUM){
            $moNewCredit = new AbaUserCreditForms();
            $moNewCredit->setUserCreditForm($moUser->id, KIND_CC_DISCOVER, "6011020000245045", "2018", "05", "123", $name." ".$surnames, 1, '', '');
            $moNewCredit->insertUserCreditFormByUserId($moUser->id);

            $pay2= $this->createTestPayment($moUser, PAY_SUCCESS,
                HeDate::getDateAdd( HeDate::todaySQL(true), 0, -3/*months*/),
                $countryId, $moUser->idPartnerFirstPay, $moUser->idPartnerFirstPay, 180/*month period*/, 'EUR',
                $countryId.'_30', $moNewCredit->id);

            $pay3= $this->createTestPayment($moUser, PAY_PENDING,
                HeDate::getDateAdd( HeDate::todaySQL(true), 0, 3/*months*/),
                $countryId, $moUser->idPartnerFirstPay, $moUser->idPartnerFirstPay, 180/*month period*/, 'EUR',
                $countryId.'_30', $moNewCredit->id);
        }

        if($gender != '') {
            $moUserN = new AbaUser();
            if($moUserN->getUserByEmail($email)) {

                $moUserN->gender = $gender;

                $sql = " UPDATE " . $moUserN->tableName() .
                    " SET `gender`=:GENDER " .
                    " WHERE `id` = " . $moUserN->id . " ";

                $aBrowserValues = array(":GENDER" => $moUserN->gender);

                $moUserN->updateSQL($sql, $aBrowserValues);
            }
        }

        if($firstLoginDone) {
            $moUserN = new AbaUser();
            if($moUserN->getUserByEmail($email)) {

                $moUserN->firstLoginDone = 1;

                $sql = " UPDATE " . $moUserN->tableName() .
                    " SET `firstLoginDone`=:FIRSTLOGINDONE " .
                    " WHERE `id` = " . $moUserN->id . " ";

                $aBrowserValues = array(":FIRSTLOGINDONE" => $moUserN->firstLoginDone);

                $moUserN->updateSQL($sql, $aBrowserValues);
            }
        }

        return  $moUser->id;
    }




    protected function createUsersIntoDbInvoice( $email, $userType, $countryId, $name, $surnames, $expired=false, $lang='en')
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail($email,"",false);

        $moUser->email = $email;
        $moUser->userType = $userType;
        $moUser->countryId = $countryId;
        $moUser->countryIdCustom = $moUser->countryId;
        $moUser->currentLevel = 6;
        $moUser->entryDate = HeDate::todaySQL(true);
        if($userType==PREMIUM){
            if($expired){
                $moUser->expirationDate = HeDate::getDateAdd(HeDate::todaySQL(false),-4/*days*/);
            }else{
                $moUser->expirationDate = HeDate::getDateAdd(HeDate::todaySQL(false),5/*days*/);
            }
        }else{
            $moUser->expirationDate = HeDate::todaySQL(false);
        }
        $moUser->teacherId = 0;
        $moUser->name = $name;
        $moUser->surnames = $surnames;
        $moUser->langCourse = $lang;
        $moUser->langEnv = $lang;
        if($userType==PREMIUM){
            $moUser->idPartnerFirstPay = Yii::app()->config->get("ABA_PARTNERID");
            $moUser->idPartnerCurrent = Yii::app()->config->get("ABA_PARTNERID");
        }
        else{
            $moUser->idPartnerFirstPay = NULL;
            $moUser->idPartnerCurrent = NULL;
        }
        $moUser->idPartnerSource = Yii::app()->config->get("ABA_PARTNERID");
        $moUser->telephone = "931511515";
        $moUser->setPassword("123456789",true);

        $commUser = new RegisterUsersCommon();
        $commUser->insertUserForRegistration($commUser);

        return  $moUser->id;
    }





    /** Sets up an scenario in database for an Ex-Premium user
     * @param $email
     * @param $userType
     * @param $countryId
     * @param $name
     * @param $surnames
     * @return bool
     */
    protected function createUsersExPremium( $email, $userType, $countryId, $name, $surnames )
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail($email,"",false);

        $moUser->email = $email;
        $moUser->userType = $userType;
        $moUser->countryId = $countryId;
        $moUser->countryIdCustom =$moUser->countryId;
        $moUser->currentLevel = 4;
        $moUser->entryDate = HeDate::getDateAdd( HeDate::todaySQL(true), 0/*days*/, -4/*months*/);

        if($userType==PREMIUM){
            $moUser->expirationDate = HeDate::getDateAdd(HeDate::todaySQL(false),5);
        }else{ // FREE
            $moUser->expirationDate = HeDate::getDateAdd( HeDate::todaySQL(true), 0/*days*/, -1/*months*/);
        }
        $moUser->teacherId = 0;
        $moUser->name = $name;
        $moUser->surnames = $surnames;
        $moUser->langCourse = "en";
        $moUser->langEnv = "en";
        $moUser->registerUserSource = REGISTER_USER;
        $moUser->idPartnerSource = 45; //Groupon Finland
        $moUser->idPartnerFirstPay = Yii::app()->config->get("ABA_PARTNERID");
        $moUser->idPartnerCurrent = PARTNER_ID_RECURRINGPAY;
        $moUser->telephone = "931511515";
        $moUser->setPassword("123456789",true);
        $moUser->cancelReason = 1;
        $moUser->firstLoginDone = 1;

        $moUser->insertUserPremiumForRegistration();

        $pay1= $this->createTestPayment($moUser, PAY_SUCCESS, HeDate::getDateAdd( HeDate::todaySQL(true), 0, -3/*months*/),
            $countryId, $moUser->idPartnerFirstPay, $moUser->idPartnerSource, 30/*month period*/, 'USD', $countryId.'_30');

        $pay2= $this->createTestPayment($moUser, PAY_SUCCESS, HeDate::getDateAdd( HeDate::todaySQL(true), 0, -2/*months*/),
            $countryId, PARTNER_ID_RECURRINGPAY, $moUser->idPartnerFirstPay, 30/*month period*/, 'USD', $countryId.'_30');

        $pay3= $this->createTestPayment($moUser, PAY_CANCEL, HeDate::getDateAdd( HeDate::todaySQL(true), 0, -1/*months*/),
            $countryId, PARTNER_ID_RECURRINGPAY, PARTNER_ID_RECURRINGPAY, 30/*month period*/, 'USD', $countryId.'_30');

        return $pay1 && $pay2 && $pay3;
    }

    /** Sets up an scenario in database for a Premium user that has cancelled.
     *
     * @param $email
     * @param $userType
     * @param $countryId
     * @param $name
     * @param $surnames
     *
     * @return bool
     */
    protected function createUsersPremiumCancel( $email, $userType, $countryId, $name, $surnames )
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail($email,"",false);

        $moUser->email = $email;
        $moUser->userType = $userType;
        $moUser->countryId = $countryId;
        $moUser->countryIdCustom =$moUser->countryId;
        $moUser->currentLevel = 4;
        $moUser->entryDate = HeDate::getDateAdd( HeDate::todaySQL(true), 0/*days*/, -4/*months*/);
        $moUser->expirationDate = HeDate::getDateAdd(HeDate::todaySQL(false), 0, 9);
        $moUser->teacherId = 0;
        $moUser->name = $name;
        $moUser->surnames = $surnames;
        $moUser->langCourse = "en";
        $moUser->langEnv = "en";
        $moUser->registerUserSource = REGISTER_USER;
        $moUser->idPartnerSource = PARTNER_ID_ADWORDS_MEXICO;
        $moUser->idPartnerFirstPay = PARTNER_ID_ADWORDS_MEXICO;
        $moUser->idPartnerCurrent = PARTNER_ID_RECURRINGPAY;
        $moUser->telephone = "654 15456 987 ";
        $moUser->setPassword("123456789",true);
        $moUser->cancelReason = 1;
        $moUser->firstLoginDone = 1;

        $moUser->insertUserPremiumForRegistration();

        $pay1= $this->createTestPayment($moUser, PAY_SUCCESS, HeDate::getDateAdd( HeDate::todaySQL(true), 0, -4/*months*/),
            $countryId, $moUser->idPartnerFirstPay, $moUser->idPartnerSource, 360/*month period*/, 'USD', $countryId.'_360');
        $pay2= $this->createTestPayment($moUser, PAY_CANCEL, HeDate::getDateAdd( HeDate::todaySQL(true), 0, 8/*months*/),
            $countryId, PARTNER_ID_RECURRINGPAY, $moUser->idPartnerFirstPay, 360/*month period*/, 'USD', $countryId.'_360');

        return $pay1 && $pay2;
    }

    /**
     * @param $email
     * @param $userType
     * @param $countryId
     * @param $name
     * @param $surnames
     * @param bool $expired
     *
     * @return bool
     */
    protected function createUserB2bIntoDb($email, $userType, $countryId, $name, $surnames, $expired=false)
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail($email, "", false);

        $moUser->email = $email;
        $moUser->userType = $userType;
        $moUser->countryId = $countryId;
        $moUser->countryIdCustom = $moUser->countryId;
        $moUser->currentLevel = 6;
        $moUser->entryDate = HeDate::todaySQL(true);
        $moUser->teacherId = 0;
        $moUser->name = $name;
        $moUser->surnames = $surnames;
        $moUser->langCourse = "en";
        $moUser->langEnv = "en";
        $moUser->idPartnerFirstPay = PARTNER_JUNTAEXTREMADURA;
        $moUser->idPartnerCurrent = PARTNER_JUNTAEXTREMADURA;
        $moUser->idPartnerSource = PARTNER_JUNTAEXTREMADURA;
        $moUser->telephone = "931511515";
        $moUser->setPassword("123456789", true);
        if ($userType == PREMIUM) {
            if ($expired) {
                $moUser->expirationDate = HeDate::getDateAdd(HeDate::todaySQL(false), -5, 0);
                $moUser->entryDate = HeDate::getDateAdd(HeDate::todaySQL(false), 0, -6);

                $moUser->insertUserPremiumForRegistration();

                $this->createTestPayment($moUser, PAY_SUCCESS, $moUser->entryDate, $countryId,
                    $moUser->idPartnerFirstPay, $moUser->idPartnerFirstPay, 180/*6 month period*/, 'EUR',
                    $countryId . '_180', NULL, PAY_SUPPLIER_B2B);
            } else {
                $moUser->expirationDate = HeDate::getDateAdd(HeDate::todaySQL(false), 0, 6);

                $moUser->insertUserPremiumForRegistration();

                $this->createTestPayment($moUser, PAY_SUCCESS, HeDate::todaySQL(true), $countryId,
                    $moUser->idPartnerFirstPay, $moUser->idPartnerFirstPay, 180/*6 month period*/, 'EUR',
                    $countryId . '_180', NULL, PAY_SUPPLIER_B2B);
            }
        } else {
            $moUser->expirationDate = HeDate::getDateAdd(HeDate::todaySQL(false), 0, -1);
            $moUser->entryDate = HeDate::getDateAdd(HeDate::todaySQL(false), 0, -7);

            $moUser->insertUserPremiumForRegistration();

            $this->createTestPayment($moUser, PAY_SUCCESS, $moUser->entryDate, $countryId,
                $moUser->idPartnerFirstPay, $moUser->idPartnerFirstPay, 180/*6 month period*/, 'EUR',
                $countryId . '_180', NULL, PAY_SUPPLIER_B2B);
        }

        return true;
    }

    /** Create mock payments for being altered and checked by
     * all the rest of the tests
     * @param AbaUser $moUser
     * @param         $status
     * @param         $dateEndTransaction
     * @param         $countryId
     * @param         $idPartner
     * @param         $idPartnerPrePay
     * @param         $idPeriodPay
     * @param         $currency
     * @param         $idProduct
     * @param integer $idUserCreditForm
     *
     * @return bool
     */
    public function createTestPayment(AbaUser $moUser, $status, $dateEndTransaction,
                                      $countryId, $idPartner, $idPartnerPrePay, $idPeriodPay, $currency, $idProduct, $idUserCreditForm= NULL)
    {
        $moPayment = new Payment();
        $moPayment->id = HeAbaSHA::generateIdPayment($moUser, $dateEndTransaction );
        $moPayment->userId = $moUser->id;
        $moPayment->idProduct = $idProduct;
        $moPayment->idCountry = $countryId;
        $moPayment->idPeriodPay = $idPeriodPay ;

        if(!empty($idUserCreditForm)){
            $moPayment->idUserCreditForm = $idUserCreditForm;
        }

        $moPayment->idPromoCode = '';
        $moPayment->currencyTrans = $currency;
        $moPayment->foreignCurrencyTrans = $currency;
        $moPayment->amountOriginal = 19.99;
        $moPayment->amountDiscount = 0.00;
        $moPayment->amountPrice = 19.99;

        // Get currencies involved in order to stamp them in the transaction
        $xRates = new AbaCurrency( $currency );
        $moPayment->xRateToEUR = $xRates->getConversionRateTo(CCY_DEFAULT) ;
        $moPayment->xRateToUSD = $xRates->getConversionRateTo($currency) ;

        // Gateway and register Transaction relevant data:
        $moPayment->paySuppExtId = PAY_SUPPLIER_CAIXA ;
        $moPayment->paySuppOrderId = $moPayment->id.'.666' ;
        $moPayment->status =  $status ;
        if($status!=PAY_PENDING){
            $moPayment->dateStartTransaction = $dateEndTransaction;
            $moPayment->dateEndTransaction = $dateEndTransaction;
        }else{
            $moPayment->dateStartTransaction = '0000-00-00';
            $moPayment->dateEndTransaction = '0000-00-00';
        }
        $moPayment->dateToPay = $dateEndTransaction;
        $moPayment->lastAction = 'TestOnly';

        $moPayment->idPartner = $idPartner;
        $moPayment->idPartnerPrePay = $idPartnerPrePay;
        $moPayment->isRecurring = 0;
        if($status!=PAY_SUCCESS){
            $moPayment->isRecurring = 1;
        }

        //
        // tax rate
        //
        $moPayment->setAmountWithoutTax();

        $ret = $moPayment->paymentProcess($moPayment->userId);

        return $ret;
    }

    /** Standard recognition of HOME PAGE
     * @param $userType
     * @param string $langEnv
     *
     * @return bool
     */
    protected function assertHomePage( $userType, $langEnv = 'en' )
    {
        if ($this->isElementPresent("id=CampusModals")) {
            $this->click("class=ui-dialog-titlebar-close ui-corner-all");
        }

        $this->assertTextPresent(Yii::t('mainApp', 'key_sectionGreetingUser_0',  array(),NULL, $langEnv));
        $this->assertElementContainsText( 'id=secInfo_subTitle',
                                    Yii::t('mainApp', 'key_sectionTabDescription_0',  array(),NULL, $langEnv));

        $this->assertElementPresent('id=my_account');

        if( $userType == FREE){
            $this->assertElementPresent('class=cmp_haztePremium');
        }

        return true;
    }

    /** Standard recognition of CONFIRMATION PAGE
     * Asserts that the current page is the WELCOME TO PREMIUM page, after the payment has just been done.     *
     * @param string $langEnv
     *
     * @return bool
     */
    protected function assertCongratulationsPage($langEnv='en')
    {
        $this->assertElementPresent('id=pIdCongratulations');
        $this->assertEquals(  Yii::t('mainApp', '¡Enhorabuena!', array(), NULL, $langEnv),
                    $this->getText("id=pIdCongratulations"),
                    'Congratulations message does not appear on web page');
        $this->assertElementPresent("id=idDivBtnStartPremium");

        return true;
    }

    /** Standard recognition of PRODUCT LIST SELECTION PAGE WITHIN PAYMENTS
     *
     */
    protected function assertProductSelectionPage( $idCountry=301 )
    {
        $this->assertElementPresent('id=idLi1');
        $this->assertElementPresent('id=idLi6');
        $this->assertElementPresent('id=idLi12');
        $this->assertElementPresent('id=idLi24');

        $this->assertElementPresent('id=BtnMonthContinue-1');
        $this->assertElementPresent('id=BtnMonthContinue-6');
        $this->assertElementPresent('id=BtnMonthContinue-12');
        $this->assertElementPresent('id=BtnMonthContinue-24');

        if ( $idCountry == COUNTRY_SPAIN ){
            $this->assertElementContainsText( 'idTable-1', '14 ,99');
        }

        return true;

    }

    /**
     *  asserts selection product page but with final price discount active.
     * @param int $idCountry
     * @param string $finalPrice
     * @return bool
     */
    protected function assertProductSelectionPageWithFinalPrice( $idCountry=301, $finalPrice='1 ,00' )
    {
        $this->assertElementPresent('id=idLi1');
        $this->assertElementPresent('id=idLi6');
        $this->assertElementPresent('id=idLi12');
        $this->assertElementPresent('id=idLi24');

        $this->assertElementPresent('id=BtnMonthContinue-1');
        $this->assertElementPresent('id=BtnMonthContinue-6');
        $this->assertElementPresent('id=BtnMonthContinue-12');
        $this->assertElementPresent('id=BtnMonthContinue-24');

        $this->assertElementPresent('class=bestOption');
        $euro = '€';//html_entity_decode('&#8364;');
        $this->assertElementContainsText('class=bestOption', $finalPrice.' '.$euro);

        return true;
    }

    /** Asserts that the My Account(Profile) page is being displayed. All tabs are available.
     *
     * @param string $langEnv
     * @return bool
     */
    protected function assertMyAccountPage($langEnv='en')
    {
        $this->assertEquals(  Yii::t('mainApp', 'Progreso', array(), NULL, $langEnv), $this->getText('id=aTabMyProgress'));
        $this->assertEquals( Yii::t('mainApp', 'Certificados', array(), NULL, $langEnv), $this->getText('id=aTabCertificates'));
        $this->assertEquals( Yii::t('mainApp', 'Suscripción', array(), NULL, $langEnv), $this->getText('id=aTabSubscription'));
//        $this->assertEquals(Yii::t('mainApp', 'Invita a tus amigos', array(), NULL, $langEnv) , $this->getText('id=aTabInvite'));
        $this->assertEquals(Yii::t('mainApp', 'Datos personales', array(), NULL, $langEnv), $this->getText('id=aTabMyAccount'));
        
        return true;
    }

    /** It asserts the course index page
     *
     * @param string $langEnv
     * @param int $level
     * @param int $userType
     *
     * @return bool
     */
    protected function assertCourseIndexPage( $langEnv = 'en', $level=6, $userType=1 )
    {
        $startUnit = HeUnits::getStartUnitByLevel($level);
        /*$this->assertText(Yii::t('mainApp','key_title_subject_'.$startUnit, array(), NULL, $langEnv));
        $this->assertText(Yii::t('mainApp','key_title_subject_'.($startUnit+1), array(), NULL, $langEnv));*/

        $this->assertElementPresent('idDivUnit'.strval($userType-1).$startUnit, 'Unit is not present') ;
        $this->assertElementPresent('idDivUnit'.strval($userType-1).($startUnit+1), 'Unit is not present') ;


        return true;
    }

    /**
     * It goes to " Inicio link " or if this is not present opens url index.php.
     */
    protected function goHomePage()
    {
        if ( $this->isElementPresent('id=idUlHome') ){
            $this->clickAndWait('id=idUlHome');
        } else {
            $this->open('/');
            $this->waitForPageToLoad('20000');
        }

        return true;
    }

    /** Deletes user from database.
     *
     * @param string $email
     * @return bool
     */
    protected function resetDownUser($email)
    {
        $moUser = new AbaUser();
        if($moUser->getUserByEmail($email, "", false)){
            $moUser->deleteUserThroughResetCall();
            return true;
        }

        return false;
    }

    /**
     * @param array $aInput1
     * @param array $aInput2
     * @return array
     */
    protected function unitTestDataCartesian($aInput1, $aInput2)
    {
        $aFirstInput = $aInput1;
        $aCombinations = array();
        foreach($aFirstInput as $aCombination){
            foreach($aInput2 as $subElems){
                foreach($aCombination as $combination){
                    array_push($subElems, $combination);
                }
                $aCombinations[] = $subElems;
            }
        }


        return array($aCombinations);
    }

    /** Data provider of sample cards.
     *
     * @return array
     */
    public function dataCardsSamples()
    {
        return DataCollections::$aCreditForms;
    }

    /** Data Provider of show case users.
     *
     * @return array
     */
    public function dataUsersCase()
    {
        return DataCollections::$aUsersShowCase;
    }

    /**
     * @return array
     */
    public function dataExtranetCase()
    {
        return DataCollections::$aUsersExtranetCase;
    }

    /**
     * @param $idPartner
     * @return array
     */
    public function unitTestExtranetData($idPartner)
    {
        $aUsersExtranetCase = $this->dataExtranetCase();

        $aResponse = array();

        foreach($aUsersExtranetCase as $iKey => $aUser) {

            $aResponse[] = array
            (
                'email' =>          $aUser[0],
                'pwd' =>            '123456789',
                'langEnv' =>        'en',
                'name' =>           'Extranet test',
                'surnames' =>       'Premium user',
                'currentLevel' =>   '3',
                'teacherEmail' =>   'miriamsanchez@linkup-learning.es',
                'codeDeal' =>       '',
                'partnerId' =>      $idPartner,
                'periodId' =>       '6',
                'idCountry' =>      '199',
                'device' =>         DEVICE_SOURCE_EXTRANET,
                'error' =>          true
            );
        }

        return $aResponse;
    }


    /**
     * Create user facebook id
     *
     * @param $sEmail
     * @param $iFbid
     *
     * @return int
     */
    protected function createUserFbidIntoDb($sEmail, $iFbid)
    {
        $moUser =       new AbaUser();
        $userExists =   $moUser->getUserByEmail($sEmail);

        $moUserFb =     new AbaUser();
        $userFbExists = $moUserFb->getUserByFacebookId($iFbid);

        if($userExists && !$userFbExists) {

            $moUser->fbId = $iFbid;

            $sql = " UPDATE " . $moUser->tableName() .
                " SET `fbId`=:FBID " .
                " WHERE `id` = " . $moUser->id . " ";

            $aBrowserValues = array(":FBID" => $moUser->fbId);

            if ( $moUser->updateSQL($sql, $aBrowserValues) > 0 ) {
                return true;
            }
        }
        return false;
    }


    /**
     * @param $sEmail
     * @param $sPass
     */
    public function loginInCampus($sEmail, $sPass) {
        //
        // Login facebook
        //
        $this->waitForElementPresent("id=LoginForm_email");
        $this->waitForElementPresent("id=LoginForm_password");

        $this->type("id=LoginForm_email", $sEmail);
        $this->type("id=LoginForm_password", $sPass);


//        $this->assertTextPresent(Yii::t("mainApp", 'Iniciar sesión', NULL, NULL, Yii::app()->params["langAssert"]));
//        $this->assertTextPresent(Yii::t("mainApp", 'Contraseña', NULL, NULL, Yii::app()->params["langAssert"]));

//        $this->click("id=btnFbSubmit");
        $this->click("id=btnStartSession");




//        $this->waitForPopUp("", 10000);
//
//        $allTitles = $this->getAllWindowTitles();
//
//        $this->selectWindow($allTitles[1]);
//        $this->assertEquals("Facebook", $this->getTitle());
//
//        $this->waitForElementPresent("id=email");
//        $this->type("id=email", $sEmail);
//        $this->type("id=pass", $sPass);
//        $this->clickAndWait("id=loginbutton", 10000);
//
//        $this->close($allTitles[1]);
//
//        $this->selectWindow($allTitles[0]);
//        $this->refreshAndWait();
    }





    /**
     * @param AbaUser $moUser
     * @param $sFbid
     * @param $sEmail
     * @param $sName
     * @param $sSurname
     * @param $sGender
     * @param $langEnv
     *
     * @param bool $checkFbId
     */
    public function assertFacebookUser($sFbid, $sEmail, $sName, $sSurname, $sGender, $langEnv, $checkFbId=true, $checkCountry=true) {

        $userCommon =   new UserCommon();
        $moUser =       $userCommon->checkIfFbUserExists($sFbid);

        $sGender = (trim($sGender) == 'female' ? 'F' : (trim($sGender) == 'male' ? 'M' : ''));

        if($checkFbId) {
            $this->assertEquals( $sFbid, $moUser->fbId, "No valid facebook ID: " . $sFbid);
        }
        $this->assertEquals( $moUser->id, $moUser->id, "No valid user Id: " . $moUser->id);
        $this->assertEquals( $sEmail, $moUser->email, "No valid user email: " . $sEmail);
        $this->assertEquals( $sName, $moUser->name, "No valid user name: " . $sName);
        $this->assertEquals( $sSurname, $moUser->surnames, "No valid user surnames: " . $sSurname);
        $this->assertEquals( $sGender, $moUser->gender, "No valid user gender: " . $sGender);

        $this->assertInternalType('integer', intval($moUser->id));
        $this->assertGreaterThanOrEqual(6000, intval($moUser->id));

        $moUserN = new AbaUser();
        if ($moUserN->getUserById($moUser->id)) {

            $this->assertEquals( FREE, $moUserN->userType, "No valid userType: " . $moUserN->userType);
            if($checkCountry) {
                $this->assertEquals( Yii::app()->config->get("ABACountryId"), $moUserN->countryId, "No valid countryId: " . $moUserN->countryId);
            }
            $this->assertEquals( $langEnv, $moUserN->langEnv, "No valid langEnv: " . $moUserN->langEnv);

            $moUserN->deleteUserThroughResetCall();
        }
    }

    /**
     * @param $sEmail1
     * @param $sPass
     */
    public function loginInFacebookWeb($sEmail, $sPass, $bFree=true, $bLogged=false) {

        if(!$bLogged) {
            $this->waitForElementPresent("id=sendSuscriptionFacebookButton");

            if($bFree) {
                $this->assertTextPresent(Yii::t("mainApp", 'Regístrate gratis y empieza ya a hablar inglés', NULL, NULL, Yii::app()->params["langAssert"]));
            }

            $this->click("id=sendSuscriptionFacebookButton");

            $this->waitForPopUp("", 10001);

            $allTitles = $this->getAllWindowTitles();

            $this->selectWindow($allTitles[1]);
            $this->assertEquals("Facebook", $this->getTitle());

            $this->waitForElementPresent("id=email");
            $this->type("id=email", $sEmail);
            $this->type("id=pass", $sPass);
            $this->clickAndWait("id=loginbutton", 10002);

            $this->close($allTitles[1]);

            $this->selectWindow($allTitles[0]);
        }
        else {
            $this->click("id=sendSuscriptionFacebookButton");
        }
        $this->refreshAndWait();

    }

    /**
     * @param $sEmail
     * @param $sPass
     */
    public function loginInFacebookCampus($sEmail, $sPass) {
        //
        // Login facebook
        //
        $this->waitForElementPresent("id=btnFbSubmit");

        $this->assertTextPresent(Yii::t("mainApp", 'Iniciar sesión', NULL, NULL, Yii::app()->params["langAssert"]));
        $this->assertTextPresent(Yii::t("mainApp", 'Contraseña', NULL, NULL, Yii::app()->params["langAssert"]));

        $this->click("id=btnFbSubmit");

        $this->waitForPopUp("", 10000);

        $allTitles = $this->getAllWindowTitles();

        $this->selectWindow($allTitles[1]);
        $this->assertEquals("Facebook", $this->getTitle());

        $this->waitForElementPresent("id=email");
        $this->type("id=email", $sEmail);
        $this->type("id=pass", $sPass);
        $this->clickAndWait("id=loginbutton", 10000);

        $this->close($allTitles[1]);

        $this->selectWindow($allTitles[0]);
        $this->refreshAndWait();
    }

    public function assertCampus($firstLoginDone=false) {

        if($firstLoginDone) {
            $this->waitForElementPresent("css=span.ui-icon.ui-icon-closethick");
            $this->click("css=span.ui-icon.ui-icon-closethick");
            $this->assertTextPresent(Yii::t("mainApp", 'Mi Cuenta', NULL, NULL, Yii::app()->params["langAssert"]));
            $this->assertTextPresent(Yii::t("mainApp", 'Salir', NULL, NULL, Yii::app()->params["langAssert"]));
        }
        else {
            $this->assertTextPresent(Yii::t("mainApp", 'Antes de empezar a aprender, indícanos por qué nivel quieres empezar', NULL, NULL, Yii::app()->params["langAssert"]));
            $this->assertTextPresent(Yii::t("mainApp", 'Mi Cuenta', NULL, NULL, Yii::app()->params["langAssert"]));
            $this->assertTextPresent(Yii::t("mainApp", 'Salir', NULL, NULL, Yii::app()->params["langAssert"]));
        }
    }


}
