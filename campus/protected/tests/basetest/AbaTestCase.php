<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abaenglish
 * Date: 9/04/13
 * Time: 13:46
 * To change this template use File | Settings | File Templates.
 */
class AbaTestCase extends CTestCase
{

    public function setUp()
    {
        // PREMIUM
        $this->createUsersIntoDb("unit.test.premium.user@yopmail.com",
                PREMIUM, Yii::app()->config->get("ABACountryId"), "UnitPremium", "Testing");
        // FREE
        $this->createUsersIntoDb("unit.test.free.user@yopmail.com",
                FREE, Yii::app()->config->get("ABACountryId"), "UnitFree", "Testing");
        // DELETED: unit.test.lead.user@yopmail.com
        $this->createUsersIntoDb("unit.test.deleted.user@yopmail.com",
                DELETED, Yii::app()->config->get("ABACountryId"), "UnitDeleted", "Testing");
        // FREE that was PREMIUM in the PAST EX-PREMIUM
        $this->createUsersExPremium("unit.test.ex.premium.user@yopmail.com",
                FREE, 105/*Italy*/, "UnitExPremium", "Testing");
        // PREMIUM CANCEL
        $this->createUsersPremiumCancel("unit.test.premium.cancelled.user@yopmail.com",
            PREMIUM, 138, "UnitPremium", "Testing Cancelled");
        // PREMIUM B2B
        $this->createUserB2bIntoDb('unit.test.premium.b2b.active@yopmail.com',
            PREMIUM, 199, "UnitPremium", "B2B Testing Premium Active");

        parent::setUp();
    }

    /**
     *
     */
    public function setUpAdyen()
    {
        // PREMIUM
        $this->createUsersIntoDb("unit.test.premium.user@yopmail.com", PREMIUM, 47, "UnitPremium", "Testing", false, '', '', false, PAY_SUPPLIER_ADYEN);
        // FREE
        $this->createUsersIntoDb("unit.test.free.user@yopmail.com", FREE, 47, "UnitFree", "Testing", false, '', '', false, PAY_SUPPLIER_ADYEN);
        // DELETED: unit.test.lead.user@yopmail.com
        $this->createUsersIntoDb("unit.test.deleted.user@yopmail.com", DELETED, 73, "UnitDeleted", "Testing", false, '', '', false, PAY_SUPPLIER_ADYEN);
        // FREE that was PREMIUM in the PAST EX-PREMIUM
        $this->createUsersExPremium("unit.test.ex.premium.user@yopmail.com", FREE, 73, "UnitExPremium", "Testing", false, '', '', false, PAY_SUPPLIER_ADYEN);
        // PREMIUM CANCEL
        $this->createUsersPremiumCancel("unit.test.premium.cancelled.user@yopmail.com", PREMIUM, 73, "UnitPremium", "Testing Cancelled", false, '', '', false, PAY_SUPPLIER_ADYEN);
        // PREMIUM B2B
        $this->createUserB2bIntoDb('unit.test.premium.b2b.active@yopmail.com', PREMIUM, 47, "UnitPremium", "B2B Testing Premium Active", false, '', '', false, PAY_SUPPLIER_ADYEN);

        parent::setUp();
    }

    /**
     *
     */
    public function tearDown()
    {
        Yii::app()->user->setId('');

        $moUser = new AbaUser();
        $moUser->getUserByEmail('unit.test.premium.user@yopmail.com', '', false);
        $moUser->deleteUserThroughResetCall();

        $moUser = new AbaUser();
        $moUser->getUserByEmail('unit.test.free.user@yopmail.com', '', false);
        $moUser->deleteUserThroughResetCall();

        $moUser = new AbaUser();
        $moUser->getUserByEmail('unit.test.deleted.user@yopmail.com', '', false);
        $moUser->deleteUserThroughResetCall();

        $moUser = new AbaUser();
        $moUser->getUserByEmail('unit.test.ex.premium.user@yopmail.com', '', false);
        $moUser->deleteUserThroughResetCall();

        $moUser = new AbaUser();
        $moUser->getUserByEmail('unit.test.premium.cancelled.user@yopmail.com', '', false);
        $moUser->deleteUserThroughResetCall();

        $moUser = new AbaUser();
        $moUser->getUserByEmail('unit.test.premium.b2b.active@yopmail.com', '', false);
        $moUser->deleteUserThroughResetCall();
    }

    /** Creates simple users for testing purposes only. Later in the tear down()
     * are eliminated.
     * @param $email
     * @param $userType
     * @param $countryId
     * @param $name
     * @param string $surnames
     * @param bool $expired
     *
     * @return bool
     */
    public function createUsersIntoDb( $email, $userType, $countryId, $name, $surnames, $expired=false, $gender='', $lang='', $firstLoginDone=false, $paySuppExtId=1 )
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail($email,"",false);

        $moUser->email = $email;
        $moUser->userType = $userType;
        $moUser->countryId = $countryId;
        $moUser->countryIdCustom =$moUser->countryId;
        $moUser->currentLevel = 6;
        $moUser->entryDate = HeDate::todaySQL(true);
        if($userType==PREMIUM){
            if($expired){
                $moUser->expirationDate = HeDate::getDateAdd(HeDate::todaySQL(false),-4/*days*/);
            }else{
//                $moUser->expirationDate = HeDate::getDateAdd(HeDate::todaySQL(false),5/*days*/);
                $moUser->expirationDate = HeDate::getDateAdd( HeDate::todaySQL(true), 0, 3);
            }
        }else{
            $moUser->expirationDate = HeDate::todaySQL(false);
        }
        $moUser->teacherId = 0;
        $moUser->name = $name;
        $moUser->surnames = $surnames;
        $moUser->langCourse = "en";
        $moUser->langEnv = "en";

        if($lang != '') {
            $moUser->langCourse =   $lang;
            $moUser->langEnv =      $lang;
        }

        if($userType==PREMIUM){
            $moUser->idPartnerFirstPay = Yii::app()->config->get("ABA_PARTNERID");
            $moUser->idPartnerCurrent = Yii::app()->config->get("ABA_PARTNERID");
        }
        else{
            $moUser->idPartnerFirstPay = NULL;
            $moUser->idPartnerCurrent = NULL;
        }
        $moUser->idPartnerSource = Yii::app()->config->get("ABA_PARTNERID");
        $moUser->telephone = "931511515";
        $moUser->setPassword("123456789",true);

        $moUser->insertUserPremiumForRegistration();

        //#4400
        if(!is_numeric($paySuppExtId)) {
            $paySuppExtId = 1;
        }

        if($userType==PREMIUM){
            $moNewCredit = new AbaUserCreditForms();
            $moNewCredit->setUserCreditForm($moUser->id, KIND_CC_DISCOVER,
                "6011020000245045", "2018", "05", "123", $name." ".$surnames, 1, '', '');
            $moNewCredit->insertUserCreditFormByUserId($moUser->id);

            $pay2= $this->createTestPayment($moUser, PAY_SUCCESS,
                HeDate::getDateAdd( HeDate::todaySQL(true), 0, -3/*months*/),
                $countryId, $moUser->idPartnerFirstPay, $moUser->idPartnerFirstPay, 180/*month period*/, 'EUR',
                $countryId.'_30', $moNewCredit->id, $paySuppExtId);
            $months = 3;
            if($expired){
                $months = -1;
            }
            $pay3= $this->createTestPayment($moUser, PAY_PENDING,
                HeDate::getDateAdd( HeDate::todaySQL(true), 0, $months),
                $countryId, PARTNER_ID_RECURRINGPAY, $moUser->idPartnerFirstPay, 180/*month period*/, 'EUR',
                $countryId.'_30', $moNewCredit->id, $paySuppExtId);
        }

        if($gender != '') {
            $moUser = new AbaUser();
            if($moUser->getUserByEmail($email)) {

                $moUser->gender = $gender;

                $sql = " UPDATE " . $moUser->tableName() .
                    " SET `gender`=:GENDER " .
                    " WHERE `id` = " . $moUser->id . " ";

                $aBrowserValues = array(":GENDER" => $moUser->gender);

                $moUser->updateSQL($sql, $aBrowserValues);
            }
        }

        if($firstLoginDone) {
            $moUserN = new AbaUser();
            if($moUserN->getUserByEmail($email)) {

                $moUserN->firstLoginDone = 1;

                $sql = " UPDATE " . $moUserN->tableName() .
                    " SET `firstLoginDone`=:FIRSTLOGINDONE " .
                    " WHERE `id` = " . $moUserN->id . " ";

                $aBrowserValues = array(":FIRSTLOGINDONE" => $moUserN->firstLoginDone);

                $moUserN->updateSQL($sql, $aBrowserValues);
            }
        }

        return true;
    }

    /**
     * Create user facebook id
     *
     * @param $sEmail
     * @param $iFbid
     *
     * @return int
     */
    protected function createUserFbidIntoDb( $sEmail, $iFbid)
    {
        $moUser =       new AbaUser();
        $userExists =   $moUser->getUserByEmail($sEmail);

        $moUserFb =     new AbaUser();
        $userFbExists = $moUserFb->getUserByFacebookId($iFbid);

        if($userExists && !$userFbExists) {

            $moUser->fbId = $iFbid;

            $sql = " UPDATE " . $moUser->tableName() .
                " SET `fbId`=:FBID " .
                " WHERE `id` = " . $moUser->id . " ";

            $aBrowserValues = array(":FBID" => $moUser->fbId);

            if ( $moUser->updateSQL($sql, $aBrowserValues) > 0 ) {
                return true;
            }
        }
        return false;
    }

    /** Sets up an scenario in database for an Ex-Premium user
     * @param $email
     * @param $userType
     * @param $countryId
     * @param $name
     * @param $surnames
     * @return bool
     */
    public function createUsersExPremium( $email, $userType, $countryId, $name, $surnames, $paySuppExtId=1 )
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail($email,"",false);

        $moUser->email = $email;
        $moUser->userType = $userType;
        $moUser->countryId = $countryId;
        $moUser->countryIdCustom =$moUser->countryId;
        $moUser->currentLevel = 4;
        $moUser->entryDate = HeDate::getDateAdd( HeDate::todaySQL(true), 0/*days*/, -4/*months*/);

        if($userType==PREMIUM){
            $moUser->expirationDate = HeDate::getDateAdd(HeDate::todaySQL(false),5);
        }else{ // FREE
            $moUser->expirationDate = HeDate::getDateAdd( HeDate::todaySQL(true), 0/*days*/, -1/*months*/);
        }
        $moUser->teacherId = 0;
        $moUser->name = $name;
        $moUser->surnames = $surnames;
        $moUser->langCourse = "en";
        $moUser->langEnv = "en";
        $moUser->registerUserSource = REGISTER_USER;
        $moUser->idPartnerSource = 45; //Groupon Finland
        $moUser->idPartnerFirstPay = Yii::app()->config->get("ABA_PARTNERID");
        $moUser->idPartnerCurrent = PARTNER_ID_RECURRINGPAY;
        $moUser->telephone = "931511515";
        $moUser->setPassword("123456789",true);
        $moUser->cancelReason = 1;
        $moUser->firstLoginDone = 1;

        $moUser->insertUserPremiumForRegistration();

        //#4400
        if(!is_numeric($paySuppExtId)) {
            $paySuppExtId = 1;
        }

        $pay1= $this->createTestPayment($moUser, PAY_SUCCESS, HeDate::getDateAdd( HeDate::todaySQL(true), 0, -3/*months*/),
                    $countryId, $moUser->idPartnerFirstPay, $moUser->idPartnerSource, 30/*month period*/, 'USD', $countryId.'_30', null, $paySuppExtId);

        $pay2= $this->createTestPayment($moUser, PAY_SUCCESS, HeDate::getDateAdd( HeDate::todaySQL(true), 0, -2/*months*/),
            $countryId, PARTNER_ID_RECURRINGPAY, $moUser->idPartnerFirstPay, 30/*month period*/, 'USD', $countryId.'_30', null, $paySuppExtId);

        $pay3= $this->createTestPayment($moUser, PAY_CANCEL, HeDate::getDateAdd( HeDate::todaySQL(true), 0, -1/*months*/),
            $countryId, PARTNER_ID_RECURRINGPAY, PARTNER_ID_RECURRINGPAY, 30/*month period*/, 'USD', $countryId.'_30', null, $paySuppExtId);

        return $pay1 && $pay2 && $pay3;
    }

    /** Sets up an scenario in database for a Premium user that has cancelled.
     *
     * @param $email
     * @param $userType
     * @param $countryId
     * @param $name
     * @param $surnames
     *
     * @return bool
     */
    protected function createUsersPremiumCancel( $email, $userType, $countryId, $name, $surnames, $paySuppExtId=1 )
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail($email,"",false);

        $moUser->email = $email;
        $moUser->userType = $userType;
        $moUser->countryId = $countryId;
        $moUser->countryIdCustom =$moUser->countryId;
        $moUser->currentLevel = 4;
        $moUser->entryDate = HeDate::getDateAdd( HeDate::todaySQL(true), 0/*days*/, -4/*months*/);
        $moUser->expirationDate = HeDate::getDateAdd(HeDate::todaySQL(false), 0, 9);
        $moUser->teacherId = 0;
        $moUser->name = $name;
        $moUser->surnames = $surnames;
        $moUser->langCourse = "en";
        $moUser->langEnv = "en";
        $moUser->registerUserSource = REGISTER_USER;
        $moUser->idPartnerSource = PARTNER_ID_ADWORDS_MEXICO;
        $moUser->idPartnerFirstPay = PARTNER_ID_ADWORDS_MEXICO;
        $moUser->idPartnerCurrent = PARTNER_ID_RECURRINGPAY;
        $moUser->telephone = "654 15456 987 ";
        $moUser->setPassword("123456789",true);
        $moUser->cancelReason = 1;
        $moUser->firstLoginDone = 1;

        $moUser->insertUserPremiumForRegistration();

        //#4400
        if(!is_numeric($paySuppExtId)) {
            $paySuppExtId = 1;
        }

        $pay1= $this->createTestPayment($moUser, PAY_SUCCESS, HeDate::getDateAdd( HeDate::todaySQL(true), 0, -4/*months*/),
            $countryId, $moUser->idPartnerFirstPay, $moUser->idPartnerSource, 360/*month period*/, 'USD', $countryId.'_360', null, $paySuppExtId);
        $pay2= $this->createTestPayment($moUser, PAY_CANCEL, HeDate::getDateAdd( HeDate::todaySQL(true), 0, 8/*months*/),
            $countryId, PARTNER_ID_RECURRINGPAY, $moUser->idPartnerFirstPay, 360/*month period*/, 'USD', $countryId.'_360', null, $paySuppExtId);

        return $pay1 && $pay2;

    }

    /** Create mock payments for being altered and checked by
     * all the rest of the tests
     * @param AbaUser $moUser
     * @param $status
     * @param $dateEndTransaction
     * @param $countryId
     * @param $idPartner
     * @param $idPartnerPrePay
     * @param $idPeriodPay
     * @param $currency
     * @param $idProduct
     * @param integer $idUserCreditForm
     * @param int $paySuppExtId
     *
     * @return bool
     */
    public function createTestPayment(AbaUser $moUser, $status, $dateEndTransaction,
            $countryId, $idPartner, $idPartnerPrePay, $idPeriodPay, $currency, $idProduct, $idUserCreditForm=NULL,
            $paySuppExtId = 1)
    {
        $moPayment = new Payment();
        $moPayment->id = HeAbaSHA::generateIdPayment($moUser, $dateEndTransaction );
        $moPayment->userId = $moUser->id;
        $moPayment->idProduct = $idProduct;
        $moPayment->idCountry = $countryId;
        $moPayment->idPeriodPay = $idPeriodPay ;
        if(!empty($idUserCreditForm)){
            $moPayment->idUserCreditForm = $idUserCreditForm;
        }

        $moPayment->idPromoCode = '';
        $moPayment->currencyTrans = $currency;
        $moPayment->foreignCurrencyTrans = $currency;
        $moPayment->amountOriginal = 19.99;
        $moPayment->amountDiscount = 0.00;
        $moPayment->amountPrice = 19.99;



        // Get currencies involved in order to stamp them in the transaction
        $xRates = new AbaCurrency( $currency );
        $moPayment->xRateToEUR = $xRates->getConversionRateTo(CCY_DEFAULT) ;
        $moPayment->xRateToUSD = $xRates->getConversionRateTo($currency) ;

        // Gateway and register Transaction relevant data:
        $moPayment->paySuppExtId = $paySuppExtId ;
        $moPayment->paySuppOrderId = $moPayment->id.'.666' ;
        $moPayment->status =  $status ;
        if($status!=PAY_PENDING){
            $moPayment->dateStartTransaction = $dateEndTransaction;
            $moPayment->dateEndTransaction = $dateEndTransaction;
        }else{
            $moPayment->dateStartTransaction = '0000-00-00';
            $moPayment->dateEndTransaction = '0000-00-00';
        }
        $moPayment->dateToPay = $dateEndTransaction;
        $moPayment->lastAction = 'TestOnly';

        $moPayment->idPartner = $idPartner;
        $moPayment->idPartnerPrePay = $idPartnerPrePay;
        $moPayment->isRecurring = 0;
        if($status!=PAY_SUCCESS){
            $moPayment->isRecurring = 1;
        }

        //
        // tax rate
        //
        $moPayment->setAmountWithoutTax();

        $ret = $moPayment->paymentProcess($moPayment->userId);

        return $ret;
    }

    /**
     * @param AbaUser $moUser
     * @param $street
     * @param $zipCode
     * @param $city
     * @param $state
     * @param $idCountry
     * @param $idProduct
     * @param integer $idPeriodPay
     * @param $cardKind
     * @param $cardName
     * @param $cardYear
     * @param $cardMonth
     * @param $cardNumber
     * @param $amountPrice
     * @param $currency
     * @param $cardCvc
     * @param string $cpfBrasil
     *
     * @return PaymentControlCheck
     */
    protected function prepareMockPayment(AbaUser $moUser, $street, $zipCode, $city, $state, $idCountry,
                                          $idProduct, $idPeriodPay, $cardKind, $cardName, $cardYear,$cardMonth, $cardNumber,
                                          $amountPrice, $currency, $cardCvc, $cpfBrasil='')
    {
        if( $state!='' ){
            $moUserAddress = new AbaUserAddressInvoice();
            $moUserAddress->setUserAddressForm($moUser->getId(), $cardName, "", $cpfBrasil, $street, "",
                $zipCode, $city, $state, $idCountry, true);
            $moUserAddress->insertUserAddress();
        }

        $testTransaction = new PaymentControlCheck();
        $testTransaction->id = HeAbaSHA::generateIdPayment( $moUser );
        $testTransaction->userId = $moUser->id;
        $testTransaction->dateStartTransaction = HeDate::todaySQL(true);
        $testTransaction->dateEndTransaction = HeDate::todaySQL(true);
        $testTransaction->dateToPay = HeDate::todaySQL(true);
        $testTransaction->cardName = $cardName;
        $testTransaction->cardCvc = $cardCvc;
        $testTransaction->cardYear = $cardYear;
        $testTransaction->cardMonth = $cardMonth;
        $testTransaction->cardNumber = $cardNumber;
        $testTransaction->kind = $cardKind;
        $testTransaction->cpfBrasil = $cpfBrasil;
        $testTransaction->amountPrice = $amountPrice;
        $testTransaction->idProduct = $idProduct;
        $testTransaction->idCountry = $idCountry;
        $testTransaction->idPeriodPay = $idPeriodPay;
        $testTransaction->currencyTrans = $currency;

        //
        //#4681
        //
        $tempCardData = array(
            "cardNumber" => $cardNumber,
            "cardYear" =>   $cardYear,
            "cardMonth" =>  $cardMonth,
            "cardCvc" =>    $cardCvc,
            "cardName" =>   $cardName,
        );
        $testTransaction->setTempCardData($tempCardData);

        return $testTransaction;
    }

    /**
     * @param $email
     * @param $userType
     * @param $countryId
     * @param $name
     * @param $surnames
     * @param bool $expired
     * 
     * @return bool
     */
    protected function createUserB2bIntoDb($email, $userType, $countryId, $name, $surnames, $expired=false)
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail($email, "", false);

        $moUser->email = $email;
        $moUser->userType = $userType;
        $moUser->countryId = $countryId;
        $moUser->countryIdCustom = $moUser->countryId;
        $moUser->currentLevel = 6;
        $moUser->entryDate = HeDate::todaySQL(true);
        $moUser->teacherId = 0;
        $moUser->name = $name;
        $moUser->surnames = $surnames;
        $moUser->langCourse = "en";
        $moUser->langEnv = "en";
        $moUser->idPartnerFirstPay = PARTNER_JUNTAEXTREMADURA;
        $moUser->idPartnerCurrent = PARTNER_JUNTAEXTREMADURA;
        $moUser->idPartnerSource = PARTNER_JUNTAEXTREMADURA;
        $moUser->telephone = "931511515";
        $moUser->setPassword("123456789", true);
        if ($userType == PREMIUM) {
            if ($expired) {
                $moUser->expirationDate = HeDate::getDateAdd(HeDate::todaySQL(false), -5, 0);
                $moUser->entryDate = HeDate::getDateAdd(HeDate::todaySQL(false), 0, -6);

                $moUser->insertUserPremiumForRegistration();

                $this->createTestPayment($moUser, PAY_SUCCESS, $moUser->entryDate, $countryId,
                    $moUser->idPartnerFirstPay, $moUser->idPartnerFirstPay, 180/*6 month period*/, 'EUR',
                    $countryId . '_180', NULL, PAY_SUPPLIER_B2B);
            } else {
                $moUser->expirationDate = HeDate::getDateAdd(HeDate::todaySQL(false), 0, 6);

                $moUser->insertUserPremiumForRegistration();

                $this->createTestPayment($moUser, PAY_SUCCESS, HeDate::todaySQL(true), $countryId,
                    $moUser->idPartnerFirstPay, $moUser->idPartnerFirstPay, 180/*6 month period*/, 'EUR',
                    $countryId . '_180', NULL, PAY_SUPPLIER_B2B);
            }
        } else {
            $moUser->expirationDate = HeDate::getDateAdd(HeDate::todaySQL(false), 0, -1);
            $moUser->entryDate = HeDate::getDateAdd(HeDate::todaySQL(false), 0, -7);

            $moUser->insertUserPremiumForRegistration();

            $this->createTestPayment($moUser, PAY_SUCCESS, $moUser->entryDate, $countryId,
                $moUser->idPartnerFirstPay, $moUser->idPartnerFirstPay, 180/*6 month period*/, 'EUR',
                $countryId . '_180', NULL, PAY_SUPPLIER_B2B);
        }

        return true;
    }


    public function assertFacebookUser(AbaUser $moUser, $sFbid, $sEmail, $sName, $sSurname, $sGender, $langEnv, $checkFbId=true) {

        $sGender = (trim($sGender) == 'female' ? 'F' : (trim($sGender) == 'male' ? 'M' : ''));

        if($checkFbId) {
            $this->assertEquals( $sFbid, $moUser->fbId, "No valid facebook ID: " . $sFbid);
        }
        $this->assertEquals( $moUser->id, $moUser->id, "No valid user Id: " . $moUser->id);
        $this->assertEquals( $sEmail, $moUser->email, "No valid user email: " . $sEmail);
        $this->assertEquals( $sName, $moUser->name, "No valid user name: " . $sName);
        $this->assertEquals( $sSurname, $moUser->surnames, "No valid user surnames: " . $sSurname);
        $this->assertEquals( $sGender, $moUser->gender, "No valid user gender: " . $sGender);

        $this->assertInternalType('integer', intval($moUser->id));
        $this->assertGreaterThanOrEqual(6000, intval($moUser->id));

        $moUserN = new AbaUser();
        if ($moUserN->getUserById($moUser->id)) {

            $this->assertEquals( FREE, $moUserN->userType, "No valid userType: " . $moUserN->userType);
            $this->assertEquals( Yii::app()->config->get("ABACountryId"), $moUserN->countryId, "No valid countryId: " . $moUserN->countryId);
            $this->assertEquals( $langEnv, $moUserN->langEnv, "No valid langEnv: " . $moUserN->langEnv);

            $moUserN->deleteUserThroughResetCall();
        }
    }


    /** Data provider of sample cards.
     *
     * @return array
     */
    public function dataCardsSamples()
    {
        return DataCollections::$aCreditForms;
    }

    /** Data Provider of show case users.
     *
     * @return array
     */
    public function dataUsersCase()
    {
        return DataCollections::$aUsersShowCase;
    }

    /**
     * @return array
     */
    public function dataAdyenCase()
    {
        return DataCollections::$aUsersAdyenCase;
    }

}
