<?php
/**
 * Created by Joaquim Forcada (Quino) in Aba English with PHP Storm
 * User: Joaquim Forcada (Quino)
 * Date: 2/05/14
 * Time: 12:28
 
 ---Please brief description here---- 
 */

abstract class DataCollections {
    static $aCreditForms = array(
        "PayPal" => array(true,
            KIND_CC_PAYPAL,
            "",
            "",
            "",
            "",
            "unit.test.account.paypal@yopmail.com",
            "",
            ""
        ),
        "MasterCard" => array(true,
            KIND_CC_MASTERCARD,
            "5453010000066167",
            "2018",
            "05",
            "123",
            "Unit Test Mastercard",
            "",
            ""
        ),
        "Discover" => array(true,
            KIND_CC_DISCOVER,
            "6011020000245045",
            "2018",
            "05",
            "123",
            "Test Unit Free Credit Card Aba English",
            "123.456.789-09",
            "CPF",
        ),
        /* NEGATIVE */
        "VISAf" => array(false,
            KIND_CC_VISA,
            "4012888888888888",
            "2018",
            "05",
            "123",
            "Test Failure Unit Free Credit Card Aba English ",
            "123.456.789-09",
            "CNPJ"
        ),
        "MASTERCARDf" =>  array(false,
            KIND_CC_MASTERCARD,
            "5453010000066167",
            "2018",
            "05",
            "446488",
            "Test Failure Unit Free Credit Card Aba English ",
            "",
            ""
        )
    );

    static $aCreditFormsAdyen = array(
        "MasterCard" => array(true,
            KIND_CC_MASTERCARD,
            "5585558555855583",
            "2016",
            "06",
            "7373",
            "Unit Test Mastercard ES Adyen",
            "",
            ""
        ),
        "Discover" => array(true,
            KIND_CC_DISCOVER,
            "6011601160116611",
            "2016",
            "06",
            "737",
            "Test Unit Discover US Adyen",
            "",
            "",
        ),
        "VISA" => array(false,
            KIND_CC_VISA,
            "4977949494949497",
            "2016",
            "06",
            "737",
            "Test Unit Visa FR Adyen ",
            "",
            ""
        ),
        "AMEX" =>  array(false,
            KIND_CC_MASTERCARD,
            "374251021090003",
            "2016",
            "06",
            "737",
            "Test Failure Unit Free Credit Card Aba English ",
            "",
            ""
        )
    );

    static $aUsersShowCase = array(
        'PREMIUM_ACTIVE_CAIXA' =>  array('unit.test.premium.user@yopmail.com'),
        'FREE' =>  array('unit.test.free.user@yopmail.com'),
        'DELETED' =>  array('unit.test.deleted.user@yopmail.com'),
        'FREE_EXPREMIUM_CAIXA' =>  array('unit.test.ex.premium.user@yopmail.com'),
        'PREMIUM_CANCEL_CAIXA' =>  array('unit.test.premium.cancelled.user@yopmail.com'),
        'PREMIUM_ACTIVE_B2B' => array('unit.test.premium.b2b.active@yopmail.com'),
    );

    static $aUsersExtranetCase = array(
        'PREMIUM' =>  array('unit.functional.premium.extranet.ex@yopmail.com'),
        'FREE' =>  array('unit.functional.free.extranet.ex@yopmail.com'),
        'FREE_PREMIUM' =>  array('unit.functional.ex.premium.extranet.ex@yopmail.com'),
        'PREMIUM_CANCEL' =>  array('unit.functional.premium.cancelled.extranet.ex@yopmail.com'),
        'DELETED' =>  array('unit.functional.deleted.extranet.ex@yopmail.com'),
    );

    static $aUsersAdyenCase = array(
        'PREMIUM_ACTIVE_ADYEN' =>   array('unit.test.premium.user@yopmail.com'),
        'FREE' =>                   array('unit.test.free.user@yopmail.com'),
        'DELETED' =>                array('unit.test.deleted.user@yopmail.com'),
        'FREE_EXPREMIUM_ADYEN' =>   array('unit.test.ex.premium.user@yopmail.com'),
        'PREMIUM_CANCEL_ADYEN' =>   array('unit.test.premium.cancelled.user@yopmail.com'),
        'PREMIUM_ACTIVE_B2B' =>     array('unit.test.premium.b2b.active@yopmail.com'),
    );


}