<?php

/**
 * To be able to run tests we should start the Selenium Server.
 */
class PaymentsAllpagoControllerTest extends PaymentsControllerTestBase
{
    /**
     * Creates all test users and any other test data used specific from this test.
     */
    public function setUp()
    {
        $aUsersToCreate = $this->dataBrazilUsersTest();
        foreach ($aUsersToCreate as $userCols) {
            $this->createUsersIntoDb($userCols[0], FREE, $userCols[1], $userCols[2], $userCols[3], false, $userCols[4]);
        }

        parent::setUp();
    }

    /**
     * Destroy all test users and any other test data used specific from this test.
     */
    public function tearDown()
    {
        $aUsersToDelete = $this->dataBrazilUsersTest();
        foreach($aUsersToDelete as $userCols){
            $this->resetDownUser($userCols[0]);
        }

        parent::tearDown();
    }

    /**
     * Tests The payment method page and it process a payment.
     * @dataProvider dataCombinations
     */
    public function testPaymentMethodTPVAndBuy($email, $countryId, $name, $lastName, $langEnv, $creditType,
                                               $cardNumber, $cardMonth, $cardYear, $cardCvc, $cpf, $ipLocation )
    {
        if ($this->selectPaymentProductPrice($email, 12, '?IP_FALSE_TEST='.$ipLocation)) {

//            $this->select('id=PaymentForm_creditCardType', 'value=' .KIND_CC_JCB);
            $this->type('id=PaymentForm_creditCardType', 'value=' .KIND_CC_NO_CARD);
            $this->type("id=PaymentForm_creditCardName", $name);
            $this->type("id=PaymentForm_creditCardNumber", $cardNumber);
            $this->select('id=PaymentForm_creditCardMonth', 'index=' . $cardMonth);
            $this->select('id=PaymentForm_creditCardYear', 'label=' . $cardYear);
            $this->type("id=PaymentForm_firstName", $name);
            $this->type("id=PaymentForm_secondName", $lastName);
            $this->type("id=PaymentForm_cpfBrasil", $cpf);

            $this->clickAndWait("id=btnABA");
            // A validation should arise, and then we correct it:
            $this->assertTextPresent(Yii::t('mainApp', 'Debes introducir el código CVC', array(), NULL, $langEnv));
            // Reloading page at this point...
            $this->assertTrue($this->isElementPresent("id=btnABA"));
            // Fix and submit form again
            $this->type("id=PaymentForm_CVC", $cardCvc);
            $this->clickAndWait("id=btnABA");

            //We have forced a validation from allPago, brand/credit not corresponding to each other:
            //#4848
//            $this->assertTrue($this->isElementPresent("id=dIdErrorSummary"));
            $this->assertFalse($this->isElementPresent("id=dIdErrorSummary"));
            $moUser = new AbaUser();
            $moUser->getUserByEmail($email);

            $moLogs = new PayGatewayLogAllpago();
            $moLogs->getLastLogByUserId($moUser->getId());
            //#4848
//            $this->assertEquals('100.100.700', $moLogs->errorCode, 'The error code from ALLPAGO should be invalid brand combination');
            $this->assertEquals(null, $moLogs->errorCode, 'The error code from ALLPAGO should be invalid brand combination');

            //#4848
//            // Fix validation message selecting the right card cvc:
//            $this->select('id=PaymentForm_creditCardType', 'value=' .strval($creditType));
//            $this->clickAndWait("id=btnABA");

            $this->goAndAssertPremiumAfterPayment($langEnv);
            $this->assertRegitrationOnAllPago( $moUser, $cpf );


        } else {
            $this->assertFalse(true);
        }

        return true;
    }


    public function dataBrazilUsersTest()
    {
        return array(
            array( 'unit.test.brazil.pt_A@yopmail.com', COUNTRY_BRAZIL,
                'Unit Test', 'Portûguese AllPago', 'pt'),
            array( 'unit.test.brazil.pt_B@yopmail.com', COUNTRY_BRAZIL,
                'Unit Test', 'Brazil English AllPago', 'en'),
        );
    }

    public function dataIps()
    {
        return array(
            array('46.136.173.1'), //Brazil,
            array('37.72.0.15'), //Spain, Valencia
        );
    }

    public function dataCardTypes()
    {
        return array(
            array(KIND_CC_VISA, '4012001038443335', '05', '2018', '123', '123.456.789-09'),
            array(KIND_CC_MASTERCARD, '5453010000066167', '05', '2018', '123', '804.654.341-18'),
            array(KIND_CC_AMERICAN_EXPRESS, '376449047333005', '05', '2018', '1234', '123.456.789-09'),
            array(KIND_CC_DISCOVER, '6011020000245045', '05', '2018', '123', '804.654.341-18'),
        );
    }

    /** Combines all of three previous data arrays together.
     *
     * @return array|null
     */
    public function dataCombinations()
    {
        $retArrayCombinations = array();
        $aCombinations = $this->unitTestDataCartesian($this->dataCardTypes(), $this->dataBrazilUsersTest());
        foreach ($aCombinations[0] as $comb) {
            $retArrayCombinations[] = $comb;
        }
        $aCombinations = $this->unitTestDataCartesian($this->dataIps(), $retArrayCombinations);
        $retArrayCombinations = NULL;
        foreach ($aCombinations[0] as $comb) {
            $retArrayCombinations[] = $comb;
        }
        return $retArrayCombinations;
    }

}
