<?php
/**
 * To test web services that are in charge of registering users
 * User: Quino
 * Date: 7/05/13
 * Time: 12:39
 * To change this template use File | Settings | File Templates.
 */
class WsoapabaControllerTest extends AbaWebTestCase
{
    protected $urlWs;

    /**
     * Override to set URL public ws
     */
    public function setUp()
    {
        parent::setUp();
        $this->urlWs = "http://" . Yii::app()->config->get("SELF_DOMAIN") . "/wsoapaba/usersapi/";
    }

    /**
     * Deletes test users used in this class
     */
    public function tearDown()
    {
        parent::tearDown();
    }

    /** Checks if the WSDL definition file is public and available.
     * @test
     *
     * @return Boolean
     */
    public function testWsdlAvailable()
    {
        $this->open("/wsoapaba/usersapi/");

        $this->assertTrue($this->isElementPresent("css=definitions[name=\"WsoapabaController\"]"));

        $opts = array(
            'http' => array(
                'method'=>"GET",
                'header'=>"Content-Type: text/xml; charset=utf-8"
            )
        );
        $context = stream_context_create($opts);
        $xmlWsdl = file_get_contents($this->urlWs, false, $context );
        try{
            $oXmlWsdl =  new SimpleXMLElement($xmlWsdl);
            // IsValid XML
            $this->assertGreaterThan(5, count($oXmlWsdl->getDocNamespaces()));
        }
        catch(Exception $e){
            $this->assertTrue (false, 'The WSDL is not accesible or is invalid: '.$e->getMessage());
        }

        return true;
    }

    /** Checks WSDL, functions and version method
     * @test
     *
     * @return SoapClient SoapClient
     */
    public function testFunctionsAvailable()
    {
        ini_set("soap.wsdl_cache_enabled", 0);
        $client = new SoapClient($this->urlWs, array('soap_version'=> SOAP_1_2, 'port'=> 80, 'trace' => 1));

        $aFunctions = $client->__getFunctions();
        $this->assertTrue((strpos($aFunctions[0], 'Array version()') >= 0));
        $this->assertTrue((strpos($aFunctions[4], 'Array registerUserPremiumB2bExtranet') >= 0));

        /** @noinspection PhpUndefinedMethodInspection */
        $version = $client->version();
        $this->assertTrue(floatval($version["version"]) == 1.29);

        return $client;
    }

    /**
     * @param int $nUsers
     * @return array
     *
     * 9, 21
     *
     */
    protected static function getTestUsers($nUsers=9) {

        $sEmailPrefix = 'unit.test.premium.user';
        $sEmailSuffix = 'extranetusertest.com';

        //
        // user template
        $sUserT = array
        (
            'email' =>          '',
            'pwd' =>            '121212121',
            'langEnv' =>        'es',
            'name' =>           'Unit test',
            'surnames' =>       'Premium user extranet',
            'currentLevel' =>   '3',
            'teacherEmail' =>   'miriamsanchez@linkup-learning.es',
            'codeDeal' =>       'EXTRANET180',
            'partnerId' =>      '7002',
            'periodId' =>       '6',
            'idCountry' =>      '199',
            'device' =>         'd'
        );

        $aUsers = array();

        for($iCounter = 1; $iCounter <= $iNumUsers; $iCounter++) {

            $aUserTmpl =            $sUserT;
            $aUserTmpl['email'] =   $sEmailPrefix . $iCounter . '@' . $sEmailSuffix;

            //
            // enabled extranet periods
            //
            if($nUsers == 9) {
                if($iCounter < 3) { $aUserTmpl['codeDeal'] = 'EXTRANET180'; $aUserTmpl['periodId'] = '6'; }
                elseif($iCounter < 6) { $aUserTmpl['codeDeal'] = 'EXTRANET360'; $aUserTmpl['periodId'] = '12'; }
                else{ $aUserTmpl['codeDeal'] = 'EXTRANET180'; $aUserTmpl['periodId'] = '3'; }
            }
            else {
                if($iCounter < 3) { $sUserTmpl['codeDeal'] = 'EXTRANET30'; $sUserTmpl['periodId'] = '1'; }
                elseif($iCounter < 6) { $sUserTmpl['codeDeal'] = 'EXTRANET90'; $sUserTmpl['periodId'] = '3'; }
                elseif($iCounter < 9) { $sUserTmpl['codeDeal'] = 'EXTRANET180'; $sUserTmpl['periodId'] = '6'; }
                elseif($iCounter < 12) { $sUserTmpl['codeDeal'] = 'EXTRANET360'; $sUserTmpl['periodId'] = '12'; }
                elseif($iCounter < 15) { $sUserTmpl['codeDeal'] = 'EXTRANET540'; $sUserTmpl['periodId'] = '18'; }
                elseif($iCounter < 18) { $sUserTmpl['codeDeal'] = 'EXTRANET720'; $sUserTmpl['periodId'] = '24'; }
                else{ $sUserTmpl['codeDeal'] = 'EXTRANET180'; $sUserTmpl['periodId'] = '3'; }
            }

            $aUsers[] = $aUserTmpl;
        }

        return array(
            'users' =>  $aUsers,
            'prefix' => $sEmailPrefix,
            'suffix' => $sEmailSuffix,
        );
    }

    /** Tests That Extranet users converted to Premium users
     * @test
     *
     * @param SoapClient $client
     * @return bool
     */
    public function testNotRegisterUserValidationsCheck(SoapClient $client)
    {
        ini_set("soap.wsdl_cache_enabled",  0);
        ini_set("default_socket_timeout",   600);

        $aUserT = self::getTestUsers(9);

        $aUsers = $aUserT['users'];

        $aUsersInvalidSignature = array();
        $aUsersDisabledExtranet = array();

        foreach($aUsers as $iKey => $aUser) {

            //
            // invalid signature
            $aUserInvalidSignaturerTmpl =           $aUser;
            $aUserInvalidSignaturerTmpl['pwd'] =    '090909090';
            $aUsersInvalidSignature[] =             $aUserInvalidSignaturerTmpl;

            //
            // force disabled product period
            $aUserDisabledExtranet =                $aUser;
            $aUserDisabledExtranet['periodId'] =    '1';
            $aUsersDisabledExtranet[] =             $aUserDisabledExtranet;
        }

        $sUsers = json_encode($aUsers);
        $sUsersInvalidSignature = json_encode($aUsersInvalidSignature);
        $sUsersDisabledExtranet = json_encode($aUsersDisabledExtranet);

        $signature = HeAbaSHA::_md5(WORD4WSEXTRANET . $sUsers);
        $invalidSignature = HeAbaSHA::_md5(WORD4WSEXTRANET . $sUsersInvalidSignature);
        $disabledExtranetSignature = HeAbaSHA::_md5(WORD4WSEXTRANET . $sUsersDisabledExtranet);


        /* @var WsoapabaController $client */
        try{

            //
            // invalid signature
            $aResponseSignature = $client->registerUserPremiumB2bExtranet($invalidSignature, $sUsers);

            $this->assertArrayHasKey("502", $aResponseSignature);
            $this->assertContains("Invalid Signature", $aResponseSignature["502"]);

            //
            // disabled product price for extranet
            $aResponseDisabled = $client->registerUserPremiumB2bExtranet($disabledExtranetSignature, $sUsersDisabledExtranet);

            foreach($aResponseDisabled as $iKey => $aUserResp) {

                $aUserResponse = $aUserResp['response'];

                $this->assertArrayHasKey("508", $aUserResponse);
                $this->assertContains('Id product ', $aUserResponse["508"]); // "The product is not valid"
            }

            foreach($aResponseSignature as $iKey => $aUserResp) {
                $aUserResponse = $aUserResp['response'];

                if(isset($aUserResponse["userId"])) {
                    $moUser = new AbaUser();
                    if ($moUser->getUserById($aUserResponse["userId"])) {
                        $moUser->deleteUserThroughResetCall();
                    }
                }
            }

            foreach($aResponseDisabled as $iKey => $aUserResp) {
                $aUserResponse = $aUserResp['response'];

                if(isset($aUserResponse["userId"])) {
                    $moUser = new AbaUser();
                    if ($moUser->getUserById($aUserResponse["userId"])) {
                        $moUser->deleteUserThroughResetCall();
                    }
                }
            }

        }catch (Exception $exc){
            $response =  $client->__getLastResponse();
            $fullResponse = var_export ( $response );
            echo $fullResponse;
        }

        return true;
    }


//    /** Tests That users Ex-Premium are converted to Premium and will belong to B2b
//     * @test
//     *
//     * @depends testFunctionsAvailable
//     *
//     * @param SoapClient $client
//     * @return bool
//     */
//    public function testRegisterUserB2bExPremiumExtranet(SoapClient $client)
//    {
////        ini_set("soap.wsdl_cache_enabled",  0);
////        ini_set("default_socket_timeout",   600);
//
//        $aUserT = self::getTestUsers(21);
//
//        $aUsers = $aUserT['users'];
//
//        $aUsersInvalidSignature = array();
//        $aUsersDisabledExtranet = array();
//
//        foreach($aUsers as $iKey => $aUser) {
//
//            //
//            // invalid signature
//            $aUserInvalidSignaturerTmpl =           $aUser;
//            $aUserInvalidSignaturerTmpl['pwd'] =    '090909090';
//            $aUsersInvalidSignature[] =             $aUserInvalidSignaturerTmpl;
//
//            //
//            // force disabled product period
//            $aUserDisabledExtranet =                $aUser;
//            $aUserDisabledExtranet['periodId'] =    '1';
//            $aUsersDisabledExtranet[] =             $aUserDisabledExtranet;
//        }
//
//
//
//        $sEmailPrefix = 'unit.test.premium.user';
//        $sEmailSuffix = 'abaenglish.com';
//
//        //
//        // user template
//        $sUserT = array
//        (
//            'email' =>          '',
//            'pwd' =>            '121212121',
//            'langEnv' =>        'es',
//            'name' =>           'Unit test',
//            'surnames' =>       'Premium user extranet',
//            'currentLevel' =>   '3',
//            'teacherEmail' =>   'miriamsanchez@linkup-learning.es',
////         'codeDeal' =>       'EXTRANET90',
//            'codeDeal' =>       '',
//            'partnerId' =>      '7002',
//            'periodId' =>       '6',
//            'idCountry' =>      '199',
//            'device' =>         'd'
//        );
//
//        $iNumUsers = 35;
//
//        for($iCounter = 1; $iCounter <= $iNumUsers; $iCounter++) {
//
//            $sUserTmpl =            $sUserT;
//            $sUserTmpl['email'] =   $sEmailPrefix . $iCounter . '@' . $sEmailSuffix;
//
//            if($iCounter < 5) { /* $sUserTmpl['codeDeal'] = 'EXTRANET30'; */ $sUserTmpl['periodId'] = '1'; }
//            elseif($iCounter < 10) { /* $sUserTmpl['codeDeal'] = 'EXTRANET90'; */ $sUserTmpl['periodId'] = '3'; }
//            elseif($iCounter < 15) { /* $sUserTmpl['codeDeal'] = 'EXTRANET180'; */ $sUserTmpl['periodId'] = '6'; }
//            elseif($iCounter < 20) { /* $sUserTmpl['codeDeal'] = 'EXTRANET360'; */ $sUserTmpl['periodId'] = '12'; }
//            elseif($iCounter < 25) { /* $sUserTmpl['codeDeal'] = 'EXTRANET540'; */ $sUserTmpl['periodId'] = '18'; }
//            elseif($iCounter < 30) { /* $sUserTmpl['codeDeal'] = 'EXTRANET720'; */ $sUserTmpl['periodId'] = '24'; }
//            else{ /* $sUserTmpl['codeDeal'] = 'EXTRANET180'; */ $sUserTmpl['periodId'] = '3'; }
//
//            $aUsers[] = $sUserTmpl;
//        }
//
//        $sUsers = json_encode($aUsers);
//
//        $signature = HeAbaSHA::_md5(WORD4WSEXTRANET . $sUsers);
//
//
//        /* @var WsoapabaController $client */
//        try{
//
//            $aResponse = $client->registerUserPremiumB2bExtranet($signature, $sUsers);
//
//        }catch (Exception $exc){
//            $response =  $client->__getLastResponse();
//            $fullResponse = var_export ( $response );
//            echo $fullResponse;
//        }
//
//        foreach($aResponse as $iKey => $aUserResp) {
//
//            $aUserResponse = $aUserResp['response'];
//
//            //It should equal to $aUserResponse = array("userId","result")
//            $this->assertArrayHasKey('userId', $aUserResponse, "Response web service returned: " . implode(array_keys($aUserResponse)));
//            $this->assertArrayHasKey('result', $aUserResponse);
//            $this->assertInternalType('integer', intval((isset($aUserResponse["userId"]) ? $aUserResponse["userId"] : 0)));
//            $this->assertGreaterThanOrEqual(6000, intval((isset($aUserResponse["userId"]) ? $aUserResponse["userId"] : 0)));
//
//            // Check consistency fields from table users and others:
//            $moUser = new AbaUser();
//            $this->assertTrue( $moUser->getUserById(intval((isset($aUserResponse["userId"]) ? $aUserResponse["userId"] : 0))));
//            $this->assertTrue( $moUser->getUserById((isset($aUserResponse["userId"]) ? $aUserResponse["userId"] : 0)), 'User id not found, expected after callign WS');
//
//            // General assertions on USER EX PREMIUM CONVERTED TO B2B PREMIUM:
//            $this->assertEquals( PREMIUM, intval($moUser->userType));
//            $this->assertTrue( HeDate::isGreaterThanToday(HeDate::europeanToSQL($moUser->expirationDate)));
//            $this->assertNotEquals(intval($moUser->idPartnerSource), 7002, 'Partner Source for an Ex-Premium should not be updated');
//            $this->assertEquals( intval($moUser->idPartnerCurrent), 7002, 'Partner current should be updated');
//
//            $moTeacher = new AbaTeacher();
//            $moTeacher->getUserByEmail('miriamsanchez@linkup-learning.es');
//            $this->assertEquals( $moTeacher->id, $moUser->teacherId);
//            $this->assertEquals( 'Test Ex-Premium B2b', $moUser->name, 'Name is updated even it existed.');
//
//            $moPayment = new Payment();
//            $moPayment = $moPayment->getLastSuccessPayment($moUser->id);
//            $this->assertEquals( intval($moPayment->idPartner), 7002, 'Partner payment should be 7002');
//            $this->assertNotEquals( intval($moPayment->idPartnerPrePay), 7002, 'Partner payment should not be 7002');
//            $this->assertEquals(PAY_SUPPLIER_B2B, $moPayment->paySuppExtId, 'Method of payment should be B2B');
////            $this->assertEquals($moPayment->idPromoCode, 'COMPUTENSE180');
//
//        }
//
//        foreach($aResponse as $iKey => $aUserResp) {
//            $aUserResponse = $aUserResp['response'];
//
//            if(isset($aUserResponse["userId"])) {
//                $moUser = new AbaUser();
//                if ($moUser->getUserById($aUserResponse["userId"])) {
//                    $moUser->deleteUserThroughResetCall();
//                }
//            }
//        }
//
//        return true;
//    }


}
