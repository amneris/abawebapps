<?php
/**
 * Created by Joaquim Forcada (Quino) in Aba English
 * User: Joaquim Forcada (Quino)
 * Date: 1/04/14
 * Time: 11:04
 
 ---Please brief description here---- 
 */

class WsoapabaselligentControllerTest extends AbaWebTestCase
{
    protected $urlWs;

    /**
     * Override to set URL public ws
     */
    public function setUp()
    {
        parent::setUp();
        $this->urlWs = "http://" . Yii::app()->config->get("SELF_DOMAIN") . "/wsoapabaselligent/senddata/";
    }

    /**
     * Deletes test users used in this class
     */
    public function tearDown()
    {
        parent::tearDown();
    }


    /** Checks if the WSDL definition file is public and available.
     * @test
     * @return Boolean
     */
    public function testWsdlAvailable()
    {
        $this->open("/wsoapabaselligent/senddata/");

        $this->assertTrue($this->isElementPresent("css=definitions[name=\"WsoapabaselligentController\"]"));

        $opts = array(
            'http' => array(
                'method'=>"GET",
                'header'=>"Content-Type: text/xml; charset=utf-8"
            )
        );
        $context = stream_context_create($opts);
        $xmlWsdl = file_get_contents($this->urlWs, false, $context );
        try{
            $oXmlWsdl =  new SimpleXMLElement($xmlWsdl);
            // IsValid XML
            $this->assertGreaterThan(5, count($oXmlWsdl->getDocNamespaces()));
        }
        catch(Exception $e){
            $this->assertTrue (false, 'The WSDL is not accesible or is invalid: '.$e->getMessage());
        }

        return true;
    }

    /** Checks WSDL, functions and version method
     * @test
     * @return SoapClient SoapClient
     */
    public function testFunctionsAvailable()
    {
        ini_set("soap.wsdl_cache_enabled", 0);
        /* @var WsoapabaselligentController $client */
        $client = new SoapClient($this->urlWs, array('soap_version'=> SOAP_1_2, 'port'=> 80, 'trace' => 1));

        $aFunctions = $client->__getFunctions();
        $this->assertTrue((strpos($aFunctions[0], 'Array version()') >= 0));
        $this->assertTrue((strpos($aFunctions[1], 'Array cancellations') >= 0));
        $this->assertTrue((strpos($aFunctions[2], 'Array passwordChange') >= 0));
        $this->assertTrue((strpos($aFunctions[3], 'Array synchroUser') >= 0));
        $this->assertTrue((strpos($aFunctions[4], 'Array unsubscribe') >= 0));
        $this->assertTrue((strpos($aFunctions[5], 'Array refundPayment') >= 0));
        $this->assertTrue((strpos($aFunctions[6], 'Array changeCreditInPayment') >= 0));
        $this->assertTrue((strpos($aFunctions[7], 'Array createCreditForm') >= 0));
        $this->assertTrue((strpos($aFunctions[8], 'Array cancelPayment') >= 0));

        /** @noinspection PhpUndefinedMethodInspection */
        $version = $client->version();
        $this->assertTrue(floatval($version["version"]) == 1.7);

        return $client;
    }

    /** Tests web service of register user in a SIMPLE way, only connection and
     * creation of user
     * @test
     *
     * @depends testFunctionsAvailable
     */
    public function testPasswordChange( SoapClient $client)
   {
       // Invalid Signature
       $signature =  HeAbaSHA::_md5('123456789' . 'unit.test.ex.premium.user@yopmail.com' . 'testFunctional123456789');
       $moUser = new AbaUser();
       $moUser->getUserByEmail('unit.test.ex.premium.user@yopmail.com', false, false);
       $oldPassword = $moUser->password;
       $oldKeyExtLogin = $moUser->keyExternalLogin;
       /* @var WsoapabaselligentController $client */
       $aResponse = $client->passwordChange($signature . '123', $moUser->id, 'testFunctional1234567890');

       //It should equal to $aResponse = array("userId","result","redirectUrl")
       $this->assertArrayHasKey('result', $aResponse,
           "Response web service returned: ".implode(array_keys($aResponse)));
       $this->assertArrayHasKey('details', $aResponse);


       $moUser->getUserByEmail('unit.test.ex.premium.user@yopmail.com', false, false);
//       $moUser->refresh();
       $newPassword = $moUser->password;
       $newKeyExtLogin = $moUser->keyExternalLogin;
       $this->assertEquals($oldKeyExtLogin, $newKeyExtLogin, 'KeyExternal Login should be equal');
       $this->assertEquals($oldPassword, $newPassword, 'Password should be equal');


       return true;
   }
}
