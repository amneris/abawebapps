<?php

/**
 *
 */
class PaymentsPayPalControllerTest extends PaymentsControllerTestBase
{
    /**
     * Creates all test users and any other test data used specific from this test.
     */
    public function setUp()
    {
        // FREE FROM BRAZIL
        $this->createUsersIntoDb('unit.test.free.user.spain@yopmail.com', FREE, 199, "UnitFree",
                    "Testing Spain", false, 'es');

        parent::setUp();
    }

    /**
     * Destroy all test users and any other test data used specific from this test.
     */
    public function tearDown()
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail('unit.test.free.user.spain@yopmail.com', "", false);
        $moUser->deleteUserThroughResetCall();

        parent::tearDown();
    }


    /** Tests Payment with PayPal, but just until
     * the step when PayPal returns to Campus page for last review.
     *
     * @return bool
     */
    public function testPaymentMethodPayPalAuthenticationAnd1stStep(  )
    {
        if($this->selectPaymentProductPrice('unit.test.free.user.spain@yopmail.com', 12) )
        {
            // Method Payment in Campus----------------
             //css=#idTotalAmountFinalPrice > div.normalPrice
            $amountPrice = $this->getText("css=#idTotalAmountFinalPrice > div.normalPrice");
            $amountPrice = intval( str_replace('US $','',$amountPrice) );

            $this->click("id=PaymentForm_methodPayment_1");
            $this->assertEquals("on", $this->getValue("id=PaymentForm_methodPayment_1"));
            $this->assertElementPresent("id=divIdPurchaseSummary");
            $this->click("id=PaymentForm_acceptConditions");
            $this->clickAndWait("id=btnABA");

            // PayPal authentication page:--------------------
            $this->assertTrue($this->isElementPresent("css=span.spriteLogo.paypallock"));
            $this->assertTrue($this->isElementPresent("id=submitLogin"));
            $this->type("id=login_email", "test.spain.sandbox@yopmail.com");
            $this->type("id=login_password", "123456789");
            $this->click("id=submitLogin");

            // Agree and Continue
            $this->waitForElementPresent("id=continue");
            $this->clickAndWait("id=continue");

            // Back to Campus Review Page:-------------
            $this->assertTextPresent(Yii::t('mainApp', "review_confirmation_paypal_key_0"));
            $this->assertElementPresent("id=divIdBuyCarReviewPaymentBody");
            $this->assertElementPresent("id=spIdAmountPrice");
            $this->assertText("id=spIdAmountPrice",strval($amountPrice));
            $this->click("id=aBtnConfirmPayPal");
            $this->waitForPageToLoad("45000");

            // Welcome to Premium page:----------------
            $this->goAndAssertPremiumAfterPayment('es');
        }
        else
        {
            $this->assertFalse(true);
        }

        return true;

    }

}
