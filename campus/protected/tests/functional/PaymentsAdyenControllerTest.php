<?php

/**
 *
 */
class PaymentsAdyenControllerTest extends PaymentsControllerTestBase
{
    /**
     * Creates all test users and any other test data used specific from this test.
     */
    public function setUp()
    {
        $this->createUsersIntoDb('unit.test.free.user.colombia@yopmail.com',    FREE, 47,   "UnitFree", "Testing Colombia", false, 'en');
        $this->createUsersIntoDb('unit.test.free.user.francia@yopmail.com',     FREE, 73,   "UnitFree", "Testing Francia",  false, 'fr');
        $this->createUsersIntoDb('unit.test.free.user.usa@yopmail.com',         FREE, 226,  "UnitFree", "Testing Russia",   false, 'ru');

        parent::setUpNoCard();
    }

    /**
     * Destroy all test users and any other test data used specific from this test.
     */
    public function tearDown()
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail('unit.test.free.user.colombia@yopmail.com', "", false);
        $moUser->deleteUserThroughResetCall();

        $moUser = new AbaUser();
        $moUser->getUserByEmail('unit.test.free.user.francia@yopmail.com', "", false);
        $moUser->deleteUserThroughResetCall();

        $moUser = new AbaUser();
        $moUser->getUserByEmail('unit.test.free.user.usa@yopmail.com', "", false);
        $moUser->deleteUserThroughResetCall();

        parent::tearDownNoCard();
    }

    /**
     *  *** Countries 47, 73 & 226  paySupplier = 10 (Adyen) ***
     *
     * @return array
     */
    public function dataUsers()
    {
        return array(
            array( 'unit.test.free.user.colombia@yopmail.com',  47,     'Test Unit Free Colombia',   'English Colombia',   'en', 12,   '01-09-1977', 'Сергиев Посад',  '009988776655', '2я станционная',   '4б, второй блок с права, 2я лестница', 'Сергиев Посад',    '012343210',    'Ы2345432Ы'),
            array( 'unit.test.free.user.francia@yopmail.com',   73,     'Test Unit Free Francia',    'Frances Francia',    'fr', 1,    '01-09-1978', 'Brasov',         '008877665544', 'Gran Via',         '66',                                   'Brasov',           '12345',        'X1111111W'),
            array( 'unit.test.free.user.usa@yopmail.com',       226,    'Test Unit Free Usa',        'Russian Usa',        'ru', 6,    '01-09-1979', 'New York',       '007766554433', 'Big Street',       '99',                                   'Washington',       '234567899',    'Ю-12345-Ъ'),
        );
    }

    public function dataCountries()
    {
        return array(
            47 => array(47, 'CO'),
            73 => array(73, 'FR'),
        );
    }

    public function dataIps()
    {
        return array(
//            array('46.136.173.1'),  // Brazil,
            array('37.72.0.15'),    // Spain, Valencia
        );
    }

    public function dataCardTypes()
    {
        return array(
            array(KIND_CC_VISA,             '4988 4388 4388 4305',  '06', '2016', '737'),   // VISA Classic	ES
            array(KIND_CC_AMERICAN_EXPRESS, '374251021090003',      '06', '2016', '7377'),  // AMEX
            array(KIND_CC_DISCOVER,         '6011 6011 6011 6611',  '06', '2016', '737'),   // DISCOVER US
            array(KIND_CC_MASTERCARD,       '5577 0000 5577 0004',  '06', '2016', '737'),   // MasterCard PL
            array(KIND_CC_MASTERCARD,       '3600 6666 3333 44',    '06', '2016', '737'),   // DINERS
        );
    }

    /** Combines all of three previous data arrays together.
     *
     * @return array|null
     */
    public function dataCombinations()
    {
        $retArrayCombinations = array();
        $aCombinations = $this->unitTestDataCartesian($this->dataCardTypes(), $this->dataUsers());
        foreach ($aCombinations[0] as $comb) {
            $retArrayCombinations[] = $comb;
        }
        $aCombinations = $this->unitTestDataCartesian($this->dataIps(), $retArrayCombinations);
        $retArrayCombinations = NULL;
        foreach ($aCombinations[0] as $comb) {
            $retArrayCombinations[] = $comb;
        }
        return $retArrayCombinations;
    }

    /**
     * Tests selection of product and checks price on the Payment method
     */
    protected function gotoPaymentSelectProductAndPrice($email, $months)
    {
        $this->loginUserByEmail($email);
        // If Popup User Objectives questionnaire is displayed, then we close the dialog box
        if( $this->isElementPresent("id=CampusModals") ) {
            $this->click("class=ui-dialog-titlebar-close ui-corner-all");
        }
        // We click on Go To Premium banner
        $this->clickAndWait("id=idDivGoPremium");

        $moUser = new AbaUser();
        $moUser->getUserByEmail($email, "", false);

        // Assert Selection product selection product page is open:
        $this->assertTrue( $this->assertProductSelectionPage($moUser->countryId) );

        // We click on Continue to 6 months subscription:
        $this->clickAndWait('id=BtnMonthContinue-'.$months);
        $this->assertPaymentMethodPage( $moUser->langEnv, $months);

        return true;
    }

    /**
     * Tests The payment method page and it process a payment.
     * @dataProvider dataCombinations
     */
    public function testPaymentMethodCardWithInvoices(
        $email, $countryId, $name, $lang, $langEnv, $monthId, $birthDate, $city, $telephone, $street, $streetMore, $province, $cp, $ident,
        $creditType, $cardNumber, $cardMonth, $cardYear, $cardCvc,
        $ipLocation
    ) {
        if ($this->gotoPaymentSelectProductAndPrice($email, $monthId)) {

            $this->type("id=PaymentForm_creditCardName",    $name);
            $this->type("id=PaymentForm_creditCardNumber",  $cardNumber); // VISA Classic	ES
            $this->select('id=PaymentForm_creditCardMonth', 'index=' . $cardMonth);
            $this->select('id=PaymentForm_creditCardYear',  'label=' . $cardYear);
            $this->clickAndWait("id=btnABAadyen"); // btnABA

            // A validation should arise, and then we correct it:
            if($langEnv == 'en') {
                $this->assertTextPresent(Yii::t('mainApp', 'Please enter your CVC number'));
            }
            elseif($langEnv == 'ru') {
                $this->assertTextPresent(Yii::t('mainApp', 'Необходимо ввести код CVC'));
            }
            elseif($langEnv == 'fr') {
                $this->assertTextPresent(Yii::t('mainApp', 'Veuillez introduire le code CVC'));
            }
            $this->assertTrue($this->isElementPresent("id=btnABAadyen"));

            // Fix and submit form again
            $this->type("id=PaymentForm_CVC", $cardCvc);
            $this->clickAndWait("id=btnABAadyen");

            // It should fail if pay_gateway is connected to Adyen:
            $this->assertFalse($this->isElementPresent("id=dIdErrorSummary"));
            $this->assertTextNotPresent(substr(Yii::t('mainApp', 'gateway_failure_key'), 1, 80));

            $this->goAndAssertPremiumAfterPayment($langEnv);


            /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
            $moUser = new AbaUser();
            $moUser->getUserByEmail($email);

            /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
            $this->assertUserInvoices($moUser, $email, false, $countryId);

            /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
            $this->paymentSaveInvoiceData($birthDate, $city, $telephone, $countryId, $street, $streetMore, $province, $cp, $ident);

            /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
            $this->downloadAllInvioces($moUser);

            /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
            $this->assertUserInvoices($moUser, $email, true, $countryId);
            /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/


        } else {
            $this->assertFalse(true);
        }
        return true;
    }


    /** Tests a full payment with Adyen, buying a one month product in EUROS
     *  for a FRANCIA user. Includes One Month promocode. It will check next payment is not
     *  of one euro.
     *
     * @test
     */
    public function testPaymentWith1MonthPromocode()
    {
        $moPromocode = new ProductPromo('MONTHTESTPROMO');
        $this->assertInstanceOf('ProductPromo',$moPromocode);

        // Enters with promocode already into the URL:
        $this->gotoPayment('MONTHTESTPROMO', NULL, 'unit.test.free.user.francia@yopmail.com');
        $this->assertProductSelectionPageWithFinalPrice( COUNTRY_SPAIN );

        // Once in Payment selection product, select month product, with month promocode:
        $this->clickAndWait("id=BtnMonthContinue-1");

        // Fill Card details for Europe country:
        $this->type("id=PaymentForm_creditCardName", "Test Unit Free");
        $this->select('id=PaymentForm_creditCardMonth','index=06');
        $this->select('id=PaymentForm_creditCardYear','label=2016');
        $this->type("id=PaymentForm_creditCardNumber", "374251021090003");
        $this->type("id=PaymentForm_CVC","7373");

        // Submit the form:
        $this->clickAndWait("id=btnABAadyen");

        // Welcome to Premium page:----------------
        $this->goAndAssertPremiumAfterPayment('fr');

        // Back-End database assertions:
        $moUser = new AbaUser();
        $moUser->getUserByEmail('unit.test.free.user.francia@yopmail.com');
        $this->assertEquals( PREMIUM, $moUser->userType, 'User should be PREMIUM');

        $moPay = new Payment();
        $this->assertTrue($moPay->getLastSuccessPayment($moUser->getId())!==false);
        $this->assertEquals($moPay->paySuppExtId, PAY_SUPPLIER_ADYEN);
        $this->assertEquals( $moPay->amountPrice, 1.00, ' Amount price should be 1.00 but is '.$moPay->amountPrice);
        $moPay = null;

        $moPay = new Payment();
        $this->assertTrue($moPay->getLastPayPendingByUserId($moUser->getId())!==false);
        $this->assertEquals($moPay->paySuppExtId, PAY_SUPPLIER_ADYEN);
        $this->assertEquals( $moPay->amountPrice, 25.00, ' Amount price should be 25.00 but is '.$moPay->amountPrice);

        return true;
    }

    /** Test a full payment process, skipping validations.
     * And once is finished, it checks values of the user and its payments.
      */
    public function testPaymentWithPromoCodeAndPartner()
    {
        $idPartner =        6003;
        $userEmailTest =    'unit.test.free.user.colombia@yopmail.com';
        $moPromocode =      new ProductPromo('Conv1E10');
        $this->assertInstanceOf('ProductPromo',$moPromocode);

        // Enters with promocode already into the URL:
        $this->gotoPayment('', $idPartner, $userEmailTest);
//        $this->assertProductSelectionPage( COUNTRY_SPAIN );
        $this->assertProductSelectionPage( 74 );

        $this->click('id=sTypePromocode');
        $this->type('id=promocodeInputText', 'Conv1E10' );
        $this->clickAndWait('id=SubmitPromocode2');

        $this->assertElementContainsText( 'idTable-6', 'US $ 22 .50 a month');
        $this->assertElementContainsText( 'idTable-12', 'US $ 17 .92 a month');
        $this->assertTextPresent('10% OFF');

        // Once in Payment selection product, select month product, with month promocode:
        $this->clickAndWait("id=BtnMonthContinue-6");

        // Fill Card details for Europe country:
        $this->type("id=PaymentForm_creditCardName", "Test Unit Free");
        $this->select('id=PaymentForm_creditCardMonth','index=06');
        $this->select('id=PaymentForm_creditCardYear','label=2016');
        $this->type("id=PaymentForm_creditCardNumber", "4400 0000 0000 0008"); // Debit	US
        $this->type("id=PaymentForm_CVC", "737");

        // Submit the form:
        $this->clickAndWait("id=btnABAadyen");

        // Welcome to Premium page:----------------
        $this->goAndAssertPremiumAfterPayment('en');

        // Once the payment is done, we check multiple values directly from database:
        $moUser = new AbaUser();
        $this->assertTrue( $moUser->getUserByEmail($userEmailTest) );
        $this->assertEquals($idPartner, $moUser->idPartnerFirstPay);
        $this->assertEquals($idPartner, $moUser->idPartnerCurrent);
        $this->assertEquals( PREMIUM, $moUser->userType, 'User should be PREMIUM');

        $moPayment = new Payment();
        $this->assertInstanceOf('Payment', $moPayment->getLastSuccessPaymentNoFree( $moUser->id ) );
        $this->assertEquals( HeDate::todaySQL(false), substr($moPayment->dateEndTransaction, 0, 10), ' The End Date of PAyment should be now.');
        $this->assertEquals($idPartner, $moPayment->idPartner, 'Partner in success payment does not match.');
        $this->assertEquals($idPartner, $moPayment->idPartner);
        $this->assertEquals($moPayment->id, $moPayment->idPayControlCheck);
        $this->assertEquals( $moPayment->amountPrice, 135, ' Amount price should be 135 but is ' . $moPayment->amountPrice);

        $moPay = new Payment();
        $this->assertTrue($moPay->getLastPayPendingByUserId($moUser->getId()) !== false);
        $this->assertEquals($moPay->paySuppExtId, PAY_SUPPLIER_ADYEN);
        $this->assertEquals( $moPay->amountPrice, 150, ' Amount price should be 150 but is ' . $moPay->amountPrice);

        return true;
    }

}
