<?php

/**
 *
 */
class PaymentsCaixaControllerTest extends PaymentsControllerTestBase
{
    /**
     * Creates all test users and any other test data used specific from this test.
     */
    public function setUp()
    {
        // FREE FROM BRAZIL
        $this->createUsersIntoDb('unit.test.free.user.spain@yopmail.com', FREE, 199, "UnitFree",
                    "Testing Spain", false, 'es');

        parent::setUp();
    }

    /**
     * Destroy all test users and any other test data used specific from this test.
     */
    public function tearDown()
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail('unit.test.free.user.spain@yopmail.com', "", false);
        $moUser->deleteUserThroughResetCall();

        parent::tearDown();
    }

    /** Checks that a user PREMIUM if tries to access the payment page
     * it is redirected to HOME PAGE.
     * @return bool
     */
    public function testNotPaymentSelectProductAndPrice()
    {
        $this->loginUserByEmail('unit.test.premium.user@yopmail.com', true);

        $this->assertElementNotPresent("id=idDivGoPremium");

        $this->open('payments/payment');

        $this->assertTrue( $this->assertHomePage(PREMIUM) );

        return true;
    }

    /**
     * Tests The payment method page and it process a payment.
     */
    public function testPaymentMethodTPVAndBuy()
    {
        if ($this->gotoPaymentSelectProductAndPrice('unit.test.free.user.spain@yopmail.com', 12)) {
//            $this->select('PaymentForm[creditCardType]', 'VISA');
            $this->type("id=PaymentForm_creditCardName", "Test Unit Free");
            $this->type("id=PaymentForm_creditCardNumber", "5540500001000004");
            $this->select('id=PaymentForm_creditCardMonth', 'index=12');
            $this->select('id=PaymentForm_creditCardYear', 'label=2020');
            $this->clickAndWait("id=btnABA");


            // A validation should arise, and then we correct it:
            $this->assertTextPresent(Yii::t('mainApp', 'Debes introducir el código CVC'));
            $this->assertTrue($this->isElementPresent("id=btnABA"));

            // Fix and submit form again
            $this->type("id=PaymentForm_CVC", "285");
            $this->clickAndWait("id=btnABA");

            // It should fail if pay_gateway is connected to LaCaixa:
            $this->assertTrue($this->isElementPresent("id=dIdErrorSummary"));
            $this->assertTextPresent(substr(Yii::t('mainApp', 'gateway_failure_key'), 1, 80));

            // After La Caixa rejected a card, we set a special one for testing communications with
            // CiberPac:
            $this->type("id=PaymentForm_creditCardNumber", "4548 8120 4940 0004");
            $this->clickAndWait("id=btnABA");

            $this->goAndAssertPremiumAfterPayment('es');
        } else {
            $this->assertFalse(true);
        }

        return true;

    }

    /** Tests a full payment with La Caixa, buying a one month product in DOLLARS
     *  for a BRAZILIAN user. Includes One Month promocode. It will check next payment is not
     *  of one euro.
     * @test
     */
    public function testPaymentWith1MonthPromocode()
    {
        $moPromocode = new ProductPromo('MONTHTESTPROMO');
        $this->assertInstanceOf('ProductPromo',$moPromocode);

        // Enters with promocode already into the URL:
        $this->gotoPayment('MONTHTESTPROMO', NULL, 'unit.test.free.user.spain@yopmail.com');
        $this->assertProductSelectionPageWithFinalPrice( COUNTRY_SPAIN );

        // Once in Payment selection product, select month product, with month promocode:
        $this->clickAndWait("id=BtnMonthContinue-1");

        // Fill Card details for Europe country:
//        $this->select('id=PaymentForm_creditCardType', 'value=' .KIND_CC_VISA);
        $this->type("id=PaymentForm_creditCardName", "Test Unit Free");
        $this->select('id=PaymentForm_creditCardMonth','index=12');
        $this->select('id=PaymentForm_creditCardYear','label=2020');
        $this->type("id=PaymentForm_creditCardNumber", "4548812049400004");
        $this->type("id=PaymentForm_CVC","285");

        // Submit the form:
        $this->clickAndWait("id=btnABA");

        // Welcome to Premium page:----------------
        $this->goAndAssertPremiumAfterPayment('es');

        // Back-End database assertions:
        $moUser = new AbaUser();
        $moUser->getUserByEmail('unit.test.free.user.spain@yopmail.com');
        $this->assertEquals( PREMIUM, $moUser->userType, 'User should be PREMIUM');
        $moPay = new Payment();
        $this->assertTrue($moPay->getLastSuccessPayment($moUser->getId())!==false);
        $this->assertEquals($moPay->paySuppExtId, PAY_SUPPLIER_CAIXA);
        $this->assertEquals( $moPay->amountPrice, 1.00, ' Amount price should be 1.00 but is '.$moPay->amountPrice);
        $moPay = null;
        $moPay = new Payment();
        $this->assertTrue($moPay->getLastPayPendingByUserId($moUser->getId())!==false);
        $this->assertEquals($moPay->paySuppExtId, PAY_SUPPLIER_CAIXA);
        $this->assertEquals( $moPay->amountPrice, 14.99, ' Amount price should be 14.99 but is '.$moPay->amountPrice);

        return true;
    }

    /** Test a full payment process, skipping validations.
     * And once is finished, it checks values of the user and its payments.
      */
    public function testPaymentWithPromoCodeAndPartner()
    {
        $idPartner = 6003;
        $userEmailTest = 'unit.test.free.user.spain@yopmail.com';
        $moPromocode = new ProductPromo('Conv1E10');
        $this->assertInstanceOf('ProductPromo',$moPromocode);

        // Enters with promocode already into the URL:
        $this->gotoPayment('', $idPartner, $userEmailTest);
        $this->assertProductSelectionPage( COUNTRY_SPAIN );

        $this->click('id=sTypePromocode');
        $this->type('id=promocodeInputText', 'Conv1E10' );
        $this->clickAndWait('id=SubmitPromocode2');

        $this->assertElementContainsText( 'idTable-6', '10 ,49 €');
        $this->assertElementContainsText( 'idTable-12', '7 ,49 € / mes');
        $this->assertTextPresent('10% DTO');

        // Once in Payment selection product, select month product, with month promocode:
        $this->clickAndWait("id=BtnMonthContinue-6");

        // Fill Card details for Europe country:
        $this->type("id=PaymentForm_creditCardName", "Test Unit Free");
//        $this->select('id=PaymentForm_creditCardType', 'value=' .KIND_CC_VISA);
        $this->select('id=PaymentForm_creditCardMonth','index=12');
        $this->select('id=PaymentForm_creditCardYear','label=2020');
        $this->type("id=PaymentForm_creditCardNumber", "4548812049400004");
        $this->type("id=PaymentForm_CVC","285");

        // Submit the form:
        $this->clickAndWait("id=btnABA");

        // Welcome to Premium page:----------------
        $this->goAndAssertPremiumAfterPayment('es');

        // Once the payment is done, we check multiple values directly from database:
        $moUser = new AbaUser();
        $this->assertTrue( $moUser->getUserByEmail($userEmailTest) );
        $this->assertEquals($idPartner, $moUser->idPartnerFirstPay);
        $this->assertEquals($idPartner, $moUser->idPartnerCurrent);
        $this->assertEquals( PREMIUM, $moUser->userType, 'User should be PREMIUM');
        $moPayment = new Payment();
        $this->assertInstanceOf('Payment', $moPayment->getLastSuccessPaymentNoFree( $moUser->id ) );
        $this->assertEquals( HeDate::todaySQL(false), substr($moPayment->dateEndTransaction,0,10),
                            ' The End Date of PAyment should be now.');
        $this->assertEquals($idPartner, $moPayment->idPartner, 'Partner in success payment does not match.');
        $this->assertEquals($idPartner, $moPayment->idPartner);
        $this->assertEquals($moPayment->id, $moPayment->idPayControlCheck);
        $moPay = new Payment();
        $this->assertTrue($moPay->getLastPayPendingByUserId($moUser->getId())!==false);
        $this->assertEquals($moPay->paySuppExtId, PAY_SUPPLIER_CAIXA);
        $this->assertEquals( $moPay->amountPrice, 69.99, ' Amount price should be 69.99 but is '.$moPay->amountPrice);

        return true;
    }

    /**
     * Tests selection of product and checks price on the Payment method
     */
    protected function gotoPaymentSelectProductAndPrice($email, $months)
    {
        $this->loginUserByEmail($email);
        // If Popup User Objectives questionnaire is displayed, then we close the dialog box
        if( $this->isElementPresent("id=CampusModals") )
        {
            $this->click("class=ui-dialog-titlebar-close ui-corner-all");
        }
        // We click on Go To Premium banner
//        $this->clickAndWait("id=divBtnGoPremium");
        $this->clickAndWait("id=idDivGoPremium");

        $moUser = new AbaUser();
        $moUser->getUserByEmail($email, "", false);

        // Assert Selection product selection product page is open:
        $this->assertTrue( $this->assertProductSelectionPage($moUser->countryId) );

        // We click on Continue to 6 months subscription:
        $this->clickAndWait('id=BtnMonthContinue-'.$months);
        $this->assertPaymentMethodPage( $moUser->langEnv, $months);

        return true;
    }

}
