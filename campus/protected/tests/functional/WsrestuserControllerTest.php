<?php
/**
 * To test web services REST that are called from mobile devices.
 * User: Quino
 */
class WsrestuserControllerTest extends WsrestControllerTest
{
    protected $urlWs;
    protected static $emailUserTest = "unit.test.mobile.free@yopmail.com";

    /**
     * Override to set URL public ws
     */
    public function setUp()
    {
        $this->createUsersIntoDb(WsrestuserControllerTest::$emailUserTest, FREE, 30, "UnitFree",
            "Testing Brazil", false, 'pt');

//        $wsAppStoreData = $this->wsAppStoreData();
//        foreach($wsAppStoreData as $iKey => $user) {
//            $this->createUsersIntoDb($user[0], FREE, $user[6], $user[4] . " - UnitFree", $user[5] . " - Testing", true);
//        }

        parent::setUp();

        $this->urlWs = "http://" . Yii::app()->config->get("SELF_DOMAIN") . "/wsrestuser";
    }

    public function tearDown()
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail(WsrestuserControllerTest::$emailUserTest, "", false);
        $moUser->deleteUserThroughResetCall();

        $wsAppStoreData = $this->wsAppStoreData();
        foreach($wsAppStoreData as $iKey => $user) {
            $moUser = new AbaUser();
            if($moUser->getUserByEmail($user[0], "", false)) {
                $moUser->deleteUserThroughResetCall();
            }
        }

        parent::tearDown();
    }


    /**
     * @test
     *
     * @return bool
     */
    public function testUpdateUserpassword()
    {
        $token = $this->doLoginWsRestUserPassword();

        $aFieldsToBeSent["object"]= 'userpassword';
        $aFieldsToBeSent["password"]= "abaenglish";
        $varsPost = json_encode($aFieldsToBeSent);

        $response = HeComm::sendHTTPPostCurl($this->urlWs."/update",
                                array('PHP_AUTH_TOKEN: '.$token.''), $aFieldsToBeSent);

        $aResponse = json_decode($response);

        $this->assertTrue( property_exists($aResponse,"token") );

        return true;
    }


    /**
     * @test WsrestuserController::updateFollowupVideo
     *
     * @param integer $videoId
     *
     * @return array
     */
    public function testUpdateFollowupVideo( $videoId=130 )
    {
        $token = $this->doLoginWsRestUserPassword();

        $moUser = new AbaUser();
        $moUser->getUserByEmail(self::$emailUserTest);
        $moFollowUp = new AbaFollowup();
        $moFollowUp->getFollowupByIds( $moUser->id, $videoId);
        $unitProgressBefore = $moFollowUp->getUnitProgress();

        $this->assertInternalType("string", $token);
        // Check that this video is 0 progress completed:-------
        $responseListVideoClasses = $this->getListVideoclasses( $token );
        $this->assertTrue( property_exists($responseListVideoClasses, "aUnits"),
            'Should return list of 144 videos-classes');
        $objVideoclass = $responseListVideoClasses->aUnits->{''.$videoId};
        $this->assertEquals( 0, $objVideoclass->VideoClassProgress, 'Should be 0 progress.');

        // Update the progress of this video to completed:-------
        $aFieldsToBeSent["object"]= 'Followupvideo';
        $aFieldsToBeSent["followUpVideo"]= 100;
        $aFieldsToBeSent["id"]= $videoId;
        $varsPost = json_encode($aFieldsToBeSent);
        $response = HeComm::sendHTTPPostCurl($this->urlWs."/update",
            array('PHP_AUTH_TOKEN: '.$token.''), $aFieldsToBeSent);
        $aResponse = json_decode($response);
        $this->assertTrue( property_exists($aResponse,"status") );
        $this->assertEquals( 'OK', $aResponse->status );


        // Check that this video is 100 progress completed after updating:-------
        $responseListVideoClasses = $this->getListVideoclasses( $token );
        $this->assertTrue( property_exists($responseListVideoClasses, "aUnits"),
            'Should return list of 144 videos-classes');
        $objVideoclass = $responseListVideoClasses->aUnits->{''.$videoId};
        $this->assertEquals( 100, $objVideoclass->VideoClassProgress);
        $this->assertEquals( 6,  $objVideoclass->level);
        $this->assertEquals( ($videoId-HeUnits::getStartUnitByLevel(6)),  $objVideoclass->lockInfoStatus,
                                                "Days left to be unlocked does not match.");
        $this->assertEquals( 144, count((array)($responseListVideoClasses->aUnits)),
                                            'It should return 144 units.');

        // Update the progress to 50% of this video to completed:-------
        $aFieldsToBeSent["object"]= 'Followupvideo';
        $aFieldsToBeSent["followUpVideo"]= 50;
        $aFieldsToBeSent["id"]= $videoId;
        $varsPost = json_encode($aFieldsToBeSent);
        $response = HeComm::sendHTTPPostCurl($this->urlWs."/update",
            array('PHP_AUTH_TOKEN: '.$token.''), $aFieldsToBeSent);
        $aResponse = json_decode($response);
        $this->assertTrue( property_exists($aResponse,"status") );
        $this->assertEquals( 'OK', $aResponse->status );

        // Check that this video is 50% progress completed after updating:-------
        $responseListVideoClasses = $this->getListVideoclasses( $token );
        $this->assertTrue( property_exists($responseListVideoClasses, "aUnits"),
            'Should return list of 144 videos-classes');
        $objVideoclass = $responseListVideoClasses->aUnits->{''.$videoId};
        $this->assertEquals( 100, $objVideoclass->VideoClassProgress);
        $this->assertEquals( 6,  $objVideoclass->level);

        $moFollowUp->getFollowupByIds( $moUser->id, $videoId);
        $unitProgressAfter = $moFollowUp->getUnitProgress();
        $this->assertTrue(HeDate::validDateSQL($moFollowUp->lastchange, true));
        $this->assertGreaterThan( $unitProgressBefore, $unitProgressAfter);

        return array($token, $videoId);

    }

    /**
     * @return bool
     */
    public  function testListVideoclassesPremium()
    {
        $aFieldsToBeSent["signature"]= 1446464123;
        $aFieldsToBeSent["deviceId"]= "CCC464123GGR4353";
        $aFieldsToBeSent["sysOper"]= "IPHONE 5.0";
        $aFieldsToBeSent["deviceName"]= "IPHONE 5.0";
        $aFieldsToBeSent["email"]= "unit.test.premium.user@yopmail.com";
        $aFieldsToBeSent["password"]= "123456789";
        json_encode($aFieldsToBeSent);
        $response = HeComm::sendHTTPPostCurl("http://" . Yii::app()->config->get("SELF_DOMAIN") . "/wsrestregister".
                                                                                    "/login", null, $aFieldsToBeSent);
        $aResponse = json_decode($response);
        $responseListVideoClasses = $this->getListVideoclasses( $aResponse->token );

        $this->assertTrue( property_exists($responseListVideoClasses, "aUnits"),
                                                            'Should return list of 144 videos-classes');
        $this->assertEquals( HeUnits::getMaxUnits(), count((array)($responseListVideoClasses->aUnits)),
                                                            'It should return only 24 units, above 6 level.');
        for( $u=1; $u<HeUnits::getMaxUnits(); $u++){
            $this->assertTrue($responseListVideoClasses->aUnits->{''.$u}->userAccess,
                                                            ' should have access to this unit and all others');
            $this->assertEquals( UNLOCKED_VC, $responseListVideoClasses->aUnits->{''.$u}->lockInfoStatus,
                                                            'Lock should be UNLOCKED.');
        }

        return true;

    }

    /** Tests list videoclasses for FREE users
     *
     * @return bool
     */
    public  function testListVideoclassesFree()
    {
        $aFieldsToBeSent["signature"]= 1446464123;
        $aFieldsToBeSent["deviceId"]= "156156-44646F-4645646-JJH1212";
        $aFieldsToBeSent["sysOper"]= "IPHONE 8.0";
        $aFieldsToBeSent["deviceName"]= "IPHONE 8.0";
        $aFieldsToBeSent["email"]= self::$emailUserTest;
        $aFieldsToBeSent["password"]= "123456789";
        $response = HeComm::sendHTTPPostCurl("http://" . Yii::app()->config->get("SELF_DOMAIN") . "/wsrestregister".
            "/login", null, $aFieldsToBeSent);
        $aResponse = json_decode($response);

        $this->deleteAllVisibleCookies();

        $moUser = new AbaUser();
        $this->assertTrue($moUser->getUserByEmail(self::$emailUserTest));
        $curLevel = $moUser->currentLevel;

        $responseListVideoClasses = $this->getListVideoclasses( $aResponse->token );

        $this->assertTrue( property_exists($responseListVideoClasses, "aUnits"),
            'FREE user: Should return list of N videos-classes');
        $this->assertEquals( HeUnits::getMaxUnits(), count((array)($responseListVideoClasses->aUnits)),
            'FREE user: It should return only 24 units, above 6 level.');
        for( $u=1; $u<HeUnits::getMaxUnits(); $u++){
            if( HeUnits::isFirstIdLevel($u)){
                $this->assertEquals( UNLOCKED_VC, intval($responseListVideoClasses->aUnits->{''.$u}->lockInfoStatus),
                    '1st unit should be UNLOCKED.');
            } else{
                $this->assertFalse($responseListVideoClasses->aUnits->{''.$u}->userAccess,
                    ' should have access to this unit and all others');
                $moUnits = new UnitsCourses();
                if ($curLevel<=$moUnits->getLevelByUnitId($u)){
                    $this->assertEquals( abs(HeUnits::getStartUnitByLevel($curLevel)-$u), abs( $responseListVideoClasses->aUnits->{''.$u}->lockInfoStatus ),
                        'Unit '.$u.' should be free access on '.$u.' days.');
                }
            }

        }

        return true;

    }

    /**
     * @param $token
     *
     * @return array|mixed
     */
    protected function getListVideoclasses( $token )
    {

        $response = HeComm::sendHTTPPostCurl($this->urlWs."/list/object/videoclasses",
            array('PHP_AUTH_TOKEN: '.$token.''));

        $aResponse = json_decode($response);

        return $aResponse;
    }


    /**
     * @test
     *
     * @dataProvider wsAppStoreData
     *
     * @return bool
     */
    public function testFreetopremium($tEmail, $periodId, $currency, $price, $name, $surnames, $country, $device, $countryId, $userId, $purchaseReceipt)
    {
        $this->createUsersIntoDb($tEmail, FREE, $country, $name . " - UnitFree", $surnames . " - Testing", true);

//        $token = $this->doLoginWsRestUserPassword($tEmail, $device);
        $token = $this->doLoginWsRestUserPassword();
//        $token = "a96624d4e40940a3d3475caefa710fd3";

        $aFieldsToBeSent["object"] =            'freetopremium';
        $aFieldsToBeSent["periodid"] =          $periodId;
        $aFieldsToBeSent["currency"] =          $currency;
        $aFieldsToBeSent["price"] =             $price;
        $aFieldsToBeSent["countryid"] =         $countryId;
        $aFieldsToBeSent["userid"] =            $userId;
        $aFieldsToBeSent["purchasereceipt"] =   $purchaseReceipt;

        $varsPost =     json_encode($aFieldsToBeSent);
        $response =     HeComm::sendHTTPPostCurl($this->urlWs . "/update", array('PHP_AUTH_TOKEN: ' . $token . ''), $aFieldsToBeSent);
        $aResponse =    json_decode($response);

        $this->assertTrue( property_exists($aResponse, "token") );

        return true;
    }

    /**
     * @return array
     */
    public function wsAppStoreData()
    {
        return array(
            array("unit.test.free.usercommon1@yopmail.com", 1,   'EUR',  '190.98', 'Alek 1',  'Sander',  30, 'FG441651561651-1',  30, 1495748, '{"json":"test"}'),
            array("unit.test.free.usercommon2@yopmail.com", 6,   'BRL', '1290.87', 'Alek 6',  'Sander', 105, 'FG441651561651-2', 105, 1495748, '{"json":"test"}'),
            array("unit.test.free.usercommon3@yopmail.com", 12,  'USD', '25.96',   'Alek 12', 'Sander', 177, 'FG441651561651-3', 177, 1495748, '{"json":"test"}'),
            array("unit.test.free.usercommon4@yopmail.com", 24,  'EUR', '50.95',   'Alek 24', 'Sander', 199, 'FG441651561651-4', 199, 1495748, '{"json":"test"}'),
            array("unit.test.free.usercommon5@yopmail.com", 12,  '',    '25.96',   'Alek 12', 'Sander', 177, 'FG441651561651-5', 177, 1495748, '{"json":"test"}'),
            array("unit.test.free.usercommon6@yopmail.com", 24,  'EUR', '',        'Alek 24', 'Sander', 199, 'FG441651561651-6', 199, 1495748, '{"json":"test"}'),
            array("unit.test.free.usercommon7@yopmail.com", 12,  '',    '',        'Alek 12', 'Sander', 177, 'FG441651561651-7', 177, 1495748, '{"json":"test"}'),
        );
    }

}
