<?php

/**
 *
 */
class PaymentsAdyenHppControllerTest extends PaymentsControllerTestBase
{
    /**
     * Creates all test users and any other test data used specific from this test.
     */
    public function setUp()
    {
        $this->createUsersIntoDb('unit.test.free.user.germany@yopmail.com',     FREE, 80,   "UnitFree", "Testing Germany",  false, 'de');
//        $this->createUsersIntoDb('unit.test.free.user.colombia@yopmail.com',    FREE, 47,   "UnitFree", "Testing Colombia", false, 'es');

        parent::setUpNoCard();
    }

    /**
     * Destroy all test users and any other test data used specific from this test.
     */
    public function tearDown()
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail('unit.test.free.user.germany@yopmail.com', "", false);
        $moUser->deleteUserThroughResetCall();

//        $moUser = new AbaUser();
//        $moUser->getUserByEmail('unit.test.free.user.colombia@yopmail.com', "", false);
//        $moUser->deleteUserThroughResetCall();

        parent::tearDownNoCard();
    }

    /**
     *  *** Countries 47, 73 & 226  paySupplier = 10 (Adyen) ***
     *
     * @return array
     */
    public function dataUsers()
    {
        return array(
//            array( 'unit.test.free.user.colombia@yopmail.com',  47,     'Test Unit Free Colombia',   'English Colombia',   'es', 12,   '01-09-1977', 'Сергиев Посад',  '009988776655', '2я станционная',   '4б, второй блок с права, 2я лестница', 'Сергиев Посад',    '012343210',    'Ы2345432Ы'),
            array( 'unit.test.free.user.germany@yopmail.com',   80,     'Test Unit Free Germany',    'Testing Germany',    'de', 1,    '01-09-1978', 'Leipzig',        '008877665544', 'Gran Via',         '66',                                   'Leipzig',          '12345',        'X1111111W'),
        );
    }

    public function dataCountries()
    {
        return array(
//            47 => array(47, 'CO'),
            80 => array(80, 'DE'),
        );
    }

    public function dataIps()
    {
        return array(
//            array('46.136.173.1'),  // Brazil,
            array('37.72.0.15'),    // Spain, Valencia
        );
    }

    public function dataCardTypes()
    {
        return array(
//            array(KIND_CC_VISA,             '4988 4388 4388 4305',  '06', '2016', '737'),   // VISA Classic	ES
//            array(KIND_CC_AMERICAN_EXPRESS, '374251021090003',      '06', '2016', '7377'),  // AMEX
//            array(KIND_CC_DISCOVER,         '6011 6011 6011 6611',  '06', '2016', '737'),   // DISCOVER US
//            array(KIND_CC_MASTERCARD,       '5577 0000 5577 0004',  '06', '2016', '737'),   // MasterCard PL
//            array(KIND_CC_MASTERCARD,       '3600 6666 3333 44',    '06', '2016', '737'),   // DINERS

//            array(100001,       '6243 0300 0000 0001',  '06',       '2016',         '737',  ''),        // China Union Pay - Express Pay (cup)
//            array(100002,       '1234567890',           '44448888', 'jo mateixxx',  '10',   '4000'),    // GiroPay
            array(100002,       '1234567890',           '44448888', '1111111111',  '2222222222',   '3333333333'),    // elv
        );
    }

    /** Combines all of three previous data arrays together.
     *
     * @return array|null
     */
    public function dataCombinations()
    {
        $retArrayCombinations = array();
        $aCombinations = $this->unitTestDataCartesian($this->dataCardTypes(), $this->dataUsers());
        foreach ($aCombinations[0] as $comb) {
            $retArrayCombinations[] = $comb;
        }
        $aCombinations = $this->unitTestDataCartesian($this->dataIps(), $retArrayCombinations);
        $retArrayCombinations = NULL;
        foreach ($aCombinations[0] as $comb) {
            $retArrayCombinations[] = $comb;
        }
        return $retArrayCombinations;
    }

    /**
     * Tests selection of product and checks price on the Payment method
     */
    protected function gotoPaymentSelectProductAndPrice($email, $months)
    {
        $this->loginUserByEmail($email);
        // If Popup User Objectives questionnaire is displayed, then we close the dialog box
        if( $this->isElementPresent("id=CampusModals") ) {
            $this->click("class=ui-dialog-titlebar-close ui-corner-all");
        }
        // We click on Go To Premium banner
        $this->clickAndWait("id=idDivGoPremium");

        $moUser = new AbaUser();
        $moUser->getUserByEmail($email, "", false);

        // Assert Selection product selection product page is open:
        $this->assertTrue( $this->assertProductSelectionPage($moUser->countryId) );

        // We click on Continue to 6 months subscription:
        $this->clickAndWait('id=BtnMonthContinue-'.$months);
        $this->assertPaymentMethodPage( $moUser->langEnv, $months);

        return true;
    }

    /**
     * Tests The payment method page and it process a payment.
     * @dataProvider dataCombinations
     */
    public function testPaymentMethodCardWithInvoices(
        $email, $countryId, $name, $lang, $langEnv, $monthId, $birthDate, $city, $telephone, $street, $streetMore, $province, $cp, $ident,
        $creditType, $cardNumber, $cardMonth, $cardYear, $cardCvc, $custom1,
        $ipLocation
    ) {
        if ($this->gotoPaymentSelectProductAndPrice($email, $monthId)) {

            $this->click("id=PaymentForm_idPayMethod_2"); // btnABA

            $this->waitForElementPresent("class=alternativePayments", 10000);

            $this->clickAndWait("id=iconAdien_elv");

            $this->waitForElementPresent("id=mainSubmit", 10000);

            $this->type("id=bankAccountNumber", $cardNumber);
            $this->type("id=bankLocationId",    $cardMonth);
            $this->type("id=bankName",          $cardYear);
            $this->type("id=bankLocation",      $cardCvc);
            $this->type("id=accountHolderName", $custom1);

            $this->waitForElementPresent("id=mainSubmit", 10000);

            $this->clickAndWait("id=mainSubmit");

//            $this->waitForElementPresent("id=idDivBtnStartPremium", 10000);


            ini_set("soap.wsdl_cache_enabled",  0);
//ini_set("default_socket_timeout",   600);


//
//            // A validation should arise, and then we correct it:
//            if($langEnv == 'en') {
//                $this->assertTextPresent(Yii::t('mainApp', 'Please enter your CVC number'));
//            }
//            elseif($langEnv == 'ru') {
//                $this->assertTextPresent(Yii::t('mainApp', 'Необходимо ввести код CVC'));
//            }
//            elseif($langEnv == 'fr') {
//                $this->assertTextPresent(Yii::t('mainApp', 'Veuillez introduire le code CVC'));
//            }
//            $this->assertTrue($this->isElementPresent("id=btnABAadyen"));
//
//            // Fix and submit form again
//            $this->type("id=PaymentForm_CVC", $cardCvc);
//            $this->clickAndWait("id=btnABAadyen");
//
//            // It should fail if pay_gateway is connected to Adyen:
//            $this->assertFalse($this->isElementPresent("id=dIdErrorSummary"));
//            $this->assertTextNotPresent(substr(Yii::t('mainApp', 'gateway_failure_key'), 1, 80));
//
//            $this->goAndAssertPremiumAfterPayment($langEnv);
//
//
//            /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
//            $moUser = new AbaUser();
//            $moUser->getUserByEmail($email);
//
//            /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
//            $this->assertUserInvoices($moUser, $email, false, $countryId);
//
//            /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
//            $this->paymentSaveInvoiceData($birthDate, $city, $telephone, $countryId, $street, $streetMore, $province, $cp, $ident);
//
//            /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
//            $this->downloadAllInvioces($moUser);
//
//            /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
//            $this->assertUserInvoices($moUser, $email, true, $countryId);
//            /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/


        } else {
            $this->assertFalse(true);
        }
        return true;
    }

//
//    /** Tests a full payment with Adyen, buying a one month product in EUROS
//     *  for a FRANCIA user. Includes One Month promocode. It will check next payment is not
//     *  of one euro.
//     *
//     * @test
//     */
//    public function testPaymentWith1MonthPromocode()
//    {
//        $moPromocode = new ProductPromo('MONTHTESTPROMO');
//        $this->assertInstanceOf('ProductPromo',$moPromocode);
//
//        // Enters with promocode already into the URL:
//        $this->gotoPayment('MONTHTESTPROMO', NULL, 'unit.test.free.user.francia@yopmail.com');
//        $this->assertProductSelectionPageWithFinalPrice( COUNTRY_SPAIN );
//
//        // Once in Payment selection product, select month product, with month promocode:
//        $this->clickAndWait("id=BtnMonthContinue-1");
//
//        // Fill Card details for Europe country:
//        $this->type("id=PaymentForm_creditCardName", "Test Unit Free");
//        $this->select('id=PaymentForm_creditCardMonth','index=06');
//        $this->select('id=PaymentForm_creditCardYear','label=2016');
//        $this->type("id=PaymentForm_creditCardNumber", "374251021090003");
//        $this->type("id=PaymentForm_CVC","7373");
//
//        // Submit the form:
//        $this->clickAndWait("id=btnABAadyen");
//
//        // Welcome to Premium page:----------------
//        $this->goAndAssertPremiumAfterPayment('fr');
//
//        // Back-End database assertions:
//        $moUser = new AbaUser();
//        $moUser->getUserByEmail('unit.test.free.user.francia@yopmail.com');
//        $this->assertEquals( PREMIUM, $moUser->userType, 'User should be PREMIUM');
//
//        $moPay = new Payment();
//        $this->assertTrue($moPay->getLastSuccessPayment($moUser->getId())!==false);
//        $this->assertEquals($moPay->paySuppExtId, PAY_SUPPLIER_ADYEN_HPP);
//        $this->assertEquals( $moPay->amountPrice, 1.00, ' Amount price should be 1.00 but is '.$moPay->amountPrice);
//        $moPay = null;
//
//        $moPay = new Payment();
//        $this->assertTrue($moPay->getLastPayPendingByUserId($moUser->getId())!==false);
//        $this->assertEquals($moPay->paySuppExtId, PAY_SUPPLIER_ADYEN_HPP);
//        $this->assertEquals( $moPay->amountPrice, 25.00, ' Amount price should be 25.00 but is '.$moPay->amountPrice);
//
//        return true;
//    }
//
//    /** Test a full payment process, skipping validations.
//     * And once is finished, it checks values of the user and its payments.
//      */
//    public function testPaymentWithPromoCodeAndPartner()
//    {
//        $idPartner =        6003;
//        $userEmailTest =    'unit.test.free.user.colombia@yopmail.com';
//        $moPromocode =      new ProductPromo('Conv1E10');
//        $this->assertInstanceOf('ProductPromo',$moPromocode);
//
//        // Enters with promocode already into the URL:
//        $this->gotoPayment('', $idPartner, $userEmailTest);
////        $this->assertProductSelectionPage( COUNTRY_SPAIN );
//        $this->assertProductSelectionPage( 74 );
//
//        $this->click('id=sTypePromocode');
//        $this->type('id=promocodeInputText', 'Conv1E10' );
//        $this->clickAndWait('id=SubmitPromocode2');
//
//        $this->assertElementContainsText( 'idTable-6', 'US $ 22 .50 a month');
//        $this->assertElementContainsText( 'idTable-12', 'US $ 17 .92 a month');
//        $this->assertTextPresent('10% OFF');
//
//        // Once in Payment selection product, select month product, with month promocode:
//        $this->clickAndWait("id=BtnMonthContinue-6");
//
//        // Fill Card details for Europe country:
//        $this->type("id=PaymentForm_creditCardName", "Test Unit Free");
//        $this->select('id=PaymentForm_creditCardMonth','index=06');
//        $this->select('id=PaymentForm_creditCardYear','label=2016');
//        $this->type("id=PaymentForm_creditCardNumber", "4400 0000 0000 0008"); // Debit	US
//        $this->type("id=PaymentForm_CVC", "737");
//
//        // Submit the form:
//        $this->clickAndWait("id=btnABAadyen");
//
//        // Welcome to Premium page:----------------
//        $this->goAndAssertPremiumAfterPayment('en');
//
//        // Once the payment is done, we check multiple values directly from database:
//        $moUser = new AbaUser();
//        $this->assertTrue( $moUser->getUserByEmail($userEmailTest) );
//        $this->assertEquals($idPartner, $moUser->idPartnerFirstPay);
//        $this->assertEquals($idPartner, $moUser->idPartnerCurrent);
//        $this->assertEquals( PREMIUM, $moUser->userType, 'User should be PREMIUM');
//
//        $moPayment = new Payment();
//        $this->assertInstanceOf('Payment', $moPayment->getLastSuccessPaymentNoFree( $moUser->id ) );
//        $this->assertEquals( HeDate::todaySQL(false), substr($moPayment->dateEndTransaction,0,10), ' The End Date of PAyment should be now.');
//        $this->assertEquals($idPartner, $moPayment->idPartner, 'Partner in success payment does not match.');
//        $this->assertEquals($idPartner, $moPayment->idPartner);
//        $this->assertEquals($moPayment->id, $moPayment->idPayControlCheck);
//        $this->assertEquals( $moPayment->amountPrice, 135, ' Amount price should be 135 but is ' . $moPayment->amountPrice);
//
//        $moPay = new Payment();
//        $this->assertTrue($moPay->getLastPayPendingByUserId($moUser->getId())!==false);
//        $this->assertEquals($moPay->paySuppExtId, PAY_SUPPLIER_ADYEN_HPP);
//        $this->assertEquals( $moPay->amountPrice, 150, ' Amount price should be 150 but is ' . $moPay->amountPrice);
//
//        return true;
//    }

}
