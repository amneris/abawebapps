<?php
/**
 * To test web services REST that are called from mobile devices.
 * User: Quino
 */
class WsRestControllerTest extends AbaWebTestCase
{
    protected $urlWs;
    protected static $emailUserTest = "unit.test.mobile.free@yopmail.com";

    public function setUp()
    {
        parent::setUp();
    }

    /** Returns Token to be able to test the rest of web services.
     *
     * @param string $email
     * @return mixed
     */
    protected function doLoginWsRestUserPassword($email="", $device="")
    {
        //$signature, $deviceId, $sysOper, $deviceName, $email, $password, $langEnv, $name, $surnames, $ip
        $aFieldsToBeSent["signature"] =     1446464123;
        $aFieldsToBeSent["sysOper"] =       "ANDROID Jelly Beans 4.1";
        $aFieldsToBeSent["deviceName"] =    "Samsung galaxy III";
        $aFieldsToBeSent["password"] =      "123456789";
        $aFieldsToBeSent["deviceId"] =      "FG441651561651";
        $aFieldsToBeSent["email"] =         self::$emailUserTest;

        if($device != '') {
            $aFieldsToBeSent["deviceId"] = $device;
        }
        if($email != '') {
            $aFieldsToBeSent["email"] = $email;
        }

        $varsPost = json_encode($aFieldsToBeSent);

        $response = HeComm::sendHTTPPostCurl("http://" . Yii::app()->config->get("SELF_DOMAIN") . "/wsrestregister/login", null, $aFieldsToBeSent);

        $aResponse = json_decode($response);

        $this->assertTrue( property_exists($aResponse,"userId") );
        $this->assertTrue( property_exists($aResponse,"userType") );
        $this->assertTrue( property_exists($aResponse,"userLevel") );
        $this->assertEquals( 6, $aResponse->userLevel );
        $this->assertTrue( property_exists($aResponse,"token") );

        $this->assertEquals(1,intval($aResponse->userType));

        if (property_exists($aResponse,"userId")) {
            $moUser = new AbaUser();
            if ($moUser->getUserById($aResponse->userId)) {
                $this->assertEquals( $moUser->email, self::$emailUserTest);
                $this->assertEquals($aResponse->userLevel, $moUser->currentLevel);
            }
        }

        return $aResponse->token;
    }
}
