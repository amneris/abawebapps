<?php

/**
 * Tests the Site Controller, home page, after login landing.
 */
class SiteControllerTest extends AbaWebTestCase
{
    public function setUp()
    {
        parent::setUp();
        // FREE
        $this->createUsersIntoDb("unit.test.free.user.honduras@yopmail.com", FREE,
            95, "UnitLead", "Testing");
    }

    public function tearDown()
    {
        parent::tearDown();

        $moUser = new AbaUser();
        if($moUser->getUserByEmail("unit.test.free.user.honduras@yopmail.com","",false)){
            $moUser->deleteUserThroughResetCall();
        }

        $moUser = new AbaUser();
        if($moUser->getUserByEmail("alek_dcfscto_sander@tfbnw.net","",false)){
            $moUser->deleteUserThroughResetCall();
        }
        $moUser = new AbaUser();
        if($moUser->getUserByEmail("dick_kzmgqig_martinez@tfbnw.net","",false)){
            $moUser->deleteUserThroughResetCall();
        }
        $moUser = new AbaUser();
        if($moUser->getUserByEmail("mykola_hmojixi_paganini@tfbnw.net","",false)){
            $moUser->deleteUserThroughResetCall();
        }
        $moUser = new AbaUser();
        if($moUser->getUserByEmail("sofxa_utqwoev_gubajdulina@tfbnw.net","",false)){
            $moUser->deleteUserThroughResetCall();
        }
    }

    /** Login OK
     *
     */
    public function testLoginOK()
	{
		$this->open('');
        $this->assertTextPresent(Yii::t("mainApp", 'Iniciar sesión', NULL, NULL, Yii::app()->params["langAssert"]));
        $this->assertTextPresent(Yii::t("mainApp", 'Contraseña', NULL, NULL, Yii::app()->params["langAssert"]));

        $this->type('name=LoginForm[email]','unit.test.free.user@yopmail.com');
        $this->type('name=LoginForm[password]','123456789');

        $this->click("id=btnStartSession");
        $this->waitForPageToLoad();

        $this->assertPopupSelectLevel();
        $this->selectLevelFromPopup();
        $this->assertChooseToInvitePopup();
        $this->rejectInvitePopup();

//        if( $this->isElementPresent("id=my_account") )
//        {
            $this->assertTextPresent(Yii::t("mainApp", 'Exit', NULL, NULL, Yii::app()->params["langAssert"]));
            $this->assertTextPresent(Yii::t("mainApp", 'My Account', NULL, NULL, Yii::app()->params["langAssert"]));
//        }
	}

    /** Login Failed for DELETED users
     *
     */
    public function testLoginFail()
    {
        $this->open('');
        $this->assertTextPresent(Yii::t("mainApp", 'Iniciar sesión', NULL, NULL, Yii::app()->params["langAssert"]));
        $this->assertTextPresent(Yii::t("mainApp", 'Contraseña', NULL, NULL, Yii::app()->params["langAssert"]));

        $this->type('name=LoginForm[email]','unit.test.deleted.user@yopmail.com');
        $this->type('name=LoginForm[password]','123456789');

        $this->click("id=btnStartSession");
        $this->waitForPageToLoad();

        $this->assertTextNotPresent('Exit');
        $this->assertTextNotPresent('My Account');

        $this->assertTextPresent(Yii::t("mainApp", 'Iniciar sesión', NULL, NULL, Yii::app()->params["langAssert"]));
    }

    /**
     * @depends testLoginOK
     */
    public function testIndexFree()
    {
        $this->open('site/index');

        $this->type('name=LoginForm[email]','unit.test.free.user@yopmail.com');
        $this->type('name=LoginForm[password]','123456789');
        $this->clickAndWait("//input[@value='".Yii::t("mainApp", 'Iniciar sesión', NULL, NULL, Yii::app()->params["langAssert"])."']");

        $this->assertTrue($this->assertHomePage(FREE));

    }

    /**
     * @depends testLoginOK
     */
    public function testIndexPremium()
    {
        $this->open('site/index');

        $this->type('name=LoginForm[email]','unit.test.premium.user@yopmail.com');
        $this->type('name=LoginForm[password]','123456789');
        $this->clickAndWait("//input[@value='".Yii::t("mainApp", 'Iniciar sesión', NULL, NULL, Yii::app()->params["langAssert"])."']");

        $this->assertTrue( $this->assertHomePage(PREMIUM) );

    }

    /**
     * Test that the recover password page works as expected.
      */
    public function testRecoverPassword()
    {
        $emailUserTest='unit.test.premium.user@yopmail.com';
        $moUser = new AbaUser();
        $moUser->getUserByEmail($emailUserTest,false);
        $pwdOld = $moUser->password;

        $this->open("/en/login");
        $this->type("id=LoginForm_email", $emailUserTest);
        $this->click("id=aLinkForgotPassword");
        $this->waitForPageToLoad("30000");

        $this->type("id=RecoverPwdForm_email", $emailUserTest);
        $this->click("id=btnRecoverPass");
        $this->waitForPageToLoad("30000");


        $this->assertTextPresent('Please check your mail box to see your new password');
        $this->assertTrue($this->isElementPresent("id=aLinkForgotPassword"));
        $this->assertTrue($this->isElementPresent("id=aLinkBackToWeb"));
        // Some background checks:
        $moUser->getUserByEmail($emailUserTest,false);
        $this->assertTrue($pwdOld!==$moUser->password, 'The password has not changed at all.');
        $moUser->setPassword("123456789",false);
        $this->assertTrue($moUser->updatePwd());

        return true;
    }


    /**
     * @test
     *
     * @param $email
     * @param $countryName
     * @param $countryId
     * @param $ip
     *
     * @dataProvider dataCountriesRecognition
     * @return bool
     */
    public function testCountriesRecognition( $countryName, $countryId, $ip, $email )
    {
        $moUser = new AbaUser();
        $moUser->getUserByEmail($email);

        $countryBfreLogin = intval($moUser->countryId);
        $countryCustomBfreLogin = intval($moUser->countryIdCustom);

        $this->loginUserByEmail( $email, true, '?IP_FALSE_TEST='.$ip  );
        //$this->open('site/index/?IP_FALSE_TEST='.$ip);
        //$this->waitForPageToLoad(3000);

        if( ($countryBfreLogin==intval(Yii::app()->config->get("ABACountryId")) || empty($countryBfreLogin)) &&
            intval($moUser->userType)!=PREMIUM )
        {
            $moUser->refresh();
            $this->assertEquals( $countryId, intval($moUser->countryId),
                    'Expect '.$countryId.' for user type '.$moUser->userType.' but is '.$moUser->countryId);
            $this->assertEquals( $countryId, intval($moUser->countryIdCustom),
                    'Expect '.$countryId.' for user type '.$moUser->userType.' but is '.$moUser->countryIdCustom);

            $moCountry = new AbaCountry($moUser->countryId);
            $this->assertEquals($moCountry->name_m, $countryName);
        }
        elseif( $moUser->userType==PREMIUM || !empty($moUser->cancelReason))
        {
            $moUser->refresh();
            $this->assertEquals( $countryBfreLogin, intval($moUser->countryId) );
            $this->assertEquals( $countryCustomBfreLogin, intval($moUser->countryIdCustom) );
        }
        elseif( $moUser->userType==FREE && $countryBfreLogin!==intval(Yii::app()->config->get("ABACountryId"))  )
        {
            $moUser->refresh();
            $this->assertEquals( $countryBfreLogin, intval($moUser->countryId) );
            $this->assertEquals( $countryCustomBfreLogin, intval($moUser->countryIdCustom) );
        }

        $this->logoutUserByEmail($email);

        return true;
    }

    /** Checks double login for a user that is FREE without a country,
     * and asserts that in the first login country changes, but in the second no.
     * @test
     *
     * @return bool
     */
    public function testDoubleCountriesRecognition(  )
    {
        $email='unit.test.free.user@yopmail.com';
        $countryName = "ITALY";
        $countryId = 105;
        $ip = '87.17.178.25';

        // 'BRAZIL' :
        $ipBrasil = '177.8.32.14';

        $moUser = new AbaUser();
        $moUser->getUserByEmail($email);
        $this->assertEquals( Yii::app()->config->get("ABACountryId"), intval($moUser->countryId) );

        // 1st Login
        $this->loginUserByEmail( $email, true, '?IP_FALSE_TEST='.$ip );
        $this->open('site/index/?IP_FALSE_TEST='.$ip);
        $this->waitForPageToLoad(1000);

        $moUser->refresh();
        $this->assertEquals( $countryId, intval($moUser->countryId) );
        $this->assertEquals( $countryId, intval($moUser->countryIdCustom) );

        $this->logoutUserByEmail($email);

        // 2nd Login
        $this->loginUserByEmail( $email, true, '?IP_FALSE_TEST='.$ipBrasil );
        $this->open('site/index/?IP_FALSE_TEST='.$ipBrasil);
        $this->waitForPageToLoad(1000);

        $this->logoutUserByEmail($email);
        $moUser->refresh();

        $this->assertEquals( $countryId, intval($moUser->countryId) );
        $this->assertEquals( $countryId, intval($moUser->countryIdCustom) );

        $moCountry = new AbaCountry($moUser->countryId);
        $this->assertEquals($moCountry->name_m, $countryName);

        return true;

    }

    /** List of countries and emails to check recognition
     * @return array
     */
    public function dataCountriesRecognition()
    {
        $aCombinations = array();
        $aCountries = array(
            array("BRAZIL", 30, '177.8.32.14'),
            array("ITALY", 105, '87.17.178.25'),
            array("SPAIN", 199, '89.140.161.225'),
            array("ABA DEFAULT COUNTRY NEW CAMPUS 4.0", intval(Yii::app()->config->get("ABACountryId")), '127.0.0.1')
        );

        $aEmails = $this->dataGenericUsersTest();
        foreach( $aEmails as $email=>$value)
        {
            if(is_string($value[0])){
                foreach( $aCountries as $aCountry)
                {
                   array_push( $aCountry, $value[0] );
                    array_push( $aCombinations, $aCountry) ;
                }
            }
        }


        return $aCombinations;
    }

    /** Returns users that are created temporary in database
     * for testing purposes. To be used in any of the Web functional tests
     * @return array
     */
    public function dataGenericUsersTest()
    {
        return array(
            array( 'unit.test.premium.user@yopmail.com' ),
            array( 'unit.test.free.user@yopmail.com' ),
            array( 'unit.test.free.user.honduras@yopmail.com' ),
        );
    }

    /**
     * @return bool
     */
    protected function assertPopupSelectLevel()
    {
        $this->assertElementPresent('id=greetingPopupLevel');

        return true;
    }



    /** Tests login OK
     *
     */
    public function testCampusLoginWithFacebook()
    {
        $sFbid =    '1382300135394759';
        $sEmail1 =  'alek_dcfscto_sander@tfbnw.net';
        $sPass =    '123456789';
        $lang =     'es';
        $sName =    'Alek';
        $sSurname = 'Sander';
        $sGender =  'male';

        $firstLoginDone = true;

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
        $createResponse =   $this->createUsersIntoDb($sEmail1, FREE, Yii::app()->config->get("ABACountryId"), $sName, $sSurname, false, $lang, (trim($sGender) == 'female' ? 'F' : (trim($sGender) == 'male' ? 'M' : '')), $firstLoginDone);
        $userIdFb =         $this->createUserFbidIntoDb($sEmail1, $sFbid);
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

        $this->open('http://campus.dev.abaenglish.com');

        //
        // Login facebook
        //
        $this->loginInFacebookCampus($sEmail1, $sPass);

        //
        // STEP 1 - CHECK if $sEmail1 exists
        //
        $this->waitForElementPresent("id=btnFbSubmit");
        $this->clickAndWait("id=btnFbSubmit");

        //
        // CAMPUS
        //
        $this->assertCampus($firstLoginDone);

        $this->assertFacebookUser( $sFbid, $sEmail1, $sName, $sSurname, $sGender, $lang);
    }


    /** Tests. Check register user with NO CHANGED e-mail
     *
     */
    public function testCampusRegisterUserNoChangedEmailFacebook()
    {
        $sFbid =            '1382300135394759';
        $sEmail1 =          'alek_dcfscto_sander@tfbnw.net';
        $sPass =            '123456789';
        $lang =             'es';
        $sName =            'Alek';
        $sSurname =         'Sander';
        $sGender =          'female';
        $firstLoginDone =   false;

        $this->open('http://campus.dev.abaenglish.com');

        //
        // Login facebook
        //
        $this->loginInFacebookCampus($sEmail1, $sPass);


        //
        // STEP 1 - CHECK if $sEmail1 exists
        //
        $this->waitForElementPresent("id=btnFbSubmit");
        $this->clickAndWait("id=btnFbSubmit", 10000);

        //
        // STEP 2 - E-MAIL
        //
        $this->waitForElementPresent("id=LoginFacebookForm_fbEmail");
        $this->waitForElementPresent("id=btnFbSubmitReg");
        $this->clickAndWait("id=btnFbSubmitReg", 10000);

        //
        // CAMPUS
        //
        $this->assertCampus($firstLoginDone);

        $this->assertFacebookUser($sFbid, $sEmail1, $sName, $sSurname, $sGender, $lang);
    }


    /** Tests. Check register user with CHANGED e-mail if not exists in DB
     *
     */
    public function testCampusRegisterUserChangedEmailFacebook()
    {
        $sFbid =            '1382300135394759';
        $sEmail1 =          'alek_dcfscto_sander@tfbnw.net';
        $sEmail2 =          'dick_kzmgqig_martinez@tfbnw.net';
        $sPass =            '123456789';
        $lang =             'es';
        $sName =            'Alek';
        $sSurname =         'Sander';
        $sGender =          'female';
        $firstLoginDone =   false;

        $this->open('http://campus.dev.abaenglish.com');

        //
        // Login facebook
        //
        $this->loginInFacebookCampus($sEmail1, $sPass);

        //
        // STEP 1 - CHECK if $sEmail1 exists
        //
        $this->waitForElementPresent("id=btnFbSubmit");
        $this->clickAndWait("id=btnFbSubmit", 20000);

        //
        // STEP 2 - E-MAIL
        //
        $this->waitForElementPresent("id=LoginFacebookForm_fbEmail");
        $this->waitForElementPresent("id=btnFbSubmitReg");
        $this->type("id=LoginFacebookForm_fbEmail", $sEmail2);
        $this->clickAndWait("id=btnFbSubmitReg", 20000);

        //
        // CAMPUS
        //
        $this->assertCampus($firstLoginDone);

        $this->assertFacebookUser($sFbid, $sEmail2, $sName, $sSurname, $sGender, $lang);
    }

    /** Tests. Register user with CHANGED e-mail
     *
     */
    public function testCampusRegisterUserExistingEmailFacebook()
    {
        $sFbid1 =           '1382300135394759';
        $sFbid2 =           '388497624635911';
        $sEmail1 =          'alek_dcfscto_sander@tfbnw.net';
        $sEmail2 =          'dick_kzmgqig_martinez@tfbnw.net';
        $sPass =            '123456789';
        $sName =            'Ivan';
        $sSurname =         'Martinez';
        $sGender =          'female';
        $langEnv =          HeMixed::getAutoLangDecision();
        $firstLoginDone =   false;

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
        $createResponse = $this->createUsersIntoDb($sEmail1, FREE, Yii::app()->config->get("ABACountryId"), $sName, $sSurname, false, $langEnv, (trim($sGender) == 'female' ? 'F' : (trim($sGender) == 'male' ? 'M' : '')), $firstLoginDone);
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

        $userCommon = new UserCommon();

        $moUser1 = $userCommon->checkIfFbUserExists($sFbid1);
        $this->assertInternalType('boolean', $moUser1);
        $this->assertEquals( false, $moUser1, "STEP 1: User exists with facebook ID " . $sFbid1);

        $this->open('http://campus.dev.abaenglish.com');

        //
        // Login facebook
        //
        $this->loginInFacebookCampus($sEmail2, $sPass);

        //
        // STEP 1 - CHECK if $sEmail2 exists
        //
        $this->waitForElementPresent("id=btnFbSubmit");
        $this->clickAndWait("id=btnFbSubmit", 20000);

        //
        // STEP 2 - E-MAIL
        //
        $this->waitForElementPresent("id=LoginFacebookForm_fbEmail");
        $this->waitForElementPresent("id=btnFbSubmitReg");
        $this->type("id=LoginFacebookForm_fbEmail", $sEmail1);
        $this->clickAndWait("id=btnFbSubmitReg", 20000);

        //
        // STEP 3 - PASSWORD
        //
        $this->waitForElementPresent("id=LoginFacebookForm_fbEmail");
        $this->waitForElementPresent("id=LoginFacebookForm_fbPassword");
        $this->waitForElementPresent("id=btnFbSubmitReg");
        $this->type("id=LoginFacebookForm_fbPassword", $sPass);
        $this->clickAndWait("id=btnFbSubmitReg", 20000);

        //
        // CAMPUS
        //
        $this->assertCampus(true);

        $this->assertFacebookUser($sFbid2, $sEmail1, $sName, $sSurname, $sGender, $langEnv, true, false);
    }



    /** Tests login OK
     *
     */
    public function testWebLoginWithFacebook()
    {
        $sFbid =    '330128443835820';
        $sEmail1 =  'mykola_hmojixi_paganini@tfbnw.net';
        $sPass =    '123456789';
        $lang =     'es';
        $sName =    'Мыкола';
        $sSurname = 'Паганини';
        $sGender =  'male';
        $firstLoginDone = true;

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
        $createResponse =   $this->createUsersIntoDb($sEmail1, FREE, Yii::app()->config->get("ABACountryId"), $sName, $sSurname, false, $lang, (trim($sGender) == 'female' ? 'F' : (trim($sGender) == 'male' ? 'M' : '')), $firstLoginDone);
        $userIdFb =         $this->createUserFbidIntoDb($sEmail1, $sFbid);
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

        $this->open('http://www.dev.abaenglish.com/');

        $this->clickAndWait("css=div.buttonStd > a", 10000);

        //
        // Login facebook
        //
        $this->loginInFacebookWeb($sEmail1, $sPass);

        //
        // STEP 1 - CHECK if $sEmail1 exists
        //
        $this->waitForElementPresent("id=sendSuscriptionFacebookButton");
        $this->clickAndWait("id=sendSuscriptionFacebookButton");

        //
        // CAMPUS
        //
        $this->assertCampus($firstLoginDone);

        $this->assertFacebookUser($sFbid, $sEmail1, $sName, $sSurname, $sGender, $lang);
    }

    /** Tests. Check register user with NO CHANGED e-mail
     *
     */
    public function testWebRegisterUserNoChangedEmailFacebook()
    {
        $sFbid =    '330128443835820';
        $sEmail1 =  'mykola_hmojixi_paganini@tfbnw.net';
        $sPass =    '123456789';
        $lang =     'es';
        $sName =    'Мыкола';
        $sSurname = 'Паганини';
        $sGender =  'male';
        $firstLoginDone = false;

        $this->open('http://www.dev.abaenglish.com/');

        $this->clickAndWait("css=div.buttonStd > a", 10000);

        //
        // Login facebook
        //
        $this->loginInFacebookWeb($sEmail1, $sPass);

        //
        // STEP 1 - CHECK if $sEmail1 exists
        //
        $this->waitForElementPresent("id=sendSuscriptionFacebookButton");
        $this->click("id=sendSuscriptionFacebookButton");

        //
        // STEP 2 - E-MAIL
        //
        $this->waitForTextPresent(Yii::t("mainApp", 'Volver atrás', NULL, NULL, Yii::app()->params["langAssert"]));
        $this->waitForElementPresent("id=fbEmail");
        $this->waitForElementPresent("id=registerSuscriptionFacebookButton");
        $this->clickAndWait("id=registerSuscriptionFacebookButton", 20000);

        //
        // CAMPUS
        //
        $this->assertCampus($firstLoginDone);

        $this->assertFacebookUser( $sFbid, $sEmail1, $sName, $sSurname, $sGender, $lang, true, false);
    }

    /** Tests. Check register user with CHANGED e-mail if not exists in DB
     *
     */
    public function testWebRegisterUserChangedEmailFacebook()
    {
        $sFbid =    '330128443835820';
        $sEmail1 =  'mykola_hmojixi_paganini@tfbnw.net';
        $sEmail2 =  'sofxa_utqwoev_gubajdulina@tfbnw.net';
        $sPass =    '123456789';
        $lang =     'es';
        $sName =    'Мыкола';
        $sSurname = 'Паганини';
        $sGender =  'male';
        $firstLoginDone = false;

        $this->open('http://www.dev.abaenglish.com/');

        $this->clickAndWait("css=div.buttonStd > a", 10000);

        //
        // Login facebook
        //
        $this->loginInFacebookWeb($sEmail1, $sPass);

        //
        // STEP 1 - CHECK if $sEmail1 exists
        //
        $this->waitForElementPresent("id=sendSuscriptionFacebookButton");
        $this->click("id=sendSuscriptionFacebookButton");

        //
        // STEP 2 - E-MAIL
        //
        $this->waitForTextPresent(Yii::t("mainApp", 'Volver atrás', NULL, NULL, Yii::app()->params["langAssert"]));
        $this->waitForElementPresent("id=fbEmail");
        $this->waitForElementPresent("id=registerSuscriptionFacebookButton");
        $this->type("id=fbEmail", $sEmail2);
        $this->clickAndWait("id=registerSuscriptionFacebookButton", 20000);

        //
        // CAMPUS
        //
        $this->assertCampus($firstLoginDone);

        $this->assertFacebookUser( $sFbid, $sEmail2, $sName, $sSurname, $sGender, $lang, true, false);
    }

    /** Tests. Register user with CHANGED e-mail
     *
     */
    public function testWebRegisterUserExistingEmailFacebook()
    {
        $sFbid1 =    '330128443835820';
        $sFbid2 =    '1503298756588700';
        $sEmail1 =  'mykola_hmojixi_paganini@tfbnw.net';
        $sEmail2 =  'sofxa_utqwoev_gubajdulina@tfbnw.net';
        $sPass =    '123456789';
        $sName =    'Мыкола';
        $sSurname = 'Паганини';
        $sGender =  'male';
        $langEnv =  HeMixed::getAutoLangDecision();
        $firstLoginDone = false;

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
        $createResponse =   $this->createUsersIntoDb($sEmail1, FREE, Yii::app()->config->get("ABACountryId"), $sName, $sSurname, false, $langEnv, (trim($sGender) == 'female' ? 'F' : (trim($sGender) == 'male' ? 'M' : '')), $firstLoginDone);
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

        $userCommon = new UserCommon();

        $moUser1 = $userCommon->checkIfFbUserExists($sFbid1);
        $this->assertInternalType('boolean', $moUser1);
        $this->assertEquals( false, $moUser1, "STEP 1: User exists with facebook ID " . $sFbid1);


        $this->open('http://www.dev.abaenglish.com/');

        $this->clickAndWait("css=div.buttonStd > a", 10000);

        //
        // Login facebook
        //
        $this->loginInFacebookWeb($sEmail2, $sPass);

        //
        // STEP 1 - CHECK if $sEmail1 exists
        //
        $this->waitForElementPresent("id=sendSuscriptionFacebookButton");
        $this->click("id=sendSuscriptionFacebookButton");

        //
        // STEP 2 - E-MAIL
        //
//        $this->waitForTextPresent(Yii::t("mainApp", 'Volver atrás', NULL, NULL, Yii::app()->params["langAssert"]), 3000);
        $this->waitForTextPresent("Софья", 3000);
        $this->waitForElementPresent("id=fbEmail", 3000);
        $this->waitForElementPresent("id=registerSuscriptionFacebookButton", 3000);
        $this->type("id=fbEmail", $sEmail1);
        $this->click("id=registerSuscriptionFacebookButton");


        //
        // STEP 3 - PASSWORD
        //
//        $this->waitForTextPresent(Yii::t("mainApp", 'Volver atrás', NULL, NULL, Yii::app()->params["langAssert"]), 3000);
//        $this->waitForElementPresent("id=fbPassBlock", 3000);
//        $this->waitForElementPresent("id=fbEmail", 3000);
//        $this->waitForElementPresent("id=fbPass", 3000);
//        $this->waitForElementPresent("id=registerSuscriptionFacebookButton", 3000);
        $this->type("id=fbPass", $sPass);
        $this->clickAndWait("id=registerSuscriptionFacebookButton");

        //
        // CAMPUS
        //
        $this->assertCampus(true);

        $this->assertFacebookUser( $sFbid2, $sEmail1, $sName, $sSurname, $sGender, $langEnv, true, false);
    }


    /** Tests. Register user with CHANGED e-mail
     *
     */
    public function testWebRegisterUserPremiumWithCancelExistingEmailFacebook()
    {
        $sFbid1 =    '330128443835820';
        $sFbid2 =    '1503298756588700';
        $sEmail1 =  'mykola_hmojixi_paganini@tfbnw.net';
        $sEmail2 =  'sofxa_utqwoev_gubajdulina@tfbnw.net';
        $sPass =    '123456789';
        $sName =    'Мыкола';
        $sSurname = 'Паганини';
        $sGender =  'male';
        $langEnv =  HeMixed::getAutoLangDecision();
        $firstLoginDone = false;

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
        $createResponse =   $this->createUsersIntoDb($sEmail1, FREE, Yii::app()->config->get("ABACountryId"), $sName, $sSurname, false, $langEnv, (trim($sGender) == 'female' ? 'F' : (trim($sGender) == 'male' ? 'M' : '')), $firstLoginDone);
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

        $userCommon = new UserCommon();

        $moUser1 = $userCommon->checkIfFbUserExists($sFbid1);
        $this->assertInternalType('boolean', $moUser1);
        $this->assertEquals( false, $moUser1, "STEP 1: User exists with facebook ID " . $sFbid1);

        $this->open('http://www.dev.abaenglish.com/');

//        $this->clickAndWait("id=menu-item-94", 10000);
        $this->clickAndWait("link=" . Yii::t("mainApp", 'Planes', NULL, NULL, Yii::app()->params["langAssert"]), 10001);
        $this->clickAndWait("css=a.btn-red > strong", 10002);
        $this->clickAndWait("css=td.shadow-left > a.btn-blue", 10003);


//        $this->clickAndWait("css=div.buttonStd > a", 10000);

        //
        // Login facebook
        //
        $this->loginInFacebookWeb($sEmail2, $sPass, false);

        //
        // STEP 1 - CHECK if $sEmail1 exists
        //
        $this->waitForElementPresent("id=sendSuscriptionFacebookButton");
        $this->click("id=sendSuscriptionFacebookButton");

        //
        // STEP 2 - E-MAIL
        //
//        $this->waitForTextPresent(Yii::t("mainApp", 'Volver atrás', NULL, NULL, Yii::app()->params["langAssert"]), 3000);
        $this->waitForTextPresent("Софья", 3001);
        $this->waitForElementPresent("id=fbEmail", 3002);
        $this->waitForElementPresent("id=registerSuscriptionFacebookButton", 3003);
        $this->type("id=fbEmail", $sEmail1);
        $this->click("id=registerSuscriptionFacebookButton");

        //
        $this->click("id=cancelSuscriptionFacebookButton");

        //
        // Login facebook
        //
        $this->loginInFacebookWeb($sEmail2, $sPass, false, true);

        //
        // STEP 2 - E-MAIL
        //
        $this->type("id=fbEmail", $sEmail1);
        $this->click("id=registerSuscriptionFacebookButton");

        //
        // STEP 3 - PASSWORD
        //
//        $this->waitForTextPresent(Yii::t("mainApp", 'Volver atrás', NULL, NULL, Yii::app()->params["langAssert"]), 3000);
//        $this->waitForElementPresent("id=fbPassBlock", 3000);
//        $this->waitForElementPresent("id=fbEmail", 3000);
//        $this->waitForElementPresent("id=fbPass", 3000);
//        $this->waitForElementPresent("id=registerSuscriptionFacebookButton", 3000);
        $this->type("id=fbPass", $sPass);
        $this->clickAndWait("id=registerSuscriptionFacebookButton");

        //
        // CAMPUS
        //
        $this->assertCampus(true);

        $this->assertFacebookUser( $sFbid2, $sEmail1, $sName, $sSurname, $sGender, $langEnv, true, false);
    }




}
