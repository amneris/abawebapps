<?php

/**
 * To be able to run tests we should start the Selenium Server:
 * C:\Program Files (x86)\Java\jre7\bin>java.exe -jar C:\wamp\bin\selenium\selenium-server-standalone-2.31.0.jar
 */
class ProfileControllerTest extends AbaWebTestCase
{

    protected static $emailUserTest = "unit.test.premium.user@yopmail.com";


    /**
     * Tests the action of typing and saving my personal details.
     * @dataProvider dataUsersCase
     *
     * @param string $email
     *
     * @return bool
     */
    public function testProfileSaveMyAccount($email)
    {
        if (!$this->loginUserByEmail($email) && "unit.test.deleted.user@yopmail.com" == $email) {
            $this->assertTrue(true);
            return true;
        }
        // Closes the dialog box of JQuery User Objectives Form
        if ($this->isElementPresent("id=CampusModals")) {
            $this->click("class=ui-dialog-titlebar-close ui-corner-all");
        }

        $this->clickAndWait("id=headLinkMenuMyAccount");
        // Asserts that we see My account page.
        $this->assertMyAccountPage();

        $this->assertElementValueEquals("id=ProfileForm_email", $email,
            "Email should be " . $email);
        $this->assertTrue($this->isElementPresent("id=btnSaveMyAccount"));

        $this->type("id=ProfileForm_name", "SeleniumTest");
        $this->type("id=ProfileForm_surnames", "Web Case Yii");
        $this->type("id=ProfileForm_birthDate", "08-05-1947");
        $this->type("id=ProfileForm_city", "Guayaba");
        $this->type("id=ProfileForm_telephone", "458878798");

        $this->clickAndWait("id=btnSaveMyAccount");

        $this->assertEquals("Saved correctly.", $this->getText("id=msgConfirmation"));

        $this->assertEquals("SeleniumTest", $this->getValue("id=ProfileForm_name"));
        $this->assertEquals("Web Case Yii", $this->getValue("id=ProfileForm_surnames"));
        $this->assertEquals("08-05-1947", $this->getValue("id=ProfileForm_birthDate"));
        $this->assertEquals("Guayaba", $this->getValue("id=ProfileForm_city"));

        return true;
    }


}