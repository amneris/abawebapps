<?php
/**
 * To test web services that are in charge of registering users
 * User: Quino
 * Date: 7/05/13
 * Time: 12:39
 * To change this template use File | Settings | File Templates.
 */
class WsoapabaControllerTest extends AbaWebTestCase
{
    protected $urlWs;

    /**
     * Override to set URL public ws
     */
    public function setUp()
    {
        parent::setUp();
        $this->urlWs = "http://" . Yii::app()->config->get("SELF_DOMAIN") . "/wsoapaba/usersapi/";
    }

    /**
     * Deletes test users used in this class
     */
    public function tearDown()
    {
        $this->resetDownUser('unit.test.functional.free@yopmail.com');
        $this->resetDownUser('unit.test.mobile.free@yopmail.com');
        $this->resetDownUser('test.fabcebook.user.1@facebookuserabatest.com');

        //
        // delete "existing" users for extranet test
        //
        $this->resetDownUser("unit.functional.premium.extranet.ex@yopmail.com");
        $this->resetDownUser("unit.functional.free.extranet.ex@yopmail.com");
        $this->resetDownUser("unit.functional.ex.premium.extranet.ex@yopmail.com");
        $this->resetDownUser("unit.functional.premium.cancelled.extranet.ex@yopmail.com");
        $this->resetDownUser("unit.functional.deleted.extranet.ex@yopmail.com");

        parent::tearDown();
    }


    /** Checks if the WSDL definition file is public and available.
     * @test
     *
     * @return Boolean
     */
    public function testWsdlAvailable()
    {
        $this->open("/wsoapaba/usersapi/");

        $this->assertTrue($this->isElementPresent("css=definitions[name=\"WsoapabaController\"]"));

        $opts = array(
            'http' => array(
                'method'=>"GET",
                'header'=>"Content-Type: text/xml; charset=utf-8"
            )
        );
        $context = stream_context_create($opts);
        $xmlWsdl = file_get_contents($this->urlWs, false, $context );
        try{
            $oXmlWsdl =  new SimpleXMLElement($xmlWsdl);
            // IsValid XML
            $this->assertGreaterThan(5, count($oXmlWsdl->getDocNamespaces()));
        }
        catch(Exception $e){
            $this->assertTrue (false, 'The WSDL is not accesible or is invalid: '.$e->getMessage());
        }
        return true;
    }

    /** Checks WSDL, functions and version method
     * @test
     *
     * @return SoapClient SoapClient
     */
    public function testFunctionsAvailable()
    {
        ini_set("soap.wsdl_cache_enabled", 0);
        $client = new SoapClient($this->urlWs, array('soap_version'=> SOAP_1_2, 'port'=> 80, 'trace' => 1));

        $aFunctions = $client->__getFunctions();
        $this->assertTrue((strpos($aFunctions[0], 'Array version()') >= 0));
        $this->assertTrue((strpos($aFunctions[1], 'Array registerUser') >= 0));
        $this->assertTrue((strpos($aFunctions[2], 'Array registerUserPartner') >= 0));
        $this->assertTrue((strpos($aFunctions[3], 'Array registerUserFromLevelTest') >= 0));
        $this->assertTrue((strpos($aFunctions[4], 'Array registerUserAffiliate') >= 0));
        $this->assertTrue((strpos($aFunctions[4], 'Array registerUserPremiumB2b') >= 0));

        /** @noinspection PhpUndefinedMethodInspection */
        $version = $client->version();
        $this->assertTrue(floatval($version["version"]) == 1.29);

        return $client;
    }

    /** Tests web service of register user in order to raise validations work
     * @test
     *
     * @depends testFunctionsAvailable
     */
    public function testNotRegisterUserValidationsCheck(SoapClient $client)
    {
        // Invalid Signature
        $signature = 'AbaEnglishLocal' . 'unit.test.functional.free@yopmail.com' . '123456789' .
            'en' . 'FunctionalTest' . 'YiiPHP' ."199_30" . "promocode" . "HOME" . '199' . '20';
        /* @var WsoapabaController $client */
        $aResponse = $client->registerUser($signature, 'unit.test.functional.free@yopmail.com', '123456789', 'en',
            'FunctionalTest', 'YiiPHP',
            '199_30', 'promocode', 'HOME', 199, 20);

        $this->assertArrayHasKey("502", $aResponse);
        $this->assertContains("Invalid Signature", $aResponse["502"]);

        // Invalid Id Product
        $aResponse = $client->registerUser($signature . "123", 'unit.test.functional.free@yopmail.com', '123456789', 'en',
            'FunctionalTest', 'YiiPHP',
            'XXX_YY', 'promocode', 'HOME', 199, 20, DEVICE_SOURCE_PC);

        $this->assertArrayHasKey('508', $aResponse);
        $this->assertContains('Id product ', $aResponse["508"]);

        return true;
    }

    /** Tests web service of register user in a SIMPLE way, only connection and
     * creation of user
     * @test
     *
     * @depends testFunctionsAvailable
     */
    public function testRegisterUser(SoapClient $client)
    {
        // Invalid Signature
        $signature =  HeAbaSHA::_md5(WORD4WS . 'unit.test.functional.free@yopmail.com' . '123456789' . 'en' .
            'FunctionalTest' . 'YiiPHP' . '199_30' . "" . "HOME" . '199' . '20'. 1);
        /** @noinspection PhpUndefinedMethodInspection */
        $aResponse = $client->registerUser($signature . '123', 'unit.test.functional.free@yopmail.com', '123456789', 'en',
            'FunctionalTest', 'YiiPHP',
            '199_30', '', 'HOME', 199, 20, 1);

        //It should equal to $aResponse = array("userId","result","redirectUrl")
        $this->assertArrayHasKey('userId', $aResponse,
            "Response web service returned: ".implode(array_keys($aResponse)));
        $this->assertArrayHasKey('result', $aResponse);
        $this->assertArrayHasKey('redirectUrl', $aResponse);

        $this->assertInternalType('integer', intval($aResponse["userId"]));
        $this->assertGreaterThanOrEqual(6000, intval($aResponse["userId"]));

        $isUrl = preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $aResponse['redirectUrl']);

        $this->assertGreaterThanOrEqual(1, $isUrl, "No valid URL returned for auto Login");


        if ($aResponse["userId"]) {
            $moUser = new AbaUser();
            if ($moUser->getUserById($aResponse["userId"])) {
                $this->assertEquals( FREE, $moUser->userType);
                $this->assertEquals( 199, $moUser->countryId);
                $this->assertEquals( 1, $moUser->idSourceList);

                $moUser->deleteUserThroughResetCall();
            }

        }

        return true;
    }

    /** Tests web service with parameter device.
     * Creation of new user through SOAP WS.
     * @test
     *
     * @depends testFunctionsAvailable
     */
    public function testRegisterUserDevice(SoapClient $client)
    {
        // Invalid Signature
        $signature =  HeAbaSHA::_md5(WORD4WS . 'unit.test.mobile.free@yopmail.com' . '123456789' . 'en' .
        'FunctionalTest' . 'YiiPHP' . '199_30' . "" . "HOME" . '199' . '20'. '1'. DEVICE_SOURCE_MOBILE);
        /** @noinspection PhpUndefinedMethodInspection */
        /* @var WsoapabaController $client */
        $aResponse = $client->registerUser($signature . '123', 'unit.test.mobile.free@yopmail.com', '123456789', 'en',
            'FunctionalTest', 'YiiPHP',
            '199_30', '', 'HOME', 199, 20, 1, DEVICE_SOURCE_MOBILE);

        //It should equal to $aResponse = array("userId","result","redirectUrl")
        $this->assertArrayHasKey('userId', $aResponse,
            "Response web service returned: ".implode(array_keys($aResponse)));
        $this->assertArrayHasKey('result', $aResponse);
        $this->assertArrayHasKey('redirectUrl', $aResponse);
        $this->assertInternalType('integer', intval($aResponse["userId"]));
        $isUrl = preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $aResponse['redirectUrl']);
        $this->assertGreaterThanOrEqual(1, $isUrl, "No valid URL returned for auto Login");
            $moUser = new AbaUser();
            if ($moUser->getUserById($aResponse["userId"])) {
                $this->assertEquals( DEVICE_SOURCE_MOBILE, $moUser->deviceTypeSource, 'Device source should be ');
                $moUser->deleteUserThroughResetCall();
            }
    }

    /** Tests web service of register user from level test in a SIMPLE way, only connection and
     * creation of user
     * @test
     *
     * @depends testFunctionsAvailable
     */
    public function testRegisterUserFromLevelTest(SoapClient $client)
    {
        // Invalid Signature
        $signature = 'sfdsfsfsdf' . '123'.'unit.test.functional.free.level@yopmail.com'.'en'.
        '3'.'0'.'199'.PARTNER_ID_ADWORDS;

        /** @noinspection PhpUndefinedMethodInspection */
        /* @var WsoapabaController $client */
        $aResponse = $client->registerUserFromLevelTest($signature . '123', 'unit.test.functional.free.level@yopmail.com'
            , 'en', 3, 0, 199, PARTNER_ID_ADWORDS, 1);

        //It should equal to $aResponse = array("userId","result","redirectUrl")
        $this->assertArrayHasKey('userId', $aResponse,
            "Response web service returned: ".implode(array_keys($aResponse)));
        $this->assertArrayHasKey('result', $aResponse);
        $this->assertArrayHasKey('redirectUrl', $aResponse);

        $this->assertInternalType('integer', intval($aResponse["userId"]));
        $this->assertGreaterThanOrEqual(6000, intval($aResponse["userId"]));

        $isUrl = preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $aResponse['redirectUrl']);

        $this->assertGreaterThanOrEqual(1, $isUrl, "No valid URL returned for auto Login");


        if ($aResponse["userId"]) {
            $moUser = new AbaUser();
            if ($moUser->getUserById($aResponse["userId"])) {
                $this->assertEquals( FREE, $moUser->userType);
                $this->assertEquals( 199, $moUser->countryId);
                $this->assertEquals( 1 , $moUser->idSourceList);

                $moUser->deleteUserThroughResetCall();
            }

        }

        return true;
    }

    /** Tests web service of register user in a simple way.
     * @test
     *
     * @depends testFunctionsAvailable
     */
    public function testRegisterUserPartner(SoapClient $client)
    {
        ini_set("soap.wsdl_cache_enabled", 0);
        $idPartner = 72;
        // Valid Signature
        $signature = HeAbaSHA::_md5(WORD4WS.'unit.test.functional.premium@yopmail.com'.'123456789'."en"."Unit"."Test".
                "999999999".'unittestvoucher'.
                'secur_unittestvoucher'.$idPartner.'3'.'199'.DEVICE_SOURCE_TABLET);
        /** @noinspection PhpUndefinedMethodInspection */
        /* @var WsoapabaController $client */
        $aResponse = $client->registerUserPartner( $signature , 'unit.test.functional.premium@yopmail.com', '123456789',
                "en", "Unit", "Test",
                "999999999", 'unittestvoucher',
                'secur_unittestvoucher', $idPartner, 3, 199, DEVICE_SOURCE_TABLET);

        //It should equal to $aResponse = array("userId","result","redirectUrl")
        $this->assertArrayHasKey('userId', $aResponse,
            "Response web service returned: ".implode(array_keys($aResponse)));
        $this->assertArrayHasKey('result', $aResponse);
        $this->assertArrayHasKey('redirectUrl', $aResponse);

        $this->assertInternalType('integer', intval($aResponse["userId"]));
        $this->assertGreaterThanOrEqual(6000, intval($aResponse["userId"]));

        $isUrl = preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $aResponse['redirectUrl']);

        $this->assertGreaterThanOrEqual(1, $isUrl, "No valid URL returned for auto Login");


        if ($aResponse["userId"]) {
            $moUser = new AbaUser();
            if ($moUser->getUserById($aResponse["userId"])) {
                $this->assertEquals( PREMIUM, $moUser->userType);
                $this->assertEquals( $idPartner, $moUser->idPartnerCurrent);
                $this->assertEquals( 199, $moUser->countryId);
                $this->assertEquals( DEVICE_SOURCE_TABLET, $moUser->deviceTypeSource, 'The device is not registered.');

                $moUser->deleteUserThroughResetCall();
            }

        }

        return true;
    }


    /** Tests web service of register user in a simple way.
     * @test WsoapabaController::registerUserPremiumB2b
     *
     * @depends testFunctionsAvailable
     */
    public function testNotRegisterUserB2bPremium(SoapClient $client)
    {
        ini_set("soap.wsdl_cache_enabled", 0);
        $emailTest = DataCollections::$aUsersShowCase['PREMIUM_ACTIVE_CAIXA'];
        $emailTest = $emailTest[0];
        /*
         $email, $pwd, $langEnv, $name, $surnames,
            $currentLevel, $teacherId, $codeDeal, $partnerId, $periodId, $idCountry, $device
        */
        $signature = HeAbaSHA::_md5(WORD4WS.$emailTest.'123456789'.'es'.'Test Premium B2b'.'Functional Selenium'.
            '3'.'1381077'.'COMPUTENSE180'.'7002'.'180'.'199'.DEVICE_SOURCE_PC);
        /* @var WsoapabaController $client */
        $aResponse = $client->registerUserPremiumB2b( $signature , $emailTest, '123456789','es','Test Premium B2b',
            'Functional Selenium', '3', '1381077', 'COMPUTENSE180', '7002', '180', '199', DEVICE_SOURCE_PC);
        //It should equal to $aResponse = array("userId","result","redirectUrl")
        $this->assertArrayHasKey("519", $aResponse);
        $this->assertContains("already PREMIUM", $aResponse["519"]);

        return true;
    }

    /** Tests That users Ex-Premium are converted to Premium and will belong to B2b
     * @test
     *
     * @depends testFunctionsAvailable
     *
     * @param SoapClient $client
     * @return bool
     */
    public function testRegisterUserB2bExPremium(SoapClient $client)
    {
        ini_set("soap.wsdl_cache_enabled", 0);
        $emailTest = DataCollections::$aUsersShowCase['FREE_EXPREMIUM_CAIXA'];
        $emailTest = $emailTest[0];
        /*
         $email, $pwd, $langEnv, $name, $surnames,
            $currentLevel, $teacherId, $codeDeal, $partnerId, $periodId, $idCountry, $device
        */
        $signature = HeAbaSHA::_md5(WORD4WS.$emailTest.'123456789'.'en'.'Test Ex-Premium B2b'.'Functional Selenium'.
            '3'.'miriamsanchez@linkup-learning.es'.'COMPUTENSE180'.'7002'.'6'.'199'.DEVICE_SOURCE_PC);
        /* @var WsoapabaController $client */
        try{
        $aResponse = $client->registerUserPremiumB2b( $signature , $emailTest, '123456789','en','Test Ex-Premium B2b',
            'Functional Selenium', '3', 'miriamsanchez@linkup-learning.es', 'COMPUTENSE180', '7002', '6', '199', DEVICE_SOURCE_PC);
        }catch (Exception $exc){
            $response =  $client->__getLastResponse();
            $fullResponse = var_export ( $response );
            echo $fullResponse;
        }
        //It should equal to $aResponse = array("userId","result","redirectUrl")
        $this->assertArrayHasKey('userId', $aResponse,
            "Response web service returned: ".implode(array_keys($aResponse)));
        $this->assertArrayHasKey('result', $aResponse);
        $this->assertArrayHasKey('redirectUrl', $aResponse);
        $this->assertInternalType('integer', intval($aResponse["userId"]));
        $this->assertGreaterThanOrEqual(6000, intval($aResponse["userId"]));
        $isUrl = preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $aResponse['redirectUrl']);
        $this->assertGreaterThanOrEqual(1, $isUrl, "No valid URL returned for auto Login");

        // Check consistency fields from table users and others:
        $moUser = new AbaUser();
        $this->assertTrue( $moUser->getUserById(intval($aResponse["userId"])));
        $this->assertTrue( $moUser->getUserById($aResponse["userId"]), 'User id not found, expected after callign WS');

        // General assertions on USER EX PREMIUM CONVERTED TO B2B PREMIUM:
        $this->assertEquals( PREMIUM, intval($moUser->userType));
        $this->assertTrue( HeDate::isGreaterThanToday(HeDate::europeanToSQL($moUser->expirationDate)));
        $this->assertNotEquals(intval($moUser->idPartnerSource), 7002,
                                'Partner Source for an Ex-Premium should not be updated');
        $this->assertEquals( intval($moUser->idPartnerCurrent), 7002, 'Partner current should be updated');
        $moTeacher = new AbaTeacher();
        $moTeacher->getUserByEmail('miriamsanchez@linkup-learning.es');
        $this->assertEquals( $moTeacher->id, $moUser->teacherId);
        $this->assertEquals( 'Test Ex-Premium B2b', $moUser->name, 'Name is updated even it existed.');

        $moPayment = new Payment();
        $moPayment = $moPayment->getLastSuccessPayment($moUser->id);
        $this->assertEquals( intval($moPayment->idPartner), 7002, 'Partner payment should be 7002');
        $this->assertNotEquals( intval($moPayment->idPartnerPrePay), 7002, 'Partner payment should not be 7002');
        $this->assertEquals(PAY_SUPPLIER_B2B, $moPayment->paySuppExtId, 'Method of payment should be B2B');
        $this->assertEquals($moPayment->idPromoCode, 'COMPUTENSE180');

        return true;
    }

    /** Tests That users free are converted to Premium and will belong to B2b
     * @test
     *
     * @depends testFunctionsAvailable
     *
     * @param SoapClient $client
     * @return bool
     */
    public function testRegisterUserB2bFree(SoapClient $client)
    {
        ini_set("soap.wsdl_cache_enabled", 0);
        $emailTest = DataCollections::$aUsersShowCase['FREE'];
        $emailTest = $emailTest[0];
        /*
         $email, $pwd, $langEnv, $name, $surnames,
            $currentLevel, $teacherId, $codeDeal, $partnerId, $periodId, $idCountry, $device
        */
        $signature = HeAbaSHA::_md5(WORD4WS.$emailTest.'123456789'.'en'.'Test Free'.'Functional Selenium'.
            '3'.'miriamsanchez@linkup-learning.es'.'COMPUTENSE360'.'7002'.'12'.COUNTRY_ITALY.DEVICE_SOURCE_PC);
        /* @var WsoapabaController $client */
        $aResponse = $client->registerUserPremiumB2b( $signature , $emailTest, '123456789','en','Test Free',
            'Functional Selenium', '3', 'miriamsanchez@linkup-learning.es', 'COMPUTENSE360', '7002', '12',
            COUNTRY_ITALY, DEVICE_SOURCE_PC);
        //It should equal to $aResponse = array("userId","result","redirectUrl")
        $this->assertArrayHasKey('userId', $aResponse,
            "Response web service returned: ".implode(array_keys($aResponse)));
        $this->assertArrayHasKey('result', $aResponse);
        $this->assertArrayHasKey('redirectUrl', $aResponse);
        $this->assertInternalType('integer', intval($aResponse["userId"]));
        $this->assertGreaterThanOrEqual(6000, intval($aResponse["userId"]));
        $isUrl = preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $aResponse['redirectUrl']);
        $this->assertGreaterThanOrEqual(1, $isUrl, "No valid URL returned for auto Login");

        // Check consistency fields from table users and others:
        $moUser = new AbaUser();
        $this->assertTrue( $moUser->getUserById(intval($aResponse["userId"])));
        $this->assertTrue( $moUser->getUserById($aResponse["userId"]), 'User id not found, expected after callign WS');

        // General assertions on USER EX PREMIUM CONVERTED TO B2B PREMIUM:
        $this->assertEquals( PREMIUM, intval($moUser->userType));
        $this->assertTrue( HeDate::isGreaterThanToday(HeDate::europeanToSQL($moUser->expirationDate)));
        $this->assertNotEquals(intval($moUser->idPartnerSource), 7002,
            'Partner Source for an Ex-Premium should not be updated');
        $this->assertEquals( intval($moUser->idPartnerCurrent), 7002, 'Partner current should be updated');
        $this->assertEquals( intval($moUser->idPartnerFirstPay), 7002, 'Partner firstPay should be updated');
        $moTeacher = new AbaTeacher();
        $moTeacher->getUserByEmail('miriamsanchez@linkup-learning.es');
        $this->assertEquals( $moTeacher->id, $moUser->teacherId);
        $this->assertEquals( 'Test Free', $moUser->name, 'Name is updated even it existed.');

        $moPayment = new Payment();
        $moPayment = $moPayment->getLastSuccessPayment($moUser->id);
        $this->assertEquals( intval($moPayment->idPartner), 7002, 'Partner payment should be 7002');
        $this->assertEmpty($moPayment->idPartnerPrePay, 'PrePay should be NULL');
        $this->assertEquals(PAY_SUPPLIER_B2B, $moPayment->paySuppExtId, 'Method of payment should be B2B');
        $this->assertEquals($moPayment->idPromoCode, 'COMPUTENSE360');
        $this->assertEquals(''.COUNTRY_ITALY.'_360',$moPayment->idProduct, 'Id Product does not match.');

        return true;
    }

    /** Tests web service of register user from affiliates
     * Creation of user and deletion
     *
     * @test
     * @depends testFunctionsAvailable
     */
    public function No_testRegisterUserAffiliate(SoapClient $client)
    {
        // Invalid Signature
        $signature = HeAbaSHA::_md5(WORD4WSPARTNERS . 'unit.test.functional.free@yopmail.com' . '123456789' . 'en' .
                                                        'Test Unit' . 'Taiwanese' . 'HOME'.'27.52.0.15' . '58' . '1');
        /** @var WsoapabaController $client */
        $aResponse = $client->registerUserAffiliate($signature, 'unit.test.functional.free@yopmail.com', '123456789',
                                        'en', 'Test Unit', 'Taiwanese', 'HOME', '27.52.0.15', 58, 1);

        //It should equal to $aResponse = array("userId","result","redirectUrl")
        $this->assertArrayHasKey('userId', $aResponse,
            "Response web service returned: ".implode(array_keys($aResponse)));
        $this->assertArrayHasKey('result', $aResponse);
        $this->assertArrayHasKey('redirectUrl', $aResponse);

        $this->assertInternalType('integer', intval($aResponse["userId"]));
        $this->assertGreaterThanOrEqual(6000, intval($aResponse["userId"]));

        $isUrl = preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $aResponse['redirectUrl']);

        $this->assertGreaterThanOrEqual(1, $isUrl, "No valid URL returned for auto Login");


        if ($aResponse["userId"]) {
            $moUser = new AbaUser();
            if ($moUser->getUserById($aResponse["userId"])) {
                $this->assertEquals( FREE, $moUser->userType);
                $this->assertEquals( 208, $moUser->countryId, 'Taiwan is expected based on the IP');
                $this->assertEquals( 1, $moUser->idSourceList);
                $this->assertEquals( 58, $moUser->idPartnerCurrent);
                $this->assertEquals( 58, $moUser->idPartnerSource);

                $moUser->deleteUserThroughResetCall();
            }

        }

        return true;
    }

    /** Tests web service of register user in a simple way.
     * @test
     *
     * @depends testFunctionsAvailable
     */

    public function testRegisterUserB2bExtranet(SoapClient $client)
    {
        ini_set("soap.wsdl_cache_enabled",  0);
        ini_set("default_socket_timeout",   600);

        $idPartner = 7002;
        $iNumUsers = 12; // total users

        $sEmailPrefix = 'unit.functional.premium.extranet';
        $sEmailSuffix = 'yopmail.com';

        //
        // Create "existing" users
        //
        // PREMIUM
        $userIdPremium = $this->createUsersIntoDb("unit.functional.premium.extranet.ex@yopmail.com", PREMIUM, 199, "UnitPremium", "Testing");

        // FREE
        $userIdFree = $this->createUsersIntoDb("unit.functional.free.extranet.ex@yopmail.com", FREE, 199, "UnitFree", "Testing");

        // FREE that was PREMIUM in the PAST
        $userIdExPremium = $this->createUsersExPremium("unit.functional.ex.premium.extranet.ex@yopmail.com", FREE, 199, "UnitExPremium", "Testing");

        // PREMIUM CANCEL
        $userIdPremiumCancel = $this->createUsersPremiumCancel("unit.functional.premium.cancelled.extranet.ex@yopmail.com", PREMIUM, 199, "UnitPremium", "Testing Cancelled");

        // DELETED: unit.test.lead.user@yopmail.com
        $userIdDeleted = $this->createUsersIntoDb("unit.functional.deleted.extranet.ex@yopmail.com", DELETED, 199, "UnitLead", "Testing");

        //
        $aUsersExtranet = $this->unitTestExtranetData($idPartner);

        foreach ($aUsersExtranet as $iKey => $aUser) {
            $aUsers[] = $aUser;
        }

        //
        // Periodicity
        //
        for($iCounter = 1; $iCounter <= $iNumUsers; $iCounter++) {

            $sUserTmpl =            $aUsersExtranet[0];
            $sUserTmpl['email'] =   $sEmailPrefix . $iCounter . '@' . $sEmailSuffix;

//            if($iCounter < 0) { $sUserTmpl['codeDeal'] = 'EXTRANET30'; $sUserTmpl['periodId'] = '1'; }
//        elseif($iCounter < 30) { $sUserTmpl['codeDeal'] = 'EXTRANET180'; $sUserTmpl['periodId'] = '6'; }
//        elseif($iCounter < 50) { $sUserTmpl['codeDeal'] = 'EXTRANET540'; $sUserTmpl['periodId'] = '18'; }
            if($iCounter < 3) { $sUserTmpl['codeDeal'] = ''; $sUserTmpl['periodId'] = '3'; }  // EXTRANET90
            elseif($iCounter < 6) { $sUserTmpl['codeDeal'] = ''; $sUserTmpl['periodId'] = '12'; }  // EXTRANET360
            elseif($iCounter < 9) { $sUserTmpl['codeDeal'] = ''; $sUserTmpl['periodId'] = '24'; }  // EXTRANET720
            else{ $sUserTmpl['codeDeal'] = 'EXTRANET180'; $sUserTmpl['periodId'] = '3'; }

            $aUsers[] = $sUserTmpl;
        }

        $sUsers = json_encode($aUsers);

        $signatureWsAbaEnglish = md5('Aba English Extranet Key' . $sUsers);

        $aResponse = $client->registerUserPremiumB2bExtranet( $signatureWsAbaEnglish, $sUsers );

        foreach($aResponse as $iKey => $aUserResponse) {

            $this->assertArrayHasKey('email', $aUserResponse);
            $this->assertArrayHasKey('response', $aUserResponse);

            $email =    $aUserResponse['email'];
            $response = $aUserResponse['response'];

            $this->assertArrayHasKey('userId', $response, "Response web service returned: " . implode(array_keys($response)));
            $this->assertArrayHasKey('result', $response);

            $this->assertInternalType('integer', intval($response["userId"]));
            $this->assertGreaterThanOrEqual(6000, intval($response["userId"]));

            if ($response["userId"]) {

                $moUser = new AbaUser();

                if ($moUser->getUserById($response["userId"])) {

                    $this->assertEquals( PREMIUM, $moUser->userType);
                    $this->assertEquals( $idPartner, $moUser->idPartnerCurrent);
                    $this->assertEquals( 199, $moUser->countryId);
//                    $this->assertEquals( DEVICE_SOURCE_EXTRANET, $moUser->deviceTypeSource, 'The device is not registered: ' . $moUser->id);

                    $moUser->deleteUserThroughResetCall();
                }
            }
        }

        return true;
    }



    /** Tests web service. Check if user exists
     * @test
     *
     * @depends testFunctionsAvailable
     */
    public function checkExistsUserFacebook(SoapClient $client)
    {
        $sFbid =    '1234567890';
        $sEmail1 =  'test.fabcebook.user.1@facebookuserabatest.com';
        $lang =     'es';
        $sName =    'Ivan';
        $sSurname = 'Martinez';
        $sGender =  'male';

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
        $createResponse =   $this->createUsersIntoDb($sEmail1, FREE, Yii::app()->config->get("ABACountryId"), $sName, $sSurname, false, $lang, (trim($sGender) == 'female' ? 'F' : (trim($sGender) == 'male' ? 'M' : '')));
        $userIdFb =         $this->createUserFbidIntoDb($sEmail1, $sFbid);
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

        $userCommon = new UserCommon();

        $moUser = $userCommon->checkIfFbUserExists($sFbid);

        $this->assertFacebookUser($moUser, $sFbid, $sEmail1, $sName, $sSurname, $sGender, $lang);

        return true;
    }

    /** Tests web service. Check register user NO CHANGED e-mail AND CHANGED e-mail if not exists in DB
     *
     * @test
     *
     * @depends testFunctionsAvailable
     */
    public function testRegisterUserChangedAndNoChangedEmailFacebook()
    {
        $sFbid =            '1234567890';
        $sEmail1 =          'test.fabcebook.user.1@facebookuserabatest.com';
        $sEmail2 =          'test.fabcebook.user.2@facebookuserabatest.com';
        $sName =            'Ivan';
        $sSurname =         'Martinez';
        $sGender =          'female';
        $sEmailFacebook =   'test.fabcebook.user.1@facebookuserabatest.com';
        $langEnv =          HeMixed::getAutoLangDecision();
        $idCountry =        '';
        $idPartner =        '';
        $idSourceList =     '';
        $device =           'c';
        $currentLevel =     '1';

        $userCommon =   new UserCommon();
        $rulerWs =      new RegisterUserService();

        // STEP 1
        $moUser1 = $userCommon->checkIfFbUserExists($sFbid);

        $this->assertInternalType('boolean', $moUser1);
        $this->assertEquals( false, $moUser1, "STEP 1: User exists with facebook ID " . $sFbid);


        // STEP 2
        // case no changed email
        $moUser2 = $userCommon->checkIfChangedEmailExists($sEmail1, $sEmailFacebook);

        $this->assertInternalType('boolean', $moUser2, "STEP 2 (no changed e-mail)");
        $this->assertEquals( false, $moUser2, "STEP 2 (no changed e-mail): User exists with facebook ID " . $sFbid);

        // STEP 2
        // case changed email
        $moUser3 = $userCommon->checkIfChangedEmailExists($sEmail2, $sEmailFacebook);

        $this->assertInternalType('boolean', $moUser3, "STEP 2 (changed e-mail)");
        $this->assertEquals( false, $moUser3, "STEP 2 (changed e-mail): User exists with facebook ID " . $sFbid);


        // REGISTER
        // case no changed email
        $moUser4 = $rulerWs->registerUserFacebook($sEmail1, $sName, $sSurname, $sFbid, $sGender, $langEnv, $idCountry, $idPartner, $idSourceList, $device, $currentLevel);

        $this->assertFacebookUser($moUser4, $sFbid, $sEmail1, $sName, $sSurname, $sGender, $langEnv);

        // case changed email
        $moUser5 = $rulerWs->registerUserFacebook($sEmail2, $sName, $sSurname, $sFbid, $sGender, $langEnv, $idCountry, $idPartner, $idSourceList, $device, $currentLevel);

        $this->assertFacebookUser($moUser5, $sFbid, $sEmail2, $sName, $sSurname, $sGender, $langEnv);

        return true;
    }

    /** Tests web service. Check if CHANGED e-mail exists
     *
     * @test
     *
     * @depends testFunctionsAvailable
     */
    public function testRegisterUserExistingEmailFacebook()
    {
        $sFbid =            '1234567890';
        $sEmail1 =          'test.fabcebook.user.1@facebookuserabatest.com';
        $sEmail2 =          'test.fabcebook.user.2@facebookuserabatest.com';
        $sName =            'Ivan';
        $sSurname =         'Martinez';
        $sGender =          'female';
        $sEmailFacebook =   'test.fabcebook.user.2@facebookuserabatest.com';
        $langEnv =          HeMixed::getAutoLangDecision();
        $sPass =            '123456789';

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
        $createResponse = $this->createUsersIntoDb($sEmail1, FREE, Yii::app()->config->get("ABACountryId"), $sName, $sSurname, false, $langEnv, (trim($sGender) == 'female' ? 'F' : (trim($sGender) == 'male' ? 'M' : '')));
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

        $userCommon =   new UserCommon();
        $rulerWs =      new RegisterUserService();

        // STEP 1
        $moUser1 = $userCommon->checkIfFbUserExists($sFbid);

        $this->assertInternalType('boolean', $moUser1);
        $this->assertEquals( false, $moUser1, "STEP 1: User exists with facebook ID " . $sFbid);


        $abaUser =      new AbaUser();
        $userExists =   $abaUser->getUserByEmail($sEmail2);
        $this->assertInternalType('boolean', $userExists);
        $this->assertEquals( false, $userExists);

        // STEP 2
        // case changed email
        $moUser3 = $userCommon->checkIfChangedEmailExists($sEmail1, $sEmailFacebook);
        $this->assertInternalType('boolean', $moUser3, "STEP 2 (changed e-mail)");
        $this->assertEquals( true, $moUser3, "STEP 2 (changed e-mail): User exists with facebook ID " . $sFbid);

        // STEP 3
        // case changed email
        $moUser4 = new AbaUser();
        $moUser4->getUserByEmail($sEmail1, $sPass, true);

        $this->assertFacebookUser($moUser4, $sFbid, $sEmail1, $sName, $sSurname, $sGender, $langEnv, false);

        // STEP 3
        // case changed email
        // update any user data
        $moUser5 = $userCommon->changeUserFbData( $moUser4, $sName, $sSurname, $sFbid, $sGender);

        return true;
    }


    /** Tests web service of register user with level in a SIMPLE way, only connection and
     * creation of user
     * @test
     *
     * @depends testFunctionsAvailable
     */
    public function testRegisterUserLevel(SoapClient $client)
    {
        $iCurrentLevel =    null;
        $sEmail =           'unit.test.functional.free.level@yopmail.com';

        // Invalid Signature
        $signature = HeAbaSHA::_md5(WORD4WS . $sEmail . '123456789' . 'en' . 'FunctionalTest' . 'YiiPHP' . '199_180' . '' . 'HOME' . 199 . 300001 . 1 . NULL . $iCurrentLevel);

        /** @noinspection PhpUndefinedMethodInspection */
        $aResponse = $client->registerUser($signature . '123', $sEmail, '123456789', 'en', 'FunctionalTest', 'YiiPHP', '199_180', '', 'HOME', 199, 300001, 1, NULL, $iCurrentLevel);

        //It should equal to $aResponse = array("userId","result","redirectUrl")
        $this->assertArrayHasKey('userId', $aResponse, "Response web service returned: ".implode(array_keys($aResponse)));
        $this->assertArrayHasKey('result', $aResponse);
        $this->assertArrayHasKey('redirectUrl', $aResponse);

        $this->assertInternalType('integer', intval($aResponse["userId"]));
        $this->assertGreaterThanOrEqual(6000, intval($aResponse["userId"]));

        $isUrl = preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $aResponse['redirectUrl']);

        $this->assertGreaterThanOrEqual(1, $isUrl, "No valid URL returned for auto Login");

        if ($aResponse["userId"]) {
            $moUser = new AbaUser();
            if ($moUser->getUserById($aResponse["userId"])) {

                $this->assertEquals( FREE, $moUser->userType);
                $this->assertEquals( 199, $moUser->countryId);
                $this->assertEquals( 1, $moUser->idSourceList);
                $this->assertEquals( (is_numeric($iCurrentLevel) ? $iCurrentLevel : 1), $moUser->currentLevel, "No valid Level: " . $moUser->currentLevel);

                $moUser->deleteUserThroughResetCall();
            }
        }

        return true;
    }

    /** Tests web service of register user in a SIMPLE way, only connection and
     * creation of user
     * @test
     *
     * @depends testFunctionsAvailable
     */
    public function testRegisterUserObjectives(SoapClient $client)
    {
        $iCurrentLevel =    5;
        $sEmail =           'unit.test.functional.free.level@yopmail.com';

        $viajar = 1;
        $trabajar = 0;
        $estudiar = 0;
        $nivel = 1;
        $selEngPrev = 1;
        $genero = 'M';
        $dedicacion = 3;
        $engPrev = 0;
//        $day = '01';
//        $month = '01';
//        $year = '1901';
        $otros = 0;

        $aObjectives =   array(
            'viajar' => $viajar,
            'trabajar' => $trabajar,
            'estudiar' => $estudiar,
            'nivel' => $nivel,
            'selEngPrev' => $selEngPrev,
            'genero' => $genero,
            'dedicacion' => $dedicacion,
            'engPrev' => $engPrev,
//            'day' => $day,
//            'month' => $month,
//            'year' => $year,
            'otros' => $otros,
        );
        $sObjectives = json_encode($aObjectives);

        // Invalid Signature
        $signature = HeAbaSHA::_md5(WORD4WS . $sEmail . '123456789' . 'es' . 'FunctionalTest' . 'YiiPHP' . '199_180' . '' . 'HOME_LEVEL_UNIT' . 199 . 300001 . 1 . NULL . $iCurrentLevel . $sObjectives);

        /** @noinspection PhpUndefinedMethodInspection */
        $aResponse = $client->registerUserObjectives($signature . '123', $sEmail, '123456789', 'es', 'FunctionalTest', 'YiiPHP', '199_180', '', 'HOME_LEVEL_UNIT', 199, 300001, 1, NULL, $iCurrentLevel, $sObjectives);

        //It should equal to $aResponse = array("userId","result","redirectUrl","resultObjectives")
        $this->assertArrayHasKey('userId', $aResponse, "Response web service returned: ".implode(array_keys($aResponse)));
        $this->assertArrayHasKey('result', $aResponse);
        $this->assertArrayHasKey('redirectUrl', $aResponse);
        $this->assertArrayHasKey('resultObjectives', $aResponse);

        $this->assertInternalType('integer', intval($aResponse["userId"]));
        $this->assertGreaterThanOrEqual(6000, intval($aResponse["userId"]));

        $isUrl = preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $aResponse['redirectUrl']);

        $this->assertGreaterThanOrEqual(1, $isUrl, "No valid URL returned for auto Login");

        if ($aResponse["userId"]) {
            $moUser = new AbaUser();
            if ($moUser->getUserById($aResponse["userId"])) {

                $this->assertEquals( FREE, $moUser->userType);
                $this->assertEquals( 199, $moUser->countryId);
                $this->assertEquals( 1, $moUser->idSourceList);
                $this->assertEquals( (is_numeric($iCurrentLevel) ? $iCurrentLevel : 1), $moUser->currentLevel, "No valid Level: " . $moUser->currentLevel);

                $moUserObjective = new AbaUserObjective();
                if($moUserObjective->getObjectivesByUserId($moUser->getId())) {

                    $this->assertEquals( $viajar, $moUserObjective->travel);
                    $this->assertEquals( $trabajar, $moUserObjective->work);
                    $this->assertEquals( $estudiar, $moUserObjective->estudy);
                    $this->assertEquals( $nivel, $moUserObjective->level);
                    $this->assertEquals( $selEngPrev, $moUserObjective->selEngPrev);
                    $this->assertEquals( $genero, $moUser->gender);
                    $this->assertEquals( $dedicacion, $moUserObjective->dedication);
                    $this->assertEquals( $engPrev, $moUserObjective->engPrev);
                    $this->assertEquals( $otros, $moUserObjective->others);
                }

                $moUser->deleteUserThroughResetCall();
            }
        }

        return true;
    }





    /** Tests web service of register user in a simple way.
     * @test
     *
     * @depends testFunctionsAvailable
     */
    public function testRegisterTeacher(SoapClient $client)
    {
        ini_set("soap.wsdl_cache_enabled",  0);
        ini_set("default_socket_timeout",   600);

        $idPartner = 7002;
        $iNumUsers = 12; // total users

        $sEmailPrefix = 'unit.functional.premium.extranet';
        $sEmailSuffix = 'yopmail.com';

        //
        // Create "existing" users
        //
        // PREMIUM
        $userIdPremium = $this->createUsersIntoDb("unit.functional.premium.extranet.ex@yopmail.com", PREMIUM, 199, "UnitPremium", "Testing");

        // FREE
        $userIdFree = $this->createUsersIntoDb("unit.functional.free.extranet.ex@yopmail.com", FREE, 199, "UnitFree", "Testing");

        // FREE that was PREMIUM in the PAST
        $userIdExPremium = $this->createUsersExPremium("unit.functional.ex.premium.extranet.ex@yopmail.com", FREE, 199, "UnitExPremium", "Testing");

        // PREMIUM CANCEL
        $userIdPremiumCancel = $this->createUsersPremiumCancel("unit.functional.premium.cancelled.extranet.ex@yopmail.com", PREMIUM, 199, "UnitPremium", "Testing Cancelled");

        // DELETED: unit.test.lead.user@yopmail.com
        $userIdDeleted = $this->createUsersIntoDb("unit.functional.deleted.extranet.ex@yopmail.com", DELETED, 199, "UnitLead", "Testing");

        //
        $aUsersExtranet = $this->unitTestExtranetData($idPartner);

        foreach ($aUsersExtranet as $iKey => $aUser) {
            $aUsers[] = $aUser;
        }

        //
        // Periodicity
        //
        for($iCounter = 1; $iCounter <= $iNumUsers; $iCounter++) {

            $sUserTmpl =            $aUsersExtranet[0];
            $sUserTmpl['email'] =   $sEmailPrefix . $iCounter . '@' . $sEmailSuffix;

//            if($iCounter < 0) { $sUserTmpl['codeDeal'] = 'EXTRANET30'; $sUserTmpl['periodId'] = '1'; }
//        elseif($iCounter < 30) { $sUserTmpl['codeDeal'] = 'EXTRANET180'; $sUserTmpl['periodId'] = '6'; }
//        elseif($iCounter < 50) { $sUserTmpl['codeDeal'] = 'EXTRANET540'; $sUserTmpl['periodId'] = '18'; }
            if($iCounter < 3) { $sUserTmpl['codeDeal'] = ''; $sUserTmpl['periodId'] = '3'; }  // EXTRANET90
            elseif($iCounter < 6) { $sUserTmpl['codeDeal'] = ''; $sUserTmpl['periodId'] = '12'; }  // EXTRANET360
            elseif($iCounter < 9) { $sUserTmpl['codeDeal'] = ''; $sUserTmpl['periodId'] = '24'; }  // EXTRANET720
            else{ $sUserTmpl['codeDeal'] = 'EXTRANET180'; $sUserTmpl['periodId'] = '3'; }

            $aUsers[] = $sUserTmpl;
        }

        $sUsers = json_encode($aUsers);

        $signatureWsAbaEnglish = md5('Aba English Extranet Key' . $sUsers);

        $aResponse = $client->registerUserPremiumB2bExtranet( $signatureWsAbaEnglish, $sUsers );

        foreach($aResponse as $iKey => $aUserResponse) {

            $this->assertArrayHasKey('email', $aUserResponse);
            $this->assertArrayHasKey('response', $aUserResponse);

            $email =    $aUserResponse['email'];
            $response = $aUserResponse['response'];

            $this->assertArrayHasKey('userId', $response, "Response web service returned: " . implode(array_keys($response)));
            $this->assertArrayHasKey('result', $response);

            $this->assertInternalType('integer', intval($response["userId"]));
            $this->assertGreaterThanOrEqual(6000, intval($response["userId"]));

            if ($response["userId"]) {

                $moUser = new AbaUser();

                if ($moUser->getUserById($response["userId"])) {

                    $this->assertEquals( PREMIUM, $moUser->userType);
                    $this->assertEquals( $idPartner, $moUser->idPartnerCurrent);
                    $this->assertEquals( 199, $moUser->countryId);
//                    $this->assertEquals( DEVICE_SOURCE_EXTRANET, $moUser->deviceTypeSource, 'The device is not registered: ' . $moUser->id);

                    $moUser->deleteUserThroughResetCall();
                }
            }
        }

        return true;
    }







}
