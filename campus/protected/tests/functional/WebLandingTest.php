<?php
/**
 * Tests the landing web page, the registration of word press based web.
 */
class WebLandingTest extends AbaWebTestCase
{
    const TEST_WEB_WORDPRESS_URL ='http://www.dev.abaenglish.com/';

    public function setUp()
    {
        parent::setUp();

        $this->setBrowserUrl(TEST_BASE_URL);
    }

    public function tearDown()
    {
        $this->resetDownUser("unit.test.registration.web.en@yopmail.com");
        $this->resetDownUser("unit.test.registration.web.pt@yopmail.com");
        $this->resetDownUser("unit.test.registration.web.it@yopmail.com");
        parent::tearDown();
    }

//
//    /** Connects to the web, goes to the landing page, registers a new user and redirects to the Campus.
//     * @dataProvider dataGenericSimpleRegistrationTest
//     */
//    public function testRegistrationSimple($langEnv, $email)
//    {
//        $this->open(self::TEST_WEB_WORDPRESS_URL.$langEnv.'/');
//        $this->waitForPageToLoad("20000");
//
//        switch($langEnv){
//            case 'pt':
//                $this->click("link=Comece gratis");
//                break;
//            case 'en':
//                $this->click("link=Start now for free");
//                break;
//            case 'it':
//                $this->click("link=Comincia gratis");
//                break;
//        }
//
//        $this->waitForPageToLoad("30000");
//        $this->type("id=name", "UnitTest");
//        $this->type("id=surname", "User ".$langEnv." Registration");
//        $this->type("id=email", $email);
//        $this->type("id=pass", "123456789");
//
//        $this->click("id=sendSuscriptionButton");
//
//        $this->waitForPageToLoad("30000");
//
//        $moUser = new AbaUser();
//        $this->assertTrue( $moUser->getUserByEmail($email, "", false) );
//        $this->assertEquals(HeDate::todaySQL(), HeDate::europeanToSQL($moUser->entryDate));
//
//        // Now we are redirected to the Campus:
//        $this->assertEquals(Yii::t('mainApp', '¡Hola',  array(),NULL, $langEnv)." ".$moUser->name."!",
//                                                                $this->getText("css=span.saludo"));
//        $this->click("css=span.ui-icon.ui-icon-closethick");
//
//        $this->assertHomePage($moUser->userType, $langEnv);
//
//        if($langEnv=='pt'){
//        $this->assertEquals("Verá um curta-metragem de uma situação actual e da vida real.",
//                                                               $this->getText("id=secInfo_subTitle"));
//        }
//
//        $this->assertTrue($this->isElementPresent("id=divBtnGoPremium"));
//        $this->click("css=span.cmp_pasateaBold");
//
//        $this->logoutUserByEmail($email);
//
//        $moUser = new AbaUser();
//        if($moUser->getUserByEmail($email, "", false)){
//            $moUser->deleteUserThroughResetCall();
//        }
//    }


    /** Returns users that are created temporary in database
     * for testing purposes. To be used in any of the Web functional tests
     * @return array
     */
    public function dataGenericSimpleRegistrationTest()
    {
        return array(
            'PT user'=>array( 'pt','unit.test.registration.web.pt@yopmail.com' ),
            'EN user'=>array( 'en','unit.test.registration.web.en@yopmail.com' ),
            'IT user'=>array( 'it','unit.test.registration.web.it@yopmail.com' ),
        );
    }


    /** Returns users that are created temporary in database
     * for testing purposes. To be used in any of the Web functional tests
     * @return array
     */
    public function dataGenericRegistrationWithObjectivesTest()
    {
        return array(
            'ES user'=>array( 'es','unit.test.registration.web.es@yopmail.com' ),
        );
    }


    /** Connects to the web, goes to the landing page, registers a new user and redirects to the Campus.
     * @dataProvider dataGenericSimpleRegistrationTest
     */
    public function testRegistrationSimple($langEnv, $email)
    {
        $this->open(self::TEST_WEB_WORDPRESS_URL.$langEnv.'/');
        $this->waitForPageToLoad("20000");

        switch($langEnv){
            case 'pt':
                $this->click("link=Comece gratis");
                break;
            case 'en':
                $this->click("link=Start now for free");
                break;
            case 'it':
                $this->click("link=Comincia gratis");
                break;
        }

        $this->waitForPageToLoad("20000");
        $this->type("id=name", "UnitTest");
        $this->type("id=surname", "User ".$langEnv." Registration");
        $this->type("id=email", $email);
        $this->type("id=pass", "123456789");

        $this->click("id=sendSuscriptionButton");

        $this->waitForPageToLoad("30000");

        $moUser = new AbaUser();
        $this->assertTrue( $moUser->getUserByEmail($email, "", false) );
        $this->assertEquals(HeDate::todaySQL(), HeDate::europeanToSQL($moUser->entryDate));

        // Now we are redirected to the Campus:
        $this->selectLevelFromPopup(3);
        $this->assertHomePage($moUser->userType, $moUser->langEnv);

        $this->assertTextPresent( $moUser->name.'!');
        $this->assertTextPresent(Yii::t('mainApp', 'key_sectionGreetingUser_0',  array(),NULL, $langEnv));

        if($langEnv=='pt'){
            $this->assertEquals("Verá um curta-metragem de uma situação actual e da vida real.",
                $this->getText("id=secInfo_subTitle"));
        }

        $this->assertTrue($this->isElementPresent("id=divBtnGoPremium"));
        // We click on Go To Premium banner
        $this->clickAndWait("id=divBtnGoPremium");

        // Assert at least 4 products available:
        $this->assertProductSelectionPage( $moUser->countryId );

        $this->goHomePage();
//        $this->selectLevelFromPopup(3);
        $this->logoutUserByEmail($email);

        $moUser = new AbaUser();
        if($moUser->getUserByEmail($email, "", false)){
            $moUser->deleteUserThroughResetCall();
        }
    }

    /** Connects to the web, goes to the landing page, registers a new user and redirects to the Campus.
     * @dataProvider dataGenericRegistrationWithObjectivesTest
     */
    public function testRegistrationWithObjectives($langEnv, $email)
    {
        $this->open(self::TEST_WEB_WORDPRESS_URL.$langEnv.'/cuestionario-personalizacion-curso-ingles/');
        $this->waitForPageToLoad("20000");


//$this->assertClassHasStaticAttribute("selected", "Paso1Form_trabajar_check");


        $this->assertElementPresent("id=Paso1Form_trabajar_check");

        $this->click("id=Paso1Form_trabajar_check");
//        $this->type("id=Paso1Form_trabajar", "1");
        $this->click("id=next-button");

//        $this->assertEquals( "on", $this->getValue("id=Paso1Form_trabajar"));
////        $this->classHasAttribute("Paso1Form_trabajar_check=selected");
//        $this->assertClassHasAttribute("selected", "Paso1Form_trabajar_check");

        $this->assertElementPresent("id=Paso1Form_nivel_0_check");

        $this->click("id=Paso1Form_nivel_0_check");
//        $this->type("id=Paso1Form_nivel_0", "1");
        $this->click("id=next-button");

        $this->assertElementPresent("id=Paso1Form_engPrev_1_check");
        $this->click("id=Paso1Form_engPrev_1_check");
        $this->click("id=next-button");

        $this->assertElementPresent("id=Paso1Form_dedicacion_check");
        $this->click("id=Paso1Form_dedicacion_check");
        $this->click("id=next-button");

        $this->assertElementPresent("id=5-nivel-de-partida_0_check");
        $this->click("id=5-nivel-de-partida_0_check");
        $this->click("id=next-button");


//        $this->click("link=Entra en tu Campus");

//        $this->waitForElementPresent("id=menu-item-90");
//        $this->assertElementPresent("id=menu-item-90");



//        $this->waitForPageToLoad("30000");

//        $this->assertEquals(  Yii::t('mainApp', '¡Enhorabuena!',array(),NULL,'es'),
//        $this->getText("id=pIdCongratulations"));
//        $this->assertText('css=div.oldPrice', '14,99 \€');
//        $this->assertElementPresent("id=idDivBtnStartPremium");


//        // Click on Start Now!
//        $this->clickAndWait("id=idDivBtnStartPremium");
//        //Check the layout Premium:
//        $this->assertElementNotPresent('class=cmp_haztePremium');



//
////        $this->waitForPageToLoad("30000");
////
////        $this->type("id=name", "UnitTest");
////        $this->type("id=email", $email);
////        $this->type("id=pass", "123456789");
////
//////        sendSuscriptionButton
//////        $this->check();
////
//////        $this->click("id=sendSuscriptionButton");
////
////        $this->waitForPageToLoad("30000");
//
//        $this->waitForPageToLoad("5000");
//
//        $this->click("link=Entra en tu Campus");


//
//
//        switch($langEnv){
//            case 'pt':
//                $this->click("link=Comece gratis");
//                break;
//            case 'en':
//                $this->click("link=Start now for free");
//                break;
//            case 'it':
//                $this->click("link=Comincia gratis");
//                break;
//        }
//
//        $this->waitForPageToLoad("30000");
//        $this->type("id=name", "UnitTest");
//        $this->type("id=surname", "User ".$langEnv." Registration");
//        $this->type("id=email", $email);
//        $this->type("id=pass", "123456789");
//
//        $this->click("id=sendSuscriptionButton");
//
//        $this->waitForPageToLoad("30000");
//
//        $moUser = new AbaUser();
//        $this->assertTrue( $moUser->getUserByEmail($email, "", false) );
//        $this->assertEquals(HeDate::todaySQL(), HeDate::europeanToSQL($moUser->entryDate));
//
//        // Now we are redirected to the Campus:
//        $this->assertEquals(Yii::t('mainApp', '¡Hola',  array(),NULL, $langEnv)." ".$moUser->name."!",
//            $this->getText("css=span.saludo"));
//        $this->click("css=span.ui-icon.ui-icon-closethick");
//
//        $this->assertHomePage($moUser->userType, $langEnv);
//
//        if($langEnv=='pt'){
//            $this->assertEquals("Verá um curta-metragem de uma situação actual e da vida real.",
//                $this->getText("id=secInfo_subTitle"));
//        }
//
//        $this->assertTrue($this->isElementPresent("id=divBtnGoPremium"));
//        $this->click("css=span.cmp_pasateaBold");
//
//        $this->logoutUserByEmail($email);
//
//        $moUser = new AbaUser();
//        if($moUser->getUserByEmail($email, "", false)){
//            $moUser->deleteUserThroughResetCall();
//        }
    }

}
