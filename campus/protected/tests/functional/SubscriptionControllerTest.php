<?php

/**
 * To be able to run tests we should start the Selenium Server:
 * C:\Program Files (x86)\Java\jre7\bin>java.exe -jar C:\wamp\bin\selenium\selenium-server-standalone-2.31.0.jar
 */
class SubscriptionControllerTest extends AbaWebTestCase
{
    /**
     * Tests the action of typing and saving my personal details.
     * @dataProvider dataUsersCase
     *
     * @param string $email
     *
     * @return bool
     */
    public function testSubscriptionIndex($email)
    {
        if (!$this->loginUserByEmail($email, true) && "unit.test.deleted.user@yopmail.com" == $email) {
            $this->assertTrue(true);
            return true;
        }

        $moUser = new AbaUser();
        $moUser->getUserByEmail($email);

        $commRecurrent = new RecurrentPayCommon();
        $typeSubscriptionStatus = $commRecurrent->getTypeSubscriptionStatus($moUser);
        if (RecurrentPayCommon::isDisplaySubscription($typeSubscriptionStatus)) {
            $this->clickAndWait("id=headLinkMenuSubsc");
            $this->assertTrue($this->assertSubscriptionPage($moUser));
        } else{
            $this->assertElementNotPresent('id=headLinkMenuSubsc');
        }

        return true;
    }

    /** Asserts that user that has cancel can not change his credit card.
     *
     * @return bool
     */
    public function testSubscriptionNotChange()
    {
        $email = DataCollections::$aUsersShowCase["PREMIUM_CANCEL_CAIXA"][0];
        $moUser = new AbaUser();
        $moUser->getUserByEmail($email);

        $this->loginUserByEmail($email, true);
        $this->clickAndWait("id=headLinkMenuSubsc");
        $this->assertElementPresent("id=idAChangeCreditCard");

        $this->clickAndWait("id=idAChangeCreditCard");

        $this->assertTextPresent( Yii::t('mainApp', 'key_notallowedchangecreditnotpendingpay', array(), null, $moUser->langEnv) );

        return true;
    }

    /** Asserts that premium user that can change his credit card and replace the current one.
     * @dataProvider dataCardsSamples
     *
     * @param $successExpected
     * @param $creditCardType
     * @param $creditCardNumber
     * @param $creditCardYear
     * @param $creditCardMonth
     * @param $CVC
     * @param $creditCardName
     * @param $cpfBrasil
     * @param $typeCpf
     *
     * @return bool
     */
    public function testSubscriptionChange( $successExpected, $creditCardType, $creditCardNumber, $creditCardYear,
                                            $creditCardMonth, $CVC, $creditCardName, $cpfBrasil, $typeCpf)
    {
        // We skip PAYPAL and Discover because it is too difficult to check.
        if($creditCardType==KIND_CC_PAYPAL || $creditCardType==KIND_CC_DISCOVER){
            $this->assertTrue(true);
            return true;
        }

        $email = DataCollections::$aUsersShowCase["PREMIUM_ACTIVE_CAIXA"][0];
        $moUser = new AbaUser();
        $moUser->getUserByEmail($email);

        $moDefaultCard = new AbaUserCreditForms($moUser->id);

        $this->assertNotEquals($creditCardNumber, $moDefaultCard->cardNumber);
        $this->assertNotEquals(HeDate::todaySQL(false), $moDefaultCard->lastModified);

        $this->loginUserByEmail($email, true);

        $this->clickAndWait("id=headLinkMenuSubsc");
        $this->assertElementPresent("id=idAChangeCreditCard");

        // Check Form is displayed:
        $this->clickAndWait("id=idAChangeCreditCard");

        $this->assertTrue($this->assertChangeCreditFormActive($moUser));

        // Input all data for credit card:
        $this->select('id=CreditCardForm_creditCardType', 'value=' .strval($creditCardType));
        $this->type("id=CreditCardForm_creditCardName", $creditCardName);
        $this->type("id=CreditCardForm_creditCardNumber", $creditCardNumber);
        $this->select('id=CreditCardForm_creditCardMonth', 'index=' . $creditCardMonth);
        $this->select('id=CreditCardForm_creditCardYear', 'label=' . $creditCardYear);
        $this->type("id=CreditCardForm_CVC", $CVC);

        $this->clickAndWait("id=btnSaveNewCreditCard");

        if ( !$successExpected ){
            $this->assertTextPresent(Yii::t('mainApp', 'errorsavepayment_key', array(), null, $moUser->langEnv));
            $this->assertTrue($this->assertChangeCreditFormActive($moUser));
        } else {
            // Success on change credit card:
            $this->assertTrue($this->assertSubscriptionPage($moUser));
            $this->assertTextPresent(Yii::t('mainApp', 'Se ha guardado correctamente', array(), null, $moUser->langEnv));

            $moDefaultCard->getUserCreditFormByUserId($moUser->id);

            $this->assertEquals($creditCardNumber, $moDefaultCard->cardNumber);
            $this->assertEquals($creditCardMonth, $moDefaultCard->cardMonth);
            $this->assertEquals(HeDate::todaySQL(false), HeDate::removeTimeFromSQL($moDefaultCard->lastModified) );
        }


        return true;
    }

    /** Asserts that Subscription page and tab are being displayed and active
     * @param AbaUser $moUser
     * @return bool
     */
    protected function assertSubscriptionPage(AbaUser $moUser)
    {
        $recurrentComm = new RecurrentPayCommon();
        $subscType = $recurrentComm->getTypeSubscriptionStatus($moUser);
        if ($subscType==SUBS_PREMIUM_THROUGHPARTNER){
            $this->assertElementNotPresent('id=aTabSubscription');
            return true;
        }

        $this->assertElementPresent("id=aTabSubscription");
        if($subscType==SUBS_EXPREMIUM_CARD || $subscType==SUBS_EXPREMIUM_PAYPAL){
            $this->assertElementPresent('id=divGoActivatePremium');
        }elseif ($moUser->userType <= FREE) {
            $this->assertElementNotPresent('id=idAChangeCreditCard');
            $this->assertElementPresent('id=divGoActivatePremium');
            $this->assertTextPresent(strip_tags(Yii::t('mainApp', 'free1', array(), null, $moUser->langEnv)));
        }elseif($subscType==SUBS_PREMIUM_C_PAYPAL || $subscType==SUBS_PREMIUM_C_CARD_C ||
            $subscType==SUBS_PREMIUM_C_CARD_A){
            $this->assertElementPresent('id=idAChangeCreditCard');
        } else{
            $this->assertElementNotPresent('id=divGoActivatePremium');
            if ($subscType==SUBS_PREMIUM_A_PAYPAL){
                $this->assertElementPresent('class=iconPaypal');
            }
            $this->assertElementPresent("id=idAChangeCreditCard");
        }

        return true;
    }

    /** Asserts that Subscription page and particularly the Credit Card Form is are being displayed and active
     *
     * @param AbaUser $moUser
     * @return bool
     */
    protected function assertChangeCreditFormActive(AbaUser $moUser)
    {
        $this->assertElementPresent("id=changecredit-form");
        $this->assertElementPresent("id=CreditCardForm_creditCardName");
        $this->assertElementPresent("id=CreditCardForm_creditCardType");

        $this->assertElementPresent("id=CreditCardForm_creditCardType");
        $this->assertElementPresent("id=CreditCardForm_creditCardType");
        $this->assertElementPresent("id=CreditCardForm_creditCardName");

        if ( $moUser->countryId==COUNTRY_BRAZIL) {
            $this->assertElementPresent("id=CreditCardForm_cpfBrasil");
            $this->assertElementPresent("id=CreditCardForm_typeCpf");
        }


        return true;
    }
}