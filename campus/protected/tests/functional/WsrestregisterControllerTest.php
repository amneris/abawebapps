<?php
/**
 * To test web services REST that are called from mobile devices.
 * User: Quino
 */
class WsrestregisterControllerTest extends WsrestControllerTest
{
    protected $urlWs;
    protected static $emailUserTest = "unit.test.mobile.free@yopmail.com";
    /**
     * Override to set URL public ws
     */
    public function setUp()
    {
        parent::setUp();
        $this->urlWs = "http://" . Yii::app()->config->get("SELF_DOMAIN") . "/wsrestregister";
    }

    public function tearDown()
    {
        parent::tearDown();
        /*$moUser = new AbaUser();
        if ($moUser->getUserByEmail(self::$emailUserTest)){
            $this->resetDownUser(self::$emailUserTest);
        }*/
    }


    /**
     * @test
     *
     * @return bool
     */
    public function testIpRecognition()
    {
        $opts = array(
            'http' => array(
                'method'=> "GET",
                'header'=> "Content-Type: text/xml; charset=utf-8"
            )
        );
        $context = stream_context_create($opts);
        $response = file_get_contents($this->urlWs."/view/object/ipRecognition", false, $context);
        $aResponse = json_decode($response);

        $this->assertTrue( property_exists($aResponse,"publicIp") );
        $this->assertTrue( (ip2long($aResponse->publicIp)!== false) );
        $this->assertTrue(("192.168.56.1"==$aResponse->publicIp || "127.0.0.1"==$aResponse->publicIp));

        return true;
    }

    /** Checks if the Version Rest Web Service function is public and available..
     * http://wcampus.local/wsrestregister/view/object/version
     * @test
     *
     * @return Boolean
     */
    public function testVersionAvailable()
    {

        $opts = array(
            'https' => array(
                'method'=> "GET",
                'header'=> "Content-Type: text/xml; charset=utf-8"
            )
        );
        $context = stream_context_create($opts);
        $response = file_get_contents($this->urlWs."/view/object/version", false, $context);
        $aResponse = json_decode($response);

        $this->assertTrue( property_exists($aResponse,"versionNumber") );
        $this->assertEquals( '2.5', $aResponse->versionNumber );

        $this->assertTrue( property_exists($aResponse,"history") );


        return true;
    }

    /** Checks registration through REST services, but in a negative way.
     * Asserts that validations and consistency rules are working and are not permissive with bad attempts.
     *
     * http://wcampus.local//wsrestregister/register
     * @test
     *
     * @return Boolean
     */
    public function testActionRegisterError()
    {
        $aFieldsToBeSent = array();
        //array($deviceId, $sysOper, $deviceName, $email, $password,$langEnv, $name, $surnames, $ipMobile)
        $aFieldsToBeSent["deviceId"]= "FG446-5858-464123-GGGBT";
        $aFieldsToBeSent["sysOper"]= "IPAD OS";
        $aFieldsToBeSent["deviceName"]= "IPad Mini Retina";
        $aFieldsToBeSent["email"]= self::$emailUserTest;
        $aFieldsToBeSent["password"]= "";
        $aFieldsToBeSent["langEnv"]= "";
        $aFieldsToBeSent["name"]= "Unit";
        $aFieldsToBeSent["surnames"]= "Test Mobile";
        $aFieldsToBeSent["ipMobile"]= "174.45.12.65"; // Corresponds to country 226
        $aFieldsToBeSent["signature"]= md5(WORD4MOBILEWS.$aFieldsToBeSent["deviceId"].
                                                        $aFieldsToBeSent["sysOper"].
                                                        $aFieldsToBeSent["deviceName"].
                                                        $aFieldsToBeSent["email"].
                                                        $aFieldsToBeSent["password"].
                                                        $aFieldsToBeSent["langEnv"].
                                                        $aFieldsToBeSent["name"].
                                                        $aFieldsToBeSent["surnames"].
                                                        $aFieldsToBeSent["ipMobile"]);

        $response = HeComm::sendHTTPPostCurl($this->urlWs."/register", null, $aFieldsToBeSent);
        $aResponse = json_decode($response);

        $this->assertTrue( property_exists($aResponse,"httpCode") );
        $this->assertTrue( property_exists($aResponse,"message") );
        $this->assertEquals( 500, intval($aResponse->httpCode) );
        $this->assertEquals( "exception", strtolower($aResponse->status) );

        return true;
    }


    /** Checks registration through REST.
     * http://wcampus.local//wsrestregister/register
     * @test
     *
     * @return Boolean
     */
    public function testActionRegister()
    {
        $aFieldsToBeSent = array();
        //array($deviceId, $sysOper, $deviceName, $email, $password,$langEnv, $name, $surnames, $ipMobile)
        $aFieldsToBeSent["deviceId"]= "FG446-5858-464123-GGGBT";
        $aFieldsToBeSent["sysOper"]= "IPAD OS";
        $aFieldsToBeSent["deviceName"]= "IPad Mini Retina";
        $aFieldsToBeSent["email"]= self::$emailUserTest;
        $aFieldsToBeSent["password"]= "123456789";
        $aFieldsToBeSent["langEnv"]= "fr";
        $aFieldsToBeSent["name"]= "Unit";
        $aFieldsToBeSent["surnames"]= " Test Mobile";
        $ipUser = "174.45.12.65";
        $aFieldsToBeSent["ipMobile"]= $ipUser; // Corresponds to country 226
        $aFieldsToBeSent["idPartner"]= PARTNER_ID_MOBILE;
        $aFieldsToBeSent["idSourceList"]= "66";
        $aFieldsToBeSent["deviceTypeSource"]= DEVICE_SOURCE_TABLET;
        $aFieldsToBeSent["signature"]= md5(WORD4MOBILEWS.$aFieldsToBeSent["deviceId"].
                                                            $aFieldsToBeSent["sysOper"].
                                                            $aFieldsToBeSent["deviceName"].
                                                            $aFieldsToBeSent["email"].
                                                            $aFieldsToBeSent["password"].
                                                            $aFieldsToBeSent["langEnv"].
                                                            $aFieldsToBeSent["name"].
                                                            $aFieldsToBeSent["surnames"].
                                                            $aFieldsToBeSent["ipMobile"].
                                                            $aFieldsToBeSent["idPartner"].
                                                            $aFieldsToBeSent["idSourceList"].
                                                            $aFieldsToBeSent["deviceTypeSource"]);

        $response = HeComm::sendHTTPPostCurl($this->urlWs."/register", null, $aFieldsToBeSent);
        $aResponse = json_decode($response);

        $this->assertTrue( property_exists($aResponse,"userId") );
        $this->assertTrue( property_exists($aResponse,"userType") );
        $this->assertTrue( property_exists($aResponse,"userLevel") );
        $this->assertTrue( property_exists($aResponse,"token") );
        $this->assertTrue( property_exists($aResponse,"countryNameIso") );

        $this->assertEquals(1,intval($aResponse->userType));
        $this->assertEquals("US", $aResponse->countryNameIso);

        if (property_exists($aResponse,"userId")) {
            $moUser = new AbaUser();
            if ($moUser->getUserById($aResponse->userId)) {
                $this->assertEquals( FREE, $moUser->userType);
                $this->assertEquals( 226, $moUser->countryId);
                $this->assertEquals( PARTNER_ID_MOBILE, intval($moUser->idPartnerSource));
                $this->assertEquals( 66, intval($moUser->idSourceList));
                $this->assertEquals( DEVICE_SOURCE_TABLET, $moUser->deviceTypeSource, 'Device source should Tablet');
                $moUserLoc = new AbaUserLocation();
                $moUserLoc->getLocationByIdUserIdIp($moUser->id, $ipUser);
                $this->assertEquals("Gillette", $moUserLoc->city);
                $this->assertEquals("US", $moUserLoc->isoCountryCode);
            }

        } else {
            $this->assertTrue(FALSE, 'It should return an user Id, not user created.');
        }

        return $aResponse;
    }


    /** Login with POST fields instead of with a TOKEN
     *
     * @depends testActionRegister
     * @test
     * @param $aResponseRegister
     *
     * @return mixed
     */
    public function testActionLoginWithFields($aResponseRegister)
    {
        $token = $aResponseRegister->token;
        $aFieldsToBeSent = array();
        //array($deviceId, $sysOper, $deviceName, $email, $password,$langEnv, $name, $surnames, $ipMobile)
        $aFieldsToBeSent["deviceId"]= "FG446-5858-464123-GGGBT";
        $aFieldsToBeSent["sysOper"]= "IPAD OS";
        $aFieldsToBeSent["deviceName"]= "IPad Mini Retina";
        $aFieldsToBeSent["email"]= self::$emailUserTest;
        $aFieldsToBeSent["password"]= "123456789";
        $aFieldsToBeSent["signature"]= md5(WORD4MOBILEWS.$aFieldsToBeSent["deviceId"].
                                                        $aFieldsToBeSent["sysOper"].
                                                        $aFieldsToBeSent["deviceName"].
                                                        $aFieldsToBeSent["email"].
                                                        $aFieldsToBeSent["password"]);

        $varsPost = json_encode($aFieldsToBeSent);
        $response = HeComm::sendHTTPPostCurl($this->urlWs."/login", null, $aFieldsToBeSent);
        $aResponse = json_decode($response);

        $this->assertTrue( property_exists($aResponse,"token") );
        $this->assertTrue( property_exists($aResponse,"userId") );
        $this->assertTrue( property_exists($aResponse,"name") );
        $this->assertTrue( property_exists($aResponse,"surnames") );
        $this->assertTrue( property_exists($aResponse,"countryNameIso") );
        $this->assertEquals("US", $aResponse->countryNameIso);


        if (property_exists($aResponse,"userId")) {
            $moUser = new AbaUser();
            if ($moUser->getUserById($aResponse->userId)) {
                $this->assertEquals( FREE, $moUser->userType,
                                        'User should be FREE');
                $this->assertEquals( 226, $moUser->countryId,
                                        'Country is no the one expected for user '.self::$emailUserTest);
            }

        }

        return $aResponse;
    }

    /**
     * test
     *
     * @depends testActionRegister
     *
     * @param $aResponseRegister
     *
     * @return bool
     */
    public function testActionLogin($aResponseRegister)
    {
        $response = HeComm::sendHTTPPostCurl($this->urlWs."/login",
            array('PHP_AUTH_TOKEN: '.$aResponseRegister->token.''), null);

        $aResponse = json_decode($response);

        $this->assertTrue( property_exists($aResponse,"token") );
        $this->assertTrue( property_exists($aResponse,"userId") );
        $this->assertTrue( property_exists($aResponse,"name") );
        $this->assertTrue( property_exists($aResponse,"surnames") );
        $this->assertTrue( property_exists($aResponse,"countryNameIso") );

        if (property_exists($aResponse,"userId")) {
            $moUser = new AbaUser();
            if ($moUser->getUserById($aResponse->userId)) {
                $this->assertEquals( self::$emailUserTest, $moUser->email);
                $moUser->deleteUserThroughResetCall();
            }

        }

        return true;
    }

    /**
     * @depends testActionRegister
     *
     * @test WsrestregisterController::ActionRecoverPassword
     *
     * @return bool
     */
    public function testActionRecoverPassword()
    {
        $moUser = new AbaUser();
        $userId = $this->createUsersIntoDb(self::$emailUserTest, PREMIUM, 199, 'Unit', 'Test Mobile');
        $moUser->getUserById($userId);
        $this->assertEquals( self::$emailUserTest, $moUser->email);
        $oldPwd = $moUser->password;
        $oldKeyExternalLogin = $moUser->keyExternalLogin;

        $aFieldsToBeSent["email"]= self::$emailUserTest;
        $aFieldsToBeSent["langEnv"]= "fr";
        $aFieldsToBeSent["signature"]= md5(WORD4MOBILEWS.$aFieldsToBeSent["email"].$aFieldsToBeSent["langEnv"]);


        $response = HeComm::sendHTTPPostCurl($this->urlWs."/recoverpassword",null, $aFieldsToBeSent);
        $aResponse = json_decode($response);

        $this->assertTrue( property_exists($aResponse,"status") );
        $this->assertEquals( 'success', $aResponse->status );


        $moUser->refresh();

        $this->assertNotEquals( $oldPwd, $moUser->password, 'Password should have changed');
        $this->assertNotEquals( $oldKeyExternalLogin, $moUser->keyExternalLogin, 'External Login should have changed');

        $moUser->deleteUserThroughResetCall();

        return true;
    }

}
