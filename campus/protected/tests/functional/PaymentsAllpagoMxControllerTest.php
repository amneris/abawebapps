<?php

/**
 * To be able to run tests we should start the Selenium Server.
 */
class PaymentsAllpagoMxControllerTest extends PaymentsControllerTestBase
{
    /**
     * Creates all test users and any other test data used specific from this test.
     */
    public function setUp()
    {
        $aUsersToCreate = $this->dataMexicoUsersTest();
        foreach($aUsersToCreate as $userCols){
            $this->resetDownUser($userCols[0]);
            $this->createUsersIntoDb($userCols[0], FREE, $userCols[1], $userCols[2], $userCols[3], false, $userCols[4]);
        }

        parent::setUp();
    }

    /**
     * Destroy all test users and any other test data used specific from this test.
     */
    public function tearDown()
    {
        $aUsersToDelete = $this->dataMexicoUsersTest();
        foreach($aUsersToDelete as $userCols){
            $this->resetDownUser($userCols[0]);
        }

        parent::tearDown();
    }

    /**
     * Tests The payment method page and it process a payment.
     * @dataProvider dataCombinations
     */
    public function testPaymentMethodTPVAndBuy($email, $countryId, $name, $lastName, $langEnv,
                                              $street, $zipCode, $city, $state,
                                               $creditType, $cardNumber, $cardMonth, $cardYear, $cardCvc,
                                               $cAccepted, $ipLocation )
    {
        if ( $this->selectPaymentProductPrice($email, 12, '?IP_FALSE_TEST='.$ipLocation) ) {

            $this->assertElementPresent('id=PaymentForm_street');
            $this->assertElementPresent('id=PaymentForm_zipCode');
            $this->assertElementPresent('id=PaymentForm_city');
            $this->assertElementPresent('id=PaymentForm_state');

//            $this->select('id=PaymentForm_creditCardType', 'value=' .strval(KIND_CC_VISA));
            $this->type('id=PaymentForm_creditCardType', 'value=' .strval(KIND_CC_NO_CARD));
            $this->type("id=PaymentForm_creditCardName", $name);
            $this->type("id=PaymentForm_creditCardNumber", $cardNumber);
            $this->select('id=PaymentForm_creditCardMonth', 'index=' . $cardMonth);
            $this->select('id=PaymentForm_creditCardYear', 'label=' . $cardYear);

            $this->type("id=PaymentForm_firstName", $name);
            $this->type("id=PaymentForm_secondName", $lastName);

            $this->type("id=PaymentForm_street", $street);
            $this->type("id=PaymentForm_zipCode", $zipCode);
            $this->type("id=PaymentForm_city", $city);

            $this->clickAndWait("id=btnABA");
            // A validation should arise, and then we correct it:
            $this->assertElementContainsText('idErrCVC', Yii::t('mainApp', 'Debes introducir el código CVC', array(), NULL, $langEnv));
            $this->assertElementContainsText('idErrState', Yii::t('mainApp', 'w_required', array(), NULL, $langEnv));
            $this->type("id=PaymentForm_state", $state);

            // Only in case of cards accepted by MEXICO we set the right card type. In case of the others, as long as
            // they are not available we can't select them:
            if ($cAccepted){
                $this->select('id=PaymentForm_creditCardType', 'value=' . strval($creditType));
            }
            // Reloading page at this point...
            $this->assertTrue($this->isElementPresent("id=btnABA"));
            // Fix and submit form again
            $this->type("id=PaymentForm_CVC", $cardCvc);

            $this->clickAndWait("id=btnABA");

            $moUser = new AbaUser();
            $moUser->getUserByEmail($email);

            if (!$cAccepted){
                //We have forced a validation from allPago, brand/credit not corresponding to each other:
                $this->assertTrue($this->isElementPresent("id=dIdErrorSummary"));

                $moLogs = new PayGatewayLogAllpago();
                $moLogs->getLastLogByUserId($moUser->getId());
                $this->assertEquals('100.100.700', $moLogs->errorCode,
                    'The error code from ALLPAGO should be invalid brand combination');
                $this->assertGreaterThanOrEqual( 1, strpos( $moLogs->xmlRequest, $lastName),
                                'Be careful, lsatName is not being sent to ALLPAGO-MX' );
                return true;
            }

            $this->goAndAssertPremiumAfterPayment($langEnv);
            $this->assertRegitrationOnAllPago($moUser, '');

            // Asserts address data:
            $moUserAddressInvoice = new AbaUserAddressInvoice();
            $moUserAddressInvoice->getUserAddressByUserId($moUser->id, true);
            $this->assertEquals($street, $moUserAddressInvoice->street, 'Street set on the payment process on Mexico should be the same');
            $this->assertEquals($zipCode, $moUserAddressInvoice->zipCode, 'zipCode set on the payment process on Mexico should be the same');
            $this->assertEquals($city, $moUserAddressInvoice->city, 'city set on the payment process on Mexico should be the same');
            $this->assertEquals($state, $moUserAddressInvoice->state, 'state set on the payment process on Mexico should be the same');
            $this->assertEquals($countryId, $moUserAddressInvoice->country, 'Country set on the payment process on Mexico should be the same');
        } else {
            $this->assertFalse(true);
        }

        return true;

    }


    /** Sample users for Mexico
     *
     * @return array
     */
    public function dataMexicoUsersTest()
    {

        $sSufix = Yii::app()->config->get("EMAIL_TEST_SUFFIX");
        return array(
            array( 'unit.test.mexico.mx_A' . $sSufix . '@yopmail.com', COUNTRY_MEXICO, 'Unit Test', 'Español AllPago',
                'en', 'Callejuela Chingada 1', 'ZORRO12', 'Tenancingo', 'Mexico DF'),
            array( 'unit.test.mexico.mx_B' . $sSufix . '@yopmail.com', COUNTRY_MEXICO, 'Unit Test', 'Mexico English AllPago',
                'es', 'Avenida Chingaabuela 14', 'CHIHUAHUA12', 'Telpozlán', 'Cuernavaca'),
        );
    }

    /** Sample IPs for tests Mexico AllPago.
     *
     * @return array
     */
    public function dataIps()
    {
        return array(
            array('148.204.0.15'), //Mexico,
            array('37.72.0.15'), //Spain, Valencia, Alicante
        );
    }

    /**
     * @return array
     */
    public function dataCardTypes()
    {
        return array(
            array( KIND_CC_VISA, '4012001038443335', '05', '2018', '123', TRUE),
            array( KIND_CC_MASTERCARD, '5453010000066167', '05', '2018', '123', TRUE),
            array(KIND_CC_AMERICAN_EXPRESS, '376449047333005', '05', '2018', '1234', FALSE),
            array(KIND_CC_DISCOVER, '6011020000245045', '05', '2018', '123', FALSE),
        );
    }


    /** Combines all of three previous data arrays together.
     *
     * @return array|null
     */
    public function dataCombinations()
    {
        $aCombinations = $this->unitTestDataCartesian($this->dataCardTypes(), $this->dataMexicoUsersTest());
        foreach ($aCombinations[0] as $comb) {
            $retArrayCombinations[] = $comb;
        }
        $aCombinations = $this->unitTestDataCartesian($this->dataIps(), $retArrayCombinations);
        $retArrayCombinations = NULL;
        foreach ($aCombinations[0] as $comb) {
            $retArrayCombinations[] = $comb;
        }
        return $retArrayCombinations;
    }

}
