<?php

/**
 *
 */
class PaymentsCaixaInvoiceControllerTest extends PaymentsControllerTestBase
{
    /**
     * Creates all test users and any other test data used specific from this test.
     */
    public function setUp()
    {
        parent::setUp();

        $aUsersToCreate = $this->dataUsers();
        foreach ($aUsersToCreate as $userCols) {
            $this->createUsersIntoDbInvoice($userCols[0], FREE, $userCols[1], $userCols[2], $userCols[3], false, $userCols[4]);
        }
    }

    /**
     * Destroy all test users and any other test data used specific from this test.
     */
    public function tearDown()
    {
        $aUsersToDelete = $this->dataUsers();
        foreach($aUsersToDelete as $userCols){
            $this->resetDownUser($userCols[0]);
        }
        parent::tearDown();
    }

    /**
     * Tests The payment method page and it process a payment.
     * @dataProvider dataCombinations
     */
    public function testInvoice($email, $countryId, $name, $lastName, $langEnv,
                                $iProduct, $birthDate, $city, $telephone, $street, $streetMore, $province, $cp, $ident,
                                $creditType, $cardNumber, $cardMonth, $cardYear, $cardCvc, $cpf, $ipLocation)
    {
        $this->open('site/index');
        $this->type('name=LoginForm[email]', $email);
        $this->type('name=LoginForm[password]', '123456789');
        $this->clickAndWait("//input[@value='".Yii::t("mainApp", 'Iniciar sesión', NULL, NULL, Yii::app()->params["langAssert"])."']");
        $this->assertElementPresent("id=divBtnGoPremium");

        $this->open('payments/payment');

        $this->assertProductSelectionPage($countryId);
        $this->assertElementPresent("id=BtnMonthContinue-" . $iProduct);
        $this->clickAndWait("id=BtnMonthContinue-" . $iProduct);

        $isBrazil = false;
        if($this->isElementPresent("id=PaymentForm_cpfBrasil")) {
            $isBrazil = true;
        }

        $this->paymentMethodTPVAndBuyForInvoice($isBrazil, $email, $countryId, $name, $lastName, $langEnv, $creditType, $cardNumber, $cardMonth, $cardYear, $cardCvc, $cpf, $ipLocation);

        $this->assertElementPresent("id=my_account");
        $this->click("id=my_account");

        $this->assertElementPresent("id=headLinkMenuInvoices");
        $this->clickAndWait("id=headLinkMenuInvoices");


        $moUser = new AbaUser();
        $moUser->getUserByEmail($email);

        /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
        $this->assertUserInvoices($moUser, $email, false, $countryId);

        /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
        $this->paymentSaveInvoiceData($birthDate, $city, $telephone, $countryId, $street, $streetMore, $province, $cp, $ident);

        /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
        $this->downloadAllInvioces($moUser);

        /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
        if($isBrazil) {
            $this->assertRegitrationOnAllPago($moUser, $cpf);
        }

        /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
        $this->assertUserInvoices($moUser, $email, true, $countryId);
        /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
    }

    /**
     * @return array
     */
    public function dataUsers()
    {
        return array(
            array( 'unittest.user.invoice.br.spain@yopmail.com', COUNTRY_BRAZIL, 'Unit Invoice', 'Portûguese Brazil', 'pt', 1, '01-09-1980', 'Sao Paolo', '006655443322', 'Gran Via', 'santa monica & santa caterina', 'Sao Paolo', '12345', 'X1111111W'),
            array( 'unittest.user.invoice.lv.spain@yopmail.com', 117, 'Unit Invoice', 'English Latvia', 'en',               6, '01-09-1981', 'Jürmala', '007766554433', 'Via Jürmala', '', 'Jürmala', '12345', 'X1111111W'),
            array( 'unittest.user.invoice.ro@yopmail.com', 176, 'Unit Invoice', 'Spanish Romania', 'es',                    12, '01-09-1982', 'Brasov', '008877665544', 'Gran Via', '66', 'Brasov', '12345', 'X1111111W'),
            array( 'unittest.user.invoice.ru@yopmail.com', 177, 'Unit Invoice', 'English Russian Federation', 'en',         24, '01-09-1983', 'Сергиев Посад', '009988776655', '2я станционная', '4б, второй блок с права, 2я лестница', 'Сергиев Посад', '012343210', 'Ы2345432Ы'),
            array( 'unittest.user.invoice.sp@yopmail.com', COUNTRY_SPAIN, 'Unit Invoice', 'Spanish Spain', 'es',            12, '01-09-1984', 'Picamoixons', '005544332211', 'Gran Via, 001', '', 'Badalona', '12345', 'X5555555W'),
        );
    }

    public function dataCountries()
    {
        return array(
            COUNTRY_BRAZIL => array(COUNTRY_BRAZIL, 'BR'),
            117 => array(117, 'LV'),
            176 => array(176, 'RO'),
            177 => array(177, 'RU'),
            COUNTRY_SPAIN => array(COUNTRY_SPAIN, 'SP'),
        );
    }

    public function dataIps()
    {
        return array(
//            array('46.136.173.1'), //Brazil,
            array('37.72.0.15'), //Spain, Valencia
        );
    }

    public function dataCardTypes()
    {
        return array(
//            array(KIND_CC_VISA, '4012001038443335', '05', '2018', '123', '123.456.789-09'),
            array(KIND_CC_VISA, '4548812049400004', '12', '2020', '285', '123.456.789-09'),
//            array(KIND_CC_MASTERCARD, '5453010000066167', '05', '2018', '123', '804.654.341-18'),
//            array(KIND_CC_AMERICAN_EXPRESS, '376449047333005', '05', '2018', '1234', '123.456.789-09'),
//            array(KIND_CC_DISCOVER, '6011020000245045', '05', '2018', '123', '804.654.341-18'),
        );
    }

    /** Combines all of three previous data arrays together.
     *
     * @return array|null
     */
    public function dataCombinations()
    {
        $retArrayCombinations = array();
        $aCombinations = $this->unitTestDataCartesian($this->dataCardTypes(), $this->dataUsers());
        foreach ($aCombinations[0] as $comb) {
            $retArrayCombinations[] = $comb;
        }
        $aCombinations = $this->unitTestDataCartesian($this->dataIps(), $retArrayCombinations);
        $retArrayCombinations = NULL;
        foreach ($aCombinations[0] as $comb) {
            $retArrayCombinations[] = $comb;
        }
        return $retArrayCombinations;
    }

}
