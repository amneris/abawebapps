<?php
/*
 * Model associated with migration, it is linked to cron manual/MoveFollowUpHtml.
 * If this cron it's not in the crons folder anymore, then probably this model is useless, so you can
 * throw it away. Today is 2013-10-23
 *
 */

/**
 * @property integer $id
 * @property string timeStart
 * @property string timeEnd
 * @property string lastId
 * @property string rowsAffected
 *
 */
class LogMigrationFollowupHtml extends AbaActiveRecord
{
    public $id;

    /**
     * @returns LogMigrationFollowupHtml
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return Yii::app()->params['dbCampusLogs'].'.log_migration_followup_html';
    }

    /** List of all fields available in table
     * @return string
     */
    public function mainFields()
    {
        return " `id`, `timeStart`, `timeEnd`, `lastId`, `rowsAffected` ";
    }


    /** SQL creation of the table.
     *
     * @return bool|int|string
     */
    public function createTableLogsMigration()
    {
        $sqlDdl =  " CREATE TABLE `".Yii::app()->params["dbCampusLogs"]."`.`log_migration_followup_html` (
                              `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'auto id',
                              `timeStart` DATETIME DEFAULT NULL COMMENT 'Time when starts select insert into',
                              `timeEnd` DATETIME DEFAULT NULL COMMENT 'Time when select insert into Ends',
                              `lastId` INTEGER UNSIGNED DEFAULT NULL COMMENT 'Last Id imported from followup_html4',
                              `rowsAffected` INTEGER UNSIGNED DEFAULT NULL COMMENT 'Quantity of rows imported.',
                              PRIMARY KEY (`id`)
                              )
                    ENGINE = InnoDB
                    CHARACTER SET utf8 COLLATE utf8_general_ci
                    COMMENT = 'Used for migrating tables followup_html' ";

        $ret = $this->executeSQL( $sqlDdl );

        return $ret;
    }

    /** Initializes all the properties for the log cron object.
     * If no Id is passed through all properties are set to NULL
     * @param null $id
     *
     * @return bool|LogMigrationFollowupHtml
     */
    public function getLogById( $id=NULL )
    {
        if(is_null($id))
        {
            $id= -1;
        }
        $sql=" SELECT ".$this->mainFields()." FROM ".$this->tableName()." l  WHERE l.`id`=".$id."; ";

        $dataReader=$this->querySQL( $sql );
        if(($row = $dataReader->read())!==false)
        {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /** Returns the last row of migration log.
     * @return $this|bool
     */
    public function getLogLastByTimeEnd( )
    {
        $sql=" SELECT ".$this->mainFields()." FROM ".$this->tableName()." l  ORDER BY l.timeEnd DESC LIMIT 1;";

        $dataReader=$this->querySQL( $sql );
        if(($row = $dataReader->read())!==false)
        {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /** Inserts THE START of execution of a cron process
     *
     * @return bool
     */
    public function insertLog()
    {
        $sql= " INSERT INTO ".$this->tableName()." (`timeStart`, `timeEnd`, `lastId`, `rowsAffected`)
                          VALUES( :TIMESTART, :TIMEEND, :LASTID, :ROWSAFFECTED ); ";
        $aBrowserValues = array( ":TIMESTART"=>$this->timeStart, ":TIMEEND"=>$this->timeEnd,
                                 ":LASTID"=>$this->lastId , ":ROWSAFFECTED"=> $this->rowsAffected );
        $this->id = $this->executeSQL( $sql, $aBrowserValues , false);
        if ( $this->id > 0)
        {
            return $this->id;
        }
        else
        {
            return false;
        }
    }

    /** Updates a cron log with the end of execution, Success or Failure
     * @return bool
     */
    public function updateLogEndRequest()
    {
        $query = "UPDATE ".$this->tableName()." SET
                                 WHERE `id`=".$this->id." ";

        $aBrowserValues = array(  );

        if ( $this->updateSQL($query, $aBrowserValues )>0 )
        {
            return true;
        }
        else
        { return false; }
    }

    /**
     * Returns several rows with all cron logs unfinished
     * @param $idExclude
     *
     * @return bool|mixed
     */
    public function getAllCronsUnfinished($idExclude)
    {
        $sql=" SELECT ".$this->mainFields()." FROM ".$this->tableName()." l  WHERE l.`success` = 0 AND
                                                                l.`reviewed` = 0 AND
                                                                l.id<>".$idExclude." AND
                                                                ABS(TIMESTAMPDIFF(MINUTE,l.`dateStart`, NOW() ))>30 ;" ;

        $dataRows = $this->queryAllSQL($sql);
        if(!$dataRows)
        {
            return false;
        }

        return $dataRows;
    }
}
