<?php

/**
 * @property integer id
 * @property string dateModified
 * @property integer newUserLevel
 * @property integer oldUserLevel
 * @property string sourceAction
 * @property integer userId
 */
class LogUserLevelChange extends AbaActiveRecord
{
    public $id;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }


    /** Correspondence with database table
     * @return string
     */
    public function tableName()
    {
        return Yii::app()->params['dbCampusLogs'] . '.log_user_level_change';
    }

    /** Fields to be retrieved
     * @return string
     */
    public function mainFields()
    {
        return "  l.`id`, l.`userId`, l.`dateModified`, l.`oldUserLevel`, l.`newUserLevel`, l.`sourceAction`  ";
    }

    /**
     * @param null $id
     *
     * @return bool|LogUserLevelChange
     */
    public function getLogById($id = null)
    {
        if (is_null($id)) {
            $id = -156156106;
        }
        $sql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " l  WHERE l.`id`=" . $id . "; ";

        $dataReader = $this->querySQL($sql);
        return $this->readRowFillDataColsThis($dataReader);
    }


    /**
     * @param integer $userId
     * @param bool $lastOne
     * @param bool $limitOne
     *
     * @return bool
     *
     */
    public function getLogByUserId($userId, $lastOne = true, $limitOne = true)
    {
        $sqlOrder = "";
        if ($lastOne) {
            $sqlOrder = " ORDER BY l.`dateModified` DESC ";
        }

        $sqlLimit = "";
        if ($limitOne) {
            $sqlLimit = " LIMIT 1 ";
        }

        $sql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() .
          " l  WHERE l.`userId`=" . $userId . " " . $sqlOrder . $sqlLimit;

        $dataReader = $this->querySQL($sql);
        return $this->readRowFillDataCols($dataReader);
    }


    /** Sets 3 fields for this model, called previously
     * to insert.
     * @param integer $userId
     * @param integer $oldUserLevel
     * @param integer $newUserLevel
     * @param string $sourceAction
     *
     * @return bool
     */
    public function setUserChangeLevel($userId, $oldUserLevel, $newUserLevel, $sourceAction)
    {
        $this->userId = $userId;
        $this->sourceAction = $sourceAction;
        $this->newUserLevel = $newUserLevel;
        $this->oldUserLevel = $oldUserLevel;

        return true;
    }

    /** Insert log
     * @return bool|int
     */
    public function insertLog()
    {
//        $stLogData = array(
//          "userId" => $this->userId,
//          "oldUserLevel" => $this->oldUserLevel,
//          "newUserLevel" => $this->newUserLevel,
//          "sourceAction" => $this->sourceAction,
//        );
//
//        return HeLogger::sendLog("User level change", HeLogger::USERACTIVITY, HeLogger::INFO,
//          $stLogData);

        $sql = " INSERT INTO " . $this->tableName() . "
                (`userId`, `dateModified`, `oldUserLevel`, `newUserLevel`, `sourceAction`)
                VALUES
                ( :USERID, :DATEMODIFIED, :OLDUSERLEVEL, :NEWUSERLEVEL, :SOURCEACTION );";
        $aBrowserValues = array(
          ':USERID' => $this->userId,
          ':DATEMODIFIED' => HeDate::todaySQL(true),
          ':OLDUSERLEVEL' => $this->oldUserLevel,
          ':NEWUSERLEVEL' => $this->newUserLevel,
          ':SOURCEACTION' => $this->sourceAction
        );

        $this->id = $this->executeSQL($sql, $aBrowserValues, false);
        if ($this->id > 0) {
            return $this->id;
        } else {
            return false;
        }
    }
}
