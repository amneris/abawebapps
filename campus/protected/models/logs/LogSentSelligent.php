<?php
/**
 * @property integer  $id
 * @property string $dateSent
 * @property string $errorDesc
 * @property string $fields
 * @property string $response
 * @property string $sourceAction
 * @property string $url
 * @property integer $userId
 * @property string $webservice
 **/
class LogSentSelligent extends AbaActiveRecord
{
    public $id;

    /**
     * @return LogSentSelligent
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function tableName()
    {
        return Yii::app()->params['dbCampusLogs'].'.log_sent_selligent';
    }

    public function mainFields()
    {
        return "   l.`id`, l.`dateSent`, l.`url`, l.`webservice`, l.`fields`, l.`response`, l.`userId`, l.`sourceAction` ";
    }


    public function getLogById( $id='' )
    {
        $sql=" SELECT ".$this->mainFields()." FROM ".$this->tableName()." l  WHERE l.`id`='".$id."'; ";

        $dataReader=$this->querySQL( $sql );
        if(($row = $dataReader->read())!==false)
        {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $url
     * @param $webservice
     * @param $fields
     * @param $response
     * @param $userId
     * @param $sourceAction
     * @param $errMessage
     *
     * @return bool
     */
    public function insertLog( $url, $webservice, $fields, $response, $userId, $sourceAction, $errMessage)
    {
        $sql= " INSERT INTO ".$this->tableName()." (`url`, `webservice`, `fields`, `response`, `userId`, `sourceAction`, `errorDesc`)
                                        VALUES( :URL, :WEBSERVICE, :FIELDS, :RESPONSE, :USERID, '".$sourceAction."', :ERRORDESC ); ";

        $aBrowserValues = array(":URL"=> $url, ":WEBSERVICE"=> $webservice, ":WEBSERVICE"=> $webservice,
            ":FIELDS"=> $fields, ":RESPONSE"=> $response, ":USERID"=>$userId , ":ERRORDESC"=>$errMessage );
        $this->id = $this->executeSQL( $sql, $aBrowserValues , false);
        if ( $this->id > 0)
        {
            return $this->id;
        }
        else
        {
            return false;
        }
    }

    /**
     * @param integer $userId
     * @param string $webService
     *
     * @return $this|bool
     */
    public function getLogByUserId($userId, $webService='')
    {
        $sqlWebService = "";
        if($webService!=''){
            $sqlWebService = " AND l.webservice='".$webService."'";
        }
        $sql=" SELECT ".$this->mainFields()." FROM ".$this->tableName()." l  ".
            "WHERE l.`userId`='".$userId."'".
            $sqlWebService.
            "; ";

        $dataReader=$this->querySQL( $sql );
        if(($row = $dataReader->read())!==false)
        {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }
}