<?php

/**
 *
 *
 **/
class LogCronDeletion extends AbaActiveRecord
{
    public $id;
    public $dynTable;
    public $dynDb;

    const BD_ABA_B2C = "aba_b2c";
    const BD_ABA_B2C_LOGS = "aba_b2c_logs";

    /**
     * @param string $tableName
     * @param string $dynDb
     *
     * @return LogCronDeletion
     */
    public function __construct($tableName, $dynDb)
    {
        $this->dynTable = $tableName;
        $this->dynDb = $dynDb;
        parent::__construct();
    }

    public function tableName()
    {
        if ($this->dynDb == self::BD_ABA_B2C_LOGS) {
            return Yii::app()->params['dbCampusLogs'] . '.' . $this->dynTable;
        }
        return Yii::app()->params['dbCampus'] . '.' . $this->dynTable;
    }

    /**
     * @param $colSearch
     * @param $dateFilter
     *
     * @return int
     */
    public function deleteLog($colSearch, $dateFilter)
    {
        $sql = " DELETE FROM " . $this->tableName() . " WHERE " .
          " DATE(" . $colSearch . ") < DATE('" . $dateFilter . "')";

        $rowsAffected = $this->executeSQL($sql, array(), true);
        if ($rowsAffected > 0) {
            return $rowsAffected;
        } else {
            return 0;
        }
    }
}