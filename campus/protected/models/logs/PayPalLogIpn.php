<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 30/11/12
 * Time: 16:04
 * To change this template use File | Settings | File Templates.
 *
 * @property integer id
 * @property string dateCreation
 * @property string header
 * @property integer paymentStatus
 * @property string postFields
 * @property string transactionType
 * @property integer userId
 *
 */
class PayPalLogIpn extends AbaActiveRecord
{
    public function tableName()
    {
//        return Yii::app()->params['dbCampusLogs'].'.log_ipn_paypal';
        return 'pay_gateway_paypal_ipn';
    }

    public function mainFields()
    {
        return " l.`id`, l.`dateCreation`, l.`header`, l.`postFields`, l.`transactionType`,
                                l.`paymentStatus`, l.`userId` ";
    }

    public function insertLog()
    {
        $this->beginTrans();
        $successTrans = true;
        //-----------------
        $sql = " INSERT INTO " . $this->tableName() . " (`dateCreation`, `header`, `postFields`, `transactionType`, `paymentStatus`)
                                VALUES (:DATECREATION ,:HEADER ,:POSTFIELDS ,:TRANSACTIONTYPE ,:PAYMENTSTATUS ) ";

        $aParamValues = array(
          ':DATECREATION' => $this->dateCreation,
          ':HEADER' => $this->header,
          ':POSTFIELDS' => $this->postFields,
          ':TRANSACTIONTYPE' => $this->transactionType,
          ':PAYMENTSTATUS' => $this->paymentStatus
        );

        $this->id = $this->executeSQL($sql, $aParamValues);
        if ($this->id <= 0) {
            $successTrans = false;
        }

        //---------------
        if ($successTrans) {
            $this->commitTrans();
            return $this->id;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

}
