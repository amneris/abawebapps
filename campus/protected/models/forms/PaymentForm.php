<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteAbaController'.
 */
class PaymentForm extends AbaCFormModel
{
    public $firstName;
    public $secondName;
    public $street;
    public $zipCode;
    public $city;
    public $state;

    public $mail;

    /* @ var integer $idPayMethod; Corresponds to table pay_methods */
    public $idPayMethod = PAY_METHOD_CARD;
    public $creditCardType;
    public $creditCardName;
    public $creditCardNumber;
    public $creditCardMonth;
    public $creditCardYear;
    public $CVC;
    public $cpfBrasil;
    public $typeCpf;
    public $packageSelected;
    public $productsPricesPlans;
    public $isChangeProd = false;
    public $pwdToChangeProd;

    /* @var $idPaySupplier integer; points to same value as table pay_suppliers */
    protected $idPaySupplier;

    /**
     * @return int
     */
    public function getIdPaySupplier()
    {
        return $this->idPaySupplier;
    }

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('productsPricesPlans', 'required', 'message' => Yii::t('mainApp', 'selectmustmethod_key')),
            array('packageSelected', 'required', 'message' => Yii::t('mainApp', 'rule_paymentmethod_validkey')),

            array('firstName', 'type', 'type' => 'string', 'allowEmpty' => true),
            array('secondName', 'type', 'type' => 'string', 'allowEmpty' => true),
            array('street', 'type', 'type' => 'string', 'allowEmpty' => true),
            array('zipCode', 'type', 'type' => 'string', 'allowEmpty' => true),
            array('city', 'type', 'type' => 'string', 'allowEmpty' => true),
            array('state', 'type', 'type' => 'string', 'allowEmpty' => true),

            array('mail', 'email', 'allowEmpty' => false),
            array('idPayMethod', 'type', 'type' => 'integer', 'allowEmpty' => false,
                'message' => Yii::t('mainApp', 'rule_paymentmethod_validkey')),
            array('creditCardType', 'isValidTPVRequired',
                'message' => Yii::t('mainApp', 'Debes seleccionar el tipo de tarjeta')),
            array('creditCardName', 'isValidTPVRequired',
                'message' => Yii::t('mainApp', 'Debes introducir el nombre del titular de la tarjeta')),
            array('creditCardNumber', 'isValidCreditNumber'),
            array('creditCardNumber', 'isValidTPVRequired'),
            array('creditCardMonth', 'isValidTPVRequired',
                'message' => Yii::t('mainApp', 'Debes seleccionar el mes de caducidad de la tarjeta')),
            array('creditCardYear', 'isValidTPVRequired',
                'message' => Yii::t('mainApp', 'Debes seleccionar el año de caducidad de la tarjeta')),
            array('CVC', 'isValidTPVRequired',
                'message' => Yii::t('mainApp', 'Debes introducir el código CVC')),
            array('cpfBrasil', 'isValidateCpf'),
            array('firstName', 'isNameRequired'),
            array('secondName', 'isSecondNameRequired'),
            array('street', 'isAddressRequired'),
            array('city', 'isAddressRequired'),
            array('state', 'isAddressRequired'),
            array('zipCode', 'isAddressRequired'),
            array('typeCpf', 'type', 'type' => 'string', 'allowEmpty' => true),
            array('isChangeProd',  'type', 'type'=>'boolean', 'allowEmpty' => true),
            array('pwdToChangeProd', 'isValidPassword'),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that iss
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'firstName' => Yii::t('mainApp', 'Nombre'),
            'secondName' => Yii::t('mainApp', 'Apellidos'),
            'mail' => Yii::t('mainApp', 'Email'),
            'idPayMethod' => Yii::t('mainApp', 'methodPayment_key'),
            'creditCardType' => Yii::t('mainApp', 'Tipo de tarjeta'),
            'creditCardName' => Yii::t('mainApp', 'Nombre del titular'),
            'creditCardNumber' => Yii::t('mainApp', 'cardNumber_key'),
            'expirationDate' => Yii::t('mainApp', 'Caducidad'),
            'CVC' => Yii::t('mainApp', 'CVC:'),
            'acceptConditions' => Yii::t('mainApp', 'Acepto la condiciones de uso y la politica de privacidad'),
            'productsPricesPlans' => Yii::t('mainApp', 'suscription_w'),
            'packageSelected' => Yii::t('mainApp', 'suscription_w'),
            'cpfBrasil' => Yii::t('mainApp', 'key_nifBrazil'),
            'typeCpf' => Yii::t('mainApp', 'key_nifBrazil'),
            'isChangeProd' => 'Change product',     // It is not displayed anywhere.
            'pwdToChangeProd' => Yii::t('mainApp', 'key_securityChangeProd'),
            'street' => Yii::t('mainApp', 'wAddress'),
            'city' => Yii::t('mainApp', 'Ciudad'),
            'state' => Yii::t('mainApp', 'Provincia'),
            'zipCode' => Yii::t('mainApp', 'PostalCode'),
        );
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function isValidCreditNumber( $attribute, $params)
    {
        if (!$this->isChangeProd) {
            if (intval($this->idPayMethod) == PAY_METHOD_CARD && !HeMixed::isValidCreditCard($this->creditCardNumber)) {
                $this->addError("creditCardNumber", Yii::t('mainApp', 'credit_card_wrong_key'));
            }
        }
    }

    /** Validates CPF brazilian for payment through AllPago.com
     *
     * @param $attribute
     * @param $params
     */
    public function isValidateCpf( $attribute, $params )
    {
        if ( intval($this->idPaySupplier) == PAY_SUPPLIER_ALLPAGO_BR ||
            intval($this->idPaySupplier) == PAY_SUPPLIER_ALLPAGO_BR_BOL ){
            if ( $this->typeCpf == PAY_ID_BR_CPF && !HeMixed::isValidateCPF($this->cpfBrasil) ) {
                $this->addError("cpfBrasil", Yii::t('mainApp', 'key_novalidnifBrazil'));
            } elseif ( $this->typeCpf == PAY_ID_BR_CNPJ && !HeMixed::isValidateCNPJ($this->cpfBrasil) ) {
                $this->addError("cpfBrasil", Yii::t('mainApp', 'key_novalidnifBrazil'));
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function isNameRequired( $attribute, $params)
    {
        if ( intval($this->idPayMethod) == PAY_METHOD_BOLETO ||
            intval($this->idPaySupplier) == PAY_SUPPLIER_ALLPAGO_BR ||
            intval($this->idPaySupplier) == PAY_SUPPLIER_ALLPAGO_MX ){
            if ( $this->firstName=='' ) {
                $this->addError("firstName", Yii::t('mainApp', 'w_required'));
            }
        }
    }

    /** At the moment at 2014-10-24 only for Brazil
     *
     * @param $attribute
     * @param $params
     */
    public function isSecondNameRequired($attribute, $params)
    {
        if ( intval($this->idPayMethod) == PAY_METHOD_BOLETO ||
             intval($this->idPaySupplier) == PAY_SUPPLIER_ALLPAGO_BR ||
             intval($this->idPaySupplier) == PAY_SUPPLIER_ALLPAGO_MX ){
            if ( $this->secondName=='' ) {
                $this->addError("secondName", Yii::t('mainApp', 'w_required'));
            }
        }
    }

    /** It validates for Mexico payment gateway that the address related fields are filled.
     * @param $attribute
     * @param $params
     */
    public function isAddressRequired( $attribute, $params )
    {
        if ( intval($this->idPaySupplier) == PAY_SUPPLIER_ALLPAGO_MX || intval($this->idPayMethod)==PAY_METHOD_BOLETO ){
            if ( $this->street=='') {
                $this->addError('street', Yii::t('mainApp', 'w_required'));
            }
            if ( $this->state=='') {
                $this->addError('state', Yii::t('mainApp', 'w_required'));
            }
            if ( $this->city=='') {
                $this->addError('city', Yii::t('mainApp', 'w_required'));
            }
            if ( $this->zipCode=='') {
                $this->addError('zipCode', Yii::t('mainApp', 'w_required'));
            }
            if ( intval($this->idPayMethod)==PAY_METHOD_BOLETO){
                if ( strlen($this->zipCode)!==8){
                    $this->addError('zipCode', Yii::t('mainApp', 'key_8chars_zipcode'));
                }
                if ( !PayAllPagoCodes::validBrazilState($this->state) ) {
                    $this->addError('state', Yii::t('mainApp', 'key_stateNotCorrect'));
                }
            }

        }
    }

    /** Check security question for Changing product.
     * @param $attribute
     * @param $params
     */
    public function isValidPassword( $attribute, $params)
    {
        if ($this->isChangeProd) {
            if ($this->pwdToChangeProd == '') {
                $this->addError('pwdToChangeProd', Yii::t('mainApp', 'key_securityChangeProd_required'));
            } else {
                $moUser = new AbaUser();
                if (!$moUser->getUserByEmail(Yii::app()->user->getEmail(), $this->pwdToChangeProd, true, false)) {
                    $this->addError('pwdToChangeProd', Yii::t('mainApp', 'La contraseña no es correcta'));
                } else {
                    $moCredit = new AbaUserCreditForms($moUser->id);
                    if ($moCredit->cardCvc != $this->CVC) {
                        $this->addError("CVC", Yii::t('mainApp', 'cvc_notvalid_key'));
                    }
                }
            }
        }
    }

    /**
     * In case of TPV as a method of payment
     * we validate all credit card details as required
     *
     * @param $attribute
     * @param $params
     */
    public function isValidTPVRequired($attribute, $params)
    {
        if (intval($this->idPayMethod) == PAY_METHOD_CARD ) {
            if (trim($this->$attribute) == '') {
                $key = 'notnull_key';
                switch ($attribute) {
                    case "creditCardType":
                        $key = 'Debes seleccionar el tipo de tarjeta';
                        break;
                    case "creditCardName":
                        $key = 'Debes introducir el nombre del titular de la tarjeta';
                        break;
                    case "creditCardMonth":
                        $key = 'Debes seleccionar el mes de caducidad de la tarjeta';
                        break;
                    case "creditCardYear":
                        $key = 'Debes seleccionar el año de caducidad de la tarjeta';
                        break;
                    case "CVC":
                        $key = 'Debes introducir el código CVC';
                        break;
                }
                $this->addError($attribute, Yii::t('mainApp', $key));
            }
            if ($attribute == 'CVC') {
                if (strlen($this->$attribute) <= 2 || strlen($this->$attribute) >= 5) {
                    $this->addError("CVC", Yii::t('mainApp', 'cvc_notvalid_key') . " (2)");
                }
            }
        }
    }

    /** Change some of the values
     * @return bool
     */
    protected function parseFields()
    {
        if ($this->cpfBrasil != '') {
            $this->cpfBrasil = preg_replace('/[^0-9+]/', '', $this->cpfBrasil);
        }

        if ($this->zipCode != '') {
            $this->zipCode = preg_replace('/[^0-9+]/', '', $this->zipCode);
        }

        $this->idPayMethod = (int)$this->idPayMethod;
        $this->creditCardNumber = str_replace(' ', '', $this->creditCardNumber);
        return true;
    }

    /** We will have to know which platform associates to this payment method details.
     * In case we need to validate or to update special data to the payment gateway.
     *
     * @param AbaUser $moUser
     */
    public function setIdPaySupplier( AbaUser $moUser)
    {
        if ($this->idPayMethod==PAY_METHOD_CARD || empty($this->idPayMethod)){
            $this->idPaySupplier = PaymentFactory::whichCardGatewayPay($moUser);
        } else {
            $this->idPaySupplier = PaymentFactory::getSupplierByMethodAndCountry( $this->idPayMethod, $moUser->countryId);
        }

    }


    /** Returns a list of fields specific for the current Payment gateway supplier
     *
     * @param PaymentControlCheck $payCtrlCheck
     * @return array|bool
     */
    public function getSpecialFieldsPaySupplier( PaymentControlCheck $payCtrlCheck )
    {
        $moPaySupplierComm = PaymentFactory::createPayment( $payCtrlCheck, $this->idPaySupplier);
        $aFields = $moPaySupplierComm->getSpecialDisplayFields();
        return $aFields;
    }
}