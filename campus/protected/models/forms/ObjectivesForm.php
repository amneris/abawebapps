<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ObjectivesForm extends CFormModel{
	
	public $quest1;
	public $quest2;
	public $quest3;
	public $quest4;
	public $quest5;

	/**
	 * Declares the validation rules.
	 */
	
	public function rules(){
		
		return array(
			array(	"questOptions1", 	"required"	),
			array(	"questOptions2", 	"required"	),
			array(	"questOptions3", 	"required"	),
			array(	"questOptions4", 	"required"	),
			array(	"questOptions5", 	"required"	)
		);
		
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that iss
	 * the same as its name with the first letter in upper case.
	 */
	
	public function attributeLabels(){
	
		return array(
				'questOptions1' 	=>	Yii::t('mainApp', '1. ¿Para que quieres aprender Ingles?'),
				'questOptions2'		=>	Yii::t('mainApp', '2. ¿Hasta que nivel quieres llegar?'),
				'questOptions3'		=>	Yii::t('mainApp', '3. ¿Para que necesitas el Ingles?'),
				'questOptions4'		=>	Yii::t('mainApp', '4. ¿A que te dedicas?'),
				'questOptions5'		=>	Yii::t('mainApp', '5. Eres')
		);
	
	}
	
}