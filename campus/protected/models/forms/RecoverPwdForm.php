<?php

/**
 * RecoverPwdForm class.
 */
class RecoverPwdForm extends CFormModel
{
    public $email;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('email', 'email', 'allowEmpty'=>false),
        );
    }


	/**
	 * Declares customized attribute labels.
	 */
    public function attributeLabels()
    {
        return array(
            'email' => Yii::t('mainApp', 'Email'),
        );
    }
	
}