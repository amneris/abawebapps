<?php

/**
 * ProfileForm class.
 * ProfileForm is the data structure for keeping
 * profile form data. It is used by the 'profile' action of 'SiteController'.
 */
class ProfileForm extends AbaCFormModel
{
    public $id;
    public $name;
    public $surnames;
    public $email;
    public $password;
    public $userType;
    public $currentLevel;
    public $canChangeLanguage;
    public $langEnv;
    public $birthDate;
    public $gender;
    public $countryId;
    public $city;
    public $telephone;
    //invoices inputs
    public $fDisplayInvoices;
    public $invoicesId;
    public $invoicesName;
    public $invoicesSurname;
    public $invoicesStreet;
    public $invoicesStreetMore;
    public $invoicesCity;
    public $invoicesProvince;
    public $invoicesPostalCode;
    public $invoicesCountry;
    public $invoicesTaxIdNumber;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('id', 'ruleIdUser'),
            array('name', 'required', 'message' => Yii::t('mainApp', 'validationnotsucc_k')),
            array('surnames', 'required', 'message' => Yii::t('mainApp', 'validationnotsucc_k')),
            array('email', 'email', 'allowEmpty' => false),
            array('birthDate', 'date', 'format' => 'dd-mm-yyyy', 'allowEmpty' => true,
                                        'message' => Yii::t('mainApp', 'key_not_valid_birthDate')),
            array('birthDate', 'ruleDate', 'required'),
            array('gender', 'type', 'type' => 'string', 'allowEmpty' => true),
            array('countryId', 'type', 'type' => 'integer', 'allowEmpty' => true),
            array('city', 'type', 'type' => 'string', 'allowEmpty' => true),
            array('currentLevel', 'type', 'type' => 'integer', 'allowEmpty' => false),
            array('currentLevel', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => 6),
            array('langEnv', 'type', 'type' => 'string', 'allowEmpty' => false),
            array('telephone', 'type', 'type' => 'string', 'allowEmpty' => true),
            //invoices data rules
            array('fDisplayInvoices', 'numerical', 'integerOnly' => true,'allowEmpty' => true),
            array('invoicesId',  'numerical', 'integerOnly' => true,'allowEmpty' => true),
            array('invoicesName', 'ruleInvoicesRequired'),
            array('invoicesSurname', 'ruleInvoicesRequired'),
            array('invoicesStreet', 'ruleInvoicesRequired'),
            array('invoicesStreetMore',  'type', 'type' => 'string', 'allowEmpty' => true),
            array('invoicesCity', 'ruleInvoicesRequired'),
            array('invoicesProvince', 'ruleInvoicesRequired'),
            array('invoicesPostalCode', 'ruleInvoicesRequired'),
            array('invoicesCountry','ruleInvoicesRequired'),
            array('invoicesTaxIdNumber', 'ruleInvoicesRequired')
        );
    }

    public function ruleIdUser($id)
    {
        if (intval(Yii::app()->user->getId())!==intval($this->$id)) {
            $this->addError("name", "El id de usuario no es correcto. Cierre sesión y vuelva a intentarlo.");
        }
    }
    
    public function ruleDate($date)
    {
        if (!HeDate::validDate($this->birthDate, false, "-")) {
            $this->addError("birthDate", Yii::t('mainApp', 'key_not_valid_birthDate'));
        }
    }


    /**
     * @param $attribute
     * @param $params
     */
    public function ruleInvoicesRequired($attribute, $params)
    {
        if ((intval($this->fDisplayInvoices) == 1) && trim($this->$attribute) == '') {
            $this->addError($attribute, Yii::t('mainApp', 'validationnotsucc_k'));
        }
    }


    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that iss
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'id',
            'name' => Yii::t('mainApp', 'Nombre'),
            'surnames' => Yii::t('mainApp', 'Apellidos'),
            'email' => Yii::t('mainApp', 'Email'),
            'password' => Yii::t('mainApp', 'Contraseña'),
            'userType' => Yii::t('mainApp', 'Tipo de alumno'),
            'birthDate'=> Yii::t('mainApp', 'Fecha de nacimiento'),
            'langEnv' => Yii::t('mainApp', 'language'),
            'gender' => Yii::t('mainApp', 'Gender'),
            'countryId' => Yii::t('mainApp', 'País'),
            'city' => Yii::t('mainApp', 'Ciudad'),
            'telephone' => Yii::t('mainApp', 'Teléfono'),
            //invoices
            'invoicesName' => Yii::t('mainApp', 'key_InvoiceName'),
            'invoicesSurname' => Yii::t('mainApp', 'Apellidos'),
            'invoicesStreet' => Yii::t('mainApp', 'wAddress'),
            'invoicesStreetMore' => Yii::t('mainApp', 'wAddressMore'),
            'invoicesCity' => Yii::t('mainApp', 'Ciudad'),
            'invoicesProvince' => Yii::t('mainApp', 'Provincia'),
            'invoicesPostalCode' => Yii::t('mainApp', 'PostalCode'),
            'invoicesCountry' => Yii::t('mainApp', 'Country'),
            'invoicesTaxIdNumber' => Yii::t('mainApp', 'TaxNumber')
        );
    }
}
