<?php

/**
 * LoginFacebookForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteAbaController'.
 */
class LoginFacebookForm extends CFormModel
{
	public $fbEmail;
	public $fbPassword;

	/**
	 * Declares the validation rules.
	 * The rules state that email and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// email and password are required
			array('fbEmail', 'required'),
			//email validation
			array('fbEmail', 'email'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels(){
		return array(
			'fbEmail' => Yii::t('mainApp', 'Email'),
			'fbPassword' => Yii::t('mainApp', 'Contraseña'),
		);
	}

}
