<?php

/**
 * ProfileForm class.
 * ProfileForm is the data structure for keeping
 * profile form data. It is used by the 'profile' action of 'SiteAbaController'.
 */
class Paso1Form extends CFormModel{

    //public $id;
    private $idUser;
    private $viajar = false;
    private $estudiar = false;
    private $trabajar = false;
    private $nivel;
    private $engPrev = 0;
    private $selEngPrev;
    private $dedicacion;
    private $birthDate;
    private $genero;
    private $day;
    private $month;
    private $year;
    private $otros;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('idUser', 'ruleIdUser', 'required'),
            array('idUser', 'type', 'type' => 'integer', 'allowEmpty' => false),
            array('viajar', 'boolean', 'allowEmpty' => true),
            array('viajar', 'ruleViajar', 'required'),
            array('estudiar', 'boolean', 'allowEmpty' => true),
            array('estudiar', 'ruleEstudiar', 'required'),
            array('otros', 'boolean', 'allowEmpty' => true),
            array('otros', 'ruleOtros', 'required'),
            array('trabajar', 'boolean', 'allowEmpty' => true),
            array('trabajar', 'ruleTrabajar', 'required'),
            array('nivel', 'ruleNivel', 'required'),
            array('nivel', 'type', 'type' => 'integer', 'allowEmpty' => false),
            array('engPrev', 'ruleEngPrev', 'required'),
            array('engPrev', 'boolean', 'allowEmpty' => false),
            array('selEngPrev', 'ruleSelEngPrev', 'required'),
            array('selEngPrev', 'type', 'type' => 'string', 'allowEmpty' => false),
            array('dedicacion', 'ruleDedicacion', 'required'),
            array('dedicacion', 'type', 'type' => 'string', 'allowEmpty' => false),
            array('birthDate', 'date', 'format' => 'dd-mm-yyyy', 'allowEmpty' => false, 'message' => Yii::t('mainApp', 'key_not_valid_birthDate')),
            array('birthDate', 'ruleDate', 'required'),
            array('genero', 'ruleGenero', 'required'),
            array('genero', 'type', 'type' => 'string', 'allowEmpty' => false),
        );
    }

    public function ruleIdUser($idUser)
    {
//        if( intval(Yii::app()->user->getId())!==intval($this->$idUser) )
//        {
//            $this->addError("name", Yii::t('mainApp', 'El id de usuario no es correcto. Cierre sesión y vuelva a intentarlo.'));
//        }
    }

    public function ruleViajar($viajar)
    {
        if(((int)$this->$viajar)===1)
            $this->viajar = 1;
        else{
            $this->viajar = 0;
            if((((int)$this->trabajar)!==1)&&(((int)$this->estudiar)!==1)&&(((int)$this->otros)!==1))
                $this->addError ("viajar", Yii::t('mainApp', 'tiene que rellenar alguno de los 4 campos'));
        }
    }
    public function ruleOtros($otros)
    {
        if(((int)$this->$otros)===1)
            $this->otros = 1;
        else{
            $this->otros = 0;
            if((((int)$this->trabajar)!==1)&&(((int)$this->estudiar)!==1)&&(((int)$this->viajar)!==1))
                $this->addError ("viajar", Yii::t('mainApp', 'tiene que rellenar alguno de los 4 campos'));
        }
    }
    
    public function ruleDate($date)
    {
        if(!HeDate::validDate($this->birthDate,false,"-"))
            $this->addError("birthDate", Yii::t('mainApp', 'key_not_valid_birthDate'));
    }
    
    public function ruleEstudiar($estudiar)
    {
        if(((int)$this->$estudiar)===1)
            $this->estudiar = 1;
        else{
            $this->estudiar = 0;
            if((((int)$this->trabajar)!==1)&&(((int)$this->viajar)!==1)&&(((int)$this->otros)!==1))
                $this->addError ("estudiar", Yii::t('mainApp', 'tiene que rellenar alguno de los 4 campos'));
        }
    }
    
    public function ruleTrabajar($trabajar)
    {
        if(((int)$this->$trabajar)===1)
            $this->trabajar = 1;
        else{
            $this->trabajar = 0;
            if((((int)$this->viajar)!==1)&&(((int)$this->estudiar)!==1)&&(((int)$this->otros)!==1))
                $this->addError ("trabajar", Yii::t('mainApp', 'tiene que rellenar alguno de los 4 campos'));
        }
    }
    
    public function ruleEngPrev($engPrev)
    {
        if(((int)$this->$engPrev)===1)
            $this->engPrev = 1;
        else{
            $this->engPrev = 0;
            if(((int)$this->selEngPrev < 1))
                $this->addError("engPrev", Yii::t('mainApp', 'usted tiene que rellenar donde usted aprendio ingles'));
        }
    }
    
    public function ruleGenero($genero)
    {
        if(($this->$genero==="F")||($this->$genero==="M"))
            $this->genero = $this->$genero;
        else{
            $this->addError("genero", Yii::t('mainApp', 'usted tiene que rellenar su genero'));
        }
    }
    
    public function ruleNivel($nivel)
    {
        if($this->$nivel < 1)
            $this->addError("nivel", Yii::t('mainApp', 'usted tiene que rellenar hasta que nivel quiere llegar'));
    }
    
    public function ruleSelEngPrev($selEngPrev)
    {
        if(($this->$selEngPrev < 1)&&($this->engPrev===true))
            $this->addError("selEngPrev", Yii::t('mainApp', 'usted tiene que rellenar donde usted aprendio ingles'));
    }
    
    public function ruleDedicacion($dedicacion){
        if($this->$dedicacion < 1)
            $this->addError("dedicacion", Yii::t('mainApp', 'usted tiene que rellenar su dedicación'));
    }
    
    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that iss
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'idUser' => 'idUser',
            'viajar' => Yii::t('mainApp', 'viajar'),
            'estudiar' => Yii::t('mainApp', 'estudiar'),
            'trabajar' => Yii::t('mainApp', 'trabajar'),
            'nivel' => Yii::t('mainApp', 'nivel'),
            'dedicacion' => Yii::t('mainApp', 'dedicacion'),
            'birthDate'=> Yii::t('mainApp', 'Fecha de nacimiento'),
            'genero' => Yii::t('mainApp', 'genero')
        );

    }
    //getters
    public function getidUser(){
        return $this->idUser;
    }
    public function getviajar(){
        return $this->viajar;
    }
    public function getestudiar(){
        return $this->estudiar;
    }
    public function getotros(){
        return $this->otros;
    }
    public function gettrabajar(){
        return $this->trabajar;
    }
    public function getnivel(){
        return $this->nivel;
    }
    public function getengPrev(){
        return $this->engPrev;
    }
    public function getselEngPrev(){
        return $this->selEngPrev;
    }
    public function getdedicacion(){
        return $this->dedicacion;
    }
    public function getbirthDate(){
        return $this->birthDate;
    }
    public function getgenero(){
        return $this->genero;
    }
    public function getday(){
        return $this->day;
    }
    public function getmonth(){
        return $this->month;
    }
    public function getyear(){
        return $this->year;
    }
    //setters
    public function setidUser($idUser){
        $this->idUser = $idUser;
    }
    public function setviajar($viajar){
        $this->viajar = $viajar;
    }
    public function setestudiar($estudiar){
        $this->estudiar = $estudiar;
    }
    public function settrabajar($trabajar){
        $this->trabajar = $trabajar;
    }
    public function setnivel($nivel){
        $this->nivel = $nivel;
    }
    public function setotros($otros){
        $this->otros = $otros;
    }
    public function setengPrev($engPrev){
        $this->engPrev = $engPrev;
    }
    public function setselEngPrev($selEngPrev){
        $this->selEngPrev = $selEngPrev;
    }
    public function setdedicacion($dedicacion){
        $this->dedicacion = $dedicacion;
    }
    public function setbirthDate($birthDate){
        $this->birthDate = $birthDate;
    }
    public function setgenero($genero){
        $this->genero = $genero;
    }
    public function setday($day){
        $this->day = $day;
    }
    public function setmonth($month){
        $this->month = $month;
    }
    public function setyear($year){
        $this->year = $year;
    }
}