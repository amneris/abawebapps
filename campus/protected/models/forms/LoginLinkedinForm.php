<?php

/**
 * LoginFacebookForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteAbaController'.
 */
class LoginLinkedinForm extends CFormModel
{
	public $inEmail;
	public $inPassword;

	/**
	 * Declares the validation rules.
	 * The rules state that email and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// email and password are required
			array('inEmail', 'required'),
			//email validation
			array('inEmail', 'email'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels(){
		return array(
			'inEmail' => Yii::t('mainApp', 'Email'),
			'inPassword' => Yii::t('mainApp', 'Contraseña'),
		);
	}

}
