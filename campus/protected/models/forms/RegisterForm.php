<?php

/**
 * RegisterForm class.
 * RegisterForm is the data structure for keeping
 * User register form data. It is used by the 'login' action of 'SiteAbaController'.
 */
class RegisterForm extends CFormModel
{
	public $name;
	public $email;
	public $password;

	/**
	 * Declares the validation rules.
	 * The rules state that email and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// email and password are required
			array('name, email, password', 'ext.MyValidators.ValidatorClientLogin'),
            //name validation
            array('name', 'type', 'type' => 'string', 'allowEmpty' => false),
			//email validation
			array('email', 'ext.MyValidators.ValidatorEmailCorrect'),
			// password needs to be authenticated
			array('password', 'type', 'type' => 'string', 'allowEmpty' => false),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels(){
		return array(
			'name' => Yii::t('mainApp', 'Name'),
			'email' => Yii::t('mainApp', 'Email'),
			'password' => Yii::t('mainApp', 'Contraseña'),
		);
	}
}
