<?php

/**
 * CreditCardForm class used from  action of SubscriptionController.
 *
 */
class CreditCardForm extends AbaCFormModel
{
    public $idPayMethod;
    public $creditCardType; // Kind
    public $creditCardName;
    public $creditCardNumber;
    public $creditCardMonth;
    public $creditCardYear;
    public $CVC;
    public $cpfBrasil;
    public $typeCpf;

    /* @var $idPaySupplier integer; points to same value as table pay_suppliers */
    protected $idPaySupplier;

    /**
     * @param string $scenario
     */
    public function __construct($scenario = '')
    {
        parent::__construct($scenario);
        $this->idPayMethod = PAY_METHOD_CARD; // at 2014-10-01 the only method available to update payment details.
        $this->idPaySupplier = PAY_SUPPLIER_CAIXA; // Default value, 90% countries
    }

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('idPayMethod', 'type', 'type' => 'integer', 'allowEmpty' => false),
            array('creditCardType', 'isValidTPVRequired',
                'message' => Yii::t('mainApp', 'Debes seleccionar el tipo de tarjeta')),
            array('creditCardName', 'isValidTPVRequired',
                'message' => Yii::t('mainApp', 'Debes introducir el nombre del titular de la tarjeta')),
            array('creditCardNumber', 'isValidCreditNumber'),
            array('creditCardNumber', 'isValidTPVRequired'),
            array('creditCardMonth', 'isValidTPVRequired',
                'message' => Yii::t('mainApp', 'Debes seleccionar el mes de caducidad de la tarjeta')),
            array('creditCardYear', 'isValidTPVRequired',
                'message' => Yii::t('mainApp', 'Debes seleccionar el año de caducidad de la tarjeta')),
            array('CVC', 'isValidTPVRequired',
                'message' => Yii::t('mainApp', 'Debes introducir el código CVC')),
            array('cpfBrasil', 'isValidateCpf'),
            array('typeCpf', 'type', 'type' => 'string', 'allowEmpty' => true),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that iss
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'idPayMethod' => Yii::t('mainApp', 'methodPayment_key'),
            'creditCardType' => Yii::t('mainApp', 'Tipo de tarjeta'),
            'creditCardName' => Yii::t('mainApp', 'Nombre del titular'),
            'creditCardNumber' => Yii::t('mainApp', 'cardNumber_key'),
            'expirationDate' => Yii::t('mainApp', 'Caducidad'),
            'CVC' => Yii::t('mainApp', 'CVC:'),
            'cpfBrasil' => Yii::t('mainApp', 'key_nifBrazil'),
            'typeCpf' => Yii::t('mainApp', 'key_nifBrazil'),
        );
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function isValidCreditNumber($attribute, $params)
    {
        if (intval($this->idPaySupplier) !== PAY_SUPPLIER_PAYPAL &&
            !HeMixed::isValidCreditCard($this->creditCardNumber)
        ) {
            $this->addError("creditCardNumber", Yii::t('mainApp', 'credit_card_wrong_key'));
        }
    }

    /** Validates CPF brazilian for payment through AllPago.com
     *
     * @param $attribute
     * @param $params
     */
    public function isValidateCpf($attribute, $params)
    {
        if (intval($this->idPaySupplier) == PAY_SUPPLIER_ALLPAGO_BR &&
            $this->typeCpf == PAY_ID_BR_CPF && !HeMixed::isValidateCPF($this->cpfBrasil)
        ) {
            $this->addError("cpfBrasil", Yii::t('mainApp', 'key_novalidnifBrazil'));
        } elseif (intval($this->idPaySupplier) == PAY_SUPPLIER_ALLPAGO_BR &&
            $this->typeCpf == PAY_ID_BR_CNPJ && !HeMixed::isValidateCNPJ($this->cpfBrasil)
        ) {
            $this->addError("cpfBrasil", Yii::t('mainApp', 'key_novalidnifBrazil'));
        }
    }


    /**
     * In case of TPV as a method of payment
     * we validate all credit card details as required
     *
     * @param $attribute
     * @param $params
     */
    public function isValidTPVRequired($attribute, $params)
    {
        if (intval($this->idPaySupplier) !== PAY_SUPPLIER_PAYPAL) {
            if (trim($this->$attribute) == '') {
                $key = 'notnull_key';
                switch ($attribute) {
                    case "creditCardType":
                        $key = 'Debes seleccionar el tipo de tarjeta';
                        break;
                    case "creditCardName":
                        $key = 'Debes introducir el nombre del titular de la tarjeta';
                        break;
                    case "creditCardMonth":
                        $key = 'Debes seleccionar el mes de caducidad de la tarjeta';
                        break;
                    case "creditCardYear":
                        $key = 'Debes seleccionar el año de caducidad de la tarjeta';
                        break;
                    case "CVC":
                        $key = 'Debes introducir el código CVC';
                        break;
                }
                $this->addError($attribute, Yii::t('mainApp', $key));
            }
            if ($attribute == 'CVC') {
                if (strlen($this->$attribute) <= 2 || strlen($this->$attribute) >= 5) {
                    $this->addError("CVC", Yii::t('mainApp', 'cvc_notvalid_key') . " (2)");
                }
            }
        }
    }

    /** Change some of the values
     * @return bool
     */
    protected function parseFields()
    {
        if ($this->idPaySupplier == PAY_SUPPLIER_ALLPAGO_BR) {
            if ($this->cpfBrasil != '') {
                $this->cpfBrasil = preg_replace('/[^0-9+]/', '', $this->cpfBrasil);
            }
        }

        $this->creditCardNumber = str_replace(' ', '', $this->creditCardNumber);
        return true;
    }

    /** We will have to know which platform associates to this payment method details.
     * In case we need to validate or to update special data to the payment gateway.
     *
     * @param AbaUser $moUser
     */
    public function setIdPaySupplier( AbaUser $moUser)
    {
        $this->idPaySupplier = PaymentFactory::whichCardGatewayPay($moUser);
    }

    /**
     * @return int
     */
    public function getIdPaySupplier() {
        return $this->idPaySupplier;
    }
}
