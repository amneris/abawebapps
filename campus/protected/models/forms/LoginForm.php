<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteAbaController'.
 */
class LoginForm extends CFormModel
{
	public $email;
	public $password;
	public $rememberMe;

  public $msgPopup = '';
    /**
     * @var AbaUserIdentity
     */
  private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that email and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// email and password are required
			array('email, password', 'ext.MyValidators.ValidatorClientLogin'),

			//email validation
			array('email', 'ext.MyValidators.ValidatorEmailCorrect'),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),
			// password needs to be authenticated
			array('password', 'authenticate'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels(){
		return array(
			'email' => Yii::t('mainApp', 'Email'),
			'password' => Yii::t('mainApp', 'Contraseña'),
			'rememberMe' => Yii::t('mainApp', 'Recordarme en este equipo'),
            'notCloseSession' => Yii::t('mainApp', 'No cerrar sesión'),
		);
	}

  protected function abaAddError() {

    switch($this->_identity->errorCode)
    {
      case AbaUserIdentity::ERROR_NONE:
        $this->login();
        break;
      case AbaUserIdentity::ERROR_USERNAME_INVALID:
        $this->addError('email',Yii::t('mainApp', 'El email no es correcto'));
        break;
      case AbaUserIdentity::ERROR_PASSWORD_INVALID:
        $this->addError('password',Yii::t('mainApp', 'La contraseña no es correcta'));
        break;
      case AbaUserIdentity::ERROR_UNKNOWN_IDENTITY:
        $this->addError('email',Yii::t('mainApp', 'user_unknown'));
        break;
      case AbaUserIdentity::ERROR_NO_ACCESS:
        $this->addError('email',Yii::t('mainApp', 'user_no_access'));
        break;
      case AbaUserIdentity::ERROR_USER_LIMITEHORARIO:
//        $this->addError('email',Yii::t('mainApp', $this->_identity->errorMessage));
          $this->msgPopup = $this->_identity->errorMessage;
        break;
      default: // UserIdentity::ERROR_PASSWORD_INVALID
        $this->addError('password',Yii::t('mainApp', 'La contraseña no es correcta'));
        break;
    }
  }

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params){
        $this->_identity=new AbaUserIdentity($this->email, $this->password, $this->rememberMe);
        $this->_identity->authenticate();
        $this->abaAddError();
	}

	/**
	 * Logs in the user using the given email and password in the model.
	 * @return boolean whether login is successful
	 */
    public function login($pwdEncrypted = false)
    {

        if ($this->_identity === null) {
            $this->_identity = new AbaUserIdentity($this->email, $this->password, $this->rememberMe);
            $ret = $this->_identity->authenticate($pwdEncrypted);

            $this->abaAddError();
        }

        if ($this->_identity->errorCode === AbaUserIdentity::ERROR_NONE) {
            Yii::app()->user->login($this->_identity);
            if (!$pwdEncrypted) {
                /* issue 4120: show welcome back only if login through /site/login form (encrypted pwd means auto login) */
                Yii::app()->user->setState("welcomeBack", 1);
            }

            //
            //#abawebapps-190
            $this->_identity->initPaymentControlCheckId();

            return true;
        } else {
            return false;
        }
    }

    /** This is used for newsletter, first registration, redirection from all web services of registration.
     * @param $keyExternalLogin
     * @return bool
     */
    public function autoLogin( $keyExternalLogin )
    {
        $user = new AbaUser();
        if( !$user->getUserByKeyExternalLogin( $keyExternalLogin) )
        {
            return false;
        }
        else
        {
            $this->email = $user->email;
            $this->password = $user->password;
            $this->rememberMe = true;
            if( $this->login( true )){
                return true;
            }
        }

        return false;
    }

    /**
     * @param $keyExternalLogin
     *
     * @return AbaUser|bool
     */
    public function autoLoginPartial( $keyExternalLogin )
    {
        $user = new AbaUser();
        if( $user->getUserByKeyExternalLogin( $keyExternalLogin) ) {
            return $user;
        }
        return false;
    }

}
