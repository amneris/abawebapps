<?php

/**
 * PasswordForm class.
 */
class PasswordForm extends CFormModel
{
    public $userId;
    public $oldPassword;
    public $newPassword;
    public $confirmPassword;


    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('oldPassword', 'required'),
            array('newPassword', 'required'),
            array('newPassword', 'length', 'min'=>6, 'max'=>20),
            array('confirmPassword', 'required'),
            array('confirmPassword', 'length', 'min'=>6, 'max'=>20),
        );
    }


	/**
	 * Declares customized attribute labels.
	 */
    public function attributeLabels()
    {
        return array(
            'oldPassword' => Yii::t('mainApp', 'Contraseña actual'),
            'newPassword' => Yii::t('mainApp', 'Contraseña nueva'),
            'confirmPassword' => Yii::t('mainApp', 'Repetir contraseña nueva'),
        );
    }
	
}