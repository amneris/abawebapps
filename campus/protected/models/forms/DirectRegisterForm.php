<?php

/**
 * RegisterForm class.
 * RegisterForm is the data structure for keeping
 * User register form data. It is used by the 'login' action of 'SiteAbaController'.
 */
class DirectRegisterForm extends CFormModel
{
    public $name;
    public $surnames;
    public $email;
    public $password;

    /**
     * Declares the validation rules.
     * The rules state that email and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            // email and password are required
          array('name, email, password', 'ext.MyValidators.ValidatorClientLogin'),
          array('name', 'type', 'type' => 'string', 'allowEmpty' => false),
          array('surnames', 'type', 'type' => 'string', 'allowEmpty' => true),
          array('email', 'ext.MyValidators.ValidatorEmailCorrect'),
          array('password', 'type', 'type' => 'string', 'allowEmpty' => false),
        );
    }


    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
          'name' => Yii::t('mainApp', 'directpayment_username'),
          'surnames' => Yii::t('mainApp', 'directpayment_usersurnames'),
          'email' => Yii::t('mainApp', 'directpayment_useremail'),
          'password' => Yii::t('mainApp', 'directpayment_userpassword'),
        );
    }
}
