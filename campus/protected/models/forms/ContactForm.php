<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteAbaController'.
 */
class ContactForm extends CFormModel
{
//    public $name;
//    public $email;
    public $subject;
    public $aListSubjects;
    public $body;
//    public $verifyCode;
    public $conditions;

    public function __construct($b2b = false)
    {
        if ($b2b) {
            $this->aListSubjects = array(Yii::t('mainApp', 'technicalsupport_key') => Yii::t('mainApp', 'technicalsupport_key'),);
        } else {
            $this->aListSubjects = array(Yii::t('mainApp', 'technicalsupport_key') => Yii::t('mainApp', 'technicalsupport_key'),
                Yii::t('mainApp', 'Información básica') => Yii::t('mainApp', 'Información básica'),
                Yii::t('mainApp', 'Facturas') => Yii::t('mainApp', 'Facturas'),);
        }
    }

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            // name, email, subject, body and conditions are required
            array('subject, body', 'required'),
            array('subject', 'isValidSubject'),
//            array('conditions', 'compare', 'compareValue' => true, 'message'=>Yii::t("mainApp", "mustselectconditions_key")),
          //  array('conditions', 'required', 'requiredValue' => 1, 'message' => Yii::t('mainApp', 'mustselectconditions_key') ),

            // email has to be a valid email address
//            array('email', 'email'),
            // verifyCode needs to be entered correctly
//            array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
        );
    }

	public function attributeLabels(){
		return array(
			'subject' => Yii::t('mainApp', 'Asunto'),
			'body' => Yii::t('mainApp', 'Cuerpo'),
		);
	}


    public function isValidSubject( $attribute, $params )
    {
        $aEmails = $this->getEmailsSubject();
        if( array_search($this->subject, array_keys($aEmails) )===false )
        {
            $this->addError($attribute, Yii::t('mainApp', 'mustselectsubject_key'));
        }

    }

    /**
     * @return array
     */
    public function getEmailsSubject()
    {
        return array(Yii::t('mainApp', 'technicalsupport_key') => Yii::app()->config->get("EMAIL_DPT_SUPPORT"),
                        Yii::t('mainApp', 'Información básica') => Yii::app()->config->get("EMAIL_DPT_INFO"),
                        Yii::t('mainApp', 'Facturas')=> Yii::app()->config->get("EMAIL_DPT_PAYMENT") );
    }

    /**
     * @return mixed
     */
    public function getEmailFromSubject()
    {
        $aEmails = $this->getEmailsSubject();

        if(Yii::app()->user->getLanguage()!='es'){
            if ($this->subject == (Yii::t('mainApp', 'technicalsupport_key'))){
                $aEmails[$this->subject] = str_replace("rerere@", '_'.Yii::app()->user->getLanguage()."@",
                    $aEmails[$this->subject]);
            }
        }

        return $aEmails[$this->subject];
    }
}