<?php
/**
 * ResetPasswordForm class.
 */
class ResetPasswordForm extends CFormModel
{
    public $password;
    public $passwordconfirm;
    public $email;
    public $autol;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
//            array('password, passwordconfirm', 'required'),
            array('password, passwordconfirm', 'type', 'type' => 'string', 'allowEmpty' => true),
//            array('password, passwordconfirm', 'ext.MyValidators.ValidatorEmailCorrect'),
            array(
                'password, passwordconfirm',
                'isValidPassword',
                'message' => Yii::t('mainApp', 'recoverPassword_emptypassword')
            ),
            array('email, autol', 'type', 'type' => 'string', 'allowEmpty' => true),
        );
    }

    /**
     * Declares customized attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'password' => Yii::t('mainApp', 'recoverPassword_password'),
            'passwordconfirm' => Yii::t('mainApp', 'recoverPassword_passwordconfirm'),
            'email' => Yii::t('mainApp', 'Email'),
            'autol' => '',
        );
    }

    /**
     * @param $attribute
     *
     * @return bool
     */
    public function isValidPassword($attribute)
    {
        if (trim($this->$attribute) == '') {
            $key = 'notnull_key';
            switch ($attribute) {
                case "password":
                case "passwordconfirm":
                    $key = 'recoverPassword_emptypassword';
                    break;
            }
            $this->addError($attribute, Yii::t('mainApp', $key));
        }
        return true;
    }

    /**
     * @param $sPassword
     * @param $sPasswordconfirm
     *
     * @return bool
     */
    public function isSamePassword($sPassword, $sPasswordconfirm)
    {
        if($sPassword != $sPasswordconfirm) {
            $this->addError("password", Yii::t('mainApp', 'recoverPassword_distinctpasswords'));
            $this->addError("passwordconfirm", Yii::t('mainApp', 'recoverPassword_distinctpasswords'));
            return false;
        }
        return true;
    }


}