<?php
/* !! IMPORTANT, DO NOT MODIFY THE PHP DOC COMMENTS ON THE HEADER OF EVERY FUNCTION. **************************************************************
    * IT IS USED BY Yii IN ORDER TO CREATE WSDL. FORBIDDEN! ***************************************************************************************
    */
/**
 * User: Quino
 * Date: 25/03/13
 * Time: 16:58
 * To change this template use File | Settings | File Templates.
 */
class WsRegUserArray
{
    /* !! IMPORTANT, DO NOT MODIFY THE PHP DOC COMMENTS ON THE HEADER OF EVERY FUNCTION. *******************************
    * IT IS USED BY Yii IN ORDER TO CREATE WSDL. FORBIDDEN! ************************************************************/

    /**
     * @var integer id of the new user or error code in case of result not equal to success
     * @soap
     */
    public $userId;
    /**
     * @var string status result from the operation, should be success
     * @soap
     */
    public $result;
    /**
     * @var string should be a valid url in https protocol
     * @soap
     */
    public $redirectUrl;
}
