<?php

/**
 * @property string id
 * @property string description
 * @property string extCode
 * @property string outEur
 * @property string symbol
 * @property string unitEur
 * @property string unitUsd
 * @property string outUsd
 * @property string decimalPoint
 */
class AbaCurrency  extends AbaActiveRecord
{
    public $id;

    /**
     * @param null|integer $id
     */
    public function  __construct($id=NULL)
    {
        parent::__construct();
        if(!is_null($id))
        {
            return $this->getCurrencyById($id);
        }
        return $this;
    }


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'currencies';
    }

    /**
     * List of all its fields
     * @return string
     */
    public function mainFields()
    {
        return " cr.`id`, cr.`extCode`, cr.`description`, cr.`symbol`, cr.`unitEur`, cr.`outEur`,
                    cr.`unitUsd`, cr.`decimalPoint` ";
    }


    /**
     * @param $id
     *
     * @return AbaCurrency|bool
     */
    public function getCurrencyById( $id )
    {
        $sql = " SELECT ".$this->mainFields()." FROM ".$this->tableName()." cr WHERE cr.id=:IDCURRENCY ";
        $paramsToSQL = array(":IDCURRENCY"=>$id);

        $dataReader=$this->querySQL($sql,$paramsToSQL);
        if(($row = $dataReader->read())!==false)
        {
            $this->fillDataColsToProperties( $row );
            return $this;
        }

        return false;
    }


    public function getConversionRateFrom( $fromIdCcy = 'EUR')
    {
        if( $fromIdCcy == CCY_DEFAULT )
        {
            return HeMixed::getCcyFromSQL( $this->outEur );
        }
    }

    /**
     * @param string $toIdCcy
     *
     * @return float
     * @throws CDbException
     */
    public function getConversionRateTo ($toIdCcy = 'EUR')
    {
        if( $toIdCcy==CCY_DEFAULT )
        {
            return HeMixed::getCcyFromSQL( $this->unitEur );
        }
        elseif( $toIdCcy==CCY_2DEF_USD )
        {
            return HeMixed::getCcyFromSQL( $this->unitUsd );
        }
        else
        {
            // Example of 1 USD to THB, 1 EUR to USD, 1 THB to EUR, 1 USD to THB
            // Currently we are using conversion through third currency "Euro" exchange rate. Which is not 100% accurate
            $sql = " SELECT cr.unitEur FROM ".$this->tableName()." cr WHERE cr.id=:IDCURRENCY ";
            $paramsToSQL = array(":IDCURRENCY"=>$toIdCcy);
            $dataReader=$this->querySQL($sql,$paramsToSQL);
            if(($row = $dataReader->read())!==false)
            {
                $rateEurToIdCcy = HeMixed::getCcyFromSQL( $row["unitEur"] );
                if( $this->id==CCY_DEFAULT ){
                    $rateFinal = $rateEurToIdCcy ;
                }else{
                    $rateFinal = $rateEurToIdCcy / $this->unitEur;
                }
                return HeMixed::getCcyFromSQL($rateFinal);
            }
            else
            {
                throw new CDbException(" We do not have the currency ".$toIdCcy);
            }
        }

    }

    /**
     * @param        $amount
     * @param string $toIdCcy
     *
     * @return float
     */
    public function getConversionAmountTo($amount, $toIdCcy = 'EUR')
    {
        if( $this->id == $toIdCcy ){
            return HeMixed::getCcyFromSQL($amount);
        }

        $qRate = $this->getConversionRateTo( $toIdCcy );

        if( $this->id == CCY_DEFAULT ){
            $amount = $amount * $qRate;
        }
        else{
            $amount = $amount / $qRate;
        }

        return HeMixed::getCcyFromSQL($amount);
    }

    /**
     * Symbol to be displayed in the front-end. For example $USD or &GBP
     * @return string
     */
    public function getSymbol()
    {
        if( strlen($this->symbol)===0)
        {
            return $this->id;
        }
        else
        {
            return $this->symbol;
        }
    }


    /**
     * It returns the codes need to communicate with
     * our payments supplier. Only For La Caixa Gateway.
     * @return array
     *
     * @throws CException
     */
    public function getPaySupplierCodes()
    {
        if ($this->id == "USD") {
            return array("code" => Yii::app()->config->get("PAY_CAIXA_USDCODE_TPV"),
                "terminal" => "001",
                "password" => Yii::app()->config->get("PAY_CONTRA_LACAIXA_GW_USD"),
                "currency" => PaySupplierLaCaixa::USD_CAIXA_CODE);
        } elseif ($this->id == "EUR") {
            return array("code" => Yii::app()->config->get("PAY_CAIXA_EURCODE_TPV"),
                "terminal" => "002",
                "password" => Yii::app()->config->get("PAY_CONTRA_LACAIXA_GW_EUR"),
                "currency" => PaySupplierLaCaixa::EUR_CAIXA_CODE);
        } else {
            throw new CException("ABA, you can not pay with this currency from this country.".
                        " Allowed currencies are USD and EUR only. Please contact support for more information.");
        }
    }
}
?>