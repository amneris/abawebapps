<?php
/**
 * @property integer id
 * @property string name_m
 * @property string name
 * @property string iso
 * @property string iso3
 * @property string days
 * @property string numcode
 * @property string ABAIdCurrency
 * @property string ABALanguage
 * @property string decimalPoint
 * @property integer invoice
 */
class AbaCountry  extends CmAbaCountry
{
    /**
     * Constructor, null implementation
     */
    public function __construct($id=NULL)
    {
        parent::__construct($id);
        $this->connection = Yii::app()->getComponent(AbaActiveRecord::DB_DEFAULT);
    }
}
