<?php

namespace appCampus\models\leveltest;

use Yii;

/**
 * This is the model class for table "leveltest_Questions".
 *
 * The followings are the available columns in table 'leveltest_Questions':
 * @property integer $id
 * @property integer $idLevel
 * @property string $question
 * @property integer $answer
 * @property string $option1
 * @property string $option2
 * @property string $option3
 * @property integer $type
 * @property string $created
 * @property string $deleted
 * @property integer $isdeleted
 */
class LevelTestQuestions extends \BaseLevelTestQuestions
{
    // ** Tipos de pregunta.
    const _TYPE_TEXT = 1;
    const _TYPE_AUDIO = 2;

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LeveltestQuestions the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
