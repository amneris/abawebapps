<?php

/**
 * This is the model class for table "test_questions".
 *
 * The followings are the available columns in table 'test_questions':
 * @property integer $id
 * @property integer $idLevel
 * @property string $question
 * @property integer $answer
 * @property string $option1
 * @property string $option2
 * @property string $option3
 * @property integer $type
 * @property string $created
 * @property string $deleted
 * @property integer $isdeleted
 */
class BaseLevelTestQuestions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'leveltest_Questions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idLevel, question, answer, option1, option2, option3, type', 'required'),
			array('idLevel, answer, type, isdeleted', 'numerical', 'integerOnly'=>true),
			array('question, option1, option2, option3', 'length', 'max'=>255),
			array('created, deleted', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idLevel, question, answer, option1, option2, option3, type, created, deleted, isdeleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idLevel' => 'Id Level',
			'question' => 'Question',
			'answer' => 'Answer',
			'option1' => 'Option1',
			'option2' => 'Option2',
			'option3' => 'Option3',
			'type' => 'Type',
			'created' => 'Created',
			'deleted' => 'Deleted',
			'isdeleted' => 'Isdeleted',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idLevel',$this->idLevel);
		$criteria->compare('question',$this->question,true);
		$criteria->compare('answer',$this->answer);
		$criteria->compare('option1',$this->option1,true);
		$criteria->compare('option2',$this->option2,true);
		$criteria->compare('option3',$this->option3,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('deleted',$this->deleted,true);
		$criteria->compare('isdeleted',$this->isdeleted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BaseTestQuestions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
