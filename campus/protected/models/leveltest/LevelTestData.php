<?php

namespace appCampus\models\leveltest;

use Yii;

/**
 * This is the model class for table "leveltest_Data".
 *
 * The followings are the available columns in table 'leveltest_Data':
 * @property integer $id
 * @property string $code
 * @property integer $status
 * @property integer $spentTime
 * @property string $jsonData
 * @property integer $idLevel
 * @property integer $numErrors
 * @property integer $numQuestions
 * @property string $completed
 * @property string $created
 * @property string $deleted
 * @property integer $isdeleted
 */
class LevelTestData extends \BaseLevelTestData
{
    const _STATUS_NEW = 1;
    const _STATUS_PENDING = 2;
    const _STATUS_DONE = 3;

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LeveltestData the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public static function getNewInstance() {
        $dateTime = date("Y-m-d H:i:s");

        $model = new LeveltestData();
        $model->code = md5( time().'_levelTest_'.microtime(true) );
        $model->status = self::_STATUS_NEW;
        $model->spentTime = 0;
        $model->jsonData = '';
        $model->idLevel = 1;
        $model->numErrors = 0;
        $model->numQuestions = 0;
        $model->completed = $dateTime;
        $model->created = $dateTime;
        $model->deleted = $dateTime;
        $model->isdeleted = 0;

        return $model;
    }

}