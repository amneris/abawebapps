<?php


/**
 * This is the model class for table "leveltest_user".
 *
 * The followings are the available columns in table 'leveltest_user':
 * @property integer $id
 * @property integer $idUser
 * @property string $code
 * @property string $created
 * @property string $deleted
 * @property integer $isdeleted
 * @property string $typeleveltest
 */
class LevelTestUser extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'leveltest_user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
          array('idUser, code', 'required'),
          array('idUser, isdeleted', 'numerical', 'integerOnly' => true),
          array('code', 'length', 'max' => 255),
          array('typeleveltest', 'length', 'max' => 255),
          array('created, deleted', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
          array('id, idUser, code, created, deleted, isdeleted, typeleveltest', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
          'id' => 'ID',
          'idUser' => 'Id User',
          'code' => 'Code',
          'created' => 'Created',
          'deleted' => 'Deleted',
          'isdeleted' => 'Isdeleted',
          'typeleveltest' => 'Type Level Test'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('idUser', $this->idUser);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('created', $this->created, true);
        $criteria->compare('deleted', $this->deleted, true);
        $criteria->compare('isdeleted', $this->isdeleted);
        $criteria->compare('typeleveltest', $this->typeleveltest);

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LevelTestUser the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
