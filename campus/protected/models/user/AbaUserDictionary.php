<?php
class AbaUserDictionary extends CmAbaUserDictionary
{
    /**
     * Constructor, null implementation
     */
    public function __construct()
    {
        parent::__construct();
        $this->connection = Yii::app()->getComponent(AbaActiveRecord::DB_DEFAULT);
    }
}
