<?php
/**
 * Collection of AbaUserInvoices
 */
class ColInvoices extends AbaActiveRecord
{

    /** Used in profile controller/invoices, to display all list of potential payments to become invoices.
     *
     * @param $userId
     * @return array|bool
     */
    public function getAllInvoicesByUserId($userId)
    {
        $sql = "SELECT p.dateEndTransaction, pr.descriptionText, IF(p.`status`<>30, (-1*p.amountPrice), p.amountPrice) as amountPrice, p.currencyTrans, i.dateLastDownload,
                       p.idProduct, p.id  as `idPayment`, p.`status`, i.id, i.numberInvoice, i.usedByUser
              FROM payments p
              LEFT JOIN user_invoices i ON p.id=i.idPayment
              LEFT JOIN products_prices pr ON p.idProduct=pr.idProduct
              WHERE p.userId=" . $userId . " AND p.`status` IN (" . PAY_SUCCESS . ", " . PAY_REFUND . ")
                    AND p.paySuppExtId NOT IN (".PAY_SUPPLIER_GROUPON.", ".PAY_SUPPLIER_APP_STORE.", ".PAY_SUPPLIER_Z.")
              ORDER BY p.dateEndTransaction ASC";

        $dataRows = $this->queryAllSQL($sql);
        if (!$dataRows) {
            return false;
        }
        return $dataRows;
    }

    /** Used in profile controller/invoices, to display all list of potential payments to become invoices.
     *
     * @param $userId
     *
     * @return array|bool
     */
    public function getAllInvoicesByUserIdTest($userId)
    {
        $sql = "SELECT p.dateEndTransaction, pr.descriptionText, IF(p.`status`<>30, (-1*p.amountPrice), p.amountPrice) as amountPrice, p.currencyTrans, i.dateLastDownload,
                       p.idProduct, p.id  as `idPayment`, p.`status`, i.id, i.numberInvoice,
                       i.usedByUser, i.userStreet, i.userVatNumber, i.userZipCode, i.userCityName, i.userStateName,
                       p.idCountry,
                       i.amountPrice AS iAmountPrice, i.amountPriceWithoutTax AS iAmountPriceWithoutTax,
                       i.taxRateValue AS iTaxRateValue, i.amountTax AS iAmountTax
              FROM payments p
              LEFT JOIN user_invoices i ON p.id=i.idPayment
              LEFT JOIN products_prices pr ON p.idProduct=pr.idProduct
              WHERE p.userId=" . $userId . " AND p.`status` IN (" . PAY_SUCCESS . ", " . PAY_REFUND . ")
                    AND p.paySuppExtId NOT IN (".PAY_SUPPLIER_GROUPON.", ".PAY_SUPPLIER_APP_STORE.", ".PAY_SUPPLIER_Z.")
              ORDER BY p.dateEndTransaction ASC";

        $dataRows = $this->queryAllSQL($sql);
        if (!$dataRows) {
            return false;
        }
        return $dataRows;
    }

}
