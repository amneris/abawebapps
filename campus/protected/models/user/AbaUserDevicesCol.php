<?php
/**
 * Returns a collection of AbaUserDevice, based on a multiple criteria search.
 *
 *SELECT u.`id`, u.`userId`, u.`deviceName`, u.`sysOper`, u.`token` FROM user_devices u
WHERE u.`userId`;

 */
class AbaUserDevicesCol extends AbaActiveRecord
{

    /**
     * @param $userId
     *
     * @return AbaUserDevice[]|bool
     */
    public function getAllUserDevicesByUserId( $userId )
    {
        /* @var AbaUserDevice[] $aDevices */
        $aDevices = array();

        $sql = " SELECT ud.*
                 FROM `user_devices` ud
                 WHERE  ud.userId=:USERID
                 ORDER BY ud.`dateLastModified` DESC ";

        $aBrowserValues = array( ":USERID"=>$userId );
        $dataRows = $this->queryAllSQL( $sql, $aBrowserValues );
        if(!$dataRows)
        {
            return false;
        }

        foreach($dataRows as $row)
        {
            $moDevice = new AbaUserDevice();
            $moDevice->getDeviceByIdUserId($row['id'], $row['userId']);
            $aDevices[] = $moDevice;
        }

        return $aDevices;

    }

}