<?php
/**
 * COLLECTIONS related with [users]
 *
 */
class AbaUsers extends AbaActiveRecord
{
    /**
     * Returns list of expired users because they probably  did not pay or his promotion exceeded date off.
     * It also expires users without payments associated. This last case is a big exception as a result of the
     * import of old users from old system. Also there is a special rule for users from PAYPAL,
     * we grant them a grace period of Xdays.
     *
     * @param string $paySuppExtIdToExpire
     * @return bool|array
     */
    public function getAllUsersPremiumExpired($paySuppExtIdToExpire="")
    {
        $wPaySuppExtIdToExpire = " p.paySuppExtId IN ($paySuppExtIdToExpire) ";
        if ($paySuppExtIdToExpire == "") {
            $paySuppExtIdToExpire = PAY_SUPPLIER_ALLPAGO_MX . "," . PAY_SUPPLIER_ALLPAGO_BR . "," . PAY_SUPPLIER_CAIXA . "," .
                                    PAY_SUPPLIER_PAYPAL . "," . PAY_SUPPLIER_GROUPON."," . PAY_SUPPLIER_B2B . "," . PAY_SUPPLIER_Z;
            $wPaySuppExtIdToExpire = " p.paySuppExtId IN ($paySuppExtIdToExpire) OR  p.id IS NULL ";

        } elseif ($paySuppExtIdToExpire == PAY_SUPPLIER_XFRIENDS) {
            $wPaySuppExtIdToExpire = " p.id IS NULL ";
        }

        $sPartnerGroup = " AND abp.`idPartnerGroup` <> " . PARTNER_B2B_G . " ";

        switch ($paySuppExtIdToExpire) {
            case PAY_SUPPLIER_GROUPON:
            case PAY_SUPPLIER_XFRIENDS:
            case PAY_SUPPLIER_CAIXA:
            case PAY_SUPPLIER_ALLPAGO_BR:
            case PAY_SUPPLIER_ALLPAGO_MX:
            case PAY_SUPPLIER_Z:
                $gracePeriod = WAIT_TPV_EXPIRE_PERIOD_DAYS;
                break;
            case PAY_SUPPLIER_B2B:
                $gracePeriod = WAIT_TPV_EXPIRE_PERIOD_DAYS;
                $sPartnerGroup = "";
                break;
            case PAY_SUPPLIER_ALLPAGO_BR_BOL:
            case PAY_SUPPLIER_APP_STORE:
            case PAY_SUPPLIER_ADYEN:
                $gracePeriod = WAIT_TPV_EXPIRE_PERIOD_DAYS;
                break;
            case PAY_SUPPLIER_ADYEN_HPP:
                $gracePeriod = WAIT_ADYEN_HPP_EXPIRE_PERIOD_DAYS;
                break;
            case PAY_SUPPLIER_ANDROID_PLAY_STORE:
                $gracePeriod = WAIT_ANDROID_PLAYSTORE_EXPIRE_PERIOD_DAYS;
                break;
            case PAY_SUPPLIER_PAYPAL:
                $gracePeriod = WAIT_PAYPAL_EXPIRE_PERIOD_DAYS;
                break;
            default:
                $gracePeriod = WAIT_TPV_EXPIRE_PERIOD_DAYS;
                break;
        }

        $sql = "
        SELECT u.`userType`, DATE_FORMAT(u.`expirationDate`,'%d-%m-%Y') as 'expirationDate', u.`id`, u.`name`, u.`surnames`, u.`email`, u.`countryId`, c.`name` as 'countryName'
        FROM `user` u
        LEFT JOIN `country` c ON u.countryId=c.id
        LEFT JOIN `payments` p ON u.id=p.userId
        LEFT JOIN `aba_partners_list` abp ON u.idPartnerSource=abp.idPartner
        WHERE
            ( ".$wPaySuppExtIdToExpire." ) AND
            (u.`userType`=".PREMIUM." AND u.`userType`<>".TEACHER.")
            " . $sPartnerGroup . "
            AND DATE_ADD(DATE(u.`expirationDate`),INTERVAL ".$gracePeriod." DAY)<DATE(NOW())
            AND DATE(u.`expirationDate`)<>'0000-00-00'
        GROUP BY u.id
        ORDER BY u.expirationDate DESC; ";

        $dataRows = $this->queryAllSQL( $sql );
        if(!$dataRows) {
            return false;
        }
        return $dataRows;
    }


    /** It collects all users that meet criteria to be updated in Selligent.
     * @param int $limit Maximum of user that we want to be sent to Selligent.
     *
     * @return bool|mixed
     */
    public function getAllUsersShouldSentSelligent( $limit=1000)
    {
        $sql = " SELECT u.`id`, u.`email`, u.`userType`, u.`dateLastSentSelligent`
                FROM `user` u
                WHERE
                    ( u.userType=".PREMIUM." OR
                        (u.userType=".FREE." AND u.firstLoginDone=1)
                     )
                     AND
                    ( u.`dateLastSentSelligent` is null OR
                        u.`dateLastSentSelligent` < DATE( DATE_ADD(NOW(), INTERVAL ".INTERVAL_SELLIGENT." DAY) ))

                GROUP BY u.id

                ORDER BY u.dateLastSentSelligent ASC
                LIMIT ".$limit."; ";

        $dataRows = $this->queryAllSQL( $sql );
        if(!$dataRows){
            return false;
        }
        return $dataRows;
    }

    /**
     * @param array $stUsersIds
     *
     * @return array|bool
     */
    public function getAppstoreUsersShouldSentSelligent( $stUsersIds=array()) {

        if(count($stUsersIds) == 0) {
            return false;
        }

        $sSql = "
            SELECT u.`id`, u.`email`, u.`userType`, u.`dateLastSentSelligent`
            FROM `user` u
            WHERE u.id IN (" . implode(',', $stUsersIds) . ")
        ; ";

        $dataRows = $this->queryAllSQL( $sSql );
        if(!$dataRows) {
            return false;
        }
        return $dataRows;
    }


    /** It collects users in a period of time that have not been sent to SELLIGENT
     * @param     $entryDate
     * @param int $limit
     *
     * @return bool|mixed
     */
    public function getAllUsersToCreateInSelligent( $entryDate=NULL, $limit=1000)
    {
        $wEntryDate = "";
        $aBrowserValues = array();
        if( !empty($entryDate) ){
            $aBrowserValues = array( ":ENTRYDATE"=>$entryDate );
            $wEntryDate = " DATE(u.`entryDate`)=:ENTRYDATE AND  ";
        }

        $sql = "SELECT u.registerUserSource, u.*
                 FROM aba_b2c.`user` u
                 WHERE  u.`userType` IN (".FREE.",".PREMIUM.") AND ".
                        $wEntryDate.
                        " u.`dateLastSentSelligent` IS NULL
                  ORDER BY u.id ASC
                 LIMIT ".$limit."; ";

        $dataRows = $this->queryAllSQL($sql, $aBrowserValues, AbaActiveRecord::DB_SLAVE);
        if(!$dataRows) {
            return false;
        }
        return $dataRows;
    }

    /** Returns an array  rows for all users B2B expired.
     *
     * @return array|bool
     */
    public function getAllB2bUsersPremiumExpired()
    {
        $gracePeriod = WAIT_TPV_EXPIRE_PERIOD_DAYS;

        $sql = " SELECT u.`userType`, DATE_FORMAT(u.`expirationDate`,'%d-%m-%Y') as 'expirationDate',
                           u.`id`, u.`name`, u.`surnames`,
                           u.`email`, u.`countryId`, c.`name` as 'countryName',
                           abp.name as `namePartnerSource`, u.idPartnerSource
                FROM `user` u
                     LEFT JOIN `country` c ON u.countryId=c.id
                     LEFT JOIN `payments` p ON u.id=p.userId
                     LEFT JOIN `aba_partners_list` abp ON u.idPartnerSource=abp.idPartner
                WHERE
                   p.paySuppExtId = ".PAY_SUPPLIER_GROUPON."
                   AND u.`userType`=".PREMIUM."
                   AND abp.`idPartnerGroup` = ".PARTNER_B2B_G."
                   AND DATE_ADD(DATE(u.`expirationDate`),INTERVAL ".$gracePeriod." DAY)<DATE(NOW())
                   AND DATE(u.`expirationDate`)<>'0000-00-00'
                GROUP BY u.id
                ORDER BY u.expirationDate DESC; ";

        $dataRows = $this->queryAllSQL( $sql );
        if(!$dataRows) {
            return false;
        }
        return $dataRows;
    }

    /** Returns users that belong to PAYPAL any moment in the past.
     *
     * @param int $start
     * @param int $limit
     * @return array|bool
     */
    public function getAllUsersCheckProfilePayPal($start = 1, $limit = 10)
    {
        $sql = " SELECT u.id as userId FROM `user` u
                    INNER JOIN payments p ON u.id=p.userId
                WHERE u.`userType` IN (" . FREE . "," . PREMIUM . ") AND " .
            " p.`paySuppExtId`=" . PAY_SUPPLIER_PAYPAL .
            " AND p.`status`<>" . PAY_REFUND . " AND " .
            " DATE(p.`dateToPay`)>DATE('2013-12-01')
                GROUP BY u.id
                ORDER BY u.entryDate DESC
                LIMIT " . $start.', '. $limit . "; ";

        $dataRows = $this->queryAllSQL($sql);
        if (!$dataRows) {
            return false;
        }
        return $dataRows;
    }
}

