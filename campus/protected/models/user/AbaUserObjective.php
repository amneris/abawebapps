<?php
/**
 *
SELECT uo.`userId`, uo.`id`, uo.`travel`, uo.`estudy`, uo.`work`, uo.`others`,
    uo.`level`, uo.`selEngPrev`, uo.`engPrev`, uo.`dedication`
    FROM user_objective uo;
 */
class AbaUserObjective extends AbaActiveRecord
{
    public function tableName()
    {
        return 'user_objective';
    }



    public function mainFields()
    {
        return "  uo.`userId`, uo.`id`, uo.`travel`, uo.`estudy`, uo.`work`, uo.`others`,
                    uo.`level`, uo.`selEngPrev`, uo.`engPrev`, uo.`dedication`,
                 CONCAT(IF(uo.`travel`=1,'travel,',''),
                        IF(uo.`estudy`=1,'study,',''),
                        IF(uo.`work`=1,'work,',''),
                        IF(uo.`others`=1,'other,','') ) as `whyStudy`,
                 CASE uo.`level` WHEN '1' THEN 'medium' WHEN '2' THEN 'high' WHEN '3' THEN 'native' END as `levelToReach`,
                 CASE uo.`selEngPrev` WHEN 1 THEN 'school'
                     WHEN 2 THEN 'academy'
                     WHEN 3 THEN 'online'
                     WHEN 4 THEN 'personal class'
                     WHEN 5 THEN 'other'
                      ELSE 'no'
                      END as `previousEngExperience`,
                 CASE uo.`dedication` WHEN 1 THEN 'study' WHEN 2 THEN 'work' WHEN 3 THEN 'looking for a job' END as `occupationText`
                     ";
    }

    /**
     * @param $userId
     *
     * @return AbaUserObjective|bool
     */
    public function getObjectivesByUserId( $userId )
	{
		$sql= " SELECT  ".$this->mainFields()." FROM ".$this->tableName()." uo WHERE uo.`userId`=:USERID ";
        $paramsToSQL = array( ":USERID" => $userId );

        $dataReader=$this->querySQL($sql, $paramsToSQL);
		if(($row = $dataReader->read())!==false)
		{
            $this->fillDataColsToProperties($row);
            return $this;
		}

		return false;
	}

    /**
     * inserts a new record in user_objectives. Expect the WHOLE form as a parameter.
     * @param Paso1Form $form
     *
     * @return bool
     */
    public function saveFormObjectives(Paso1Form $form)
    {
        if( !is_null($form->getidUser()) )
        {
            $query = "INSERT INTO ".$this->tableName().
                " (userId, travel, estudy, work, level, selEngPrev, engPrev, dedication, birthDate, others)
                VALUES (:userId, :viajar, :estudiar, :trabajar, :nivel, :selEngPrev, :engPrev, :dedicacion, :birthDate, :others)";
            $params = array(":userId" => $form->getidUser(), ":viajar" => $form->getviajar(), ":estudiar" => $form->getestudiar(),
                ":trabajar" => $form->gettrabajar(), ":nivel" => $form->getnivel(), ":selEngPrev" => $form->getselEngPrev(),
                ":engPrev" => $form->getengPrev(), ":dedicacion" => $form->getdedicacion(), ":birthDate" => $form->getbirthDate(),
                ":others" => $form->getotros());
            $id= $this->executeSQL($query, $params);
            if ( $id > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        return false;
    }

    /**
     * en el caso de que consiguiera hacer la encuesta 2 o mas veces se hace un update de su anterior respuesta
     * @param Paso1Form $form
     *
     * @return bool
     */
    public function updateFormObjectives(Paso1Form $form)
    {
        if(($form->getidUser())===(Yii::app()->user->getId()))
        {
            $query = "UPDATE ".$this->tableName()." SET travel=:viajar, estudy=:estudiar, work=:trabajar, level=:nivel, selEngPrev=:selEngPrev,
                        engPrev=:engPrev, dedication=:dedicacion, birthDate=:birthDate, others=:others where userId=:userId";
            $params = array(":userId" => $form->getidUser(), ":viajar" => $form->getviajar(), ":estudiar" => $form->getestudiar(),
                ":trabajar" => $form->gettrabajar(), ":nivel" => $form->getnivel(), ":selEngPrev" => $form->getselEngPrev(),
                ":engPrev" => $form->getengPrev(), ":dedicacion" => $form->getdedicacion(), ":birthDate" => $form->getbirthDate(),
                ":others" => $form->getotros());

            if ( $this->updateSQL($query, $params )>0 )
            {
              return true;
            }
            else
            { return false; }
        }
    }

    /**
     * @param AbaUser $user
     * @param $aObjectives
     * @return bool
     */
    public function saveFormObjectivesWs(AbaUser $user, $aObjectives) {

        if(is_array($aObjectives) && count($aObjectives) > 0) {

            $model = new Paso1Form();

            $model->setidUser($user->getId());
            $model->setviajar((isset($aObjectives["viajar"]) ? $aObjectives["viajar"] : 0));
            $model->settrabajar((isset($aObjectives["trabajar"]) ? $aObjectives["trabajar"] : 0));
            $model->setestudiar((isset($aObjectives["estudiar"]) ? $aObjectives["estudiar"] : 0));
            $model->setnivel((isset($aObjectives["nivel"]) ? $aObjectives["nivel"] : 0));
            $model->setotros((isset($aObjectives["otros"]) ? $aObjectives["otros"] : 0));
            $model->setdedicacion((isset($aObjectives["dedicacion"]) ? trim($aObjectives["dedicacion"]) : 0));

            $model->setengPrev((isset($aObjectives["engPrev"]) ? $aObjectives["engPrev"] : 0));
            $model->setselEngPrev((isset($aObjectives["selEngPrev"]) ? trim($aObjectives["selEngPrev"]) : 0));

            if(isset($aObjectives["genero"])) {
                $model->setgenero((trim($aObjectives["genero"]) == '' ? null : trim($aObjectives["genero"])));
            }
            else {
                $model->setgenero(null);
            }

            $model->setbirthDate(null);

            $moUserObjectives = new AbaUserObjective();

            if (!$moUserObjectives->getObjectivesByUserId($user->getId())) {
                if ($moUserObjectives->saveFormObjectives($model) === true) {
                    $user->birthDate =  HeDate::europeanToSQL($model->getbirthDate(), false, "-");
                    $user->gender =     $model->getgenero();
                    if (!$user->updateSaveUserObjectiveFields()) {
                        HeLogger::sendLog(
                            "Fields birthDate and Gender could not be saved during questionnaire.",
                            HeLogger::IT_BUGS,
                            HeLogger::CRITICAL,
                            "For some reason user ".$user->email." could not be saved some fields during questionnaire."
                        );
                        return true;
                    }
                    $user->updateSavedUserObjective(1);
                    return true;
                } else {
                    $moUserObjectives->addError(
                      "userObjective",
                      "Some error has ocurred while inserting your questionnaire."
                    );
                }
            } else {
                if ($moUserObjectives->updateFormObjectives($model)) {
                    $user->birthDate =  HeDate::europeanToSQL($model->getbirthDate(), false, "-");
                    $user->gender =     $model->getgenero();
                    if (!$user->updateSaveUserObjectiveFields()) {
                        HeLogger::sendLog(
                          "Fields birthDate and Gender could not be saved during questionnaire.",
                          HeLogger::IT_BUGS,
                          HeLogger::CRITICAL,
                          "For some reason user ".$user->email." could not be saved some fields during questionnaire."
                        );
                        return true;
                    }
                    $user->updateSavedUserObjective(1);
                    return true;
                } else {
                    $moUserObjectives->addError(
                      "userObjective",
                      "Some error has ocurred while updating your questionnaire."
                    );
                }
            }
        }
        return false;
    }
}
