<?php
/**
 * @property integer id
 * @property integer userId
 * @property integer registered
 * @property string expireDate
 */
class AbaMemberGetMember extends AbaActiveRecord
{
    public function __construct()
    {
        parent::__construct();
    }

    public function tableName()
    {
        return 'member_get_member';
    }

    public function mainFields()
    {
        return " mgm.`id`, mgm.`userId`, mgm.`registered`, mgm.`expireDate` ";
    }

    public function getMGMByUserId($userId = NULL)
    {
        if (empty($userId)) {
            $userId = $this->userId;
        }

        $query = "SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " mgm WHERE mgm.userId = $userId";
        $dataReader = $this->querySQL($query);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return true;
        }
        return false;
    }

    public function insertUser($userId = 0)
    {
        if($userId !== 0)
        {
            $query = "INSERT INTO ".$this->tableName()." (userId, registered)
                VALUES (:USER_ID, :REGISTERED)";

            $params = array(":USER_ID" => $userId, ":REGISTERED" => 1);

            $id = $this->executeSQL($query, $params);

            return $id > 0;
        }

        return false;
    }

    public function addRegister($userId = 0)
    {
        if(!$this->getMGMByUserId($userId))
        {
            $this->insertUser($userId);
        }
        else
        {
            $query = " UPDATE ".$this->tableName()." SET `registered`= :REGISTERED
		        WHERE `userId`= :USER_ID ";

            $params = array( ':REGISTERED' => $this->registered + 1, ':USER_ID' => $this->userId);

            return $this->updateSQL($query, $params) > 0;
        }
    }

    public function premiumActivate($userId = 0)
    {
        if($this->registered >= MGM_REQUIRED_USERS)
        {
            $query = " UPDATE ".$this->tableName()." SET `expireDate`= NOW() + INTERVAL ".MGM_PREMIUM_DAYS." DAY
		        WHERE `userId`= :USER_ID ";

            $params = array(":USER_ID" => $userId);

            if($this->updateSQL($query, $params) > 0) {
                $this->refresh();
                return true;
            }
        }

        return false;
    }

    public function toArray() {

        if($this->userId === NULL) return NULL;

        $arr = array();
        $arr['userId'] = $this->userId;
        $arr['registered'] = $this->registered;
        $arr['expireDate'] = $this->expireDate;

        return $arr;
    }

    public function refresh() {
        $this->getMGMByUserId($this->userId);
    }
}