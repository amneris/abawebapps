<?php
/**
 * SELECT ups.`id`, ups.`userId`, ups.`idProduct`,
ups.`idCountry`, ups.`idPeriodPay`, ups.`priceOfficialCry`,
ups.`idCurrency`, ups.`dateCreation`, ups.`idPromoCode`,
ups.`upToDatePaid`
FROM user_products_stamp ups;
 */
class AbaUserProductStamp extends AbaActiveRecord
{
    // PRIMARY KEY
    public $id;
    // FK to [products_prices] and to [users]
    public $userId;
    public $idProduct;
    public $idCountry;
    public $idPeriodPay;
    //-------------------------

    /**
     * @param Integer $unitId
     */
    public function  __construct()
    {
        parent::__construct();
    }


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_products_stamp';
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    public function rules()
    {
        return array(
            array('idProduct',         'required'),
            array('idPeriodPay',       'required'),

            array('priceOfficialCry', 'type', 'type'=>'float','allowEmpty'=>false),
            array('idCurrency',   'type', 'type'=>'string','allowEmpty'=>false),
            array('idPromoCode',    'type', 'type'=>'string','allowEmpty'=>false),
            array('upToDatePaid',      'type', 'type'=>'integer','allowEmpty'=>false),
        );
    }

    public function mainFields()
    {
        return " ups.`id`, ups.`userId`, ups.`idProduct`,
      ups.`idCountry`, ups.`idPeriodPay`, ups.`priceOfficialCry`,
      ups.`idCurrency`, ups.`dateCreation`, ups.`idPromoCode`,
      ups.`upToDatePaid` ";
    }


    public function getProductStampByFK($userId, $idProduct, $idCountry, $idPeriodPay)
    {
         $sql = "SELECT ".$this->mainFields().
                  " FROM ".$this->tableName()." ups
                  WHERE ups.`userId`= :USERID
                    AND ups.`idProduct`=:IDPRODUCT
                    AND ups.`idCountry`= :IDCOUNTRY
                    AND ups.`idPeriodPay`= :IDPERIODPAY";
        $paramsToSQL = array(":USERID"=>$userId, ":IDPRODUCT"=>$idProduct, ":IDCOUNTRY"=>$idCountry , ":IDPERIODPAY"=>$idPeriodPay );
        $dataReader=$this->querySQL($sql,$paramsToSQL);
        if(($row = $dataReader->read())!==false)
        {
            $this->fillDataColsToProperties($row);
            return true;
        }

        return false;
    }


    public function copyProdPriceToUserProd($userId, $idProduct, $idCountry, $idPeriodPay, $idPromoCode="")
    {
        // getproductMixedKey from ProductPrices
        $package = new ProductPrice();
        if($package->getProductByMixedId($idProduct, $idCountry, $idPeriodPay, $idPromoCode))
        {
            // setUserProductStamp
            if( $this->setUserProductStamp( $userId, $package->idProduct, $package->idCountry, $package->idPeriodPay,
                        $package->getFinalPriceTransaction(), $package->ABAIdCurrency,$package->idPromoCode, 0 ) )
            {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $userId
     * @param $idProduct
     * @param $idCountry
     * @param $idPeriodPay
     * @param $priceOfficialCry Price on the country currency at the moment of purchase
     * @param $idCurrency   Currency code in which the priceOfficial is paid.
     * @param $idPromoCode
     * @param $idCurrency
     * @param $upToDatePaid If this product has been paid by the user or is pending.
     *
     * @return bool
     */
    public function setUserProductStamp($userId, $idProduct, $idCountry, $idPeriodPay,
                                        $priceOfficialCry, $idCurrency, $idPromoCode, $upToDatePaid )
    {
        /*
         * ups.`id`, ups.`userId`, ups.`idProduct`,  ups.`idCountry`, ups.`idPeriodPay`,
        ups.`priceOfficialCry`, ups.`idCurrency`, ups.`dateCreation`, ups.`idPromoCode`,
      ups.`upToDatePaid`
         * */
        if( !empty($userId) )
        {
            $this->userId = $userId;
        }
        if( !empty($idProduct) )
        {
            $this->idProduct = $idProduct;
        }
        if( !empty($idCountry) )
        {
            $this->idCountry = intval($idCountry);
        }
        if( !empty($idPeriodPay) )
        {
            $this->idPeriodPay = $idPeriodPay;
        }
        $this->priceOfficialCry = $priceOfficialCry;
        $this->idCurrency = $idCurrency;
        $this->idPromoCode = $idPromoCode;
        $this->idCurrency = $idCurrency;
        $this->upToDatePaid = $upToDatePaid;

        if( !$this->validate() )
        {
            return false;
        }

        if(strlen($idCurrency)!==3)
        {
            $this->addError("idCurrency", Yii::t('mainApp', 'currency_wrong_key'));
            return false;
        }

        return true;

    }

    public function insertUserProductStamp(  )
    {
        $this->beginTrans();
        $successTrans = true;
        //-----------------
        if($successTrans)
        {
            $sql = " INSERT INTO ".$this->tableName()."
              ( `userId`, `idProduct`,
              `idCountry`, `idPeriodPay`,
              `priceOfficialCry`, `idCurrency`,
              `dateCreation`, `idPromoCode`,
              `upToDatePaid`)
            VALUES ( :USERID , :IDPRODUCT,
                :IDCOUNTRY, :IDPERIODPAY,
                :PRICE, :IDCURRENCY,
                NOW(), :IDPROMOCODE,
                :UPTODATE )
            ON DUPLICATE KEY UPDATE `dateCreation`=NOW() ";

            $aBrowserValues = array(":USERID"=> $this->userId,
                            ":IDPRODUCT"=>      strval($this->idProduct),
                            ":IDCOUNTRY"=>      strval($this->idCountry),
                            ":IDPERIODPAY"=>    strval($this->idPeriodPay),
                            ":PRICE"=>       strval($this->priceOfficialCry),
                            ":IDCURRENCY"=> $this->idCurrency,
                            ":IDPROMOCODE"=>$this->idPromoCode,
                            ":UPTODATE"=>   $this->upToDatePaid,
            );

            $this->id = $this->executeSQL($sql, $aBrowserValues);
            if( $this->id > 0 )
            {
                $user = new AbaUser();
                if($user->getUserById( $this->userId ))
                {
                    if(!$user->abaUserLogUserActivity->saveUserLogActivity($this->tableName(),
                        "New product selected for payment process", "Payment"))
                    {
                        $successTrans= false;
                    }
                }
            }
            else
            {
                $successTrans = false;
            }
        }
        //---------------
        if( $successTrans )
        {
            $this->commitTrans();
            return true;
        }
        else
        {
            $this->rollbackTrans();
            return false;
        }
    }


}
?>