<?php

/**
 * @property integer id
 * @property string birthDate
 * @property integer cancelReason
 * @property string city
 * @property string complete
 * @property integer countryId
 * @property integer countryIdCustom
 * @property integer currentLevel
 * @property string email
 * @property string entryDate
 * @property string expirationDate
 * @property integer firstLoginDone
 * @property string gender
 * @property string hasWorkedCourse
 * @property integer idPartnerCurrent
 * @property integer idPartnerFirstPay
 * @property integer idPartnerSource
 * @property string keyExternalLogin
 * @property string langCourse
 * @property string langEnv
 * @property string name
 * @property string password
 * @property string registerUserSource
 * @property string releaseCampus
 * @property string savedUserObjective
 * @property string surnames
 * @property integer teacherId
 * @property string telephone
 * @property integer userType
 * @property string dateLastSentSelligent
 * @property integer followUpAtFirstPay
 * @property integer watchedVideosAtFirstPay
 * @property integer idSourceList
 * @property string deviceTypeSource
 * @property string dateUpdateEmma
 *
 * @property bool isNewUser
 *
 * @property string lastLoginTime
 * @property integer fbId
 * @property integer registerLevel
 *
 * @property string inId
 */
class AbaUser extends AbaActiveRecord
{
    public $id; //primary key

    public $idPartnerLimit;
    const _IDPARTNER_ECLAP = 200241;

    /**
     * @var AbaLogUserActivity
     */
    public $abaUserLogUserActivity;
    protected $errorMsg;
    protected $_md;

    // **********************--------------------------------------------------------------*****************************
    // **********************------------------------START OF YII NECESSARY FUNCTIONS------*****************************
    // **********************--------------------------------------------------------------*****************************
    /**
     *  Main constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user';
    }

    /**
     * Before the save method is executed.
     * @return array
     */
    public function rules()
    {
        return array(
          array('id', 'required'),
          array('name', 'required'),
          array('surnames', 'required'),
          array('email', 'email', 'allowEmpty' => false),
          array('birthDate', 'date', 'format' => 'dd-mm-yyyy'),
          array('countryId', 'type', 'type' => 'integer', 'allowEmpty' => true),
          array('city', 'type', 'type' => 'string', 'allowEmpty' => true),
          array('telephone', 'type', 'type' => 'string', 'allowEmpty' => true),
          array('currentLevel', 'type', 'type' => 'integer', 'allowEmpty' => true),
//            array('langEnv',      "isValidLangEnv", 'on'=>'registerUser'),
        );
    }

    /** Customized validation in Yii format. Language should be one out of 6 available.
     * @param $attribute
     * @param $params
     * @noinspection PhpUnusedParameterInspection
     */
    public function isValidLangEnv($attribute, $params)
    {
        if (!array_key_exists($this->langEnv, Yii::app()->params['languages'])) {
            $this->addError("langEnv", Yii::t('mainApp', 'langEnv_invalid_key'));
        }

    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }
    // **********************--------------------------------------------------------------*****************************
    // **********************--------------------------END OF YII NECESSARY FUNCTIONS------*****************************
    // **********************--------------------------------------------------------------*****************************


    // *****ABA customized functions ***********************************************************************************

    /** Used in config/main.php, messages Extension.
     * @return string
     */
    public function getFullName()
    {
        return $this->email;
    }

    /** Used from Messages extension
     * @param $q
     *
     * @return array
     */
    public function getSuggest($q)
    {
        $c = new CDbCriteria();
        $c->alias = 'u';
        $c->condition = " u.teacherId=" . Yii::app()->user->getId() . " AND u.email LIKE '%" . $q . "%' ";
        $c->order = ' u.name ASC';
        $users = $this->findAll($c);
        return $users;
    }


    /** All fields available in this model
     * @return string
     */
    public function mainFields()
    {
        return "u.`id`, u.`name`, u.`surnames`, u.`email`, u.`password` as `password`, u.`complete`, " .
        " u.`city`, u.`countryId`, u.`countryIdCustom`, " .
        "  DATE_FORMAT(u.birthDate,'%d-%m-%Y') as 'birthDate'," .
        " u.`telephone`, u.`langEnv`, u.`langCourse`, u.`userType`, u.`currentLevel`, " .
        " DATE_FORMAT(u.entryDate,'%d-%m-%Y') as 'entryDate', " .
        " DATE_FORMAT(u.expirationDate,'%d-%m-%Y') as 'expirationDate', " .
        " u.`teacherId`, u.`idPartnerFirstPay`, u.`idPartnerSource`, u.`idPartnerCurrent`, " .
        " u.`savedUserObjective`, u.`firstLoginDone`, u.`keyExternalLogin`, u.`gender`, " .
        " u.`cancelReason`, u.`registerUserSource`, u.`hasWorkedCourse`, u.`dateLastSentSelligent`, " .
        " u.`followUpAtFirstPay`, u.`watchedVideosAtFirstPay`, u.`idSourceList`, u.`deviceTypeSource`, " .
        " u.`dateUpdateEmma`, u.`dateUpdateEmma`, u.`fbId`, u.`registerLevel`, u.`inId` ";
    }


    /**
     * @param $password
     *
     * @return bool
     */
    public function validatePassword($password)
    {
        return $this->hashPassword($password) === $this->password;
    }

    /**
     * @param $password
     *
     * @return string
     */
    public function hashPassword($password)
    {
        return HeAbaSHA::_md5($password);
    }


    /**
     * @param $id
     */
    function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    function getId()
    {
        //---------------------------------------------------------------------------------------------------------------
        //----------------SQL functions--------------------
        //---------------------------------------------------------------------------------------------------------------

        return $this->id;
    }

    /**
     * @param int $inId
     *
     * @return bool
     */
    public function getUserById($inId = 0)
    {
        if ($inId == 0) {
            $inId = $this->id;
        }
        $query = "SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " u WHERE id=$inId";
        $dataReader = $this->querySQL($query);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            $this->abaUserLogUserActivity = new AbaLogUserActivity($this);
            return true;
        }
        return false;
    }

    /**
     * Initializes the object AbaUser based on email. Can be used to authenticate an user.
     * @param string $email
     * @param string $password
     * @param bool $usePassword
     * @param bool $pwdEncrypted
     *
     * @return bool
     */
    public function getUserByEmail($email, $password = "", $usePassword = false, $pwdEncrypted = false)
    {
        if (is_array($email)) {
            $email = $email['email'];
        }

        $verifyPassword = ($usePassword == true) ? " AND (u.`password`='" . HeAbaSHA::_md5($password) .
          "' OR u.`password`=CONCAT('(',:PASSWORD,')') )" :
          "";
        $verifyPassword = ($usePassword && $pwdEncrypted) ? " AND (u.`password`='" . $password .
          "' OR u.`password`=CONCAT('(',:PASSWORD,')') )" :
          $verifyPassword;

        $sql = "SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " u WHERE u.`email` = :EMAIL  $verifyPassword";
        $paramsToSQL = array(":EMAIL" => $email);
        if ($verifyPassword !== '') {
            $paramsToSQL[":PASSWORD"] = $password;
        }
        $dataReader = $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            $this->abaUserLogUserActivity = new AbaLogUserActivity($this);
            return true;
        }
        return false;
    }

    /**
     * Initializes the object AbaUser based on keyExternalLogin. Can be used to authenticate an user.
     * @param $keyExternalLogin
     *
     * @return bool
     */
    public function getUserByKeyExternalLogin($keyExternalLogin)
    {
        $sql = "SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " u WHERE u.`keyExternalLogin` = :KEYEXTERNALLOGIN ";
        $paramsToSQL = array(":KEYEXTERNALLOGIN" => $keyExternalLogin);

        $dataReader = $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            $this->abaUserLogUserActivity = new AbaLogUserActivity($this);
            return true;
        }
        return false;
    }

    /**
     * @param int $updateUser
     *
     * @return bool
     */
    public function insertUser($updateUser = 0)
    {
        $this->beginTrans();

        $sql = " INSERT INTO user (`name`, `surnames`, `email`, `password`, `langEnv`, `langCourse`,
                                `teacherId`, `userType`, `currentLevel`, `registerLevel`,
                                 `expirationDate`, `entryDate`,
                                `idPartnerSource`, `keyExternalLogin`, `countryId`, `countryIdCustom`,
                                `registerUserSource`, `idPartnerCurrent`, `idSourceList`, `deviceTypeSource` )
                VALUES           ( :NAME, :SURNAMES, :EMAIL, :PASSWORD, :LANGENV, :LANGCOURSE,
                                  :TEACHERID, :USERTYPE, :CURRENTLEVEL, :REGISTERLEVEL,
                                  :EXPIRATIONDATE, :ENTRYDATE,
                                  :IDPARTNERSOURCE, '" . $this->keyExternalLogin . "', :COUNTRYID, :COUNTRYIDCUSTOM,
                                  :REGISTERUSERSOURCE, :IDPARTNERCURRENT, :IDSOURCELIST, :DEVICETYPESOURCE  )
		        ON DUPLICATE KEY UPDATE
		                `name` = :NAME,
                        `surnames` = :SURNAMES,
                        `password` = :PASSWORD,
                        `langEnv` = :LANGENV,
                        `langCourse` = :LANGCOURSE,
                        `teacherId` = :TEACHERID,
                        `userType` = :USERTYPE,
                        `currentLevel`= :CURRENTLEVEL,
                        `registerLevel`= :REGISTERLEVEL,
                        `expirationDate` = :EXPIRATIONDATE,
                        `entryDate` = :ENTRYDATE,
                        `keyExternalLogin` = '" . $this->keyExternalLogin . "',
                        `countryId` = :COUNTRYID,
                        `countryIdCustom` = :COUNTRYIDCUSTOM,
                        `registerUserSource` = :REGISTERUSERSOURCE,
                        `idPartnerSource`= :IDPARTNERSOURCE,
                        `idSourceList`= :IDSOURCELIST,
                        `deviceTypeSource`= :DEVICETYPESOURCE
                        ";

        $aBrowserValues = array(
          ":EMAIL" => $this->email,
          ":PASSWORD" => $this->password,
          ":NAME" => $this->name,
          ":SURNAMES" => $this->surnames,
          ":LANGENV" => $this->langEnv,
          ":LANGCOURSE" => $this->langEnv,
          ":TEACHERID" => $this->teacherId,
          ":USERTYPE" => $this->userType,
          ":CURRENTLEVEL" => $this->currentLevel,
          ":REGISTERLEVEL" => $this->registerLevel,
          ":EXPIRATIONDATE" => $this->expirationDate,
          ":ENTRYDATE" => $this->entryDate,
          ":IDPARTNERSOURCE" => $this->idPartnerSource,
          ":COUNTRYID" => $this->countryId,
          ":COUNTRYIDCUSTOM" => $this->countryIdCustom,
          ':REGISTERUSERSOURCE' => $this->registerUserSource,
          ":IDPARTNERCURRENT" => $this->idPartnerCurrent,
          ":IDSOURCELIST" => $this->idSourceList,
          ":DEVICETYPESOURCE" => $this->deviceTypeSource
        );
        $id = $this->executeSQL($sql, $aBrowserValues);
        if ($id > 0) {
            $this->id = $id;
            $this->abaUserLogUserActivity = new AbaLogUserActivity($this);
            if ($this->abaUserLogUserActivity->saveUserLogActivity("user",
              " New user registered from createUserRegistration " . $this->email, "insertUserForRegistration")
            ) {
                $this->commitTrans();
                return true;
            } else {
                $this->rollbackTrans();
                return false;
            }
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /** Used to insert or update a new PREMIUM OR FREE User into database. Probably registered
     * through a GROUPON.
     * @return bool
     */
    public function insertUserPremiumForRegistration()
    {
        $result = $this->insertUserPremium();

        return $result;
    }

    /**
     * @return bool
     */
    public function insertUserPremium()
    {
        $this->beginTrans();

        $sql = " INSERT INTO user (`name`, `surnames`, `email`, `password`, `keyExternalLogin`, `langEnv`, `currentLevel`,
                                `teacherId`, `userType`, `expirationDate`, `entryDate`,
                                `idPartnerSource`, `idPartnerCurrent`, `idPartnerFirstPay`, `telephone`, `countryId`, `countryIdCustom`,
                                 `registerUserSource`, `cancelReason`, `deviceTypeSource` )
                VALUES           ( :NAME, :SURNAMES, :EMAIL, :PASSWORD, :KEYEXTERNALLOGIN, :LANGENV, " . $this->currentLevel . ",
                                  :TEACHERID, :USERTYPE, :EXPIRATIONDATE, :ENTRYDATE,
                                  :IDPARTNERSOURCE, :IDPARTNERCURRENT, :IDPARTNERFIRSTPAY, :TELEPHONE, :COUNTRYID, :COUNTRYID,
                                  :REGISTERUSERSOURCE, :CANCELREASON, :DEVICETYPESOURCE )
                ON DUPLICATE KEY UPDATE
                        `password` = :PASSWORD,
                        `keyExternalLogin` = :KEYEXTERNALLOGIN,
                        `expirationDate` = :EXPIRATIONDATE,
                        `name` = :NAME,
                        `surnames` = :SURNAMES,
                        `idPartnerCurrent` = :IDPARTNERCURRENT,
                        `idPartnerSource` = :IDPARTNERSOURCE,
                        `idPartnerFirstPay` = :IDPARTNERFIRSTPAY,
                        `userType` = :USERTYPE ,
                        `teacherId` = :TEACHERID,
                        `telephone` = :TELEPHONE,
                        `registerUserSource` = :REGISTERUSERSOURCE ";
        $aBrowserValues = array(
          ":NAME" => $this->name,
          ":SURNAMES" => $this->surnames,
          ":EMAIL" => $this->email,
          ":PASSWORD" => $this->password,
          ":KEYEXTERNALLOGIN" => $this->keyExternalLogin,
          ":LANGENV" => $this->langEnv,
          ":TEACHERID" => $this->teacherId,
          ":USERTYPE" => $this->userType,
          ":EXPIRATIONDATE" => $this->expirationDate,
          ":ENTRYDATE" => $this->entryDate,
          ":IDPARTNERSOURCE" => $this->idPartnerSource,
          ":IDPARTNERCURRENT" => $this->idPartnerCurrent,
          ":IDPARTNERFIRSTPAY" => $this->idPartnerFirstPay,
          ":TELEPHONE" => $this->telephone,
          ":COUNTRYID" => $this->countryId,
          ':REGISTERUSERSOURCE' => $this->registerUserSource,
          ':CANCELREASON' => $this->cancelReason,
          ':DEVICETYPESOURCE' => $this->deviceTypeSource
        );
        $id = $this->executeSQL($sql, $aBrowserValues);
        if ($id > 0) {
            $this->id = $id;
            $this->abaUserLogUserActivity = new AbaLogUserActivity($this);
            if ($this->abaUserLogUserActivity->saveUserLogActivity("user",
              " User registered from insertUserPremiumForRegistration " . $this->email,
              "insertUserPremiumForRegistration")
            ) {
                $this->commitTrans();
                return true;
            } else {
                $this->rollbackTrans();
                return false;
            }
        } else {
            $this->rollbackTrans();
            return false;
        }
    }


    /**
     * @return bool
     */
    public function updateUser()
    {
        $this->beginTrans();

        $this->birthDate = HeDate::europeanToSQL($this->birthDate, false, "-", true, true);

        $sql = " UPDATE " . $this->tableName() . " SET `name`=:NAME, `surnames`= :SURNAMES, " .
          " `birthDate` =" . $this->birthDate . ",
                                `city`= :CITY, `countryIdCustom`= :COUNTRY, `telephone`= :TELEPHONE, `currentLevel` = :CURRENTLEVEL,
                                `langCourse` = :LANGCOURSE, `langEnv` = :LANGENV, `gender` = :GENDER
		        WHERE `id`=:ID ";
        $aBrowserValues = array(
          ":ID" => $this->id,
          ":NAME" => $this->name,
          ":SURNAMES" => $this->surnames,
          ":COUNTRY" => $this->countryIdCustom,
          ":CITY" => $this->city,
          ":TELEPHONE" => $this->telephone,
          ":CURRENTLEVEL" => $this->currentLevel,
          ":LANGCOURSE" => $this->langEnv,
          ":LANGENV" => $this->langEnv,
          ":GENDER" => $this->gender
        );

        if ($this->updateSQL($sql, $aBrowserValues) > 0) {
            if ($this->abaUserLogUserActivity->saveUserChangesData($this, "Save personal information by user",
              "Modificar Datos")
            ) {
                $this->commitTrans();
                return true;
            }
        } else {
            $this->rollbackTrans();
            return false;
        }
        $this->rollbackTrans();
        return false;
    }


    /** Comes from registrationUserFromLevelTest. Updates a list of fields associated with registration process.
     * @return bool
     */
    public function updateFromRegistration()
    {
        $this->beginTrans();
        $sql = " UPDATE " . $this->tableName() .
          " SET `countryId`= :COUNTRYID,
                  `countryIdCustom`= :COUNTRYID,
                  `currentLevel` = :CURRENTLEVEL,
                  `langEnv` = :LANGENV,
                  `langCourse` = :LANGENV,
                  `userType` = :USERTYPE,
                  `teacherId` = :TEACHERID,
                  `expirationDate` = :EXPIRATIONDATE,
                  `entryDate` = :ENTRYDATE,
                  `registerUserSource` = :REGISTERUSERSOURCE,
                  `idPartnerSource` = :IDPARTNERSOURCE

		        WHERE `id`=:ID ";

        $aBrowserValues = array(
          ":ID" => $this->id,
          ":COUNTRYID" => $this->countryId,
          ":CURRENTLEVEL" => $this->currentLevel,
          ":LANGENV" => $this->langEnv,
          ":USERTYPE" => $this->userType,
          ":TEACHERID" => $this->teacherId,
          ":EXPIRATIONDATE" => $this->expirationDate,
          ":ENTRYDATE" => $this->entryDate,
          ":REGISTERUSERSOURCE" => $this->registerUserSource,
          ":IDPARTNERSOURCE" => $this->idPartnerSource,
        );

        if ($this->updateSQL($sql, $aBrowserValues) > 0) {
            if ($this->abaUserLogUserActivity->saveUserChangesData($this, "Update user from registration request," .
              " probably before was LEAD OR DELETED", "Modificar Datos")
            ) {
                $this->commitTrans();
                return true;
            }
        } else {
            $this->rollbackTrans();
            return false;
        }

        $this->rollbackTrans();
        return false;
    }

    /**
     * @return bool
     */
    public function updateUserFromPayment()
    {
        $this->beginTrans();

        $sql = "UPDATE user SET name= :NAME, surnames= :SURNAMES " .
          " WHERE id=" . $this->id . " ";
        $aBrowserValues = array(":NAME" => $this->name, ":SURNAMES" => $this->surnames);
        if ($this->updateSQL($sql, $aBrowserValues) > 0) {
            if ($this->abaUserLogUserActivity->saveUserChangesData($this,
              "Payment Form: save personal information by user ", "Modificar Datos")
            ) {
                $this->commitTrans();
                return true;
            }
        } else {
            $this->rollbackTrans();
            return false;
        }
        return false;
    }

    /**
     * @param integer $userType
     * @param null $idPartner
     *
     * @return bool
     */
    public function updateUserType($userType, $idPartner = null)
    {
        $this->beginTrans();
        if (!isset($idPartner)) {
            $idPartner = Yii::app()->config->get("ABA_PARTNERID");
        }

        $sql = " UPDATE " . $this->tableName() . " set userType=" . $userType . ", idPartnerCurrent=:IDPARTNERCURRENT  WHERE id=" . $this->id . " ";
        $aBrowserValues = array(":IDPARTNERCURRENT" => $idPartner);
        if ($this->updateSQL($sql, $aBrowserValues) > 0) {
            if ($this->abaUserLogUserActivity->saveUserChangesData($this,
              "Payment done: user paid so user type becomes " . $userType, "Modificar Datos")
            ) {
                $this->commitTrans();
                return true;
            }
        } else {
            $this->rollbackTrans();
            return false;
        }

        return false;
    }

    /** Updates only Progress field
     * @param $maxProgress
     *
     * @return bool
     */
    public function updateFollowUpAtFirstPay($maxProgress)
    {
        $this->followUpAtFirstPay = $maxProgress;

        $this->beginTrans();
        $sql = " UPDATE " . $this->tableName() . " SET followUpAtFirstPay=" . $maxProgress .
          " WHERE id=" . $this->id . " ";
        if ($this->updateSQL($sql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /** Deletes through a procedure a user
     * @return bool
     */
    public function deleteUserThroughResetCall()
    {
        $this->beginTrans();
        $this->abaUserLogUserActivity = new AbaLogUserActivity($this);

        $sql = " CALL resetUserFromCampus(:EMAIL,@userId) ";

        $aBrowserValues = array(":EMAIL" => $this->email);
        $userId = $this->executeCallSQL($sql, $aBrowserValues);
        if ($userId > 0) {
            if ($this->abaUserLogUserActivity->saveUserLogActivity("user",
              " User deleted from deleteUserThroughResetCall " . $this->email, "deleteUserThroughResetCall")
            ) {
                $this->commitTrans();
                return true;
            } else {
                $this->rollbackTrans();
                return false;
            }
        } else {
            $this->rollbackTrans();
            return false;
        }
    }


    /** Update both fields related with authentication
     * @return bool
     */
    public function updatePwd()
    {
        $this->beginTrans();
        $sql = " UPDATE `user` SET `password`='" . $this->password . "', `keyExternalLogin`='" . $this->keyExternalLogin . "' WHERE `id`=" . $this->id . " ";
        if ($this->updateSQL($sql) > 0) {
            if ($this->abaUserLogUserActivity->saveUserChangesData($this,
              "User change password: user changed the password", "Change Personal Data")
            ) {
                $this->commitTrans();
                return true;
            }
            return false;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }


    /**
     * @param $userType
     * @param $expirationDate
     * @param $idPartnerCurrent
     *
     * @return bool
     */
    public function updateUserRenewal($userType, $expirationDate, $idPartnerCurrent)
    {
        $this->beginTrans();
        $sql = " UPDATE user set `userType` = " . $userType . ", `expirationDate`='$expirationDate', `idPartnerCurrent` = '$idPartnerCurrent'
                WHERE id=" . $this->id . " ";
        if ($this->updateSQL($sql) > 0) {
            if ($this->abaUserLogUserActivity->saveUserChangesData($this,
              "Payment renewal done: user credit credential approved the renewal so user type becomes " . $userType,
              "Modificar Datos Automático")
            ) {
                $this->commitTrans();
                return true;
            }
        } else {
            $this->rollbackTrans();
            return false;
        }

        return false;
    }


    /**
     * @param integer $cancelReason
     *
     * @return bool
     */
    public function updateUserToCancelled($cancelReason)
    {
        $this->beginTrans();
        $sql = " UPDATE `user` SET userType=" . FREE . ", cancelReason=" . $cancelReason .
          " WHERE id=" . $this->id . " ";
        if ($this->updateSQL($sql) > 0) {
            if ($this->abaUserLogUserActivity->saveUserChangesData($this,
              "Payment cancelled due to failure in the renewal process or
                simply it has expired: user type becomes " . FREE, "Renewal Payment")
            ) {
                $this->commitTrans();
                return true;
            }
        } else {
            $this->rollbackTrans();
            return false;
        }

        return false;
    }

    /**
     * Updates after login has been successful for the first time
     *
     * @param $value
     *
     * @return bool
     */
    public function updateFirstLoginDone($value)
    {
        $sql = " UPDATE user SET firstLoginDone=:FIRSTLOGINDONE WHERE id=" . $this->id . " ";
        $aBrowserValues = array(":FIRSTLOGINDONE" => $value);
        if ($this->updateSQL($sql, $aBrowserValues) > 0) {
            if ($this->abaUserLogUserActivity->saveUserChangesData($this,
              "First Login: save first authentication ", "Login")
            ) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }


    /**
     * @param $value
     *
     * @return bool
     */
    public function updateHasWorkedCourse($value)
    {
        $sql = " UPDATE user SET hasWorkedCourse=:HASWORKEDCOURSE WHERE id=" . $this->id . " ";
        $aBrowserValues = array(":HASWORKEDCOURSE" => $value);
        if ($this->updateSQL($sql, $aBrowserValues) > 0) {
            if ($this->abaUserLogUserActivity->saveUserChangesData($this,
              "First detection has worked in Course", "Login")
            ) {
                return true;
            }
        } else {
            return false;
        }

        return false;
    }

    /**
     * Called after user has saved the test of objectives in the home page. Once he save it for the
     * first time, it should not appear again.
     *
     * @param $value
     *
     * @return bool
     */
    public function updateSavedUserObjective($value)
    {
        $sql = " UPDATE user SET `savedUserObjective`=:SAVEDUSEROBJECTIVE WHERE id=" . $this->id . " ";
        $aBrowserValues = array(":SAVEDUSEROBJECTIVE" => $value);
        if ($this->updateSQL($sql, $aBrowserValues) > 0) {
            if ($this->abaUserLogUserActivity->saveUserChangesData($this, "Saved user objectives ",
              "Form User Objectives")
            ) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    /** Called from the popup form user objectives, updates only 3 fields specific used in this form.
     * Called in the Home Page always til user has filled it in.
     * @return bool
     */
    public function updateSaveUserObjectiveFields()
    {
        $sql = " UPDATE `" . $this->tableName() .
          "` SET `birthDate`=:BIRTHDATE, `gender`=:GENDER  WHERE `id`=" . $this->id . " ";
		if ($this->birthDate == "null") $this->birthDate = null;
        $aBrowserValues = array(":BIRTHDATE" => $this->birthDate, ":GENDER" => $this->gender);
        if ($this->updateSQL($sql, $aBrowserValues) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /** It is used to save the date in which operation was sent to Selligent
     *
     * @param $value
     *
     * @return bool
     */
    public function updateDateLastSentSelligent($value = null)
    {
        $sql = " UPDATE user SET `dateLastSentSelligent`=:DATELASTSENTSELLIGENT WHERE id=" . $this->id . " ";

        $this->dateLastSentSelligent = $value;
        $aBrowserValues = array(":DATELASTSENTSELLIGENT" => $value);
        if ($this->updateSQL($sql, $aBrowserValues) > 0) {
            if ($this->abaUserLogUserActivity->saveUserChangesData($this,
              "Just sent an operation to selligent, save dateLastSent.", "Sent info to Selligent")
            ) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    /** It is used to save the date when idpartnersource or idsourcelist was updated by eMMa
     *
     * @param $value
     *
     * @return bool
     */
    public function updateDateSyncWithEmma($date, $idpartnersource, $idSourceList)
    {
        $sql = " UPDATE user SET `dateUpdateEmma`=:DATEUPDATEEMMA, `idpartnersource`=:IDPARTNERSOURCE, `idSourceList`=:IDSOURCELIST  WHERE id=" . $this->id . " ";

        $aBrowserValues = array(
          ":DATEUPDATEEMMA" => $date,
          ":IDPARTNERSOURCE" => $idpartnersource,
          ":IDSOURCELIST" => $idSourceList,
        );
        if ($this->updateSQL($sql, $aBrowserValues) > 0) {
            if ($this->abaUserLogUserActivity->saveUserChangesData($this,
              "Cron to sync eMMa user information with our database.", "Cron eMMa")
            ) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }


    /** Returns the user type if FREE or the Partner Group Name of PREMIUM.
     *
     * @param string $id
     *
     * @return bool|int|string
     */
    public function getUserTypePartnerStatus($id = "")
    {
        if (!isset($this->email)) {
            if (!$this->getUserById($id)) {
                return false;
            }
        }

        if ($this->userType == FREE) {
            return FREE;
        }

        if ($this->userType == PREMIUM) {
            $moPartner = new Partners();
            if (!empty($this->idPartnerCurrent)) {
                if (!$moPartner->getPartnerByIdPartner($this->idPartnerCurrent)) {
                    return PARTNER_ABA_W;
                }
                return $moPartner->nameGroup;
            } elseif (!empty($this->idPartnerFirstPay)) {
                if (!$moPartner->getPartnerByIdPartner($this->idPartnerFirstPay)) {
                    return PARTNER_ABA_W;
                }
                return $moPartner->nameGroup;
            } else {
                return PARTNER_ABA_W;
            }
        }

        return $this->userType;
    }


    /**
     * @param string $newPassword
     * @param bool $toEncrypt
     *
     * @throws CException
     */
    public function setPassword($newPassword, $toEncrypt = true)
    {
        if ($newPassword == "" || $this->email == "") {
            throw new CException("ABA email " . $this->email . " or password $newPassword are not valid. Trying to set password.");
        }

        if (!$toEncrypt) {
            $this->password = "(" . $newPassword . ")";
        } else {
            $this->password = HeAbaSHA::_md5($newPassword);
        }

        $this->keyExternalLogin = HeAbaSHA::_md5($this->email . $this->password);
    }

    /**
     * It updates the field currentLevel.
     *
     * @param $level
     *
     * @return bool|int
     */
    public function updateUserCourseLevel($level)
    {
        $this->currentLevel = $level;
        if (!is_null($level)) {
            $query = "UPDATE " . $this->tableName() . " SET `currentLevel`=:LEVEL where `id`=:USERID";
            $params = array(":LEVEL" => $this->currentLevel, ":USERID" => $this->getId());

            if ($this->updateSQL($query, $params) > 0) {
                if ($this->abaUserLogUserActivity->saveUserChangesData($this, "Change user level from objectives form.",
                  "User Objectives Form")
                ) {
                    return true;
                }

                return false;
            } else {
                return false;
            }

        }

        return true;
    }

    /**
     * @param $quantity
     *
     * @return bool
     */
    public function updateWatchedVideos($quantity)
    {
        $this->watchedVideosAtFirstPay = $quantity;

        $this->beginTrans();
        $sql = " UPDATE " . $this->tableName() . " SET watchedVideosAtFirstPay=" . $quantity .
          " WHERE id=" . $this->id . " ";
        if ($this->updateSQL($sql) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /** Calls procedure in MYSQL. Deletes all linked tables, and updates the user table in order to
     * make user deleted.
     *
     * @param integer $cancelReason
     *
     * @return bool
     */
    public function unsubscribe($cancelReason)
    {
        $this->beginTrans();
        $this->abaUserLogUserActivity = new AbaLogUserActivity($this);

        $sql = " CALL softDeleteUserFromCampus( :EMAIL, @userId )";

        $aBrowserValues = array(":EMAIL" => $this->email);
        $userId = $this->executeCallSQL($sql, $aBrowserValues);
        if ($userId > 0) {
            $this->cancelReason = $cancelReason;
            $this->update(array("cancelReason"));
            if ($this->abaUserLogUserActivity->saveUserLogActivity("user", " User unsubscribed from " .
              " softDeleteUserFromCampus " . $this->email, "softDeleteUserFromCampus")
            ) {
                $this->commitTrans();
                return true;
            } else {
                $this->rollbackTrans();
                return false;
            }
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /** Returns days between the entry date of the student and today.
     *
     * @param string $today YYYY-MM-DD SQL time format
     * @return bool|float
     */
    public function getDaysSinceRegistration($today = null)
    {
        if (empty($today)) {
            $today = HeDate::todaySQL();
        }

        if (HeDate::validDate($this->entryDate)) {
            $entryDate = HeDate::europeanToSQL($this->entryDate);
            return HeDate::getDifferenceDateSQL($today, $entryDate, HeDate::DAY_SECS, false);
        }

        return false;
    }

    /**
     * @return bool
     */
    public function updateLastLoginTime()
    {
        $this->lastLoginTime = HeDate::todaySQL(true);

        $this->beginTrans();

        $sql = " UPDATE " . $this->tableName() .
          " SET `lastLoginTime`=:LASTLOGINTIME " .
          " WHERE `id` = " . $this->id . " ";

        $aBrowserValues = array(":LASTLOGINTIME" => $this->lastLoginTime);

        if ($this->updateSQL($sql, $aBrowserValues) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @param $iFbid
     * @return bool
     */
    public function getUserByFacebookId($iFbid)
    {
        if (!is_numeric($iFbid) || $iFbid == 0) {
            return false;
        }

        $query = "SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " u WHERE fbId='$iFbid'";
        $dataReader = $this->querySQL($query);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            $this->abaUserLogUserActivity = new AbaLogUserActivity($this);
            return true;
        }
        return false;
    }

    /**
     * @param $iINid
     * @return bool
     */
    public function getUserByLinkedinId($iINid)
    {

        if (trim($iINid) == '') {
            return false;
        }

        $query = "SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " u WHERE inId='$iINid'";
        $dataReader = $this->querySQL($query);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            $this->abaUserLogUserActivity = new AbaLogUserActivity($this);
            return true;
        }
        return false;
    }

    /**
     * Consulta si el alumno puede entrar en este rango horario (inicialmente solo para B2B).
     *
     * @return bool
     */
    public function limiteHorario()
    {
        $Ndate = date('N');
        $GiDate = date('Gi');
        $idUser = $this->id;
        $idPartnerCurrent = $this->idPartnerCurrent;

        // ** Es alumno de B2B y tiene la entrada restringida en este horario ?
        $sql = " select etr.errorMsg
              from abaenglish_extranet.student s, abaenglish_extranet.enterprise e, abaenglish_extranet.enterprise_timerange etr
              where s.idStudentCampus = :idUser
                and s.idEnterprise = e.id
                and e.idPartner = :idPartner
                and etr.idEnterprise = e.id
                and etr.diasemana = :diasemana
                and etr.horainicio <= :hora
                and etr.horafin > :hora
                and etr.isdeleted = 0
              limit 1";

        $connection = Yii::app()->dbExtranet;
        $command = $connection->createCommand($sql);
        $command->bindParam(":idUser", $idUser, PDO::PARAM_INT);
        $command->bindParam(":idPartner", $idPartnerCurrent, PDO::PARAM_INT);
        $command->bindParam(":diasemana", $Ndate, PDO::PARAM_INT);
        $command->bindParam(":hora", $GiDate, PDO::PARAM_INT);
        $this->errorMsg = $command->query()->readColumn(0);

        if ($this->errorMsg !== false) {
            return $this->errorMsg;
        } else {
            return true;
        }

    }

    /**
     * @return bool
     */
    public function updateUserTeacherId()
    {
        $this->beginTrans();

        $sSql = "
            UPDATE " . $this->tableName() . "
            SET `teacherId` = :TEACHERID
            WHERE `id` = " . $this->id . "
        ";
        $aBrowserValues = array(":TEACHERID" => $this->teacherId);

        if ($this->updateSQL($sSql, $aBrowserValues) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function updateUserTeacher()
    {
        $this->beginTrans();

        $sSql = "
            UPDATE " . $this->tableName() . "
            SET
                `name` =                :NAME,
                `surnames` =            :SURNAMES,
                `langEnv` =             :LANGENV,
                `langCourse` =          :LANGCOURSE,
                `countryId` =           :COUNTRYID,
                `countryIdCustom` =     :COUNTRYIDCUSTOM,
                `deviceTypeSource` =    :DEVICETYPESOURCE,
                `password` =            :PASSWORD,
                `keyExternalLogin` =    :KEYEXTERNALLOGIN
            WHERE
                `id` = " . $this->id . "
        ";
        $aBrowserValues = array(
          ":NAME" => $this->name,
          ":SURNAMES" => $this->surnames,
          ":LANGENV" => $this->langEnv,
          ":LANGCOURSE" => $this->langEnv,
          ":COUNTRYID" => $this->countryId,
          ":COUNTRYIDCUSTOM" => $this->countryIdCustom,
          ":DEVICETYPESOURCE" => $this->deviceTypeSource,
          ":PASSWORD" => $this->password,
          ":KEYEXTERNALLOGIN" => $this->keyExternalLogin,
        );

        if ($this->updateSQL($sSql, $aBrowserValues) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function changeUserLicense()
    {
        $this->beginTrans();

        $sSql = "
            UPDATE " . $this->tableName() . "
            SET
                `teacherId` =           :TEACHERID,
                `idPartnerCurrent` =    :IDPARTNERCURRENT
            WHERE
                `id` = " . $this->id . "
        ";
        $aBrowserValues = array(
          ":TEACHERID" => $this->teacherId,
          ":IDPARTNERCURRENT" => $this->idPartnerCurrent,
        );

        if ($this->updateSQL($sSql, $aBrowserValues) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

    /**
     * @TODO - translations
     *
     * @param $sGender
     *
     * @return string
     */
    public static function getGenderDescription($sGender)
    {

        $sGenderDescription = "";

        switch ($sGender) {
            case "M":
            case "m":
                $sGenderDescription = "Male";
                break;
            case "F":
            case "f":
                $sGenderDescription = "Female";
                break;
        }
        return $sGenderDescription;
    }

}
