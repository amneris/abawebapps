<?php
/**
 * We can store also
 *regionName = California
City = Mountain View
postalCode = 94043
37.4192
-122.0574
AreaCode=650
dmaCode=807

SELECT  `id`, `userId`, `ip`, `longitude`, `latitude`, `city`, `region`, `default`  FROM user_locations;
 * */
/*
 *
 * @property integer userId
 * @property string  ip
 * @property double longitude
 * @property double latitude
 * @property string isoCountryCode
 * @property string isoRegionCode
 * @property string city
 * @property integer default
 */
class AbaUserLocation extends AbaActiveRecord
{

    /** Compulsory for Yii framework
     * @return string
     */
    public function tableName()
    {
        return 'user_locations';
    }

    /** Compulsory for Yii framework
     * @return string
     */
    public function mainFields()
    {
        return " `userId`, `ip`, `longitude`, `latitude`, `isoCountryCode`, `isoRegionCode`, `city`, `default`  ";
    }

    /**
     * @param $userId
     * @param string $ipLoc
     * @param int $default
     * @return $this|bool
     */
    public function getLocationByIdUserIdIp($userId, $ipLoc='', $default=1)
    {
        $wIp = "";
        if ($ipLoc !== "") {
            $wIp = " AND ul.ip='" . $ipLoc . "'";
        }

        $wDef = "";
        if ($default == "1") {
            $wDef = " AND ul.default=$default ";
        }

        $sql = " SELECT  " . $this->mainFields() . " FROM " . $this->tableName() .
            " ul WHERE  ul.`userId`=:USERID  " . $wIp . $wDef;

        $paramsToSQL = array(":USERID" => $userId);

        $dataReader = $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }


        return false;
    }


    /** Inserts a new record into database. Returns true if successful.
     * @param $userId
     * @param $ipLocation
     * @param $longitude
     * @param $latitude
     * @param $isoCountryCode,
     * @param $isoRegionCode
     * @param $city
     * @param int $default
     *
     * @return bool
     */
    public function insertNewLocation($userId, $ipLocation, $longitude, $latitude, $isoCountryCode, $isoRegionCode,
                                                                                                $city, $default=1)
    {
        $sql = "INSERT INTO " . $this->tableName() .
            " ( userId, ip, longitude, latitude,  `isoCountryCode`, `isoRegionCode`, `city`, `default` )
               VALUES ( :USERID, :IP, :LONGITUDE, :LATITUDE, :ISOCOUNTRYCODE, :ISOREGIONCODE, :CITY, $default )
               ON DUPLICATE KEY UPDATE latitude=:LATITUDE,
                                    longitude=:LONGITUDE,
                                    isoCountryCode = :ISOCOUNTRYCODE,
                                    isoRegionCode = :ISOREGIONCODE,
                                    city=:CITY,
                                    `default`= $default;";

        $params = array( ":USERID" => $userId, ":IP" => $ipLocation,":LONGITUDE" => $longitude,
                         ":LATITUDE" => $latitude, ":ISOCOUNTRYCODE" => $isoCountryCode,
                         ":ISOREGIONCODE" => $isoRegionCode, ":CITY" => $city);
        $returnId = $this->executeSQL($sql, $params);
        if ($returnId > 0) {
            return true;
        }

        return false;
    }


    /** Only one default location can be possible. Everytime a new one is inserted the rest are updated to 0.
     *
     * @return bool
     */
    public function updateRestNoDefault()
    {
        $sql = " UPDATE   " . $this->tableName() ." ul SET ul.default = 0 ".
                " WHERE ul.default =1 AND ul.userId=".$this->userId.";";

        if ( $this->updateSQL( $sql ) > 0 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * @param integer|null $userId
     *
     * @return bool|string
     */
    public function getLastIp($userId=NULL)
    {
        if( empty($userId)){
            $userId = $this->userId;
        }

        $sql = " SELECT l.ip as `lastIp` FROM ".$this->tableName()." l
                 WHERE l.`userId`=$userId  AND l.`default`=1  ORDER BY l.`id` DESC LIMIT 1 ";

        $dataReader=$this->querySQL($sql);
        if (($row = $dataReader->read()) !== false) {
            return $row["lastIp"];
        } else {
            $sql = " SELECT l.ip as `lastIp` FROM ".$this->tableName()." l
                 WHERE l.`userId`=$userId ORDER BY l.`id` DESC LIMIT 1 ";
            $dataReader=$this->querySQL($sql);
            if (($row = $dataReader->read()) !== false) {
                return $row["lastIp"];
            }
        }

        return false;
    }

}