<?php
/**
 *
    SELECT  u.id, u.name, u.surnames, u.email, u.password, u.complete, u.countryId,
        DATE_FORMAT(u.birthDate,'%d-%m-%Y') as 'birthDate', u.telephone,
        ut.nationality, ut.language, ut.photo
        FROM user u
        LEFT JOIN user_teacher ut ON u.id=ut.userId
        WHERE ua.userType=4;
 */
class AbaTeacher extends AbaUser
{
    // **********************--------------------------------------------------------------*****************************
    // **********************------------------------START OF YII NECESSARY FUNCTIONS------*****************************
    // **********************--------------------------------------------------------------*****************************
    /**
     * Initialize default values
     */
    public function __construct($userId=null)
    {
        parent::__construct();
        if(isset($userId))
        {
            return $this->getTeacherById($userId);
        }
    }

    // **********************--------------------------------------------------------------*****************************
    // **********************--------------------------END OF YII NECESSARY FUNCTIONS------*****************************
    // **********************--------------------------------------------------------------*****************************
	/**
	example:
		$teacher = new AbaTeacher();
		$teacher->setIdLearner(63008);
		if($teacher->getUserByEmail("miemail@campo.es",  md5("password"), true))  == login de usuario se busca email/password
		{
			echo(
				"<br>".$teacher->getIdLearner()."<br>".
				$teacher->name."<br>".
				$teacher->surnames."<br>".
				$teacher->email."<br>".
				$teacher->password."<br>".
				$teacher->complete."<br>".
				$teacher->idCountry
			);
		}
		else
		{
			echo("NO found");	
		}
	**/
    /**
     * @param int $inTeacherId
     *
     * @return AbaTeacher|bool
     */
    function getTeacherById($inTeacherId=0)
	{
		if($inTeacherId == 0) {
			$inTeacherId = $this->id;
        }
        if(!is_numeric($inTeacherId)) {
            $inTeacherId = 0;
        }
		$sql= "SELECT ".$this->mainFields().", ut.nationality, ut.language, ut.photo, ut.mobileImage2x, ut.mobileImage3x " .
            " FROM " . $this->tableName() . " u " .
            " LEFT JOIN user_teacher ut ON u.id=ut.userId " .
            " WHERE u.id='$inTeacherId' AND u.userType=" . TEACHER . ";";
        $dataReader=$this->querySQL($sql);
		if(($row = $dataReader->read())!==false) {
            foreach($row as $fieldName=>$value) {
                $this->$fieldName = $value;
            }
			return $this;
		}
		return false;
	}

    /**
     * @param string $lang
     * 
     * @return $this|bool
     */
    function getTeacherDefaultByLang( $lang='en' )
    {
        $lang = ucfirst($lang);
        $sql= "SELECT ".$this->mainFields().", ut.nationality, ut.language, ut.photo, ut.mobileImage2x, ut.mobileImage3x ".
            " FROM ".$this->tableName()." u ".
            " LEFT JOIN user_teacher ut ON u.id=ut.userId ".
            " WHERE u.userType=".TEACHER." AND ut.language LIKE '%$lang%' LIMIT  1 ;";

        $dataReader = $this->querySQL( $sql );
        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $userId
     *
     * @return array|bool|false
     */
    function existsTeacher($userId) {
        $sSql =         "SELECT ut.* FROM user_teacher AS ut WHERE ut.userid='" . (int) $userId . "' ";
        $dataReader =   $this->querySQL($sSql);
        if(($row = $dataReader->read()) !== false) {
            return $row;
        }
        return false;
    }

    /**
     * @param $teacherData
     *
     * @return bool
     */
    public function insertTeacher($teacherData)
    {

        $this->beginTrans();

        $sSql = "
            INSERT INTO `user_teacher`  ( `userid`, `nationality`, `language`, `photo`, `mobileImage2x`, `mobileImage3x`, `registerTeacherSource` )
            VALUES                      ( :USERID, :NATIONALITY, :LANGUAGE, :PHOTO, :MOBILEIMAGE2X, :MOBILEIMAGE3X, :REGISTERTEACHERSOURCE )
            ON DUPLICATE KEY UPDATE
                `nationality` =           :NATIONALITY,
                `language` =              :LANGUAGE,
                `photo` =                 :PHOTO,
                `mobileImage2x` =         :MOBILEIMAGE2X,
                `mobileImage3x` =         :MOBILEIMAGE3X
        ";

        $aBrowserValues = array(
            ":USERID" =>                $teacherData['userid'],
            ":NATIONALITY" =>           $teacherData['nationality'],
            ":LANGUAGE" =>              $teacherData['language'],
            ":PHOTO" =>                 $teacherData['photo'],
            ":MOBILEIMAGE2X" =>         $teacherData['mobileImage2x'],
            ":MOBILEIMAGE3X" =>         $teacherData['mobileImage3x'],
            ":REGISTERTEACHERSOURCE" => $teacherData['registerTeacherSource'],
        );

        $id = $this->executeSQL($sSql, $aBrowserValues);

        if ( $id > 0) {
            $this->commitTrans();
            return true;
        }
        else {
            $this->rollbackTrans();
            return false;
        }
    }


}
