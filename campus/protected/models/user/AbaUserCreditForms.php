<?php
/**
 * SELECT ucf.`userId`, ucf.`id`, ucf.`kind`, ucf.`cardNumber`, ucf.`cardYear`, ucf.`cardMonth`, ucf.`cardCvc`,
 *                  ucf.`cardName`, ucf.`cardLastName`, ucf.`default` FROM user_credit_forms ucf;
 */
/**
 * Class AbaUserCreditForms
 *
 * @property integer userId
 * @property integer id
 * @property integer kind
 * @property integer cardNumber
 * @property integer cardYear
 * @property integer cardMonth
 * @property integer cardCvc
 * @property integer cardName
 * @property integer default
 * @property integer cpfBrasil
 * @property string registrationId
 * @property string typeCpf
 * @property string lastModified
 *
 */
class AbaUserCreditForms extends AbaActiveRecord
{
    public $id;

    private $word4Cards;

    /**
     * @param null $userId
     */
    public function __construct($userId=null)
    {
        parent::__construct();
        $this->word4Cards = HeMixed::getWord4Card();
        if (!is_null($userId)) {
            return $this->getUserCreditFormByUserId($userId);
        }
        return $this;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_credit_forms';
    }

    /**
     * Before the save method is executed.
     * @return array
     */
    public function rules()
    {
        return array(
            array('userId',       'required'),
//            array('kind',         'required'),
            array('cardNumber',   'isValidCreditNumber'),
            array('cardName',     'type', 'type'=>'string','allowEmpty'=>false),
            array('cardCvc',      'isValidTPVRequired')
        );
    }


    public function mainFields()
    {
        return "  ucf.`userId`, ucf.`id`, ucf.`kind`,
                    AES_DECRYPT(ucf.`cardNumber`,'".$this->word4Cards."') as `cardNumber`,
                    AES_DECRYPT(ucf.`cardYear`,'".$this->word4Cards."')  as `cardYear`,
                    AES_DECRYPT(ucf.`cardMonth`,'".$this->word4Cards."') as `cardMonth`,
                    AES_DECRYPT(ucf.`cardCvc`,'".$this->word4Cards."') as `cardCvc`,
                    ucf.`cardName`, ucf.`default`,
                    AES_DECRYPT(ucf.`cpfBrasil`,'".$this->word4Cards."') as `cpfBrasil`,
                    ucf.`registrationId`,
                    ucf.`typeCpf`, ucf.`lastModified` ";
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function isValidCreditNumber($attribute, $params)
    {
        if ( (intval($this->kind) !== KIND_CC_PAYPAL) && !HeMixed::isValidCreditCard($this->cardNumber)) {
            $this->addError("cardNumber", ' Credit card number is incorrect');
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function isValidTPVRequired($attribute, $params)
    {
        if (intval($this->kind) !== KIND_CC_PAYPAL && intval($this->kind) !== KIND_CC_BOLETO && intval($this->kind) !== KIND_CC_NO_CARD) {
            if (trim($this->$attribute) == '') {
                switch ($attribute) {
                    case "cardNumber":
                        $key = 'Debes seleccionar el tipo de tarjeta';
                        break;
                    case "cardYear":
                        $key = 'Debes introducir el nombre del titular de la tarjeta';
                        break;
                    case "cardMonth":
                        $key = 'Debes seleccionar el mes de caducidad de la tarjeta';
                        break;
                    case "cardCvc":
                        $key = 'Debes seleccionar el año de caducidad de la tarjeta';
                        break;
                    case "cardName":
                        $key = 'Debes introducir el código CVC';
                        break;
                }
                $this->addError($attribute, Yii::t('mainApp', 'required'));
            }
            if ($attribute == 'cardCvc') {
                if (strlen($this->$attribute) <= 2 || strlen($this->$attribute) >= 5) {
                    $this->addError( 'cardCvc', Yii::t('mainApp', 'cvc_notvalid_key'));
                }
            }
        }
    }

    /**
     * SQL FUNCTIONS ---------------------------------------------------------------------------
     */
    /**
     * @param $userId
     *
     * @return AbaUserCreditForms|bool
     * @throws CDbException
     */
    public function getUserCreditFormByUserId( $userId )
    {
        if (empty($userId)) {
            return false;
        }

        $query = "SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " ucf
                    WHERE ucf.userId=:USERID  AND ucf.default=1 ";
        $paramsToSQL = array(":USERID" => $userId);

        $dataReader = $this->querySQL($query, $paramsToSQL);
        if ($dataReader->rowCount > 1) {
            throw new CDbException("ABA: It is not allowed to have multiple credit cards or forms of payment active and default.");
        }
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /** Returns the user credit form by its id.
     *
     * @param $id
     * @return $this|bool
     * @throws CDbException
     */
    public function getUserCreditFormById( $id )
    {
        if (empty($id)) {
            return false;
        }
        $query = "SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " ucf
                    WHERE ucf.id=".$id;
        $dataReader = $this->querySQL($query);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /** Sets all data to the model, just in memory
     * @param $userId
     * @param $kind
     * @param $cardNumber
     * @param $cardYear
     * @param $cardMonth
     * @param $cardCvc
     * @param $cardName
     * @param int $default
     * @param string $cpfBrasil
     * @param string $registrationId
     * @param string $typeCpf
     *
     * @return bool
     */
    public function setUserCreditForm($userId, $kind, $cardNumber,$cardYear, $cardMonth, $cardCvc, $cardName,
                                      $default=1, $cpfBrasil='', $registrationId='', $typeCpf='' )
    {
        $this->userId =         $userId;
        $this->kind =           $kind;
        $this->cardNumber =     $cardNumber;
        $this->cardYear =       $cardYear;
        $this->cardMonth =      substr("00" . $cardMonth, -2);
        $this->cardCvc =        $cardCvc;
        $this->cardName =       $cardName;
        $this->default =        $default;
        $this->cpfBrasil =      $cpfBrasil;
        $this->registrationId = $registrationId;
        $this->typeCpf =        $typeCpf;

        if (!$this->validate()) {
            return false;
        }
        return true;

    }

    /**
     * @param $userId
     * @param $kind
     * @param $cardNumber
     * @param $cardYear
     * @param $cardMonth
     * @param $cardCvc
     * @param $cardName
     * @param int $default
     * @param string $cpfBrasil
     * @param string $registrationId
     * @param string $typeCpf
     *
     * @return bool
     */
    public function setUserCreditFormFromPayment($userId, $kind, $cardNumber='', $cardYear='', $cardMonth='', $cardCvc='', $cardName='',
                                      $default=1, $cpfBrasil='', $registrationId='', $typeCpf='' )
    {
        $this->userId =         $userId;
        $this->kind =           $kind;
        $this->cardNumber =     $cardNumber;
        $this->cardYear =       $cardYear;
        if(trim($cardMonth) == '') {
            $this->cardMonth =  $cardMonth;
        }
        else {
            $this->cardMonth =  substr("00" . $cardMonth, -2);
        }
        $this->cardCvc =        $cardCvc;
        $this->cardName =       $cardName;
        $this->default =        $default;
        $this->cpfBrasil =      $cpfBrasil;
        $this->registrationId = $registrationId;
        $this->typeCpf =        $typeCpf;

        if (!is_numeric($this->userId) OR !is_numeric($this->kind)) {
            return false;
        }
        return true;

    }
    /**
     * @param integer $userId
     * @return bool
     */
    public function insertUserCreditFormByUserId( $userId=null )
    {
        if (empty($userId)) {
            $userId = $this->userId;
        }


        $this->beginTrans();
        $successTrans = true;
        //-----------------
        if ($this->default) {
            $sql = " UPDATE " . $this->tableName() . " set `default`=0 WHERE `userId`=" . $userId . " AND `default`=1 ";
            if (!$this->updateSQL($sql)) {
                $successTrans = false;
            }
        }

        if ($successTrans) {
            $sql = " INSERT INTO  " . $this->tableName() . " " .
                " (`userId`, `kind`, `cardNumber`, `cardYear`, `cardMonth`, `cardCvc`, `cardName`,
                   `default`, `cpfBrasil`, `registrationId`, `typeCpf` )" .
                " VALUES (" . $userId . ",
                    :KIND,
                    AES_ENCRYPT(:CARDNUMBER,'" . $this->word4Cards . "'),
                    AES_ENCRYPT(:CARDYEAR,'" . $this->word4Cards . "'),
                    AES_ENCRYPT(:CARDMONTH,'" . $this->word4Cards . "'),
                    AES_ENCRYPT(:CARDCVC,'" . $this->word4Cards . "'),
                    :CARDNAME,
                    " . $this->default . ",
                    AES_ENCRYPT(:CPFBRASIL,'" . $this->word4Cards . "'),
                    :REGISTRATIONID,
                    :TYPECPF
                    )";
            $aBrowserValues = array(":KIND" => $this->kind,
                ":CARDNUMBER" => strval($this->cardNumber),
                ":CARDYEAR" => strval($this->cardYear),
                ":CARDMONTH" => strval($this->cardMonth),
                ":CARDCVC" => strval($this->cardCvc),
                ":CARDNAME" => $this->cardName,
                ":CPFBRASIL" => $this->cpfBrasil,
                ":REGISTRATIONID" => $this->registrationId,
                ":TYPECPF" => $this->typeCpf
            );

            $this->id = $this->executeSQL($sql, $aBrowserValues);
            if ($this->id > 0) {
                $user = new AbaUser();
                if ($user->getUserById($userId)) {
                    if (!$user->abaUserLogUserActivity->saveUserLogActivity($this->tableName(),
                        "New credit card inserted, default " . $this->default,
                        "Payment")
                    ) {
                        $successTrans = false;
                    }
                }
            } else {
                $successTrans = false;
            }
        }
        //---------------
        if ($successTrans) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /** Used to find a credit card by the email from PAYPAL
     *
     * @param string $cardName
     * @return $this|bool
     */
    public function getUserCreditFormByCardName($cardName)
    {
        $query = "SELECT " . $this->mainFields() .
                " FROM " . $this->tableName() . " ucf ".
                " WHERE ucf.cardName='".$cardName."' ";
        $dataReader = $this->querySQL($query);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }
}
