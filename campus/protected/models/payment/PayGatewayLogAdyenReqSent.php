<?php
/**
 * Class PayGatewayLogAdyenReqSent
 */
/*
    @property integer id
    @property string idPayment
    @property string dateSent
    @property string merchantAccount
    @property string amountValue
    @property string amountCurrency
    @property string reference
    @property string originalReference
    @property string shopperIP
    @property string userId
    @property string shopperEmail
    @property string shopperReference
    @property string fraudOffset
    @property string additionalData
    @property string selectedRecurringDetailReference
    @property string recurringContract
    @property string typeGateway
 */

class PayGatewayLogAdyenReqSent extends PayGatewayLog
{
    /**
     * @return string
     */
    public function tableName() {
        return "pay_gateway_adyen_sent";
    }

    /**
     * @return bool
     */
    public function insertRequestSent() {

        $this->beginTrans();

        $successTrans = true;

        $sSql= " INSERT INTO " . $this->tableName() . " (
                    `idPayment`, `dateSent`, `merchantAccount`, `amountValue`,
                    `amountCurrency`, `reference`, `originalReference`, `shopperIP`, `userId`, `shopperEmail`,
                    `shopperReference`, `fraudOffset`, `additionalData`,
                    `selectedRecurringDetailReference`, `recurringContract`, `typeGateway`
                ) ".
            " VALUES (
                    :IDPAYMENT, :DATESENT, :MERCHANTACCOUNT, :AMOUNTVALUE,
                    :AMOUNTCURRENCY, :REFERENCE, :ORIGINALREFERENCE, :SHOPPERIP, :USERID, :SHOPPEREMAIL,
                    :SHOPPERREFERENCE, :FRAUDOFFSET, :ADDITIONALDATA,
                    :SELECTEDRECURRINGDETAILREFERENCE, :RECURRINGCONTRACT, :TYPEGATEWAY
                )";

        $aParamValues= array(
            ":IDPAYMENT" =>         $this->idPayment,
            ":DATESENT" =>          $this->dateSent,
            ":MERCHANTACCOUNT" =>   $this->merchantAccount,
            ":AMOUNTVALUE" =>       $this->amountValue,
            ":AMOUNTCURRENCY" =>    $this->amountCurrency,
            ":REFERENCE" =>         $this->reference,
            ":ORIGINALREFERENCE" => $this->originalReference,
            ":SHOPPERIP" =>         $this->shopperIP,
            ":USERID" =>            $this->userId,
            ":SHOPPEREMAIL" =>      $this->shopperEmail,
            ":SHOPPERREFERENCE" =>  $this->shopperReference,
            ":FRAUDOFFSET" =>       $this->fraudOffset,
            ":ADDITIONALDATA" =>    $this->additionalData,

            ":SELECTEDRECURRINGDETAILREFERENCE" =>  $this->selectedRecurringDetailReference,
            ":RECURRINGCONTRACT" =>                 $this->recurringContract,
            ":TYPEGATEWAY" =>                       $this->typeGateway,
        );

        $this->id = $this->executeSQL($sSql, $aParamValues);
        if( $this->id <= 0 ) {
            $successTrans = false;
        }
        //---------------
        if( $successTrans ) {
            $this->commitTrans();
            return true;
        }
        else {
            $this->rollbackTrans();
            return false;
        }
    }

    /** Returns the unique id of the gateway supplier platform. Usually "their transaction identification"
     * Each platform returns its value in several ways.
     * @return mixed
     */
    public function getUniqueId() {
        return $this->reference;
    }

    /**
     * @return string
     */
    public function mainFields() {
        return "
            als.`id`, als.`idPayment`, als.`dateSent`, als.`merchantAccount`, als.`amountValue`,
            als.`amountCurrency`, als.`reference`, als.`originalReference`, als.`shopperIP`, als.`userId`,
            als.`shopperEmail`, als.`shopperReference`, als.`fraudOffset`, als.`additionalData`, als.`selectedRecurringDetailReference`,
            als.`recurringContract`, als.`typeGateway`
        ";
    }

    /**
     * @param $reference
     *
     * @return $this|bool
     */
    public function getLogByReference($reference) {
        $sSql =     "SELECT " . $this->mainFields() . " ";
        $sSql .=    "FROM " . $this->tableName() . " als ";
        $sSql .=    "WHERE als.`reference` = :REFERENCE ";
        $sSql .=    "ORDER BY id DESC  ";
        $sSql .=    "LIMIT 0, 1 ";

        $paramsToSQL =  array(":REFERENCE" => $reference);
        $dataReader =   $this->querySQL($sSql, $paramsToSQL);
        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $id
     *
     * @return $this|boolç
     */
    public function getLogById($id) {
        $sSql =     "SELECT " . $this->mainFields() . " ";
        $sSql .=    "FROM " . $this->tableName() . " als ";
        $sSql .=    "WHERE als.`id` = :ID ";
        $sSql .=    "ORDER BY id DESC  ";
        $sSql .=    "LIMIT 0, 1 ";

        $paramsToSQL =  array(":ID" => $id);
        $dataReader =   $this->querySQL($sSql, $paramsToSQL);
        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $responseData
     *
     * @return bool
     */
    public function validateLogByResponse($responseData, $bValidateSig=true) {

//        if($bValidateSig && trim($this->additionalData) != trim($responseData['merchantSig'])) {
//            return false;
//        }

        if(trim($this->reference) != trim($responseData['merchantReference'])) {
            return false;
        }

        if(trim($this->originalReference) != trim($responseData['skinCode'])) {
            return false;
        }

        return true;
    }

    /** @TODO
     *
     * @return mixed|string
     */
    public function getPaySuppExtProfId() {
        return "";
    }

}
