<?php
/**
 * @property integer id
 * @property string name
 * @property string description
 *
 */
class SourcesList extends AbaActiveRecord
{

    public function tableName()
    {
        return "sources_list";
    }


    /** Compulsory for Yii framework
     * @return string
     */
    public function mainFields()
    {
        return " s.* ";
    }


    /** Loads and Returns model for the table above.
     * @param integer $sourceId
     *
     * @return $this|bool
     */
    public function getSourceById( $sourceId )
    {
        $sql = " SELECT  " . $this->mainFields() . " FROM " . $this->tableName() .
            " s WHERE s.id=:SOURCEID ";
        $paramsToSQL = array( ":SOURCEID" => $sourceId );

        $dataReader = $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }

        return false;
    }


}
