<?php
/**
 * Created by PhpStorm.
 * User: akorotkov
 * Date: 27/11/14
 * Time: 17:49
 */
class RenewalsPlans extends CmAbaRenewalsPlans {

    /**
     * Constructor, null implementation
     */
    public function __construct()
    {
        parent::__construct();
        $this->connection = Yii::app()->getComponent(AbaActiveRecord::DB_DEFAULT);
    }

}