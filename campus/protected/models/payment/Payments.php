<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 22/11/12
 * Time: 17:07
 *
 *
 *
 * Collection of Payments
 * QUERIES WITH MULTIPLE MODELS / ROWS
 */
class Payments extends AbaActiveRecord
{

    public function mainFields()
    {
        return "
         p.`id`, p.`idUserProdStamp`, p.`userId`, p.`idProduct`, p.`idCountry`, p.`idPeriodPay`,
            p.`idUserCreditForm`, p.`paySuppExtId`, p.`paySuppOrderId`, p.`status`, p.`dateStartTransaction`,
            p.`dateEndTransaction`, p.`dateToPay`, p.`xRateToEUR`, p.`xRateToUSD`, p.`currencyTrans`,
            p.`foreignCurrencyTrans`, p.`idPromoCode`, p.`amountOriginal`, p.`amountDiscount`, p.`amountPrice`,
            p.`idPayControlCheck`, p.`idPartner`, p.`paySuppExtProfId`
        ";
    }

    /**
     * @return bool|Payment[]
     * @throws CDbException
     */
    public function getAllPaymentsPendingLaCaixa()
    {
        $aPayments = array();

        $sql = "SELECT p.dateToPay, p.* FROM payments p
                    LEFT JOIN `user` u ON p.userId=u.id
                    WHERE u.userType=2 AND
                          p.`paySuppExtId` = '" . PAY_SUPPLIER_CAIXA . "'
                          AND p.`status` = " . PAY_PENDING . "
                          AND p.`dateToPay` <= CONCAT( DATE(NOW()),' 00:00:00')
                     ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC ;";

        $dataReader = $this->querySQL($sql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }

            $aPayments[] = $moPayment;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        /* @var Payment[] $aPayments */
        return $aPayments;
    }


    /**
     * @return bool|Payment[]
     * @throws CDbException
     */
    public function getAllPaymentsPendingLaCaixaFails($sDateFrom, $sDateTo)
    {
        $aPayments = array();

        $sql = "
          SELECT p.dateToPay, p.* FROM payments p
          JOIN `user` AS u ON p.userId = u.id
          JOIN `payments_fails` AS pf ON p.id = pf.paymentId
          WHERE
            u.userType = '" . PREMIUM . "'
            AND p.`paySuppExtId` = '" . PAY_SUPPLIER_CAIXA . "'
            AND p.`status` = " . PAY_PENDING . "
	          AND DATE(p.`dateToPay`) BETWEEN '" . $sDateFrom . "' AND '" . $sDateTo . "'
            ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC
        ;";

        $dataReader = $this->querySQL($sql);

        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }

            $aPayments[] = $moPayment;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        /* @var Payment[] $aPayments */
        return $aPayments;
    }


    /** Returns all payments not reconciled
     * @param string $dateOffReconc
     *
     * @return bool|array
     * @throws CDbException
     */
    public function getAllPaymentsPendingReconciliation($dateOffReconc="")
    {
        $aPayments = array();
        if($dateOffReconc==""){
            $dateOffReconc = HeDate::getDateAdd(HeDate::todaySQL(false), -14, 0);
        }

        $sql = "SELECT p.id, p.paySuppExtId as paySuppExtId, p.`paySuppOrderId`, p.`status`,
                    p.`dateEndTransaction`, p.`dateToPay`, ROUND(p.`amountPrice`,2) as `amountPrice`, p.`isRecurring`,".
                    " p.`paySuppExtUniId`
                    FROM `payments` p
                    WHERE
                       p.`paySuppExtId` NOT IN (".PAY_SUPPLIER_GROUPON.",".PAY_SUPPLIER_PAYPAL.",".PAY_SUPPLIER_B2B.")
                           AND p.`status` IN  (". PAY_SUCCESS .", ". PAY_REFUND .")".
                         " AND DATE(p.`dateEndTransaction`) > DATE('2014-03-19') ".
                         " AND DATE(p.`dateEndTransaction`) < DATE('".$dateOffReconc."')
                           AND (p.paySuppLinkStatus = 0 OR p.paySuppLinkStatus IS NULL)
                     ORDER BY p.`dateEndTransaction` ASC; ";

        $dataRows = $this->queryAllSQL( $sql );
        if(!$dataRows)
        {
            return false;
        }

        return $dataRows;
    }

    /**
     *  Returns all payments of a user sorted by dateToPay
     * @param integer $userId
     * @param string $orderBy
     *
     * @return array|bool|Payment[]
     * @throws CDbException
     */
    public function getAllPaymentsByUserId($userId, $orderBy = 'p.dateToPay')
    {
        $aPayments = array();

        $sql = "SELECT " . $this->mainFields() . " FROM payments p
                     WHERE p.userId=" . $userId . "
                     ORDER BY " . $orderBy . " ASC , p.`dateStartTransaction` ASC ;";

        $dataReader = $this->querySQL($sql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }

            $aPayments[] = $moPayment;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        /* @var Payment[] $aPayments */
        return $aPayments;
    }

    /** Search and collects all payments from Allpago from Brazil.
     * @return bool|Payment[]
     *
     * @throws CDbException
     */
    public function getAllPaymentsPendingAllpago()
    {
        $aPayments = array();

        $sql = 'SELECT p.dateToPay, p.* FROM payments p
                    LEFT JOIN `user` u ON p.userId=u.id
                    WHERE u.userType=' . PREMIUM . ' AND
                          p.`paySuppExtId` IN (' . PAY_SUPPLIER_ALLPAGO_BR . ',' . PAY_SUPPLIER_ALLPAGO_MX . ')
                          AND p.`status` = ' . PAY_PENDING . '
                          AND p.`dateToPay` <= CONCAT( DATE(NOW()),\' 00:00:00\')
                     ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC ;';

        $dataReader = $this->querySQL($sql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }

            $aPayments[] = $moPayment;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        /* @var Payment[] $aPayments */
        return $aPayments;
    }


    /** Search and collects all payments from Allpago from Brazil.
     * @return bool|Payment[]
     *
     * @throws CDbException
     */
    public function getAllPaymentsPendingAllpagoMX()
    {
        $aPayments = array();

        $sql = 'SELECT p.dateToPay, p.* FROM payments p
                    LEFT JOIN `user` u ON p.userId=u.id
                    WHERE u.userType=' . PREMIUM . ' AND
                          p.`paySuppExtId` IN (' . PAY_SUPPLIER_ALLPAGO_MX . ')
                          AND p.`status` = ' . PAY_PENDING . '
                          AND p.`dateToPay` <= CONCAT( DATE(NOW()),\' 00:00:00\')
                     ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC ;';

        $dataReader = $this->querySQL($sql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }

            $aPayments[] = $moPayment;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        /* @var Payment[] $aPayments */
        return $aPayments;
    }


    /** Search and collects all payments from Allpago from Brazil.
     * @return bool|Payment[]
     *
     * @throws CDbException
     */
    public function getAllPaymentsPendingAllpagoBR()
    {
        $aPayments = array();

        $sql = 'SELECT p.dateToPay, p.* FROM payments p
                    LEFT JOIN `user` u ON p.userId=u.id
                    WHERE u.userType=' . PREMIUM . ' AND
                          p.`paySuppExtId` IN (' . PAY_SUPPLIER_ALLPAGO_BR . ')
                          AND p.`status` = ' . PAY_PENDING . '
                          AND p.`dateToPay` <= CONCAT( DATE(NOW()),\' 00:00:00\')
                     ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC ;';

        $dataReader = $this->querySQL($sql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }

            $aPayments[] = $moPayment;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        /* @var Payment[] $aPayments */
        return $aPayments;
    }

    /**
     * @return array|bool|Payment[]
     *
     * @throws CDbException
     */
    public function getAllPaymentsPendingAllpagoTest()
    {
        $aPayments = array();

        $sql = 'SELECT p.dateToPay, p.* FROM payments p
                    LEFT JOIN `user` u ON p.userId=u.id
                    WHERE u.userType=' . PREMIUM . ' AND
                          p.`paySuppExtId` IN (' . PAY_SUPPLIER_ALLPAGO_BR . ',' . PAY_SUPPLIER_ALLPAGO_MX . ')
                          AND p.`status` = ' . PAY_PENDING . '
                          AND p.`userId` >= 1393360
                     ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC ;';

echo "\n" . $sql . "\n"; return false;

        $dataReader = $this->querySQL($sql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }

            $aPayments[] = $moPayment;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        /* @var Payment[] $aPayments */
        return $aPayments;
    }


    /** Retrieves all payments that are going to be renewed on a certain date.
     *
     * @param $onDate
     * @param int $limit
     * @param bool $excludeMonthSubscription
     *
     * @throws CDbException
     * @return array|bool|Payment[]
     *
     */
    public function getAllPaymentsPendingOnDate($onDate, $limit=0, $excludeMonthSubscription = false)
    {
        $aPayments = array();

        $sqlWPeriod = '';
        if ($excludeMonthSubscription){
            $sqlWPeriod = ' AND p.idPeriodPay <> 30 ';
        }
        $sql = "SELECT p.`id`  FROM payments p
                    LEFT JOIN `user` u ON p.userId=u.id
                    WHERE  u.userType=".PREMIUM."
                           AND p.`status` = " . PAY_PENDING . "
                           AND p.`paySuppExtId` NOT IN (" . PAY_SUPPLIER_GROUPON . ",". PAY_SUPPLIER_B2B ." )
                           AND p.`isRecurring` = 1
                           AND DATE(p.`dateToPay`) =  DATE('".$onDate."')
                           $sqlWPeriod
                     GROUP BY p.`userId`
                     ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC ";
        if($limit>0){
            $sql = $sql ." LIMIT ".$limit." ";
        }

        $dataReader = $this->querySQL($sql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }

            $aPayments[] = $moPayment;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        /* @var Payment[] $aPayments */
        return $aPayments;
    }

    public function getAllPaymentsPendingSupplierTest($oUsersIds, $paySuppExtId=PAY_SUPPLIER_CAIXA)
    {
        $aPayments = array();

        $sql = "SELECT p.dateToPay, p.* FROM payments p
                    LEFT JOIN `user` u ON p.userId=u.id
                    WHERE
                        u.id IN (" . implode(", ", $oUsersIds) . ") AND
                        u.userType = 2 AND
                        p.`paySuppExtId` = '" . $paySuppExtId . "'
                        AND p.`status` = " . PAY_PENDING . "
                    ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC ;";

        $dataReader = $this->querySQL($sql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }
            $aPayments[] = $moPayment;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        /* @var Payment[] $aPayments */
        return $aPayments;
    }

    /**
     * @param array $testUsers
     * @return array|bool|Payment[]
     *
     * @throws CDbException
     */
    public function getAllPaymentsPendingLaCaixaTest2($testUsers)
    {
        $aPayments = array();

        $sql = "SELECT p.dateToPay, p.* FROM payments p
                    LEFT JOIN `user` u ON p.userId=u.id
                    WHERE u.userType=2 AND
                          p.`paySuppExtId` = '" . PAY_SUPPLIER_CAIXA . "'
                          AND p.`status` = " . PAY_PENDING . "
                          AND p.userId >= '" . $testUsers . "'
                     ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC ;";

        $dataReader = $this->querySQL($sql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }

            $aPayments[] = $moPayment;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        /* @var Payment[] $aPayments */
        return $aPayments;
    }


    /**
     * @param $dateRequestFrom
     *
     * @return array
     */
    public function getAllPaymentsPendingAppstore($dateRequestFrom)
    {
/** @TODO - FECHAS !!! **/
/** @TODO - FECHAS !!! **/
/** @TODO - FECHAS !!! **/

        $responseArray = array();

        $sSql =     " SELECT pa.*, ";
        $sSql .=    "   pio.id AS appStoreId, pio.idPayment AS appStoreIdPayment, pio.idCountry AS appStoreIdCountry, pio.userId AS appStoreUserId, pio.purchaseReceipt AS appStorePurchaseReceipt ";
        $sSql .=    " FROM payments AS pa ";
        $sSql .=    " JOIN `user` AS us ON us.id = pa.userId ";
        $sSql .=    " JOIN payments_appstore AS pio ON pa.idPayControlCheck = pio.idPayment ";
        $sSql .=    "   AND pa.userId = pio.userId ";
        $sSql .=    "   AND pa.paySuppExtId = " . PAY_SUPPLIER_APP_STORE . " ";
        $sSql .=    "   AND pa.`status` = " . PAY_PENDING . " ";
        $sSql .=    " WHERE ";
        $sSql .=    "   us.userType = " . PREMIUM . " ";
        $sSql .=    "   AND pio.`status` = " . PAY_APP_STORE_PAY_PENDING . " ";
        $sSql .=    "   AND pa.`dateToPay` < CONCAT( DATE(NOW()),' 00:00:00') ";
        $sSql .=    " ORDER BY pa.dateToPay ASC, pa.dateStartTransaction ASC ";

        $dataReader = $this->querySQL($sSql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
//                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }
            /*  @var AbaPaymentsAppstore $moAppstorePayment */
            $moAppstorePayment = new AbaPaymentsAppstore();
            if (!$moAppstorePayment->getPaymentAppstoreById($row["appStoreId"])) {
//                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }
            $responseArray[] = array("payment" => $moPayment, "appStorePayment" => $moAppstorePayment);
        }
        if (!is_array($responseArray)) {
            return array();
        }
        return $responseArray;
    }

    public function getAllPaymentsPendingPlayStore($dateRequestFrom)
    {

        $responseArray = array();

        $sSql =     " SELECT pa.*, ";
        $sSql .=    "   pio.id AS PlayStoreId, pio.idPayment AS PlayStoreIdPayment, pio.idCountry AS PlayStoreIdCountry, pio.userId AS PlayStoreUserId, pio.purchaseReceipt AS PlayStorePurchaseReceipt ";
        $sSql .=    " FROM payments AS pa ";
        $sSql .=    " JOIN `user` AS us ON us.id = pa.userId ";
        $sSql .=    " JOIN payments_playstore AS pio ON pa.idPayControlCheck = pio.idPayment ";
        $sSql .=    "   AND pa.userId = pio.userId ";
        $sSql .=    "   AND pa.paySuppExtId = " . PAY_SUPPLIER_ANDROID_PLAY_STORE . " ";
        $sSql .=    "   AND pa.`status` = " . PAY_PENDING . " ";
        $sSql .=    " WHERE ";
        $sSql .=    "   us.userType = " . PREMIUM . " ";
        $sSql .=    "   AND pio.`status` = " . PAY_APP_STORE_PAY_PENDING . " ";
        $sSql .=    "   AND pa.`dateToPay` < CONCAT( DATE(NOW()),' 00:00:00') ";
        $sSql .=    " ORDER BY pa.dateToPay ASC, pa.dateStartTransaction ASC ";

        $dataReader = $this->querySQL($sSql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
//                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }

            $moPlayStorePayment = new AbaPaymentsPlaystore();
            if (!$moPlayStorePayment->getPaymentPlayStoreById($row["PlayStoreId"])) {
//                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }
            $responseArray[] = array("payment" => $moPayment, "PlayStorePayment" => $moPlayStorePayment);
        }
        if (!is_array($responseArray)) {
            return array();
        }
        return $responseArray;
    }

    /**
     * @param $dateRequestFrom
     * @param $sUserIds
     * @return array
     */
    public function getAllPaymentsPendingPlayStoreTest($dateRequestFrom, $sUserIds)
    {

        $responseArray = array();

        $sSql =     " SELECT pa.*, ";
        $sSql .=    "   pio.id AS PlayStoreId, pio.idPayment AS PlayStoreIdPayment, pio.idCountry AS PlayStoreIdCountry, pio.userId AS PlayStoreUserId, pio.purchaseReceipt AS PlayStorePurchaseReceipt ";
        $sSql .=    " FROM payments AS pa ";
        $sSql .=    " JOIN `user` AS us ON us.id = pa.userId ";
        $sSql .=    " JOIN payments_playstore AS pio ON pa.idPayControlCheck = pio.idPayment ";
        $sSql .=    "   AND pa.userId = pio.userId ";
        $sSql .=    "   AND pa.paySuppExtId = " . PAY_SUPPLIER_ANDROID_PLAY_STORE . " ";
        $sSql .=    "   AND pa.`status` = " . PAY_PENDING . " ";
        $sSql .=    " WHERE ";
        $sSql .=    "   us.userType = " . PREMIUM . " ";
        $sSql .=    "   AND pio.`status` = " . PAY_APP_STORE_PAY_PENDING . " ";
//         $sSql .=    "   AND pa.`dateToPay` < CONCAT( DATE(NOW()),' 00:00:00') ";
        $sSql .=    "   AND pa.`userId` IN (" . $sUserIds . ") ";
        $sSql .=    " ORDER BY pa.dateToPay ASC, pa.dateStartTransaction ASC ";


        $dataReader = $this->querySQL($sSql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
//                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }

            $moPlayStorePayment = new AbaPaymentsPlaystore();
            if (!$moPlayStorePayment->getPaymentPlayStoreById($row["PlayStoreId"])) {
//                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }
            $responseArray[] = array("payment" => $moPayment, "PlayStorePayment" => $moPlayStorePayment);
        }
        if (!is_array($responseArray)) {
            return array();
        }
        return $responseArray;
    }

    public function getAllPaymentsInWarrantyPlayStore($dateRequestFrom)
    {
        /** @TODO - FECHAS !!! **/
        /** @TODO - FECHAS !!! **/
        /** @TODO - FECHAS !!! **/

        $responseArray = array();

        $sSql =     " SELECT pa.*, ";
        $sSql .=    "   pio.id AS appStoreId, pio.idPayment AS appStoreIdPayment, pio.idCountry AS appStoreIdCountry, pio.userId AS appStoreUserId, pio.purchaseReceipt AS appStorePurchaseReceipt ";
        $sSql .=    " FROM payments AS pa ";
        $sSql .=    " JOIN `user` AS us ON us.id = pa.userId ";
        $sSql .=    " JOIN payments_appstore AS pio ON pa.idPayControlCheck = pio.idPayment ";
        $sSql .=    "   AND pa.userId = pio.userId ";
        $sSql .=    "   AND pa.paySuppExtId = " . PAY_SUPPLIER_APP_STORE . " ";
        $sSql .=    "   AND pa.`status` = " . PAY_PENDING . " ";
        $sSql .=    " WHERE ";
        $sSql .=    "   us.userType = " . PREMIUM . " ";
        $sSql .=    "   AND pio.`status` = " . PAY_APP_STORE_PAY_PENDING . " ";
        $sSql .=    "   AND pa.`dateToPay` < CONCAT( DATE(NOW()),' 00:00:00') ";
        $sSql .=    " ORDER BY pa.dateToPay ASC, pa.dateStartTransaction ASC ";

        $dataReader = $this->querySQL($sSql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
//                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }
            /*  @var AbaPaymentsAppstore $moAppstorePayment */
            $moAppstorePayment = new AbaPaymentsAppstore();
            if (!$moAppstorePayment->getPaymentAppstoreById($row["appStoreId"])) {
//                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }
            $responseArray[] = array("payment" => $moPayment, "appStorePayment" => $moAppstorePayment);
        }
        if (!is_array($responseArray)) {
            return array();
        }
        return $responseArray;
    }

    public function getAllPaymentsPendingAppstoreTest($dateRequestFrom)
    {
        $responseArray = array();

        $sSql =     " SELECT pio.id AS appStoreId, pio.idPayment AS appStoreIdPayment, pio.idCountry AS appStoreIdCountry, pio.userId AS appStoreUserId, pio.purchaseReceipt AS appStorePurchaseReceipt ";
        $sSql .=    " FROM payments_appstore AS pio ";

        $dataReader = $this->querySQL($sSql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var AbaPaymentsAppstore $moAppstorePayment */
            $moAppstorePayment = new AbaPaymentsAppstore();
            if (!$moAppstorePayment->getPaymentAppstoreById($row["appStoreId"])) {
            }
            $responseArray[] = array("payment" => new Payment(), "appStorePayment" => $moAppstorePayment);
        }
        if (!is_array($responseArray)) {
            return array();
        }
        return $responseArray;
    }


    /**
     * @param $testUser
     * @return array|bool|Payment[]
     *
     * @throws CDbException
     */
    public function getAllPaymentsLaCaixaMigration($paySuppOrderId)
    {
        $aPayments = array();

        $sSql = "
            SELECT p.*
            FROM payments p
            LEFT JOIN `user` u ON p.userId=u.id
            WHERE
                p.`paySuppExtId` = :PAYSUPPEXTID
                AND p.`paySuppOrderId` = :PAYSUPPORDERID
                AND (p.`paySuppExtProfId` IS NULL OR p.`paySuppExtProfId` = '' )
            ORDER BY p.`dateToPay` ASC, p.`dateStartTransaction` ASC; ";

        $paramsToSQL =  array(":PAYSUPPEXTID" => PAY_SUPPLIER_CAIXA, ":PAYSUPPORDERID" => $paySuppOrderId);
        $dataReader =   $this->querySQL( $sSql, $paramsToSQL);

        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }
            $aPayments[] = $moPayment;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        /* @var Payment[] $aPayments */
        return $aPayments;
    }


    /**
     * @param $dateLimit
     * @param $limitSql
     *
     * @return array|bool|Payment[]
     * @throws CDbException
     */
    public function getAllPaymentsPendingLaCaixa31($dateLimit, $limitSql)
    {
        $aPayments = array();

        $sSql = "
            SELECT p.dateToPay, p.* FROM payments p
            LEFT JOIN `user` u ON p.userId=u.id
            WHERE
                u.userType=2
                AND p.`paySuppExtId` = '" . PAY_SUPPLIER_CAIXA . "'
                AND p.`status` = " . PAY_PENDING . "
                AND p.`dateToPay` <= CONCAT( DATE('" . $dateLimit . "'),' 00:00:00')
            ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC
            LIMIT 0, " . $limitSql . "
        ;";

//echo "\n"; print_r($sSql); echo "\n"; return false;

        $dataReader = $this->querySQL($sSql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }

            $aPayments[] = $moPayment;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        /* @var Payment[] $aPayments */
        return $aPayments;
    }

    /**
     * @param $dateLimit
     * @param $limitSql
     *
     * @return array|bool|Payment[]
     * @throws CDbException
     */
    public function getAllPaymentsPendingAllpago31($dateLimit, $limitSql)
    {
        $aPayments = array();

        $sSql = '
            SELECT p.dateToPay, p.* FROM payments p
            LEFT JOIN `user` u ON p.userId=u.id
            WHERE
                u.userType=2
                AND p.`paySuppExtId` IN (' . PAY_SUPPLIER_ALLPAGO_BR . ',' . PAY_SUPPLIER_ALLPAGO_MX . ')
                AND p.`status` = 0
                AND p.`dateToPay` <= CONCAT( DATE(\'' . $dateLimit . '\'), \' 00:00:00\')
            ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC
            LIMIT 0, ' . $limitSql . '
        ;';

//echo "\n"; print_r($sSql); echo "\n"; return false;

        $dataReader = $this->querySQL($sSql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }

            $aPayments[] = $moPayment;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        /* @var Payment[] $aPayments */
        return $aPayments;
    }

    /**
     * @param $dateLimit
     * @param $limitSql
     *
     * @return array|bool|Payment[]
     * @throws CDbException
     */
    public function getAllPaymentsPendingAdyen31($dateLimit, $limitSql)
    {
        $aPayments = array();

        $sSql = "
            SELECT p.dateToPay, p.* FROM payments p
            LEFT JOIN `user` u ON p.userId=u.id
            WHERE
                u.userType=2
                AND p.`paySuppExtId` = '10'
                AND p.`status` = 0
                AND p.`dateToPay` <= CONCAT( DATE('" . $dateLimit . "'),' 00:00:00')
            ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC
            LIMIT 0, " . $limitSql . "
            ;";

//echo "\n"; print_r($sSql); echo "\n"; return false;

        $dataReader = $this->querySQL($sSql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }

            $aPayments[] = $moPayment;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        /* @var Payment[] $aPayments */
        return $aPayments;
    }

    /**
     * @param $dateLimit
     * @param $limitSql
     *
     * @return array|bool
     */
    public function getAllPaymentsPendingAdyenhpp31($dateLimit, $limitSql)
    {
        $aPayments = array();

        $sSql = "
            SELECT
                p.dateToPay, p.*,
                ahppr.id AS idHppRenewal, ahppr.merchantReference, ahppr.`status` AS hppStatus
            FROM payments p
            LEFT JOIN `user` u
                ON p.userId = u.id
            LEFT JOIN `payments_adyen_hpp_renewals` ahppr
                ON p.id = ahppr.idPayment
            WHERE
                u.userType = '" . PREMIUM . "'
                AND p.`paySuppExtId` = '" . PAY_SUPPLIER_ADYEN_HPP . "'
                AND p.`status` = " . PAY_PENDING . "
                AND p.`dateToPay` <= CONCAT( DATE('" . $dateLimit . "'),' 00:00:00')
            ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC
            LIMIT 0, " . $limitSql . "
        ;";

//echo "\n"; print_r($sSql); echo "\n"; return false;

        $dataReader = $this->querySQL($sSql);
        while (($row = $dataReader->read()) !== false) {
            $aPayments[] = $row;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        return $aPayments;
    }


    /**
     * @return array|bool|Payment[]
     *
     * @throws CDbException
     */
    public function getAllPaymentsPendingAdyen()
    {
        $aPayments = array();

        $sql = "SELECT p.dateToPay, p.* FROM payments p
                    LEFT JOIN `user` u ON p.userId=u.id
                    WHERE u.userType=2 AND
                          p.`paySuppExtId` = '" . PAY_SUPPLIER_ADYEN . "'
                          AND p.`status` = " . PAY_PENDING . "
                          AND p.`dateToPay` <= CONCAT( DATE(NOW()),' 00:00:00')
                     ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC ;";

        $dataReader = $this->querySQL($sql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }

            $aPayments[] = $moPayment;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        /* @var Payment[] $aPayments */
        return $aPayments;
    }

    /**
     * @return array|bool
     */
    public function getAllPaymentsPendingAdyenhpp()
    {
        $aPayments = array();

        $sSql = "
            SELECT
                p.dateToPay, p.*,
                ahppr.id AS idHppRenewal, ahppr.merchantReference, ahppr.`status` AS hppStatus
            FROM payments p
            LEFT JOIN `user` u
                ON p.userId = u.id
            LEFT JOIN `payments_adyen_hpp_renewals` ahppr
                ON p.id = ahppr.idPayment
            WHERE
                u.userType = '" . PREMIUM . "'
                AND p.`paySuppExtId` = '" . PAY_SUPPLIER_ADYEN_HPP . "'
                AND p.`status` = " . PAY_PENDING . "
                AND p.`dateToPay` <= CONCAT( DATE(NOW()),' 00:00:00')
            ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC ;
        ";

        $dataReader = $this->querySQL($sSql);
        while (($row = $dataReader->read()) !== false) {
            $aPayments[] = $row;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        return $aPayments;
    }

    /**
     * @return array|bool
     */
    public function getAllPaymentsPendingAdyenhppTest()
    {
        $aPayments = array();

        $sSql = "
            SELECT
                p.dateToPay, p.*,
                ahppr.id AS idHppRenewal, ahppr.merchantReference, ahppr.`status` AS hppStatus
            FROM payments p
            LEFT JOIN `user` u
                ON p.userId = u.id
            LEFT JOIN `payments_adyen_hpp_renewals` ahppr
                ON p.id = ahppr.idPayment
            WHERE
                u.userType = '" . PREMIUM . "'
                AND p.`paySuppExtId` = '" . PAY_SUPPLIER_ADYEN_HPP . "'
                AND p.`status` = " . PAY_PENDING . "
                AND p.`dateToPay` <= '2017-01-12 00:00:00'
            ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC ;
        ";

        $dataReader = $this->querySQL($sSql);
        while (($row = $dataReader->read()) !== false) {
            $aPayments[] = $row;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        return $aPayments;
    }

    /**
     * @return array|bool|Payment[]
     *
     * @throws CDbException
     */
    public function getAllPaymentsPendingAdyenTest()
    {
        $aPayments = array();

        $sql = "SELECT p.dateToPay, p.* FROM payments p
            LEFT JOIN `user` u ON p.userId=u.id
            WHERE u.userType=2 AND
                  p.`paySuppExtId` = '" . PAY_SUPPLIER_ADYEN . "'
                  AND p.`status` = " . PAY_PENDING . "
                  AND p.userId = 5745196
             ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC ;";

        $dataReader = $this->querySQL($sql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }

            $aPayments[] = $moPayment;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        /* @var Payment[] $aPayments */
        return $aPayments;
    }

    /**
     * @param $userId
     * @return array|bool|Payment[]
     *
     * @throws CDbException
     */
    public function getAllSuccessPayment($userId)
    {
        $aPayments = array();

        $sSql = "
            SELECT p.*
            FROM payments p
            JOIN `user` u ON p.userId=u.id
            WHERE
                p.userId='" . $userId . "'
                AND u.userType='" . PREMIUM . "'
                AND p.`status`='" . PAY_SUCCESS . "'
            ORDER BY p.`dateStartTransaction` ASC ;";

        $dataReader = $this->querySQL($sSql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }
            $aPayments[] = $moPayment;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        /* @var Payment[] $aPayments */
        return $aPayments;
    }


    /** Cron renewals Family Plan
     *
     * @param $sDate
     *
     * @return array|bool
     */
    public function getAllPaymentsPendingAllSuppliers($sDate)
    {
        $aPayments = array();

        $sSql = "
            SELECT p.dateToPay, p.* FROM payments p
            LEFT JOIN `user` u ON p.userId=u.id
            WHERE
                u.userType='" . PREMIUM . "' AND
                p.`paySuppExtId` IN ('" . PAY_SUPPLIER_CAIXA . "', '" . PAY_SUPPLIER_ALLPAGO_BR . "', '" . PAY_SUPPLIER_ALLPAGO_MX . "', '" . PAY_SUPPLIER_ADYEN . "', '" . PAY_SUPPLIER_PAYPAL . "')
                AND p.`status` = " . PAY_PENDING . "
                AND DATE(p.`dateToPay`) = '" . $sDate . "'
            ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC
        ;";

        $dataReader = $this->querySQL($sSql);
        while (($row = $dataReader->read()) !== false) {
            $aPayments[] = $row;
        }
        return $aPayments;
    }


    /**
     * @return bool|Payment[]
     * @throws CDbException
     */
    public function getAllPaymentsPendingLaCaixaTest()
    {
        $aPayments = array();

        $sql = "SELECT p.dateToPay, p.* FROM payments p
                    LEFT JOIN `user` u ON p.userId=u.id
                    WHERE u.userType=2 AND
                          p.`paySuppExtId` = '" . PAY_SUPPLIER_CAIXA . "'
                          AND p.`status` = " . PAY_PENDING . "
                          AND p.`userId` IN (2389007, 5667126)
                     ORDER BY p.`dateToPay` ASC , p.`dateStartTransaction` ASC ;";

        $dataReader = $this->querySQL($sql);
        while (($row = $dataReader->read()) !== false) {
            /*  @var Payment $moPayment */
            $moPayment = new Payment();
            if (!$moPayment->getPaymentById($row["id"])) {
                throw new CDbException(" ABA: There has been a problem retrieving the payment " . $row["id"]);
            }

            $aPayments[] = $moPayment;
        }
        if (count($aPayments) == 0) {
            return false;
        }

        /* @var Payment[] $aPayments */
        return $aPayments;
    }

}
