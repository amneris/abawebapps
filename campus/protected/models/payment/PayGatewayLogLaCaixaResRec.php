<?php
/**
 *
SELECT p.`id`, p.`idPayment`, p.`fecha`, p.`Order_number`, p.`Codigo`, p.`Amount`, p.`Currency`,
p.`Signature`, p.`MerchantCode`, p.`Terminal`, p.`Response`, p.`AuthorisationCode`,
p.`TransactionType`, p.`SecurePayment`, p.`MerchantData`, p.`MerchantIdentifier`, p.`ExpiryDate`, p.`XML`
 * User: abaenglish
 * Date: 29/10/12
 * Time: 17:06
 * To change this template use File | Settings | File Templates.
 */

/*
 *
    @property integer id
    @property string idPayment
    @property string fecha
    @property string Order_number
    @property string Codigo
    @property string Amount
    @property string Currency
    @property string Signature
    @property string MerchantCode
    @property string Terminal
    @property string Response
    @property string AuthorisationCode
    @property string TransactionType
    @property string SecurePayment
    @property string MerchantData
    @property string MerchantIdentifier
    @property string ExpiryDate
    @property string XML
*/
class PayGatewayLogLaCaixaResRec extends PayGatewayLog
{
    public function tableName() {
        return "pay_gateway_response";
    }

    /**
     * @return bool
     */
    public function insertResponseReceived()
    {
        $this->beginTrans();
        $successTrans = true;
        //-----------------
        $sql = "
            INSERT INTO " . $this->tableName() . " (
                `idPayment`,  `fecha`,  `Order_number`,`Codigo`,`Amount`, `Currency`,
                `Signature`, `MerchantCode`, `Terminal`,  `Response`, `AuthorisationCode`,
                `TransactionType`, `SecurePayment`, `MerchantData`, `MerchantIdentifier`, `ExpiryDate`,
                `XML` ) " .
            "
            VALUES(
                :IDPAYMENT, :FECHA, :ORDER_NUMBER, :CODIGO, :AMOUNT, :CURRENCY,
                :SIGNATURE, :MERCHANTCODE, :TERMINAL, :RESPONSE, :AUTHORISATIONCODE,
                :TRANSACTIONTYPE, :SECUREPAYMENT, :MERCHANTDATA, :MERCHANTIDENTIFIER, :EXPIRYDATE,
                :XML )"
        ;

        $aParamValues = array(
            ":IDPAYMENT" =>             $this->idPayment,
            ":FECHA" =>                 $this->fecha,
            ":ORDER_NUMBER" =>          $this->Order_number,
            ":CODIGO" =>                $this->Codigo,
            ":AMOUNT" =>                $this->Amount,
            ":CURRENCY" =>              $this->Currency,
            ":SIGNATURE" =>             $this->Signature,
            ":MERCHANTCODE" =>          $this->MerchantCode,
            ":TERMINAL" =>              $this->Terminal,
            ":RESPONSE" =>              $this->Response,
            ":AUTHORISATIONCODE" =>     $this->AuthorisationCode,
            ":TRANSACTIONTYPE" =>       $this->TransactionType,
            ":SECUREPAYMENT" =>         $this->SecurePayment,
            ":MERCHANTDATA" =>          $this->MerchantData,
            //#4681
            ":MERCHANTIDENTIFIER" =>    $this->MerchantIdentifier,
            ":EXPIRYDATE" =>            $this->ExpiryDate,
            ":XML" =>                   $this->XML
        );

        $this->id = $this->executeSQL($sql, $aParamValues, true);
        if( $this->id <= 0 ) {
            $successTrans = false;
        }
        //---------------
        if( $successTrans ) {
            $this->commitTrans();
            return true;
        }
        else {
            $this->rollbackTrans();
            return false;
        }
    }

    /** Returns string representing the number Caixa gives to the transaction.
     * @return int|mixed
     */
    public function getUniqueId() {
       return $this->AuthorisationCode;
    }
}
