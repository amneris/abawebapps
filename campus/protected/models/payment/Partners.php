<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 27/11/12
 * Time: 12:44
 * To query and update the old tables of 'ventas' flash.
 * Later on probably we should change the source tables.
 */
/**
 * @property integer idPartner
 * @property integer idPartnerGroup
 * @property string name
 * @property string nameGroup (Magic property, only in some funcitons)
 * @property integer idCategory
 * @property integer idBusinessArea
 * @property integer idType
 * @property integer idGroup
 * @property integer idChannel
 * @property integer idCountry
 * @property integer partnerIdCancel (Magic property, only in some funcitons)
 * */
class Partners extends AbaActiveRecord
{

    public function mainFields()
    {
        return "
            a.`idPartner`, a.`name`, a.`idPartnerGroup`, a.`nameGroup`, a.`idCategory`, 
            a.`idBusinessArea`, a.`idType`, a.`idGroup`, a.`idChannel`, a.`idCountry`
        ";

    }

    /** Returns table name model.
     * @return string
     */
    public function tableName()
    {
        return 'aba_partners_list';
    }


    /** Loads all columns for main table
     * @param integer @id
     *
     * @return bool|Partners
     */
    public function getPartnerById( $id )
    {
        $sql= " SELECT ".$this->mainFields()." FROM  ".$this->tableName().
            " a WHERE a.`idPartner`=:IDPARTNER ; ";
        $paramsToSQL = array(":IDPARTNER" => $id);

        $dataReader=$this->querySQL($sql, $paramsToSQL);
        if( $this->readRowFillDataCols($dataReader) )
        {
            return $this;
        }

        return false;
    }

    /** Returns the id Partner for Cancellation
     * @param $id
     *
     * @return bool|Partners
     */
    public function getPartnerCancelById( $id )
    {
        $sql= " SELECT ".$this->mainFields().
            ", CASE a.`idPartnerGroup` WHEN ".PARTNER_ABA_G." THEN ".PARTNER_ID_EXABAENGLISH.
                                    " WHEN ".PARTNER_ABAMOBILE_G." THEN ".PARTNER_ID_EXABAENGLISH.
                                    " WHEN ".PARTNER_GROUPON_G." THEN ".PARTNER_ID_EXFLASHSALES.
                                    " WHEN ".PARTNER_AFFILIATES_G." THEN ".PARTNER_ID_EXSOCIALAFFILIATES.
                                    " WHEN ".PARTNER_POSTAFFILIATES_G." THEN ".PARTNER_ID_EXSOCIALAFFILIATES.
                                    " WHEN ".PARTNER_SOCIAL_G." THEN ".PARTNER_ID_EXSOCIALAFFILIATES.
                                    " ELSE ".PARTNER_ID_EXSOCIALAFFILIATES." END as `partnerIdCancel`".
              " FROM  ".$this->tableName().
            " a WHERE a.`idPartner`=:IDPARTNER ; ";
        $paramsToSQL = array(":IDPARTNER" => $id);

        $dataReader=$this->querySQL($sql, $paramsToSQL);
        if( $this->readRowFillDataCols($dataReader) )
        {
            return $this;
        }

        return false;
    }

    /**
     * @param $id
     * @param $colPeriod
     *
     * @return bool|integer
     */
    public function getGrouponTableByIdAndPeriod($id, $colPeriod)
    {
        $sql= " SELECT v.`idventasFlashScript`, v.`tableName`, v.`prefijoMensual`,
                     v.`prefijoBimensual`,
                     v.`prefijoTrimestral`, v.`prefijoSemestral`, v.`prefijoAnual`,
                     v.`prefijo18Meses`, v.`prefijo24Meses`
              FROM ".Yii::app()->params['dbVentasFlash'].".ventasFlashScript v
              WHERE v.idventasFlashScript = $id ";

        $dataReader = $this->querySQL( $sql );
        if(($row = $dataReader->read())!==false)
        {
            return $row['tableName'].$row[ $colPeriod ] ;
        }

        return false;
    }


    /**
     * @param $tableName
     * @param $code1
     *
     * @return array|bool
     */
    public function getGrouponCodeFreeByCode( $tableName, $code1 )
    {
        $sql= " SELECT g.* FROM ".Yii::app()->params['dbVentasFlash'].".`".$tableName."` g
                        WHERE g.`code1`=:CODE1 AND
                               g.`used` = 0 ";
        $aParams = array(":CODE1"=>$code1);

        $dataReader = $this->querySQL( $sql, $aParams );
        if(($row = $dataReader->read())!==false)
        {
            return $row;
        }

        return false;
    }

    /** Returns row if voucher code1 has been used already
     * @param $tableName
     * @param $code1
     *
     * @return array|bool|false
     */
    public function getGrouponCodeBusyByCode( $tableName, $code1 )
    {
        $sql= " SELECT g.* FROM ".Yii::app()->params['dbVentasFlash'].".`".$tableName."` g
                        WHERE g.`code1`=:CODE1 AND
                               g.`used` = 1 ";
        $aParams = array(":CODE1"=>$code1);
        $dataReader = $this->querySQL( $sql, $aParams );
        if(($row = $dataReader->read())!==false) {
            return $row;
        }
        return false;
    }

    /** Updates a Voucher as used.
     * @param $tableName
     * @param $code1
     * @param $code2
     * @param $email
     *
     * @return bool True if successful
     */
    public function updateGrouponCodeUsed( $tableName, $code1, $code2 , $email )
    {
        $this->beginTrans();

        $sql = " UPDATE ".Yii::app()->params['dbVentasFlash'].".`".$tableName."` g
                SET g.`date`=NOW(), g.`code2`=:CODE2, g.`used`=IF(g.`count`=1, 1, g.`used`),
                            g.`email`=IF(g.`email`<>'' AND !ISNULL(g.`email`),CONCAT(g.`email`,', ',:EMAIL),:EMAIL),
                            g.`count`=(g.`count`-1)
                WHERE g.`code1`=:CODE1 AND g.`used` = 0; ";
        $aParams = array(":CODE1"=>$code1, ":CODE2"=>$code2, ":EMAIL"=>$email );

        if ( $this->updateSQL( $sql, $aParams ) > 0 )
        {
            $this->commitTrans();
            return true;
        }
        else
        {
            $this->rollbackTrans();
            return false;
        }
    }

    /** Returns campaign based on voucher code.
     * @param      $tableName
     * @param      $code1
     * @param      $partnerId
     *
     * @return bool|null
     */
    public function getCampaignByCode1(  $tableName, $code1, $partnerId)
    {

        $sql= " SELECT g.`campaign` FROM ".Yii::app()->params['dbVentasFlash'].".`".$tableName."` g WHERE g.`code1`=:CODE1; ";

        $aParams = array(":CODE1"=>$code1);
        $dataReader=$this->querySQL($sql, $aParams);
        if(($row = $dataReader->read())!==false)
        {
            return $row["campaign"];
        }

        return false;
    }

    /** get Alias of Partner Group for Selligent
     * @param $idPartner
     *
     * @return bool|Partners
     */
    public function getPartnerByIdPartner( $idPartner )
    {
        $sql= " SELECT a.*, CASE a.`idPartnerGroup` WHEN ".PARTNER_ABA_G." THEN '".PARTNER_ABA_W.
                                                 "' WHEN ".PARTNER_GROUPON_G." THEN '".PARTNER_GROUPON_W.
                                                 "' WHEN ".PARTNER_AFFILIATES_G." THEN '".PARTNER_AFFILIATES_W.
                                                 "' WHEN ".PARTNER_SOCIAL_G." THEN '".PARTNER_SOCIAL_W.
                                                 "' WHEN ".PARTNER_B2B_G." THEN '".PARTNER_B2B_W.
                                                "' WHEN ".PARTNER_ABAMOBILE_G." THEN '".PARTNER_ABAMOBILE_W.
                                                "' WHEN ".PARTNER_POSTAFFILIATES_G." THEN '".PARTNER_POSTAFFILIATES_W.
                                                 "' ELSE a.nameGroup END as `nameGroup`
		            FROM aba_partners_list a WHERE a.`idPartner`=:IDPARTNER ; ";
        $paramsToSQL = array(":IDPARTNER" => $idPartner);

        $dataReader=$this->querySQL($sql, $paramsToSQL);
        if(($row = $dataReader->read())!==false)
        {
            $this->fillDataColsToProperties($row);
            return $this;
        }

        return false;
    }


    public function getListAffiliates()
    {

        $sql="SELECT idPartner FROM aba_partners_list WHERE idPartnerGroup =".PARTNER_POSTAFFILIATES_G.";";

        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $dataReader=$command->query();
        $theArray=array();
        while(($row = $dataReader->read())!==false)
            $theArray[]=$row['idPartner'];
        return $theArray;

    }

}
