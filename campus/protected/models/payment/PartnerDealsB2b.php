<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 14/08/2014
 * Time: 10:00
 * To validate deals for B2b users and other stuff.
 */

/**
 * @property integer $id
 * @property string $code
 * @property integer $idPartner
 * @property integer $idPeriodPay
 * @property integer $numValidations
 * @property string $expirationDate
 * */
class PartnerDealsB2b extends AbaActiveRecord
{

    public function mainFields()
    {
        return " pd.`id`, pd.`code`, pd.`idPartner`, pd.`idPeriodPay`, pd.`numValidations`,
                    pd.expirationDate, pd.`creationDate` ";

    }

    /** Returns table name model.
     * @return string
     */
    public function tableName()
    {
        return Yii::app()->params['dbExtranet'] . '.`partner_deals`';
    }


    /** Loads and returns partnerDeal by the code to be validated later on.
     * @param $code
     *
     * @return $this|bool
     */
    public function getPartnerDealByCode($code)
    {
        $sql = " SELECT " . $this->mainFields() . " FROM  " . $this->tableName() .
            " pd WHERE pd.`code`=:CODE ; ";
        $paramsToSQL = array(":CODE" => $code);
        $dataReader = $this->querySQL($sql, $paramsToSQL);
        if ($this->readRowFillDataCols($dataReader)) {
            return $this;
        }

        return false;
    }


    /**
     *
     * @param integer $idPartner
     *
     * @return array|bool
     */
    public function getDealsByPartnerId($idPartner)
    {
        $sql = " SELECT " . $this->mainFields() . " FROM  " . $this->tableName() .
            " pd WHERE pd.`idPartner`=:IDPARTNER ; ";
        $paramsToSQL = array(":IDPARTNER" => $idPartner);

        $dataRows = $this->queryAllSQL($sql, $paramsToSQL);
        if ($dataRows !== false) {
            return $dataRows;
        }

        return false;
    }

    /** Here we will have to validate a DEAL from a B2b partner. It will
     * reduce Q users from numValidations.
     * @param integer $subtractUsers
     *
     * @return bool
     */
    public function redeemDeal($subtractUsers)
    {
        $this->numValidations = $this->numValidations - $subtractUsers;
        return $this->update(array('numValidations'));
    }

    /**
     * @param $idPartner
     * @param $idPeriodPay
     * @param $subtractUsers
     * @param string $date YYYY-MM-DD
     * @return bool|integer
     */
    public function checkDealValidity($idPartner, $idPeriodPay, $subtractUsers, $date)
    {
        if (intval($idPartner) !== intval($this->idPartner)) {
            return -1;
        }
        if ($idPeriodPay !== $this->idPeriodPay) {
            return -2;
        }
        if ($subtractUsers > $this->numValidations) {
            return -3;
        }
        if (!HeDate::isGreaterThan($this->expirationDate, $date)) {
            return -4;
        }

        return true;
    }

    /**
     * @param int $discountPercent
     *
     * @return string
     */
    public function createB2bExtranetPromoCode($discountPercent=0) {
        return "EPROMO" . trim($discountPercent);
    }


}
