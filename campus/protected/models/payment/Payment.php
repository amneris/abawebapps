<?php
/** Magic properties extracted from table in database dynamically
 *
 * @property string id
 * @property float amountDiscount
 * @property float amountOriginal
 * @property float amountPrice
 * @property string autoId
 * @property string currencyTrans
 * @property string dateEndTransaction
 * @property string dateStartTransaction
 * @property string dateToPay
 * @property string foreignCurrencyTrans
 * @property string idCountry
 * @property string idPartner
 * @property string idPayControlCheck
 * @property string idPeriodPay
 * @property string idProduct
 * @property string idPromoCode
 * @property string idUserCreditForm
 * @property string idUserProdStamp
 * @property string lastAction
 * @property string paySuppExtId
 * @property string paySuppExtProfId
 * @property string paySuppOrderId
 * @property integer status
 * @property integer userId
 * @property float xRateToEUR
 * @property float xRateToUSD
 * @property integer $idPartnerPrePay
 * @property integer $isRecurring
 * @property string  $paySuppExtUniId
 * @property integer $paySuppLinkStatus
 * @property integer $isPeriodPayChange
 * @property integer $cancelOrigin  null - No cancel,
 *                                  1 - All payments until 01/10/2014 or user cancelled,
 *                                  2 - Intranet employee cancellation
 *                                  3 - PayPal cancellation
 *                                  4 - Expiration
 *                                  5 - Upgrade / Downgrade
 * @property float $taxRateValue
 * @property float $amountTax
 * @property float $amountPriceWithoutTax
 * @property int $isExtend
 * @property int $experimentVariationAttributeId
 * @property string $idSession
 *
 */
class Payment extends AbaActiveRecord
{
    public $id;
    private $autoId;

    /**
     * @var AbaUser $parentAbaUser
     */
    private $parentAbaUser;

    /**
     * @return Payment
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function tableName()
    {
        return 'payments';
    }

    public function mainFields()
    {
        return "
         p.`id`, p.`idUserProdStamp`, p.`userId`, p.`idProduct`, p.`idCountry`, p.`idPeriodPay`,
            p.`idUserCreditForm`, p.`paySuppExtId`, p.`paySuppOrderId`, p.`status`, p.`dateStartTransaction`,
            p.`dateEndTransaction`,p.`dateToPay`, p.`xRateToEUR`, p.`xRateToUSD`, p.`currencyTrans`,
            p.`foreignCurrencyTrans`,p.`idPromoCode`, p.`amountOriginal`, p.`amountDiscount`, p.`amountPrice`,
            p.`idPayControlCheck`,p.`idPartner`, p.`idPartnerPrePay`, p.`paySuppExtProfId`, p.autoId, p.`lastAction`,
            p.`isRecurring`, p.`paySuppExtUniId`, p.`paySuppLinkStatus`, p.`isPeriodPayChange`, p.`cancelOrigin`,
            p.`taxRateValue`, p.`amountTax`, p.`amountPriceWithoutTax`, p.`isExtend`,p.`experimentVariationAttributeId`,
            p.`idSession`
        ";
    }

    public function rules()
    {
        return array(
            array('idProduct',     'required'),
            array('idCountry',     'required'),
            array('idPeriodPay',   'required'),
        );
    }

    /**
     * SQL FUNCTIONS ---------------------------------------------------------------------------
      */

    /**
     * Returns the last payment pending to be paid. Used to find the recurrent
     * payments both for PAYPAL and for La Caixa.
     * @param $email
     * @param $paySuppOrderId
     * @param string $status
     * @param bool $dateLimitCheck
     * @return bool
     */
    public function getPaymentByPaySuppOrderId($email, $paySuppOrderId, $status = PAY_PENDING, $dateLimitCheck = true)
    {
        $strPayStatus = " AND p.`status` = $status ";
        if ($status == "") {
            $strPayStatus = "";
        }
        $strDateLimitCheck = ' AND DATEDIFF(NOW(), p.dateToPay) < 28 '; /* DAYS */
        if (!$dateLimitCheck) {
            $strDateLimitCheck = '';
        }
        /*DAYS, we do not search further backwards than these days*/
        $sql = "SELECT ".$this->mainFields()." FROM ".$this->tableName()." p
                                                    INNER JOIN `user` u ON p.userId=u.id
                WHERE u.`email`= '".$email."'
                   AND p.`paySuppOrderId` = :PAYSUPPORDERID
                   ".$strPayStatus."
                   ".$strDateLimitCheck."
                ORDER BY p.`dateToPay` DESC
                LIMIT 1";

        $paramsToSQL = array(":PAYSUPPORDERID" => $paySuppOrderId);
        $dataReader=$this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return true;
        }
        return false;
    }

    /**
     * @param bool $id
     *
     * @return bool|Payment
     */
    public function getPaymentById($id = false)
    {
        $sql =          "SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " p WHERE p.`id`= :ID";
        $paramsToSQL =  array(":ID" => $id);
        $dataReader =   $this->querySQL($sql,$paramsToSQL);
        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /** Returns last payment that is not a REFUND
     * @param integer $userId
     * @return $this|bool
     */
    public function getLastPaymentNotRefundByUserId($userId)
    {
        $sql = " SELECT ".$this->mainFields()."
                    FROM ".$this->tableName()." p
                      LEFT JOIN `pay_suppliers` ps ON p.paySuppExtId=ps.id
                    WHERE p.userId=$userId
                        AND p.status IN (".PAY_SUCCESS.",".PAY_PENDING.",".PAY_FAIL.",".PAY_CANCEL.")
                    ORDER BY p.dateToPay DESC, p.`dateEndTransaction` DESC, p.`dateStartTransaction` DESC, p.`autoId` DESC
                    LIMIT 1; ";

        $dataReader=$this->querySQL($sql);
        if (($row = $dataReader->read())!==false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /** Get last PENDING PAYMENT for this user.
     *
     * @param integer $userId
     * @return $this|bool
     */
    public function getLastPayPendingByUserId($userId)
    {
        $sql = " SELECT ".$this->mainFields()."
                    FROM ".$this->tableName()." p
                      LEFT JOIN `pay_suppliers` ps ON p.paySuppExtId=ps.id
                    WHERE p.`userId`=$userId AND p.`status` = ".PAY_PENDING."
                    ORDER BY p.dateToPay DESC, p.`dateEndTransaction` DESC,
                             p.`dateStartTransaction` DESC, p.`autoId` DESC
                    LIMIT 1; ";

        $dataReader=$this->querySQL($sql);
        if (($row = $dataReader->read())!==false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }

        return false;
    }

    /**
     * @param bool $id
     *
     * @return $this|bool
     */
    public function getPaymentPendingById($id = false)
    {
        $sql = "SELECT " . $this->mainFields() .
            " FROM " . $this->tableName() . " p WHERE p.`id`=:ID AND p.`status`=:STATUS ";
        $paramsToSQL =  array(":ID" => $id, ":STATUS" => PAY_PENDING);
        $dataReader =   $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read())!==false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * Returns calculated columns for the last payment user has done or should have been done.
     * @param $userId
     * @param bool $transactionExecuted
     *
     * @return bool|Payment
     */
    public function getLastPaymentByUserId($userId, $transactionExecuted = false)
    {
        $whTransDone = " ";
        if($transactionExecuted)
        {
            $whTransDone = " AND p.`status` <>  ".PAY_PENDING;
        }
        $sql = " SELECT p.`idProduct`, p.idPeriodPay, p.`status`, p.`amountOriginal`, p.`amountPrice`, ps.`name` as 'supplierName',
                        p.`paySuppOrderId`, DATE_FORMAT(p.`dateToPay`,'%d-%m-%Y') as 'dateToPay', p.paySuppExtId,
                        p.id, p.idPartner, p.idPartnerPrePay, p.dateEndTransaction,
                        p.idUserCreditForm,
                        p.taxRateValue, p.amountTax, p.amountPriceWithoutTax, p.experimentVariationAttributeId, p.paySuppExtProfId, p.idSession
                    FROM ".$this->tableName()." p
                      LEFT JOIN `pay_suppliers` ps ON p.paySuppExtId=ps.id
                    WHERE p.userId=$userId
                    ".$whTransDone."
                    ORDER BY p.dateToPay DESC, p.dateEndTransaction DESC
                    LIMIT 1; ";

        $dataReader=$this->querySQL($sql);
        if (($row = $dataReader->read())!==false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /** Get the last payment for a certain profile.
     *
     * @param $userId
     * @param $paySuppExtId
     * @param $paySuppExtProfId
     * @param string $status
     *
     * @return $this|bool
     */
    public function getLastByPaySuppExtProfId($userId, $paySuppExtId, $paySuppExtProfId, $status = "")
    {
        $strPayStatus = " AND p.`status` = $status ";
        if ($status == "") {
            $strPayStatus = "";
        }

        if (empty($paySuppExtProfId)) {
            return false;
        }
        $sql = " SELECT ".$this->mainFields()."
                    FROM ".$this->tableName()." p
                    WHERE p.userId = $userId AND
                        p.paySuppExtId = $paySuppExtId AND
                        paySuppExtProfId = '$paySuppExtProfId' ".
                        $strPayStatus."
                    ORDER BY p.dateToPay DESC LIMIT 1; ";
        $dataReader=$this->querySQL($sql);
        if (($row = $dataReader->read())!==false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /** Returns and initializes the last successful real payment transaction
     * @param integer $userId
     *
     * @return bool|Payment
     */
    public function getLastSuccessPaymentNoFree($userId)
    {
        $sql = " SELECT ".$this->mainFields()."
            FROM ".$this->tableName()." p
            WHERE p.userId=$userId AND
            p.`status`=".PAY_SUCCESS." AND p.`paySuppExtId` NOT IN (".PAY_SUPPLIER_GROUPON.",".PAY_SUPPLIER_B2B.")
            ORDER BY p.dateStartTransaction DESC, p.dateEndTransaction DESC
            LIMIT 1; ";
        $dataReader=$this->querySQL($sql);
        if (($row = $dataReader->read())!==false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /** Returns and initializes the last successful payment transaction even if it is 0 value.
     * @param integer $userId
     *
     * @return bool|Payment
     */
    public function getLastSuccessPayment($userId)
    {
        $sql = " SELECT ".$this->mainFields()."
            FROM ".$this->tableName()." p
            WHERE p.userId=$userId AND
            p.`status`=".PAY_SUCCESS.
            " ORDER BY p.dateStartTransaction DESC, p.dateEndTransaction DESC
            LIMIT 1; ";

        $dataReader=$this->querySQL($sql);
        if(($row = $dataReader->read())!==false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $userId
     *
     * @return $this|bool
     */
    public function getActiveSuccessPlanPayment($userId)
    {
        $sSql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " p
            JOIN `user` AS u ON u.id = p.userId AND u.userType = '" . PREMIUM . "'
            JOIN products_prices AS pp ON pp.idProduct = p.idProduct
            WHERE
                p.userId = '" . $userId . "'
                AND p.`status` = '" . PAY_SUCCESS . "'
                AND p.`paySuppExtId` NOT IN (" . PAY_SUPPLIER_GROUPON . "," . PAY_SUPPLIER_B2B . ")
                AND pp.`isPlanPrice` = 1
            ORDER BY p.dateStartTransaction DESC, p.dateEndTransaction DESC
            LIMIT 1;
        ";
        $dataReader = $this->querySQL($sSql);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }


    /** Based on the original payment creates the PENDING one, so the next one.
     * @return bool|Payment
     */
    public function createNextPayment()
    {
        $this->idPromoCode = "";
        $this->idPartner = PARTNER_ID_RECURRINGPAY;
        $this->isRecurring = 1;
        $this->paySuppExtUniId = null;
        $this->paySuppLinkStatus = null;
        $this->amountDiscount = 0.00;
        $this->amountPrice = $this->amountOriginal;
        $this->status = PAY_PENDING ;
        $this->dateStartTransaction = '0000-00-00';
        $this->dateEndTransaction = '0000-00-00';

        $prodPeriodicity = new ProdPeriodicity();
        $prodPeriodicity->getProdPeriodById($this->idPeriodPay);
        if (!$prodPeriodicity) {
            return false;
        }
        $this->dateToPay = $prodPeriodicity->getNextDateBasedOnDate($this->dateToPay);
        
        //
        //#5739 - ! before setAmountWithoutTax() !
        $this->setOriginalProductPrice();

        // tax rate
        $this->setAmountWithoutTax();

        if (!$this->validate()) {
            return false;
        }
        return $this;
    }

    /**
     * @return Payment
     */
    public function createDuplicateNextPayment()
    {
        $newPay = new Payment();
        $newPay = clone($this);
        $newPay = $newPay->createNextPayment();
        return $newPay;
    }

    /**
     * @param integer $userId; optional
     * @return bool
     */
    public function paymentProcess($userId = null) {
        $bInsert = $this->insertPayment();
        if($bInsert && ($this->status == PAY_SUCCESS || $this->status == PAY_REFUND) && $this->validateInvoicePartner()) {
            $moInvoice = new AbaUserInvoices();
            if(!is_numeric($userId)) {
                $userId = Yii::app()->user->getId();
            }
            if(!$moInvoice->createPreInvoice($this, $userId)) {
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_LOGIC_L . ' Process create pre invoice failed',
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    'paymentProcess - createPreInvoice'
                );
            }
        }
        return $bInsert;
    }

    /**
     * @return bool
     */
    public function insertPayment()
    {
        $this->beginTrans();
        $successTrans = true;
        //-----------------
        $sql = ' INSERT INTO payments
        (`id`,`idUserProdStamp`, `userId`, `idProduct`, `idCountry`, `idPeriodPay`,
        `idUserCreditForm`, `paySuppExtId`, `paySuppOrderId`, `status`, `dateStartTransaction`,
        `dateEndTransaction`,`dateToPay`, `xRateToEUR`, `xRateToUSD`, `currencyTrans`,
        `foreignCurrencyTrans`, `idPromoCode`, `amountOriginal`,  `amountDiscount`, `amountPrice`, `idPayControlCheck`,
        `idPartner`, `paySuppExtProfId`, `idPartnerPrePay`, `isRecurring`, `paySuppExtUniId`, `isPeriodPayChange`,
        `cancelOrigin`, `taxRateValue`, `amountTax`, `amountPriceWithoutTax`, `isExtend`, `experimentVariationAttributeId`,
        `idSession` )
             VALUES(:ID, :IDUSERPRODSTAMP , :USERID , :IDPRODUCT , :IDCOUNTRY , :IDPERIODPAY ,
            :IDUSERCREDITFORM , :PAYSUPPEXTID , :PAYSUPPORDER , :STATUS , :DATESTARTTRANSACTION ,
            :DATEENDTRANSACTION ,:DATETOPAY , :XRATETOEUR , :XRATETOUSD , :CURRENCYTRANS ,
            :FOREIGNCURRENCYTRANS ,:IDPROMOCODE , :AMOUNTORIGINAL , :AMOUNTDISCOUNT , :AMOUNTPRICE , :IDPAYCONTROLCHECK,
            :IDPARTNER, :PAYSUPPEXTPROFID, :IDPARTNERPREPAY, :ISRECURRING, :PAYSUPPEXTUNIID, :ISPERIODPAYCHANGE,
            :CANCELORIGIN, :TAXRATEVALUE, :AMOUNTTAX, :AMOUNTPRICEWITHOUTTAX, :ISEXTEND, :experimentVariationAttributeId,
            :idSession) ';

        $aParamValues = array(
            ':ID'=>$this->id,':IDUSERPRODSTAMP'=> $this->idUserProdStamp,  ':USERID'=> $this->userId,  ':IDPRODUCT'=> $this->idProduct,
            ':IDCOUNTRY'=> $this->idCountry,  ':IDPERIODPAY'=> $this->idPeriodPay,':IDUSERCREDITFORM'=> $this->idUserCreditForm,
            ':PAYSUPPEXTID'=> $this->paySuppExtId,  ':PAYSUPPORDER'=> $this->paySuppOrderId,  ':STATUS'=> $this->status,
            ':DATESTARTTRANSACTION'=> $this->dateStartTransaction, ':DATEENDTRANSACTION'=> $this->dateEndTransaction,
            ':DATETOPAY'=> $this->dateToPay,  ':XRATETOEUR'=> $this->xRateToEUR,  ':XRATETOUSD'=> $this->xRateToUSD,
            ':CURRENCYTRANS'=> $this->currencyTrans, ':FOREIGNCURRENCYTRANS'=> $this->foreignCurrencyTrans,
            ':IDPROMOCODE'=> $this->idPromoCode,  ':AMOUNTORIGINAL'=> $this->amountOriginal,  ':AMOUNTDISCOUNT'=> $this->amountDiscount,
            ':AMOUNTPRICE'=> $this->amountPrice, ':IDPAYCONTROLCHECK'=>$this->idPayControlCheck,
            ':IDPARTNER'=> $this->idPartner, ':PAYSUPPEXTPROFID'=>$this->paySuppExtProfId, ':IDPARTNERPREPAY'=> $this->idPartnerPrePay,
            ':ISRECURRING'=> $this->isRecurring, ':PAYSUPPEXTUNIID'=> $this->paySuppExtUniId, ':ISPERIODPAYCHANGE'=>$this->isPeriodPayChange,
            ':CANCELORIGIN'=> $this->cancelOrigin, ':TAXRATEVALUE'=> $this->taxRateValue, ':AMOUNTTAX'=> $this->amountTax,
            ':AMOUNTPRICEWITHOUTTAX'=> $this->amountPriceWithoutTax, ':ISEXTEND'=>$this->isExtend,
            ':experimentVariationAttributeId' => $this->experimentVariationAttributeId, ':idSession' => $this->idSession
      );

        $this->autoId = $this->executeSQL($sql, $aParamValues);
        if( $this->autoId > 0 )
        {
            $user = new AbaUser();
            if($user->getUserById( $this->userId ))
            {
                if(!$user->abaUserLogUserActivity->saveUserLogActivity($this->tableName(),
                    "New payment DONE AND INSERTED through ".$this->paySuppExtId." gateway", "Payment confirmed"))
                {
                    $successTrans= false;
                }
            }
        }
        else
        {
            $successTrans = false;
        }
        //---------------
        if( $successTrans )
        {
            $this->commitTrans();
            return true;
        }
        else
        {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @param $paySuppExtId
     * @param $paySuppOrderId
     * @param $status
     * @param $dateStart
     * @param $dateEnd
     * @param $xRateToEUR
     * @param $xRateToUSD
     * @param string|null $paySuppExtUniId
     *
     * @return bool
     */
    public function recurringPaymentProcess( $paySuppExtId, $paySuppOrderId, $status, $dateStart, $dateEnd, $xRateToEUR, $xRateToUSD, $paySuppExtUniId=NULL, $paySuppExtProfId='' )
    {
        $bUpdate = $this->updateRecurringPayment($paySuppExtId, $paySuppOrderId, $status, $dateStart, $dateEnd, $xRateToEUR, $xRateToUSD, $paySuppExtUniId, $paySuppExtProfId);

        try {
            if ($bUpdate && $this->validateInvoicePartner()) {
                $oPayment = new Payment();
                $oPayment->getPaymentById($this->id);
                $moInvoice = new AbaUserInvoices();
                if(!$moInvoice->createPreInvoice($oPayment, $oPayment->userId)) {
                    HeLogger::sendLog(
                      HeLogger::PREFIX_ERR_LOGIC_L . ' Process create pre invoice failed',
                        HeLogger::IT_BUGS,
                        HeLogger::CRITICAL,
                        'recurringPaymentProcess - createPreInvoice'
                    );
                }
            }
            else {
                HeLogger::sendLog(
                  HeLogger::PREFIX_ERR_LOGIC_L . ' Process create pre invoice failed',
                    HeLogger::IT_BUGS,
                    HeLogger::CRITICAL,
                    'recurringPaymentProcess - validateInvoicePartner'
                );
            }
        }
        catch (Exception $e) {
            HeLogger::sendLog(
              HeLogger::PREFIX_ERR_LOGIC_L . ' Process create pre invoice failed',
                HeLogger::IT_BUGS,
                HeLogger::CRITICAL,
                'recurringPaymentProcess'
            );
        }

        return $bUpdate;
    }


    /**
     * @param $paySuppExtId
     * @param $paySuppOrderId
     * @param $status
     * @param $dateStart
     * @param $dateEnd
     * @param $xRateToEUR
     * @param $xRateToUSD
     * @param string|null $paySuppExtUniId
     *
     * @return bool
     */
    public function updateRecurringPayment(
      $paySuppExtId,
      $paySuppOrderId,
      $status,
      $dateStart,
      $dateEnd,
      $xRateToEUR,
      $xRateToUSD,
      $paySuppExtUniId = null,
      $paySuppExtProfId = ''
    ) {

        //#4480
        if ($paySuppExtId != $this->paySuppExtId) {

            $paySuppExtIdTmp = $this->paySuppExtId;
            $this->paySuppExtId = $paySuppExtId;
            $this->setAmountWithoutTax($dateEnd);
            $this->paySuppExtId = $paySuppExtIdTmp;
        } else {
            $this->setAmountWithoutTax($dateEnd);
        }
        $this->beginTrans();

        $stData = array(
          ':PAYSUPPEXTUNIID' => $paySuppExtUniId,
          ':TAXRATEVALUE' => $this->taxRateValue,
          ':AMOUNTTAX' => $this->amountTax,
          ':AMOUNTPRICEWITHOUTTAX' => $this->amountPriceWithoutTax
        );

        $sql = " UPDATE " . $this->tableName() . " p SET p.`paySuppExtId`='$paySuppExtId', p.`paySuppOrderId`='$paySuppOrderId',
                        p.`status`=$status, p.`dateStartTransaction`='$dateStart', p.`dateEndTransaction`='$dateEnd',
                        p.xRateToEUR = $xRateToEUR , p.xRateToUSD = $xRateToUSD,
                        p.`lastAction` = IF(`lastAction` IS NULL,
                                        CONCAT( NOW(), ' [ ', 'Recurrent payment SUCCESS', ' ] ' ),
                                        CONCAT(`lastAction`, ' ; ', NOW(), ' [ ', 'Recurrent payment SUCCESS', ' ] ')
                                        ),
                        p.paySuppExtUniId = :PAYSUPPEXTUNIID,
                        p.taxRateValue = :TAXRATEVALUE,
                        p.amountTax = :AMOUNTTAX,
                        p.amountPriceWithoutTax = :AMOUNTPRICEWITHOUTTAX";

        if($this->paySuppExtId == PAY_SUPPLIER_CAIXA) {
            if(trim($this->paySuppExtProfId) == '' AND trim($paySuppExtProfId) <> '') {
                $stData[":PAYSUPPEXTPROFID"] = $paySuppExtProfId;
                $sql .= ",
                        p.`paySuppExtProfId` = :PAYSUPPEXTPROFID ";
            }
        }

        $sql .= "
                WHERE p.`id`='" . $this->id . "' ; ";

        if ($this->updateSQL($sql, $stData) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /** Used to update the dateToPay, in the scenario that an active PREMIUM user has executed another payment, so that the current
     * pending payment is due to be executed further ahead.
     *
     *
     * @param string $sName
     * @param $dateToPay
     *
     * @return bool
     */
    public function updateDateToPayInPending($sName='Groupon', $dateToPay='')
    {
        if ($dateToPay==''){
            $dateToPay = $this->dateToPay;
        }

        $this->beginTrans();
        $sql = " UPDATE ".$this->tableName()." p SET p.`dateToPay`= :DATETOPAY,
                        p.`lastAction` = IF(`lastAction` IS NULL,
                                        CONCAT( NOW(), ' [ ', 'Extend dateToPay due to " . $sName . " purchase', ' ] ' ),
                                        CONCAT(`lastAction`, ' ; ', NOW(), ' [ ', 'Extend dateToPay due to " . $sName . " purchase', ' ] ')
                                  )
                WHERE p.`id`='".$this->id."' ; ";

        $aParamValues = array(':DATETOPAY' => $dateToPay);
        if ( $this->updateSQL( $sql, $aParamValues ) > 0 ) {
            $this->dateToPay = $dateToPay;
            $this->commitTrans();
            return true;
        }
        else
        {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @param $dateStartTrans
     * @param string $actionDetails
     * @param int $cancelOrigin
     *
     * @return bool
     */
    public function updateCancelRenewPay($dateStartTrans, $actionDetails="", $cancelOrigin=null)
    {
        $this->beginTrans();

        $sql = "UPDATE ".$this->tableName()." SET `status`=".PAY_CANCEL.", `dateStartTransaction` = '".$dateStartTrans."',
                `dateEndTransaction` = NOW(),
                `cancelOrigin` = :CANCELORIGIN,
                `lastAction` = IF(`lastAction` IS NULL,
                                        CONCAT( NOW(), ' [ ', :LASTPAYMENT, ' ] ' ),
                                        CONCAT(`lastAction`, ' ; ', NOW(), ' [ ', :LASTPAYMENT, ' ] ')
                                  )
                WHERE `id` = '".$this->id."' ;";
        $aParamValues = array(':LASTPAYMENT'=>$actionDetails, ':CANCELORIGIN'=>$cancelOrigin);

        if ( $this->updateSQL( $sql, $aParamValues ) > 0 )
        {
            $this->commitTrans();
            return true;
        }
        else
        {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @param date   $dateStartTrans
     * @param string $actionDetails
     *
     * @return bool
     */
    public function updateFailedRenewPay($dateStartTrans, $actionDetails="")
    {
        $this->beginTrans();

        $sql = "UPDATE ".$this->tableName()." SET `status`=".PAY_FAIL.", `dateStartTransaction` = '".$dateStartTrans."',
                `paySuppOrderId` = :PAYSUPPORDERID,
                `dateEndTransaction` = NOW(),
                `cancelOrigin` = " . STATUS_CANCEL_EXPFAIL . ",
                `lastAction` = IF(`lastAction` IS NULL,
                                        CONCAT( NOW(), ' [ ', :LASTPAYMENT, ' ] ' ),
                                        CONCAT(`lastAction`, ' ; ', NOW(), ' [ ', :LASTPAYMENT, ' ] ')
                                  )
                WHERE `id` = '".$this->id."' ;";
        $aParamValues = array(':LASTPAYMENT'=>$actionDetails, ':PAYSUPPORDERID'=> $this->paySuppOrderId);

        if ( $this->updateSQL( $sql, $aParamValues ) > 0 )
        {
            $this->commitTrans();
            return true;
        }
        else
        {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @return bool
     */
    public function updateAppstoreRecurringPayment($dateEnd)
    {

        $this->setAmountWithoutTax($dateEnd);

        $this->beginTrans();
        $sSql = " UPDATE ".$this->tableName()." p SET
                    p.`idProduct`= :IDPRODUCT,
                    p.`idCountry`= :IDCOUNTRY,
                    p.`idPeriodPay`= :IDPERIODPAY,
                    p.`xRateToEUR`= :XRATETOEUR,
                    p.`xRateToUSD`= :XRATETOUSD,
                    p.`currencyTrans`= :CURRENCYTRANS,
                    p.`foreignCurrencyTrans`= :FOREIGNCURRENCYTRANS,
                    p.`lastAction`= IF(`lastAction` IS NULL,
                                        CONCAT( NOW(), ' [ ', 'Recurrent appstore payment CHANGED', ' ] ' ),
                                        CONCAT(`lastAction`, ' ; ', NOW(), ' [ ', 'Recurrent appstore payment CHANGED', ' ] ')
                                    ),
                    p.amountOriginal = :AMOUNTORIGINAL,
                    p.amountPrice = :AMOUNTPRICE,
                    p.taxRateValue = :TAXRATEVALUE,
                    p.amountTax = :AMOUNTTAX,
                    p.amountPriceWithoutTax = :AMOUNTPRICEWITHOUTTAX
                WHERE p.`id`='".$this->id."' ; ";

        if ( $this->updateSQL( $sSql, array(
                ':IDPRODUCT' =>             $this->idProduct,
                ':IDCOUNTRY' =>             $this->idCountry,
                ':IDPERIODPAY' =>           $this->idPeriodPay,
                ':XRATETOEUR' =>            $this->xRateToEUR,
                ':XRATETOUSD' =>            $this->xRateToUSD,
                ':CURRENCYTRANS' =>         $this->currencyTrans,
                ':FOREIGNCURRENCYTRANS' =>  $this->foreignCurrencyTrans,
                ':AMOUNTORIGINAL' =>        $this->amountOriginal,
                ':AMOUNTPRICE' =>           $this->amountPrice,
                ':TAXRATEVALUE' =>          $this->taxRateValue,
                ':AMOUNTTAX' =>             $this->amountTax,
                ':AMOUNTPRICEWITHOUTTAX' => $this->amountPriceWithoutTax
            ) ) > 0 )
        {
            $this->commitTrans();
            return true;
        }
        else
        {
            $this->rollbackTrans();
            return false;
        }
    }

    /** LaCaixa migration process. Update payment reference
     *
     * @param $paySuppExtProfId
     *
     * @return bool
     */
    public function updatePaySuppExtProfIdPayment($paySuppExtProfId)
    {
        $this->beginTrans();
        $sSql = "
            UPDATE " . $this->tableName() . " p
            SET
                p.`paySuppExtProfId` = :PAYSUPPEXTPROFID,
                p.`lastAction` = IF(`lastAction` IS NULL,
                    CONCAT( NOW(), ' [ ', 'Update laCaixa payment reference', ' ] ' ),
                    CONCAT(`lastAction`, ' ; ', NOW(), ' [ ', 'Update laCaixa payment reference', ' ] ')
                )
            WHERE p.`id` = '" . $this->id . "';
        ";

        if ( $this->updateSQL( $sSql, array(':PAYSUPPEXTPROFID' => $paySuppExtProfId)) > 0 ) {
            $this->commitTrans();
            return true;
        }
        else
        {
            $this->rollbackTrans();
            return false;
        }
    }


    /**
     * --------------------------------------------------------------------- END SQL FUNCTIONS
      */
    /**
     * @param string $email
     *
     * @return AbaUser|bool
     */
    public function findParentAbaUser($email="")
    {
        if($email=="")
        {
            $email = Yii::app()->user->getEmail();
        }

        $parentAbaUser = new AbaUser();
        if($parentAbaUser->getUserByEmail($email, "", false))
        {
            $this->parentAbaUser = $parentAbaUser;
            return $this->parentAbaUser;
        }
        return false;
    }


    /** For an Upgrade or downgrade we copy some values from the previous CANCELLED payment
     *
     * @param string $newId
     * @param PaymentControlCheck $PayControlDraft
     * @param Payment $moPendingOld
     * @param integer $idPartner
     * @param integer $caseChangePlan SIGNED INTEGER
     *
     * @return $this|bool
     */
    public function copyFromCancelPayToPending( $newId, PaymentControlCheck $PayControlDraft,
                                                Payment $moPendingOld, $idPartner, $caseChangePlan )
    {
        // New Values, taken from the DRAFT
        $this->id = $newId;
        $this->idProduct = $PayControlDraft->idProduct;
        $this->idCountry = $PayControlDraft->idCountry;
        $this->idPeriodPay = $PayControlDraft->idPeriodPay;
        $this->idUserProdStamp = $PayControlDraft->idUserProdStamp;
        $this->idPromoCode = $PayControlDraft->idPromoCode;
        $this->amountOriginal = $PayControlDraft->amountOriginal;
        $this->amountDiscount = $PayControlDraft->amountDiscount;
        $this->amountPrice = $PayControlDraft->amountPrice;
        $this->xRateToEUR = $PayControlDraft->xRateToEUR;
        $this->xRateToUSD = $PayControlDraft->xRateToUSD;
        /* Price should be converted to the original currency from the
         original payment PENDING, but with the price from the current $PayControlDraft: */
        $country = new AbaCountry($moPendingOld->idCountry);
        if ( $moPendingOld->currencyTrans != $country->ABAIdCurrency ){
            $newCcy = new AbaCurrency($country->ABAIdCurrency);
            $this->amountOriginal = $newCcy->getConversionAmountTo($PayControlDraft->amountOriginal, $moPendingOld->currencyTrans);
            $this->amountDiscount = $newCcy->getConversionAmountTo($PayControlDraft->amountDiscount, $moPendingOld->currencyTrans);
            $this->amountPrice = $newCcy->getConversionAmountTo($PayControlDraft->amountPrice, $moPendingOld->currencyTrans);
            $oldCcy = new AbaCurrency($moPendingOld->currencyTrans);
            $this->xRateToEUR =   $oldCcy->getConversionRateTo(CCY_DEFAULT);
            $this->xRateToUSD = $oldCcy->getConversionRateTo(CCY_2DEF_USD);
        }
        /* --------------------- */
        $this->status = PAY_PENDING;
        $this->dateStartTransaction = $PayControlDraft->dateStartTransaction;
        $this->dateEndTransaction = '0000-00-00 00:00:00';
        $this->idPartner = $idPartner;
        $this->isRecurring = 1;
        $this->isExtend = 0;
        $this->isPeriodPayChange = $caseChangePlan;

        // Old values, copied from the old PENDING one:
        $this->idPayControlCheck = $moPendingOld->id;
        $this->userId = $moPendingOld->userId;
        $this->currencyTrans = $moPendingOld->currencyTrans;
        $this->foreignCurrencyTrans = $moPendingOld->foreignCurrencyTrans;
        $this->idUserCreditForm = $moPendingOld->idUserCreditForm;
        $this->paySuppExtId = $moPendingOld->paySuppExtId;
        $this->paySuppOrderId = $moPendingOld->paySuppOrderId;
        $this->paySuppExtProfId = $moPendingOld->paySuppExtProfId;
        $this->dateToPay = $moPendingOld->dateToPay;

        //
        // tax rate
        //
        $this->setAmountWithoutTax();

        if (!$this->validate()) {
            return false;
        }

        return $this;
    }

    /**
     * @param PaymentControlCheck $paymentControlCheck
     * @param AbaUserCreditForms $userCreditCard
     * @param PayGatewayLog $paymentSupplierLog
     * @param integer $idPartner
     * @param int $isExtend
     *
     * @return bool
     */
    public function copyFromPayControlCheckToPayment( PaymentControlCheck $paymentControlCheck,
                                                 AbaUserCreditForms $userCreditCard=null,
                                                 PayGatewayLog $paymentSupplierLog=null, $idPartner=null, $isExtend=0, $isRecurring = 0 )
    {
        $this->id = $paymentControlCheck->id;
        $this->userId = $paymentControlCheck->userId;

        $this->idProduct = $paymentControlCheck->idProduct;
        $this->idCountry = $paymentControlCheck->idCountry;
        $this->idPeriodPay = $paymentControlCheck->idPeriodPay;

        $this->idUserProdStamp = $paymentControlCheck->idUserProdStamp;

        $this->idPromoCode = $paymentControlCheck->idPromoCode;
        $this->currencyTrans = $paymentControlCheck->currencyTrans;
        $this->foreignCurrencyTrans = $paymentControlCheck->foreignCurrencyTrans;
        $this->amountOriginal = $paymentControlCheck->amountOriginal;
        $this->amountDiscount = $paymentControlCheck->amountDiscount;
        $this->amountPrice = $paymentControlCheck->amountPrice;

        if(isset($userCreditCard)){
            $this->idUserCreditForm = $userCreditCard->id;
        }

        $this->xRateToEUR = $paymentControlCheck->xRateToEUR;
        $this->xRateToUSD = $paymentControlCheck->xRateToUSD;
        if( isset($paymentSupplierLog) ){
            $this->paySuppExtId =       $paymentSupplierLog->getPaySuppExtId();    // $paymentSupplier->id;
            $this->paySuppOrderId =     $paymentSupplierLog->getOrderNumber() ;  // $paymentSupplier->orderId;
            $this->paySuppExtUniId =    $paymentSupplierLog->getUniqueId();
            //
            //#4681
            $this->paySuppExtProfId =   $paymentSupplierLog->getPaySuppExtProfId();
        }

        $this->status = PAY_SUCCESS ;
        $this->dateStartTransaction = $paymentControlCheck->dateStartTransaction;
        $this->dateEndTransaction = date("Y-m-d H:i:s");
        $this->dateToPay = $paymentControlCheck->dateToPay;
        $this->idPayControlCheck = $paymentControlCheck->id;
        $this->idPartner = $idPartner;
        $this->isRecurring = $isRecurring;
        $this->isExtend = $isExtend;

        //
        //#5739
        $this->experimentVariationAttributeId = $paymentControlCheck->experimentVariationAttributeId;
        $this->idSession = $paymentControlCheck->idSession;

        //
        // tax rate
        //
        $this->setAmountWithoutTax($this->dateEndTransaction);

        if( !$this->validate() ) {
            return false;
        }

        return true;
    }

    /**
     * @param $aComputedValues
     *
     * @return bool
     */
    public function setPayment( $aComputedValues )
    {
        foreach( $aComputedValues as $fieldName=>$value )
        {
            $this->$fieldName = $value;
        }

        if( !$this->validate() )
        {
            return false;
        }

        return true;
    }

    public function validate($attributes=null, $clearErrors=true)
    {
        if( !parent::validate() ){
            return false;
        }
        // All extra and customized validations
        return true;
    }


    /**
     * NO GROUPON OR B2B OR APPSTORE
     *
     * @return bool
     */
    public function validateInvoicePartner() {

        //#4480
        // if($this->paySuppExtId == PAY_SUPPLIER_GROUPON || $this->paySuppExtId == PAY_SUPPLIER_B2B || $this->paySuppExtId == PAY_SUPPLIER_APP_STORE) {
        if($this->paySuppExtId == PAY_SUPPLIER_GROUPON || $this->paySuppExtId == PAY_SUPPLIER_B2B) {
            return false;
        }
        return true;
    }

    /**
     * @param $idUserCreditForm
     * @return bool
     */
    public function updateCreditForm($idUserCreditForm)
    {
        $this->beginTrans();

        $sql = "UPDATE ".$this->tableName()." SET `idUserCreditForm`=:IDUSERCREDITFORM,
                                    `lastAction` = IF(`lastAction` IS NULL,
                                        CONCAT( NOW(), ' [ ', :LASTACTION, ' ] ' ),
                                        CONCAT(`lastAction`, ' ; ', NOW(), ' [ ', :LASTACTION, ' ] ')
                                         )
                WHERE `id` = '".$this->id."' ;";
        $aParamValues = array(':IDUSERCREDITFORM'=>$idUserCreditForm,
                              ':LASTACTION'=> "Change in credit form from  ".
                                $this->idUserCreditForm." to ".$idUserCreditForm);
        if ( $this->updateSQL( $sql, $aParamValues ) > 0 )
        {
            $this->commitTrans();
            return true;
        }
        else
        {
            $this->rollbackTrans();
            return false;
        }
    }

    /** Returns last FAILED OR CANCEL PAYMENT for a certain user.
     *
     * @param integer $userId
     *
     * @return $this|bool
     */
    public function getLastCancelPayment($userId)
    {
        $sql = " SELECT " . $this->mainFields() .
            " FROM " . $this->tableName() . " p " .
            " WHERE p.`userId`=".$userId.
            " AND p.`status` IN (" . PAY_CANCEL .",".PAY_FAIL. ")".
            " ORDER BY p.`dateEndTransaction` DESC LIMIT 1; ";

        $dataReader = $this->querySQL($sql);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }

        return false;
    }

    /** Returns last payment whether PENDING or CANCEL from the last subscription.
     * @param integer $userId
     *
     * @return $this|bool
     */
    public function getLastPayLastSubscription($userId)
    {
        if (!$this->getLastPayPendingByUserId($userId)) {
            if (!$this->getLastCancelPayment($userId)) {
                return false;
            }
        }

        return $this;
    }


    /**
     * @return bool|float
     */
    public function getAmountPriceInForeignCcy()
    {
        $moCurrentCcy = new AbaCurrency($this->currencyTrans);
        $amountConverted = $moCurrentCcy->getConversionAmountTo($this->amountPrice, $this->foreignCurrencyTrans);

        if (is_numeric($amountConverted)) {
            return $amountConverted;
        }

        return false;
    }


    /** Returns a description of the product purchased by the user with dates period inclusive.
     * @param string $langEnv
     *
     * @return string
     */
    public function getProductPeriodDesc($langEnv="")
    {
        if (empty($langEnv)) {
            $userId = Yii::app()->user->getId();
            if (empty($userId)) {
                $moUser = new AbaUser();
                $moUser->getUserById($this->userId);
                $langEnv = $moUser->langEnv;
            } else {
                $langEnv = Yii::app()->user->getLanguage();
            }
        }

        $moProduct = new ProductPrice();
        $moProduct->getProductById($this->idProduct, 0);
        $moPeriodicity = new ProdPeriodicity();
        $moPeriodicity->getProdPeriodById($this->idPeriodPay);
        $dateCourseUntil = $moPeriodicity->getNextDateBasedOnDate($this->dateEndTransaction);
        $dateCourseUntil = HeDate::removeTimeFromSQL($dateCourseUntil);
        $dateCourseFrom = HeDate::removeTimeFromSQL($this->dateEndTransaction);
        $strPeriodCourse = " " . $dateCourseFrom . " / " . $dateCourseUntil;
        return Yii::t('mainApp', $moProduct->descriptionText, array(), null, $langEnv) . $strPeriodCourse;
    }


    /**
     * Set tax value, tax amount and total amount without tax
     *
     * @return bool
     */
    public function setAmountWithoutTax($dateEnd='') {

        //#4480
        if($this->paySuppExtId == PAY_SUPPLIER_APP_STORE) {

            $this->taxRateValue =           0;
            $this->amountTax =              0;
            $this->amountPriceWithoutTax =  $this->amountPrice;

            return true;
        }


        $oTaxRate = new TaxRates();
        $oTaxRate->getTaxRateByCountry($this->idCountry, $dateEnd);

        $iAmountPriceWithoutTax =       $oTaxRate->getPriceWithoutTax($this->amountPrice);

        $this->taxRateValue =           $oTaxRate->getTaxRateValue();
        $this->amountTax =              HeMixed::getRoundAmount($this->amountPrice - $iAmountPriceWithoutTax);
        $this->amountPriceWithoutTax =  HeMixed::getRoundAmount($iAmountPriceWithoutTax);

        return true;
    }

    /**
     * @param int $discountPercent
     *
     * @return bool
     */
    public function setExtranetAmounts($priceExtranet=0, $discountPercent=0) {

        if(!is_numeric($discountPercent)) {
            $discountPercent = 0;
        }

        $amountDiscount =   HeMixed::getRoundAmount($priceExtranet / 100 * $discountPercent);
        $amountPrice =      HeMixed::getRoundAmount($priceExtranet - $amountDiscount);

        $this->amountOriginal = $priceExtranet;
        $this->amountDiscount = $amountDiscount;
        $this->amountPrice =    $amountPrice;

        return true;
    }

    /**
     * Specific scenarios payments function
     *
     * @return bool
     */
    public function setOriginalProductPrice() {

        if(is_numeric($this->experimentVariationAttributeId) AND $this->experimentVariationAttributeId > 0) {

            $oAbaExperimentsVariationsAttributes = new ExperimentsVariationsAttributes();

            if($oAbaExperimentsVariationsAttributes->getExperimentsVariationsAttributesPriceById($this->experimentVariationAttributeId)) {

                $oProductPrice = new ProductPrice();

                if($oProductPrice->getProductById($oAbaExperimentsVariationsAttributes->idProduct)) {

                    $this->amountOriginal = $oProductPrice->priceOfficialCry;
                    $this->amountPrice =    $oProductPrice->priceOfficialCry;
                }
            }
        }

        return true;
    }

    /**
     * @param $userId
     * @param $idUserCreditForm
     * @param $paySuppOrderId
     * @param $idPayControlCheck
     *
     * @return bool
     */
    public function getPaymentByRecurringPayment( $userId, $idUserCreditForm, $paySuppOrderId, $idPayControlCheck )
    {
        $sql = "
            SELECT ".$this->mainFields()."
            FROM ".$this->tableName()." p
            WHERE
                p.`userId` = :USERID
                AND p.`idUserCreditForm` = :IDUSERCREDITFORM
                AND p.`paySuppOrderId` = :PAYSUPPORDERID
                AND p.`idPayControlCheck` = :IDPAYCONTROLCHECK
                AND p.`status` = :STATUS
            LIMIT 1";

        $paramsToSQL = array(":USERID" => $userId, ":IDUSERCREDITFORM" => $idUserCreditForm, ":PAYSUPPORDERID" => $paySuppOrderId, ":IDPAYCONTROLCHECK" => $idPayControlCheck, ":STATUS" => PAY_SUCCESS);

        $dataReader=$this->querySQL( $sql, $paramsToSQL);
        if(($row = $dataReader->read())!==false)
        {
            $this->fillDataColsToProperties( $row );
            return true;
        }
        return false;
    }

    /**
     * @param $userId
     * @param $paySuppExtUniId
     * @param $idPayControlCheck
     *
     * @return bool
     */
    public function getAdyenHppSuccessPayment( $userId, $paySuppExtUniId, $idPayControlCheck )
    {
        $sql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " p
            WHERE
                p.`userId` = :USERID
                AND p.`paySuppExtUniId` = :PAYSUPPEXTUNIID
                AND p.`idPayControlCheck` = :IDPAYCONTROLCHECK
            LIMIT 1";

        $paramsToSQL =  array(":USERID" => $userId, ":PAYSUPPEXTUNIID" => $paySuppExtUniId, ":IDPAYCONTROLCHECK" => $idPayControlCheck);
        $dataReader =   $this->querySQL( $sql, $paramsToSQL);
        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties( $row );
            if($this->status == PAY_SUCCESS OR $this->status == PAY_PENDING) {
                return true;
            }
        }
        return false;
    }
    /**
     * @param $userId
     *
     * @return bool|Payment
     */
    public function getLastValidSuccessPaymentNoFree( $userId, $bIsExtend=null, $notPaymentIds=array() )
    {
        $sSql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " p
            JOIN `user` AS u ON u.id=p.userId AND u.userType='" . PREMIUM . "'
            WHERE
                p.userId='" . $userId . "' AND p.`status`=" . PAY_SUCCESS . "
                AND p.`paySuppExtId` NOT IN (" . PAY_SUPPLIER_GROUPON . "," . PAY_SUPPLIER_B2B . ")
        ";
//        AND p.`paySuppExtId` NOT IN (" . PAY_SUPPLIER_GROUPON . "," . PAY_SUPPLIER_B2B . "," . PAY_SUPPLIER_ALLPAGO_BR_BOL . "," . PAY_SUPPLIER_APP_STORE . ")
        if(is_numeric($bIsExtend)) {
            $sSql .= "
                AND p.`isExtend`='" . $bIsExtend . "'
            ";
        }
        if(count($notPaymentIds) > 0) {
            $sSql .= "
                AND p.`id` NOT IN ('" . implode("', '", $notPaymentIds) . "')
            ";
        }
        $sSql .= "
            ORDER BY p.dateStartTransaction DESC, p.dateEndTransaction DESC
            LIMIT 1;
        ";

        $dataReader = $this->querySQL($sSql);
        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $userId
     * @param $paymentId
     *
     * @return $this
     */
    public function getRefererOfRefundPayment($userId, $pckId)
    {
        $sSql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " p
            WHERE
                p.userId='" . $userId . "' AND p.idPayControlCheck='" . $pckId . "' AND p.`status`=" . PAY_REFUND . "
        ";
        $dataReader = $this->querySQL($sSql);
        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isRefundedSuccessPayment() {

        if($this->status == PAY_SUCCESS) {

            if(HePayments::isParcialSuccess($this->id)) {
                return false;
            }

            $oPaymentRefunded = new Payment();
            if($oPaymentRefunded->getRefererOfRefundPayment($this->userId, $this->idPayControlCheck)) {
                if($oPaymentRefunded->idPayControlCheck == $this->idPayControlCheck) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param $userId
     *
     * @return bool
     */
    public function userHasActiveFamilyPlan( $userId ) {

        $oActivePlanPayment = new Payment();

        if($oActivePlanPayment->getActiveSuccessPlanPayment($userId)) {

            $iMonthPeriod =             HePayments::periodToMonth($oActivePlanPayment->idPeriodPay);
            $sPaymentExpirationDate =   HeDate::getDateAdd($oActivePlanPayment->dateToPay, 0, $iMonthPeriod);

            if(HeDate::isGreaterThanToday($sPaymentExpirationDate, false)) {

                if(!$oActivePlanPayment->isRefundedSuccessPayment()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return array
     */
    public function getPaymentInfoForSelligent( ) {

        try {
            $iPeriodMonth = HePayments::periodToMonth($this->idPeriodPay);
            $stDateStart =  explode(" ", $this->dateToPay);
            $sDateStart =   $stDateStart[0];
            $sDateEnd =     HeDate::getDateAdd($this->dateToPay, 0, $iPeriodMonth);

            $amountPrice = HeMixed::numberFormat($this->amountPrice);
            $formattedPrice = HeViews::formatAmountInCurrency($amountPrice, $this->currencyTrans);

            return array(
                "MONTH_PERIOD" =>   strval($iPeriodMonth),
                "PAY_DATE_START" => strval($sDateStart . " 00:00:00"),
                "PAY_DATE_END" =>   strval($sDateEnd . " 00:00:00"),
                "AMOUNT_PRICE" =>   $amountPrice,
                "CURRENCY" =>       strval($this->currencyTrans),
                "PRICE_CURR" =>     strval($formattedPrice),
                "TRANSID" =>        strval($this->id),
            );
        }
        catch(Exception $e) { }

        return array(
            "MONTH_PERIOD" =>   "",
            "PAY_DATE_START" => "",
            "PAY_DATE_END" =>   "",
            "AMOUNT_PRICE" =>   "",
            "CURRENCY" =>       "",
            "PRICE_CURR" =>     "",
            "TRANSID" =>        "",
        );
    }

    /**
     * @param $paySuppOrderId
     * @param $merchantReference
     * @param string $status
     *
     * @return bool
     */
    public function getPaymentBySuppOrderIdAndSuppExtUniId(
      $paySuppOrderId,
      $merchantReference,
      $status = ""
    )
    {
        $paramsToSQL = array(":PAYSUPPORDERID" => $paySuppOrderId, ":PAYSUPPEXTUNIID" => $merchantReference);

        $strPayStatus = "";
        if (trim($status) <> "") {
            $strPayStatus = " AND p.`status` = '$status' ";
        }

        $sSql = "
            SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " p
            WHERE
                p.`paySuppOrderId` = :PAYSUPPORDERID
                AND p.`paySuppExtUniId` = :PAYSUPPEXTUNIID
               " . $strPayStatus . "
            ORDER BY p.`dateToPay` DESC
            LIMIT 0, 1
        ";

        $tsSql = $sSql;
        foreach($paramsToSQL as $k => $v) {
            $tsSql = str_replace($k, "'" . $v . "'", $tsSql);
        }

        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return true;
        }

        return false;
    }


    /**
     * @return bool
     * @throws CDbException
     */
    public function updatePendingAmounts( ) {
        $this->beginTrans();

        $sql = "
            UPDATE " . $this->tableName() . "
            SET
                `idPromoCode`=:IDPROMOCODE,
                `amountDiscount`=:AMOUNTDISCOUNT,
                `amountPrice`=:AMOUNTPRICE,
                `amountPriceWithoutTax`=:AMOUNTPRICEWITHOUTTAX,
                `taxRateValue`=:TAXRATEVALUE,
                `amountTax`=:AMOUNTTAX
            WHERE `id` = '" . $this->id . "'
            ;
        ";

        $aParamValues = array(
          ':IDPROMOCODE' => $this->idPromoCode,
          ':AMOUNTDISCOUNT' => $this->amountDiscount,
          ':AMOUNTPRICE' => $this->amountPrice,
          ':AMOUNTPRICEWITHOUTTAX' => $this->amountPriceWithoutTax,
          ':TAXRATEVALUE' => $this->taxRateValue,
          ':AMOUNTTAX' => $this->amountTax
        );

        if ($this->updateSQL($sql, $aParamValues) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @return bool
     * @throws CDbException
     */
    public function updateNewPendingAmounts( ) {
        $this->beginTrans();

        $sql = "
            UPDATE " . $this->tableName() . "
            SET
                `idPromoCode`=:IDPROMOCODE,
                `amountOriginal`=:AMOUNTORIGINAL,
                `amountDiscount`=:AMOUNTDISCOUNT,
                `amountPrice`=:AMOUNTPRICE,
                `amountPriceWithoutTax`=:AMOUNTPRICEWITHOUTTAX,
                `taxRateValue`=:TAXRATEVALUE,
                `amountTax`=:AMOUNTTAX
            WHERE `id` = '" . $this->id . "'
            ;
        ";

        $aParamValues = array(
          ':IDPROMOCODE' => $this->idPromoCode,
          ':AMOUNTORIGINAL' => $this->amountOriginal,
          ':AMOUNTDISCOUNT' => $this->amountDiscount,
          ':AMOUNTPRICE' => $this->amountPrice,
          ':AMOUNTPRICEWITHOUTTAX' => $this->amountPriceWithoutTax,
          ':TAXRATEVALUE' => $this->taxRateValue,
          ':AMOUNTTAX' => $this->amountTax
        );

        if ($this->updateSQL($sql, $aParamValues) > 0) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @param $userId
     * @param $paySuppExtId
     * @param $idCountry
     * @param $idUserCreditForm
     *
     * @return bool
     */
    public function getFirstPaymentByUserIdAndMore($userId, $paySuppExtId, $idCountry, $idUserCreditForm)
    {

        $sSql = "
            SELECT " . $this->mainFields() . ",
                p.amountPriceWithoutTax, p.taxRateValue, p.amountTax
            FROM payments AS p
            JOIN
            (
                SELECT pa.*
                FROM `payments` AS pa
                WHERE
                    pa.`idCountry` = '" . $idCountry . "'
                    AND pa.`paySuppExtId` = '" . $paySuppExtId . "'
                    AND pa.`status` = 0
                    AND pa.userId = '" . $userId . "'
                    AND pa.`idUserCreditForm` = '" . $idUserCreditForm . "'
            ) AS t1
                ON t1.userId = p.userId
                    AND t1.`idUserCreditForm` = p.`idUserCreditForm`
            WHERE
                p.`isRecurring` = 0
                AND p.`status` = 30
            ORDER BY DATE(p.`dateToPay`)
            LIMIT 0, 1
            ;
        ";

        $dataReader = $this->querySQL($sSql);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return true;
        }

        return false;
    }

    /**
     * @param $userId
     * @param $paySuppExtId
     * @param $idCountry
     * @param $idUserCreditForm
     *
     * @return bool
     */
    public function getLastPendingPaymentByUserIdAndMore($userId, $paySuppExtId, $idCountry, $idUserCreditForm)
    {

        $sSql = "
            SELECT " . $this->mainFields() . "
            FROM `payments` AS p
            WHERE
                p.`idCountry` = '" . $idCountry . "'
                AND p.`paySuppExtId` = '" . $paySuppExtId . "'
                AND p.`status` = 0
                AND p.userId = '" . $userId . "'
                AND p.`idUserCreditForm` = '" . $idUserCreditForm . "'
            ORDER BY p.`autoId` DESC
            LIMIT 0, 1
        ";

        $dataReader = $this->querySQL($sSql);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return true;
        }

        return false;
    }

}
