<?php
/**
 * @property string $id
 * @property float $amountDiscount
 * @property float $amountOriginal
 * @property float $amountPrice
 * @property string $cardCvc
 * @property string $cardMonth
 * @property string $cardName
 * @property string $cardNumber
 * @property string $cardYear
 * @property string $currencyTrans
 * @property string $dateEndTransaction
 * @property string $dateStartTransaction
 * @property string $dateToPay
 * @property string $foreignCurrencyTrans
 * @property integer $idCountry
 * @property integer $idPeriodPay
 * @property string $idProduct
 * @property string $idPromoCode
 * @property integer $idUserProdStamp
 * @property integer $kind
 * @property integer $lastAction
 * @property integer $payerPayPalId
 * @property integer $paySuppExtId
 * @property string $paySuppOrderId
 * @property integer $status
 * @property string $tokenPayPal
 * @property integer $userId
 * @property float $xRateToEUR
 * @property float $xRateToUSD
 * @property string $cpfBrasil
 * @property string $typeCpf
 * @property integer $attempts
 * @property integer $idPartner
 * @property integer $experimentVariationAttributeId
 * @property string $idSession
 *
 */
class PaymentControlCheck extends AbaActiveRecord
{
    public $id;
    /**
     * @var AbaUser $parentAbaUser
     */
    private $parentAbaUser;
    private $word4Cards;
    private $firstNameTmp;
    private $secondNameTmp;

    protected $tempCardData = array();

    public function __construct( )
    {
        $this->word4Cards = HeMixed::getWord4Card();
        parent::__construct();
    }

    public function tableName()
    {
        return 'payments_control_check';
    }

    public function mainFields()
    {
        return "  p.`id`,
         p.`idUserProdStamp`, p.`userId`, p.`idProduct`, p.`idCountry`, p.`idPeriodPay`, p.`paySuppExtId`,
         p.`paySuppOrderId`, p.`status`, p.`dateStartTransaction`, p.`dateEndTransaction`, p.`dateToPay`, p.`xRateToEUR`,
         p.`xRateToUSD`, p.`currencyTrans`, p.`foreignCurrencyTrans`, p.`idPromoCode`, p.`amountOriginal`, p.`amountDiscount`,
         p.`amountPrice`, p.`kind`, p.`cardNumber`, p.`cardYear`, p.`cardMonth`, p.`cardCvc`, p.`cardName`, p.`tokenPayPal`, p.`payerPayPalId`,
         p.`lastAction`, p.`attempts`, p.`cpfBrasil`, p.`typeCpf`, p.`idPartner`, p.`experimentVariationAttributeId`,
         p.`idSession`";
    }

    public function rules()
    {
        return array(
            array('idProduct', 'required'),
            array('idCountry', 'required'),
            array('idPeriodPay', 'required'),

            array('kind' , 'required'),

            array('cardName', 'isValidTPVRequired', 'message' => Yii::t('mainApp', 'Debes introducir el nombre del titular de la tarjeta')),
            array('cardNumber', 'isValidCreditNumber'),
            array('cardMonth', 'isValidTPVRequired', 'message' => Yii::t('mainApp', 'Debes seleccionar el mes de caducidad de la tarjeta')),
            array('cardYear', 'isValidTPVRequired', 'message' => Yii::t('mainApp', 'Debes seleccionar el año de caducidad de la tarjeta')),
            array('cardCvc', 'isValidTPVRequired', 'message' => Yii::t('mainApp', 'Debes introducir el código CVC')),

            array('amountPrice','isValidAmount'),
        );
    }


    /**
     * In case of TPV as a method of payment
     * we validate all credit card details as required
     *
     * @param $attribute
     * @param $params
     */
    public function isValidTPVRequired( $attribute, $params )
    {
        if( intval($this->kind) !== KIND_CC_PAYPAL && intval($this->kind) !== KIND_CC_BOLETO && intval($this->kind) !== KIND_CC_NO_CARD )
        {
            if( trim($this->$attribute) == '' ) {
                $this->addError($attribute, Yii::t('mainApp', 'notnull_key'));
            }

            if($attribute == 'CVC'){
                //array('CVC', 	'match',  'pattern'	 =>	 '(^\d{3,4}$)' ,  	'message' => Yii::t('mainApp', 'nocvc_key')),
                if( !preg_match ("/d{3,4}/", $this->$attribute ) ){
                    $this->addError("CVC", Yii::t('mainApp','cvc_notvalid_key') );
                }
            }
        }
        elseif( intval($this->kind) === KIND_CC_NO_CARD ) {
            if($attribute == 'CVC') {
                if( !preg_match ("/d{3,4}/", $this->$attribute ) ) {
                    $this->addError("CVC", Yii::t('mainApp','cvc_notvalid_key') );
                }
            }
        }
    }

    /**
     * @param $attribute
     *
     * @param $params
     */
    public function isValidAmount($attribute, $params)
    {
        if( $this->amountPrice<0 || $this->amountOriginal<0 || $this->amountDiscount<0 ) {
            $this->addError("amountPrice", ' Amount is incorrect ');
        }

        // @todo    Missing cross validation against Prices displayed in the Form by the user.
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function isValidCreditNumber($attribute, $params)
    {
        if( ( intval($this->kind)!==KIND_CC_PAYPAL  && intval($this->kind)!==KIND_CC_BOLETO ) &&
            !HeMixed::isValidCreditCard( $this->cardNumber ) )
        {
            $this->addError("cardNumber", Yii::t('mainApp', 'credit_card_wrong_key') );
        }
    }

    /**
     * SQL FUNCTIONS ---------------------------------------------------------------------------
      */
    /**
     * @param bool $id
     *
     * @return bool|PaymentControlCheck
     */
    public function getPaymentById( $id=false )
    {
        $sql = "SELECT ".$this->mainFields().
            " FROM ".$this->tableName()." p WHERE p.`id`= :ID";

        $paramsToSQL = array(":ID" => $id);
        $dataReader = $this->querySQL( $sql, $paramsToSQL);
        return $this->readRowFillDataColsThis( $dataReader );
    }

    /**
     * @param bool|false $id
     *
     * @return $this|bool
     */
    public function getPaymentControlCheckById($id = false)
    {
        $sSql = "
            SELECT " . $this->mainFields() . "
            FROM " . $this->tableName() . " p
            WHERE p.`id`= :ID
        ";
        $paramsToSQL = array(":ID" => $id);
        $dataReader = $this->querySQL($sSql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param        $status
     * @param        $paySuppExtId
     * @param        $paySuppOrderId
     * @param string $lastAction
     * @param string $tokenPayPal
     * @param string $payerPayPalId
     *
     * @return bool
     */
    public function updateStatusAfterPayment( $status, $paySuppExtId, $paySuppOrderId, $lastAction="", $tokenPayPal="", $payerPayPalId="" )
    {
        $this->beginTrans();
        //----------------
        $sqlDateEnd = "";

        if($status==PAY_SUCCESS){  $sqlDateEnd=" p.`dateEndTransaction` = NOW(), p.`reviewed`=1, "; }

        $sql= " UPDATE ".$this->tableName()." p SET p.`paySuppExtId`= :PAYSUPPEXTID ,
                p.`paySuppOrderId`= :PAYSUPPORDERID , p.`status`= ".$status.",
                ".$sqlDateEnd." p.`lastAction`= CONCAT(p.`lastAction`,' \>\>\>\> ',:LASTACTION),
                p.`tokenPayPal`= :TOKENPAYPAL, p.`payerPayPalId`=:PAYERPAYPALID,
                p.dateToPay = DATE('".$this->dateToPay."')
            WHERE p.`id`='".$this->id."' ";

        $aParamValues = array( ":PAYSUPPEXTID"=>$paySuppExtId, ":LASTACTION" => $lastAction, ':PAYSUPPORDERID' => $paySuppOrderId,
                                ":TOKENPAYPAL" => $tokenPayPal, ':PAYERPAYPALID'=> $payerPayPalId);
        if ( $this->updateSQL($sql,$aParamValues) > 0 ){
            $this->commitTrans();    return true;    }
        else{
            $this->rollbackTrans(); return false;        }
    }


    /** Updates all previous attempts of purchase. Up to 7 days.
     * @param integer $userId
     *
     * @return bool
     */
    public function updateReviewedLastWeekAttempts( $userId )
    {
        if( !$userId ){
            return false;
        }

        $this->beginTrans();
        //----------------
        $sqlDateEnd = "";

        $sql= " UPDATE ".$this->tableName()." p SET p.`reviewed`= 1
                WHERE p.`userId`=".$this->userId." AND p.`dateStartTransaction`>DATE_SUB( NOW(), INTERVAL 7 DAY)  ";

        if ( $this->updateSQL( $sql ) > 0 )
        {
            $this->commitTrans();
            return true;
        }
        else
        {
            $this->rollbackTrans();
            return false;
        }

        return true;
    }

    /**
     * @param string $ControlCheckId
     *
     * @return bool
     */
    public function insertPayment( $ControlCheckId="" )
    {
        $this->beginTrans();
        $successTrans = true;
        //-----------------
        $sql = "  INSERT INTO ".$this->tableName()."
        (`id`, `idUserProdStamp`, `userId`, `idProduct`, `idCountry`, `idPeriodPay`,
        `paySuppExtId`, `paySuppOrderId`, `status`, `dateStartTransaction`,
        `dateEndTransaction`,`dateToPay`, `xRateToEUR`, `xRateToUSD`, `currencyTrans`,
        `foreignCurrencyTrans`, `idPromoCode`, `amountOriginal`,  `amountDiscount`, `amountPrice`,
        `kind`, `cardNumber`, `cardYear`, `cardMonth`, `cardCvc`, `cardName`, `tokenPayPal`, `payerPayPalId`,
        `lastAction`, `cpfBrasil`, `typeCpf`, `idPartner`, `experimentVariationAttributeId`, `idSession`)

         VALUES('".$ControlCheckId."', :IDUSERPRODSTAMP , :USERID , :IDPRODUCT , :IDCOUNTRY , :IDPERIODPAY ,
            :PAYSUPPEXTID , :PAYSUPPORDER , :STATUS , :DATESTARTTRANSACTION ,
            :DATEENDTRANSACTION ,:DATETOPAY , :XRATETOEUR , :XRATETOUSD , :CURRENCYTRANS ,
            :FOREIGNCURRENCYTRANS ,:IDPROMOCODE , :AMOUNTORIGINAL , :AMOUNTDISCOUNT , :AMOUNTPRICE,
             :KIND,  AES_ENCRYPT(:CARDNUMBER,'".$this->word4Cards."'),
                    AES_ENCRYPT(:CARDYEAR,'".$this->word4Cards."'),
                    AES_ENCRYPT(:CARDMONTH,'".$this->word4Cards."'),
                    AES_ENCRYPT(:CARDCVC,'".$this->word4Cards."'), :CARDNAME, :TOKENPAYPAL, :PAYERPAYPALID,
         :LASTACTION, :CPFBRASIL, :TYPECPF, :IDPARTNER, :experimentVariationAttributeId, :idSession ) ";

        $sql.=" ON DUPLICATE KEY UPDATE
             `idUserProdStamp` = :IDUSERPRODSTAMP, `userId` = :USERID,
             `idProduct` = :IDPRODUCT, `idCountry` = :IDCOUNTRY,
             `idPeriodPay` = :IDPERIODPAY, `paySuppExtId`= :PAYSUPPEXTID ,
             `paySuppOrderId` = :PAYSUPPORDER, `status`= :STATUS,
             `dateStartTransaction` = :DATESTARTTRANSACTION, `dateEndTransaction` = :DATEENDTRANSACTION,
             `dateToPay` = :DATETOPAY, `xRateToEUR` = :XRATETOEUR,
             `xRateToUSD` = :XRATETOUSD, `currencyTrans` = :CURRENCYTRANS,
             `foreignCurrencyTrans` = :FOREIGNCURRENCYTRANS, `idPromoCode` = :IDPROMOCODE,
             `amountOriginal` = :AMOUNTORIGINAL,  `amountDiscount` = :AMOUNTDISCOUNT,
             `amountPrice` = :AMOUNTPRICE,
             `kind` = :KIND, `cardNumber` = AES_ENCRYPT(:CARDNUMBER,'".$this->word4Cards."'),
             `cardYear` = AES_ENCRYPT(:CARDYEAR,'".$this->word4Cards."'), `cardMonth` = AES_ENCRYPT(:CARDMONTH,'".$this->word4Cards."'),
             `cardCvc` = AES_ENCRYPT(:CARDCVC,'".$this->word4Cards."'), `cardName` = :CARDNAME,
             `tokenPayPal` = :TOKENPAYPAL,
             `payerPayPalId` = :PAYERPAYPALID,
             `lastAction`= CONCAT(`lastAction`,' \>\>\>\> ',:LASTACTION),
             `attempts` = `attempts`+1,
             `cpfBrasil` = :CPFBRASIL,
             `typeCpf` = :TYPECPF,
             `idPartner` = :IDPARTNER,
             `experimentVariationAttributeId` = :experimentVariationAttributeId,
             `idSession` = :idSession
        ; ";


      $aParamValues = array(':IDUSERPRODSTAMP'=> $this->idUserProdStamp,  ':USERID'=> $this->userId,
            ':IDPRODUCT'=> $this->idProduct, ':IDCOUNTRY'=> $this->idCountry,  ':IDPERIODPAY'=> $this->idPeriodPay,
            ':PAYSUPPEXTID'=> $this->paySuppExtId,  ':PAYSUPPORDER'=> $this->paySuppOrderId,  ':STATUS'=> $this->status,
            ':DATESTARTTRANSACTION'=> $this->dateStartTransaction, ':DATEENDTRANSACTION'=> $this->dateEndTransaction,
            ':DATETOPAY'=> $this->dateToPay,  ':XRATETOEUR'=> $this->xRateToEUR,  ':XRATETOUSD'=> $this->xRateToUSD,
            ':CURRENCYTRANS'=> $this->currencyTrans, ':FOREIGNCURRENCYTRANS'=> $this->foreignCurrencyTrans,
            ':IDPROMOCODE'=> $this->idPromoCode,  ':AMOUNTORIGINAL'=> $this->amountOriginal,
            ':AMOUNTDISCOUNT'=> $this->amountDiscount,':AMOUNTPRICE'=> $this->amountPrice,
            ':KIND'=>$this->kind, ':CARDNUMBER'=>$this->cardNumber, ':CARDYEAR'=> $this->cardYear,
            ':CARDMONTH' => $this->cardMonth, ':CARDCVC'=> $this->cardCvc, ':CARDNAME'=> $this->cardName,
            ':TOKENPAYPAL'=> $this->tokenPayPal, ':PAYERPAYPALID'=> $this->payerPayPalId ,
            ':LASTACTION' => $this->lastAction, ':CPFBRASIL' => $this->cpfBrasil, ':TYPECPF' => $this->typeCpf,
            ':IDPARTNER' => $this->idPartner, ':experimentVariationAttributeId' =>$this->experimentVariationAttributeId,
            ':idSession' => $this->idSession,
      );

//      $this->id = $this->executeSQL($sql, $aParamValues);
        if( $this->executeSQL($sql, $aParamValues) > 0 )
        {
            $user = new AbaUser();
            if($user->getUserById( $this->userId ))
            {
                if(!$user->abaUserLogUserActivity->saveUserLogActivity($this->tableName(),
                            "New payment process INITIATED ".$this->id, "Payment"))
                {
                    $successTrans= false;
                }
            }
        }
        else
        {
            $successTrans = false;
        }
        //---------------
        if( $successTrans )
        {
            $this->commitTrans();
            return true;
        }
        else
        {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * --------------------------------------------------------------------- END SQL FUNCTIONS
      */
    /**
     * @param string $email
     *
     * @return AbaUser|bool
     */
    public function findParentAbaUser($email="")
    {
        if($email=="")
        {
            $email = Yii::app()->user->getEmail();
        }

        $parentAbaUser = new AbaUser();
        if($parentAbaUser->getUserByEmail($email, "", false))
        {
            $this->parentAbaUser = $parentAbaUser;
            return $this->parentAbaUser;
        }
        return false;
    }

    /**
     * @param integer $userId
     * @param integer $idUserProdStamp
     * @param string $idProduct
     * @param integer $idCountry
     * @param integer $idPeriodPay
     * @param string $idPromoCode
     * @param integer $kind
     * @param string $cardName
     * @param integer $cardNumber
     * @param integer $cardYear
     * @param string $cardMonth
     * @param integer $cardCvc
     * @param integer $paySuppExtId
     * @param Payment $payment Original payment, the PENDING one
     * @param string $cpfBrasil
     * @param string $typeCpf
     *
     * @return bool
     */
    public function createPayControlCheckFromPayment( $userId, $idUserProdStamp, $idProduct, $idCountry, $idPeriodPay,
                                                $idPromoCode, $kind, $cardName, $cardNumber,$cardYear, $cardMonth,
                                                $cardCvc, $paySuppExtId, Payment $payment, $cpfBrasil='', $typeCpf='', $validate=true, $paySuppExtProfId='')
    {
        $aComputedValues = array();
        $aComputedValues['id'] =            $payment->id;
        // FK to [users]
        $aComputedValues['userId'] =        $userId ;
        // FK to [products_prices]
        $aComputedValues['idProduct'] =     $idProduct ;
        $aComputedValues['idCountry'] =     $idCountry ;
        $aComputedValues['idPeriodPay'] =   $idPeriodPay ;

        // Get the reference to the product purchase, [user_product_stamp]:
        $aComputedValues['idUserProdStamp'] = $idUserProdStamp ;

        // Get all data for the current selected payment:
        $aComputedValues['idPromoCode'] =           $idPromoCode ;
        $aComputedValues['currencyTrans'] =         $payment->currencyTrans ;
        $aComputedValues['foreignCurrencyTrans'] =  $payment->foreignCurrencyTrans ;
        // WE ASSUME THAT THE PENDING -PAYMENT WHICH IS BASED THIS VALUES FROM- HAVE THE CORRECT VALUES ALREADY,
        // SO WE DO NOT TAKE THE ORIGINAL PRICE. WE DID THAT ALREADY IN THE CREATION OF THE PENDING LINE:
        $aComputedValues['amountOriginal'] =        $payment->amountOriginal ;
        $aComputedValues['amountDiscount'] =        $payment->amountDiscount;
        $aComputedValues['amountPrice'] =           $payment->amountPrice ;

        // Get currencies involved in order to stamp them in the transaction
        $xRates = new AbaCurrency( $aComputedValues['foreignCurrencyTrans'] );
        $aComputedValues['xRateToEUR'] = $xRates->getConversionRateTo(CCY_DEFAULT) ;
        $aComputedValues['xRateToUSD'] = $xRates->getConversionRateTo("USD") ;

        // Gateway and register Transaction relevant data:
        $aComputedValues['paySuppExtId'] =          $paySuppExtId ;
        $aComputedValues['paySuppOrderId'] =        $payment->paySuppOrderId ;
        $aComputedValues['status'] =                $payment->status ;
        $aComputedValues['dateStartTransaction'] =  date("Y-m-d H:i:s");
        $aComputedValues['dateEndTransaction'] =    "" ;
        $aComputedValues['dateToPay'] =             $payment->dateToPay ;
        // User credit details
        $aComputedValues["kind"] =          $kind;
        $aComputedValues["cardNumber"] =    $cardNumber;
        $aComputedValues["cardYear"] =      $cardYear;
        $aComputedValues["cardMonth"] =     substr("00".$cardMonth, -2);
        $aComputedValues["cardCvc"] =       $cardCvc;
        $aComputedValues["cardName"] =      $cardName;
        $aComputedValues["cpfBrasil"] =     $cpfBrasil;
        $aComputedValues["typeCpf"] =       $typeCpf;

        //
        //#5739
        $aComputedValues["experimentVariationAttributeId"] = null;
        $aComputedValues['idSession'] =     $payment->idSession ;

        // PayPal control values
        $aComputedValues["tokenPayPal"] =       "";
        $aComputedValues["payerPayPalId"] =     "";
        $aComputedValues["lastAction"] =        "[".date("Y-m-d H:i:s")."] copying payment in order to create Control Check ";
        $aComputedValues["paySuppExtProfId"] =  $paySuppExtProfId;

        //
        //#4681
        //
        $tempCardData = array(
            "cardNumber" => $aComputedValues["cardNumber"],
            "cardYear" =>   $aComputedValues["cardYear"],
            "cardMonth" =>  $aComputedValues["cardMonth"],
            "cardCvc" =>    $aComputedValues["cardCvc"],
            "cardName" =>   $aComputedValues["cardName"],
        );
        $this->setTempCardData($tempCardData);

        return $this->setPayment( $aComputedValues, $validate );
    }

    /**
     * @return array
     */
    public function getTempCardData() {
        return $this->tempCardData;
    }

    /**
     * @param array $tempCardData
     */
    public function setTempCardData($tempCardData=array()) {
        foreach($tempCardData as $iKey => $stValue) {
            $this->tempCardData[$iKey] = $stValue;
        }
    }

    /**
     * @param $userId
     * @param $idUserProdStamp
     * @param $idProduct
     * @param $idCountry
     * @param $idPeriodPay
     * @param $idPromoCode
     * @param $kind
     * @param $cardName
     * @param $cardNumber
     * @param $cardYear
     * @param $cardMonth
     * @param $cardCvc
     * @param $paySuppExtId
     * @param string $cpfBrasil
     * @param string $typeCpf
     * @param int $idPartner
     *
     * @return bool
     */
    public function copyPaymentFormToControlCheck( $userId, $idUserProdStamp, $idProduct, $idCountry, $idPeriodPay,
                                                $idPromoCode, $kind, $cardName, $cardNumber,$cardYear, $cardMonth,
                                                $cardCvc, $paySuppExtId, $cpfBrasil='', $typeCpf='', $idPartner=300001,
                                                $validate=true, $bPlanPrice=false, $iProductPrice=null, $experimentVariationAttributeId=null,
                                                $idSession=null, $sCurrency=""
    )
    {
        $aComputedValues = array();
        // FK to [users]
        $aComputedValues['userId'] =        $userId;
        // FK to [products_prices]
        $aComputedValues['idProduct'] =     $idProduct;
        $aComputedValues['idCountry'] =     $idCountry;
        $aComputedValues['idPeriodPay'] =   $idPeriodPay;

        // Get the reference to the product purchase, [user_product_stamp]:
        $aComputedValues['idUserProdStamp'] = $idUserProdStamp;

        // Get all data for the current selected package:
        $package = new ProductPrice();

        //
        //#5739
        if($package->getProductByMixedId($idProduct, $idCountry, $idPeriodPay, $idPromoCode, null,
          $iProductPrice, $sCurrency)) {

            $aComputedValues['idPromoCode'] =           $package->idPromoCode;
            $aComputedValues['currencyTrans'] =         $package->ABAIdCurrency;
            $aComputedValues['foreignCurrencyTrans'] =  $package->officialIdCurrency;
            $aComputedValues['amountOriginal'] =        $package->getOriginalPriceTransaction();
            $aComputedValues['amountDiscount'] =        $package->getAmountDiscountedTransaction();
            $aComputedValues['amountPrice'] =           $package->getFinalPriceTransaction();

            //
            //#5087
            if($bPlanPrice) {

                $prodSelected = new ProductPrice();
                if($prodSelected->isPlanProduct($idProduct)) {

                    $abaUser = new AbaUser();
                    $abaUser->getUserById($userId);

                    $iDiscount = $prodSelected->getDiscountPlan($abaUser, $idProduct);

                    $productPrice = array(
                        "originalPrice" =>      $aComputedValues['amountOriginal'],
                        "amountDiscounted" =>   $aComputedValues['amountDiscount'],
                        "finalPrice" =>         $aComputedValues['amountPrice'],
                    );

                    $productPriceSelected = $prodSelected->setPaymentDiscount($iDiscount, $productPrice);

                    $aComputedValues['amountDiscount'] =        $productPriceSelected["amountDiscounted"];
                    $aComputedValues['amountPrice'] =           $productPriceSelected["finalPrice"];
                }
            }
        }
        else {
            return false;
        }

        // Get currencies involved in order to stamp them in the transaction
        $xRates = new AbaCurrency( $aComputedValues['foreignCurrencyTrans'] );

        $aComputedValues['xRateToEUR'] =            $xRates->getConversionRateTo(CCY_DEFAULT) ;
        $aComputedValues['xRateToUSD'] =            $xRates->getConversionRateTo("USD") ;

        // Gateway and register Transaction relevant data:
        $aComputedValues['paySuppExtId'] =          $paySuppExtId;
        $aComputedValues['paySuppOrderId'] =        "";
        $aComputedValues['status'] =                PAY_INITIATED;
        $aComputedValues['dateStartTransaction'] =  date("Y-m-d H:i:s");
//        $aComputedValues['dateEndTransaction'] =    "";
        $aComputedValues['dateEndTransaction'] =    null;
        $aComputedValues['dateToPay'] =             date("Y-m-d H:i:s");

        //
        //#4681
        //
        $tempCardData = array(
            "cardNumber" => $cardNumber,
            "cardYear" =>   $cardYear,
            "cardMonth" =>  substr("00" . $cardMonth, -2),
            "cardCvc" =>    $cardCvc,
            "cardName" =>   $cardName,
        );
        $this->setTempCardData($tempCardData);

        //
        // User credit details
        //
        $aComputedValues["kind"] =          $kind;
        $aComputedValues["cardNumber"] =    "";
        $aComputedValues["cardYear"] =      "";
        $aComputedValues["cardMonth"] =     "";
        $aComputedValues["cardCvc"] =       "";
        $aComputedValues["cardName"] =      "";

        $aComputedValues["cpfBrasil"] = $cpfBrasil;
        $aComputedValues["typeCpf"] =   $typeCpf;
        $aComputedValues["idPartner"] = $idPartner;

        //
        //#5739
        $aComputedValues["experimentVariationAttributeId"] = $experimentVariationAttributeId;
        $aComputedValues["idSession"] = $idSession;

        // PayPal control values
        $aComputedValues["tokenPayPal"] =   "";
        $aComputedValues["payerPayPalId"] = "";
        $aComputedValues["lastAction"] =    "[".date("Y-m-d H:i:s")."] Initiating payment, method ".$paySuppExtId;

        return $this->setPayment( $aComputedValues, $validate );
    }

    /**
     * @param $aComputedValues
     *
     * @return bool
     */
    public function setPayment( $aComputedValues, $validate=true )
    {
        foreach( $aComputedValues as $fieldName=>$value ) {
            $this->$fieldName = $value;
        }

        if($validate) {
            if( !$this->validate() ) {
                return false;
            }
        }

        return true;
    }

    public function setFirstNameTmp($firstNameTmp)
    {
        $this->firstNameTmp = $firstNameTmp;
    }

    public function getFirstNameTmp()
    {
        return $this->firstNameTmp;
    }

    public function setSecondNameTmp($secondNameTmp)
    {
        $this->secondNameTmp = $secondNameTmp;
    }

    public function getSecondNameTmp()
    {
        return $this->secondNameTmp;
    }

    /**
     * @return bool|float
     */
    public function getAmountPriceInForeignCcy()
    {
        $moCurrentCcy = new AbaCurrency($this->currencyTrans);
        $amountConverted = $moCurrentCcy->getConversionAmountTo($this->amountPrice, $this->foreignCurrencyTrans);

        if (is_numeric($amountConverted)) {
            return $amountConverted;
        }

        return false;
    }

    /**
     * @param $cardNumber
     */
    public function detectUnknownCreditCardType($cardNumber) {
        if($this->paySuppExtId == PAY_SUPPLIER_ADYEN OR $this->paySuppExtId == PAY_SUPPLIER_ADYEN_HPP OR $this->paySuppExtId == PAY_SUPPLIER_ALLPAGO_BR OR $this->paySuppExtId == PAY_SUPPLIER_ALLPAGO_MX OR $this->paySuppExtId == PAY_SUPPLIER_CAIXA) {
            $this->kind = HePayments::getCreditCardType($cardNumber);
        }
    }

}
