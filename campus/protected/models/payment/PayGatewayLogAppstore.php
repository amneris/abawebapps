<?php
/**
 * @property integer id
 * @property integer idPaymentIos
 * @property integer userId
 * @property string operation
 * @property string dateRequest
 * @property string dateResponse
 * @property string jsonRequest
 * @property string jsonResponse
 * @property string errorCode
 * @property integer success
 * @property integer idPaySupplier
 *
 */
class PayGatewayLogAppstore extends PayGatewayLog
{
    /**
     * @param $idKey
     *
     * @return $this|bool
     */
    public function getLogById($idKey)
    {
        $sSql =          " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " l  WHERE l.`id`=" . $idKey . "; ";
        $dataReader =   $this->querySQL($sSql);
        return $this->readRowFillDataColsThis($dataReader);
    }

    /**
     * @param $userId
     *
     * @return $this|bool
     */
    public function getLastLogByUserId($userId)
    {
        $sSql =          " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " l  WHERE l.`userId`=" . $userId . " ORDER BY l.`id` DESC LIMIT 1 ; ";
        $dataReader =   $this->querySQL($sSql);
        return $this->readRowFillDataColsThis($dataReader);
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " `id`, `idPaymentIos`, `userId`, `operation`, `dateRequest`, `dateResponse`, `jsonRequest`, `jsonResponse`, `errorCode`, `success`, `idPaySupplier` ";
    }

    /**
     * @return string
     */
    public function tableName()
    {
//        return Yii::app()->params['dbCampusLogs'].".log_appstore";
        return "pay_gateway_appstore";
    }

    /** Returns the log for a former payment
     *
     * @param string $idPayment
     * @return $this|bool
     */
    public function getByIdPayment($idPayment)
    {
        $sSql =          " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " l  WHERE l.`idPaymentIos`= :IDPAYMENT  ORDER BY l.`id` DESC LIMIT 1 ; ";
        $dataReader =   $this->querySQL($sSql, array(':IDPAYMENT' => $idPayment));
        return $this->readRowFillDataColsThis($dataReader);
    }

    /** Inserts a record in the table
     * @return bool|int
     */
    public function insertLog()
    {
        $this->beginTrans();
        $successTrans = true;
        //-----------------
        $sSql= " INSERT INTO " . $this->tableName() . " (`idPaymentIos`, `userId`, `operation`, `dateRequest`, `dateResponse`, `jsonRequest`, `jsonResponse`, `errorCode`, `success`, `idPaySupplier` )
                VALUES ( :IDPAYMENT, :USERID, :OPERATION, :DATEREQUEST, :DATERESPONSE, :JSONREQUEST, :JSONRESPONSE, :ERRORCODE, :SUCCESS, :IDPAYSUPPLIER ) ";

        $aParamValues = array(
            ':IDPAYMENT'=>$this->idPaymentIos, ':USERID'=>$this->userId, ':OPERATION'=>$this->operation, ':DATEREQUEST'=>$this->dateRequest, ':DATERESPONSE'=>$this->dateResponse,
            ':JSONREQUEST'=>$this->jsonRequest, ':JSONRESPONSE'=>$this->jsonResponse, ':ERRORCODE'=>$this->errorCode, ':SUCCESS'=>$this->success, ':IDPAYSUPPLIER'=>$this->idPaySupplier
        );

        $this->id = $this->executeSQL($sSql, $aParamValues);
        if ($this->id <= 0) {
            $successTrans = false;
        }

        //---------------
        if ($successTrans) {
            $this->commitTrans();return $this->id;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @return mixed|null
     */
    public function getUniqueId() {
        return null;
    }

    /** @TODO
     *
     * @return mixed|string
     */
    public function getPaySuppExtProfId() {
        return "";
    }

}
