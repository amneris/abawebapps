<?php
/**
 * Class PayGatewayLogAdyen
 */
/*
    @property integer id
    @property string idPayment
 */

class PayGatewayLogAdyen extends PayGatewayLog
{
    /**
     * @return string
     */
    public function tableName() {
        return "pay_gateway_adyen";
    }

    /**
     * @return bool
     */
    public function insertRequestSent() {

        $this->beginTrans();

        $successTrans = true;

        $sSql= " INSERT INTO " . $this->tableName() . " (
                    `idPayment`, `dateSent`, `merchantAccount`, `amountValue`,
                    `amountCurrency`, `reference`, `originalReference`, `shopperIP`, `shopperEmail`,
                    `shopperReference`, `fraudOffset`, `additionalData`
                ) ".
            " VALUES (
                    :IDPAYMENT, :DATESENT, :MERCHANTACCOUNT, :AMOUNTVALUE,
                    :AMOUNTCURRENCY, :REFERENCE, :ORIGINALREFERENCE, :SHOPPERIP, :SHOPPEREMAIL,
                    :SHOPPERREFERENCE, :FRAUDOFFSET, :ADDITIONALDATA
                )";

        $aParamValues= array(
            ":IDPAYMENT" =>         $this->idPayment,
            ":DATESENT" =>          $this->dateSent,
            ":MERCHANTACCOUNT" =>   $this->merchantAccount,
            ":AMOUNTVALUE" =>       $this->amountValue,
            ":AMOUNTCURRENCY" =>    $this->amountCurrency,
            ":REFERENCE" =>         $this->reference,
            ":ORIGINALREFERENCE" => $this->originalReference,
            ":SHOPPERIP" =>         $this->shopperIP,
            ":SHOPPEREMAIL" =>      $this->shopperEmail,
            ":SHOPPERREFERENCE" =>  $this->shopperReference,
            ":FRAUDOFFSET" =>       $this->fraudOffset,
            ":ADDITIONALDATA" =>    $this->additionalData,
        );

        $this->id = $this->executeSQL($sSql, $aParamValues);
        if( $this->id <= 0 ) {
            $successTrans = false;
        }
        //---------------
        if( $successTrans ) {
            $this->commitTrans();
            return true;
        }
        else {
            $this->rollbackTrans();
            return false;
        }
    }

    /** Returns the unique id of the gateway supplier platform. Usually "their transaction identification"
     * Each platform returns its value in several ways.
     * @return mixed
     */
    public function getUniqueId() {
        return $this->reference;
    }

    public function getCsvResponse() {
    }

    /** @TODO
     *
     * @return mixed|string
     */
    public function getPaySuppExtProfId() {
        return "";
    }

}
