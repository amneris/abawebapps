<?php
/**
 * Created by PhpStorm.
 * User: akorotkov
 * Date: 06/07/15
 * Time: 12:12
 */
class ProductsPromosDescription extends CmAbaProductsPromosDescription {

    /**
     * Constructor, null implementation
     */
    public function __construct()
    {
        parent::__construct();
        $this->connection = Yii::app()->getComponent(AbaActiveRecord::DB_DEFAULT);
    }

}