<?php
/**
 * Created by JetBrains PhpStorm.
 * User: abaenglish
 * Date: 29/10/12
 * Time: 17:06
 * To change this template use File | Settings | File Templates.
 */
class PayGatewayLogLaCaixaReqSent extends PayGatewayLog
{

    public function tableName()
    {
        return "pay_gateway_sent";
    }

    public function insertRequestSent()
    {
        $this->beginTrans();
        $successTrans = true;
        $this->word4Cards = HeMixed::getWord4Card();
        //-----------------
        /*
        SELECT pgs.`id`, pgs.`idPayment`, pgs.`fecha`, pgs.`Order_number`, pgs.`Currency`,
        pgs.`TransactionType`, pgs.`MerchantData`, pgs.`Amount`, pgs.`MerchantName`,
         pgs.`MerchantSignature`, pgs.`MerchantCode`, pgs.`CreditTarget`, pgs.`ExpiryDate`,
         pgs.`Cvv2`, pgs.`XML` FROM pay_gateway_sent pgs;
         * */
        $sql= " INSERT INTO ".$this->tableName()." (  `idPayment`,  `fecha`,  `Order_number`,  `Currency`,
                                `TransactionType`,  `MerchantData`,  `Amount`,  `MerchantName`,
                                `MerchantSignature`,  `MerchantCode`,  `CreditTarget`,  `ExpiryDate`, `Cvv2`, `MerchantIdentifier`,
                                 `XML` ) ".
            " VALUES(    :IDPAYMENT ,   :FECHA ,   :ORDER_NUMBER ,   :CURRENCY ,
                              :TRANSACTIONTYPE ,   :MERCHANTDATA ,   :AMOUNT ,   :MERCHANTNAME ,
                              :MERCHANTSIGNATURE ,   :MERCHANTCODE ,
                              AES_ENCRYPT(:CREDITTARGET,'" . $this->word4Cards . "'),
                              AES_ENCRYPT(:EXPIRYDATE,'" . $this->word4Cards . "'),
                              AES_ENCRYPT(:CVV2,'" . $this->word4Cards . "'),
                              :MERCHANTIDENTIFIER,
                              :XML  )";

        $aParamValues = array(
            ":IDPAYMENT" =>             $this->idPayment,
            ":FECHA" =>                 $this->fecha,
            ":ORDER_NUMBER" =>          $this->Order_number,
            ":CURRENCY" =>              $this->Currency,
            ":TRANSACTIONTYPE" =>       $this->TransactionType,
            ":MERCHANTDATA" =>          $this->MerchantData ,
            ":AMOUNT" =>                $this->Amount,
            ":MERCHANTNAME" =>          $this->MerchantName ,
            ":MERCHANTSIGNATURE" =>     $this->MerchantSignature,
            ":MERCHANTCODE" =>          $this->MerchantCode,
//            ":CREDITTARGET" =>          $this->CreditTarget,
//            ":EXPIRYDATE" =>            $this->ExpiryDate ,
//            ":CVV2" =>                  $this->Cvv2,
            ":CREDITTARGET" =>          "",
            ":EXPIRYDATE" =>            "",
            ":CVV2" =>                  "",
            ":MERCHANTIDENTIFIER" =>    $this->MerchantIdentifier,
            ":XML" =>                   $this->XML
        );
        $this->id = $this->executeSQL($sql, $aParamValues);
        if( $this->id <= 0 )
        {
            $successTrans = false;
        }

        //---------------
        if( $successTrans )
        {
            $this->commitTrans();
            return true;
        }
        else
        {
            $this->rollbackTrans();
            return false;
        }
    }

    /** Returns the unique id of the gateway supplier platform. Usually "their transaction identification"
     * Each platform returns its value in several ways.
     * @return mixed
     */
    public function getUniqueId()
    {
        return "";
    }
}
