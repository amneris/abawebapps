<?php

/**
 * This is the model class for table "country_paymethod_paysupplier".
 *
 * The followings are the available columns in table 'country_paymethod_paysupplier':
 * @property integer $idCountry
 * @property integer $idPayMethod
 * @property integer $idPaySupplier
 */
class CountryPaymethodPaysupplier extends AbaActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return Yii::app()->params['dbCampus'].'.country_paymethod_paysupplier';
	}

    /** Returns all fields.
     *
     * @return string
     */
    public function mainFields()
    {
        return ' `idCountry`, `idPayMethod`, `idPaySupplier` ';
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCountry, idPayMethod, idPaySupplier', 'required'),
			array('idCountry', 'length', 'max'=>4),
			array('idPayMethod, idPaySupplier', 'length', 'max'=>3),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCountry' => 'FK to Country',
			'idPayMethod' => 'FK to pay_methods',
			'idPaySupplier' => 'FK to pay_suppliers',
		);
	}


    /** Returns for a method of payment and a certain country the assigned
     * Pay Supplier. Useful to know the PaySupplierCommon inherited.
     *
     * @param $idCountry
     * @param $idPayMethod
     * @return $this|bool
     */
    public function getPaySupplierByMethodAndCountry( $idCountry, $idPayMethod)
    {
        $sql = " SELECT  " . $this->mainFields() . " FROM " . $this->tableName() .
            " cpp WHERE cpp.idCountry=:IDCOUNTRY  AND cpp.idPayMethod=:IDPAYMETHOD";
        $paramsToSQL = array( ':IDCOUNTRY' => $idCountry, ':IDPAYMETHOD'=>$idPayMethod );

        $dataReader = $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }

        return false;
    }

    /** Returns for a method of payment all Suppliers
     *
     * @param $idPayMethod
     * @return array|bool
     */
    public function getPaySuppliersByMethod( $idPayMethod )
    {
        $sql = ' SELECT  DISTINCT cpp.`idPaySupplier` as `idPaySupplier`
                FROM '.$this->tableName() . ' cpp WHERE cpp.idPayMethod=:IDPAYMETHOD ';

        $paramsToSQL = array( ':IDPAYMETHOD'=>$idPayMethod );
        $dataRows = $this->queryAllSQL($sql, $paramsToSQL);
        if(!$dataRows){
            return false;
        } else{
            foreach($dataRows as $row){
             $aRows[]=$row['idPaySupplier'];
            }

            return $aRows;
        }
    }


    /** Returns for a certain country the assigned Pay Methods available.
     *
     * @param integer $idCountry

     * @return array|bool
     */
    public function getPayMethodsByCountryId( $idCountry )
    {
        $sql = ' SELECT  ' . $this->mainFields() . ' FROM ' . $this->tableName() .
            ' cpp WHERE cpp.idCountry=:IDCOUNTRY ';
        $paramsToSQL = array( ':IDCOUNTRY' => $idCountry );
        $dataRows = $this->queryAllSQL($sql, $paramsToSQL);
        if(!$dataRows){
            return false;
        } else{
            foreach($dataRows as $row){
                $aRows[]=$row['idPayMethod'];
            }

            return $aRows;
        }
    }
}
