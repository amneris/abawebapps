<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Quino
 * Date: 10/06/2013
 * Time: 16:04
 * To change this template use File | Settings | File Templates.
 */
/**
 * @property integer id
 * @property string dateRequest
 * @property string errorCode
 * @property string jsonResponse
 * @property string operation
 * @property integer success
 * @property integer userId
 * @property string xmlRequest
 * @property string idPayment
 */
class PayGatewayLogPayPal extends PayGatewayLog
{
    const SETEXPRESSCHECKOUT = 'SetExpressCheckout';
    const GETEXPRESSCHECKOUT = 'GetExpressCheckoutDetails';
    const CREATERECURRINGPAYMENT = 'CreateRecurringPaymentsProfile';
    const DOEXPRESSCHECKOUTPAYMENT = 'DoExpressCheckoutPayment';

    /* @var $aFullResponse string */
    private $paySuppOrderId;

    /* @var $aFullResponse Array */
    public $aFullResponse;

    public function tableName()
    {
//        return Yii::app()->params['dbCampusLogs'].".log_paypal_express_co";
        return "pay_gateway_paypal_express_co";
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " l.`id`, l.`userId`, l.`operation`, l.`dateRequest`, l.`xmlRequest`,
                l.`jsonResponse`, l.`errorCode`, l.`success`, l.`idPayment` ";
    }

    /** Returns all fields from log PayPal by Id
     * @param $id
     *
     * @return bool|LogUserLevelChange
     */
    public function getLogById($id)
    {
        $sql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " l  WHERE l.`id`=" . $id . "; ";

        $dataReader = $this->querySQL($sql);
        return $this->readRowFillDataColsThis($dataReader);
    }

    /** Inserts a record in the table
     * @return bool|int
     */
    public function insertLog()
    {
        $this->beginTrans();
        $successTrans = true;
        //-----------------
        $sql = " INSERT INTO " . $this->tableName() . " ( `userId`, `operation`, `dateRequest`, `xmlRequest`,
                                                    `jsonResponse`, `errorCode`, `success`, `idPayment` )
                                        VALUES ( :USERID, :OPERATION, :DATEREQUEST, :XMLREQUEST,
                                                    :JSONRESPONSE, :ERRORCODE, :SUCCESS, :IDPAYMENT  ) ";

        $aParamValues = array(':USERID' => $this->userId, ':OPERATION' => $this->operation,
            ':DATEREQUEST' => $this->dateRequest, ':XMLREQUEST' => $this->xmlRequest,
            ':JSONRESPONSE' => $this->jsonResponse, ':ERRORCODE' => $this->errorCode,
            ':SUCCESS' => $this->success, ':IDPAYMENT' => $this->idPayment);

        $this->id = $this->executeSQL($sql, $aParamValues);
        if ($this->id <= 0) {
            $successTrans = false;
        }

        //---------------
        if ($successTrans) {
            $this->commitTrans();
            return $this->id;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /** Gets the reference we set in the remote payment gateway platform.
     * @return mixed|string
     */
    public function getOrderNumber()
    {
        return $this->paySuppOrderId;
    }


    public function setOrderNumber($paySuppOrderId)
    {
        $this->paySuppOrderId = $paySuppOrderId;
    }

    /** Returns a string containing the universal id in Paypal platform.
     * @return mixed|string
     */
    public function getUniqueId()
    {
        if ( isset($this->aFullResponse) && is_array($this->aFullResponse) ){
            if (array_key_exists("DoExpressCheckoutPaymentResponseDetails",$this->aFullResponse)){
                if (array_key_exists("PaymentInfo",$this->aFullResponse["DoExpressCheckoutPaymentResponseDetails"])){
                    if (array_key_exists("TransactionID",$this->aFullResponse["DoExpressCheckoutPaymentResponseDetails"]["PaymentInfo"])){
                        return strval($this->aFullResponse["DoExpressCheckoutPaymentResponseDetails"]["PaymentInfo"]["TransactionID"]);
                    }
                }
            }
        }

        return "";
    }

    /** @TODO
     *
     * @return mixed|string
     */
    public function getPaySuppExtProfId() {
        return "";
    }

}
