<?php

/**
 * This is the model class for table "pending refunds".
 *
 * This is the model class for table "payments_adyen_pending_refunds".
 * The followings are the available columns in table 'payments_adyen_pending_refunds':
 *
 */
class AdyenPendingRefunds extends CmAbaPaymentsAdyenPendingRefunds
{
    /**
     * Constructor, null implementation
     */
    public function __construct()
    {
        parent::__construct();
        $this->connection = Yii::app()->getComponent(AbaActiveRecord::DB_DEFAULT);
    }
}
