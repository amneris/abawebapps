<?php

/**
 * Class PayGatewayLog
 */
abstract class PayGatewayLog extends  AbaActiveRecord
{
    /* @var integer $idPaySupplier; Pay_supplier: Caixa, AllPago, Paypal, B2b, Appstore, Adyen, etc. */
    protected $idPaySupplier;

    public function __construct($idPaySupplier=null)
    {
        parent::__construct();
        $this->idPaySupplier = $idPaySupplier;
    }

    public function setXMLFieldsToProperties( $aXMLFields )
    {
        foreach( $aXMLFields as $fieldName=>$value )
        {
            $this->$fieldName = $value;
        }

        return true;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getOrderNumber()
    {
        return $this->Order_number;
    }

    public function getPaySuppExtId()
    {
        return $this->idPaySupplier;
    }

    public function getPaySuppExtProfId() {
        return $this->MerchantIdentifier;
    }

    /** Returns the unique id of the gateway supplier platform. Usually "their transaction identification"
     * Each platform returns its value in several ways.
     * @return mixed
     */
    public abstract function getUniqueId();

}
?>