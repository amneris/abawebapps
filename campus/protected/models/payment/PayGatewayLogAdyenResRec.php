<?php
/**
 * @property integer id
 * @property string idPayment
 * @property string dateSent
 * @property string authCode
 * @property string dccAmount
 * @property string dccSignature
 * @property string fraudResult
 * @property string issuerUrl
 * @property string md
 * @property string paRequest
 * @property string Order_number
 * @property string refusalReason
 * @property string resultCode
 * @property string additionalData
 * @property string XML
 * @property string numNotifications
 * @property string reference
 * @property string adyenEncryptedData
 */

class PayGatewayLogAdyenResRec extends PayGatewayLog
{
    public $reference = '';

    /**
     * @return string
     */
    public function tableName() {
        return "pay_gateway_adyen_response";
    }

    /**
     * @return string
     */
    public function mainFields() {
        return " * ";
    }

    /**
     * @return bool
     */
    public function insertResponseReceived() {
        $this->beginTrans();
        $successTrans = true;
        //-----------------
        $sql= " INSERT INTO ".$this->tableName()." (
                    `idPayment`, `dateSent`, `authCode`, `dccAmount`,
                    `dccSignature`, `fraudResult`, `issuerUrl`, `md`,
                    `paRequest`, `Order_number`, `refusalReason`, `resultCode`,
                    `additionalData`, `XML`, `numNotifications`
                ) VALUES (
                    :IDPAYMENT, :DATESENT, :AUTHCODE, :DCCAMOUNT,
                    :DCCSIGNATURE, :FRAUDRESULT, :ISSUERURL, :MD,
                    :PAREQUEST, :ORDERNUMBER, :REFUSALREASON, :RESULTCODE,
                    :ADDITIONALDATA, :XML, :NUMNOTIFICATIONS
                )";

        $aParamValues= array(
            ":IDPAYMENT" =>         $this->idPayment,
            ":DATESENT" =>      $this->dateSent,
            ":AUTHCODE" =>          $this->authCode,
            ":DCCAMOUNT" =>         $this->dccAmount,
            ":DCCSIGNATURE" =>      $this->dccSignature,
            ":FRAUDRESULT" =>       $this->fraudResult,
            ":ISSUERURL" =>         $this->issuerUrl,
            ":MD" =>                $this->md,
            ":PAREQUEST" =>         $this->paRequest,
            ":ORDERNUMBER" =>       $this->Order_number,
            ":REFUSALREASON" =>     $this->refusalReason,
            ":RESULTCODE" =>        $this->resultCode,
            ":ADDITIONALDATA" =>    $this->additionalData,
            ":XML" =>               $this->XML,
            ":NUMNOTIFICATIONS" =>  $this->numNotifications
        );
        $this->id = $this->executeSQL($sql, $aParamValues, true);
        if( $this->id <= 0 ) {
            $successTrans = false;
        }
        //---------------
        if( $successTrans ) {
            $this->commitTrans();
            return true;
        }
        else {
            $this->rollbackTrans();
            return false;
        }
        return false;
    }

    /**
     * @param $uniqueId
     *
     * @return $this|bool
     */
    public function getAdyenPaymentByUniqueId( $uniqueId )
    {
        $sql =          " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS paa WHERE paa.`Order_number`=:UNIQUEID ";
        $paramsToSQL =  array(":UNIQUEID" => $uniqueId);
        $dataReader =   $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /**
     * @param $paymentId
     *
     * @return $this|bool
     */
    public function getAdyenPaymentByPaymentId( $paymentId )
    {
        $sql =          " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS paa WHERE paa.`idPayment`=:IDPAYMENT ";
        $paramsToSQL =  array(":IDPAYMENT" => $paymentId);
        $dataReader =   $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    public function getAdyenPaymentByUniqueIdAndPaymentId( )
    {
        $sql =          " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " AS paa WHERE paa.`Order_number`=:UNIQUEID AND paa.`idPayment`=:IDPAYMENT ";
        $paramsToSQL =  array(":UNIQUEID" => $this->getUniqueId(), ":IDPAYMENT" => $this->getPaymentId());
        $dataReader =   $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }
        return false;
    }

    /** Returns string representing the number Adyen gives to the transaction.
     * @return int|mixed
     */
    public function getUniqueId() {
        return $this->Order_number;
    }

    /**
     * @return array|mixed|null|string
     */
    public function getOrderNumber() {
        return $this->reference;
    }

    /**
     * @return string
     */
    public function getPaymentId() {
        return $this->idPayment;
    }

    /**
     * @return string
     */
    public function getAdyenEncryptedData() {
        return $this->adyenEncryptedData;
    }

    /**
     * @return bool
     */
    public function updateNotifications() {

        $this->beginTrans();

        $sSql =  " UPDATE " . $this->tableName();
        $sSql .= " SET `numNotifications` = `numNotifications` + 1 ";
        $sSql .= " WHERE `id` = '" . $this->id . "' ;";

        if ( $this->updateSQL( $sSql ) > 0 ) {
            $this->commitTrans();
            return true;
        } else {
            $this->rollbackTrans();
        }
        return false;
    }

    /** @TODO
     *
     * @return mixed|string
     */
    public function getPaySuppExtProfId() {
        return "";
    }

}
