<?php

/**
 * This is the model class for table "payments_adyen_frauds".
 * The followings are the available columns in table 'payments_adyen_frauds':
 */
class AdyenFraud extends CmAbaPaymentsAdyenFraud
{
    /**
     * Constructor, null implementation
     */
    public function __construct()
    {
        parent::__construct();
        $this->connection = Yii::app()->getComponent(AbaActiveRecord::DB_DEFAULT);
    }
}
