<?php

/**
 * @property integer id
 * @property string idPayment
 * @property integer userId
 * @property string operation
 * @property string dateRequest
 * @property string dateResponse
 * @property string xmlRequest
 * @property string xmlResponse
 * @property string errorCode
 * @property integer success
 * @property integer idPaySupplier
 *
 */
class PayGatewayLogAllpago extends PayGatewayLog
{
    /** Returns all fields from log All Pago by Id
     * @param $idKey
     *
     * @return bool|PayGatewayLogAllpago
     */
    public function getLogById($idKey)
    {
        $sql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " l  WHERE l.`id`=" . $idKey . "; ";

        $dataReader = $this->querySQL($sql);
        return $this->readRowFillDataColsThis($dataReader);
    }

    /** Gets all logs by an userId. Used from Functional Tests
     *
     * @param integer $userId
     * @return bool|LogUserLevelChange
     */
    public function getLastLogByUserId($userId)
    {
        $sql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() . " l  WHERE l.`userId`=" .
          $userId . " ORDER BY l.`id` DESC LIMIT 1 ; ";
        $dataReader = $this->querySQL($sql);
        return $this->readRowFillDataColsThis($dataReader);
    }

    /**
     * @return string
     */
    public function mainFields()
    {
        return " `id`, `idPayment`, `userId`, `operation`, `dateRequest`, `dateResponse`, `xmlRequest`,
                                                    `xmlResponse`, `errorCode`, `success`, `idPaySupplier` ";
    }

    public function tableName()
    {
        return "pay_gateway_allpago";
    }

    /** Inserts a record in the table
     * @return bool|int
     */
    public function insertLog()
    {
        $this->beginTrans();
        $successTrans = true;
        //-----------------
        $sql = " INSERT INTO " . $this->tableName() . " (`idPayment`, `userId`, `operation`, `dateRequest`, `dateResponse`, `xmlRequest`,
                                                    `xmlResponse`, `errorCode`, `success`, `idPaySupplier` )
                                        VALUES ( :IDPAYMENT, :USERID, :OPERATION, :DATEREQUEST, :DATERESPONSE, :XMLREQUEST,
                                                    :XMLRESPONSE, :ERRORCODE, :SUCCESS, :IDPAYSUPPLIER  ) ";

        $aParamValues = array(
          ':IDPAYMENT' => $this->idPayment,
          ':USERID' => $this->userId,
          ':OPERATION' => $this->operation,
          ':DATEREQUEST' => $this->dateRequest,
          ':DATERESPONSE' => $this->dateResponse,
          ':XMLREQUEST' => $this->xmlRequest,
          ':XMLRESPONSE' => $this->xmlResponse,
          ':ERRORCODE' => $this->errorCode,
          ':SUCCESS' => $this->success,
          ':IDPAYSUPPLIER' => $this->idPaySupplier
        );

        $this->id = $this->executeSQL($sql, $aParamValues);
        if ($this->id <= 0) {
            $successTrans = false;
        }

        //---------------
        if ($successTrans) {
            $this->commitTrans();
            return $this->id;
        } else {
            $this->rollbackTrans();
            return false;
        }
    }

    /**
     * @return null|string
     */
    public function getUniqueId()
    {
        try {
            if (!empty($this->xmlResponse)) {
                $oXmlResponse = $this->getXmlResponse();
                $aUniqueId = $oXmlResponse->xpath("/Response/Transaction/Identification/UniqueID");
                if (count($aUniqueId) >= 1) {
                    return strval($aUniqueId[0]);
                }
            }
        } catch (Exception $e) {
            // continue...
        }
        return null;

    }

    /**
     * @return null|SimpleXMLElement
     */
    public function getXmlResponse()
    {
        try {
            if (!empty($this->xmlResponse)) {
                $xmlResponse = new SimpleXMLElement($this->xmlResponse);
                return $xmlResponse;
            }
        } catch (Exception $e) {
            // continue...
        }
        return null;
    }

    /** Saves XML response to disk, in the runtime application directory.
     * @return bool|string
     */
    public function getXmlResponseAsFile()
    {
        try {
            if (!empty($this->xmlResponse)) {
                $xmlResponse = new SimpleXMLElement($this->xmlResponse);
                $tmpFilePath = Yii::app()->basePath . '/runtime/tmp' . HeDate::todayNoSeparators(true) . '.xml';
//                file_put_contents($tmpFilePath,$xmlResponse->saveXML())
                $isSaved = $xmlResponse->saveXML($tmpFilePath);
                if ($isSaved) {
                    return $tmpFilePath;
                } else {
                    return false;
                }
            }
        } catch (Exception $e) {
            // continue...
        }
        return false;
    }

    /** Returns our internal id for future communications with them. A way to find our payments by
     * our id in their systems.
     *
     * @return mixed|null|string
     */
    public function getOrderNumber()
    {
        try {
            if (!empty($this->xmlResponse)) {
                $oXmlResponse = $this->getXmlResponse();
                $aUniqueId = $oXmlResponse->xpath("/Response/Transaction/Identification/TransactionID");
                if (count($aUniqueId) >= 1) {
                    return strval($aUniqueId[0]);
                }
            }
        } catch (Exception $e) {
            // continue...
        }
        return null;
    }

    /** From cron, to find out if the cron has been executed every single day.
     * We try this query to go to Slave at the moment.
     *
     * @param string $dateRequest
     * @param integer $idPaySupplier
     * @param bool $success
     *
     * @return bool|PayGatewayLogAllpago
     */
    public function getLastQuery($dateRequest, $idPaySupplier, $success = false)
    {
        return false;
    }

    /** Returns the URL to the BOLETO.
     *  <ConnectorDetails>
     * <Result name="ConnectorTxID1">2033.7691.1010</Result>
     * <Result name="EXTERNAL_SYSTEM_LINK">http://mu ...
     *
     * @return null|string
     */
    public function getUrlBoleto()
    {
        if ($this->idPaySupplier == PAY_SUPPLIER_ALLPAGO_BR_BOL) {
            try {
                if (!empty($this->xmlResponse)) {
                    $oXmlResponse = $this->getXmlResponse();
                    $aResults = $oXmlResponse->xpath("/Response/Transaction/Processing/ConnectorDetails/Result");
                    if (count($aResults) >= 2) {
                        return strval($aResults[2]);
                    }
                    elseif(count($aResults) >= 1) {
                        return strval($aResults[1]);
                    } else {
                        return strval($aResults[0]);
                    }
                }
            } catch (Exception $e) {
                // continue...
            }
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getXmlOrderId()
    {
        if ($this->idPaySupplier == PAY_SUPPLIER_ALLPAGO_BR_BOL) {
            try {
                if (!empty($this->xmlResponse)) {
                    $oXmlResponse = $this->getXmlResponse();
//                    $aResults = $oXmlResponse->xpath("/Response/Transaction/Processing/Redirect/Parameter[@name=\"OrderId\"]");
                    $aResults = $oXmlResponse->xpath("/Response/Transaction/Identification/ShortID");
                    if (count($aResults) >= 1) {
                        return strval($aResults[0]);
                    } else {
                        return strval($aResults);
                    }
                }
            } catch (Exception $e) {
                // continue...
            }
        }
        return null;
    }

    /** Returns the log for a former payment
     *
     * @param string $idPayment
     * @return $this|bool
     */
    public function getByIdPayment($idPayment)
    {
        $sql = " SELECT " . $this->mainFields() . " FROM " . $this->tableName() .
          " l  WHERE l.`idPayment`= :IDPAYMENT  ORDER BY l.`id` DESC LIMIT 1 ; ";
        $dataReader = $this->querySQL($sql, array(':IDPAYMENT' => $idPayment));
        return $this->readRowFillDataColsThis($dataReader);
    }

    /** @TODO
     *
     * @return mixed|string
     */
    public function getPaySuppExtProfId()
    {
        return "";
    }

}
