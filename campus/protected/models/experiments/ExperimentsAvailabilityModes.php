<?php

class ExperimentsAvailabilityModes extends CmAbaExperimentsAvailabilityModes {

    /**
     * Constructor, null implementation
     */
    public function __construct()
    {
        parent::__construct();
        $this->connection = Yii::app()->getComponent(AbaActiveRecord::DB_DEFAULT);
    }

}
