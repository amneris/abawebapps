<?php
/**
 * Class QueueUserRelease
 *
 * @property integer $currentLevel
 * @property string $dateCronFilter
 * @property integer $daysPassed
 * @property string $email
 * @property integer $id
 * @property integer $levelsNotFinished
 * @property integer $maxLastModified
 * @property integer $maxVideoId
 * @property bool $unlock
 * @property integer $userId
 * @property integer $userLevel
 */
class QueueUserRelease extends AbaActiveRecord
{

    /** Standard for Yii
     * @return string
     */
    public function tableName()
    {
        return Yii::app()->params['dbCampusSummary'].'.queue_users_to_release';
    }

    /** Standard method for Yii
     * @return string
     */
    public function mainFields()
    {
        return " `id`, `dateCronFilter`, `userId`, `email`, `currentLevel`, `userLevel`, `unlock`,
                `levelsNotFinished`, `maxVideoId`, `maxLastModified`, `daysPassed` ";
    }

    /**
     * Returns eldest row of the table.
     *
     * @return $this|bool
     */
    public function getFirstRow()
    {
        $sql = " SELECT  " . $this->mainFields() . " FROM " . $this->tableName() . "
		ORDER BY `maxLastModified` ASC LIMIT 1";

        $dataReader = $this->querySQL($sql);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }

        return false;
    }

    /**
     * Returns eldest row of the table.
     *
     * @param integer $pkId
     *
     * @return $this|bool
     */
    public function getReleaseById( $pkId )
    {
        $sql = " SELECT  " . $this->mainFields() . " FROM " . $this->tableName() . "
                WHERE `id`= ".$pkId;

        $dataReader = $this->querySQL($sql);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }

        return false;
    }


    /** Returns array of rows with all columns available
     * @param int $limit
     * @return array|bool
     */
    public function getAllRowsSorted($limit=500)
    {
        $sql = " SELECT  " . $this->mainFields() . " FROM " . $this->tableName() .
               "  ORDER BY `maxLastModified` ASC LIMIT $limit ";

        $dataRows = $this->queryAllSQL($sql);
        if (!$dataRows) {
            return false;
        }

        return $dataRows;
    }

    /** Runs a SELECT INTO INSERT query. It is a summary type operation, which means
     * transfers summary data from 3 tables to one to avoid time consumption.
     * Returns number of rows inserted.
     *
     * @return bool|int
     */
    public function collectInsertInto()
    {
        $entryDate = HeDate::getDateAdd(HeDate::todaySQL(false),-180,0);

        $sql =" INSERT INTO ".$this->tableName().
       " (`dateCronFilter`, `userId`, `email`, `currentLevel`)
         SELECT DATE(NOW()) as dateCronFilter,
                  u.id as `userId`,
                  u.email,
                  u.currentLevel
                FROM `user` u
                  WHERE
                     u.userType=".FREE."
                     AND DATE(u.entryDate)>DATE('".$entryDate."')
                     AND u.cancelReason is null
                    AND (
                     uvu.id IS NULL
                         OR
                    u.currentLevel<=6
                        )

                        GROUP BY u.id


                       HAVING
                          `daysPassed` BETWEEN 1 AND 30
                            AND (
                                   MaxVideoId IS NULL
                                   OR
                                   (
                                       (u.currentLevel<".MAX_ENGLISH_LEVEL." AND MaxVideoId<".HeUnits::getMaxUnits().")
                                    OR (u.currentLevel=".MAX_ENGLISH_LEVEL." AND MaxVideoId<".HeUnits::getMaxUnits().")
                                    OR (
                                         MaxVideoId=".HeUnits::getMaxUnits()."
                                         AND u.currentLevel<".MAX_ENGLISH_LEVEL."
                                         AND (LevelsNotFinished>=1)
                                       )
                                   )
                                 )
                         ORDER BY `MaxLastModified` ASC ";

        $retRows =  $this->executeSQL( $sql, NULL, true );
        if ($retRows > 0) {
            return $retRows;
        }

        return false;

    }

    /**
     * @param $todaySQL
     *
     * @return bool
     */
    public function markReleaseAsDone($todaySQL)
    {
        Yii::app()->config->set("RELEASE_LAST_EXECUTION", $todaySQL);

        return true;
    }

    /**
     * @param $todaySQL
     * @return bool
     */
    public function isReleaseDone($todaySQL)
    {
        if ( trim(Yii::app()->config->get("RELEASE_LAST_EXECUTION"))==$todaySQL  ){
            return true;
        }

        return false;

    }


}
