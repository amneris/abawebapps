<?php
/**
 * Collection of UnitCourse
 */
class UnitsCourses extends AbaActiveRecord
{
    protected $classTypeUnit = "UnitCourse";
    /**
     * @var array UnitCourse
     */
    protected $aUnitsCourses = array();

    /** returns array with 24 units MoUnitCourse
     * @param integer $level
     *
     * @return UnitCourse[]
     * @throws CDbException
     */
    public function getAllUnitsByLevel($level=NULL)
    {
        $collectSucces = true;
        if(is_null($level))
        {
            $level = Yii::app()->user->currentLevel;
        }

        $maxUnitLevel = $this->getEndUnitByLevel($level);
        for( $inx= $this->getStartUnitByLevel($level); $inx<=$maxUnitLevel; $inx++ )
        {
            /* @var UnitCourse $tmpUnitCourse */
            $tmpUnitCourse = new $this->classTypeUnit();
            if( $tmpUnitCourse->getUnit($inx) )
            {
                $this->aUnitsCourses[$inx] = $tmpUnitCourse;
            }
            else
            {
                $collectSucces = false;
                $this->aUnitsCourses[$inx] = NULL;
            }
        }

        if( count($this->aUnitsCourses) < UNITS_PER_LEVEL  || !$collectSucces )
        {
            throw new CDbException("Some units are missing. It is expected ".UNITS_PER_LEVEL." units and only ".
                count($this->aUnitsCourses)." were collected.");
        }

        return $this->aUnitsCourses;
    }


    /**
     * Returns all units names for a certain level
     * @static
     *
     * @param $level
     *
     * @return array|bool|int
     */
    public static function getUnitsNamesByLevel($level)
    {
        return HeUnits::getUnitNameByLevel( $level );
    }

    /**
     * @param $level
     *
     * @return int
     */
    public function getStartUnitByLevel($level)
    {
        return HeUnits::getStartUnitByLevel($level);
    }

    /**
     * @param $level
     *
     * @return int
     */
    public function getEndUnitByLevel($level)
    {
        return HeUnits::getEndUnitByLevel($level);
    }

    /**
     * Returns which level the unitId belongs to.
     * @param int $unitId
     *
     * @return int
     */
    public function getLevelByUnitId( $unitId )
    {
        $decLevel = $unitId / UNITS_PER_LEVEL;
        if( is_integer($decLevel) )
        {
            return $decLevel;
        }
        else
        {
            return intval($decLevel+1);
        }
    }

    /** Returns an array containing the first unit of every level.
     * @param $lowerBound
     * @param $topBound
     *
     * @return array|bool
     */
    public function getArrayStartUnits( $lowerBound, $topBound)
    {
        return HeUnits::getArrayStartUnitsByLevel( $lowerBound, $topBound);
    }

    /**
     * Returns an array containing test an option ids in case user has an assigned test
     *
     * @param int $userId
     * @param string $language
     * @return array|bool
     */
    public function getUserCourse ( $userId = NULL, $language = 'es' )
    {
        if(empty($userId)) {
            return false;
        }

        $sql = "SELECT ct.courseId, ct.description
                FROM course_test ct
                JOIN course_test_user ctu ON ctu.courseId = ct.courseId
                WHERE ctu.userId = :USERID AND ct.locale LIKE :LANGUAGE
                LIMIT 1";

        $params = array(":USERID" => $userId, ":LANGUAGE" => '%'.$language.'%');

        $dataReader = $this->querySQL($sql, $params);
        $row = $dataReader->read();

        if ($row !== false) {
            return $row;
        }

        return false;
    }
}
