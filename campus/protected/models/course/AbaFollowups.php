<?php
/**
 * Collection of AbaFollowup
 */
class AbaFollowups extends AbaActiveRecord
{


    /**
     * @var array AbaFollowup
     */
    protected $aFollowup = array();

    /**
     * @param      $aUnits
     * @param null $userId
     *
     * @return array
     * @throws CDbException
     */
    public function getAllAbaFollowupByLevelUser( $aUnits, $userId=null )
    {
        $collectSuccess = true;

        if(is_null($userId))
        {
            $userId = Yii::app()->user->getId();
        }

        /* @var UnitCourse[] $aUnits */
        /* @var UnitCourse $unit */
        foreach( $aUnits as $key=>$unit)
        {
            $tmpFollow = new AbaFollowup();
            if( $tmpFollow->getFollowupByIds( $userId, $unit->getUnitId() ) )
            {
                $this->aFollowup[$key] = $tmpFollow;
            }
            else
            {
                $collectSuccess = false;
                $this->aFollowup[$key] = null;
            }

        }

        if( count($this->aFollowup)<UNITS_PER_LEVEL  || !$collectSuccess )
        {
            throw new CDbException("Some follow ups are missing. It is expected ".UNITS_PER_LEVEL." follow ups and only were collected.");
        }

        return $this->aFollowup;
    }

    /** Returns the quantity of video classes watched
     * @param int $userId
     *
     * @return int
     */
    public function getNumberOfVideosWatched( $userId )
    {
        $sql = " SELECT count(f.id) as `videosWatched` FROM followup4 f
                  WHERE f.`userid`=:USERID AND f.`gra_vid`>=100 ";

        $params = array( ":USERID"=>$userId );

        $dataReader=$this->querySQL($sql, $params );
        $row = $dataReader->read();
        if ($row !== false) {
            return intval($row["videosWatched"]);
        }

        return 0;
    }

    /**
     * @param integer $userId
     * @param integer $level
     *
     * @return int - User's current level progress %
     */
    public function getCurrentLevelProgress($userId, $level=NULL)
    {
        if ( isset($level) ) {
            $currentLevel = $level;
        } else {
            $currentLevel = Yii::app()->user->currentLevel;
        }

        $key = array_keys(HeUnits::getUnitNameByLevel($currentLevel));

        //Para un futuro no seria mala idea para ganar rendimiento cambiar el trozo siguiente por
        //Select sum(floor((sit_por+stu_por+dic_por+rol_por+gra_vid+wri_por+new_por+if(eva_por*10>80,100,eva_por*10))/8))/24 from followup4 where userid=28678 and themeid between 25 and 48

        $progress = 0;
        for($k = 0; $k < count($key); $k ++)
        {
            $followup = new AbaFollowup();
            $followup->getFollowupByIds($userId, $key[$k]);
            $progress = $progress + $followup->getUnitProgress(true); // this is temporary, we have to check the calculation of this value...
        }
        return $progress = (int)($progress/count($key));
    }

    /** Returns if all units from a certain level is fully finished with exams included.
     * @param integer $userId
     * @param integer $level
     *
     * @return bool
     */
    public function isLevelCompleted($userId, $level)
    {
        $percentageTotal = $this->getCurrentLevelProgress($userId, $level);
        if ($percentageTotal>99){
            return true;
        }

        return false;
    }
}
