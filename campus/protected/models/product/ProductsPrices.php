<?php
/**
 *
 * SELECT
pr.idProduct, pr.idCountry, pr.idPeriodPay,
pr.priceOfficialCry, c.officialIdCurrency,c.ABAIdCurrency,
prm.value as `discount`,prm.idPromoCode,prm.descriptionText as `descPromo` , prm.type,
p.descriptionText as `descProd` , pr.descriptionText as `descPackage` , ppd.descriptionText as `descPeriod`

FROM
products_list p
LEFT JOIN products_prices pr ON p.id=pr.idProduct
LEFT JOIN products_promos prm ON (prm.enabled<>0 AND pr.idProduct=prm.idProduct AND pr.idPeriodPay=prm.idPeriodPay)
LEFT JOIN prod_periodicity ppd ON pr.idPeriodPay=ppd.id
LEFT JOIN country c ON pr.idcountry=c.id


WHERE
(pr.idCountry=226 OR pr.idCountry=0 )

    AND (prm.idCountry = 226 OR  prm.idCountry = 0 OR prm.idCountry is null)
AND prm.visibleWeb=1

GROUP BY pr.idProduct, pr.idCountry, pr.idPeriodPay
ORDER BY p.hierarchy DESC,pr.idProduct ASC, pr.idPeriodPay, pr.idCountry DESC ;

 */
class ProductsPrices extends AbaActiveRecord
{
    //-------------------------

    public function  __construct($idCountry=NULL)
    {
        parent::__construct();
        if( isset($idCountry) )
        {
            return $this->getAllProductsByCountryId($idCountry);
        }
    }


    public function mainFields()
    {
        return "  pr.idProduct, pr.idCountry, pr.idPeriodPay,
            pr.priceOfficialCry, pr.descriptionText as `descProd`, pr.`userType`,
            pr.`isPlanPrice`, pr.`visibleWeb`,
            ppd.descriptionText as `descPeriod`,
            c.officialIdCurrency, c.ABAIdCurrency ";
    }

    /**
     * @param integer    $idCountry
     * @param int|string $idPromoCode
     * @param bool $onlyVisible
     *
     * @throws CDbException
     * @return array|bool|null|ProductPrice[]
     */
    public function getAllProductsByCountryId( $idCountry, $idPromoCode="", $onlyVisible=true, $stUserProdsPrices=array() )
    {
        /* @var $aProds ProductPrice[] */
        $aProds = null;
        $sqlFieldsPromocodes = ", null as `discount`, null as `idPromoCode`,null as `descPromo` , null as `type`, null as `idPromoType` ";
        $sqlFromPromo = " ";
        $sqlWPromoCode = " ";
        $sqlWOnlyVisible = ' AND pr.visibleWeb=1 ';
        if( !$onlyVisible ){
            $sqlWOnlyVisible = ' ';
        }

        if( !is_null($idPromoCode) && $idPromoCode!=="" )
        {
            $sqlFieldsPromocodes = ", IF(pr.idPeriodPay=30 AND prm.idPeriodPay IS NULL AND prm.idProduct IS NULL, NULL, prm.`value`) as `discount`,
                                    IF(pr.idPeriodPay=30 AND prm.idPeriodPay IS NULL AND prm.idProduct IS NULL, NULL, prm.`idPromoCode`)  as `idPromoCode`,
                                    IF(pr.idPeriodPay=30 AND prm.idPeriodPay IS NULL AND prm.idProduct IS NULL, NULL, prm.`descriptionText`) as `descPromo` ,
                                    IF(pr.idPeriodPay=30 AND prm.idPeriodPay IS NULL AND prm.idProduct IS NULL, NULL, prm.`type`) as `type`, prm.idPromoType
                                    ";
            $sqlFromPromo = " LEFT JOIN products_promos prm ON
                                (   prm.`idPromoCode`=:IDPROMOCODE AND
                                    prm.visibleWeb=1 AND
                                    prm.enabled=1 AND
                                    prm.dateStart <= DATE(NOW()) AND
                                    prm.dateEnd >= DATE(NOW()) AND
                                    (prm.idProduct is null OR prm.idProduct=pr.idProduct) AND
                                    (prm.idPeriodPay IS NULL OR prm.idPeriodPay=pr.idPeriodPay)
                                ) ";
            $sqlWPromoCode = "  ";
        }

        $sql = "SELECT ".$this->mainFields().$sqlFieldsPromocodes."
                   FROM products_prices pr
                    LEFT JOIN prod_periodicity ppd ON pr.idPeriodPay=ppd.id
                    LEFT JOIN country c ON pr.idcountry=c.id
                    ".$sqlFromPromo."
                 WHERE pr.enabled=1
                    $sqlWOnlyVisible
                    AND ( pr.idCountry=:IDCOUNTRY OR
                        pr.idCountry=".Yii::app()->config->get("ABACountryId")." )
                    AND ( pr.userType<=".MAX_ALLOWED_LEVEL." )
                    ".$sqlWPromoCode."
                GROUP BY pr.idProduct, pr.idCountry, pr.idPeriodPay
                ORDER BY pr.hierarchy DESC,
                        pr.idPeriodPay ASC,
                        pr.idProduct ASC,
                        pr.idCountry DESC  /* getAllProductsByCountryId */;";

        $paramsToSQL = array(":IDCOUNTRY"=>$idCountry);
        if( !is_null($idPromoCode) && $idPromoCode!=="" )
        {
            $paramsToSQL[":IDPROMOCODE"] = $idPromoCode;
        }

        $dataReader=$this->querySQL($sql, $paramsToSQL);
        if( !$dataReader )
        {
            return false;
        }

        while(($row = $dataReader->read()) !== false)
        {
            //
            //#4.101.1
            $idPromoCode = ProductPrice::adjustPromoCode($row["idPromoCode"], $row["isPlanPrice"], $row["idPromoType"]);

            $prod = new ProductPrice();

            //
            //#5739
            $iProductPrice = null;

            foreach($stUserProdsPrices as $stUserProdsPrice) {
                if($stUserProdsPrice['idProduct'] == $row["idProduct"]) {
                    $iProductPrice = $stUserProdsPrice["price"];
                    break;
                }
            }

            if( !$prod->getProductByMixedId($row["idProduct"], $row["idCountry"], $row["idPeriodPay"], $idPromoCode, $idCountry, $iProductPrice) ) {
                throw new CDbException( " ABA: There has been a problem retrieving the product  " . $row["idProduct"] . $row["idCountry"] . $row["idPeriodPay"] );
            }

            $aProds[]=$prod;
        }

        return $aProds;
    }

    /**
     * @param $idProduct
     * @param $idCountry
     * @param string $idPromoCode
     * @param bool|true $onlyVisible
     * @param array $stUserProdsPrices
     *
     * @return bool|ProductPrice[]
     * @throws CDbException
     */
    public function getProductByCountryId( $idProduct, $idCountry, $idPromoCode="", $onlyVisible=true, $stUserProdsPrices=array() )
    {
        /* @var $aProds ProductPrice[] */
        $aProds = null;
        $sqlFieldsPromocodes = ", null as `discount`, null as `idPromoCode`,null as `descPromo` , null as `type`, null as `idPromoType` ";
        $sqlFromPromo = " ";
        $sqlWPromoCode = " ";
        $sqlWOnlyVisible = ' AND pr.visibleWeb=1 ';
        if( !$onlyVisible ){
            $sqlWOnlyVisible = ' ';
        }

        if( !is_null($idPromoCode) && $idPromoCode!=="" )
        {
            $sqlFieldsPromocodes = ", IF(pr.idPeriodPay=30 AND prm.idPeriodPay IS NULL AND prm.idProduct IS NULL, NULL, prm.`value`) as `discount`,
                                    IF(pr.idPeriodPay=30 AND prm.idPeriodPay IS NULL AND prm.idProduct IS NULL, NULL, prm.`idPromoCode`)  as `idPromoCode`,
                                    IF(pr.idPeriodPay=30 AND prm.idPeriodPay IS NULL AND prm.idProduct IS NULL, NULL, prm.`descriptionText`) as `descPromo` ,
                                    IF(pr.idPeriodPay=30 AND prm.idPeriodPay IS NULL AND prm.idProduct IS NULL, NULL, prm.`type`) as `type`, prm.idPromoType
                                    ";
            $sqlFromPromo = " LEFT JOIN products_promos prm ON
                                (   prm.`idPromoCode`=:IDPROMOCODE AND
                                    prm.visibleWeb=1 AND
                                    prm.enabled=1 AND
                                    prm.dateStart <= DATE(NOW()) AND
                                    prm.dateEnd >= DATE(NOW()) AND
                                    (prm.idProduct is null OR prm.idProduct=pr.idProduct) AND
                                    (prm.idPeriodPay IS NULL OR prm.idPeriodPay=pr.idPeriodPay)
                                ) ";
            $sqlWPromoCode = "  ";
        }

        $sql = "SELECT ".$this->mainFields().$sqlFieldsPromocodes."
                   FROM products_prices pr
                    LEFT JOIN prod_periodicity ppd ON pr.idPeriodPay=ppd.id
                    LEFT JOIN country c ON pr.idcountry=c.id
                    ".$sqlFromPromo."
                 WHERE
                    pr.enabled=1
                    AND pr.idProduct = '" . trim($idProduct) . "'
                    $sqlWOnlyVisible
                    AND ( pr.idCountry=:IDCOUNTRY OR
                        pr.idCountry=".Yii::app()->config->get("ABACountryId")." )
                    AND ( pr.userType<=".MAX_ALLOWED_LEVEL." )
                    ".$sqlWPromoCode."
                GROUP BY pr.idProduct, pr.idCountry, pr.idPeriodPay
                ORDER BY pr.hierarchy DESC,
                        pr.idPeriodPay ASC,
                        pr.idProduct ASC,
                        pr.idCountry DESC  /* getAllProductsByCountryId */;";

        $paramsToSQL = array(":IDCOUNTRY"=>$idCountry);
        if( !is_null($idPromoCode) && $idPromoCode!=="" )
        {
            $paramsToSQL[":IDPROMOCODE"] = $idPromoCode;
        }

        $dataReader=$this->querySQL($sql, $paramsToSQL);
        if( !$dataReader )
        {
            return false;
        }

        while(($row = $dataReader->read()) !== false)
        {
            //
            //#4.101.1
            $idPromoCode = ProductPrice::adjustPromoCode($row["idPromoCode"], $row["isPlanPrice"], $row["idPromoType"]);

            $prod = new ProductPrice();

            //
            //#5739
            $iProductPrice = null;

            foreach($stUserProdsPrices as $stUserProdsPrice) {
                if($stUserProdsPrice['idProduct'] == $row["idProduct"]) {
                    $iProductPrice = $stUserProdsPrice["price"];
                    break;
                }
            }

            if( !$prod->getProductByMixedId($row["idProduct"], $row["idCountry"], $row["idPeriodPay"], $idPromoCode, $idCountry, $iProductPrice) ) {
                throw new CDbException( " ABA: There has been a problem retrieving the product  " . $row["idProduct"] . $row["idCountry"] . $row["idPeriodPay"] );
            }

            $aProds[]=$prod;
        }

        return $aProds;
    }


}
?>