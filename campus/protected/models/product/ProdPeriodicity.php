<?php
/** SELECT pp.`id`, pp.`days`, pp.`months`, pp.`descriptionText`
 * FROM prod_periodicity pp WHERE pp.`id`=30;
 *
 * @property integer id
 * @property integer days
 * @property string descriptionText
 * @property integer months
 **/
class ProdPeriodicity extends AbaActiveRecord
{
    const MONTH_1=30;
    const MONTH_3=90;
    const MONTH_4=120;
    const MONTH_6=180;
    const MONTH_9=270;
    const MONTH_12=360;
    const MONTH_18=540;
    const MONTH_24=720;

    public function tableName()
    {
        return 'prod_periodicity';
    }

    public function mainFields()
    {
        return "  pp.`id`, pp.`days`, pp.`months`, pp.`descriptionText` ";
    }

    /**
     * @param $id
     *
     * @return bool|ProdPeriodicity
     */
    public function getProdPeriodById( $id )
	{
		$sql= " SELECT pp.`id`, pp.`days`, pp.`months`, pp.`descriptionText`
                FROM prod_periodicity pp WHERE pp.`id`=:PERIODID ";
        $paramsToSQL = array(":PERIODID" => $id);
        $dataReader=$this->querySQL($sql, $paramsToSQL);
		if(($row = $dataReader->read())!==false)
		{
            $this->fillDataColsToProperties($row);
            return $this;
		}

		return false;
	}


    /**
     * @param $months
     *
     * @return bool|ProdPeriodicity
     */
    public function getProdPeriodByMonths( $months )
    {
        $sql= " SELECT pp.`id`, pp.`days`, pp.`months`, pp.`descriptionText`
                FROM prod_periodicity pp WHERE pp.`months`=:MONTHS ";

        $paramsToSQL = array(":MONTHS" => $months);
        $dataReader=$this->querySQL($sql, $paramsToSQL);
        if(($row = $dataReader->read())!==false)
        {
            $this->fillDataColsToProperties($row);
            return $this;
        }

        return false;
    }

    /**
     * Returns next date
     * @param string $dateYYYYMMDD
     *
     * @return string
     */
    public function getNextDateBasedOnDate( $dateYYYYMMDD="" )
    {
        if( $dateYYYYMMDD == "" )
        {
            $dateYYYYMMDD = HeDate::todaySQL();
        }

        // Cases after 28/mm/yyyy -----------------------------------------------------------------------
        if( intval(HeDate::getDayFromDate($dateYYYYMMDD)) > LAST_DAY_PAYMENT )
        {
            if($this->months==0)
            {
                $dateYYYYMMDD= HeDate::getDateAdd($dateYYYYMMDD, $this->days, 0);
            }
            // We add X months to the current Date
            else
            {
                $dateYYYYMMDD = HeDate::setDayFromDate($dateYYYYMMDD,LAST_DAY_PAYMENT);
                $dateYYYYMMDD= HeDate::getDateAdd($dateYYYYMMDD, 0, ($this->months) );
            }
            return HeDate::getDateWithLastDay($dateYYYYMMDD);

        }

        // Cases before 28/mm/yyyy: ------------------------------------------------------------------------
        if($this->months==0)
        {
            return HeDate::getDateAdd($dateYYYYMMDD, $this->days, 0);
        }
        // We add X months to the current Date
        else
        {
            return HeDate::getDateAdd($dateYYYYMMDD, 0, $this->months);
        }
    }

    /**
     * @param $idPeriodPay
     *
     * @return float|int
     */
    public function getPeriodicityMonth($idPeriodPay, $bStatic=true) {

        $iMonthPeriod = 0;

        if($bStatic) {
            $iMonthPeriod = HePayments::periodToMonth($idPeriodPay);
        }

        if($this->getProdPeriodById($idPeriodPay)) {
            $iMonthPeriod = $this->months;
        }

        return $iMonthPeriod;
    }

}
