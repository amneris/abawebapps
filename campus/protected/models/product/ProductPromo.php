<?php
/**
 * @property string idPromoCode
 * @property string dateEnd
 * @property string dateStart
 * @property string descriptionText
 * @property boolean enabled
 * @property string idProduct
 * @property integer type
 * @property float value
 * @property integer visibleWeb
 * @property integer idDescription
 * @property integer idPromoType
 * SELECT pp.`idPromoCode`, pp.`idProduct`, pp.`idPartner`, pp.`descriptionText`,
 * pp.`type`, pp.`value`, pp.`dateStart`, pp.`dateEnd`, pp.`enabled`, pp.`visibleWeb`, pp.`idDescription`, pp.`idPromoType`
 * FROM products_promos pp;
 */
class ProductPromo extends AbaActiveRecord
{
    const PROMO_TYPE_GENERAL = 1;
    const PROMO_TYPE_FAMILYPLAN = 2;
    const PROMO_TYPE_ALL = 3;

    public function __construct($idPromoCode="")
    {
        parent::__construct();
        if($idPromoCode!=='')
        {
            return $this->getProdPromoById($idPromoCode);
        }
    }

    public function tableName()
    {
        return 'products_promos';
    }

    public function mainFields()
    {
        return "  pp.`idPromoCode`, pp.`idProduct`, pp.`descriptionText`,
                  pp.`type`, pp.`value`, pp.`dateStart`, pp.`dateEnd`, pp.`enabled`, pp.`visibleWeb`, pp.`showValidationType`, pp.`idDescription`, pp.`idPromoType` ";
    }

    /**
     * @param     $idPromoCode
     * @param int $enable
     * @param int $visibleWeb
     * @param bool $dateValid
     *
     * @return bool|ProductPromo
     */
    public function getProdPromoById( $idPromoCode, $enable=1, $visibleWeb=1, $dateValid=false ) {
        $sqlVisible = " ";
        $sqlDateValid = " ";
        if( $dateValid) {
            $sqlDateValid = " AND  pp.dateStart <= DATE(NOW()) AND pp.dateEnd >= DATE(NOW())";
        }
        if( $visibleWeb) {
            $sqlVisible = " AND pp.`visibleWeb`=".$visibleWeb;
        }

        $sql= " SELECT ".$this->mainFields()." FROM ".$this->tableName()." pp WHERE pp.`idPromoCode`=:IDPROMOCODE
		                                                        AND pp.`enabled`=".$enable.
		                                                        $sqlVisible." ".$sqlDateValid." ; ";
        $paramsToSQL =  array(":IDPROMOCODE" => $idPromoCode);
        $dataReader =   $this->querySQL($sql, $paramsToSQL);

        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }

        return false;
    }

    public function getProdPromoSelligentById( $idPromoCode ) {

        $sql= " SELECT ".$this->mainFields()." FROM ".$this->tableName()." pp WHERE pp.`idPromoCode`=:IDPROMOCODE";
        $paramsToSQL =  array(":IDPROMOCODE" => $idPromoCode);
        $dataReader =   $this->querySQL($sql, $paramsToSQL);

        if(($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }

        return false;
    }
}
?>