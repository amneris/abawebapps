<?php
/**
 * @property string descriptionText
 * @property integer userType
 * @property string officialIdCurrency
 * @property string ABAIdCurrency
 * @property string priceOfficialCry
 *
 */
class ProductPrice extends AbaActiveRecord
{
    // PRIMARY KEY
    public $idProduct;
    public $idCountry;
    public $idPeriodPay;
    // Additional Calculated Fields NOT PRESENT IN DATABASE
    // All of these prices are already converted into the ABA Currency, for displaying purposes
    private $originalPrice; // converted into user country currency already , for displaying purposes
    private $finalPrice; //After discount applied; converted into the user currency, for displaying purposes
    private $amountDiscounted; //Amount of discount, Percentage applied over original Price; , for displaying purposes
    private $packageTitleKey; //Final Key Description of the Product-Price-Periodicity
    private $discountTitle; // concept and amount of the discount displayed to the user UI.
    private $txtShowValidationTypePromo; // In case of any Promocode being applied, then we set this with the text that has to be displayed in the payment page.
    private $ccyTransactionSymbol; //Currency symbol of the transaction in the form user UI.
    private $ccyDisplaySymbol; //Currency symbol of be displayed in the form user UI.

    private $originalPriceTransaction; // Price Original into ABA currency (currency transaction price)
    private $finalPriceTransaction; // Price with discount applied into ABA currency (currency transaction price)
    private $amountDiscountedTransaction; //Amount of discount into ABA currency (currency transaction price)
    private $priceMonthlyOriginalTrans; // Price that the user is finally paying monthly, for example if every 4 months he pays 49.99, this property values 4.9. Without discount applied
    //In user currency
    private $priceMonthly; // Price that the user is finally paying monthly, for example if every 4 months he pays 49.99, this property values 4.9. Discount applied
    //In user currency

    public $priceExtranet; // Extranet price
    public $enabledExtranet; // Enabled use of Extrant price
    public $isPlanPrice; // Family plan
    public $visibleWeb; // Family plan

    //-------------------------

    /**
     * @param Integer $unitId
     */
    public function  __construct()
    {
        parent::__construct();
    }


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'products_prices';
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    public function mainFields()
    {
        return "pr.idProduct, pr.idCountry, pr.idPeriodPay,
            pr.priceOfficialCry, c.officialIdCurrency,c.ABAIdCurrency,
            prm.type, prm.value as `discount`,prm.idPromoCode,prm.descriptionText as `descPromo`,
            pr.descriptionText as `descProd` , ppd.descriptionText as `descPeriod`, ppd.`months` as `periodInMonths`,
            pr.priceExtranet, pr.enabledExtranet, pr.isPlanPrice, pr.visibleWeb
        ";
    }

    /**
     * Select the product by the multiple key
     * and the promo -if any- to be applied.
     *
     * @param        $idProduct
     * @param        $idCountry
     * @param        $idPeriodPay
     * @param string $idPromoCode
     * @param null $userIdCountry
     *
     * @return bool
     * @throws CDbException
     */
    public function getProductByMixedId($idProduct, $idCountry, $idPeriodPay, $idPromoCode = "", $userIdCountry = null,
      $iProductPrice = null, $sCurrency = "")
    {

        $sqlFieldsPromocodes = ", null as `discount`, null as `idPromoCode`,null as `descPromo` ,
                                    null as `type`, null as `showValidationTypePromo`, null as `dateEndPromo` ";
        $sqlFromPromo = " ";
        $sqlWPromoCode = " ";
        if (isset($idPromoCode) && !empty($idPromoCode)) {
            $sqlFieldsPromocodes = ",IF(pr.idPeriodPay=30 AND prm.idPeriodPay IS NULL AND prm.idProduct IS NULL, NULL, prm.`value`) as `discount`,
								IF(pr.idPeriodPay=30 AND prm.idPeriodPay IS NULL AND prm.idProduct IS NULL, NULL, prm.`idPromoCode`)  as `idPromoCode`,
								IF(pr.idPeriodPay=30 AND prm.idPeriodPay IS NULL AND prm.idProduct IS NULL, NULL, prm.`descriptionText`) as `descPromo` ,
								IF(pr.idPeriodPay=30 AND prm.idPeriodPay IS NULL AND prm.idProduct IS NULL, NULL, prm.`type`) as `type` ,
							    IF(pr.idPeriodPay=30 AND prm.idPeriodPay IS NULL AND prm.idProduct IS NULL, NULL, prm.`showValidationType`) as `showValidationTypePromo`,
							    IF(pr.idPeriodPay=30 AND prm.idPeriodPay IS NULL AND prm.idProduct IS NULL, NULL, prm.dateEnd) as `dateEndPromo`";
            $sqlFromPromo = " LEFT JOIN products_promos prm ON
                            (   prm.`idPromoCode`=:IDPROMOCODE AND
                                prm.visibleWeb=1 AND
                                prm.enabled=1 AND
                                (prm.idProduct IS NULL OR prm.idProduct=pr.idProduct) AND
                                (prm.idPeriodPay IS NULL OR prm.idPeriodPay=pr.idPeriodPay)
                            )";
            $sqlWPromoCode = "  ";
        }

        $sql = " SELECT pr.idProduct, pr.idCountry, pr.idPeriodPay, pr.priceOfficialCry, pr.descriptionText as `descProd`, pr.isPlanPrice, pr.visibleWeb,
                        c.officialIdCurrency,c.ABAIdCurrency, ppd.descriptionText as `descPeriod`, ppd.`months` as `periodInMonths`
                         " . $sqlFieldsPromocodes . "
                 FROM products_prices pr
                         LEFT JOIN prod_periodicity ppd ON pr.idPeriodPay=ppd.id
                         LEFT JOIN country c ON pr.idcountry=c.id
                        " . $sqlFromPromo . "
                WHERE pr.idProduct =:IDPRODUCT AND pr.idCountry= :IDCOUNTRY AND pr.idPeriodPay = :IDPERIODPAY AND
                       pr.enabled=1
                    " . $sqlWPromoCode . " ; ";
        /* 2014-12-18 This condition was removed : pr.visibleWeb=1 AND*/
        $paramsToSQL = array(":IDPRODUCT" => $idProduct, ":IDCOUNTRY" => $idCountry, ":IDPERIODPAY" => $idPeriodPay);
        if (isset($idPromoCode) && !empty($idPromoCode)) {
            $paramsToSQL[":IDPROMOCODE"] = $idPromoCode;
        }

        $dataReader = $this->querySQL($sql, $paramsToSQL);

        if (($row = $dataReader->read()) !== false) {

            //
            //#abawebapps-158
            $iProductPriceOriginal = $row["priceOfficialCry"];

            //#5739
            if (is_numeric($iProductPrice)) {
                $row["priceOfficialCry"] = $iProductPrice;
            }

            //
            //#abawebapps-158
            if(trim($sCurrency) <> "") {
                $row["officialIdCurrency"] = $sCurrency;
                $row["ABAIdCurrency"] = $sCurrency;
            }

            $this->fillDataColsToProperties($row);

            if (is_null($userIdCountry)) {
                $userIdCountry = Yii::app()->user->getCountryId();
                if (is_null($userIdCountry) || $userIdCountry == 0) {
                    HeLogger::sendLog(
                        "User without country Id",
                        HeLogger::IT_BUGS,
                        HeLogger::CRITICAL,
                        "User with no Id Country has reached the Payment page, we automatically assigned the ".
                        "ABA country, but should not reach here without country set. " .
                        "Please follow the lead, check user_log_history."
                    );
                    $userIdCountry = Yii::app()->config->get("ABACountryId");
                }
            }

            $countryOfUser = new AbaCountry($userIdCountry);
            if (!$countryOfUser) {
                throw new CDbException(" ABA: Country of the user was not found or ocurred an error while loading. ");
            }

            //
            //#abawebapps-158
            $officialUserIdCurrency = $countryOfUser->officialIdCurrency;

            if(trim($sCurrency) <> "") {
                $officialUserIdCurrency = $sCurrency;
            }

            $oCcyTransaction = new AbaCurrency($row["ABAIdCurrency"]);

            $this->ccyTransactionSymbol = $oCcyTransaction->getSymbol(); // Currency in which the transaction will be made

            $this->packageTitleKey = Yii::t('mainApp', $row["descProd"]);

            if (!$this->setFinalPriceAndDiscount(
                $row["officialIdCurrency"],
                $row["priceOfficialCry"],
                $row["type"],
                $row["discount"],
                $officialUserIdCurrency,
                $row["showValidationTypePromo"]
            )
            ) {
                throw new CDbException(" ABA: Setting prices and discounts generated an error. ");
            }

            //
            //#abawebapps-158
            if(self::isAdyenHppProductPrice($iProductPrice, $sCurrency)) {
                $this->setAlternativeFinalPrice($iProductPriceOriginal, $row["priceOfficialCry"], $row["type"]);
            }

            // We calculate what the user would be paying monthly, so he can imagine the real discount is being applied:
            $moPeriod = new ProdPeriodicity();
            $this->priceMonthly = $this->finalPrice;
            $auxMonthly = $this->priceMonthly;
            $this->priceMonthlyOriginalTrans = $this->finalPrice;
            $auxMonthlyOriginalTrans = $this->priceMonthlyOriginalTrans;
            if ($moPeriod->getProdPeriodById($this->idPeriodPay)) {

                if (intval($moPeriod->months) > 0) {
                    $auxMonthly = strval($this->finalPrice / $moPeriod->months);
                    $auxMonthlyOriginalTrans = strval($this->originalPriceTransaction / $moPeriod->months);
                } else {
                    $auxMonthly = strval($this->finalPrice / $moPeriod->days);
                    $auxMonthlyOriginalTrans = strval($this->originalPriceTransaction / $moPeriod->days);
                }
                $this->priceMonthly = floatval(substr($auxMonthly, 0, (strpos($auxMonthly, ".") + 3)));
                $this->priceMonthlyOriginalTrans = floatval(
                    substr($auxMonthlyOriginalTrans, 0, (strpos($auxMonthlyOriginalTrans, ".") + 3))
                );
            }

            return true;
        }

        return false;
    }


    /**
     * @param $iProductPrice
     * @param $sCurrency
     *
     * @return bool
     */
    public static function isAdyenHppProductPrice($iProductPrice, $sCurrency) {
        if(is_numeric($iProductPrice) AND trim($sCurrency) <> "") {
            return true;
        }
        return false;
    }


    /**
     * @param $idProduct
     * @param int $enabled
     *
     * @return bool|ProductPrice
     */
    public function getProductById($idProduct, $enabled = 1)
    {
        $wEnabled = "";
        if ($enabled) {
            $wEnabled = " AND pr.enabled=" . $enabled;
        }
        $sql = " SELECT pr.* FROM products_prices pr WHERE pr.idProduct = :IDPRODUCT " . $wEnabled . " ";
        $paramsToSQL = array(":IDPRODUCT" => $idProduct);
        $dataReader = $this->querySQL($sql, $paramsToSQL);
        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }

        return false;
    }


    /**
     * Return the Product for a periodicity and a country and a UserType. If 2 are encountered then the firs one
     * is returned.
     * @param     $idCountry
     * @param     $idPeriodPay
     * @param int $userType
     *
     * @return bool|ProductPrice
     */
    public function getProductByCountryIdPeriodId($idCountry, $idPeriodPay, $userType = 2, $enabledExtranet = false)
    {
        $sql = " SELECT pr.*
                    FROM products_prices pr
                    WHERE pr.idPeriodPay=$idPeriodPay AND
                          pr.idCountry IN (" . $idCountry . ", " . Yii::app()->config->get("ABACountryId") . ") AND
                          pr.enabled=1 AND
                          pr.userType=$userType
        ";
        if ($enabledExtranet) {
            $sql .= "
                    AND pr.enabledExtranet=1
            ";
        }
        $sql .= "
                    ORDER BY pr.idCountry ASC
                    LIMIT 1 ";

        $dataReader = $this->querySQL($sql);

        if (($row = $dataReader->read()) !== false) {
            $this->fillDataColsToProperties($row);
            return $this;
        }

        return false;
    }

    /**
     * @param $originalPrice
     * @param $discountType
     * @param $discountValue
     *
     * @return bool
     * @throws CDbException
     */
    private function validPrices($originalPrice, &$discountType, &$discountValue)
    {
        $valid = true;
        if ($originalPrice < 0) {
            $valid = false;
            throw new CDbException(" Price can't be negative ");
        }
        if ($discountType == AMOUNT && $discountValue >= $originalPrice) {
            $valid = false;
            $discountValue = 0;
            $discountType = null;
        }
        if ($discountValue < 0) {
            $valid = false;
            $discountValue = 0;
            $discountType = null;
        }

        return $valid;
    }

    /**
     * @param String $fromCurrency
     * @param Float $originalPrice
     * @param null $discountType
     * @param int $discountValue
     * @param string $toCurrency
     * @param integer $showValidationTypePromo
     *
     * @return bool
     * @throws CException
     */
    private function setFinalPriceAndDiscount(
        $fromCurrency,
        $originalPrice,
        $discountType = null,
        $discountValue = 0,
        $toCurrency = "",
        $showValidationTypePromo = null
    ) {
        // General obvious validations on prices, RULES OF INTEGRITY:
        // we cancel discounts or even throw an exception
        if (!$this->validPrices($originalPrice, $discountType, $discountValue)) {
            return false;
        }
        $displayOriginalPrice = $originalPrice;
        $displayDiscountValue = $discountValue;

        $aComputedValues = array();
        switch ($discountType) {
            case PERCENTAGE:
                $aComputedValues = $this->getCalcPercentageDiscount($displayOriginalPrice, $displayDiscountValue);
                break;
            case AMOUNT:
                $aComputedValues = $this->getCalcPercentageDiscount($displayOriginalPrice, $displayDiscountValue);
                break;
            case FINALPRICE:
                $aComputedValues = $this->getCalcFinalDiscount($displayOriginalPrice, $displayDiscountValue);
                break;
            default:
                $aComputedValues = array(
                    "finalPrice" => $displayOriginalPrice,
                    "amountDiscounted" => 0.00,
                    "discountTitle" => ""
                );
                break;
        }

        switch ($showValidationTypePromo) {
            case 'A':
                $this->txtShowValidationTypePromo = $this->dateEndPromo;
                break;
            case 'C':
                $this->txtShowValidationTypePromo = HeDate::getDateAdd(HeDate::todaySQL(false), 3, 0);
                break;
            case 'D':
                $this->txtShowValidationTypePromo = HeDate::getDateAdd(HeDate::todaySQL(false), 1, 0);
                break;
            case 'B':
                $this->txtShowValidationTypePromo = "";
                break;
            default:
                $this->txtShowValidationTypePromo = "";
                break;
        }

        // IF THERE IS NEED TO CONVERT CURRENCIES THIS SHOULD BE
        // THE ONLY PLACE IS DONE FOR PRODUCTS ACROSS
        // ALL SOURCE CODE
        $oToCcy = new AbaCurrency($fromCurrency);
        $displayFinalPrice = $aComputedValues["finalPrice"];
        if ($toCurrency !== "" && $fromCurrency != $toCurrency) {
            /* We have to convert rates for all prices
            We assume that each price is on the price of the country of the product, also the promos price.
            */
            $oFromCcy = new AbaCurrency($fromCurrency);
            $displayOriginalPrice = $oFromCcy->getConversionAmountTo($displayOriginalPrice, $toCurrency);
            $displayFinalPrice = $oFromCcy->getConversionAmountTo($aComputedValues["finalPrice"], $toCurrency);
            if ($discountType == 'AMOUNT' || $discountType == 'FINALPRICE') {
                $aComputedValues["amountDiscounted"] = $oFromCcy->getConversionAmountTo(
                    $aComputedValues["amountDiscounted"],
                    $toCurrency
                );
            }
            $oToCcy = null;
            $oToCcy = new AbaCurrency($toCurrency);
        }

        $this->originalPrice = HeMixed::getRoundAmount($displayOriginalPrice);
        $this->amountDiscounted = HeMixed::getRoundAmount($aComputedValues["amountDiscounted"]);
        $this->finalPrice = HeMixed::getRoundAmount($displayFinalPrice);
        $this->discountTitle = $aComputedValues["discountTitle"];
        $this->ccyDisplaySymbol = $oToCcy->getSymbol();
        if ($fromCurrency == $this->ABAIdCurrency) {
            $this->originalPriceTransaction = HeMixed::getRoundAmount($originalPrice);
            $this->amountDiscountedTransaction = HeMixed::getRoundAmount($aComputedValues["amountDiscounted"]);
            $this->finalPriceTransaction = HeMixed::getRoundAmount($aComputedValues["finalPrice"]);
        } else {
            $this->originalPriceTransaction = $oToCcy->getConversionAmountTo($originalPrice, $this->ABAIdCurrency);
            $this->amountDiscountedTransaction = $oToCcy->getConversionAmountTo(
                $aComputedValues["amountDiscounted"],
                $this->ABAIdCurrency
            );
            $this->finalPriceTransaction = $oToCcy->getConversionAmountTo(
                $aComputedValues["finalPrice"],
                $this->ABAIdCurrency
            );
        }

        return true;
    }


    /**
     * @param $iOriginalPrice
     * @param $iAlternativeOriginalPrice
     * @param $discountType
     *
     * @return bool
     */
    public function setAlternativeFinalPrice($iOriginalPrice, $iAlternativeOriginalPrice, $discountType) {

        if($discountType == FINALPRICE AND $this->idPeriodPay <> ProdPeriodicity::MONTH_1) {

            $iFinalPrice = ($iAlternativeOriginalPrice * $this->finalPrice) / $iOriginalPrice;

            //
            $iAmountDiscounted = $iAlternativeOriginalPrice - $iFinalPrice;
            $this->amountDiscounted = HeMixed::getRoundAmount($iAmountDiscounted);

            //
            $this->finalPrice = HeMixed::getRoundAmount($iFinalPrice);

            $this->amountDiscountedTransaction = HeMixed::getRoundAmount($this->amountDiscounted);
            $this->finalPriceTransaction = HeMixed::getRoundAmount($this->finalPrice);
        }

        return true;
    }


    /**
     * @param $originalPrice
     * @param $discountValue
     *
     * @return array
     */
    private function getCalcPercentageDiscount($originalPrice, $discountValue)
    {
        $amountDiscounted = ($discountValue * $originalPrice) / 100;
        $finalPrice = ($originalPrice - $amountDiscounted);
        $amountDiscounted = $amountDiscounted;
        $discountValue = HeMixed::getRoundAmount($discountValue);

        return array(
            "finalPrice" => $finalPrice,
            "amountDiscounted" => $amountDiscounted,
            "discountTitle" => $discountValue . "%"
        );
    }

    /**
     * @param $originalPrice
     * @param $discountValue
     *
     * @return array
     */
    private function getCalcAmountDiscount($originalPrice, $discountValue)
    {
        $amountDiscounted = $discountValue;
        $finalPrice = ($originalPrice - $amountDiscounted);
        $discountValue = HeMixed::getRoundAmount($discountValue);

        return array(
            "finalPrice" => $finalPrice,
            "amountDiscounted" => $amountDiscounted,
            "discountTitle" => $discountValue
        );
    }

    /**
     * @param $originalPrice
     * @param $discountValue
     *
     * @return array
     */
    private function getCalcFinalDiscount($originalPrice, $discountValue)
    {
        $amountDiscounted = $discountValue;
        $finalPrice = $amountDiscounted;
        $amountDiscounted = $originalPrice - $finalPrice;
        $percInverse = HeMixed::getRoundAmount(($amountDiscounted * 100) / $originalPrice);

        return array(
            "finalPrice" => $finalPrice,
            "amountDiscounted" => $amountDiscounted,
            "discountTitle" => $finalPrice
        );
    }

    public function getFinalPrice()
    {
        return $this->finalPrice;
    }

    public function getOriginalPrice()
    {
        return $this->originalPrice;
    }

    public function getAmountDiscounted()
    {
        return $this->amountDiscounted;
    }

    public function getPackageTitleKey()
    {
        return $this->packageTitleKey;
    }

    public function getDiscountTitle()
    {
        return $this->discountTitle;
    }

    public function getCcyTransactionSymbol()
    {
        return $this->ccyTransactionSymbol;
    }

    public function getCcyDisplaySymbol()
    {
        return $this->ccyDisplaySymbol;
    }

    public function getFinalPriceTransaction()
    {
        return $this->finalPriceTransaction;
    }

    public function getOriginalPriceTransaction()
    {
        return $this->originalPriceTransaction;
    }

    public function getAmountDiscountedTransaction()
    {
        return $this->amountDiscountedTransaction;
    }

    public function getPriceMonthly()
    {
        return $this->priceMonthly;
    }

    public function getTxtShowValidationTypePromo()
    {
        return $this->txtShowValidationTypePromo;
    }

    public function getPriceMonthlyOriginalTrans()
    {
        return $this->priceMonthlyOriginalTrans;
    }

    /**
     * @param $currency
     *
     * @return string
     */
    public function getAppStoreCurrency($currency)
    {
        if (trim($currency) != '') {
            return $currency;
        }
        $moCountry = new AbaCountry($this->idCountry);
        return $moCountry->ABAIdCurrency;
    }

    /**
     * @param $idProduct
     *
     * @return bool
     */
    public function isPlanProduct($idProduct)
    {
        if ($this->getProductById($idProduct)) {
            if ($this->isPlanPrice == 1) {
                return true;
            }
        }
        return false;
    }


    /**
     * @param AbaUser $moUser
     * @param string $idProduct
     *
     * @return float|int
     */
    public function getDiscountPlan(AbaUser $moUser, $idProduct = "")
    {

        $iDiscount = 0;

        if (!$this->isPlanProduct($idProduct)) {
            return $iDiscount;
        }

        $userExpirationDate1 = HeDate::europeanToSQL($moUser->expirationDate);

        $notPaymentIds = array();

        //
        // FIRST Payment
        //
        $moPay = new Payment();
        $moSuccessPay1 = $moPay->getLastValidSuccessPaymentNoFree($moUser->getId(), null);

        if ($moSuccessPay1) {

            $bIsRefundedSuccessPayment1 = $moSuccessPay1->isRefundedSuccessPayment();

            $oProdPeriodicity = new ProdPeriodicity();
            $iMonthPeriod = $oProdPeriodicity->getPeriodicityMonth($moSuccessPay1->idPeriodPay);

            $userExpirationDate2 = HeDate::getDateAdd($userExpirationDate1, 0, -($iMonthPeriod)); // " 00:00:00"

            //
            // SECOND Payment
            //
            if (HeDate::isGreaterThanToday($userExpirationDate2, true)) {

                if (!$bIsRefundedSuccessPayment1) {
                    $iDiscount += $moSuccessPay1->amountPrice;
                }

                $notPaymentIds[$moSuccessPay1->id] = $moSuccessPay1->id;

                $moPay = new Payment();
                $moSuccessPay2 = $moPay->getLastValidSuccessPaymentNoFree($moUser->getId(), null, $notPaymentIds);

                if ($moSuccessPay2) {

                    $bIsRefundedSuccessPayment2 = $moSuccessPay2->isRefundedSuccessPayment();

                    $oProdPeriodicity = new ProdPeriodicity();
                    $iMonthPeriod2 = $oProdPeriodicity->getPeriodicityMonth($moSuccessPay2->idPeriodPay);

                    $userExpirationDate3 = HeDate::getDateAdd(
                        $userExpirationDate2,
                        0,
                        -($iMonthPeriod2)
                    ); // " 00:00:00"

                    //
                    // THIRD Payment
                    //
                    if (HeDate::isGreaterThanToday($userExpirationDate3, true)) {

                        if (!$bIsRefundedSuccessPayment2) {
                            $iDiscount += $moSuccessPay2->amountPrice;
                        }

                        /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **
                         * if only 2 payments
                         */
                        $notPaymentIds[$moSuccessPay2->id] = $moSuccessPay2->id;

                        $moPay = new Payment();
                        $moSuccessPay3 = $moPay->getLastValidSuccessPaymentNoFree(
                            $moUser->getId(),
                            null,
                            $notPaymentIds
                        );

                        if ($moSuccessPay3) {

                            $bIsRefundedSuccessPayment3 = $moSuccessPay3->isRefundedSuccessPayment();

                            $oProdPeriodicity = new ProdPeriodicity();
                            $iMonthPeriod3 = $oProdPeriodicity->getPeriodicityMonth($moSuccessPay3->idPeriodPay);

                            $userExpirationDate4 = HeDate::getDateAdd(
                                $userExpirationDate3,
                                0,
                                -($iMonthPeriod3)
                            ); // " 00:00:00"

                            //
                            // FOURTH Payment
                            //
                            if (HeDate::isGreaterThanToday($userExpirationDate4, true)) {
                                if (!$bIsRefundedSuccessPayment3) {
                                    $iDiscount += $moSuccessPay3->amountPrice;
                                }

                                /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **
                                 * if only 3 payments
                                 */
                                $notPaymentIds[$moSuccessPay3->id] = $moSuccessPay3->id;

                                $moPay = new Payment();
                                $moSuccessPay4 = $moPay->getLastValidSuccessPaymentNoFree(
                                    $moUser->getId(),
                                    null,
                                    $notPaymentIds
                                );

                                if ($moSuccessPay4) {

                                    $bIsRefundedSuccessPayment4 = $moSuccessPay4->isRefundedSuccessPayment();

                                    //
                                    // ! Last Payment !
                                    //
                                    if (!$bIsRefundedSuccessPayment4) {

                                        $oProdPeriodicity = new ProdPeriodicity();
                                        $iMonthPeriod4 = $oProdPeriodicity->getPeriodicityMonth(
                                            $moSuccessPay4->idPeriodPay
                                        );

                                        $userExpirationDate5 = HeDate::getDateAdd(
                                            $userExpirationDate4,
                                            0,
                                            -($iMonthPeriod4)
                                        ); // " 00:00:00"

                                        //
                                        // otro pago
                                        if (HeDate::isGreaterThanToday($userExpirationDate5, true)) {
                                            if (!$bIsRefundedSuccessPayment4) {
                                                $iDiscount += $moSuccessPay4->amountPrice;
                                            }
                                        } else {
                                            if (!$bIsRefundedSuccessPayment4) {
                                                $iDiscount += $this->getPaymentDiscount($moSuccessPay4, $iMonthPeriod4);
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (!$bIsRefundedSuccessPayment3) {
                                    $iDiscount += $this->getPaymentDiscount($moSuccessPay3, $iMonthPeriod3);
                                }
                            }
                        }
                    } else {
                        if (!$bIsRefundedSuccessPayment2) {
                            $iDiscount += $this->getPaymentDiscount($moSuccessPay2, $iMonthPeriod2);
                        }
                    }
                }
            } else {
                if (!$bIsRefundedSuccessPayment1) {
                    $iDiscount += $this->getPaymentDiscount($moSuccessPay1, $iMonthPeriod);
                }
            }
        }

        $iDiscount = round($iDiscount, 2);

        return $iDiscount;
    }

    /**
     * @param Payment $oPayment
     * @param null $iMonthPeriod
     *
     * @return float
     */
    public function getPaymentDiscount(Payment $oPayment, $iMonthPeriod = null)
    {

        $dateIni = $oPayment->dateStartTransaction;
        $stDateIni = explode(" ", $dateIni);
        $dateIni = $stDateIni[0] . " 00:00:00";

        if (!is_numeric($iMonthPeriod)) {
            $oProdPeriodicity = new ProdPeriodicity();
            $iMonthPeriod = $oProdPeriodicity->getPeriodicityMonth($oPayment->idPeriodPay);
        }

        //
        //Nº days between start & end
        //
        $dateFi = HeDate::getDateAdd($dateIni, 0, $iMonthPeriod) . " 00:00:00";
        $dateDiff = HeDate::getDifferenceDateSQL($dateIni, $dateFi, (3600 * 24));
        $dateDiff = round($dateDiff);

        //
        //Nº days between now & end
        //
        $dateIniNew = HeDate::today() . " 00:00:00";
        $dateDiffNew = HeDate::getDifferenceDateSQL($dateIniNew, $dateFi, (3600 * 24));

        //
        // % Discount
        //
        $iPercent = $dateDiffNew * 100 / $dateDiff;

        //
        // Total Discount
        //
        $iDiscount = ($iPercent * $oPayment->amountPrice) / 100;

        return $iDiscount;
    }

    /**
     * @param $iDiscount
     * @param array $productPrice
     *
     * @return array
     */
    public function setPaymentDiscount($iDiscount, $productPrice = array())
    {

        if ($iDiscount >= $productPrice["finalPrice"]) {
            $productPrice['amountDiscounted'] = $productPrice["originalPrice"];
            $productPrice['finalPrice'] = 0;
        } else {
            $productPrice['amountDiscounted'] += $iDiscount;
            $productPrice['finalPrice'] -= $iDiscount;
        }

        return $productPrice;
    }

    /**
     * @param string $idPromoCode
     * @param int $isPlanPrice
     * @param string $idPromoType
     *
     * @return string
     */
    public static function adjustPromoCode($idPromoCode='', $isPlanPrice=0, $idPromoType='') {

        switch($idPromoType) {
            case ProductPromo::PROMO_TYPE_GENERAL:
                if($isPlanPrice == 1) {
                    $idPromoCode = '';
                }
                break;
            case ProductPromo::PROMO_TYPE_FAMILYPLAN:
                if($isPlanPrice != 1) {
//                if($isPlanPrice == 0) {
                    $idPromoCode = '';
                }
                break;
            case ProductPromo::PROMO_TYPE_ALL:
            default:
                break;
        }

        return $idPromoCode;
    }

}
