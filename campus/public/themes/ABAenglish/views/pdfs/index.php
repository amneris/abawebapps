<?php
$pdf = Yii::createComponent('application.extensions.mpdf.mpdf');
    $imagePath = HeMixed::getUrlPathImg("header-PDF-certificados.png");
    $imageTag = CHtml::image($imagePath, "Logo ABA English", array("style" => "margin-left: auto; margin-right: auto; margin-top: 0;", "width" => "7.91cm", "height" => "4.44cm"));
    $imagePathFirma = HeMixed::getUrlPathImg("firmaSevero.png");
    $imageTagFirma = CHtml::image($imagePathFirma, "Firma Principal", array("style" => "width: 3.89cm; height: 0.95cm;", "width" => "3.89cm", "height" => "0.95cm"));
    $imagePathSince = HeMixed::getUrlPathImg("SinceABAEnglish.png");
    $imageTagSince = CHtml::image($imagePathSince, "Since ABA English in 1970", array("style" => "width: 21.01cm; height: 0.25cm;", "width" => "21.01cm", "height" => "0.25cm"));
$html = '
    <div style="text-align: center;">
        '.$imageTag.'
        <div style="margin-top: 1.13cm; width: 100%; text-align: center; height: 0.41cm; line-height: 0.41cm;">
            '.$imageTagSince.'
            <div style="font-family: \'museoslab700\'; font-size: 3.53mm; color: #4c565c; margin-top: -0.41cm;">Since 1970</div>
        </div>
        <div style="text-align: center; font-family: \'museoslab900\'; color: #'.$color.'; font-size: 6.35mm; margin-top: 2.24cm;">
            '.$finalStringLevel.'
        </div>
        <div style="text-align: center; font-family: \'museoslab700\'; color: #4c565c; font-size: 6.35mm; margin-top: 1.76cm;">
            This is to certify that:
        </div>
        <div style="text-align: center; font-family: arial; color: #4c565c; font-size: 6.35mm; margin-top: 1.64cm;">
            '.$name.' '.$surnames.'
        </div>
        <div style="font-family: \'museoslab500ita\'; text-align: center; color: #4c565c; font-size: 4.23mm; margin-top: 1.68cm;">
            has successfully completed all requirements and criteria for the 
        </div>
        <div style="font-family: \'museoslab500ita\'; text-align: center; color: #4c565c; font-size: 4.23mm;">
            <span style="font-family: \'museoslab700\'; color: #'.$color.'; font-size: 4.23mm;">'.$finalStringLevel.'</span> level of our course ('.$hours.' hours). This level includes
        </div>
        <div style="font-family: \'museoslab500ita\'; text-align: center; color: #4c565c; font-size: 4.23mm;">
            the study of structures, functional spoken English and '.$words.' words.
        </div>
        <div style="margin-top: 2.17cm; height: 0.95cm; line-height: 0.95cm; text-align: center; padding-left: 3.06cm; padding-right: 2.68cm;">
            <div style="text-align: left; font-family: \'museoslab300\'; font-size: 3.18mm; color: #4c565c;">Barcelona, '.date('jS F Y').'</div>
            <div style="text-align: right; margin-top: -0.95cm;">'.$imageTagFirma.'</div>
        </div>
        <div style="font-family: \'museoslab300\'; font-size: 3.18mm; color: #4c565c; text-align: right; padding-right: 4.03cm; margin-top: 0;">
            Principal
        </div>
        <div style="font-family: \'museoslab500ita\'; font-size: 3.18mm; color: #a5abae; padding-left: 3.08cm; text-align: left; margin-top: 2.02cm;">
            *Common European Framework of Reference for Languages (CEFR)
        </div>
        <div style="background-color: #4c565c; text-align: left; padding-left: 1.32cm; height: 4.33cm; margin-top: 0.51cm; margin-bottom: 0;">
            <div style="margin-top: 0.93cm; font-family: \'museoslab700\'; font-size: 3.53mm; color: #fff;">
                Assessment of language skills:
            </div>
            <div style="margin-top: 0.54cm;">
                <span style="font-family: \'museoslab700\'; color: #fff; font-size: 3.18mm;">
                    Listening: 
                </span>
                <span style="font-family: \'museoslab300\'; font-size: 3.18mm; color: #fff;">
                    '.$footerString[0].'
                </span>
            </div>
            <div style="margin-top: 0;">
                <span style="font-family: \'museoslab700\'; color: #fff; font-size: 3.18mm;">
                    Speaking: 
                </span>
                <span style="font-family: \'museoslab300\'; font-size: 3.18mm; color: #fff;">
                    '.$footerString[1].'
                </span>
            </div>
            <div style="margin-top: 0;">
                <span style="font-family: \'museoslab700\'; color: #fff; font-size: 3.18mm;">
                    Writing: 
                </span>
                <span style="font-family: \'museoslab300\'; font-size: 3.18mm; color: #fff;">
                    '.$footerString[2].'
                </span>
            </div>
            <div style="margin-top: 0;">
                <span style="font-family: \'museoslab700\'; color: #fff; font-size: 3.18mm;">
                    Reading: 
                </span>
                <span style="font-family: \'museoslab300\'; font-size: 3.18mm; color: #fff;">
                    '.$footerString[3].'
                </span>
            </div>
        </div>
        <div style="background-color: #2d2b33; height: 0.98cm; text-align: center; line-height: 0.98cm; margin: 0; padding: 0;">
            <a href="http://'.Yii::app()->config->get("URL_DOMAIN_WEB").'/" style="text-decoration: none; color: #fff; font-size: 3.18mm; font-family: \'museoslab500ita\';">'.Yii::app()->config->get("URL_DOMAIN_WEB").'</a>
        </div>
    </div>
';

$mpdf = new mPDF('utf-8', 'A4','','',0,0,0,0,0,0);
$mpdf->useOnlyCoreFonts = false;
$mpdf->useSubstitutions  = true;
$mpdf->SetAutoFont(AUTOFONT_ALL);
$mpdf->AddFont("museoslab900");
$mpdf->AddFont("museoslab700");
$mpdf->AddFont("museoslab500ita");
$mpdf->AddFont("museoslab300");
$mpdf->showImageErrors = true;
$mpdf->WriteHTML($html);
$fileName = trim($finalStringLevel).$name.'-'.$surnames;
$mpdf->Output(str_replace(array(" ","*","--","---","----"), "-", $fileName).'.pdf','D');
exit;
?>