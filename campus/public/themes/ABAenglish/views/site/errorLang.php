<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <meta name="language" content="<?php echo Yii::app()->language; ?>" />
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/assets/out/application.css'); ?>
    <title>
        <?php echo CHtml::encode($this->pageTitle); ?>
    </title>
</head>
<body class="ab-error-page">
<section class="ab-center-box">
    <div class="text-center">
        <img class="img-responsive" src="<?= Yii::app()->theme->baseUrl ?>/assets/img/logo_water.png">
        <h1>Oops, campus site is not available for your language settings.</h1>
        <p>Please use the mobile application instead.</p>
        <a class="ab-error-page-btn" href="/site/logout">Logout</a>
    </div>
</section>
</body>
</html>
