<div id="linkedinLogin">
    <div class="containerForm">
        <div class="login-info">
            <?php
            if($inAction == 'register') {
                echo Yii::t('mainApp', 'fbLogin_existingUser', array('{fbUsername}' => $inUsername));
            }
            else {
                echo Yii::t('mainApp', 'fbLogin_confirmEmail', array('{fbUsername}' => $inUsername));
            }
            ?>
        </div>

        <?php $formLinkedin = $this->beginWidget('CActiveForm', array(
            'id' => 'login-form-in',
            'action'=> $this->createURL('/site/inlogin'),
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        ));?>

        <?php if($msgConfirmation != "") { ?>
            <div class="red">
                <?= Yii::t('mainApp', $msgConfirmation); ?>
            </div>
        <?php } ?>
        <div class="containerInputs">

            <div class="form-group">
                <?=
                $formLinkedin->textField(
                    $modelLinkedin, 'inEmail',
                    array('class' => 'form-control icon-envelope', 'placeholder' => 'Email')
                );
                ?>
                <?= $formLinkedin->error($modelLinkedin, 'inEmail'); ?>
            </div>
            <div class="clear"></div>

            <?php if($inAction == 'register') { ?>
                <div class="form-group">
                    <?=
                    $formLinkedin->passwordField(
                        $modelLinkedin, 'inPassword',
                        array('class' => 'form-control icon-key', 'placeholder' => 'Contraseña')
                    );
                    ?>
                    <?= $formLinkedin->error($modelLinkedin, 'inPassword'); ?>
                </div>
            <?php } ?>

            <div class="buttons">
                <button type="button" id="btnInSubmitReg" onclick="abaIN.sendUser();" class="buttonslogins btn-linkedin">
                    <div class="iconos iconosin">
                        <i class="fa fa-linkedin"></i>
                    </div>
                    <?= Yii::t('mainApp', 'Continuar') ?>
                </button>
            </div>
            <div class="clear"></div>

            <div class="err">
                <span style="color: #cc0000;"></span>
                <div style="clear: both;"></div>
            </div>

            <input name="inAction" id="inAction" value="<?php echo $inAction; ?>" type="hidden">
<!--            <input name="accessToken" id="accessToken" value="--><?php //echo $fbAccessToken; ?><!--" type="hidden">-->
            <input name="inEmailLinkedin" id="inEmailLinkedin" value="<?php echo $inEmailLinkedin; ?>" type="hidden">

            <?php $this->endWidget(); ?>

        </div>
    </div>
</div>