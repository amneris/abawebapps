<script type="text/javascript" src="//platform.linkedin.com/in.js">
    api_key:  77l1t5hni37pnz
    authorize: false
    lang: es_ES
</script>

<script type="text/javascript">
    var resultfinal;
    // Setup an event listener to make an API call once auth is complete
    function onLinkedInLoad() {
        IN.User.authorize(function(){
            getProfileData();
        });
        //IN.Event.on(IN, "auth", getProfileData);
    }

    // Handle the successful return from the API call
    function onSuccess(data) {
        console.log(data);

        resultfinal =data.values[0]

        abaIN.checkUser(resultfinal);

    }

    // Handle an error response from the API call
    function onError(error) {
        console.log(error);
    }

    // Use the API call wrapper to request the member's basic profile data
    function getProfileData() {
        IN.API.Profile("me").fields("id", "first-name", "last-name", "email-address").result(onSuccess).error(onError);
    }

    $( document ).ready(function() {
        $(".linkedinLogin").on('click',function(e) {

            onLinkedInLoad();

            //$(this).empty();

            //$(this).prop( "disabled", true );
           // $(this).html("<i class='fa fa-spinner fa-spin'></i>");

        });

        $('#cancelSuscriptionLinkedinButton').on('click',function(e) {

            abaIN.setAction('');

            $.fancybox.close();
            //$('.linkedinLogin').empty();
            //$('.linkedinLogin').html('<div class="iconos iconosin"><i class="fa fa-linkedin"></i></div> Linkedin');


            $("#INEmail").val('');
            $("#INPass").val('');
        });

    });

    var abaIN = {
        t :         '',
        e :         '',
        a :         '',
        setEmail :  function (e) { this.e = e; },
        getEmail :  function () { return this.e; },
        setAction : function (a) { this.a = a; },
        getAction : function () { return this.a; },
        isset :     function () {
            try {
                var a = arguments, l = a.length, i = 0, undef;
                if (l === 0) {
                    throw new Error('Empty isset');
                }
                while (i !== l) {
                    if (a[i] === undef || a[i] === null) {
                        return false;
                    }
                    i++;
                }
                return true;
            }
            catch (err){ return false; }
        },
        blockAction : function (a, e){
            if(a) {
                $('#' + e).attr({"disabled":"disabled"});
                $('#' + e).css({"opacity":"0.4", "filter":"alpha(opacity=40)"});
            }
            else {
                $('#' + e).removeAttr("disabled");
                $('#' + e).css({"opacity":"1", "filter":"alpha(opacity=100)"});
            }
        },
        checkUser : function (resultado){
            try {
                abaIN.setAction('exists');

                var url_action =    "<?= $this->createURL('/site/inLogin') ?>";
                var data =          {};

                data['INAction']    =   abaIN.getAction();
                data['result'] =   resultado;

                $.ajax({
                    type: 'POST',
                    url: url_action,
                    data: data,
                    beforeSend: function(){
                        g_utils.showLoader();
                    },
                    success: function(response) {
                        try {
                            var response = $.parseJSON(response);
                            g_utils.showLoader();
                            window.location.href = response.redirect;
                            return;
                        }
                        catch(e){
                            abaIN.showInContainer(response);
                        }
                        abaIN.showInContainer(response);
                    }
                });
            }
            catch (err){
                abaIN.blockAction(false, 'btnFbSubmit');
                console.log(err);
            }
        },
        sendUser : function (){
            try {
                var url_action =    "<?= $this->createURL('/site/inlogin') ?>";
                var data =          {};
                data['result']    =   resultfinal;

                $('#login-form-in input').each(function() {
                    data[this.name] = this.value;
                });

                $.ajax({
                    type: 'POST',
                    url: url_action,
                    data: data,
                    beforeSend: function(){
                        g_utils.showLoader();
                    },
                    success: function(response) {
                        try {
                            var response = $.parseJSON(response);
                            window.location.href = response.redirect;
                            return;
                        }
                        catch(err) {
                            abaIN.showInContainer(response);
                        }
                    }
                });
            }
            catch (err) {
                abaIN.blockAction(false, 'btnFbSubmitReg');
            }
        },
        showInContainer : function(content) {
            $.fancybox( {
                content: content,
                closeBtn: true,
                helpers   : {
                    overlay : {closeClick: false} // prevents closing when clicking OUTSIDE fancybox
                }
            });

            return true;
        }
    };


</script>
