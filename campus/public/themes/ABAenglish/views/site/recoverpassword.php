<div class="password-container">
    <h1><?php echo Yii::t('mainApp', 'recoverPassword_title'); ?></h1>
    <?php $recoverForm=$this->beginWidget('CActiveForm', array(
      'id'=>'recoverpwd-form',
        'action' => $this->createURL('site/recoverpassword', array()),
        'enableClientValidation' => false,
        'clientOptions' => array('validateOnSubmit' => true),
        'focus'=>array($modelRecoverPwd, 'email'),

        )); ?>

    <?php
    $stFormErrors = $modelRecoverPwd->getErrors();
    $sEmailErrors = (isset($stFormErrors['email'][0]) ? trim(Yii::t('mainApp', 'recoverPassword_emptyemail')) : '');
    ?>

    <div class="form-group">
        <?php if (empty($msgConfirmation)): ?>
        <?php echo '<p class="sub-header">' . Yii::t('mainApp', 'recoverPassword_email') . '</p>'; ?>
        <?php endif; ?>
        <?php
        if (!empty($msgConfirmation)) {
            echo '<div class="alert alert-success" role="alert">'.$msgConfirmation.'</div>';
        } else if (!empty($msgError)) {
            if($sEmailErrors <> '') {
                echo '<div class="alert alert-error" role="alert">'.$sEmailErrors.'</div>';
            }
            else {
                echo '<div class="alert alert-error" role="alert">'.$msgError.'</div>';
            }
        }
        ?>
    </div>

    <?php if (empty($msgConfirmation)): ?>
    <div class="form-group">
        <?php echo $recoverForm->textField($modelRecoverPwd, 'email', array('placeholder' => Yii::t('mainApp', 'Introduzca el email'))); ?>
    </div>
    <?php endif; ?>

    <div class="form-group submit-block">
        <?php
        if (empty($msgConfirmation)) {
            echo CHtml::button('Recuperar contraseña', array('id' => 'btnRecoverPass', 'class' => 'btn', 'value' => Yii::t('mainApp', 'Recuperar')));
        }
        ?>
    </div>

    <?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
    $('form#recoverpwd-form').bind('keypress keydown keyup', function(e){
        if(e.keyCode == 13) {
            e.preventDefault();
        }
    });

    $('#btnRecoverPass').on("click", function(e){
        e.stopPropagation();
        e.preventDefault();
        $(this).attr('disabled', 'disabled');
        $('form#recoverpwd-form').submit();
    });
</script>

