<?php
/* @var DirectRegisterForm|CModel $directRegisterForm */
?>

<div class="container ab-payment-wrapper" id="page">

    <!-- START PAYMENT FORM -->
    <div class="div-payment-form">

        <?php echo $this->renderPartial('/payments/directPaymentSteps', array()); ?>

        <div class="row">
            <div class="col-sm-12">
                <h3 class="ab-payment-title"><?php echo Yii::t('mainApp', 'directpayment_account'); ?>:</h3>
            </div>
        </div>

        <div class="ab-payment-form with-bg ab-payment-form-js">
            <div class="row">
                <div class="col-md-7">
                    <div class="ab-payment-info" style="background: none transparent; margin-top:0;">
                            <section>
                                <h3 class="ab-payment-title"><?php echo Yii::t('mainApp',
                                      'directpayment_add'); ?></h3>

                                <?php
                                $actionFormDefault = "direct-payment";

                                /* @var CActiveForm $frmYiiPayMethod */
                                $frmYiiPayMethod = $this->beginWidget('CActiveForm', array(
                                  'id' => 'payment-form',
                                  'action' => $actionFormDefault,
                                  'enableClientValidation' => false,
                                  'clientOptions' => array(
                                    'validateOnSubmit' => true,
                                  ),
                                  'htmlOptions' => array(
                                    'class' => 'form-horizontal',
                                  ),
                                ));


                                $readOnly = "";
                                if ($bLogin) {
                                    $readOnly = "readonly";
                                }
                                ?>
                                <div class="form-group">
                                    <?php echo $frmYiiPayMethod->labelEx($directRegisterForm, 'name',
                                      array('class' => 'col-sm-5 control-label', 'style' => 'text-align:left')); ?>
                                    <div class="col-sm-7">
                                        <?php echo $frmYiiPayMethod->textField($directRegisterForm, 'name',
                                          array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                                        <?php echo $frmYiiPayMethod->error($directRegisterForm, 'name',
                                          array('class' => 'ab-form-error')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?php echo $frmYiiPayMethod->labelEx($directRegisterForm, 'surnames',
                                      array('class' => 'col-sm-5 control-label', 'style' => 'text-align:left')); ?>
                                    <div class="col-sm-7">
                                        <?php echo $frmYiiPayMethod->textField($directRegisterForm,
                                          'surnames',
                                          array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                                        <?php echo $frmYiiPayMethod->error($directRegisterForm, 'surnames',
                                          array('class' => 'ab-form-error')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?php echo $frmYiiPayMethod->labelEx($directRegisterForm, 'email',
                                      array('class' => 'col-sm-5 control-label', 'style' => 'text-align:left')); ?>
                                    <div class="col-sm-7">
                                        <?php echo $frmYiiPayMethod->textField($directRegisterForm, 'email',
                                          array(
                                            'class' => 'form-control',
                                            'autocomplete' => 'off',
                                            'readonly' => $readOnly
                                          )); ?>
                                        <?php echo $frmYiiPayMethod->error($directRegisterForm, 'email',
                                          array('class' => 'ab-form-error')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?php echo $frmYiiPayMethod->labelEx($directRegisterForm, 'password',
                                      array('class' => 'col-sm-5 control-label', 'style' => 'text-align:left')); ?>
                                    <div class="col-sm-7">
                                        <?php echo $frmYiiPayMethod->passwordField($directRegisterForm,
                                          'password',
                                          array(
                                            "class" => "form-control",
                                            'autocomplete' => 'off',
                                            'readonly' => $readOnly
                                          )); ?>
                                        <?php echo $frmYiiPayMethod->error($directRegisterForm, 'password',
                                          array('class' => 'ab-form-error')); ?>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-sm-offset-5 col-md-7">
                                            <a name="btnABA" id="btnABA"
                                               class="ab-btn ab-btn-blue ab-btn-big btn-payment-js"><?php
                                                echo Yii::t('mainApp', 'directpayment_createaccount') ?>
                                            </a>
                                            <?php
                                            $imagePath = HeMixed::getUrlPathImg("loading.gif");
                                            echo CHtml::image($imagePath, "Loading",
                                              array("id" => "loadingGif", 'class' => 'hidden ab-loader'));
                                            ?>
                                            <sub>
                                                <?php echo Yii::t('mainApp', 'directpayment-registrado'); ?>
                                                <a
                                                  href="<?php echo Yii::app()->createUrl("/site/index") ?>">
                                                    <?php echo Yii::t('mainApp', 'Entrar'); ?></a>
                                            </sub>
                                        </div>
                                    </div>
                                </div>
                                <input id="idProductPricePKSelected" type="hidden" name="productPricePKSelected"
                                       value="<?php echo $packageSelected; ?>"/>

                                <?php $this->endWidget(); ?>

                            </section>
                    </div>
                </div>
                <div class="col-md-5 mob-with-bg">
                    <div class="ab-payment-info">
                        <div class="row">
                            <div class="col-md-12 clearfix">
                                <section>
                                    <h3 class="ab-payment-title"><?php echo Yii::t('mainApp',
                                          'directpayment_title'); ?></h3>

                                    <ul class="ab-academia-explanation-list list-unstyled">
                                        <li style="margin-bottom: 15px;">
                                            <i class="fa fa-check icon-payment-check"></i>
                                            <?php echo Yii::t('mainApp',
                                              'directpayment_content1'); ?></li>
                                        <li style="margin-bottom: 15px;">
                                            <i class="fa fa-check icon-payment-check"></i>
                                            <?php echo Yii::t('mainApp',
                                              'directpayment_content2'); ?></li>
                                        <li style="margin-bottom: 15px;">
                                            <i class="fa fa-check icon-payment-check"></i>
                                            <?php echo Yii::t('mainApp',
                                              'directpayment_content3'); ?></li>
                                        <li style="margin-bottom: 15px;">
                                            <i class="fa fa-check icon-payment-check"></i>
                                            <?php echo Yii::t('mainApp',
                                              'directpayment_content4'); ?></li>
                                        <li style="margin-bottom: 15px;">
                                            <i class="fa fa-check icon-payment-check"></i>
                                            <?php echo Yii::t('mainApp',
                                              'directpayment_content5'); ?></li>
                                        <li style="margin-bottom: 15px;">
                                            <i class="fa fa-check icon-payment-check"></i>
                                            <?php echo Yii::t('mainApp',
                                              'directpayment_content6'); ?></li>
                                    </ul>

                                    <ul id="divIdPurchaseSummary" class="list-unstyled">

                                        <h4 class="ab-payment-title with-border">&nbsp;</h4>

                                        <?php if (isset($stProduct["originalPrice"]) AND $stProduct["originalPrice"] !== ''): ?>
                                            <li
                                              style="text-decoration:line-through;"><?php echo Yii::t('mainApp',
                                                  'payments_originalPrice'); ?>
                                                <?php
                                                echo HeViews::formatAmountInCurrency($stProduct["originalPrice"],
                                                  $stProduct['ccyIsoCode'], $stProduct["ccyDisplaySymbol"]);
                                                ?>
                                            </li>
                                            <li>
                                                <?php if ($stProduct["originalPrice"] <> $stProduct["finalPrice"]): ?>
                                                    <?php echo Yii::t('mainApp', 'payments_diffPrice'); ?>
                                                    <?php
                                                    echo HeViews::formatAmountInCurrency(round($stProduct["originalPrice"] - $stProduct["finalPrice"],
                                                      2), $stProduct['ccyIsoCode'],
                                                      $stProduct["ccyDisplaySymbol"]);
                                                    ?>
                                                <?php endif; ?>
                                            </li>
                                        <?php endif; ?>

                                        <?php if (isset($stProduct["originalPrice"]) AND $stProduct["originalPrice"] !== ''): ?>
                                            <li id="IdPFinalPrice">
                                                <b
                                                  class="final-price <?php if ($stProduct["originalPrice"] !== '') {
                                                      echo 'red';
                                                  } ?>">
                                                    <?php echo Yii::t('mainApp', 'payments_resumePrice'); ?>
                                                    <?php
                                                    echo HeViews::formatAmountInCurrency($stProduct["finalPrice"],
                                                      $stProduct['ccyIsoCode'], $stProduct["ccyDisplaySymbol"]);
                                                    ?>
                                                </b>
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <ul class="ab-support-block">
                    <li><?php echo Yii::t('mainApp', 'payments_contactUs1') ?></li>

                    <li>
                        <i class="glyphicon glyphicon-earphone"></i>

                        <?php echo "+34 93 220 24 83"; ?>

                        <?php $comUser = new UserCommon(); ?>
                    </li>

                    <li><?php echo Yii::t('mainApp', 'payments_contactUs2') ?></li>
                </ul>
            </div>
        </div>


        <div class="row ab-payment-explanation">
            <div class="col-md-6 mob-m-b-35">
                <h4 class="ab-payment-title with-border"><?php echo Yii::t('mainApp',
                      'payments_paybackTitle2'); ?></h4>
                <?php
                echo Yii::t('mainApp', 'payments_paybackInfo2');
                ?>

                <span class="iconHoverVerisignSecured"></span>
                <span style="margin-left: 10px" class="iconHoverComodoSecured"></span>
            </div>

            <div class="col-md-6">
                <h4 class="ab-payment-title with-border"><?php echo Yii::t('mainApp',
                      'payments_supportTitle') ?></h4>

                <ul class="list-unstyled">
                    <li><a href="mailto:support@abaenglish.com">support@abaenglish.com</a></li>
                    <li>ABA English</li>
                    <li class="m-t-15"><img src="../../themes/ABAenglish/media/images/support_girl.png"/></li>
                    <li><b>Maria Chiara</b></li>
                    <li><?php echo Yii::t('mainApp', 'payments_supportInfo') ?></li>
                </ul>
            </div>
        </div>

    </div>

</div>

