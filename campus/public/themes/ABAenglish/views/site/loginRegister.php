<?php
/* @var LoginForm $model */
/* @var integer $timeout
/* @var string $forceUrl */
/* @var string $msgConfirmation */
/* @var string $createAccountUrl */
?>
<!DOCTYPE html>
<html id="downApp">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="language" content="<?php echo Yii::app()->language; ?>" />
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/main.css'); ?>
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/fonts.css'); ?>
        <?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/publicEvents.js'); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/plugins/jquery.infieldlabel.min.js'); ?>

        <title>
            <?php echo CHtml::encode($this->pageTitle); ?>
        </title>
    </head>

    <body id="downloadLogin">

    <div class="left cent" id="container">
     <?php HeAnalytics::includeScriptsCommonAnalyitics(); ?>
        <?php
        $this->pageTitle = Yii::app()->name . ' - Login';
        $this->breadcrumbs = array(
                'Login',
        );
        ?>
         <div class="top-menu left cent top-menu-download">
             <div id="slot_1">
                 <?php echo Yii::t('mainApp', 'Tu curso de inglés online para la vida real') ?>
             </div>
         </div>
        <div class="containerLogin">
            <div id="sliderBlockBorder"></div>
            <img id="logo" src="/themes/ABAenglish/media/images/ABA-English-logo-adwords.png" />
            <div class="containerForm">
                <div id="alreadyReg"><?php echo Yii::t('mainApp', '¿Ya tienes cuenta?') ?></div>
                <?php $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'login-form',
                        'action'=> $this->createURL('/site/login'),
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                                'validateOnSubmit' => true,
                        ),
                )); ?>
                <?php if($msgConfirmation!="") { ?>
                    <div class="row red">
                        <?php echo Yii::t('mainApp', $msgConfirmation); ?>
                    </div>
                <?php } ?>
                <div class="clear"></div>
                <div class="containerInputs">
                    <div class="row label wregister">
                        <?php echo $form->labelEx($model, 'email'); ?>
                        <?php echo $form->textField($model, 'email'); ?>
                        <?php echo $form->error($model, 'email'); ?>
                    </div>
                    <div class="clear"></div>
                    <div class="row label wregister">
                        <?php echo $form->labelEx($model, 'password'); ?>
                        <?php echo $form->passwordField($model, 'password'); ?>
                        <?php echo $form->error($model, 'password'); ?>
                    </div>
                    <div class="clear"></div>
                    <div class="row rememberMe">
                        <?php echo $form->checkBox($model, 'rememberMe'); ?>
                        <?php echo $form->label($model, 'notCloseSession'); ?>
                        <?php echo $form->error($model, 'rememberMe'); ?>
                    </div>
                </div>
                <!-- Old Container Inputs
                <div class="containerInputs">
                    <div class="row label">
                        <?php echo $form->labelEx($model, 'email'); ?>
                        <?php echo $form->error($model, 'email'); ?>
                    </div>
                    <div class="clear"></div>
                    <div class="row input">
                        <?php echo $form->textField($model, 'email'); ?>
                        <p class="hint" id="emailHint"><?php echo Yii::t('mainApp', 'EmailDown') ?></p>
                    </div>
                    <div class="clear"></div>
                    <div class="row label">
                        <?php echo $form->labelEx($model, 'password'); ?>
                        <?php echo $form->error($model, 'password'); ?>
                    </div>
                    <div class="clear"></div>
                    <div class="row input">
                        <?php echo $form->passwordField($model, 'password'); ?>
                        <p class="hint" id="passHint"><?php echo Yii::t('mainApp', 'Contraseña') ?></p>
                    </div>
                    <div class="clear"></div>
                    <div class="row rememberMe">
                            <?php echo $form->checkBox($model, 'rememberMe'); ?>
                            <?php echo $form->label($model, 'notCloseSession'); ?>
                            <?php echo $form->error($model, 'rememberMe'); ?>
                    </div>
                </div>
                -->
                <div class="clear"></div>
                <div class="row buttons">
                        <?php echo CHtml::submitButton('Login', array('class' => 'buttonStd', 'id'=>'btnStartSession', 'value' => Yii::t('mainApp', 'Entrar'))); ?>
                </div>
                <div class="clear"></div>
                <div class="forgottenPassword">
                    <a id="aLinkForgotPassword" href="<?php echo Yii::app()->createUrl("site/recoverpassword") ?>">
                        <?php echo Yii::t('mainApp', '¿Has olvidado tu contraseña?') ?>
                    </a>
                </div>
                <div class="notRegistered">
                    <a id="aLinkForgotPassword" href="<?php echo $createAccountAppUrl ?>">
                        <?php echo Yii::t('mainApp', '¿Aún no estás registrado?') ?>
                    </a>
                </div>
                <input name="wRegister"  value="1" type="hidden">
                <?php if(isset($forceUrl)){ ?>
                    <input name="forceUrl"  value="<?php echo $forceUrl ?>" type="hidden">
                <?php } ?>
                <?php $this->endWidget(); ?>

                <div class="clear"></div>
            </div><!-- form -->
            <div id="containerFeat">
                <div id="bgTrans"></div>
                <ul>
                    <li id="grammar"><?php echo Yii::t('mainApp', 'Comprende la gramática') ?><br/><?php echo Yii::t('mainApp', 'en fáciles videoclases') ?></li>
                    <li id="films"><?php echo Yii::t('mainApp', 'Aprende viendo nuestras') ?></br><?php echo Yii::t('mainApp', 'películas') ?></li>
                    <li id="micro"><?php echo Yii::t('mainApp', 'Practica y mejora tu') ?></br><?php echo Yii::t('mainApp', 'pronunciación') ?></li>
                </ul>
            </div>

        </div>
         <div class="conditions left cent">
             <div class="content-content download-footer relative center total-width no-bg clearfix">
                 <p>&copy; ABA English</p>
             </div><!-- end .content-content -->
         </div><!-- end .conditions -->
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $("label.required").inFieldLabels();
        });
    </script>

    </body>

</html>

