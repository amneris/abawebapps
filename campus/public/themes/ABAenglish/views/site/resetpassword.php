<div class="password-container">
    <h1><?php echo Yii::t('mainApp', 'recoverPassword_title'); ?></h1>
    
    <?php
        $recoverForm = $this->beginWidget(
            'CActiveForm',
            array(
                'id' => 'resetpwd-form',
                'action' => $this->createURL('site/resetpassword', array()),
                'enableClientValidation' => false,
                'clientOptions' => array('validateOnSubmit' => true),
                'focus'=>array($modelResetPwd, 'password'),
            )
        );
    ?>

    <div class="form-group">
        <?php if (empty($msgConfirmation)): ?>
            <?php if(trim($email) == ''): ?>
                <?php echo '<p class="sub-header">' . Yii::t('mainApp', 'recoverPassword_newpassword_expired') . '</p>'; ?>
            <?php else: ?>
                <?php echo '<p class="sub-header">' . Yii::t('mainApp', 'recoverPassword_newpassword', array("{user.email}" => $email)) . '</p>'; ?>
            <?php endif; ?>
        <?php endif; ?>

        <?php
        if (!empty($msgConfirmation)) {
            echo '<div class="alert alert-success" role="alert">'.$msgConfirmation.'</div>';
        } else if (!empty($msgError)) {
            echo '<div class="alert alert-error" role="alert">'.$msgError.'</div>';
        }
        ?>
    </div>

    <?php if (empty($msgFormatError)): ?>

    <div class="form-group">
        <?php echo $recoverForm->passwordField($modelResetPwd, 'password', array("placeholder" => Yii::t('mainApp', 'recoverPassword_password'))); ?>
        <?php echo $recoverForm->error($modelResetPwd, 'password', array('class' => "error-explanation")); ?>
    </div>

    <div class="form-group">
        <?php echo $recoverForm->passwordField($modelResetPwd, 'passwordconfirm', array('placeholder' => Yii::t('mainApp', 'recoverPassword_passwordconfirm'))); ?>
        <?php echo $recoverForm->error($modelResetPwd, 'passwordconfirm', array('class' => "error-explanation")); ?>
    </div>

    <div class="form-group submit-block">
        <?php
            echo CHtml::submitButton('Enviar', array('id' => 'btnResetPass', 'class' => 'btn', 'value' => Yii::t('mainApp', 'Enviar')));
        ?>
    </div>

    <?php endif; ?>

    <?php echo $recoverForm->hiddenField($modelResetPwd, 'email'); ?>
    <?php echo $recoverForm->hiddenField($modelResetPwd, 'autol'); ?>

    <?php $this->endWidget(); ?>
</div>
