<div id="facebookLogin">
    <div class="containerForm">
        <div class="login-info">
            <?php
            if($fbAction == 'register') {
                echo Yii::t('mainApp', 'fbLogin_existingUser', array('{fbUsername}' => $fbUsername));
            }
            else {
                echo Yii::t('mainApp', 'fbLogin_confirmEmail', array('{fbUsername}' => $fbUsername));
            }
            ?>
        </div>

        <?php $formFacebook = $this->beginWidget('CActiveForm', array(
            'id' => 'login-form-fb',
            'action'=> $this->createURL('/site/fblogin'),
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        ));?>

        <?php if($msgConfirmation != "") { ?>
            <div class="red">
                <?= Yii::t('mainApp', $msgConfirmation); ?>
            </div>
        <?php } ?>
        <div class="containerInputs">

            <div class="form-group">
                <?=
                $formFacebook->textField(
                    $modelFacebook, 'fbEmail',
                    array('class' => 'form-control icon-envelope', 'placeholder' => 'Email')
                );
                ?>
                <?= $formFacebook->error($modelFacebook, 'fbEmail'); ?>
            </div>
            <div class="clear"></div>

            <?php if($fbAction == 'register') { ?>
                <div class="form-group">
                    <?=
                    $formFacebook->passwordField(
                        $modelFacebook, 'fbPassword',
                        array('class' => 'form-control icon-key', 'placeholder' => 'Contraseña')
                    );
                    ?>
                    <?= $formFacebook->error($modelFacebook, 'fbPassword'); ?>
                </div>
            <?php } ?>

            <div class="buttons">
                <button type="button" id="btnFbSubmitReg" onclick="abaFb.sendUser();" class="abaButton-fb">
                    <span class="icon-facebook"></span><?= Yii::t('mainApp', 'Continuar') ?>
                </button>
            </div>
            <div class="clear"></div>

            <div class="err">
                <span style="color: #cc0000;"></span>
                <div style="clear: both;"></div>
            </div>

            <input name="fbAction" id="fbAction" value="<?php echo $fbAction; ?>" type="hidden">
            <input name="accessToken" id="accessToken" value="<?php echo $fbAccessToken; ?>" type="hidden">
            <input name="fbEmailFacebook" id="fbEmailFacebook" value="<?php echo $fbEmailFacebook; ?>" type="hidden">

            <?php $this->endWidget(); ?>

        </div>
    </div>
</div>
