<?php
/* @var integer $showObjectives */
/* @var integer $unreaded */
/* @var integer $notRead */
/* @var array $aLastUnitToGo  elements key are
 *      "sectionGreetingUser" => string,
 *      "pathImg" => string,
 *      "id",
 *      "title",
 *      "sectionTab"=>integer,
 *      "sectionTabTitle"=>string,
 *      "sectionTabProgress"=>integer,
 *      "sectionTabPopupInfo"=>string,
 *      "sectionTabDescription"=>string ,
 *      "sectionTabHowToComplete"=>string,
 *      "buttonTittle"=>string
 */
/* @var string $aLastVideoIdToGo  elements key are "id", "title", "pathImg" */
/* @var string $levelName */
/* @var string $countryName */

/*--ALL CONVERSION AND TRACKING FOR REGISTRATION OF FREE USERS and anything that has to be executed
    on client side comes anchored in this PLACE----------------------------------------------*/
/*-------------------------------------------------------------------------------------------*/
?> <div class="jsUseOnlySoHidden"><?php HeAnalytics::scriptRegisterUserConversionSingleOptIn(); ?></div><?php
/*-------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------*/


?>

<div id="wrapperDinamicContent">
    <div id="unitNumber">Unit <?php echo round($aLastUnitToGo["id"]); ?></div>
    <h3>
        <img class="teacher" src="<?= Yii::app()->theme->baseUrl ?>/media/images/teachers/<?= $teacherPicture ?>">
        <span>
            <?php
            if(Yii::app()->user->getState("welcomeBack")) {
                echo 'Welcome back, '.Yii::app()->user->getName().'!';
                Yii::app()->user->setState("welcomeBack", 0);
            }
            else {
                echo str_replace('{username}', Yii::app()->user->getName(), Yii::t('mainApp', 'hello_user' ));
            }
            ?>
        </span>
        <br>
        <?php echo Yii::t('mainApp', $aLastUnitToGo["sectionGreetingUser"]); ?>
    </h3>
    <div id="imageWrapper">
        <a href="<?php echo Yii::app()->createUrl('/course/AllUnits', array('unit' => $aLastUnitToGo["id"])) . '/section/'. $aLastUnitToGo["sectionTab"]; ?>">
            <?php
            $imagePath = $aLastUnitToGo["pathImg"];//Yii::app()->theme->baseUrl."/media/images/indexLine1image1.png";
            echo CHtml::image($imagePath, "Course Situation");
            ?>
        </a>
    </div>
    <div id="sectionInfo">
        <div id="secInfo_title"><span id="secInfo_title_num"><?php echo $aLastUnitToGo["sectionTab"]?></span><span id="secInfo_title_sec"><?php echo Yii::t('mainApp',$aLastUnitToGo["sectionTabTitle"]) ; ?></span></div>
        <div class="secInfo_Progress">
            <span class="secInfo_Progress_BarNivel left"></span>
            <span class="secInfo_Progress_BarNivelColor left" style="width: <?php echo (80*$aLastUnitToGo['sectionTabProgress']/100);?>px"></span>
            <span class="secInfo_percentage"><?php echo $aLastUnitToGo['sectionTabProgress'] ?>%</span>
        </div>
        <div class="clear"></div>
        <div id="secInfo_subTitle"><?php echo Yii::t('mainApp', $aLastUnitToGo["sectionTabDescription"]) ;?></div>
        <div id="secInfo_complete"><?php echo Yii::t('mainApp', $aLastUnitToGo["sectionTabHowToComplete"]) ;?></div>
        <?php
        $unitUrl = Yii::app()->createUrl('/course/AllUnits', array('unit' => $aLastUnitToGo["id"])) . '/section/'. $aLastUnitToGo["sectionTab"];
        if(isset($show_free_vs_premium) && $show_free_vs_premium === true)
        {
            $unitUrl .= '/ref/home';
        }
        ?>
        <a href="<?php echo $unitUrl; ?>" class="homeButton">
            <?php echo Yii::t('mainApp', $aLastUnitToGo['buttonTittle']); ?>
        </a>
    </div>

</div>
<div class="ContainerIndexSite">
<!-- Section of Unit Course in Home page:
we display last Unit completed plus one ..........................................-->

<!-- End Section of Unit Course ................................................................................-->
<!-- Section of Video class in Home page: we display last Videoclass watched + 1 (FREE users) or first Video-class not watched( PREMIUM users)-->
    <div class="videoClassContainer marginLeft16 left">
        <div class="containerLine1Title2">
            <div>
                <span><?php echo Yii::t('mainApp', 'Tu videoclase de hoy') ?></span>
            </div>
        </div>
        <div class="containerIndexImagesLine1">
            <div>
                <?php
                $videoUrl = Yii::app()->createUrl('/videoclass/AllUnits', array('unit' => $aLastUnitToGo["id"]));
                if(isset($show_free_vs_premium) && $show_free_vs_premium === true)
                {
                    $videoUrl .= '/ref/home';
                }
                ?>
                <a href="<?php echo $videoUrl; ?>">
                    <?php
                    $imagePath = $aLastVideoIdToGo["pathImg"];
                    echo CHtml::image($imagePath, "Today VideoClass");
                    ?>
                </a>
            </div>
        </div>
        <a href="<?php echo $videoUrl; ?>">> <?php echo $aLastVideoIdToGo["title"];?></a>
    </div>
    <div class="messagesConatiner left">
        <div class="subContainerLeftLine2Title">
            <?php echo Yii::t('mainApp', 'Mensajes') ?>
        </div>
        <div class="subContainerLeftLine2SubTitle">

            <?php if ($notRead==1) { ?>
            <?php echo Yii::t('mainApp', 'Tienes') ?> <?php echo $notRead ?> <?php echo Yii::t('mainApp', 'nuevos mensajes singular') ?>
            <?php } else echo Yii::t('mainApp', 'Tienes') . " " . $notRead . " " . Yii::t('mainApp', 'nuevos mensajes')?>

        </div>
        <?php if(array_key_exists(0, $unreaded)){
            if(strlen($unreaded[0]['subject'])>20)
                $subject = substr($unreaded[0]['subject'],0,20)."...";
            else
                $subject = $unreaded[0]['subject'];
            ?>
            <div class="subContainerLeftLine2NewMessages">
                <span><?php echo $unreaded[0]['date'] ?></span>
                <a href="<?php echo $this->createUrl('message/view/', array('message_id' => $unreaded[0]['id'])) ?>"><?php echo $subject ?></a>
            </div>
        <?php } ?>
        <?php if(array_key_exists(1, $unreaded)){
            if(strlen($unreaded[1]['subject'])>45)
                $subject = substr($unreaded[1]['subject'],0,45)."...";
            else
                $subject = substr($unreaded[1]['subject'],0,20)."...";
            ?>
            <div class="subContainerLeftLine2NewMessages">
                <span><?php echo $unreaded[1]['date'] ?></span>
                <a href="<?php echo $this->createUrl('message/view/', array('message_id' => $unreaded[1]['id'])) ?>"><?php echo $subject ?></a>
            </div>
        <?php } ?>
        <a href="<?php echo Yii::app()->createUrl('message/inbox') ?>">> <?php echo Yii::t('mainApp', 'Ver mis mensajes') ?></a>
    </div>
<!-- End Section of Video class .............................................................................................-->
    <div class="clear"></div>
    <div class="containerLine2 left">
        <div class="progressContainer left">
            <div class="progressContainerTitle">
                    <?php echo Yii::t('mainApp', 'Tu Progreso') ?>
            </div>
            <div id="progressImage">
                <a href="<?php echo Yii::app()->createUrl("/profile/progress") ?>">
                    <?php
                    $imagePath = HeMixed::getUrlPathImg("progress.jpg");
                    echo CHtml::image($imagePath, "Tu Progreso");
                    ?>
                </a>
            </div>
            <a href="<?php echo Yii::app()->createUrl("/profile/progress") ?>"><?php echo Yii::t('mainApp', 'See_progress') ?></a>
        </div>
        <div class="certificateContainer left">
            <div class="progressContainerTitle">
                <?php echo Yii::t('mainApp', 'Descarga tus certificados') ?>
            </div>
            <div id="progressImage">
                <a href="<?php echo Yii::app()->createUrl('profile/certificates') ?>">
                    <?php
                    $imagePath = HeMixed::getUrlPathImg("certificate.jpg");
                    echo CHtml::image($imagePath, "Certificados");
                    ?>
                </a>
            </div>
            <a href="<?php echo Yii::app()->createUrl('profile/certificates') ?>">> <?php echo Yii::t('mainApp', 'Descargar') ?></a>
        </div>
    </div>
    <div class="clear"></div>
</div>
<div id="wrapperBottomSite">
    <div class="readyConatiner left">
        <div class="containerLine1Title2">
            <div>
                <span><?php echo Yii::t('mainApp', '¿Está preparado tu ordenador?') ?></span>
            </div>
        </div>
        <?php
        $isMobile = HeDetectDevice::isMobile();
        if($isMobile) {
            $readyToWorkUrl = 'javascript:campusUtils.openModal(\''.Yii::app()->createUrl('/modals/popupErrorDevice').'\', 700, 200)';
        }
        else {
            $readyToWorkUrl = Yii::app()->createUrl("/readytowork/index");
        }
        ?>
        <div>
            <a href="<?php echo $readyToWorkUrl ?>">
                <?php
                $imagePath = HeMixed::getUrlPathImg("escucha-graba-compara-home.png");
                echo CHtml::image($imagePath, "Configura tu ordenador");
                ?>
            </a>
        </div>
        <a href="<?php echo $readyToWorkUrl ?>">> <?php echo Yii::t('mainApp', 'Configura tu ordenador') ?></a>
    </div>
    <div class="opinionConatiner left">
        <div class="title">
            <?php echo Yii::t('mainApp', '¡Danos tu opinión!') ?>
        </div>
        <div id="opinionBody">
            <?php echo Yii::t('mainApp', 'Nos encantaría saber tu opinión sobre el nuevo curso de inglés de ABA 4.0.') ?><br/>
            <?php echo Yii::t('mainApp', 'Por favor, escríbenos a') ?>
        </div>
        <div class="cmp_Email">
            <a href="mailto:newaba@abaenglish.com">> newaba@abaenglish.com</a>
        </div>
    </div>

    <div class="clear"></div>
</div>

<?php
//#ABAWEBAPPS-693
$sCooladata = HeCooladata::createDefaultEvent(HeCooladata::EVENT_NAME_ENTERED_CAMPUS_HOME);
Yii::app()->clientScript->registerScript(md5($sCooladata), $sCooladata, CClientScript::POS_READY);
?>

<?php
//importante no borrar para el popup y formulario de primer login
if(isset($welcomeUrl))
{
    Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/modal.css');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jquery-ui.custom.js');

    ?>
    <div id="welcomePopup" style="display: none">
        <?php $this->renderPartial($welcomeUrl, $welcomeData); ?>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            var closeBtn = '<?php if (isset($welcomeData['closeBtn'])) {
                echo $welcomeData['closeBtn'];
} else {
                echo 'false';
} ?>';
            campusPopup.renderHiddenElement('welcomePopup', closeBtn);
        });
    </script>
    <?php
}
