<?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/assets/out/mobile_splash.css'); ?>

<div class="ab-mobile-splash">

    <div class="ab-splash-content">
        <div class="text-center">
            <img class="ab-splash-logo" src="<?= Yii::app()->theme->baseUrl ?>/assets/img/logo_water.png">
        </div>

        <div class="text-center ab-slogan">
            <h1><?= Yii::t('mainApp', 'appSplash_header') ?></h1>
        </div>

        <div class="text-center">
            <a href="#" onclick="tryLaunchNativeApp()" class="ab-mobile-splash-btn" id="app-btn"><?= Yii::t('mainApp', 'appSplash_btnApp') ?></a>
            <a href="<?= Yii::app()->createUrl('/site/index') ?>" class="ab-mobile-splash-btn" id="campus-btn"><?= Yii::t('mainApp', 'appSplash_btnCampus') ?></a>
        </div>
    </div>

    <div class="text-center ab-fixed-buttons">
        <input type="checkbox" id="no-splash" />
        <label for="no-splash"><?= Yii::t('mainApp', 'appSplash_checkbox') ?></label>
    </div>

    <script src="<?php echo Yii::app()->theme->baseUrl.'/assets/out/mobile_splash.js' ?>"></script>
</div>