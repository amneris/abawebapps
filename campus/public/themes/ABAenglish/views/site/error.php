<?php
    $this->pageTitle = 'Error ' . $error['code'];

    $this->widget('application.components.widgets.BreadcrumbHeader',
       array('curMenuSectionTab' => 'Error ' . $error['code']));
?>
<div class="error-wrapper">
    <div class="divRightGeneral left">
        <p class="disculpa">
            <?php echo Yii::t('mainApp', 'Oops, sorry!') ?>
        </p>
        <p class="pError">
            <span>Error <?php echo $error['code']; ?></span>
        </p>
        <p class="pExplicacion"> <?php
            if ($error['code']=="404"){
                echo Yii::t('mainApp', 'La página que intentas solicitar no está en el servidor.');
            } else {
                echo $error['message'];
            }
            ?></p>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-1201726-1', 'auto');
            ga('send', 'pageview', '/error/' + <?php echo $error['code'] ?> +'?url=' + document.location.pathname + document.location.search + '&ref=' + document.referrer);
        </script>
    </div>
</div>
