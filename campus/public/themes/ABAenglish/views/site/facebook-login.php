<div id="fb-root"></div>
<script type="text/javascript">

    // Load the SDK asynchronously
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    window.fbAsyncInit = function() {
        FB.init({
            appId: '<?php echo (isset($fbAppId) ? $fbAppId : ''); ?>',
            status: true,
            cookie: true,
            xfbml: true,
            oauth: true,
            version: 'v2.1'
        });
    };

    function statusChangeCallback(response) {
        try {
            if(abaFb.isset(response.status)) {
                if (response.status === 'connected') {

                    ar = FB.getAuthResponse();

                    if(ar) {
                        if(abaFb.isset(ar.accessToken)) {
                            abaFb.setToken(ar.accessToken);
                        }
                        abaFb.checkUser();
                    }
                }
                else{
                    abaFb.blockAction(false, 'btnFbSubmit');
                }
            }
        }
        catch (err){
            abaFb.blockAction(false, 'btnFbSubmit');
        }
    }

    function checkLoginState() {
        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    }

    function loginWithFacebook() {
        FB.getLoginStatus(function(response){
            if(abaFb.isset(response.status) && response.status == 'connected'){
                checkLoginState();
            } else {
                FB.login(function(response) {
                    statusChangeCallback(response);
                }, {scope : "<?php echo $fbScope; ?>"});
            }
        });
    }

    var abaFb = {
        t :         '<?php if(!empty($fbId)) echo $fbId; ?>',
        t :         '<?php if(!empty($fbAccessToken)) echo $fbAccessToken; ?>',
        e :         '<?php if(!empty($fbEmailFacebook)) echo $fbEmailFacebook; ?>',
        setToken :  function (t) { this.t = t; },
        getToken :  function () { return this.t; },
        setEmail :  function (e) { this.e = e; },
        getEmail :  function () { return this.e; },
        setAction : function (a) { this.a = a; },
        getAction : function () { return this.a; },
        isset :     function () {
            try {
                var a = arguments, l = a.length, i = 0, undef;
                if (l === 0) {
                    throw new Error('Empty isset');
                }
                while (i !== l) {
                    if (a[i] === undef || a[i] === null) {
                        return false;
                    }
                    i++;
                }
                return true;
            }
            catch (err){ return false; }
        },
        blockAction : function (a, e){
            if(a) {
                $('#' + e).attr({"disabled":"disabled"});
                $('#' + e).css({"opacity":"0.4", "filter":"alpha(opacity=40)"});
            }
            else {
                $('#' + e).removeAttr("disabled");
                $('#' + e).css({"opacity":"1", "filter":"alpha(opacity=100)"});
            }
        },
        checkUser : function (){
            try {
                abaFb.setAction('exists');

                var url_action =    "<?= $this->createURL('/site/fblogin') ?>";
                var data =          {};

                data['fbAction']    =   abaFb.getAction();
                data['accessToken'] =   abaFb.getToken();

                $.ajax({
                    type: 'POST',
                    url: url_action,
                    data: data,
                    beforeSend: function(){
                        g_utils.showLoader();
                    },
                    success: function(response) {
                        try {
                            var response = $.parseJSON(response);
                            g_utils.showLoader();
                            window.location.href = response.redirect;
                            return;
                        }
                        catch(e){
                            abaFb.showFbContainer(response);
                        }
                        abaFb.showFbContainer(response);
                    }
                });
            }
            catch (err){
                abaFb.blockAction(false, 'btnFbSubmit');
            }
        },
        sendUser : function (){
            try {
                var url_action =    "<?= $this->createURL('/site/fblogin') ?>";
                var data =          {};

                $('#login-form-fb input').each(function() {
                    data[this.name] = this.value;
                });

                $.ajax({
                    type: 'POST',
                    url: url_action,
                    data: data,
                    beforeSend: function(){
                        g_utils.showLoader();
                    },
                    success: function(response) {
                        try {
                            var response = $.parseJSON(response);
                            window.location.href = response.redirect;
                            return;
                        }
                        catch(err) {
                            abaFb.showFbContainer(response);
                        }
                    }
                });
            }
            catch (err) {
                abaFb.blockAction(false, 'btnFbSubmitReg');
            }
        },
        showFbContainer : function(content) {
            $.fancybox( {
                content: content,
                closeBtn: true,
                helpers   : {
                    overlay : {closeClick: false} // prevents closing when clicking OUTSIDE fancybox
                }
            });

            return true;
        }
    };

    $(function() {

        $('button.facebookLogin').on('click',function(e) {
            e.preventDefault();
            //removeErrorTip();
            loginWithFacebook();
        });

        $('#cancelSuscriptionFacebookButton').on('click',function(e) {

            abaFb.setAction('');

            $.fancybox.close();

            $("#fbEmail").val('');
            $("#fbPass").val('');
        });

        $("#fbPassBlock").hide();
    });

</script>