<div class="password-container">
    <h1><?php echo Yii::t('mainApp', 'recoverPassword_title'); ?></h1>

    <div class="form-group">
        <?php
        if (!empty($msgConfirmation)) {
            echo '<div class="alert alert-success" role="alert">'.$msgConfirmation.'</div>';
        }
        ?>
    </div>

    <div class="form-group text-center">
        <a id="idDivBtnGoToCampus"  href="<?php echo $url; ?>" class="btn">
            <?php echo strtoupper(Yii::t('mainApp', 'key_BackCampus')); ?>
        </a>
    </div>
</div>
<script type="application/javascript">
    setTimeout(function () {
        location.href = "<?php echo $url; ?>";
    }, 10000);
</script>

