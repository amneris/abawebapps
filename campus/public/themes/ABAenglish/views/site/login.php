<html>
<head>
    <title><?php echo Yii::t('mainApp', 'loginSite_title') ?></title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="language" content="<?php echo Yii::app()->language; ?>" />
    <meta name="description" content="<?php echo Yii::t('mainApp', 'loginSite_desc') ?>" />
    <?php $this->renderPartial('//layouts/shared/versioninfo'); ?>
    <link href="//campus.abaenglish.com/themes/ABAenglish/media/images/favicon.ico" rel="shortcut icon" >

    <?php Yii::app()->clientScript->registerPackage('bootstrap'); ?>
    <?php Yii::app()->clientScript->registerPackage('font-awesome'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/responsive/main.css'); ?>

    <?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>

    <?php Yii::app()->clientScript->registerPackage('jquery.fancybox'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/utils.js'); ?>
    <?php $this->renderPartial('//layouts/shared/cooladataManager'); ?>
    <?php $this->renderPartial('//layouts/shared/newrelic'); ?>
</head>
<body>

<?php HeAnalytics::includeScriptsCommonAnalyitics(); ?>

<?php $this->renderPartial('//layouts/shared/tagManager'); ?>

<?php
if($fbEnabled == 1) {
    include 'facebook-login.php';
    include 'linkedin-login.php';
}

$language = Yii::app()->language;
$showRegister = $modelRegister->hasErrors();

$returnUrl = isset($_GET['returnTo']) ? urldecode($_GET['returnTo']) : $forceUrl;

?>

<section id="header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="logo">
                    <a class="inner" href="http://www.abaenglish.com"></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-7">
                <h1 class="marginTop-0"><?= Yii::t('mainApp', 'loginSite_header') ?></h1>
                <p class="h3-size"><strong><?= Yii::t('mainApp', 'loginSite_header2') ?></strong></p>
                <p class="h5-size marginTop-30 info"><?= Yii::t('mainApp', 'loginSite_header3') ?></p>
            </div>
            <div class="col-md-4 col-sm-5">
                <div class="box loginForm">
                    <div id="loginForm_collapse" class="collapsable header-login">
                        <div>
                            <strong><?= Yii::t('mainApp', 'loginSite_loginHeader') ?></strong>
                            <span class="icon-arrow"></span>
                        </div>
                    </div>
                    <div class="inner">
                        <?php $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'login-form',
                            'action' => $this->createURL('/site/login'),
                            'enableClientValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                        )); ?>

                        <?php if (!empty($returnUrl)) { ?>
                            <input name="forceUrl"  value="<?php echo $returnUrl ?>" type="hidden">
                        <?php } ?>

                        <div class="form-group with-error-message">
                            <?= $form->textField($model, 'email', array('class' => 'form-control icon-envelope', 'autocomplete' => 'off', 'placeholder' => Yii::t('mainApp', 'loginSite_email'))); ?>
                            <?= $form->error($model, 'email'); ?>
                        </div>
                        <div class="form-group with-error-message">
                            <?= $form->passwordField($model, 'password', array('class' => 'form-control icon-key', 'autocomplete' => 'off', 'placeholder' => Yii::t('mainApp', 'loginSite_pwd'))); ?>
                            <?= $form->error($model, 'password'); ?>
                        </div>
                        <div class="form-group form-buttons">
                            <?= CHtml::submitButton('Login', array('class' => 'abaButton-blue', 'id'=>'btnStartSession', 'value' => Yii::t('mainApp', 'loginSite_btnStart'))); ?>
                            <?php
                            if($fbEnabled == 1) {
                                ?>
                                <div class="o-separator"><?= Yii::t('mainApp', 'loginSite_btnLoginSeparator') ?></div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <button type="button" class="buttonslogins btn-facebook facebookLogin">
                                            <div class="iconos">
                                                <i class="fa fa-facebook"></i>
                                            </div>
                                            <?= Yii::t('mainApp', 'loginSite_btnRegisterFb') ?></button>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <button id="linkedinLogin" type="button" class="buttonslogins btn-linkedin linkedinLogin">
                                            <div class="iconos iconosin">
                                                <i class="fa fa-linkedin"></i>
                                            </div>
                                            Linkedin
                                        </button>
                                    </div>
                                </div>

                                <!--<button type="button" class="abaButton-fb facebookLogin"><span class="icon-facebook"></span><?= Yii::t('mainApp', 'loginSite_btnStart') ?></button> -->
                                <?php
                            }
                            ?>
                            <div class="clearer"></div>
                        </div>
                        <div class="small-info">
                            <?= $form->checkBox($model, 'rememberMe') ?>
                            <label for="LoginForm_rememberMe"><?= Yii::t('mainApp', 'loginSite_rememberMe') ?></label>
<!--                            <a class="abaLink-blue forgotPassword" href="javascript:recoverPassword.open();">--><?//= Yii::t('mainApp', 'loginSite_forgotPwd') ?><!--</a>-->
<!--                            <a class="abaLink-blue forgotPassword" href="//campus.dev.abaenglish.com/recover-password">--><?//= Yii::t('mainApp', 'loginSite_forgotPwd') ?><!--</a>-->
                            <a class="abaLink-blue forgotPassword" href="<?php echo Yii::app()->createUrl("site/recoverpassword"); ?>"><?= Yii::t('mainApp', 'loginSite_forgotPwd') ?></a>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
                <div class="box registerForm">
                    <div id="registerForm_collapse" class="collapsable header">
                        <div>
                            <strong><?= Yii::t('mainApp', 'loginSite_registerHeader') ?></strong>
                            <span><?= Yii::t('mainApp', 'loginSite_registerHeader2') ?></span>
                            <span class="icon-arrow"></span>
                        </div>
                    </div>
                    <div class="inner">
                        <?php $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'register-form',
                            'action' => $this->createURL('/site/login'),
                            'enableClientValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                        )); ?>

                        <?php if (!empty($returnUrl)) { ?>
                            <input name="forceUrl"  value="<?php echo $returnUrl ?>" type="hidden">
                        <?php } ?>

                        <div class="form-group with-error-message">
                            <?= $form->textField($modelRegister, 'name', array('class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => Yii::t('mainApp', 'loginSite_name'))); ?>
                            <?= $form->error($modelRegister, 'name'); ?>
                        </div>
                        <div class="form-group with-error-message">
                            <?= $form->textField($modelRegister, 'email', array('class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => Yii::t('mainApp', 'loginSite_email'))); ?>
                            <?= $form->error($modelRegister, 'email'); ?>
                        </div>
                        <div class="form-group with-error-message">
                            <?= $form->passwordField($modelRegister, 'password', array('class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => Yii::t('mainApp', 'loginSite_createPwd'))); ?>
                            <?= $form->error($modelRegister, 'password'); ?>
                        </div>
                        <div>
                            <?= CHtml::submitButton('Register', array('class' => 'abaButton-blue', 'id'=>'btnRegister', 'value' => Yii::t('mainApp', 'loginSite_btnRegister'))); ?>
                            <?php
                            if($fbEnabled == 1) {
                                ?>
                                <div class="o-separator"><?= Yii::t('mainApp', 'loginSite_btnRegisterSeparator') ?></div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <button type="button" class="buttonslogins btn-facebook facebookLogin">
                                            <div class="iconos">
                                                <i class="fa fa-facebook"></i>
                                            </div>
                                            <?= Yii::t('mainApp', 'loginSite_btnRegisterFb') ?></button>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <button type="button" class="buttonslogins btn-linkedin linkedinLogin">
                                            <div class="iconos iconosin">
                                                <i class="fa fa-linkedin"></i>
                                            </div>
                                            Linkedin
                                        </button>
                                    </div>
                                </div>

                                <!--<button type="button" class="abaButton-fb facebookLogin"><span class="icon-facebook"></span><?= Yii::t('mainApp', 'loginSite_btnStart') ?></button> -->
                                <?php
                            }
                            ?>
                            <div class="clearer"></div>
                        </div>
                        <div class="small-info">
                            <?php
                            $txtTerms = '<a class="abaLink-blue" href="'.Yii::t('mainApp', 'loginSite_termsLink').'">'.Yii::t('mainApp', 'loginSite_terms').'</a>';
                            $txtPrivacy = '<a class="abaLink-blue" href="'.Yii::t('mainApp', 'loginSite_privacyLink').'">'.Yii::t('mainApp', 'loginSite_privacy').'</a>';
                            $txtRegister = Yii::t('mainApp', 'loginSite_btnRegister');
                            ?>
                            <?= Yii::t('mainApp', 'loginSite_termsAndPrivacy', array('{btnRegister}' => $txtRegister, '{terms}' => $txtTerms, '{privacy}' => $txtPrivacy)) ?>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
                <p class="h5-size info2 lastElement" lang="<?= $language ?>"><?= Yii::t('mainApp', 'loginSite_header3') ?></p>
            </div>
        </div>
    </div>
</section>
<section id="ourMethod" class="lastElement" lang="<?= $language ?>">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <h4><?= Yii::t('mainApp', 'loginSite_info1Title') ?></h4>
                <p><?= Yii::t('mainApp', 'loginSite_info1Text') ?></p>
            </div>
            <div class="col-md-4 col-sm-4">
                <h4><?= Yii::t('mainApp', 'loginSite_info2Title') ?></h4>
                <p><?= Yii::t('mainApp', 'loginSite_info2Text') ?></p>
            </div>
            <div class="col-md-4 col-sm-4">
                <h4><?= Yii::t('mainApp', 'loginSite_info3Title') ?></h4>
                <p><?= Yii::t('mainApp', 'loginSite_info3Text') ?></p>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <hr>
                <ul class="list-unstyled">
                    <li><a href="<?= Yii::t('mainApp', 'loginSite_footer1Link') ?>"><?= Yii::t('mainApp', 'loginSite_footer1') ?></a></li>
                    <li><a href="<?= Yii::t('mainApp', 'loginSite_footer2Link') ?>"><?= Yii::t('mainApp', 'loginSite_footer2') ?></a></li>
                    <li><a href="<?= Yii::t('mainApp', 'loginSite_footer3Link') ?>"><?= Yii::t('mainApp', 'loginSite_footer3') ?></a></li>
                    <li><a href="<?= Yii::t('mainApp', 'loginSite_footer4Link') ?>"><?= Yii::t('mainApp', 'loginSite_footer4') ?></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript">
    var loginFormSize = 0;
    var registerFormSize = 0;

    $(document).ready(function() {

        /* **** Open proper form and calculate extra padding to maintain window size **** */
        loginFormSize = $('section#main div.box.loginForm .inner').height();
        registerFormSize = $('section#main div.box.registerForm .inner').height();

        var collapsedForm = '<?= $showRegister ? 'loginForm' : 'registerForm' ?>';

        $('#' + collapsedForm + '_collapse').addClass('collapsed');
        formActions.toggleForm(collapsedForm);
        formActions.initFocus();
        formActions.bodyResize(collapsedForm);

        $('#registerForm_collapse, #loginForm_collapse').on('click', function() {
            if ($(this).hasClass('collapsed')) {
                var collapsedForm = $(this).attr('id') == 'registerForm_collapse' ? 'loginForm' : 'registerForm';
                var expandedForm = $(this).attr('id') == 'loginForm_collapse' ? 'loginForm' : 'registerForm';

                $('.collapsable').toggleClass('collapsed');
                formActions.toggleForm(collapsedForm);
                formActions.toggleForm(expandedForm);
                formActions.initFocus();
                formActions.bodyResize(collapsedForm);
            }
        });
        /* **** End **** */

        /* ************* Google Analytics tracking events **************** */
        var openRegisterForm = false;
        var clickRegister = false;
        var clickRegisterFacebook = false;
        $("#registerForm_collapse").on("click", function() {
            if(!openRegisterForm) {
                ga('send', 'event', 'Campus register', 'Expand register form');
                openRegisterForm = true;
            }
        });
        $("#btnRegister").on("click", function() {
            if(!clickRegister) {
                ga('send', 'event', 'Campus register', 'Click register button');
                clickRegister = true;
            }
        });
        $("#btnRegisterFb").on("click", function() {
            if(!clickRegisterFacebook) {
                ga('send', 'event', 'Campus register', 'Click facebook register button');
                clickRegisterFacebook = true;
            }
        });
        /* ************* End Google Analytics tracking events **************** */

      <?php if ( !empty($model->msgPopup) ) { ?>
      $.fancybox(<?php echo CJavaScript::encode($model->msgPopup); ?>, { closeBtn: true, modal:false });
      <?php } ?>
    });

    var formActions = {
        toggleForm: function(form) {
            var elements = [$("section#main div.box." + form + " .inner"), $("section#main div.box." + form + " .icon-arrow")];
            elements.forEach(function (el) {
                $(el).toggleClass('collapsed');
            });
        },
        bodyResize: function(collapsedForm) {
            var collapsedSize = collapsedForm == 'loginForm' ? loginFormSize : registerFormSize;
            var expandedSize = collapsedForm == 'loginForm' ? registerFormSize : loginFormSize;
            var extraSize = collapsedSize - expandedSize;
            
            if (extraSize < 0) extraSize = 0;
            $('section#ourMethod').css('padding-top', extraSize + 'px');
            $('section#main .info2').css('padding-bottom', extraSize + 'px');
        },
        initFocus: function() {
            $('section#main div.box .inner:not(.collapsed) input[type="text"]').first().focus();
        }
    };

    var recoverPassword = {
        open: function() {
            $.ajax({
                type: "GET",
                url: "<?php echo Yii::app()->createUrl("site/recoverpassword"); ?>",
                beforeSend: function(){
                    g_utils.showLoader();
                },
                success: function(response) {
                    g_utils.showPopup(response);
                }
            });
        },
        send: function() {
            var data = {};
            $('#recoverpwd-form input').each(function() {
                data[this.name] = this.value;
            });

            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createUrl("site/recoverpasswordprocess"); ?>",
                data: data,
                beforeSend: function(){
                    g_utils.showLoader();
                },
                success: function(response) {
                    g_utils.showPopup(response);
                }
            });
        },
        reset: function() {
            var data = {};
            $('#resetpwd-form input').each(function() {
                data[this.name] = this.value;
            });

            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createUrl("site/resetpassword"); ?>",
                data: data,
                beforeSend: function(){
                    g_utils.showLoader();
                },
                success: function(response) {
                    g_utils.showPopup(response);
                }
            });
        }
    }
</script>
</body>
</html>