<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jquery-ui.custom.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/AC_OETags.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/readytowork.js');
?>
<div class="ContainerIndexSite">
    <div class="testSoftwareTitle">
        <?php echo Yii::t('mainApp', '¿Tu ordenador está preparado para ABA English?') ?>
    </div>
    <p id="testFlash" class="comprovacionTitle" style="display: none;">1. <?php echo Yii::t('mainApp', 'Comprobación Flash') ?></p>
    <div class="clear"></div>
    <p id="TestFlashExiste" class="comprovacionSubtitle">
        <span id="ok" style="display: none;">
            <span class="iconCheckModal left"></span>
            <span class="text"><?php echo Yii::t('mainApp', 'Flash instalado') ?>.</span>
        </span>
        <span id="ko" style="display: none;">
            <span class="iconWrongModal left"></span>
            <span class="text"><?php echo str_replace('{param1}', Yii::t('mainApp', 'key_linkNoFlash'), Yii::t('mainApp', 'key_noFlash')) ?>.</span>       
        </span>
    </p>
    <div class="clear"></div>
    <p id="TestVersion" class="comprovacionSubtitle">
    </p>
    <div class="clear"></div>
    <div id="TestdeMicrofono" style="display: none;">
        <p class="comprovacionTitle">2. <?php echo Yii::t('mainApp', 'Comprobación de audio y micrófono') ?></p>
        <div class="clear"></div>
        <p class="comprovacionSubtitle">
            <span class="iconCheckModal left"></span>
            <span class="text"><?php echo Yii::t('mainApp', 'Micrófono instalado') ?>.</span>
        </p>
        <div class="clear"></div>
    </div>
    <div id="pregunta1" style="display: none;">
        <div class="intonationTitle">
            <span class="bold"><?php echo Yii::t('mainApp', 'Comprobación del ejercicio Escucha-Graba-Compara (LRC®)') ?></span>
        </div>
        <div class="clear"></div>
        <div class="sectionTitleIntonation">
            <div class="number">1</div>
            <p class="p1"><?php echo Yii::t('mainApp', 'Escucha') ?>:</p>
            <p class="p2"><?php echo Yii::t('mainApp', 'Haz clic en la frase para escucharla') ?>.</p>
        </div>
        <div class="clear"></div>
        <div class="explicacionContainer">
            <div class="iconContainerModal left">
                <div class="iconEscuchaModal left"></div>
            </div>
            <div class="textContainerModal left">
                <p class="p2"><span class="bold"><?php echo Yii::t('mainApp', 'Frase') ?>: </span><span id="hearCampus" class="sub">A blue sea, white sand and a beautiful girl</span></p>
                <div class="pregunta" style="display: none;">
                    <?php echo Yii::t('mainApp', '¿Has escuchado la frase?') ?> <input type="radio" id="p1" name="p1" value="1" /> <?php echo Yii::t('mainApp', 'si') ?> <input id="p1n" type="radio" name="p1" value="0" /> <?php echo Yii::t('mainApp', 'no') ?>
                </div>
                <div id="p1EscucharFalse" class="pregunta" style="display: none;">
                    <?php echo Yii::t('mainApp', 'Visita: ') ?><a href="<?php echo Yii::app()->createUrl("help/ayudatecnica") ?>"><?php echo Yii::t('mainApp', 'Ayuda técnica') ?></a>
                </div>
            </div>
        </div>
    </div>
    <div id="pregunta2" style="display: none;">
        <div class="clear"></div>
        <div class="sectionTitleIntonation">
            <div class="number">2</div>
            <p class="p1"><?php echo Yii::t('mainApp', 'Graba') ?>:</p>
            <p class="p2"><?php echo Yii::t('mainApp', 'Haz clic en el icono del micro para grabar tu voz; para terminar la grabación vuelve a hacer clic en el mismo icono') ?>.</p>
        </div>
        <div class="clear"></div>
        <div class="explicacionContainer">
            <div class="iconContainerModal left">
                <div class="iconGrabaModal left"></div>
            </div>
            <div class="textContainerModal left">
                <p class="p2"><span class="bold"><?php echo Yii::t('mainApp', 'Frase') ?>: </span><span id="recordCampus" class="sub">A blue sea, white sand and a beautiful girl</span></p>
            </div>
        </div>
    </div>
    <div id="pregunta3" style="display: none;">
        <div class="clear"></div>
        <div class="sectionTitleIntonation">
            <div class="number">3</div>
            <p class="p1"><?php echo Yii::t('mainApp', 'Compara') ?>:</p>
            <p class="p2"><?php echo Yii::t('mainApp', 'Haz clic en el icono correspondiente para comparar tu voz con la del nativo') ?>.</p>
        </div>
        <div class="clear"></div>
        <div class="explicacionContainer">
            <div class="iconContainerModal left">
                <div class="iconComparaModal left"></div>
            </div>
            <div class="textContainerModal left">
                <p class="p2"><span class="bold"><?php echo Yii::t('mainApp', 'Frase') ?>: </span><span id="compareCampus" class="sub">A blue sea, white sand and a beautiful girl</span></p>
                <div class="pregunta" style="display: none;">
                    <?php echo Yii::t('mainApp', '¿Has escuchado la frase original y tu voz?') ?> <input type="radio" id="p3" name="p3" value="1" /> <?php echo Yii::t('mainApp', 'si') ?> <input id="p3n" type="radio" name="p3" value="0" /> <?php echo Yii::t('mainApp', 'no') ?>
                </div>
                <div id="p2EscucharFalse" class="pregunta" style="display: none;">
                    <?php echo Yii::t('mainApp', 'Visita: ') ?><a href="<?php echo Yii::app()->createUrl("help/ayudatecnica") ?>"><?php echo Yii::t('mainApp', 'Ayuda técnica') ?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="testSoftwareFooter" style="display: none;">
        <?php echo Yii::t('mainApp', 'Has finalizado todas las comprobaciones') ?>
    </div>
    <div class="clear"></div>
    <div class="separatorReadytoWorkFooter left" style="display: none;"></div>
    <div class="clear"></div>
    <div id="ocultar"  style="display: none;">
    <p class="ParrafosFinalEGC">
        <?php echo Yii::t('mainApp', 'El ejercicio Escucha-Graba-Compara (LRC®: Listen, Record and Compare), es muy importante porque te permite aprender a hablar y pronunciar inglés correctamente como un nativo con gran facilidad.') ?>
    </p>
    <p class="ParrafosFinalEGC">
        <?php echo Yii::t('mainApp', 'Si después de la realización de este test todavía tienes dudas, puedes consultar la sección Ayuda o escribir un correo a support@abaenglish.com') ?>
    </p>
    <p class="ayudaRelacionadaModal">
        <?php echo Yii::t('mainApp', 'Ayuda relacionada') ?>
    </p>
    <p class="subAyudaRelacionadaModal">
        <span class="iconPreg">?</span><a href="<?php echo Yii::app()->createUrl("help/ayudatecnica") ?>"><?php echo Yii::t('mainApp', '¿Qué requisitos técnicos son necesarios para realizar el curso de ABA English?') ?></a>
    </p>
    <p class="subAyudaRelacionadaModal">
        <span class="iconPreg">?</span><a href="<?php echo Yii::app()->createUrl("help/ayudatecnica") ?>"><?php echo Yii::t('mainApp', 'No puedo grabar o no oigo el audio del curso') ?>.</a>
    </p>
    <p class="subAyudaRelacionadaModal">
        <span class="iconPreg">?</span><a href="<?php echo Yii::app()->createUrl("help/ayudatecnica") ?>"><?php echo Yii::t('mainApp', '¿Cómo puedo aumentar el volumen de la grabación?') ?></a>
    </p>
    </div>
    <input type="hidden" id="userMail" value="<?php echo $mail ?>" />
    <div id="contentTooltipCampus" style="display: none;">
        <span id="hearCampusButton" class="buttonTooltipCampus"></span>
        <span id="recordCampusButton" class="buttonTooltipCampus"></span>
        <span id="compareCampusButton" class="buttonTooltipCampus"></span>
    </div>
    <input id="urlPopupFlash" type="hidden" value="<?php echo Yii::app()->createUrl('modals/popupMic'); ?>" />
</div>
<div id="videoHolder" style="position: absolute; top: -1000px; left: -1000px;">
    <script language="JavaScript" type="text/javascript">
        sReleaseCampus='<?php echo Yii::app()->user->getReleaseCampus(); ?>';
        if (DetectFlashVer(9, 0, 28)) {
            showOverlay();

            var zoom = 1;
            var w = 215;
            var h = 138;
            var flashVersion = GetSwfVer();

            flashVersion = flashVersion.split('.');
            if (flashVersion[0] < 18) {
                // detect zoom
                (function(root,ns,factory){"use strict";"undefined"!=typeof module&&module.exports?module.exports=factory(ns,root):"function"==typeof define&&define.amd?define("detect-zoom",function(){return factory(ns,root)}):root[ns]=factory(ns,root)})(window,"detectZoom",function(){var devicePixelRatio=function(){return window.devicePixelRatio||1},fallback=function(){return{zoom:1,devicePxPerCssPx:1}},ie8=function(){var zoom=Math.round(100*(screen.deviceXDPI/screen.logicalXDPI))/100;return{zoom:zoom,devicePxPerCssPx:zoom*devicePixelRatio()}},ie10=function(){var zoom=Math.round(100*(document.documentElement.offsetHeight/window.innerHeight))/100;return{zoom:zoom,devicePxPerCssPx:zoom*devicePixelRatio()}},webkitMobile=function(){var deviceWidth=90==Math.abs(window.orientation)?screen.height:screen.width,zoom=deviceWidth/window.innerWidth;return{zoom:zoom,devicePxPerCssPx:zoom*devicePixelRatio()}},webkit=function(){var important=function(str){return str.replace(/;/g," !important;")},div=document.createElement("div");div.innerHTML="1<br>2<br>3<br>4<br>5<br>6<br>7<br>8<br>9<br>0",div.setAttribute("style",important("font: 100px/1em sans-serif; -webkit-text-size-adjust: none; text-size-adjust: none; height: auto; width: 1em; padding: 0; overflow: visible;"));var container=document.createElement("div");container.setAttribute("style",important("width:0; height:0; overflow:hidden; visibility:hidden; position: absolute;")),container.appendChild(div),document.body.appendChild(container);var zoom=1e3/div.clientHeight;return zoom=Math.round(100*zoom)/100,document.body.removeChild(container),{zoom:zoom,devicePxPerCssPx:zoom*devicePixelRatio()}},firefox4=function(){var zoom=mediaQueryBinarySearch("min--moz-device-pixel-ratio","",0,10,20,1e-4);return zoom=Math.round(100*zoom)/100,{zoom:zoom,devicePxPerCssPx:zoom}},firefox18=function(){return{zoom:firefox4().zoom,devicePxPerCssPx:devicePixelRatio()}},opera11=function(){var zoom=window.top.outerWidth/window.top.innerWidth;return zoom=Math.round(100*zoom)/100,{zoom:zoom,devicePxPerCssPx:zoom*devicePixelRatio()}},mediaQueryBinarySearch=function(property,unit,a,b,maxIter,epsilon){function binarySearch(a,b,maxIter){var mid=(a+b)/2;if(0>=maxIter||epsilon>b-a)return mid;var query="("+property+":"+mid+unit+")";return matchMedia(query).matches?binarySearch(mid,b,maxIter-1):binarySearch(a,mid,maxIter-1)}var matchMedia,head,style,div;window.matchMedia?matchMedia=window.matchMedia:(head=document.getElementsByTagName("head")[0],style=document.createElement("style"),head.appendChild(style),div=document.createElement("div"),div.className="mediaQueryBinarySearch",div.style.display="none",document.body.appendChild(div),matchMedia=function(query){style.sheet.insertRule("@media "+query+"{.mediaQueryBinarySearch "+"{text-decoration: underline} }",0);var matched="underline"==getComputedStyle(div,null).textDecoration;return style.sheet.deleteRule(0),{matches:matched}});var ratio=binarySearch(a,b,maxIter);return div&&(head.removeChild(style),document.body.removeChild(div)),ratio},detectFunction=function(){var func=fallback;return isNaN(screen.logicalXDPI)||isNaN(screen.systemXDPI)?window.navigator.msMaxTouchPoints?func=ie10:"orientation"in window&&"string"==typeof document.body.style.webkitMarquee?func=webkitMobile:"string"==typeof document.body.style.webkitMarquee?func=webkit:navigator.userAgent.indexOf("Opera")>=0?func=opera11:window.devicePixelRatio?func=firefox18:firefox4().zoom>.001&&(func=firefox4):func=ie8,func}();return{zoom:function(){return detectFunction().zoom},device:function(){return detectFunction().devicePxPerCssPx}}});

                zoom = detectZoom.device();
                w /= zoom;
                h /= zoom;
            }

            document.write(AC_FL_RunContent(
                "src", "<?php echo Yii::app()->theme->baseUrl."/media/course/players/ABAPlayer1_2" ?>",
                "width", w,
                "height", h,
                "align", "middle",
                "id", "ABAPlayer3",
                "name", "ABAPlayer3",
                "quality", "high",
                "bgcolor", "#ffffff",
                "scale", "noscale",
                "wmode","window",
                "menu","false",
                "type", "application/x-shockwave-flash",
                "pluginspage", "http://www.adobe.com/go/getflashplayer",
                "flashvars", "<?php echo "urlServer=stream.abaenglish.com&copyright=false&comments=off&html=apl/&levelColor=#EAEAEA&username=".urlencode($mail)."&password=".urlencode($pass)."&source=abaenglishb2c&testmic=1" ?>"
            ));
        }
        else {
            $(document).ready(function() {
                $("#testFlash").css("display","block");
                $("div.separatorReadytoWorkFooter.left").css("display", "block");
                $("#videoHolder").css({'width' : 0, 'height' : 0});
                $("#TestFlashExiste > #ko").css("display", "inline");

            });
        }
    </script>
</div>