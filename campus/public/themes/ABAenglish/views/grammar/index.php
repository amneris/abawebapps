<?php
$urlBase = Yii::app()->getBaseUrl(true).Yii::app()->theme->baseUrl."/media/course/units/grammar4/".strtolower(Yii::app()->getLanguage())."/mc/";
$scriptBase = "<script type=\"text/javascript\">
                    var base = document.createElement('base');
                    base.href = '$urlBase';
                    document.getElementsByTagName('head')[0].appendChild(base);
                </script>";
echo $scriptBase;

$url = Yii::app()->theme->basePath."/media/course/units/grammar4/".strtolower(Yii::app()->getLanguage())."/mc/grammar.html";
$buffer = file_get_contents($url);

$isMobile = HeDetectDevice::isMobile();
$theName = Yii::app()->user->getName();
$theUserId = Yii::app()->user->getId();
$theMail = Yii::app()->user->getEmail();
$thePassword = Yii::app()->user->getPassword();
$theReleaseCampus = Yii::app()->user->getReleaseCampus();

$urlAbs = $urlBase = Yii::app()->getBaseUrl(true).Yii::app()->theme->baseUrl."/media/course/";

$theKeys = array("#isMobile#", "#userId#", "#username#", "#password#", "#name#", "#urlAbs#", "#section#", "#chapter#", "#page#", "#Cerrar#", "#instruccionesString#", "#diccionarioString#", "#PREVString#", "#PAGEString#", "#OFString#", "#NEXTString#", "#releaseCampus#", "#isB2B#");//include releaseCampus second time
$theValues = array($isMobile, $theUserId, $theMail, $thePassword, $theName, $urlAbs, $section, $chapter, $page, Yii::t('mainApp', 'Cerrar'), Yii::t('mainApp', 'Instrucciones'), Yii::t('mainApp', 'Diccionario del curso'), Yii::t('mainApp', 'Previous'), Yii::t('mainApp', 'Página'), Yii::t('mainApp', 'de_home'), Yii::t('mainApp', 'Next'), $theReleaseCampus, $isB2B);
$theBuffer = str_replace($theKeys, $theValues, $buffer);
echo($theBuffer);

// global variables from php that course needs to access
?>
<script>
    var _glossaryUrl = "<?= Yii::app()->params['glossaryUrl'] ?>";
    var _transactionsUrl = "<?= Yii::app()->params['transactionsUrl'] ?>";
</script>

<?php
//#ABAWEBAPPS-701
$stParams = array("levelId" => array("value" => "cooladataLevelId", "type" => "js"));
$sCooladata = HeCooladata::createDefaultEvent(HeCooladata::EVENT_NAME_ENTERED_INTERACTIVE_GRAMMAR, true, $stParams);
Yii::app()->clientScript->registerScript(md5($sCooladata), $sCooladata, CClientScript::POS_READY);
?>
