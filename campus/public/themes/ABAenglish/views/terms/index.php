<?php
/* @var $paySuppExtId integer */
/* @var $idTextSpecificConditions string */

if (isset($withBreadcrumb) && $withBreadcrumb === true) {
    $this->widget(
        'application.components.widgets.BreadcrumbHeader',
        array('curMenuSectionTab' => 'Condiciones generales')
    );
}
?>
<div class="divRightGeneral left">
    <p class="termsTitle"><?php echo Yii::t('mainApp', 'Condiciones generales') ?></p>
    
    <?php if (Yii::app()->language != 'ru'){?>
    <p class="PTerms">
        <?php echo Yii::t('mainApp', 'En cumplimiento a la Ley 34/2002, del 11 de julio, de Servicios de la Sociedad de la Información y de Comercio Electrónico (LSSICE), a continuación se indican los datos de información general de este sitio web.') ?><span class="bold">ABAEnglish.com</span><?php echo Yii::t('mainApp', ' es un dominio de Internet y marca registrada por ENGLISH WORLWIDE, S.L., C.I.F. B-64.401.482, inscrita en el Reg. Mercantil Barcelona, T. 39235, F. 216, Secc. Gral, Hoja/Dup B-342505, Inscr. 1ª, sita en la calle Guitard, 45 planta 4ª 08014 Barcelona ') ?><a href="mailto:info@abaenglish.com">info@abaenglish.com</a>
    </p>

    <p class="PTerms">
        <?php echo Yii::t('mainApp', $idTextSpecificConditions, array("{tag_br}"=>"<br/>","{br}"=>"<br/>")); ?>
    </p>

    <p class="PTerms">
        <span class="bold">ENGLISH WORLWIDE, S.L.</span><?php echo Yii::t('mainApp', ', en adelante ') ?><span class="bold">ABAEnglish.com</span><?php echo Yii::t('mainApp', ', está adherida desde hace años a los principales códigos éticos del sector del comercio electrónico, concretamente a los promovidos por la Asociación Española de Comercio Electrónico. La utilización por el usuario de los servicios contenidos en la web de ') ?><span class="bold">ABAEnglish.com</span><?php echo Yii::t('mainApp', ' implica la aceptación expresa de las siguientes condiciones generales:') ?>
    </p>
    <p class="PTerms">
        <?php echo Yii::t('mainApp', 'Condiciones generales de contratación entre ') ?><span class="bold">ABA English</span><?php echo Yii::t('mainApp', ' y el usuario:') ?>
    </p>
    <p class="PTerms">
        <span class="bold">1.</span><?php echo Yii::t('mainApp', ' El usuario declara que es mayor de edad (mayor de 18 años) y dispone de capacidad legal necesaria para contratar los servicios ofertados por ') ?><span class="bold">ABA English</span><?php echo Yii::t('mainApp', ', manifestando asimismo que acepta la vinculación a este acuerdo y entiende, y acepta en su totalidad las condiciones aquí enunciadas para utilizar esta web y a contratar los servicios ofertados') ?>.
    </p>
    <p class="PTerms">
        <span class="bold">2.</span><?php echo Yii::t('mainApp', ' Al registrarse en ') ?><span class="bold">ABA English</span><?php echo Yii::t('mainApp', ' usted dispondrá de un nombre de usuario y una contraseña elegidos por usted y que posteriormente se le comunicará por email. ') ?><span class="bold">ABA English</span><?php echo Yii::t('mainApp', ' no se hace responsable del mal uso de las contraseñas que usted pueda llevar a cabo como usuario registrado. Es responsabilidad del usuario custodiar debidamente las claves y contraseñas que se suministren para el acceso como usuario registrado, impidiendo el uso indebido o acceso por parte de terceros.') ?>
    </p>
    <p class="PTerms">
        <span class="bold">3.</span><?php echo Yii::t('mainApp', ' El usuario, para poder hacer uso de los servicios de ') ?><span class="bold">ABA English</span><?php echo Yii::t('mainApp', ' deberá registrarse como cliente, a cuyos efectos declara que toda la información suministrada a la hora de registrarse es verdadera, completa y precisa y que de conformidad con la Ley Orgánica 15/1999 de Protección de Datos de Carácter Personal, la persona que se registra autoriza expresamente a ') ?><span class="bold">ABA English</span><?php echo Yii::t('mainApp', ' para que proceda a incluir en un fichero automatizado los datos de carácter personal que figuran en los campos del formulario por resultar necesarios para la adecuada prestación de sus servicios. Se indica que el Responsable del fichero es la sociedad mercantil ABA English S.L. (c/ Guitard 45, 4a - 08014 Barcelona, Tlfno.: 902 024 386 fax: 93 268 25 12, E-mail: ') ?><a href="mailto:info@abaenglish.com">info@abaenglish.com</a>.<?php echo Yii::t('mainApp', ' Esta dirección de correo electrónico está protegida contra los robots de spam, necesitas tener Javascript activado para poder verla ), a efectos del ejercicio de los derechos de acceso, rectificación, cancelación y oposición previstos en la Ley. En cada formulario de recogida de datos de la presente página web se especificarán de forma concreta los usos y finalidades de los datos obtenidos. Asimismose autoriza expresamente la comunicación de dichos datos a terceros cuando resulte necesarios para la adecuada prestación de los servicios que se contraten respetando en todo momento la legislación en vigor. El usuario da su consentimiento expreso para recibir comunicaciones electrónicas con publicidad y novedades comerciales de ') ?><span class="bold">ABA English</span><?php echo Yii::t('mainApp', ' y de los productos o servicios que comercializa o promociona. ') ?><span class="bold">ABA English</span><?php echo Yii::t('mainApp', ' pone a disposición del usuarios los medios de contacto referidos en el párrafo anterior para que estos revoquen su consentimiento. Los precios indicados en pantalla incluyen cualquier impuesto que fuera de aplicación y serán en todo momento vigentes, salvo error tipográfico') ?>.
    </p>
    <p class="PTerms">
        <span class="bold">3.1</span><?php echo Yii::t('mainApp', ' En el caso de curso online, bajo cualquiera de las suscripciones vigentes en el curso, éstas se renovarán automáticamente, según la periodicidad elegida en el momento del registro. Así mismo, ') ?><span class="bold">ABA</span><?php echo Yii::t('mainApp', ' se reserva el derecho de avisar previamente, pues queda aceptada la renovación automática de las cuotas en el momento de la compra de la suscripción') ?>.
    </p>
    <p class="PTerms">
        <span class="bold">3.2</span><?php echo Yii::t('mainApp', ' Durante los treinta primeros días, posteriores a la compra del producto, se puede cancelar en cualquier momento la inscripción, teniendo derecho a la devolución del importe de dicha compra. Esta garantía de devolución no se aplica en las renovaciones de las cuotas de las suscripciones al curso online') ?>.
    </p>
    <p class="PTerms">
        <span class="bold">3.3 </span><?php echo Yii::t('mainApp', 'key_allpago_conditions_privacy_boleto') ?>
    </p>

    <p class="PTerms">
        <span class="bold">4.</span><?php echo Yii::t('mainApp', ' Todos los contenidos, marcas, logos, dibujos etc. que figuran en la WEB de ') ?><span class="bold">ABA English</span><?php echo Yii::t('mainApp', ', se hallan protegidos por los derechos de propiedad intelectual e industrial que son expresamente reservados por ') ?><span class="bold">ABA English</span><?php echo Yii::t('mainApp', ' o, en su caso, por las personas o empresas que figuran como autores o titulares de los derechos. La violación de los anteriores derechos será perseguida de conformidad con la legislación vigente. Por lo tanto queda prohibida la reproducción, explotación, alteración, distribución o comunicación pública por cualquier título o medio de la totalidad de los contenidos de la WEB de ') ?><span class="bold">ABA English</span><?php echo Yii::t('mainApp', ' para usos diferentes de la legítima información o contratación por los usuarios de los servicios ofertados. En todo caso será necesario el consentimiento previo por escrito de ') ?><span class="bold">ABA English</span> <?php echo strtolower(Yii::app()->user->getLanguage()) === 'en' ? ' will be necessary' : '' ?>.
    </p>
    <p class="PTerms">
        <span class="bold">5.</span><?php echo Yii::t('mainApp', ' El usuario acepta que la legislación aplicable al funcionamiento de este servicio es la española, sometiéndose a la jurisdicción y competencia de los Juzgados y Tribunales del domicilio del usuario, cuando se encuentre éste en España, o a la de los Juzgados y Tribunales de Barcelona, cuando se encuentre fuera de España, renunciando expresamente a cualquier otro fuero o jurisdicción que en Derecho pudiere corresponder') ?>.
    </p>
    <p class="PTerms">
        <span class="bold">6. ABA English</span><?php echo Yii::t('mainApp', ' se reserva el derecho a realizar los cambios que considere oportunos en los términos y condiciones establecidos. Las modificaciones se incluirán de forma destacada en la WEB de ') ?><span class="bold">ABA English</span><?php echo strtolower(Yii::app()->user->getLanguage()) === 'en' ? ' web page' : '' ?>.
    </p>
    <p class="PTerms">
        <span class="bold">7.</span><?php echo Yii::t('mainApp', ' El envío y la remisión de datos que se realice por el usuario a través de la WEB de ') ?><span class="bold">ABA English</span><?php echo Yii::t('mainApp', ' o la información que ésta remita, se encuentra protegida por las más modernas técnicas de seguridad electrónica en la red. Asimismo los datos suministrados y almacenados en nuestras bases de datos se encuentran igualmente protegidos por sistemas de seguridad que impiden el acceso de terceros no autorizados a los mismos. ') ?><span class="bold">ABA English</span><?php echo Yii::t('mainApp', ' realiza sus mejores esfuerzos para disponer de los sistemas más actualizados para la eficacia de estos sistemas de seguridad') ?>.
    </p>
    <p class="PTerms">
        <span class="bold">8.</span><?php echo Yii::t('mainApp', ' Los links o enlaces con otras páginas WEB que aparezcan en las páginas de ') ?><span class="bold">ABA English</span><?php echo Yii::t('mainApp', ' se ofrecen a efectos meramente informativos al usuario, por lo que ') ?><span class="bold">ABA English</span><?php echo Yii::t('mainApp', ' no se responsabiliza sobre los productos, servicios o contenidos que se oferten o suministren en las páginas de destino ubicados en otro dominio') ?>.
    </p>
    <p class="PTerms">
        <span class="bold">9.</span><?php echo Yii::t('mainApp', ' El acceso a nuestro Web Site puede implicar la utilización de cookies. Las cookies son pequeñas cantidades de información que se almacenan en el navegador utilizado por cada usuario para que el servidor recuerde cierta información que posteriormente únicamente el servidor que la implementó leerá. Las cookies tienen, generalmente, una duración limitada en el tiempo. Ninguna cookie permite que pueda contactarse con el número de teléfono del ususario, su dirección de correo electrónico o con cualquier otro medio de contacto. Ninguna cookie puede extraer información del disco duro del usuario o robar información personal. Aquellos usuarios que no deseen recibir cookies o quieran ser informados de su fijación pueden configurar su navegador a tal efecto') ?>.
    </p>
    <p class="PTerms">
        <span class="bold">10.</span><?php echo Yii::t('mainApp', ' Si cualquier cláusula incluida en estas condiciones generales fuese declarada, total o parcialmente, nula o ineficaz, tal nulidad o ineficacia afectará tan sólo a dicha disposición o a la parte de la misma que resulte nula o ineficaz, subsistiendo las condiciones generales en todo lo demás, teniéndose tal disposición, o la parte de la misma que resultase afectada, por no puesta') ?>.
    </p>
    <p class="registredCopyrightTerms">Copyright © ABA English</p>

    <?php }
    //todo: Unificar todos los idiomas con los tags del ruso

    elseif(Yii::app()->language == 'ru'){

        echo Yii::t('mainApp', 'terms_info1');

        echo Yii::t('mainApp', 'terms_info2');

        echo Yii::t('mainApp', 'terms_info3');

        echo Yii::t('mainApp', 'terms_info4');

        echo Yii::t('mainApp', 'terms_info5');

        echo Yii::t('mainApp', 'terms_p1');

        echo Yii::t('mainApp', 'terms_p2');

        echo Yii::t('mainApp', 'terms_p3');

        echo Yii::t('mainApp', 'terms_p3.1');

        echo Yii::t('mainApp', 'terms_p3.2');

        echo Yii::t('mainApp', 'terms_p4');

        echo Yii::t('mainApp', 'terms_p5');

        echo Yii::t('mainApp', 'terms_p6');

        echo Yii::t('mainApp', 'terms_p7');

        echo Yii::t('mainApp', 'terms_p8');

        echo Yii::t('mainApp', 'terms_p9');

        echo Yii::t('mainApp', 'terms_p10');

        echo Yii::t('mainApp', 'terms_copyright');
    } ?>
</div>