<?php
    if($language2 !== "menu"){
        foreach(Yii::app()->getMessages() as $key => $value)
        {
            if($key=='basePath')
                $base = $value;
        }
        $prova = require_once $base.DIRECTORY_SEPARATOR."es".DIRECTORY_SEPARATOR."mainApp.php";
        $provaComparar = require_once $base.DIRECTORY_SEPARATOR.$language2.DIRECTORY_SEPARATOR."mainApp.php";
        header("Cache-Control: public");
//        header('Content-Type: text/csv; charset=utf-8');
//        header('Content-Disposition: attachment; filename=Traducciones-'.$language2.'.csv');
//        foreach($prova as $key => $value)
//        {
//            if(array_key_exists($key, $provaComparar))
//                echo '"'.$value.'";"'.$provaComparar[$key].'"'.PHP_EOL;
//            else
//                echo '"'.$value.'";""'.PHP_EOL;
//        }
        header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1
        header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1
        header('Pragma: no-cache');
        header('Expires: 0');
        header('Content-Transfer-Encoding: none');
        header('Content-Type: application/vnd.ms-excel'); // This should work for IE & Opera
        header('Content-type: application/x-msexcel'); // This should work for the rest
        header('Content-Disposition: attachment; filename="Traducciones-'.$language2.'.xls"');
        echo '<?xml version="1.0"?>'.PHP_EOL;
        echo '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"'.PHP_EOL;
            echo 'xmlns:o="urn:schemas-microsoft-com:office:office"'.PHP_EOL;
            echo 'xmlns:x="urn:schemas-microsoft-com:office:excel"'.PHP_EOL;
            echo 'xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"'.PHP_EOL;
            echo 'xmlns:html="http://www.w3.org/TR/REC-html40">'.PHP_EOL;
            echo '<Worksheet ss:Name="Sheet1">'.PHP_EOL;
                echo '<Table>'.PHP_EOL;
                    echo '<Row>'.PHP_EOL;
                        echo '<Cell><Data ss:Type="String">Español</Data></Cell>'.PHP_EOL;
                        echo '<Cell><Data ss:Type="String">'.Yii::app()->params->languages[$language2].'</Data></Cell>'.PHP_EOL;
                    echo '</Row>'.PHP_EOL;
                    foreach($prova as $key => $value)
                    {
                        echo '<Row>'.PHP_EOL;
                            if(array_key_exists($key, $provaComparar))
                                echo '<Cell><Data ss:Type="String">'.$value.'</Data></Cell>'.PHP_EOL.'<Cell><Data ss:Type="String">'.$provaComparar[$key].'</Data></Cell>'.PHP_EOL;
                            else
                                echo '<Cell><Data ss:Type="String">'.$value.'</Data></Cell>'.PHP_EOL.'<Cell><Data ss:Type="String"></Data></Cell>'.PHP_EOL;
                        echo '</Row>'.PHP_EOL;
                    }
                echo '</Table>'.PHP_EOL;
            echo '</Worksheet>'.PHP_EOL;
        echo '</Workbook>'.PHP_EOL;
        
    }
    else{
        foreach(Yii::app()->params->languages as $languagekey => $language)   
            echo '<p><a href="'.Yii::app()->createUrl("about/exportarTraducciones", array("idioma" => $languagekey)).'">'.$language.'</a></p>';
    }
?>
