<?php
/**
 * @var array $aUnits
    $aUnits[$unitId]["unitId"]
    $aUnits[$unitId]["mainNameNoTrans"]
    $aUnits[$unitId]["titleTrans"]
    $aUnits[$unitId]["thumbUrlLink"]
    $aUnits[$unitId]["thumbPath"]
    $aUnits[$unitId]["unitProgress"]
    boolean $aUnits[$unitId]["userAccess"] If 1 has access otherwise no access
 * @var integer $firstUnit
 * @var integer $lastUnit
 * @var integer $lastAccessUnitId *
 *
 @var string Yii::app()->user->getState('langCourse')
 @var boolean $showPopupGoToPremium : False means that no action has to be taken when click on grey thumbnails, "sepia"
*/
$langCourse = Yii::app()->user->getState('langCourse');
$this->widget('application.components.widgets.BreadcrumbHeader', array('curMenuSectionTab' => ucfirst(strtolower('UNIDADES'))));
?>
<div class="contadorUnidadesContainer">
    <span class="mostrando"><?php echo Yii::t('mainApp', 'Mostrando') ?> <?php echo $firstUnit ?> - <?php echo $lastUnit ?><?php echo Yii::t('mainApp', ' de ') ?></span>
    <span class="mostrandoBold">144 <?php echo Yii::t('mainApp', 'Unidades') ?></span>
</div>
<div class="clear"></div>
<div class="divRightGeneral left">
    <?php 
        $i=0;
        foreach($aUnits as &$unit){
            $uAccess = '1';
            $unitId = $unit["unitId"];

            if($i%4==0)
            {
        ?>
                <div class="clear"></div>
                <div id="idDivUnit<?php echo $uAccess.$unitId;?>" class="contenedorUnidadIndividual left">
                    <div class="borderexternofotoUnidad">
                        <div class="borderinternofotoUnidad">
                            <a href="<?php echo Yii::app()->createUrl('/course/AllUnits',array('unit'=>$unit["unitId"])) ?>">
                                <div class="CourseLink">
                                    <img width="156px" height="84px" src="<?php echo HeMixed::getUrlPathImg("curso/".(int)$unit["unitId"].".jpg"); ?>" alt="fotoUnidad <?php echo $unit["unitId"] ?>" />
                                    <div></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="contenedorTextoInferior">
                        <span class="numeroUnidad"><?php echo Yii::t('mainApp', 'UNIDAD') ?>&nbsp;<?php echo (int)$unit["unitId"] ?></span>
                        <a class="nombreUnidad" href="<?php echo Yii::app()->createUrl('/course/AllUnits',array('unit'=>$unit["unitId"])) ?>">> <?php echo $unit["mainNameNoTrans"]; ?></a>
                        <span class="descripcionUnidad"><?php echo $unit["titleTrans"]; ?></span>
                        <div class="contenedorPorcentageUnidad">
                            <?php
                                if($unit["unitProgress"] >= 100){
                                    echo "<span class=\"marginLeft5 OKIconCourseCampus\"></span>";
                                    echo "<span class=\"marginLeft5 textProximamenteVerde\">".Yii::t('mainApp', 'completado')."</span>";
                                }
                                else{
                                    echo "<span class=\"marginLeft5 textProximamenteRojo\">".$unit["unitProgress"].Yii::t('mainApp', '% completado')."</span>";
                                }
                            ?>
                        </div>
                    </div>
                </div>
        <?php
            }
            else{
        ?>
            <div  id="idDivUnit<?php echo $uAccess.$unitId;?>"  class="contenedorUnidadIndividual left marginLeft4">
                <div class="borderexternofotoUnidad">
                    <div class="borderinternofotoUnidad">
                        <a href="<?php echo Yii::app()->createUrl('/course/AllUnits',array('unit'=>$unit["unitId"] )) ?>">
                            <div class="CourseLink">
                                <img width="156px" height="84px" src="<?php echo HeMixed::getUrlPathImg("curso/".(int)$unit["unitId"].".jpg"); ?>" alt="fotoUnidad <?php echo $unit["unitId"] ?>" />
                                <div></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="contenedorTextoInferior">
                    <span class="numeroUnidad"><?php echo Yii::t('mainApp', 'UNIDAD') ?>&nbsp;<?php echo (int)$unit["unitId"]; ?></span>
                    <a class="nombreUnidad" href="<?php echo $unit["thumbUrlLink"]; ?>">> <?php echo $unit["mainNameNoTrans"]; ?></a>
                    <span class="descripcionUnidad"><?php echo $unit["titleTrans"]; ?></span>
                    <div class="contenedorPorcentageUnidad">
                        <?php
                            if($unit["unitProgress"] >= 100){
                                echo "<span class=\"marginLeft5 OKIconCourseCampus\"></span>";
                                echo "<span class=\"marginLeft5 textProximamenteVerde\">".Yii::t('mainApp', 'completado')."</span>";
                            }
                            else{
                                echo "<span class=\"marginLeft5 textProximamenteRojo\">".$unit["unitProgress"].Yii::t('mainApp', '% completado')."</span>";
                            }
                        ?>
                    </div>
                </div>
            </div>
        <?php
        }
        $i++;
    }
    ?>
</div>
<!-- Should care about $showPopupGoToPremium to decide if Popup has to be displayed or not
Modify fucntion popUpPasateaPremium(), line 156 ?? -->
<input id="urlPopup" type="hidden" value="<?php echo Yii::app()->createUrl('modals/PopupPasateaPremiumCourse'); ?>" />

<?php
//#ABAWEBAPPS-693
$stParams = array("levelId" => array("value" => "cooladataLevelId", "type" => "js"));
$sCooladata = HeCooladata::createDefaultEvent(HeCooladata::EVENT_NAME_ENTERED_COURSE_INDEX, true, $stParams);
Yii::app()->clientScript->registerScript(md5($sCooladata), $sCooladata, CClientScript::POS_READY);
?>

