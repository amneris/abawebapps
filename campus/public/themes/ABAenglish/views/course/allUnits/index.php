<?php

/* @var integer $unit */
/* @var integer $userTypeAccessUnit : could be INCOMPLETEUNIT, FULLUNIT or 0 */
/* @var string $type: "unit" or "video" */
/* @var bool $isB2B */
/* @var string teacherImg */

switch ($userTypeAccessUnit) {
    case OP_INCOMPLETEUNIT:
        $coursePath = 'cursofree';
        $unitsPath = 'campusfree4';
        break;

    case OP_FULLUNIT:
        $coursePath = 'curso';
        $unitsPath = 'campus4';
        break;

    default:
        Yii::app()->request->redirect(Yii::app()->createUrl('course/index'));
        break;
}

switch ($section) {
    case 1:
        $section = 'SITUATION';
        break;
    case 2:
        $section = 'STUDY';
        break;
    case 3:
        $section = 'DICTATION';
        break;
    case 4:
        $section = 'ROLEPLAY_SELECT';
        break;
    case 5:
        $section = 'GRAMMAR';
        break;
    case 6:
        $section = 'WRITING';
        break;
    case 7:
        $section = 'NEWWORDS';
        break;
    case 8:
        $section = 'MINITEST';
        break;
    default:
        $section = 'SITUATION';
        break;
}

$userLanguage = strtolower(Yii::app()->getLanguage());

$urlAbsolute = Yii::app()->getBaseUrl(true).Yii::app()->theme->baseUrl.'/media/course/'.$coursePath.'/';
$urlBase = Yii::app()->getBaseUrl(true).Yii::app()->theme->baseUrl.'/media/course/units/'.$unitsPath.'/'.$userLanguage.'/'.$unit.'/';
$scriptBase = "<script type=\"text/javascript\">
                    var base = document.createElement('base');
                    base.href = '$urlBase';
                    document.getElementsByTagName('head')[0].appendChild(base);
                </script>";

echo $scriptBase;

$path = Yii::app()->theme->basePath.'/media/course/units/'.$unitsPath.'/'.$userLanguage.'/'.$unit.'/master'.$unit.'.html';


if (file_exists($path)) {
    $buffer = file_get_contents($path);
} else {
    throw new CHttpException(404, 'The specified path cannot be found.');
}

$theUserId = Yii::app()->user->getId();
$theMail = Yii::app()->user->getEmail();
$thePassword = Yii::app()->user->getPassword();
$theReleaseCampus = Yii::app()->user->getReleaseCampus();
$userType = Yii::app()->user->refreshAndGetUserType();

if ($userType == FREE) {
    // member get member check premium trial access
    if (Yii::app()->user->isPremiumFriend()) {
        $userType = PREMIUM;
    }
}

$isMobile = HeDetectDevice::isMobile();
$theName = Yii::app()->user->getName();
$jwPlayer = $isMobile ? 'js/jwplayer6/jwplayer.js' : 'js/jwplayer.js';

$theKeys = array( '#isMobile#', '#userId#', '#username#', '#password#',
                  '#section#', '#page#', '#name#', '#urlAbs#', '#releaseCampus#',
                  '#userType#', '#jwPlayer#', '#isB2B#' );

$theValues = array( $isMobile, $theUserId, $theMail, $thePassword,
                    $section, $page, $theName, $urlAbsolute, $theReleaseCampus,
                    $userType, $jwPlayer, $isB2B );

// print output course
$theBuffer = str_replace($theKeys, $theValues, $buffer);
echo $theBuffer;

// in case of teacher help direct link
if (!empty($teacherImg)) {
    ?>
    <script>
        $(window).resize(function() {
            repositionCourseTeacherHelpLink();
        });

        $(window).scroll(function() {
            repositionCourseTeacherHelpLink();
        });

        function repositionCourseTeacherHelpLink() {

            var $container = $("#courseTeacherHelpLink");
            var bodyTopPosition = 825;

            $container.addClass("fixed");

            if ($container.offset().top > bodyTopPosition) {
                $container.removeClass("fixed");
            }
        }
    </script>
    <div id="courseTeacherHelpLink" class="fixed">
        <div class="courseTeacherHelpPanel" style="display: none;">
            <h3><?= Yii::t('mainApp', 'courseTeacherHelpTitle'); ?></h3>
            <a href="<?= Yii::app()->createUrl('contact/index') ?>" class="courseTeacherBTN"><?= Yii::t('mainApp', 'courseTeacherHelpLink1'); ?></a>
            <a href="<?= Yii::app()->createUrl('message/inbox/inbox') ?>" class="courseTeacherBTN"><?= Yii::t('mainApp', 'courseTeacherHelpLink2'); ?></a>
        </div>
        <img src="<?= Yii::app()->theme->baseUrl ?>/media/images/teachers/<?= $teacherImg ?>">
    </div>
    <?php
}

// global variables from php that course needs to access
?>
<script>
    var _glossaryUrl = "<?= Yii::app()->params['glossaryUrl'] ?>";
    var _transactionsUrl = "<?= Yii::app()->params['transactionsUrl'] ?>";
</script>

<?php
//#ABAWEBAPPS-693
$stParams = array(
  "levelId" => array("value" => "cooladataLevelId", "type" => "js"),
  "unitId" => array("value" => ($unit * 1), "type" => "phpInteger")
);
$sCooladata = HeCooladata::createDefaultEvent(HeCooladata::EVENT_NAME_ENTERED_UNIT, true, $stParams);
Yii::app()->clientScript->registerScript(md5($sCooladata), $sCooladata, CClientScript::POS_READY);
?>

