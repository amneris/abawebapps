
<?php
// User is Premium and has registered through V1 testAB block
if (Yii::app()->user->getIdPartnerCurrent() == BCSPARTNERID) {
    $cssClass = "ABAPremium";
} else {
    $cssClass = "ABA";
}
?>
<!--
 div that gives information how to call support for free
-->
<div class="support <?php echo $cssClass ?>" style="display:none;">
    <div class="cmp_ContainerFeedBack">
        <div class="cmp_title">
            <?php

            if ($country == COUNTRY_PORTUGAL || $country == COUNTRY_FRANCE || $country == COUNTRY_ITALY) {
                echo $callUs4Free;
            } else {
                echo $callUs;
            }
            ?>
        </div>
        <div class="cmp_telf">

<?php

$supportTelephoneNumberToDisplay = $prefix . " ". $telf;

switch ($country) {
    case COUNTRY_FRANCE:
        $supportTelephoneNumberToDisplay = Yii::app()->config->get("SUPPORT_NUMBER_FRANCE");
        break;
    case COUNTRY_ITALY:
        $supportTelephoneNumberToDisplay = Yii::app()->config->get("SUPPORT_NUMBER_ITALY");
        break;
    case COUNTRY_SPAIN:
        $supportTelephoneNumberToDisplay = Yii::app()->config->get("SUPPORT_NUMBER_SPAIN");
        break;
    case COUNTRY_BRAZIL:
        $supportTelephoneNumberToDisplay = Yii::app()->config->get("SUPPORT_NUMBER_BRAZIL");
        break;
}
echo "<span class=\"number\">" . $supportTelephoneNumberToDisplay;

//now write out schedule for free support
echo "</span></div><br /><div class='info_contact_free'>" . $callUsM2F;
?>
        </div>
        <div class="ico_telf"></div>
        <div class="cmp_body">
            <?php
            echo $consultant;
            ?>
        </div>
    </div>
</div>
