<?php
$bLogin = false;
if (isset(Yii::app()->user->id) AND is_numeric(Yii::app()->user->id)) {
    $bLogin = true;
}
?>
<footer>
    <section class="ab-social-block">
        <p><?php echo Yii::t('mainApp', 'SÍGUENOS EN:') ?></p>
        <ul class="ab-share-list">
            <li class="twit"><a href="https://twitter.com/abaenglish" target="_blank"></a></li>
            <li class="face"><a href="http://www.facebook.com/ABAEnglish?v=wall" target="_blank"></a></li>
            <li class="yout"><a href="http://www.youtube.com/user/abaenglish" target="_blank"></a></li>
            <li class="goog"><a href="https://plus.google.com/110431215339604931646/posts" target="_blank"></a></li>
            <li class="blog"><a href="http://<?php echo Yii::app()->config->get("URL_DOMAIN_WEB"); ?>/blog/<?php
                if ($bLogin) {
                    if (Yii::app()->user->getLanguage() == 'it') {
                        echo "it";
                    } elseif (Yii::app()->user->getLanguage() == 'es') {
                        echo "es";
                    } else {
                        echo "?en";
                    }
                } else {
                    echo "es";
                }
                ?>" target="_blank"><span
                <?php
                if ($bLogin) {
                    if (Yii::app()->user->getLanguage() == 'ru') {
                        echo "class='iconABABlogLayout_RU'></span></a>";
                    } else {
                        echo "class='iconABABlogLayout'></span></a>";
                    }
                } else {
                    echo "class='iconABABlogLayout'></span></a>";
                }
                ?>
            </li>
        </ul>
    </section>

    <?php if ($bLogin): ?>

        <section class="ab-main-footer hidden-xs">
            <div class="container">
                <ul class="ab-footer-list">
                    <li>
                        <span>Campus</span>
                        <ul class="sub-menu">
                            <li><a
                                  href="<?php echo Yii::app()->createUrl("/site/index") ?>"><?php echo Yii::t('mainApp',
                                      'Inicio') ?></a></li>
                            <li><a
                                  href="<?php echo Yii::app()->createUrl("/videoclass/index") ?>"><?php echo Yii::t('mainApp',
                                      'Videoclases') ?></a></li>
                            <li><a
                                  href="<?php echo Yii::app()->createUrl("/course/index") ?>"><?php echo Yii::t('mainApp',
                                      'Cursos') ?></a></li>
                            <li><a
                                  href="<?php echo Yii::app()->createUrl("/grammar/index") ?>"><?php echo Yii::t('mainApp',
                                      'Gramática') ?></a></li>
                            <li><a
                                  href="<?php echo Yii::app()->createUrl("/readytowork/index") ?>"><?php echo Yii::t('mainApp',
                                      'Escucha, graba y compara') ?></a></li>
                            <li><a
                                  href="<?php echo Yii::app()->createUrl("/message/inbox/inbox") ?>"><?php echo Yii::t('mainApp',
                                      'Mensajes') ?></a></li>
                        </ul>
                    </li>
                    <li>
                        <span><?php echo Yii::t('mainApp', 'Tu ABA') ?></span>
                        <ul class="sub-menu">
                            <li><a
                                  href="<?php echo Yii::app()->createUrl("profile/index") ?>"><?php echo Yii::t('mainApp',
                                      'Mi cuenta') ?></a></li>
                            <li><a id="aLinkFootLogout"
                                   href="<?php echo Yii::app()->createUrl('site/logout') ?>"><?php echo Yii::t('mainApp',
                                      'Salir') ?></a></li>
                        </ul>
                    </li>
                    <li>
                        <span><?php echo Yii::t('mainApp', 'Servicios') ?></span>
                        <ul class="sub-menu">
                            <li><a
                                  href="<?php echo Yii::app()->createUrl("plans/abapremium") ?>"><?php echo Yii::t('mainApp',
                                      'ABA Premium') ?></a></li>
                            <li><a
                                  href="<?php echo Yii::app()->createUrl("profile/certificates") ?>"><?php echo Yii::t('mainApp',
                                      'Certificados') ?></a></li>
                            <li><a
                                  href="<?php echo Yii::app()->createUrl("plans/plans") ?>"><?php echo Yii::t('mainApp',
                                      'Planes') ?></a></li>
                        </ul>
                    </li>
                    <li>
                        <span><?php echo Yii::t('mainApp', 'Soporte') ?></span>
                        <ul class="sub-menu">
                            <li><a
                                  href="<?php echo Yii::app()->createUrl('contact/index') ?>"><?php echo Yii::t('mainApp',
                                      'Contacto') ?></a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('help/index') ?>"><?php echo Yii::t('mainApp',
                                      'Ayuda') ?></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </section>

        <section class="ab-main-footer-mobile visible-xs">
            <div class="container">
                <ul class="ab-footer-list">
                    <li><a href="<?php echo Yii::app()->createUrl("/site/index") ?>"><?php echo Yii::t('mainApp',
                              'Inicio') ?></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl("/videoclass/index") ?>"><?php echo Yii::t('mainApp',
                              'Videoclases') ?></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl("/course/index") ?>"><?php echo Yii::t('mainApp',
                              'Cursos') ?></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl("/grammar/index") ?>"><?php echo Yii::t('mainApp',
                              'Gramática') ?></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl("/readytowork/index") ?>"><?php echo Yii::t('mainApp',
                              'Escucha, graba y compara') ?></a></li>
                    <li><a
                          href="<?php echo Yii::app()->createUrl("/message/inbox/inbox") ?>"><?php echo Yii::t('mainApp',
                              'Mensajes') ?></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl("profile/index") ?>"><?php echo Yii::t('mainApp',
                              'Mi cuenta') ?></a></li>
                    <li><a id="aLinkFootLogout"
                           href="<?php echo Yii::app()->createUrl('site/logout') ?>"><?php echo Yii::t('mainApp',
                              'Salir') ?></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl("plans/abapremium") ?>"><?php echo Yii::t('mainApp',
                              'ABA Premium') ?></a></li>
                    <li><a
                          href="<?php echo Yii::app()->createUrl("profile/certificates") ?>"><?php echo Yii::t('mainApp',
                              'Certificados') ?></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl("plans/plans") ?>"><?php echo Yii::t('mainApp',
                              'Planes') ?></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl('contact/index') ?>"><?php echo Yii::t('mainApp',
                              'Contacto') ?></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl('help/index') ?>"><?php echo Yii::t('mainApp',
                              'Ayuda') ?></a></li>
                </ul>
            </div>
        </section>

        <section class="ab-legal-footer hidden-xs">
            <div class="container">
                <ul>
                    <li><a href="<?php echo Yii::app()->createUrl('terms/index',
                          array()); ?>"><?php echo Yii::t('mainApp', 'Condiciones generales') ?></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl('privacy/index') ?>"><?php echo Yii::t('mainApp',
                              'Política de privacidad'); ?></a></li>
                </ul>
            </div>
        </section>

    <?php else: ?>
        <section class="ab-main-footer hidden-xs">
            <div class="container">
                <ul class="ab-footer-list footer-nologin">
                    <li>
                        <span><a href="<?= Yii::t('mainApp', 'loginSite_footer1Link') ?>"><?= Yii::t('mainApp',
                                  'loginSite_footer1') ?></a></span>
                    </li>
                    <li>
                        <span><a href="<?= Yii::t('mainApp', 'loginSite_footer2Link') ?>"><?= Yii::t('mainApp',
                                  'loginSite_footer2') ?></a></span>
                    </li>
                    <li>
                        <span><a href="<?= Yii::t('mainApp', 'loginSite_footer3Link') ?>"><?= Yii::t('mainApp',
                                  'loginSite_footer3') ?></a></span>
                    </li>
                    <li>
                        <span><a href="<?= Yii::t('mainApp', 'loginSite_footer4Link') ?>"><?= Yii::t('mainApp',
                                  'loginSite_footer4') ?></a></span>
                    </li>
                </ul>
            </div>
        </section>

    <?php endif; ?>

</footer>

<input type="hidden" id="tradUrl" value="<?php echo Yii::app()->createUrl("about/traduccionesJS") ?>"/>
