<?php

$cooladataSdkApiKey;
try {
    $cooladataSdkApiKey = Yii::app()->config->get("COOLADATA_SDK_API_KEY");
} catch (Exception $e) {
    unset($cooladataSdkApiKey);
}

$sCooladataJsKey = " var COOLADATA_SDK_API_KEY = ''; ";

if (isset($cooladataSdkApiKey)) {
    $sCooladataJsKey = " var COOLADATA_SDK_API_KEY = '" . $cooladataSdkApiKey . "'; ";
}

$sCooladataJsDebug = " var COOLADATA_DEBUG_ENABLE = ''; ";
try {
    $sCooladataJsDebug = " var COOLADATA_DEBUG_ENABLE = '" . Yii::app()->config->get("COOLADATA_DEBUG_ENABLE") . "'; ";
}
catch (Exception $e) { }

Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/assets/out/cooladata-core.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScript(md5($sCooladataJsKey), $sCooladataJsKey, CClientScript::POS_HEAD);
echo '<script type="text/javascript" src="' . Yii::app()->theme->baseUrl . '/assets/out/cooladata.js' . '"></script>';

$sCooladataInit = HeCooladata::init();
Yii::app()->clientScript->registerScript(md5($sCooladataInit), $sCooladataInit, CClientScript::POS_HEAD);

//#ABAWEBAPPS-597
if (isset(Yii::app()->user->cooladataUserRegister) AND Yii::app()->user->cooladataUserRegister == COOLDATA_USER_REGISTER) {

    Yii::app()->user->cooladataUserRegister = "";
    Yii::app()->user->cooladataUserLogin = ""; //!important

    echo HeCooladata::createEventLoggedOrRegisterIn(HeCooladata::EVENT_NAME_USER_REGISTERED);
} elseif (isset(Yii::app()->user->cooladataUserLogin) AND Yii::app()->user->cooladataUserLogin == COOLDATA_USER_LOGIN) {

    Yii::app()->user->cooladataUserLogin = "";

    echo HeCooladata::createEventLoggedOrRegisterIn(HeCooladata::EVENT_NAME_LOGGED_IN);
}

Yii::app()->clientScript->registerScript(md5($sCooladataJsDebug), $sCooladataJsDebug, CClientScript::POS_HEAD);

$sCooladata = HeCooladata::createDefaultEventForSendToTracker();
Yii::app()->clientScript->registerScript(md5($sCooladata), $sCooladata, CClientScript::POS_HEAD);
