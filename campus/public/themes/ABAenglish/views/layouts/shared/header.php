<?php
$role=FREE;
if(isset(Yii::app()->user->role) ){
    $role=Yii::app()->user->role;
}
?>

<header class="ab-header">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 text-center ab-logo-wrapper">
                <?php
                $imagePath = HeMixed::getUrlPathImg("logoAba.png");
                $imageTag = CHtml::image($imagePath, "Logo ABA English");
                echo CHtml::link($imageTag, Yii::app()->createUrl('site/index'));
                ?>
            </div>

            <div class="col-sm-9">
                <h1 class="slogan hidden-xs">
                    <?php
                    if (empty($this->headerTitle)) {
                        echo Yii::t('mainApp', '¿Listo para aprender inglés?');
                        echo '<span>'.Yii::t('mainApp', 'payments_subtitle').'</span>';
                    } else {
                        echo $this->headerTitle;
                    }
                    ?>
                </h1>
            </div>
        </div>
    </div>
</header>