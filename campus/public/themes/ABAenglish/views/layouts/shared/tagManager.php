<?php
$id;
try {
    $id = Yii::app()->config->get("TAG_MANAGER_CONTAINER_ID");
} catch (Exception $e) {
    unset($id);
}
if (isset($id)) { ?>

    <!-- Google Tag Manager Data Layer -->
    <script>
        dataLayer = [{
            'userId': '<?php echo Yii::app()->dataLayer->getUserId() ?>',
            'partnerSource': '<?php echo Yii::app()->dataLayer->getPartnerSource() ?>',
            'email': '<?php echo Yii::app()->dataLayer->getEmail() ?>',
            'device': '<?php echo Yii::app()->dataLayer->getDevice() ?>',
            'paymentValue': '<?php echo Yii::app()->dataLayer->getPaymentValue() ?>',
            'promoCode': '<?php echo Yii::app()->dataLayer->getPromoCode() ?>',
            'discount': '<?php echo Yii::app()->dataLayer->getDiscount() ?>',
            'currency': '<?php echo Yii::app()->dataLayer->getCurrency() ?>',
            'idProduct': '<?php echo Yii::app()->dataLayer->getIdProduct() ?>',
            'paySuppExtId': '<?php echo Yii::app()->dataLayer->getPaySuppExtId() ?>',
            'idPayment': '<?php echo Yii::app()->dataLayer->getIdPayment() ?>',
            'quantity': '<?php echo Yii::app()->dataLayer->getQuantity() ?>',
            'productPlan': '<?php echo Yii::app()->dataLayer->getProductPlan(); ?>',
            'idSite': '<?php echo Yii::app()->dataLayer->getIdSite(); ?>',
        }];
    </script>
    <!-- End Google Tag Manager Data Layer -->

    <!-- Google Tag Manager Initialisation --> 
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=<?php echo $id ?>"   height="0" width="0"
                style="display:none;visibility:hidden"></iframe>
    </noscript> 
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});
            var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', '<?php echo $id ?>');
    </script> 
    <!-- End Google Tag Manager Initialisation -->


    <!-- push events that need firing to google tag manager data layer -->
    <script>
        // send event to google tag manager if user login
        // is customer lead (i.e, is first user login and event has not been sent previously)
        eventLead();

        function eventLead() {

            var userCameFromFirstTimeRegistration = isUserComingFromFirstTimeRegistration();
            if (userCameFromFirstTimeRegistration === 1){
                pushLeadEvent();
            }
        }

        function isUserComingFromFirstTimeRegistration(){
            <?php
                $userCameFromFirstTimeRegistration = Yii::app()->dataLayer->getComesFromRegisterUserFirstTime();
                if(!isset($userCameFromFirstTimeRegistration)){
                    $userCameFromFirstTimeRegistration = 0;
                }
            ?>
            var userCameFromFirstTimeRegistration = <?php echo $userCameFromFirstTimeRegistration?>;
            return userCameFromFirstTimeRegistration;
        }

        function pushLeadEvent(){
            // if lead event has not previously been sent to google tag manager then send it
            console.log("sending lead event");
            dataLayer.push({'event': 'lead'});
        }
    </script>

<?php } ?>
