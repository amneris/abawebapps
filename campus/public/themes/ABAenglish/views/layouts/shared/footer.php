<?php
$moUser = new AbaUser();
$moUser->getUserById(Yii::app()->user->getId());
$paySuppExtId = PaymentFactory::whichCardGatewayPay($moUser);
?>
<footer>
    <section class="ab-social-block">
        <p><?php echo Yii::t('mainApp', 'SÍGUENOS EN:') ?></p>
        <ul class="ab-share-list">
            <li class="twit"><a href="https://twitter.com/abaenglish" target="_blank"></a></li>
            <li class="face"><a href="http://www.facebook.com/ABAEnglish?v=wall" target="_blank"></a></li>
            <li class="yout"><a href="http://www.youtube.com/user/abaenglish" target="_blank"></a></li>
            <li class="goog"><a href="https://plus.google.com/110431215339604931646/posts" target="_blank"></a></li>
            <li class="blog"><a href="http://<?php echo Yii::app()->config->get("URL_DOMAIN_WEB"); ?>/blog/<?php
                if (Yii::app()->user->getLanguage() =='it'){
                    echo  "it";
                }
                elseif(Yii::app()->user->getLanguage() =='es'){
                    echo  "es";
                }
                else{
                    echo "?en";
                }
                ?>" target="_blank"><span
                <?php

                if (Yii::app()->user->getLanguage() == 'ru') {
                    echo "class='iconABABlogLayout_RU'></span></a>";
                } else {
                    echo "class='iconABABlogLayout'></span></a>";
                }
                ?>
            </li>
        </ul>
    </section>
    <section class="ab-main-footer hidden-xs">
        <div class="container">
            <ul class="ab-footer-list">
                <li>
                    <span>Campus</span>
                    <ul class="sub-menu">
                        <li><a href="<?php echo Yii::app()->createUrl("/site/index") ?>"><?php echo Yii::t('mainApp', 'Inicio') ?></a></li>
                        <li><a href="<?php echo Yii::app()->createUrl("/videoclass/index") ?>"><?php echo Yii::t('mainApp', 'Videoclases') ?></a></li>
                        <li><a href="<?php echo Yii::app()->createUrl("/course/index") ?>"><?php echo Yii::t('mainApp', 'Cursos') ?></a></li>
                        <li><a href="<?php echo Yii::app()->createUrl("/grammar/index") ?>"><?php echo Yii::t('mainApp', 'Gramática') ?></a></li>
                        <li><a href="<?php echo Yii::app()->createUrl("/readytowork/index") ?>"><?php echo Yii::t('mainApp', 'Escucha, graba y compara') ?></a></li>
                        <li><a href="<?php echo Yii::app()->createUrl("/message/inbox/inbox") ?>"><?php echo Yii::t('mainApp', 'Mensajes') ?></a></li>
                    </ul>
                </li>
                <li>
                    <span><?php echo Yii::t('mainApp', 'Tu ABA') ?></span>
                    <ul class="sub-menu">
                        <li><a href="<?php echo Yii::app()->createUrl("profile/index") ?>"><?php echo Yii::t('mainApp', 'Mi cuenta') ?></a></li>
                        <li><a id="aLinkFootLogout" href="<?php echo Yii::app()->createUrl('site/logout') ?>"><?php echo Yii::t('mainApp', 'Salir') ?></a></li>
                    </ul>
                </li>
                <li>
                    <span><?php echo Yii::t('mainApp', 'Servicios') ?></span>
                    <ul class="sub-menu">
                        <li><a href="<?php echo Yii::app()->createUrl("plans/abapremium") ?>"><?php echo Yii::t('mainApp', 'ABA Premium') ?></a></li>
                        <li><a href="<?php echo Yii::app()->createUrl("profile/certificates") ?>"><?php echo Yii::t('mainApp', 'Certificados') ?></a></li>
                        <li><a href="<?php echo Yii::app()->createUrl("plans/plans") ?>"><?php echo Yii::t('mainApp', 'Planes') ?></a></li>
                    </ul>
                </li>
                <li>
                    <span><?php echo Yii::t('mainApp', 'Soporte') ?></span>
                    <ul class="sub-menu">
                        <li><a href="<?php echo Yii::app()->createUrl('contact/index') ?>"><?php echo Yii::t('mainApp', 'Contacto') ?></a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('help/index') ?>"><?php echo Yii::t('mainApp', 'Ayuda') ?></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </section>

    <section class="ab-main-footer-mobile visible-xs">
        <div class="container">
            <ul class="ab-footer-list">
                <li><a href="<?php echo Yii::app()->createUrl("/site/index") ?>"><?php echo Yii::t('mainApp', 'Inicio') ?></a></li>
                <li><a href="<?php echo Yii::app()->createUrl("/videoclass/index") ?>"><?php echo Yii::t('mainApp', 'Videoclases') ?></a></li>
                <li><a href="<?php echo Yii::app()->createUrl("/course/index") ?>"><?php echo Yii::t('mainApp', 'Cursos') ?></a></li>
                <li><a href="<?php echo Yii::app()->createUrl("/grammar/index") ?>"><?php echo Yii::t('mainApp', 'Gramática') ?></a></li>
                <li><a href="<?php echo Yii::app()->createUrl("/readytowork/index") ?>"><?php echo Yii::t('mainApp', 'Escucha, graba y compara') ?></a></li>
                <li><a href="<?php echo Yii::app()->createUrl("/message/inbox/inbox") ?>"><?php echo Yii::t('mainApp', 'Mensajes') ?></a></li>
                <li><a href="<?php echo Yii::app()->createUrl("profile/index") ?>"><?php echo Yii::t('mainApp', 'Mi cuenta') ?></a></li>
                <li><a id="aLinkFootLogout" href="<?php echo Yii::app()->createUrl('site/logout') ?>"><?php echo Yii::t('mainApp', 'Salir') ?></a></li>
                <li><a href="<?php echo Yii::app()->createUrl("plans/abapremium") ?>"><?php echo Yii::t('mainApp', 'ABA Premium') ?></a></li>
                <li><a href="<?php echo Yii::app()->createUrl("profile/certificates") ?>"><?php echo Yii::t('mainApp', 'Certificados') ?></a></li>
                <li><a href="<?php echo Yii::app()->createUrl("plans/plans") ?>"><?php echo Yii::t('mainApp', 'Planes') ?></a></li>
                <li><a href="<?php echo Yii::app()->createUrl('contact/index') ?>"><?php echo Yii::t('mainApp', 'Contacto') ?></a></li>
                <li><a href="<?php echo Yii::app()->createUrl('help/index') ?>"><?php echo Yii::t('mainApp', 'Ayuda') ?></a></li>
            </ul>
        </div>
    </section>

    <section class="ab-legal-footer hidden-xs">
        <div class="container">
            <ul>
                <li><a href="<?php echo Yii::app()->createUrl('terms/index', array("paySuppExtId"=>$paySuppExtId)) ?>"><?php echo Yii::t('mainApp', 'Condiciones generales') ?></a></li>
                <li><a href="<?php echo Yii::app()->createUrl('privacy/index') ?>"><?php echo Yii::t('mainApp', 'Política de privacidad') ?></a></li>
            </ul>
        </div>
    </section>
</footer>

<?php
/*--CONVERSION OF USERS TO FREE, AFTER FIRST LOGIN--------------------------------------------*/
/*-------------------------------------------------------------------------------------------*/
HeAnalytics::scriptRegisterUserConversionFooterDoubleOptIn();
/*-------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------*/
?>
    <input type="hidden" id="tradUrl" value="<?php echo Yii::app()->createUrl("about/traduccionesJS") ?>"/>
<?php
//member get member - activation of premium period popup
if (Yii::app()->user->getState('showModal_popupMgmActivatePremium')) {
    Yii::app()->user->setState('showModal_popupMgmActivatePremium',
      false); // avoid that appears again when refreshing the page
    Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/modal.css');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery-ui.custom.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/WelcomeWorkflow.js?v=1');
    ?>
    <div id="welcomeForm" data-close>
        <?php $this->renderPartial('//welcome/mgmActivatePremium'); ?>
    </div>
    <?php
}
?>