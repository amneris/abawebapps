<?php

/* AB TEST: Control shows Customer Support (CS) block to FREE users, Variation 1 (V1) shows CTA for switching to Premium
/*          to get CS. The CS block will always show for Premium users and the V1 will not. AB test only work for Free
/*          users */

/* CONTROL */

/* This block should also be always shown for Premium users as it is controlled by the BackOffice maintenance and it's
/* logic is handled by HeContactSupportTel for this case alone */ ?>


<?php
    // TEST AB //
    $this->renderPartial('//layouts/shared/bannerCallSupport', array(
    'country'=>$country,
    'callUs'=>$callUs,
    'callUs4Free'=>$callUs4Free,
    'prefix'=>$prefix,
    'telf'=>$telf,
    'consultant'=>$consultant,
    'callUsM2F'=>$callUsM2F
    ));
?>


<?php
// TEST AB //
$this->renderPartial('//layouts/shared/bannerUpgradeToPremiumForFreeCallSuppot', array(
    'empezarPremium' => $empezarPremium,
    'paraObtener' => $paraObtener,
    'atencionGratis' => $atencionGratis,
    'personalizacion' => $personalizacion,
    'planEstudios' => $planEstudios,
    'atnLaF' => $atnLaF
));
?>