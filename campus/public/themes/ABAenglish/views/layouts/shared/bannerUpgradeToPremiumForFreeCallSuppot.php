<?php
/* VARIATION 1 (div for panel advertising that premium users have access to free telephone support) */

/* This will not be shown for Premium users */

if (Yii::app()->user->role == FREE) {
    ?>
    <div class="support ABB tel-bkg" style="display:none;">
        <div class="cmp_ContainerFeedBack">
            <a id="support-premium" class="subscribeteABAPremiumButton aba-button support-abb"
               href="<?php echo Yii::app()->createUrl(LAND_PAYMENT); ?>/idpartner/300277">
                <div><?php echo $empezarPremium ?></div>
            </a>

            <div class="cmp_body"><?php echo $paraObtener ?>
                <br>
                <b><?php echo $atencionGratis ?></b>
                <br>
                <?php echo $personalizacion ?>
                <br>
                <?php echo $planEstudios ?>
            </div>
            <div class='info_contact_free'>
                <?php echo $atnLaF ?>
                <br/>
                <br/>
            </div>
        </div>
    </div>
    <?php

}

?>