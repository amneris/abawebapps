<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<?php echo Yii::app()->language; ?>">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="<?php echo Yii::app()->language; ?>" />
        <?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
        <?php HeAnalytics::includeScriptVWO(); ?>
        <script type="application/javascript">var delC=function(e){document.cookie=e+"=;expires=Thu, 01 Jan 1970 00:00:01 GMT;"};delC("go");</script>
        <?php $this->renderPartial('//layouts/shared/cooladataManager'); ?>
        <?php $this->renderPartial('//layouts/shared/newrelic'); ?>
    </head>
    <body>
            <div style="display: none; ">
            <?php $this->renderPartial('//layouts/shared/tagManager'); ?>
            </div>
            <?php echo $content; ?>
            <div class="clearfix"></div>
    </body>
</html>
