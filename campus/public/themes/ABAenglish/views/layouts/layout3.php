<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="<?php echo Yii::app()->language; ?>" />
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/main.css'); ?>
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/fonts.css'); ?>
        <?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/dropDownListImplement.js'); ?>

        <?php HeAnalytics::includeScriptsCommonAnalyitics(); ?>
        <title>
            <?php echo CHtml::encode($this->pageTitle); ?>
        </title>
        <script type="application/javascript">var delC=function(e){document.cookie=e+"=;expires=Thu, 01 Jan 1970 00:00:01 GMT;"};delC("go");</script>
    </head>
    <body>
        <div class="container" id="page">
        <?php $this->renderPartial('//layouts/headerColumn2'); ?>
            <?php echo $content; ?>
            <div class="clearfix"></div>
            <div id="modalWindow"></div>
        </div>
        <?php $this->renderPartial('//layouts/footer'); ?>
    </body>
</html>