<?php
$role=FREE;
if(isset(Yii::app()->user->role) ){
    $role=Yii::app()->user->role;
}
?>

<div id="header">
    <div class="clearfix"></div>
    <?php
        $imagePath = HeMixed::getUrlPathImg("logoAba.png");
        $imageTag = CHtml::image($imagePath, "Logo ABA English");
        echo CHtml::link($imageTag, Yii::app()->createUrl('site/index'), array('class' => 'cmp_logoImg left'));
    ?>
    <div class="cmp_subheader left">
        <div class="cmp_anuncioColumn1">
            <?php echo Yii::t('mainApp', '¿Listo para aprender inglés?') ?>
            <?php echo '<span style="color:#92908a;font-style:italic;margin-left:10px">'.Yii::t('mainApp', 'payments_subtitle').'</span>' ?>
        </div>
    </div>
</div>
<div class="clearfix"></div>