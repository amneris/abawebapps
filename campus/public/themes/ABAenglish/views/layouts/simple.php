<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <meta name="language" content="<?php echo Yii::app()->language; ?>" />
    <meta charset="UTF-8">
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/assets/out/basic.css'); ?>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <script>var delC=function(e){document.cookie=e+"=;expires=Thu, 01 Jan 1970 00:00:01 GMT;"};delC("go");</script>
</head>

<body>

<header>
    <?php
    $imagePath = HeMixed::getUrlPathImg("logo_dark.png");
    $imageTag = CHtml::image($imagePath, "Logo ABA English", array('class'=>'logo'));
    echo $imageTag;
    ?>
</header>

<?php echo $content; ?>

<footer>
    <ul class="footer-nav">
        <li><a href="<?= Yii::t('mainApp', 'loginSite_footer1Link') ?>"><?= Yii::t('mainApp', 'loginSite_footer1') ?></a></li>
        <li><a href="<?= Yii::t('mainApp', 'loginSite_footer2Link') ?>"><?= Yii::t('mainApp', 'loginSite_footer2') ?></a></li>
        <li><a href="<?= Yii::t('mainApp', 'loginSite_footer3Link') ?>"><?= Yii::t('mainApp', 'loginSite_footer3') ?></a></li>
        <li><a href="<?= Yii::t('mainApp', 'loginSite_footer4Link') ?>"><?= Yii::t('mainApp', 'loginSite_footer4') ?></a></li>
    </ul>
</footer>
</body>
</html>
