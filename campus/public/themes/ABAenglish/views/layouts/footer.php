<?php
$moUser = new AbaUser();
$moUser->getUserById(Yii::app()->user->getId());
$paySuppExtId = PaymentFactory::whichCardGatewayPay($moUser);

// ** ZENDESK Help Center.
$zendeskHelpCenter = Yii::app()->config->get("ZENDESK_HELP_CENTER");
$zendeskHelpCenterABA = Yii::app()->config->get("ZENDESK_HELP_CENTER_ABA");
$zendeskHelpCenter = ($zendeskHelpCenterABA && str_replace('@abaenglish.com', '',
    $moUser->email) != $moUser->email) ? true : $zendeskHelpCenter;
$zendeskLanguage = (isset(Yii::app()->params['ZENDESK_LANGUAGETABLE'][Yii::app()->language])) ?
  Yii::app()->params['ZENDESK_LANGUAGETABLE'][Yii::app()->language] : Yii::app()->language;
// ** Zendesk only for english students.
//$zendeskHelpCenter = ($zendeskHelpCenter && (Yii::app()->language == 'en'));

?>
<div class="clearfix">
    <div id="footer">
    <div class="cmp_social">
        <div class="cmp_siguenos">
            <?php echo Yii::t('mainApp', 'SÍGUENOS EN:') ?>
        </div>
        <div class="cmp_socialIcons">
            <a href="https://twitter.com/abaenglish" target="_blank"><span class="iconTwitterLayout"></span></a>
            <a href="http://www.facebook.com/ABAEnglish?v=wall" target="_blank"><span class="iconFacebookLayout"></span></a>
            <a href="http://www.youtube.com/user/abaenglish" target="_blank"><span class="iconYouTubeLayout"></span></a>
            <a href="https://plus.google.com/110431215339604931646/posts" target="_blank"><span class="iconGooglePlusLayout"></span></a>
            <a href="http://<?php echo Yii::app()->config->get("URL_DOMAIN_WEB"); ?>/blog/<?php
            if (Yii::app()->user->getLanguage() =='it'){
                echo  "it";
            }
            elseif(Yii::app()->user->getLanguage() =='es'){
                echo  "es";
            }
            else{
                echo "?en";
            }
            ?>" target="_blank"><span
            <?php

            if (Yii::app()->user->getLanguage() == 'ru') {
                echo "class='iconABABlogLayout_RU'></span></a>";
            } else {
                echo "class='iconABABlogLayout'></span></a>";
            }
            ?>


        </div>
        <div class="cmp_webMap">
            <div class="cmp_subWebMap">
                <ul>
                    <li>Campus</li>
                    <li><a href="<?php echo Yii::app()->createUrl("/site/index") ?>"><?php echo Yii::t('mainApp', 'Inicio') ?></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl("/videoclass/index") ?>"><?php echo Yii::t('mainApp', 'Videoclases') ?></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl("/course/index") ?>"><?php echo Yii::t('mainApp', 'Cursos') ?></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl("/grammar/index") ?>"><?php echo Yii::t('mainApp', 'Gramática') ?></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl("/readytowork/index") ?>"><?php echo Yii::t('mainApp', 'Escucha, graba y compara') ?></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl("/message/inbox/inbox") ?>"><?php echo Yii::t('mainApp', 'Mensajes') ?></a></li>
                </ul>
                <ul>
                    <li><?php echo Yii::t('mainApp', 'Tu ABA') ?></li>
                    <li><a href="<?php echo Yii::app()->createUrl("profile/index") ?>"><?php echo Yii::t('mainApp', 'Mi cuenta') ?></a></li>
                    <li><a id="aLinkFootLogout" href="<?php echo Yii::app()->createUrl('site/logout') ?>"><?php echo Yii::t('mainApp', 'Salir') ?></a></li>
                </ul>
                <ul>
                    <li><?php echo Yii::t('mainApp', 'Servicios') ?></li>
                    <li><a href="<?php echo Yii::app()->createUrl("plans/abapremium") ?>"><?php echo Yii::t('mainApp', 'ABA Premium') ?></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl("profile/certificates") ?>"><?php echo Yii::t('mainApp', 'Certificados') ?></a></li>
                    <li><a href="<?php echo Yii::app()->createUrl("plans/plans") ?>"><?php echo Yii::t('mainApp', 'Planes') ?></a></li>

                </ul>
                <ul>
                    <li><?php echo Yii::t('mainApp', 'Soporte') ?></li>
                    <li><a href="<?php echo Yii::app()->createUrl('contact/index') ?>"><?php echo Yii::t('mainApp', 'Contacto') ?></a></li>

                    <?php if ($zendeskHelpCenter) { ?>
                        <li><a target="_blank" onclick="javascript: sendDefaultEventToTracker('<?php echo HeCooladata::EVENT_NAME_OPENED_HELP; ?>');" href="<?php echo 'https://'.Yii::app()->params['ZENDESK_DOMAIN'].'/hc/'.$zendeskLanguage.'/signin?return_to='.urlencode('https://'.Yii::app()->params['ZENDESK_DOMAIN'].'/hc/'.$zendeskLanguage); ?>"><?php echo Yii::t('mainApp', 'Ayuda') ?></a></li>
                    <?php } else { ?>
                        <li><a href="<?php echo Yii::app()->createUrl('help/index') ?>"><?php echo Yii::t('mainApp', 'Ayuda') ?></a></li>
                    <?php } ?>
                </ul>
                <ul>
                    <li><?php echo Yii::t('mainApp', 'Recursos') ?></li>
                    <li><span onclick="startDemoTestLevel()"><?php echo Yii::t('mainApp', 'level-test-footerMenu') ?></span></li>
                </ul>
            </div>
        </div>
        <div class="cmp_finalFooter">
            <ul>
                <li><a href="<?php echo Yii::app()->createUrl('terms/index', array("paySuppExtId"=>$paySuppExtId)) ?>"><?php echo Yii::t('mainApp', 'Condiciones generales') ?></a></li>
                <li><a href="<?php echo Yii::app()->createUrl('privacy/index') ?>"><?php echo Yii::t('mainApp', 'Política de privacidad') ?></a></li>
            </ul>
        </div>
    </div>
</div>
<?php
/*--CONVERSION OF USERS TO FREE, AFTER FIRST LOGIN--------------------------------------------*/
/*-------------------------------------------------------------------------------------------*/
HeAnalytics::scriptRegisterUserConversionFooterDoubleOptIn();
/*-------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------*/
?>
<input type="hidden" id="tradUrl" value="<?php echo Yii::app()->createUrl("about/traduccionesJS") ?>" />

<?php
//member get member - activation of premium period popup
if(Yii::app()->user->getState('showModal_popupMgmActivatePremium')) {
    Yii::app()->user->setState('showModal_popupMgmActivatePremium', false); // avoid that appears again when refreshing the page
    Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/modal.css');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jquery-ui.custom.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/WelcomeWorkflow.js?v=1');

    ?>
    <div id="welcomeForm" data-close>
        <?php $this->renderPartial('//welcome/mgmActivatePremium'); ?>
    </div>
<?php
}
?>