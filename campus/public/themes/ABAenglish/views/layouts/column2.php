<?php
/**
 * param int $hasAbaMoments (-1 when no access)
 */

$this->beginContent('//layouts/mainColumn2');

$isMobile = HeDetectDevice::isMobile();
if ($isMobile) {
    $readyToWorkUrl = 'javascript:campusUtils.openModal(\''.Yii::app()->createUrl('/modals/popupErrorDevice').'\', 700, 200)';
} else {
    $readyToWorkUrl = array('/readytowork/index');
}

$msgCounter = Yii::app()->getModule('message')->getCountUnreadedMessages(Yii::app()->user->getId());
if (!empty($msgCounter)) {
    $msgCounter=' ('.$msgCounter.')';
} else {
    $msgCounter=" ";
}

// fetching user
$user = new AbaUser();
$userId = Yii::app()->user->getId();
$user->getUserById($userId);

// ** ZENDESK Help Center.
$zendeskHelpCenter = Yii::app()->config->get("ZENDESK_HELP_CENTER");
$zendeskHelpCenterABA = Yii::app()->config->get("ZENDESK_HELP_CENTER_ABA");
$zendeskHelpCenter = ($zendeskHelpCenterABA && str_replace('@abaenglish.com', '',
    $user->email) != $user->email) ? true : $zendeskHelpCenter;
$zendeskLanguage = (isset(Yii::app()->params['ZENDESK_LANGUAGETABLE'][Yii::app()->language])) ?
  Yii::app()->params['ZENDESK_LANGUAGETABLE'][Yii::app()->language] : Yii::app()->language;
// ** Zendesk only for english students.
//$zendeskHelpCenter = ($zendeskHelpCenter && (Yii::app()->language == 'en'));

?>
<?php
    MAX_ALLOWED_LEVEL > Yii::app()->user->role ? $pruebaPremium = '<span class="pruebaPremiumMenu"> '.Yii::t('mainApp', '¡Pruébalo ya!').'</span>' : $pruebaPremium = "";
?>

<div id="styleWrapper_<?php echo $this->getAction()->getId(); ?>">
	<div id="divLeftMenu" class="cmp_blockLeft left">
            <?php
                // start campus left menu
                $arItems = array(
                  array('label' => Yii::t('mainApp', 'Inicio'),
                    'url' => array('/site/index'),
                    'linkOptions' => array('id' => 'idUlHome'),
                    'active' => ( ($this->getId() == 'site') && ($this->getAction()->getId() == 'index') ) ),
                  array('label' => '<span>'.Yii::t('mainApp', 'Curso_completo').'</span>',
                    'url' => array('/course/index'),
                    'linkOptions' => array('id' => 'idUlCourse'),
                    'active' => ( ($this->getId() == 'course') && ($this->getAction()->getId() == 'index') ) ));

                // append aba moments item when the user has it visible
                if ($this->hasAbaMoments > -1) {
                    $badgeNotification = '';

                    if ($this->hasAbaMoments) {
                        $badgeNotification = '<div class="badgeNotification">' . $this->hasAbaMoments . '</div>';
                    }

                    array_push($arItems,
                        array('label' => '<span>ABAmoments</span>' . $badgeNotification,
                            'url' => array('/abamoments/index'),
                            'linkOptions' => array('id' => 'idUlAbaMoments'),
                            'active' => ( ($this->getId() == 'abamoments') && ($this->getAction()->getId() == 'index') ) )
                    );
                }

                // append the following menu items
                $arItems = array_merge($arItems,
                    array(
                        array('label' => Yii::t('mainApp', 'Video Clases'),
                            'url' => array('/videoclass/index'),
                            'linkOptions' => array('id' => 'idUlVideos'),
                            'active' => ( ($this->getId() == 'videoclass') && ($this->getAction()->getId() == 'index') ) ),
                        array('label' => Yii::t('mainApp', 'Gramática interactiva'),
                            'url' => array('/grammar/index'),
                            'linkOptions' => array('id' => 'idUlGrammar'),
                            'active' => ( ($this->getId() == 'grammar') && ($this->getAction()->getId() == 'index') ) ),
                        array('label' => Yii::t('mainApp', 'Mensajes_al_profesor').$msgCounter,
                            'url' => array('/message/inbox/inbox' ),
                            'linkOptions' => array('id' => 'idUlMessages'),
                            'items' => array(
                                array('label' => '> '.Yii::t('mainApp', 'Mensajes recibidos').$msgCounter,
                                    'url' => array('/message/inbox/inbox' ),
                                    'linkOptions' => array('id' => 'idUlMessagesInbox'),
                                    'active' => ( ($this->getId() == 'inbox')  ),
                                    /*'itemOptions'=>array('class'=>'subMenuMessage')*/),
                                array('label' => '> '.Yii::t('mainApp', 'Mensajes_enviados'),
                                    'url' => array('/message/sent/sent' ),
                                    'linkOptions' => array('id' => 'idUlMessagesOutbox'),
                                    'active' => ( ($this->getId() == 'sent')  ),
                                )),
                            'active' => ((($this->getId() == 'history') && ($this->getAction()->getId() == 'history')) || (($this->getId() == 'view') && ($this->getAction()->getId() == 'view')) || (($this->getId() == 'compose') && ($this->getAction()->getId() == 'compose')) || (($this->getId() == 'inbox') && ($this->getAction()->getId() == 'inbox') ) || (($this->getId() == 'sent') && ($this->getAction()->getId() == 'sent') ))  ),
                    )
                );

                if ($zendeskHelpCenter) {
                    $arItems[] = array('label' => Yii::t('mainApp', 'Ayuda'),
                      'url' => 'https://'.Yii::app()->params['ZENDESK_DOMAIN'].'/hc/'.$zendeskLanguage.'/signin?return_to='.urlencode('https://'.Yii::app()->params['ZENDESK_DOMAIN'].'/hc/'.$zendeskLanguage),
                      'linkOptions' => array('target' => '_blank', 'onclick' => 'javascript: sendDefaultEventToTracker(\'' . HeCooladata::EVENT_NAME_OPENED_HELP . '\');'),
                    );
                } else {
                    $arItems[] = array('label' => Yii::t('mainApp', 'Ayuda'),
                      'url' => array('/help/index'),
                      'itemOptions' => array('class'=>'help'),
                      'items' => array(
                        array('label' => '> '.Yii::t('mainApp', 'Preguntas_frecuentes'),
                          'url' => array('/help/index' ),
                          'active' => ( ($this->getId() == 'help')  ),
                        ),
                        array('label' => '> '.Yii::t('mainApp', 'Configura tu ordenador'),
                          'url' => $readyToWorkUrl,
                          'active' => ( ($this->getId() == 'readytowork')  ),
                        ),

                        array('label' => '> '.Yii::t('mainApp', 'Contacto'),
                          'url' => array('/contact/index' ),
                          'active' => ( ($this->getId() == 'contact')  ),
                        )),
                      'active' => ( ($this->getId() == 'readytowork') || ($this->getId() == 'help') || ($this->getId() == 'contact'))
                    );
                }



                $this->widget('application.components.ABAMenu.ABAMenu', array('encodeLabel' => false,
                    'id' => "mainMenu",
                    'submenuHtmlOptions' => array(
                        'class' => 'dropdown-menu',
                    ),
                    'items' => $arItems
                    )
                );
            ?>
            <div class="clear"></div>

        <?php
        // member get member
        $mgm = Yii::app()->user->getMemberGetMember();
        if ($mgm !== false) {
            $daysDiff = 0;
            if ($mgm['expireDate'] !== null) {
                $daysDiff = HeDate::getDifferenceDateSQL($mgm['expireDate'], date('Y-m-d'), HeDate::DAY_SECS, false);
            }
            if ($mgm['expireDate'] !== null && $daysDiff <= 0) {
                // if the date has expired then it means that the user is free again, so we do nothing
            } else {
                $text = '';
                $button = false;
                if ($mgm['userId'] === null) {
                    $text = Yii::t('mainApp', 'mgm_invite');
                    $button = true;
                } elseif ($mgm['registered'] < MGM_REQUIRED_USERS) {
                    $missing = MGM_REQUIRED_USERS - $mgm['registered'];
                    if ($missing === 1) {
                        $text = Yii::t('mainApp', 'mgm_1_friend_missing');
                    } elseif ($missing >= 2 && $missing <= 4 && $user->langEnv == 'ru') {
                        $text = str_replace('#', $missing, Yii::t('mainApp', 'mgm_2_4_friend_missing_RU'));
                    } else {
                        $text = str_replace('#', $missing, Yii::t('mainApp', 'mgm_x_friends_missing'));
                    }
                    $button = true;
                } elseif ($mgm['expireDate'] !== null) {
                    if ($daysDiff === 1) {
                        $text = Yii::t('mainApp', 'mgm_1_day_expire');
                    } elseif ($daysDiff >= 2 && $daysDiff <= 4 && $user->langEnv == 'ru') {
                        $text = str_replace('#', $daysDiff, Yii::t('mainApp', 'mgm_2_4_days_expire_RU'));
                    } else {
                        $text = str_replace('#', $daysDiff, Yii::t('mainApp', 'mgm_x_days_expire'));
                    }
                    $button = true;
                }

                if ($button) {
                    ?>
                    <a class="leftMenu_mgm" class="button" href="<?php echo Yii::app()->createUrl('/profile/invite'); ?>">
                        <?php echo $text; ?>
                    </a>
                    <?php
                } else {
                    ?>
                    <div class="leftMenu_mgm">
                        <?php echo $text; ?>
                    </div>
                    <?php
                }

            }
        }

        //we check if the help contact support tel is enabled.

        if (HeContactSupportTel::includeContactSupportTel()) {
        /* BEGIN 6086 */

            $comUser = new UserCommon();
            $userType = $user->userType;
            $idPartnerCurrent = $user->idPartnerCurrent;
            $callUsM2F = Yii::t('mainApp', 'horarios');
            // TEST AB //
            $this->renderPartial('//layouts/shared/contactSupportAB', array(
                'comUser'=>$comUser,
                'userType'=>$userType,
                'idPartnerCurrent'=>$idPartnerCurrent,
                'country'=>$comUser->getCountryForPhonesByIp(Yii::app()->request->getUserHostAddress()),
                'callUs'=>Yii::t('mainApp', 'Llámanos'),
                'callUs4Free'=>Yii::t('mainApp', 'Llámanos_gratis'),
                'prefix'=>Yii::t('mainApp', 'pref'),
                'telf'=>Yii::t('mainApp', 'telf'),
                'consultant'=>Yii::t('mainApp', 'consultant'),
                'callUsM2F'=>$callUsM2F,
                'empezarPremium' => Yii::t('mainApp', 'Empezar_Premium'),
                'paraObtener' => Yii::t('mainApp', 'para_obtener'),
                'atencionGratis' => Yii::t('mainApp', 'atencion_gratis'),
                'personalizacion' => Yii::t('mainApp', 'personalizacion'),
                'planEstudios' => Yii::t('mainApp', 'plan_estudios'),
                'atnLaF' => Yii::t('mainApp', 'Atencion_de_LAF')
            ));
        }
/* ENDS 6086 */

        ?>
      <div>
          <p class="frase_fb marginBottom5"><?php echo Yii::t('mainApp', 'key_frase_in') ?></p>
          <a href="https://lnkd.in/ddUeAQJ" target="_blank" class="btn-linkedin-column" onclick="javascript: sendDefaultEventToTracker('<?php echo HeCooladata::EVENT_NAME_ADDED_LINKEDIN; ?>');">
              <i class="fa fa-linkedin"></i>
              <?php echo Yii::t('mainApp', 'btn_column_in') ?>
          </a>
      </div>
      <hr class="marginTop20">
            <div>
                <p class="frase_fb"><?php echo Yii::t('mainApp', 'key_frase_fb') ?></p>
                    <iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2FABAEnglish&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;font&amp;colorscheme=light&amp;action=like&amp;height=21" scrolling="no" frameborder="0" style="" allowTransparency="true"></iframe>
            </div>
	</div><!-- mainmenu -->

	<div id="divRightContent" class="cmp_blockRight right">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<?php $this->endContent(); ?>
