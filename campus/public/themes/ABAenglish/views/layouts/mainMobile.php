<!DOCTYPE HTML>
<html lang="<?php echo Yii::app()->language; ?>">
    <head>
        <?php  //HeAnalytics::getAnalyticsExperimentTag();?>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="<?php echo Yii::app()->language; ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/mobile/mobile.css'); ?>
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/mobile/flexslider.css'); ?>
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/fonts.css'); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/mobile/jquery.flexslider-min.js'); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/mobile/mobileFunctions.js'); ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function(){
                $('.flexslider').flexslider();
            });
        </script>

        <title>
            renderPartial  <?php echo CHtml::encode($this->pageTitle); ?>
        </title>
        <?php HeAnalytics::includeCommonMobileAnalytics(); ?>
        <script type="application/javascript">var delC=function(e){document.cookie=e+"=;expires=Thu, 01 Jan 1970 00:00:01 GMT;"};delC("go");</script>
        <?php $this->renderPartial('//layouts/shared/cooladataManager'); ?>
        <?php $this->renderPartial('//layouts/shared/newrelic'); ?>
    </head>
    <body>
        <div style="display: none; ">
        <?php $this->renderPartial('//layouts/shared/tagManager'); ?>
        </div>
        <?php echo $content; ?>
    </body>
</html>
