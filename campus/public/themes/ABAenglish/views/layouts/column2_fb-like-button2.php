<?php
$this->beginContent('//layouts/mainColumn2');

$isMobile = HeDetectDevice::isMobile();
if ($isMobile) {
    $readyToWorkUrl = 'javascript:campusUtils.openModal(\''.Yii::app()->createUrl('/modals/popupErrorDevice').'\', 700, 200)';
} else {
    $readyToWorkUrl = array('/readytowork/index');
}

$msgCounter = Yii::app()->getModule('message')->getCountUnreadedMessages(Yii::app()->user->getId());
if (!empty($msgCounter)) {
    $msgCounter=' ('.$msgCounter.')';
} else {
    $msgCounter=" ";
}

// fetching user
$user = new AbaUser();
$userId = Yii::app()->user->getId();
$user->getUserById($userId);

// ** ZENDESK Help Center.
$zendeskHelpCenter = Yii::app()->config->get("ZENDESK_HELP_CENTER");
$zendeskHelpCenterABA = Yii::app()->config->get("ZENDESK_HELP_CENTER_ABA");
$zendeskHelpCenter = ($zendeskHelpCenterABA && str_replace('@abaenglish.com', '',
    $user->email) != $user->email) ? true : $zendeskHelpCenter;
$zendeskLanguage = (isset(Yii::app()->params['ZENDESK_LANGUAGETABLE'][Yii::app()->language])) ?
  Yii::app()->params['ZENDESK_LANGUAGETABLE'][Yii::app()->language] : Yii::app()->language;
// ** Zendesk only for english students.
//$zendeskHelpCenter = ($zendeskHelpCenter && (Yii::app()->language == 'en'));

?>
<?php
    MAX_ALLOWED_LEVEL > Yii::app()->user->role ? $pruebaPremium = '<span class="pruebaPremiumMenu"> '.Yii::t('mainApp', '¡Pruébalo ya!').'</span>' : $pruebaPremium = "";
?>

<div id="styleWrapper_<?php echo $this->getAction()->getId(); ?>">
	<div id="divLeftMenu" class="cmp_blockLeft left">
            <?php
                $arItems = array(
                  array('label' => Yii::t('mainApp', 'Inicio'),
                    'url' => array('/site/index'),
                    'linkOptions' => array('id' => 'idUlHome'),
                    'active' => ( ($this->getId() == 'site') && ($this->getAction()->getId() == 'index') ) ),
                  array('label' => '<span>'.Yii::t('mainApp', 'Curso_completo').'</span>',
                    'url' => array('/course/index'),
                    'linkOptions' => array('id' => 'idUlCourse'),
                    'active' => ( ($this->getId() == 'course') && ($this->getAction()->getId() == 'index') ) ),
                  array('label' => Yii::t('mainApp', 'Video Clases'),
                    'url' => array('/videoclass/index'),
                    'linkOptions' => array('id' => 'idUlVideos'),
                    'active' => ( ($this->getId() == 'videoclass') && ($this->getAction()->getId() == 'index') ) ),
                  array('label' => Yii::t('mainApp', 'Gramática interactiva'),
                    'url' => array('/grammar/index'),
                    'linkOptions' => array('id' => 'idUlGrammar'),
                    'active' => ( ($this->getId() == 'grammar') && ($this->getAction()->getId() == 'index') ) ),
                  array('label' => Yii::t('mainApp', 'Mensajes_al_profesor').$msgCounter,
                    'url' => array('/message/inbox/inbox' ),
                    'linkOptions' => array('id' => 'idUlMessages'),
                    'items' => array(
                      array('label' => '> '.Yii::t('mainApp', 'Mensajes recibidos').$msgCounter,
                        'url' => array('/message/inbox/inbox' ),
                        'linkOptions' => array('id' => 'idUlMessagesInbox'),
                        'active' => ( ($this->getId() == 'inbox')  ),
                          /*'itemOptions'=>array('class'=>'subMenuMessage')*/),
                      array('label' => '> '.Yii::t('mainApp', 'Mensajes_enviados'),
                        'url' => array('/message/sent/sent' ),
                        'linkOptions' => array('id' => 'idUlMessagesOutbox'),
                        'active' => ( ($this->getId() == 'sent')  ),
                      )),
                    'active' => ((($this->getId() == 'history') && ($this->getAction()->getId() == 'history')) || (($this->getId() == 'view') && ($this->getAction()->getId() == 'view')) || (($this->getId() == 'compose') && ($this->getAction()->getId() == 'compose')) || (($this->getId() == 'inbox') && ($this->getAction()->getId() == 'inbox') ) || (($this->getId() == 'sent') && ($this->getAction()->getId() == 'sent') ))  ),
                );
                if ($zendeskHelpCenter) {
                    $arItems[] = array('label' => Yii::t('mainApp', 'Ayuda'),
                      'url' => 'https://'.Yii::app()->params['ZENDESK_DOMAIN'].'/hc/'.$zendeskLanguage.'/signin?return_to='.urlencode('https://'.Yii::app()->params['ZENDESK_DOMAIN'].'/hc/'.$zendeskLanguage),
                      'linkOptions' => array('target' => '_blank', 'onclick' => 'javascript: sendDefaultEventToTracker(\'' . HeCooladata::EVENT_NAME_OPENED_HELP . '\');'),
                    );
                } else {
                    $arItems[] = array('label' => Yii::t('mainApp', 'Ayuda'),
                      'url' => array('/help/index'),
                      'itemOptions' => array('class'=>'help'),
                      'items' => array(
                        array('label' => '> '.Yii::t('mainApp', 'Preguntas_frecuentes'),
                          'url' => array('/help/index' ),
                          'active' => ( ($this->getId() == 'help')  ),
                        ),
                        array('label' => '> '.Yii::t('mainApp', 'Configura tu ordenador'),
                          'url' => $readyToWorkUrl,
                          'active' => ( ($this->getId() == 'readytowork')  ),
                        ),

                        array('label' => '> '.Yii::t('mainApp', 'Contacto'),
                          'url' => array('/contact/index' ),
                          'active' => ( ($this->getId() == 'contact')  ),
                        )),
                      'active' => ( ($this->getId() == 'readytowork') || ($this->getId() == 'help') || ($this->getId() == 'contact'))
                    );
                }



                $this->widget('application.components.ABAMenu.ABAMenu', array('encodeLabel' => false,
                    'id' => "mainMenu",
                    'submenuHtmlOptions' => array(
                        'class' => 'dropdown-menu',
                    ),
                    'items' => $arItems
                    )
                );
            ?>
            <div class="clear"></div>

        <?php
        // member get member
        $mgm = Yii::app()->user->getMemberGetMember();
        if ($mgm !== false) {
            $daysDiff = 0;
            if ($mgm['expireDate'] !== null) {
                $daysDiff = HeDate::getDifferenceDateSQL($mgm['expireDate'], date('Y-m-d'), HeDate::DAY_SECS, false);
            }
            if ($mgm['expireDate'] !== null && $daysDiff <= 0) {
                // if the date has expired then it means that the user is free again, so we do nothing
            } else {
                $text = '';
                $button = false;
                if ($mgm['userId'] === null) {
                    $text = Yii::t('mainApp', 'mgm_invite');
                    $button = true;
                } elseif ($mgm['registered'] < MGM_REQUIRED_USERS) {
                    $missing = MGM_REQUIRED_USERS - $mgm['registered'];
                    if ($missing === 1) {
                        $text = Yii::t('mainApp', 'mgm_1_friend_missing');
                    } elseif ($missing >= 2 && $missing <= 4 && $user->langEnv == 'ru') {
                        $text = str_replace('#', $missing, Yii::t('mainApp', 'mgm_2_4_friend_missing_RU'));
                    } else {
                        $text = str_replace('#', $missing, Yii::t('mainApp', 'mgm_x_friends_missing'));
                    }
                    $button = true;
                } elseif ($mgm['expireDate'] !== null) {
                    if ($daysDiff === 1) {
                        $text = Yii::t('mainApp', 'mgm_1_day_expire');
                    } elseif ($daysDiff >= 2 && $daysDiff <= 4 && $user->langEnv == 'ru') {
                        $text = str_replace('#', $daysDiff, Yii::t('mainApp', 'mgm_2_4_days_expire_RU'));
                    } else {
                        $text = str_replace('#', $daysDiff, Yii::t('mainApp', 'mgm_x_days_expire'));
                    }
                    $button = true;
                }

                if ($button) {
                    ?>
                    <a class="leftMenu_mgm" class="button" href="<?php echo Yii::app()->createUrl('/profile/invite'); ?>">
                        <?php echo $text; ?>
                    </a>
                    <?php
                } else {
                    ?>
                    <div class="leftMenu_mgm">
                        <?php echo $text; ?>
                    </div>
                    <?php
                }

            }
        }

        //we check if the help contact support tel is enabled.

        if (HeContactSupportTel::includeContactSupportTel()) {
        /* BEGIN 6086 */

            $comUser = new UserCommon();
            $userType = $user->userType;
            $idPartnerCurrent = $user->idPartnerCurrent;
            $callUsM2F = Yii::t('mainApp', 'horarios');
            // TEST AB //
            $this->renderPartial('//layouts/shared/contactSupportAB', array(
                'comUser'=>$comUser,
                'userType'=>$userType,
                'idPartnerCurrent'=>$idPartnerCurrent,
                'country'=>$comUser->getCountryForPhonesByIp(Yii::app()->request->getUserHostAddress()),
                'callUs'=>Yii::t('mainApp', 'Llámanos'),
                'callUs4Free'=>Yii::t('mainApp', 'Llámanos_gratis'),
                'prefix'=>Yii::t('mainApp', 'pref'),
                'telf'=>Yii::t('mainApp', 'telf'),
                'consultant'=>Yii::t('mainApp', 'consultant'),
                'callUsM2F'=>$callUsM2F,
                'empezarPremium' => Yii::t('mainApp', 'Empezar_Premium'),
                'paraObtener' => Yii::t('mainApp', 'para_obtener'),
                'atencionGratis' => Yii::t('mainApp', 'atencion_gratis'),
                'personalizacion' => Yii::t('mainApp', 'personalizacion'),
                'planEstudios' => Yii::t('mainApp', 'plan_estudios'),
                'atnLaF' => Yii::t('mainApp', 'Atencion_de_LAF')
            ));
        }
/* ENDS 6086 */

        ?>
      <div>
          <p class="frase_fb marginBottom5"><?php echo Yii::t('mainApp', 'key_frase_in') ?></p>
          <a href="https://lnkd.in/ddUeAQJ" target="_blank" class="btn-linkedin-column" onclick="javascript: sendDefaultEventToTracker('<?php echo HeCooladata::EVENT_NAME_ADDED_LINKEDIN; ?>');">
              <i class="fa fa-linkedin"></i>
              <?php echo Yii::t('mainApp', 'btn_column_in') ?>
          </a>
      </div>
      <hr class="marginTop20">
            <div>
                <p class="frase_fb"><?php echo Yii::t('mainApp', 'key_frase_fb') ?></p>
<!---->
<!--<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2FABAEnglish&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;font&amp;colorscheme=light&amp;action=like&amp;height=21" scrolling="no" frameborder="0" style="" allowTransparency="true"></iframe>-->
<!---->
<?php /*
<script src="http://connect.facebook.net/en_US/all.js"></script>
<script>
FB.init({
    appId  : '<?php echo Yii::app()->config->get("FB_APP_CAMPUS_LOGIN_APPID"); ?>',
    status : true, // check login status
    cookie : true, // enable cookies to allow the server to access the session
    xfbml  : true, // parse XFBML
    channelUrl : 'http://www.abaenglish.com/channel.html', // channel.html file
    oauth  : true // enable OAuth 2.0
});

    var page_like_or_unlike_callback = function(url, html_element) {
        console.log("page_like_or_unlike_callback");
        console.log(url);
        console.log(html_element);
    }

    // In your onload handler
    FB.Event.subscribe('edge.create', page_like_or_unlike_callback);
    FB.Event.subscribe('edge.remove', page_like_or_unlike_callback);

    //    FB.Event.subscribe(event, callback);
    //
    //    FB.Event.subscribe('edge.create', function(href, widget) {
    //        alert('You just liked the page!');
    //    });
    //    $(document).ready(function () {
    //
    //        $('.connect_widget_like_button clearfix like_button_no_like').live('click', function () {
    //            alert('clicked');
    //        });
    //    });
</script>
*/ ?>
<div id="fb-rootContainer" style="position: relative; border: none; width:450px; height:21px; text-align: left; "> <?php /* border:none; overflow:hidden; width:450px; height:21px; */ ?>

    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/ca_ES/sdk.js#xfbml=1&version=v2.6&appId=<?php echo Yii::app()->config->get("FB_APP_CAMPUS_LOGIN_APPID"); ?>";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <div style="overflow: hidden !important;" class="fb-like" data-href="http://www.facebook.com/ABAEnglish" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false" data-send="false" data-width="450" data-height="21" data-colorscheme="light"></div>
</div>
<style>
   .fb-like {
       overflow: hidden !important;
       border: none !important;
       width: 450px !important;
       height: 21px !important;
       top: 0 !important;
       left: 0 !important;
       z-index: 9000 !important;
   }
   .fb_like iframe {
       left: 0 !important;
   }
   #u_0_6, #u_0_6 form, .fb_edge_comment_widget {
       position: relative !important;
       display: none !important;
       top: 30px !important;
       z-index: -9000 !important;
   }
   .fb_edge_widget_with_comment span.fb_edge_comment_widget iframe.fb_ltr {
       display: none !important;
   }
   .like_counter_hider {
       position: absolute;
       top: 0;
       left: 45px;
       width: 35px;
       height: 20px;
       background-color: #f3f3f3;
       z-index: 200;
   }
</style>

            </div>
	</div><!-- mainmenu -->

	<div id="divRightContent" class="cmp_blockRight right">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<?php $this->endContent(); ?>
