<?php
if( !isset($typeSubscriptionStatus)){
    $moUser = new AbaUser();
    $moUser->getUserById(Yii::app()->user->getId());
    $moLastPaySubs = new Payment();
    $commRecurring = new RecurrentPayCommon();
    $typeSubscriptionStatus = $commRecurring->getTypeSubscriptionStatus($moUser, $moLastPaySubs);
}

?>

<div id="header">

    <div class="clearfix"></div>
    <?php 
        $imagePath = HeMixed::getUrlPathImg("logoAba.png");
        $imageTag = CHtml::image($imagePath, "Logo ABA English");
        echo CHtml::link($imageTag, Yii::app()->createUrl('site/index'), array('class' => 'cmp_logoImg left'));
    ?>
    <div class="cmp_subheader left">
        <div lang="<?php echo Yii::app()->language?>" class="cmp_nivelProgresoContainer">
            <?php  
                $this->widget('application.components.widgets.LevelSelector', array('currentLevel' => Yii::app()->request->getPost('level')));
            ?>
        </div>
        <ul id="my_account_nav">
            <li>
                <span lang="<?php echo Yii::app()->language?>" id="my_account"><?php  echo Yii::t('mainApp', 'Mi Cuenta') ?></span>
                <ul>
                    <li><a  id="headLinkMenuMyAccount"  href="/profile/index"><?php echo Yii::t('mainApp', 'Datos personales') ?></a></li>
                    <li><a  id="headLinkMenuProgress"  href="/profile/progress"><?php echo Yii::t('mainApp', 'Mi progreso') ?></a></li>
                    <li><a  id="headLinkMenuCertificates"  href="/profile/certificates"><?php echo Yii::t('mainApp', 'Mis certificados') ?></a></li>
                    <?php
                    $comInvoice = new InvoicesCommon();
                    if ( $comInvoice->isDisplayInvoices(Yii::app()->user->getId()) ){
                        ?>
                        <li>
                            <a  id="headLinkMenuInvoices"  href="<?php echo Yii::app()->createUrl('profile/invoices'); ?>" >
                                <?php echo Yii::t('mainApp', 'Mis facturas') ?>
                            </a>
                        </li>
                    <?php } ?>
                    <?php
                    if ( RecurrentPayCommon::isDisplaySubscription($typeSubscriptionStatus) ){
                        ?>
                        <li>
                            <a id="headLinkMenuSubsc" href="<?php echo Yii::app()->createUrl('subscription/index'); ?>" >
                                <?php echo Yii::t('mainApp', 'w_subscription') ?>
                            </a>
                        </li>
                    <?php } ?>
                    <?php
                    // member get member
                    $mgm = Yii::app()->user->getMemberGetMember();
                    if($mgm !== false) {
                        $daysDiff = 0;
                        if($mgm['expireDate'] !== NULL) {
                            $daysDiff = HeDate::getDifferenceDateSQL($mgm['expireDate'], date('Y-m-d'), HeDate::DAY_SECS, false);
                        }
                        if($mgm['expireDate'] !== NULL && $daysDiff <= 0) {
                            // if the date expired means the user is free again, so we do nothing
                        }
                        else
                        {
?>
                            <li>
                                <a id="headLinkMenuInvoices"  href="<?php echo Yii::app()->createUrl('profile/invite'); ?>" >
                                    <?php echo Yii::t('mainApp', 'mgm_invite') ?>
                                </a>
                            </li>
<?php
                        }
                    }
?>
                    <li><a id="aLinkHeadLogout" href="/site/logout"><?php echo Yii::t('mainApp', 'Salir') ?></a></li>
                </ul>
                <div id="triangle"></div>
            </li>

        </ul>
        <!-- <span id="my_account">Mi cuenta</span> -->
        <?php if( Yii::app()->config->get('ENABLE_DYNAMIC_BANNERS')==1 ){ ?>
            <div id="idBannerConversion">
               &nbsp;<!-- Here it goes the call to dynamic banners, probably Selligent or a standard widget-->&nbsp;
            </div>
        <?php }
        else{
            $this->widget('application.components.widgets.SimulatorBannerDynSelligent', array());
        } ?>
    </div>

</div>
<div class="clearfix"></div>