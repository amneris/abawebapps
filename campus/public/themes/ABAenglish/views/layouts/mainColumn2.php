<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<?php echo Yii::app()->language; ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="<?php echo Yii::app()->language; ?>" />
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/main.css'); ?>
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/fonts.css'); ?>
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/leveltest/leveltest.css'); ?>
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/assets/out/campus-v5.css'); ?>
        <?php Yii::app()->clientScript->registerPackage('font-awesome'); ?>

        <?php
        $urlsSelligent = Yii::app()->config->get("URL_DYNAMIC_CSS_SELLIGENT");

        $urls = explode(',',$urlsSelligent);
        foreach($urls as $url){
            Yii::app()->clientScript->registerCssFile($url);
        }
        ?>

        <?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
        <?php if(Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/modernizr.js')) ?>
        <?php if(Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/dropDownListImplement.js')) ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/abaFunctionsLayout2.js'); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/leveltest.js'); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/campusLevelTest.js'); ?>

        <!-- FancyBox -->
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/js/fancyapps-fancyBox/jquery.fancybox.css?v=2.1.5'); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/fancyapps-fancyBox/jquery.fancybox.pack.js?v=2.1.5'); ?>

        <title>
            <?php echo CHtml::encode($this->pageTitle); ?>
        </title>
        <?php HeAnalytics::includeScriptsCommonAnalyitics(); ?>
        <?php HeAnalytics::includeScriptVWO(); ?>
        <script type="application/javascript">var delC=function(e){document.cookie=e+"=;expires=Thu, 01 Jan 1970 00:00:01 GMT;"};delC("go");</script>
        <?php $this->renderPartial('//layouts/shared/versioninfo'); ?>
        <?php $this->renderPartial('//layouts/shared/cooladataManager'); ?>
        <?php $this->renderPartial('//layouts/shared/newrelic'); ?>
    </head>

    <body>

    <div style="display: none; ">
        <?php $this->renderPartial('//layouts/shared/tagManager'); ?>
    </div>
    <!--[if lte IE 9]>
    <?php
      echo "<div class='alert-banner'>". str_replace('{username}', Yii::app()->user->getName(), Yii::t('mainApp', 'no_iexplorer11' )). "</div>"
    ?>
    <![endif]-->

    <span id="urlBannerDynamic" class="jsUseOnlySoHidden">
        <?php echo Yii::app()->config->get('URL_PROXY_BANNER_SELLIGENT').Yii::app()->user->getId(); ?>
    </span>
    <span id="urlSelfBannerSecure" class="jsUseOnlySoHidden">
        <?php echo Yii::app()->config->get('URL_SECURITY_BANNER_FRONTSIDE').Yii::app()->user->getId(); ?>
    </span>

    <div class="container" id="page">
        <?php $this->renderPartial('//layouts/headerColumn2'); ?>

        <?php echo $content; ?>

        <div class="clearfix"></div>
        <div id="modalWindow"></div>
    </div><!-- page -->

    <?php $this->renderPartial('//layouts/footer'); ?>

    <script type="text/javascript">
        function showErrorMessage(message) {
            $("body").prepend("<div class='alert-banner'>"+ message +"</div>");
        }

        var detectFlash = (function() {
            Modernizr.on('flash', function(result) {
                if (result) {
                    console.log('Flash is here');
                } else {
                    showErrorMessage("<?php echo Yii::t('mainApp', 'error_flash' ) ?>");
                }
            });
        }());
    </script>

    <script type="application/javascript" src="<?= Yii::app()->theme->baseUrl ?>/assets/out/application.js"></script>
    </body>
</html>
