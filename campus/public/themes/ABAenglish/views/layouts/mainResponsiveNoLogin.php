<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="robots" content="noindex, follow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <meta name="language" content="<?php echo Yii::app()->language; ?>" />
    <meta charset="UTF-8">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/modal.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/assets/out/payments.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/fonts.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/modal.css'); ?>

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <?php HeAnalytics::includeScriptsCommonAnalyitics(); ?>
    <?php HeAnalytics::includeScriptVWO(); ?>
    <script type="application/javascript">var delC=function(e){document.cookie=e+"=;expires=Thu, 01 Jan 1970 00:00:01 GMT;"};delC("go");</script>
    <?php $this->renderPartial('//layouts/shared/cooladataManager'); ?>
    <?php $this->renderPartial('//layouts/shared/newrelic'); ?>
</head>
<body>
    <div style="display: none; ">
    <?php $this->renderPartial('//layouts/shared/tagManager'); ?>
    </div>

    <?php $this->renderPartial('//layouts/shared/header'); ?>

    <?php echo $content; ?>
    <div id="modalWindow"></div>

    <?php $this->renderPartial('//layouts/shared/footerNoLogin'); ?>

    <?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/assets/out/payments.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/abaFunctionsLayout2.js'); ?>

</body>
</html>
