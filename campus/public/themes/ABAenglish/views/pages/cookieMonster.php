<?php

// unset cookies

$environment = $_SERVER['ABAWEBAPPS_ENV'];

if (isset($_SERVER['HTTP_REFERER'])) {
    $referer = $_SERVER['HTTP_REFERER'];
} else {
    $referer = $_SERVER['HTTP_HOST'];
}

if (isset($_SERVER['HTTP_COOKIE'])) {
    $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
    $cookie_domains = "'','.".$_SERVER['ABA_CAMPUS_COOKIES_URL']."','".$_SERVER['ABA_CAMPUS_COOKIES_URL']."', '".$_SERVER['ABA_SITE_COOKIES_URL']."'";    $domains = array($cookie_domains);

    foreach ($cookies as $cookie) {
        $parts = explode('=', $cookie);
        $name = trim($parts[0]);
        $f_string = "";

        foreach ($domains as $domain) {
            setcookie($name, '', time()-1000, '/', $domain);
        }
    }
}

header('Location: '.$referer);

?>