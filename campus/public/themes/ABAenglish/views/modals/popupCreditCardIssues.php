<h3 class="ab-payment-title no-margin text-danger"><?php echo Yii::t('mainApp', 'gateway_failure_key_reasons_title') ?></h3>
<ol class="m-b-30">
    <li><?php echo Yii::t('mainApp', 'gateway_failure_key_reasons_0') ?></li>
    <li><?php echo Yii::t('mainApp', 'gateway_failure_key_reasons_1') ?></li>
    <li><?php echo Yii::t('mainApp', 'gateway_failure_key_reasons_2') ?></li>
</ol>