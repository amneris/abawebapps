<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div class="clearfix"></div>
<div id="creditCardWrapper">
    <div class="clear"></div>
    <div>
        <h3><?php echo Yii::t('mainApp', 'gateway_failure_key_reasons_title') ?></h3>
        <ol>
            <li><?php echo Yii::t('mainApp', 'paypal_fail_reasons_0') ?></li>
            <li><?php echo Yii::t('mainApp', 'paypal_fail_reasons_1') ?></li>
            <li><?php echo Yii::t('mainApp', 'paypal_fail_reasons_2') ?></li>
        </ol>
    </div>
</div>