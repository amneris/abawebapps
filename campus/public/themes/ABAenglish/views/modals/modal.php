<div class="form" id="jobDialogForm">
 
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'objForm',
    'enableAjaxValidation'=>true,
));
 
//I have enableAjaxValidation set to true so i can validate on the fly the
?>
 	<p>
		<h1>1. ¿para que quieres aprender Ingles?</h1>
	</p>
			
    <p class="note">Fields with <span class="required">*</span> are required.</p>
 
    <?php echo $form->errorSummary($model); ?>
 
	    <div class="row">
	        <?php echo $form->labelEx($model,'quest1'); ?>
	        <?php echo $form->checkBox( $model, 'quest1', array("value" => "1") ); ?>
	        <?php echo $form->checkBox( $model, 'quest1', array("value" => "2") ); ?>
	        <?php echo $form->checkBox( $model, 'quest1', array("value" => "3") ); ?>
	        <?php echo $form->error($model,'quest1'); ?>
	    </div>
	 
	 <p>
		<h1>2. ¿Hasta que nivel quieres llegar?</h1>
	</p>
	
	    <div class="row">
	        <?php echo $form->labelEx($model,'quest2'); ?>
	        <?php echo $form->radioButton( $model, 'quest2', array("value" => "1") ); ?>
	        <?php echo $form->radioButton( $model, 'quest2', array("value" => "2") ); ?>
	        <?php echo $form->radioButton( $model, 'quest2', array("value" => "3") ); ?>
	        <?php echo $form->error($model,'quest2'); ?>
	    </div>
	 
	 <p>
		<h1>3. ¿Para que necesitas el Ingles?</h1>
	</p>
	   
	    <div class="row">
	        <?php echo $form->labelEx($model,'quest3'); ?>
	        <?php echo $form->checkBox( $model, 'quest3', array("value" => "1") ); ?>
	        <?php echo $form->checkBox( $model, 'quest3', array("value" => "2") ); ?>
	        <?php echo $form->checkBox( $model, 'quest3', array("value" => "3") ); ?>
	        <?php echo $form->error($model,'quest3'); ?>
	    </div>
	 
	 <p>
		<h1>4. ¿A que te dedicas?</h1>
	</p>
	
	    <div class="row">
	        <?php echo $form->labelEx($model,'quest4'); ?>
	
	        <?php echo $form->error($model,'quest4'); ?>
	    </div>
	  
	 <p>
		<h1>5. Eres</h1>
	</p>
	
		<div class="row">
	        <?php echo $form->labelEx($model,'quest5'); ?>
	        <?php echo $form->radioButton(	$model, 'quest5', array("value" => "1") ); ?>
	        <?php echo $form->radioButton(	$model, 'quest5', array("value" => "2") ); ?>
	        <?php echo $form->error($model,'quest5'); ?>
	    </div>
	
	    <div class="row buttons">
	        <?php echo CHtml::ajaxSubmitButton(Yii::t('send','Enviar'),
	        		   CHtml::normalizeUrl(array('modals/objectives','render'=>false)),
	        			array('success'=>'js: function(data) {
	                        $("#Person_jid").append(data);
	                        $("#modalWindow").dialog("close");
	                    }'),array('id'=>'closeJobDialog')); ?>
	    </div>
 
<?php $this->endWidget(); ?>
 
</div>