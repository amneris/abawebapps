<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div class="clearfix"></div>
<div class="helpContainer">
    <div class="clear"></div>
    <div class="comprendetext">
        <div id="microContent">
            <h3><?php echo Yii::t('mainApp', 'Micro_not_active') ?></h3>
            <h4></h4>
            <ul id="microSteps">
                <li>1. <?php echo Yii::t('mainApp', 'Do_click') ?><strong><?php echo Yii::t('mainApp', 'Allow_Flash') ?></strong></li>
                <li>2. <?php echo Yii::t('mainApp', 'Do_click') ?><strong><?php echo Yii::t('mainApp', 'Allow_Remember') ?></strong></li>
                <li>3. <?php echo Yii::t('mainApp', 'Do_click_after') ?><strong><?php echo Yii::t('mainApp', 'Close_Flash') ?></strong></li>
            </ul>
            <a class="continue" href="javascript:callPanelSettingsReady();"><?php echo Yii::t('mainApp', 'Continuar') ?></a>
            <!-- <p>O continúa sin acceso a tu micrófono si no deseas grabar tu voz. Advertencia: no se completará el ejercicio y no podrás acceder a la evaluación.</p> -->
            <div id="flash_background">
            </div>
        </div>
    </div>
</div>
