<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div class="clearfix"></div>
<div id="paypalProcessingWrapper">
    <div class="clear"></div>
    <div>
        <div style="display: block;" class="loadingPaypalContainer" id="IdPPProcessingSendPayment">
            <span class="loadingPaypalInterior">&nbsp;</span>
        </div>
        <ul>
            <li id="paypalPopup_0"><?php echo Yii::t('mainApp', 'paypal_wait_processing_key_0') ?></li>
            <li id="paypalPopup_1"><?php echo Yii::t('mainApp', 'paypal_wait_processing_key_1') ?></li>
            <li id="paypalPopup_2"><?php echo Yii::t('mainApp', 'paypal_wait_processing_key_2') ?></li>
        </ul>
    </div>
</div>