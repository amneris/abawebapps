<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div class="clearfix"></div>
<div class="helpContainer">
    <div class="clear"></div>
    <div class="comprendetext">
        <p class="marginBottom6 saludo"><?php echo Yii::t('mainApp', '¡Hola') ?> <?php echo $name ?>!</p>
        <p class="marginBottom12 colortextayuda"><?php echo Yii::t('mainApp', 'Las consultas ilimitadas solo están disponibles para los alumnos ABA Premium.') ?></p>
        <p class="marginBottom12 colortextayuda"><?php echo Yii::t('mainApp', 'Si quieres que ').$teacherName.Yii::t('mainApp', ' tu profesor asignado responda a cualquier duda o consulta pedagógica, pásate a ABA Premium.') ?></p>
        <img class="imagenCentrada marginBottom6" src="<?php echo HeMixed::getUrlPathImg("videoclasspopupcurso.png") ?>" alt="<?php echo Yii::t('mainApp', 'secciones') ?>" />
        <p class="marginBottom12 colortextayuda"><?php echo Yii::t('mainApp', 'Además, si eres ABA Premium podrás disfrutar de todos los contenidos de forma ilimitada y obtener los certificados de American & British Academy.') ?></p>
        <div class="popup_anuncioPremium">
            <div class="frasepopup_anuncioPremium">
                <span class="popup_pasateap1"><?php echo Yii::t('mainApp', 'Desbloquea ABA Premium') ?></span><br />
                <span class="popup_pasateap2"><?php echo Yii::t('mainApp', 'y disfrutarás de todos los contenidos') ?></span>
            </div>
            <a href="<?php echo Yii::app()->createUrl(LAND_PAYMENT) ?>" class="left">
                <div class="popup_haztePremium left">
                    <span class="popup_pasateaNoBold"><?php echo Yii::t('mainApp', 'Pásate a ') ?></span>
                    <span class="popup_pasateaBold">ABA </span>
                    <span class="popup_pasateaNoBold">Premium</span>
                </div>
            </a>
        </div>
        <div class="clear"></div>
        <!--
        <span id="cerrarFree" class="sigueFree">> Sigue estudiando</span>
        -->
    </div>
</div>