<div class="bodyPopUpCambiodeLevel">
    <div class="firstLine">
        <div class="iconAlertPopUp left">
        </div>
        <div class="right">
            <p><?php echo Yii::t('mainApp', 'Estás a punto de acceder a los ') ?></p>
            <p><?php echo str_replace("{level}", strtoupper($level), Yii::t('mainApp', 'contenidos del nivel ')) ?></p>
            <p><?php echo Yii::t('mainApp', 'si quieres que este cambio sea permanente') ?></p>
            <p>
                <?php echo Yii::t('mainApp', ', debes hacerlo desde') ?> <a href="<?php echo Yii::app()->createUrl('profile/index') ?>"><?php echo Yii::t('mainApp', 'Mi Cuenta') ?></a>
            </p>
        </div>
    </div>
    <div class="clear"></div>
    <a>
        <div class="button">
            <?php echo Yii::t('mainApp', 'Continuar') ?> >
        </div>
    </a>
    <!--<a href="<?php echo Yii::app()->createUrl('profile/index') ?>">
        <div class="button right">
            <?php echo Yii::t('mainApp', 'Sí, cambiar desde mi cuenta ') ?>>
        </div>
    </a>-->
</div>