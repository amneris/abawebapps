<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<div class="bodyPopUp">
    <div class="popupIntroduccion">
        
        <span class="saludoDespues"></span><span class="saludo"><?php echo Yii::t('mainApp', 'Bienvenido') ?></span>
        <span class="saludoDespues"><?php echo Yii::t('mainApp', 'al Campus de ABA English!') ?></span>
    </div>
    <span class="empezar"><?php echo Yii::t('mainApp', 'Te explicamos las ventajas del mejor curso de inglés online') ?></span>
    <div class="pasos">
        <span class="pasoHecho left"><?php echo Yii::t('mainApp', 'Tus datos') ?></span>
        <span class="pasoAnteriorHechoSiguienteNoHechoIconPrev left"></span>
        <span class="pasosHechosIcon left"></span>
        <span class="paso2o3Hecho left"><?php echo Yii::t('mainApp', 'Tu nivel de inglés') ?></span>
        <span class="pasoAnteriorHechoSiguienteNoHechoIconPrev left"></span>
        <span class="pasosHechosIcon left"></span>
        <span class="paso2o3Hecho left width168"><?php echo Yii::t('mainApp', 'Bienvenida') ?></span>
    </div>
    <div class="clear"></div>
    <div class="seccionesBienvenida marginRight12 marginTop8 left">
        <img src="<?php echo HeMixed::getUrlPathImg("videoclassPopupBienvenida.png") ?>" alt="<?php echo Yii::t('mainApp', 'videoclass') ?>" />
        <p>
            <?php echo MAX_ALLOWED_LEVEL > Yii::app()->user->role ? Yii::t('mainApp', 'Como usuario ABA Free, cada día tendrás una nueva Videoclase para que disfrutes de un completo aprendizaje del idioma.') : Yii::t('mainApp', 'Como usuario ABA Premium, tendrás todas las Videoclases para que disfrutes de un completo aprendizaje del idioma.'); ?>
        </p>
    </div>
    <div class="seccionesBienvenida marginTop8 left">
        <img src="<?php echo HeMixed::getUrlPathImg("cursoPopupBienvenida.png") ?>" alt="<?php echo Yii::t('mainApp', 'curso') ?>" />   
        <p>
            <?php echo MAX_ALLOWED_LEVEL > Yii::app()->user->role ? Yii::t('mainApp', 'También tienes una unidad completa del curso para que pruebes nuestra metodología de aprendizaje única.') : Yii::t('mainApp', 'Cada nivel de estudio se estructura en 24 unidades. Te recomendamos empezar por la primera y completar el 100% para pasar a la siguiente.'); ?>
        </p>
    </div>
    <div class="clear"></div>
    <div class="seccionesBienvenida marginRight12 marginTop8 left">
        <img src="<?php echo HeMixed::getUrlPathImg("testPopupBienvenida.png") ?>" alt="<?php echo Yii::t('mainApp', 'test de nivel') ?>" />   
        <p><?php echo Yii::t('mainApp', 'Antes de empezar el curso, te aconsejamos que realices el test de configuración de requisitos técnicos de tu ordenador.') ?></p>
    </div>
    <div class="seccionesBienvenida marginTop8 left">
        <img src="<?php echo HeMixed::getUrlPathImg("certificadosPopupBienvenida.png") ?>" alt="<?php echo Yii::t('mainApp', 'certificados') ?>" />
        <p><?php echo Yii::t('mainApp', '¡Al completar las 24 unidades del nivel podrás obtener los certificados oficiales de American & British Academy!') ?></p>
    </div>
    <div class="clear"></div>
    <div class="registrerSendButton" onclick="$('#sharerdialog').dialog('destroy');document.location.reload();"><?php echo Yii::t('mainApp', '¡Comienza ya a hablar en inglés!') ?></div>
    <div class="clear"></div>
</div>