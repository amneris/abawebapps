<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div class="clearfix"></div>
<div class="helpContainer">
    <div class="clear"></div>
    <div class="comprendetext">
        <p class="marginBottom6 saludo"><?php echo Yii::t('mainApp', '¡Hola') ?> <?php echo $name ?>!</p>
        <p class="marginBottom12 colortextayuda"><?php echo Yii::t('mainApp', 'La unidad del curso que estás intentando visualizar está bloqueada.') ?></p>
        <p class="marginBottom12 colortextayuda"><?php echo Yii::t('mainApp', 'Como ABA Free, podrás probar la primera unidad de cada nivel para que veas nuestra metodología única.') ?></p>
        <p class="marginBottom12 colortextayuda"><?php echo Yii::t('mainApp', 'Every_day_one_new_videoclass') ?></p>
        <img class="imagenCentrada marginBottom6" src="<?php echo HeMixed::getUrlPathImg("pasateapremiumpopup_".strtolower(Yii::app()->user->getLanguage()).".png") ?>" alt="<?php echo Yii::t('mainApp', 'secciones') ?>" />
        <div class="popup_anuncioPremium">
            <div class="frasepopup_anuncioPremium">
                <span class="popup_pasateap1"><?php echo Yii::t('mainApp', 'Desbloquea ABA Premium') ?></span><br />
                <span class="popup_pasateap2"><?php echo Yii::t('mainApp', 'y disfrutarás de todos los contenidos') ?></span>
            </div>
            <a href="<?php echo Yii::app()->createUrl(LAND_PAYMENT) ?>" class="left">
                <div class="popup_haztePremium left">
                    <span class="popup_pasateaNoBold"><?php echo Yii::t('mainApp', 'Pásate a ') ?></span>
                    <span class="popup_pasateaBold">ABA </span>
                    <span class="popup_pasateaNoBold">Premium</span>
                </div>
            </a>
        </div>
        <div class="clear"></div>
        <!--
        <span id="cerrarFree" class="sigueFree">> Sigue estudiando</span>
        -->
    </div>
</div>