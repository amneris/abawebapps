<div class="centralContainermainColumn1">
    <div class="headerABAForever left">
        <p><span class="tituloAbaPremiumBold">ABA</span>&nbsp;&nbsp;<span class="tituloAbaPremium"><?php echo Yii::t('mainApp', 'Forever') ?></span></p>
        <div class="fraseCentralHeader">
            <div class="fraseCentralHeaderSuperior">
                <span><?php echo Yii::t('mainApp', 'A ritmo con el mundo es tener la tranquilidad') ?></span>
            </div>
            <div class="fraseCentralHeaderInferior">
                <span><?php echo Yii::t('mainApp', 'de disfrutar de ABA English gratis y para siempre.') ?></span>
            </div>
        </div>
        <div class="botonUneteYa left">
            <?php echo Yii::t('mainApp', 'Únete ya') ?>
        </div>
    </div>
    <div class="clear"></div>
    <div class="contentABAForever">
        <div class="paso1ABAForever left">
            <span>1.&nbsp;&nbsp;<?php echo Yii::t('mainApp', 'Apúntate a ABA Free') ?></span>
        </div>
        <div class="separadorABAForever left"></div>
        <div class="paso2ABAForever left">
            <span>2.&nbsp;&nbsp;<?php echo Yii::t('mainApp', 'Pásate a ABA Premium') ?></span>
        </div>
        <div class="separadorABAForever left"></div>
        <div class="paso3ABAForever left">
            <span>3.&nbsp;&nbsp;<?php echo Yii::t('mainApp', 'Consigue ABA Forever') ?></span>
        </div>
        <div class="clear"></div>
        <div class="ContenedorRoles left">
            <div class="ContenedorRol left">
                <div class="ABAFreeIcon left"></div>
                <div class="prova">
                    <div class="text"><span class="bold"><?php echo Yii::t('mainApp', 'Empieza a aprender inglés gratuitamente.') ?></span></div>
                    <a href="">
                        <span>> <?php echo Yii::t('mainApp', 'Únete ya') ?></span>
                    </a>
                </div>
            </div>
            <div class="ContenedorRol left">
                <div class="ABAPremiumIcon left"></div>
                <div class="prova">
                    <div class="text"><?php echo Yii::t('mainApp', 'Obtén') ?> <span class="bold"><?php echo Yii::t('mainApp', 'todos los beneficios') ?></span><?php echo Yii::t('mainApp', ' de nuestro curso online y ') ?><span class="bold"><?php echo Yii::t('mainApp', 'descarga tus certificados') ?></span>.</div>
                    <a href="">
                        <span>> <?php echo Yii::t('mainApp', 'Suscríbete ahora') ?></span>
                    </a>
                </div>
            </div>
            <div class="ContenedorRol left">
                <div class="ABAForeverAndEverIcon left"></div>
                <div class="prova">
                    <div class="text"><?php echo Yii::t('mainApp', 'Después de 18 meses como alumno de ABA Premium te conviertes en alumno de ABA Forever: ') ?><span class="bold"><?php echo Yii::t('mainApp', 'tendrás el curso de ABA English gratis y para siempre') ?></span>.</div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="opinionesAbaPremiumTitulo">
            <span class="italic"><?php echo Yii::t('mainApp', 'Qué comentan sobre') ?></span>
            <span class="greenBold">ABA</span>
            <span class="greenNormal"><?php echo Yii::t('mainApp', 'Forever') ?></span>
        </div>
        <div class="clear"></div>
        <div class="contenedorQuotes left">
            <span class="quoteIcon left"></span>
            <span class="QuoteText left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis velit massa, vulputate vitae aliquet eu, accumsan pretium nisl. Nulla facilisi.</span>
            <span class="QuoteFirma left">Maria</span>
        </div>
        <div class="contenedorQuotes left">
            <span class="quoteIcon left"></span>
            <span class="QuoteText left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis velit massa, vulputate vitae aliquet eu, accumsan pretium nisl. Nulla facilisi.</span>
            <span class="QuoteFirma left">Antonio</span>
        </div>
        <div class="contenedorQuotes left">
            <span class="quoteIcon left"></span>
            <span class="QuoteText left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis velit massa, vulputate vitae aliquet eu, accumsan pretium nisl. Nulla facilisi.</span>
            <span class="QuoteFirma left">Max</span>
        </div>
        <div class="clear"></div>
        <div class="ABAForeverFooter left">
            <div class="ABAForeverFraseFinal left">
                <span class="normal"><?php echo Yii::t('mainApp', 'Aprende') ?></span>
                <span class="blue">&nbsp;<?php echo Yii::t('mainApp', 'inglés gratis') ?></span>
            </div>
            <div class="ABAForeverButtonFinal left">
                <?php echo Yii::t('mainApp', 'Únete a ABA Forever') ?>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>