<div class="centralContainermainColumn1">
    <div class="headerABAPremium left">
        <p>
            <span class="tituloAbaPremiumBold">ABA</span>&nbsp;&nbsp;<span class="tituloAbaPremium"><?php echo Yii::t('mainApp', 'Premium') ?></span>
        </p>
        <div class="fraseCentralHeader">
            <div class="fraseCentralHeaderSuperior">
                <span><?php echo Yii::t('mainApp', 'Ampliar tus horizontes es estar a ritmo con el mundo.') ?></span>
            </div>

            <div class="fraseCentralHeaderInferior">
                <!--<span><?php echo Yii::t('mainApp', 'Accede a todos los contenidos por solo 9€ al mes.') ?></span>-->
            </div>
        </div>
        <?php
            if(MAX_ALLOWED_LEVEL > Yii::app()->user->role)
            {
        ?>
                <a href="<?php echo Yii::app()->createUrl(LAND_PAYMENT) ?>">
                    <div class="botonSubscribir">
                        <?php echo Yii::t('mainApp', 'Suscríbete ya') ?>
                    </div>
                </a>
        <?php
            }
        ?>
    </div>
    <div class="clear"></div>
    <div class="contentABAPremium left">
        <div class="contenedorCaracteristicasABAPremium left">
            <div class="contenedorCaracteristicasABAPremiumTitulo">
                <?php echo Yii::t('mainApp', 'Todos los contenidos') ?>
            </div>
            <div class="clear"></div>
            <div class="contenedorCaracteristicasABAPremiumBody">
                <span class="ContenidosIcon left"></span>
                <span class="descripcionCaracteristicaPremium"><?php echo Yii::t('mainApp', 'Accede a todas las unidades del curso de forma ilimitada y disfruta de las videoclases y de los minifilms en cualquier momento.') ?></span>
            </div>
        </div>
        <div class="contenedorCaracteristicasABAPremium left">
            <div class="contenedorCaracteristicasABAPremiumTitulo">
                <?php echo Yii::t('mainApp', 'Obtén los certificados') ?>
            </div>
            <div class="clear"></div>
            <div class="contenedorCaracteristicasABAPremiumBody">
                <span class="CertificadosIcon left"></span>
                <span class="descripcionCaracteristicaPremium"><?php echo Yii::t('mainApp', 'Solo con ABA Premium obtendrás un certificado expedido por American & British Academy cada vez que termines uno de los seis niveles.') ?></span>
            </div>
        </div>
        <div class="contenedorCaracteristicasABAPremium left">
            <div class="contenedorCaracteristicasABAPremiumTitulo">
                <?php echo Yii::t('mainApp', 'Consultas ilimitadas') ?>
            </div>
            <div class="clear"></div>
            <div class="contenedorCaracteristicasABAPremiumBody">
                <span class="ConsultasIlimitadasIcon left"></span>
                <span class="descripcionCaracteristicaPremium"><?php echo Yii::t('mainApp', '¡Contacta con tu profesor siempre que lo necesites! Podrás realizar todas las consultas a los profesores de ABA de forma ilimitada en cualquier momento del día.') ?></span>
            </div>
        </div>
        <div class="clear"></div>
        <div class="opinionesAbaPremiumTitulo">
            <span class="italic"><?php echo Yii::t('mainApp', 'Qué comentan sobre') ?></span>
            <span class="redBold">ABA</span>
            <span class="redNormal"><?php echo Yii::t('mainApp', 'Premium') ?></span>
        </div>
        <div class="clear"></div>
        <div class="contenedorQuotes left">
            <span class="quoteIcon left"></span>
            <span class="QuoteText left"><?php echo Yii::t('mainApp', 'Nunca pensé que podría existir un curso de inglés que nos gustara a toda la familia: ¡nos lo pasamos muy bien y ya hablamos inglés con nuestros hijos!') ?></span>
                <span class="QuoteFirma left">Cristina P. (45 <?php echo Yii::t('mainApp', 'años') ?>)<?php echo Yii::t('mainApp', ' y ') ?>David C. (48 <?php echo Yii::t('mainApp', 'años') ?>)</span>
        </div>
        <div class="contenedorQuotes left">
            <span class="quoteIcon left"></span>
                <span class="QuoteText left"><?php echo Yii::t('mainApp', '¡Estoy totalmente enganchado a los cortometrajes de ABA! ¡Finalmente puedo entender todas las películas en inglés!') ?></span>
                <span class="QuoteFirma left">Paul R. (22 <?php echo Yii::t('mainApp', 'años') ?>)</span>
        </div>
        <div class="contenedorQuotes left">
            <span class="quoteIcon left"></span>
                <span class="QuoteText left"><?php echo Yii::t('mainApp', 'Finalmente he conseguido vencer el miedo a las entrevistas de trabajo: ¡ahora estoy deseando que me entrevisten en inglés!') ?></span>
                <span class="QuoteFirma left">Sophie S. (32 <?php echo Yii::t('mainApp', 'años') ?>)</span>
        </div>
        <div class="clear"></div>
        <div class="ABAPremiumFooter left">
            <div class="ABAPremiumFraseFinal left">

                    <span class="normal">
                        <?php echo Yii::t('mainApp', 'Empieza a disfrutar ya') ?>
                        <br/>
                    </span>
                    <span class="red">
                        <?php echo Yii::t('mainApp', 'mejor curso de inglés online') ?>
                    </span>

            </div>
           <?php
                if(MAX_ALLOWED_LEVEL > Yii::app()->user->role)
                {
            ?>
                    <a href="<?php echo Yii::app()->createUrl(LAND_PAYMENT) ?>">
                        <div class="ABAPremiumButtonFinal left">
                            <span class="normal"><?php echo Yii::t('mainApp', 'Suscríbete a') ?>&nbsp;</span>
                            <span class="bold">ABA</span>
                            <span class="normal">&nbsp;Premium</span>
                        </div>
                    </a>
            <?php
                }
            ?>
        </div>
        <div class="clear"></div>
    </div>
</div>
