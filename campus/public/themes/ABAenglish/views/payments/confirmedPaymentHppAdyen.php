<?php
/**
 * @var string $urlBoleto
 * @var string $dueDateBoleto; Date
 */

/**
 * After Payment has been done - it doesn't matter wether through PayPal or La Caixa- we display the summary of all info
 */

/*****************************************Variables passed by the controller action************************************/
/* @var Payment $payment  */
/*********************************************************************************************************************/
?>

<?php foreach($msgs as $iErr => $stData ): ?>
  <?php if($iErr == ADYEN_HPP_RESULT_PENDING OR $iErr == ADYEN_HPP_RESULT_REFUSED OR $iErr == ADYEN_HPP_RESULT_CANCELLED): ?>
    <?php
    /* ------------------ TRACKINGS AND PIXELS to register a purchase, please do not remove from here-------------------*/
      if(isset($paymentControlCheck->id) AND trim($paymentControlCheck->id) != '') {
        HeAnalytics::scriptPurchaseUserConversion($paymentControlCheck);
      }
    /* -----------------------------------------------------------------------------------------------------------------*/
    ?>
  <?php endif; ?>
<?php endforeach; ?>

<div class="container ab-payment-wrapper" id="page">
  <?php foreach($msgs as $iErr => $stData ): ?>
    <?php $msgClass = "adyenResError"; ?>
    <?php $sMsg = Yii::t('mainApp', 'payments_adyen_err_000'); ?>
      <?php
      switch($stData["errCode"]) {
        case ADYEN_HPP_RESULT_OK:
          $sStyle =   "alert-success";
          $sMsg =     Yii::t('mainApp', 'payments_adyen_err_009');
          break;
        case ADYEN_HPP_RESULT_NOT_VALID_RESPONSE:
          $sStyle =   "alert-danger";
          $sMsg =     Yii::t('mainApp', 'payments_adyen_err_000');
          break;
        case ADYEN_HPP_RESULT_PENDING:
          $sStyle =   "alert-success";
          $sMsg =     Yii::t('mainApp', 'payments_adyen_err_003');
          break;
        case ADYEN_HPP_RESULT_REFUSED:
          $sStyle =   "alert-warning";
          $sMsg =     Yii::t('mainApp', 'payments_adyen_err_004');
          break;
        case ADYEN_HPP_RESULT_CANCELLED:
          $sStyle =   "alert-warning";
          $sMsg =     Yii::t('mainApp', 'payments_adyen_err_005');
          break;
        case ADYEN_HPP_RESULT_INTERNAL_ERR:
        default:
          $sStyle =   "alert-danger";
          $sMsg =     Yii::t('mainApp', 'payments_adyen_err_000');
          break;
      }
      ?>

      <div class="alert text-center m-b-30 <?php echo $sStyle; ?>" >
        <?php echo $sMsg; ?>

        <?php if($iErr == ADYEN_HPP_RESULT_OK): ?>
            <div class="loadingAdyenConfirmContainer">
                <span class="loadingPaypalInterior">&nbsp;</span>
            </div>
        <?php endif; ?>
      </div>

      <?php if($iErr == ADYEN_HPP_RESULT_OK): ?>
        <script>
          var hppInterval = null;
          var abaAdyenHppSuccess = {
          intervalFreq :  <?php echo $attInterval; ?>,
          action :        '/payments/gethppsuccess',
          hppData :       {
                            "idPayCtrlCk" :     "<?php echo $idPayCtrlCk; ?>",
                            "pspReference" :    "<?php echo $pspReference; ?>"
                          },

          getHppSuccess : function () {
            try {
              $.ajax({
                type:       'POST',
                url:        this.action,
                data:       this.hppData,
                dataType:   "json",
                beforeSend: function(){
                },
                success: function(response) {
                  try {
                    if ((typeof response.success) !== 'undefined' && (typeof response.redirectUrl) !== 'undefined' && response.success == true) {
                        clearIntervalHpp();
                        location.href = response.redirectUrl;
                    }
                    return;
                  }
                  catch(err) { }
                }
              });
            }
          catch (err) { }
        }
      };

      function clearIntervalHpp() { clearInterval(hppInterval); }
      $(function() { hppInterval = setInterval(function(){ abaAdyenHppSuccess.getHppSuccess() }, abaAdyenHppSuccess.intervalFreq); });
    </script>
  <?php endif; ?>
  <?php endforeach; ?>

  <div class="row m-b-30">
    <div class="col-sm-4 col-sm-offset-4">
      <a id="idDivBtnBackToCampus"  href="<?php echo Yii::app()->createUrl("course/index") ?>" class="ab-btn ab-btn-blue ab-btn-big">
        <?php echo strtoupper(Yii::t('mainApp', 'key_BackCampus')); ?>
      </a>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <ul class="ab-support-block">
        <li><?php echo Yii::t('mainApp', 'payments_contactUs1') ?></li>

        <li>
          <i class="glyphicon glyphicon-earphone"></i>

          <?php echo "+34 93 220 24 83"; ?>

          <?php $comUser = new UserCommon(); ?>
        </li>

        <li><?php echo Yii::t('mainApp', 'payments_contactUs2') ?></li>
      </ul>
    </div>
  </div>

  <div class="row ab-payment-explanation">
    <div class="col-md-6 mob-m-b-35">
      <h4 class="ab-payment-title with-border"><?php echo Yii::t('mainApp', 'payments_paybackTitle2'); ?></h4>
      <?php
        $guaranteedDesc = Yii::t('mainApp', 'payments_paybackInfo2', array('{userEmail}' => Yii::app()->user->getEmail()));
        $guaranteedDesc .= ''.Yii::t('mainApp', 'key_ExceptBoleto');
        $guaranteedDesc = str_replace('100% ','100% (*) ', $guaranteedDesc).'<br/>';
        echo $guaranteedDesc;
      ?>

      <span class="iconHoverVerisignSecured"></span>
      <span style="margin-left: 10px" class="iconHoverComodoSecured"></span>
    </div>

    <div class="col-md-6">
      <h4 class="ab-payment-title with-border"><?php echo Yii::t('mainApp', 'payments_supportTitle') ?></h4>

      <ul class="list-unstyled">
        <li><a href="mailto:support@abaenglish.com">support@abaenglish.com</a></li>
        <li>ABA English</li>
        <li class="m-t-15"><img src="<?php echo HeMixed::getUrlPathImg("support_girl.png"); ?>" /></li>
        <li><b>Maria Chiara</b></li>
        <li><?php echo Yii::t('mainApp', 'payments_supportInfo') ?></li>
      </ul>
    </div>
  </div>
</div>

<?php HeChatSupport::includeChatSupport(); ?>
