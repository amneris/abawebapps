<?php
/* ----------------------- PRODUCT SELECTION PAGE AND CHANGE PLAN SUBSCRIPTION----------------------------------------*/
/* ---------------------------------- STEP 1 for PAYMENT   -----------------------------------------------------------*/
/* -------------------- ************** NEW REFACTOR **************************************----------------------------*/
/* -------------------------------------------------------------------------------------------------------------------*/

/* @var array $isPaymentPlus */
/* @var array $products */
/* @var array $productsPlan */
/* @var string $packageSelected */
/* @var string $currentProduct  Current subscription in case of PREMIUM user */
/* @var bool|integer $anyDiscount  If any of the products has a discount */
/* @var integer $typeDiscount FINAL, PERCENTAGE OR AMOUNT */
/* @var string  $validationTypePromo */
/* @var string $urlContinue Flow to payment Page or to review change plans */
/* @var string $urlWithPromo */

/* @var AbaPaymentsBoleto $moBoleto */
/* @var string $boletoPrice */
/* @var string $productBoleto */

/* @var string $sPaymentLanguage */
/* @var integer $iPaymentCountry */

if(!$isPaymentPlus) {
    /* ------------------ TRACKING AND PIXELS to register a product selection, please do not remove from here-------------------*/
    HeAnalytics::scriptProductSelection(HeViews::getSelectedProduct($products, $packageSelected));
    /* -----------------------------------------------------------------------------------------------------------------*/
}

$enableTracking = !$isChangePlan ? 1 : 0;

$moCountry = new AbaCountry();
$moCountry->getCountryById($iPaymentCountry);
$countryIso = $moCountry->iso;
?>
<input type="hidden" id="trackEventsGoogleAnalytics" value="<?= $enableTracking ?>">
<input type="hidden" id="iso" value="<?= $countryIso ?>">

<div class="container ab-payment-wrapper" id="page">
    <div class="ab-prices ab-prices-js">
    <?php if(!$isChangePlan): ?>
        <div class="row">
            <div class="col-md-8 ab-payment-header">
                <h3 class="ab-payment-title">
                    <?php echo $isPaymentPlus ? Yii::t('mainApp', 'key_payPr_extendPremium') : Yii::t('mainApp', 'Tu suscripccion a') ?>
                    <span class="red">ABA Premium</span>
                    <?php if($sPaymentLanguage == 'en'): ?>
                        <span class="normal"><?php echo 'subscription'; ?></span>
                    <?php endif; ?>
                    <span class="normal">:</span>
                </h3>
            </div>
            <div class="col-md-4">
                <a href="<?php echo Yii::app()->createUrl('plans/abapremium') ?>" target="_blank" class="ab-payment-title-link"><?php echo Yii::t('mainApp', '(¿Qué es ') ?>ABA Premium?</a>
            </div>
        </div>

        <?php if ($isPaymentPlus) { ?>
            <h4 class="ab-payment-title"><?php echo Yii::t('mainApp', 'key_payPr_chooseExtendPayment') ?></h4>
        <?php } ?>
    <?php endif; ?>

    <?php echo CHtml::form(Yii::app()->createUrl($urlContinue), 'post', array("id" => "payment-form")); ?>
        <!--    Payment list-->
        <ul class="ab-packages">
            <?php
            $i = 0;
            $isSelectedExternal = false;

            if($isPaymentPlus) {
                $bestOption = 0;
            }
            else {
                $bestOption = isset($_GET['best_opt']) ? $_GET['best_opt'] : 12;
            }

            $oldPriceBottom = array('USD', 'MXN', 'BRL');

            foreach($products as $key => $productPrice) {

                if($productPrice['isPlanPrice'] == 0) {

                    $alwaysFinalPrice = $isPaymentPlus || $productPrice['ccyIsoCode'] == 'BRL'; // then show final price format, never monthly

                    $isSelected = false;

                    if($currentProduct) { // PREMIUM USER
                        if($packageSelected) {
                            if($packageSelected==$productPrice["productPricePK"]) {
                                $isSelected = true;
                                $isSelectedExternal = true;
                            }
                        } else if(!$isSelectedExternal) {
                            $isSelected = true;
                            $isSelectedExternal = true;
                        }
                    } else { // FREE USER
                        if ($packageSelected==$productPrice["productPricePK"]) {
                            $isSelected = true;
                            $isSelectedExternal = true;
                        }
                    } ?>

                    <!-- Start of package container -->
                    <li id="<?php echo 'idLi'.$productPrice['monthsPeriod']; ?>" class="<?php if($productPrice['monthsPeriod'] == $bestOption) echo 'best-option ' ?> <?php if($anyDiscount && $productPrice['amountDiscounted'] > 0.00) echo 'with-discount' ?>">
                        <!-- if has a discount -->
                        <?php if($anyDiscount && $productPrice['amountDiscounted'] > 0.00 && $typeDiscount==PERCENTAGE) { ?>
                            <div class="ab-discount percent">
                                <strong><?= $productPrice['discountTitle'] ?></strong>
                                <span><?= Yii::t('mainApp', 'payments_discountAbr'); ?></span>
                            </div>
                        <?php } else if ($anyDiscount && $productPrice['amountDiscounted'] > 0.00 && $typeDiscount==FINALPRICE) { ?>

                            <div class="ab-discount final">
                                <?php $priceStyled = HeViews::applyStylesInAmount($productPrice['finalPrice'], $productPrice, 'priceSmall', 'priceBig dscFinalPrice', 'priceSmall'); ?>
                                <?= $priceStyled[0] ?><?= $priceStyled[1] ?>
                            </div>
                        <?php } ?>

                        <div class="ab-package-header <?php if($productPrice['monthsPeriod'] == $bestOption) echo 'with-bg individual' ?>">
                          <?php echo Yii::t('mainApp', $productPrice['packageTitleKey']); ?>

                          <span class="ab-package-badge">
                                <?php
                                    if ($isPaymentPlus) {
                                        echo Yii::t('mainApp', 'key_payPr_PayNoRecurring');
                                    } else {
                                        echo Yii::t('mainApp', $productPrice['monthsPeriod'].'_mark');
                                    }
                                ?>
                            </span>
                        </div>

                        <div class="ab-package-body <?php if($currentProduct && $currentProduct == $productPrice['idProduct'] AND !$isPaymentPlus) { echo 'ab-current-product'; } ?>">
                            <div class="ab-price-block">
                                <?php if($currentProduct && $currentProduct == $productPrice['idProduct'] AND !$isPaymentPlus) { ?>
                                    <p class="subsdcription-date"><?php
                                        $dateToPay = HeDate::removeTimeFromSQL($dateToPay);
                                        $sDateToPay = HeDate::SQLToUserRegionalDate($dateToPay,'-',$iPaymentCountry);
                                        echo Yii::t('mainApp', 'payments_resumeRenew', array('{dateToPay}' => $sDateToPay));
                                    ?></p>
                                <?php } else {
                                    if($anyDiscount) { ?>
                                        <input type="hidden" id="productfinalpricedetectedOld" value="<?php echo  HeViews::formatAmountInCurrency( $productPrice['originalPrice'], $productPrice['ccyIsoCode'], $productPrice["ccyDisplaySymbol"]); ?>" />

                                        <?php
                                            if ($productPrice['amountDiscounted'] > 0.00) {
                                                $price = $alwaysFinalPrice ? $productPrice['originalPrice'] : $productPrice['monthlyPriceOriginal'];
                                                $priceStyled = HeViews::applyStylesInAmount($price, $productPrice); ?>
                                                <div class="ab-old-price">
                                                    <?php echo Yii::t('mainApp', 'payments_oldPrice') ?>
                                                    <?= $priceStyled[0] . $priceStyled[1] ?>
                                                    <?php echo $alwaysFinalPrice ? '&nbsp;' : Yii::t('mainApp', 'payments_SlashMonth'); ?>
                                                </div>
                                        <?php } ?>
                                    <?php }

                                    $cssForAmountInt = 'priceBig';
                                    $cssForAmountCents = 'priceSmall';

                                    $price = $alwaysFinalPrice ? $productPrice['finalPrice'] : $productPrice['monthlyPrice'];
                                    $priceStyled = HeViews::applyStylesInAmount($price, $productPrice, 'priceSmall', $cssForAmountInt, $cssForAmountCents);
                                ?>

                                <input type="hidden" id="productfinalpricedetected" value="<?php echo ($productPrice['ccyDisplaySymbol'] == "€") ? str_replace(".",",",$productPrice['finalPrice'])." ".$productPrice["ccyDisplaySymbol"] : $productPrice["ccyDisplaySymbol"].$productPrice['finalPrice'] ?>" />

                                <dl id="<?php echo 'idTable-'.$productPrice['monthsPeriod']; ?>" class="ab-main-price">
                                    <dt><?= $priceStyled[0] ?></dt>
                                    <dd>
                                        <strong><?= $priceStyled[1] ?></strong>
                                        <?php echo $alwaysFinalPrice ? '&nbsp;' : Yii::t('mainApp', 'payments_SlashMonth'); ?>
                                    </dd>
                                </dl>
                            </div>

                            <button id="BtnMonthContinue-<?php echo $productPrice['monthsPeriod']; ?>" type="button" class="payment-select-js ab-btn ab-btn-blue ab-btn-big" name="<?= $productPrice["productPricePK"] ?>" data-final-price="<?= $productPrice['finalPrice'] ?>">
                                <?php echo Yii::t('mainApp', 'Continuar'); ?>
                            </button>
                        <?php } ?>

                        <?php if($currentProduct && $currentProduct == $productPrice['idProduct'] AND !$isPaymentPlus) { ?>
                        </div>

                        <div class="ab-curent-label"><?php echo Yii::t('mainApp', 'payments_currentSubscription'); ?></div>
                        <?php } ?>
                        </div>
                    </li>
                <!-- End of package block-->
                <?php
                $i++;
                }
            } ?>

    <?php if(!$isPaymentPlus): ?>

<!--   *********************************     DOING FAMILY-->
<?php
    foreach($productsPlan as $key => $productPrice) {

        if($productPrice['isPlanPrice'] == 1) {

            $alwaysFinalPrice = $isPaymentPlus || $productPrice['ccyIsoCode'] == 'BRL'; // then show final price format, never monthly

            $isSelected = false;

            if($currentProduct) { // PREMIUM USER
                if($packageSelected) {
                    if($packageSelected==$productPrice["productPricePK"]) {
                        $isSelected = true;
                        $isSelectedExternal = true;
                    }
                } else if(!$isSelectedExternal) {
                    $isSelected = true;
                    $isSelectedExternal = true;
                }
            } else { // FREE USER
                if ($packageSelected==$productPrice["productPricePK"]) {
                    $isSelected = true;
                    $isSelectedExternal = true;
                }
            }
?>
    <li class="ab-family-tooltip">
        <h2 class="text-center"><?php echo Yii::t('mainApp', $productPrice['monthsPeriod'] . '_fp_title'); ?></h2>
        <p><?php echo Yii::t('mainApp', $productPrice['monthsPeriod'] . '_fp_description'); ?></p>
        <figure class="happy">
            <img src="<?php echo HeMixed::getUrlPathImg("happy.png"); ?>" alt=""/>
        </figure>
    </li>

    <!-- Start of package container --> <!-- !!! ab-family best-option with-discount !!! -->
    <li id="<?php echo 'idLi'.$productPrice['monthsPeriod']; ?>-family" class="ab-family <?php if($productPrice['monthsPeriod'] == $bestOption) echo 'best-option ' ?> <?php if($anyDiscount && $productPrice['amountDiscounted'] > 0.00) echo 'with-discount' ?>">
        <!-- if has a discount -->
        <?php if($anyDiscount && $productPrice['amountDiscounted'] > 0.00 && $typeDiscount==PERCENTAGE): ?>
            <div class="ab-discount percent">
                <strong><?= $productPrice['discountTitle'] ?></strong>
                <span><?= Yii::t('mainApp', 'payments_discountAbr'); ?></span>
            </div>
        <?php elseif($anyDiscount && $productPrice['amountDiscounted'] > 0.00 && $typeDiscount==FINALPRICE): ?>
            <div class="ab-discount final">
                <?php $priceStyled = HeViews::applyStylesInAmount($productPrice['finalPrice'], $productPrice, 'priceSmall', 'priceBig dscFinalPrice', 'priceSmall'); ?>
                <?php echo $priceStyled[0]; ?><?php echo $priceStyled[1]; ?>
            </div>
        <?php endif; ?>

        <div class="ab-package-header with-bg family">
            <?php echo Yii::t('mainApp', $productPrice['packageTitleKey']); ?>

            <span class="ab-package-badge">
<?php
                if ($isPaymentPlus) {
                    echo Yii::t('mainApp', 'key_payPr_PayNoRecurring');
                } else {
                    echo Yii::t('mainApp', $productPrice['monthsPeriod'].'_fp_subtitle');
                }
?>
            </span>
        </div>

        <div class="ab-package-body <?php if($currentProduct && $currentProduct == $productPrice['idProduct'] AND !$isPaymentPlus) { echo 'ab-current-product'; } ?>">
            <div class="ab-price-block">
                <?php if($currentProduct && $currentProduct == $productPrice['idProduct'] AND !$isPaymentPlus) { ?>
                    <p class="subsdcription-date"><?php
                        $dateToPay =    HeDate::removeTimeFromSQL($dateToPay);
                        $sDateToPay =   HeDate::SQLToUserRegionalDate($dateToPay, '-', $iPaymentCountry);
                        echo Yii::t('mainApp', 'payments_resumeRenew', array('{dateToPay}' => $sDateToPay));
                        ?></p>
<?php
                } else {
                    if($anyDiscount) {
?>
                    <input type="hidden" id="productfinalpricedetectedOld" value="<?php echo  HeViews::formatAmountInCurrency( $productPrice['originalPrice'], $productPrice['ccyIsoCode'], $productPrice["ccyDisplaySymbol"]); ?>" />
<?php
                    if ($productPrice['amountDiscounted'] > 0.00) {
                        $price = $alwaysFinalPrice ? $productPrice['originalPrice'] : $productPrice['monthlyPriceOriginal'];
                        $priceStyled = HeViews::applyStylesInAmount($price, $productPrice); ?>
                        <div class="ab-old-price">
                            <?php echo Yii::t('mainApp', 'payments_oldPrice') ?>
                            <?php echo $priceStyled[0] . $priceStyled[1]; ?>
                            <?php echo $alwaysFinalPrice ? '&nbsp;' : Yii::t('mainApp', 'payments_SlashMonth'); ?>
                        </div>
<?php
                    }
                }

                $cssForAmountInt =      'priceBig';
                $cssForAmountCents =    'priceSmall';

                $price =        $alwaysFinalPrice ? $productPrice['finalPrice'] : $productPrice['monthlyPrice'];
                $priceStyled =  HeViews::applyStylesInAmount($price, $productPrice, 'priceSmall', $cssForAmountInt, $cssForAmountCents);
?>

                <input type="hidden" id="productfinalpricedetected" value="<?php echo ($productPrice['ccyDisplaySymbol'] == "€") ? str_replace(".",",",$productPrice['finalPrice'])." ".$productPrice["ccyDisplaySymbol"] : $productPrice["ccyDisplaySymbol"].$productPrice['finalPrice'] ?>" />

                <dl id="<?php echo 'idTable-'.$productPrice['monthsPeriod']; ?>-family" class="ab-main-price">
                    <dt><?= $priceStyled[0] ?></dt>
                    <dd>
                        <strong><?= $priceStyled[1] ?></strong>
                        <?php echo $alwaysFinalPrice ? '&nbsp;' : Yii::t('mainApp', 'payments_SlashMonth'); ?>
                    </dd>
                </dl>
            </div>

            <button id="BtnMonthContinue-<?php echo $productPrice['monthsPeriod']; ?>-fp" type="button" class="payment-select-js ab-btn ab-btn-blue ab-btn-big" name="<?= $productPrice["productPricePK"] ?>" data-final-price="<?= $productPrice['finalPrice'] ?>">
                <?php echo Yii::t('mainApp', 'Continuar'); ?>
            </button>
            <?php } ?>
            <?php if($currentProduct && $currentProduct == $productPrice['idProduct'] AND !$isPaymentPlus) { ?>
        </div>
        <div class="ab-curent-label"><?php echo Yii::t('mainApp', 'payments_currentSubscription'); ?></div>
            <?php } ?>
        </div>
    </li>
    <!-- End of package block-->
<?php
            $i++;
        }
    }
?>
<?php endif; ?>

<!--   *********************************     DOING FAMILY-->
        </ul>
        <input id="idProductPricePKSelected" type="hidden" name="productPricePKSelected" value="<?= $packageSelected ?>" />
    <?php echo CHtml::endForm(); ?>

<?php echo $this->renderPartial('/payments/footercontent', array(
    "urlWithPromo" => $urlWithPromo,
    "validationTypePromo" => $validationTypePromo,
    "anyDiscount" => $anyDiscount,
)); ?>

    </div>
</div>

</div>

<?php HeChatSupport::includeChatSupport(); ?>

<?php if(!$isChangePlan): ?>
<?php
if($moBoleto){
    $btnPrintBoleto = '<a href="'.$moBoleto->url.'" target=blank class="printBoleto">'.Yii::t('mainApp', 'key_PrintOutBoleto').'</a>';
    $dueDate = HeDate::SQLToUserRegionalDate(HeDate::removeTimeFromSQL($moBoleto->dueDate),'-',$iPaymentCountry);
    $requestDate = HeDate::SQLToUserRegionalDate(HeDate::removeTimeFromSQL($moBoleto->requestDate),'-',$iPaymentCountry);
    ?>
    <div id="IdModalBoletoPending" class="req-boleto-top-sentence" style="display: none;">
      <div class="alert alert-warning m-b-30 m-t-15">
        <?php echo Yii::t('mainApp', 'key_DialogBoxBoleto', array( '{requestDate}'=> $requestDate,
                                                        '{dueDate}'=> $dueDate,
                                                        '{amountPrice}' => $boletoPrice,
                                                        '{product}'=> $productBoleto,
                                                        '{linkPrintBoleto}'=> $btnPrintBoleto,
                                                    ) ); ?>
      </div>
      <a id="idDivBtnPrintBoleto"  href="<?php echo $moBoleto->url; ?>" class="ab-btn ab-btn-blue ab-btn-big m-b-30" target="_blank" >
        <?php echo strtoupper(Yii::t('mainApp', 'key_PrintOutBoleto')); ?>
      </a>
    </div>

    <script language="JavaScript">
        $(document).ready(function(){
            campusUtils.openModalFromHtml('IdModalBoletoPending', 650, "auto");
        });
    </script>
<?php
}
?>
<?php endif; ?>

<?php
//#ABAWEBAPPS-693

$bCheck = true;

try {
    if(isset($bCheckCooladataUserData)) {
        $bCheck = $bCheckCooladataUserData;
    }
}
catch(Exception $e) { }

$sCooladata = HeCooladata::createDefaultEvent(HeCooladata::EVENT_NAME_OPENED_PRICES, $bCheck);
Yii::app()->clientScript->registerScript(md5($sCooladata), $sCooladata, CClientScript::POS_READY);
?>
