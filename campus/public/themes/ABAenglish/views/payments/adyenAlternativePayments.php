<?php if(isset($errMsg) AND trim($errMsg) != ""): ?>
    <div class="col-sm-offset-6 col-md-6">
        <div class="alert alert-danger" role="alert">
            <?php echo $errMsg; ?>
        </div>
    </div>
<?php else: ?>
    <script>
        var abaAdyen = {
            a : '/payments/processPaymentAdyen',
            sendAdyenMethod : function (brandCode){
                try {
                    $("#paymentBrandCode").val(brandCode);
                    $("#payment-form").attr('action', this.a);
                    $("#payment-form").submit();
                }
                catch (err) {}
            }
        };
    </script>

    <div class="alternativePayments">
        <?php if (empty($adyenHppPaymentMethods) OR count($adyenHppPaymentMethods) == 0): ?>
            <h2 class="alternativePayments-title-placeholder"><?php echo Yii::t('mainApp', 'payments_adyen_err_001'); ?></h2>
        <?php else: ?>
            <label class="col-sm-6 col-xs-12 control-label"><?php echo Yii::t('mainApp', 'payments_adyen_err_007'); ?></label>

            <div class="col-sm-6">
                <ul class="ab-alternative-payments-list">
                    <?php foreach($adyenHppPaymentMethods as $sKey => $stMethod): ?>
                        <li onclick="abaAdyen.sendAdyenMethod('<?php echo $stMethod['brandCode']; ?>');" title="<?php echo $stMethod['brandName']; ?>">
                            <em id="iconAdien_<?php echo $stMethod['brandCode']; ?>" class="icon-adyen icon-adyen-<?php echo $stMethod['brandCode']; ?>" onclick="abaAdyen.sendAdyenMethod('<?php echo $stMethod['brandCode']; ?>');" title="<?php echo $stMethod['brandName']; ?>" />
                            <span class="hidden"><?php echo $stMethod['brandName']; ?></span>
                            <?php if (isset($stMethod['prices']['currency']) AND isset($stMethod['prices']['finalPrice'])): ?>
                            <span class="sm-price"><?php echo Yii::t('mainApp', 'payments_adyen_msg_010', array('{amountWithCurrency}' => HeViews::formatWithoutAmountInCurrency( $stMethod['prices']['currency'], $stMethod['prices']['symbol']))); ?></span>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>
