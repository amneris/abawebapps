
<div class="row -ab-payment-form -with-bg -ab-payment-form-js">
    <?php
    if (!isset($dPay)) {
        $dPay = 0;
    }
    ?>
    <?php if ($dPay != 1) { ?>
        <div class="col-md-6 col-md-offset-3 marginTop20-pay">
            <div class="breadcrumbs-pay">
                <span class="breadcrumb-item left b1 b-active"><strong><?php echo Yii::t('mainApp', 'directpayment_step1'); ?></strong></span>
                <span class="breadcrumb-item right b2"><?php echo Yii::t('mainApp', 'directpayment_step2'); ?></span>
            </div>
        </div>

    <?php } else { ?>
        <div class="col-md-6 col-md-offset-3 marginTop20-pay">
            <div class="breadcrumbs-pay bp-active">
                <span class="breadcrumb-item bi-active left b-check"><?php echo Yii::t('mainApp', 'directpayment_step1'); ?></span>
                <span class="breadcrumb-item bi-active right b2 b-active"><?php echo Yii::t('mainApp', 'directpayment_step2'); ?></span>
            </div>
        </div>
    <?php }?>
</div>

<script>


</script>
