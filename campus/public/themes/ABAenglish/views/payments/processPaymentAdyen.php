
<?php if($paySuppExtId == PAY_SUPPLIER_ADYEN_HPP): ?>

<style>
    #adyen-payment-container {
        width: 921px;
        border: 1px solid #e1dfdc;
        background-color: #fff;
        padding-left: 20px;
        position: relative;
    }
    .adyen-payment-container-off {
        width: 100%;
        height: 900px;
        display: none;
    }
    .adyen-payment-container-on {
        width: 100%;
        height: 900px;
        display: inline;
    }
</style>

<div class="adyen-payment-container-off" id="adyen-payment-container">
    <form id="payment-order-form-<?php echo PAY_METHOD_ADYEN_HPP; ?>"
          action="<?php echo $adyenPostData['adyenPaymentUrl']; ?>"
          method="post">
        <input type="hidden" name="merchantReference"   value="<?php echo $adyenPostData['merchantReference']; ?>"/>
        <input type="hidden" name="paymentAmount"       value="<?php echo $adyenPostData['paymentAmount']; ?>"/>
        <input type="hidden" name="currencyCode"        value="<?php echo $adyenPostData['currencyCode']; ?>"/>
        <input type="hidden" name="shipBeforeDate"      value="<?php echo $adyenPostData['shipBeforeDate']; ?>"/>
        <input type="hidden" name="skinCode"            value="<?php echo $adyenPostData['skinCode']; ?>"/>
        <input type="hidden" name="merchantAccount"     value="<?php echo $adyenPostData['merchantAccount']; ?>"/>
        <input type="hidden" name="sessionValidity"     value="<?php echo $adyenPostData['sessionValidity']; ?>"/>
        <input type="hidden" name="shopperLocale"       value="<?php echo $adyenPostData['shopperLocale']; ?>"/>
        <input type="hidden" name="orderData"           value="<?php echo $adyenPostData['orderData']; ?>"/>
        <input type="hidden" name="countryCode"         value="<?php echo $adyenPostData['countryCode']; ?>"/>
        <input type="hidden" name="shopperEmail"        value="<?php echo $adyenPostData['shopperEmail']; ?>"/>
        <input type="hidden" name="shopperReference"    value="<?php echo $adyenPostData['shopperReference']; ?>"/>
        <input type="hidden" name="allowedMethods"      value="<?php echo $adyenPostData['allowedMethods']; ?>"/>
        <input type="hidden" name="blockedMethods"      value="<?php echo $adyenPostData['blockedMethods']; ?>"/>
        <input type="hidden" name="offset"              value="<?php echo $adyenPostData['offset']; ?>"/>
        <input type="hidden" name="recurringContract"   value="<?php echo $adyenPostData['recurringContract']; ?>"/>

<?php if(isset($adyenPostData['brandCode']) AND $adyenPostData['brandCode'] != ''): ?>
        <input type="hidden" name="brandCode" value="<?php echo $adyenPostData['brandCode']; ?>"/>
<?php endif; ?>

        <input type="hidden" name="merchantSig" value="<?php echo $adyenPostData['merchantSig']; ?>"/>
    </form>
    <div class="clear"></div>
</div>
<div class="clear"></div>

<script>
<!--
    $("#payment-order-form-<?php echo PAY_METHOD_ADYEN_HPP; ?>").submit();
//-->
</script>

<?php endif; ?>
