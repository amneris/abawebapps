<?php
/* @var PaymentForm|CModel $modelPayForm */
/* @var array $modelPayForm->productsPricesPlans
"productPricePK" => $productPrice->idProduct.DELIMITER_KEYS.$productPrice->idCountry.DELIMITER_KEYS.$productPrice->idPeriodPay,
"packageTitleKey" => $productPrice->getPackageTitleKey(),
"discountTitle" => $productPrice->getDiscountTitle(),
"validationTypePromo" => $productPrice->gettxtShowValidation(),
"originalPrice" => $productPrice->getOriginalPrice(),
"amountDiscounted" => $productPrice->getAmountDiscounted(),
"finalPrice" => $productPrice->getFinalPrice(),
"ccyDisplaySymbol" => $productPrice->getCcyDisplaySymbol(),
"finalPriceTransaction" => $productPrice->getFinalPriceTransaction(),
"ccyTransactionSymbol" => $productPrice->getCcyTransactionSymbol(),
"monthlyPriceOriginal" => $productPrice->getPriceMonthly(),
"monthlyPrice"         => $productPrice->getPriceMonthly(),
"monthsPeriod"         => $moPeriod->months,
 */
/* @var array  $listOfCreditCards */
/* @var array $urlsFormProcess */
/* @var string $urlReturn */
/* @var AbaUser $user */
/* @var integer $paySuppExtId */
/* @var array $aIdsPaysMethods; array of integers */
/* @var array $aSpecialFields; array of strings, name of the specific fields for the current payment gateway and country */
/* @var array $typeSubsStatus; Array with date, subscriptionType */
/* @var json $aAllFieldsByMethod */
/* @var bool $isPaymentPlus */
/* @var bool $isPlanPrice */
/* @var bool $isPlanPriceFree */
/* @var bool $isUpgrade */

$actionFormDefault = ($modelPayForm->idPayMethod) ? $urlsFormProcess[$modelPayForm->idPayMethod] : $urlsFormProcess[PAY_METHOD_CARD];

$aOptionalFieldsNoCard = array( 'firstName', 'secondName', 'street', 'city', 'state', 'zipCode' );
$readOnly = false;
if ($modelPayForm->isChangeProd) $readOnly = true;
?>

<script type="text/javascript">
    var PAY_METHOD_CARD =           '<?php echo PAY_METHOD_CARD; ?>';
    var PAY_METHOD_PAYPAL =         '<?php echo PAY_METHOD_PAYPAL; ?>';
    var PAY_METHOD_BOLETO =         '<?php echo PAY_METHOD_BOLETO; ?>';

    var PAY_METHOD_ADYEN_HPP =      '<?php echo PAY_METHOD_ADYEN_HPP; ?>';
    var ADYEN_HPP_PAYMENTAMOUNT =   '<?php echo $adyenPaymentData['paymentAmount']; ?>';
    var ADYEN_HPP_CURRENCYCODE =    '<?php echo $adyenPaymentData['currencyCode']; ?>';
    var ADYEN_HPP_SKINCODE =        '<?php echo $adyenPaymentData['skinCode']; ?>';
    var ADYEN_HPP_COUNTRYCODE =     '<?php echo $adyenPaymentData['countryCode']; ?>';
    var ADYEN_HPP_SESSIONVALIDITY = '<?php echo $adyenPaymentData['sessionValidity']; ?>';
//    var ADYEN_VERSION =             '<?php //echo $version; ?>//';
    var ADYEN_HPP_IDPRODUCT =       '<?php echo $adyenPaymentData['idProduct']; ?>';

    var fieldsByMethod = '<?php echo json_encode($aAllFieldsByMethod); ?>';
</script>


<?php
/* ------------------ TRACKINGS AND PIXELS to register a product selection, please do not remove from here-------------------*/
HeAnalytics::scriptPaymentMethod( HeViews::getSelectedProduct( $modelPayForm->productsPricesPlans, $modelPayForm->packageSelected) );
/* -----------------------------------------------------------------------------------------------------------------*/
?>

<?php if($paySuppExtId == PAY_SUPPLIER_ADYEN OR $paySuppExtId == PAY_SUPPLIER_ADYEN_HPP): ?>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/adyen.encrypt.min.js"></script>
<?php endif; ?>
<div class="container ab-payment-wrapper" id="page">
    <!-- START PAYMENT FORM -->
    <div class="div-payment-form">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="ab-payment-title"><?php echo Yii::t('mainApp', 'cardcreditdetails_key') ?>:</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="ab-payment-form with-bg ab-payment-form-js">
                    <?php
                    /* @var CActiveForm $frmYiiPayMethod */
                    $frmYiiPayMethod = $this->beginWidget('CActiveForm', array(
                        'id' => 'payment-form',
                        'action' => $actionFormDefault,
                        'enableClientValidation' => false,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                        ),
                    ));

                    echo $frmYiiPayMethod->hiddenField($modelPayForm, 'isChangeProd');
                    echo $frmYiiPayMethod->hiddenField($modelPayForm, 'packageSelected');
                    foreach($modelPayForm->productsPricesPlans as $value) {
                        echo $frmYiiPayMethod->hiddenField($modelPayForm, 'productsPricesPlans', array("value" => $value["productPricePK"]))."\n";
                    }
                    ?>

                    <?php
                    $promocodeTranslationKey = "";
                    $i = 0;
                    $isSelectedExternal = false;
                    $finalPrice = '';
                    $originalPrice = '';
                    $diffPrice = '';
                    $productName = '';
                    $currency = NULL;
                    $productPeriod = '';
                    foreach ($modelPayForm->productsPricesPlans as $key => $productPrice) {
                        $isSelected = "";
                        if($modelPayForm->packageSelected == $productPrice["productPricePK"]){

                            if(isset($productPrice['promocodeTranslationKey'])) {
                                $promocodeTranslationKeyTr = Yii::t('mainApp', $productPrice['promocodeTranslationKey']);
                                if($promocodeTranslationKeyTr != $productPrice['promocodeTranslationKey']) {
                                    $promocodeTranslationKey = $promocodeTranslationKeyTr;
                                }
                            }

                            $currency = $productPrice['ccyIsoCode'];
                            $isSelected = " checked ";
                            $isSelectedExternal = true;
                            $productName = Yii::t('mainApp', $productPrice['packageTitleKey']);
                            $productPeriod = Yii::t('mainApp', $productPrice['monthsPeriod']);
                            $finalPrice = HeViews::formatAmountInCurrency( $productPrice['finalPrice'], $productPrice['ccyIsoCode'], $productPrice["ccyDisplaySymbol"]);

                            if($isPlanPrice) {
                                if($productPrice['amountDiscounted'] > 0.00){
                                    $originalPrice =    HeViews::formatAmountInCurrency( $productPrice['originalPrice'], $productPrice['ccyIsoCode'], $productPrice["ccyDisplaySymbol"]);
                                    $diffPrice =        HeViews::formatAmountInCurrency( round($productPrice['originalPrice'] - $productPrice['finalPrice'], 2), $productPrice['ccyIsoCode'], $productPrice["ccyDisplaySymbol"]);
                                }
                            }
                            else {
                                if(((int)$productPrice['amountDiscounted']) > 0.00){
                                    $originalPrice = HeViews::formatAmountInCurrency( $productPrice['originalPrice'], $productPrice['ccyIsoCode'], $productPrice["ccyDisplaySymbol"]);
                                    $diffPrice = HeViews::formatAmountInCurrency( $productPrice['originalPrice']-$productPrice['finalPrice'], $productPrice['ccyIsoCode'], $productPrice["ccyDisplaySymbol"]);
                                }
                            }
                        }
                        $i++;
                    }

                    // MOVE IT FROM THE BOTTOM
                    $dateToPay = HeDate::removeTimeFromSQL($typeSubsStatus['dateToPay']);
                    $sDateToPay = ($isSelectedExternal===true) ? HeDate::SQLToUserRegionalDate($dateToPay,'-',Yii::app()->user->getCountryId()) : '';
                    $dateToStart = HeDate::removeTimeFromSQL($typeSubsStatus['dateStartSubscription']);
                    $sDateToStart = ($isSelectedExternal===true) ? HeDate::SQLToUserRegionalDate($dateToStart,'-',Yii::app()->user->getCountryId()) : '';
                    ?>

                    <!--********List of general ERRORS if any******** -->
                    <?php
                    $arrayErrors = $modelPayForm->getErrors();

                    if(isset($arrayErrors["PaymentControlProcess"]) || isset($arrayErrors["paymentProcess"]) || isset($arrayErrors["credit"])){ ?>
                        <div class="alert alert-danger ab-payment-alert ab-payment-alert-js" role="alert">
                            <ul>
                                <?php
                                if(isset($arrayErrors["PaymentControlProcess"])){
                                    foreach($arrayErrors["PaymentControlProcess"] as $value);
                                    echo '<li>'.$value.'</li>';
                                }
                                if(isset($arrayErrors["paymentProcess"])){
                                    foreach($arrayErrors["paymentProcess"] as $value2);
                                    echo '<li>'.$value2.'</li>';
                                }

                                if(isset($arrayErrors["credit"])){
                                    foreach($arrayErrors["credit"] as $value3);
                                    echo '<li>'.$value3.'</li>';
                                } ?>
                            </ul>
                        </div>
                    <?php } ?>
                    <?php $error = $frmYiiPayMethod->error($modelPayForm, 'idPayMethod') ?>
                    <?php if($error != "") : ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="alert alert-danger ab-payment-alert ab-payment-alert-js" role="alert">
                                    <?php echo $frmYiiPayMethod->error($modelPayForm, 'idPayMethod'); ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <!-- ------ End of general errors------ -->
                    <div class="row">
                        <div class="col-md-7">
                            <div class="ab-payment-fields">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-6 control-label"><?php echo Yii::t('mainApp', 'Email'); ?>:</label>
                                        <div class="col-sm-6">
                                            <?php echo $frmYiiPayMethod->emailField($modelPayForm, 'mail', array('readonly'=>true, "class"=>"readonly form-control")); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <?php echo $frmYiiPayMethod->labelEx($modelPayForm, 'idPayMethod', array('class' => 'col-sm-6 control-label')); ?>
                                        <div class="col-sm-6">
                                            <?php echo $this->renderPartial('/payments/listCardPayMethods',
                                                array('user'=>$user,
                                                    'modelPayForm'=>$modelPayForm,
                                                    'form'=>$frmYiiPayMethod,
                                                    'aMethodsAvailable'=>$aIdsPaysMethods,
                                                    'currency' => $currency,
                                                    'urlsFormProcess' => $urlsFormProcess,
                                                    'paySuppExtId' => $paySuppExtId
                                                )
                                            );
                                            ?>
                                        </div>
                                    </div>

                                    <?php if($paySuppExtId == PAY_SUPPLIER_ADYEN OR $paySuppExtId == PAY_SUPPLIER_ADYEN_HPP): ?>
                                        <?php /* +++ START ADYEN FIELDS +++ */ ?>
                                        <input type="hidden" id="payment-form-number" name="payment-form-number" value="" data-encrypted-name="number" />
                                        <input type="hidden" id="payment-form-holder-name" name="payment-form-holder-name" value="" data-encrypted-name="holderName" />
                                        <input type="hidden" id="payment-form-cvc" name="payment-form-cvc" value="" data-encrypted-name="cvc" />
                                        <input type="hidden" id="payment-form-expiry-month" name="payment-form-expiry-month" value="" data-encrypted-name="expiryMonth" />
                                        <input type="hidden" id="payment-form-expiry-year" name="payment-form-expiry-year" value="" data-encrypted-name="expiryYear" />

                                        <input type="hidden" id="payment-form-expiry-generationtime" name="payment-form-expiry-generationtime" value="<?php echo date("c"); ?>" data-encrypted-name="generationtime" />
                                        <div class="clear"></div>
                                        <?php /* +++ END ADYEN FIELDS +++ */ ?>
                                    <?php endif; ?>

                                    <input type="hidden" id="paymentBrandCode" name="paymentBrandCode" value="" />

                                    <?php if(isset($isPaymentPlus) && $isPaymentPlus === true): ?>
                                        <input type="hidden" id="isPaymentPlus" name="isPaymentPlus" value="1" />
                                    <?php else: ?>
                                        <input type="hidden" id="isPaymentPlus" name="isPaymentPlus" value="0" />
                                    <?php endif; ?>

                                    <section class="optional-form-groups-js">
                                        <?php foreach($aOptionalFieldsNoCard as $fieldOptName){ ?>
                                            <div id="IdDivInput_<?php echo $fieldOptName;?>" class="optional-form-group-js form-group <?php echo in_array($fieldOptName, $aSpecialFields) ? '' : 'hidden' ?>">
                                                <?php echo $frmYiiPayMethod->labelEx($modelPayForm, $fieldOptName, array('class' => 'col-sm-6 control-label')); ?>
                                                <div class="col-sm-6">
                                                    <?php
                                                    switch($fieldOptName) {
                                                        case 'state':
                                                            if ($user->countryId==COUNTRY_BRAZIL){
                                                                echo $frmYiiPayMethod->dropDownList($modelPayForm, 'state', PayAllPagoCodes::$aBrazilStates, array('disabled'=>$readOnly, 'class'=>'form-control') );
                                                            } else {
                                                                // Probably we will implement list of Mexico states and other countries.
                                                                echo $frmYiiPayMethod->textField($modelPayForm, $fieldOptName, array('readonly'=>$readOnly, 'class' => 'form-control'));
                                                            }
                                                            break;
                                                        default:
                                                            echo $frmYiiPayMethod->textField($modelPayForm, $fieldOptName, array('readonly'=>$readOnly, 'class' => 'form-control'));
                                                            break;
                                                    }
                                                    ?>
                                                </div>
                                                <div class="col-sm-6 col-sm-offset-6">
                                                    <?php echo $frmYiiPayMethod->error($modelPayForm, $fieldOptName, array('class' => 'ab-form-error')); ?>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <!-- Brazil CPF -->
                                        <?php if ( in_array('cpfBrasil', $aSpecialFields) ) { ?>
                                            <div id="IdDivInput_cpfBrasil" class="optional-form-group-js form-group <?php echo in_array('cpfBrasil', $aSpecialFields) ? '' : 'hidden' ?>">
                                                <?php echo $frmYiiPayMethod->labelEx($modelPayForm, 'cpfBrasil', array('class' => 'col-sm-6 col-xs-12 control-label')); ?>

                                                <div class="col-sm-3 col-xs-5">
                                                    <?php echo $frmYiiPayMethod->dropDownList($modelPayForm, 'typeCpf', array(PAY_ID_BR_CPF=>PAY_ID_BR_CPF,
                                                        PAY_ID_BR_CNPJ=>PAY_ID_BR_CNPJ), array('disabled'=>$readOnly, 'class' => 'form-control') ); ?>
                                                </div>

                                                <div class="col-sm-3 col-xs-7">
                                                    <?php if (in_array('cpfBrasil', $aSpecialFields) ) {
                                                        echo $frmYiiPayMethod->textField($modelPayForm, 'cpfBrasil', array("maxlength"=>19, 'class' => 'form-control', 'readonly'=>$readOnly));
                                                    } else {
                                                        echo $frmYiiPayMethod->hiddenField($modelPayForm, 'cpfBrasil');
                                                    } ?>
                                                </div>

                                                <div class="col-sm-6 col-xs-12 col-sm-offset-6">
                                                    <?php echo $frmYiiPayMethod->error($modelPayForm, 'cpfBrasil', array('class' => 'ab-form-error')); ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </section>
                                </div>

                                <div class="form-horizontal">
                                    <!-- Credit card details -->
                                    <?php $classCreditCardDetails = ($modelPayForm->idPayMethod !== PAY_METHOD_CARD) ? 'hidden': ''; ?>

                                    <div id="divCreditCardDetails" class="ab-credit-card-details-js <?php echo $classCreditCardDetails; ?>" >
                                        <!-- creditCardType -->
                                        <?php if($paySuppExtId == PAY_SUPPLIER_ADYEN OR $paySuppExtId == PAY_SUPPLIER_ALLPAGO_BR OR $paySuppExtId == PAY_SUPPLIER_ALLPAGO_MX): ?>
                                            <input type="hidden" name="PaymentForm[creditCardType]" id="PaymentForm_creditCardType" value="<?php echo KIND_CC_NO_CARD ?>" />
                                        <?php else: ?>
                                            <div class="form-group">
                                                <?php echo $frmYiiPayMethod->labelEx($modelPayForm, 'creditCardType', array('class' => 'col-sm-6 control-label')); ?>

                                                <div class="col-sm-6">
                                                    <?php echo $frmYiiPayMethod->dropDownList($modelPayForm, 'creditCardType', $listOfCreditCards, array('disabled'=>$readOnly, 'class' => 'form-control', 'empty' => Yii::t('mainApp', '(Select a card type)'))); ?>
                                                    <?php echo $frmYiiPayMethod->error($modelPayForm, 'creditCardType', array('class' => 'ab-form-error')); ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if($modelPayForm->isChangeProd AND ($paySuppExtId == PAY_SUPPLIER_ADYEN  OR $paySuppExtId == PAY_SUPPLIER_ALLPAGO_BR OR $paySuppExtId == PAY_SUPPLIER_ALLPAGO_MX)): ?>
                                            <!-- Empty if-->
                                        <?php else: ?>
                                            <div class="form-group">
                                                <?php echo $frmYiiPayMethod->labelEx($modelPayForm, 'creditCardName', array('class' => 'col-sm-6 control-label')); ?>

                                                <div class="col-sm-6">
                                                    <?php echo $frmYiiPayMethod->textField($modelPayForm, 'creditCardName', array('readonly'=>$readOnly, 'class' => 'form-control')); ?>
                                                    <?php echo $frmYiiPayMethod->error($modelPayForm, 'creditCardName', array('class' => 'ab-form-error')); ?>
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <?php echo $frmYiiPayMethod->labelEx($modelPayForm, 'creditCardNumber', array('class' => 'col-sm-6 control-label')); ?>

                                                <div class="col-sm-6">
                                                    <?php if($readOnly) {
                                                        $modelPayForm->creditCardNumber = HeViews::formatCardNumberSecret($modelPayForm->creditCardNumber, false);
                                                    }
                                                    echo $frmYiiPayMethod->textField( $modelPayForm, 'creditCardNumber', array('readonly'=>$readOnly, 'class' => 'form-control'));
                                                    ?>

                                                    <?php echo $frmYiiPayMethod->error($modelPayForm, 'creditCardNumber', array('class' => 'ab-form-error')); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <?php echo $frmYiiPayMethod->labelEx($modelPayForm, 'expirationDate', array('class' => 'col-sm-6 control-label col-xs-12')); ?>

                                                <div class="col-sm-3 col-xs-6">
                                                    <?php echo $frmYiiPayMethod->dropDownList($modelPayForm, 'creditCardMonth', Yii::app()->getLocale()->getMonthNames(), array('disabled'=>$readOnly, 'class' => 'form-control', 'empty' => ucwords(Yii::t('mainApp', '(Select a month)')))); ?>
                                                </div>

                                                <div class="col-sm-3 col-xs-6">
                                                    <?php echo $frmYiiPayMethod->dropDownList($modelPayForm, 'creditCardYear', HeDate::getDateYears( date('Y'), 0, 10), array('disabled'=>$readOnly, 'class' => 'form-control', 'empty' => ucwords(Yii::t('mainApp', '(Select a year)')))); ?>
                                                </div>

                                                <div class="col-sm-6 col-xs-12 col-sm-offset-6">
                                                    <?php echo $frmYiiPayMethod->error($modelPayForm, 'creditCardMonth', array('class' => 'ab-form-error')); ?>
                                                    <?php echo $frmYiiPayMethod->error($modelPayForm, 'creditCardYear', array('class' => 'ab-form-error')); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <?php echo $frmYiiPayMethod->labelEx($modelPayForm, 'CVC', array('class' => 'col-sm-6 col-xs-12 control-label label-inline')); ?>
                                                <div class="col-sm-3 col-xs-10 with-help">
                                                    <?php echo $frmYiiPayMethod->textField($modelPayForm, 'CVC', array('readonly'=>false, "class" => "form-control")); ?>
                                                    <div class="ab-help-wrapper">
                                                        <div class="ab-help-box">
                                                            <?php echo Yii::t('mainApp', 'Número de tres dígitos que se encuentra impreso en el dorso de la tarjeta, en el espacio reservado a tu firma.') ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12 col-sm-offset-6">
                                                    <?php echo $frmYiiPayMethod->error($modelPayForm, 'CVC', array( 'id'=>'idErrCVC', 'class' => 'ab-form-error')); ?>
                                                </div>
                                            </div>

                                            <!-- contra to kind of secure and reassure change of product  -->
                                            <?php if ($modelPayForm->isChangeProd){ ?>
                                                <div class="form-group">
                                                    <?php echo $frmYiiPayMethod->labelEx( $modelPayForm, 'pwdToChangeProd', array('class' => 'col-sm-6 control-label')); ?>
                                                    <div class="col-sm-6">
                                                        <?php echo $frmYiiPayMethod->passwordField( $modelPayForm, 'pwdToChangeProd', array("class" => "form-control")); ?>
                                                        <?php echo $frmYiiPayMethod->error( $modelPayForm, 'pwdToChangeProd', array('class' => 'ab-form-error')); ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        <?php endif; ?>
                                    </div><!-- Credit Card Details -->

                                    <?php $classAdyenHppDetails = ($modelPayForm->idPayMethod !== PAY_METHOD_ADYEN_HPP) ? "hidden": ""; ?>
                                    <div class="ab-adyen-details-js <?php echo $classAdyenHppDetails; ?>">
                                        <div class="form-group">
                                            <!-- Adyen HPP details -->
                                            <div class="ab-adyen-data-container-js"></div>
                                            <div class="ab-adyen-loader-js hidden">
                                                <div class="col-sm-offset-6 col-sm-6">
                                                    <?php
                                                    $imagePath = HeMixed::getUrlPathImg("loading.gif");
                                                    echo CHtml::image($imagePath, "Loading",  array ( "id"=>"loadingGif", 'class' => 'ab-loader') );
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                    if ($modelPayForm->isChangeProd) { ?>
                                        <div class="row changePlanInfo">
                                            <div class="col-xs-12">
                                                <p><?php echo Yii::t('mainApp', 'payments_changePlanInfo', array('{dateToStart}' => $sDateToStart)); ?></p>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-sm-offset-6 col-md-6">
                                                <?php
                                                // In test environment we constantly bump into the Plus missing products situtation, so to end up with this shitty situation and future complaints:
                                                // Also in case of any product from old platform with 0 euros final price value could appear we achieve potential payment.
                                                if($modelPayForm->productsPricesPlans[0]["finalPriceTransaction"]!=="0.0000"){  ?>
                                                    <?php if($paySuppExtId == PAY_SUPPLIER_ADYEN): ?>
                                                        <input name="btnABAadyen" id="btnABAadyen" type="submit" class="ab-btn ab-btn-blue ab-btn-big btn-payment-js" value="<?php echo Yii::t('mainApp', 'Enviar') ?>"/>
                                                    <?php else: ?>
                                                        <a name="btnABA" id="btnABA" class="ab-btn ab-btn-blue ab-btn-big btn-payment-js"><?php echo Yii::t('mainApp', 'Enviar') ?></a>
                                                    <?php endif; ?>

                                                    <?php
                                                    $imagePath = HeMixed::getUrlPathImg("loading.gif");
                                                    echo CHtml::image($imagePath, "Loading",  array ( "id"=>"loadingGif", 'class' => 'hidden ab-loader') );
                                                    ?>

                                                    <p id="redirectCreditCard" class="hidden"><?php echo Yii::t('mainApp', 'credit_card_redirect') ?></p>
                                                    <p id="redirectPaypal" class="hidden"><?php echo Yii::t('mainApp', 'paypal_redirect_key') ?></p>
                                                <?php } ?>

                                                <input id="urlPopup" type="hidden" value="<?php echo Yii::app()->createUrl('modals/popupCreditCardIssues'); ?>" />
                                                <input id="urlPopupPaypal" type="hidden" value="<?php echo Yii::app()->createUrl('modals/popupPaypalIssues'); ?>" />
                                                <sub>
                                                    <?php echo str_replace(array("{url1}", "{url2}"),
                                                        array(Yii::app()->createURL("terms/index", array("paySuppExtId"=>$paySuppExtId)),
                                                            Yii::app()->createURL("privacy/index")), str_replace(' <span class="required">*</span>', "",
                                                            $frmYiiPayMethod->labelEx($modelPayForm, 'acceptConditions', array("style" => "float: none; display: inline;"))));
                                                    ?>
                                                </sub>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $this->endWidget(); ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5 mob-with-bg">
                            <div class="ab-payment-info">
                                <div class="row">
                                    <div class="col-md-12 clearfix">
                                        <section>
                                            <h3 class="ab-payment-title"><?php echo Yii::t('mainApp', 'payments_resume') ?>:</h3>
                                            <ul id="divIdPurchaseSummary" class="list-unstyled">
                                                <li id="IdPPeriodicity">
                                                    <?php if(isset($isPaymentPlus) && $isPaymentPlus === true) {
                                                        echo Yii::t('mainApp', 'key_ExtendUrSubsc', array('{nMonths}' => $productPeriod));
                                                    } else {
                                                        echo Yii::t('mainApp', 'suscription_w').': ';
                                                        echo ($isSelectedExternal===true) ? $productName : '';
                                                    } ?>
                                                </li>

                                                <li id="IdPPlanMonth" >
                                                    <?php echo Yii::t('mainApp', 'payments_resumePeriod').': ';
                                                    if($isSelectedExternal === true){
                                                        echo Yii::t('mainApp', 'payments_resumePeriod'.$productPeriod.'Month');
                                                    }
                                                    ?>
                                                </li>

                                                <li id="IdPFromtoPeriod">
                                                    <?php if($isSelectedExternal === true) {
                                                        echo Yii::t('mainApp', 'payments_resumeFrom').' '.$sDateToStart.' '.Yii::t('mainApp', 'payments_resumeTo').' '.$sDateToPay;
                                                    } ?>
                                                </li>

                                                <li id="IdPDateToPay" class="m-b-30">
                                                    <?php echo Yii::t('mainApp', 'payments_resumeRenew', array('{dateToPay}' => $sDateToPay)); ?>
                                                </li>

                                                <?php if($originalPrice !== '') { ?>
                                                    <li style="text-decoration:line-through;"><?php echo ($isUpgrade ? Yii::t('mainApp', 'payments_originalPriceFamilyPlan') : Yii::t('mainApp', 'payments_originalPrice')) . ': '; ?>
                                                        <?php if($isSelectedExternal===true) {
                                                            echo $originalPrice;
                                                        } ?>
                                                    </li>
                                                    <li>
                                                        <?php echo ($isUpgrade ? Yii::t('mainApp', 'payments_diffPriceFamilyPlan') : Yii::t('mainApp', 'payments_diffPrice')) . ': '; ?>
                                                        <?php if($isSelectedExternal===true) {
                                                            echo $diffPrice;
                                                        } ?>
                                                    </li>
                                                <?php } ?>

                                                <li id="IdPFinalPrice" >
                                                    <b class="final-price <?php if($originalPrice !== ''){ echo 'red'; }?>">
                                                        <?php echo ($isUpgrade ? Yii::t('mainApp', 'payments_resumePriceFamilyPlan') : Yii::t('mainApp', 'payments_resumePrice')) . ': '; ?>
                                                        <?php if($isSelectedExternal===true) {
                                                            echo $finalPrice;
                                                        } ?>
                                                    </b>
                                                </li>
                                            </ul>
                                        </section>

                                        <?php if(trim($promocodeTranslationKey) != ''): ?>
                                            <section id="promocodeDescription" style="color: #9c0034;">
                                                <p class="ipad-float-section">
                                                    <?php echo $promocodeTranslationKey;  ?>
                                                </p>
                                            </section>
                                        <?php endif; ?>

                                        <?php if($isPlanPriceFree): ?>
                                            <section id="familyPlanDescription" style="color: #9c0034;">
                                                <p class="ipad-float-section">
                                                    <?php echo Yii::t('mainApp', 'familyPlanDescription'); ?>
                                                </p>
                                            </section>
                                        <?php endif; ?>

                                        <section>
                                            <p class="ipad-float-section">
                                                <?php
                                                if(isset($isPaymentPlus) && $isPaymentPlus === true) {
                                                    echo Yii::t('mainApp', 'key_payPr_continueStudying', array('{nMonths}' => $productPeriod));
                                                    echo '<br>';
                                                }

                                                echo Yii::t('mainApp', 'payments_resumeInfo');
                                                ?>
                                            </p>
                                            <?php $idproductUrl = explode("|",$modelPayForm->packageSelected); ?><br/>
                                            <?php if(!$isPlanPrice): ?>
                                                <a id="aSelectAnotherProd" class="ipad-float-link" href="<?php echo Yii::app()->createUrl($urlReturn, array("idproduct" => $idproductUrl[0])) ?>"><?php echo Yii::t('mainApp', 'Elige otro tipo de suscripción') ?></a>
                                            <?php endif; ?>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <ul class="ab-support-block">
                    <li><?php echo Yii::t('mainApp', 'payments_contactUs1') ?></li>

                    <li>
                        <i class="glyphicon glyphicon-earphone"></i>

                        <?php echo "+34 93 220 24 83"; ?>

                        <?php $comUser = new UserCommon(); ?>
                    </li>

                    <li><?php echo Yii::t('mainApp', 'payments_contactUs2') ?></li>
                </ul>
            </div>
        </div>

        <div class="row ab-payment-explanation">
            <div class="col-md-6 mob-m-b-35">
                <h4 class="ab-payment-title with-border"><?php echo Yii::t('mainApp', 'payments_paybackTitle2'); ?></h4>
                <?php
                $guaranteedDesc = Yii::t('mainApp', 'payments_paybackInfo2', array('{userEmail}' => Yii::app()->user->getEmail()));

                if ( in_array(PAY_METHOD_BOLETO, $aIdsPaysMethods) ) {
                    $guaranteedDesc .= ''.Yii::t('mainApp', 'key_ExceptBoleto');
                    $guaranteedDesc = str_replace('100% ','100% (*) ', $guaranteedDesc).'<br/>';
                }
                echo $guaranteedDesc;
                ?>

                <span class="iconHoverVerisignSecured"></span>
                <span style="margin-left: 10px" class="iconHoverComodoSecured"></span>
            </div>

            <div class="col-md-6">
                <h4 class="ab-payment-title with-border"><?php echo Yii::t('mainApp', 'payments_supportTitle') ?></h4>

                <ul class="list-unstyled">
                    <li><a href="mailto:support@abaenglish.com">support@abaenglish.com</a></li>
                    <li>ABA English</li>
                    <li>HOLA ENRIC</li>
                    <li class="m-t-15"><img src="<?php echo HeMixed::getUrlPathImg("support_girl.png"); ?>" </li>
                    <li><b>Maria Chiara</b></li>
                    <li><?php echo Yii::t('mainApp', 'payments_supportInfo') ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<?php if($paySuppExtId == PAY_SUPPLIER_ADYEN OR $paySuppExtId == PAY_SUPPLIER_ADYEN_HPP): ?>
    <script type="text/javascript">
        var form =      document.getElementById('payment-form');
        var key =       "<?php echo $adyenPublicKey; ?>";
        var options =   {};
        adyen.encrypt.createEncryptedForm( form, key, options);
    </script>
<?php endif; ?>

<?php HeChatSupport::includeChatSupport(); ?>
