<?php
/* @var PaymentForm $modelPayForm */
/* @var CActiveForm $form */
/* @var AbaUser $user */
/* @var array $urlsFormProcess */
/* @var array $aMethodsAvailable; array of integers */
/* @var string $currency */
/* @var integer $paySuppExtId */

$readOnly = false;

if (isset($modelPayForm->isChangeProd) && $modelPayForm->isChangeProd){
    $readOnly = true;
}
?>

<div id="divIdRadioPayType" class="row">
    <div class="ab-payment-radio-wrapper">
        <?php
        $cardsListClass = $currency == 'USD' ? 'def-card usd-card' : 'def-card';
        // Methods of payment available:
        $aLabels = array();

        foreach($aMethodsAvailable as $method) {
            $node = '';
            switch($method) {
                case PAY_METHOD_CARD:

                    $sPymentCardIcon = 'ccard-icon noamex-card';

                    if (
                      $currency == 'EUR'
                      OR
                      ($currency == 'USD' AND ($paySuppExtId == PAY_SUPPLIER_ADYEN OR $paySuppExtId == PAY_SUPPLIER_ADYEN_HPP))
                      OR
                      ($currency == 'BRL' AND $paySuppExtId == PAY_SUPPLIER_ALLPAGO_BR)
                    ) {
                        $sPymentCardIcon = 'ccard-icon';
                    }

                    $node = '<i class="' . $sPymentCardIcon . ' ' . $cardsListClass . ' paymentURL-js" data-url="' . $urlsFormProcess[PAY_METHOD_CARD] . '"></i>';

                    switch($user->countryId){
                        case COUNTRY_ITALY:
                            $node .= '<i class="ccard-icon italy-card"></i>';
                            break;
                        case COUNTRY_FRANCE:
                            $node .= '<i class="ccard-icon france-card"></i>';
                            break;
                    }
                    break;
                case PAY_METHOD_PAYPAL:
                    $node = '<i class="ccard-icon paypal-card paymentURL-js" data-url="'.$urlsFormProcess[PAY_METHOD_PAYPAL].'"></i>';
                    break;

                case PAY_METHOD_BOLETO:
                    $node = '<i class="ccard-icon boleto-card paymentURL-js" data-url="'.$urlsFormProcess[PAY_METHOD_BOLETO].'"></i>';
                    break;
                case PAY_METHOD_ADYEN_HPP:
                    $node = '<i class="ccard-icon adyen-card paymentURL-js" data-url="'.$urlsFormProcess[PAY_METHOD_ADYEN_HPP].'"></i> ' . Yii::t('mainApp', 'payments_adyen_err_008');
                    break;
            }

            $aLabels[$method] = $node;
        }
        $templateStr = "<div class='col-xs-12'><div class='payment-radio-wrapper'>{input}{label}</div></div>";
        $radioList3 = $form->radioButtonList($modelPayForm, 'idPayMethod', $aLabels, array('disabled'=> $readOnly, 'class' => 'payment-input-js', 'separator' => '', 'template' => $templateStr));

        $listReplace3 = preg_replace('<span([^\>]+)(s*[^\/])>', '', $radioList3, -1, $count);
        $List3 = str_replace('</span>', '', $listReplace3);
        $List3 = str_replace('<<', '<', $List3);
        $ListFinal3 = explode("<br />", $List3);
        foreach($ListFinal3 as $sublist3){
            echo ''.$sublist3.'';
        }
        ?>
    </div>
</div>