<?php
/**
 * @var string $urlBoleto
 * @var string $dueDateBoleto; Date
 */
/**
 * After Payment has been done - it doesn't matter wether through PayPal or La Caixa- we display the summary of all info--------------
 */
/*****************************************Variables passed by the controller action************************************/
/*   @var Payment $payment      */
/*********************************************************************************************************************/
?>

<?php
/* ------------------ TRACKINGS AND PIXELS to register a purchase, please do not remove from here-------------------*/
        HeAnalytics::scriptPurchaseUserConversion($payment);
/* -----------------------------------------------------------------------------------------------------------------*/
?>
<div class="div-payment-form">
    <div class="boleto-confirmation-request" style="height:310px;">
            <div class="clear"></div>
            <div class="pagoPasoTitulo11_2 marginTop15">
                <span class="normal"><?php echo Yii::t('mainApp', 'key_YaFaltaPoco') ?></span>
            </div>
            <div class="clear"></div>
            <div class="req-boleto-top-sentence">
                <span> <?php echo Yii::t('mainApp','key_payYourBoletoAndEnjoy'); ?> <span>
            </div>
            <div class="clear"></div>
            <br/>
            <div class="clear"></div>
            <div id="requestBoletoConfirmedColLeft" class="req-boleto-divColLeft">
                <p class="req-boleto-mini-sentence">
                    <img src="/themes/ABAenglish/media/images/boleto-printer.png" class="boleto-icons">
                    <?php echo Yii::t('mainApp', 'key_PrintClickBoleto') ?></p>
                <div class="clear"></div>
                <p class="req-boleto-mini-sentence">
                    <img src="/themes/ABAenglish/media/images/boleto-money.png" class="boleto-icons">
                    <?php echo Yii::t('mainApp', 'key_PayYouBoletoBankAgency') ?></p>
                <div class="clear"></div>
                <a id="idDivBtnPrintBoleto"  href="<?php echo $urlBoleto ?>" class="btn-blue-boleto" target="_blank">
                    <?php echo strtoupper(Yii::t('mainApp', 'key_PrintOutBoleto')); ?>
                </a>
            </div>
            <!--<div style="width: 50px; float:left;">&nbsp;</div>-->
            <div id="requestBoletoConfirmedColRight"  class="req-boleto-divColRight" >
                <p  class="req-boleto-mini-sentence">
                    <img src="/themes/ABAenglish/media/images/boleto-calendar.png" class="boleto-icons">
                    <?php echo Yii::t('mainApp', 'key_3daysValidBoleto') ?>
                </p>
                <div class="clear"></div>
                <p  class="req-boleto-mini-sentence">
                    <img src="/themes/ABAenglish/media/images/boleto-envelope.png" class="boleto-icons">
                    <?php echo Yii::t('mainApp', 'key_JustSendAnEmailBoleto') ?>
                </p>
                <div class="clear"></div>
                <a id="idDivBtnBackToCampus"  href="<?php echo Yii::app()->createUrl("course/index") ?>" class="btn-blue-boleto">
                    <?php echo strtoupper(Yii::t('mainApp', 'key_BackCampus')); ?>
                </a>
            </div>
    </div>
    <div class="contenedorContact" style="font-size---: 11px;">
        <span><?php echo Yii::t('mainApp', 'payments_contactUs1') ?></span>
        <span class="phoneIconWhite">&nbsp;</span>
        <span>

            <?php echo "+34 93 220 24 83"; ?><br />

            <?php $comUser = new UserCommon(); ?>
        </span>
        <span><?php echo Yii::t('mainApp', 'payments_contactUs2') ?></span>
    </div>
    <div class="clear"></div>

    <ul class="paymentsInfo">
        <li style="width:55%">
            <h3><?php echo Yii::t('mainApp', 'payments_paybackTitle2'); ?></h3>
            <?php
            $guaranteedDesc = Yii::t('mainApp', 'payments_paybackInfo2', array('{userEmail}' => Yii::app()->user->getEmail()));
            $guaranteedDesc .= ''.Yii::t('mainApp', 'key_ExceptBoleto');
            $guaranteedDesc = str_replace('100% ','100% (*) ', $guaranteedDesc).'<br/>';
            echo $guaranteedDesc;
            ?>
            <span class="iconHoverVerisignSecured"></span>
            <span style="margin-left: 10px" class="iconHoverComodoSecured"></span>
        </li>
        <li style="width:35%">
          <h3><?php echo Yii::t('mainApp', 'payments_supportTitle') ?></h3>
          <a href="mailto:support@abaenglish.com">support@abaenglish.com</a>
          <br>ABA English
          <br><img style="margin: 10px 0px;" src="../../themes/ABAenglish/media/images/support_girl.png" />
          <br><b>Maria Chiara</b>
          <br><?php echo Yii::t('mainApp', 'payments_supportInfo') ?>
        </li>
    </ul>

    <div class="clear"></div>
    <div class="marginBottom30"></div>

</div>

<?php HeChatSupport::includeChatSupport(); ?>