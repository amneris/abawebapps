<div class="row">
    <div class="col-sm-12">
        <div id="idPromocodeContainer" class="ab-promo-code clearfix">
            <div class="ab-promo-title">
                <?php echo Yii::t('mainApp', 'payments_discountCode1') ?>
                <strong id="sTypePromocode" class="blue cup promo-toggle-js"><?php echo Yii::t('mainApp',
                      'payments_discountCode2') ?></strong>?
            </div>

            <div class="ab-promo-wrapper promo-wrapper-js hide">
                <?php echo CHtml::form(Yii::app()->createUrl($urlWithPromo), 'get', array("id" => "promocode-form")); ?>
                <input type="hidden" name="promocode" id="promocode"/>
                <input type="text" class="promo-input-js"/>
                <button class="ab-btn ab-btn-blue promo-btn-js">OK</button>
                <?php echo CHtml::endForm(); ?>
            </div>

            <?php if (trim($validationTypePromo) != '') { ?>
                <span class="validation-error">
                            <?php
                            if ($anyDiscount > 0 && HeDate::validDateSQL($validationTypePromo)) {
                                echo " &nbsp;" . Yii::t('mainApp', ' Promoción especial hasta el ');
                                echo HeDate::SQLToEuropean($validationTypePromo, "-");
                            } elseif ($anyDiscount == 0 && HeDate::validDateSQL($validationTypePromo)) {
                                echo " &nbsp;" . Yii::t('mainApp', 'Promoción finalizada');
                            } else {
                                echo " &nbsp;" . Yii::t('mainApp', 'Promotion not valid');
                            }
                            ?>
                        </span>
            <?php } ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <ul class="ab-support-block">
            <li><?php echo Yii::t('mainApp', 'payments_contactUs1') ?></li>
            <li>
                <i class="glyphicon glyphicon-earphone"></i>

                <?php echo "+34 93 220 24 83"; ?>

                <?php $comUser = new UserCommon(); ?>
            </li>

            <li><?php echo Yii::t('mainApp', 'payments_contactUs2') ?></li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <h3 class="ab-payment-title with-border"><?php echo Yii::t('mainApp', 'Todos los contenidos') ?></h3>

        <p class="ab-feature-text complete-course-ico">
            <span class="ContenidosIcon left"></span>
            <?php echo Yii::t('mainApp',
              'Accede a todas las unidades del curso de forma ilimitada y disfruta de las videoclases y de los minifilms en cualquier momento.') ?>
        </p>
    </div>
    <div class="col-md-4">
        <h3 class="ab-payment-title with-border"><?php echo Yii::t('mainApp', 'Obtén los certificados') ?></h3>

        <p class="ab-feature-text certificates-ico">
            <span class="CertificadosIcon left"></span>
            <?php echo Yii::t('mainApp',
              'Solo con ABA Premium obtendrás un certificado expedido por American & British Academy cada vez que termines uno de los seis niveles.') ?>
        </p>
    </div>
    <div class="col-md-4">
        <h3 class="ab-payment-title with-border"><?php echo Yii::t('mainApp', 'Consultas ilimitadas') ?></h3>

        <p class="ab-feature-text unlimited-ico">
            <span class="ConsultasIlimitadasIcon left"></span>
            <?php echo Yii::t('mainApp',
              '¡Contacta con tu profesor siempre que lo necesites! Podrás realizar todas las consultas a los profesores de ABA de forma ilimitada en cualquier momento del día.') ?>
        </p>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <h3 class="ab-payment-title with-border">
            <?php echo Yii::t('mainApp', 'Qué comentan sobre') ?>
            <strong class="red">ABA</strong>
            <span class="red"><?php echo Yii::t('mainApp', 'Premium') ?></span>
        </h3>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <blockquote class="ab-blockquote">
            <p><?php echo Yii::t('mainApp',
                  'Nunca pensé que podría existir un curso de inglés que nos gustara a toda la familia: ¡nos lo pasamos muy bien y ya hablamos inglés con nuestros hijos!') ?></p>
            <footer>Cristina P. (45 <?php echo Yii::t('mainApp', 'años') ?>)<?php echo Yii::t('mainApp', ' y ') ?>David
                C. (48 <?php echo Yii::t('mainApp', 'años') ?>)
            </footer>
        </blockquote>
    </div>
    <div class="col-md-4">
        <blockquote class="ab-blockquote">
            <p><?php echo Yii::t('mainApp',
                  '¡Estoy totalmente enganchado a los cortometrajes de ABA! ¡Finalmente puedo entender todas las películas en inglés!') ?></p>
            <footer>Paul R. (22 <?php echo Yii::t('mainApp', 'años') ?>)</footer>
        </blockquote>
    </div>
    <div class="col-md-4">
        <blockquote class="ab-blockquote">
            <p><?php echo Yii::t('mainApp',
                  'Finalmente he conseguido vencer el miedo a las entrevistas de trabajo: ¡ahora estoy deseando que me entrevisten en inglés!') ?></p>
            <footer>Sophie S. (32 <?php echo Yii::t('mainApp', 'años') ?>)</footer>
        </blockquote>
    </div>
</div>