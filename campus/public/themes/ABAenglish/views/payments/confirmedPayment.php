<?php
/**
 * After Payment has been done - it doesn't matter wether through PayPal or La Caixa- we display the summary of all info--------------
 */
/*****************************************Variables passed by the controller action************************************/
/*   @var Payment $payment      */
/*   @var bool $isPaymentPlus      */
/*   @var int $months      */
/*   @var string $sPaymentConfirmMonths      */
/*   @var string $sPaymentConfirmDateToRenewal      */
/*   @var string $sPaymentConfirmPrice      */
/*********************************************************************************************************************/

// ** ZENDESK Help Center.
$zendeskHelpCenter = Yii::app()->config->get("ZENDESK_HELP_CENTER");
$zendeskHelpCenterABA = Yii::app()->config->get("ZENDESK_HELP_CENTER_ABA");
$zendeskHelpCenter = ($zendeskHelpCenterABA && str_replace('@abaenglish.com', '',
        $userData['email']) != $userData['email']) ? true : $zendeskHelpCenter;
$zendeskLanguage = (isset(Yii::app()->params['ZENDESK_LANGUAGETABLE'][Yii::app()->language])) ?
    Yii::app()->params['ZENDESK_LANGUAGETABLE'][Yii::app()->language] : Yii::app()->language;
// ** Zendesk only for english students.

$helpCenterLink = $zendeskHelpCenter ? $zendeskHelpCenter : Yii::app()->createUrl('help/index');

if (!$isPaymentPlus) {
    /* ------------------ TRACKINGS AND PIXELS to register a purchase, please do not remove from here-------------------*/
    HeAnalytics::scriptPurchaseUserConversion($payment);
    /* -----------------------------------------------------------------------------------------------------------------*/
}

$sConfirmationId = $payment->id;
if($payment->paySuppExtId == PAY_SUPPLIER_Z AND trim($payment->paySuppExtProfId) <> "") {
    $sConfirmationId = $payment->paySuppExtProfId;
}

?>

<script>
    // send event to google tag manager if user has just made premium payment
    pushPremiumLeadEvent();

    function pushPremiumLeadEvent(){
        // send premium lead event to google tag manager
        console.log("sending premium lead event");
        dataLayer.push({'event': 'premium_lead'});
    }
</script>

<div class="container">
    <div class="jumbotron text-center ab-confirm-payment">
        <h1 id="pIdCongratulations"><?php echo Yii::t('mainApp', '¡Enhorabuena!') ?> <?php echo Yii::t('mainApp', 'Tu compra se ha realizado con éxito') ?></h1>

        <hr/>

        <div class="row">
            <div class="ab-jumbotron col-sm-12">
                <p class="header"><b><?= Yii::t('mainApp', 'hello_user', array('{username}' => $userData['fullName'])); ?></b></p>
                <p class="header"><b><?= Yii::t('mainApp', 'paymentConfirm_info2'); ?></b></p>

                <p class="header"><?= Yii::t('mainApp', 'paymentConfirm_info3'); ?></p>

                <ul class="payment-confirmation-summary user-data col-sm-12 col-md-6">
                    <li><strong><?= Yii::t('mainApp', 'paymentConfirm_userSummary'); ?></strong></li>
                    <li><b><?= Yii::t('mainApp', 'paymentConfirm_userSummary_idPay'); ?> <?php echo $sConfirmationId; ?></b></li>
                    <li><?= Yii::t('mainApp', 'paymentConfirm_userSummary_name'); ?> <?= $userData['fullName']; ?></li>
                    <li><?= Yii::t('mainApp', 'paymentConfirm_userSummary_email'); ?> <?= $userData['email']; ?></li>
                </ul>

                <ul class="payment-confirmation-summary pay-data col-sm-12 col-md-6">
                    <li><strong><?= Yii::t('mainApp', 'paymentConfirm_paySummary'); ?></strong></li>
                    <li><b><?php echo Yii::t('mainApp', 'paymentConfirm_price') . " " . $sPaymentConfirmPrice; ?></b></li>
                    <li><?php echo Yii::t('mainApp', 'suscription_w') . ': ' . $sPaymentConfirmMonths; ?></li>
                    <li><?php echo $sPaymentConfirmDateToRenewal; ?></li>
                </ul>

                <div class="payment-confirmation-button col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                    <a id="idDivBtnStartPremium"  href="<?php echo Yii::app()->createUrl("course/index") ?>" class="ab-btn ab-btn-red ab-btn-big">
                        <?php echo $isPaymentPlus ? Yii::t('mainApp', 'Continuar') : Yii::t('mainApp', 'paymentConfirm_button') ?>
                    </a>
                </div>

                <p class="col-sm-12"><?php echo Yii::t('mainApp', 'paymentConfirm_description1', array('{helpDeskLink}' => $helpCenterLink)); ?></p>
                <p class="col-sm-12"><?php echo Yii::t('mainApp', 'payments_resumeInfoConfirmedPayment'); ?></p>
                <p class="col-sm-12"><?php echo Yii::t('mainApp', 'paymentConfirm_description2', array('{userEmail}' => $userData['email'])); ?></p>

            </div>
        </div>
    </div>
</div>

<?php
$sCooladata = HeCooladata::createDefaultEvent(HeCooladata::EVENT_NAME_OPENED_CONFIRMATION, true);
Yii::app()->clientScript->registerScript(md5($sCooladata), $sCooladata, CClientScript::POS_READY);

$stParams = array(
    "price_paid" => array("value" => number_format($payment->amountPrice, 2)),
    "discount_applied" => array("value" => number_format($payment->amountDiscount, 2)),
    "transaction_currency" => array("value" => $payment->currencyTrans),
    "product_bought" => array("value" => $payment->idPeriodPay)
);
$sCooladata = HeCooladata::createDefaultEvent(HeCooladata::EVENT_NAME_PAID, true, $stParams);
Yii::app()->clientScript->registerScript(md5($sCooladata), $sCooladata, CClientScript::POS_READY);
?>
<?php HeChatSupport::includeChatSupport(); ?>
