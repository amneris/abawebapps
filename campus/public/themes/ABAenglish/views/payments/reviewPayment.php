<?php
/**
 * Only to be called after setExpressChcekout, it is displayed after PayPal Login and Purchase-------------------------
 */
/*****************************************Variables passed by the controller action************************************/
/*   @var AbaUser $user */
/*   @var array  $productPrice
    $productPrice["finalPrice"]= $paymentControlCheck->amountPrice;
    $productPrice["originalPrice"]= $paymentControlCheck->amountOriginal;
    $productPrice["ccyTransactionSymbol"]= $paymentControlCheck->currencyTrans ;
    $productPrice["packageTitleKey"] = $prodDetails->getPackageTitleKey();
    $productPrice['ccyTransactionSymbol'] = $prodDetails->getCcyDisplaySymbol()." ".$prodDetails->getCcyTransactionSymbol();
 */
/*   @var string $urlDoExpressCheckout */
/*   @var array  $userPayPal   */
/*   @var string firstnamePayForm   */
/*   @var string lastnamePayForm   */
/*********************************************************************************************************************/
?>


<div class="clear"></div>
<div class="centralContainermainColumn1" style="background-color: #fff;">
    <div id="divIdTitleReviewPayment" class="titleReviewPayment">
        <p><?php echo Yii::t('mainApp', "review_confirmation_paypal_key_0") ?></p>
        <p class="confirmPaypal"><?php echo Yii::t('mainApp', "review_confirmation_paypal_key_1") ?></p>
        <input id="urlPopup" type="hidden" value="<?php echo Yii::app()->createUrl('modals/popupPaypalProcessing'); ?>" />
    </div>
    <div class="clear"></div>
    <div class="buyCarReviewPaymentContainer left">
        <div class="buyCarReviewPaymentTitle">
            <?php echo Yii::t('mainApp', "Tu compra") ?>
        </div>
        <div id="divIdBuyCarReviewPaymentBody" class="buyCarReviewPaymentBody">
            <p>
                <span class="BigBold"><?php echo Yii::t('mainApp', 'Nombre de usuario ABA: ').$user->email ?></span>
            </p>
            <p>
                <span class="Bold"><?php echo Yii::t('mainApp', 'Cuenta PayPal: ') ?></span>
                <span id="spIdPayPalAccount" class="normal"><?php echo $userPayPal["Payer"]; ?></span>
            </p>
            <p>
                <span class="Bold"><?php echo Yii::t('mainApp', 'Importe: ') ?></span>
                <span class="normal"><?php echo $productPrice['finalPrice']." ".$productPrice['ccyTransactionSymbol'] ?></span>
            </p>
            <p>
                <span class="Bold"><?php echo Yii::t('mainApp', 'Recurrencia de los pagos: ') ?></span><span class="normal"><?php echo $productPrice['packageTitleKey'] ?></span>
            </p>
            <div class="separatorBuyCarReviewPaymentBody"></div>
            <p>
                <span class="Bold"><?php echo Yii::t('mainApp', 'Recurrencia de los pagos: ') ?></span>
                <span class="Bold">
                        <span id="spIdAmountPrice"><?php echo $productPrice['originalPrice']; ?></span>
                        <?php echo " ".$productPrice['ccyTransactionSymbol']; ?>
                </span>
            </p>
            <a id="aBtnConfirmPayPal" class="expressCheckoutButton" href="<?php echo $urlDoExpressCheckout; ?>">
                <div>
                    <?php echo Yii::t('mainApp', 'Confirmar') ?>
                </div>
            </a>
            <div id="IdPPProcessingSendPayment" class="loadingPaypalContainer" style="display: none;">
                <span class="loadingPaypalInterior">&nbsp;</span>
            </div>
        </div>
    </div>
    <div class="ContenedorDudas2 left">
        <div class="titulo">
            <span class="normal"><?php echo Yii::t('mainApp', '¿Dudas?') ?></span>
            <span class="red"><?php echo Yii::t('mainApp', 'Llámanos') ?></span>
        </div>
        <span class="phoneIcon left"></span>
        <div class="clear"></div>
        <div class="telf">
            <span class="prefix">(+34)</span>
            <span class="number">902 024 386</span>
        </div>
        <div class="daysOpen">
            <?php echo Yii::t('mainApp', 'horarios') ?>
        </div>
    </div>
    <div class="clear"></div>
    <div class="contenedorCaracteristicasABAPago marginLeft20 left">
        <div class="contenedorCaracteristicasABAPagoTitulo">
            <?php echo Yii::t('mainApp', 'Te devolvemos el dinero') ?>
        </div>
        <div class="clear"></div>
        <div class="contenedorCaracteristicasABAPagoBody">
            <span class="dias30Icon left"></span>
            <span class="descripcionCaracteristicaPago left"><?php echo Yii::t('mainApp', 'Tienes 30 días para probar el curso. Si no estás satisfecho te devolvemos el dinero.') ?></span>
        </div>
    </div>
    <div class="contenedorCaracteristicasABAPago left">
        <div class="contenedorCaracteristicasABAPagoTitulo">
            <?php echo Yii::t('mainApp', 'Cancela cuando quieras') ?>
        </div>
        <div class="clear"></div>
        <div class="contenedorCaracteristicasABAPagoBody">
            <span class="devolucionIcon left"></span>
            <span class="descripcionCaracteristicaPago left"><?php echo Yii::t('mainApp', 'Puedes cancelar cuando quieras accediendo a ') ?><a href="<?php echo Yii::app()->createUrl("profile/index") ?>"><?php echo Yii::t('mainApp', 'Mi cuenta') ?></a></span>
        </div>
    </div>
    <div class="contenedorCaracteristicasABAPago left">
        <div class="contenedorCaracteristicasABAPagoTitulo">
            <?php echo Yii::t('mainApp', 'Pago seguro') ?>
        </div>
        <div class="clear"></div>
        <div class="contenedorCaracteristicasABAPagoBody">
            <span class="securePaymentIcon left"></span>
            <span class="descripcionCaracteristicaPago left"><?php echo Yii::t('mainApp', 'Nuestro sitio web cumple los máximos requisitos de seguridad en materia de protección de datos y pago seguro.') ?></span>
        </div>
    </div>
    <div class="clear"></div>    
</div>

<?php HeChatSupport::includeChatSupport(); ?>