<?php
/**
 * ${NAME}
 * Author: mgadegaard
 * Date: 25/03/2015
 * Time: 16:10
 * © ABA English 2015
 */
?>
<html>
<head>
    <meta http-equiv="refresh" content="0;url=campusapp://"/>
</head>
<body>
<script type="application/javascript">
    //will redirect to $url after 3 secs.
    setTimeout(
        function () {
            window.location.href = "http://<?php echo $url ?>";
        },
        3000
    );
</script>
</body>
</html>