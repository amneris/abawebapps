<div id="modal">
    <h3><?php echo Yii::t('mobile', 'Email_sent') ?></h3>
    <p><?php echo Yii::t('mobile', 'Email_sent_desc_1') ?></p>
    <p><?php echo Yii::t('mobile', 'Email_sent_desc_2') ?></p>
    <a id="email_accept" href="#">OK</a>
</div>