<?php
/**
 * @var string $signature
 * @var integer $userId
 * @var integer $lang
 * @var boolean $userType
 * @var bool $sendEmail
 *
 */
?>
<div id="wrapper">
    <?php if (isset($sendEmail) && $sendEmail==0 && $userType==FREE) { ?>
        <div id="try_premium">
            <?php echo Yii::t('mobile', 'Try_Premium_1') ?><strong><?php echo Yii::t('mobile', 'ABA English') ?></strong><?php echo Yii::t('mobile', 'Try_Premium_2') ?><br/>
            <?php echo Yii::t('mobile', 'Try_Premium_3') ?>
        </div>
        <div id="boton_try">
            <input type="hidden" id="url_Popup" value="<?php echo Yii::app()->createAbsoluteUrl('mobile/tryforfree',
                array("signature"=>$signature,
                    "userId"=>$userId ,
                    "lang"=>$lang,
                    "userType"=>$userType,
                    'sendEmail' => 1)) ?>">
            <a id="try_for_free_button" href="#">
                <?php echo Yii::t('mobile', 'Try_Button') ?></a>
        </div>
        <p id="email_notif"><?php echo Yii::t('mobile', 'Email_notif') ?></p>
    <?php } elseif(isset($complete) || $userType==PREMIUM){ ?>
        <div class="boton_prem">
            <p><?php echo Yii::t('mobile', 'Working_Full_App') ?></p>
        </div>
    <?php } ?>
    <div class="flexslider">
        <ul class="slides">
            <li>
                <img src="<?php echo HeMixed::getUrlPathImg("mobile/aba_films.jpg"); ?>" />
                <div id="desc">
                    <h2><?php echo Yii::t('mobile', 'Aba_Films') ?></h2>
                    <p><?php echo Yii::t('mobile', 'Aba_Films_desc') ?></p>
                </div>
            </li>
            <li>
                <img src="<?php echo HeMixed::getUrlPathImg("mobile/habla.jpg"); ?>" alt="" />
                <div id="desc">
                    <h2><?php echo Yii::t('mobile', 'Habla') ?></h2>
                    <p><?php echo Yii::t('mobile', 'Habla_desc') ?></p>
                </div>
            </li>
            <li>
                <img src="<?php echo HeMixed::getUrlPathImg("mobile/escribe.jpg"); ?>" />
                <div id="desc">
                    <h2><?php echo Yii::t('mobile', 'Escribe') ?></h2>
                    <p><?php echo Yii::t('mobile', 'Escribe_desc') ?></p>
                </div>
            </li>
            <li>
                <img src="<?php echo HeMixed::getUrlPathImg("mobile/interpreta.jpg"); ?>" />
                <div id="desc">
                    <h2><?php echo Yii::t('mobile', 'Interpreta') ?></h2>
                    <p><?php echo Yii::t('mobile', 'Interpreta_desc') ?></p>
                </div>
            </li>
            <li>
                <img src="<?php echo HeMixed::getUrlPathImg("mobile/videoclass.jpg"); ?>" />
                <div id="desc">
                    <h2><?php echo Yii::t('mobile', 'Videoclase') ?></h2>
                    <p><?php echo Yii::t('mobile', 'Videoclase_desc') ?></p>
                </div>
            </li>
            <li>
                <img src="<?php echo HeMixed::getUrlPathImg("mobile/ejercicios.jpg"); ?>" />
                <div id="desc">
                    <h2><?php echo Yii::t('mobile', 'Ejercicios') ?></h2>
                    <p><?php echo Yii::t('mobile', 'Ejercicios_desc') ?></p>
                </div>
            </li>
            <li>
                <img src="<?php echo HeMixed::getUrlPathImg("mobile/vocabulario.jpg"); ?>" />
                <div id="desc">
                    <h2><?php echo Yii::t('mobile', 'Vocabulario') ?></h2>
                    <p><?php echo Yii::t('mobile', 'Vocabulario_desc') ?></p>
                </div>
            </li>
            <li>
                <img src="<?php echo HeMixed::getUrlPathImg("mobile/evaluacion.jpg"); ?>" />
                <div id="desc">
                    <h2><?php echo Yii::t('mobile', 'Evaluacion') ?></h2>
                    <p><?php echo Yii::t('mobile', 'Evaluacion_desc') ?></p>
                </div>
            </li>
        </ul>
    </div>
</div>
