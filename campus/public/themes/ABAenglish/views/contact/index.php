<?php
/* @var ContactForm $modelContact */
/* @var string $msgConfirmation */
/* @var string $typeUser */

$moUser = new AbaUser();
$moUser->getUserById(Yii::app()->user->getId());
$paySuppExtId = PaymentFactory::whichCardGatewayPay($moUser);
?>

<div class="contenedorAyuda left">
    <div class="tituloSeccionCampus"><?php echo Yii::t('mainApp', 'Contacta con nosotros') ?></div>
    <div class="columnLeftContact left">

        <?php if($msgConfirmation!==""){ ?>
            <p><?php echo Yii::t('mainApp', $msgConfirmation); ?></p>
        <?php }
        else{ ?>

            <p><?php echo Yii::t('mainApp', 'Si no encuentras respuesta a tus preguntas en nuestra sección ') ?><a href="<?php echo Yii::app()->createUrl('help/index') ?>"><?php echo Yii::t('mainApp', 'Ayuda') ?></a> <?php echo Yii::t('mainApp', 'contacta con nosotros a través del siguiente formulario, ¡te reponderemos lo antes posible!') ?></p>
            <p><?php echo Yii::t('mainApp', 'Recuerda que para enviar consultas de tipo lingüístico a tu profesor deberás acceder a la sección ') ?><a href="<?php echo Yii::app()->createUrl('message/inbox/inbox') ?>"><?php echo Yii::t('mainApp', 'Mensajes') ?></a><?php echo Yii::t('mainApp', ' del campus') ?>.</p>
            <?php
            $frmWidgetContact = $this->beginWidget('CActiveForm', array(
                'id' => 'contact-form',
                'action' => $this->createURL('/contact/processcontactinfo'),
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>
            <div class="containerFormContact">
                <!--<?php //echo $frmWidgetContact->errorSummary($modelContact); ?>-->
                <div class="row">
                    <?php echo $frmWidgetContact->error($modelContact, 'subject'); ?>
                </div>
                <div class="clear"></div>
                <div class="row">
                    <label class="required">
                        <?php echo Yii::t('mainApp', 'Asunto') ?>
                        <span class="required">*</span>
                    </label>
                </div>
                <div class="clear"></div>
                <?php if ($typeUser == "5") { ?>
                    <div class="row">
                        <?php echo $frmWidgetContact->dropDownList($modelContact, 'subject', $modelContact->aListSubjects, array('empty' => Yii::t('mainApp', 'selectsubject_key'))); ?>
                    </div>
                <?php
                } else {
                    ?>
                    <div class="row">
                        <?php echo $frmWidgetContact->dropDownList($modelContact, 'subject', $modelContact->aListSubjects, array('empty' => Yii::t('mainApp', 'selectsubject_key'))); ?>
                    </div>
                <?php } ?>
                <div class="clear"></div>
                <div class="separator"></div>
                <div class="row">
                    <?php echo $frmWidgetContact->error($modelContact, 'body'); ?>
                </div>
                <div class="clear"></div>
                <div class="row contactMessage">
                    <label class="required">
                        <?php echo Yii::t('mainApp', 'Mensaje') ?>
                        <span class="required">*</span>
                    </label>
                </div>
                <div class="clear"></div>
                <div class="row">
                    <?php echo $frmWidgetContact->textArea($modelContact, 'body', array("rows" => "6")); ?>
                </div>
                <div class="clear"></div>
                <div class="row">
                    <?php echo $frmWidgetContact->error($modelContact, 'conditions'); ?>
                </div>
                <div class="clear"></div>
                <div style="margin-top:10px"></div>

                <div class="row">
                    <?php echo CHtml::submitButton(Yii::t('mainApp', 'Enviar'), array("class" => "contact-button")); ?>
                </div>
            </div>
            <?php $this->endWidget(); ?>

        <?php } ?>
    </div>

    <div class="columnRightContact left">
        <div class="contenedorDudasContact left">
            <div class="titulo">
                <?php
                $comUser = new UserCommon();
                $country = $comUser->getCountryForPhonesByIp(Yii::app()->request->getUserHostAddress());
                if ($country==COUNTRY_PORTUGAL || $country==COUNTRY_FRANCE || $country==COUNTRY_ITALY){
                    ?>
                    <span class="normal"><?php echo Yii::t('mainApp', '¿Dudas?') ?></span>
                    <span class="green"><?php echo Yii::t('mainApp', 'Llámanos_gratis') ?></span>
                <?php
                } else {
                    ?>
                    <span class="normal"><?php echo Yii::t('mainApp', '¿Dudas?') ?></span>
                    <span class="green"><?php echo Yii::t('mainApp', 'Llámanos') ?></span>
                <?php
                }
                ?>

            </div>
            <span class="phoneIconContact left"></span>
            <div class="clear"></div>
            <div class="telfContact">
                <span class="number">
                    <?php
                    if ($typeUser == "5"){
                        echo  '</span><span class="number">(+34)</span>'. " " .'<span class="number">934 091 396</span>';
                    }
                    else{
                    if($moUser->idPartnerSource != PARTNER_JUNTAEXTREMADURA){
                        $comUser = new UserCommon();

                        switch ($comUser->getCountryForPhonesByIp(Yii::app()->request->getUserHostAddress())) {
                            case COUNTRY_PORTUGAL:
                                echo '</span><span class="number">'.Yii::t('mainApp', 'pref').'</span>'. " " .'<span class="number">'.Yii::t('mainApp', 'telf').'</span>';
                                break;
                            case COUNTRY_FRANCE:
                                echo '</span><span class="number">(+33)</span>'. " " . '<span class="number">975 18 91 82</span>';
                                break;
                            case COUNTRY_ITALY:
                                echo "+39 041 852 0059". '</span>';
                                break;
                            case COUNTRY_SPAIN:
                                echo "94 458 0254". '</span>';
                                break;
                            case COUNTRY_BRAZIL:
                                echo "+55 11 4933 7783". '</span>';
                                break;
                            default:
                                echo  '</span><span class="number">'.Yii::t('mainApp', 'pref').'</span>'. " " .'<span class="number">'.Yii::t('mainApp', 'telf').'</span>';
                        }
                    }else{
                    // JUNTA EXTREMADURA. B2B PATCH
                    ?>
                    910228782 <br/> 652180744</span>
                <?php }} ?>
                </span>
            </div>
            <div class="horario"><?php echo Yii::t('mainApp', 'horarios') ?></div>
        </div>
        <div class="clear"></div>
        <p class="OficinasCentrales"><?php echo Yii::t('mainApp', 'Nuestras oficinas centrales están situadas en Barcelona, España') ?>.</p>
        <p class="bold">ABA English</p>
        <p>c/ Aribau, 240 - 7º</p>
        <p>08006 Barcelona · <?php echo Yii::t('mainApp', 'España') ?></p>
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d23946.5691179851!2d2.1425691999999996!3d41.3888317!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4987d0c07e427%3A0xad41d16ec420fcc7!2sABA+English%2C+Online+English+School!5e0!3m2!1ses!2ses!4v1423226295474" width="229" height="161" frameborder="0" style="border:0"></iframe>
    </div>
</div>