var studentActions  = {
    config : {
        identifier: "thisUnit",
        generalTooltipClass: "tip_top",
        selectedClass: 'selected',
        mediaFont: 'f3',
        normalFont: 'f4',
        recordRoleClass: "recordRole",
        playRoleClass: "playRole",
        blockTextClass: "block_text",
        leftColumnClass: "column_left",
        rightColumnClass: "column_right",
        classStepComplete: "stepComplete",
        listenerTooltipClass: "listenerTooltip",
        //answerMessage: "porfavor, selecciona un campo de texto y haz clic en el bot&oacute;n \"?\""
        answerMessage: "porfavor, selecciona un campo de texto"
    },

     /**
     * inintialize object
     * @param $config
     */
    init: function( config ){
        var $memo = $("body");
        var data = $memo.data();
        var sSectionUnit = ABA_env.getSection();
        //Adds data from the call to the options object
        studentActions.config = $.extend(studentActions.config, config);
        //debug.log("unit instanciada con : "+studentActions.config.identifier);
        //Set-Up main properties
        studentActions.setUpGadgetIdentifier(studentActions.config.identifier);
        // attach listener on start building;
        studentActions.attachListenerUnitReady();

        studentActions.loadEvents(sSectionUnit);
        studentActions.buildUi(studentActions.$gadget);

        // notify end
        studentActions.notifyUnitReady();
    },

     /**
     * Set-Up of the gadget identifier, in case we are the identifier. 
     * The default value is the "body" tag.
     */
    setUpGadgetIdentifier: function (id){
        if (studentActions.config.identifier !== null){
            studentActions.$gadget = $("#" + id);
        }
        else{
            studentActions.$gadget = $("body");
        }
    },

    /**
     * Builds the initially required user interface elements
     * @param $gadget
     */
    buildUi: function ($gadget){
        var sSection =  ABA_env.getSection();
        //studentActions.loadCufon();
        studentActions.setNumberQuestions(0);
        switch(sSection){
            case "NEWWORDS":
                PROXY_env.HTMLGetNewWords();
                studentActions.loadAllTooltips();
                //studentActions.loadPaginateWidget();
                break;
            case "MINITEST":						
                minitestActions.loadPaginateWidgetMinitestCase($gadget);
                break;
            case "ROLEPLAY": 
                studentActions.manageChangesRolePlaySection();
                //studentActions.loadPaginateWidget();
                studentActions.resizeBlockPagePadding(sSection);
                break;
            case "ROLEPLAY_SELECT":
                break;
            case "DICTATION":
                //studentActions.loadPaginateWidget();
                studentActions.resizeBlockPagePadding(sSection);
                studentActions.loadAllTooltips();
                break;
            case "SPEAKING":
                studentActions.resizeBlockPagePadding(sSection);
            default: 
                //studentActions.loadPaginateWidget();
                studentActions.loadAllTooltips();
                break;
        }
    },

     /**
     * Resizing of paddings for the block text of the sections 
     * that require liquid position 
     * @param sSection
     */
    resizeBlockPagePadding: function(sSection){
        var $blockText = studentActions.$gadget.find("."+studentActions.config.blockTextClass);
        var sPaddingBlockText = $blockText.css("padding-top");
        var nPaddingBlockText = parseInt(sPaddingBlockText.replace("px",""));
        var nPaddingSpaceFilled = $blockText.size() * nPaddingBlockText; 

        var $colummRight = studentActions.$gadget.find("."+studentActions.config.rightColumnClass);
        var nColHeight = 0 ;
        var nPaddingColHeight = 0;
        $colummRight.each(function(){
                nPaddingColHeight = $(this).innerHeight();
                nColHeight = nColHeight + nPaddingColHeight;
        });

        //var nTotalHeader = studentActions.$gadget.find("#notebook_header").outerHeight();

        var sPositionEndElement = 0;
        var nPositionEndElement = 0;
        var nblockButtonsSpace = 0;
        var rolebuton = 0;
        switch (sSection){
            case "DICTATION":
                nblockButtonsSpace = studentActions.$gadget.find("#answerElement").outerHeight();
                //nPositionEndElement = "426";
                nPositionEndElement = parseInt(Math.round($("div#notebook_content_DICTATION").outerHeight() - $("div#answerElement").outerHeight()));
            break;
            case "SPEAKING":
                nblockButtonsSpace = 0;
                sPositionEndElement = studentActions.$gadget.find("#answerElement").css("top");
                nPositionEndElement = parseInt(sPositionEndElement.replace("px",""));
            break;
            case "ROLEPLAY":
                nblockButtonsSpace = studentActions.$gadget.find(".block_buttons").outerHeight();
                //nPositionEndElement = "360";
                if($("div#RoleButton").outerHeight() !== undefined)
                    rolebuton = $("div#RoleButton").outerHeight();
                else
                    rolebuton = $("div#playButton").outerHeight();
                nPositionEndElement = parseInt(Math.round($("div#notebook_content_ROLEPLAY").outerHeight() - rolebuton));
            break;
        }

        var nTotalSpace = nColHeight + nPaddingSpaceFilled + nblockButtonsSpace;//nTotalHeader + 
        var nFreeSpace = nPositionEndElement - nTotalSpace; 

        var nFreeSpacePerBlock = 0;

        switch (sSection){
            case "DICTATION":
            case "SPEAKING":
                nFreeSpacePerBlock = parseInt(nFreeSpace / ($blockText.size() + 2) );
            break;
            case "ROLEPLAY":
                nFreeSpacePerBlock = parseInt((nFreeSpace / ($blockText.size() + 2)) - (parseInt($("div.block_text").css("margin-top").replace("px",""))));
            break;
        }

        if ($blockText.size()>3){
            studentActions.$gadget.find("."+studentActions.config.blockTextClass).css("padding-top",nFreeSpacePerBlock+nPaddingBlockText);
        }
    },

     /**
     * Changes of the section Roleplay.
     * Manage the selected roles by the user and remove the default changes 
     */
    manageChangesRolePlaySection: function(){
        studentActions.$gadget.find(".block_buttons").show();
        var $target= null;
        var sRoleRecord  	= ABA_env.getRoleRecord();
        //debug.log("sRoleRecord:"+sRoleRecord);
        $target = studentActions.$gadget.find("[role='"+sRoleRecord+"']");
        $target.removeClass();
        $target.addClass(studentActions.config.mediaFont);
        $target.addClass(studentActions.config.recordRoleClass);

        var sRolePlay = ABA_env.getRolePlay();
        $.each(sRolePlay, function(){
            //debug.log("sRolePlay:"+this);
            $target = studentActions.$gadget.find("[role='"+this+"']");
            $target.removeClass();
            $target.addClass(studentActions.config.normalFont);
            $target.addClass(studentActions.config.playRoleClass);	
        });
    }, 

    /**
     * Load the special fonts to this gadget
     */
    /*loadCufon: function(){
            studentActions.$gadget.find(".infoItem").show();
            studentActions.$gadget.find(".infoItemSp").show();
            studentActions.$gadget.find(".f15").show();
            studentActions.$gadget.find(".f7").show();
            studentActions.$gadget.find(".f16").show();
            studentActions.$gadget.find(".f17").show();

            Cufon.replace(".f5");
            Cufon.replace(".f6");
            Cufon.replace(".f7");
            Cufon.replace(".f11");
            Cufon.replace(".f12");
            Cufon.replace(".f13");
            Cufon.replace(".f14");
            Cufon.replace(".f15");
            Cufon.replace(".f16");
            Cufon.replace(".f17");
            Cufon.now();
    },*/

    /**
     * Load the events related to this gadget
     * @param sSection
     */
    loadEvents: function(sSection){
        studentActions.$gadget.undelegate();
        switch(sSection){
            case "MINITEST":
                studentActions.$gadget
                                .delegate( "#arrows", "click", $.proxy(this.managePaginateWidgetButtonsMinitestCase, this.target) )
                                .delegate( "input[type='checkbox']" , "click", $.proxy(this.manageCheckboxInput, this.target) )
                                //.delegate( ".buttonCheck", "click",  $.proxy(this.manageCheckButtonClick, this.target) );
                                .delegate( ".botonFinalizar", "click", $.proxy(this.manageCheckButtonClick, this.target) );
                break;
                case "ROLEPLAY_SELECT":
                    studentActions.$gadget
                                    .delegate( ".blueButton", "click", $.proxy(this.manageRolePlaySelection, this.target) );
                    break;
                case "DICTATION":
                case "WRITING":
                    studentActions.$gadget
                            .delegate( "div.buttonAnswer", "click", $.proxy(this.manageAnswerActions, this.target) )
                            .delegate( "div.txtAnswer", "click", $.proxy(this.manageAnswerActions, this.target) )
                            .delegate( "#arrows", "click", $.proxy(this.managePaginateWidgetButtons, this.target) );
                            /*.delegate( ".buttonAnswer img", "hover", 		$.proxy(this.manageRolloverEffectsAnswerActions, this.target) )
                            .delegate( ".buttonAnswer img", "mousedown", 	$.proxy(this.managePressEffectsAnswerActions, this.target) )
                            .delegate( ".buttonAnswer img", "mouseup", 		$.proxy(this.manageLeaveEffectsAnswerActions, this.target) );*/
                    break;
                case "ROLEPLAY":
                    studentActions.$gadget
                                    .delegate( ".block_buttons", "click", $.proxy(this.manageRolePlayProcess, this.target) );
                default: 
                    studentActions.$gadget
                                    .delegate( "#arrows", "click", $.proxy(this.managePaginateWidgetButtons, this.target) )
                                    .delegate( ".anteriorText", "hover", $.proxy(this.manageRolloverEffectIconPaginacionAnterior, this.target) );
                    break;
        }
    },

    //		hideButtonAnswer: function(){
    //			if(studentActions.$gadget==undefined){	
    //				
    //			}
    //		},

    //		showButtonAnswer: function($target){
    //			var $inputElement = $target.filter("input");	
    //			if( (studentActions.$gadget==undefined) || ($inputElement.size() == 0) ){
    //				
    //			}
    //		},

    manageAnswerActions: function(event){
        ABA_utils.cancelBubble(event);
        var $tooltipHolders = studentActions.$gadget.find("."+studentActions.config.generalTooltipClass+":visible");

        if($tooltipHolders.size()>0){

            $tooltipHolders.each(function(){
                var linkId = $(this).attr("linkid");
                var okText = $(this).attr("okText");
                studentActions.$gadget.find("#answerElement .response").html(okText.replace(/\\'/g, "'"));
                $("#"+linkId).filter("input").focus();
            });

        } else {
            studentActions.$gadget.find("#answerElement .response").html(studentActions.config.answerMessage);
        }
    },

    managePressEffectsAnswerActions: function(event){
        var $target = $(event.target);
        var $targetContainer = $target.parent(); 
        $targetContainer.addClass("buttonTooltip_press");
        setTimeout(function(){
            $targetContainer.removeClass("buttonTooltip_press");
        }, 200);
    },

    manageLeaveEffectsAnswerActions: function(event){	
        var $target = $(event.target);
        var $targetContainer=$target.parent();
        $targetContainer.removeClass("buttonTooltip_press");
    },

    manageRolloverEffectsAnswerActions: function(event){
        var $target = $(event.target);
        var $targetContainer=$target.parent();
        if (event.type == 'mouseenter') {
            $targetContainer.addClass("buttonTooltip_rollover");
        } else {
            $targetContainer.removeClass("buttonTooltip_rollover");
            $targetContainer.addClass("buttonTooltip");
        }
    },
    //metodo por cflo
    manageRolloverEffectIconPaginacionAnterior: function(event){
        var $target = $(event.target);
        var $targetContainer=$target.siblings("#previous");

        if (event.type == 'mouseenter') {
            $targetContainer.removeClass("botonAnterior");
            $targetContainer.addClass("botonAnterior_hover");
        } else {
            //alert("hola2");
            $targetContainer.removeClass("botonAnterior_hover");
            $targetContainer.addClass("botonAnterior");
        }
    },

     /**
     * Set-Up of the gadget identifier, in case we are the identifier. 
     * The default value is the "body" tag.
     * @param event
     */
    manageRolePlaySelection: function(event){
        var $target = $(event.target);
        var $buttonTarget = $target.closest(".blueButton");
        var sRole = $buttonTarget.attr("id");

        ABA_env.resetRolePlay();
        ABA_env.setRoleRecord(sRole);

        var $otherRoles = studentActions.$gadget.find(".blueButton").not($buttonTarget);
        $otherRoles.each(function(){
            sRole = $otherRoles.attr("id");
            ABA_env.setRolePlay(sRole);
        });

        masterActions.jumpSection("ROLEPLAY");
    },

     /**
     * refresh element of the number of questions answered in NEWWORDS section.
     * @param number
     */
    refreshNumberQuestions: function(number){
        studentActions.$gadget.find("#numberQuestions").html(number);
        //Cufon.refresh();
    },

     /**
     * getter of the number of questions answered in NEWWORDS section.
     */
    getNumberQuestions: function(){
        return studentActions.config.numberQuestions;
    },

     /**
     * setter of the number of questions answered in NEWWORDS section.
     * @param number
     */
    setNumberQuestions: function(number){
        studentActions.config.numberQuestions = number;
    },

     /**
     * call to external minitest.js (minitestActions)
     * @param event
     */
    manageCheckButtonClick: function(event){
        minitestActions.valida(studentActions.$gadget);			
    },

     /**
     * Set-Up of the gadget identifier, in case we are the identifier. 
     * The default value is the "body" tag.
     */
    loadAllTooltips: function(){
        $.each(studentActions.config.tooltipParams, function(){
            studentActions.$gadget.find("."+this.tooltipStarterClass).tooltp_init(this);
        });
    },

     /**
     * manageRolePlayProcess: 
     * the main action function for the Roleplay Section
     * @param event
     */
    manageRolePlayProcess: function(event){
        var $target = $(event.target);
        var $buttonPress =  $target.closest(".block_buttons");
        var sAction = $buttonPress.attr("action");
        ABA_utils.cancelBubble(event);
        $otherButtons = studentActions.$gadget.find(".block_buttons").not($buttonPress);

        switch(sAction){
            case "startRole":
                //debug.log("startRole");
                $buttonPress.addClass(studentActions.config.selectedClass);
                studentActions.disableBlockTextButton($otherButtons);
                studentActions.changeToStopButton($buttonPress);
                studentActions.nextStepRolePlayProccess();
                $buttonPress.attr("action","stopRole");
                $buttonPress.find("[text='start']").siblings(".playicon").hide();
                $buttonPress.find("[text='start']").hide();
                $buttonPress.find("[text='stop']").siblings(".pararicon").show();
                $buttonPress.find("[text='stop']").show();

                break;

            case "stopRole":
                //debug.log("stopRole");
                $buttonPress.removeClass(studentActions.config.selectedClass);
                studentActions.enableBlockTextButton($otherButtons);
                studentActions.changeToPlayButton($otherButtons);
                studentActions.changeToRecordButton($buttonPress);
                studentActions.stopRolePlayProccess();
                $buttonPress.attr("action","startRole");
                $buttonPress.find("[text='stop']").siblings(".pararicon").hide();
                $buttonPress.find("[text='stop']").hide();
                $buttonPress.find("[text='start']").show();
                $buttonPress.find("[text='start']").siblings(".playicon").show();
                break;

            case "startPlay":
                //debug.log("startPlay");
                $buttonPress.addClass(studentActions.config.selectedClass);
                studentActions.disableBlockTextButton($otherButtons);
                studentActions.changeToStopButton($buttonPress);
                studentActions.nextStepRolePlayProccess();
                $buttonPress.attr("action","stopPlay");
                $buttonPress.find("[text='start']").siblings(".escucharicon").hide();
                $buttonPress.find("[text='start']").hide();
                $buttonPress.find("[text='stop']").show();
                $buttonPress.find("[text='stop']").siblings(".pararescuchaicon").show();
                break;

            case "stopPlay":
                //debug.log("stopPlay");
                $buttonPress.removeClass(studentActions.config.selectedClass);
                studentActions.enableBlockTextButton($otherButtons);
                studentActions.changeToPlayButton($buttonPress);
                studentActions.stopRolePlayProccess();
                $buttonPress.attr("action","startPlay");
                $buttonPress.find("[text='stop']").siblings(".pararescuchaicon").hide();
                $buttonPress.find("[text='stop']").hide();
                $buttonPress.find("[text='start']").show();
                $buttonPress.find("[text='start']").siblings(".escucharicon").show();
                break;
        }

    },
    changeToStopButton: function($target){
        $target.removeClass("play");
        $target.removeClass("stop");
        $target.removeClass("record");
        $target.addClass("stop");
    },
    changeToPlayButton: function($target){
        $target.removeClass("play");
        $target.removeClass("stop");
        $target.removeClass("record");
        $target.addClass("play");
    },
    changeToRecordButton: function($target){
        $target.removeClass("play");
        $target.removeClass("stop");
        $target.removeClass("record");
        $target.addClass("record");
    },

     /**
     * disableBlockTextButton:
     * disable the button passed by param
     * @param $target: disable the button passed by param
     */
    disableBlockTextButton: function($target){
        if(!$target.hasClass("disable")){
            var sAction = $target.attr("action");
            $target.attr("action","none");
            $target.attr("memoaction",sAction);
            $target.addClass("disable");
        }
    },

     /**
     * enableBlockTextButton:
     * enable the button passed by param
     * @param $target: disable the button passed by param
     */
    enableBlockTextButton: function($target){
        var isAnyBlockRecorded = false;
        var $blockRecorded = studentActions.$gadget.find(".recordRole.rolePlayStepComplete");
        if($blockRecorded.size()>0){
            isAnyBlockRecorded = true;
        }
        if($target.hasClass("disable")){
            var sAction = $target.attr("memoaction");
            $target.attr("action",sAction);
            $target.removeClass("disable");
        }
    },

     /**
     * nextStepRolePlayProccess:
     * set next step to roleplay process depending of the $buttonPress mode
     * load the tooltip needed for the role
     */
    nextStepRolePlayProccess: function(){
        $buttonPress = studentActions.$gadget.find(".block_buttons").filter("."+studentActions.config.selectedClass);
        var sMode = $buttonPress.attr("id");
        if (sMode!=undefined) {

            var $workingListener = studentActions.$gadget.find("."+studentActions.config.listenerTooltipClass);
            var $nextStep = studentActions.$gadget.find("[role]").not("."+studentActions.config.classStepComplete).eq(0);
            //debug.log("first element");
            //debug.log($nextStep);
            var sRole = $nextStep.attr("role");

            if(sRole!=undefined){

                $.each(studentActions.config.tooltipParams, function(){

                    switch(sMode){
                        case "RoleButton":

                            if ($nextStep.hasClass(this.tooltipStarterClass)){
                                //debug.log("mached tooltip params!!!!!!");
                                //debug.log(this);

                                var extendedOptions = {};
                                extendedOptions.serverOriginAudio = "demo";

                                $nextStep.tooltp_init( $.extend(this,extendedOptions) );
                            }
                            //alert("hola3");
                            break;
                        case "playButton":

                            if(this.tooltipStarterClass == "playRole"){
                                //debug.log("mached tooltip params!!!!!!");
                                //debug.log(this);

                                var extendedOptions = {};

                                if($nextStep.hasClass(studentActions.config.recordRoleClass)){
                                        extendedOptions.serverOriginAudio = "student";
                                } else {
                                        extendedOptions.serverOriginAudio = "demo";
                                }

                                $nextStep.tooltp_init( $.extend(this,extendedOptions) );
                            }
                            //alert("hola4");
                            break;
                    }

                });

                $nextStep.click();
                $nextStep.addClass(studentActions.config.classStepComplete);
                //debug.log("current listener");
                //debug.log($workingListener);
                $workingListener.tooltp_Unbind();

            } else {
                $buttonPress.click();
            }
        }
    },

     /**
     * stopRolePlayProccess:
     * stop the roleplay process publish  click Event on toottip 
     * and unbinding the events handler attached to this
     */
    stopRolePlayProccess: function(){
        var $workingListener = studentActions.$gadget.find("."+studentActions.config.listenerTooltipClass);
        var $ElementsStep = studentActions.$gadget.find("[role]");
        $ElementsStep.removeClass(studentActions.config.classStepComplete);
        $notebook = $ElementsStep.closest("#notebook");
        $.fn.tooltp_deleteTooltip($notebook);
        $workingListener.tooltp_Unbind();
    },

     /**
     * stopRolePlayProccess:
     * stop the roleplay process publish  click Event on toottip 
     * and unbinding the events handler attached to this
     */
    attachListenerUnitReady: function(){
        var sSectionUnit = null;
        $.subscribe("/unitEvents/unitReady", function(event) { 
            sSectionUnit = ABA_env.getSection();
            if (sSectionUnit=="ROLEPLAY"){
                sSectionUnit="ROLEPLAY_SELECT";
            }
            masterActions.setStepBreadCrumb(sSectionUnit);
            ABA_utils.startUnitTimer(0);
            $.unsubscribe("/unitEvents/unitReady");
        });	
            //debug.log("BreadCrumb listening to UNIT READY");
    },

     /**
     * notifyUnitReady:
     */
    notifyUnitReady: function(){
        $.publish("/unitEvents/unitReady"); 
        //debug.log("UNIT READY -----> FIRE!!!!");
    },		

    /**
    /* deleteInputKeyboardEvent
     */
    deleteInputKeyboardEvent: function(event){
        var $target = $(event.target);
        $target.unbind('keypress');
    },

     /**
     * validateInputText
     * validation of input for WRITING AND DICTATION section
     * @param inputId , okText , filename
     * @returns {Boolean}
     **/
    validateInputText: function(inputId , okText , filename){
        alert('buenas...');
        var $inputTarget = studentActions.$gadget.find("#"+inputId);
        var textTrimmed = ABA_utils.trim( $inputTarget.val() );
        var sSection =  ABA_env.getSection();

        studentActions.removeRolloverClasses($inputTarget);

        if(  textTrimmed != "" ){
            var resultTextOK = ABA_utils.doValidate($inputTarget.val(), okText);
            switch (sSection){
                case "DICTATION":
                    //debug.log("send info validate DICTATION");
                    PROXY_env.HTMLSetDictationItemValue( inputId , filename , resultTextOK.isTextOk, $inputTarget.val() );
                    break;
                case "WRITING":
                    //debug.log("send info validate WRITING");
                    PROXY_env.HTMLSetExerciseItemValue( inputId , filename , resultTextOK.isTextOk, $inputTarget.val() );
                    break;
            }
            if(resultTextOK.isTextOk){
                //studentActions.stopRolePlayProccess();
                /*var $targetSibling = $inputTarget.siblings("input[id*='z']");
                if($targetSibling.attr("id").match("[0-9]z"))
                {
                    $targetSibling.attr("value",$inputTarget.attr("text"));
                    $targetSibling.css("display","inline");
                    $targetSibling.css("background-color","#e5e3dd");
                    $targetSibling.removeClass("workingTooltip");
                }*/
                studentActions.markInputAsOK($inputTarget);
                return true;
            } else {
                studentActions.markInputAsNOOK($inputTarget);
                $inputTarget.focus();
                $inputTarget.setCursorPosition(resultTextOK.nErrorPosition);
                return false;
            }
        }
        else{
            studentActions.notMarkInput($inputTarget);
            return false;
        }

    },

     /**
     * removeRolloverClasses effect 
     * for WRITING AND DICTATION section
     * @param inputId , okText , filename
     */
    removeRolloverClasses: function($target){
        $target.removeClass("colorBackgroundHover");
        $target.removeClass("colorBackgroundHoverInputNOOk");
        $target.removeClass("colorBackgroundHoverInputOk");
    },

     /**
     * notMarkInput: 
     * for WRITING AND DICTATION section
     * @param $inputTarget
     */
    notMarkInput: function($inputTarget){
        var $store = $inputTarget;
        $inputTarget.removeClass("inputOk");
        $inputTarget.removeClass("inputNook");
        $store.data("inputOk",null);
    },

     /**
     * markInputAsOK: 
     * for WRITING AND DICTATION section
     * @param $inputTarget
     */
    markInputAsOK: function($inputTarget){
        var $store = $inputTarget;
        var memo = $store.data();
        var linkid = $inputTarget.attr("id");
        //var $targetSibling = $inputTarget.siblings("input");

        $inputTarget.removeClass("inputNook");
        $inputTarget.addClass("inputOk");
        /*if(ABA_env.getSection()=="WRITING"){
            $inputTarget.attr("readonly","readonly");
            $inputTarget.removeClass("workingTooltip");
            $inputTarget.addClass("listenerTooltip");
            $inputTarget.attr("value",$inputTarget.attr("text"));
            var $targetSibling = $inputTarget.siblings("input[id*='z']");
            $targetSibling.attr("value",$inputTarget.attr("text"));
            $targetSibling.css("display","inline");
            $targetSibling.css("background-color","#e5e3dd");
            $targetSibling.removeClass("workingTooltip");
            //alert($inputTarget.siblings("input").attr("id"));
            //if($inputTarget.attr("class").contains("workingTooltip"))
            if(!$inputTarget.attr("id").match("[0-9]z"))
                $inputTarget.css("display","none");
        }*/
        $store.data("inputOk",true);

        $tooltipHolder = memo.gadget.find(".tip_top[linkid='"+linkid+"']");
        $tooltipHolder.find("#compare").parent().addClass("buttonTooltip_audio_hold");
    },

     /**
     * markInputAsNOOK: 
     * for WRITING AND DICTATION section
     * @param $inputTarget
     */
    markInputAsNOOK: function($inputTarget){
        var $store = $inputTarget;
        var memo = $store.data();
        var linkid = $inputTarget.attr("id");

        $inputTarget.removeClass("inputOk");
        $inputTarget.addClass("inputNook");
        $store.data("inputOk",false);

        $tooltipHolder = memo.gadget.find(".tip_top[linkid='"+linkid+"']");
        $tooltipHolder.find("#compare").parent().removeClass("buttonTooltip_audio_hold");
        if ( memo.tooltipCLicked != null ){
            $tooltipHolder.find("#"+memo.tooltipCLicked).click();
        }
        $store.data("compareMade",false);
    },

    /**: 
     * getter: 
     */
    getUsername: function(){
        return PROXY_env.getUsernameProxy();
    },

    /**
     * getter: 
     */
    getMicroPermission: function(){
        return parent.masterActions.getMicroPermission();
    },

    /**
     * cueStopHearing: 
     * stop signal from flash object : serarch the tooltip actions and click.
     */
    cueStopHearing: function(){
        // current link active and rebuild memo
        //debug.log("trying to cue...");
        var $tooltipHolders = studentActions.$gadget.find("."+studentActions.config.generalTooltipClass);

        $tooltipHolders.each(function(){

            var linkId = $(this).attr("linkid");

            var $store = $("#"+linkId);
            var memo = $("#"+linkId).data();

            var sTooltipClicked = memo.tooltipCLicked;
            var sCompareState = memo.compareState;
            $store.data("cueFromServer",true);

            //debug.log(memo.tooltipCLicked);

            // rebuild target and event for call function event
            if ( (memo.tooltipCLicked!=null) && (memo.tooltipCLicked!=undefined) ) {
                $target = $(this).find("#"+memo.tooltipCLicked);

                $target.click();
                if(sTooltipClicked=="compare" && sCompareState!="step2"){
                        iconTimeoutCompare = setTimeout(function(){$target.click();}, 100);
                }
            }

        });
    },

    /**
     * start play actions: 
     * @param filename,audioType,listenType
     */
    startPlay: function(filename,audioType,listenType){
        PROXY_env.HTMLListenElement(filename,audioType,listenType);
    },

    /**
     * stop play actions: 
     * @param inNotify
     */
    stopElement: function(inNotify){
        //debug.log("listen call stop");
        PROXY_env.HTMLStopElement(inNotify);
    },

    /**
     * start Record actions: 
     * @param filename
     */
    startRecord: function(filename){
        if(studentActions.getMicroPermission){
            PROXY_env.HTMLRecordElement(filename);						
        } else {
            // TODO: TRIGGER ERROR window
        }
    },

    /**
     * loadNewWordsInfo: 
     * @param data
     */
    loadNewWordsInfo: function(data){
        //debug.log("loadNewWordsInfo");
        //debug.log(data);
        studentActions.setUpGadgetIdentifier(studentActions.config.identifier);	
        studentActions.setNumberQuestions(data.Values[0]);
        studentActions.refreshNumberQuestions(data.Values[0]);
    },

    /**
     * called from env / entry of JSON data  
     * @param data
     */
    loadMemoAudioListened: function(data){
        //debug.log("loadAudioListened");
        //debug.log(data);
        studentActions.setUpGadgetIdentifier(studentActions.config.identifier);
        var sSection =  ABA_env.getSection();
        $.each(data.Values, function(){
            var $store = studentActions.$gadget.find("[filename='"+this+"']");
            var memo = studentActions.$gadget.find("[filename='"+this+"']").data();
            if(memo!=undefined){
                var linkId = studentActions.$gadget.find("[filename='"+this+"']").attr("id");
                //debug.log("audiolistened target.memo!."+this);

                switch(sSection){
                    case "WRITING":
                    case "GRAMMAR":
                        $store.data("hearMade",true);
                        //studentActions.$gadget.find("#"+linkId+":not(input)").css("text-decoration","underline");
                        studentActions.$gadget.find("#"+linkId+":not(input)").css("background-color","#e5e3dd");
                        break;
                    case "NEWWORDS":
                        $store.data("hearMade",true);
                        if(memo.recordMade){
                            $store.data("compareMade",true);
                            //studentActions.$gadget.find("#"+linkId+":not(input)").css("text-decoration","underline");
                            studentActions.$gadget.find("#"+linkId+":not(input)").css("background-color","#e5e3dd");
                        }
                        break;
                    case "ROLEPLAY":
                        if(memo.recordMade){
                            //studentActions.$gadget.find("#"+linkId+".recordRole:not(input)").css("text-decoration","underline");
                            studentActions.$gadget.find("#"+linkId+".recordRole:not(input)").css("background-color","#e5e3dd");
                        }
                        break;
                    default:
                        $store.data("hearMade",true);
                        if(memo.recordMade){
                            $store.data("compareMade",true);
                            //studentActions.$gadget.find("#"+linkId+":not(input)").css("text-decoration","underline");
                            studentActions.$gadget.find("#"+linkId+":not(input)").css("background-color","#e5e3dd");
                        }
                        break;
                }
            }
        });
    },

    /**
     * called from env / entry of JSON data  
     * @param data
     */
    loadMemoAudioRecorded: function(data){
        //debug.log("loadAudioRecoreded");
        //debug.log(data);
        studentActions.setUpGadgetIdentifier(studentActions.config.identifier);
        var sSection =  ABA_env.getSection();

        $.each(data.Values, function(){
            var $store = studentActions.$gadget.find("[filename='"+this+"']");
            var memo = studentActions.$gadget.find("[filename='"+this+"']").data();
            if(memo!=undefined){
                var linkId = studentActions.$gadget.find("[filename='"+this+"']").attr("id");
                //debug.log("audiorecorded target.memo!."+this);

                switch(sSection){
                    case "ROLEPLAY":
                        $store.data("recordMade",true);
                        studentActions.enableBlockTextButton( studentActions.$gadget.find("#playButton") );
                        studentActions.changeToPlayButton( studentActions.$gadget.find("#playButton") )
                        if(memo.hearMade){
                                //studentActions.$gadget.find("#"+linkId+".recordRole:not(input)").css("text-decoration","underline");
                                studentActions.$gadget.find("#"+linkId+".recordRole:not(input)").css("background-color","#e5e3dd");
                        }
                    break;
                    default:
                        $store.data("recordMade",true);
                        if(memo.hearMade){
                                $store.data("compareMade",true);
                                //studentActions.$gadget.find("#"+linkId+":not(input)").css("text-decoration","underline");
                                studentActions.$gadget.find("#"+linkId+":not(input)").css("background-color","#e5e3dd");
                        }
                    break;
                }
            }
        });	

        switch(sSection){
            case "NEWWORDS":
                studentActions.setNumberQuestions(nQuest);
                studentActions.refreshNumberQuestions(nQuest);
            break;
        }

    },

    /**
     * called from env / entry of JSON data  
     * @param data
     */
    loadMemoInputText: function(data){
        //debug.log("loadMemoDictation");
        //debug.log(data);

        studentActions.setUpGadgetIdentifier(studentActions.config.identifier);
        $.each(data.Values, function(){
            var sFile = this.file;
            var $store = studentActions.$gadget.find("[filename='"+sFile+"']");
            var memo = studentActions.$gadget.find("[filename='"+sFile+"']").data();
            if(memo!=undefined){
                var $inputTarget = studentActions.$gadget.find("[filename='"+sFile+"']");
                studentActions.markInputAsOK($inputTarget);
                $inputTarget.val($inputTarget.attr("text").replace(/\\'/g, "'"));
                $store.data("compareMade",true);
                $store.data("hearMade",true);
            }
        });
    },

    /**
     * called from env / entry of JSON data  
     * @param data
     */
    loadMemoInputTextFalse: function(data){
        //debug.log("loadMemoDictationFalse");
        //debug.log(data);
        studentActions.setUpGadgetIdentifier(studentActions.config.identifier);
        $.each(data.Values, function(){
            var sFile = this.file;
            var sText = this.text;
            var memo = studentActions.$gadget.find("[filename='"+sFile+"']").data();
            if(memo!=undefined){
                var $inputTarget = studentActions.$gadget.find("[filename='"+sFile+"']");
                if(sText!=""){
                    studentActions.markInputAsNOOK($inputTarget);
                    $inputTarget.val(sText.replace(/\\'/g, "'") );
                }
            }
        });
    },

    /**
     * called from env / entry of JSON data  
     * @param data
     */
    loadMemoWritingInputText: function(data){
        //debug.log("loadMemoWriting");
        //debug.log(data);
        studentActions.setUpGadgetIdentifier(studentActions.config.identifier);
        $.each(data.Values, function(){
            var sFile = this.file;
            var nId = this.id;
            var $store = studentActions.$gadget.find("[filename='"+sFile+"']").filter("#"+nId);
            var memo = studentActions.$gadget.find("[filename='"+sFile+"']").filter("#"+nId).data();
            if(memo!=undefined){
                var $inputTarget = studentActions.$gadget.find("[filename='"+sFile+"']").filter("#"+nId);
                studentActions.markInputAsOK($inputTarget);
                $inputTarget.val($inputTarget.attr("text").replace(/\\'/g, "'"));
                $store.data("compareMade",true);
                //$inputTarget.closest("."+memo.tooltipWritingAudioType).css("text-decoration","underline");
                $inputTarget.closest("."+memo.tooltipWritingAudioType).css("background-color","#e5e3dd");
            }
        });
    },

    /**
     * called from env / entry of JSON data  
     * @param data
     */
    loadMemoWritingInputTextFalse: function(data){
        //debug.log("loadMemoWritingFalse");
        //debug.log(data);
        studentActions.setUpGadgetIdentifier(studentActions.config.identifier);
        $.each(data.Values, function(){
            var sFile = this.file;
            var sText = this.text;
            var nId = this.id;
            var memo = studentActions.$gadget.find("[filename='"+sFile+"']").filter("#"+nId).data();
            if(memo!=undefined){
                var $inputTarget = studentActions.$gadget.find("[filename='"+sFile+"']").filter("#"+nId);
                if(sText!=""){
                    studentActions.markInputAsNOOK($inputTarget);
                    $inputTarget.val(sText.replace(/\\'/g, "'"));
                }
            }
        });
    },

    /**
     * loadMemoDataMinitest:
     * called from env / entry of JSON data  
     * @param data
     */
    loadMemoDataMinitest: function(data){
        //debug.log("loadMemoDataMinitests");
        //debug.log(data);
        studentActions.setUpGadgetIdentifier(studentActions.config.identifier);
        $collectionQuest = studentActions.$gadget.find("."+studentActions.config.rightColumnClass);

        $.each(data.Values.userResp, function(index){
            var nQuest = $collectionQuest.eq(index);
            var $target= nQuest.find("input[value='"+this+"']");
            var memo = $target.data();
            var $store = $target;
            if(memo!=undefined){
                $store.data("isClicked",true);
                $target.attr("checked","checked");
            }
        });

        studentActions.$gadget.find("form#questions").attr("score",data.Values[0].score);
        minitestActions.manageMinitestQualifications(studentActions.$gadget ,  data);
    },

    /**
     * called from env / entry of JSON data  
     */
    loadPaginateWidget: function(){
        studentActions.setUpGadgetIdentifier(studentActions.config.identifier);
        var $gadget = studentActions.$gadget;
        var sPreviousOne = ABA_env.getPreviousPage();
        var sNextOne = ABA_env.getNextPage();
        var sStep = String(ABA_env.getInxPageSection());
        var sTotalSteps = String(ABA_env.getTotalPagesSection());

        var $widget = $('<div id="arrows"></div>');
        if( (sPreviousOne!=undefined) && (sPreviousOne!=null) && (sPreviousOne!="") ){
            $widget.append('<img alt="left arrow" src="images/left_arrow.png" id="previous" page="'+sPreviousOne+'" class="left_arrow left"/>');
        }
        if( (sNextOne!=undefined) && (sNextOne!=null)  && (sNextOne!="") ){
            $widget.append('<img alt="right arrow" src="images/right_arrow.png" id="next" page="'+sNextOne+'" class="right"/>');
        }
        $widget.append('<p class="pag"><span class="f5">' + studentActions.config.steepText +'</span> <span  class="f6">'+sStep+'</span><span class="f5">/'+sTotalSteps+'</span></p>');
        $gadget.find("#notebook_footer").append($widget);
        //studentActions.loadCufon();
    },

    /**
     * managePaginateWidgetButtons
     * @param event
     */
    managePaginateWidgetButtons: function(event){
        var $target = $(event.target);
        var sIdPaginate = $target.attr("id");
        var sPage = $target.attr("page"); 

        //debug.log("NEXT PAGE: >>>"+sPage);
        switch(sIdPaginate){
            case "previous":
            case "next":
                masterActions.loadUnit(sPage);
                break;
        }
    },

    /**
     * managePaginateWidgetButtons for minitest SECTION
     * @param event
     */
    managePaginateWidgetButtonsMinitestCase: function(event){
        var $target = $(event.target);
        var sIdPaginate = $target.attr("id");
        var sPage = $target.attr("page"); 
        //debug.log("NEXT PAGE: >>>"+sPage);
        switch(sIdPaginate){
            case "previous":
            case "next":
                minitestActions.loadMinitestPage(sPage);
                break;
        }
    },

    /**
     * manageCheckboxInput for minitest SECTION
     * @param event
     */
    manageCheckboxInput: function(event){
        var $target = $(event.target);
        var $store = $target;
        var memo = $store.data();

        var isClickedThis = memo.isClicked;
        studentActions.$gadget.find("form#questions").attr("verified","nook");

        $target.closest("."+studentActions.config.rightColumnClass).find("input[type='checkbox']").each(function(){
            var $store = $(this);
            var memo = $(this).data();
            if (memo.isClicked){
                $(this).removeAttr("checked");
                $store.data("isClicked",false);
            }
        });

        if (!isClickedThis){
            $target.attr("checked","checked");
            $store.data("isClicked",true);
        } else {
            $target.removeAttr("checked");
            $store.data("isClicked",false);
        }

    }
};