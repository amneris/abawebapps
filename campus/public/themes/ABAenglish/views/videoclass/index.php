<?php
// Coming from VideoclassController:
//        array("aUnits" => $aUnits, "firstUnit" => $firstUnit, "lastUnit" => $lastUnit
/* @var array $aUnits */
/*  @var boolean $aUnits['userAccess'][] */
/* @var integer $aUnits["lockInfoStatus"][]  possible values are : -1 locked, 0 unlocked, >0 days left to be active */

// Texts already created related with locking stuff. It may be useful at the end.
// Yii::t('mainApp', '% completado')
// Yii::t('mainApp', 'watched_k')
// Yii::t('mainApp', 'nowatched_k')
// Yii::t('mainApp', 'active_in_k', array("{nDays}"=>n Days) )
// Yii::t('mainApp', 'locked_w')

/* @var integer $firstUnit */
/* @var integer $lastUnit */
/* @var integer $lastAccessVideoId */
/* @var string Yii::app()->user->getState('langCourse') */
/* @var boolean $showPopupGoToPremium : False means that no action has to be taken when click on grey thumbnails, "sepia"*/
?>
<?php
    $this->widget('application.components.widgets.BreadcrumbHeader', array('curMenuSectionTab' => ucfirst(strtolower("VIDEOCLASE"))));
?>
<div class="contadorUnidadesContainer">
    <span class="mostrando"><?php echo Yii::t('mainApp', 'Mostrando') ?> <?php echo $firstUnit ?> - <?php echo $lastUnit ?><?php echo Yii::t('mainApp', ' de ') ?></span>
    <span class="mostrandoBold">144 <?php echo Yii::t('mainApp', 'Unidades') ?></span>
</div>
<div class="clear"></div>
<div class="divRightGeneral left">
    <?php 
        $k = 0;
        for ($i=$firstUnit;$i<$lastUnit+1;$i++) {
            if ($k%4==0) {

        ?>
                <div class="clear"></div>
                <div class="contenedorUnidadIndividual left">
                    <div class="borderexternofotoUnidad">
                        <div class="borderinternofotoUnidad">
                            <a href="<?php echo Yii::app()->createUrl('/videoclass/AllUnits',array('unit' => $aUnits['unit'][$i])) ?>">
                                <div class="VideoClassLink">
                                    <img width="156px" height="84px" src="<?php echo HeMixed::getUrlPathImg("videoclass/".(int)$aUnits['unit'][$i].".jpg"); ?>" alt="fotoVideoclass <?php echo $aUnits['unit'][$i] ?>" />
                                    <div></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="contenedorTextoInferior">
                        <span class="numeroUnidad"><?php echo Yii::t('mainApp', 'VIDEOCLASE') ?>&nbsp;<?php echo (int)$aUnits['unit'][$i] ?></span>
                        <a class="nombreUnidad" href="<?php echo Yii::app()->createUrl('/videoclass/AllUnits',array('unit' => $aUnits['unit'][$i])) ?>">>&nbsp;<?php echo $aUnits['title'][$i] ?></a>
                        <?php if(Yii::app()->user->getState('langCourse')=='ru'){
                            echo "</br>";
                        }
                        ?>
                        <img src="<?php echo HeMixed::getUrlPathImg("countries/".strtolower($aTeachers[$aUnits['teacherName'][$i]]).".png"); ?>" />
                        <span class="nombreTeacher"><?php echo $aUnits['teacherName'][$i] ?></span>
                        <div class="contenedorPorcentageUnidad">
                            <?php
                                if($aUnits["VideoClassProgress"][$i] >= 100){
                                    echo "<span class=\"marginLeft5 OKIconCourseCampus\"></span>";
                                    echo "<span class=\"marginLeft5 textProximamenteVerde\">".Yii::t('mainApp', 'watched_k')."</span>";
                                }
                                else{
                                    echo "<span class=\"marginLeft5 textProximamenteRojo\">".Yii::t('mainApp', 'nowatched_k')."</span>";
                                }
                            ?>
                        </div>
                    </div>
                </div>
    <?php
         }
        else{
        ?>
                <div class="contenedorUnidadIndividual left marginLeft4">
                    <div class="borderexternofotoUnidad">
                        <div class="borderinternofotoUnidad">
                            <a href="<?php echo Yii::app()->createUrl('/videoclass/AllUnits',array('unit' => $aUnits['unit'][$i])) ?>">
                                <div class="VideoClassLink">
                                    <img width="156px" height="84px" src="<?php echo HeMixed::getUrlPathImg("videoclass/".(int)$aUnits['unit'][$i].".jpg"); ?>" alt="fotoVideoclass <?php echo $aUnits['unit'][$i] ?>" />
                                    <div></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="contenedorTextoInferior">
                        <span class="numeroUnidad"><?php echo Yii::t('mainApp', 'VIDEOCLASE') ?>&nbsp;<?php echo (int)$aUnits['unit'][$i] ?></span>
                        <a class="nombreUnidad" href="<?php echo Yii::app()->createUrl('/videoclass/AllUnits',array('unit' => $aUnits['unit'][$i])) ?>">>&nbsp;<?php echo $aUnits['title'][$i] ?></a>
                        <?php if(Yii::app()->user->getState('langCourse')=='ru'){
                            echo "</br>";
                        }
                        ?>
                        <img src="<?php echo HeMixed::getUrlPathImg("countries/".strtolower($aTeachers[$aUnits['teacherName'][$i]]).".png"); ?>" />
                        <span class="nombreTeacher"><?php echo $aUnits['teacherName'][$i]?></span>
                        <div class="contenedorPorcentageUnidad">
                            <?php
                                if($aUnits["VideoClassProgress"][$i] >= 100){
                                    echo "<span class=\"marginLeft5 OKIconCourseCampus\"></span>";
                                    echo "<span class=\"marginLeft5 textProximamenteVerde\">".Yii::t('mainApp', 'watched_k')."</span>";
                                }
                                else{
                                    echo "<span class=\"marginLeft5 textProximamenteRojo\">".Yii::t('mainApp', 'nowatched_k')."</span>";
                                }
                            ?>
                        </div>
                    </div>
                </div>
    <?php
        }
            $k++;
        }
    ?>
</div>

<!-- Should care about $showPopupGoToPremium to decide if Popup has to be displayed or not
Modify fucntion popUpPasateaPremium(), line 156 ?? -->
<input id="urlPopup" type="hidden" value="<?php echo Yii::app()->createUrl('modals/PopupPasteaPremiumVideoclass') ?>" />

<?php
//#ABAWEBAPPS-693
$stParams = array("levelId" => array("value" => "cooladataLevelId", "type" => "js"));
$sCooladata = HeCooladata::createDefaultEvent(HeCooladata::EVENT_NAME_OPENED_VIDEOCLASSES_INDEX, true, $stParams);
Yii::app()->clientScript->registerScript(md5($sCooladata), $sCooladata, CClientScript::POS_READY);
?>

