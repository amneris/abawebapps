<?php
switch ($userTypeAccessUnit) {
    case OP_INCOMPLETEUNIT:
        $coursePath = 'cursofree';
        $unitsPath = 'campusfree4';
        break;

    case OP_FULLUNIT:
        $coursePath = 'curso';
        $unitsPath = 'campus4';
        break;

    default:
        Yii::app()->request->redirect(Yii::app()->createUrl('videoclass/index'));
        break;
}

$userLanguage = strtolower(Yii::app()->getLanguage());

$urlAbsolute = Yii::app()->getBaseUrl(true).Yii::app()->theme->baseUrl.'/media/course/'.$coursePath.'/';
$urlBase = Yii::app()->getBaseUrl(true).Yii::app()->theme->baseUrl.'/media/course/units/'.$unitsPath.'/'.$userLanguage.'/'.$unit.'/';

$scriptBase = "<script type=\"text/javascript\">
                    var base = document.createElement('base');
                    base.href = '$urlBase';
                    document.getElementsByTagName('head')[0].appendChild(base);
                </script>";

echo $scriptBase;

$path = Yii::app()->theme->basePath.'/media/course/units/'.$unitsPath.'/'.$userLanguage.'/'.$unit.'/master'.$unit.'.html';
$buffer = file_get_contents($path);
$theUserId = Yii::app()->user->getId();
$theMail = Yii::app()->user->getEmail();
$thePassword = Yii::app()->user->getPassword();
$theReleaseCampus = Yii::app()->user->getReleaseCampus();
$theName = Yii::app()->user->getName();
$userType = Yii::app()->user->refreshAndGetUserType();
$isMobile = HeDetectDevice::isMobile();
$theSection = 'GRAMMAR';
$jwPlayer = $isMobile ? 'js/jwplayer6/jwplayer.js' : 'js/jwplayer.js';

$theKeys = array( '#isMobile#', '#userId#', '#username#', '#password#', '#section#',
                  '#name#', '#urlAbs#', '#releaseCampus#', '#userType#', '#jwPlayer#', '#isB2B#' );

$theValues = array( $isMobile, $theUserId, $theMail, $thePassword, $theSection,
                    $theName, $urlAbsolute, $theReleaseCampus, $userType, $jwPlayer, $isB2B );

// print output course
$theBuffer = str_replace($theKeys, $theValues, $buffer);
echo $theBuffer;

// in case of teacher help direct link
if (!empty($teacherImg)) { ?>
    <script>
        $(window).resize(function() {
            repositionCourseTeacherHelpLink();
        });

        $(window).scroll(function() {
            repositionCourseTeacherHelpLink();
        });

        function repositionCourseTeacherHelpLink() {

            var $container = $("#courseTeacherHelpLink");
            var bodyTopPosition = 825;

            $container.addClass("fixed");

            if ($container.offset().top > bodyTopPosition) {
                $container.removeClass("fixed");
            }
        }
    </script>

    <div id="courseTeacherHelpLink" class="fixed">
        <div class="courseTeacherHelpPanel" style="display: none;">
            <h3><?= Yii::t('mainApp', 'courseTeacherHelpTitle'); ?></h3>
            <a href="<?= Yii::app()->createUrl('contact/index') ?>" class="courseTeacherBTN"><?= Yii::t('mainApp', 'courseTeacherHelpLink1'); ?></a>
            <a href="<?= Yii::app()->createUrl('message/inbox/inbox') ?>" class="courseTeacherBTN"><?= Yii::t('mainApp', 'courseTeacherHelpLink2'); ?></a>
        </div>
        <img src="<?= Yii::app()->theme->baseUrl ?>/media/images/teachers/<?= $teacherImg ?>">
    </div>
<?php
}

// global variables from php that course needs to access
?>
<script>
    var _glossaryUrl = "<?= Yii::app()->params['glossaryUrl'] ?>";
    var _transactionsUrl = "<?= Yii::app()->params['transactionsUrl'] ?>";
</script>