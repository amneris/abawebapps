<div class="contenedorAyuda left">
    <div class="tituloSeccionCampus"><?php echo Yii::t('mainApp', 'Ayuda') ?></div>
    <?php $this->renderPartial('menu'); ?>
    <div class="bodyAyuda left">
        <p><?php echo Yii::t('mainApp', 'Precios y formas de pago') ?></p>
        <ol>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Qué incluye el curso ABA Premium?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Con el curso ABA Premium tienes acceso a todos los contenidos del curso y a todos los niveles, desde el Beginners hasta el Business, y puedes ver todas las videoclases y los ABA Films del curso. Además, tienes un profesor particular que puedes contactar en cualquier momento desde el Campus a través de mensajes.') ?>
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'También dispones de herramientas de consulta muy útiles como la gramática interactiva y el diccionario del curso. Al completar un nivel, obtienes el certificado oficial de American & British Academy del nivel correspondiente.') ?>
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Qué incluye el curso ABA Free?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Con el curso ABA Free puedes ver progresivamente todas las videoclases y consultar la gramática interactiva de forma gratuita. Los mejores profesores de ABA English te explicarán a través de ABA Films muy entretenidos toda la gramática inglesa y con la gramática interactiva podrás consultarla de forma rápida; también podrás escuchar todas las palabras, grabarlas para mejorar tu pronunciación y ver ejemplos para ampliar tu vocabulario.') ?>
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'Además, con ABA Free también tienes la posibilidad de probar la primera unidad de tu nivel y realizar todas las secciones de esta unidad para que pruebes nuestra metodología única de aprendizaje.') ?>
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Qué debo hacer si quiero cambiar de ABA Free a ABA Premium?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Si eres Free y quieres pasarte a Premium debes pulsar en el banner de pásate a Premium') ?>
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'Si eres Premium y quieres pasarte a Free debes enviar un mail a ') ?><a  target="_blank" href="https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998"><?php echo Yii::t('mainApp', 'Ayuda'); ?></a>.
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Qué formas de pago existen?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'En ABA English puedes pagar con tarjeta de crédito o Paypal. No es posible pagar por domiciliación bancaria, si tienes alguna dificultad envíanos un email a ') ?><a  target="_blank" href="https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998"><?php echo Yii::t('mainApp', 'Ayuda'); ?></a><?php echo Yii::t('mainApp', ' y te ayudaremos') ?>.
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo puedo modificar los datos de pago?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'En el caso de que necesites cambiar tu forma o datos de pagos, o realizar cualquier otra consulta relacionada con el pago del curso puedes enviarnos un mail a ') ?><a  target="_blank" href="https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998"><?php echo Yii::t('mainApp', 'Ayuda'); ?></a>.
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo puedo obtener el recibo del curso?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Si quieres obtener el recibo del curso envía un mail a ') ?><a  target="_blank" href="https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998"><?php echo Yii::t('mainApp', 'Ayuda'); ?></a><?php echo Yii::t('mainApp', ' y te lo remitiremos.') ?>
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cuando se renueva la suscripción al curso?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'En el caso en que hayas facilitado tus datos de pago a través de nuestra página web, se renueva automáticamente cada vez que se alcance el fin de tu periodo de suscripción que puede ser mensual, semestral o anual') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'Si no quieres que la suscripción se renueve deberás escribir un mail a ') ?><a  target="_blank" href="https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998"><?php echo Yii::t('mainApp', 'Ayuda'); ?></a><?php echo Yii::t('mainApp', ' unos días antes del cobro, comunicándonos tu decisión, podrás seguir estudiando hasta el fin de la suscripción y no volveremos a realizar más cargos en tu cuenta.') ?>
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo puedo reactivar mi suscripción Premium?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Si has sido alumno Premium tus datos y progreso quedarán siempre guardados en el campus con tu usuario. Para volver a disfrutar del curso completo deberás pulsar en el banner de "pásate a premium" escoger el tipo de suscripción y completar de nuevo tus datos de pago. También puedes realizar este cambio accediendo al campus en el apartado "mi cuenta"')?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'Si tienes cualquier dificultad puedes escribir un mail a ') ?><a  target="_blank" href="https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998"><?php echo Yii::t('mainApp', 'Ayuda'); ?></a>
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo puedo cambiar la periodicidad de la suscripción?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Para cambiar la periodicidad de la suscripción y pasarla a uno, seis o doce meses, debes enviarnos un mail a ') ?><a  target="_blank" href="https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998"><?php echo Yii::t('mainApp', 'Ayuda'); ?></a><?php echo Yii::t('mainApp', ' y te responderemos lo antes posible') ?>.
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo puedo utilizar un descuento?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'En el caso de que hayas recibido un descuento en una newsletter indícanos la fecha y tipo de descuento que recibiste y te indicaremos los pasos a seguir para beneficiarte del mismo. Si tienes dudas puedes escribir a ') ?><a  target="_blank" href="https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998"><?php echo Yii::t('mainApp', 'Ayuda'); ?></a>
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo puedo darme de baja?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Puedes darte de baja en cualquier momento, para solicitarlo debes enviarnos un mail a ') ?><a  target="_blank" href="https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998"><?php echo Yii::t('mainApp', 'Ayuda'); ?></a><?php echo Yii::t('mainApp', ' unos días antes de que se renueve tu suscripción. Para garantizar un servicio ininterrumpido, las suscripciones se renuevan automáticamente utilizando la tarjeta de crédito registrada') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'A partir del momento en que nos comuniques la baja se cancelarán las renovaciones y podrás seguir utilizando el curso hasta la fecha de expiración de la cuota') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'Si la baja se solicita durante los primeros 30 días del curso tienes derecho a la devolución del importe que hayas pagado') ?>.
                    </p>
                </div>
            </li>
        </ol>
    </div>
</div>
<div class="clear"></div>