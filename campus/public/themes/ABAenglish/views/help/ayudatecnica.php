<div class="contenedorAyuda left">
<div class="tituloSeccionCampus"><?php echo Yii::t('mainApp', 'Preguntas_frecuentes') ?></div>
<div class="bodyAyuda left">
<div class="help_dropdwon closed">
    <span class="arrow_closed"></span>
    <p><?php echo Yii::t('mainApp', 'Top FAQS') ?></p>
    <ol>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Qué incluye el curso ABA Premium?') ?></span>
            <div>
                <p><?php echo Yii::t('mainApp', 'Con el curso ABA Premium tienes acceso a todos los contenidos del curso y a todos los niveles, desde el Beginners hasta el Business, y puedes ver todas las videoclases y los ABA Films del curso. Además, tienes un profesor particular que puedes contactar en cualquier momento desde el Campus a través de mensajes.') ?></p>
                <p><?php echo Yii::t('mainApp', 'También dispones de herramientas de consulta muy útiles como la gramática interactiva y el diccionario del curso. Al completar un nivel, obtienes el certificado oficial de American & British Academy del nivel correspondiente.') ?></p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Qué incluye el curso ABA Free?') ?></span>
            <div>
                <p><?php echo Yii::t('mainApp', 'Con el curso ABA Free puedes ver progresivamente todas las videoclases y consultar la gramática interactiva de forma gratuita. Los mejores profesores de ABA English te explicarán a través de vídeo muy entretenidos toda la gramática inglesa y con la gramática interactiva podrás consultarla de forma rápida; también podrás escuchar todas las palabras, grabarlas para mejorar tu pronunciación y ver ejemplos para ampliar tu vocabulario.') ?></p>
                <p><?php echo Yii::t('mainApp', 'Además, con ABA Free también tienes la posibilidad de probar la primera unidad de tu nivel y realizar todas las secciones de esta unidad para que pruebes nuestra metodología única de aprendizaje.') ?></p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Qué certificado obtengo con el curso de ABA English?') ?></span>
            <div>
                <p><?php echo Yii::t('mainApp', 'Se trata de un certificado emitido por American & British Academy que avala los conocimientos adquiridos con nuestro curso de inglés.') ?></p>
                <p><?php echo Yii::t('mainApp', 'El certificado indica el nivel superado y las competencias lingüísticas adquiridas en relación al Marco Común Europeo de Referencia para las Lenguas (MCER); por otra parte, también indica el nº de horas necesarias para terminar el curso y la fecha de obtención del mismo') ?>.</p>
                <p><?php echo Yii::t('mainApp', 'El certificado solo está disponible para alumnos ABA Premium. Para obtener el certificado de tu nivel debes completar el 100% de las primeras siete secciones de todas las unidades del nivel correspondiente y superar el test final que corresponde a cada unidad') ?>.</p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Interpreta?') ?></span>
            <div>
                <p><?php echo Yii::t('mainApp', 'Para completar la sección debes interpretar todos los personajes.') ?></p>
                <ol>
                    <li><?php echo Yii::t('mainApp', 'Elige un personaje') ?></li>
                    <li><?php echo Yii::t('mainApp', 'Haz clic en comenzar cuando estés preparado') ?></li>
                    <li><?php echo Yii::t('mainApp', 'Graba asumiendo el papel del personaje apretando en "botón de grabar" y otra vez cuando hayas terminado la frase.') ?></li>
                </ol>
                <p><?php echo Yii::t('mainApp', 'Al finalizar el diálogo, podrás escuchar toda la conversación apretando en "botón de escuchar"') ?></p>
                <p><?php echo Yii::t('mainApp', 'Una vez interpretado un personaje, debes interpretar también el otro personaje volviendo al inicio de la sección.') ?></p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', 'No puedo grabar o no oigo el audio del curso') ?></span>
            <div>
                <p><?php echo Yii::t('mainApp', 'En ambos casos puede tratarse de una incidencia en la configuración del micrófono y auriculares que utilizas para realizar el curso, sigue estos pasos: ') ?></p>
                <p><?php echo Yii::t('mainApp', 'A. Comprueba que tus auriculares estén bien conectados a tu ordenador') ?>.</p>
                <p><?php echo Yii::t('mainApp', 'Algunos auriculares tienen un botón de silencio; comprueba que no estén puestos en dicha posición. Existen auriculares tipo clavija o tipo usb, en el caso de los que son tipo clavija, normalmente son de dos colores, el color rosa debe ir conectado a la clavija del micro y el color verde debe ir conectado a la clavija de los auriculares. En el caso de los auriculares tipo USB deben estar conectados a una entrada USB.') ?></p>
                <p><?php echo Yii::t('mainApp', 'B. Accede al curso') ?></p>
                <ul>
                    <li><?php echo Yii::t('mainApp', 'Cuando aparezca la ventana de configuración de Adobe Flash Player que te solicita el acceso a tu cámara y micro, NO pulses ninguno de los dos botones') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'Sitúate encima de la ventana de configuración y haz clic con el botón derecho') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'Aparecerá un menú en el que debes seleccionar la opción Configuración') ?></li>
                    <li><?php echo Yii::t('mainApp', 'Esta ventana de configuración te aparecerá con la opción "denegar" marcada, ya que todavía no hemos aceptado el acceso a micro y cámara. Pulsa encima del icono del micrófono que ves en esta ventana') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'Verás que se activa una barra vertical de señal de micrófono, vamos a probarlo') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'Habla con el micrófono y observa como el indicador (la barra) reacciona a la señal de la voz') ?>.</li>
                </ul>
                <p><?php echo Yii::t('mainApp', 'Si la señal no llega: ') ?></p>
                <p><?php echo Yii::t('mainApp', '- Puedes nivelar el volumen de registro, moviendo el cursor que ves en la misma ventana') ?>.</p>
                <p><?php echo Yii::t('mainApp', '- Verifica que el micrófono esté conectado correctamente') ?>.</p>
                <ul>
                    <li><?php echo Yii::t('mainApp', 'Si verificados estos parámetros, continuamos sin señal, significa que el micrófono no está correctamente configurado en el sistema. A continuación te explicamos cómo configurar el micrófono en tu ordenador:') ?></li>
                </ul>
                <p><?php echo Yii::t('mainApp', 'C. Configura el micrófono y auriculares adecuadamente:') ?></p>
                <ul>
                    <li><?php echo Yii::t('mainApp', 'Accede al panel de control de tu ordenador') ?></li>
                    <li><?php echo Yii::t('mainApp', 'Haz clic en Hardware y Sonido') ?></li>
                    <li><?php echo Yii::t('mainApp', 'En el apartado sonido, accede a "administrar dispositivos de audio"') ?></li>
                    <li><?php echo Yii::t('mainApp', 'Para el micro: Sitúate en la pestaña grabar, asegúrate de que el micrófono que estas utilizando para el curso aparece marcado como dispositivo predeterminado. Si no es así sitúate encima del que corresponda, pulsa con el botón derecho del mouse, y del menú que aparece selecciona "establecer como dispositivo predeterminado"') ?></li>
                    <li><?php echo Yii::t('mainApp', 'Para los auriculares: Sitúate en la pestaña reproducción, asegúrate de que los auriculares que estas utilizando para el curso aparecen marcados como dispositivo predeterminado. Si no es así sitúate encima del que corresponda, pulsa con el botón derecho del mouse, y del menú que aparece selecciona "establecer como dispositivo predeterminado"') ?></li>
                </ul>
                <p><?php echo Yii::t('mainApp', 'D. Acepta el permiso que se solicita en la ventana de Flash') ?></p>
                <p><?php echo Yii::t('mainApp', 'Asegúrate de que le has dado a aceptar a la ventana de Flash que aparece al acceder a la unidad, si no le das a permitir en la ventana de Flash, no podrás realizar las grabaciones del curso.') ?></p>
                <p><?php echo Yii::t('mainApp', 'Comprueba que la versión de flash que estas utilizando es la más adecuada. Si no dispones de Flash o la versión que tienes no es la última puedes descargarte el software en ') ?><a href="http://get.adobe.com/es/flashplayer/">get.adobe.com/es/flashplayer/</a>.</p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', 'La ventana de Flash Media Player no aparece cuando accedo a las unidades') ?>.</span>
            <div>
                <p><?php echo Yii::t('mainApp', 'Si la ventana de Flash no aparece debes asegurarte de que Flash esté instalado en tu ordenador, para ello te recomendamos que realices el test de instalación accediendo al campus. Si el test te indica que no dispone de Flash, puedes descargarlo haciendo clic en:') ?></p>
                <p>
                    <a href="http://get.adobe.com/es/flashplayer/">get.adobe.com/es/flashplayer/</a>
                </p>
                <p><?php echo Yii::t('mainApp', 'Si el test no te indica ningún error, puede que la ventana no aparezca porque no estás visualizando la pantalla en el tamaño adecuado, puedes resolverlo siguiendo estos pasos:') ?></p>
                <ol>
                    <li><?php echo Yii::t('mainApp', 'Ir al menú "ver" de la barra del navegador, seleccionar opción tamaño y escoger opción "inicio"') ?></li>
                    <li><?php echo Yii::t('mainApp', 'Puedes realizar lo mismo pulsando en el teclado simultáneamente las tecla Ctrl+0') ?>.</li>
                </ol>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', 'El curso se ha quedado bloqueado') ?></span>
            <div>
                <p><?php echo Yii::t('mainApp', 'En algunas ocasiones el rendimiento del curso puede verse afectado por el funcionamiento del software antivirus y/o de software de descargas, en estos casos te recomendamos que:') ?></p>
                <p><?php echo Yii::t('mainApp', '- Desinstales momentáneamente el software antivirus y compruebes el funcionamiento del curso') ?>.</p>
                <p><?php echo Yii::t('mainApp', '- Prueba la conexión al curso, sin tener abierto ningún programa de descargas') ?>.</p>
                <p><?php echo Yii::t('mainApp', 'En otras ocasiones es debido al tipo de navegador que utilizas para conectarte a Internet. Nuestro curso funciona con todos los navegadores pero te recomendamos que utilices Mozilla Firefox o Google Chrome, puedes descargarlos en estos links') ?></p>
                <p>
                    <a href="http://www.mozilla-europe.org">www.mozilla-europe.org</a>
                </p>
                <p>
                    <a href="http://www.google.com/chrome">www.google.com/chrome</a>
                </p>
                <p><?php echo Yii::t('mainApp', 'Por otra parte a veces el rendimiento puede verse afectado por la necesidad de activar cookies o por la necesidad de borrar la memoria caché de tu ordenador. A continuación te indicamos como actuar en ambos casos en función del navegador que utilices') ?></p>
                <ul>
                    <li>Mozilla Firefox</li>
                </ul>
                <p><?php echo Yii::t('mainApp', 'Activar cookies:') ?></p>
                <p>
                    <a href="http://support.mozilla.org/es/kb/Borrar%20cookies">support.mozilla.org/es/kb/Borrar%20cookies</a>
                </p>
                </p><?php echo Yii::t('mainApp', 'Borrar caché:') ?></p>
                <p>
                    <a href="http://support.mozilla.org/es/kb/como-limpiar-la-cache?s=cach%C3%A9&r=0&e=es&as=s">support.mozilla.org/es/kb/como-limpiar-la-cache?s=cach%C3%A9&r=0&e=es&as=s</a>
                </p>
                <ul>
                    <li>Chrome</li>
                </ul>
                <p><?php echo Yii::t('mainApp', 'Activar cookies:') ?></p>
                <p>
                    <a href="http://support.google.com/chrome/bin/answer.py?hl=es&answer=95647">support.google.com/chrome/bin/answer.py?hl=es&answer=95647</a>
                </p>
                <p><?php echo Yii::t('mainApp', 'Borrar caché:') ?></p>
                <p>
                    <a href="http://support.google.com/chrome/bin/answer.py?hl=es&answer=95582">support.google.com/chrome/bin/answer.py?hl=es&answer=95582</a>
                </p>
            </div>
        </li>
    </ol>
</div>
<div class="help_dropdwon closed">
    <span class="arrow_closed"></span>
    <p><?php echo Yii::t('mainApp', 'Precios y formas de pago') ?></p>
    <ol>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Qué incluye el curso ABA Premium?') ?></span>
            <div>
                <p>
                    <?php echo Yii::t('mainApp', 'Con el curso ABA Premium tienes acceso a todos los contenidos del curso y a todos los niveles, desde el Beginners hasta el Business, y puedes ver todas las videoclases y los ABA Films del curso. Además, tienes un profesor particular que puedes contactar en cualquier momento desde el Campus a través de mensajes.') ?>
                </p>
                <p>
                    <?php echo Yii::t('mainApp', 'También dispones de herramientas de consulta muy útiles como la gramática interactiva y el diccionario del curso. Al completar un nivel, obtienes el certificado oficial de American & British Academy del nivel correspondiente.') ?>
                </p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Qué incluye el curso ABA Free?') ?></span>
            <div>
                <p>
                    <?php echo Yii::t('mainApp', 'Con el curso ABA Free puedes ver progresivamente todas las videoclases y consultar la gramática interactiva de forma gratuita. Los mejores profesores de ABA English te explicarán a través de ABA Films muy entretenidos toda la gramática inglesa y con la gramática interactiva podrás consultarla de forma rápida; también podrás escuchar todas las palabras, grabarlas para mejorar tu pronunciación y ver ejemplos para ampliar tu vocabulario.') ?>
                </p>
                <p>
                    <?php echo Yii::t('mainApp', 'Además, con ABA Free también tienes la posibilidad de probar la primera unidad de tu nivel y realizar todas las secciones de esta unidad para que pruebes nuestra metodología única de aprendizaje.') ?>
                </p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Qué debo hacer si quiero cambiar de ABA Free a ABA Premium?') ?></span>
            <div>
                <p>
                    <?php echo Yii::t('mainApp', 'Si eres Free y quieres pasarte a Premium debes pulsar en el banner de pásate a Premium') ?>
                </p>
                <p>
                    <?php echo Yii::t('mainApp', 'Si eres Premium y quieres pasarte a Free debes enviar un mail a ') ?><a  target="_blank" href="https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998"><?php echo Yii::t('mainApp', 'Ayuda'); ?></a>.
                </p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Qué formas de pago existen?') ?></span>
            <div>
                <p>
                    <?php echo Yii::t('mainApp', 'En ABA English puedes pagar con tarjeta de crédito o Paypal. No es posible pagar por domiciliación bancaria, si tienes alguna dificultad envíanos un email a ') ?><a  target="_blank" href="https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998"><?php echo Yii::t('mainApp', 'Ayuda'); ?></a><?php echo Yii::t('mainApp', ' y te ayudaremos') ?>.
                </p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Cómo puedo modificar los datos de pago?') ?></span>
            <div>
                <p>
                    <?php echo Yii::t('mainApp', 'En el caso de que necesites cambiar tu forma o datos de pagos, o realizar cualquier otra consulta relacionada con el pago del curso puedes enviarnos un mail a ') ?><a  target="_blank" href="https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998"><?php echo Yii::t('mainApp', 'Ayuda'); ?></a>.
                </p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Cómo puedo obtener el recibo del curso?') ?></span>
            <div>
                <p>
                    <?php echo Yii::t('mainApp', 'Si quieres obtener el recibo del curso envía un mail a ') ?><a  target="_blank" href="https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998"><?php echo Yii::t('mainApp', 'Ayuda'); ?></a><?php echo Yii::t('mainApp', ' y te lo remitiremos.') ?>
                </p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Cuando se renueva la suscripción al curso?') ?></span>
            <div>
                <p>
                    <?php echo Yii::t('mainApp', 'En el caso en que hayas facilitado tus datos de pago a través de nuestra página web, se renueva automáticamente cada vez que se alcance el fin de tu periodo de suscripción que puede ser mensual, semestral o anual') ?>.
                </p>
                <p>
                    <?php echo Yii::t('mainApp', 'Si no quieres que la suscripción se renueve deberás escribir un mail a ') ?><a  target="_blank" href="https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998"><?php echo Yii::t('mainApp', 'Ayuda'); ?></a><?php echo Yii::t('mainApp', ' unos días antes del cobro, comunicándonos tu decisión, podrás seguir estudiando hasta el fin de la suscripción y no volveremos a realizar más cargos en tu cuenta.') ?>
                </p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Cómo puedo reactivar mi suscripción Premium?') ?></span>
            <div>
                <p>
                    <?php echo Yii::t('mainApp', 'Si has sido alumno Premium tus datos y progreso quedarán siempre guardados en el campus con tu usuario. Para volver a disfrutar del curso completo deberás pulsar en el banner de "pásate a premium" escoger el tipo de suscripción y completar de nuevo tus datos de pago. También puedes realizar este cambio accediendo al campus en el apartado "mi cuenta"')?>.
                </p>
                <p>
                    <?php echo Yii::t('mainApp', 'Si tienes cualquier dificultad puedes escribir un mail a ') ?><a  target="_blank" href="https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998"><?php echo Yii::t('mainApp', 'Ayuda'); ?></a>
                </p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Cómo puedo cambiar la periodicidad de la suscripción?') ?></span>
            <div>
                <p>
                    <?php echo Yii::t('mainApp', 'Para cambiar la periodicidad de la suscripción y pasarla a uno, seis o doce meses, debes enviarnos un mail a ') ?><a  target="_blank" href="https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998"><?php echo Yii::t('mainApp', 'Ayuda'); ?></a><?php echo Yii::t('mainApp', ' y te responderemos lo antes posible') ?>.
                </p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Cómo puedo utilizar un descuento?') ?></span>
            <div>
                <p>
                    <?php echo Yii::t('mainApp', 'En el caso de que hayas recibido un descuento en una newsletter indícanos la fecha y tipo de descuento que recibiste y te indicaremos los pasos a seguir para beneficiarte del mismo. Si tienes dudas puedes escribir a ') ?><a  target="_blank" href="https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998"><?php echo Yii::t('mainApp', 'Ayuda'); ?></a>
                </p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Cómo puedo darme de baja?') ?></span>
            <div>
                <p>
                    <?php echo Yii::t('mainApp', 'Puedes darte de baja en cualquier momento, para solicitarlo debes enviarnos un mail a ') ?><a  target="_blank" href="https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998"><?php echo Yii::t('mainApp', 'Ayuda'); ?></a><?php echo Yii::t('mainApp', ' unos días antes de que se renueve tu suscripción. Para garantizar un servicio ininterrumpido, las suscripciones se renuevan automáticamente utilizando la tarjeta de crédito registrada') ?>.
                </p>
                <p>
                    <?php echo Yii::t('mainApp', 'A partir del momento en que nos comuniques la baja se cancelarán las renovaciones y podrás seguir utilizando el curso hasta la fecha de expiración de la cuota') ?>.
                </p>
                <p>
                    <?php echo Yii::t('mainApp', 'Si la baja se solicita durante los primeros 30 días del curso tienes derecho a la devolución del importe que hayas pagado') ?>.
                </p>
            </div>
        </li>
    </ol>
</div>
<div class="help_dropdwon closed">
    <span class="arrow_closed"></span>
    <p><?php echo Yii::t('mainApp', 'Certificados') ?></p>
    <ol>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Qué certificado obtengo con el curso de ABA English?') ?></span>
            <div>
                <p><?php echo Yii::t('mainApp', 'Se trata de un certificado emitido por American & British Academy que avala los conocimientos adquiridos con nuestro curso de inglés') ?>.</p>
                <p><?php echo Yii::t('mainApp', 'El certificado indica el nivel superado y las competencias lingüísticas adquiridas en relación al Marco Común Europeo de Referencia para las Lenguas (MCER); por otra parte, también indica el nº de horas necesarias para terminar el curso y la fecha de obtención del mismo') ?>.</p>
                <p><?php echo Yii::t('mainApp', 'El certificado solo está disponible para alumnos ABA Premium. Para obtener el certificado de tu nivel debes completar el 100% de las primeras siete secciones de todas las unidades del nivel correspondiente y superar el test final que corresponde a cada unidad') ?>.</p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', '¿El curso de ABA English sirve para preparar exámenes oficiales?') ?></span>
            <div>
                <p><?php echo Yii::t('mainApp', 'El curso de ABA English te prepara para alcanzar el nivel adecuado en cualquiera de los exámenes que mencionamos a continuación.') ?>.</p>
                <p>
                    <u><?php echo Yii::t('mainApp', 'Certificados de estudios de Inglés Británico Cambridge ESOL - English for Speakers of Other Languages:') ?></u>
                </p>
                <ul>
                    <li><?php echo Yii::t('mainApp', 'KET, Key English Test: es un certificado de nivel principiante') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'PET, Preliminary English Test: es un certificado de nivel intermedio, implica la habilidad del hablante para comunicarse en distintas situaciones de la vida real. Es necesario un nivel de comprensión avanzado') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'FCE, First Certificate in English: es un certificado de nivel intermedio alto, indica un importante conocimiento de las estructuras gramaticales y un vocabulario muy amplio.') ?></li>
                </ul>
                <p>
                    <?php echo Yii::t('mainApp', 'En ABA English te indicamos el centro asociado de la Universidad de Cambridge más cercano') ?>.
                </p>
                <p>
                    <u>
                        <?php echo Yii::t('mainApp', 'El Certificado de Inglés Americano TOEFL - Test of English as a Foreign Language') ?>
                    </u>
                </p>
                <p>
                    <?php echo Yii::t('mainApp', 'Evalúa el nivel de competencia de inglés. En las convocatorias de examen para estos certificados no hay aprobado o suspenso sino que en todos los casos hay un resultado equivalente al nivel de inglés que tienes. Cada una de las universidades americanas exige un nivel TOEFL determinado. Los exámenes se realizan en los centros acreditados y se utiliza un PC. No hay examen oral.') ?>.
                </p>
                <p>
                    <?php echo Yii::t('mainApp', 'En ABA English te indicamos dónde puedes examinarte para el examen TOEFL') ?>.
                </p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Por qué no puedo descargarme el certificado?') ?></span>
            <div>
                <p>
                    <?php echo Yii::t('mainApp', 'La opción de descarga del certificado esta disponible para los alumnos con plan Premium, o alumnos que hubieran contratado anteriormente un plan Premium y superado el nivel') ?>.
                </p>
                <p>
                    <?php echo Yii::t('mainApp', 'Para poder descargártelo deberás haber completado el 100 % de las primeras siete secciones de cada unidad de tu nivel, y haber superado con éxito la sección evaluación de cada unidad del nivel. Si cumples con estas condiciones y tu certificado no está disponible contacta con nosotros escribiendo un mail a ') ?><a href="mailto:support@abaenglish.com">support@abaenglish.com</a>.
                </p>
            </div>
        </li>
    </ol>
</div>
<div class="help_dropdwon closed">
<span class="arrow_closed"></span>
<p><?php echo Yii::t('mainApp', 'Planes') ?></p>
<ol>
<li>
    <span><?php echo Yii::t('mainApp', '¿Cuánto dura el curso?') ?></span>
    <div>
        <p>
            <?php echo Yii::t('mainApp', 'Depende del tipo de suscripción que escojas, con cualquiera de nuestros planes podrás escoger la suscripción mensual, semestral y anual. Para garantizar un servicio ininterrumpido, la suscripción al curso se renovará automáticamente, utilizando la tarjeta de crédito registrada. Cuando tú quieras podrás solicitar la baja del curso.') ?>
        </p>
    </div>
</li>
<li>
    <span><?php echo Yii::t('mainApp', '¿Cuánto tiempo se tarda en completar un nivel?') ?></span>
    <div>
        <p>
            <?php echo Yii::t('mainApp', 'El curso empieza en el momento en que te des de alta y finaliza en el momento que tú decidas. Durante este tiempo podrás acceder a nuestro campus las 24 horas del día, 7 días a la semana') ?>.
        </p>
        <p>
            <?php echo Yii::t('mainApp', 'Puedes avanzar en el curso a tu ritmo. Tu progreso dependerá del tiempo que le dediques a cada unidad y de la frecuencia con la que te conectes') ?>.
        </p>
        <p>
            <?php echo Yii::t('mainApp', 'Para completar cada nivel debes realizar las 24 unidades que lo componen y cada unidad se completa realizando las ocho secciones') ?>.
        </p>
    </div>
</li>
<li>
    <span><?php echo Yii::t('mainApp', '¿Por qué nivel empiezo?') ?></span>
    <div>
        <p>
            <?php echo Yii::t('mainApp', 'Antes de acceder al Campus de ABA English escoges el nivel por el que quieres empezar; si no sabes cuál es tu nivel puedes realizar el test para situarte. Para un correcto y progresivo aprendizaje del idioma te aconsejamos que empieces por el nivel Beginners') ?>.
        </p>
        <p>
            <?php echo Yii::t('mainApp', 'Puedes consultar la lista de los niveles de ABA English en relación al Marco Común Europeo de Referencia para las Lenguas (MCER) aquí:') ?>
        </p>
        <p>
            <a href="">añadir link a página web de los niveles de ABA.</a>
        </p>
        <p>
            <?php echo Yii::t('mainApp', 'A lo largo del curso podrás acceder a todos los niveles y cambiar de nivel si lo deseas. Te recomendamos que no realices dos niveles de forma simultánea ya que los conocimientos de uno a otro son acumulativos.') ?>
        </p>
    </div>
</li>
<li>
    <span><?php echo Yii::t('mainApp', '¿Cómo puedo contactar con mi teacher?') ?></span>
    <div>
        <p>
            <?php echo Yii::t('mainApp', 'Esta posibilidad se ofrece con el Plan Premium') ?>.
        </p>
        <p>
            <?php echo Yii::t('mainApp', 'El contacto con el teacher se realiza a través de mensajes ilimitados de audio que podrás enviar y recibir desde nuestro campus. Las consultas que debes enviar a tu teacher deben ser de carácter lingüístico. Si tienes una consulta técnica puedes enviar un mail a ') ?><a href="mailto:support@abaenglish.com">support@abaenglish.com</a>.
        </p>
    </div>
</li>
<li>
    <span><?php echo Yii::t('mainApp', '¿Qué incluye cada unidad?') ?></span>
    <div>
        <p>
            <?php echo Yii::t('mainApp', 'Cada unidad está dividida en 8 secciones:') ?>.
        </p>
        <ol>
            <li>
                <u>ABA Film</u>: <?php echo Yii::t('mainApp', 'Verás un cortometraje de una situación, producción audiovisual exclusiva de American & British Academy, con los subtítulos en tu idioma') ?>.
            </li>
            <li>
                <u><?php echo Yii::t('mainApp', 'Habla') ?></u>: <?php echo Yii::t('mainApp', 'Grabarás y estudiarás toda la conversación anterior frase por frase, traducida y con ampliaciones a otros contextos') ?>.
            </li>
            <li>
                <u><?php echo Yii::t('mainApp', 'Escribe') ?></u>: <?php echo Yii::t('mainApp', 'Realizarás un dictado con todas las frases del ABA Film inicial para mejorar tu ortografía') ?>.
            </li>
            <li>
                <u><?php echo Yii::t('mainApp', 'Interpreta') ?></u>: <?php echo Yii::t('mainApp', 'Asumirás el papel de uno de los personajes del ABA Film inicial para mejorar tu fluidez viviendo una situación de la vida real') ?>.
            </li>
            <li>
                <u><?php echo Yii::t('mainApp', 'Videoclase') ?></u>: <?php echo Yii::t('mainApp', 'Los mejores profesores de American & British Academy te explicarán toda la gramática inglesa') ?>.
            </li>
            <li>
                <u><?php echo Yii::t('mainApp', 'Ejercicios') ?></u>: <?php echo Yii::t('mainApp', 'Encontrarás ejercicios escritos para practicar todo lo aprendido') ?>.
            </li>
            <li>
                <u><?php echo Yii::t('mainApp', 'Vocabulario') ?></u>: <?php echo Yii::t('mainApp', 'Repasarás las nuevas palabras en inglés que has aprendido en la unidad') ?>.
            </li>
            <li>
                <u><?php echo Yii::t('mainApp', 'Evaluación') ?></u>: <?php echo Yii::t('mainApp', 'Realizarás un test para comprobar lo que has aprendido') ?>.
            </li>
        </ol>
    </div>
</li>
<li>
    <span><?php echo Yii::t('mainApp', '¿Puedo repetir las unidades?') ?></span>
    <div>
        <p>
            <?php echo Yii::t('mainApp', 'Los ejercicios escritos no se pueden borrar, las grabaciones pueden volver a realizarse. Si lo que deseas es realizar toda la unidad de nuevo deberás enviarnos un mail con tu petición a ') ?><a href="mailto:support@abaenglish.com">support@abaenglish.com</a>.
        </p>
    </div>
</li>
<!--<li>
                <span><?php echo Yii::t('mainApp', '¿Puedo repetir las unidades?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Los ejercicios escritos no se pueden borrar, las grabaciones pueden volver a realizarse. Si lo que deseas es realizar toda la unidad de nuevo deberás enviarnos un mail con tu petición a ') ?><a href="mailto:support@abaenglish.com">support@abaenglish.com</a>.
                    </p>
                </div>
            </li>-->
<li>
    <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección ABA Film?') ?></span>
    <div>
        <p>
            <?php echo Yii::t('mainApp', 'Para completar la sección ABA Film debes ver el cortometraje por lo menos dos veces. Te recomendamos verlo una vez con subtítulos en español y otra con subtítulos en inglés') ?>.
        </p>
        <p>
            <?php echo Yii::t('mainApp', 'De cualquier forma, puedes ver el cortometraje tantas veces lo necesites hasta que lo entiendas perfectamente sin subtítulos') ?>.
        </p>
    </div>
</li>
<li>
    <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Habla?') ?></span>
    <div>
        <p>
            <?php echo Yii::t('mainApp', 'Para completar la sección Habla, debes realizar el ejercicio Escucha-Graba-Compara (LRC®) con al menos el 50% de todas las frases en inglés:') ?>
        </p>
        <ol>
            <li><?php echo Yii::t('mainApp', 'Haz clic en cada frase en inglés') ?>.</li>
            <li><?php echo Yii::t('mainApp', 'Haz clic en el botón de grabar para grabar la frase y otra vez cuando hayas acabado') ?>.</li>
            <li><?php echo Yii::t('mainApp', 'Compara la frase que has grabado con la voz nativa') ?>.</li>
        </ol>
        <p>
            <?php echo Yii::t('mainApp', 'De todas formas, te recomendamos realizar el ejercicio con todas las frases de la sección') ?>.
        </p>
        <p>
            <?php echo Yii::t('mainApp', 'Recuerda que puedes repetir el ejercicio todas las veces que lo necesites hasta que tu pronunciación se parezca a la del inglés nativo') ?>.
        </p>
    </div>
</li>
<li>
    <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Escribe?') ?></span>
    <div>
        <p>
            <?php echo Yii::t('mainApp', 'Para completar la sección Escribe debes escribir todas las frases') ?>.
        </p>
        <ol>
            <li><?php echo Yii::t('mainApp', 'Haz en cada recuadro') ?>.</li>
            <li><?php echo Yii::t('mainApp', 'Escucha la voz del nativo') ?>.</li>
            <li><?php echo Yii::t('mainApp', 'Escribe la frase correspondiente') ?>.</li>
        </ol>
        <p>
            <?php echo Yii::t('mainApp', 'Si escribes la frase correctamente, esta aparecerá en verde y ya podrás pasar a la siguiente, pero si la frase que escribes no es correcta, el recuadro aparecerá en rojo: la frase se reproduce de nuevo y el cursor se coloca en el lugar de la frase donde está el error') ?>.
        </p>
        <p>
            <?php echo Yii::t('mainApp', 'Siempre puedes consultar la respuesta correcta haciendo en el botón "?"') ?>.
        </p>
    </div>
</li>
<li>
    <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Interpreta?') ?></span>
    <div>
        <p>
            <?php echo Yii::t('mainApp', 'Para completar la sección debes interpretar a todos los personajes.') ?>
        </p>
        <ol>
            <li><?php echo Yii::t('mainApp', 'Elige un personaje') ?>.</li>
            <li><?php echo Yii::t('mainApp', 'Haz clic en "comenzar la interpretación" cuando estés preparado') ?>.</li>
            <li><?php echo Yii::t('mainApp', 'Graba asumiendo el papel del personaje apretando en el botón de grabar y otra vez cuando hayas terminado la frase') ?>.</li>
        </ol>
        <p>
            <?php echo Yii::t('mainApp', 'Al finalizar el diálogo, podrás escuchar toda la conversación apretando en el botón de "escuchar la interpretación"') ?>.
        </p>
        <p>
            <?php echo Yii::t('mainApp', 'Una vez interpretado a un personaje, debes interpretar también al otro personaje volviendo al inicio de la sección') ?>.
        </p>
    </div>
</li>
<li>
    <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Videoclase?') ?></span>
    <div>
        <p>
            <?php echo Yii::t('mainApp', 'Para completar la sección Videoclase debes ver el vídeo') ?>.
        </p>
        <p>
            <?php echo Yii::t('mainApp', 'También puedes leer toda la explicación gramatical en las siguientes páginas de esta sección') ?>.
        </p>
    </div>
</li>
<li>
    <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Ejercicios?') ?></span>
    <div>
        <p>
            <?php echo Yii::t('mainApp', 'Para completar la sección Ejercicios debes realizar todos los ejercicios') ?>.
        </p>
        <ol>
            <li><?php echo Yii::t('mainApp', 'Haz clic en el espacio de cada ejercicio') ?></li>
            <li><?php echo Yii::t('mainApp', 'Escribe la solución correspondiente') ?></li>
        </ol>
        <p>
            <?php echo Yii::t('mainApp', 'Si escribes la frase correctamente, esta aparecerá en verde y ya podrás pasar a la siguiente, pero si la frase que escribes no es correcta, el recuadro aparecerá en rojo: la frase se reproduce de nuevo y el cursor se coloca en el lugar de la frase donde está el error') ?>.
        </p>
        <p>
            <?php echo Yii::t('mainApp', 'Siempre puedes consultar la respuesta correcta haciendo en el botón "?"') ?>.
        </p>
    </div>
</li>
<li>
    <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Vocabulario?') ?></span>
    <div>
        <p>
            <?php echo Yii::t('mainApp', 'Para completar la sección deberás hacer el ejercicio Escucha-Graba-Compara (LRC®) con al menos el 50% de todas las palabras') ?>.
        </p>
        <ol>
            <li><?php echo Yii::t('mainApp', 'Haz clic en cada frase en inglés') ?></li>
            <li><?php echo Yii::t('mainApp', 'Haz clic en el botón de grabar para grabar la frase y otra vez cuando hayas acabado') ?></li>
            <li><?php echo Yii::t('mainApp', 'Compara la frase que has grabado con la voz nativa') ?>.</li>
        </ol>
        <p>
            <?php echo Yii::t('mainApp', 'De todas formas, te recomendamos realizar este ejercicio con todas las palabras') ?>.
        </p>
        <p>
            <?php echo Yii::t('mainApp', '¡El contador te mostrará las palabras que ya has aprendido!') ?>
        </p>
    </div>
</li>
<li>
    <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Evaluación?') ?></span>
    <div>
        <p>
            <?php echo Yii::t('mainApp', 'Puedes realizar esta sección sólo si has completado el 100% de las primeras siete secciones de la unidad') ?>.
        </p>
        <p>
            <?php echo Yii::t('mainApp', 'Haz clic en "Comenzar el Test" para empezar la prueba. Al final del test, puedes comprobar tu puntuación haciendo clic en "VER RESULTADO"') ?>.
        </p>
        <p>
            <?php echo Yii::t('mainApp', 'Para superar el test, debes obtener una puntuación superior a 8/10. Si no obtienes este resultado, debes repetir la prueba') ?>.
        </p>
    </div>
</li>
<li>
    <span><?php echo Yii::t('mainApp', '¿Qué es la gramática interactiva?') ?></span>
    <div>
        <p>
            <?php echo Yii::t('mainApp', 'Con la Gramática Interactiva puedes aprender y practicar a la vez. Es una guía útil que te ayudará durante el proceso de aprendizaje a entender de forma amena toda la gramática inglesa') ?>.
        </p>
        <p>
            <?php echo Yii::t('mainApp', 'Además, gracias a nuestra tecnología Escucha-Graba-Compara (LRC) podrás aprender a pronunciar correctamente todas las palabras') ?>.
        </p>
    </div>
</li>
<li>
    <span><?php echo Yii::t('mainApp', '¿Qué indica la barra de progreso de las unidades?') ?></span>
    <div>
        <p>
            <?php echo Yii::t('mainApp', 'Al acceder a la unidad y debajo del título de cada sección encontrarás una barra de progreso que refleja tu avance en cada una de las 7 secciones. Este avance se calcula en base a lo que hayas realizado en cada sección') ?>.
        </p>
        <p>
            <u>ABA Film</u>: <?php echo Yii::t('mainApp', 'se considera que esta sección está completada cuando se visualiza el ABA Film dos veces') ?>.
        </p>
        <p>
            <u><?php echo Yii::t('mainApp', 'Habla') ?></u>: <?php echo Yii::t('mainApp', 'se considera que esta sección está completada cuando hayas realizado el ejercicio Escucha-Graba-Compara con el 50% de todas las frases') ?>.
        </p>
        <p>
            <u><?php echo Yii::t('mainApp', 'Escribe') ?></u>: <?php echo Yii::t('mainApp', 'se considera que esta sección está completada cuando hayas completado todas las frases correctamente (deben estar en verde)') ?>.
        </p>
        <p>
            <u><?php echo Yii::t('mainApp', 'Interpreta') ?></u>: <?php echo Yii::t('mainApp', 'se considera que esta sección está completada cuando hayas interpretado todas las frases de todos los personajes') ?>.
        </p>
        <p>
            <u><?php echo Yii::t('mainApp', 'Videoclase') ?></u>: <?php echo Yii::t('mainApp', 'se considera que esta sección está completada cuando hayas visualizado la videoclase una vez') ?>.
        </p>
        <p>
            <u><?php echo Yii::t('mainApp', 'Ejercicios') ?></u>: <?php echo Yii::t('mainApp', 'se considera que esta sección está completada cuando hayas realizado todos los ejercicios de la sección correctamente (es decir, todos los ejercicios tienen que estar en verde)') ?>.
        </p>
        <p>
            <u><?php echo Yii::t('mainApp', 'Vocabulario') ?></u>: <?php echo Yii::t('mainApp', 'se considera que esta sección está completada cuando hayas realizado el ejercicio Escucha-Graba-Compara con el 50% de todas las palabras') ?>.
        </p>
        <p>
            <u><?php echo Yii::t('mainApp', 'Evaluación') ?></u>: <?php echo Yii::t('mainApp', 'se considera que esta sección esta completada cuando hayas obtenido un resultado de 8/10 o más') ?>.
        </p>
        <p>
            <?php echo Yii::t('mainApp', 'La barra de progreso total en la unidad que se encuentra en la parte superior se basa en la suma del promedio de las 7 primeras secciones') ?>.
        </p>
        <p>
            <?php echo Yii::t('mainApp', 'Puedes ver información más detallada sobre tu progreso si accedes al apartado "mi progreso" dentro de "mi cuenta"') ?>.
        </p>
    </div>
</li>
</ol>
</div>
<div class="help_dropdwon closed">
    <span class="arrow_closed"></span>
    <p><?php echo Yii::t('mainApp', 'Contacto') ?></p>
    <ol>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Cómo puedo contactar con vosotros?') ?></span>
            <div>
                <p>
                    <?php echo Yii::t('mainApp', 'Si no encuentras respuesta consultando la sección de ayuda de nuestra web, puedes contactar con nosotros enviando un mail a:') ?>
                </p>
                <p>
                    <a href="mailto:support@abaenglish.com">support@abaenglish.com</a><?php echo Yii::t('mainApp', ' si se trata de una consulta técnica') ?>
                </p>
                <p>
                    <a  target="_blank" href="https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998"><?php echo Yii::t('mainApp', 'Ayuda'); ?></a><?php echo Yii::t('mainApp', ' si se trata de una consulta relacionada con pagos o tipo de suscripción') ?>
                </p>
                <p>
                    <a href="mailto:info@abaenglish.com">info@abaenglish.com</a><?php echo Yii::t('mainApp', ' si deseas información sobre el curso') ?>
                </p>
                <p>
                    <?php echo Yii::t('mainApp', 'Si lo prefieres puedes llamarnos al 902 024 386 (personalizar en cada país) en horario de 9-14 y de 15-19h de lunes a viernes o enviarnos un mail con tu teléfono y te llamaremos') ?>.
                </p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Puedo pasar por vuestras oficinas?') ?></span>
            <div>
                <p><?php echo Yii::t('mainApp', 'Claro, estaremos encantados de conocerte. Puedes encontrarnos en: ') ?><a href="<?php echo Yii::app()->createUrl("contact/index") ?>"><?php echo Yii::t('mainApp', 'Contacto') ?></a></p>
            </div>
        </li>
    </ol>
</div>
<div class="help_dropdwon">
    <span class="arrow_opened"></span>
    <p><?php echo Yii::t('mainApp', 'Ayuda técnica') ?></p>
    <ol>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Qué requisitos técnicos son necesarios para realizar el curso de ABA English?') ?></span>
            <div>
                <p><?php echo Yii::t('mainApp', 'Hardware') ?></p>
                <ul>
                    <li><?php echo Yii::t('mainApp', 'PC Pentium III o superior / Macintosh') ?></li>
                    <li><?php echo Yii::t('mainApp', 'Tarjeta de sonido 16 Bits compatible Sound Blaster') ?></li>
                    <li><?php echo Yii::t('mainApp', 'Auriculares con micrófono(recomendado) o micrófono y altavoces independientes') ?></li>
                </ul>
                <p><?php echo Yii::t('mainApp', 'Software') ?></p>
                <ul>
                    <li><?php echo Yii::t('mainApp', 'Windows 98 / ME / 2000 / XP / Vista / Linux / MAC OS o superior') ?></li>
                    <li><?php echo Yii::t('mainApp', 'Navegador: Todos, pero recomedamos Mozilla Firefox o Chrome') ?></li>
                    <li><?php echo Yii::t('mainApp', 'Resolución de pantalla: 1024 x 768') ?></li>
                    <li>
                        <p><?php echo Yii::t('mainApp', 'Flash: Si no dispones del software puede descargarlo aquí:') ?></p>
                        <p>
                            <a href="http://get.adobe.com/es/flashplayer/">get.adobe.com/es/flashplayer/</a>
                        </p>
                    </li>
                </ul>
                <p><?php echo Yii::t('mainApp', 'Conexión a Internet') ?></p>
                <ul>
                    <li><?php echo Yii::t('mainApp', 'ADSL, RDSI, FIBRA ÓPTICA') ?></li>
                    <li><?php echo Yii::t('mainApp', 'La conexión a Internet debe ser estable. En ocasiones algunas conexiones tipo USB o 3G no lo son, y dificultan la realización del curso. Si te conectas mediante una red wifi debes tener una buena intensidad de la señal') ?>.</li>
                </ul>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', 'Comprueba que tu ordenador está preparado para realizar el curso de ABA English') ?>.</span>
            <div>
                <p>
                    <?php echo Yii::t('mainApp', 'En el campus encontrarás una aplicación que permite testear el estado de tu ordenador y conexión a Internet con el fin de asegurarnos de que realizas el curso en las mejores condiciones') ?>.
                </p>
                <p>
                    <?php echo Yii::t('mainApp', 'Por favor realiza el test de instalación haciendo accediendo al campus y sigue los consejos que te muestra el test. En el caso de que tengas dudas, puedes escribir un mail a ') ?><a href="mailto:support@abaenglish.com">support@abaenglish.com</a>.
                </p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', '¿El curso es compatible con cualquier dispositivo?') ?></span>
            <div>
                <p>
                    <?php echo Yii::t('mainApp', 'Por el momento nuestro curso no es compatible con tablets ni terminales telefónicos, estamos trabajando para solucionarlo') ?>.
                </p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', 'No puedo grabar o no oigo el audio del curso.') ?></span>
            <div>
                <p><?php echo Yii::t('mainApp', 'En ambos casos puede tratarse de una incidencia en la configuración del micrófono y auriculares que utilizas para realizar el curso, sigue estos pasos: ') ?></p>
                <p><?php echo Yii::t('mainApp', 'A. Comprueba que tus auriculares estén bien conectados a tu ordenador') ?>.</p>
                <p><?php echo Yii::t('mainApp', 'Algunos auriculares tienen un botón de silencio; comprueba que no estén puestos en dicha posición. Existen auriculares tipo clavija o tipo usb, en el caso de los que son tipo clavija, normalmente son de dos colores, el color rosa debe ir conectado a la clavija del micro y el color verde debe ir conectado a la clavija de los auriculares. En el caso de los auriculares tipo USB deben estar conectados a una entrada USB.') ?></p>
                <p><?php echo Yii::t('mainApp', 'B. Accede al curso') ?></p>
                <ul>
                    <li><?php echo Yii::t('mainApp', 'Cuando aparezca la ventana de configuración de Adobe Flash Player que te solicita el acceso a tu cámara y micro, NO pulses ninguno de los dos botones') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'Sitúate encima de la ventana de configuración y haz click con el botón derecho') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'Aparecerá un menú en el que debes seleccionar la opción Configuración') ?></li>
                    <li><?php echo Yii::t('mainApp', 'Esta ventana de configuración te aparecerá con la opción "denegar" marcada, ya que todavía no hemos aceptado el acceso a micro y cámara. Pulsa encima del icono del micrófono que ves en esta ventana') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'Verás que se activa una barra vertical de señal de micrófono, vamos a probarlo') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'Habla con el micrófono y observa como el indicador (la barra) reacciona a la señal de la voz') ?>.</li>
                </ul>
                <p><?php echo Yii::t('mainApp', 'Si la señal no llega: ') ?>
                <p><?php echo Yii::t('mainApp', '- Puedes nivelar el volumen de registro, moviendo el cursor que ves en la misma ventana') ?>.</p>
                <p><?php echo Yii::t('mainApp', '- Verifica que el micrófono esté conectado correctamente') ?>.</p>
                <ul>
                    <li><?php echo Yii::t('mainApp', 'Si verificados estos parámetros, continuamos sin señal, significa que el micrófono no está correctamente configurado en el sistema. A continuación te explicamos cómo configurar el micrófono en tu ordenador:') ?></li>
                </ul>
                <p><?php echo Yii::t('mainApp', 'C. Configura el micrófono y auriculares adecuadamente:') ?></p>
                <ul>
                    <li><?php echo Yii::t('mainApp', 'Accede al panel de control de tu ordenador') ?></li>
                    <li><?php echo Yii::t('mainApp', 'Haz clic en Hardware y Sonido') ?></li>
                    <li><?php echo Yii::t('mainApp', 'En el apartado sonido, accede a "administrar dispositivos de audio"') ?></li>
                    <li><?php echo Yii::t('mainApp', 'Para el micro: Sitúate en la pestaña grabar, asegúrate de que el micrófono que estas utilizando para el curso aparece marcado como dispositivo predeterminado. Si no es así sitúate encima del que corresponda, pulsa con el botón derecho del mouse, y del menú que aparece selecciona "establecer como dispositivo predeterminado"') ?></li>
                    <li><?php echo Yii::t('mainApp', 'Para los auriculares: Sitúate en la pestaña reproducción, asegúrate de que los auriculares que estas utilizando para el curso aparece marcado como dispositivo predeterminado. Si no es así sitúate encima del que corresponda, pulsa con el botón derecho del mouse, y del menú que aparece selecciona "establecer como dispositivo predeterminado"') ?></li>
                </ul>
                <p><?php echo Yii::t('mainApp', 'D. Acepta el permiso que se solicita en la ventana de Flash') ?></p>
                <p><?php echo Yii::t('mainApp', 'Asegúrate de que le has dado a aceptar a la ventana de Flash que aparece al acceder a la unidad, si no le das a permitir en la ventana de Flash, no podrás realizar las grabaciones del curso.') ?></p>
                <p><?php echo Yii::t('mainApp', 'Comprueba que la versión de flash que estas utilizando es la más adecuada. Si no dispones de Flash o la versión que tienes no es la última puedes descargarte el software en ') ?><a href="http://get.adobe.com/es/flashplayer/">get.adobe.com/es/flashplayer/</a>.</p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', '¿Cómo puedo aumentar el volumen de la grabación?') ?></span>
            <div>
                <p><?php echo Yii::t('mainApp', 'Para realizar los ejercicios de audio correctamente es necesario que tu grabación se escuche en un volumen alto, similar al del personaje con el comparas tu voz. Para subir el volumen hay que:') ?></p>
                <p><?php echo Yii::t('mainApp', 'Acceder al curso:') ?></p>
                <ul>
                    <li><?php echo Yii::t('mainApp', 'Cuando aparezca la ventana de configuración de Adobe Flash Player que te solicita el acceso a tu cámara y micro, NO pulses ninguno de los dos botones') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'Sitúate encima de la ventana de configuración y haz click con el botón derecho') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'Aparecerá un menú en el que debes seleccionar la opción Configuración') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'Esta ventana de configuración te aparecerá con la opción "denegar" marcada, ya que todavía no hemos aceptado el acceso a micro y cámara. Pulsa encima del icono del micrófono que ves en esta ventana') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'Verás que se activa una barra vertical de señal de micrófono, vamos a probarlo') ?>.</li>
                    <li>
                        <p><?php echo Yii::t('mainApp', 'Habla con el micrófono y observa como el indicador (la barra) reacciona a la señal de la voz') ?>.</p>
                        <p><?php echo Yii::t('mainApp', '- Nivela el volumen de registro, moviendo el cursor que ves en la misma ventana') ?>.</p>
                        <p><?php echo Yii::t('mainApp', '- Haz click en aceptar') ?></p>
                    </li>
                </ul>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', 'La ventana de Flash Media Player no aparece cuando accedo a las unidades') ?>.</span>
            <div>
                <p><?php echo Yii::t('mainApp', 'Si la ventana de Flash no aparece debes asegurarte de que Flash esté instalado en tu ordenador, para ello te recomendamos que realices el test de instalación, haciendo click aquí o accediendo al campus. Si el test te indica que no dispone de Flash, puedes descargarlo haciendo clic en:') ?></p>
                <p>
                    <a href="http://get.adobe.com/es/flashplayer/">get.adobe.com/es/flashplayer/</a>
                </p>
                <p><?php echo Yii::t('mainApp', 'Si el test no te indica ningún error, puede que la ventana no aparezca porque no estás visualizando la pantalla en el tamaño adecuado, puedes resolverlo siguiendo estos pasos:') ?></p>
                <ol>
                    <li><?php echo Yii::t('mainApp', 'Ir al menú "ver" de la barra del navegador, seleccionar opción tamaño y escoger opción "inicio"') ?></li>
                    <li><?php echo Yii::t('mainApp', 'Puedes realizar lo mismo pulsando en el teclado simultáneamente las tecla Ctrl+0') ?>.</li>
                </ol>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', 'La ventana de Flash aparece en rojo y no puedo pulsar en el botón permitir') ?>.</span>
            <div>
                <p><?php echo Yii::t('mainApp', 'Esto es debido a una incidencia de configuración del permiso permanente de la ventana del Flash Player. Para que esto no vuelva a ocurrir es necesario que cada vez que accedas a una unidad la ventana aparezca y le des a permitir') ?>.</p>
                <p>
                    <?php echo Yii::t('mainApp', 'Te indicamos los pasos para volver a la configuración adecuada:') ?>
                </p>
                <ul>
                    <li><?php echo Yii::t('mainApp', 'Accede al curso y accede a una unidad') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'Espera a que aparezca la ventana de flash que se queda colgada (normalmente aparece con un fondo rojo)') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'Sitúate encima de la ventana y haz clic con el botón derecho') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'Aparece un menú, selecciona configuración global') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'Aparece un ventana, haz clic en la pestaña cámara y micrófono') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'Haz clic en el botón gris que pone configuración de cámara y micrófono por sitio') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'Selecciona el sitio web ') ?>
                            <a href="http://<?php echo Yii::app()->config->get("URL_DOMAIN_WEB"); ?>">
                                <?php echo Yii::app()->config->get("URL_DOMAIN_WEB"); ?>
                            </a>
                    </li>
                    <li><?php echo Yii::t('mainApp', 'Debajo de la frase "si el sitio seleccionado quiere utilizar una cámara o micrófono" aparece una cajita con varias opciones, escoge la de pedirme permiso y dale a cerrar') ?>.</li>
                    <li><?php echo Yii::t('mainApp', 'Sal del curso y vuelve a entrar') ?></li>
                    <li><?php echo Yii::t('mainApp', 'Ahora debería aparecer la ventana de flash y debes darle a permitir') ?>.</li>
                </ul>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', 'El curso se ha quedado bloqueado') ?></span>
            <div>
                <p><?php echo Yii::t('mainApp', 'En algunas ocasiones el rendimiento del curso puede verse afectado por el funcionamiento del software antivirus y/o de software de descargas, en estos casos te recomendamos que:') ?></p>
                <p><?php echo Yii::t('mainApp', '- Desinstales momentáneamente el software antivirus y compruebes el funcionamiento del curso') ?>.</p>
                <p><?php echo Yii::t('mainApp', '- Prueba la conexión al curso, sin tener abierto ningún programa de descargas') ?>.</p>
                <p><?php echo Yii::t('mainApp', 'En otras ocasiones es debido al tipo de navegador que utilizas para conectarte a Internet. Nuestro curso funciona con todos los navegadores pero te recomendamos que utilices Mozilla Firefox o Google Chrome, puedes descargarlos en estos links') ?></p>
                <p>
                    <a href="http://www.mozilla-europe.org">www.mozilla-europe.org</a>
                </p>
                <p>
                    <a href="http://www.google.com/chrome">www.google.com/chrome</a>
                </p>
                <p><?php echo Yii::t('mainApp', 'Por otra parte a veces el rendimiento puede verse afectado por la necesidad de activar cookies o por la necesidad de borrar la memoria caché de tu ordenador. A continuación te indicamos como actuar en ambos casos en función del navegador que utilices') ?></p>
                <ul>
                    <li>Mozilla Firefox</li>
                </ul>
                <p><?php echo Yii::t('mainApp', 'Activar cookies:') ?></p>
                <p>
                    <a href="http://support.mozilla.org/es/kb/Borrar%20cookies">support.mozilla.org/es/kb/Borrar%20cookies</a>
                </p>
                </p><?php echo Yii::t('mainApp', 'Borrar caché:') ?></p>
                <p>
                    <a href="http://support.mozilla.org/es/kb/como-limpiar-la-cache?s=cach%C3%A9&r=0&e=es&as=s">support.mozilla.org/es/kb/como-limpiar-la-cache?s=cach%C3%A9&r=0&e=es&as=s</a>
                </p>
                <ul>
                    <li>Chrome</li>
                </ul>
                <p><?php echo Yii::t('mainApp', 'Activar cookies:') ?></p>
                <p>
                    <a href="http://support.google.com/chrome/bin/answer.py?hl=es&answer=95647">support.google.com/chrome/bin/answer.py?hl=es&answer=95647</a>
                </p>
                <p><?php echo Yii::t('mainApp', 'Borrar caché:') ?></p>
                <p>
                    <a href="http://support.google.com/chrome/bin/answer.py?hl=es&answer=95582">support.google.com/chrome/bin/answer.py?hl=es&answer=95582</a>
                </p>
            </div>
        </li>
        <li>
            <span><?php echo Yii::t('mainApp', 'No puedo conectarme al curso, aparece, "servidor de audio fallido"') ?>.</span>
            <div>
                <p><?php echo Yii::t('mainApp', 'En este caso puede tratarse de una incidencia en nuestros servidores, si es así seguro que ya estamos trabajando para solventarlo. Por favor escríbenos un mail a ') ?><a href="mailto:support@abaenglish.com">support@abaenglish.com</a><?php echo Yii::t('mainApp', ' y te informaremos del estado de la incidencia') ?>.</p>
            </div>
        </li>
    </ol>
</div>

</div>
</div>
<div class="clear"></div>