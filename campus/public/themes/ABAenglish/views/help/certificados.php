<div class="contenedorAyuda left">
    <div class="tituloSeccionCampus"><?php echo Yii::t('mainApp', 'Ayuda') ?></div>
    <?php $this->renderPartial('menu'); ?>
    <div class="bodyAyuda left">
        <p><?php echo Yii::t('mainApp', 'Certificados') ?></p>
        <ol>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Qué certificado obtengo con el curso de ABA English?') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'Se trata de un certificado emitido por American & British Academy que avala los conocimientos adquiridos con nuestro curso de inglés') ?>.</p>
                    <p><?php echo Yii::t('mainApp', 'El certificado indica el nivel superado y las competencias lingüísticas adquiridas en relación al Marco Común Europeo de Referencia para las Lenguas (MCER); por otra parte, también indica el nº de horas necesarias para terminar el curso y la fecha de obtención del mismo') ?>.</p>
                    <p><?php echo Yii::t('mainApp', 'El certificado solo está disponible para alumnos ABA Premium. Para obtener el certificado de tu nivel debes completar el 100% de las primeras siete secciones de todas las unidades del nivel correspondiente y superar el test final que corresponde a cada unidad') ?>.</p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿El curso de ABA English sirve para preparar exámenes oficiales?') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'El curso de ABA English te prepara para alcanzar el nivel adecuado en cualquiera de los exámenes que mencionamos a continuación.') ?>.</p>
                    <p>
                        <u><?php echo Yii::t('mainApp', 'Certificados de estudios de Inglés Británico Cambridge ESOL - English for Speakers of Other Languages:') ?></u>
                    </p>
                    <ul>
                        <li><?php echo Yii::t('mainApp', 'KET, Key English Test: es un certificado de nivel principiante') ?>.</li>
                        <li><?php echo Yii::t('mainApp', 'PET, Preliminary English Test: es un certificado de nivel intermedio, implica la habilidad del hablante para comunicarse en distintas situaciones de la vida real. Es necesario un nivel de comprensión avanzado') ?>.</li>
                        <li><?php echo Yii::t('mainApp', 'FCE, First Certificate in English: es un certificado de nivel intermedio alto, indica un importante conocimiento de las estructuras gramaticales y un vocabulario muy amplio.') ?></li>
                    </ul>
                    <p>
                        <?php echo Yii::t('mainApp', 'En ABA English te indicamos el centro asociado de la Universidad de Cambridge más cercano') ?>.
                    </p>
                    <p>
                        <u>
                            <?php echo Yii::t('mainApp', 'El Certificado de Inglés Americano TOEFL - Test of English as a Foreign Language') ?>
                        </u>
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'Evalúa el nivel de competencia de inglés. En las convocatorias de examen para estos certificados no hay aprobado o suspenso sino que en todos los casos hay un resultado equivalente al nivel de inglés que tienes. Cada una de las universidades americanas exige un nivel TOEFL determinado. Los exámenes se realizan en los centros acreditados y se utiliza un PC. No hay examen oral.') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'En ABA English te indicamos dónde puedes examinarte para el examen TOEFL') ?>.
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Por qué no puedo descargarme el certificado?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'La opción de descarga del certificado esta disponible para los alumnos con plan Premium, o alumnos que hubieran contratado anteriormente un plan Premium y superado el nivel') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'Para poder descargártelo deberás haber completado el 100 % de las primeras siete secciones de cada unidad de tu nivel, y haber superado con éxito la sección evaluación de cada unidad del nivel. Si cumples con estas condiciones y tu certificado no está disponible contacta con nosotros escribiendo un mail a ') ?><a href="mailto:support@abaenglish.com">support@abaenglish.com</a>.
                    </p>
                </div>
            </li>
        </ol>
    </div>
</div>
<div class="clear"></div>