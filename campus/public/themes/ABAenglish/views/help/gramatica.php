<div class="contenedorAyuda left">
    <div class="tituloSeccionCampus"><?php echo Yii::t('mainApp', 'Ayuda') ?></div>
    <?php $this->renderPartial('menu'); ?>
    <div class="bodyAyuda left">
        <p><?php echo Yii::t('mainApp', 'Gramática interactiva') ?></p>
        <ol>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Qué es la gramática interactiva?') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'Con la Gramática Interactiva puedes aprender y practicar a la vez. Es una guía útil que te ayudará durante el proceso de aprendizaje a entender de forma amena toda la gramática inglesa') ?>.</p>
                    <p><?php echo Yii::t('mainApp', 'Además, gracias a nuestra tecnología Escucha-Graba-Compara (LRC) podrás aprender a pronunciar correctamente todas las palabras') ?>.</p>
                    <p><?php echo Yii::t('mainApp', '¡Consúltala cada vez que tengas una duda!') ?></p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo funciona la gramática interactiva?') ?></span>
                <div>
                    <p>falta texto</p>
                </div>
            </li>
        </ol>
    </div>
</div>
<div class="clear"></div>