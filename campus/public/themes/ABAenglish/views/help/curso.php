<div class="contenedorAyuda left">
    <div class="tituloSeccionCampus"><?php echo Yii::t('mainApp', 'Ayuda') ?></div>
    <?php $this->renderPartial('menu'); ?>
    <div class="bodyAyuda left">
        <p><?php echo Yii::t('mainApp', 'Curso') ?></p>
        <ol>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Qué incluye el curso?') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'En los planes Plus, Premium y Forever, tienes acceso al contenido completo del curso que se estructura en estas tres herramientas; las unidades, la gramática interactiva y los mensajes de tu profesor') ?>.</p>
                    <ul>
                        <li><?php echo Yii::t('mainApp', 'Las unidades del curso conforman la parte más amplia del mismo, cada una de ellas te ayudará a desarrollar las 4 competencias básicas del idioma (leer, escuchar, escribir y hablar)') ?>.</li>
                        <li><?php echo Yii::t('mainApp', 'La gramática interactiva te acompañará a lo largo de todo el curso, sirviéndote como herramienta de apoyo y de consulta a lo largo de todas las unidades, te ayudará a entender de forma sintética y amena  toda la gramática inglesa, podrás escuchar todas las palabras y verás ejemplos para ampliar tu vocabulario') ?>.</li>
                        <li><?php echo Yii::t('mainApp', 'Dispondrás de un profesor que resolverá tus dudas y que te ayudará durante tu aprendizaje') ?>.</li>
                    </ul>
                    <p><?php echo Yii::t('mainApp', 'En el plan ABA Free, podrás realizar una unidad de cada nivel, y ver progresivamente y una sola vez las videoclases de todo las unidades. Con este plan también dispones de la gramática interactiva') ?>.</p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cuánto dura el curso?') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'Depende del tipo de suscripción que escojas, con cualquiera de nuestros planes podrás escoger la suscripción mensual, semestral y anual. La renovación de la suscripción es automática a menos que nos indiques lo contrario. Puedes estar en ABA el tiempo que lo desees, transcurridos 18 meses de una suscripción Premium o Plus, pasarás a ser ABA Forever y podrás disfrutar de nuestro curso para siempre sin tener que pagar ninguna suscripción') ?>.</p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cuánto tiempo se tarda en completar un nivel?') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'El curso empieza en el momento que te des de alta y finaliza en el momento que tú decidas. Durante este tiempo podrás acceder a nuestro campus las 24 horas del día, 7 días a la semana.') ?></p>
                    <p><?php echo Yii::t('mainApp', 'Puedes avanzar en el curso a tu ritmo. Tu progreso dependerá del tiempo que le dediques a cada unidad y de la frecuencia con la que te conectes') ?>.</p>
                    <p><?php echo Yii::t('mainApp', 'Para completar cada nivel debes realizar las 24 unidades que lo componen y cada unidad se completa realizando los ocho ejercicios') ?>.</p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Por qué nivel empiezo?') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'Puedes empezar por el nivel que tú quieras pero te recomendamos que realices primero el test de nivel o te guíes por los contenidos de cada nivel') ?>.</p>
                    <p><?php echo Yii::t('mainApp', 'A lo largo del curso podrás acceder a todos los niveles y cambiar de nivel si lo deseas. Te recomendamos que no realices dos niveles de forma simultánea ya que los conocimientos de uno a otro son acumulativos') ?>.</p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo puedo contactar con mi teacher?') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'Esta posibilidad solo se ofrece en el Plan Plus') ?>.</p>
                    <p><?php echo Yii::t('mainApp', 'El contacto con el teacher se realiza a través de mensajes que podrás enviar y recibir desde nuestro campus') ?>.</p>
                    <p><?php echo Yii::t('mainApp', 'Con el Plan Plus dispondrás de consultas ilimitadas') ?>.</p>
                    <p><?php echo Yii::t('mainApp', 'ABA English no ofrece clases telefónicas ni one to one, pero estamos seguros de que nos nuestra metodología e interactividad lograrás alcanzar el nivel que deseas') ?>.</p>
                </div>
            </li>
        </ol>
    </div>
</div>
<div class="clear"></div>