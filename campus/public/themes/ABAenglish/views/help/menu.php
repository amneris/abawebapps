<div class="menuAyuda left">
    <ul>
        <li>
            <a href="<?php echo Yii::app()->createUrl("help/index") ?>">
                <span class="left"><?php echo Yii::t('mainApp', 'Top FAQS') ?></span>
                <span class="right"></span>
            </a>
        </li>
        <li>
            <a href="<?php echo Yii::app()->createUrl("help/precios") ?>">
                <span class="left"><?php echo Yii::t('mainApp', 'Precios y formas de pago') ?></span>
                <span class="right"></span>
            </a>
        </li>
        <li>
            <a href="<?php echo Yii::app()->createUrl("help/certificados") ?>">
                <span class="left"><?php echo Yii::t('mainApp', 'Certificados') ?></span>
                <span class="right"></span>
            </a>
        </li>
        <li>
            <a href="<?php echo Yii::app()->createUrl("help/planes") ?>">
                <span class="left"><?php echo Yii::t('mainApp', 'Planes') ?></span>
                <span class="right"></span>
            </a>
        </li>
        <!--
        <li>
            <a href="<?php echo Yii::app()->createUrl("help/curso") ?>">
                <span class="left"><?php echo Yii::t('mainApp', 'Curso') ?></span>
                <span class="right"></span>
            </a>
        </li>
        <li>
            <a href="<?php echo Yii::app()->createUrl("help/unidades") ?>">
                <span class="left"><?php echo Yii::t('mainApp', 'Unidades') ?></span>
                <span class="right"></span>
            </a>
        </li>
        -->
        <li>
            <a href="<?php echo Yii::app()->createUrl("help/contacto") ?>">
                <span class="left"><?php echo Yii::t('mainApp', 'Contacto') ?></span>
                <span class="right"></span>
            </a>
        </li>
        <!--
        <li>
            <a href="<?php echo Yii::app()->createUrl("help/teachers") ?>">
                <span class="left"><?php echo Yii::t('mainApp', 'Teachers') ?></span>
                <span class="right"></span>
            </a>
        </li>
        -->
        <li>
            <a href="<?php echo Yii::app()->createUrl("help/ayudatecnica") ?>">
                <span class="left"><?php echo Yii::t('mainApp', 'Ayuda técnica') ?></span>
                <span class="right"></span>
            </a>
        </li>
    </ul>
</div>