<div class="contenedorAyuda left">
    <div class="tituloSeccionCampus"><?php echo Yii::t('mainApp', 'Ayuda') ?></div>
    <?php $this->renderPartial('menu'); ?>
    <div class="bodyAyuda left">
        <p><?php echo Yii::t('mainApp', 'Unidades') ?></p>
        <ol>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Qué incluye cada unidad?') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'Cada unidad esta dividida en 8 secciones:') ?></p>
                    <ol>
                        <li><?php echo Yii::t('mainApp', 'Comprende: en esta sección verás el cortometraje introductorio de la unidad en inglés, con subtítulos en tu idioma y en inglés') ?>.</li>
                        <li><?php echo Yii::t('mainApp', 'Habla: en esta sección analizarás el vídeo inicial, estudiando cada frase y ampliando su contexto') ?>.</li>
                        <li><?php echo Yii::t('mainApp', 'Escribe: en esta sección aprenderás a escribir las frases del diálogo inicial como si fuera un dictado, mejorando así tu ortografía') ?>.</li>
                        <li><?php echo Yii::t('mainApp', 'Interpreta: en esta sección asumes el papel de unos de los personajes del vídeo inicial para mejorar tu fluidez viviendo una situación de la vida real') ?>.</li>
                        <li><?php echo Yii::t('mainApp', 'Videoclase: en esta sección uno de nuestros teachers te explicará la gramática de la unidad') ?>.</li>
                        <li><?php echo Yii::t('mainApp', 'Ejercicios: en esta sección pondrás en práctica la gramática completando distintos ejercicios') ?>.</li>
                        <li><?php echo Yii::t('mainApp', 'Vocabulario: en esta sección aprenderás el vocabulario de la unidad, podrás leer su significado, y escuchar y grabar cada palabra') ?>.</li>
                        <li><?php echo Yii::t('mainApp', 'Evaluación: es esta sección evaluaremos los conocimientos adquiridos a lo largo de la unidad') ?>.</li>
                    </ol>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Puedo repetir las unidades?') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'Los ejercicios escritos no se pueden borrar, las grabaciones pueden volver a realizarse. Si lo que deseas es realizar toda la unidad de nuevo deberás enviarnos un mail con tu petición a ') ?><a href="mailto:support@abaenglish.com">support@abaenglish.com</a>.</p>
                    <p>
                        <u>
                            <?php echo Yii::t('mainApp', 'En el Plan ABA Free solo podrás ver las videoclases del nivel que hayas escogido una vez.') ?>
                        </u>
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Comprende?') ?></span>
                <div>
                    <p>falta texto</p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Habla?') ?></span>
                <div>
                    <p>falta texto</p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Escribe?') ?></span>
                <div>
                    <p>falta texto</p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Interpreta?') ?></span>
                <div>
                    <p>falta texto</p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección de la Videoclasse?') ?></span>
                <div>
                    <p>falta texto</p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Ejercicios?') ?></span>
                <div>
                    <p>falta texto</p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Vocabulario?') ?></span>
                <div>
                    <p>falta texto</p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Evaluación?') ?></span>
                <div>
                    <p>falta texto</p>
                </div>
            </li>
        </ol>
    </div>
</div>
<div class="clear"></div>