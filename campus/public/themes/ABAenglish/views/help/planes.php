<div class="contenedorAyuda left">
    <div class="tituloSeccionCampus"><?php echo Yii::t('mainApp', 'Ayuda') ?></div>
    <?php $this->renderPartial('menu'); ?>
    <div class="bodyAyuda left">
        <p><?php echo Yii::t('mainApp', 'Planes') ?></p>
        <ol>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cuánto dura el curso?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Depende del tipo de suscripción que escojas, con cualquiera de nuestros planes podrás escoger la suscripción mensual, semestral y anual. Para garantizar un servicio ininterrumpido, la suscripción al curso se renovará automáticamente, utilizando la tarjeta de crédito registrada. Cuando tú quieras podrás solicitar la baja del curso.') ?>
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cuánto tiempo se tarda en completar un nivel?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'El curso empieza en el momento en que te des de alta y finaliza en el momento que tú decidas. Durante este tiempo podrás acceder a nuestro campus las 24 horas del día, 7 días a la semana') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'Puedes avanzar en el curso a tu ritmo. Tu progreso dependerá del tiempo que le dediques a cada unidad y de la frecuencia con la que te conectes') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'Para completar cada nivel debes realizar las 24 unidades que lo componen y cada unidad se completa realizando las ocho secciones') ?>.
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Por qué nivel empiezo?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Antes de acceder al Campus de ABA English escoges el nivel por el que quieres empezar; si no sabes cuál es tu nivel puedes realizar el test para situarte. Para un correcto y progresivo aprendizaje del idioma te aconsejamos que empieces por el nivel Beginners') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'Puedes consultar la lista de los niveles de ABA English en relación al Marco Común Europeo de Referencia para las Lenguas (MCER) aquí:') ?>
                    </p>
                    <p>
                        <a href=""><?php echo Yii::t('mainApp', 'A lo largo del curso podrás acceder a todos los niveles y cambiar de nivel si lo deseas. Te recomendamos que no realices dos niveles de forma simultánea ya que los conocimientos de uno a otro son acumulativos.') ?></a>
                    </p>
                    <p>
                        <a href=""><?php echo Yii::t('mainApp', 'link_to_aba_english_levels') ?></a>
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo puedo contactar con mi teacher?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Esta posibilidad se ofrece con el Plan Premium') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'El contacto con el teacher se realiza a través de mensajes ilimitados de audio que podrás enviar y recibir desde nuestro campus. Las consultas que debes enviar a tu teacher deben ser de carácter lingüístico. Si tienes una consulta técnica puedes enviar un mail a ') ?><a href="mailto:support@abaenglish.com">support@abaenglish.com</a>.
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Qué incluye cada unidad?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Cada unidad está dividida en 8 secciones:') ?>.
                    </p>
                    <ol>
                        <li>
                            <u>ABA Film</u>: <?php echo Yii::t('mainApp', 'Verás un cortometraje de una situación, producción audiovisual exclusiva de American & British Academy, con los subtítulos en tu idioma') ?>.
                        </li>
                        <li>
                            <u><?php echo Yii::t('mainApp', 'Habla') ?></u>: <?php echo Yii::t('mainApp', 'Grabarás y estudiarás toda la conversación anterior frase por frase, traducida y con ampliaciones a otros contextos') ?>.
                        </li>
                        <li>
                            <u><?php echo Yii::t('mainApp', 'Escribe') ?></u>: <?php echo Yii::t('mainApp', 'Realizarás un dictado con todas las frases del ABA Film inicial para mejorar tu ortografía') ?>.
                        </li>
                        <li>
                            <u><?php echo Yii::t('mainApp', 'Interpreta') ?></u>: <?php echo Yii::t('mainApp', 'Asumirás el papel de uno de los personajes del ABA Film inicial para mejorar tu fluidez viviendo una situación de la vida real') ?>. 
                        </li>
                        <li>
                            <u><?php echo Yii::t('mainApp', 'Videoclase') ?></u>: <?php echo Yii::t('mainApp', 'Los mejores profesores de American & British Academy te explicarán toda la gramática inglesa') ?>.
                        </li>
                        <li>
                            <u><?php echo Yii::t('mainApp', 'Ejercicios') ?></u>: <?php echo Yii::t('mainApp', 'Encontrarás ejercicios escritos para practicar todo lo aprendido') ?>.
                        </li>
                        <li>
                            <u><?php echo Yii::t('mainApp', 'Vocabulario') ?></u>: <?php echo Yii::t('mainApp', 'Repasarás las nuevas palabras en inglés que has aprendido en la unidad') ?>. 
                        </li>
                        <li>
                            <u><?php echo Yii::t('mainApp', 'Evaluación') ?></u>: <?php echo Yii::t('mainApp', 'Realizarás un test para comprobar lo que has aprendido') ?>.
                        </li>
                    </ol>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Puedo repetir las unidades?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Los ejercicios escritos no se pueden borrar, las grabaciones pueden volver a realizarse. Si lo que deseas es realizar toda la unidad de nuevo deberás enviarnos un mail con tu petición a ') ?><a href="mailto:support@abaenglish.com">support@abaenglish.com</a>.
                    </p>
                </div>
            </li>
            <!--<li>
                <span><?php echo Yii::t('mainApp', '¿Puedo repetir las unidades?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Los ejercicios escritos no se pueden borrar, las grabaciones pueden volver a realizarse. Si lo que deseas es realizar toda la unidad de nuevo deberás enviarnos un mail con tu petición a ') ?><a href="mailto:support@abaenglish.com">support@abaenglish.com</a>.
                    </p>
                </div>
            </li>-->
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección ABA Film?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Para completar la sección ABA Film debes ver el cortometraje por lo menos dos veces. Te recomendamos verlo una vez con subtítulos en español y otra con subtítulos en inglés') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'De cualquier forma, puedes ver el cortometraje tantas veces lo necesites hasta que lo entiendas perfectamente sin subtítulos') ?>.
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Habla?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Para completar la sección Habla, debes realizar el ejercicio Escucha-Graba-Compara (LRC®) con al menos el 50% de todas las frases en inglés:') ?>
                    </p>
                    <ol>
                        <li><?php echo Yii::t('mainApp', 'Haz clic en cada frase en inglés') ?>.</li>
                        <li><?php echo Yii::t('mainApp', 'Haz clic en el botón de grabar para grabar la frase y otra vez cuando hayas acabado') ?>.</li>
                        <li><?php echo Yii::t('mainApp', 'Compara la frase que has grabado con la voz nativa') ?>.</li>
                    </ol>
                    <p>
                        <?php echo Yii::t('mainApp', 'De todas formas, te recomendamos realizar el ejercicio con todas las frases de la sección') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'Recuerda que puedes repetir el ejercicio todas las veces que lo necesites hasta que tu pronunciación se parezca a la del inglés nativo') ?>.
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Escribe?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Para completar la sección Escribe debes escribir todas las frases') ?>.
                    </p>
                    <ol>
                        <li><?php echo Yii::t('mainApp', 'Haz en cada recuadro') ?>.</li>
                        <li><?php echo Yii::t('mainApp', 'Escucha la voz del nativo') ?>.</li>
                        <li><?php echo Yii::t('mainApp', 'Escribe la frase correspondiente') ?>.</li>
                    </ol>
                    <p>
                        <?php echo Yii::t('mainApp', 'Si escribes la frase correctamente, esta aparecerá en verde y ya podrás pasar a la siguiente, pero si la frase que escribes no es correcta, el recuadro aparecerá en rojo: la frase se reproduce de nuevo y el cursor se coloca en el lugar de la frase donde está el error') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'Siempre puedes consultar la respuesta correcta haciendo en el botón "?"') ?>.
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Interpreta?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Para completar la sección debes interpretar a todos los personajes.') ?>
                    </p>
                    <ol>
                        <li><?php echo Yii::t('mainApp', 'Elige un personaje') ?>.</li>
                        <li><?php echo Yii::t('mainApp', 'Haz clic en "comenzar la interpretación" cuando estés preparado') ?>.</li>
                        <li><?php echo Yii::t('mainApp', 'Graba asumiendo el papel del personaje apretando en el botón de grabar y otra vez cuando hayas terminado la frase') ?>.</li>
                    </ol>
                    <p>
                        <?php echo Yii::t('mainApp', 'Al finalizar el diálogo, podrás escuchar toda la conversación apretando en el botón de "escuchar la interpretación"') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'Una vez interpretado a un personaje, debes interpretar también al otro personaje volviendo al inicio de la sección') ?>.
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Videoclase?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Para completar la sección Videoclase debes ver el vídeo') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'También puedes leer toda la explicación gramatical en las siguientes páginas de esta sección') ?>.
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Ejercicios?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Para completar la sección Ejercicios debes realizar todos los ejercicios') ?>.
                    </p>
                    <ol>
                        <li><?php echo Yii::t('mainApp', 'Haz clic en el espacio de cada ejercicio') ?></li>
                        <li><?php echo Yii::t('mainApp', 'Escribe la solución correspondiente') ?></li>
                    </ol>
                    <p>
                        <?php echo Yii::t('mainApp', 'Si escribes la frase correctamente, esta aparecerá en verde y ya podrás pasar a la siguiente, pero si la frase que escribes no es correcta, el recuadro aparecerá en rojo: la frase se reproduce de nuevo y el cursor se coloca en el lugar de la frase donde está el error') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'Siempre puedes consultar la respuesta correcta haciendo en el botón "?"') ?>.
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Vocabulario?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Para completar la sección deberás hacer el ejercicio Escucha-Graba-Compara (LRC®) con al menos el 50% de todas las palabras') ?>.
                    </p>
                    <ol>
                        <li><?php echo Yii::t('mainApp', 'Haz clic en cada frase en inglés') ?></li>
                        <li><?php echo Yii::t('mainApp', 'Haz clic en el botón de grabar para grabar la frase y otra vez cuando hayas acabado') ?></li>
                        <li><?php echo Yii::t('mainApp', 'Compara la frase que has grabado con la voz nativa') ?>.</li>
                    </ol>
                    <p>
                        <?php echo Yii::t('mainApp', 'De todas formas, te recomendamos realizar este ejercicio con todas las palabras') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', '¡El contador te mostrará las palabras que ya has aprendido!') ?>
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo debo realizar la sección Evaluación?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Puedes realizar esta sección sólo si has completado el 100% de las primeras siete secciones de la unidad') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'Haz clic en "Comenzar el Test" para empezar la prueba. Al final del test, puedes comprobar tu puntuación haciendo clic en "VER RESULTADO"') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'Para superar el test, debes obtener una puntuación superior a 8/10. Si no obtienes este resultado, debes repetir la prueba') ?>.
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Qué es la gramática interactiva?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Con la Gramática Interactiva puedes aprender y practicar a la vez. Es una guía útil que te ayudará durante el proceso de aprendizaje a entender de forma amena toda la gramática inglesa') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'Además, gracias a nuestra tecnología Escucha-Graba-Compara (LRC) podrás aprender a pronunciar correctamente todas las palabras') ?>.
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Qué indica la barra de progreso de las unidades?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Al acceder a la unidad y debajo del título de cada sección encontrarás una barra de progreso que refleja tu avance en cada una de las 7 secciones. Este avance se calcula en base a lo que hayas realizado en cada sección') ?>.
                    </p>
                    <p>
                        <u>ABA Film</u>: <?php echo Yii::t('mainApp', 'se considera que esta sección está completada cuando se visualiza el ABA Film dos veces') ?>.
                    </p>
                    <p>
                        <u><?php echo Yii::t('mainApp', 'Habla') ?></u>: <?php echo Yii::t('mainApp', 'se considera que esta sección está completada cuando hayas realizado el ejercicio Escucha-Graba-Compara con el 50% de todas las frases') ?>.
                    </p>
                    <p>
                        <u><?php echo Yii::t('mainApp', 'Escribe') ?></u>: <?php echo Yii::t('mainApp', 'se considera que esta sección está completada cuando hayas completado todas las frases correctamente (deben estar en verde)') ?>.
                    </p>
                    <p>
                        <u><?php echo Yii::t('mainApp', 'Interpreta') ?></u>: <?php echo Yii::t('mainApp', 'se considera que esta sección está completada cuando hayas interpretado todas las frases de todos los personajes') ?>.
                    </p>
                    <p>
                        <u><?php echo Yii::t('mainApp', 'Videoclase') ?></u>: <?php echo Yii::t('mainApp', 'se considera que esta sección está completada cuando hayas visualizado la videoclase una vez') ?>.
                    </p>
                    <p>
                        <u><?php echo Yii::t('mainApp', 'Ejercicios') ?></u>: <?php echo Yii::t('mainApp', 'se considera que esta sección está completada cuando hayas realizado todos los ejercicios de la sección correctamente (es decir, todos los ejercicios tienen que estar en verde)') ?>.
                    </p>
                    <p>
                        <u><?php echo Yii::t('mainApp', 'Vocabulario') ?></u>: <?php echo Yii::t('mainApp', 'se considera que esta sección está completada cuando hayas realizado el ejercicio Escucha-Graba-Compara con el 50% de todas las palabras') ?>.
                    </p>
                    <p>
                        <u><?php echo Yii::t('mainApp', 'Evaluación') ?></u>: <?php echo Yii::t('mainApp', 'se considera que esta sección esta completada cuando hayas obtenido un resultado de 8/10 o más') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'La barra de progreso total en la unidad que se encuentra en la parte superior se basa en la suma del promedio de las 7 primeras secciones') ?>.
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'Puedes ver información más detallada sobre tu progreso si accedes al apartado "mi progreso" dentro de "mi cuenta"') ?>.
                    </p>
                </div>
            </li>
        </ol>
    </div>
</div>
<div class="clear"></div>