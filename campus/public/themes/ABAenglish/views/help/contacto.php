<div class="contenedorAyuda left">
    <div class="tituloSeccionCampus"><?php echo Yii::t('mainApp', 'Ayuda') ?></div>
    <?php $this->renderPartial('menu'); ?>
    <div class="bodyAyuda left">
        <p><?php echo Yii::t('mainApp', 'Contacto') ?></p>
        <ol>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo puedo contactar con vosotros?') ?></span>
                <div>
                    <p>
                        <?php echo Yii::t('mainApp', 'Si no encuentras respuesta consultando la sección de ayuda de nuestra web, puedes contactar con nosotros enviando un mail a:') ?>
                    </p>
                    <p>
                        <a href="mailto:support@abaenglish.com">support@abaenglish.com</a><?php echo Yii::t('mainApp', ' si se trata de una consulta técnica') ?>
                    </p>
                    <p>
                        <a  target="_blank" href="https://abaenglish.zendesk.com/hc/en-gb/requests/new?ticket_form_id=191998"><?php echo Yii::t('mainApp', 'Ayuda'); ?></a><?php echo Yii::t('mainApp', ' si se trata de una consulta relacionada con pagos o tipo de suscripción') ?>
                    </p>
                    <p>
                        <a href="mailto:info@abaenglish.com">info@abaenglish.com</a><?php echo Yii::t('mainApp', ' si deseas información sobre el curso') ?>
                    </p>
                    <p>
                        <?php echo Yii::t('mainApp', 'Si lo prefieres puedes llamarnos al 902 024 386 (personalizar en cada país) en horario de 9-14 y de 15-19h de lunes a viernes o enviarnos un mail con tu teléfono y te llamaremos') ?>.
                    </p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Puedo pasar por vuestras oficinas?') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'Claro, estaremos encantados de conocerte. Puedes encontrarnos en: ') ?><a href="<?php echo Yii::app()->createUrl("contact/index") ?>"><?php echo Yii::t('mainApp', 'Contacto') ?></a></p>
                </div>
            </li>
        </ol>
    </div>
</div>
<div class="clear"></div>