<div class="contenedorAyuda left">
    <div class="tituloSeccionCampus"><?php echo Yii::t('mainApp', 'Preguntas_frecuentes') ?></div>
    <div class="bodyAyuda left">
        <div class="help_dropdwon">
            <span class="arrow_opened"></span>
            <p class="faq_category"><?php echo Yii::t('mainApp', 'titulo1') ?></p>
        <ol>
            <li>
                <span><?php echo Yii::t('mainApp', 'topTitle_001') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'topDescription_001') ?></p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', 'topTitle_002') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'topDescription_002') ?></p>
                 </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', 'topTitle_003') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'topDescription_003') ?></p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', 'topTitle_004') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'topDescription_004') ?></p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', 'topTitle_005') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'topDescription_005') ?></p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', 'topTitle_006') ?>.</span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'topDescription_006') ?></p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', 'topTitle_007') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'topDescription_007') ?></p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', 'topTitle_008') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'topDescription_008') ?></p>
                </div>
            </li>
        </ol>
        </div>
        <div id="subject_1" class="help_dropdwon  closed">
        <span class="arrow_closed"></span>
        <p class="faq_category"><a name="menu1"></a><?php echo Yii::t('mainApp', 'titulo2') ?></p>
        <ol>
            <li>
                <span><a name="help1"></a><?php echo Yii::t('mainApp', 'title_001') ?></span>
                <div id="question_1">
                    <p>
                        <?php echo Yii::t('mainApp', 'description_001') ?>
                    </p>
                </div>
            </li>
            <li>
                <span><a name="help2"></a><?php echo Yii::t('mainApp', 'title_002') ?></span>
                <div id="question_2">
                    <p>
                        <?php echo Yii::t('mainApp', 'description_002') ?>
                    </p>
                </div>
            </li>
            <li>
                <span><a name="help3"></a><?php echo Yii::t('mainApp', 'title_003') ?></span>
                <div id="question_3">
                    <p>
                        <?php echo Yii::t('mainApp', 'description_003', array('{userEmail}' => Yii::app()->user->getEmail())); ?>
                    </p>
                </div>
            </li>
            <li>
                <span><a name="help4"></a><?php echo Yii::t('mainApp', 'title_004') ?></span>
                <div id="question_4">
                    <p>
                        <?php echo Yii::t('mainApp', 'description_004', array('{userEmail}' => Yii::app()->user->getEmail())); ?>
                    </p>
                </div>
            </li>
            <li>
                <span><a name="help5"></a><?php echo Yii::t('mainApp', 'title_005') ?></span>
                <div id="question_5">
                    <p>
                        <?php echo Yii::t('mainApp', 'description_005', array('{userEmail}' => Yii::app()->user->getEmail())) ?>
                    </p>
                </div>
            </li>
            <li>
                <span><a name="help6"></a><?php echo Yii::t('mainApp', 'title_006') ?></span>
                <div id="question_6">
                    <p>
                        <?php echo Yii::t('mainApp', 'description_006', array('{userEmail}' => Yii::app()->user->getEmail())); ?>
                    </p>
                </div>
            </li>
            <li>
                <span><a name="help7"></a><?php echo Yii::t('mainApp', 'title_007') ?></span>
                <div id="question_7">
                    <p>
                        <?php echo Yii::t('mainApp', 'description_007', array('{userEmail}' => Yii::app()->user->getEmail())); ?>
                    </p>
                </div>
            </li>
            <li>
                <span><a name="help8"></a><?php echo Yii::t('mainApp', 'title_008') ?></span>
                <div id="question_8">
                    <p>
                        <?php echo Yii::t('mainApp', 'description_008', array('{userEmail}' => Yii::app()->user->getEmail())); ?>
                    </p>
                </div>
            </li>
            <li>
                <span><a name="help9"></a><?php echo Yii::t('mainApp', 'title_009') ?></span>
                <div id="question_9">
                    <p>
                        <?php echo Yii::t('mainApp', 'description_009', array('{userEmail}' => Yii::app()->user->getEmail())); ?>
                    </p>
                </div>
            </li>
            <li>
                <span><a name="help10"></a><?php echo Yii::t('mainApp', 'title_010') ?></span>
                <div id="question_10">
                    <p>
                        <?php echo Yii::t('mainApp', 'description_010', array('{userEmail}' => Yii::app()->user->getEmail())); ?>
                    </p>
                </div>
            </li>
        </ol>
        </div>
        <div id="subject_2" class="help_dropdwon closed">
            <span class="arrow_closed"></span>
        <p class="faq_category"><a name="menu2"></a><?php echo Yii::t('mainApp', 'titulo3') ?></p>
        <ol start="11">
            <li>
                <span><a name="help11"></a><?php echo Yii::t('mainApp', 'title_011') ?></span>
                <div id="question_11">
                    <p><?php echo Yii::t('mainApp', 'description_011') ?></p>
               </div>
            </li>
            <li>
                <span><a name="help12"></a><?php echo Yii::t('mainApp', 'title_012') ?></span>
                <div id="question_12">
                    <p><?php echo Yii::t('mainApp', 'description_012') ?></p>
                </div>
            </li>
            <li>
                <span><a name="help13"></a><?php echo Yii::t('mainApp', 'title_013') ?></span>
                <div id="question_13">
                    <p>
                        <?php echo Yii::t('mainApp', 'description_013') ?>
                    </p>
                </div>
            </li>
        </ol>
        </div>
        <div id="subject_3" class="help_dropdwon closed">
        <span class="arrow_closed"></span>
        <p class="faq_category"><a name="menu3"></a><?php echo Yii::t('mainApp', 'titulo4') ?></p>
        <ol start="14">
        <li>
            <span><a name="help14"></a><?php echo Yii::t('mainApp', 'title_014') ?></span>
            <div id="question_14">
                <p>
                    <?php echo Yii::t('mainApp', 'description_014') ?>
                </p>
            </div>
        </li>
        <li>
            <span><a name="help15"></a><?php echo Yii::t('mainApp', 'title_015') ?></span>
            <div id="question_15">
                <p>
                    <?php echo Yii::t('mainApp', 'description_015') ?>
                </p>
            </div>
        </li>
        <li>
            <span><a name="help16"></a><?php echo Yii::t('mainApp', 'title_016') ?></span>
            <div id="question_16">
                <p>
                    <?php echo Yii::t('mainApp', 'description_016') ?>
                </p>
            </div>
        </li>
        <li>
            <span><a name="help17"></a><?php echo Yii::t('mainApp', 'title_017') ?></span>
            <div id="question_17">
                <p>
                    <?php echo Yii::t('mainApp', 'description_017') ?>
                </p>
            </div>
        </li>
        </ol>
        </div>
        <div id="subject_4" class="help_dropdwon closed">
            <span class="arrow_closed"></span>
            <p class="faq_category"><a name="menu4"></a><?php echo Yii::t('mainApp', 'titulo5') ?></p>
            <ol start="18">
                <li>
                    <span><a name="help18"></a><?php echo Yii::t('mainApp', 'title_018') ?></span>
                    <div id="question_18">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_018') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help19"></a><?php echo Yii::t('mainApp', 'title_019') ?></span>
                    <div id="question_19">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_019') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help20"></a><?php echo Yii::t('mainApp', 'title_020') ?></span>
                    <div id="question_20">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_020') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help21"></a><?php echo Yii::t('mainApp', 'title_021') ?></span>
                    <div id="question_21">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_021') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help22"></a><?php echo Yii::t('mainApp', 'title_022') ?></span>
                    <div id="question_22">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_022') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help23"></a><?php echo Yii::t('mainApp', 'title_023') ?></span>
                    <div id="question_23">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_023') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help24"></a><?php echo Yii::t('mainApp', 'title_024') ?></span>
                    <div id="question_24">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_024') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help25"></a><?php echo Yii::t('mainApp', 'title_025') ?></span>
                    <div id="question_25">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_025') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help26"></a><?php echo Yii::t('mainApp', 'title_026') ?></span>
                    <div id="question_26">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_026') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help27"></a><?php echo Yii::t('mainApp', 'title_027') ?></span>
                    <div id="question_27">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_027') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help28"></a><?php echo Yii::t('mainApp', 'title_028') ?></span>
                    <div id="question_28">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_028') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help29"></a><?php echo Yii::t('mainApp', 'title_029') ?></span>
                    <div id="question_29">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_029') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help30"></a><?php echo Yii::t('mainApp', 'title_030') ?></span>
                    <div id="question_30">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_030') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help31"></a><?php echo Yii::t('mainApp', 'title_031') ?></span>
                    <div id="question_31">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_031') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help32"></a><?php echo Yii::t('mainApp', 'title_032') ?></span>
                    <div id="question_32">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_032') ?>
                        </p>
                    </div>
                </li>
            </ol>
        </div>
        <div id="subject_5" class="help_dropdwon closed">
            <span class="arrow_closed"></span>
            <p class="faq_category"><a name="menu5"></a><?php echo Yii::t('mainApp', 'titulo6') ?></p>
            <ol start="33">
                <li>
                    <span><a name="help33"></a><?php echo Yii::t('mainApp', 'title_033') ?></span>
                    <div id="question_33">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_033') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help34"></a><?php echo Yii::t('mainApp', 'title_034') ?></span>
                    <div id="question_34">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_034') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help35"></a><?php echo Yii::t('mainApp', 'title_035') ?></span>
                    <div id="question_35">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_035') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help36"></a><?php echo Yii::t('mainApp', 'title_036') ?></span>
                    <div id="question_36">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_036') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help37"></a><?php echo Yii::t('mainApp', 'title_037') ?></span>
                    <div id="question_37">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_037') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help38"></a><?php echo Yii::t('mainApp', 'title_038') ?></span>
                    <div id="question_38">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_038') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help39"></a><?php echo Yii::t('mainApp', 'title_039') ?></span>
                    <div id="question_39">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_039') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help40"></a><?php echo Yii::t('mainApp', 'title_040') ?></span>
                    <div id="question_40">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_040') ?>
                        </p>
                    </div>
                </li>
                <li>
                    <span><a name="help41"></a><?php echo Yii::t('mainApp', 'title_041') ?></span>
                    <div id="question_41">
                        <p>
                            <?php echo Yii::t('mainApp', 'description_041') ?>
                        </p>
                    </div>
                </li>
            </ol>
        </div>
        <div id="subject_6" class="help_dropdwon closed">
        <span class="arrow_closed"></span>
        <p class="faq_category"><a name="menu6"></a><?php echo Yii::t('mainApp', 'titulo7') ?></p>
        <ol start="42">
            <li>
                <span><a name="help42"></a><?php echo Yii::t('mainApp', 'title_042') ?></span>
                <div id="question_42">
                    <p>
                        <?php echo Yii::t('mainApp', 'description_042') ?>
                    </p>
                </div>
            </li>
            <li>
                <span><a name="help43"></a><?php echo Yii::t('mainApp', 'title_043') ?></span>
                <div id="question_43">
                    <p>
                        <?php echo Yii::t('mainApp', 'description_043') ?>
                    </p>
                </div>
            </li>
            <li>
                <span><a name="help44"></a><?php echo Yii::t('mainApp', 'title_044') ?></span>
                <div id="question_44">
                    <p>
                        <?php echo Yii::t('mainApp', 'description_044') ?>
                    </p>
                </div>
            </li>
            <li>
                <span><a name="help45"></a><?php echo Yii::t('mainApp', 'title_045') ?></span>
                <div id="question_45">
                    <p>
                        <?php echo Yii::t('mainApp', 'description_045') ?>
                    </p>
                </div>
            </li>
            <li>
                <span><a name="help46"></a><?php echo Yii::t('mainApp', 'title_046') ?></span>
                <div id="question_46">
                    <p>
                        <?php echo Yii::t('mainApp', 'description_046') ?>
                    </p>
                </div>
            </li>
            <li>
                <span><a name="help47"></a><?php echo Yii::t('mainApp', 'title_047') ?></span>
                <div id="question_47">
                    <p>
                        <?php echo Yii::t('mainApp', 'description_047') ?>
                    </p>
                </div>
            </li>
        </ol>
        </div>
        <div id="subject_7" class="help_dropdwon closed">
        <span class="arrow_closed"></span>
        <p class="faq_category"><a name="menu7"></a><?php echo Yii::t('mainApp', 'titulo8') ?></p>
        <ol start="48">
            <li>
                <span><a name="help48"></a><?php echo Yii::t('mainApp', 'title_048') ?></span>
                <div id="question_48">
                    <p>
                        <?php echo Yii::t('mainApp', 'description_048', array('{userEmail}' => Yii::app()->user->getEmail())); ?>
                    </p>
                </div>
            </li>
            <li>
                <span><a name="help49"></a><?php echo Yii::t('mainApp', 'title_049') ?></span>
                <div id="question_49">
                    <p><?php echo Yii::t('mainApp', 'description_049') ?></p>
                </div>
            </li>
        </ol>
        </div>
    </div>
</div>
<div class="clear"></div>