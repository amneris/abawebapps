<div class="contenedorAyuda left">
    <div class="tituloSeccionCampus"><?php echo Yii::t('mainApp', 'Ayuda') ?></div>
    <?php $this->renderPartial('menu'); ?>
    <div class="bodyAyuda left">
        <p><?php echo Yii::t('mainApp', 'Gramática interactiva') ?></p>
        <ol>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo puedo hacer consultas a mi teacher?') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'Según el plan que tengas contratado podrás disponer de consultas a tu teacher') ?>.</p>
                    <p><?php echo Yii::t('mainApp', 'ABA Free: con este plan gratuito no dispones de consultas al teacher') ?>.</p>
                    <p><?php echo Yii::t('mainApp', 'ABA Premium: con este plan no dispones de consultas al teacher') ?>.</p>
                    <p><?php echo Yii::t('mainApp', 'ABA Forever: si lo deseas, puedes realizar consultas ilimitadas a tu teacher pagando una cuota') ?>.</p>
                    <p><?php echo Yii::t('mainApp', 'Para poder contactarle debes acceder al campus, en el apartado mensajes podrás consultar sus mensajes y hacerle llegar tus consultas') ?>.</p>
                </div>
            </li>
        </ol>
    </div>
</div>
<div class="clear"></div>