<div class="contenedorAyuda left">
    <div class="tituloSeccionCampus"><?php echo Yii::t('mainApp', 'Ayuda') ?></div>
    <?php $this->renderPartial('menu'); ?>
    <div class="bodyAyuda left">
        <p><?php echo Yii::t('mainApp', 'Mi cuenta') ?></p>
        <ol>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo puedo modificar mis datos personales?') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'Puedes modificar tus datos personales, nombre, apellidos y nacionalidad desde el apartado ') ?><a href="<?php echo Yii::app()->createUrl("profile/index") ?>"><?php echo Yii::t('mainApp', 'Mi cuenta') ?></a><?php echo Yii::t('mainApp', ' que encontrarás en el campus') ?>.</p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cómo puedo modificar mi usuario?') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'Si deseas cambiar el usuario con el que accedes al curso, debes remitir un mail a ') ?><a href="mailto:support@abaenglish.com">support@abaenglish.com</a><?php echo Yii::t('mainApp', ' indicándonos el usuario actual y el nuevo usuario, recibirás una respuesta con la confirmación del cambio') ?>.</p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', 'He olvidado mi contraseña, ¿Cómo puedo recuperarla?') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'Si has olvidado tu contraseña debes escribir un mail a ') ?><a href="mailto:support@abaenglish.com">support@abaenglish.com</a><?php echo Yii::t('mainApp', ' indicándonos el mail que utilizas como usuario del curso') ?>.</p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Puedo modificar la lengua del curso?') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'Si deseas cambiar la lengua del curso en las traducciones y explicaciones, debes remitir un mail a ') ?><a href="mailto:support@abaenglish.com">support@abaenglish.com</a><?php echo Yii::t('mainApp', ' indicando en que idioma quieres ver las traducciones del curso') ?>.</p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Puedo conectarme desde cualquier lugar?') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'Sí, puedes conectarte desde cualquier lugar, solo necesitas acceder a internet e introducir tu usuario y contraseña') ?>.</p>
                </div>
            </li>
            <li>
                <span><?php echo Yii::t('mainApp', '¿Cuántas personas pueden realizar el curso con un usuario?') ?></span>
                <div>
                    <p><?php echo Yii::t('mainApp', 'Cada usuario es válido para un sólo alumno') ?>.</p>
                </div>
            </li>
        </ol>
    </div>
</div>
<div class="clear"></div>