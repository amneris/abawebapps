<?php
use appCampus\models\leveltest\LevelTestQuestions;
?>

<div class="modal-test">

    <div class="titleLevelTest">
        Level test!
    </div>
    <div aria-valuenow="<?php echo $this->getSessionVar('curProgress'); ?>" aria-valuemax="100" aria-valuemin="0"
         role="progressbar" class="ui-progressbar ui-widget ui-widget-content ui-corner-all" id="progress_bar" style="margin: 0 auto;">
        <div style="width: <?php echo $this->getSessionVar('curProgress'); ?>%;"
             class="ui-progressbar-value ui-corner-left ui-widget-header2"></div>
    </div>
    <div><br></div>

    <div class="divTestOptions">
        <?php if ($mQuestion->type == LevelTestQuestions::_TYPE_TEXT) { ?>
            <div class="questionLevelTest">
                <?php echo $this->getSessionVar('curQuestion') . '.- ' . Yii::t('leveltest', $mQuestion->question); ?>
            </div>
        <?php } else {
            if ($mQuestion->type == LevelTestQuestions::_TYPE_AUDIO) { ?>
                <div class="questionLevelTest">
                    <?php echo $this->getSessionVar('curQuestion') . '.- ' ?>
                    <object type="application/x-shockwave-flash"
                            data="<?php echo getenv('CAMPUS_URL'); ?>/themes/ABAenglish/media/dewplayer/dewplayer.swf"
                            width="200" height="20" id="dewplayer" name="dewplayer">
                        <param name="wmode" value="transparent"/>
                        <param name="movie" value="dewplayer.swf"/>
                        <param name="flashvars"
                               value="mp3=<?php echo getenv('CAMPUS_URL'); ?>/themes/ABAenglish/media/leveltest/<?php echo 'audio_' . $mQuestion->id . '.mp3' ?>&amp;showtime=1"/>
                    </object>
                </div>
            <?php }
        } ?>

        <ul class="welcomeLevelTest">
            <li>
                <div class="inner-content">
                    <input id="Opcion1" type="radio" name="radioTestOptions" value="1">
                    <label2 class="answerLevelTest levelLabel" for="Opcion1">
                        <span><?php echo Yii::t( 'leveltest',  $mQuestion->option1 ); ?></span>
                    </label2>

                </div>
            </li>
            <li>
                <div class="inner-content">
                    <input id="Opcion2" type="radio" name="radioTestOptions" value="2">
                    <label2 class="answerLevelTest levelLabel" for="Opcion2">
                        <span><?php echo Yii::t( 'leveltest',  $mQuestion->option2 ); ?></span>
                    </label2>

                </div>
            </li>
            <li>
                <div class="inner-content">
                    <input id="Opcion3" type="radio" name="radioTestOptions" value="3">
                    <label2 class="answerLevelTest levelLabel" for="Opcion3">
                        <span><?php echo Yii::t( 'leveltest',  $mQuestion->option3 ); ?></span>
                    </label2>

                </div>
            </li>
        </ul>

        <div class="centerLevelTest">
            <a title="Realizar el test" href="#" id="form_submit" class="abaButtonLevelTest" onclick="abaLeveltest.submit(<?php echo CJavaScript::encode( $this->getSessionVar('code') ); ?>, <?php echo CJavaScript::encode( getenv('CAMPUS_URL') ); ?>, <?php echo CJavaScript::encode( $this->getSessionVar('isoLanguage') ); ?>); return false;">Go <span></span></a>
        </div>

    </div>
</div>