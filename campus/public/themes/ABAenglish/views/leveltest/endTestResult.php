<div id="CampusModals">
    <div class="titleLevelTest"><?php echo $testResultString . ' ' . $levelTitle; ?></div>
    <div class="containerAnalysis">
        <img class="imgAnalysis" src="/themes/ABAenglish/media/images/iconFinalTest.png"/>

        <div>
            <div class="titleAnalysis"><?php echo $analysisString; ?></div>
            <div class="descriptionAnalysis"><?php echo $analysis; ?></div>
        </div>
    </div>
    <div class="textLevelTest">
        <?php echo $objectives; ?>
    </div>
    <div class="text-align-center">
        <form id="finishTest" action="<?php echo $url; ?>">
            <button type="submit" class="registrerSendButton margin-bottom20 width290">
                <?php echo Yii::t('mainApp', 'Continuar'); ?>
            </button>
        </form>
    </div>
</div>
