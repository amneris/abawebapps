<div id="CampusModals">
    <div class="titleLevelTest"><?php echo $testResultString . ' ' . $levelTitle; ?></div>
    <div class="containerAnalysis">
        <img class="imgAnalysis" src="/themes/ABAenglish/media/images/iconFinalTest.png"/>

        <div>
            <div class="titleAnalysis"><?php echo $analysisString; ?></div>
            <div class="descriptionAnalysis"><?php echo $analysis; ?></div>
        </div>
    </div>
    <div></br></div>
    <div class="text-align-center">
        <button type="button" class="registrerSendButton margin-bottom20 width290" onclick="abaLeveltest.close()">
            <?php echo Yii::t('mainApp', 'level-test-demoFinishBtn'); ?>
        </button>

    </div>
</div>
