<?php
$this->widget('application.components.widgets.BreadcrumbHeader', array('curMenuSectionTab' => "ABAmoments", 'uppercase' => false));
?>

<div id="abaMoments-container">
    <div id="abaMoments-loading" class="aba-loading">
        <div class="loading-container">
            <div class="loading-spinner">
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function loadAbaMomentsIFrame() {
      var container = document.getElementById('abaMoments-container');
      var iframe = document.createElement('iframe');

      iframe.src = '<?= $abaMomentsUrl ?>';
      iframe.id = 'abamoments-container';
      iframe.height = '0';
      iframe.width = '100%';

      iframe.onload = function() {
        var loader = document.getElementById('abaMoments-loading');
        container.removeChild(loader);

        iframe.height = '730px';
      }

      container.appendChild(iframe);
    }

    loadAbaMomentsIFrame();
</script>

