<?php
///yahoo api code
if((!$importsucess))
{
    require dirname(__FILE__).'/includes/lib/Yahoo.inc';
    session_save_path('/tmp/');

    $yahoohasSession = YahooSession::hasSession(MGM_YAHOO_CONSUMER_KEY, MGM_YAHOO_CONSUMER_SECRET, MGM_YAHOO_APP_ID);
    if($yahoohasSession == FALSE)
    {
        // create the callback url,
        if(isset($_GET['back']))
        {$callback = $absoluteUrl."&in_popup";}
        else
        {$callback = $absoluteUrl."?in_popup";}

        // pass the credentials to get an auth url.
        // this URL will be used for the pop-up.
        $yahoo_auth_url = YahooSession::createAuthorizationUrl(MGM_YAHOO_CONSUMER_KEY, MGM_YAHOO_CONSUMER_SECRET, $callback);
    }
    else {
        // pass the credentials to initiate a session
        $yahoo_session = YahooSession::requireSession(MGM_YAHOO_CONSUMER_KEY, MGM_YAHOO_CONSUMER_SECRET, MGM_YAHOO_APP_ID);

        // if the in_popup flag is detected,
        // the pop-up has loaded the callback_url and we can close this window.

        if(array_key_exists("in_popup", $_GET))
        {
            close_popup();
            exit;
        }
    }
}
//end yahoo api

///user IP
$userip=$_SERVER['REMOTE_ADDR'];

if(isset($_POST['act']) && $_POST['act']=="showContacts")  //import from addressbook
{
    if(!isset($_POST['username'])) $_POST['username'] = '';
    if(!isset($_POST['password'])) $_POST['password'] = '';

    ///banned email
    ///service
    if($_POST['service']!='otheremail' && $_POST['service']!='fastmail' && $_POST['service']!='gmx.net' && $_POST['service']!='web.de' &&(strpos($_POST['service'],'additionalemail_')=== false))
    { $userfullemailaddress=trim($_POST['username'])."@".$_POST['service'].".com"; }
    if ($_POST['service']=='fastmail'){$userfullemailaddress=trim($_POST['username'])."@fastmail.fm";}
    if ($_POST['service']=='otheremail') {$userfullemailaddress=trim($_POST['username']);}
    if ($_POST['service']=='gmx.net'){$userfullemailaddress=trim($_POST['username'])."@gmx.net";}
    if ($_POST['service']=='web.de'){$userfullemailaddress=trim($_POST['username'])."@web.de";}

    if(strpos($_POST['service'],'additionalemail_')!== false)
    {
        $webemail_ext_arr=explode("_",$_POST['service']);
        $userfullemailaddress=trim($_POST['username'])."@".$webemail_ext_arr[1];
    }
    //end banned email

    RemoveDir($tempdirpath,false);
    $message="";
    $message2="";
    $message3="";
    $importsucess=false;
    include(dirname(__FILE__)."/includes/curl.inc.php");

    switch ($_POST['service'])
    {
        case "web.de" :
            $only_username=explode("@",$_POST['username']);
            $_POST['username'] = $only_username[0];

            include(dirname(__FILE__)."/includes/webde.inc.php");
            $res=get_contacts($only_username[0]."@".$_POST['service'], $_POST['password']);
            if ($res==false)
            {
                $message=$importermessage1;
            }
            else
            {
                $contacts=$res;
                if(count($contacts)==0)
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2."<br>";
                    $importsucess=true;

                }
            }
            break;

        case "yahoo" :
            $only_username=explode("@",$_POST['username']);
            $_POST['username'] = $only_username[0];
            //$_POST['username'] = @str_replace("@yahoo.com", "", $_POST['username']);
            include(dirname(__FILE__)."/includes/yahoo_api.inc.php");
            $res=yahoologin(trim($_POST['username']), trim($_POST['password']));
            if (count($res) == 0)
            {
                $message=$importermessage1;
            }
            else
            {
                $contacts=$res;
                if(count($contacts)==0)
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2;
                    $importsucess=true;

                }
            }
            break;

        case "ymail" :
            $only_username=explode("@",$_POST['username']);
            $_POST['username'] = $only_username[0];
            //$_POST['username'] = @str_replace("@yahoo.com", "", $_POST['username']);
            include(dirname(__FILE__)."/includes/yahoo_api.inc.php");
            $res=yahoologin(trim($_POST['username']."@ymail.com"), trim($_POST['password']));
            if (stripos($res, 'Invalid ID or password.'))
            {
                $message=$importermessage1;
            }
            else if (stripos($res, 'This ID is not yet taken.'))
            {
                $message=$importermessage1;
            }
            else
            {
                $contacts=$res;
                if(count($contacts)==0)
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2;
                    $importsucess=true;

                }
            }
            break;
        case "rocketmail" :
            $only_username=explode("@",$_POST['username']);
            $_POST['username'] = $only_username[0];
            //$_POST['username'] = @str_replace("@yahoo.com", "", $_POST['username']);
            include(dirname(__FILE__)."/includes/yahoo_api.inc.php");
            $res=yahoologin(trim($_POST['username']."@rocketmail.com"), trim($_POST['password']));
            if (stripos($res, 'Invalid ID or password.'))
            {
                $message=$importermessage1;
            }
            else if (stripos($res, 'This ID is not yet taken.'))
            {
                $message=$importermessage1;
            }
            else
            {
                $contacts=$res;
                if(count($contacts)==0)
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2;
                    $importsucess=true;

                }
            }
            break;
        case "aol" :
            //$_POST['username'] = @str_replace("@aol.com", "", $_POST['username']);
            $only_username=explode("@",$_POST['username']);
            $_POST['username'] = $only_username[0];

            require(dirname(__FILE__)."/includes/aol.inc.php");
            $res=getContactList(trim($_POST['username']), trim($_POST['password']));
            if(count($res) == 0)
            {
                $message=$importermessage1;
            }
            else
            {
                $contacts=$res;
                if(count($contacts)==0)
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2;
                    $importsucess=true;

                }
            }
            break;
        case "hotmail" :
            //$only_username=explode("@",$_POST['username']);
            //$_POST['username'] = $only_username[0];

            //$_POST['username'] = @str_replace("@hotmail.com", "", $_POST['username']);
            include(dirname(__FILE__)."/includes/hotmail_api.inc.php");
            //$res=hotmail_login(trim($_POST['username'])."@hotmail.com", trim($_POST['password']));
            $res=hotmail_login(trim($_POST['username']), trim($_POST['password']));
            if (in_array('The email address or password is incorrect.', $res))
            {
                $message=$importermessage1;
            }
            else
            {
                $contacts=$res;
                if(empty($contacts) || (!is_array($contacts) && trim($contacts[0])=='T'&&trim($contacts[1])=='h' &&trim($contacts[2])=='e' &&trim($contacts[4])=='e'))
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2;
                    $importsucess=true;

                }
            }
            break;
        case "msn" :
            $only_username=explode("@",$_POST['username']);
            $_POST['username'] = $only_username[0];

            //$_POST['username'] = @str_replace("@msn.com", "", $_POST['username']);
            include(dirname(__FILE__)."/includes/msn_api.inc.php");
            $res=hotmail_login(trim($_POST['username'])."@msn.com", trim($_POST['password']));

            if (in_array('The email address or password is incorrect.', $res))
            {
                $message=$importermessage1;
            }
            else
            {
                $contacts=$res;
                if(empty($contacts) || (trim($contacts[0])=='T'&&trim($contacts[1])=='h' &&trim($contacts[2])=='e' &&trim($contacts[4])=='e'))
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2;
                    $importsucess=true;

                }
            }
            break;
        case "gmail" :
            //$_POST['username'] = @str_replace("@gmail.com", "", $_POST['username']);
            $only_username=explode("@",$_POST['username']);
            $_POST['username'] = $only_username[0];

            include(dirname(__FILE__)."/includes/gmail_api.inc.php");
            $login = trim($_POST['username']);
            $password = trim($_POST['password']);
            $contacts=get_contacts($login, $password);

            if(is_array($contacts))
            {
                $message2=count($contacts)." ".$importermessage2;
                $importsucess=true;

            }
            else
            {
                switch ($contacts)
                {
                    case 1: #invalid login
                        $message = $importermessage1;
                        break;
                    case 2: #empty username or password
                        $message = $importermessage1;
                        break;
                }
            }
            break;

        case "googlemail" :
            $only_username=explode("@",$_POST['username']);
            $_POST['username'] = $only_username[0];

            include(dirname(__FILE__)."/includes/gmail_api.inc.php");
            $login = trim($_POST['username']);
            $password = trim($_POST['password']);
            $contacts=get_contacts($login, $password);
            if(is_array($contacts))
            {
                $message2=count($contacts)." ".$importermessage2;
                $importsucess=true;

            }
            else
            {
                switch ($contacts)
                {
                    case 1: #invalid login
                        $message = $importermessage1;
                        break;
                    case 2: #empty username or password
                        $message = $importermessage1;
                        break;
                }
            }
            break;

        case "lycos" :
            //$_POST['username'] = @str_replace("@lycos.com", "", $_POST['username']);
            $only_username=explode("@",$_POST['username']);
            $_POST['username'] = $only_username[0];

            include(dirname(__FILE__)."/includes/lycos.inc.php");
            $res=getContactList(trim($_POST['username']), trim($_POST['password']));
            if(count($res) == 0)
            {
                $message=$importermessage1;
            }
            else
            {
                $contacts=$res;
                if(count($contacts)==0)
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2;
                    $importsucess=true;

                }
            }
            break;

        case "maildotcom" :
            //$_POST['username'] = @str_replace("@mail.com", "", $_POST['username']);
            $only_username=explode("@",$_POST['username']);
            $_POST['username'] = $only_username[0];

            $_POST['service']="mail";
            include(dirname(__FILE__)."/includes/mail.inc.php");
            $res=getContactList(trim($_POST['username']), trim($_POST['password']));
            if(count($res) == 0)
            {
                $message=$importermessage1;
            }
            else
            {
                $contacts=$res;
                if(count($contacts)==0)
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2;
                    $importsucess=true;

                }
            }
            break;

        case "live" :
            //$_POST['username'] = @str_replace("@live.com", "", $_POST['username']);
            $only_username=explode("@",$_POST['username']);
            $_POST['username'] = $only_username[0];

            include(dirname(__FILE__)."/includes/hotmail_api.inc.php");
            $res=hotmail_login(trim($_POST['username'])."@live.com", trim($_POST['password']));

            if (in_array('The email address or password is incorrect.', $res))
            {
                $message=$importermessage1;
            }
            else
            {
                $contacts=$res;
                if(empty($contacts) || (trim($contacts[0])=='T'&&trim($contacts[1])=='h' &&trim($contacts[2])=='e' &&trim($contacts[4])=='e'))
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2;
                    $importsucess=true;

                }
            }
            break;
        ///
        case "icqmail" :
            //$_POST['username'] = @str_replace("@icqmail.com", "", $_POST['username']);
            $only_username=explode("@",$_POST['username']);
            $_POST['username'] = $only_username[0];

            require("includes/icq.inc.php");
            $res=getContactList_icq(trim($_POST['username'])."@icqmail.com", trim($_POST['password']));
            if (count($res) == 0)
            {
                $message=$importermessage1;
            }
            else
            {
                $contacts=$res;
                if(count($res)==0)
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($res)." ".$importermessage2;
                    $importsucess=true;

                }
            }
            break;
        ///
        case  "rediffmail":
            //$_POST['username'] = @str_replace("@rediffmail.com", "", $_POST['username']);
            $only_username=explode("@",$_POST['username']);
            $_POST['username'] = $only_username[0];
            include(dirname(__FILE__)."/includes/rediffmail.inc.php");
            $res=getContactList_rediff(trim($_POST['username']), trim($_POST['password']));
            if ($res==-1)
            {
                $message=$importermessage1;
                $importsucess=false;
            }
            else
            {
                $contacts=$res;
                if(count($contacts)==0)
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2;
                    $importsucess=true;

                }
            }
            break;
        case  "fastmail" :
            //$_POST['username'] = @str_replace("@fastmail.fm", "", $_POST['username']);
            $only_username=explode("@",$_POST['username']);
            $_POST['username'] = $only_username[0];

            include(dirname(__FILE__)."/includes/fastmail.inc.php");
            $res=getContactList_fastmail(trim($_POST['username'])."@fastmail.fm", trim($_POST['password']));
            if ($res==-1)
            {
                $message=$importermessage1;
                $importsucess=false;
            }
            else
            {
                $contacts=$res;
                if(count($contacts)==0)
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2;
                    $importsucess=true;

                }
            }
            break;

        case "gmx.net" :
            //$_POST['username'] = @str_replace("@gmx.net", "", $_POST['username']);
            $only_username=explode("@",$_POST['username']);
            $_POST['username'] = $only_username[0];

            include(dirname(__FILE__)."/includes/gmx.inc.php");
            $login = trim($_POST['username'])."@gmx.net";
            $password = trim($_POST['password']);
            $contacts=get_contacts($login, $password);
            if(is_array($contacts))
            {
                $message2=count($contacts)." ".$importermessage2;
                $importsucess=true;

            }
            else
            {
                switch ($contacts)
                {
                    case 1: #invalid login
                        $message = $importermessage1;
                        break;
                    case 2: #empty username or password
                        $message = $importermessage1;
                        break;
                }
            }
            break;
        ////
    }

    ////new code ->additional emails
    if(strpos($_POST['service'],'additionalemail_')!== false)
    {

        $webemail_ext_arr=explode("_",$_POST['service']);
        $useremailaddress=trim($_POST['username'])."@".$webemail_ext_arr[1];


        if(strpos($webemail_ext_arr[1],"gmx")!== false) //gmx
        {

            include(dirname(__FILE__)."/includes/gmx.inc.php");
            $login = trim($useremailaddress);
            $password = trim($_POST['password']);
            $contacts=get_contacts($login, $password);
            if(is_array($contacts))
            {
                $message2=count($contacts)." ".$importermessage2;
                $importsucess=true;

            }
            else
            {
                switch ($contacts)
                {
                    case 1: #invalid login
                        $message = $importermessage1;
                        break;
                    case 2: #empty username or password
                        $message = $importermessage1;
                        break;
                }
            }
        }

        if(strpos($webemail_ext_arr[1],"yahoo")!== false) //yahoo
        {

            include(dirname(__FILE__)."/includes/yahoo_api.inc.php");
            $res=yahoologin($_POST['username'], $_POST['password']);
            if ($res==false)
            {
                $message=$importermessage1;
            }
            else
            {
                $contacts=$res;
                if(count($contacts)==0)
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2;
                    $importsucess=true;

                }
            }
        }
        if(strpos($webemail_ext_arr[1],"ymail")!== false) //ymail
        {
            include(dirname(__FILE__)."/includes/yahoo_api.inc.php");
            $res=yahoologin($_POST['username'], $_POST['password']);
            if ($res==false)
            {
                $message=$importermessage1;
            }
            else
            {
                $contacts=$res;
                if(count($contacts)==0)
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2;
                    $importsucess=true;

                }
            }
        }


        if(strpos($webemail_ext_arr[1],"rocketmail")!== false) //rocketmail
        {
            include(dirname(__FILE__)."/includes/yahoo_api.inc.php");
            $res=yahoologin($_POST['username'], $_POST['password']);
            if ($res==false)
            {
                $message=$importermessage1;
            }
            else
            {
                $contacts=$res;
                if(count($contacts)==0)
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2;
                    $importsucess=true;

                }
            }
        }

        if(strpos($webemail_ext_arr[1],"aol")!== false) //aol
        {
            require("includes/aol.inc.php");
            $res=getContactList(trim($_POST['username']), $_POST['password']);
            if (stripos($res, 'Invalid Screen Name or Password.'))
            {
                $message=$importermessage1;
            }
            else
            {
                $contacts=$res;
                if(count($contacts)==0)
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2;
                    $importsucess=true;

                }
            }
        }

        if(strpos($webemail_ext_arr[1],"icqmail")!== false) //icqmail
        {
            require("includes/icq.inc.php");
            $res=getContactList_icq($useremailaddress, $_POST['password']);
            if (stripos($res, 'Invalid Screen Name or Password.'))
            {
                $message=$importermessage1;
            }
            else
            {
                $contacts=$res;
                if(count($res)==0)
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($res)." ".$importermessage2;
                    $importsucess=true;

                }
            }
        }

        if(strpos($webemail_ext_arr[1],"hotmail")!== false) //hotmail
        {
            include(dirname(__FILE__)."/includes/hotmail_api.inc.php");
            $res=hotmail_login($useremailaddress, $_POST['password']);

            if (in_array('The email address or password is incorrect.', $res))
            {
                $message=$importermessage1;
            }
            else
            {
                $contacts=$res;
                if(empty($contacts) || (trim($contacts[0])=='T'&&trim($contacts[1])=='h' &&trim($contacts[2])=='e' &&trim($contacts[4])=='e'))
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2;
                    $importsucess=true;

                }
            }
        }

        if(strpos($webemail_ext_arr[1],"msn")!== false) //msn
        {
            include(dirname(__FILE__)."/includes/msn_api.inc.php");
            $res=hotmail_login($useremailaddress, $_POST['password']);

            if (in_array('The email address or password is incorrect.', $res))
            {
                $message=$importermessage1;
            }
            else
            {
                $contacts=$res;
                if(empty($contacts) || (trim($contacts[0])=='T'&&trim($contacts[1])=='h' &&trim($contacts[2])=='e' &&trim($contacts[4])=='e'))
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2;
                    $importsucess=true;

                }
            }
        }

        if(strpos($webemail_ext_arr[1],"live")!== false) //live
        {
            include(dirname(__FILE__)."/includes/hotmail_api.inc.php");
            $res=hotmail_login($useremailaddress, $_POST['password']);

            if (in_array('The email address or password is incorrect.', $res))
            {
                $message=$importermessage1;
            }
            else
            {
                $contacts=$res;
                if(empty($contacts) || (trim($contacts[0])=='T'&&trim($contacts[1])=='h' &&trim($contacts[2])=='e' &&trim($contacts[4])=='e'))
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2;
                    $importsucess=true;

                }
            }
        }

        if(strpos($webemail_ext_arr[1],"gmail")!== false) //gmail
        {
            include(dirname(__FILE__)."/includes/gmail_api.inc.php");
            $login = $useremailaddress;
            $password = $_POST['password'];
            $contacts=get_contacts($login, $password);
            if(is_array($contacts))
            {
                $message2=count($contacts)." ".$importermessage2;
                $importsucess=true;

            }
            else
            {
                switch ($contacts)
                {
                    case 1: #invalid login
                        $message = $importermessage1;
                        break;
                    case 2: #empty username or password
                        $message = $importermessage1;
                        break;
                }
            }
        }

        if(strpos($webemail_ext_arr[1],"lycos")!== false) //lycos
        {
            include(dirname(__FILE__)."/includes/lycos.inc.php");
            $res=getContactList($_POST['username'], $_POST['password']);
            if (in_array('The email address or password is incorrect.', $res))
            {
                $message=$importermessage1;
            }
            else
            {
                $contacts=$res;
                if(count($contacts)==0)
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2;
                    $importsucess=true;

                }
            }
        }


        if(strpos($webemail_ext_arr[1],"rediffmail")!== false) //lycos
        {
            include(dirname(__FILE__)."/includes/rediffmail.inc.php");
            $res=getContactList_rediff($_POST['username'], $_POST['password']);
            if ($res==-1)
            {
                $message=$importermessage1;
                $importsucess=false;
            }
            else
            {
                $contacts=$res;
                if(count($contacts)==0)
                {
                    $message=$importermessage1;
                }
                else
                {
                    $message2=count($contacts)." ".$importermessage2;
                    $importsucess=true;

                }
            }
        }



    }

    ///end new code for additional

    ////check again contacts
    if(empty($contacts) || count($contacts)==0)
    {
        $importsucess=false;
        $message=$importermessage1;
    }

    ///insert into import here///
    if($importsucess==true)
    {
        if(!isset($_POST['affiliateid'])) $_POST['affiliateid'] = '';

        ///service
        if($_POST['service']!='otheremail' && $_POST['service']!='fastmail' && $_POST['service']!='gmx.net'&& $_POST['service']!='web.de'&&(strpos($_POST['service'],'additionalemail_')=== false))
        { $userfullemailaddress=trim($_POST['username'])."@".$_POST['service'].".com"; }
        if ($_POST['service']=='fastmail'){$userfullemailaddress=trim($_POST['username'])."@fastmail.fm";}
        if ($_POST['service']=='otheremail') {$userfullemailaddress=trim($_POST['username']);}
        if ($_POST['service']=='gmx.net'){$userfullemailaddress=trim($_POST['username'])."@gmx.net";}
        if ($_POST['service']=='web.de'){$userfullemailaddress=trim($_POST['username'])."@web.de";}
        if(strpos($_POST['service'],'additionalemail_')!== false)
        {
            $webemail_ext_arr=explode("_",$_POST['service']);
            $userfullemailaddress=trim($_POST['username'])."@".$webemail_ext_arr[1];
        }
    }
}