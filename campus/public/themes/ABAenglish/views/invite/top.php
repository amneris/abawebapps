<?php
$isfrommail = false;
if(isset($_SERVER['HTTP_REFERER']))
{
    if(stripos('INBOX', strtoupper($_SERVER['HTTP_REFERER'])))
    {
        $isfrommail = true;
    }
    if(!isset($_SESSION['reff']) && $isfrommail=false)
    {
        $_SESSION['reff'] = trim($_SERVER['HTTP_REFERER']);
    }
    if(isset($_SESSION['reff']) && trim($_SESSION['reff']) == '')
    {
        $_SESSION['reff'] = trim($_SERVER['HTTP_REFERER']);
    }
}

$invite_url = Yii::app()->createAbsoluteUrl('invite/invite');
$successUrl = isset($successUrl) ? $successUrl : NULL;
$sharer_link_to_promote = RegisterUsersCommon::getCreateAccountUrl();

include_once("config/scriptvars.php");
include_once("config/textvars.php");
include_once("includes/functions.php");  //script functions

set_time_limit(0);