<?php
$footer_note=Yii::t('mainApp', 'mgm_footer_note');
$privacity=Yii::t('mainApp', 'mgm_privacity');

$handlecontacts = 2;

$toplink1=Yii::t('mainApp', 'mgm_toplink1');
$toplink3=Yii::t('mainApp', 'mgm_toplink3');
$toplink5=Yii::t('mainApp', 'mgm_toplink5');

//invite success
$success_continue=Yii::t('mainApp', 'mgm_success_continue');
$success_finish=Yii::t('mainApp', 'mgm_success_finish');

//importer
$importerformcontrol1=Yii::t('mainApp', 'mgm_importerformcontrol1');
$importerformcontrol2=Yii::t('mainApp', 'mgm_importerformcontrol2');
$importerformcontrol3=Yii::t('mainApp', 'mgm_importerformcontrol3');
$lastoptionservicecombo=Yii::t('mainApp', 'mgm_lastoptionservicecombo');
$importerformcontrol4=Yii::t('mainApp', 'mgm_importerformcontrol4');
$importerformcontrol5=Yii::t('mainApp', 'mgm_importerformcontrol5');
$importerformcontrol6=Yii::t('mainApp', 'mgm_importerformcontrol6');
$importerformcontrol7=Yii::t('mainApp', 'mgm_importerformcontrol7');

$importerjserror4=Yii::t('mainApp', 'mgm_importerjserror4');
$importerjserror5=Yii::t('mainApp', 'mgm_importerjserror5');

$importerheader1=Yii::t('mainApp', 'mgm_importerheader1');
$importerheader2=Yii::t('mainApp', 'mgm_importerheader2');

$importerformcontrol1_ai=Yii::t('mainApp', 'mgm_importerformcontrol1_ai');
$importerformcontrol2_ai=Yii::t('mainApp', 'mgm_importerformcontrol2_ai');

$importermessage1=Yii::t('mainApp', 'mgm_importermessage1');
$importermessage2=Yii::t('mainApp', 'mgm_importermessage2');
$importermessage3=Yii::t('mainApp', 'mgm_importermessage3');

//manual invite
$manualformcontrol4=Yii::t('mainApp', 'mgm_manualformcontrol4');
$manualformcontrol5=Yii::t('mainApp', 'mgm_manualformcontrol5');
$manualformcontrol6=Yii::t('mainApp', 'mgm_manualformcontrol6');
$manualformcontrol9=Yii::t('mainApp', 'mgm_manualformcontrol9');
$manualformcontrol10=Yii::t('mainApp', 'mgm_manualformcontrol10');
$manualjserror2=Yii::t('mainApp', 'mgm_manualjserror2');
$manualjserror3=Yii::t('mainApp', 'mgm_manualjserror3');

//sharer
$sharercontrol5=Yii::t('mainApp', 'mgm_sharercontrol5');
$sharercontrol6=Yii::t('mainApp', 'mgm_sharercontrol6');
$sharercontrol7=Yii::t('mainApp', 'mgm_sharercontrol7');