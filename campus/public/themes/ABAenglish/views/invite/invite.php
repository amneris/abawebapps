<?php
//send invitations emails ->service importer
if(isset($_POST['act']) && $_POST['act']=="sendMessage")
{
    if(trim($_POST['fromemail'])!='')
    {
        $partnerId = 'import';

        if(!isset($_POST['optintomailinglist'])) $_POST['optintomailinglist'] = '';
        if(!isset($_POST['affiliateid'])) $_POST['affiliateid'] = '';

        /////////////////////email
        //sending email to selected contacts ->
        for($i=1;$i<=$_POST['nrcontacts'];$i++)
        {
            if((isset($_POST['address_'.$i])))
            {
                if(!$import_sent) {
                    $import_sent = true;
                }
                array_push($emailsList, $_POST['address_'.$i]);
            }
        }
    }
}

//manual invite here
if(isset($_POST['fromemail']) && (trim($_POST['fromemail']) != '') && isset($_POST['totalnumberemails']) && ($_POST['totalnumberemails']>0) && ($_POST['nrofemails']>0) && ($_POST['act']=='manualinvite'))
{
    $loop=$_POST['nrofemails'];

    ///loop emails here
    for($i=1;$i<=$loop;$i++)
    {
        if(isset($_POST['friend_'.$i]))
        {
            $receiver=$_POST['friend_'.$i];

            if(!$manual_sent) {
                $manual_sent = true;
            }
            array_push($emailsList, $receiver);
        }
    }
}