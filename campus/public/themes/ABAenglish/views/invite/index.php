<script type="text/javascript">
    var detailsWindowTimer = null;
    var detailsWindow = null;
    var detailsWindowTimer2 = null;
    var detailsWindow2 = null;
</script>
<div id="mgmContainer">
<?php
include(dirname(__FILE__)."/top.php");

$importsucess = FALSE;
$import_sent = false;
$manual_sent = false;

$emailsList = array();
$partnerId = 0;
$sourceId = $userId;

$yahoo_auth_url = '';
$somecontent = '';

$message = NULL;

include_once(dirname(__FILE__).'/import_providers.php'); // import contacts from all the available providers

// send invitations flow
if(isset($_POST['fromemail'])) {
    include_once(dirname(__FILE__).'/invite.php');
}

Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/components/staf/stylesheets/font_styles_clear.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/components/staf/jscript/jquery.slimscroll.js');
?>
<script language="javascript" type="text/javascript">
<!-- sort dropdown -->
function sortDropDownListByText() {
	// Loop for each select element on the page.
    $("select").each(function() {

        // Keep track of the selected option.
        var selectedValue = $(this).val();

        // Sort all the options by text. I could easily sort these by val.
        $(this).html($("option", $(this)).sort(function(a, b) {
            return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
        }));

        // Select one option.
        $(this).val(selectedValue);
    });
}

<!-- char count -->

function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}
</script>
<!--- -->

<script language="JavaScript">
function stripHTML()
{
var re= /<\S[^><]*>/g
	for (i=0; i<arguments.length; i++)
	{
	arguments[i].value=arguments[i].value.replace(re, "");
	}
}

function trimAll( strValue ) {
    var objRegExp = /^(\s*)$/;

    //check for all spaces
    if(objRegExp.test(strValue)) {
        strValue = strValue.replace(objRegExp, '');
        if( strValue.length == 0)
            return strValue;
    }

    //check for leading & trailing spaces
    objRegExp = /^(\s*)([\W\w]*)(\b\s*$)/;
    if(objRegExp.test(strValue)) {
        //remove leading and trailing whitespace characters
        strValue = strValue.replace(objRegExp, '$2');
    }
    return strValue;
}

function byteCount(s) {
    return encodeURI(s).split(/%..|./).length - 1;
}

function parentExists()
{
    return (parent.location == window.location)? false : true;
}

function check_email(e)
{
    ok = "1234567890qwertyuiop[]asdfghjklzxcvbnm.@-_QWERTYUIOPASDFGHJKLZXCVBNM";
    var i=0;
    for(i=0; i < e.length ;i++)
    {
        if(ok.indexOf(e.charAt(i))<0)
        {
            return (false);
        }
    }

    if (document.images)
    {
        re = /(@.*@)|(\.\.)|(^\.)|(^@)|(@$)|(\.$)|(@\.)/;
        re_two = /^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (!e.match(re) && e.match(re_two))
        {
            return (-1);
        }
    }
}

function CalcKeyCode(aChar)
{
  var character = aChar.substring(0,1);
  var code = aChar.charCodeAt(0);
  return code;
}
function checknothtml(val) {
  var strPass = val.value;
  var strLength = strPass.length;
  var lchar = val.value.charAt((strLength) - 1);
  var cCode = CalcKeyCode(lchar);
  if (cCode == 60 || cCode == 62 ) {
    var myNumber = val.value.substring(0, (strLength) - 1);
    val.value = myNumber;
  }
  return false;
}
function manual_reset() {
    document.getElementById("manual_success").style.display = "none";
    document.getElementById("manual_content").style.display = "block";
}
function import_reset() {
    document.getElementById("import_success").style.display = "none";
    document.getElementById("import_content").style.display = "block";
}
function goToSuccess() {
    if(parentExists()) {
        window.parent.location='<?php echo $successUrl ?>';
    } else {
        window.location='<?php echo $successUrl ?>';
    }
}
function importCancel() {
    window.location = window.location.href.replace(/#.*$/, "");
}
function submitenter(myfield,e)
{
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13)
    {
       verimport();
       return false;
    }
    else
       return true;
}
function submitenter3(myfield,e)
{
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13)
    {
       vermanualsend();
       return false;
    }
    else
       return true;
}
function submitenter4(myfield,e)
{
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13)
    {
       checkimavailability();
       return false;
    }
    else
       return true;
}

function submitenter5(myfield,e)
{
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13)
    {
       document.getElementById('addmanualfriend').click();
       return false;
    }
    else
       return true;
}

function togglechecked(){
    var ch;
    if(document.sendemailsform.elements[0].checked==false) {ch=false;}
    if(document.sendemailsform.elements[0].checked==true) {ch=true;}
    for (var i = 1; i < document.sendemailsform.elements.length; i++)
    {
        var e = document.sendemailsform.elements[i];

        if (e.type == 'checkbox' && e.id!='optintomailinglist')
        {
            e.checked = ch;
        }

    }
}

function is_one_checked()
{
    var ischecked;
    ischecked=false;
    document.getElementById('selectatleastonecontact').style.display='none';
    for (var i = 1; i < document.sendemailsform.elements.length; i++)
    {
        var e = document.sendemailsform.elements[i];

        if (e.type == 'checkbox' && e.id!='optintomailinglist')
        {
            if(e.checked){ischecked=true;}
        }

    }
    if(ischecked)
    {
        document.sendemailsform.submit();
    }
    else
    {
        document.getElementById('selectatleastonecontact').style.display='block';
        $('#selectatleastonecontact').delay(3000).fadeOut('slow', function() {
            document.getElementById('selectatleastonecontact').style.display='none';
        });
    }

}

function unchecktop(ischecked,id)
{
	if(ischecked==false)
	{
	document.sendemailsform.allbox2.checked=false;
	}
}

function verimport()
{
    document.getElementById('message').style.display='none';
	if(trimAll(document.massimport.elements[0].value)=='')
	{
	document.getElementById('message').style.display='block';
	document.getElementById('message').innerHTML='<?php echo($importerjserror5) ?>';

    document.massimport.elements[0].focus();

	$('#message').delay(2500).fadeOut('slow', function() {
	document.getElementById('message').style.display='none';
	});

	return false;
	}

	if(trimAll(document.massimport.elements[3].value)=='')
	{
	document.getElementById('message').style.display='block';
	document.getElementById('message').innerHTML='<?php echo($importerjserror4) ?>';
	document.massimport.elements[3].focus();

	$('#message').delay(2500).fadeOut('slow', function() {
	});

	return false;
	}
	document.getElementById('message').innerHTML="<img src=<?php echo Yii::app()->theme->baseUrl ?>/components/staf/images/loader.gif border=0 hspace=0 vspace=0>";
	document.getElementById('message').style.background = 'url(none.jpg)';
	document.getElementById('message').style.paddingLeft='0px';
    document.getElementById('message').style.display='block';


	document.massimport.masssubmit.value='<?php echo($importerformcontrol5) ?>';
	document.massimport.masssubmit.disabled=true;
	document.getElementById('errorlabel').style.padding='0';
	document.massimport.clientname.focus();
	window.setTimeout("document.massimport.submit()",1000);
}

function vermanualsend()
{
    document.getElementById('error').style.display='none';

	if(document.manualsend.totalnumberemails.value=='0')
	{
        document.getElementById('error').innerHTML='<?php echo $manualjserror3; ?>';
        document.getElementById('error').style.display='block';

		$('#error').delay(2500).fadeOut('slow', function() {
			document.getElementById('error').style.display='none';
			});
	    return false;
	}
    document.getElementById('error2').style.display='block';
	document.getElementById('error2').innerHTML="<img src='<?php echo Yii::app()->theme->baseUrl ?>/components/staf/images/loader2.gif' align=absmiddle border=0 hspace=3 vspace=1> <span style='color:#00D025'><?php echo $manualformcontrol10 ?></span>";
	document.getElementById('manualsubmit').disabled=true;
	document.getElementById('manualsubmit').style.display='none';
	window.setTimeout("document.manualsend.submit()", 1000);
}
function addField(area,field,limit)
{
    if(!check_email(trimAll(document.manualsend.toemail.value)))
    {
    document.getElementById('error').innerHTML="<?php echo $manualjserror2; ?>";
    document.getElementById('error').style.display='block';
    document.manualsend.toemail.focus();

    $('#error').delay(2500).fadeOut('slow', function() {
        document.getElementById('error').style.display='none';
        });

    return false;
    }


	var field_area = document.getElementById(area);
	var all_inputs = field_area.getElementsByTagName("input");
	var last_item = all_inputs.length - 1;
	var last = all_inputs[last_item].id;
	var count = Number(last.split("_")[1]) + 1;
	if(count > limit && limit > 0) return;

	if(document.createElement)
		{
		var p = document.createElement("p");
		p.innerHTML ='<input type="hidden" name="'+field+count+'" id="'+field+count+'"> ';
		//p.innerHTML += '<img src="images/user.png" border="0" align="absmiddle"> ';
        p.innerHTML +='<a href="javascript:;" class="slink" onClick="removeField(this)"><img src="<?php echo Yii::app()->theme->baseUrl ?>/components/staf/images/user_delete.png" border="0" align="absmiddle" title="<?php echo $manualformcontrol9 ?>"></a>';
		p.innerHTML += document.manualsend.toemail.value;
		field_area.appendChild(p);
		document.getElementById(field+count).value=document.manualsend.toemail.value;
		document.manualsend.toemail.value='';
		document.manualsend.nrofemails.value=parseInt(document.manualsend.nrofemails.value)+1;
		document.manualsend.totalnumberemails.value=parseInt(document.manualsend.totalnumberemails.value)+1;
		document.getElementById('error').innerHTML='';
		document.getElementById('error').style.display='none';
	 	}
	else
	{
		field_area.innerHTML += "<input name='"+(field+count)+"' id='"+(field+count)+"' type='text' /> <a onclick='this.parentNode.parentNode.removeChild(this.parentNode);'><?php echo $manualformcontrol9 ?></a><br>";
	}

    var container = $('#manual_emails');
    container.slimScroll({scrollTo: container.prop('scrollHeight') + 'px'});

    document.getElementById("manualsubmit").style.display = "block";
}

function removeField(obj)
{
    document.manualsend.totalnumberemails.value=parseInt(document.manualsend.totalnumberemails.value)-1;
    obj.parentNode.parentNode.removeChild(obj.parentNode);
    if(document.manualsend.totalnumberemails.value == 0) document.getElementById("manualsubmit").style.display = "none";
}

var sharewindow_timer;
function sendsociallink()
{
    var share_url,share_title, share_short_text,share_long_text;
    share_url="<?php echo Yii::t('mainApp', 'mgm_shareOnFacebook') ?>%3Fpartnerid=<?php echo PARTNER_MGM_FACEBOOK ?>%26sourceid=<?php echo $userId ?>";

    ///screen width
    var fulllink=document.getElementById('fullurltosend').value;

    //replace url
    fulllink=fulllink.replace("[URL]", share_url);

    var finallinkvalue=fulllink;

    sharewindow =window.open(finallinkvalue,"bookmark","location=1,status=1,scrollbars=1, width=700, height=550",true);
    var l=(screen.width - 500)/2;
    var t=(screen.height - 500)/2;
    sharewindow.moveTo(l,t);
    sharewindow.focus();
    sharewindow_timer = setInterval("watchsharerwindowforclose()",500);

}

function watchsharerwindowforclose()
{
    if (!sharewindow || sharewindow.closed)
    {
        document.getElementById('social1').style.display='none';
        document.getElementById('social2').style.display='block';
        clearInterval(sharewindow_timer); //stop the timer
    }
}
</script>

<?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/components/staf/stylesheets/social.css'); ?>

<!-- accordion -->
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/components/staf/jscript/ddaccordion.js'); ?>
<script type="text/javascript">

ddaccordion.init({
	headerclass: "openpreviewemail", //Shared CSS class name of headers group
	contentclass: "displayemailtxt", //Shared CSS class name of contents group
	revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click" or "mouseover"
	mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
	collapseprev: true, //Collapse previous content (so only one open at any time)? true/false
	defaultexpanded: [], //index of content(s) open by default [index1, index2, etc]. [] denotes no content.
	onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
	animatedefault: false, //Should contents open by default be animated into view?
	persiststate: false, //persist state of opened contents within browser session?
	toggleclass: ["", "openpreviewemail2"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["prefix", "<img src='<?php echo Yii::app()->theme->baseUrl ?>/components/staf/images/plus.png' align='absmiddle' border='0' /> Preview the email invitation. [Click to open] ", "<img src='<?php echo Yii::app()->theme->baseUrl ?>/components/staf/images/minus.png' align='absmiddle' border='0' /> Preview the email invitation. [Click to close] "],
	//two images added to the end of the header //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed: "normal", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
	oninit:function(expandedindices){ //custom code to run when headers have initalized
		//do nothing
	},
	onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
		//do nothing
	}
})

</script>
<!-- end accordion -->

<!-- box -->
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/components/staf/jscript/window/highslide-full.js'); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/components/staf/jscript/window/highslide.css'); ?>
<script type="text/javascript">
    hs.graphicsDir = '<?php echo Yii::app()->theme->baseUrl ?>/components/staf/jscript/window/graphics/';
    hs.outlineType = 'rounded-white';
</script>
<!-- end box -->

<!--yahoo api code -->
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/components/staf/jscript/yahoo-dom-event.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/components/staf/jscript/popupmanager.js'); ?>

    <script type="text/javascript">
      var Event = YAHOO.util.Event;
      var _gel = function(el) {return document.getElementById(el)};

      function handleDOMReady()
	  {
        if(_gel("yloginLink")) {
          Event.addListener("yloginLink", "click", handleLoginClick);
        }
      }

      function handleLoginClick(event)
	  {
        Event.preventDefault(event);
        var auth_url = _gel("yloginLink").href;
        PopupManager.open(auth_url,640,640);
      }

      Event.onDOMReady(handleDOMReady);
  </script>
<?php function close_popup() { ?>
    <script type="text/javascript">
        window.close();
    </script>
<?php } ?>
<!--end yahoo api code -->

<!--gmail api code -->
<script language="javascript">

function WatchDetailsWindowForClose()
{
    if (!detailsWindow || detailsWindow.closed)
    {
        clearInterval(detailsWindowTimer); //stop the timer
        $("#mgmBody").hide();
        window.location='<?php echo $absoluteUrl ?>?importype=import&gmail=import';
    }
}
function OpenDetailsWindow()
{
    if(detailsWindow == null) {
        document.getElementById("login").style.display = "none";
        /*var sel =  document.massimport.service;

        for(var i = 0; i < sel.options.length; i ++) {
            if(sel.options[i].value === event.target.value) {
                sel.selectedIndex = i;
                break;
            }
        }*/

        var url='https://accounts.google.com/o/oauth2/auth?client_id=<?php echo MGM_GMAIL_CLIENT_ID ?>&redirect_uri=<?php echo MGM_GMAIL_REDIRECT_URI ?>&scope=https://www.google.com/m8/feeds/ https://www.googleapis.com/auth/userinfo.profile&response_type=code&max_auth_age=0';
        detailsWindow = window.open(url,'Gmail','height=560,width=560',true);
        var ie=false;
        if (navigator.appVersion.indexOf("MSIE") != -1)
        {
            ie=true;
        }

        if(ie==false)
        {

        setTimeout(function()
        {
               var l=(screen.width - 500)/2;
               var t=(screen.height - 500)/2;
               detailsWindow.moveTo(l,t);
        },50);
        }
        detailsWindowTimer = setInterval("WatchDetailsWindowForClose()",500);
    }
    detailsWindow.focus();
}

function showOtherServices(show)
{
    if(show == 1) {
        document.getElementById("login").style.display = "block";
        document.getElementById("showOtherServices").style.display = "none";
        document.getElementById("hideOtherServices").style.display = "block";
    }
    else {
        document.getElementById("login").style.display = "none";
        document.getElementById("showOtherServices").style.display = "block";
        document.getElementById("hideOtherServices").style.display = "none";
    }
}
</script>
<!--end gmail api code -->

<!--hotmail API code -->
<script language="javascript">

function WatchDetailsWindowForClose2(service)
{
    if (!detailsWindow2 || detailsWindow2.closed)
    {
        clearInterval(detailsWindowTimer2); //stop the timer
        if(service === 'yahoo') window.location='<?php echo $absoluteUrl ?>?importype=import';
        else window.location='<?php echo $absoluteUrl ?>?importype=import&hotmail=import';
    }
}
function OpenDetailsWindow2(service)
{
    if(detailsWindow2 == null) {
        var url='';
        if(service === 'yahoo') url = '<?php echo $yahoo_auth_url ?>';
        else url = 'https://login.live.com/oauth20_authorize.srf?client_id=<?php echo MGM_HOTMAIL_CLIENT_ID ?>&scope=wl.basic,wl.contacts_emails&response_type=code&redirect_uri=<?php echo MGM_HOTMAIL_REDIRECT_URI ?>';
        detailsWindow2 = window.open(url,'Gmail','height=560,width=560',true);
        var ie=false;
        if (navigator.appVersion.indexOf("MSIE") != -1)
        {
            ie=true;
        }

        if(ie==false)
        {

        setTimeout(function()
        {
               var l=(screen.width - 500)/2;
               var t=(screen.height - 500)/2;
               detailsWindow2.moveTo(l,t);
        },50);
        }
        detailsWindowTimer2 = setInterval("WatchDetailsWindowForClose2('"+service+"')",500);
    }
    detailsWindow2.focus();
}

</script>
<!--end hotmail API code --->

<!-- tabs -->
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/components/staf/jscript/tabs/tabs.css'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/components/staf/jscript/tabs/jquery.hashchange.min.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/components/staf/jscript/tabs/jquery.easytabs.min.js'); ?>
<style type="text/css">
#mgmBody {
  display: none;
}
</style>
    <script type="text/javascript">
        var currentDocument = (parentExists()) ? window.parent.document : window.document;
        $(currentDocument).ready( function() {
            $('#tab-container').easytabs(
                <?php if($manual_sent) { ?>
                    { defaultTab: "li#tab-manual" }
                <?php } ?>
            );
            $('#manual_emails').slimscroll({
                height: '140px',
                alwaysVisible: true,
                color: '#3D3D3D',
                wheelStep:1,
                position:'right',
                distance:'1px'
            });
            $("#import ul li").bind("click", function() {
                $("#import ul li").removeClass("selected");
                $(this).addClass("selected");
            });
            $("#mgmBody").show();

            if(parentExists()) {
                $('.ab-testing').addClass('ab-testing-invite');
            }
        });
    </script>
<!-- end tabs -->
</head>
<body id="mgmBody">
<div id="selligent_response"></div>
<noscript>
<div class="warning">
<span style="font-family:Tahoma; size:15px; font-weight:bold; color:#CC3300 ">You must enable javascript to be able to run this application!</span>
</div>
</noscript>

<!-- main TB -->
<table width="100%" cellpadding="0" cellspacing="0" border="0"  align="center">
<tr><td align="center">
<!-- start tabs navigation -->
<div id="tab-container" class='tab-container'>
    <ul class='etabs'>
    <li id="tab-import" class='tab'><a href="#import"><?php echo $toplink1 ?></a></li>
    <li id="tab-manual" class='tab'><a href="#manual"><?php echo $toplink3 ?></a></li>
    <li id="tab-share" class='tab'><a href="#share"><?php echo $toplink5 ?></a></li>
    </ul>

<div class='panel-container'>

<!-- show this tab -->
<div id="import">
    <div id="import_success" align="center" style="<?php if(!$import_sent) echo 'display:none;' ?>">
        <div class="message_success"><?php echo $sharercontrol7; ?></div>
        <input class="submit" type="button" onClick="import_reset();" value="<?php echo $success_continue ?>" >
        <?php if($successUrl !== NULL) { ?>
            <input class="submit" type="button" onClick="goToSuccess();" value="<?php echo $success_finish ?>" >
        <?php } ?>
    </div>
    <div id="import_content" style="<?php if($import_sent) echo 'display:none;' ?>">
        <?php  //start webmail contact importer
        if(!$importsucess)
        {
            ?>
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
            <td align="center" style="padding-top:10px">
                <?php
                $other_services = array('aol', 'fastmail', 'gmx.net', 'icqmail', 'lycos', 'mail', 'rediffmail', 'web.de', 'ymail_api');
                ?>
                <ul>
                    <li>
                        <a value="gmail" onclick="OpenDetailsWindow();" <?php if(isset($_POST['service']) && $_POST['service']=='gmail') {echo("selected");} ?>>
                            <table>
                                <tr>
                                    <td><img src=<?php echo Yii::app()->theme->baseUrl ?>/components/staf/images/mgm_gmail.png border=0 hspace=0 vspace=0></td>
                                    <td style="line-height: 20px;">@gmail.com<br>@googlemail.com</td>
                                </tr>
                            </table>
                        </a>
                    </li>
                    <li style="border-right: 1px #e5e5e5 solid; border-left: 1px #e5e5e5 solid;">
                        <a value="hotmail" onclick="OpenDetailsWindow2();" <?php if(isset($_POST['service']) && $_POST['service']=='hotmail') {echo("selected");} ?>>
                            <table>
                                <tr>
                                    <td><img src=<?php echo Yii::app()->theme->baseUrl ?>/components/staf/images/mgm_hotmail.png border=0 hspace=0 vspace=0></td>
                                    <td style="line-height: 20px;">@hotmail.com<br>@live.com<br>@msn.com</td>
                                </tr>
                            </table>
                        </a>
                    </li>
                    <li>
                        <a value="yahoo" onclick="OpenDetailsWindow2('yahoo');" <?php if(isset($_POST['service']) && $_POST['service']=='yahoo') {echo("selected");} ?>>
                            <table>
                                <tr>
                                    <td><img src=<?php echo Yii::app()->theme->baseUrl ?>/components/staf/images/mgm_yahoo.png border=0 hspace=0 vspace=0></td>
                                    <td>@yahoo.com</td>
                                </tr>
                            </table>
                        </a>
                    </li>
                    <li class="other">
                        <a id="showOtherServices" onclick="showOtherServices(1);" <?php if(isset($_POST['service']) && in_array($_POST['service'], $other_services)) {echo("selected");} ?>><?php echo $importerformcontrol6 ?></a>
                        <a id="hideOtherServices" style="display:none;" onclick="showOtherServices(0);" <?php if(isset($_POST['service']) && in_array($_POST['service'], $other_services)) {echo("selected");} ?>><?php echo $importerformcontrol7 ?></a>
                    </li>
                </ul>
            </td>
            </tr><tr><td align="left" id="errorlabel" colspan="2" ><div id="message" class="message_error" style="margin-top: 5px;"><?php echo $message; ?></div></td></tr>
            <tr>
            <td>
            <div id="login" style="display: none;">
            <form method="post" action="<?php echo $absoluteUrl ?>" name="massimport" autocomplete="off">
            <table border="0" cellspacing="0" cellpadding="0" class="tableimport" width="530">
            <tr>
                <td height="30" class="input-labels"><?php echo $importerformcontrol1 ?></td>
                <td height="30" class="input-labels"><?php echo $importerformcontrol2 ?></td>
            </tr>
            <tr>
                <td align="left" style="padding-left: 15px;">
                    <input class="inputbox" style="width:165px; float:left;" name="username"  onKeyPress="return submitenter(this,event)">
                    <select name="service" style="float:left;margin-left: 0px;" class="comboservice" onChange="if(document.massimport.username.value==''){document.massimport.username.focus();} else{document.massimport.password.focus();}">
                        <option value="aol" <?php if((isset($_POST['service']) && $_POST['service']=='aol') || (!isset($_POST['service']))) {echo("selected");} ?> >@aol.com</option>
                        <option value="fastmail" <?php if(isset($_POST['service']) && $_POST['service']=='fastmail') {echo("selected");} ?>>@fastmail.fm</option>
                        <option value="gmx.net" <?php if(isset($_POST['service']) && $_POST['service']=='gmx.net') {echo("selected");} ?>>@gmx.net</option>
                        <option value="icqmail" <?php if(isset($_POST['service']) && $_POST['service']=='icqmail') {echo("selected");} ?>>@icqmail.com</option>
                        <option value="lycos" <?php if(isset($_POST['service']) && $_POST['service']=='lycos') {echo("selected");} ?>>@lycos.com</option>
                        <option value="maildotcom" <?php if(isset($_POST['service']) && $_POST['service']=='mail') {echo("selected");} ?>>@mail.com</option>
                        <option value="rediffmail" <?php if(isset($_POST['service']) && $_POST['service']=='rediffmail') {echo("selected");} ?>>@rediffmail.com</option>
                        <option value="web.de" <?php if((isset($_POST['service']) && $_POST['service']=='web.de')) {echo("selected");} ?>>@web.de</option>
                        <option value="ymail_api" <?php if((isset($_POST['service']) && $_POST['service']=='ymail_api')) {echo("selected");} ?>>@ymail.com</option>
                    </select>
                </td>
                <td style="padding-left: 15px;">
                    <input name="clientname" type="hidden" value="">
                    <input class="inputbox" type="text" style="width:180px" name="password" onKeyPress="return submitenter(this,event)" autocomplete="off">
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2" style="padding-left: 15px;">
                    <input name="act" type="hidden" id="act" value="showContacts">
                    <input class="submit" style="min-width: 140px; margin-top: 15px;" type="button" name="masssubmit" value="<?php echo($importerformcontrol4) ?>" onClick="verimport()">
                </td>
            </tr>
            </table>
                <input type="hidden" name="serviceAux" value="" />
            </form>
            </div>
            </td>
            </tr>
            </table>
            <?php
        }  //end import form
        ?>
        <?php
        if($importsucess) ///start importing contacts
        {
            unset($_SESSION['gmail_username']);
            unset($_SESSION['gmail_email']);
            unset($_SESSION['gmail_loggedin']);

             ?>
            <div class="importsucess"><?php echo $message2 ?></div>
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
            <td align="center">
            <form method="post" name="sendemailsform" id="sendemailsform">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
            <td width="30" height="20" align="center" class="headercontacts" style="max-width: 30px;">
            <input name="allbox2" type="checkbox" id="allbox2" onClick="togglechecked()" value="nothing"></td>
            <td align="left" width="200" class="headercontacts" style="max-width: 200px;"><?php echo $importerheader1 ?></td>
            <td align="left" class="headercontacts" style="max-width: 280px;"><?php echo $importerheader2 ?></td>
            </tr>
            </table>

            <div id="showcontactsscroll">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <?php
            $i=0;
            foreach($contacts as $contact)
            {
                $i++;
                $email = trim($contact['email']);
                $name = trim($contact['name']);
                if($name==''){$ouputname = $email; }
                else{$ouputname= $name; }

                if($email!='' && $name!='')
                {
                    ?>
                    <tr <?php if($i%2=="0") {echo "class=\"showcontacts_bg\"";} else{echo "class=\"showcontacts_bg2\"";} ?>>
                    <td align="center" class="showcontacts" width="30" height="20" style="max-width: 30px;">
                    <input  name="address_<?php echo $i?>" type="checkbox" id="address_<?php echo $i?>" value="<?php echo $email;?>" onClick="unchecktop(this.checked,<?php echo $i ?>)">
                    <input name="recname_<?php echo $i?>" type="hidden" id="recname_<?php echo $i?>" value="<?php echo $name;?>" >
                    </td>
                    <td align="left" width="200" class="showcontacts" style="max-width: 200px;"><?php echo $ouputname; ?></td>
                    <td align="left" class="showcontacts" style="max-width: 280px;"><?php echo $email;?></td>
                    </tr>
                    <?php
                }
            } ?>
            </table>
            </div>

            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin-top:10px">
            <tr>
            <td colspan="3" class="textbeforeemailtext2" align="left" height="24">
            <div class="message_error" style="display:none; margin-bottom:4px;" id="selectatleastonecontact"><?php echo $importermessage3 ?></div>
            <div align="center">
                <input class="submit ab-testing" style="MIN-WIDTH: 160px" type="button" onClick="is_one_checked()" value="<?php echo $importerformcontrol1_ai ?>" >
                <input class="submit" style="MIN-WIDTH: 100px" type="button" value="<?php echo $importerformcontrol2_ai ?>" onClick="importCancel();">
            </div>
            </td>
            </tr>
            </table>
            <input name="act" type="hidden" id="act" value="sendMessage">
            <input name="nrcheckedcontacts" type="hidden" id="nrcheckedcontacts" value="0">
            <input type="hidden" name="sendemails" value="yes">
            <input type="hidden" name="clientname" value="<?php echo (trim($_POST['clientname']))?>">

            <?php if(($_POST['service']!='otheremail')&&($_POST['service']!='fastmail')&&($_POST['service']!='gmx.net')&&($_POST['service']!='web.de')&&(strpos($_POST['service'],'additionalemail_')=== false)) {  ?>
            <input type="hidden" name="fromemail" value="<?php echo ($_POST['username']."@".$_POST['service'].".com")?>">
            <?php }


            if(($_POST['service']=='otheremail')) { ?>
            <input type="hidden" name="fromemail" value="<?php echo ($_POST['username'])?>">
            <?php }
            if(($_POST['service']=='fastmail')) { ?>
            <input type="hidden" name="fromemail" value="<?php echo ($_POST['username']."@fastmail.fm")?>">
            <?php }
            if(($_POST['service']=='gmx.net')) { ?>
            <input type="hidden" name="fromemail" value="<?php echo (trim($_POST['username'])."@gmx.net")?>">
            <?php }
            if(($_POST['service']=='web.de')) { ?>
            <input type="hidden" name="fromemail" value="<?php echo (trim($_POST['username'])."@web.de")?>">
            <?php }
            if(strpos($_POST['service'],'additionalemail_')!== false) {
            $webemail_ext_arr=explode("_",$_POST['service']);
            ?>
            <input type="hidden" name="fromemail" value="<?php echo (trim($_POST['username'])."@".$webemail_ext_arr[1])?>">
            <?php } ?>
            <input name="nrcontacts" type="hidden" id="nrcontacts" value="<?php echo count($contacts)?>">
            </form>
            </td>
            </tr>
            </table>
            <?php
        } ///end contact importer
        ?>
    </div>
    <div class="bottom_explanation"><p><?php echo $footer_note; ?></p><p><?php echo $privacity; ?></p></div>
</div>
<!-- end show this tab -->
<!-- show this tab -->
<div id="manual">
    <div id="manual_success" align="center" style="<?php if(!$manual_sent) echo 'display:none;' ?>">
        <div class="message_success"><?php echo $sharercontrol7; ?></div>
        <input class="submit" type="button" onClick="manual_reset();" value="<?php echo $success_continue ?>" >
        <?php if($successUrl !== NULL) { ?>
            <input class="submit" type="button" onClick="goToSuccess();" value="<?php echo $success_finish ?>" >
        <?php } ?>
    </div>
    <div id="manual_content" style="<?php if($manual_sent) echo 'display:none;' ?>">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
        <td align="center" style="padding-top:5px">
        <form method="post" action="<?php echo $absoluteUrl ?>" name="manualsend">
            <input type="hidden" name="clientname" value="">

            <div id="login3" style="margin:0; padding:0; ">
                <input type="hidden" name="fromemail" value="manual_import">
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td class="input-labels"><?php echo $manualformcontrol4 ?></td>
                        <td align="left">
                        <input class="inputbox" style="margin-left: 5px; width:230px" name="toemail" onKeyPress="return submitenter5(this,event)">
                        <input class="submit submit_mini" id="addmanualfriend"  title="<?php echo $manualformcontrol5 ?>" type="button" onClick="addField('manual_emails','friend_',0);" value="<?php echo $manualformcontrol5 ?>" >
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="input-labels" style="padding-top: 10px;">
                        <div id="manual_emails" class="manualaddbox">
                        <input type="hidden" name="friend_0" id="friend_0">
                        </div>
                        </td>
                    </tr>
                    <tr>
                        <td height="24" colspan="4" align="left">
                        <div id="error" class="message_error"></div>
                        <input name="act" type="hidden" id="act" value="manualinvite">
                        <input name="nrofemails" type="hidden" id="nrofemails" value="0">
                        <input name="totalnumberemails" type="hidden" id="totalnumberemails" value="0">
                    </td>
                    </tr>
                </table>
            </div>
        </form>
        </td>
        </tr>
        <tr>
        <td colspan="3" class="textbeforeemailtext2">
        <div align="center">
        <div id="error2" class="message_error2"></div>
        <input class="submit ab-testing" id="manualsubmit"  name="manualsubmit" style="display:none;" type="button" onClick="vermanualsend()" value="<?php echo $importerformcontrol1_ai ?>" >
        </div>
        </td>
        </tr>
        </table>
    </div> <!-- end manual content -->
    <div class="bottom_explanation"><?php echo $footer_note; ?></div>
</div>
<!-- end show this tab -->

<!-- show this tab -->
<div id="share">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
    <!-- social networks share here --->
    <?php
    $share_url=trim($sharer_link_to_promote);
    if(trim($share_url=="")) {$share_url=trim($sharer_link_to_promote);}
    //end share url
    ?>
    <tr>
    <td align="left" style="vertical-align:top; margin-top:0px; padding-top:0px">
        <div id="social1" style="margin:0; padding:0;">

            <!--social here !!!-->
            <div align="center">
                <input type="hidden" name="fullurltosend" id="fullurltosend" value="http://www.facebook.com/sharer.php?u=[URL]">
                <input type="hidden" name="serviceselected" id="serviceselected" value="Facebook">
                <input class="submit ab-testing" style="MIN-WIDTH:120px" type="button" id="socialinvite2" name="socialinvite2"  value="<?php echo $sharercontrol5 ?>" onClick="sendsociallink()">
            </div>
        </div>
        <div id="social2" style="display:none" align="center"><br>
            <div class="message_success"><?php echo $sharercontrol7 ?></div><br>
            <input class="submit" type="button" id="socialinvite" name="socialinvite"  value="<?php echo $sharercontrol5 ?>" onClick="document.getElementById('social2').style.display='none';document.getElementById('social1').style.display='block';">
            <?php if($successUrl !== NULL) { ?>
                <input class="submit" type="button" onClick="goToSuccess();" value="<?php echo $success_finish ?>" >
            <?php } ?>
        </div>
    </td>
    </tr>
     <!-- end social networks sharer --->
    </table>
    <div class="bottom_explanation"><?php echo $footer_note; ?></div>
</div>
<!-- end show this tab -->

</div> <!-- end tab-container -->
</div> <!-- end panel-container -->
<!-- end tabs navigation -->
</td>
</tr>
</table>
<!-- END main TB -->
<?php
if((!$importsucess))
{
    if($message == '') {
        if(!isset($_POST['username'])) $_POST['username'] = '';
        if(!isset($_POST['clientname'])) $_POST['clientname'] = '';
        echo("<script>
        document.massimport.username.value='".trim($_POST['username'])."';
        document.massimport.clientname.value='".trim($_POST['clientname'])."';
        </script>");
    }
}

if($importsucess)
{
    if(sizeof($contacts)>6)
    {
        echo ("<script type=\"text/javascript\">
           $(function(){
              $('#showcontactsscroll').slimscroll({
                height: '162px',
                alwaysVisible: true,
                color: '#3D3D3D',
                wheelStep:1,
                position:'right',
                distance:'1px'
              });
              });
        </script>");
    }
}

if(isset($message) && trim($message)!='' && !($importsucess))
{
    echo("<script>document.getElementById('message').style.display='block';</script>");
    echo("<script>
    $('#message').delay(3500).fadeOut('slow', function() {
    document.getElementById('message').style.display='none';
    });
    </script>");
}

//yahoo api code
if((!$importsucess)&&($yahoohasSession != FALSE) && ((!isset($_GET['importype']) || $_GET['importype']=="import")))
{
    // if a session is initialized, fetch the user's profile information
    if($yahoo_session)
    {

        // Get the currently sessioned user.
        $user = $yahoo_session->getSessionedUser();
        // Load the profile for the current user.
        $profile = $user->getProfile();
        $username = '';
        $clientname = '';
        // email
        if(isset($profile->emails)) {
            $nremails=sizeof($profile->emails);

            for($n=0;$n<=$nremails;$n++)
            {

                if(isset($profile->emails[$n]->primary))
                {
                    $username = $profile->emails[$n]->handle;
                }
            }

            if(trim($username==""))
            {
                $username=trim($profile->emails[0]->handle);
            }

            if(trim($username==""))
            {
                $username=trim($profile->emails[1]->handle);
            }
        }
        //name
        if(isset($profile->givenName) && isset($profile->familyName)) {
            $clientname=trim(trim($profile->givenName)." ".trim($profile->familyName));
        }
        if(trim($clientname)=="" && isset($profile->nickname))
        {
            $clientname=$profile->nickname;
        }
        if(trim($clientname)=="" && isset($profile->emails) && isset($profile->emails[0]))
        {
            $nn=explode("@",$profile->emails[0]->handle);
            $clientname=$nn[0];
        }

        ///hidden profile
        if($username=="" && isset($profile->nickname))
        {
            $username=strtolower($profile->nickname)."@yahoo.com";
            if(trim($clientname)=="")
            {$clientname=trim($profile->nickname);}

        }

        ///logout
        YahooSession::clearSession();

        $filename=MGM_TMP_FOLDER.'yahoocontacts_'.Yii::app()->user->getId().'.txt';
        $handle = fopen($filename, 'w');

        $w=0;
        ///get contacts here
        $contact=$user->getContacts(0,5000);

        $n1=$contact->contacts->total;
        $contacts=$contact->contacts->contact;
        foreach($contacts as $cont)
        {

            $num=count($cont->fields);
            for($i=0;$i<$num;$i++)
            {
                $name="";


                for($i1=0;$i1<$num;$i1++)
                {
                    if($cont->fields[$i1]->type=="name")
                    {


                        $fname=trim($cont->fields[$i1]->value->givenName);
                        $lname=trim($cont->fields[$i1]->value->familyName);
                        $mname=trim($cont->fields[$i1]->value->middleName);
                        $name=trim($fname." ".$mname." ".$lname);
                    }
                }

            }

            //email
            $email="";
            if(trim($cont->fields[0]->type)=="email")
            {
                $email=trim($cont->fields[0]->value);
            }

            if(isset($cont->fields[1]) && trim($cont->fields[1]->type)=="email")
            {
                $email=trim($cont->fields[1]->value);
            }

            if(trim($cont->fields[0]->type)=="yahooid" && $email=="")
            {
                $email=$cont->fields[0]->value."@yahoo.com";
            }

            if(trim($cont->fields[0]->type)=="otherid" && $email=="")
            {
                $email=trim($cont->fields[0]->value);
            }

            //fix name
            if (trim($name)=="")
            {
                $nn=explode("@",$email);
                $name=$nn[0];
            }

            if($email!="")
            {
                $somecontent=$somecontent.$email."%%%".$name."|||\n";
                $w++;
            }
        }

        //write in contacts file
        fwrite($handle, $somecontent);
        fclose($handle);

        echo("<script>
         document.massimport.clientname.value='".$clientname."';
         document.massimport.clientname.focus();
         document.massimport.username.value='".$username."';
         document.massimport.clientname.readOnly=true;
         document.massimport.username.readOnly=true;
         document.massimport.password.value='oauth';
         document.massimport.password.disabled=true;
         document.massimport.service.setAttribute('disabled','disabled');
         document.massimport.serviceAux.value='yahoo';
         document.massimport.serviceAux.setAttribute('name', 'service');
         document.getElementById('message').style.background = 'url(none.jpg)';
         document.getElementById('message').style.paddingLeft='0px';
         document.getElementById('message').innerHTML='<img src=".Yii::app()->theme->baseUrl."/components/staf/images/loader.gif border=0 hspace=0 vspace=0>';
         document.getElementById('message').style.display='block';
         document.getElementById('errorlabel').style.padding='0';
         document.massimport.submit();
         </script>");
    }
}
///end yahoo api code

//gmail api
else if((!$importsucess)&&(isset($_GET['gmail'])) &&($_GET['gmail']=="import") &&(isset($_SESSION['gmail_loggedin'])) && (($_GET['importype']=="import" || !isset($_GET['importype']))))
{
    ?>
	<script>
	 document.massimport.clientname.value='<?php echo $_SESSION['gmail_username'] ?>';
	 document.massimport.clientname.focus();
	 document.massimport.username.value='<?php echo $_SESSION['gmail_email'] ?>';
	 document.massimport.clientname.readOnly=true;
	 document.massimport.username.readOnly=true;
	 document.massimport.password.value='oauth';
	 document.massimport.password.disabled=true;
     document.massimport.service.setAttribute("disabled","disabled");
     document.massimport.serviceAux.value='gmail';
     document.massimport.serviceAux.setAttribute("name", "service");
	 document.getElementById('message').style.background = 'url(none.jpg)';
	 document.getElementById('message').style.paddingLeft='0px';
     document.getElementById('message').style.display='block';
	 document.getElementById('message').innerHTML='<img src="<?php echo Yii::app()->theme->baseUrl ?>/components/staf/images/loader.gif" border="0" hspace="0" vspace="0">';
	 document.getElementById('errorlabel').style.padding='0';
	 document.massimport.submit();
	 </script>
    <?php
	unset($_SESSION['gmail_loggedin']);
}
//end gmail api

///start hotmail api code
else if((!$importsucess)&&(isset($_GET['hotmail']))&&($_GET['hotmail']=="import") &&(isset($_SESSION['hotmail_loggedin'])) && (($_GET['importype']=="import" || !isset($_GET['importype']))))
{
echo("<script>
	 document.massimport.clientname.value='".$_SESSION['hotmail_username']."';
	 document.massimport.clientname.focus();
	 document.massimport.username.value='".$_SESSION['hotmail_email']."';
	 document.massimport.clientname.readOnly=true;
	 document.massimport.username.readOnly=true;
	 document.massimport.password.value='oauth';
	 document.massimport.password.disabled=true;
	 document.massimport.service.setAttribute('disabled','disabled');
     document.massimport.serviceAux.value='hotmail';
     document.massimport.serviceAux.setAttribute('name', 'service');
	 document.getElementById('message').style.background = 'url(none.jpg)';
	 document.getElementById('message').style.paddingLeft='0px';
     document.getElementById('message').style.display='block';
	 document.getElementById('message').innerHTML='<img src=".Yii::app()->theme->baseUrl."/components/staf/images/loader.gif border=0 hspace=0 vspace=0>';
	 document.getElementById('errorlabel').style.padding='0';
	 document.massimport.submit();
	 </script>");
	unset($_SESSION['hotmail_loggedin']);
}
///end hotmail api code

if($manual_sent || $import_sent) {
    ?>
    <script type="text/javascript">
        var data = "<?php echo implode(',', $emailsList) ?>";
        sendInvitations(data);
        function sendInvitations(data) {
            // call to send the invitations as many times as data is bigger than 2KB (maximum browser post size)
            if(byteCount(data) > 2000) {
                var aData = data.split(',');
                var indexToSplit = Math.round(aData.length/2);
                var data1 = aData.slice(0, indexToSplit);
                var data2 = aData.slice(indexToSplit + 1);
                sendInvitations(data1.join(','));
                sendInvitations(data2.join(','));
            }
            $.ajax({
                type: "POST",
                url: "<?php echo $invite_url ?>",
                data: {"emails" : data}
            }).done(function(response){
                document.getElementById("selligent_response").innerHTML = response;
            });
        }
    </script>
    <?php
}
?>
<img style="display:none;" src=<?php echo Yii::app()->theme->baseUrl ?>/components/staf/images/loader.gif border=0 hspace=0 vspace=0>
</div>