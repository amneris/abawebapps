<div class="bodyPopUp <?php if (isset($closeBtn)) echo 'popupCloseBtn' ?>">
    <?php
        switch($header)
        {
            case 'full':
                $this->renderPartial('//welcome/userDataHeaderFull', array('name' => $name));
            break;
            default:
                $this->renderPartial('//welcome/userDataHeader', array('name' => $name));
            break;
        }
        $form = $this->beginWidget('CActiveForm', array('id' => 'formUserData', 'action' => Yii::app()->createAbsoluteUrl('welcome/userData')));
    ?>
    <div class="left">
        <div class="left">
            <div class="question">
                 <span class="bold">1.</span><span class="normal"><?php echo Yii::t('mainApp', '¿Para qué quieres aprender inglés?') ?></span>
            </div>
            <ul class="registro comprimidos left" lang="<?php echo Yii::app()->language?>">
                <li class="errores">
                    <?php
                        $errorStr = str_replace(' viajar',"", $form->error($model, 'viajar'));
                        echo $errorStr;
                    ?>
                </li>
                <li class="clear"></li>
                <li lang="<?php echo Yii::app()->language?>">
                    <?php echo $form->checkBox($model, "viajar", array('value' => '1')) ?>
                    <?php echo Yii::t('mainApp', 'Viajar') ?>
                </li>
                <li lang="<?php echo Yii::app()->language?>">
                    <?php echo $form->checkBox($model, "estudiar", array('value' => '1')) ?>
                    <?php echo Yii::t('mainApp', 'Estudiar') ?>
                </li>
                <li class="clear"></li>
                <li lang="<?php echo Yii::app()->language?>">
                    <?php echo $form->checkBox($model, "trabajar", array('value' => '1')) ?>
                    <?php echo Yii::t('mainApp', 'Trabajar') ?>
                </li>
                <li lang="<?php echo Yii::app()->language?>">
                    <?php echo $form->checkBox($model, "otros", array('value' => '1')) ?>
                    <?php echo Yii::t('mainApp', 'Otros') ?>
                </li>
            </ul>
        </div>
        <div class="left marginLeft4">
            <div class="question">
                  <span class="bold">2.</span>&nbsp;<span class="normal"><?php echo Yii::t('mainApp', '¿Hasta qué nivel quieres llegar?') ?></span>
            </div>
            <ul class="registro">
                <li>
                    <?php echo $form->error($model,'nivel'); ?>
                </li>
                <?php
                    $radioList3 = $form->radioButtonList($model, 'nivel', array('1'=> Yii::t('mainApp', 'Quiero poder comunicarme en inglés básico'), '2' => Yii::t('mainApp', 'Quiero hablar con fluidez'), '3' => Yii::t('mainApp', 'Quiero hablar y escribir como un nativo')), array('separator' => '<br />') );
                    $listReplace3 = preg_replace('<span([^\>]+)(s*[^\/])>', '', $radioList3,-1,$count);
                    $List3 = str_replace('</span>', '', $listReplace3);
                    $List3 = str_replace('<<', '<', $listReplace3);
                    $ListFinal3 = explode("<br />", $List3);
                    foreach($ListFinal3 as $sublist3){
                        echo '<li>'.$sublist3.'</li>';
                    }
                ?>
            </ul>
        </div>
    </div>
    <div class="clear"></div>
    <div class="left">
        <div class="left">
            <div class="question">
                  <span class="bold">3.</span>&nbsp;<span class="normal"><?php echo Yii::t('mainApp', '¿Has estudiado inglés previamente?') ?></span>
            </div>
            <ul class="registro">
                <li>
                    <?php echo $form->error($model,'engPrev'); ?>
                    <?php echo $form->error($model,'selEngPrev'); ?>
                </li>
                <?php 
                    $radioList2 = $form->radioButtonList($model, 'engPrev', array('0' => Yii::t('mainApp', 'Si'), '1' => Yii::t('mainApp', 'No')), array('separator' => '<br />'));
                    $listReplace2 = preg_replace('<span([^\>]+)(s*[^\/])>', '', $radioList2,-1,$count);
                    $List2 = str_replace('</span>', '', $listReplace2);
                    $List2 = str_replace('<<', '<', $listReplace2);
                    $ListFinal2 = explode("<br />", $List2);
                    $c = 0;
                    $dropDown = $form->dropDownList($model, "selEngPrev", array('1' => Yii::t('mainApp', 'Colegio'), '2' => Yii::t('mainApp', 'Academia'), '3' => Yii::t('mainApp', 'Online'), '4' => Yii::t('mainApp', 'Clases particulares'), '5' => Yii::t('mainApp', 'Otros')), array('empty' => Yii::t('mainApp', '...')));
                    foreach($ListFinal2 as $sublist2){
                        if($c===0)
                            echo '<li>'.$sublist2.'&nbsp;'.$dropDown.'</li>';
                        else
                            echo '<li>'.$sublist2.'</li>';
                        $c++;
                    }
                ?>
            </ul>
        </div>
        <div class="left marginLeft4">
            <div class="question">
                  <span class="bold">4.</span>&nbsp;<span class="normal"><?php echo Yii::t('mainApp', '¿A qué te dedicas?') ?></span>
            </div>
            <ul class="registro">
                <li>
                    <?php echo $form->error($model,'dedicacion'); ?>
                    <?php echo $form->dropDownList($model, "dedicacion", array('1' => Yii::t('mainApp', 'Estudio'), '2' => Yii::t('mainApp', 'Trabajo'), '3' => Yii::t('mainApp', 'Busco trabajo'), '4' => Yii::t('mainApp', 'Estoy jubilado')), array('empty' => Yii::t('mainApp', '...'))) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="clear"></div>
    <div class="left">
        <div class="left">
            <div class="question">
                  <span class="bold">5.</span>&nbsp;<span class="normal"><?php echo Yii::t('mainApp', 'Fecha de nacimiento') ?></span>
            </div>
            <ul class="registro">
                <li>
                    <?php echo $form->error($model,'birthDate'); ?>
                    <?php echo $form->textField($model,'day', array('maxlength'=>'2', 'size' => 2)); ?>
                    <?php echo $form->textField($model,'month', array('maxlength'=>'2', 'size' => 2)); ?>
                    <?php echo $form->textField($model,'year', array('maxlength'=>'4', 'size' => 4, 'class' => 'year')); ?>
                    dd/mm/yyyy
                </li>
            </ul>
        </div>
        <div class="left marginLeft4">
            <div class="question">
                  <span class="bold">6.</span>&nbsp;<span class="normal"><?php echo Yii::t('mainApp', 'Eres') ?></span>
            </div>
            <ul class="registro">
                <li>
                    <?php echo $form->error($model,'genero'); ?>
                </li>
                <?php 
                    $radioList = $form->radioButtonList($model, 'genero', array('M'=> Yii::t('mainApp', 'Hombre'),'F'=> Yii::t('mainApp', 'Mujer')), array('separator' => '<br />') );
                    $listReplace = preg_replace('<span([^\>]+)(s*[^\/])>', '', $radioList,-1,$count);
                    $List = str_replace('</span>', '', $listReplace);
                    $List = str_replace('<<', '<', $listReplace);
                    $ListFinal = explode("<br />", $List);
                    foreach($ListFinal as $sublist){
                        echo '<li>'.$sublist.'</li>';
                    }
                ?>
            </ul>
        </div>
    </div>
    <div class="clear"></div>
    <p class="agradecimiento"><?php echo Yii::t('mainApp', '¡Muchas gracias por tu colaboración!') ?></p>
    <div align="center">
        <button type="button" onclick="campusPopup.renderAjaxRequest($('#formUserData'))" class="registrerSendButton marginTop8"><?= Yii::t('mainApp', 'Enviar') ?></button>
    </div>
    <?php
    $this->endWidget();
    ?>
    <div class="clear"></div>
</div>