<div class="popupIntroduccion">
    <p class="saludo"><?php echo Yii::t('mainApp', '¡Hola')." ".$name ?>!</p>
    <span class="saludoDespues"><?php echo Yii::t('mainApp', 'Antes de empezar a aprender,') ?></span>
    <span class="final"><?php echo Yii::t('mainApp', '¡háblanos de ti!') ?></span>
</div>
<div class="pasos">
    <span class="pasoHecho left"><?php echo Yii::t('mainApp', 'Tus datos') ?></span>
    <span class="pasoAnteriorHechoSiguienteNoHechoIconPrev left"></span>
    <span class="pasoAnteriorHechoSiguienteNoHechoIcon left"></span>
    <span class="pasoNoHecho left"><?php echo Yii::t('mainApp', 'Tu nivel de inglés') ?></span>
    <span class="pasosNoHechosIconPrev left"></span>
    <span class="pasosNoHechosIcon left"></span>
    <span class="pasoNoHecho left"><?php echo Yii::t('mainApp', 'Tu plan') ?></span>
</div>
<div class="clear"></div>