<div class="bodyPopUp">
    <div id="mgmPopup" align="center" style="padding: 20px;">
        <h3><?php echo Yii::t('mainApp', 'welcome_mgm_premium_title'); ?></h3>
        <p id="welcome_mgm_text"><?php echo Yii::t('mainApp', 'welcome_mgm_premium_text'); ?></p>
        <button type="button" class="registrerSendButton" onclick="$('#welcomeForm').dialog('destroy');document.location.href='<?php echo Yii::app()->createAbsoluteUrl('course/index') ?>'">
            <?php echo Yii::t('mainApp', 'Comenzar') ?>
        </button>
    </div>
</div>