<div class="bodyPopUp">
    <div class="popupIntroduccion">
        <span class="saludoDespues"><?php echo Yii::t('mainApp', 'Por último, escoge el plan por el que quieres empezar a aprender inglés online') ?></span>
    </div>
    
    <div class="pasos">
        <span class="pasoHecho left"><?php echo Yii::t('mainApp', 'Tus datos') ?></span>
        <span class="pasoAnteriorHechoSiguienteNoHechoIconPrev left"></span>
        <span class="pasosHechosIcon left"></span>
        <span class="paso2o3Hecho left"><?php echo Yii::t('mainApp', 'Tu nivel de inglés') ?></span>
        <span class="pasoAnteriorHechoSiguienteNoHechoIconPrev left"></span>
        <span class="pasosHechosIcon left"></span>
        <span class="paso2o3Hecho left width168"><?php echo Yii::t('mainApp', 'Tu plan') ?></span>
    </div>
    
    <div class="clear"></div>
   
    <style>
        
    </style>

 
    
<table class="table-plans" style="margin:20px 0px;">
    <thead>
        <tr>
            <th class="no-border-bottom"></th>
            <th class="blue">Free</th>
            <th class="red">Premium</th>
            <th class="orange no-border-bottom">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th class="" scope="row">
                <span class="left"><?php echo Yii::t('mainApp', 'level-test') ?></span> 
                <div class="helpIconPopup left"><span class="flechaHelpBox left"></span><div class="floatHelpBox left"><?php echo Yii::t('mainApp', 'level-testdesc')?></div></div></li>
            </th>
            <td class=" shadow-right"><span class="ok"></span></td>
            <td class=" "><span class="ok"></span></td>
            <td class="shadow-left no-border-bottom">&nbsp;</td>
        </tr>
        <tr>
            <th class="soft-grey" scope="row">
                <span class="left"><?php echo Yii::t('mainApp', 'progress') ?></span> 
                
                <div class="helpIconPopup left"><span class="flechaHelpBox left"></span><div class="floatHelpBox left"><?php echo Yii::t('mainApp', 'progressdesc')?></div></div></li>

            </th>
            <td class="soft-grey shadow-right"><span class="ok"></span></td>
            <td class="soft-grey "><span class="ok"></span></td>
            <td class="shadow-left no-border-bottom">&nbsp;</td>
        </tr>
        <tr>
            <th class="" scope="row">
                <span class="left"><?php echo Yii::t('mainApp', 'videoclass') ?></span> 
                
                <div class="helpIconPopup left"><span class="flechaHelpBox left"></span><div class="floatHelpBox left"><?php echo Yii::t('mainApp', 'videoclassdesc')?></div></div></li>
            </th>
            <td class=" shadow-right"><span class="ok"></span></td>
            <td class=" "><span class="ok"></span></td>
            <td class="shadow-left no-border-bottom">&nbsp;</td>
        </tr>
        <tr>
            <th class="soft-grey" scope="row">
                <span class="left"><?php echo Yii::t('mainApp', 'interactive-grammar') ?></span> 
                
                <div class="helpIconPopup left"><span class="flechaHelpBox left"></span><div class="floatHelpBox left"><?php echo Yii::t('mainApp', 'interactive-grammardesc')?></div></div></li>

            </th>
            <td class="soft-grey shadow-right"><span class="ok"></span></td>
            <td class="soft-grey "><span class="ok"></span></td>
            <td class="shadow-left no-border-bottom">&nbsp;</td>
        </tr>
        <tr>
            <th class="" scope="row">
                <span class="left"><?php echo Yii::t('mainApp', 'course') ?></span> 
                
                <div class="helpIconPopup left"><span class="flechaHelpBox left"></span><div class="floatHelpBox left"><?php echo Yii::t('mainApp', 'coursedesc')?></div></div></li>
            </th> 
            <td class=" shadow-right" style="color: green; font-size: 14px;"><?php echo Yii::t('mainApp', '1 unidad') ?></td>
            <td class=" " style="color: green; font-size: 14px;"><?php echo Yii::t('mainApp', '144 unidades') ?></td>
            <td class="shadow-left no-border-bottom">&nbsp;</td>
        </tr>
        <tr>
            <th class="soft-grey" scope="row">
                <span class="left"><?php echo Yii::t('mainApp', 'certificate') ?></span> 
                
                <div class="helpIconPopup left"><span class="flechaHelpBox left"></span><div class="floatHelpBox left"><?php echo Yii::t('mainApp', 'certificatedesc')?></div></div></li>

            </th>
            <td class="soft-grey shadow-right"><span class="x"></span></td>
            <td class="soft-grey "><span class="ok"></span></td>
            <td class="shadow-left no-border-bottom">&nbsp;</td>
        </tr>
        <tr>
            <th class="" scope="row">
                <span class="left"><?php echo Yii::t('mainApp', 'consult-the-teacher') ?></span> 
                
                <div class="helpIconPopup left"><span class="flechaHelpBox left"></span><div class="floatHelpBox left"><?php echo Yii::t('mainApp', 'professordesc')?></div></div></li>

            </th>
            <td class=" shadow-right"><span class="x"></span></td>
            <td class=" "><span class="ok"></span></td>
            <td class="shadow-left no-border-bottom">&nbsp;</td>
        </tr>

        <tr>
            <td class="no-border-bottom"></td>

            <td class="no-border-bottom shadow-right">
                <a class="btn-blue" style="font-size: 16px; cursor: pointer;" onclick="$('#welcomeForm').dialog('destroy');document.location.reload();" title="<?php echo Yii::t('mainApp', 'Quiero empezar a estudiar gratis en') ?>"><?php echo Yii::t('mainApp', 'Quiero empezar a estudiar gratis en') ?> </a>
            </td>
            <td class="no-border-bottom ">
                <a class="btn-red" style="font-size: 16px;"  href="<?php echo Yii::app()->createUrl(LAND_PAYMENT) ?>" title="<?php echo Yii::t('mainApp', 'Quiero empezar sin limitaciones en') ?> "><?php echo Yii::t('mainApp', 'Quiero empezar sin limitaciones en') ?></a>
            </td>
            <td class="no-border-bottom shadow-left">&nbsp;</td>
        </tr>
        <tr>
            <td class="no-border-bottom"></td>
            <td class="no-border-bottom shadow-corner-right"></td>
            <td class="no-border-bottom shadow-bottom"></td>
            <td class="no-border-bottom shadow-corner-left"></td>
        </tr>
    </tbody>
</table>

   
</div>