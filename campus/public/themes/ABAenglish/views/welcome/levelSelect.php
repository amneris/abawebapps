<div class="bodyPopUp <?php if (isset($closeBtn)) echo 'popupCloseBtn' ?>">
    <?php
        switch($header)
        {
            case 'full':
                $this->renderPartial('//welcome/levelSelectHeaderFull');
                break;
            default:
                $this->renderPartial('//welcome/levelSelectHeader', array('name' => $name));
                break;
        }
        $form = $this->beginWidget('CActiveForm', array('id' => 'formLevelSelect', 'action' => Yii::app()->createAbsoluteUrl('welcome/levelSelect')));
    ?>
    <div class="clear"></div>

    <ul class="welcomeLevelTest" style="margin-top: 20px;">
        <li>
            <div class="inner-content">
                <input type="radio" id="curLevel_a1" name="nivelActual" class="right" value="1" checked="checked"/>
                <label for="curLevel_a1" class="levelLabel left">
                    <div class="levelIcon">A1</div>
                    <span>Beginners (A1)</span>
                </label>
                <div class="helpIconPopup left">
                    <span class="flechaHelpBox left"></span>
                    <div class="floatHelpBox left"><?php echo Yii::t('mainApp', 'Es la primera vez que estudias inglés o quieres empezar otra vez desde lo más básico.') ?></div>
                </div>
            </div>
        </li>
        <li>
            <div class="inner-content">
                <input type="radio" id="curLevel_a2" name="nivelActual" class="right" value="2" />
                <label for="curLevel_a2" class="levelLabel left">
                    <div class="levelIcon">A2</div>
                    <span>Lower intermediate (A2)</span>
                </label>
                <div class="helpIconPopup left">
                    <span class="flechaHelpBox left"></span>
                    <div class="floatHelpBox left"><?php echo Yii::t('mainApp', 'Puedes comprender textos breves y comunicarte con frases sencillas.') ?></div>
                </div>
            </div>
        </li>
        <li>
            <div class="inner-content">
                <input type="radio" id="curLevel_b1" name="nivelActual" class="right" value="3" />
                <label for="curLevel_b1" class="levelLabel left">
                    <div class="levelIcon">B1</div>
                    <span>Intermediate (B1)</span>
                </label>
                <div class="helpIconPopup left">
                    <span class="flechaHelpBox left"></span>
                    <div class="floatHelpBox left"><?php echo Yii::t('mainApp', 'Eres capaz de describir situaciones y expresarte con un lenguaje coherente sobre cuestiones que conoces bien.') ?></div>
                </div>
            </div>
        </li>
        <li>
            <div class="inner-content">
                <input type="radio" id="curLevel_b2" name="nivelActual" class="right" value="4" />
                <label for="curLevel_b2" class="levelLabel left">
                    <div class="levelIcon">B2</div>
                    <span>Upper intermediate (B2)</span>
                </label>
                <div class="helpIconPopup left">
                    <span class="flechaHelpBox left"></span>
                    <div class="floatHelpBox left"><?php echo Yii::t('mainApp', 'Puedes entender los puntos clave de textos complejos de manera autónoma y eres capaz de expresarte de forma espontánea e improvisada.') ?></div>
                </div>
            </div>
        </li>
        <li>
            <div class="inner-content">
                <input type="radio" id="curLevel_b2c1" name="nivelActual" class="right" value="5" />
                <label for="curLevel_b2c1" class="levelLabel left">
                    <div class="levelIcon doubleLine">B2<br>C1</div>
                    <span>Advanced (B2-C1)</span>
                </label>
                <div class="helpIconPopup left">
                    <span class="flechaHelpBox left"></span>
                    <div class="floatHelpBox left"><?php echo Yii::t('mainApp', 'Puedes entender textos complejos y comunicarte con hablantes nativos con fluidez.') ?></div>
                </div>
            </div>
        </li>
        <li>
            <div class="inner-content">
                <input type="radio" id="curLevel_c1" name="nivelActual" class="right" value="6" />
                <label for="curLevel_c1" class="levelLabel left">
                    <div class="levelIcon">C1</div>
                    <span>Business (C1)</span>
                </label>
                <div class="helpIconPopup left">
                    <span class="flechaHelpBox left"></span>
                    <div class="floatHelpBox left"><?php echo Yii::t('mainApp', 'Tienes el nivel Advanced y quieres profundizar en el inglés para los negocios.') ?></div>
                </div>
            </div>
        </li>
    </ul>
    <div>
        <p class="levelTestSeparator">
            <span><?php echo Yii::t('mainApp', 'level-test-separator') ?></span>
        </p>
        <ul class="welcomeLevelTest">
            <li>
                <div class="inner-content">
                    <input type="radio" id="curLevel_test" name="nivelActual" class="right" value="-1" />
                    <label for="curLevel_test" class="levelLabel left">
                        <div class="levelIcon">&#10003;</div>
                        <span><?php echo Yii::t('mainApp', 'level-test') ?></span>
                    </label>
                    <div class="helpIconPopup left">
                        <span class="flechaHelpBox left"></span>
                        <div class="floatHelpBox left"><?php echo Yii::t('mainApp', 'level-test-helpBox') ?></div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div align="center">
        <button type="button" onclick="levelTest.start()" class="registrerSendButton marginTop8"><?= Yii::t('mainApp', 'Continuar') ?></button>
    </div>
    <?php
    $this->endWidget();
    ?>
    <div class="clear"></div>
</div>