<?php
$units = new UnitsCourses();
$level = Yii::app()->user->currentLevel;
$moUnit = new UnitCourse($units->getStartUnitByLevel($level));
$goToCourse = Yii::app()->createAbsoluteUrl('course/AllUnits/unit/' . $moUnit->getFormatUnitId($moUnit->getUnitId()));
$goToCourse = 'document.location.href=\''.$goToCourse.'\'';
?>
<div class="bodyPopUp">
    <div id="mgmPopup" align="center">
        <h3><?php echo Yii::t('mainApp', 'welcome_mgm_title'); ?></h3>
        <p id="welcome_mgm_text"><?php echo Yii::t('mainApp', 'welcome_mgm_text'); ?></p>
        <div id="mgmPre">
            <button type="button" id="popup-closeAction" class="registrerSendButtonGrey" onclick="<?php echo $goToCourse ?>"><?php echo Yii::t('mainApp', 'welcome_mgm_b2') ?></button>
            <button type="button" id="popup-registrerSendButton" class="registrerSendButton ab-testing-preinvite" onclick="memberGetMember.renderInvite();"><?php echo Yii::t('mainApp', 'welcome_mgm_b1') ?></button>
        </div>
        <div id="mgmWidget" style="display:none;">
            <div class="loading"></div>
            <iframe src="<?php echo Yii::app()->createAbsoluteUrl('invite/index'); ?>" scrolling="auto" frameborder="0" marginheight="0"></iframe>
        </div>
    </div>
</div>