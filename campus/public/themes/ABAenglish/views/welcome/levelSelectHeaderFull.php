<div class="popupIntroduccion">
    <span class="saludoDespues"><?php echo Yii::t('mainApp', 'Ahora, indícanos por qué nivel quieres empezar') ?></span>
</div>
<div class="pasos">
    <span class="pasoHecho left"><?php echo Yii::t('mainApp', 'Tus datos') ?></span>
    <span class="pasoAnteriorHechoSiguienteNoHechoIconPrev left"></span>
    <span class="pasosHechosIcon left"></span>
    <span class="paso2o3Hecho left"><?php echo Yii::t('mainApp', 'Tu nivel de inglés') ?></span>
    <span class="pasoAnteriorHechoSiguienteNoHechoIconPrev left"></span>
    <span class="pasoAnteriorHechoSiguienteNoHechoIcon left"></span>
    <span class="pasoNoHecho left width168"><?php echo Yii::t('mainApp', 'Tu plan') ?></span>
</div>