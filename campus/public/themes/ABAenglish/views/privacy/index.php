<?php
if (isset($withBreadcrumb) && $withBreadcrumb === true) {
    $this->widget(
        'application.components.widgets.BreadcrumbHeader',
        array('curMenuSectionTab' => "Política de privacidad")
    );
}
?>
<div class="divRightGeneral left">
    <p class="termsTitle"><?php echo Yii::t('mainApp', 'Política de privacidad del curso de inglés on line') ?></p>
    <?php if (Yii::app()->language != 'ru'){?>
    <p class="PTerms">
        <?php echo Yii::t('mainApp', '¿Cómo usaremos la información que nos facilitas?') ?>
    </p>
    <?php } ?>
    <p class="PTerms">
        <?php echo Yii::t('mainApp', 'Toda la información que nos transmitas será tratada de manera confidencial. Tan sólo tú y las personas de ') ?><span class="bold">ABA English</span><?php echo Yii::t('mainApp', ' que estén a tu servicio podrán conocer tus datos personales. Y lo podemos certificar porque nuestras páginas web están alojadas en un servidor seguro. Nuestro servidor en donde se encuentran nuestras páginas y que recibe tu información, cuenta con los más avanzados sistemas de protección, de forma que nadie que carezca de autorización puede acceder a la información que contiene') ?>.
    </p>
    <p class="PTerms">
        <?php echo Yii::t('mainApp', 'De conformidad con lo dispuesto en el art. 5 de la Ley Orgánica 15/1999 sobre Protección de Datos de Carácter Personal (LOPD), te informamos que sus datos personales formarán parte de ficheros titularidad de ENGLISH WORLDWIDE, S.L. con la finalidad de prestar adecuadamente el servicio solicitado, dar cumplimiento a los requisitos de facturación y contabilidad de la compañía, realizar análisis estadísticos, verificar tarjetas de crédito y otras formas de pago, así como realizar comunicaciones comerciales por correo electrónico sobre otros productos que podrían ser de su interés') ?>.
    </p>
    <p class="PTerms">
        <?php echo Yii::t('mainApp', 'Tus datos personales serán tratados con la máxima confidencialidad y con el deber de secreto requeridos. ENGLISH WORLDWIDE, S.L. ha adoptado las medidas técnicas y organizativas definidas en el Real Decreto 1720/2007, de 21 de diciembre, por el que se aprueba el Reglamento de desarrollo de a LOPD 15/1999') ?>.
    </p>
    <p class="PTerms">
        <?php echo Yii::t('mainApp', 'Puedes ejercitar sus derechos de acceso, rectificación, cancelación y oposición mediante comunicación escrita, adjuntando fotocopia del DNI o documento identificativo equivalente, ante ENGLISH WORLDWIDE, S.L. en C/ Guitard, 45. 08014 Barcelona') ?>.
    </p>
</div>