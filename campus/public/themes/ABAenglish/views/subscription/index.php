<?php
/**
 * @var integer $typeSubscriptionStatus : SUBS_PREMIUM_A_CARD_A, etc.
 * @var string $dateToPay YYYY-MM-DD
 * @var string $dateCancel; YYYY-MM-DD
 * @var float $amountPrice
 * @var AbaUserCreditForms $moUserCreditForm
 * @var integer $kindMethod
 * @var string $productDesc
 * @var string $periodDescKey
 * @var string $expirationDate
 * @var string $specialMessage
 * @var AbaPaymentsBoleto $moBoleto
 */
$this->pageTitle = Yii::app()->name . ' - Profile';
$this->breadcrumbs = array( 'Profile'  );

$aAllCards = HeViews::getListOfCards();

// Navigation index (BreadCrumb):
$this->widget('application.components.widgets.BreadcrumbHeader', array('curMenuSectionTab' => "w_subscription"));
// Row of Tabs, auto-detect the active one:
$this->renderPartial( "//profile/profileTabs", array("typeSubscriptionStatus" => $typeSubscriptionStatus) );

?>

<script>
    function enviodatos()
    {
        document.getElementById("aa").submit();
    }
</script>

<form id="aa" method="post" action="<?php Yii::app()->createUrl('subscription/changecard') ?>">
    <input type="hidden" name="ada" value="<?php echo $dateToPay; ?>" />
</form>
<div class="clear"></div>
<div class="form div-subscription-form left">
    <p class="titulosProfile left"><?php echo Yii::t('mainApp', 'w_subscription') ?></p>
    <div class="clear"></div>

    <?php
    if($specialMessage){

        echo '<div id="msgConfirmation">'.Yii::t('mainApp', $specialMessage).'</div>';
    }
    ?>
<!--  STATUS+++++++++++++++   -->
    <div class ="titlesubscribe"> <?php echo Yii::t('mainApp', 'state_subscription'); ?></div>
    <div class ="textsubscribe" >
        <?php
        $txtPeriodicity = Yii::t('mainApp', $periodDescKey);
        switch ($typeSubscriptionStatus) {
            case SUBS_PREMIUM_A_CARD_A:
            case SUBS_PREMIUM_A_CARD_C:
            case SUBS_PREMIUM_A_CARD_M:
            case SUBS_PREMIUM_A_CARD_AD:
            case SUBS_PREMIUM_A_CARD_IOS:
            case SUBS_PREMIUM_A_ZUORA:
            case SUBS_PREMIUM_A_PAYPAL:
                $sentence2Active = Yii::t('mainApp', 'premiumactivo2');
                echo Yii::t('mainApp', 'premiumactivo1');
                $sDateToPay = HeDate::removeTimeFromSQL($dateToPay);
                $sDateToPay = HeDate::SQLToUserRegionalDate($sDateToPay, '-', Yii::app()->user->getCountryId());
                echo $sDateToPay;
                echo " ($txtPeriodicity) " . $sentence2Active . $amountPrice . Yii::t('mainApp', 'premiumactivo3',
                    array('{userEmail}' => Yii::app()->user->getEmail()));
                break;

            case SUBS_PREMIUM_A_ANDROID:

                $sentence2Active = Yii::t('mainApp', 'premiumactivo2_android');
                echo Yii::t('mainApp', 'premiumactivo1_android');
                $sDateToPay = HeDate::removeTimeFromSQL($dateToPay);
                $sDateToPay = HeDate::SQLToUserRegionalDate($sDateToPay, '-', Yii::app()->user->getCountryId());
                echo $sDateToPay;
                echo " ($txtPeriodicity) " . $sentence2Active . $amountPrice . Yii::t('mainApp',
                    'premiumactivo3_android');
                break;

            case SUBS_PREMIUM_C_CARD_A:
            case SUBS_PREMIUM_C_CARD_C:
            case SUBS_PREMIUM_C_CARD_M:
            case SUBS_PREMIUM_C_CARD_AD:
            case SUBS_PREMIUM_C_CARD_IOS:
            case SUBS_PREMIUM_C_BOLETO_BR:
            case SUBS_PREMIUM_C_PAYPAL:
            case SUBS_PREMIUM_C_ANDROID:
                echo Yii::t('mainApp', 'premiumcancelado1');
                $sDateToPay = HeDate::removeTimeFromSQL($dateToPay);
                $sDateToPay = HeDate::SQLToUserRegionalDate($sDateToPay, '-', Yii::app()->user->getCountryId());
                echo $sDateToPay;
                echo Yii::t('mainApp', 'premiumcancelado2', array('{userEmail}' => Yii::app()->user->getEmail()));
                break;

            case SUBS_EXPREMIUM_CARD:
            case SUBS_EXPREMIUM_PAYPAL:
                echo Yii::t('mainApp', 'expremium1', array('{userEmail}' => Yii::app()->user->getEmail()));
                break;

            case SUBS_FREE:
            case SUBS_EXPREMIUM_THROUGHPARTNER:
                if ($moBoleto) {
                    $btnPrintBoleto = '<a href="' . $moBoleto->url . '" target=blank class="printBoleto">' . Yii::t('mainApp',
                        'key_PrintOutBoleto') . '</a>';
                    $dueDate = HeDate::SQLToUserRegionalDate(HeDate::removeTimeFromSQL($moBoleto->dueDate), '-',
                      Yii::app()->user->getCountryId());
                    $requestDate = HeDate::SQLToUserRegionalDate(HeDate::removeTimeFromSQL($moBoleto->requestDate), '-',
                      Yii::app()->user->getCountryId());
                    // If the user has requested a Boleto lately.
                    echo Yii::t('mainApp', 'key_boletoPending', array(
                      '{requestDate}' => $requestDate,
                      '{dueDate}' => $dueDate,
                      '{amountPrice}' => $amountPrice,
                      '{product}' => $txtPeriodicity,
                      '{linkPrintBoleto}' => $btnPrintBoleto,
                    ));
                } else {
                    echo Yii::t('mainApp', 'free1', array('{userEmail}' => Yii::app()->user->getEmail()));
                }
                break;

            case SUBS_PREMIUM_THROUGHPARTNER:
            case SUBS_PREMIUM_THROUGHB2B:
                echo Yii::t('mainApp', 'activogroupon') . Yii::t('mainApp', 'activogroupon2');
                break;

            default:
                echo "";
                break;
        }
        ?>

     </div>
    <div class="clear"></div>
    <?php if(($typeSubscriptionStatus)!=SUBS_EXPREMIUM_CARD){ ?>

    <!--  ACTIVE SUBSCRIPTION +++++++++++++++   -->
<?php
        switch($typeSubscriptionStatus) {
            case SUBS_PREMIUM_A_CARD_A:
            case SUBS_PREMIUM_A_CARD_M:
            case SUBS_PREMIUM_C_CARD_A:
            case SUBS_PREMIUM_C_CARD_M:
            case SUBS_PREMIUM_A_CARD_AD:
            case SUBS_PREMIUM_A_CARD_IOS:
            case SUBS_PREMIUM_C_CARD_AD:
            case SUBS_PREMIUM_A_ZUORA:
            case SUBS_PREMIUM_C_CARD_IOS:
            case SUBS_PREMIUM_A_CARD_C:
            case SUBS_PREMIUM_C_CARD_C:
            case SUBS_PREMIUM_A_ANDROID:
            case SUBS_PREMIUM_C_ANDROID:
                break;
            default:
?>
        <div class="titlesubscribe">   <?php echo Yii::t('mainApp', 'payment_subscription'); ?></div>
<?php
                break;
        }
?>
    <div class="textsubscribe">
    <?php switch($typeSubscriptionStatus){
        case SUBS_PREMIUM_C_BOLETO_BR:
        case SUBS_EXPREMIUM_CARD:

                $creditNumber = HeViews::formatCardNumberSecret($moUserCreditForm->cardNumber, false);
                echo Yii::t('mainApp', 'key_subscription_paymentC');

                echo "</br>".$aAllCards[intval($moUserCreditForm->kind)];

                echo "<strong><div class=textright>".Yii::t('mainApp', 'numerotarjeta')."</strong> ".$creditNumber;
                echo "</br><strong>".Yii::t('mainApp', 'fechacaducidad')."</strong> ".$moUserCreditForm->cardYear."/".$moUserCreditForm->cardMonth."</br></br></div>";

            if ($typeSubscriptionStatus!=SUBS_EXPREMIUM_CARD){
                echo "</br></br>".CHtml::link(Yii::t('mainApp', 'key_changecreditcard'),Yii::app()->createUrl('subscription/changecard'),
                        array('id'=>'idAChangeCreditCard','class' => 'cambiarlink', $dateToPay))."</div>";
            }
            else{
                echo "</div>";
            }

            break;

        case SUBS_PREMIUM_A_CARD_A:
        case SUBS_PREMIUM_A_CARD_M:
        case SUBS_PREMIUM_C_CARD_A:
        case SUBS_PREMIUM_C_CARD_M:
        case SUBS_PREMIUM_A_CARD_AD:
        case SUBS_PREMIUM_A_CARD_IOS:
        case SUBS_PREMIUM_C_CARD_AD:
        case SUBS_PREMIUM_C_CARD_IOS:
        case SUBS_PREMIUM_A_CARD_C:
        case SUBS_PREMIUM_C_CARD_C:
        case SUBS_PREMIUM_A_ANDROID:
        case SUBS_PREMIUM_C_ANDROID:
            echo " ";
            break;

        case SUBS_PREMIUM_A_PAYPAL:
        case SUBS_PREMIUM_C_PAYPAL:
        case SUBS_EXPREMIUM_PAYPAL:
            //echo Yii::t('mainApp', 'key_subscription_paymentP');
            echo $aAllCards[intval($moUserCreditForm->kind)]."</div>"; // Email account PayPal = $moUserCreditForm->cardName.
            break;

        case SUBS_FREE:
        case SUBS_EXPREMIUM_THROUGHPARTNER:
        case SUBS_PREMIUM_THROUGHPARTNER:
        case SUBS_PREMIUM_THROUGHB2B:
            echo Yii::t('mainApp', 'key_subscription_payment1')."</div>";
            break;

        default:
            echo " ";
            break;

    }
    ?>
     <?php } ?>
    <?php if(($typeSubscriptionStatus)==SUBS_FREE || ($typeSubscriptionStatus)==SUBS_EXPREMIUM_THROUGHPARTNER ||
            ($typeSubscriptionStatus)==SUBS_EXPREMIUM_PAYPAL || ($typeSubscriptionStatus)==SUBS_EXPREMIUM_CARD){ ?>
        <div class ="titlesubscribe">
            <?php echo Yii::t('mainApp', 'active_subscription') ?>
        </div>
        <div class ="textsubscribe">
        <?php
          echo Yii::t('mainApp', 'activarfree');
          $divGotoPremium = '<div id="divGoActivatePremium" class="cmp_haztePremium1"><span class="cmp_pasateaNoBold">'.Yii::t('mainApp', 'iconorojo').'</span></span></div>';
          echo CHtml::link($divGotoPremium, '', array('id'=>'aLinkGoToActivateCard'));
        ?>
        </div>
        <?php if($moBoleto){ ?>
            <div class="textsubscribe">
                <?php echo Yii::t('mainApp', 'key_PayDebtAndConvertToPremium');?>
            </div>
            <div style="text-align: center; height: 60px;">
                <a href="<?php echo $moBoleto->url; ?>" target=blank class="printBoletoButton" style="width: 250px;">
                    <?php echo Yii::t('mainApp', 'key_PrintOutBoleto'); ?>
                </a>
                <br/>
            </div>
        <?php } ?>
    <?php }
    /* ----------------- Section to access the change products link ------------------------------*/
    if (intval(Yii::app()->config->get('ENABLE_CHANGE_PRODUCT'))) {
        switch ($typeSubscriptionStatus) {
            case SUBS_PREMIUM_A_CARD_A:
            case SUBS_PREMIUM_A_CARD_C:
            case SUBS_PREMIUM_A_CARD_M:
            case SUBS_PREMIUM_A_CARD_AD:
                ?>
                <div class="titlesubscribe"><?php echo Yii::t('mainApp', 'key_plansubscription') ?>:</div>
                <div class="textsubscribe"><?php
                    echo "" . CHtml::link(Yii::t('mainApp', 'key_changeplans'), Yii::app()->createUrl('payments/ChangePlan'),
                            array('id' => 'idAChangePlan', 'class' => 'cambiarlink')) . "</div>"; ?>
                </div>
                <?php
                break;
            default:
                //case SUBS_PREMIUM_A_PAYPAL: Probably in the future
                // nothing at all;
                break;
        }
    }
    /* -----------------------------------------------------------------------------------------*/
    ?>
    </div>
    <div class="clear"></div>
