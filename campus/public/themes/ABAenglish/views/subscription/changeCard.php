<?php
/* @var CreditCardForm $frmMoChangeCredit */
/* @var string $msgValidation */
/* @var array $listOfCreditCards */
/* @var string $urlCardFormProcess */
/* @var string $urlPayPalProcess */
/* @var AbaUser $moUser */
/* @var string $currency */
/* @var array $urlsFormProcess */


$classCreditCardDetails = ($frmMoChangeCredit->idPayMethod == PAY_METHOD_PAYPAL) ? "creditCardDetailsHide" : "creditCardDetailsShow";
$actionFormDefault = ($frmMoChangeCredit->idPayMethod == PAY_METHOD_PAYPAL) ? $urlPayPalProcess : $urlCardFormProcess;

$this->pageTitle = Yii::app()->name . ' - Profile';
$this->breadcrumbs = array('Profile');

// Navigation index (BreadCrumb):
$this->widget('application.components.widgets.BreadcrumbHeader', array('curMenuSectionTab' => "w_subscription"));
// Row of Tabs, auto-detect the active one:
$this->renderPartial("//profile/profileTabs");
?>

<script type="text/javascript">
    var urlCardFormProcess = '<?php echo $urlCardFormProcess; ?>';
    var urlPayPalProcess = '<?php echo $urlPayPalProcess; ?>';
    var PAY_METHOD_CARD = '<?php echo PAY_METHOD_CARD; ?>';
    var PAY_METHOD_PAYPAL = '<?php echo PAY_METHOD_PAYPAL; ?>';
</script>
<div class="clear"></div>

<div class="form div-subscription-form left">

    <p class="titulosProfile">
        <?php echo Yii::t('mainApp', 'w_subscription') ?>
    </p>

    <!--*******START***************FORM FIELDS*********************START**************** -->
    <div id="divCreditCardDetails" class="<?php echo $classCreditCardDetails; ?>">
        <?php
        /* @var CActiveForm $formCreditCard */
        $formCreditCard = $this->beginWidget(
            'CActiveForm',
            array(
                'id' => 'changecredit-form',
                'action' => $actionFormDefault,
                'enableClientValidation' => false,
                'enableAjaxValidation' => false,
                'clientOptions' => array(
                  'validateOnSubmit' => true,
                ),
                'htmlOptions' => array(
                  'class'=>'ab-subscription-form clearfix',
                ),
            )
        );
        $aCountriesAllPago = array(30);

        if (in_array(intval(trim($moUser->countryId)), $aCountriesAllPago)) {
            ?>
            <div class="form-group clearfix">
                <?php echo $formCreditCard->labelEx($frmMoChangeCredit, 'cpfBrasil', array('class'=>'label')); ?>

                <div class="control-wrapper">
                    <?php echo $formCreditCard->dropDownList(
                        $frmMoChangeCredit,
                        'typeCpf',
                        array(PAY_ID_BR_CPF => PAY_ID_BR_CPF, PAY_ID_BR_CNPJ => PAY_ID_BR_CNPJ),
                        array('class' =>'selector-cpf')
                    ); ?>

                    <?php echo $formCreditCard->textField(
                        $frmMoChangeCredit,
                        'cpfBrasil',
                        array("maxlength" => 19, 'class' =>'input-cpf')
                    ); ?>

                    <?php echo $formCreditCard->error($frmMoChangeCredit, 'cpfBrasil'); ?>
                </div>
            </div>
        <?php } ?>

        <?php if (isset($msgValidation) && $msgValidation !== '') { ?>
            <div class="main-error">
                <?php echo Yii::t('mainApp', $msgValidation); ?>
            </div>
        <?php } ?>

        <div class="form-group clearfix">
            <?php echo $formCreditCard->labelEx($frmMoChangeCredit, 'idPayMethod', array('class'=>'label')); ?>

            <?php echo $this->renderPartial(
              "//payments/listCardPayMethods",
              array(
                "user" => $moUser,
                "modelPayForm" => $frmMoChangeCredit,
                "form" => $formCreditCard,
                "aMethodsAvailable" => array(PAY_METHOD_CARD),
                "currency" => $currency,
                'urlsFormProcess' => $urlsFormProcess,
                'paySuppExtId' => 0
              )
            ); ?>
        </div>

        <div class="form-group clearfix">
            <?php echo $formCreditCard->labelEx($frmMoChangeCredit, 'creditCardType', array('class'=>'label')); ?>

            <div class="control-wrapper">
                <?php echo $formCreditCard->dropDownList(
                  $frmMoChangeCredit,
                  'creditCardType',
                  $listOfCreditCards,
                  array('empty' => Yii::t('mainApp', '(Select a card type)'))
                ); ?>

                <?php echo $formCreditCard->error($frmMoChangeCredit, 'creditCardType'); ?>
            </div>
        </div>

        <div class="form-group clearfix">
            <?php echo $formCreditCard->labelEx($frmMoChangeCredit, 'creditCardName', array('class'=>'label')); ?>

            <div class="control-wrapper">
                <?php echo $formCreditCard->textField($frmMoChangeCredit, 'creditCardName'); ?>
                <?php echo $formCreditCard->error($frmMoChangeCredit, 'creditCardName'); ?>
            </div>
        </div>

        <div class="form-group clearfix">
            <?php echo $formCreditCard->labelEx($frmMoChangeCredit, 'creditCardNumber', array('class'=>'label')); ?>

            <div class="control-wrapper">
                <?php echo $formCreditCard->textField($frmMoChangeCredit, 'creditCardNumber'); ?>
                <?php echo $formCreditCard->error($frmMoChangeCredit, 'creditCardNumber'); ?>
            </div>
        </div>

        <div class="form-group clearfix">
            <?php echo $formCreditCard->labelEx($frmMoChangeCredit, 'expirationDate', array('class'=>'label')); ?>

            <div class="control-wrapper">
                <?php echo $formCreditCard->dropDownList(
                    $frmMoChangeCredit,
                    'creditCardMonth',
                    Yii::app()->getLocale()->getMonthNames(),
                    array('empty' => ucwords(Yii::t('mainApp', '(Select a month)')), 'class' =>'selector-month')
                ); ?>

                <?php echo $formCreditCard->dropDownList(
                    $frmMoChangeCredit,
                    'creditCardYear',
                    HeDate::getDateYears(date('Y'), 0, 10),
                    array('empty' => ucwords(Yii::t('mainApp', '(Select a year)')),'class' =>'selector-year')
                ); ?>

                <?php echo $formCreditCard->error($frmMoChangeCredit, 'creditCardMonth'); ?>
                <?php echo $formCreditCard->error($frmMoChangeCredit, 'creditCardYear'); ?>
            </div>
        </div>

        <div class="form-group clearfix">
            <?php echo $formCreditCard->labelEx($frmMoChangeCredit, 'CVC', array('class'=>'label')); ?>

            <div class="control-wrapper">
                <div class="helpIconPopup2 icon-help">
                    <span class="flechaHelpBox right"></span>

                    <div class="floatHelpBox right">
                        <?php echo Yii::t(
                          'mainApp',
                          'Número de tres dígitos que se encuentra impreso en el dorso de la tarjeta, en el espacio reservado a tu firma.'
                        ) ?>
                    </div>
                </div>

                <?php echo $formCreditCard->textField($frmMoChangeCredit, 'CVC', array("class" => "cvc-input")); ?>

                <?php echo $formCreditCard->error($frmMoChangeCredit, 'CVC'); ?>
            </div>
        </div>

        <div class="form-group clearfix">
            <?php echo CHtml::submitButton(
                Yii::t('mainApp', 'actualiza'),
                array("id" => "btnSaveNewCreditCard", "class" => "aba-button1")
            ); ?>
        </div>

        <div class="cambio">
            <?php echo Yii::t('mainApp', 'infoactualiza') . HeDate::removeTimeFromSQL($dateToPay); ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
    <!--*******END***************FORM FIELDS*********************END**************** -->
</div>
