<?php
$this->pageTitle=Yii::app()->name . ' - Profile';
$this->breadcrumbs=array(
    'Profile',
);
// Navigation index (BreadCrumb):
$this->widget('application.components.widgets.BreadcrumbHeader', array('curMenuSectionTab' => "ABA forever"));
// Row of Tabs, auto-detect the active one:
$this->renderPartial("profileTabs");
?>


<div class="div-progress-form left">
    &nbsp;
    <p class="titulosProfile left">ABA forever</p>
</div>