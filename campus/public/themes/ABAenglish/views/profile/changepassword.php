<?php
/*  @var PasswordForm $modelPassword */
/* @var string $msgConfirmation     */
?>

<?php
//Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/jquery-ui-1.8.24.custom.css');
//Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/datepickerImplement.js');
$this->pageTitle=Yii::app()->name . ' - Profile';
$this->breadcrumbs=array(
    'Profile',
);
// Navigation index (BreadCrumb):
$this->widget('application.components.widgets.BreadcrumbHeader', array('curMenuSectionTab' => "Mi Cuenta"));
// Row of Tabs, auto-detect the active one:
$this->renderPartial("profileTabs");
?>
<div class="clear"></div>
<div class="form div-profile-form left">
    <p class="titulosProfile left"><?php echo Yii::t('mainApp', 'Modificar password') ?></p>
    <div class="clear"></div>
    <?php 
        $passwordForm = $this->beginWidget('CActiveForm', array(
            'id' => 'password-form',
            'action' => $this->createURL('/profile/updatepwd'),
            'enableClientValidation' => false,
        ));
    ?>
    <?php if( isset($msgConfirmation) && $msgConfirmation!==''){
        $classMsgConf = "";
        if( $msgConfirmation!="Se ha guardado correctamente" ){ $classMsgConf = "errorMessage";}?>
        <div id="msgConfirmation" class="row <?php echo $classMsgConf;?>">
            <?php echo Yii::t('mainApp', $msgConfirmation); ?>
        </div>
    <?php } ?>

    <div class="row">
        <label class="required">
            <?php echo Yii::t('mainApp', 'Contraseña actual') ?>
            <span class="required">*</span>
        </label>
        <?php echo $passwordForm->passwordField($modelPassword,'oldPassword'); ?>
    </div>
    <?php
        if(trim($passwordForm->error($modelPassword,'oldPassword')) != "")
        {
     ?>
            <div class="clear"></div>
            <div class="row">
                <?php echo $passwordForm->error($modelPassword,'oldPassword'); ?>
            </div>
    <?php
        }
    ?>
    <div class="clear"></div>
    <div class="row">
        <label class="required">
            <?php echo Yii::t('mainApp', 'Contraseña nueva') ?>
            <span class="required">*</span>
        </label>
        <?php echo $passwordForm->passwordField($modelPassword,'newPassword'); ?>
    </div>
    <?php
        if(trim($passwordForm->error($modelPassword,'newPassword')) != "")
        {
     ?>
            <div class="clear"></div>
            <div class="row">
                <?php echo $passwordForm->error($modelPassword,'newPassword'); ?>
            </div>
    <?php
        }
    ?>
    <div class="clear"></div>
    <div class="row">
        <label class="required">
            <?php echo Yii::t('mainApp', 'Repetir contraseña nueva') ?>
            <span class="required">*</span>
        </label>
        <?php echo $passwordForm->passwordField($modelPassword,'confirmPassword'); ?>
    </div>
    <?php
        if(trim($passwordForm->error($modelPassword,'confirmPassword')) != "")
        {
     ?>
            <div class="clear"></div>
            <div class="row" style="height: auto !important;">
                <?php echo $passwordForm->error($modelPassword,'confirmPassword'); ?>
            </div>
    <?php
        }
    ?>
    <div class="clear"></div>
    <div class="row buttons marginBottom30">
        <?php echo CHtml::submitButton(Yii::t('mainApp', 'Modificar datos'),array("class" => "aba-button")); ?>
    </div>
    <div class="clear"></div>
<?php $this->endWidget(); ?>
</div>
<div class="clear"></div>