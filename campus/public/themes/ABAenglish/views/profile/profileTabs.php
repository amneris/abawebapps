<?php
/**
 * @var CHttpRequest $req
 * @var bool $fDisplayInvoices
 * @var $typeSubscriptionStatus integer : SUBS_PREMIUM_A_CARD_A, etc.
 */
$req = Yii::app()->getRequest();
$tab =  $req->requestUri;

if( !isset($fDisplayInvoices)){
    $comInvoice = new InvoicesCommon();
    $fDisplayInvoices = $comInvoice->isDisplayInvoices(Yii::app()->user->getId());
}

if( !isset($typeSubscriptionStatus)){
    $moUser = new AbaUser();
    $moUser->getUserById(Yii::app()->user->getId());
    $moLastPaySubs = new Payment();
    $commRecurring = new RecurrentPayCommon();
    $typeSubscriptionStatus = $commRecurring->getTypeSubscriptionStatus($moUser, $moLastPaySubs);
}
?>

<ul id="tabsProfile" class="tabs-profile">
    <li  id="liTabMyAccount" <?php echo (strpos($tab,"profile/index") || strpos($tab,"update") || strpos($tab,"changepassword"))?'class="tab-selected"':' '; ?> >
        <span class="menuleft left"></span>
        <span class="menucenter left">
            <?php echo CHtml::link(Yii::t('mainApp', 'Datos personales'), Yii::app()->createUrl('profile/index'), array("id"=>"aTabMyAccount")); ?>
        </span>
        <span class="menuright left"></span>
    </li>
    <li  id="liTabMyProgress"  <?php echo (strpos($tab,"progress"))?'class="tab-selected"':' '; ?> >
        <span class="menuleft left"></span>
        <span class="menucenter left">
            <?php echo CHtml::link(Yii::t('mainApp', 'Mi progreso'), Yii::app()->createUrl('profile/progress'), array("id"=>"aTabMyProgress")); ?>
        </span>
        <span class="menuright left"></span>
    </li>
    <li id="liTabCertificates" <?php echo (strpos($tab,"certificates"))?'class="tab-selected"':' '; ?> >
        <span class="menuleft left"></span>
        <span class="menucenter left">
            <?php echo CHtml::link(Yii::t('mainApp', 'Mis certificados'), Yii::app()->createUrl('profile/certificates'), array("id"=>"aTabCertificates")); ?>
        </span>
        <span class="menuright left"></span>
    </li>
    <!-- Check if the user is Free and doesn't have any payment line -> hide the billing tab -->
    <?php if(isset($fDisplayInvoices) && $fDisplayInvoices){ ?>
        <li id="liTabInvoices" <?php echo (strpos($tab,"profile/invoices"))?'class="tab-selected"':' '; ?> >
            <span class="menuleft left"></span>
            <span class="menucenter left">
                <?php echo CHtml::link(Yii::t('mainApp', 'Mis facturas'), Yii::app()->createUrl('profile/invoices'), array("id"=>"aTabInvoices")); ?>
            </span>
            <span class="menuright left"></span>
        </li>
    <?php }
    if(RecurrentPayCommon::isDisplaySubscription($typeSubscriptionStatus)){
    ?>
            <li id="liTabSubscription" <?php echo (strpos($tab, "subscription"))?'class="tab-selected"':' '; ?> >
                <span class="menuleft left"></span>
        <span class="menucenter left">
            <?php echo CHtml::link(Yii::t('mainApp', 'w_subscription'), Yii::app()->createUrl('subscription/index'), array("id"=>"aTabSubscription")); ?>
        </span>
                <span class="menuright left"></span>
            </li>
    <?php } ?>
    <?php
    // member get member
    $mgm = Yii::app()->user->getMemberGetMember();
    if($mgm !== false) {
        $daysDiff = 0;
        if($mgm['expireDate'] !== NULL) {
            $daysDiff = HeDate::getDifferenceDateSQL($mgm['expireDate'], date('Y-m-d'), 86400, false);
        }
        if($mgm['expireDate'] !== NULL && $daysDiff <= 0) {
            // if the date expired means the user is free again, so we do nothing
        }
        else
        {
            ?>
            <li id="liTabCertificates" <?php echo (strpos($tab,"invite"))?'class="tab-selected"':' '; ?> >
                <span class="menuleft left"></span>
                <span class="menucenter left">
                    <?php echo CHtml::link(Yii::t('mainApp', 'mgm_invite'), Yii::app()->createUrl('profile/invite'), array("id"=>"aTabInvite")); ?>
                </span>
                <span class="menuright left"></span>
            </li>
            <?php
        }
    }
    ?>
</ul>