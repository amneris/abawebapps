<?php
/* @var AbaUserInvoices $moInvoice; */
/* @var string  $ccySymbol */

    $pdf = Yii::createComponent('application.extensions.mpdf.mpdf');
    $imagePath = HeMixed::getUrlPathImg("logoAbaBill.png");
    $imageTag = CHtml::image($imagePath, "Logo ABA English", array("style" => "float:right; width:135px; height:119px;margin-right:1.5cm;margin-top:1cm;", "width" => "135", "height" => "119"));
    //Variables to set

    $billTitle = Yii::t("mainApp", "w_invoice");
    if ( floatval($moInvoice->amountPrice)<0.00 ){
        $billTitle = Yii::t("mainApp", "w_refund");
    }
    $billDate = $moInvoice->dateInvoice;

    $billNumLabel = Yii::t("mainApp", "w_NumInvoice").': ';
    $billNum = $moInvoice->numberInvoice;

    $clientLabel = Yii::t("mainApp", "key_InvoiceName");
    $clientName = $moInvoice->userName;
    $clientSurname = $moInvoice->userLastName;
    $nifLabel = Yii::t("mainApp", "TaxNumber");
    $vatNumber = $moInvoice->userVatNumber;

    $invoicesStreet = $moInvoice->userStreet;
    $invoicesStreetMore = $moInvoice->userStreetMore;
    $city = $moInvoice->userCityName;
    $invoicesProvince = $moInvoice->userStateName;
    $zipCode = $moInvoice->userZipCode;
    $invoicesCountry = $moInvoice->userCountryName;

    $quantity = Yii::t("mainApp", 'w_Quantity');
    $price = Yii::t("mainApp", 'wPrice');
    $total = Yii::t("mainApp", 'TOTAL');

    $totales = Yii::t("mainApp", 'w_Totales');
    $baseImponible = Yii::t("mainApp", 'w_Base_Imponible');
    $totalIva = Yii::t("mainApp", 'w_Total_Iva');
    $totalFactura = Yii::t("mainApp", 'w_total_factura');

    $productText = $moInvoice->productDesc;

    $totalIva .= " (" . HeMixed::roundAmount($moInvoice->taxRateValue, 0) . " %)";

    $sTotalIva = 0.00;
    if($moInvoice->amountTax != 0) {
        $sTotalIva = HeMixed::getRoundAmount($moInvoice->amountTax, $moInvoice->currencyTrans);
        $sTotalIva = HeViews::formatAmountInCurrency($sTotalIva, $moInvoice->currencyTrans, $ccySymbol);
    }
    else {
        $sTotalIva = HeViews::formatAmountInCurrency($sTotalIva, $moInvoice->currencyTrans, '', true);
    }

$html = '
    <div>
        '.$imageTag.'
        <h1 style="float:left; margin-left:1cm; font-family: \'museoslab700\'; font-size:30px;">'.$billTitle.'</h1>
        <div style="width:7cm; height:0.75cm; border:1px solid #000; padding-left:0.4cm; line-height:0.75cm; margin-left:1cm;">
             '.Yii::t("mainApp", "w_fecha").': <span style="font-family:arial;font-weight:bold;">'.$billDate.'</span>
        </div>
        <div style="width:7cm; height:0.75cm; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; margin-top:-1px; padding-left:0.4cm; line-height:0.75cm; margin-left:1cm;">
             '.$billNumLabel.'<span style="font-family:arial;font-weight:bold;">'.$billNum.'</span>
        </div>
        <br>
        <div style="margin-left:1cm;border:1px solid #000;padding-left:0.4cm; width:18cm; padding-top:0.4cm; padding-bottom:0.4cm;">
            <strong style="font-family: \'museoslab500\';">'.$moInvoice->nameAba.'</strong><br><br>
            <span>'.$nifLabel.':</span> <span style="font-family:arial; font-weight:bold;line-height:40px;">'.$moInvoice->cifAba.'</span><br>
            <span style="font-weight:bold;font-family:arial;line-height:40px;">'.$moInvoice->addressAba.'</span><br>
            <span style="font-weight:bold;font-family:arial;">'.$moInvoice->addressAbaMore.'</span><br>
            <span style="font-weight:bold;font-family:arial;">'.$moInvoice->telephoneAba.'</span><br>
        </div>
        <br>
        <div style="margin-left:1cm;border:1px solid #000;padding-left:0.4cm;width:18cm;padding-top:0.4cm; padding-bottom:0.4cm;">
            '.$clientLabel.': <strong style="font-family: \'museoslab500\';text-transform:uppercase;"> '.$clientName.' '.$clientSurname.'</strong><br>
            '.$nifLabel.': <span style="font-family:arial; font-weight:bold;">'.$vatNumber.'</span><br>
            <span style="font-family:arial; font-weight:bold;">'.$invoicesStreet.' '.$invoicesStreetMore.'</span><br>
            <span style="font-family:arial; font-weight:bold;">'.$city.'</span><span style="font-family:arial; font-weight:bold;"> ('.$invoicesProvince.')</span><br>
            <span style="font-family:arial; font-weight:bold;">'.$zipCode.'</span><br>
            <span style="font-family:arial; font-weight:bold;">'.$invoicesCountry.'</span><br>
        </div>
        <p style="font-family:\'museoslab500\';margin:1cm 0 0.5cm 1cm; text-transform:uppercase;"><strong>'
            .Yii::t("mainApp", "w_Concepto").
        '</strong></p>
        <table cellpadding="0" cellspacing="0" border="0" style="margin-left:1cm;">
            <tr>
                <td style="width:10.3cm;border-bottom:1px solid #000;line-height:0.75cm;height:0.75cm;"><strong>'.
                    Yii::t("mainApp", "w_Description").
                '</strong></td>
                <td style="width:3.2cm;border-bottom:1px solid #000;text-align:center;line-height:0.75cm;height:0.75cm;">
                    <strong>'.$quantity.'</strong>
                </td>
                <td style="width:2.6cm;border-bottom:1px solid #000;text-align:center;line-height:0.75cm;height:0.75cm;">
                    <strong>'.$price.'</strong>
                </td>
                <td style="width:2.2cm;border-bottom:1px solid #000;text-align:right;line-height:0.75cm;height:0.75cm;"><strong>'.$total.'</strong></td>
            </tr>
            <tr>
                <td style="width:10.3cm;line-height:0.75cm;height:0.75cm;">'.
                    $moInvoice->idPayment." &nbsp; ".
                    Yii::t("mainApp", "Curso").' '.Yii::t("mainApp",$productText).'</td>
                <td style="width:3.2cm;text-align:center;line-height:0.75cm;height:0.75cm;">1</td>
                <td style="width:2.6cm;text-align:center;line-height:0.75cm;height:0.75cm;">'.
                    HeViews::formatAmountInCurrency( HeMixed::getRoundAmount($moInvoice->amountPrice, $moInvoice->currencyTrans), $moInvoice->currencyTrans, $ccySymbol).
                '</td>
                <td style="width:2.2cm;text-align:right;line-height:0.75cm;height:0.75cm;">'.
                    HeViews::formatAmountInCurrency( HeMixed::getRoundAmount($moInvoice->amountPrice, $moInvoice->currencyTrans), $moInvoice->currencyTrans, $ccySymbol).
                '</td>
            </tr>
        </table>

        <div style="border:1px solid #000;float:right; width:10cm;padding:0.5cm;margin-right:1.5cm;margin-top:2cm;">
            <strong style="font-family: \'museoslab500\';">'.$totales.'</strong><br><br>
            <div style="float:left;width:6cm;">'.$baseImponible.'</div><div style="float:right;width:3cm;text-align:right;">'.HeViews::formatAmountInCurrency( HeMixed::getRoundAmount($moInvoice->amountPriceWithoutTax, $moInvoice->currencyTrans), $moInvoice->currencyTrans, $ccySymbol).'</div><br>
            <div style="float:left;width:6cm;">'.$totalIva.'</div><div style="float:right;width:3cm;text-align:right;">' . $sTotalIva . '</div>
            <br>
            <br>
            <div style="border-top:1px solid #000;padding-top:0.5cm;">
                <div style="float:left;width:6cm;font-weight:bold;">'.
                        $totalFactura.
                ':</div>
                <div style="font-weight:bold;float:right;width:3cm;text-align:right;">'.
                        HeViews::formatAmountInCurrency( HeMixed::getRoundAmount($moInvoice->amountPrice, $moInvoice->currencyTrans), $moInvoice->currencyTrans, $ccySymbol).
                '</div>
            </div>
        </div>

    </div>
';

$mpdf = new mPDF('utf-8', 'A4','','',0,0,0,0,0,0);
$mpdf->useOnlyCoreFonts = false;
$mpdf->useSubstitutions  = true;
$mpdf->SetAutoFont(AUTOFONT_ALL);
$mpdf->AddFont("museoslab900");
$mpdf->AddFont("museoslab700");
$mpdf->AddFont("museoslab500");
$mpdf->AddFont("museoslab300");
$mpdf->WriteHTML($html);
$fileName = $moInvoice->numberInvoice;
$mpdf->Output($moInvoice->numberInvoice.'.pdf','D');
exit;
