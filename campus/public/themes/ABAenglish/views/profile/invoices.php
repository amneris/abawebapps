<?php
/* @var Array $aDataInvoices
 *  Elements: ccySymbol, signature, p.dateEndTransaction, pr.descriptionText, p.amountPrice, p.currencyTrans, i.dateLastDownload, p.idProduct,
 *      p.id  as `idPayment`, p.`status`, i.id, i.numberInvoice
 * @var string $msgValidation
 * @var bool $allDownloaded
 */

$this->pageTitle=Yii::app()->name . ' - Profile';
$this->breadcrumbs=array( 'Profile', );

// Navigation index (BreadCrumb):
$this->widget('application.components.widgets.BreadcrumbHeader', array('curMenuSectionTab' => "Mis facturas"));
// Row of Tabs, auto-detect the active one:
$this->renderPartial("profileTabs");
?>

<div class="div-billing-form left">
    <p class="title"><?php echo Yii::t("mainApp", "w_MyInvoices"); ?></p>
    <?php
        if ( $allDownloaded ){ ?>
            <tr>
                <td colspan="4" class="billingDate">
                    <p class="normal">
                        <?php echo Yii::t("mainApp", "key_allInvoicesDownloaded"); ?>
                    </p>
                </td>
            </tr>
    <?php
        } ?>
    <table id="billsTable">
        <thead>
            <tr>
                <td class="billingDate"><?php echo Yii::t("mainApp", "w_fecha"); ?></td>
                <td class="billingConcept"><?php echo Yii::t("mainApp", "w_Concepto"); ?></td>
                <td class="billingImport"><?php echo Yii::t("mainApp", "w_InvoiceAmount"); ?></td>
                <td class="billingDownload"><?php echo Yii::t("mainApp", "w_InvDownload"); ?></td></td>
            </tr>
        </thead>
        <tbody>
        <?php
            if ( $msgValidation!==''){ ?>
                <tr>
                    <td colspan="4" class="needBillingData">
                        <p class="normal">
                        <?php echo Yii::t("mainApp", $msgValidation,
                                                array("{link}"=>CHtml::link(Yii::t("mainApp", "keyBillingInfo"),
                                                                        Yii::app()->createUrl('/profile/index', array("fDisplayInvoices"=>1))."/#invoicesdata", array('id'=>'idBillingInfoLink'))));
                        ?>
                        </p>
                    </td>
                </tr>
                <?php } ?>
            <?php
            foreach ($aDataInvoices as $aInvoice) { ?>
            <tr>
                <td class="billingDate">
                    <?php echo HeDate::removeTimeFromSQL($aInvoice["dateEndTransaction"]); ?>
                </td>
                <td class="billingConcept">
                    <?php echo Yii::t("mainApp", "Curso")." ".Yii::t("mainApp", $aInvoice["descriptionText"]); ?>
                </td>
                <td class="billingImport">
                    <?php  echo HeViews::formatAmountInCurrency(HeMixed::getRoundAmount($aInvoice["amountPrice"], $aInvoice["currencyTrans"]),
                                                                $aInvoice["currencyTrans"],
                                                                $aInvoice["ccySymbol"]); ?>
                </td>
                <td class="billingDownload">
                    <?php echo CHtml::link( Yii::t("mainApp", "key_downloadInvoice"),
                                       Yii::app()->createUrl("profile/invoicePdf",
                                                        array("signature"=> $aInvoice["signature"],
                                                              "idPayment"=> $aInvoice["idPayment"]) ), array("class"=>"downloadBill", "id" => $aInvoice["idPayment"] . "-" . $aInvoice["id"] ) ); ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <p class="normal acrobat"><?php echo Yii::t("mainApp", "w_InvNeedAcrobat",
                                                array("{link}" => cHTML::link("Adobe Acrobat Reader",
                                                                                "https://get.adobe.com/reader/", array("target"=>"_blank")))); ?>
    </p>

</div>