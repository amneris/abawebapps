<?php
$this->pageTitle=Yii::app()->name . ' - Profile';
$this->breadcrumbs=array(
    'Profile',
);
// Navigation index (BreadCrumb):
$this->widget('application.components.widgets.BreadcrumbHeader', array('curMenuSectionTab' => "Mis certificados"));
// Row of Tabs, auto-detect the active one:
$this->renderPartial("profileTabs");
?>


<div class="div-certificates-form left">
    <p class="titulosProfile left"><?php echo Yii::t('mainApp', 'Mis certificados') ?></p>
    <div class="clear"></div>
    <div class="pasosFlujoCertificados left">
        <span class="iconCompletar left"></span>
        <div class="clear"></div>
        <p class="bold"><?php echo Yii::t('mainApp', '1. Completa el 100% de las unidades del curso') ?></p>
        <p class="normal"><?php echo Yii::t('mainApp', 'Para obtener el certificado de tu nivel debes completar el 100% de todas las unidades.') ?></p>
    </div>
    <div class="pasosFlujoCertificados left">
        <span class="iconCertificados2 left"></span>
        <div class="clear"></div>
        <p class="bold"><?php echo Yii::t('mainApp', '2. Realiza la sección Evaluación') ?></p>
        <p class="normal"><?php echo Yii::t('mainApp', 'Una vez completado el 100% de la unidad, debes aprobar el test final de cada unidad.') ?></p>
    </div>
    <div class="pasosFlujoCertificados left">
        <span class="iconCertificados3 left"></span>
        <div class="clear"></div>
        <p class="bold"><?php echo Yii::t('mainApp', '3. Descarga el certificado') ?></p>
        <p class="normal"><?php echo Yii::t('mainApp', '¡Descargar tu certificado de ') ?><span class="bold">American and British Academy</span><?php echo Yii::t('mainApp', ' del nivel que completes!') ?></p>
    </div>
    <div class="clear"></div>
    <div class="nuevosCertificados">
        <span class="starIconWhite left"></span>
        <span class="normal"><?php echo Yii::t('mainApp', 'Tienes ').(string)$certificadosDisponibles ?></span>
        <span class="bold"><?php echo $certificadosDisponibles === 1 ? Yii::t('mainApp', ' nuevo certificado'):Yii::t('mainApp', ' nuevos certificados') ?></span>
    </div>
    <ul>
        <?php
            $class = array("buttonCertificadosDisabled", "buttonCertificadosEnabled");

            for($k=0;$k<count($certificados['level']);$k++)
            {
        ?>
                <li>
                    <span class="nivel"><?php echo $certificados['level'][$k] ?></span>
                    <span class="porcentage"><?php echo ($certificados['porcentage'][$k] > 100) ? 100 : $certificados['porcentage'][$k].Yii::t('mainApp', '% completado') ?></span>
                    <span class="emptyBar"></span>
                    <span class="fullBar" style="width: <?php echo (($certificados['porcentage'][$k]*1.2) > 120) ? 120 : ($certificados['porcentage'][$k]*1.2) ?>px;"></span>
                    <?php
                        $bool = ((int)$certificados['porcentage'][$k] > 99 ? true : false)
                    ?>
                    <?php
                        if($bool)
                        {
                    ?>
                          <script> var downloadedSertificateParams_<?php echo $levelsId[$k]; ?> = {"levelId":"<?php echo $levelsId[$k]; ?>"}; </script>
                            <a href="<?php echo Yii::app()->createUrl("pdfs/index", array("nivel" => $levelsId[$k])) ?>" onclick="javascript: sendDefaultEventToTracker('<?php echo HeCooladata::EVENT_NAME_DOWNLOADED_CERTIFICATE; ?>', downloadedSertificateParams_<?php echo $levelsId[$k]; ?>);">
                                <div class="buttonCertificadosEnabled" lang="<?php echo Yii::app()->language?>">
                                    <span class="icon"></span>
                                    <span class="text"><?php echo Yii::t('mainApp', 'Descargar') ?></span>
                                </div>
                            </a>

                            <a href="<?php echo $certificados['urlLinkedin'][$k] ?>" rel="nofollow" target="_blank" onclick="javascript: sendDefaultEventToTracker('<?php echo HeCooladata::EVENT_NAME_ADDED_LINKEDIN; ?>');">
                                <div class="linkedin-enabled" lang="<?php echo Yii::app()->language?>">
                                    <div class="icono-linkedin" >
                                        <i class="fa fa-linkedin"></i>
                                    </div>
                                    <span class="text" lang="<?php echo Yii::app()->language?>"><?php echo Yii::t('mainApp', 'añadeLinkedin') ?></span>
                                </div>
                            </a>
                    <?php
                        }
                        else{
                    ?>
                            <div class="buttonCertificadosDisabled" lang="<?php echo Yii::app()->language?>">
                                <span class="icon"></span>
                                <span class="text"><?php echo Yii::t('mainApp', 'Descargar') ?></span>
                            </div>

                            <div class="linkedin-disable" lang="<?php echo Yii::app()->language?>">
                                <div class="icono-linkedin" >
                                    <i class="fa fa-linkedin"></i>
                                </div>
                                <span class="text" lang="<?php echo Yii::app()->language?>"><?php echo Yii::t('mainApp', 'añadeLinkedin') ?></span>
                            </div>
                    <?php
                        }
                    ?>
                </li>
        <?php
            }
        ?>
    </ul>
</div>