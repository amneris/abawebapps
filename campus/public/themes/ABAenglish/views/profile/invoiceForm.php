<?php
/* @var ProfileForm $model */
/* @var CActiveForm $form */
/* @var array $listOfCountries */
?>
<a name="invoicesdata"></a>
<p id="pSubSecInvoices" class="billingData titulosProfile"><?php echo Yii::t('mainApp', 'key_InvoiceData') ?></p>
<?php echo $form->hiddenField( $model, "fDisplayInvoices"); ?>
<input type="checkbox" id="copyData"><label id="copyDataLabel"><?php echo Yii::t("mainApp", "key_copyData"); ?></label>

<div class="changeFutureInv">
    <?php echo Yii::t("mainApp", "key_ChangesFutureInvoices");?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'invoicesName'); ?>
    <?php echo $form->textField($model,'invoicesName'); ?>
</div>
<?php
if (trim($form->error($model, 'invoicesName')) !== "") {
    ?>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->error($model,'invoicesName'); ?>
    </div>
<?php
}
?>
<div class="clear"></div>
<div class="row">
    <?php echo $form->labelEx($model,'invoicesSurname'); ?>
    <?php echo $form->textField($model,'invoicesSurname'); ?>
</div>
<div class="clear"></div>
<?php
if ((trim($form->error($model, 'invoicesSurname'))) !== "") {
    ?>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->error($model,'invoicesSurname'); ?>
    </div>
<?php
}
?>
<div class="row">
    <?php echo $form->labelEx($model,'invoicesStreet'); ?>
    <?php echo $form->textField($model,'invoicesStreet'); ?>
</div>
<div class="clear"></div>
<?php
if ((trim($form->error($model, 'invoicesStreet'))) !== "") {
    ?>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->error($model,'invoicesStreet'); ?>
    </div>
<?php
}
?>
<div class="row">
    <?php echo $form->labelEx($model,'invoicesStreetMore'); ?>
    <?php echo $form->textField($model,'invoicesStreetMore'); ?>
</div>
<div class="clear"></div>
<?php
if ((trim($form->error($model, 'invoicesStreetMore'))) !== "") {
    ?>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->error($model,'invoicesStreetMore'); ?>
    </div>
<?php
}
?>
<div class="row">
    <?php echo $form->labelEx($model,'invoicesCity'); ?>
    <?php echo $form->textField($model,'invoicesCity'); ?>
</div>
<div class="clear"></div>
<?php
if ((trim($form->error($model, 'invoicesCity'))) !== "") {
    ?>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->error($model,'invoicesCity'); ?>
    </div>
<?php
}
?>
<div class="row">
    <?php echo $form->labelEx($model,'invoicesProvince'); ?>
    <?php echo $form->textField($model,'invoicesProvince'); ?>
</div>
<div class="clear"></div>
<?php
if ((trim($form->error($model, 'invoicesProvince'))) !== "") {
    ?>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->error($model,'invoicesProvince'); ?>
    </div>
<?php
}
?>
<div class="row">
    <?php echo $form->labelEx($model,'invoicesPostalCode'); ?>
    <?php echo $form->textField($model,'invoicesPostalCode'); ?>
</div>
<div class="clear"></div>
<?php
if ((trim($form->error($model, 'invoicesPostalCode'))) !== "") {
    ?>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->error($model,'invoicesPostalCode'); ?>
    </div>
<?php
}
?>
<div class="row">
    <?php echo $form->labelEx($model,'invoicesCountry'); ?>
    <?php echo $form->dropDownList($model, 'invoicesCountry', $listOfCountries,array('empty' => '(Select a country)')); ?>
</div>
<div class="clear"></div>
<?php
if ((trim($form->error($model, 'invoicesCountry'))) !== "") {
    ?>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->error($model,'invoicesCountry'); ?>
    </div>
<?php
}
?>
<div class="row">
    <?php echo $form->labelEx($model,'invoicesTaxIdNumber'); ?>
    <?php echo $form->textField($model,'invoicesTaxIdNumber'); ?>
</div>
<div class="clear"></div>
<?php
if ((trim($form->error($model, 'invoicesTaxIdNumber'))) !== "") {
    ?>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->error($model,'invoicesTaxIdNumber'); ?>
    </div>
<?php
}
?>