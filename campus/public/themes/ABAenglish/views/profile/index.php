<?php
/*  @var CActiveForm $form    */
/*  @var ProfileForm $model    */
/*  @var integer $currentLevel */
/*  @var string $entryDate */
/*  @var string $expirationDate */
/*  @var array $listOfCountries */
/*  @var string $msgConfirmation */
/* @var integer|string $userTypePartnerStatus */
/* @var bool $model->fDisplayInvoices */ //

Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/jquery-ui-1.8.24.custom.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/datepickerImplement.js');

//#ABAWEBAPPS-701
if (isset($cooladataUserData->userType)) {

    if(isset($bSendCooladataChangedPersonalDetailsEvent) AND $bSendCooladataChangedPersonalDetailsEvent) {
        $stParams = array(
          "userUserType" => array(
            "value" => $cooladataUserData->userType,
            "type" => "phpInteger"
          ),
          "userLangId" => array("value" => $cooladataUserData->langEnv, "type" => "default"),
          "birthDate" => array(
            "value" => HeDate::europeanToSQL($cooladataUserData->birthDate, false,
                "-") . " 00:00:00",
            "type" => "default"
          ),
          "userLevel" => array("value" => $cooladataUserData->currentLevel, "type" => "phpInteger"),
          "userCountryId" => array(
            "value" => $cooladataUserData->countryId,
            "type" => "phpInteger"
          ),
        );
        $sCooladata = HeCooladata::createDefaultEvent(HeCooladata::EVENT_NAME_CHANGED_PERSONAL_DETAILS, true, $stParams);
        Yii::app()->clientScript->registerScript(md5($sCooladata), $sCooladata, CClientScript::POS_READY);
    }

    if(isset($bSendCooladataChangedLevelEvent) AND $bSendCooladataChangedLevelEvent) {
        $stParams = array(
          "userLevel" => array("value" => $cooladataUserData->currentLevel, "type" => "phpInteger")
        );
        $sCooladata = HeCooladata::createDefaultEvent(HeCooladata::EVENT_NAME_CHANGED_LEVEL, true, $stParams);
        Yii::app()->clientScript->registerScript(md5($sCooladata), $sCooladata, CClientScript::POS_READY);
    }
}

$this->pageTitle=Yii::app()->name . ' - Profile';
$this->breadcrumbs=array(
    'Profile',
);
// Navigation index (BreadCrumb):
$this->widget('application.components.widgets.BreadcrumbHeader', array('curMenuSectionTab' => "Mi Cuenta"));
// Row of Tabs, auto-detect the active one:
$this->renderPartial("profileTabs", array("fDisplayInvoices" => $model->fDisplayInvoices));
?>
<div class="clear"></div>
<div class="form div-profile-form left">
    <p class="titulosProfile left"><?php echo Yii::t('mainApp', 'Mis datos personales') ?></p>
    <div class="clear"></div>
    <?php 
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'profile-form',
            'action' => $this->createURL('/profile/update'),
            'enableClientValidation' => false,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        ));
    ?>
    <?php if( isset($msgConfirmation) && $msgConfirmation!==''){
        $classMsgConf = "";
        if( ($msgConfirmation != "Se ha guardado correctamente") && ($msgConfirmation != "Se ha cambiado la contraseña correctamente") ){ $classMsgConf = "errorMessage";}?>
        <div id="msgConfirmation" class="row <?php echo $classMsgConf;?>">
            <?php echo Yii::t('mainApp', $msgConfirmation); ?>
        </div>
    <?php } ?>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name'); ?>
    </div>
    <?php
    if (trim($form->error($model, 'name')) !== "") {
     ?>
            <div class="clear"></div>
            <div class="row">
                <?php echo $form->error($model,'name'); ?>
            </div>
    <?php
        }
    ?>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->labelEx($model,'surnames'); ?>
        <?php echo $form->textField($model,'surnames'); ?>
    </div>
    <div class="clear"></div>
    <?php
    if ((trim($form->error($model, 'surnames'))) !== "") {
     ?>
            <div class="clear"></div>
            <div class="row">
                <?php echo $form->error($model,'surnames'); ?>
            </div>
    <?php
        }
    ?>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email', array('readonly' => 'readonly')); ?>
    </div>
    <div class="clear"></div>

    <div class="row">
        <?php echo $form->labelEx($model,'password'); ?>
        <?php //echo $form->passwordField($model,'password',array('disabled'=>'disabled','readonly'=>'readonly','autocomplete'=>'off')); ?>
        <div class="containerModificarPassword right">
            <?php echo CHtml::link(Yii::t('mainApp', 'Modificar password'), Yii::app()->createUrl('profile/changepassword'), array('class' => 'ModificarPassword')) ?>
        </div>
    </div>
    <div class="clear"></div>

    <div class="row">
        <?php echo $form->labelEx($model,'userType'); ?>
        <?php echo $form->textField($model,'userType', array('disabled'=>'disabled','readonly'=>'readonly')); ?>
    </div>
    <div class="clear"></div>

    <div class="row">
        <?php echo $form->labelEx($model,'birthDate'); ?>
        <?php echo $form->textField($model,'birthDate'); ?>
    </div>
    <div class="clear"></div>
    <?php
    if (trim($form->error($model, 'birthDate')) !== '') {
     ?>
            <div class="clear"></div>
            <div class="row">
                <?php echo $form->error($model,'birthDate'); ?>
            </div>
    <?php
        }
    ?>
    <div class="clear"></div>
    <div class="row">
        <?php
        $genderList = array('m' => Yii::t('mainApp', 'Hombre'), 'f' => Yii::t('mainApp', 'Mujer'));
        echo $form->labelEx($model, 'gender');
        echo $form->dropDownList($model, 'gender', $genderList, array('empty' => ''));
        ?>
    </div>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->labelEx($model,'countryId'); ?>
        <?php echo $form->dropDownList($model, 'countryId', $listOfCountries,array('empty' => '')); ?>
    </div>
    <div class="clear"></div>
    <?php
    if (trim($form->error($model, 'countryId')) !== '') {
     ?>
            <div class="clear"></div>
            <div class="row">
                <?php echo $form->error($model,'countryId'); ?>
            </div>
    <?php
        }
    ?>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->labelEx($model,'city'); ?>
        <?php echo $form->textField($model,'city'); ?>
    </div>
    <div class="clear"></div>
    <?php
    if (trim($form->error($model, 'city')) !== '') {
     ?>
            <div class="clear"></div>
            <div class="row">
                <?php echo $form->error($model,'city'); ?>
            </div>
    <?php
        }
    ?>
    <div class="clear"></div>
    <div class="row">
        <?php echo $form->labelEx($model,'telephone'); ?>
        <?php echo $form->textField($model,'telephone'); ?>
    </div>
    <div class="clear"></div>
    <?php
    if (trim($form->error($model, 'telephone')) !== '') {
     ?>
            <div class="clear"></div>
            <div class="row">
                <?php echo $form->error($model,'telephone'); ?>
            </div>
    <?php
        }
    ?>
    <div class="clear"></div>
    <div class="row">
        <label><?php  echo Yii::t('mainApp', 'Nivel actual'); ?>:</label>
        <?php
            echo $form->dropDownList($model, 'currentLevel', Yii::app()->params['levels']);
        ?>
    </div>
    <div class="clear"></div>
    <?php if($model->canChangeLanguage) { ?>
        <div class="row">
            <?php
                echo $form->labelEx($model,'langEnv');
                echo $form->dropDownList($model, 'langEnv', Yii::app()->params['languages']);
            ?>
        </div>
        <div class="clear"></div>
    <?php } else {
        echo  $form->hiddenField($model,'langEnv');
    } ?>
    <div class="row">
        <label><?php  echo Yii::t('mainApp', 'Fecha inicio Free'); ?>:</label>
        <input type="text" readonly="readonly" value="<?php echo $entryDate; ?>" />
    </div>
    <div class="clear"></div>

    <div class="<?php if($userTypePartnerStatus==FREE){ echo "rowHidden"; }else{ echo "row";} ?>">
        <label>
<?php
            if($userTypePartnerStatus == PARTNER_ABA_W || $userTypePartnerStatus == PARTNER_AFFILIATES_W || $userTypePartnerStatus == PARTNER_SOCIAL_W) {
                if(!$bLastPendingPayment) {
                    echo Yii::t('mainApp', 'Fecha fin Premium');
                }
                else {
                    echo Yii::t('mainApp', 'Fecha proximo pago');
                }
            }
            elseif($userTypePartnerStatus == PARTNER_GROUPON_W || $userTypePartnerStatus == PARTNER_B2B_W) {
                echo Yii::t('mainApp', 'Fecha fin Premium');
            }
            else {
                echo Yii::t('mainApp', 'Fecha fin Premium');
            }
?>:
        </label>
        <input type="text" readonly="readonly" value="<?php echo $expirationDate; ?>" />
    </div>
    <div class="clear"></div>

    <!--  Put a conditional statement to show/hide the billing form according if it's a free user without payments of not-->
    <?php if($model->fDisplayInvoices === 1 || $model->fDisplayInvoices === '1' ){
            echo  $form->hiddenField($model,'fDisplayInvoices');
            echo $this->renderPartial("invoiceForm",    array( "model"=> $model,
                                                               "form"=> $form,
                                                               "listOfCountries"=> $listOfCountries ));
          }
    ?>
    <!-- End of the Billing data -->

    <div class="row buttons marginBottom30">
        <?php echo  $form->hiddenField($model,'id'); ?>
        <?php echo CHtml::submitButton(Yii::t('mainApp', 'SaveData'),array("id"=>"btnSaveMyAccount","class" => "aba-button")); ?>
    </div>
    <div class="clear"></div>
<?php $this->endWidget(); ?>
</div>
<div class="clear"></div>