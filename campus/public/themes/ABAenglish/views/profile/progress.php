<?php
/* @var integer     $currentLevel */
/* @var string      $entryDate YYYY-MM-DD */
/* @var string      $expirationDate YYYY-MM-DD */
/* @var integer     $currentUnit */
/* @var AbaFollowup $followup */
/* @var array       $aUnits of key=>string*/
/* @var integer    $iAllErrorsOnUnit */
/* @var integer    $iAllAnswersOnUnit */
?>
<?php
$this->pageTitle=Yii::app()->name . ' - Progress';
$this->breadcrumbs=array(
    'Progress',
);
// Navigation index (BreadCrumb):
$this->widget('application.components.widgets.BreadcrumbHeader', array('curMenuSectionTab' => "Mi progreso"));
// Row of Tabs, auto-detect the active one:
$this->renderPartial("profileTabs");
?>

<div class="div-progress-form left">
    <p class="titulosProfile left"><?php echo Yii::t('mainApp', 'Mi progreso') ?></p>
    <a class="btn-progress" href="<?php echo $urlUnit; ?>">
        <?php echo Yii::t('mainApp', 'Ir a la unidad') . $currentUnit ?>
    </a>
    <div class="clear"></div>
    <div class="dropDownLine">
        <?php echo CHtml::form(Yii::app()->createUrl('/profile/progress'), "post", array("id" => "formProgress")); ?>
            <div class="labelDropDownsProgress left"><?php echo Yii::t('mainApp', 'Nivel') ?>:</div>
            <div id="level-select-progress" class="left">
                <?php
                    echo CHtml::dropDownList('levelProgress', $currentLevel, Yii::app()->params['levels'],
                            array(
                                'onchange' => '$("#level-select-progress").parent("#formProgress").submit();'
                            )
                        );
                ?>
            </div>
            <div class="labelDropDownsProgress left"><?php echo Yii::t('mainApp', 'Unidad') ?>:</div>
            <div id="unit-select-progress" class="left">
                <?php
                    echo CHtml::dropDownList('unit', $currentUnit, $nombresUnidades,
                            array(
                                'onchange' => '$("#level-select-progress").parent("form").submit();'
                            )
                        );
                ?>
            </div>
        <?php echo CHtml::endForm(); ?>
    </div>
    <div class="clear"></div>
    <ul class="sectionsProgressList1 left">
        <li>
            <?php echo Yii::t('mainApp', 'Sección') ?>
        </li>
        <li>
            <span class="unitNumber">1</span>
            <span class="unitTitle"><?php echo Yii::t('mainApp', 'ABA Film') ?></span>
        </li>
        <li>
            <span class="unitNumber">2</span>
            <span class="unitTitle"><?php echo Yii::t('mainApp', 'Habla') ?></span>            
        </li>
        <li>
            <span class="unitNumber">3</span>
            <span class="unitTitle"><?php echo Yii::t('mainApp', 'Escribe') ?></span>            
        </li>
        <li>
            <span class="unitNumber">4</span>
            <span class="unitTitle"><?php echo Yii::t('mainApp', 'Interpreta') ?></span>
        </li>
        <li>
            <span class="unitNumber">5</span>
            <span class="unitTitle"><?php echo Yii::t('mainApp', 'Videoclase') ?></span>
        </li>
        <li>
            <span class="unitNumber">6</span>
            <span class="unitTitle"><?php echo Yii::t('mainApp', 'Ejercicios') ?></span>
        </li>
        <li>
            <span class="unitNumber">7</span>
            <span class="unitTitle"><?php echo Yii::t('mainApp', 'Vocabulario') ?></span>
        </li>
        <li>
            <span class="unitNumber">8</span>
            <span class="unitTitle"><?php echo Yii::t('mainApp', 'Evaluación') ?></span>
        </li>
        <li class="final">
            <span class="totalText">
                <?php echo Yii::t('mainApp', 'TOTAL') ?>
            </span>
        </li>
    </ul>
    <ul class="sectionsProgressList2 left">
        <li>
            <?php echo Yii::t('mainApp', 'Progreso') ?>
        </li>
        <li>
            <span class="porcentagesCursoProfile"><?php echo (($followup->sit_por) > 100) ? 100 : ($followup->sit_por) ?>%</span>
            <span class="emptyBarProfile"></span>
            <span class="fullBarProfile" style="width: <?php echo (($followup->sit_por*1.2) > 120) ? 120 : ($followup->sit_por*1.2) ?>px;"></span>
        </li>
        <li>
            <span class="porcentagesCursoProfile"><?php echo (($followup->stu_por) > 100) ? 100 : ($followup->stu_por) ?>%</span>
            <span class="emptyBarProfile"></span>
            <span class="fullBarProfile" style="width: <?php echo (($followup->stu_por*1.2) > 120) ? 120 : ($followup->stu_por*1.2) ?>px;"></span>          
        </li>
        <li>
            <span class="porcentagesCursoProfile"><?php echo (($followup->dic_por) > 100) ? 100 : ($followup->dic_por) ?>%</span>
            <span class="emptyBarProfile"></span>
            <span class="fullBarProfile" style="width: <?php echo (($followup->dic_por*1.2) > 120) ? 120 : ($followup->dic_por*1.2) ?>px;"></span>         
        </li>
        <li>
            <span class="porcentagesCursoProfile"><?php echo (($followup->rol_por) > 100) ? 100 : ($followup->rol_por) ?>%</span>
            <span class="emptyBarProfile"></span>
            <span class="fullBarProfile" style="width: <?php echo (($followup->rol_por*1.2) > 120) ? 120 : ($followup->rol_por*1.2) ?>px;"></span>
        </li>
        <li>
            <span class="porcentagesCursoProfile"><?php echo (($followup->getVideoClassProgress()) > 100) ? 100 : ($followup->getVideoClassProgress()) ?>%</span>
            <span class="emptyBarProfile"></span>
            <span class="fullBarProfile" style="width: <?php echo (($followup->gra_vid*1.2) > 120) ? 120 : ($followup->gra_vid*1.2) ?>px;"></span>
        </li>
        <li>
            <span class="porcentagesCursoProfile"><?php echo (($followup->wri_por) > 100) ? 100 : ($followup->wri_por) ?>%</span>
            <span class="emptyBarProfile"></span>
            <span class="fullBarProfile" style="width: <?php echo (($followup->wri_por*1.2) > 120) ? 120 : ($followup->wri_por*1.2) ?>px;"></span>
        </li>
        <li>
            <span class="porcentagesCursoProfile"><?php echo (($followup->new_por) > 100) ? 100 : ($followup->new_por) ?>%</span>
            <span class="emptyBarProfile"></span>
            <span class="fullBarProfile" style="width: <?php echo (($followup->new_por*1.2) > 120) ? 120 : ($followup->new_por*1.2) ?>px;"></span>
        </li>
        <li>
            <span class="porcentagesCursoProfile"><?php echo $followup->markEvaluation ?></span>
            <span class="emptyBarProfile"></span>
            <span class="fullBarProfile" style="width: <?php echo (($followup->eva_por*1.2) > 120) ? 120 : ($followup->eva_por*1.2) ?>px;"></span>
        </li>
        <li class="final">
            <span class="porcentagesCursoProfile"><?php echo (($followup->getUnitProgress(true)) > 100) ? 100 : ($followup->getUnitProgress(true)) ?>%</span>
            <span class="emptyBarProfile"></span>
            <span class="fullBarProfile" style="width: <?php echo (($followup->getUnitProgress(true)*1.2) > 120) ? 120 : ($followup->getUnitProgress(true)*1.2) ?>px;"></span>
        </li>
    </ul>
    <ul class="sectionsProgressList3 left">
        <li>
            <span class="mayuscula"><?php echo Yii::t('mainApp', 'Respuestas') ?></span>
            <div class="botonayuda">
                <div class="floatHelpBoxprogreso ">
                    <?php echo Yii::t('mainApp', 'respuesta_correcta') ?>
                </div>
            </div>
        </li>
        <li>
            <span class="answersProgProfile">-</span>
        </li>
        <li>
            <span class="answersProgProfile">-</span>         
        </li>
        <li>
            <span class="answersProgProfile"><?php echo $followup->dic_ans; ?></span>        
        </li>
        <li>
            <span class="answersProgProfile">-</span>
        </li>
        <li>
            <span class="answersProgProfile">-</span>
        </li>
        <li>
            <span class="answersProgProfile"><?php echo $followup->wri_ans; ?></span>
        </li>
        <li>
            <span class="answersProgProfile">-</span>
        </li>
        <li>
            <span class="answersProgProfile">-</span>
        </li>
        <li class="final">
            <span class="answersProgProfile"><?php echo $iAllAnswersOnUnit ?></span>
        </li>
    </ul>
    <ul class="sectionsProgressList4 left">
        <li>
            <span class="mayuscula"><?php echo Yii::t('mainApp', 'Errores') ?></span>
            <div class="botonayuda">
                <div class="floatHelpBoxprogreso ">
                    <?php echo Yii::t('mainApp', 'errores_cometidos') ?>
                </div>
            </div>
        </li>
        <li>
            <span class="answersProgProfile">-</span>
        </li>
        <li>
            <span class="answersProgProfile">-</span>         
        </li>
        <li>
            <span class="answersProgProfile"><?php echo $followup->dic_err; ?></span>        
        </li>
        <li>
            <span class="answersProgProfile">-</span>
        </li>
        <li>
            <span class="answersProgProfile">-</span>
        </li>
        <li>
            <span class="answersProgProfile"><?php echo $followup->wri_err; ?></span>
        </li>
        <li>
            <span class="answersProgProfile">-</span>
        </li>
        <li>
            <span class="answersProgProfile">-</span>
        </li>
        <li class="final">
            <span class="answersProgProfile"><?php echo $iAllErrorsOnUnit; ?></span>
        </li>
    </ul>
    <div class="clear"></div>
</div>