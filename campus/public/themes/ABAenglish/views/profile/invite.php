<?php
$this->pageTitle=Yii::app()->name . ' - Profile';
$this->breadcrumbs=array(
'Profile',
);
// Navigation index (BreadCrumb):
$this->widget('application.components.widgets.BreadcrumbHeader', array('curMenuSectionTab' => 'mgm_invite'));
// Row of Tabs, auto-detect the active one:
$this->renderPartial("profileTabs", array("fDisplayInvoices" => ''));

$mgmPending = true;
$mgmAdvice = '';
$daysDiff = 0;

if($mgm !== false && $mgm['expireDate'] !== NULL) {
    $daysDiff = HeDate::getDifferenceDateSQL($mgm['expireDate'], date('Y-m-d'), 86400, false);
}
if($mgm !== false && $mgm['expireDate'] !== NULL && $daysDiff <= 0) {
    // if the date expired means the user is free again
    $mgmPending = false;
}
else
{
    if(is_null($mgm)) {
        // no text
    }
    else if($mgm['registered'] < MGM_REQUIRED_USERS) {
        $missing = MGM_REQUIRED_USERS - $mgm['registered'];
        if($missing === 1) {
            $mgmAdvice = Yii::t('mainApp', 'profile_mgm_1_friend_missing');
        }
        else {
            $mgmAdvice = str_replace('#', $missing, Yii::t('mainApp', 'profile_mgm_x_friends_missing'));
        }
    }
    else {
        $mgmPending = false;
        $daysDiff = HeDate::getDifferenceDateSQL($mgm['expireDate'], date('Y-m-d'), 86400, false);
        if($daysDiff === 1) {
            $mgmAdvice = Yii::t('mainApp', 'profile_mgm_1_day_expire');
        }
        else {
            $mgmAdvice = str_replace('#', $daysDiff, Yii::t('mainApp', 'profile_mgm_x_days_expire'));
        }
    }
}
?>
<div class="clear"></div>
<div class="form div-profile-form left">
    <p class="titulosProfile left"><?php echo Yii::t('mainApp', 'mgm_invite') ?></p>
    <div class="clear"></div>
    <p class="profile_mgm_text"><?php echo Yii::t('mainApp', 'welcome_mgm_text'); ?> <?php if($mgmPending) echo '*'; ?></p>
    <div align="center">
        <?php if($mgmAdvice !== '') { ?>
            <div class="profile_mgmAdvice"><?php echo $mgmAdvice; ?></div>
        <?php } ?>
        <?php if($mgmPending) { ?>
            <div id="mgmWidget">
                <?php
                $absoluteUrl = Yii::app()->createAbsoluteUrl('profile/invite');
                $this->renderPartial('//invite/index', array('absoluteUrl' => $absoluteUrl, 'userId' => Yii::app()->user->getId(), 'userLang' => Yii::app()->user->getLanguage(),
                                     '_GET' => $_GET, '_POST' => $_POST)); ?>
            </div>
        <?php } ?>
    </div>
</div>
<div class="clear"></div>