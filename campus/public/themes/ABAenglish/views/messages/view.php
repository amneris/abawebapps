<?php $this->pageTitle = Yii::app()->name . ' - ' . Yii::t('mainApp', 'Realizar consulta'); ?>
<?php 
    $isIncomeMessage = $viewedMessage->receiver_id == Yii::app()->user->getId();
    $isInBox = $viewedMessage->sender_id != Yii::app()->user->getId();
?>

<?php
//	die($viewedMessage->receiver_id." ".Yii::app()->user->getId()." ".$viewedMessage->sender_id );
	$this->breadcrumbs = array(
		Yii::t('mainApp', 'Messages'),
		($isIncomeMessage ? Yii::t('mainApp', 'Inbox') : Yii::t('mainApp', 'Sent')) => ($isIncomeMessage ? 'inbox' : 'sent'),
		CHtml::encode($viewedMessage->subject),
	);
// Navigation index (BreadCrumb):
$this->widget('application.components.widgets.BreadcrumbHeader', array('curMenuSectionTab' => "DETALLE DEL MENSAJE"));
?>
<div class="div-under-breadcrumb">
    <?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_navigation',array("accessCompose"=>$accessCompose,'teacherPicture' => $teacherPicture)) ?>

    <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'message-delete-form',
            'enableAjaxValidation' => false,
            'action' => $this->createUrl('delete/', array('id' => $viewedMessage->id))
    )); ?>
    <button class="messagesButton left marginLeft16" ><?php echo Yii::t('mainApp', 'Borrar') ?></button>
    <?php $this->endWidget(); ?>
    <?php if(Yii::app()->user->role==TEACHER){ ?>
        <?php
            echo CHtml::form($this->createUrl('history/'), 'get', array('id' => 'message-history-form', 'target' => '_blank'));
        ?>
        <input type="hidden" name="idTeacher" value="<?php echo Yii::app()->user->getId() ?>" />
        <input type="hidden" name="idStudent" value="<?php echo $message->receiver_id ?>" />
        <button class="messagesButton right marginRight16"><?php echo Yii::t('mainApp', 'Ver histórico de mensajes') ?></button>
        <?php echo CHtml::endForm(); ?>
    <?php } ?>
    <div class="clear"></div>

    <div id="oldMessage" class="div-old-message">
        <?php echo ($viewedMessage->body) ?>
    </div>
        <!-- Form COMPOSING A MESSAGE  -->
    <?php if ($isInBox && $accessCompose){ ?>
        <div class="form div-compose-form">
            <?php $form = $this->beginWidget('CActiveForm', array(
                'id' => 'message-form',
                'enableAjaxValidation' => false,
            )); ?>

            <?php echo $form->errorSummary($message); ?>
            <?php if(Yii::app()->user->role==TEACHER){ ?>
                <div class="row-compose">
                    <label><?php echo Yii::t('mainApp', 'Nombre') ?></label>
                    <input type="text" value="<?php echo $NameComplete; ?>" readonly="readonly" />
                </div>
            <?php } ?>
            <div class="clear"></div>
            <div class="row-compose">
                <?php echo $form->hiddenField($message, 'receiver_id'); ?>
                <?php echo $form->error($message, 'receiver_id'); ?>
                <?php echo $form->labelEx($message, 'receiver_id'); ?>
                <input type="text" value="<?php echo $receiverName; ?>" readonly="readonly" />
            </div>
            <div class="clear"></div>
            <div class="row-compose">
                <?php echo $form->labelEx($message, 'subject'); ?>
                <?php echo $form->textField($message, 'subject'); ?>
                <?php echo $form->error($message, 'subject'); ?>
            </div>
            <div class="clear"></div>
            <div class="row-compose">
                <?php echo $form->labelEx($message, 'body'); ?>
                <?php echo $form->textArea($message, 'body'); ?>
                <?php echo $form->error($message, 'body'); ?>
            </div>
            <div class="clear"></div>
            <div class="row-compose buttons">
                <?php echo CHtml::submitButton(Yii::t('mainApp', 'Responder'), array("class" => "SendButton")); ?>
            </div>
            <div class="clear"></div>
            <?php $this->endWidget(); ?>
        </div>
    <?php } ?>
    <?php
        if(Yii::app()->user->role==TEACHER){
            echo '<div class="div-history-messages">';
                foreach($HistoryMessages->data as $message){
                    $user = new AbaUser();
                    $user->getUserById($message->sender_id);
                    $NameSender = $user->attributes['name'].' '.$user->attributes['surnames'];
                    echo '<p>'.Yii::t('mainApp', 'Nombre').': '.$NameSender.'</p>';
                    echo '<p>'.Yii::t('mainApp', 'subject').': '.$message->subject.'</p>';
                    echo '<p>'.Yii::t('mainApp', 'body').': '.$message->body.'</p>';
                    echo '<hr />';
                }
            echo '</div>';
        }
    ?>
</div>