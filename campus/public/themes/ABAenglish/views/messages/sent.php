<?php $this->pageTitle = Yii::app()->name . ' - '.Yii::t('mainApp', 'Consultas enviadas');
	$this->breadcrumbs = array(
		Yii::t('mainApp', 'Messages'),
		Yii::t('mainApp', 'Sent'),
	);
// Navigation index (BreadCrumb):
$this->widget('application.components.widgets.BreadcrumbHeader', array('curMenuSectionTab' => "CONSULTAS ENVIADAS", "msgRight" => Yii::t('mainApp', "Tu profesor asignado es ").$receiverVisibleName));
?>
<div id="divSentMessages" class="div-under-breadcrumb">
<?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_navigation', array('accessCompose' => $accessCompose,'teacherPicture' => $teacherPicture)) ?>

<?php if ($messagesAdapter->data): ?>
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'message-delete-form',
		'enableAjaxValidation' => false,
		'action' => $this->createUrl('delete/')
	)); ?>

    
        <div class="contenedorMessages">
            <?php foreach ($messagesAdapter->data as $index => $message){ ?>
                <?php

                if(strlen($message->subject)>42)
                    $subject = substr($message->subject,0,42)."...";
                else
                    $subject = $message->subject;
                ?>
                    <div class="messageRow">
                        <?php echo CHtml::checkBox("Message[$index][selected]"); ?>
                        <?php echo $form->hiddenField($message,"[$index]id"); ?>
                        <span class="SenderName"><?php echo $message->getReceiverName(); ?></span>
                        <a href="<?php echo $this->createUrl('view/', array('message_id' => $message->id)) ?>"><?php echo $subject ?></a>
                        <span class="date"><?php echo date('d/m/Y', strtotime($message->created_at)) ?></span>
                    </div>
                    <div class="clear"></div>
            <?php } ?>
        </div>
        <div class="clear"></div>
	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('mainApp', 'Borrar seleccionados'), array("class" => "messagesButtonSubmit left")); ?>
	</div>
        <div class="clear"></div>
	<?php $this->endWidget(); ?>

	<?php $this->widget('CLinkPager', array('pages' => $messagesAdapter->getPagination())) ?>
<?php endif; ?>
</div>