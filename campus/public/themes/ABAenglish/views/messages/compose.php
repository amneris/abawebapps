<?php $this->pageTitle=Yii::app()->name . ' - '.Yii::t('mainApp', 'Realizar consulta'); ?>
<?php
	$this->breadcrumbs = array(
		Yii::t('mainApp', 'Messages'),
		Yii::t('mainApp', 'Compose'),
	);
// Navigation index (BreadCrumb):
$this->widget('application.components.widgets.BreadcrumbHeader', array('curMenuSectionTab' => "REALIZAR NUEVA CONSULTA"));

?>

<div id="divComposeMessages" class="div-under-breadcrumb">

<?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_navigation', array('accessCompose' => $accessCompose,'teacherPicture' => $teacherPicture)); ?>

<div class="form div-compose-form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'message-form',
		'enableAjaxValidation' => false,
	)); ?>

        <?php echo $form->errorSummary($model); ?>
        <div class="clear"></div>
        <div class="row-compose">
            <?php echo $form->hiddenField($model, 'receiver_id'); ?>
        </div>
        <div class="clear"></div>
        <div class="row-compose">
            <?php echo $form->labelEx($model, 'receiver_id'); ?>
            <?php if(Yii::app()->user->role==TEACHER){ ?>
                <?php echo CHtml::textField('receiver', $receiverName) ?>
                <?php echo $form->error($model, 'receiver_id'); ?>
            <?php }
            else{ ?>
                <input type="text" value="<?php echo $receiverName; ?>" readonly="readonly" />
            <?php } ?>
        </div>
        <div class="clear"></div>
        <div class="row-compose">
            <?php echo $form->labelEx($model, 'subject'); ?>
            <?php echo $form->textField($model, 'subject'); ?>
            <?php echo $form->error($model, 'subject'); ?>
	</div>
        <div class="clear"></div>
	<div class="row-compose">
            <?php echo $form->labelEx($model, 'body'); ?>
            <?php echo $form->textArea($model, 'body'); ?>
            <?php echo $form->error($model, 'body'); ?>
	</div>
        <div class="clear"></div>
	<div class="row-compose buttons">

      <?php /* escape, encodeURI, encodeURIComponent */ ?>
      <script>
          var sentProfessorMessage = function () {
              try {
                  var sentProfessorMessageParams = {
                      "messageTitle": $("#Message_subject").val(),
                      "messageContent": $("#Message_body").val()
                  };
                  sendDefaultEventToTracker('<?php echo HeCooladata::EVENT_NAME_SENT_PROFESOR_MESSAGE; ?>', sentProfessorMessageParams);
                  $('#message-form').submit();
              }
              catch (e) {
                  console.log(e.message);
              }
              return true;
          }
      </script>

      <?php echo CHtml::button(Yii::t('mainApp', Yii::t('mainApp', 'Enviar')), array(
        "class" => "SendButton",
        "onclick" => "javascript: sentProfessorMessage();"
      )); ?>
	</div>
        <div class="clear"></div>
	<?php $this->endWidget(); ?>
        
</div>
<div class="clear"></div>
<?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_suggest'); ?>
<div class="clear"></div>
</div>
<div class="clear"></div>