<?php
    //$msgCounter = (string)Yii::app()->getModule('message')->getCountUnreadedMessages(Yii::app()->user->getId());
    if(empty($msgCounter))
        $msgCounter="?";
?>
<h3 id="h3InboxMessage">
    <img src="<?= Yii::app()->theme->baseUrl ?>/media/images/teachers/<?= $teacherPicture ?>" />
    <span style="">
        <?php
        echo  Yii::app()->user->getName();
        echo (Yii::t('mainApp', 'messageTeacher')) ;
        ?>
    </span>
</h3>
<div class="clear"></div>
<div class="ContainerMessagesButton">
    <?php
        if($accessCompose===true){
    ?>
            <a href="<?php echo $this->createUrl('compose/') ?>" class="left marginLeft16">
                <span class="messagesButton left"><?php echo (Yii::t('mainApp', 'Realizar nueva consulta')) ?></span>
            </a>
    <?php
        }
        else{
    ?>
            <a id="noAccesCompose" class="left marginLeft16">
                <span class="messagesButton left"><?php echo (Yii::t('mainApp', 'Realizar nueva consulta')) ?></span>
            </a>
    <?php
        }
    ?>
<!--    <a href="--><?php //echo $this->createUrl('inbox/') ?><!--" class="right ContainerNewMessagesTienes">-->
<!--        <span class="starIcon left"></span>-->
<!--        <span class="newMessagesTienes left">--><?php //echo (Yii::t('mainApp', 'Tienes'))."&nbsp;".(string)$msgCounter."&nbsp;" ?><!--</span>-->
<!--        <span class="newMessagesTienesBold left">--><?php //echo (Yii::t('mainApp', 'nuevos mensajes')) ?><!--</span>-->
<!--    </a>-->
</div>
<input id="urlPopup" type="hidden" value="<?php echo Yii::app()->createUrl('modals/PopupPasateaPremiumMessages') ?>" />
<div class="clear"></div>
