<?php $this->pageTitle = Yii::app()->name . ' - '.Yii::t('mainApp', 'MENSAJES RECIBIDOS'); ?>
<?php
	$this->breadcrumbs = array(
		Yii::t('mainApp', 'Mensajes'),
		Yii::t('mainApp', 'Inbox'),
	);
// Navigation index (BreadCrumb):
$this->widget('application.components.widgets.BreadcrumbHeader', array('curMenuSectionTab' => "MENSAJES RECIBIDOS", "msgRight" => Yii::t('mainApp', "Tu profesor asignado es ").$receiverVisibleName));
?>

<div id="divInboxMessages" class="div-under-breadcrumb">
    <?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_navigation', array('accessCompose' => $accessCompose,'teacherPicture' => $teacherPicture)) ?>

    <?php if ($messagesAdapter->data){ ?>
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'message-delete-form',
            'enableAjaxValidation' => false,
            'action' => $this->createUrl('delete/')
        )); ?>

        <div class="contenedorMessages">
            <?php foreach ($messagesAdapter->data as $index => $message){ ?>
                <?php
                
                if(strlen($message->subject)>42)
                    $subject = substr($message->subject,0,42)."...";
                else
                    $subject = $message->subject;
                ?>
                <?php if($message->is_read){ ?>
                    <div class="messageRow">
                        <?php echo CHtml::checkBox("Message[$index][selected]"); ?>
                        <?php echo $form->hiddenField($message,"[$index]id"); ?>
                        <span class="SenderName"><?php if (Yii::app()->user->role==TEACHER){
                                                    echo $message->getSenderName();
                                                } else {
                                                    echo $receiverVisibleName;
                                                } ?></span>
                        <a href="<?php echo $this->createUrl('view/', array('message_id' => $message->id)) ?>"><?php echo $subject ?></a>
                        <span class="date"><?php echo date('d/m/Y', strtotime($message->created_at)) ?></span>
                    </div>
                    <div class="clear"></div>
                <?php
                }
                else{ 
                ?>
                   <div class="messageRow">
                        <?php echo CHtml::checkBox("Message[$index][selected]"); ?>
                        <?php echo $form->hiddenField($message,"[$index]id"); ?>
                        <span class="SenderName bold"><?php if (Yii::app()->user->role==TEACHER){
                                                            echo $message->getSenderName();
                                                        } else {
                                                            echo $receiverVisibleName;
                                                        } ?></span>
                        <span class="starLittleIcon"></span><a class="bold" href="<?php echo $this->createUrl('view/', array('message_id' => $message->id)) ?>"><?php echo $subject ?></a>
                        <span class="date bold"><?php echo date('d/m/Y', strtotime($message->created_at)) ?></span>
                    </div>
                    <div class="clear"></div>
                <?php } ?>
            <?php } ?>
        </div>
        <div class="clear"></div>
        <div class="row buttons">
            <?php echo CHtml::submitButton(Yii::t('mainApp', 'Borrar seleccionados'), array("class" => "messagesButtonSubmit left")); ?>
        </div>
        <div class="clear"></div>
        <?php $this->endWidget(); ?>
        <?php $this->widget('CLinkPager', array('pages' => $messagesAdapter->getPagination())) ?>
    <?php } ?>
</div>

<?php
//#ABAWEBAPPS-701
$sCooladata = HeCooladata::createDefaultEvent(HeCooladata::EVENT_NAME_ENTERED_PROFESSOR_MESSAGE_SECTION);
Yii::app()->clientScript->registerScript(md5($sCooladata), $sCooladata, CClientScript::POS_READY);
?>
