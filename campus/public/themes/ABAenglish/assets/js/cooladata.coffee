abaEvent = (actionName) ->
  this.actionName = actionName;
  this.currentSectionId = null;

abaEvent::getActionName = ->
  return this.actionName;

abaEvent::setActionName = (actionName) ->
  this.actionName = actionName;
  return this.actionName;

#@TODO String
abaEvent::getCurrentSectionId = ->
  return this.currentSectionId;

abaEvent::setCurrentSectionId = (sectionId) ->
  this.currentSectionId = sectionId;
  return this.currentSectionId;

abaEvent::sendToTracker = (eventProperties) ->
  try
    eventPropertiesToTrack = null;

    if (eventProperties)
      eventPropertiesToTrack = eventProperties.getProperties();
      cooladata.trackEvent(this.actionName, eventPropertiesToTrack);
      this.sendEventToConsole(eventPropertiesToTrack);

  catch err
    console.error('error sending events to event tracker');

abaEvent::sendEventToConsole = (eventProperties) ->
  try
    if (eventProperties)
      console.group("Event : " + this.actionName);
      $.each(eventProperties, (sKey, sValue) ->
        console.debug(sKey + " -> " + sValue);
      )
      console.groupEnd();
  catch err
    console.error('error sending events to event tracker');

ABA_COOLADATA_EVENT = new abaEvent null;

sendEventToTracker = (action, eventProperties) ->
  ABA_COOLADATA_EVENT.setActionName(action);
  ABA_COOLADATA_EVENT.sendToTracker(eventProperties);

abaUserEventPropertiesFromCourse = (eventAction, parametersCourse) ->
  parameters = {}
  try
    $.each(parametersCourse, (sKey, sValue) ->
      if (sKey == 'course')
        $.each(sValue, (sKeyCourse, sValueCourse) ->
          switch sKeyCourse
            when 'levelId' then parameters[sKeyCourse] = cooladataLevelId
        )
      else
        parameters[sKey] = sValue
    )
  catch err
    console.log("::COOLADATA::009::" + err.message + "::")

  return new abaUserEventProperties(eventAction, parameters)

abaUserEventProperties = (eventAction, parameters) ->
  eventProperties = {}

  @addProperties = (key, value) ->
    eventProperties[key] = value

  @getProperties = () ->
    return eventProperties

  try

    @addProperties('idSite', 1)
    @addProperties('event_timestamp_epoch', (new Date).getTime())
    @addProperties('old_platform', 1) #property to indicate the event is received from the old platform

    that = @
    $.each(parameters, (sKey, sValue) ->
      if sKey == 'customerUserId'
        that.addProperties('user_id', sValue)

      else if (eventActions[eventAction].properties)
        label = eventActions[eventAction].properties[sKey].label
        that.addProperties(label, sValue)
    )

    return

  catch err
    console.log("::COOLADATA::010::" + err.message + "::")