#Common
preventSubmitOnEnter = false

parsePlanName = (planKey, countryISO) ->
  parsePlan = planKey.split('|')
  if parsePlan.length > 0
    days = parsePlan[parsePlan.length - 1]
    nameForGA = countryISO + '_' + days;
    parseProductId = parsePlan[0].split('_');

    if parseProductId.length == 3
      nameForGA += '_familyPlan'

    return {
      nameForGA : nameForGA,
      days: days
    }

  return false

#Packages Page
pricesWrapper = $('.ab-prices-js')

#Check if submit on Enter is valid
submitOnEnter = ->
  $(window).keydown (event) ->
    if (event.keyCode == 13 and preventSubmitOnEnter is true)
      event.preventDefault()
      event.stopPropagation()
      return false

popUpPaymentsErrors = (url) ->
  $.ajax
    cache: true
    type: 'GET'
    url: url
    async: false

    success: (response) ->
      $dialog = $("<div id='sharerdialog'>#{response}</div>")

      $dialog.dialog
        autoOpen: false
        title: false
        position: 'center'
        width: 350
        resizable: false
        modal: true
        open: (event, ui) ->
          content = $('a.ui-dialog-titlebar-close').html()
          $('#ui-dialog-title-sharerdialog').remove()
          $('a.ui-dialog-titlebar-close').empty()
          $('a.ui-dialog-titlebar-close').html(content)
          $('div.ui-dialog').attr('id', 'CampusModals')

        close: ->
          $('#sharerdialog').empty().remove()
          $("#sharerdialog").dialog 'destroy'

      $dialog.dialog 'open'

togglePaymentConditions = (id) ->
  if (id == 'termsAndConditionsBox')
    $('#privacyBox').hide()
  else
    $('#termsAndConditionsBox').hide()

  $('#' + id).toggle()

$ ->
  submitOnEnter()

  # Card errors Tooltip
  $('#cardPosIssues').on 'click', (e) ->
    e.preventDefault()
    popUpPaymentsErrors($('#urlPopup').val())

  # Toggle promo code field
  $('.promo-toggle-js').on 'click', (e) ->
    e.stopPropagation()
    $('.promo-wrapper-js').toggleClass('hide')

  # Promo code submit
  $('.promo-btn-js').on 'click', (e) ->
    e.preventDefault()
    promoInput = $('.promo-input-js')
    promoCode = $.trim(promoInput.val())
    $('#promocode').val(promoCode)

    if (promoCode != '')
      $('#promocode-form').submit()


  $('.payment-select-js').on 'click', (e) ->
    e.stopPropagation()
    $('#idProductPricePKSelected').val(e.target.name)

    if parseInt($('#trackEventsGoogleAnalytics').val()) == 1
      plan = parsePlanName(e.target.name, $('#iso').val())

      if (plan)
        ga('send', 'event', 'Plans', 'Select', plan.nameForGA)
        sendDefaultEventToTracker('SELECTED_PRICE_PLAN', {"selectedPlan": plan.days});


    $('#payment-form').submit()

  $('.ab-logo-wrapper').on 'click', (e) ->

    if parseInt($('#trackEventsGoogleAnalytics').val()) == 1
      ga('send', 'event', 'Plans', 'Exit', 'Logo')

#Payment page

$(document).on 'click', '.payment-input-js', (e) ->
  e.stopPropagation()
  payMethod = $(@).val()
  paymentURL = $(@).parent().find('.paymentURL-js').attr('data-url')
  paymentForm = $('form#payment-form')
  paymentForm.attr("action", paymentURL)

  aFieldsByMethod = $.parseJSON(fieldsByMethod)
  aFields = aFieldsByMethod[payMethod]
  creditCardFields = $(".ab-credit-card-details-js")
  creditCardFields.addClass('hidden')

  if payMethod is PAY_METHOD_ADYEN_HPP
    # Prevent submit on Enter
    preventSubmitOnEnter = true

    $("#payment-form").attr("action", paymentURL)
    $(".btn-payment-js").addClass("hidden")
    $(".ab-adyen-details-js").removeClass("hidden")

    hppData =
      "paymentAmount":   ADYEN_HPP_PAYMENTAMOUNT
      "currencyCode":    ADYEN_HPP_CURRENCYCODE
      "skinCode":        ADYEN_HPP_SKINCODE
      "countryCode":     ADYEN_HPP_COUNTRYCODE
      "sessionValidity": ADYEN_HPP_SESSIONVALIDITY
      "idPayMethod":     PAY_METHOD_ADYEN_HPP
      "idProduct":       ADYEN_HPP_IDPRODUCT
    $.ajax
      type:   'POST'
      url:    '/payments/paymentMethodsAdyen'
      data:   hppData
      beforeSend: ->
        $(".ab-adyen-data-container-js").html('')
        $(".ab-adyen-loader-js").removeClass('hidden')
      success: (response) ->
        try
          $(".ab-adyen-loader-js").addClass('hidden')
          $(".ab-adyen-data-container-js").html(response)
        catch err
          console.log err.message

  else
    preventSubmitOnEnter = false
    $(".ab-adyen-details-js").addClass("hidden")
    $(".btn-payment-js").removeClass("hidden")
    if payMethod is PAY_METHOD_CARD then creditCardFields.removeClass('hidden')

  # Setup all hidden
  $(".optional-form-group-js").addClass('hidden')

  # Show specific fields
  for fieldName in aFields
    $("#IdDivInput_#{fieldName}").removeClass('hidden')

  $('.optional-form-groups-js').removeClass('hidden')


$(document).on 'click', '.btn-payment-js', (e) ->
  $(@).addClass('hidden')
  $('.ab-loader').removeClass('hidden')

  if (@.id == "btnABAadyen")
    $("#payment-form-number").val($("#PaymentForm_creditCardNumber").val())
    $("#payment-form-holder-name").val($("#PaymentForm_creditCardName").val())
    $("#payment-form-cvc").val($("#PaymentForm_CVC").val())
    $("#payment-form-expiry-month").val($("#PaymentForm_creditCardMonth").val())
    $("#payment-form-expiry-year").val($("#PaymentForm_creditCardYear").val())
  else
    e.stopPropagation()
    e.preventDefault()

    if ($('#PaymentForm_idPayMethod_0').is(":checked") || $('#PaymentForm_idPayMethod_2').is(":checked"))
      $('#redirectCreditCard').removeClass('hidden')
    else
      $('#redirectPaypal').removeClass('hidden')
    $('form#payment-form').submit()

$(document).on 'keyup', '#PaymentForm_creditCardNumber', (e) ->
  string = this.value;
  output = '';
  cursor = string.slice(0, this.selectionStart).length
  for i in [0 .. string.length]
    char = string.charAt(i)
    if !isNaN(char) && char != ' '
      output += char
    else
      cursor = cursor - 1

  this.value = output
  this.selectionStart = cursor
  this.selectionEnd = cursor

# Checkout page

trackOpenCheckoutPage = (planKey, countryIso) ->
  plan = parsePlanName(planKey, countryIso)

  if (plan)
    ga('send', 'event', 'Checkout', 'View', plan.nameForGA)