#open dialog without content
class CampusPopup
  container: null,

  isOpen: ->
    return if campusPopup.container then campusPopup.container.dialog('isOpen') else false

  setup: (content, closeBtn, width) ->

    if !campusPopup.isOpen()

      width = 650 if !width
      popup = document.createElement('div')
      popup.innerHTML = content
      campusPopup.container = $(popup)
      campusPopup.container.dialog(
        closeOnEscape: false
        autoOpen: false
        title: false
        width: width
        resizable: false
        draggable: false
        modal: true
        open: (event, ui) ->
          campusPopup.container[0].setAttribute('id', 'CampusModals')
        close: (event, ui) ->
          campusPopup.container.dialog('destroy')
          campusPopup.container = null
      )
      campusPopup.container.dialog('open')

    else
      campusPopup.container[0].innerHTML = content

    campusPopup.setupCloseBtn(closeBtn)
    levelTestClickHandler()

  renderHTML: (html, closeBtn) ->
    campusPopup.setup(html, closeBtn)
    campusPopup.center()

  renderEmpty: ->
    campusPopup.setup('', false)
    campusPopup.renderLoading()
    campusPopup.center()

  renderLoading: ->
    if !document.getElementById('popupLoading')
      loader = document.createElement('div')
      loader.id = 'popupLoading'
      loader.innerHTML = '<div class="loading"></div>'

      campusPopup.container[0].appendChild(loader)

  renderHiddenElement: (id, closeBtn) ->
    container = document.getElementById(id)
    if container
      html = container.innerHTML
      removeElementFromId(id)
      campusPopup.renderHTML(html, closeBtn)

  renderAjax: (url, data, onSuccess) ->
    campusPopup.renderEmpty() if !campusPopup.isOpen()
    $.ajax(
      type: 'POST'
      url: url
      data: data
      success: (response) ->
        if onSuccess
          onSuccess(response)
          return

        try
          parsed = $.parseJSON(response)

          if typeof parsed == 'object' # in case of json

            if parsed.url
              window.location.href = parsed.url

            else if parsed.html
              campusPopup.renderHTML(parsed.html)

        catch e
          campusPopup.renderHTML(response)
    )

  renderAjaxRequest: ($form, onSuccess) ->
    campusPopup.renderLoading()
    url = $form.attr('action')
    data = $form.serialize()
    campusPopup.renderAjax(url, data, onSuccess)

  setupCloseBtn: (withCloseBtn) ->
    withCloseBtn = document.getElementsByClassName('popupCloseBtn').length > 0 if withCloseBtn == undefined
    if campusPopup.isOpen()
      removeElementsFromClassName('ui-widget-header')

      if withCloseBtn
        element = document.createElement('div')
        if withCloseBtn == 'true'
          element.innerHTML = '<div class="ui-widget-header"><span class="ui-icon ui-icon-closethick" onclick="campusPopup.close()"></span></div>'
        else if withCloseBtn == 'render'
          element.innerHTML = '<div class="ui-widget-header"><span class="ui-icon ui-icon-closethick" onclick="campusPopup.renderAjax(\'/welcome/memberGetMember\',\'\',\'\');"></span></div>'

        campusPopup.container[0].insertBefore(element, campusPopup.container[0].children[0])
##
  center: ->
    if campusPopup.isOpen()
      campusPopup.container.dialog('option', 'position', { my: 'center', at: 'center', of: window })

  close: ->
    if campusPopup.isOpen()
      campusPopup.container.dialog('close')

class LevelTest
  start: ->
    selectedOption = $('input[name=nivelActual]:checked').val()
    return false if !selectedOption
    campusPopup.renderAjaxRequest($('#formLevelSelect'))

  startDemo: ->
    campusPopup.renderAjax('/modals/levelTest')

  nextQuestion: ->
    selectedOption = $('input[name=radioTestOptions]:checked').val()
    return false if !selectedOption
    campusPopup.renderAjaxRequest($('#CampusModals #levelTestQuestion'), levelTest.checkAnswerAndContinue)

  checkAnswerAndContinue: (response) ->
    try
      parsed = $.parseJSON(response)

      if typeof parsed == 'object' # in case of json

        if parsed.html

          answerSelected = $('input:radio[name=radioTestOptions]:checked')
          if answerSelected
            answerWrapper = answerSelected.closest('li')

            color = if parsed.okAnswer then 'green' else 'red'
            icon = document.createElement('span')
            icon.className = if parsed.okAnswer then 'ico-success ok' else 'ko'
            icon.style.marginLeft = '15px'

            answerWrapper.css('border-color', color)
            answerWrapper.find('span').css('color', color)
            answerWrapper.find('label').append(icon)

            setTimeout ( ->
              campusPopup.renderHTML(parsed.html)
            ), 800

    catch e

  finish: ->
    campusPopup.renderAjaxRequest($('#CampusModals #finishTest'))

  close: ->
    campusPopup.close()

class MemberGetMember
  renderInvite: ->
    document.getElementById('mgmPre').style.display = 'none'
    document.getElementById('mgmWidget').style.display = 'block'
    document.getElementById('welcome_mgm_text').innerHTML += '*'
    campusPopup.setupCloseBtn(true)
    campusPopup.center()

# global functions
removeElementFromId = (id) ->
  removeElement = document.getElementById(id)
  removeElement.parentNode.removeChild(removeElement) if removeElement

removeElementsFromClassName = (className) ->
  removeElements = document.getElementsByClassName(className)
  while removeElements.length > 0
    removeElements[0].parentNode.removeChild(removeElements[0])

URLToArray = (url) ->
  request = {}
  pairs = url.substring(url.indexOf('?') + 1).split('&')
  for pair in pairs
    pair = pair.split('=')
    request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1])

  return request

window.addEventListener 'resize', ((event) ->
# timeout required for proper working
  setTimeout(campusPopup.center, 100)
), false

window.addEventListener 'scroll', ((event) ->
# timeout required for proper working
  setTimeout(campusPopup.center, 100)
), false

levelTestClickHandler = () ->
  $("ul.welcomeLevelTest li").off("click").on "click", ->
    $(@).find("input").prop "checked", true


campusPopup = new CampusPopup()
memberGetMember = new MemberGetMember()
levelTest = new LevelTest()