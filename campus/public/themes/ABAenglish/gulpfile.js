var gulp = require('gulp'),
  runSequence = require('gulp-run-sequence'),
  sass = require('gulp-sass'),
  concat = require('gulp-concat'),
  cssnano = require('gulp-cssnano'),
  coffee = require('gulp-coffee'),
  gutil = require('gulp-util'),
  uglify = require('gulp-uglify'),
  rimraf = require('gulp-rimraf'),
  watch = require('gulp-watch');

gulp.task('clean_js', function() {
  gulp.src('assets/out/application.js', {read: false})
    .pipe(rimraf().on('error', gutil.log));
});

gulp.task('coffee', function() {
  gulp.src('assets/js/*.coffee')
    .pipe(coffee({bare: true}).on('error', gutil.log))
    .pipe(gulp.dest('assets/js/'))
});

gulp.task('sass', function() {
  gulp.src([
    'assets/css/*.scss',
    '!assets/css/*scsslint_tmp*.scss'
  ])
    .pipe(sass({
      includePaths: require('node-bourbon').includePaths
    }).on('error', gutil.log))
    .pipe(gulp.dest('assets/css/'));
});

var paths = {
  css: 'assets/css/*.css',
  js: 'assets/js/*.js'
};

gulp.task('js', ['clean_js'], function() {
  gulp.src([
    'assets/js/*.js',
    '!assets/js/payments.js',
    '!assets/js/cooladata.js'
  ])
    .pipe(concat('application.js'))
    .pipe(uglify())
    .pipe(gulp.dest('assets/out/'))
});

gulp.task('payments-js', ['clean_js'], function() {
  gulp.src([
    'assets/js/payments.js'
  ])
    .pipe(uglify().on('error', gutil.log))
    .pipe(gulp.dest('assets/out/'))
});

gulp.task('cooladata-js', ['clean_js'], function() {
  gulp.src([
    'assets/js/cooladata-events.js',
    'assets/js/cooladata.js'
  ])
      .pipe(concat('cooladata.js'))
      .pipe(uglify().on('error', gutil.log))
      .pipe(gulp.dest('assets/out/'))
});

gulp.task('styles', function() {
  gulp.src([
    'assets/css/main.css'
  ])
    .pipe(concat('application.css'))
    .pipe(cssnano())
    .pipe(gulp.dest('assets/out/'))
});

gulp.task('payments_styles', function() {
  gulp.src([
    'assets/css/payments.css'
  ])
    .pipe(concat('payments.css'))
    .pipe(cssnano())
    .pipe(gulp.dest('assets/out/'))
});

gulp.task('basic_layout_styles', function() {
  gulp.src([
    'assets/css/basic.css'
  ])
    .pipe(concat('basic.css'))
    .pipe(cssnano())
    .pipe(gulp.dest('assets/out/'))
});

gulp.task('campus-v5_styles', function() {
  gulp.src([
    'assets/css/campus-v5.css'
  ])
    .pipe(concat('campus-v5.css'))
    .pipe(cssnano())
    .pipe(gulp.dest('assets/out/'))
});

gulp.task('testing-sass', function() {
  gulp.src('css/ab-testing.scss')
    .pipe(sass({
      includePaths: require('node-bourbon').includePaths
    }).on('error', gutil.log))
    .pipe(gulp.dest('css/'));
});

gulp.task('testing-coffee', function() {
  gulp.src('js/ab-testing.coffee')
    .pipe(coffee({bare: true}).on('error', gutil.log))
    .pipe(gulp.dest('js/'))
});

gulp.task('watch', function() {

  /* Testing */
  gulp.watch('css/ab-testing.scss', ['testing-sass']);
  gulp.watch('js/ab-testing.coffee', ['testing-coffee']);
  /* Testing */
  gulp.watch('assets/js/*.coffee', ['coffee']);
  gulp.watch('assets/css/*.scss', ['sass']);
  gulp.watch('assets/css/shared/*.scss', ['sass']);
  gulp.watch('assets/css/main.css', ['styles']);
  gulp.watch('assets/css/payments.css', ['payments_styles']);
  gulp.watch('assets/css/basic.css', ['basic_layout_styles']);
  gulp.watch('assets/css/campus-v5.css', ['campus-v5_styles']);
  gulp.watch('assets/js/*.js', ['js']);
  gulp.watch('assets/js/payments.js', ['payments-js']);
  gulp.watch('assets/js/cooladata.js', ['cooladata-js']);
});

gulp.task('default', function() {
  runSequence(
    ['testing-sass', 'testing-coffee', 'coffee', 'sass'],
    ['styles', 'campus-v5_styles', 'payments_styles', 'basic_layout_styles', 'js', 'payments-js', 'cooladata-js']);
});
