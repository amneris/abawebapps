var abTesting;

abTesting = {};

abTesting.runOrangeButtonColorTest = function() {
  return $('body').addClass('ab-color-testing-wrapper');
};

abTesting.runLiveChatTest = function() {
  if (includeChatSupport) {
    return includeChatSupport();
  }
};

abTesting.runFamilyPlanTest = function() {
  $('.ab-family-tooltip, .ab-family').hide();
  return $('.ab-package-header.with-bg').removeClass('with-bg');
};

abTesting.runMemberGetMemberTest = function() {
  if (document.cookie.indexOf('mgm=0;') === -1) {
    return document.cookie = 'mgm=1;path=/;';
  }
};
