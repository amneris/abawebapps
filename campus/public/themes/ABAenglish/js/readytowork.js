$(document).ready(function() {
  $('#divRightContent').on('click', '#hearCampus', function(event) {
    event.preventDefault();
    event.stopPropagation();
    $('#contentTooltipCampus').removeClass().addClass('tooltip_1buttons');
    $('#contentTooltipCampus > span').hide();
    $('#contentTooltipCampus > span#hearCampusButton')
      .show()
      .off('click')
      .on('click', function(event2) {
        event2.preventDefault();
        event2.stopPropagation();
        escucharTest(event, 'hearCampusButton');
      });
    var $target = $(event.target);
    positionTooltip($target);
    escucharTest(event, 'hearCampusButton');
  })
    .on('click', '#recordCampus', function(event) {
      event.preventDefault();
      event.stopPropagation();
      $('#contentTooltipCampus').removeClass().addClass('tooltip_1buttons');
      $('#contentTooltipCampus > span').hide();
      $('#contentTooltipCampus > span#recordCampusButton')
        .show()
        .off('click')
        .on('click', function(event3) {
          //Check if the user have allow access to the Micro
          if (getMicroPermission()) {
            event3.preventDefault();
            event3.stopPropagation();
            var recordClasses = $('#recordCampusButton').attr('class');
            if (recordClasses.search('BlinkTooltip') === -1)
              grabar();
            else if (recordClasses.search('BlinkTooltip') !== -1)
              StopRecording();
          }else {
            showFlashPannel();
          }

        });

      var $target = $(event.target);
      positionTooltip($target);
    })
    .on("click", "#compareCampus", function(event){
      event.preventDefault();
      event.stopPropagation();
      $('#contentTooltipCampus').removeClass().addClass('tooltip_1buttons');
      $('#contentTooltipCampus > span').hide();
      $('#contentTooltipCampus > span#compareCampusButton')
        .show()
        .off('click')
        .on("click", function(event3){
          event3.preventDefault();
          event3.stopPropagation();
          comparePasos("1");
        });
      compare(event);
    })
    .on("click", "#p1", function(event){
      event.stopPropagation();
      $("#pregunta2").css("display", "block");
      $("#recordCampus").trigger('click');
      $("#p1EscucharFalse").css("display", "none");
      $("#ocultar").css("display","none");


    })
    .on("click", "#p3", function(){
      $("div.testSoftwareFooter").css("display", "block");
      $("#p2EscucharFalse").css("display", "none");
      $("#ocultar").css("display","none");
    })
    .on("click", "#p1n", function(){
      $("#p1EscucharFalse").css("display", "block");
      $("#ocultar").css("display","block");
    })
    .on("click", "#p3n", function(){
      $("#p2EscucharFalse").css("display", "block");
      $("div.testSoftwareFooter").css("display", "none");
      $("#ocultar").css("display","block");

    });

  $("body").on("click", function(event){
    var class1 = $("#hearCampusButton").attr("class");
    var class2 = $("#recordCampusButton").attr("class");
    var class3 = $("#compareCampusButton").attr("class");
    var $target = $(event.target);
    if((class1.search("BlinkTooltip") === -1)&&(class2.search("BlinkTooltip") === -1)&&(class3.search("BlinkTooltip") === -1)&&($target.attr("id")!="contentTooltipCampus")&&($target.attr("id")!="recordCampusButton")&&($target.attr("id")!="compareCampusButton"))
      $("#contentTooltipCampus").css("display","none");
  });

  $(window).on('resize', onWindowResize);
});
function onWindowResize() {
  var pos = campusUtils.getPos(document.getElementById("divRightContent"));
  var popup = document.getElementsByClassName('ui-dialog')[0];
  if(popup !== null && popup !== undefined) {
    repositionFlashPanel();
  }
  var overlay = document.getElementById('overlay');
  if(overlay !== null && overlay !== undefined) {
    overlay.style.top = pos.y + "px";
    overlay.style.left = pos.x + 15 + "px";
  }
}

function showPermissionConfirmation() {
  if (getMicroPermission()) {
    $('#overlay').hide();
    comprovacionDePermisos();
  }
  else {
    showFlashPannel();
  }
}

function showFlashPannel(){
  popUpFlash($("#urlPopupFlash").val());

  document.getElementById("ABAPlayer3").SWFShowPanelPermission();

  $("#ABAPlayer3").css("height", "");
  $("#ABAPlayer3").css("visibility", "visible");
  repositionFlashPanel();
}

function repositionFlashPanel() {
  var parent = campusUtils.getPos(document.getElementById("divRightContent"));
  var offsetX = 382 + 1000;
  var offsetY = 158 + 1000;
  var flashPlayer = document.getElementById("ABAPlayer3");
  if(flashPlayer !== null && flashPlayer !== undefined) {
    flashPlayer.style.top = parent.y + offsetY + "px";
    flashPlayer.style.left = parent.x + offsetX + "px";
  }
}

function callPanelSettingsReady(microallowed){
  if ($('#sharerdialog').length > 0){
    $('#sharerdialog').dialog("close");
  }
  $('#overlay').hide();
  comprovacionDePermisos();
}

function callConnectionOn() {
  showPermissionConfirmation();
}

function showOverlay() {
  var overlay = '<div id="overlay"><span id="loading_ready"></span></div>';
  var height = $('#divRightContent').height();
  var width = $('#divRightContent').width();
  var pos = campusUtils.getPos(document.getElementById('divRightContent'));

  $('#divRightContent').append(overlay);
  $('#overlay').css({width: width, height: height, left: pos.x + 15, top: pos.y});
}

function popUpFlash(url){
  $.ajax({
    beforeSend: function(){
    },
    cache: true,
    type: "GET",
    url: url,
    async: false,
    success: function(response){
      var $dialog = $('<div id="sharerdialog">'+response+'</div>');
      $dialog.dialog({
        closeOnEscape: true,
        autoOpen: false,
        title: false,
        position: [-1000, -1000],
        width: 696,
        height:400,
        /*height: auto,*/
        resizable: false,
        dialogClass: 'microPermissionModal',
        modal: true,
        draggable: false,
        open: function(event, ui) {
          var aux = $("a.ui-dialog-titlebar-close").html();
          $("#ui-dialog-title-sharerdialog").remove();
          $("a.ui-dialog-titlebar-close").empty();
          $("a.ui-dialog-titlebar-close").html(aux);
          $("div.ui-dialog").attr("id","CampusModals");
          var premium = $('#divBtnGoPremium').attr('href');
          $('.popup_anuncioPremium a').attr('href', premium);

          $('#CampusModals.ui-dialog').css('position', 'absolute');
          campusUtils.centerModal();
        },
        close: function(event, ui){
          $('#sharerdialog').empty().remove();
          $("#sharerdialog").dialog('destroy');
        }

      });
      $dialog.dialog('open');
    }
  });
}

function comprovacionDePermisos() {
  $("#ABAPlayer3").css("visibility", "hidden");
  $("#testFlash").css("display","block");
  $("div.separatorReadytoWorkFooter.left").css("display","block");
  if(!DetectFlashVer(9, 0, 28)){
    $("#TestFlashExiste > #ko").css("display", "inline");
    $("#ocultar").css("display", "inline");
  }
  else{
    $("#pregunta1").css("display", "block");
    $("#TestFlashExiste > #ko").css("display", "none");
    $("#TestFlashExiste > #ok").css("display", "inline");
    /*$("#ocultar").css("display", "inline");*/

  }
  if(GetSwfVer())
    $("#TestVersion").html('<span class="iconCheckModal left"></span><span class="text">Flash Player version: '+GetSwfVer()+'.</span>');
  if(!getMicroPermission())
    $("#TestdeMicrofono").empty().remove();

  else
    $("#TestdeMicrofono").css("display", "block");


}
function getMicroPermission(){
  return document.getElementById("ABAPlayer3").SWFHaveMicroPermission();
}
function escucharTest(event, Button){
  ParpadeoBoton(Button);
  rol = rol != "compare1" ? "escuchar" : "compare1";
  HTMLListenElement();
}
function callConfirmed(){
  //comprovacionDePermisos();
}
function HTMLListenElement(){
  var audioLocation = "0";
  var id_audio = "ST00006";
  var username = getUsernameProxy();
  audioLocation = "units/unit001/audio/"+id_audio;
  var param = "text:" + username + ":001:" + audioLocation + ":demo";
  var prova = this.SWFToPlayer(param);
  return prova;
}

function SWFToPlayer(inRequest){
  document.getElementById("ABAPlayer3").toActionScript(inRequest);
}

function SWFProxy(url_aba){
  var flashObject = document.getElementById("ABAPlayer3");
  if(flashObject != undefined)
    flashObject.SWFProxy(url_aba);
}

function getUsernameProxy(){
  var theUser =$("#userMail").val().replace(/\./g, "_");
  var theRelease=sReleaseCampus;
  if(theRelease !='' && !isNaN(theRelease) && theRelease !="3")
    return (theRelease+"/"+theUser);
  else
    return theUser;
  //  return $("#userMail").val().replace(/\./g, "_");
}
function grabar(){
  ParpadeoBoton("recordCampusButton");
  document.getElementById("ABAPlayer3").SWFRecordAudio("text:" + getUsernameProxy() + ":000:ST00006:record");
}
function StopRecording(){
  var $target = $("#recordCampusButton");
  PararParpadeo($target);
  PonerVerde($target);
  setTimeout(function() {
    $("#pregunta3").css("display", "block");
    $("#compareCampus").trigger('click');
  }, 1000);
}
function compare(event)
{
  var $target = $(event.target);
  positionTooltip($target);
}
function callPlayEnd()
{
  switch(rol)
  {
    case "compare1":
      comparePasos("2");
      break;
    case "compare2":
      var $target = $("#compareCampusButton");
      PararParpadeo($target);
      PonerVerde($target);
      $("#p3").parent(".pregunta").css("display", "block");
      break;
    case "escuchar":
      var $target = $("#hearCampusButton");
      PararParpadeo($target);
      PonerVerde($target);
      MostrarPrimeraPregunta();
      break;
    default:
      break;
  }
}
function comparePasos(paso){
  if(paso==="1"){
    rol = "compare1";
    ParpadeoBoton("compareCampusButton");
    document.getElementById("ABAPlayer3").toActionScript("text:" + getUsernameProxy() + ":000:ST00006:student");
  }
  else if(paso==="2"){
    rol = "compare2";
    HTMLListenElement();
  }
}
function ParpadeoBoton(Button)
{
  var $target = $("#"+Button);
  PararParpadeo($target);
  $target.addClass("BlinkTooltip");
  iconBlink = window.setInterval(function(){
    $target.fadeOut(500).fadeIn(500);
  }, 1000);
}
function PararParpadeo($target){
  window.clearInterval(iconBlink);
  $target.removeClass("BlinkTooltip");
}
function positionTooltip($target){
  $("#contentTooltipCampus").css("position", "absolute").css("left", ($target.offset().left + (($target.innerWidth()/2) - ($("#contentTooltipCampus").innerWidth()/2)))+"px").css("top", ((parseInt(Math.ceil($target.offset().top)) - $("#contentTooltipCampus").outerHeight(true)))+"px").css("display", "block");
}
function MostrarPrimeraPregunta(){
  $("#p1").parent(".pregunta").css("display", "block");

}
function PonerVerde($target){
  $target.addClass("buttonTooltipCampus_audio_hold");
}

var rol;
var iconBlink;
var sReleaseCampus;