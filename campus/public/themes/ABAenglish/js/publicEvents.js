$(document).ready(function() {
  $('#LoginForm_email').on('change', function(e) {
    $(e.target).val($.trim($(e.target).val()));
  });

  $('#RecoverPwdForm_email').on('change', function(e) {
    $(e.target).val($.trim($(e.target).val()));
  });

  // Actions for the Form of the Login Page through the Downloadable Application
  $('#emailHint').click(function() {
    $('#LoginForm_email').focus();
  });

  $('#passHint').click(function() {
    $('#LoginForm_password').focus();
  });
});