$(document).ready(function() {
  $('#ProfileForm_birthDate').datepicker({
    dateFormat: 'dd-mm-yy',
    changeYear: true,
    changeMonth: true,
    firstDay: 1,
    autoSize: true,
    yearRange: '-82:+0',
    maxDate: '+0D'
  });
});