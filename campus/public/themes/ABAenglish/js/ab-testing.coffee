abTesting = {}

abTesting.runOrangeButtonColorTest = () ->
  $('body').addClass 'ab-color-testing-wrapper'

abTesting.runLiveChatTest = () ->
  includeChatSupport() if includeChatSupport

abTesting.runFamilyPlanTest = () ->
  $('.ab-family-tooltip, .ab-family').hide()
  $('.ab-package-header.with-bg').removeClass('with-bg')

abTesting.runMemberGetMemberTest = () ->
  document.cookie = 'mgm=1;path=/;' if document.cookie.indexOf('mgm=0;') == -1