var Utils = function() {}

Utils.prototype = {
  showLoader: function() {
    $.fancybox(' ', {modal: true});
    $.fancybox.showLoading();
    $('.fancybox-wrap').hide();
  },

  hideLoader: function() {
    $.fancybox.close();
    $.fancybox.hideLoading();
  },

  showPopup: function(content) {
    $.fancybox({
      content: content,
      closeBtn: true,
      helpers: {
        overlay: {closeClick: false} // prevents closing when clicking OUTSIDE fancybox
      }
    });

    return true;
  }
}

var g_utils = new Utils();