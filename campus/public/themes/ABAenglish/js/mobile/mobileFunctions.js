/** Fet the call through AJAX to get the Popup when the user click on "Try for Free"
 */
$(document).ready(function(){

    var urlPopup = $('#url_Popup').val();
    //Do an Ajax call when the user click on "try for free"
    $('#try_for_free_button').click(function(){
        $.ajax({
            cache: true,
            type: "GET",
            url: urlPopup,
            async: false,
            success: function(data){
                onSuccessEmail(data);
            },
            error: function(data, textStatus, response){
                onError();
            }
        });
    });
    /**
     * Things to do when the Ajax Call has been successfull
     * @param data The Html returned from the Ajax Call
     */
    function onSuccessEmail(data){
        $('body').append('<div id="popup_background"></div>');
        $('#popup_background').height($('html').height());
        $('#popup_background').append(data);
        $('#email_accept').click(function(){
            $('#popup_background').hide();
            $('#email_notif').hide();
            $('#boton_try').addClass('boton_try_des');
        });
        $('#try_for_free_button').unbind();

    };

    /**
     * When there is an error on the Ajax call, then we show an poprp with an error displayed
     */
    function onError(){
        var modal_error = '<div id="modal"><h3>An error has ocurred</h3><p>Please, try again later</p><p>Sorry for the inconveniences</p><a id="email_accept" href="#">OK</a></div>';
        $('body').append('<div id="popup_background"></div>');
        $('#popup_background').height($('html').height());
        $('#popup_background').append(modal_error);
        $('#email_accept').click(function(){
            $('#popup_background').hide();
            $('#email_notif').hide();
            $('#boton_try').addClass('boton_try_des');
        });
        $('#try_for_free_button').unbind();
    };

});
