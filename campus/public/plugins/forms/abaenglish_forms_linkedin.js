var resultfinal;
// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {
    IN.User.authorize(function(){
        getProfileData();
    });
    //IN.Event.on(IN, "auth", getProfileData);
}

// Handle the successful return from the API call
function onSuccess(data) {
    console.log(data);

    resultfinal =data.values[0]

    abaIN.checkUser(resultfinal);

}

// Handle an error response from the API call
function onError(error) {
    console.log(error);
}

// Use the API call wrapper to request the member's basic profile data
function getProfileData() {
    IN.API.Profile("me").fields("id", "first-name", "last-name", "email-address").result(onSuccess).error(onError);
}

var abaIN = {
    t :         '',
    e :         '',
    a :         '',
    setEmail :  function (e) { this.e = e; },
    getEmail :  function () { return this.e; },
    setAction : function (a) { this.a = a; },
    getAction : function () { return this.a; },
    isset :     function () {
        try {
            var a = arguments, l = a.length, i = 0, undef;
            if (l === 0) {
                throw new Error('Empty isset');
            }
            while (i !== l) {
                if (a[i] === undef || a[i] === null) {
                    return false;
                }
                i++;
            }
            return true;
        }
        catch (err){ return false; }
    },
    blockAction : function (a, e){
        if(a) {
            document.getElementById(e).disabled = true;
            document.getElementById(e).style.opacity = "0.4";
            document.getElementById(e).style.filter = "alpha(opacity=40)";
        }
        else {
            if (e != null && document.getElementById(e)) {
                console.log(e);
                document.getElementById(e).disabled = false;
                document.getElementById(e).style.opacity = "1";
                document.getElementById(e).style.filter = "alpha(opacity=100)";
            }
        }
    },
    checkUser : function (resultado){
        try {
            abaIN.setAction('exists');

            var url_action =    "/es/site/inLogin";
            var data =          {};

            data['INAction']    =   abaIN.getAction();
            data['result'] =   resultado;

            var r = new XMLHttpRequest();
            r.onreadystatechange = function () {
                if (r.readyState == 4 || r.status == 200){
                    abaIN.showInContainer(r.responseText);
                    window.location.href = r.responseURL;
                } else {
                    abaIN.showInContainer(r.responseText);
                }
                return;
            };
            r.open("POST", url_action, true);
            r.send(data);
        }
        catch (err){
            abaIN.blockAction(false, 'btnFbSubmit');
            console.log(err);
        }
    },
    sendUser : function (){
        try {
            var url_action =    "/es/site/inlogin";
            var data =          {};
            data['result']    =   resultfinal;

            var els = document.querySelectorAll("#login-form-fb input");
            for (var i = 0, len = els.length; i < len; i++) {
                var el = els[i];
                data[el.getAttribute('name')] = el.getAttribute('value');
            }

            var r = new XMLHttpRequest();
            r.onreadystatechange = function () {
                if (r.readyState == 4 || r.status == 200){
                    abaIN.showInContainer(r.responseText);
                    window.location.href = r.responseURL;
                } else {
                    abaIN.showInContainer(r.responseText);
                }
                return;
            };
            r.open("POST", url_action, true);
            r.send(data);
        }
        catch (err) {
            abaIN.blockAction(false, 'btnFbSubmitReg');
        }
    },
    showInContainer : function(content) {
        var el = document.getElementById("aba-login-error-container");
        el.innerHTML = content;

        return true;
    }
};