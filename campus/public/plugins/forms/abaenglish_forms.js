/**
 * ABAEnglish form generator
 *
 * License: Proprietary - (C) 2016 English Worldwide, SL
 * Usage:
 *      <script src="abaenglish_forms_data.php"></script>
 *      <script src="abaenglish_forms.js"></script>
 *      <link rel="stylesheet" href="abaenglish_forms.css" />
 *      <script type="text/javascript">ABAEnglishForms.init();</script>
 * Options:
 *      formType {integer}: 1 for login form and 2 for signup form
 *      formClass {string}: styles class of form
 *      locale {string}: locale of the form labels and Campus
 *      id {string}: id of the form and preffix of element's id
 *      endPoint {string}: endpoint URL of the form for login process
 *      endPointName {string}: name of the predefined endPoint
 *      method {string}: GET or POST methods
 *      rememberMe {boolean}: show the checkbox to remember the login session
 *      recoverPassword {boolean}: shows the recover password link
 *      facebookLogin {boolean}: shows the Facebook login button
 *      linkedinLogin {boolean}: shows the LinkedIn login button
 *      appendsTo {string}: id of element to append the form
 *      showPlaceholders {boolean}: shows placeholders in input fields
 *      legalLinks {boolean}: shows the legal links in the form
 *      autocomplete {boolean}: allow autocomplete in input fields
 *      schema {object}: form schema (json)
 *      additionalSchema {object}: additional form schema (json) to be added to the form
 *      additionalSchemaPreSubmit {object}: additional form schema (json) to be added to the form previous to the submit button
 *      schemaName {string}: name of predefined schema
 *      validate {boolean}: uses HTML5 Form validation
 */
var ABAEnglishForms = function() {

    /**
     * Enum of the types of forms
     */
    var formsType = {LOGIN: 1, SIGNUP: 2};

    /**
     * Default form action URL components
     */
    var form_action_url_domain = ABAENGLISH_B2C_domain;
    var form_login_url_b2c = ABAENGLISH_B2C_domain+'/{locale}/login';
    var form_login_url_b2b = ABAENGLISH_B2B_domain+'/{locale}/login';

    var form_register_url_signup_b2c = ABAENGLISH_B2C_domain+'/{locale}/external/signup';
    var form_register_url_signup_b2c_partner = ABAENGLISH_B2C_domain+'/{locale}/external/signuppartner';
    var form_register_url_signup_b2c_objectives = ABAENGLISH_B2C_domain+'/{locale}/external/signupobjectives';
    var form_register_url_signup_b2b = ABAENGLISH_B2B_domain+'/external/signup';

    /**
     * Facebook app id
     */
    var FB_app_id = '976321839048548';

    /**
     * LinkedIn app key
     */
    var IN_app_key = '77l1t5hni37pnz';

    /**
     * Form options
     */
    var formOptions;

    /**
     * Default form options
     */
    var defaultFormOptions = {
        formType: formsType.LOGIN,
        formClass: '',
        locale: 'es',
        id: 'aba-login-form',
        endPoint: '',
        endPointName: '',
        method: 'POST',
        rememberMe: true,
        recoverPassword: true,
        facebookLogin: false,
        linkedinLogin: false,
        appendsTo: '',
        showPlaceholders: true,
        legalLinks: false,
        autocomplete: false,
        schema: '',
        additionalSchema: '',
        additionalSchemaPreSubmit: '',
        schemaName: '',
        validate: true
    };

    /**
     * Main handler
     *
     * @returns {boolean}
     */
    var handleInit = function () {
        formUtil.formCreate(formOptions.id,formOptions.appendsTo,formOptions.schema,formOptions.formType,formOptions.formClass,formOptions.validate,formOptions.method);
        return true;
    };

    var schemaLoginDefault = function() {
        return {
            row1: {
                type: 'row',
                email: {
                    label: _i18n('email'),
                    className: 'ae-1-1',
                    type: 'text',
                    value: '',
                    name: 'LoginForm[email]',
                    pattern: '^(.[^@]+)@(.+)$',
                    placeholder: _i18n('email'),
                    required: true
                },
                password: {
                    label: _i18n('password'),
                    className: 'ae-1-1',
                    type: 'password',
                    value: '',
                    name: 'LoginForm[password]',
                    placeholder: _i18n('password'),
                    required: true
                },
                submit: {
                    label: '',
                    className: 'ae-1-1',
                    name: 'btnSubmit',
                    type: 'submit',
                    value: _i18n('submit')
                }
            }
        };
    };

    var schemaRegisterDefault = function() {
        return {
            row1: {
                type: 'row',
                legend: _i18n('submit'),
                name: {
                    label: _i18n('register_name'),
                    className: 'ae-1-1',
                    type: 'text',
                    value: '',
                    name: 'name',
                    id: 'name',
                    placeholder: _i18n('register_name'),
                    required: true
                },
                email: {
                        label: _i18n('email'),
                        className: 'ae-1-1',
                        type: 'text',
                    value: '',
                    name: 'email',
                    id: 'email',
                    pattern: '^(.[^@]+)@(.+)$',
                    placeholder: _i18n('email'),
                    required: true
                },
                password: {
                    label: _i18n('register_password'),
                    className: 'ae-1-1',
                    type: 'password',
                    value: '',
                    name: 'password',
                    id: 'password',
                    pattern: '',
                    placeholder: _i18n('register_password'),
                    required: true
                },
                submit: {
                    className: 'ae-1-1',
                    name: 'btnSubmit',
                    id: 'btnSubmit',
                    type: 'submit',
                    value: _i18n('register_submit')
                }
            }
        };
    };

    var schemaRegisterDefaultSurname = function() {
        return {
            row1: {
                type: 'row',
                legend: _i18n('submit'),
                name: {
                    label: _i18n('register_name'),
                    className: 'ae-1-1',
                    type: 'text',
                    value: '',
                    name: 'name',
                    id: 'name',
                    placeholder: _i18n('register_name'),
                    required: true
                },
                surname: {
                    label: _i18n('register_surname'),
                    className: 'ae-1-1',
                    type: 'text',
                    value: '',
                    name: 'surname',
                    id: 'surname',
                    placeholder: _i18n('register_surname'),
                    required: true
                },
                email: {
                    label: _i18n('email'),
                    className: 'ae-1-1',
                    type: 'text',
                    value: '',
                    name: 'email',
                    id: 'email',
                    pattern: '^(.[^@]+)@(.+)$',
                    placeholder: _i18n('email'),
                    required: true
                },
                password: {
                    label: _i18n('register_password'),
                    className: 'ae-1-1',
                    type: 'password',
                    value: '',
                    name: 'password',
                    id: 'password',
                    pattern: '',
                    placeholder: _i18n('register_password'),
                    required: true
                },
                submit: {
                    className: 'ae-1-1',
                    name: 'btnSubmit',
                    id: 'btnSubmit',
                    type: 'submit',
                    value: _i18n('start_now')
                }
            }
        };
    };

    var schemaRegisterLanding = function() {
        return {
            row1: {
                type: 'row',
                legend: _i18n('submit'),
                name: {
                    label: _i18n('register_name'),
                    className: 'ae-c ae-1-1',
                    type: 'text',
                    value: '',
                    name: 'name',
                    id: 'name',
                    required: true
                },
                surname: {
                    label: _i18n('register_surname'),
                    className: 'ae-c ae-1-1',
                    type: 'text',
                    value: '',
                    name: 'surname',
                    id: 'surname',
                    required: true
                },
                email: {
                    label: _i18n('register_email'),
                    className: 'ae-c ae-1-1',
                    type: 'text',
                    value: '',
                    name: 'email',
                    id: 'email',
                    pattern: '^(.[^@]+)@(.+)$',
                    required: true
                },
                password: {
                    label: _i18n('register_password'),
                    className: 'ae-c ae-1-1',
                    type: 'password',
                    value: '',
                    name: 'password',
                    id: 'password',
                    required: true
                },
                code1: {
                    label: _i18n('register_landing_couponcode'),
                    className: 'ae-c ae-1-1',
                    type: 'text',
                    value: '',
                    name: 'code1',
                    id: 'code1',
                    required: true
                },
                accept_legal: {
                    label: _i18n('register_landing_legal'),
                    type: 'checkbox',
                    value: '1',
                    name: 'accept_legal',
                    id: 'accept_legal',
                    required: true
                },
                submit: {
                    /*className: 'ae-c ae-1-1',*/
                    name: 'btnSubmit',
                    id: 'btnSubmit',
                    type: 'submit',
                    value: _i18n('register_landing_submit')
                }
            }
        };
    };

    var schemaRegisterCorporate = function() {
        return {
            row1: {
                type: 'row',
                legend: _i18n('submit'),
                enterpriseName: {
                    label: _i18n('register_corporate_organ'),
                    className: 'ae-1-1',
                    type: 'text',
                    value: '',
                    name: 'enterpriseName',
                    id: 'enterpriseName',
                    required: true,
                },
                enterpriseType: {
                    label: _i18n('register_corporate_type'),
                    className: 'ae-1-1',
                    type: 'select',
                    optionBlank: _i18n('register_corporate_type_option_blank'),
                    options: {1:_i18n('register_corporate_type_option_1'),2:_i18n('register_corporate_type_option_2'),3:_i18n('register_corporate_type_option_3')},
                    name: 'enterpriseType',
                    id: 'enterpriseType',
                    required: true,
                },
                name: {
                    label: _i18n('register_corporate_name'),
                    className: 'ae-1-1',
                    type: 'text',
                    value: '',
                    name: 'name',
                    id: 'name',
                    required: true
                },
                surname: {
                    label: _i18n('register_corporate_surname'),
                    className: 'ae-1-1',
                    type: 'text',
                    value: '',
                    name: 'surname',
                    id: 'surname',
                    required: true
                },
                phone: {
                    label: _i18n('register_corporate_phone'),
                    className: 'ae-1-1',
                    type: 'text',
                    value: '',
                    name: 'phone',
                    id: 'phone',
                    required: true
                },
                email: {
                    label: _i18n('register_corporate_email'),
                    className: 'ae-1-1',
                    type: 'text',
                    value: '',
                    name: 'email',
                    id: 'email',
                    pattern: '^(.[^@]+)@(.+)$',
                    required: true
                },
                password: {
                    label: _i18n('register_corporate_password'),
                    className: 'ae-1-1',
                    type: 'password',
                    value: '',
                    name: 'password',
                    id: 'password',
                    required: true
                },
                submit: {
                    className: 'ae-c ae-1-1',
                    name: 'btnSubmit',
                    id: 'btnSubmit',
                    type: 'submit',
                    value: _i18n('register_corporate_submit')
                },
                legal: {
                    label: _i18n('register_corporate_legal'),
                    className: 'ae-c ae-1-1 corporate_legal',
                    type: 'textlayer',
                },
            }
        };
    };

    var formUtil = {

        parse: function (json_text) {
            console.log(JSON.parse(json_text));
        },

        createLabel: function (htmlFor, className) {

            var oLabel;
            oLabel = document.createElement("label");
            oLabel.htmlFor = htmlFor;
            oLabel.id = "label-" + oLabel.htmlFor;
            oLabel.className = "ae-c " + className;

            return oLabel;
        },

        createTNode: function(labelText, bRequired) {

            var tNode = document.createElement("span");
            tNode.innerHTML = labelText;

            if (formOptions.validate) {
                if (bRequired) {

                    var bRequiredNode = document.createElement("span");

                    bRequiredNode.className = "bRequired";
                    bRequiredNode.appendChild(document.createTextNode('*'));
                    tNode.appendChild(bRequiredNode);

                }
            }

            return tNode;
        },

        createInput: function(type, name, id, value, required, pattern, placeholder, className, checked){

            var oInput;

            oInput = document.createElement("input");
            oInput.type = type;
            oInput.name = name;
            oInput.id = id;
            if (typeof placeholder !== "undefined" && formOptions.showPlaceholders) oInput.placeholder = placeholder;
            if (type == 'text' || type == 'password') {
                if (formOptions.autocomplete) {
                    oInput.autocomplete = "on";
                } else {
                    oInput.autocomplete = "off";
                }
            }
            if (typeof className !== "undefined") oInput.className = className;

            oInput['value'] = value;

            if (formOptions.validate) {
                if (required) {

                    oInput.required = true;

                    if (type !== 'checkbox' && type !== 'radio' && type !== 'hidden' && type !== 'button' && type !== 'submit' && type !== 'reset' && type !== 'password') {
                        if (typeof pattern !== "undefined" && pattern != '') oInput.pattern = pattern;
                        oInput.onblur = formUtil.validate(oInput);
                    }
                }
            }

            if (type == 'checkbox' || type == 'radio') {
                if (checked) oInput.checked = "checked";
            }

            return oInput;
        },

        formCreate: function(id, appendsTo, schema, formType, formClass, validate, method) {

            var form = document.createElement('form');

            if (formClass != '') { form.className = formClass; } else { form.className = "ae-w"; }
            form.id = id;
            form.action = _form_action_url();
            form.method = method;
            if(!validate)    form.noValidate = true;

            if (appendsTo != null && appendsTo != '') {
                document.getElementById(appendsTo).appendChild(form);
            } else {
                var referenceNode = document.currentScript;
                referenceNode.parentNode.insertBefore(form, referenceNode.currentScript);
            }

            if (form.addEventListener) {

                form.addEventListener('submit', function(e) {formUtil.formSubmit(form.id, e)}, false);

            } else if (form.attachEvent) {

                form.attachEvent('onsubmit', function(e) {formUtil.formSubmit(form.id, e)});
            }

            formUtil.parseRecursive(form, schema, j=0);

            switch (formType)
            {
                case 1:
                    _extendFormForLogin(form);
                    break;
                case 2:
                    _extendFormSignup(form);
                    break;
            }

        },

        parseRecursive: function (form, object, j) {

            if (typeof j == 'undefined') j = 0;

            var i=0;

            for (var key in object) {

                if (typeof object[key] == "object" && object[key] !== null && typeof object[key].type !== "undefined") {

                    i++;
                    if (object[key].type == "row") {

                        j++;

                        var tNode;

                        var oRow = document.createElement('fieldset');
                        var oLegend = document.createElement('legend');
                        tNode = formUtil.createTNode(object[key].legend, false);

                        oRow.className = "ae-r";
                        oRow.id = "row-" + j;

                        oLegend.appendChild(tNode);
                        oRow.appendChild(oLegend);

                        form.appendChild(oRow);

                    } else if (object[key].type == "textlayer") {

                        var oLayer = document.createElement('div');
                        oLayer.innerHTML = object[key].label;
                        oLayer.className = object[key].className;

                        document.getElementById('row-' + j).appendChild(oLayer);

                    } else {

                        var sType = object[key].type;
                        var kIndex;
                        if (typeof object[key].id !== "undefined" && object[key].id != '') { kIndex = object[key].id; } else { kIndex = sType+"-" + i + "-" + j; }
                        var bRequired = object[key].required == true;
                        var sPlaceholder = object[key].placeholder;
                        var oLabel = formUtil.createLabel (kIndex, object[key].className);

                        tNode = formUtil.createTNode (object[key].label, bRequired);

                        if (sType == "select") {

                            var oSelect = document.createElement("select");
                            oSelect.id = kIndex;
                            var oOptions = object[key].options;

                            if (typeof object[key].optionBlank !== "undefined")
                            {
                                var aTag = document.createElement("option");
                                aTag.value = '';
                                aTag.appendChild(document.createTextNode(object[key].optionBlank));
                                oSelect.appendChild(aTag);
                            }

                            for (var item in oOptions){

                                var aTag = document.createElement("option");
                                aTag.value = item;
                                aTag.appendChild(document.createTextNode(oOptions[item]));
                                oSelect.appendChild(aTag);

                            }
                            oLabel.appendChild(tNode);
                            oLabel.appendChild(oSelect);

                        } else {

                            var name = '';

                            if (typeof object[key].name !== 'undefined') { name = object[key].name; } else { name = key; }

                            var oInput = formUtil.createInput(sType, name, kIndex, object[key].value, bRequired, object[key].pattern,sPlaceholder,object[key].className);

                            if (sType == "text" || sType == 'reset' || sType == 'hidden' || sType == 'password') {

                                if (sType == 'hidden') {

                                    oLabel = oInput;

                                } else {

                                    oLabel.appendChild(tNode);
                                    oLabel.appendChild(oInput);

                                }
                            }

                            if (sType == "checkbox" || sType == "radio") {

                                oLabel.appendChild(oInput);
                                oLabel.appendChild(tNode);

                            }
                        }

                        if (sType == 'button' || sType == 'submit')
                        {
                            var oLayerButton = document.createElement('div');
                            oLayerButton.innerHTML = '';
                            oLayerButton.className = object[key].className;
                            oLayerButton.appendChild(oInput);
                            document.getElementById('row-' + j).appendChild(oLayerButton);
                        } else {
                            document.getElementById('row-' + j).appendChild(oLabel);
                        }
                    }

                    if(typeof oRow !== 'undefined') document.getElementById(formOptions.id).appendChild(oRow);

                    formUtil.parseRecursive(form, object[key], j);

                }
            }
        },
        test: {
            notBlank: function(data){

                return (typeof data === 'string') ? (String(data).trim().length > 0) : false;

            },

            pattern: function(data,sRegex) {
                var str = String(data);
                var patt = new RegExp (sRegex);
                return (typeof data === 'string') ? (patt.test(str)) : false;

            },

            minLength: function (data, nLength){

                return (typeof data === 'string') ? (String(data).trim().length >= nLength) : false;

            },

            maxLength: function (data, nLength){

                return (typeof data === 'string') ? (String(data).trim().length <= nLength) : false;

            }
        },

        validate: function(element){

            var data = element.value;
            var pattern = (typeof element.getAttribute('pattern') !== 'undefined') ? element.getAttribute('pattern') : '';
            var minLength = (typeof element.getAttribute('minLength') !== 'undefined') ? element.getAttribute('minLength') : 0;
            var maxLength = (typeof element.getAttribute('maxLength') !== 'undefined') ? element.getAttribute('maxLength') : 0;

            var notBlankTest = formUtil.test.notBlank(data);
            var patternTest = (pattern !== '') ? formUtil.test.pattern(data, pattern) : true;
            var minTest = (minLength > 0) ? formUtil.test.minLength(data, minLength) : true;
            var maxTest = (maxLength > 0) ? formUtil.test.maxLength(data, maxLength) : true;
            var lengthMinMaxTest = (minLength > 0 && maxLength > 0) ? (minTest && maxTest) : true;

            return ( notBlankTest && patternTest && minTest && maxTest && lengthMinMaxTest);
        },

        checkIt: function (element) {
            var errorClassPatt = "\b/error\b/";
            var currentClasses;

            if (!formUtil.validate(element)) {

                currentClasses = element.className;

                if (!formUtil.test.pattern(currentClasses, errorClassPatt)) {

                    element.className = "error " + currentClasses;

                }

            } else {

                currentClasses = element.className;

                if (!formUtil.test.pattern(currentClasses, errorClassPatt)) {

                    element.className = currentClasses.replace("error ", "");

                }
            }
            return formUtil.validate(element);
        },

        formSubmit: function(id, event) {
            event.preventDefault();

            var errors = 0;
            var elements = document.getElementById(id).elements;

            for (var i = 0, len = elements.length; i<len ; i++) {

                if (formOptions.validate) {
                    var required = !!(typeof elements[i].getAttribute('required') !== 'undefined'
                    && (elements[i].getAttribute('required') == true
                    || elements[i].getAttribute('required') == 'required'));

                    if (required) {

                        if (formUtil.checkIt(elements[i])) {

                            errors++;

                        }

                    }
                }
                if (errors == 0) {

                    document.getElementById(id).submit();

                } else {
                    // show general error message!
                    // each field, however, should show it's own error message (the CSS for it is already there)
                    // upon creating a field, create a new text node (there's a function) with the class error.
                    // you should also take care about putting in all translations for it.
                    // Also, error messages for each one are simply dismissed on blur if they are correct
                    // or so I think.
                    // That's all folks!
                }
            }
        }
    };

    /**
     * Extends the target array changing the property value with the source. If source has extra properties
     * not contained in target, they are dismissed.
     *
     * @param target {array}
     * @param source {array}
     * @returns {array|{}}
     * @private
     */
    function _extend(target, source) {
        target = target || {};
        for (var prop in source) {
            if (target[prop] != null) {
                target[prop] = source[prop];
            }
        }
        return target;
    }

    /**
     * Validates if options are correct
     *
     * @param options {array}  form options
     * @returns {boolean}
     * @private
     */
    function _validateOptions(options) {
        return (_validateOptions_locale(options) && _validateOptions_endpoint(options));
    }

    /**
     * Validates if locale option is within the accepted locales for ABA English
     *
     * @param options {array}  form options
     * @returns {boolean}
     * @private
     */
    function _validateOptions_locale(options) {
        if (options.locale != null && options.locale != '') {
            return (ABAEnglishForms_I18N.locale_accepted.indexOf(options.locale) > -1);
        } else {
            return true;
        }
    }

    /**
     * Validates if endpoint is has a correct URL format
     *
     * @param options {array}  form options
     * @returns {boolean}
     * @private
     */
    function _validateOptions_endpoint(options) {
        if (options.endPoint != null && options.endPoint != '') {
            return (_isURL(options.endPoint) || options.endPoint == '#');
        } else {
            return true;
        }
    }

    /**
     * Checks if a string is a correctly format URL
     *
     * @param str {string} URL to check format
     * @returns {boolean}
     * @private
     */
    function _isURL(str) {
        var pattern = new RegExp('^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$');
        var pattern_relative = new RegExp('^/{1}(((/{1}\.{1})?[a-zA-Z0-9 ]+/?)+(\.{1}[a-zA-Z0-9]{2,4})?)$');

        return (pattern.test(str) || pattern_relative.test(str));
    }

    function _createFacebookButton(fieldset)
    {
        var div = document.createElement('div');
        div.className = 'ae-c ae-1-2';
        var fb = document.createElement('button');
        fb.setAttribute('type','button');
        fb.setAttribute('id',formOptions.id + '-facebookLogin');
        fb.innerHTML = '<i class="fa fa-facebook"></i> Facebook';
        div.appendChild(fb);
        fieldset.appendChild(div);

        var d = document.createElement('div');
        d.setAttribute('id','fb-root');
        document.getElementsByTagName('body')[0].appendChild(d);

        var fbjs = document.createElement('script');
        fbjs.setAttribute('type','text/javascript');
        fbjs.innerHTML = "var FB_AppId = '"+FB_app_id+"';\r\n";
        document.getElementsByTagName('body')[0].appendChild(fbjs);

        var fbjs2 = document.createElement('script');
        fbjs2.setAttribute('type','text/javascript');
        fbjs2.setAttribute('src',form_action_url_domain+'/plugins/forms/abaenglish_forms_facebook.js');
        document.getElementsByTagName('body')[0].appendChild(fbjs2);
    }

    function _createFacebookButton_events()
    {
        var fbjs_onload = "";
        fbjs_onload = fbjs_onload.concat("  var fbButton = document.getElementById('"+formOptions.id+"-facebookLogin');\r\n");
        fbjs_onload = fbjs_onload.concat("  fbButton.onclick = function() {\r\n");
        fbjs_onload = fbjs_onload.concat("      loginWithFacebook();\r\n");
        fbjs_onload = fbjs_onload.concat("  };\r\n");
        return fbjs_onload;
    }

    function _createLinkedInButton(fieldset)
    {
        var div = document.createElement('div');
        div.className = 'ae-c ae-1-2';
        var linked = document.createElement('button');
        linked.setAttribute('type','button');
        linked.setAttribute('id',formOptions.id + '-linkedinLogin');
        linked.innerHTML = '<i class="fa fa-linkedin"></i> Linkedin';
        div.appendChild(linked);
        fieldset.appendChild(div);

        var injs = document.createElement('script');
        injs.setAttribute('type','text/javascript');
        injs.setAttribute('src','//platform.linkedin.com/in.js');
        injs.innerHTML = 'api_key:'+IN_app_key+'\r\nauthorize: false\r\nlang: es_ES';
        document.getElementsByTagName('body')[0].appendChild(injs);

        var injs2 = document.createElement('script');
        injs2.setAttribute('type','text/javascript');
        injs2.setAttribute('src',form_action_url_domain+'/plugins/forms/abaenglish_forms_linkedin.js');
        document.getElementsByTagName('body')[0].appendChild(injs2);
    }

    function _createLinkedinButton_events()
    {
        var injs_onload = "";
        injs_onload = injs_onload.concat("  var inButton = document.getElementById('"+formOptions.id+"-linkedinLogin');\r\n");
        injs_onload = injs_onload.concat("  inButton.onclick = function() {\r\n");
        injs_onload = injs_onload.concat("      onLinkedInLoad();\r\n");
        injs_onload = injs_onload.concat("  };\r\n");
        return injs_onload;
    }

    function _createRememberMeInput(fieldset)
    {
        var oInput = formUtil.createInput('checkbox', 'rememberMe', 'rememberMe', '1', false, '', '', '', true);
        var oLabel = formUtil.createLabel ('rememberMe', 'label-remember-me  ae-1-2');
        var tNode = formUtil.createTNode (_i18n('rememberMe'), false);

        oLabel.appendChild(oInput);
        oLabel.appendChild(tNode);
        fieldset.appendChild(oLabel);
    }

    function _createRecoverPasswordLink(fieldset)
    {
        var div = document.createElement('div');
        div.className = "ae-c ae-1-2";
        var l = document.createElement('a');
        l.setAttribute('href',_form_recover_password_url());
        l.setAttribute('id', formOptions.id + '-reoverPassword');
        l.innerHTML = _i18n('recoverPassword');
        div.appendChild(l);
        fieldset.appendChild(div);
    }

    function _extendFormForLogin(form)
    {
        _extendFormForLogin_SocialLogins(form);
        _extendFormForLogin_Helpers(form);
    }

    function _extendFormForLogin_SocialLogins(form) {
        if (formOptions.facebookLogin || formOptions.linkedinLogin)
        {
            var fieldset;
            fieldset = document.createElement('fieldset');
            fieldset.className = "ae-r";
            var sep = document.createElement('div');
            sep.setAttribute('class','ae-c ae-1-1');
            sep.innerHTML = _i18n('o_separator');
            fieldset.appendChild(sep);
            form.appendChild(fieldset);

            fieldset = document.createElement('fieldset');
            fieldset.className = "ae-r";
            fieldset.id = formOptions.id+"-social-login";
            if (formOptions.facebookLogin) _createFacebookButton(fieldset);
            if (formOptions.linkedinLogin) _createLinkedInButton(fieldset);
            var ec = document.createElement('div');
            ec.setAttribute('class','ae-c ae-1-1');
            ec.setAttribute('id','aba-login-error-container');
            fieldset.appendChild(ec);
            form.appendChild(fieldset);

            var windowOnLoad = document.createElement('script');
            windowOnLoad.setAttribute('type','text/javascript');
            windowOnLoad_script = "";
            windowOnLoad_script = windowOnLoad_script.concat("window.onload = function() {\r\n");
            if (formOptions.facebookLogin)  windowOnLoad_script = windowOnLoad_script.concat(_createFacebookButton_events());
            if (formOptions.linkedinLogin)  windowOnLoad_script = windowOnLoad_script.concat(_createLinkedinButton_events());
            windowOnLoad_script = windowOnLoad_script.concat("};\r\n");
            windowOnLoad.innerHTML = windowOnLoad_script;
            document.getElementsByTagName('body')[0].appendChild(windowOnLoad);
        }
    }

    function _extendFormForLogin_Helpers(form) {
        if (formOptions.rememberMe || formOptions.recoverPassword)
        {
            var fieldset = document.createElement('fieldset');
            fieldset.className = "ae-r";
            fieldset.id = formOptions.id+"-helpers-login";
            if (formOptions.rememberMe) _createRememberMeInput(fieldset);
            if (formOptions.recoverPassword) _createRecoverPasswordLink(fieldset);
            form.appendChild(fieldset);
        }
    }

    function _extendFormSignup(form) {
        _extendFormForSignup_Helpers(form);
    }

    function _extendFormForSignup_Helpers(form) {
        if (formOptions.legalLinks)
        {
            var fieldset = document.createElement('fieldset');
            fieldset.className = "ae-r";
            fieldset.id = formOptions.id+"-helpers-signup";
            var div = document.createElement('div');
            div.className = "ae-c ae-1-1";
            div.innerHTML = _i18n('register_legal');
            fieldset.appendChild(div);
            form.appendChild(fieldset);
        }
    }

    /**
     * Obtains the form endpoint URL
     *
     * @returns {string}
     * @private
     */
    function _form_action_url() {
        var form_action_url;
        var endPoint = formOptions.endPoint;
        var endPointName = formOptions.endPointName;

        if (endPoint == '' && typeof endPoint !== 'undefined' ) {
            if (endPointName == '' || typeof endPointName === 'undefined') {
                if (formOptions.formType == 1) {
                    form_action_url = form_login_url_b2c;
                } else {
                    form_action_url = form_register_url_signup_b2c;
                }
            } else {
                switch (endPointName)
                {
                    case 'login_b2c':
                        form_action_url = form_login_url_b2c;
                        break;
                    case 'login_b2b':
                        form_action_url = form_login_url_b2b;
                        break;
                    case 'signup_b2c':
                        form_action_url = form_register_url_signup_b2c;
                        break;
                    case 'signup_b2c_partner':
                        form_action_url = form_register_url_signup_b2c_partner;
                        break;
                    case 'signup_b2c_objectives':
                        form_action_url = form_register_url_signup_b2c_objectives;
                        break;
                    case 'signup_b2b':
                        form_action_url = form_register_url_signup_b2b;
                        break;
                }
            }
        } else {
            form_action_url = endPoint;
        }
        form_action_url = form_action_url.replace('{locale}',formOptions.locale);
        return form_action_url;
    }

    /**
     * Obtains the recover password url
     *
     * @returns {string}
     * @private
     */
    function _form_recover_password_url() {
        return form_action_url_domain + '/recover-password?language=' + formOptions.locale;
    }

    /**
     * Obtains the I18N label of a key
     *
     * @param key {string}  key of the label
     * @returns {string}
     * @private
     */
    function _i18n(key) {
        return ABAEnglishForms_I18N.translate(key,formOptions.locale);
    }

    var handleSchema = function() {
        if (typeof formOptions.schemaName !== 'undefined' && formOptions.schemaName != '') {
            switch (formOptions.schemaName)
            {
                case 'signup_basic':
                    formOptions.schema = schemaRegisterDefault();
                    break;
                case 'signup_basic_surname':
                    formOptions.schema = schemaRegisterDefaultSurname();
                    break;
                case 'signup_extended':
                    formOptions.schema = schemaRegisterDefault();
                    break;
                case 'signup_landing':
                    formOptions.schema = schemaRegisterLanding();
                    break;
                case 'signup_corporate':
                    formOptions.schema = schemaRegisterCorporate();
                    break;
            }
        } else {
            switch (formOptions.formType)
            {
                case 1:
                    formOptions.schema = schemaLoginDefault();
                    break;
                case 2:
                    formOptions.schema = schemaRegisterDefault();
                    break;
            }
        }
    };

    var handleOptions = function (options) {
        formOptions = defaultFormOptions;
        if (typeof options !== 'undefined' && options != null && Object.keys(options).length != 0)
        {
            if (_validateOptions(options))
            {
                formOptions = _extend(formOptions,options);
            } else {
                return false;
            }
        }
        if (formOptions.schema == '')   handleSchema();

        if (options.additionalSchema != null && options.additionalSchema != '')
        {
            for (var attrname in options.additionalSchema) { formOptions.schema[attrname] = options.additionalSchema[attrname]; }
        }
        return true;
    };

    return {
        init: function (options) {
            optionsValidate = handleOptions(options);
            if (optionsValidate) {
                handleInit();
                return true;
            } else {
                return false;
            }
        }
    };

}();

var ABAEnglishFormsLogin = function() {
    return {
        init: function (options) {
            options.formType = 1;
            return ABAEnglishForms.init(options)
        }
    };
}();

var ABAEnglishFormsSignup = function() {
    return {
        init: function (options) {
            options.formType = 2;
            delete options.rememberMe;
            delete options.recoverPassword;
            delete options.facebookLogin;
            delete options.linkedinLogin;
            return ABAEnglishForms.init(options)
        }
    };
}();

var ABAEnglishForms_I18N = function() {

    /**
     * Accepted locales
     */
    var locale_accepted = ['es', 'en', 'it', 'fr', 'pt', 'de', 'ru'];

    /**
     * I18N labels
     */
    var labels = {
        email: { es:"Email", en:"Email", it:"E-mail", fr:"Email", pt:"Email", de:"E-mail", ru:"Адрес эл. почты" },
        password: { es:"Contraseña", en:"Password", it:"Password", fr:"Mot de passe", pt:"Senha", de:"Passwort", ru:"Пароль" },
        submit: { es:"Iniciar Sesión", en:"Log in", it:"Inizia sessione", fr:"Connexion", pt:"Fazer Login", de:"Anmelden", ru:"Начать сеанс" },
        rememberMe: { es:"Recordar mis datos", en:"Stay logged in", it:"Ricorda i miei dati", fr:"Enregistrer mes données", pt:"Lembrar-me", de:"Meine Daten merken", ru:"Запомнить мои данные" },
        recoverPassword: { es:"¿Olvidaste tu contraseña?", en:"Forgot your password?", it:"Non ti ricordi la password?", fr:"Vous avez oublié le mot de passe?", pt:"Esqueceu sua senha?", de:"Passwort vergessen?", ru:"Забыл свой пароль?" },
        o_separator: { es:"o inicia sesión con", en:"or log in using", it:"o accedi con", fr:"ou accédez avec", pt:"ou entre com o", de:"oder", ru:"или войдите через" },
        register_name: { es:"Nombre", en:"Name", it:"Nome", fr:"Prénom", pt:"Nome", de:"Name", ru:"Имя и фамилия" },
        register_email: { es:"Email", en:"Email", it:"E-mail", fr:"Email", pt:"Email", de:"E-mail", ru:"Адрес эл. почты" },
        register_surname: { es:"Apellidos", en:"Surname", it:"Cognome", fr:"Nom", pt:"Sobrenome", de:"Nachname", ru:"Имя и фамилия" },
        register_password: { es:"Crea tu contraseña", en:"Password", it:"Crea la tua password", fr:"Créez votre mot de passe", pt:"Crie sua senha", de:"Erstelle dir ein Passwort", ru:"Создай пароль" },
        register_submit: { es:"Regístrate gratis", en:"Sign up for free", it:"Iscriviti gratis", fr:"Inscription gratuite", pt:"Cadastre-se grátis", de:"Kopstenlos registrieren", ru:"Зарегистрируйся бесплатно" },
        start_now: { es:"Empieza ya", en:"Start now", it:"Incomincia subito", fr:"Commencez maintenant", pt:"Comece já", de:"Starte noch heute", ru:"Начни сейчас" },
        register_legal: {
            es: 'Haciendo click en "Regístrate gratis" estás aceptando las <a class="abaLink-blue" href="http://www.abaenglish.com/es/condiciones-generales/">condiciones generales</a> y <a class="abaLink-blue" href="http://www.abaenglish.com/es/politica-de-privacidad/">políticas de privacidad</a>',
            en: 'By clicking on "Sign up for Free" you agree to the general <a class="abaLink-blue" href="http://www.abaenglish.com/en/general-terms-and-conditions/">terms and conditions</a> and the <a class="abaLink-blue" href="http://www.abaenglish.com/en/privacy-policy/">privacy policy</a>',
            it: 'Cliccando su "Iscriviti gratis" accetti le <a class="abaLink-blue" href="http://www.abaenglish.com/it/condizioni-generali/">condizioni generali</a> e la <a class="abaLink-blue" href="http://www.abaenglish.com/it/politica-di-privacy/">politica sulla privacy</a>',
            fr: 'En cliquant sur “Inscription gratuite” vous acceptez les <a class="abaLink-blue" href="http://www.abaenglish.com/fr/conditions-generales/">conditions générales</a> et les <a class="abaLink-blue" href="http://www.abaenglish.com/fr/politique-de-confidentialite/">politiques de confidentialité</a>',
            pt: 'Ao clicar em "Cadastre-se grátis", você aceita os <a class="abaLink-blue" href="http://www.abaenglish.com/pt/condicoes-gerais/">termos e condições</a> e a <a class="abaLink-blue" href="http://www.abaenglish.com/pt/politica-de-privacidade/">política de privacidade</a>:',
            de: 'Sobald du auf "Kostenlos Registrieren" klickst, akzeptierst du die <a class="abaLink-blue" href="http://www.abaenglish.com/de/allgemeine-geschaeftsbedingungen/">Nutzungsbedingungen</a> und die <a class="abaLink-blue" href="http://www.abaenglish.com/de/datenschutz/">Datenschutzpolitik</a>',
            ru: 'Нажимая на "Зарегистрируйся бесплатно", ты соглашаешься с <a class="abaLink-blue" href="http://www.abaenglish.com/ru/obshchiye-usloviya/">пользовательское соглашение</a> и <a class="abaLink-blue" href="http://www.abaenglish.com/ru/politika-konfidentsialnost/">политика конфиденциальности</a>',
        },
        register_landing_couponcode: { es:"Código del cupon", en:"Coupon code", it:"Codice del coupon", fr:"Code", pt:"Código do Cupom", de:"Gutscheincode", ru:"код сертификата" },
        register_landing_submit: { es:"Apúntate", en:"Sing in", it:"Iscriviti", fr:"Inscrivez-vous", pt:"Començar a aprender", de:"Einschreibung", ru:"зарегистрироваться" },
        register_landing_legal: {
            es: 'Acepto las <a title="" href="http://www.abaenglish.com/es/condiciones-generales/" class="pop-conpol fancybox">Condiciones generales</a> y la  <a id="privacy_legal" title="" href="http://www.abaenglish.com/es/politica-de-privacidad/" class="pop-conpol fancybox">Política de privacidad</a>',
            en: 'I accept the <a title="" href="http://www.abaenglish.com/en/general-terms-and-conditions/" class="pop-conpol fancybox">General Terms and Conditions</a> and the <a id="privacy_legal" title="" href="http://www.abaenglish.com/en/privacy-policy/" class="pop-conpol fancybox">Privacy Policy</a>',
            it: 'Accetto le <a title="" href="http://www.abaenglish.com/it/condizioni-generali/" class="pop-conpol fancybox">Condizioni generali</a> e <a id="privacy_legal" title="" href="http://www.abaenglish.com/it/politica-di-privacy/" class="pop-conpol fancybox">Politica di privacy</a>',
            fr: 'J\'accepte les <a title="" href="http://www.abaenglish.com/fr/conditions-generales/" class="pop-conpol fancybox">Conditions générales</a> et la <a id="privacy_legal" title="" href="http://www.abaenglish.com/fr/politique-de-confidentialite/" class="pop-conpol fancybox">Politique de confidentialité</a>',
            pt: 'Aceito as <a title="" href="http://www.abaenglish.com/pt/condicoes-gerais/" class="pop-conpol fancybox">Condições gerais</a> e <a id="privacy_legal" title="" href="http://www.abaenglish.com/pt/politica-de-privacidade/" class="pop-conpol fancybox">Politica de privacidade</a>',
            de: 'Hiermit akzeptiere ich die <a title="" href="http://www.abaenglish.com/de/allgemeine-geschaeftsbedingungen/" class="pop-conpol fancybox">Geschäftsbedingungen</a> <a id="privacy_legal" title="" href="http://www.abaenglish.com/de/datenschutz/" class="pop-conpol fancybox">Datenschutz</a>',
            ru: 'Принять условия <a title="" href="http://www.abaenglish.com/ru/obshchiye-usloviya/" class="pop-conpol fancybox">Пользовательское соглашение</a> <a id="privacy_legal" title="" href="http://www.abaenglish.com/ru/politika-konfidentsialnost/" class="pop-conpol fancybox">Политика конфиденциальности</a>'
        },
        register_corporate_organ: { es:"Organización", en:"Name of the organisation", it:"Società", fr:"Nom de l'organisation", pt:"Nome da organização", de:"Unternehmen", ru:"Name of the organisation" },
        register_corporate_type: { es:"Tipo de organización", en:"Organisation Type", it:"Tipo di società", fr:"Type d'organisation", pt:"Tipo de organização", de:"Unternehmensart", ru:"Organisation Type" },
        register_corporate_type_option_blank: { es:"INTERESADO COMO", en:"CHOOSE AN OPTION", it:"SONO", fr:"INTÉRESSÉ EN TANT QUE", pt:"SELECIONE", de:"ALS HINTERGRUND", ru:"CHOOSE AN OPTION" },
        register_corporate_type_option_1: { es:"Empresa", en:"Company", it:"Azienda", fr:"Entreprise", pt:"Empresa", de:"Unternehmen", ru:"Company" },
        register_corporate_type_option_2: { es:"Educación", en:"Educational institution", it:"Ente educativo", fr:"Institution éducative", pt:"Instituição de ensino", de:"Education", ru:"Educational institution" },
        register_corporate_type_option_3: { es:"Institución pública", en:"Public institution", it:"Istituzione pubblica", fr:"Institution publique", pt:"Instituição pública", de:"öffentliche Einrichtung", ru:"Public institution" },
        register_corporate_name: { es:"Nombre", en:"Name", it:"Nome", fr:"Prénom", pt:"Nome", de:"Name", ru:"Name" },
        register_corporate_surname: { es:"Apellidos", en:"Surname", it:"Cognome", fr:"Nom", pt:"Sobrenome", de:"Nachname", ru:"Surname" },
        register_corporate_phone: { es:"Teléfono", en:"Phone number", it:"Telefono", fr:"Téléphone", pt:"Telefone", de:"Telefon", ru:"телефон" },
        register_corporate_email: { es:"Correo electrónico profesional", en:"Work email address", it:"Indirizzo email professionale", fr:"Adresse e-mail professionnelle", pt:"E-mail profissional", de:"Berufliche E-Mail-Adresse", ru:"Work email address" },
        register_corporate_password: { es:"Crea tu contraseña", en:"Password", it:"Crea la tua password", fr:"Créez votre mot de passe", pt:"Crie sua senha", de:"Erstelle dir ein Passwort", ru:"Создай пароль" },
        register_corporate_submit: { es:"Crear", en:"Create", it:"Creare profilo", fr:"Créer", pt:"Criar", de:"Erstelle dein Konto", ru:"Create" },
        register_corporate_legal: {
            es:'Haciendo clic en "Crear" está aceptando las <a href="http://www.abaenglish.com/es/condiciones-generales/" target="_blank">condiciones generales</a> y la <a href="http://www.abaenglish.com/es/politica-de-privacidad/" target="_blank">política de privacidad</a>.º',
            en:'By clicking "Create" you have accepted the <a href="http://www.abaenglish.com/en/general-terms-and-conditions/">general terms and conditions</a> and <a href="http://www.abaenglish.com/en/privacy-policy/">privacy policy</a>.',
            it:'Cliccando su "Creare profilo" accetta le <a href="http://www.abaenglish.com/it/condizioni-generali/">condizioni generali</a> e la <a href="http://www.abaenglish.com/it/politica-di-privacy/">politica sulla privacy</a>.',
            fr:'En cliquant sur "Créer" vous acceptez les <a href="http://www.abaenglish.com/fr/conditions-generales/">conditions générales</a> et la <a href="http://www.abaenglish.com/fr/politique-de-confidentialite/">politique de confidentialité</a>.',
            pt:'Clicando em "Criar" você está aceitando os <a href="http://www.abaenglish.com/pt/condicoes-gerais/">termos e condições</a> e a <a href="http://www.abaenglish.com/pt/politica-de-privacidade/">política de privacidade</a>.',
            de:'By clicking "Create" you have accepted the <a href="http://www.abaenglish.com/en/general-terms-and-conditions/">general terms and conditions</a> and <a href="http://www.abaenglish.com/en/privacy-policy/">privacy policy</a>.',
            ru:'By clicking "Create" you have accepted the <a href="http://www.abaenglish.com/en/general-terms-and-conditions/">general terms and conditions</a> and <a href="http://www.abaenglish.com/en/privacy-policy/">privacy policy</a>.'
        },
    };

    /**
     * Validates if locale option is within the accepted locales for ABA English
     *
     * @param locale {string}  locale
     * @returns {boolean}
     * @private
     */
    function _validate_locale(locale) {
        if (typeof locale !== 'undefined' && locale != null && locale != '') {
            return (locale_accepted.indexOf(locale) > -1);
        } else {
            return false;
        }
    }

    /**
     * Validates if key is defined
     *
     * @param key {string}  locale
     * @returns {boolean}
     * @private
     */
    function _validate_key(key) {
        if (typeof key !== 'undefined' && key != null && key != '') {
            return labels.hasOwnProperty(key);
        } else {
            return false;
        }
    }

    return {
        translate: function (key,locale) {
            if (_validate_locale(locale) && _validate_key(key)) {
                return labels[key][locale];
            } else {
                return key;
            }
        },
        locale_accepted: locale_accepted
    };
}();