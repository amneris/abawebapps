// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

window.fbAsyncInit = function() {
    FB.init({
        appId: FB_AppId,
        status: true,
        cookie: true,
        xfbml: true,
        oauth: true,
        version: 'v2.1'
    });
};

function statusChangeCallback(response) {
    try {
        if(abaFb.isset(response.status)) {
            if (response.status === 'connected') {

                ar = FB.getAuthResponse();

                if(ar) {
                    if(abaFb.isset(ar.accessToken)) {
                        abaFb.setToken(ar.accessToken);
                    }
                    abaFb.checkUser();
                }
            }
            else{
                abaFb.blockAction(false, 'btnFbSubmit');
            }
        }
    }
    catch (err){
        abaFb.blockAction(false, 'btnFbSubmit');
    }
}

function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}

function loginWithFacebook() {
    FB.getLoginStatus(function(response){
        if(abaFb.isset(response.status) && response.status == 'connected'){
            checkLoginState();
        } else {
            FB.login(function(response) {
                statusChangeCallback(response);
            }, {scope : "public_profile,email"});
        }
    });
}

var abaFb = {
    t :         '',
    t :         '',
    e :         '',
    setToken :  function (t) { this.t = t; },
    getToken :  function () { return this.t; },
    setEmail :  function (e) { this.e = e; },
    getEmail :  function () { return this.e; },
    setAction : function (a) { this.a = a; },
    getAction : function () { return this.a; },
    isset :     function () {
        try {
            var a = arguments, l = a.length, i = 0, undef;
            if (l === 0) {
                throw new Error('Empty isset');
            }
            while (i !== l) {
                if (a[i] === undef || a[i] === null) {
                    return false;
                }
                i++;
            }
            return true;
        }
        catch (err){ return false; }
    },
    blockAction : function (a, e){
        if(a) {
            document.getElementById(e).disabled = true;
            document.getElementById(e).style.opacity = "0.4";
            document.getElementById(e).style.filter = "alpha(opacity=40)";
        }
        else {
            if (e != null && document.getElementById(e)) {
                console.log(e);
                document.getElementById(e).disabled = false;
                document.getElementById(e).style.opacity = "1";
                document.getElementById(e).style.filter = "alpha(opacity=100)";
            }
        }
    },
    checkUser : function (){
        try {
            abaFb.setAction('exists');

            var url_action =    "https://campus.abaenglish.com/es/site/fblogin";
            var data =          {};

            data['fbAction']    =   abaFb.getAction();
            data['accessToken'] =   abaFb.getToken();
            var dataString = "fbAction="+abaFb.getAction()+"&accessToken="+abaFb.getToken();

            var r = new XMLHttpRequest();
            r.onreadystatechange = function () {
                if (r.readyState == 4 || r.status == 200){
                    abaFb.showFbContainer(r.responseText);
                    window.location.href = r.responseURL;
                } else {
                    abaFb.showFbContainer(r.responseText);
                }
                return;
            };
            r.open("POST", url_action, true);
            r.send(dataString);
        }
        catch (err){
            abaFb.blockAction(false, 'btnFbSubmit');
        }
    },
    sendUser : function (){
        try {
            var url_action =    "https://campus.abaenglish.com/es/site/fblogin";
            var data =          {};
            var dataString = '';

            var els = document.querySelectorAll("#login-form-fb input");
            for (var i = 0, len = els.length; i < len; i++) {
                var el = els[i];
                data[el.getAttribute('name')] = el.getAttribute('value');
                dataString += el.getAttribute('name') + '=' + el.getAttribute('value') + '&';
            }

            var r = new XMLHttpRequest();
            r.onreadystatechange = function () {
                if (r.readyState == 4 || r.status == 200){
                    abaFb.showFbContainer(r.responseText);
                    window.location.href = r.responseURL;
                } else {
                    abaFb.showFbContainer(r.responseText);
                }
                return;
            };
            r.open("POST", url_action, true);
            r.send(dataString);
        }
        catch (err) {
            abaFb.blockAction(false, 'btnFbSubmitReg');
            console.log(err);
        }
    },
    showFbContainer : function(content) {
        var error_message = document.getElementById("error-message");
        error_message.innerHTML = content;

        var error_message_container = document.getElementById("error-message-container");
        error_message_container.style.display = 'block';

        return true;
    }
};
