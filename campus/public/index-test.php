<?php
// It includes all common files and folders shared between different projects:
require dirname(__FILE__) . '/../../common/bootstrapTest.php';

// These keys overwrite the ones in the table config in the database.
// It is useful for customize a local environment ignoring the development
// database and without annoying ur sensitive buddies.
if (file_exists(dirname(__FILE__) . '/protected/config/ProgrammerConfigKeysTest.php')) {
    include_once(dirname(__FILE__) . '/protected/config/ProgrammerConfigKeysTest.php');
}

// DEVELOPMENT, INTEGRATION and STAGE: ****
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
// LIVE ENVIRONMENT ****
//defined('YII_DEBUG') or define('YII_DEBUG', false);

$config = ROOT_DIR . '/campus/protected/config/test.php';
Yii::createWebApplication($config)->run();
